insert into attribute values (null,"SERIALIZEDTYPE","Type of Serialized",1,"ankur.garg@snapdeal.com","ankur.garg@snapdeal.com",now(),now(),now(),0);
CREATE TABLE `upload_sheet_field_domain` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class_name` varchar(255) NOT NULL,
  `field_name` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `last_updated` datetime NOT NULL,
  `updated_by` varchar(128) NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `field_name_value` (`class_name`,`field_name`,`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

 insert into upload_sheet_field_domain values (null,"ProductAttributeUploadDTO","serializedType","SERIAL_NO",now(),now(),"ankur.garg@snapdeal.com",now());

 insert into upload_sheet_field_domain values (null,"ProductAttributeUploadDTO","serializedType","IMEI",now(),now(),"ankur.garg@snapdeal.com",now());
 insert into upload_sheet_field_domain values (null,"ProductAttributeUploadDTO","serializedType","NONE",now(),now(),"ankur.garg@snapdeal.com",now());

