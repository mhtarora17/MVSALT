use cocofs;
alter table attribute                    add `version` bigint(20) unsigned NOT NULL DEFAULT '0';
alter table job_detail                   add `version` bigint(20) unsigned NOT NULL DEFAULT '0';
alter table job_detail_history           add `version` bigint(20) unsigned NOT NULL DEFAULT '0';
alter table product_attribute            add `version` bigint(20) unsigned NOT NULL DEFAULT '0';
alter table product_attribute_history    add `version` bigint(20) unsigned NOT NULL DEFAULT '0';
alter table seller_supc_mapping add UNIQUE KEY `seller_supc_UNIQUE` (`seller_code`,`supc`);
