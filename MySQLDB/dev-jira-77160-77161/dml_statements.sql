use cocofs;

insert into packaging_type_attribute (attribute,attribute_description,enabled,updated_by,created_by,created,last_updated,version) values ('packaging.type.category','Useful for categorizing packaging type based on dimensions',1,'cocofs@snapdeal.com',now(),now(),now(),1);

insert into packaging_type_properties(packaging_type_id, name, value, enabled,  created, last_updated, updated_by, updated) select packaging_type.id, 'packaging.type.category', '3D', 1, now(), now(), 'packman-dev', now() from packaging_type where packaging_mode = 'NORMAL';

