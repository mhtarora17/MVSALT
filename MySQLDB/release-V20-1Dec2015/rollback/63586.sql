use cocofs;

drop table `history_state_category_price_tax_rate`;

drop table `history_seller_category_tax_rate`;

drop table `history_seller_supc_tax_rate`;

alter table seller_supc_tax_rate drop column `tax_type`, drop column `version`;

alter table seller_category_tax_rate drop column `tax_type`,drop column `version`;

alter table state_category_price_tax_rate drop column `tax_type`,drop column `version`;


delete from cocofs_property where name = 'excel.sheet.max.value';

delete from cocofs_property where name = 'tax.rate.max.value';

delete from cocofs_property where name = 'round.off.scale';

delete from upload_sheet_field_domain where class_name = 'StateCategoryPriceTaxRateUploadDTO';

delete from upload_sheet_field_domain where class_name = 'SellerCategoryTaxRateUploadDTO';

delete from upload_sheet_field_domain where class_name = 'SellerSUPCTaxRateUploadDTO';

