use cocofs;


CREATE TABLE `history_state_category_price_tax_rate` (
  `id` int(10) unsigned NOT NULL,
  `state` varchar(30) NOT NULL,
  `category_url` varchar(128) NOT NULL,
  `tax_rate` decimal(10,2) DEFAULT NULL,
  `tax_type` enum('vat/cst','service_tax') NOT NULL DEFAULT 'vat/cst',
  `lower_price_limit` decimal(10,2) DEFAULT NULL,
  `upper_price_limit` decimal(10,2) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `created` timestamp NULL DEFAULT NULL,
  `last_updated` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `_REV_TYPE` varchar(10) NOT NULL,
  `REV` bigint(20) DEFAULT NULL
);


CREATE TABLE `history_seller_category_tax_rate` (
  `id` int(10) unsigned NOT NULL,
  `seller_code` varchar(20) NOT NULL,
  `category_url` varchar(128) NOT NULL,
  `tax_rate` decimal(10,2) DEFAULT NULL,
  `tax_type` enum('vat/cst','service_tax') NOT NULL DEFAULT 'vat/cst',
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `created` timestamp NULL DEFAULT NULL,
  `last_updated` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `_REV_TYPE` varchar(10) NOT NULL,
  `REV` bigint(20) DEFAULT NULL
);

CREATE TABLE `history_seller_supc_tax_rate` (
  `id` int(10) unsigned NOT NULL,
  `seller_code` varchar(20) NOT NULL,
  `supc` varchar(32) NOT NULL,
  `tax_rate` decimal(10,2) DEFAULT NULL,
  `tax_type` enum('vat/cst','service_tax') NOT NULL DEFAULT 'vat/cst',
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `created` timestamp NULL DEFAULT NULL,
  `last_updated` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `_REV_TYPE` varchar(10) NOT NULL,
  `REV` bigint(20) DEFAULT NULL
);



alter table seller_supc_tax_rate add column `tax_type` enum('vat/cst','service_tax') NOT NULL default 'vat/cst' after tax_rate, add column `version` bigint(10) unsigned DEFAULT '0';

alter table seller_category_tax_rate add column `tax_type` enum('vat/cst','service_tax') NOT NULL default 'vat/cst' after tax_rate, add column `version` bigint(10) unsigned DEFAULT '0';

alter table state_category_price_tax_rate add column `tax_type` enum('vat/cst','service_tax') NOT NULL default 'vat/cst' after tax_rate, add column `version` bigint(10) unsigned DEFAULT '0';

insert into cocofs_property values(null, 'excel.sheet.max.value','65000',now(),now(),now(),'cocofs-dev');

insert into cocofs_property values(null, 'tax.rate.max.value','100',now(),now(),now(),'cocofs-dev');

insert into cocofs_property values(null, 'round.off.scale','2',now(),now(),now(),'cocofs-dev');

insert into upload_sheet_field_domain values (null,'StateCategoryPriceTaxRateUploadDTO','taxType','vat/cst',now(),now(),'cocofs-dev',now());
insert into upload_sheet_field_domain values (null,'StateCategoryPriceTaxRateUploadDTO','taxType','service_tax',now(),now(),'cocofs-dev',now());
insert into upload_sheet_field_domain values (null,'StateCategoryPriceTaxRateUploadDTO','enabled','true',now(),now(),'cocofs-dev',now());
insert into upload_sheet_field_domain values (null,'StateCategoryPriceTaxRateUploadDTO','enabled','false',now(),now(),'cocofs-dev',now());

insert into upload_sheet_field_domain values (null,'SellerCategoryTaxRateUploadDTO','taxType','vat/cst',now(),now(),'cocofs-dev',now());
insert into upload_sheet_field_domain values (null,'SellerCategoryTaxRateUploadDTO','taxType','service_tax',now(),now(),'cocofs-dev',now());
insert into upload_sheet_field_domain values (null,'SellerCategoryTaxRateUploadDTO','enabled','true',now(),now(),'cocofs-dev',now());
insert into upload_sheet_field_domain values (null,'SellerCategoryTaxRateUploadDTO','enabled','false',now(),now(),'cocofs-dev',now());


insert into upload_sheet_field_domain values (null,'SellerSUPCTaxRateUploadDTO','taxType','vat/cst',now(),now(),'cocofs-dev',now());
insert into upload_sheet_field_domain values (null,'SellerSUPCTaxRateUploadDTO','taxType','service_tax',now(),now(),'cocofs-dev',now());
insert into upload_sheet_field_domain values (null,'SellerSUPCTaxRateUploadDTO','enabled','true',now(),now(),'cocofs-dev',now());
insert into upload_sheet_field_domain values (null,'SellerSUPCTaxRateUploadDTO','enabled','false',now(),now(),'cocofs-dev',now());



insert into attribute(attribute, attribute_description, enabled, updated_by, created_by,created,last_updated) values ('PRIMARYLENGTH', 'Primary length of product',1, 'cocofs-dev', 'cocofs-dev',now(), now());

insert into attribute(attribute, attribute_description, enabled, updated_by, created_by,created,last_updated) values ('PRIMARYBREADTH', 'Primary breadth of product',1, 'cocofs-dev', 'cocofs-dev',now(), now());

insert into attribute(attribute, attribute_description, enabled, updated_by, created_by,created,last_updated) values ('PRIMARYHEIGHT', 'Primary height of product',1, 'cocofs-dev', 'cocofs-dev',now(), now());





















