use cocofs;

drop TABLE if exists `history_polybag_type`;
drop TABLE if exists `history_carton_type`;
drop TABLE if exists `history_carton_slab`;
drop TABLE if exists `history_polybag_slab`;
drop TABLE if exists `history_surface_type`;

drop TABLE if exists `polybag_type`;
drop TABLE if exists `carton_type`;

drop TABLE if exists `carton_slab`;
drop TABLE if exists `polybag_slab`;

drop TABLE if exists `surface_type`;

CREATE TABLE `carton_slab` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
`name` varchar(15) NOT NULL,
`slab_size` int(10) unsigned NOT NULL,
`lower_limit` int(10) unsigned NOT NULL,
`created` timestamp NULL DEFAULT NULL,
  `last_updated` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

CREATE TABLE `history_carton_slab` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(15) NOT NULL,
`slab_size` int(10) unsigned NOT NULL,
`lower_limit` int(10) unsigned NOT NULL,
`created` timestamp NULL DEFAULT NULL,
  `last_updated` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

  `_REV_TYPE` varchar(10) NOT NULL,
  `REV` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `polybag_slab` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
`name` varchar(15) NOT NULL,
`slab_size` int(10) unsigned NOT NULL,
`lower_limit` int(10) unsigned NOT NULL,
`created` timestamp NULL DEFAULT NULL,
  `last_updated` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

CREATE TABLE `history_polybag_slab` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(15) NOT NULL,
`slab_size` int(10) unsigned NOT NULL,
`lower_limit` int(10) unsigned NOT NULL,
`created` timestamp NULL DEFAULT NULL,
  `last_updated` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

  `_REV_TYPE` varchar(10) NOT NULL,
  `REV` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `carton_type` (
`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
`code` varchar(15) NOT NULL,
`description` varchar(128) DEFAULT NULL,
`length` decimal(10,2) NOT NULL,
`breadth` decimal(10,2) NOT NULL,
`height` decimal(10,2) NOT NULL,
`vol_weight` decimal(10,2) NOT NULL,
  `slab_id` int(10) unsigned DEFAULT NULL ,
`created` timestamp NULL DEFAULT NULL,
`last_updated` timestamp NULL DEFAULT NULL,
`updated_by` varchar(255) NOT NULL,
`updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
PRIMARY KEY (`id`),
UNIQUE KEY `code_UNIQUE` (`code`),
CONSTRAINT `FK_carton_slab_id` FOREIGN KEY (`slab_id`) REFERENCES carton_slab(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

CREATE TABLE `history_carton_type` (
  `id` int(10) unsigned NOT NULL,
`code` varchar(15) NOT NULL,
`description` varchar(128) DEFAULT NULL,
`length` decimal(10,2) NOT NULL,
`breadth` decimal(10,2) NOT NULL,
`height` decimal(10,2) NOT NULL,
`vol_weight` decimal(10,2) NOT NULL,
  `slab_id` int(10) unsigned DEFAULT NULL ,
`created` timestamp NULL DEFAULT NULL,
`last_updated` timestamp NULL DEFAULT NULL,
`updated_by` varchar(255) NOT NULL,
`updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `_REV_TYPE` varchar(10) NOT NULL,
  `REV` bigint(20) DEFAULT NULL,
KEY `code_x` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `polybag_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
`code` varchar(15) NOT NULL,
`description` varchar(128) DEFAULT NULL,
`length` decimal(10,2) NOT NULL,
`breadth` decimal(10,2) NOT NULL,
`height` decimal(10,2) NOT NULL,
`vol_weight` decimal(10,2) NOT NULL,
  `slab_id` int(10) unsigned DEFAULT NULL ,
`created` timestamp NULL DEFAULT NULL,
  `last_updated` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
PRIMARY KEY (`id`),
UNIQUE KEY `code_UNIQUE` (`code`),
CONSTRAINT `FK_polybag_slab_id` FOREIGN KEY (`slab_id`) REFERENCES polybag_slab(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

CREATE TABLE `history_polybag_type` (
  `id` int(10) unsigned NOT NULL,
`code` varchar(15) NOT NULL,
`description` varchar(128) DEFAULT NULL,
`length` decimal(10,2) NOT NULL,
`breadth` decimal(10,2) NOT NULL,
`height` decimal(10,2) NOT NULL,
`vol_weight` decimal(10,2) NOT NULL,
  `slab_id` int(10) unsigned DEFAULT NULL ,
`created` timestamp NULL DEFAULT NULL,
`last_updated` timestamp NULL DEFAULT NULL,
`updated_by` varchar(255) NOT NULL,
`updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `_REV_TYPE` varchar(10) NOT NULL,
  `REV` bigint(20) DEFAULT NULL,
KEY `code_x` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `surface_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
`type` varchar(15) NOT NULL,
`description` varchar(128) DEFAULT NULL,
  `last_updated` timestamp NULL DEFAULT NULL,
`created` timestamp NULL DEFAULT NULL,  
`updated_by` varchar(255) NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
PRIMARY KEY (`id`),
UNIQUE KEY `type_UNIQUE` (`type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

CREATE TABLE `history_surface_type` (
  `id` int(10) unsigned NOT NULL,
  `type` varchar(15) NOT NULL,
`description` varchar(128) DEFAULT NULL,
`created` timestamp NULL DEFAULT NULL,
  `last_updated` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

  `_REV_TYPE` varchar(10) NOT NULL,
  `REV` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


insert into role values (null,'packmanboxcreator','carton polybag create','admin',1,'/admin/packman/createCarton',now(),now(),now());

insert into role values (null,'packmanboxeditor','carton polybag view and edit','admin',1,'/admin/packman/viewCarton',now(),now(),now()); 

 insert into role values (null,'packmanslabeditor','carton polybag slab edit','admin',1,'/admin/packman/createCartonSlab',now(),now(),now());

 insert into role values (null,'packmanslabview','carton polybag slab view','admin',1,'/admin/packman/viewCartonSlab',now(),now(),now());

insert into role values (null,'packmansurfaceeditor','packman surfacepackaging view and edit','admin',1,'/admin/packman/viewSurface',now(),now(),now());

insert into role values (null,'packmansurfacecreator','packman surfacepackaging create','admin',1,'/admin/packman/createSurface',now(),now(),now());
