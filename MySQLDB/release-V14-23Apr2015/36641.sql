CREATE TABLE `fc_address` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `address_line1` varchar(400) NOT NULL,
  `address_line2` varchar(400) DEFAULT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(64) DEFAULT NULL,
  `pincode` varchar(10) NOT NULL,
  `mobile` varchar(45) DEFAULT NULL,
  `landline` varchar(45) DEFAULT NULL,
  `email` varchar(256) DEFAULT NULL,
  `dnd_active` tinyint(1) DEFAULT '0',
  `created` timestamp NULL DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ;


CREATE TABLE fc_details (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(20) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `primary_address_id` int(10) unsigned NOT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `last_updated` timestamp NULL DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `enabled` tinyint(1) DEFAULT NULL,
  `type` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `codetype_UNIQUE` (`code`,`type`),
  KEY `fk_fc_primary_address_id` (`primary_address_id`),

  CONSTRAINT `fk_fm_primary_address_id` FOREIGN KEY (`primary_address_id`) REFERENCES `fc_address` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
  
)  ;











