#!/bin/bash

echo "Sourcing the db properties file"
. ./db.properties

echo "Dumping data from table=$TABLE_FC from host:$HOST_SOURCE_URL useing User Name=$USER_NAME_SOURCE and Password=$LOGIN_PASSWORD_SOURCE at Port=$PORT_SOURCE";
mysqldump -u$USER_NAME_SOURCE -p$LOGIN_PASSWORD_SOURCE -h$HOST_SOURCE_URL -P$PORT_SOURCE $SOURCE_SCHEMA_NAME $TABLE_FC --where="type IN ('FC_VOI', 'ONESHIP', 'RMS')"  > fullfilment_bkup.sql;

echo "Dumping data from table=$TABLE_SD from host:$HOST_SOURCE_URL useing User Name=$USER_NAME_SOURCE and Password=$LOGIN_PASSWORD_SOURCE at Port=$PORT_SOURCE";
mysqldump --single-transaction -u$USER_NAME_SOURCE -p$LOGIN_PASSWORD_SOURCE -h$HOST_SOURCE_URL -P$PORT_SOURCE $SOURCE_SCHEMA_NAME $TABLE_SD --where="id IN (select primary_address_id from fulfillment_centre where type IN ('FC_VOI', 'ONESHIP', 'RMS'))" > sd.sql;

echo "Logging into Mysql and dumping data in host=$HOST_DESTINATION_URL useing User Name=$USER_NAME_DESTINATION and Password=$LOGIN_PASSWORD_DESTINATION at Port=$PORT_DESTINATION";
mysql -u$USER_NAME_DESTINATION -p$LOGIN_PASSWORD_DESTINATION -h$HOST_DESTINATION_URL -P$PORT_DESTINATION $DSETINATION_SCHEMA_NAME < db_source.sql;



