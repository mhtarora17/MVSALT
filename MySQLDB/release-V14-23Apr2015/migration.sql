use cocofs;

insert ignore into cocofs.seller_fc_mapping (seller_fm_id,fc_code,enabled,created,last_updated,updated_by) select coco.id,ship.fulfillment_code,ship.enabled,ship.created,now(),'migration' from temp_shipping.vendor_fulfillment_mapping ship,cocofs.seller_fm_mapping coco where coco.seller_code=ship.vendor_code and (coco.fulfilment_model='ONESHIP' or coco.fulfilment_model='FC_VOI' ) and ship.fulfillment_type = coco.fulfilment_model and coco.enabled=1 and ship.enabled=1;

insert ignore into cocofs.seller_supc_fc_mapping (seller_supc_fm_id,fc_code,enabled,created,last_updated,updated_by) select coco.id,ship.fulfillment_code,ship.enabled,ship.created,now(),'migration' from temp_shipping.vendor_fulfillment_mapping ship,cocofs.seller_supc_fm_mapping coco where coco.seller_code=ship.vendor_code and (coco.fulfilment_model='ONESHIP' or coco.fulfilment_model='FC_VOI' ) and ship.fulfillment_type = coco.fulfilment_model and coco.enabled=1 and ship.enabled=1; 
