DROP TABLE IF EXISTS fc_details;

DROP TABLE IF EXISTS fc_address;

SOURCE 36641.sql;

CREATE TABLE `temp_primary_address_id` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `primary_address_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
);

INSERT INTO temp_primary_address_id (primary_address_id)
SELECT distinct primary_address_id
FROM   fulfillment_centre ;

INSERT INTO fc_address(id, name, address_line1, address_line2, city, state, pincode, mobile, landline, email, dnd_active, created, updated)
SELECT t.id, name, address_line1, address_line2, city, state, pincode, mobile, landline, email, dnd_active, created, updated
FROM shipping_detail s, temp_primary_address_id t
WHERE s.id = t.primary_address_id ;



INSERT INTO fc_details(id, code, name, primary_address_id, created, last_updated, updated, enabled, type)
SELECT f.id, code, name, t.id, created, last_updated, updated, enabled, type
FROM fulfillment_centre f, temp_primary_address_id t
WHERE f.primary_address_id = t.primary_address_id ;

DROP TABLE fulfillment_centre;

DROP TABLE shipping_detail;

DROP TABLE temp_primary_address_id;



