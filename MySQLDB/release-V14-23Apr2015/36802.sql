alter table seller_supc_fm_mapping REMOVE PARTITIONING;

ALTER table seller_supc_fm_mapping drop primary key, add primary key(id);

CREATE TABLE `seller_supc_fc_mapping` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `seller_supc_fm_id` int(10) unsigned NOT NULL,
  `fc_code` varchar(20) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `created` timestamp NULL DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_updated` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `seller_supc_fc_enable_UNIQ` (`seller_supc_fm_id`,`fc_code`,`enabled`),
  KEY `idx_fc_code_enabled` (`fc_code`,`enabled`),
  KEY `idx_updated` (`updated`),
  KEY `enabled` (`enabled`),
  KEY `FK_seller_supc_fm_mapping` (`seller_supc_fm_id`),
  CONSTRAINT `FK_seller_supc_fm_to_fc_mapping` FOREIGN KEY (`seller_supc_fm_id`) REFERENCES `seller_supc_fm_mapping` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
);

 CREATE TABLE `seller_fc_mapping` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `seller_fm_id` int(10) unsigned NOT NULL,
  `fc_code` varchar(20) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `created` timestamp NULL DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_updated` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `seller_fc_enable_UNIQ` (`seller_fm_id`,`fc_code`,`enabled`),
  KEY `idx_fc_code_enabled` (`fc_code`,`enabled`),
  KEY `idx_updated` (`updated`),
  KEY `enabled` (`enabled`),
  KEY `FK_seller_fm_mapping` (`seller_fm_id`),
  CONSTRAINT `FK_seller_fm_to_fc_mapping` FOREIGN KEY (`seller_fm_id`) REFERENCES `seller_fm_mapping` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
);
