use cocofs;

insert into block values (null,'StorePackagingRuleBlock',now());
insert into block values (null,'CategoryPackagingRuleBlock',now());
insert into block values (null,'SubcategoryPackagingRuleBlock',now());

SET @blockId:= (select id from block where name='StorePackagingRuleBlock');

SET @blockId:= (select id from block where name='CategoryPackagingRuleBlock');
insert into block_params values (null,'BRAND','Brand',@blockId,now(),now(),'tech.boss@snapdeal.com');
insert into block_params values (null,'LABEL','Label',@blockId,now(),now(),'tech.boss@snapdeal.com');

SET @blockId:= (select id from block where name='SubcategoryPackagingRuleBlock');
insert into block_params values (null,'BRAND','Brand',@blockId,now(),now(),'tech.boss@snapdeal.com');
insert into block_params values (null,'LABEL','Label',@blockId,now(),now(),'tech.boss@snapdeal.com');
