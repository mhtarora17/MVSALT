use cocofs;

CREATE TABLE `history_packaging_type_item` (
  `id` int(10) unsigned NOT NULL,
  `code` varchar(15) NOT NULL,
  `packaging_type_id` int(10) unsigned,
  `description` text DEFAULT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `last_updated` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `_REV_TYPE` varchar(10) NOT NULL,
  `REV` bigint(20) DEFAULT NULL
) ENGINE=InnoDB ;

CREATE TABLE `history_store` (
  `id` int(10) unsigned NOT NULL,
  `code` varchar(64) NOT NULL,
  `name` varchar(64) NOT NULL,
  `store_front_id` varchar(64) NOT NULL,
  `description` text DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `last_updated` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `_REV_TYPE` varchar(10) NOT NULL,
  `REV` bigint(20) DEFAULT NULL
) ENGINE=InnoDB ;

CREATE TABLE `history_packaging_type` (
  `id` int(10) unsigned NOT NULL,
  `type` varchar(64) NOT NULL,
  `packaging_mode` varchar(48) NOT NULL,
  `store_id` int(10) unsigned NOT NULL,
  `default_type` tinyint(1) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `description` text DEFAULT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `last_updated` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `_REV_TYPE` varchar(10) NOT NULL,
  `REV` bigint(20) DEFAULT NULL
) ENGINE=InnoDB ;

CREATE TABLE `history_packaging_type_properties` (
  `id` int(10) unsigned NOT NULL ,
  `packaging_type_id` int(10) unsigned NOT NULL,
  `name` varchar(32) NOT NULL,
  `value` varchar(255) NOT NULL,
  `enabled` tinyint(1) DEFAULT '1',
  `created` timestamp NULL DEFAULT NULL,
  `last_updated` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `_REV_TYPE` varchar(10) NOT NULL,
  `REV` bigint(20) DEFAULT NULL
) ENGINE=InnoDB ;

CREATE TABLE `history_packaging_type_item_properties` (
	`id` int(10) unsigned NOT NULL,
	`packaging_type_item_id` int(10) unsigned NOT NULL,
	`name` varchar(32) NOT NULL,
	`value` varchar(255) NOT NULL,
	`enabled` tinyint(1) DEFAULT '1',
	`created` timestamp NULL DEFAULT NULL,
	`last_updated` timestamp NULL DEFAULT NULL,   
	`updated_by` varchar(255) NOT NULL,   
	`updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,   
	`_REV_TYPE` varchar(10) NOT NULL,
  `REV` bigint(20) DEFAULT NULL
) ENGINE=InnoDB;

CREATE TABLE `history_slab` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(15) NOT NULL,
  `slab_size` int(10) unsigned NOT NULL,
  `lower_limit` int(10) unsigned NOT NULL,
  `packaging_type_id` int(10) unsigned NOT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `last_updated` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `_REV_TYPE` varchar(10) NOT NULL,
  `REV` bigint(20) DEFAULT NULL
) ENGINE=InnoDB;

CREATE TABLE `history_store_properties` (
  `id` int(10) unsigned NOT NULL,
  `store_id` int(10) unsigned NOT NULL,
  `name` varchar(32) NOT NULL,
  `value` text NOT NULL,
  `enabled` tinyint(1) DEFAULT '1',
  `created` timestamp NULL DEFAULT NULL,
  `last_updated` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `_REV_TYPE` varchar(10) NOT NULL,
  `REV` bigint(20) DEFAULT NULL	  
) ENGINE=InnoDB ;

CREATE TABLE `history_url_role_mapping` (
 `id` int(10) unsigned NOT NULL ,
`url_id` int(10) unsigned NOT NULL,
`role_code` varchar(64) NOT NULL,
`enabled` tinyint(1) NOT NULL,
`created` timestamp NULL DEFAULT NULL,
`last_updated` timestamp NULL DEFAULT NULL,
`updated_by` varchar(255) NOT NULL,
`updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
`_REV_TYPE` varchar(10) NOT NULL,
  `REV` bigint(20) DEFAULT NULL	
);

CREATE TABLE `history_url_pattern` (
 `id` int(10) unsigned NOT NULL,
`url` varchar(256) NOT NULL,
`enabled` tinyint(1) NOT NULL,
`created` timestamp NULL DEFAULT NULL,
  `last_updated` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
`_REV_TYPE` varchar(10) NOT NULL,
  `REV` bigint(20) DEFAULT NULL	
);

CREATE TABLE `history_role_store_mapping` (
 `id` int(10) unsigned NOT NULL,
`role_code` varchar(64) NOT NULL,
`store_code` varchar(64) NOT NULL,
`enabled` tinyint(1) NOT NULL,
`created` timestamp NULL DEFAULT NULL,
`last_updated` timestamp NULL DEFAULT NULL,
`updated_by` varchar(255) NOT NULL,
`updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
`_REV_TYPE` varchar(10) NOT NULL,
  `REV` bigint(20) DEFAULT NULL	
);