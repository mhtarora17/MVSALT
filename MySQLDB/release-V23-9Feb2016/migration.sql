use cocofs;

insert into packaging_type(type, packaging_mode, store_id, default_type, description, created, last_updated, updated_by, updated) select 'CARTON', 'Normal', store.id, 0, 'carton packaging_type', now(), now(), 'packman-dev', now() from store;


insert into packaging_type(type, packaging_mode, store_id, default_type, description, created, last_updated, updated_by, updated) select 'POLYBAG', 'Normal', store.id, 0, 'polybag packaging_type', now(), now(), 'packman-dev', now() from store;


insert into packaging_type(type, packaging_mode, store_id, default_type, description, created, last_updated, updated_by, updated) select surface_type.type, 'Picknpack', store.id, 0, surface_type.description, now(), now(), 'packman-dev', now() from surface_type, store;


insert into slab(name, slab_size, lower_limit, packaging_type_id, created, last_updated, updated_by, updated) select name, slab_size, lower_limit, packaging_type.id , now(), now(),'packman-dev', now() from carton_slab, packaging_type where packaging_type.type = 'CARTON';

insert into slab(name, slab_size, lower_limit, packaging_type_id, created, last_updated, updated_by, updated) select name, slab_size, lower_limit, packaging_type.id , now(), now(),'packman-dev', now() from polybag_slab, packaging_type where packaging_type.type = 'POLYBAG';


create temporary table temp_packaging_type_item(
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(15) NOT NULL,
  `packaging_type_id` int(10) unsigned NOT NULL,
  `description` varchar(128) DEFAULT NULL,
  `slab_id` int(10) unsigned DEFAULT NULL,
  `length` decimal(10,2) NOT NULL,
  `breadth` decimal(10,2) NOT NULL,
  `height` decimal(10,2) NOT NULL,
  `vol_weight` decimal(10,2) NOT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `last_updated` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   PRIMARY KEY (`id`)
);

insert into temp_packaging_type_item(code, packaging_type_id, description, slab_id, length, breadth, height, vol_weight, created, last_updated, updated_by, updated) select carton_type.code, slab.packaging_type_id, carton_type.description, slab.id, carton_type.length, carton_type.breadth, carton_type.height, carton_type.vol_weight, now(), now(), 'packman-dev', now() from carton_type, slab INNER JOIN packaging_type on slab.packaging_type_id = packaging_type.id where packaging_type.type = 'CARTON' and slab.lower_limit < carton_type.vol_weight and (slab.lower_limit+slab_size) >=  carton_type.vol_weight;

insert into temp_packaging_type_item(code, packaging_type_id, description, slab_id, length, breadth, height, vol_weight, created, last_updated, updated_by, updated) select polybag_type.code, slab.packaging_type_id, polybag_type.description, slab.id, polybag_type.length, polybag_type.breadth, polybag_type.height, polybag_type.vol_weight, now(), now(), 'packman-dev', now() from polybag_type, slab INNER JOIN packaging_type on slab.packaging_type_id = packaging_type.id where packaging_type.type = 'POLYBAG' and slab.lower_limit < polybag_type.vol_weight and (slab.lower_limit+slab_size) >=  polybag_type.vol_weight;

insert into packaging_type_item(id, code, packaging_type_id, description, created, last_updated, updated_by, updated) select id, code, packaging_type_id, description, now(), now(), 'packman-dev', now() from temp_packaging_type_item;

insert into packaging_type_item(code, packaging_type_id, description, created, last_updated, updated_by, updated) select type,id, description, now(), now(), 'packman-dev', now() from packaging_type where packaging_mode = 'Picknpack';

insert into packaging_type_item_properties(packaging_type_item_id,name,value,enabled,created,last_updated,updated_by,updated) select id, 'length', length, true, now(), now(), 'packman-dev',now() from temp_packaging_type_item;

insert into packaging_type_item_properties(packaging_type_item_id,name,value,enabled,created,last_updated,updated_by,updated) select id, 'breadth', breadth, true, now(), now(), 'packman-dev',now() from temp_packaging_type_item;

insert into packaging_type_item_properties(packaging_type_item_id,name,value,enabled,created,last_updated,updated_by,updated) select id, 'height', height, true, now(), now(), 'packman-dev',now() from temp_packaging_type_item;

insert into packaging_type_item_properties(packaging_type_item_id,name,value,enabled,created,last_updated,updated_by,updated) select id, 'vol.weight', vol_weight, true, now(), now(), 'packman-dev',now() from temp_packaging_type_item;

insert into packaging_type_item_properties(packaging_type_item_id,name,value,enabled,created,last_updated,updated_by,updated) select id, 'slab.id', slab_id, true, now(), now(), 'packman-dev',now() from temp_packaging_type_item;


drop table temp_packaging_type_item;


insert into packaging_type_properties(packaging_type_id, name, value, enabled,  created, last_updated, updated_by, updated) select packaging_type.id, 'validation.enabled', 'true', 1, now(), now(), 'packman-dev', now() from packaging_type where packaging_type.type = 'CARTON';

insert into packaging_type_properties(packaging_type_id, name, value, enabled,  created, last_updated, updated_by, updated) select packaging_type.id, 'packaging.type.prefix', 'CB', 1, now(), now(), 'packman-dev', now() from packaging_type where packaging_type.type = 'CARTON';

insert into packaging_type_properties(packaging_type_id, name, value, enabled,  created, last_updated, updated_by, updated) select packaging_type.id, 'slab.prefix', 'SLAB', 1, now(), now(), 'packman-dev', now() from packaging_type where packaging_type.type = 'CARTON';

insert into packaging_type_properties(packaging_type_id, name, value, enabled,  created, last_updated, updated_by, updated) select packaging_type.id, 'min.slab.size', '250', 1, now(), now(), 'packman-dev', now() from packaging_type where packaging_type.type = 'CARTON';

insert into packaging_type_properties(packaging_type_id, name, value, enabled,  created, last_updated, updated_by, updated) select packaging_type.id, 'max.slab.size', '5000', 1, now(), now(), 'packman-dev', now() from packaging_type where packaging_type.type = 'CARTON';

insert into packaging_type_properties(packaging_type_id, name, value, enabled,  created, last_updated, updated_by, updated) select packaging_type.id, 'max.vol.weight', '30000', 1, now(), now(), 'packman-dev', now() from packaging_type where packaging_type.type = 'CARTON';

insert into packaging_type_properties(packaging_type_id, name, value, enabled,  created, last_updated, updated_by, updated) select packaging_type.id, 'slab.size', '500', 1, now(), now(), 'packman-dev', now() from packaging_type where packaging_type.type = 'CARTON';

insert into packaging_type_properties(packaging_type_id, name, value, enabled,  created, last_updated, updated_by, updated) select packaging_type.id, 'validation.enabled', 'true', 1, now(), now(), 'packman-dev', now() from packaging_type where packaging_type.type = 'POLYBAG';

insert into packaging_type_properties(packaging_type_id, name, value, enabled,  created, last_updated, updated_by, updated) select packaging_type.id, 'packaging.type.prefix', 'PL', 1, now(), now(), 'packman-dev', now() from packaging_type where packaging_type.type = 'POLYBAG';

insert into packaging_type_properties(packaging_type_id, name, value, enabled,  created, last_updated, updated_by, updated) select packaging_type.id, 'slab.prefix', 'SLAB', 1, now(), now(), 'packman-dev', now() from packaging_type where packaging_type.type = 'POLYBAG';

insert into packaging_type_properties(packaging_type_id, name, value, enabled,  created, last_updated, updated_by, updated) select packaging_type.id, 'min.slab.size', '250', 1, now(), now(), 'packman-dev', now() from packaging_type where packaging_type.type = 'POLYBAG';

insert into packaging_type_properties(packaging_type_id, name, value, enabled,  created, last_updated, updated_by, updated) select packaging_type.id, 'max.slab.size', '5000', 1, now(), now(), 'packman-dev', now() from packaging_type where packaging_type.type = 'POLYBAG';

insert into packaging_type_properties(packaging_type_id, name, value, enabled,  created, last_updated, updated_by, updated) select packaging_type.id, 'max.vol.weight', '20000', 1, now(), now(), 'packman-dev', now() from packaging_type where packaging_type.type = 'POLYBAG';

insert into packaging_type_properties(packaging_type_id, name, value, enabled,  created, last_updated, updated_by, updated) select packaging_type.id, 'slab.size', '500', 1, now(), now(), 'packman-dev', now() from packaging_type where packaging_type.type = 'POLYBAG';




insert into packaging_type_properties(packaging_type_id, name, value, enabled,  created, last_updated, updated_by, updated) select packaging_type.id, 'validation.enabled', 'true', 1, now(), now(), 'packman-dev', now() from packaging_type where packaging_type.type like 'SURFACE%';

insert into packaging_type_properties(packaging_type_id, name, value, enabled,  created, last_updated, updated_by, updated) select packaging_type.id, 'packaging.type.prefix', 'SURFACE_', 1, now(), now(), 'packman-dev', now() from packaging_type where packaging_type.type like 'SURFACE%';

update packaging_type set enabled = 1;








