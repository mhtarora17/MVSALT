use cocofs;

CREATE TABLE `store` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(64) NOT NULL,
  `name` varchar(64) NOT NULL,
  `store_front_id` varchar(64) NOT NULL,
  `description` text DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `last_updated` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code_UNIQUE` (`code`),
  UNIQUE KEY `store_front_id_UNIQUE`  (`store_front_id`)
) ENGINE=InnoDB ;


CREATE TABLE `packaging_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(64) NOT NULL,
  `packaging_mode` varchar(48) NOT NULL Default 'Air',
  `store_id` int(10) unsigned NOT NULL,
  `default_type` tinyint(1) NOT NULL,
  `enabled` tinyint(1) DEFAULT '1',
  `description` text DEFAULT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `last_updated` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code_store_UNIQUE` (`type`,`store_id`),
  KEY `FK_pt_store_id` (`store_id`),
  CONSTRAINT `FK_pt_store_id` FOREIGN KEY (`store_id`) REFERENCES `store` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB ;

CREATE TABLE `slab` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(15) NOT NULL,
  `slab_size` int(10) unsigned NOT NULL,
  `lower_limit` int(10) unsigned NOT NULL,
  `packaging_type_id` int(10) unsigned NOT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `last_updated` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_slab_pkg_type_id` (`packaging_type_id`),
  CONSTRAINT `FK_slab_pkg_type_id` FOREIGN KEY (`packaging_type_id`) REFERENCES `packaging_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;
 
 CREATE TABLE `packaging_type_item` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(15) NOT NULL,
  `packaging_type_id` int(10) unsigned NOT NULL,
  `description` text DEFAULT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `last_updated` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code_pt_UNIQUE` (`code`,`packaging_type_id`),
  KEY `FK_pti_pkg_type_id` (`packaging_type_id`),
  CONSTRAINT `FK_pti_pkg_type_id` FOREIGN KEY (`packaging_type_id`) REFERENCES `packaging_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
  ) ENGINE=InnoDB ;



CREATE TABLE `packaging_type_attribute` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `attribute` varchar(32) NOT NULL,
  `attribute_description` text NOT NULL,
  `enabled` tinyint(1) DEFAULT '1',
  `updated_by` varchar(255) NOT NULL,
  `created_by` varchar(255) NOT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `last_updated` timestamp NULL DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `version` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `pt_attribute_idx` (`attribute`),
  KEY `enabled_pt_attribute_idx` (`enabled`,`attribute`)
) ENGINE=InnoDB;


CREATE TABLE `packaging_type_properties` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `packaging_type_id` int(10) unsigned NOT NULL,
  `name` varchar(32) NOT NULL,
  `value` varchar(255) NOT NULL,
  `enabled` tinyint(1) DEFAULT '1',
  `created` timestamp NULL DEFAULT NULL,
  `last_updated` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_spmap_pkg_type_id` (`packaging_type_id`),
  CONSTRAINT `packaging_type_attribute_ibfk_1` FOREIGN KEY (`name`) REFERENCES `packaging_type_attribute` (`attribute`),
  CONSTRAINT `FK_spmap_pkg_type_id` FOREIGN KEY (`packaging_type_id`) REFERENCES `packaging_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB ;

CREATE TABLE `store_attribute` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `attribute` varchar(32) NOT NULL,
  `attribute_description` text NOT NULL,
  `enabled` tinyint(1) DEFAULT '1',
  `updated_by` varchar(255) NOT NULL,
  `created_by` varchar(255) NOT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `last_updated` timestamp NULL DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `version` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `pt_attribute_idx` (`attribute`),
  KEY `enabled_pt_attribute_idx` (`enabled`,`attribute`)
) ENGINE=InnoDB;

CREATE TABLE `store_properties` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `store_id` int(10) unsigned NOT NULL,
  `name` varchar(32) NOT NULL,
  `value` text NOT NULL,
  `enabled` tinyint(1) DEFAULT '1',
  `created` timestamp NULL DEFAULT NULL,
  `last_updated` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_spmap_store_id` (`store_id`),
  CONSTRAINT `store_attribute_ibfk_1` FOREIGN KEY (`name`) REFERENCES `store_attribute` (`attribute`),
  CONSTRAINT `FK_spmap_store_id` FOREIGN KEY (`store_id`) REFERENCES `store` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB ;

CREATE TABLE `url_pattern` (
 `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
`url` varchar(256) NOT NULL,
`enabled` tinyint(1) NOT NULL,
`created` timestamp NULL DEFAULT NULL,
  `last_updated` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
PRIMARY KEY (`id`)
);

CREATE TABLE `url_role_mapping` (
 `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
`url_id` int(10) unsigned NOT NULL,
`role_code` varchar(64) NOT NULL,
`enabled` tinyint(1) NOT NULL,
`created` timestamp NULL DEFAULT NULL,
`last_updated` timestamp NULL DEFAULT NULL,
`updated_by` varchar(255) NOT NULL,
`updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
PRIMARY KEY (`id`),
CONSTRAINT `FK_urm` FOREIGN KEY (`url_id`) REFERENCES `url_pattern` (`id`)
);


CREATE TABLE `role_store_mapping` (
 `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
`role_code` varchar(64) NOT NULL,
`store_code` varchar(64) NOT NULL,
`enabled` tinyint(1) NOT NULL,
`created` timestamp NULL DEFAULT NULL,
`last_updated` timestamp NULL DEFAULT NULL,
`updated_by` varchar(255) NOT NULL,
`updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
PRIMARY KEY (`id`),
CONSTRAINT `FK_store_code_r` FOREIGN KEY (`store_code`) REFERENCES `store` (`code`)
);

CREATE TABLE `supc_packagingtype_mapping` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `supc` varchar(32) NOT NULL,
  `store_code` varchar(16) NOT NULL,
  `packaging_type` varchar(32) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `created` timestamp NULL DEFAULT NULL,
  `last_updated` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_s_p_mapping` (`supc`,`store_code`)
);


create index role_code_x on role_store_mapping(role_code);
create index enable_x on role_store_mapping(enabled);
create index role_code_x on url_role_mapping(role_code);

insert into packaging_type_attribute(attribute,attribute_description,enabled,updated_by,created_by,created,last_updated,updated) values ('validation.enabled','Whether validation is required or not',1,'cocofs@snapdeal.com','cocofs@snapdeal.com',now(),now(),now());

insert into packaging_type_attribute(attribute,attribute_description,enabled,updated_by,created_by,created,last_updated,updated) values ('packaging.type.prefix','Packaging type name should start with provided prefix',1,'cocofs@snapdeal.com','cocofs@snapdeal.com',now(),now(),now());

insert into packaging_type_attribute(attribute,attribute_description,enabled,updated_by,created_by,created,last_updated,updated) values ('slab.prefix','Slab name should start with provided prefix',1,'cocofs@snapdeal.com','cocofs@snapdeal.com',now(),now(),now());

insert into packaging_type_attribute(attribute,attribute_description,enabled,updated_by,created_by,created,last_updated,updated) values ('slab.size','Default slab size',1,'cocofs@snapdeal.com','cocofs@snapdeal.com',now(),now(),now());

insert into packaging_type_attribute(attribute,attribute_description,enabled,updated_by,created_by,created,last_updated,updated) values ('min.slab.size','Minimum allowed slab size',1,'cocofs@snapdeal.com','cocofs@snapdeal.com',now(),now(),now());

insert into packaging_type_attribute(attribute,attribute_description,enabled,updated_by,created_by,created,last_updated,updated) values ('max.slab.size','Maximum allowed slab size',1,'cocofs@snapdeal.com','cocofs@snapdeal.com',now(),now(),now());

insert into packaging_type_attribute(attribute,attribute_description,enabled,updated_by,created_by,created,last_updated,updated) values ('max.vol.weight','Maximum allowed volumetric weight',1,'cocofs@snapdeal.com','cocofs@snapdeal.com',now(),now(),now());

alter table job_action modify job_type varchar(32);

insert into job_action(code,value,description,created,updated,job_type) values("SupcPackagingTypeMapping","Supc Packaging Type Mapping","Supc Packaging Type Mapping",now(),now(),"PackagingTypeMapping");

insert into store_attribute(attribute,attribute_description,enabled,updated_by,created_by,created,last_updated,updated) values ('LABEL','Associated labels with store',1,'cocofs@snapdeal.com','cocofs@snapdeal.com',now(),now(),now());

CREATE TABLE `rule_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rule_id` int(10) unsigned NOT NULL,
  `rule_details` text,
  `rule_param_details` text,
  `rule_criteria_details` text,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

alter table rule_params add column `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;
alter table rule add column `sub_start_date` datetime DEFAULT NULL;
alter table criteria add column `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;


CREATE TABLE `packaging_type_item_attribute` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `attribute` varchar(32) NOT NULL,
  `attribute_description` text NOT NULL,
  `enabled` tinyint(1) DEFAULT '1',
  `updated_by` varchar(255) NOT NULL,
  `created_by` varchar(255) NOT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `last_updated` timestamp NULL DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `version` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `pt_attribute_idp` (`attribute`),
  KEY `enabled_pt_attribute_idp` (`enabled`,`attribute`)
) ENGINE=InnoDB;

CREATE TABLE `packaging_type_item_properties` (
	`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
	`packaging_type_item_id` int(10) unsigned NOT NULL,
	`name` varchar(32) NOT NULL,
	`value` varchar(255) NOT NULL,
	`enabled` tinyint(1) DEFAULT '1',
	`created` timestamp NULL DEFAULT NULL,
	`last_updated` timestamp NULL DEFAULT NULL,   
	`updated_by` varchar(255) NOT NULL,   
	`updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,   
	PRIMARY KEY (`id`), 
	KEY `FK_pkg_type_item_id` (`packaging_type_item_id`),     
	CONSTRAINT `packaging_type_item_attribute_ibfk_1` FOREIGN KEY (`name`) REFERENCES `packaging_type_item_attribute` (`attribute`)
) ENGINE=InnoDB;

insert into packaging_type_item_attribute(attribute,attribute_description,enabled,updated_by,created_by,created,last_updated,updated) values ('length','Length of packaging type item',1,'cocofs@snapdeal.com','cocofs@snapdeal.com',now(),now(),now());

insert into packaging_type_item_attribute(attribute,attribute_description,enabled,updated_by,created_by,created,last_updated,updated) values ('breadth','Breadth of packaging type item',1,'cocofs@snapdeal.com','cocofs@snapdeal.com',now(),now(),now());

insert into packaging_type_item_attribute(attribute,attribute_description,enabled,updated_by,created_by,created,last_updated,updated) values ('height','Height of packaging type item',1,'cocofs@snapdeal.com','cocofs@snapdeal.com',now(),now(),now());

insert into packaging_type_item_attribute(attribute,attribute_description,enabled,updated_by,created_by,created,last_updated,updated) values ('vol.weight','Volumetric weight of packaging type item',1,'cocofs@snapdeal.com','cocofs@snapdeal.com',now(),now(),now());

insert into packaging_type_item_attribute(attribute,attribute_description,enabled,updated_by,created_by,created,last_updated,updated) values ('slab.id','Slab id of packaging type item',1,'cocofs@snapdeal.com','cocofs@snapdeal.com',now(),now(),now());


ALTER TABLE `cocofs`.`packaging_type_item_properties` DROP FOREIGN KEY `packaging_type_item_attribute_ibfk_1`;
ALTER TABLE `cocofs`.`packaging_type_item_properties` ADD CONSTRAINT `packaging_type_item_attribute_ibfk_1` FOREIGN KEY (`packaging_type_item_id`) REFERENCES `cocofs`.`packaging_type_item` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

update rule_params set updated = now();

update criteria set updated = now();


