use cocofs;

delete from role where code like 'packman\_%';

drop table if exists role_store_mapping;

drop table if exists url_role_mapping;

drop table if exists url_pattern;

drop table if exists packaging_type_properties;

drop table if exists packaging_type_attribute; 

drop table if exists packaging_type_item_properties;

drop table if exists packaging_type_item_attribute;

drop table if exists packaging_type_item;

drop table if exists slab;

drop table if exists packaging_type;

drop table if exists supc_packagingtype_mapping;

drop table if exists store_properties;

drop table if exists store_attribute;

drop table if exists store;

drop table if exists rule_log;

delete from criteria where rule_id in (select id from rule where block_id in (select id from block where name in ('CategoryPackagingRuleBlock','StorePackagingRuleBlock','SubcategoryPackagingRuleBlock')));

delete from rule_params where rule_id in (select id from rule where block_id in (select id from block where name in ('CategoryPackagingRuleBlock','StorePackagingRuleBlock','SubcategoryPackagingRuleBlock')));

delete from rule where block_id in (select id from block where name in ('CategoryPackagingRuleBlock','StorePackagingRuleBlock','SubcategoryPackagingRuleBlock'));

delete from block_params where block_id in (select id from block where name in ('StorePackagingRuleBlock','CategoryPackagingRuleBlock','SubcategoryPackagingRuleBlock'));

delete from block where name in ('StorePackagingRuleBlock','CategoryPackagingRuleBlock','SubcategoryPackagingRuleBlock');


alter table rule_params drop column updated;

alter table rule drop column sub_start_date;

alter table criteria drop column updated;

drop table if exists history_packaging_type_item;

drop table if exists history_store;

drop table if exists history_packaging_type;

drop table if exists history_packaging_type_properties;

drop table if exists history_packaging_type_item_properties;

drop table if exists history_slab;

drop table if exists history_store_properties;

drop table if exists history_url_role_mapping;

drop table if exists history_url_pattern;

drop table if exists history_role_store_mapping;

delete from job_action where code = 'SupcPackagingTypeMapping';






