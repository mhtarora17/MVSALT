CREATE TABLE `seller_contact_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seller_code` varchar(32) NOT NULL,  
  `pin_code` varchar(10) NOT NULL,  
  `address_line1` varchar(512) DEFAULT NULL,
  `address_line2` varchar(512) DEFAULT NULL,
  `city` varchar(128) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(64) DEFAULT NULL,
  `last_updated` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_seller` (`seller_code`),
  KEY `idx_updated` (`updated`),	
  KEY `idx_created` (`created`),
  KEY `idx_pincode` (`pin_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `seller_zone_mapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seller_code` varchar(32) NOT NULL,  
  `zone` varchar(255) NOT NULL,  
  `created` timestamp NULL DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(64) DEFAULT NULL,
  `last_updated` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unq_seller_zone` (seller_code,zone),
  KEY `idx_zone` (`zone`),	
  KEY `idx_created` (`created`),
  KEY `idx_seller_code` (`seller_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `REVINFO` (
        `REV` int NOT NULL AUTO_INCREMENT,
        `REVTSTMP` bigint(20),
        primary key (`REV`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `history_seller_contact_details` (
  `id` int(11) NOT NULL,
  `seller_code` varchar(32) NOT NULL,  
  `pin_code` varchar(10) NOT NULL,  
  `address_line1` varchar(512) DEFAULT NULL,
  `address_line2` varchar(512) DEFAULT NULL,
  `city` varchar(128) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(64) DEFAULT NULL,
  `last_updated` timestamp NULL DEFAULT NULL,
  `_REV_TYPE` varchar(10) NOT NULL,
  `REV` bigint(20) DEFAULT NULL,
  KEY `idx_updated` (`updated`),	
  KEY `idx_created` (`created`),
  KEY `idx_pincode` (`pin_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `history_seller_zone_mapping` (
  `id` int(11) NOT NULL,
  `seller_code` varchar(32) NOT NULL,  
  `zone` varchar(255) NOT NULL,  
  `created` timestamp NULL DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(64) DEFAULT NULL,
  `last_updated` timestamp NULL DEFAULT NULL,
  `_REV_TYPE` varchar(10) NOT NULL,
  `REV` bigint(20) DEFAULT NULL,
  KEY `idx_zone` (`zone`),	
  KEY `idx_created` (`created`),
  KEY `idx_seller_code` (`seller_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `history_seller_fm_mapping` (
  `id` int(10) unsigned NOT NULL,
  `seller_code` varchar(15) NOT NULL,
  `fulfilment_model` varchar(63) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `created` timestamp NULL DEFAULT NULL,
  `last_updated` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `supc_exist` tinyint(1) DEFAULT '0',
  `subcat_exist` tinyint(1) DEFAULT '0',
  `_REV_TYPE` varchar(10) NOT NULL,
  `REV` bigint(20) DEFAULT NULL,
  KEY `idx_seller_code` (`seller_code`),
  KEY `idx_updated` (`updated`),
  KEY `idx_last_updated` (`last_updated`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `history_seller_fc_mapping` (
  `id` int(10) unsigned NOT NULL,
  `seller_fm_id` int(10) unsigned NOT NULL,
  `fc_code` varchar(32) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `created` timestamp NULL DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_updated` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) NOT NULL,
  `_REV_TYPE` varchar(10) NOT NULL,
  `REV` bigint(20) DEFAULT NULL,
  KEY `idx_updated` (`updated`),	
  KEY `idx_created` (`created`),
  KEY `idx_fccode` (`fc_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `history_seller_supc_fm_mapping` (
  `id` int(10) unsigned NOT NULL,
  `seller_code` varchar(16) NOT NULL,
  `supc` varchar(32) NOT NULL,
  `fulfilment_model` varchar(128) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `created` timestamp NULL DEFAULT NULL,
  `last_updated` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `_REV_TYPE` varchar(10) NOT NULL,
  `REV` bigint(20) DEFAULT NULL,
  KEY `idx_updated` (`updated`),	
  KEY `idx_created` (`created`),
  KEY `idx_sellersupc` (`seller_code`,`supc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `history_seller_supc_fc_mapping` (
  `id` int(10) unsigned NOT NULL,
  `seller_supc_fm_id` int(10) unsigned NOT NULL,
  `fc_code` varchar(32) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `created` timestamp NULL DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_updated` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) NOT NULL,
  `_REV_TYPE` varchar(10) NOT NULL,
  `REV` bigint(20) DEFAULT NULL,
  KEY `idx_updated` (`updated`),	
  KEY `idx_created` (`created`),
  KEY `idx_fccode` (`fc_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


insert into job_action values(null,"PZUpdate","Pincode Zone Update", "Pincode Zone Update",now(),now(),"PZUpdate");

insert into role values (null,'pincodeuploader','pincodeuploader','admin',1,'/admin/pincodeZoneMapping',now(),now(),now());

insert into cocofs_property values (null,'sdgeo.web.service.url','TBD',now(),now());

