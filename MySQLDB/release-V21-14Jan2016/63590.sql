use cocofs;

drop TABLE if exists `supc_tax_class_mapping`;
drop TABLE if exists `subcat_tax_class_mapping`;
drop TABLE if exists `history_supc_tax_class_mapping`;
drop TABLE if exists `history_subcat_tax_class_mapping`;


CREATE TABLE `supc_tax_class_mapping` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `supc` varchar(30) NOT NULL UNIQUE,
  `tax_class` varchar(128) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `created` timestamp NULL DEFAULT NULL,
  `last_updated` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `version` bigint(10) unsigned DEFAULT '0',
   PRIMARY KEY (`id`)
);


CREATE TABLE `subcat_tax_class_mapping` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `subcat` varchar(128) NOT NULL UNIQUE,
  `tax_class` varchar(128) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `created` timestamp NULL DEFAULT NULL,
  `last_updated` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `version` bigint(10) unsigned DEFAULT '0',
   PRIMARY KEY (`id`)
);


CREATE TABLE `history_supc_tax_class_mapping` (
  `id` int(10) unsigned,
  `supc` varchar(30),
  `tax_class` varchar(128),
  `enabled` tinyint(1),
  `created` timestamp,
  `last_updated` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255),
   `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `version` bigint(10) unsigned DEFAULT '0',
  `_REV_TYPE` varchar(10) NOT NULL,
  `REV` bigint(20) DEFAULT NULL
);


CREATE TABLE `history_subcat_tax_class_mapping` (
  `id` int(10) unsigned,
  `subcat` varchar(128),
  `tax_class` varchar(128),
  `enabled` tinyint(1),
  `created` timestamp,
  `last_updated` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255),
   `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `version` bigint(10) unsigned DEFAULT '0',
  `_REV_TYPE` varchar(10) NOT NULL,	
  `REV` bigint(20) DEFAULT NULL
);


insert into job_action values(null,'SubcatTaxClass','Subcat Tax Class','SubcatTaxClass',now(),now(),'SubcatTaxClass');

insert into job_action values(null,'SupcTaxClass','Supc Tax Class','SupcTaxClass',now(),now(),'SupcTaxClass');

insert into upload_sheet_field_domain values (null,'SupcTaxClassMappingDTO','enabled','true',now(),now(),'cocofs-dev',now());

insert into upload_sheet_field_domain values (null,'SupcTaxClassMappingDTO','enabled','false',now(),now(),'cocofs-dev',now());

insert into cocofs_property values(null, 'enable.supc.tax.class.mapping.cache','false',now(),now(),now(),'cocofs-dev');

insert into cocofs_property values(null, 'enable.subcat.tax.class.mapping.cache','false',now(),now(),now(),'cocofs-dev');

insert into cocofs_property values(null, 'data.source.for.tax.class.read','AEROSPIKE',now(),now(),now(),'cocofs-dev');

insert into cocofs_property values(null, 'validate.supc.tax.mapping','ON_UPLOAD',now(),now(),now(),'cocofs-dev');

insert into cocofs_property values(null, 'validate.subcat.tax.mapping','ON_UPLOAD',now(),now(),now(),'cocofs-dev');

insert into cocofs_property values(null, 'default.subcat.tax.mapping','default',now(),now(),now(),'cocofs-dev');

insert into cocofs_property values(null, 'enable.tax.class','false',now(),now(),now(),'cocofs-dev');


insert into cocofs_property values(null, 'max.batch.size.for.tax.class','10',now(),now(),now(),'cocofs-dev');

update role set default_url = 'admin/taxRate/taxRateUpload' where code = 'taxrateeditor';

insert into role values (null,'taxclasseditor','taxclasseditor','admin',1 ,'admin/taxClass/taxClassUpload',now(),now(),now());










