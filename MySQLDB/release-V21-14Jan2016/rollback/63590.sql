use cocofs;

drop TABLE if exists `supc_tax_class_mapping`;
drop TABLE if exists `subcat_tax_class_mapping`;
drop TABLE if exists `history_supc_tax_class_mapping`;
drop TABLE if exists `history_subcat_tax_class_mapping`;


delete from cocofs_property where name = 'enable.supc.tax.class.mapping.cache';

delete from cocofs_property where name = 'enable.subcat.tax.class.mapping.cache';

delete from cocofs_property where name = 'data.source.for.tax.class.read';

delete from cocofs_property where name = 'validate.supc.tax.mapping';

delete from cocofs_property where name = 'validate.subcat.tax.mapping';

delete from cocofs_property where name = 'default.subcat.tax.mapping';

delete from cocofs_property where name = 'enable.tax.class';

delete from cocofs_property where name = 'max.batch.size.for.tax.class';

delete from upload_sheet_field_domain where class_name = 'SupcTaxClassMappingDTO';

update user_role set enabled = 0 where  role_code ='taxclasseditor';

update role set default_url = 'admin/taxRate' where code = 'taxrateeditor';


