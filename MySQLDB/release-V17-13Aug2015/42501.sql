alter table `cocofs_property` add `last_updated` timestamp NULL DEFAULT NULL;
alter table `cocofs_property` add `updated_by` varchar(100) DEFAULT NULL;

DROP TABLE IF EXISTS `history_cocofs_property`;

CREATE TABLE `history_cocofs_property` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(48) NOT NULL,
  `value` varchar(255) NOT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_updated` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(100) NOT NULL,
  `_REV_TYPE` varchar(10) NOT NULL,
  `REV` bigint(20) DEFAULT NULL
);



drop table if exists `cocofs_map_property`;
create table `cocofs_map_property`(
 `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
 `property_name` varchar(100) NOT NULL,
 `key` varchar(100) NOT NULL,
 `value` varchar(100) NOT NULL,
 `created` timestamp NULL DEFAULT NULL,
 `last_updated` timestamp NULL DEFAULT NULL,
 `updated_by` varchar(100) NOT NULL,
 `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`));


drop table if exists `history_cocofs_map_property`;
create table `history_cocofs_map_property`(
 `id` int(10) unsigned NOT NULL,
 `property_name` varchar(100) NOT NULL,
 `key` varchar(100) NOT NULL,
 `value` varchar(100) NOT NULL,
 `created` timestamp NULL DEFAULT NULL,
 `last_updated` timestamp NULL DEFAULT NULL,
 `updated_by` varchar(100) NOT NULL,
 `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
 `_REV_TYPE` varchar(10) NOT NULL,
  `REV` bigint(20) DEFAULT NULL
);


alter table fc_details add updated_by varchar(100) default null;
alter table fc_address add updated_by varchar(100) default null;

insert into cocofs_map_property values(1, 'fm.code.to.fc.type.map', 'OCPLUS', 'OCPLUS', now(),now(),'vaidya.nitish@snapdeal.com',now()),(2, 'fm.code.to.fc.type.map', 'FC_VOI', 'FC_VOI', now(),now(),'vaidya.nitish@snapdeal.com',now()),(3, 'fm.code.to.fc.type.map', 'ONESHIP', 'ONESHIP', now(),now(),'vaidya.nitish@snapdeal.com',now());

insert into cocofs_property values(null, 'cocofs.center.types', 'FC_VOI,ONESHIP,RMS,3PL,LIQUIDATION',now(),now(),now(),'vaidya.nitish@snapdeal.com');



