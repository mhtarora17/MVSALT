use cocofs;
insert into cocofs_property (name , value, created, updated) values ('score.web.service.url', 'http://internal-courier-allocation-internal-213516523.ap-southeast-1.elb.amazonaws.com/', now(), now());
insert into task_detail(name, impl_class, cron_expression, clustered, concurrent,quartz_managed,enabled,host_name,created,last_updated,updated_by,updated) values('Event Instance','com.snapdeal.cocofs.services.task.impl.EventInstanceTask','0 0/15 * * * ?',0,0,1,1,'cocofsadmin1',NOW(),NOW(),'gaurav',NOW());

CREATE TABLE `event_instance` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `event_type` varchar(255) NOT NULL,
  `param1` varchar(255) DEFAULT NULL,
  `param2` varchar(255) DEFAULT NULL,
  `json_arguments` text,
  `executed` tinyint(1) NOT NULL DEFAULT '0',
  `retry_count` int(5) unsigned DEFAULT '0',
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(128) DEFAULT NULL,
  `created_by` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `executed_key` (`executed`),
  KEY `event_type` (`event_type`),
  KEY `param1` (`param1`),
  KEY `param2` (`param2`),
  KEY `enabled` (`enabled`),
  KEY `created` (`created`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
