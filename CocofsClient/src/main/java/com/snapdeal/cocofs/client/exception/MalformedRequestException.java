package com.snapdeal.cocofs.client.exception;

/**
 * @author nikhil
 */
public class MalformedRequestException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 1792771472701408212L;

    public MalformedRequestException() {
        super();
    }

    public MalformedRequestException(String message) {
        super(message);
    }

}
