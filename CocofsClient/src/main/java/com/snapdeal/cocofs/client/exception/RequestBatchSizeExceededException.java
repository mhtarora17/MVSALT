package com.snapdeal.cocofs.client.exception;

/**
 * 
 * @author nikhil
 *
 */
public class RequestBatchSizeExceededException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 8337545909855298110L;

    public RequestBatchSizeExceededException() {
        super();
    }

    public RequestBatchSizeExceededException(String message) {
        super(message);
    }

}
