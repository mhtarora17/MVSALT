package com.snapdeal.cocofs.client.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.snapdeal.base.model.common.ServiceResponse;
import com.snapdeal.base.validation.ValidationError;
import com.snapdeal.cocofs.client.IClientHelper;
import com.snapdeal.cocofs.client.exception.DataNotFoundException;
import com.snapdeal.cocofs.client.exception.MalformedRequestException;
import com.snapdeal.cocofs.client.exception.RequestBatchSizeExceededException;
import com.snapdeal.cocofs.model.response.ApiErrorCode;

/**
 * @author nikhil
 */
@Service("clientHelper")
public class ClientHelperImpl implements IClientHelper {

    @Override
    public <T> void genericResponseCheckForBatchGetAPIs(T t) throws MalformedRequestException, RequestBatchSizeExceededException, DataNotFoundException, Exception {

        List<ValidationError> validationErrors = ((ServiceResponse) t).getValidationErrors();

        final Logger LOG = LoggerFactory.getLogger(ClientHelperImpl.class);

        for (ValidationError error : validationErrors) {
            if (error.getCode() == ApiErrorCode.REQUIRED_FIELD_NULL.code()) {
                throw new MalformedRequestException(error.getMessage());
            } else if (error.getCode() == ApiErrorCode.REQUEST_BATCH_SIZE_LIMIT_EXCEEDED.code()) {
                throw new RequestBatchSizeExceededException(error.getMessage());
            } else if (error.getCode() == ApiErrorCode.REQUESTED_DATA_DOES_NOT_EXISTS.code()) {
                throw new DataNotFoundException(error.getMessage());
            } else {
                LOG.info("Unknown error caught while invoking cocofs API");
                throw new Exception(error.getMessage());
            }
        }
    }

    @Override
    public <T> void genericResponseCheckerForSetAPIs(T t) throws MalformedRequestException, RequestBatchSizeExceededException, Exception {

        List<ValidationError> validationErrors = ((ServiceResponse) t).getValidationErrors();

        final Logger LOG = LoggerFactory.getLogger(ClientHelperImpl.class);

        for (ValidationError error : validationErrors) {
            if (error.getCode() == ApiErrorCode.REQUIRED_FIELD_NULL.code()) {
                throw new MalformedRequestException(error.getMessage());
            } else if (error.getCode() == ApiErrorCode.REQUEST_BATCH_SIZE_LIMIT_EXCEEDED.code()) {
                throw new RequestBatchSizeExceededException(error.getMessage());
            } else {
                LOG.info("Unknown error caught while invoking cocofs API");
                throw new Exception(error.getMessage());
            }
        }
    }
    
    @Override
    public <T> void genericResponseCheckForGetAPIs(T t) throws MalformedRequestException, DataNotFoundException, Exception {

        List<ValidationError> validationErrors = ((ServiceResponse) t).getValidationErrors();

        final Logger LOG = LoggerFactory.getLogger(ClientHelperImpl.class);

        for (ValidationError error : validationErrors) {
            if (error.getCode() == ApiErrorCode.REQUIRED_FIELD_NULL.code()) {
                throw new MalformedRequestException(error.getMessage());
            } else if (error.getCode() == ApiErrorCode.REQUESTED_DATA_DOES_NOT_EXISTS.code()) {
                throw new DataNotFoundException(error.getMessage());
            } else {
                LOG.info("Unknown error caught while invoking cocofs API");
                throw new Exception(error.getMessage());
            }
        }
    }

}
