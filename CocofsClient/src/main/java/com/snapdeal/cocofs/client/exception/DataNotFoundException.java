package com.snapdeal.cocofs.client.exception;

/**
 * 
 * @author nikhil
 *
 */
public class DataNotFoundException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 5789602653991235725L;

    public DataNotFoundException() {
        super();
    }

    public DataNotFoundException(String message) {
        super(message);
    }
    
    

}
