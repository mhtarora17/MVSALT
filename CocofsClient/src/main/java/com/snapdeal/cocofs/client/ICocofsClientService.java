package com.snapdeal.cocofs.client;

import com.snapdeal.base.exception.SnapdealWSException;
import com.snapdeal.base.exception.TransportException;
import com.snapdeal.base.model.common.ServiceResponse;
import com.snapdeal.base.validation.ValidationError;
import com.snapdeal.cocofs.client.exception.DataNotFoundException;
import com.snapdeal.cocofs.client.exception.MalformedRequestException;
import com.snapdeal.cocofs.client.exception.RequestBatchSizeExceededException;
import com.snapdeal.cocofs.model.request.AddOrUpdateCategoryModeMappingRequest;
import com.snapdeal.cocofs.model.request.AddOrUpdateSupcModeMappingRequest;
import com.snapdeal.cocofs.model.request.GetAllCategoryModeMappingRequest;
import com.snapdeal.cocofs.model.request.GetAllCenterTypesRequest;
import com.snapdeal.cocofs.model.request.GetAllDangerousGoodsTypeRequest;
import com.snapdeal.cocofs.model.request.GetAllFCDetailRequest;
import com.snapdeal.cocofs.model.request.GetAllSellersForFMRequest;
import com.snapdeal.cocofs.model.request.GetAllSupcModeMappingRequest;
import com.snapdeal.cocofs.model.request.GetCategoryModeMappingRequest;
import com.snapdeal.cocofs.model.request.GetFMAndFCBySellerSupcListRequest;
import com.snapdeal.cocofs.model.request.GetFMAndZonesBySellerSupcListRequest;
import com.snapdeal.cocofs.model.request.GetFMMappingAtSellerAndSellerSubcatRequest;
import com.snapdeal.cocofs.model.request.GetFulfilmentFeeParamRequest;
import com.snapdeal.cocofs.model.request.GetFulfilmentModelAndProductAttributesRequest;
import com.snapdeal.cocofs.model.request.GetFulfilmentModelAndWeightRequest;
import com.snapdeal.cocofs.model.request.GetFulfilmentModelBySellerSupcSubcatRequest;
import com.snapdeal.cocofs.model.request.GetFulfilmentModelRequest;
import com.snapdeal.cocofs.model.request.GetFulfilmentModelsForSellerRequest;
import com.snapdeal.cocofs.model.request.GetGiftWrapperInfoBySUPCSellerListRequest;
import com.snapdeal.cocofs.model.request.GetGiftWrapperInfoBySUPCSellerListResponse;
import com.snapdeal.cocofs.model.request.GetGiftWrapperInfoBySUPCVendorRequest;
import com.snapdeal.cocofs.model.request.GetMultiplierForPerimeterRequest;
import com.snapdeal.cocofs.model.request.GetProductDeliveryAndPackagingTypeRequest;
import com.snapdeal.cocofs.model.request.GetProductDeliveryTypeRequest;
import com.snapdeal.cocofs.model.request.GetProductFulfilmentAttributeRequest;
import com.snapdeal.cocofs.model.request.GetProductPackagingTypeRequest;
import com.snapdeal.cocofs.model.request.GetProductSystemWeightCapturedFlagRequest;
import com.snapdeal.cocofs.model.request.GetProductWeightNotificationRequest;
import com.snapdeal.cocofs.model.request.GetSDFulfilledStatusBySupcSellersSubcatRequest;
import com.snapdeal.cocofs.model.request.GetSDFulfilledStatusBySupcSellersSubcatResponse;
import com.snapdeal.cocofs.model.request.GetSellerSubcatFMMappingRequest;
import com.snapdeal.cocofs.model.request.GetShipFromLocationBySellerSupcListRequest;
import com.snapdeal.cocofs.model.request.GetSupcModeMappingRequest;
import com.snapdeal.cocofs.model.request.GetTaxClassRequest;
import com.snapdeal.cocofs.model.request.GetTaxInfoRequest;
import com.snapdeal.cocofs.model.request.GetTaxRateInfoBySUPCStateSellerPriceRequest;
import com.snapdeal.cocofs.model.request.GetVolumetricWeightFormulaParamsRequest;
import com.snapdeal.cocofs.model.request.GetZonesBySellerSupcListRequest;
import com.snapdeal.cocofs.model.request.RemoveCategoryModeMappingRequest;
import com.snapdeal.cocofs.model.request.RemoveSupcModeMappingRequest;
import com.snapdeal.cocofs.model.request.SetFMMappingRequest;
import com.snapdeal.cocofs.model.request.SetProductCapturedWeightRequest;
import com.snapdeal.cocofs.model.request.SetProductFulfilmentAttributeRequest;
import com.snapdeal.cocofs.model.request.SetSellerDetailsRequest;
import com.snapdeal.cocofs.model.request.SetSellerFMMappingRequest;
import com.snapdeal.cocofs.model.request.SetSellerSubcatFMMappingRequest;
import com.snapdeal.cocofs.model.request.ValidateSellerCodeRequest;
import com.snapdeal.cocofs.model.response.AddOrUpdateCategoryModeMappingResponse;
import com.snapdeal.cocofs.model.response.AddOrUpdateSupcModeMappingResponse;
import com.snapdeal.cocofs.model.response.AllCenterTypesResponse;
import com.snapdeal.cocofs.model.response.AllFCDetailResponse;
import com.snapdeal.cocofs.model.response.GetAllCategoryModeMappingResponse;
import com.snapdeal.cocofs.model.response.GetAllDangerousGoodsTypeResponse;
import com.snapdeal.cocofs.model.response.GetAllSellersForFMResponse;
import com.snapdeal.cocofs.model.response.GetAllSupcModeMappingResponse;
import com.snapdeal.cocofs.model.response.GetCategoryModeMappingResponse;
import com.snapdeal.cocofs.model.response.GetFMAndFCBySellerSupcListResponse;
import com.snapdeal.cocofs.model.response.GetFMAndZonesBySellerSupcListResponse;
import com.snapdeal.cocofs.model.response.GetFMMappingAtSellerAndSellerSubcatResponse;
import com.snapdeal.cocofs.model.response.GetFulFilmentFeeParamResponse;
import com.snapdeal.cocofs.model.response.GetFulfilmentModelAndProductAttributesResponse;
import com.snapdeal.cocofs.model.response.GetFulfilmentModelAndWeightResponse;
import com.snapdeal.cocofs.model.response.GetFulfilmentModelBySellerSupcSubcatResponse;
import com.snapdeal.cocofs.model.response.GetFulfilmentModelResponse;
import com.snapdeal.cocofs.model.response.GetFulfilmentModelsForSellerResponse;
import com.snapdeal.cocofs.model.response.GetGiftWrapperInfoBySUPCVendorResponse;
import com.snapdeal.cocofs.model.response.GetMultiplierForPerimeterResponse;
import com.snapdeal.cocofs.model.response.GetProductDeliveryAndPackagingTypeResponse;
import com.snapdeal.cocofs.model.response.GetProductDeliveryTypeResponse;
import com.snapdeal.cocofs.model.response.GetProductFulfilmentAttributeResponse;
import com.snapdeal.cocofs.model.response.GetProductPackagingTypeResponse;
import com.snapdeal.cocofs.model.response.GetProductSystemWeightCapturedFlagResponse;
import com.snapdeal.cocofs.model.response.GetProductWeightNotificationResponse;
import com.snapdeal.cocofs.model.response.GetSellerSubcatFMMappingResponse;
import com.snapdeal.cocofs.model.response.GetShipFromLocationBySellerSupcListResponse;
import com.snapdeal.cocofs.model.response.GetSupcModeMappingResponse;
import com.snapdeal.cocofs.model.response.GetTaxClassResponse;
import com.snapdeal.cocofs.model.response.GetTaxInfoResponse;
import com.snapdeal.cocofs.model.response.GetTaxRateInfoBySUPCStateSellerPriceResponse;
import com.snapdeal.cocofs.model.response.GetVolumetricWeightFormulaParamsResponse;
import com.snapdeal.cocofs.model.response.GetZonesBySellerSupcListResponse;
import com.snapdeal.cocofs.model.response.RemoveCategoryModeMappingResponse;
import com.snapdeal.cocofs.model.response.RemoveSupcModeMappingResponse;
import com.snapdeal.cocofs.model.response.SetFMMappingResponse;
import com.snapdeal.cocofs.model.response.SetProductAttributeFulfilmentResponse;
import com.snapdeal.cocofs.model.response.SetProductCapturedWeightResponse;
import com.snapdeal.cocofs.model.response.SetSellerDetailsResponse;
import com.snapdeal.cocofs.model.response.SetSellerFMMappingResponse;
import com.snapdeal.cocofs.model.response.SetSellerSubcatFMMappingResponse;
import com.snapdeal.cocofs.model.response.ValidateSellerCodeResponse;

/**
 * @author nikhil
 */
/**
 * @author nitish
 */
public interface ICocofsClientService {

    /**
     * @param url
     */
    void setCocoFSClientServiceURL(String url);

    /**
     * gets product fulfilment attribute for an supc.
     * 
     * @param request
     * @return
     * @throws DataNotFoundException when request data do not exists at ther sever end
     * @throws RequestBatchSizeExceededException when request batch size exceeds a configurable value at the server end
     * @throws MalformedRequestException when the request is either empty or null
     * @throws Exception unknown error(internal error)
     */
    public GetProductFulfilmentAttributeResponse getProductFulfilmentAttributes(GetProductFulfilmentAttributeRequest request) throws TransportException;

    /**
     * Sets product fulfilment attribute for batch of supc(s).
     * 
     * @param request
     * @return <p>
     *         SetProductAttributeFulfilmentResponse which consist of a map(supc-> {@link ValidationError}) which
     *         represent failed request (if any) with their reasons. Caller should iterate over it to act over any
     *         failures. {@link APIErrorCode} are wrapped in {@link ValidationError} class (already in
     *         {@link ServiceResponse} ).
     *         </p>
     *         <p>
     *         Units for length, breadth and height is cm, weight is grams.
     *         <nl>
     *         <p>
     *         Possible APIErrorCode sent by this API are REQUIRED_FIELD_NULL, DATA_OUT_OF_RANGE, DATA_ALREADY_EXISTS,
     *         INTERNAL_ERROR.
     *         </p>
     * @throws RequestBatchSizeExceededException when request batch size exceeds a configurable value at the server end
     * @throws MalformedRequestException when the request is either empty or null
     * @throws Exception unknown error(internal error)
     */
    public SetProductAttributeFulfilmentResponse setProductFulfilmentAttributes(SetProductFulfilmentAttributeRequest request) throws TransportException;

    /**
     * For given list of supcs, determine the packaging type, any supc with issues are report via failure in response
     * 
     * @param request
     * @return
     * @throws MalformedRequestException
     * @throws RequestBatchSizeExceededException
     * @throws Exception
     */
    GetProductPackagingTypeResponse getProductPackagingType(GetProductPackagingTypeRequest request) throws TransportException;

    /**
     * For given list of supcs, determine the delivery type, any supc with issues are report via failure in response
     * 
     * @param request
     * @return
     * @throws MalformedRequestException
     * @throws RequestBatchSizeExceededException
     * @throws Exception
     */
    GetProductDeliveryTypeResponse getProductDeliveryType(GetProductDeliveryTypeRequest request) throws TransportException;

    /**
     * For given list of supcs, determine the package and delivery type, any supc with issues are report via failure in
     * response
     * 
     * @param request
     * @return
     * @throws MalformedRequestException
     * @throws RequestBatchSizeExceededException
     * @throw
     */
    GetProductDeliveryAndPackagingTypeResponse getProductDeliveryAndPackagingType(GetProductDeliveryAndPackagingTypeRequest request) throws TransportException;

    /**
     * Gets Fulfilment model based on vendor code and supc
     * 
     * @param request
     * @return
     * @throws TransportException
     * @throws MalformedRequestException
     * @throws DataNotFoundException
     * @throws Exception
     */
    public GetFulfilmentModelResponse getFulfilmentModel(GetFulfilmentModelRequest request) throws TransportException;

    /**
     * Given a list of SellerSupc pairs, find fulfillment model, packaging type and delivery type for each pair. The
     * pairs for which all three can not be determined are returned as failures successCount indicates count of pairs
     * for which all three pieces of information was available
     * 
     * @param request
     * @return
     * @throws MalformedRequestException
     * @throws RequestBatchSizeExceededException
     * @throws Exception
     */
    @Deprecated
    GetFulFilmentFeeParamResponse getFulfilmentFeeParams(GetFulfilmentFeeParamRequest request) throws TransportException;

    /**
     * getProductWeightNotification for a sellerCode, paginated response is structured in two parts latestUpdate and
     * aggregatedUpdate.
     * 
     * @param request
     * @return
     * @throws TransportException
     * @throws MalformedRequestException
     * @throws RequestBatchSizeExceededException
     * @throws DataNotFoundException
     * @throws Exception
     */
    GetProductWeightNotificationResponse getProductWeightNotifications(GetProductWeightNotificationRequest request) throws TransportException;

    /**
     * Return fulfilment model and weight for seller code and supc
     * 
     * @param request
     * @return
     * @throws MalformedRequestException
     * @throws RequestBatchSizeExceededException
     * @throws Exception
     */
    GetFulfilmentModelAndWeightResponse getFulfilmentModelAndWeight(GetFulfilmentModelAndWeightRequest request) throws TransportException;

    /**
     * Given effective Fulfilment Model for a seller and supc. Checks all three level viz. seller-supc, seller-subcat(if
     * fm not found at seller-supc level) and seller (if fm not found at seller-subcat level) to return mapping subcat
     * is not a mandatory field in the request ;. If it is not there, then we hit CAMS for getting subcat from supc
     * 
     * @param request
     * @return
     * @throws MalformedRequestException
     * @throws RequestBatchSizeExceededException
     * @throws Exception
     */
    GetFulfilmentModelBySellerSupcSubcatResponse getFulfilmentModelBySellerSupcSubcat(GetFulfilmentModelBySellerSupcSubcatRequest request) throws TransportException;

    /**
     * @param request seller code
     * @return FM Mapping at seller and seller subcat level for a particular seller
     * @throws MalformedRequestException
     * @throws RequestBatchSizeExceededException
     * @throws DataNotFoundException
     * @throws Exception
     */
    GetFMMappingAtSellerAndSellerSubcatResponse getFMMappingAtSellerAndSellerSubcat(GetFMMappingAtSellerAndSellerSubcatRequest request) throws TransportException;

    /**
     * Updates fulfilment model mapping at seller and seller subcat level.
     * 
     * @param request
     * @return failure if any at both seller and seller subcat level
     * @throws MalformedRequestException
     * @throws RequestBatchSizeExceededException
     * @throws DataNotFoundException
     * @throws Exception
     */
    SetFMMappingResponse setFMMapping(SetFMMappingRequest request) throws TransportException;

    /**
     * @param request
     * @return
     * @throws MalformedRequestException
     * @throws RequestBatchSizeExceededException
     * @throws Exception
     */
    @Deprecated
    GetFulfilmentModelAndProductAttributesResponse getFulfilmentModelAndProductAttributes(GetFulfilmentModelAndProductAttributesRequest request) throws TransportException;

    /**
     * Returns all fulfilment models possible for a seller
     * 
     * @param request
     * @return
     * @throws MalformedRequestException
     * @throws RequestBatchSizeExceededException
     * @throws Exception
     */
    @Deprecated
    GetFulfilmentModelsForSellerResponse getFulfilmentModelsForSeller(GetFulfilmentModelsForSellerRequest request) throws TransportException;

    /**
     * Get all the category -> shippinng mode mappings per cocofs
     * 
     * @param request
     * @return
     * @throws MalformedRequestException
     * @throws RequestBatchSizeExceededException
     * @throws DataNotFoundException
     * @throws Exception
     */
    GetAllCategoryModeMappingResponse getAllCategoryModeMapping(GetAllCategoryModeMappingRequest request) throws TransportException;

    /**
     * Get all the supc -> shipping mode mappings per cocofs
     * 
     * @param request
     * @return
     * @throws MalformedRequestException
     * @throws RequestBatchSizeExceededException
     * @throws DataNotFoundException
     * @throws Exception
     */
    GetAllSupcModeMappingResponse getAllSupcModeMapping(GetAllSupcModeMappingRequest request) throws TransportException;

    /**
     * Get category -> shippinng mode mappings per cocofs for given list of categories, if one of the categories is
     * absent from the returned values, it means cocofs doesn't have any information/mapping for that category
     * 
     * @param request
     * @return
     * @throws MalformedRequestException
     * @throws RequestBatchSizeExceededException
     * @throws DataNotFoundException
     * @throws Exception
     */
    GetCategoryModeMappingResponse getCategoryModeMapping(GetCategoryModeMappingRequest request) throws TransportException;

    /**
     * Get supc -> shipping mode mappings per cocofs for given list of supc, if one of the supc is absent from the
     * returned values it means cocofs doesn't have any information/mapping for that supc
     * 
     * @param request
     * @return
     * @throws MalformedRequestException
     * @throws RequestBatchSizeExceededException
     * @throws DataNotFoundException
     * @throws Exception
     */
    GetSupcModeMappingResponse getSupcModeMapping(GetSupcModeMappingRequest request) throws TransportException;

    /**
     * @param request
     * @return
     * @throws MalformedRequestException
     * @throws RequestBatchSizeExceededException
     * @throws Exception
     */
    AddOrUpdateCategoryModeMappingResponse addOrUpdateCategoryModeMapping(AddOrUpdateCategoryModeMappingRequest request) throws TransportException;

    /**
     * @param request
     * @return
     * @throws MalformedRequestException
     * @throws RequestBatchSizeExceededException
     * @throws Exception
     */
    AddOrUpdateSupcModeMappingResponse addOrUpdateSupcModeMapping(AddOrUpdateSupcModeMappingRequest request) throws TransportException;

    /**
     * @param request
     * @return
     * @throws MalformedRequestException
     * @throws RequestBatchSizeExceededException
     * @throws Exception
     */
    RemoveCategoryModeMappingResponse removeCategoryModeMapping(RemoveCategoryModeMappingRequest request) throws TransportException;

    /**
     * @param request
     * @return
     * @throws MalformedRequestException
     * @throws RequestBatchSizeExceededException
     * @throws Exception
     */
    RemoveSupcModeMappingResponse removeSupcModeMapping(RemoveSupcModeMappingRequest request) throws TransportException;

    /**
     * get systemCapturedWeight flag which indicates if the weight is system captured/admin captured.
     * 
     * @param request
     * @return
     * @throws MalformedRequestException
     * @throws RequestBatchSizeExceededException
     * @throws Exception
     */
    GetProductSystemWeightCapturedFlagResponse getProductSystemWeightCapturedFlag(GetProductSystemWeightCapturedFlagRequest request) throws TransportException;

    /**
     * Set system captured weight. Also explicitly set systemWeightCaptured flag to true, this suggests that the weight
     * is captured by actual weighing machine.
     * 
     * @param request
     * @return
     * @throws MalformedRequestException
     * @throws RequestBatchSizeExceededException
     * @throws Exception
     */
    SetProductCapturedWeightResponse setProductCapturedWeight(SetProductCapturedWeightRequest request) throws TransportException;

    /**
     * Returns all possible values of dangerous Goods Type
     * 
     * @param request
     * @return
     * @throws TransportException
     */
    GetAllDangerousGoodsTypeResponse getAllDangerousGoodsType(GetAllDangerousGoodsTypeRequest request) throws TransportException;

    /**
     * Given a list of SellerSupc pairs with optional categoryUrl, finds fulfilment model, packaging type and delivery
     * type for each pair. The pairs for which all three can not be determined are returned as failures successCount
     * indicates count of pairs for which all three pieces of information was available
     * 
     * @param request
     * @return
     * @throws MalformedRequestException
     * @throws RequestBatchSizeExceededException
     * @throws Exception
     */
    com.snapdeal.cocofs.model.response.v2.GetFulFilmentFeeParamResponse getFulfilmentFeeParams(com.snapdeal.cocofs.model.request.v2.GetFulfilmentFeeParamRequest request)
            throws TransportException;

    /**
     * Sets FM Mapping at Seller Subcat level. The request represents current snapshot of seller-subcat fm mapping .
     * Previous mappings, if any, will be disabled.
     * 
     * @param request
     * @return
     * @throws TransportException
     */
    SetSellerSubcatFMMappingResponse setSellerSubcatFMMapping(SetSellerSubcatFMMappingRequest request) throws TransportException;

    /**
     * Set FM Mapping at seller level. It is the default fulfilment model to which seller is mapped to..
     * 
     * @param request
     * @return
     * @throws TransportException
     */
    SetSellerFMMappingResponse setSellerFMMapping(SetSellerFMMappingRequest request) throws TransportException;

    GetSellerSubcatFMMappingResponse getSellerSubcatFMMapping(GetSellerSubcatFMMappingRequest request) throws TransportException;

    GetGiftWrapperInfoBySUPCVendorResponse getGiftWrapperInfoBySUPCVendor(GetGiftWrapperInfoBySUPCVendorRequest request) throws TransportException;

    /**
     * API to retrieve Tax information based on either SUPC, state or both from COCOFS DB SNAPDEALTECH-25620
     * 
     * @param request
     * @return
     * @throws TransportException
     */
    GetTaxRateInfoBySUPCStateSellerPriceResponse getTaxRateInfoBySUPCAndState(GetTaxRateInfoBySUPCStateSellerPriceRequest request) throws TransportException;

    /**
     * API to retrieve sdFullfilled Flaf for SOI filter used by Search team
     * 
     * @param request
     * @return
     * @throws TransportException
     */
    GetSDFulfilledStatusBySupcSellersSubcatResponse getSDFulfilledStatusBySupcSellersSubcat(GetSDFulfilledStatusBySupcSellersSubcatRequest request) throws TransportException;

    GetGiftWrapperInfoBySUPCSellerListResponse getGiftWrapperInfoBySUPCSellerList(GetGiftWrapperInfoBySUPCSellerListRequest request) throws TransportException;

    Double getEffectiveWeight(Double deadWeight, Double volumetricWeight);

    double getVolumetricWeightFromLBH(double l, double b, double h);

    GetFMAndFCBySellerSupcListResponse getFMAndFCBySellerSupcList(GetFMAndFCBySellerSupcListRequest request) throws TransportException;

    /**
     * API to retrieve All Centre Types as List
     * 
     * @param request
     * @return
     * @throws TransportException
     */
    AllCenterTypesResponse getAllCenterTypes(GetAllCenterTypesRequest request) throws TransportException;

    /**
     * API to retrieve FulFillment Centre Details Based on Input Type IF type = null or "ALL" or ""(empty) @return ALL
     * Centres ELSE @return based on Input Type
     * 
     * @param request
     * @return
     * @throws TransportException
     */
    AllFCDetailResponse getAllFCDetails(GetAllFCDetailRequest request) throws TransportException;

    GetAllSellersForFMResponse getAllSellersForFM(GetAllSellersForFMRequest request) throws TransportException;

    SetSellerDetailsResponse createOrUpdateSeller(SetSellerDetailsRequest request) throws TransportException;

    GetZonesBySellerSupcListResponse getZonesBySellerSupcList(GetZonesBySellerSupcListRequest request) throws SnapdealWSException;

    GetFMAndZonesBySellerSupcListResponse getFMAndZonesBySellerSupcList(GetFMAndZonesBySellerSupcListRequest request) throws SnapdealWSException;

    GetVolumetricWeightFormulaParamsResponse getVolumetricWeightFormulaParams(GetVolumetricWeightFormulaParamsRequest request) throws SnapdealWSException;

    ValidateSellerCodeResponse validateSellerCodes(ValidateSellerCodeRequest request) throws SnapdealWSException;

    double getPerimeterFromLBH(double length, double breadth, double height) throws SnapdealWSException;

    GetMultiplierForPerimeterResponse getMultiplierForPerimeter(GetMultiplierForPerimeterRequest request) throws SnapdealWSException;

    GetTaxInfoResponse getTaxInfoBySellerSUPCState(GetTaxInfoRequest request) throws SnapdealWSException;

    /*
     * returns the tax class based on supc and then subcat level
     */

    GetTaxClassResponse getTaxClassBySupcSubcatPairList(GetTaxClassRequest request) throws SnapdealWSException;

    GetShipFromLocationBySellerSupcListResponse getShipFromLocationBySellerSupcList(GetShipFromLocationBySellerSupcListRequest request) throws SnapdealWSException;

}
