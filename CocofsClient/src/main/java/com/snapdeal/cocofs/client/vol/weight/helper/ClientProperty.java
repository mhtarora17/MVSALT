package com.snapdeal.cocofs.client.vol.weight.helper;

public enum ClientProperty {


	COCOFS_VOL_WEIGHT_REFRESH_INTERVAL_IN_MINUTES                                   ("cocofs.vol.weight.refresh.interval.in.minutes","1440"),
	DEFAULT_VOLUMETRIC_FORMULA_DENOMINATOR										  ("default.volumetric.formula.denominator","5.0"),
	
	COCOFS_PERIMETER_MULTIPLIER_REFRESH_INTERVAL_IN_MINUTES									("cocofs.perimeter.multiplier.refresh.interval.in.minutes", "1440"),	
	DEFAULT_MULTIPLIER_FOR_PERIMETER_FORMULA										("default.multiplier.for.perimeter.formula", "4.0");
	private String name;
    private String value;

    private ClientProperty(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }
    
    public String getValue(){
        return value;
    }
    
    /**
     * Get the value of scalar {@link ClientProperty} having a Integer value.
     * 
     * @param p
     * @return
     */
    public static int getIntegerScalar(ClientProperty p) {
        String s = p.getValue();
            return Integer.parseInt(s);
        
    }

    /**
     * Get the value of scalar {@link ClientProperty} having a Integer value.
     * 
     * @param p
     * @return
     */
    public static double getDoubleScalar(ClientProperty p) {
        String s = p.getValue();
            return Double.parseDouble(s);
        
    }
}

