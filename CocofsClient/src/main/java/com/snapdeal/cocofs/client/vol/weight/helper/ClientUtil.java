package com.snapdeal.cocofs.client.vol.weight.helper;

import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.snapdeal.base.utils.DateUtils;

//singleton class, should not be changed.
public class ClientUtil {

    private static final Logger LOG                                = LoggerFactory.getLogger(ClientUtil.class);

    private Date                volumetricFormulaNextRefreshTime   = null;

    private double              volumetricFormulaDenominator;

    private int                 volumetricFormulaRefreshIntervalInMinutes;

    // code for setting perimeter multiplier	
    private Date                perimeterMultiplierNextRefreshTime = null;

    private double              perimeterMultiplier;

    private int                 perimeterMultiplierRefreshIntervalTime;

    public double getPerimeterMultiplier() {
        return perimeterMultiplier;
    }

    public void setPerimeterMultiplier(double perimeterMultiplier) {
        this.perimeterMultiplier = perimeterMultiplier;
    }

    private static ClientUtil _instance = new ClientUtil();

    public static ClientUtil getInstance() {
        return _instance;
    }

    private ClientUtil() {
        loadVolWeightRefreshInterval();
        loadPerimeterMultiplierRefreshInterval();
    }

    private void loadVolWeightRefreshInterval() {
        ClientProperty p = ClientProperty.COCOFS_VOL_WEIGHT_REFRESH_INTERVAL_IN_MINUTES;
        try {
            if (StringUtils.isNotEmpty(System.getProperty(p.getName()))) {
                LOG.debug("using system property to set Refresh Interval time of formula denominator: " + System.getProperty(p.getName()));
                this.volumetricFormulaRefreshIntervalInMinutes = Integer.parseInt(System.getProperty(p.getName()));
            } else {
                LOG.debug("using default value to set Refresh Interval time of formula denominator: " + p.getValue());
                this.volumetricFormulaRefreshIntervalInMinutes = Integer.parseInt(p.getValue());

            }
        } catch (NumberFormatException e) {
            LOG.error("Invalid int value for " + p.getName(), e);
        }
    }

    private void loadPerimeterMultiplierRefreshInterval() {

        ClientProperty p = ClientProperty.COCOFS_PERIMETER_MULTIPLIER_REFRESH_INTERVAL_IN_MINUTES;
        try {
            if (StringUtils.isNotEmpty(System.getProperty(p.getName()))) {
                LOG.info("using system property to set Refresh Interval time of perimeter formula: " + System.getProperty(p.getName()));
                this.perimeterMultiplierRefreshIntervalTime = Integer.parseInt(System.getProperty(p.getName()));
            } else {
                LOG.info("using default value to set Refresh Interval time of perimeter formula: " + p.getValue());
                this.perimeterMultiplierRefreshIntervalTime = Integer.parseInt(p.getValue());

            }
        } catch (NumberFormatException e) {
            LOG.error("Invalid int value for " + p.getName(), e);
        }

    }

    public Date getVolumetricFormulaNextRefreshTime() {
        return volumetricFormulaNextRefreshTime;
    }

    public Date getPerimeterMultiplierNextRefreshTime() {
        return perimeterMultiplierNextRefreshTime;
    }

    public void setPerimeterMultiplierNextRefreshTime(Date perimeterMultiplierNextRefreshTime) {
        this.perimeterMultiplierNextRefreshTime = perimeterMultiplierNextRefreshTime;
    }

    public void setVolumetricFormulaNextRefreshTime(Date volumetricFormulaNextRefreshTime) {
        this.volumetricFormulaNextRefreshTime = volumetricFormulaNextRefreshTime;
    }

    public double getVolumetricFormulaDenominator() {
        return volumetricFormulaDenominator;
    }

    public void setVolumetricFormulaDenominator(double volumetricFormulaDenominator) {
        this.volumetricFormulaDenominator = volumetricFormulaDenominator;
    }

    public boolean isVolumetricFormulaReloadRequired() {
        if (volumetricFormulaNextRefreshTime == null || DateUtils.isPastTime(volumetricFormulaNextRefreshTime)) {
            return true;
        }
        return false;
    }

    public boolean isPermiterMultiplierRefreshRequired() {
        if (perimeterMultiplierNextRefreshTime == null || DateUtils.isPastTime(perimeterMultiplierNextRefreshTime)) {
            return true;
        }
        return false;
    }

    public void updateVolumetricFormulaNextRefreshTime() {
        Date endTime = DateUtils.addToDate(DateUtils.getCurrentTime(), Calendar.MINUTE, volumetricFormulaRefreshIntervalInMinutes);
        setVolumetricFormulaNextRefreshTime(endTime);

    }

    public void updatePerimeterFormulaNextRefreshTime() {
        Date endTime = DateUtils.addToDate(DateUtils.getCurrentTime(), Calendar.MINUTE, perimeterMultiplierRefreshIntervalTime);
        setPerimeterMultiplierNextRefreshTime(endTime);

    }

    public double getDefaultVolumetricFormulaDenominator() {
        ClientProperty p = ClientProperty.DEFAULT_VOLUMETRIC_FORMULA_DENOMINATOR;
        return ClientProperty.getDoubleScalar(p);
    }

    public double getDefaultMultiplierForPerimeter() {
        ClientProperty p = ClientProperty.DEFAULT_MULTIPLIER_FOR_PERIMETER_FORMULA;
        return ClientProperty.getDoubleScalar(p);
    }

}
