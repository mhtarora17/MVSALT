package com.snapdeal.cocofs.client.impl;

import java.math.BigDecimal;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import com.google.gson.Gson;
import com.snapdeal.base.exception.SnapdealWSException;
import com.snapdeal.base.exception.TransportException;
import com.snapdeal.base.model.common.ServiceRequest;
import com.snapdeal.base.transport.service.ITransportService;
import com.snapdeal.base.transport.service.ITransportService.Protocol;
import com.snapdeal.cocofs.client.ICocofsClientService;
import com.snapdeal.cocofs.client.vol.weight.helper.ClientUtil;
import com.snapdeal.cocofs.model.request.AddOrUpdateCategoryModeMappingRequest;
import com.snapdeal.cocofs.model.request.AddOrUpdateSupcModeMappingRequest;
import com.snapdeal.cocofs.model.request.GetAllCategoryModeMappingRequest;
import com.snapdeal.cocofs.model.request.GetAllCenterTypesRequest;
import com.snapdeal.cocofs.model.request.GetAllDangerousGoodsTypeRequest;
import com.snapdeal.cocofs.model.request.GetAllFCDetailRequest;
import com.snapdeal.cocofs.model.request.GetAllSellersForFMRequest;
import com.snapdeal.cocofs.model.request.GetAllSupcModeMappingRequest;
import com.snapdeal.cocofs.model.request.GetCategoryModeMappingRequest;
import com.snapdeal.cocofs.model.request.GetFMAndFCBySellerSupcListRequest;
import com.snapdeal.cocofs.model.request.GetFMAndZonesBySellerSupcListRequest;
import com.snapdeal.cocofs.model.request.GetFMMappingAtSellerAndSellerSubcatRequest;
import com.snapdeal.cocofs.model.request.GetFulfilmentFeeParamRequest;
import com.snapdeal.cocofs.model.request.GetFulfilmentModelAndProductAttributesRequest;
import com.snapdeal.cocofs.model.request.GetFulfilmentModelAndWeightRequest;
import com.snapdeal.cocofs.model.request.GetFulfilmentModelBySellerSupcSubcatRequest;
import com.snapdeal.cocofs.model.request.GetFulfilmentModelRequest;
import com.snapdeal.cocofs.model.request.GetFulfilmentModelsForSellerRequest;
import com.snapdeal.cocofs.model.request.GetGiftWrapperInfoBySUPCSellerListRequest;
import com.snapdeal.cocofs.model.request.GetGiftWrapperInfoBySUPCSellerListResponse;
import com.snapdeal.cocofs.model.request.GetGiftWrapperInfoBySUPCVendorRequest;
import com.snapdeal.cocofs.model.request.GetMultiplierForPerimeterRequest;
import com.snapdeal.cocofs.model.request.GetProductDeliveryAndPackagingTypeRequest;
import com.snapdeal.cocofs.model.request.GetProductDeliveryTypeRequest;
import com.snapdeal.cocofs.model.request.GetProductFulfilmentAttributeRequest;
import com.snapdeal.cocofs.model.request.GetProductPackagingTypeRequest;
import com.snapdeal.cocofs.model.request.GetProductSystemWeightCapturedFlagRequest;
import com.snapdeal.cocofs.model.request.GetProductWeightNotificationRequest;
import com.snapdeal.cocofs.model.request.GetSDFulfilledStatusBySupcSellersSubcatRequest;
import com.snapdeal.cocofs.model.request.GetSDFulfilledStatusBySupcSellersSubcatResponse;
import com.snapdeal.cocofs.model.request.GetSellerSubcatFMMappingRequest;
import com.snapdeal.cocofs.model.request.GetShipFromLocationBySellerSupcListRequest;
import com.snapdeal.cocofs.model.request.GetSupcModeMappingRequest;
import com.snapdeal.cocofs.model.request.GetTaxClassRequest;
import com.snapdeal.cocofs.model.request.GetTaxInfoRequest;
import com.snapdeal.cocofs.model.request.GetTaxRateInfoBySUPCStateSellerPriceRequest;
import com.snapdeal.cocofs.model.request.GetVolumetricWeightFormulaParamsRequest;
import com.snapdeal.cocofs.model.request.GetZonesBySellerSupcListRequest;
import com.snapdeal.cocofs.model.request.RemoveCategoryModeMappingRequest;
import com.snapdeal.cocofs.model.request.RemoveSupcModeMappingRequest;
import com.snapdeal.cocofs.model.request.SetFMMappingRequest;
import com.snapdeal.cocofs.model.request.SetProductCapturedWeightRequest;
import com.snapdeal.cocofs.model.request.SetProductFulfilmentAttributeRequest;
import com.snapdeal.cocofs.model.request.SetSellerDetailsRequest;
import com.snapdeal.cocofs.model.request.SetSellerFMMappingRequest;
import com.snapdeal.cocofs.model.request.SetSellerSubcatFMMappingRequest;
import com.snapdeal.cocofs.model.request.ValidateSellerCodeRequest;
import com.snapdeal.cocofs.model.response.AddOrUpdateCategoryModeMappingResponse;
import com.snapdeal.cocofs.model.response.AddOrUpdateSupcModeMappingResponse;
import com.snapdeal.cocofs.model.response.AllCenterTypesResponse;
import com.snapdeal.cocofs.model.response.AllFCDetailResponse;
import com.snapdeal.cocofs.model.response.GetAllCategoryModeMappingResponse;
import com.snapdeal.cocofs.model.response.GetAllDangerousGoodsTypeResponse;
import com.snapdeal.cocofs.model.response.GetAllSellersForFMResponse;
import com.snapdeal.cocofs.model.response.GetAllSupcModeMappingResponse;
import com.snapdeal.cocofs.model.response.GetCategoryModeMappingResponse;
import com.snapdeal.cocofs.model.response.GetFMAndFCBySellerSupcListResponse;
import com.snapdeal.cocofs.model.response.GetFMAndZonesBySellerSupcListResponse;
import com.snapdeal.cocofs.model.response.GetFMMappingAtSellerAndSellerSubcatResponse;
import com.snapdeal.cocofs.model.response.GetFulFilmentFeeParamResponse;
import com.snapdeal.cocofs.model.response.GetFulfilmentModelAndProductAttributesResponse;
import com.snapdeal.cocofs.model.response.GetFulfilmentModelAndWeightResponse;
import com.snapdeal.cocofs.model.response.GetFulfilmentModelBySellerSupcSubcatResponse;
import com.snapdeal.cocofs.model.response.GetFulfilmentModelResponse;
import com.snapdeal.cocofs.model.response.GetFulfilmentModelsForSellerResponse;
import com.snapdeal.cocofs.model.response.GetGiftWrapperInfoBySUPCVendorResponse;
import com.snapdeal.cocofs.model.response.GetMultiplierForPerimeterResponse;
import com.snapdeal.cocofs.model.response.GetProductDeliveryAndPackagingTypeResponse;
import com.snapdeal.cocofs.model.response.GetProductDeliveryTypeResponse;
import com.snapdeal.cocofs.model.response.GetProductFulfilmentAttributeResponse;
import com.snapdeal.cocofs.model.response.GetProductPackagingTypeResponse;
import com.snapdeal.cocofs.model.response.GetProductSystemWeightCapturedFlagResponse;
import com.snapdeal.cocofs.model.response.GetProductWeightNotificationResponse;
import com.snapdeal.cocofs.model.response.GetSellerSubcatFMMappingResponse;
import com.snapdeal.cocofs.model.response.GetShipFromLocationBySellerSupcListResponse;
import com.snapdeal.cocofs.model.response.GetSupcModeMappingResponse;
import com.snapdeal.cocofs.model.response.GetTaxClassResponse;
import com.snapdeal.cocofs.model.response.GetTaxInfoResponse;
import com.snapdeal.cocofs.model.response.GetTaxRateInfoBySUPCStateSellerPriceResponse;
import com.snapdeal.cocofs.model.response.GetVolumetricWeightFormulaParamsResponse;
import com.snapdeal.cocofs.model.response.GetZonesBySellerSupcListResponse;
import com.snapdeal.cocofs.model.response.RemoveCategoryModeMappingResponse;
import com.snapdeal.cocofs.model.response.RemoveSupcModeMappingResponse;
import com.snapdeal.cocofs.model.response.SetFMMappingResponse;
import com.snapdeal.cocofs.model.response.SetProductAttributeFulfilmentResponse;
import com.snapdeal.cocofs.model.response.SetProductCapturedWeightResponse;
import com.snapdeal.cocofs.model.response.SetSellerDetailsResponse;
import com.snapdeal.cocofs.model.response.SetSellerFMMappingResponse;
import com.snapdeal.cocofs.model.response.SetSellerSubcatFMMappingResponse;
import com.snapdeal.cocofs.model.response.ValidateSellerCodeResponse;

/**
 * @author nikhil
 */
@Service("cocofsClientService")
public class CocofsClientServiceImpl implements ICocofsClientService {
    private static final Gson   DEBUG_JSON = new Gson();

    private static final Logger LOG        = LoggerFactory.getLogger(CocofsClientServiceImpl.class);
    @Autowired
    private ITransportService   transportService;

    private String              webServiceUrl;

    @PostConstruct
    public void init() {
        transportService.registerService("/service/cocofs/", "cocofsserver.");
        transportService.registerService("/service/cocofs/v2/", "cocofsserver.");
    }

    <T extends ServiceRequest> String getCocofsWebServiceURL(T request) {

        if (null == request.getRequestProtocol()) {
            request.setRequestProtocol(Protocol.PROTOCOL_PROTOSTUFF);
        }
        if (null == request.getResponseProtocol()) {
            request.setResponseProtocol(Protocol.PROTOCOL_PROTOSTUFF);
        }
        if (LOG.isDebugEnabled()) {
            LOG.debug("CocoFS Service Call {} ", DEBUG_JSON.toJson(request));
        }

        return new StringBuilder(webServiceUrl).append("/service/cocofs").toString();
    }

    @Override
    public void setCocoFSClientServiceURL(String url) {
        webServiceUrl = url;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public GetProductFulfilmentAttributeResponse getProductFulfilmentAttributes(GetProductFulfilmentAttributeRequest request) throws TransportException {
        GetProductFulfilmentAttributeResponse response = new GetProductFulfilmentAttributeResponse();
        String url = new StringBuilder(getCocofsWebServiceURL(request)).append("/getProductFulfilmentAttributes").toString();
        response = (GetProductFulfilmentAttributeResponse) transportService.executeRequest(url, request, null, GetProductFulfilmentAttributeResponse.class);
        return response;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public SetProductAttributeFulfilmentResponse setProductFulfilmentAttributes(SetProductFulfilmentAttributeRequest request) throws TransportException {
        SetProductAttributeFulfilmentResponse response = new SetProductAttributeFulfilmentResponse();
        String url = new StringBuilder(getCocofsWebServiceURL(request)).append("/setProductFulfilmentAttributes").toString();
        response = (SetProductAttributeFulfilmentResponse) transportService.executeRequest(url, request, null, SetProductAttributeFulfilmentResponse.class);
        return response;
    }

    @Override
    public GetProductPackagingTypeResponse getProductPackagingType(GetProductPackagingTypeRequest request) throws TransportException {
        GetProductPackagingTypeResponse response = new GetProductPackagingTypeResponse();
        String url = new StringBuilder(getCocofsWebServiceURL(request)).append("/getPackagingType").toString();
        response = (GetProductPackagingTypeResponse) transportService.executeRequest(url, request, null, GetProductPackagingTypeResponse.class);
        return response;
    }

    @Override
    public GetProductDeliveryTypeResponse getProductDeliveryType(GetProductDeliveryTypeRequest request) throws TransportException {
        GetProductDeliveryTypeResponse response = new GetProductDeliveryTypeResponse();
        String url = new StringBuilder(getCocofsWebServiceURL(request)).append("/getDeliveryType").toString();
        response = (GetProductDeliveryTypeResponse) transportService.executeRequest(url, request, null, GetProductDeliveryTypeResponse.class);
        return response;
    }

    @Override
    public GetProductDeliveryAndPackagingTypeResponse getProductDeliveryAndPackagingType(GetProductDeliveryAndPackagingTypeRequest request) throws TransportException {
        GetProductDeliveryAndPackagingTypeResponse response = new GetProductDeliveryAndPackagingTypeResponse();
        String url = new StringBuilder(getCocofsWebServiceURL(request)).append("/getDeliveryAndPackagingType").toString();
        response = (GetProductDeliveryAndPackagingTypeResponse) transportService.executeRequest(url, request, null, GetProductDeliveryAndPackagingTypeResponse.class);
        return response;
    }

    @Override
    public GetFulFilmentFeeParamResponse getFulfilmentFeeParams(GetFulfilmentFeeParamRequest request) throws TransportException {
        GetFulFilmentFeeParamResponse response = new GetFulFilmentFeeParamResponse();
        String url = new StringBuilder(getCocofsWebServiceURL(request)).append("/getFulfilmentFeeParams").toString();
        response = (GetFulFilmentFeeParamResponse) transportService.executeRequest(url, request, null, GetFulFilmentFeeParamResponse.class);
        return response;
    }

    @Override
    public GetFulfilmentModelResponse getFulfilmentModel(GetFulfilmentModelRequest request) throws TransportException {
        GetFulfilmentModelResponse response = new GetFulfilmentModelResponse();
        String url = new StringBuilder(getCocofsWebServiceURL(request)).append("/getFulfilmentModel").toString();
        response = (GetFulfilmentModelResponse) transportService.executeRequest(url, request, null, GetFulfilmentModelResponse.class);
        return response;
    }

    @Override
    public com.snapdeal.cocofs.model.response.v2.GetFulFilmentFeeParamResponse getFulfilmentFeeParams(com.snapdeal.cocofs.model.request.v2.GetFulfilmentFeeParamRequest request)
            throws TransportException {
        String url = new StringBuilder(getCocofsWebServiceURL(request)).append("/v2/getFulfilmentFeeParams").toString();
        com.snapdeal.cocofs.model.response.v2.GetFulFilmentFeeParamResponse response = (com.snapdeal.cocofs.model.response.v2.GetFulFilmentFeeParamResponse) transportService.executeRequest(
                url, request, null, com.snapdeal.cocofs.model.response.v2.GetFulFilmentFeeParamResponse.class);
        return response;
    }

    @Override
    public GetProductWeightNotificationResponse getProductWeightNotifications(GetProductWeightNotificationRequest request) throws TransportException {
        GetProductWeightNotificationResponse response = new GetProductWeightNotificationResponse();
        String url = new StringBuilder(getCocofsWebServiceURL(request)).append("/getProductWeightNotifications").toString();
        response = (GetProductWeightNotificationResponse) transportService.executeRequest(url, request, null, GetProductWeightNotificationResponse.class);
        return response;
    }

    @Override
    public GetFulfilmentModelAndWeightResponse getFulfilmentModelAndWeight(GetFulfilmentModelAndWeightRequest request) throws TransportException {
        GetFulfilmentModelAndWeightResponse response = new GetFulfilmentModelAndWeightResponse();
        String url = new StringBuilder(getCocofsWebServiceURL(request)).append("/getFulfilmentModelAndWeight").toString();
        response = (GetFulfilmentModelAndWeightResponse) transportService.executeRequest(url, request, null, GetFulfilmentModelAndWeightResponse.class);
        return response;
    }

    @Override
    public GetFulfilmentModelBySellerSupcSubcatResponse getFulfilmentModelBySellerSupcSubcat(GetFulfilmentModelBySellerSupcSubcatRequest request) throws TransportException {
        GetFulfilmentModelBySellerSupcSubcatResponse response = new GetFulfilmentModelBySellerSupcSubcatResponse();
        String url = new StringBuilder(getCocofsWebServiceURL(request)).append("/getFulfilmentModelBySellerSupcSubcat").toString();
        response = (GetFulfilmentModelBySellerSupcSubcatResponse) transportService.executeRequest(url, request, null, GetFulfilmentModelBySellerSupcSubcatResponse.class);
        return response;
    }

    @Override
    public GetFMMappingAtSellerAndSellerSubcatResponse getFMMappingAtSellerAndSellerSubcat(GetFMMappingAtSellerAndSellerSubcatRequest request) throws TransportException {
        GetFMMappingAtSellerAndSellerSubcatResponse response = new GetFMMappingAtSellerAndSellerSubcatResponse();
        String url = new StringBuilder(getCocofsWebServiceURL(request)).append("/getFMMappingAtSellerAndSellerSubcat").toString();
        response = (GetFMMappingAtSellerAndSellerSubcatResponse) transportService.executeRequest(url, request, null, GetFMMappingAtSellerAndSellerSubcatResponse.class);
        return response;
    }

    @Override
    public SetFMMappingResponse setFMMapping(SetFMMappingRequest request) throws TransportException {
        SetFMMappingResponse response = new SetFMMappingResponse();
        String url = new StringBuilder(getCocofsWebServiceURL(request)).append("/setFMMapping").toString();
        response = (SetFMMappingResponse) transportService.executeRequest(url, request, null, SetFMMappingResponse.class);
        return response;
    }

    @Override
    public GetFulfilmentModelAndProductAttributesResponse getFulfilmentModelAndProductAttributes(GetFulfilmentModelAndProductAttributesRequest request) throws TransportException {
        GetFulfilmentModelAndProductAttributesResponse response = new GetFulfilmentModelAndProductAttributesResponse();
        String url = new StringBuilder(getCocofsWebServiceURL(request)).append("/getFulfilmentModelAndProductAttributes").toString();
        response = (GetFulfilmentModelAndProductAttributesResponse) transportService.executeRequest(url, request, null, GetFulfilmentModelAndProductAttributesResponse.class);
        return response;
    }

    @Override
    public GetFulfilmentModelsForSellerResponse getFulfilmentModelsForSeller(GetFulfilmentModelsForSellerRequest request) throws TransportException {
        GetFulfilmentModelsForSellerResponse response = new GetFulfilmentModelsForSellerResponse();
        String url = new StringBuilder(getCocofsWebServiceURL(request)).append("/getFulfilmentModelsForSeller").toString();
        response = (GetFulfilmentModelsForSellerResponse) transportService.executeRequest(url, request, null, GetFulfilmentModelsForSellerResponse.class);
        return response;
    }

    @Override
    public GetAllCategoryModeMappingResponse getAllCategoryModeMapping(GetAllCategoryModeMappingRequest request) throws TransportException {
        GetAllCategoryModeMappingResponse response = new GetAllCategoryModeMappingResponse();
        String url = new StringBuilder(getCocofsWebServiceURL(request)).append("/getAllCategoryModeMapping").toString();
        response = (GetAllCategoryModeMappingResponse) transportService.executeRequest(url, request, null, GetAllCategoryModeMappingResponse.class);
        return response;
    }

    @Override
    public GetAllSupcModeMappingResponse getAllSupcModeMapping(GetAllSupcModeMappingRequest request) throws TransportException {
        GetAllSupcModeMappingResponse response = new GetAllSupcModeMappingResponse();
        String url = new StringBuilder(getCocofsWebServiceURL(request)).append("/getAllSupcModeMapping").toString();
        response = (GetAllSupcModeMappingResponse) transportService.executeRequest(url, request, null, GetAllSupcModeMappingResponse.class);
        return response;
    }

    @Override
    public GetCategoryModeMappingResponse getCategoryModeMapping(GetCategoryModeMappingRequest request) throws TransportException {
        GetCategoryModeMappingResponse response = new GetCategoryModeMappingResponse();
        String url = new StringBuilder(getCocofsWebServiceURL(request)).append("/getCategoryModeMapping").toString();
        response = (GetCategoryModeMappingResponse) transportService.executeRequest(url, request, null, GetCategoryModeMappingResponse.class);
        return response;
    }

    @Override
    public GetSupcModeMappingResponse getSupcModeMapping(GetSupcModeMappingRequest request) throws TransportException {
        GetSupcModeMappingResponse response = new GetSupcModeMappingResponse();
        String url = new StringBuilder(getCocofsWebServiceURL(request)).append("/getSupcModeMapping").toString();
        response = (GetSupcModeMappingResponse) transportService.executeRequest(url, request, null, GetSupcModeMappingResponse.class);
        return response;
    }

    @Override
    public AddOrUpdateCategoryModeMappingResponse addOrUpdateCategoryModeMapping(AddOrUpdateCategoryModeMappingRequest request) throws TransportException {
        AddOrUpdateCategoryModeMappingResponse response = new AddOrUpdateCategoryModeMappingResponse();
        String url = new StringBuilder(getCocofsWebServiceURL(request)).append("/addOrUpdateCategoryModeMapping").toString();
        response = (AddOrUpdateCategoryModeMappingResponse) transportService.executeRequest(url, request, null, AddOrUpdateCategoryModeMappingResponse.class);
        return response;
    }

    @Override
    public AddOrUpdateSupcModeMappingResponse addOrUpdateSupcModeMapping(AddOrUpdateSupcModeMappingRequest request) throws TransportException {
        AddOrUpdateSupcModeMappingResponse response = new AddOrUpdateSupcModeMappingResponse();
        String url = new StringBuilder(getCocofsWebServiceURL(request)).append("/addOrUpdateSupcModeMapping").toString();
        response = (AddOrUpdateSupcModeMappingResponse) transportService.executeRequest(url, request, null, AddOrUpdateSupcModeMappingResponse.class);
        return response;
    }

    @Override
    public RemoveCategoryModeMappingResponse removeCategoryModeMapping(RemoveCategoryModeMappingRequest request) throws TransportException {
        RemoveCategoryModeMappingResponse response = new RemoveCategoryModeMappingResponse();
        String url = new StringBuilder(getCocofsWebServiceURL(request)).append("/removeCategoryModeMapping").toString();
        response = (RemoveCategoryModeMappingResponse) transportService.executeRequest(url, request, null, RemoveCategoryModeMappingResponse.class);
        return response;
    }

    @Override
    public RemoveSupcModeMappingResponse removeSupcModeMapping(RemoveSupcModeMappingRequest request) throws TransportException {
        RemoveSupcModeMappingResponse response = new RemoveSupcModeMappingResponse();
        String url = new StringBuilder(getCocofsWebServiceURL(request)).append("/removeSupcModeMapping").toString();
        response = (RemoveSupcModeMappingResponse) transportService.executeRequest(url, request, null, RemoveSupcModeMappingResponse.class);
        return response;
    }

    @Override
    public GetProductSystemWeightCapturedFlagResponse getProductSystemWeightCapturedFlag(GetProductSystemWeightCapturedFlagRequest request) throws TransportException {
        GetProductSystemWeightCapturedFlagResponse response = new GetProductSystemWeightCapturedFlagResponse();
        String url = new StringBuilder(getCocofsWebServiceURL(request)).append("/getProductSystemWeightCapturedFlag").toString();
        response = (GetProductSystemWeightCapturedFlagResponse) transportService.executeRequest(url, request, null, GetProductSystemWeightCapturedFlagResponse.class);
        return response;
    }

    @Override
    public SetProductCapturedWeightResponse setProductCapturedWeight(SetProductCapturedWeightRequest request) throws TransportException {
        SetProductCapturedWeightResponse response = new SetProductCapturedWeightResponse();
        String url = new StringBuilder(getCocofsWebServiceURL(request)).append("/setProductCapturedWeight").toString();
        response = (SetProductCapturedWeightResponse) transportService.executeRequest(url, request, null, SetProductCapturedWeightResponse.class);
        return response;
    }

    @Override
    public GetAllDangerousGoodsTypeResponse getAllDangerousGoodsType(GetAllDangerousGoodsTypeRequest request) throws TransportException {
        GetAllDangerousGoodsTypeResponse response = new GetAllDangerousGoodsTypeResponse();
        String url = new StringBuilder(getCocofsWebServiceURL(request)).append("/getAllDangerousGoodsType").toString();
        response = (GetAllDangerousGoodsTypeResponse) transportService.executeRequest(url, request, null, GetAllDangerousGoodsTypeResponse.class);
        return response;
    }

    @Override
    @Deprecated
    public SetSellerFMMappingResponse setSellerFMMapping(SetSellerFMMappingRequest request) throws TransportException {
        SetSellerFMMappingResponse response = new SetSellerFMMappingResponse();
        String url = new StringBuilder(getCocofsWebServiceURL(request)).append("/setSellerFMMapping").toString();
        response = (SetSellerFMMappingResponse) transportService.executeRequest(url, request, null, SetSellerFMMappingResponse.class);
        return response;
    }

    @Override
    @Deprecated
    public SetSellerSubcatFMMappingResponse setSellerSubcatFMMapping(SetSellerSubcatFMMappingRequest request) throws TransportException {
        SetSellerSubcatFMMappingResponse response = new SetSellerSubcatFMMappingResponse();
        String url = new StringBuilder(getCocofsWebServiceURL(request)).append("/setSellerSubcatFMMapping").toString();
        response = (SetSellerSubcatFMMappingResponse) transportService.executeRequest(url, request, null, SetSellerSubcatFMMappingResponse.class);
        return response;
    }

    @Override
    @Deprecated
    public GetSellerSubcatFMMappingResponse getSellerSubcatFMMapping(GetSellerSubcatFMMappingRequest request) throws TransportException {
        GetSellerSubcatFMMappingResponse response = new GetSellerSubcatFMMappingResponse();
        String url = new StringBuilder(getCocofsWebServiceURL(request)).append("/getSellerSubcatFMMapping").toString();
        response = (GetSellerSubcatFMMappingResponse) transportService.executeRequest(url, request, null, GetSellerSubcatFMMappingResponse.class);
        return response;
    }

    @Override
    public GetGiftWrapperInfoBySUPCVendorResponse getGiftWrapperInfoBySUPCVendor(GetGiftWrapperInfoBySUPCVendorRequest request) throws TransportException {
        GetGiftWrapperInfoBySUPCVendorResponse response = new GetGiftWrapperInfoBySUPCVendorResponse();
        String url = new StringBuilder(getCocofsWebServiceURL(request)).append("/getGiftWrapperInfoBySUPCVendor").toString();
        response = (GetGiftWrapperInfoBySUPCVendorResponse) transportService.executeRequest(url, request, null, GetGiftWrapperInfoBySUPCVendorResponse.class);
        return response;
    }

    @Override
    public GetGiftWrapperInfoBySUPCSellerListResponse getGiftWrapperInfoBySUPCSellerList(GetGiftWrapperInfoBySUPCSellerListRequest request) throws TransportException {
        GetGiftWrapperInfoBySUPCSellerListResponse response = new GetGiftWrapperInfoBySUPCSellerListResponse();
        String url = new StringBuilder(getCocofsWebServiceURL(request)).append("/getGiftWrapperInfoBySUPCSellerList").toString();
        response = (GetGiftWrapperInfoBySUPCSellerListResponse) transportService.executeRequest(url, request, null, GetGiftWrapperInfoBySUPCSellerListResponse.class);
        return response;
    }

    @Deprecated
    @Override
    public GetTaxRateInfoBySUPCStateSellerPriceResponse getTaxRateInfoBySUPCAndState(GetTaxRateInfoBySUPCStateSellerPriceRequest request) throws TransportException {
        GetTaxRateInfoBySUPCStateSellerPriceResponse response = new GetTaxRateInfoBySUPCStateSellerPriceResponse();
        String url = new StringBuilder(getCocofsWebServiceURL(request)).append("/getTaxRateBySellerSUPCState").toString();
        response = (GetTaxRateInfoBySUPCStateSellerPriceResponse) transportService.executeRequest(url, request, null, GetTaxRateInfoBySUPCStateSellerPriceResponse.class);
        return response;
    }

    @Override
    public GetTaxInfoResponse getTaxInfoBySellerSUPCState(GetTaxInfoRequest request) throws SnapdealWSException {
        String url = new StringBuilder(getCocofsWebServiceURL(request)).append("/getTaxInfoBySellerSUPCState").toString();
        GetTaxInfoResponse response = (GetTaxInfoResponse) transportService.executeRequest(url, request, null, GetTaxInfoResponse.class);
        return response;
    }

    @Override
    public GetSDFulfilledStatusBySupcSellersSubcatResponse getSDFulfilledStatusBySupcSellersSubcat(GetSDFulfilledStatusBySupcSellersSubcatRequest request)
            throws TransportException {
        GetSDFulfilledStatusBySupcSellersSubcatResponse response = new GetSDFulfilledStatusBySupcSellersSubcatResponse();
        String url = new StringBuilder(getCocofsWebServiceURL(request)).append("/getSDFulfilledStatusBySupcSellersSubcat").toString();
        response = (GetSDFulfilledStatusBySupcSellersSubcatResponse) transportService.executeRequest(url, request, null, GetSDFulfilledStatusBySupcSellersSubcatResponse.class);
        return response;
    }

    @Override
    public Double getEffectiveWeight(Double deadWeight, Double volumetricWeight) {
        return (deadWeight > volumetricWeight) ? deadWeight : volumetricWeight;
    }

    @Override
    public double getVolumetricWeightFromLBH(double l, double b, double h) {
        return (l * b * h) / getVolumetricFormulaDenominator();
    }

    private double getVolumetricFormulaDenominator() {

        ClientUtil volumetricWeightHelper = ClientUtil.getInstance();

        if (volumetricWeightHelper.isVolumetricFormulaReloadRequired()) {

            try {
                //call cocofs api
                LOG.info("calling cocofsAPI to fetch latest formula denominator");
                GetVolumetricWeightFormulaParamsResponse response = getVolumetricWeightFormulaParams(new GetVolumetricWeightFormulaParamsRequest());
                LOG.info("response received {}", response);
                if (response != null && response.getDenominator() > 0) {
                    volumetricWeightHelper.setVolumetricFormulaDenominator(response.getDenominator());
                    volumetricWeightHelper.updateVolumetricFormulaNextRefreshTime();
                } else {
                    LOG.info("Invalid denominator value ,  response  recieved is {}", response);
                }

            } catch (SnapdealWSException e) {
                LOG.error("error while fetching VolumetricWeightFormulaParams from API, using and returning default values", e);
            }

        }
        return (volumetricWeightHelper.getVolumetricFormulaDenominator() > 0) ? (volumetricWeightHelper.getVolumetricFormulaDenominator())
                : (volumetricWeightHelper.getDefaultVolumetricFormulaDenominator());
    }

    @Override
    public GetVolumetricWeightFormulaParamsResponse getVolumetricWeightFormulaParams(@RequestBody GetVolumetricWeightFormulaParamsRequest request) throws SnapdealWSException {
        GetVolumetricWeightFormulaParamsResponse response = new GetVolumetricWeightFormulaParamsResponse();
        String url = new StringBuilder(getCocofsWebServiceURL(request)).append("/getVolumetricWeightFormulaParams").toString();
        response = (GetVolumetricWeightFormulaParamsResponse) transportService.executeRequest(url, request, null, GetVolumetricWeightFormulaParamsResponse.class);
        return response;

    }

    @Override
    public GetFMAndFCBySellerSupcListResponse getFMAndFCBySellerSupcList(GetFMAndFCBySellerSupcListRequest request) throws TransportException {
        GetFMAndFCBySellerSupcListResponse response = new GetFMAndFCBySellerSupcListResponse();
        String url = new StringBuilder(getCocofsWebServiceURL(request)).append("/getFMAndFCBySellerSupcList").toString();
        response = (GetFMAndFCBySellerSupcListResponse) transportService.executeRequest(url, request, null, GetFMAndFCBySellerSupcListResponse.class);
        return response;
    }

    @Override
    public AllFCDetailResponse getAllFCDetails(GetAllFCDetailRequest request) throws TransportException {
        AllFCDetailResponse response = new AllFCDetailResponse();
        String url = new StringBuilder(getCocofsWebServiceURL(request)).append("/getAllFCDetails").toString();
        response = (AllFCDetailResponse) transportService.executeRequest(url, request, null, AllFCDetailResponse.class);
        return response;
    }

    @Override
    public GetAllSellersForFMResponse getAllSellersForFM(GetAllSellersForFMRequest request) throws TransportException {
        String url = new StringBuilder(getCocofsWebServiceURL(request)).append("/getAllSellersForFM").toString();
        GetAllSellersForFMResponse response = (GetAllSellersForFMResponse) transportService.executeRequest(url, request, null, GetAllSellersForFMResponse.class);
        return response;
    }

    @Override
    public SetSellerDetailsResponse createOrUpdateSeller(SetSellerDetailsRequest request) throws TransportException {
        String url = new StringBuilder(getCocofsWebServiceURL(request)).append("/createOrUpdateSeller").toString();
        SetSellerDetailsResponse response = (SetSellerDetailsResponse) transportService.executeRequest(url, request, null, SetSellerDetailsResponse.class);
        return response;
    }

    @Override
    public GetZonesBySellerSupcListResponse getZonesBySellerSupcList(GetZonesBySellerSupcListRequest request) throws SnapdealWSException {
        String url = new StringBuilder(getCocofsWebServiceURL(request)).append("/getZonesBySellerSupcList").toString();
        GetZonesBySellerSupcListResponse response = (GetZonesBySellerSupcListResponse) transportService.executeRequest(url, request, null, GetZonesBySellerSupcListResponse.class);
        return response;
    }

    @Override
    public GetFMAndZonesBySellerSupcListResponse getFMAndZonesBySellerSupcList(GetFMAndZonesBySellerSupcListRequest request) throws SnapdealWSException {
        String url = new StringBuilder(getCocofsWebServiceURL(request)).append("/getFMAndZonesBySellerSupcList").toString();
        GetFMAndZonesBySellerSupcListResponse response = (GetFMAndZonesBySellerSupcListResponse) transportService.executeRequest(url, request, null,
                GetFMAndZonesBySellerSupcListResponse.class);
        return response;
    }

    @Override
    public ValidateSellerCodeResponse validateSellerCodes(ValidateSellerCodeRequest request) throws SnapdealWSException {
        String url = new StringBuilder(getCocofsWebServiceURL(request)).append("/validateSellerCodes").toString();
        ValidateSellerCodeResponse response = (ValidateSellerCodeResponse) transportService.executeRequest(url, request, null, ValidateSellerCodeResponse.class);
        return response;
    }

    @Override
    public AllCenterTypesResponse getAllCenterTypes(GetAllCenterTypesRequest request) throws TransportException {
        String url = new StringBuilder(getCocofsWebServiceURL(request)).append("/getAllCenterTypes").toString();
        AllCenterTypesResponse response = (AllCenterTypesResponse) transportService.executeRequest(url, request, null, AllCenterTypesResponse.class);
        return response;
    }

    @Override
    public double getPerimeterFromLBH(double length, double breadth, double height) throws SnapdealWSException {
        LOG.info("Call getPerimeterFromLBH() for length: {}, breadth: {}, height: {}", length, breadth, height);
        double multiplier = getPerimterMultiplierValue();
        BigDecimal l = BigDecimal.valueOf(length);
        BigDecimal b = BigDecimal.valueOf(breadth);
        BigDecimal h = BigDecimal.valueOf(height);
        BigDecimal perimeter = (l.add(b).add(h)).multiply(BigDecimal.valueOf(multiplier));
        return perimeter.doubleValue();
    }

    private double getPerimterMultiplierValue() {

        // TODO
        ClientUtil clientUtil = ClientUtil.getInstance();

        if (clientUtil.isPermiterMultiplierRefreshRequired()) {

            try {
                // call cocofs api
                LOG.info("calling cocofsAPI to fetch latest multiplier for perimeter formula");
                GetMultiplierForPerimeterResponse response = getMultiplierForPerimeter(new GetMultiplierForPerimeterRequest());
                LOG.info("response received from cocofs for multiplier {}", response);
                if (response != null && response.getMultiplier() > 0) {
                    clientUtil.setPerimeterMultiplier(response.getMultiplier());
                    clientUtil.updatePerimeterFormulaNextRefreshTime();
                } else {
                    LOG.info("Invalid denominator value ,  response  recieved is {}", response);
                }

            } catch (SnapdealWSException e) {
                LOG.error("error while fetching Multiplier for perimeter from API, using and returning default values", e);
            }

        } else {
            LOG.info("Useing preset value for perimeter multiplier" + clientUtil.getPerimeterMultiplier());
        }
        return (clientUtil.getPerimeterMultiplier() > 0) ? (clientUtil.getPerimeterMultiplier()) : (clientUtil.getDefaultMultiplierForPerimeter());

    }

    @Override
    public GetMultiplierForPerimeterResponse getMultiplierForPerimeter(GetMultiplierForPerimeterRequest request) throws SnapdealWSException {

        GetMultiplierForPerimeterResponse response = new GetMultiplierForPerimeterResponse();
        String url = new StringBuilder(getCocofsWebServiceURL(request)).append("/getMultiplierForPerimeter").toString();
        response = (GetMultiplierForPerimeterResponse) transportService.executeRequest(url, request, null, GetMultiplierForPerimeterResponse.class);
        return response;
    }

    @Override
    public GetTaxClassResponse getTaxClassBySupcSubcatPairList(GetTaxClassRequest request) throws SnapdealWSException {
        GetTaxClassResponse response = new GetTaxClassResponse();
        String url = new StringBuilder(getCocofsWebServiceURL(request)).append("/getTaxClassBySupcSubcatPairList").toString();
        response = (GetTaxClassResponse) transportService.executeRequest(url, request, null, GetTaxClassResponse.class);
        return response;
    }
    
    @Override
    public GetShipFromLocationBySellerSupcListResponse getShipFromLocationBySellerSupcList(GetShipFromLocationBySellerSupcListRequest request) throws SnapdealWSException {
        String url = new StringBuilder(getCocofsWebServiceURL(request)).append("/getShipFromLocationBySellerSupcList").toString();
        GetShipFromLocationBySellerSupcListResponse response = (GetShipFromLocationBySellerSupcListResponse) transportService.executeRequest(url, request, null, GetShipFromLocationBySellerSupcListResponse.class);
        return response;
    }

   
}
