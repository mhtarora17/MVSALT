package com.snapdeal.cocofs.client;

import com.snapdeal.cocofs.client.exception.DataNotFoundException;
import com.snapdeal.cocofs.client.exception.MalformedRequestException;
import com.snapdeal.cocofs.client.exception.RequestBatchSizeExceededException;

/**
 * @author nikhil
 */
public interface IClientHelper {

    public <T> void genericResponseCheckForBatchGetAPIs(T serviceResponse) throws MalformedRequestException, RequestBatchSizeExceededException, DataNotFoundException, Exception;

    public <T> void genericResponseCheckerForSetAPIs(T t) throws MalformedRequestException, RequestBatchSizeExceededException, Exception;

    public <T> void genericResponseCheckForGetAPIs(T t) throws MalformedRequestException, DataNotFoundException, Exception;

}
