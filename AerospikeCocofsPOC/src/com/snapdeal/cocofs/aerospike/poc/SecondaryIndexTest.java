package com.snapdeal.cocofs.aerospike.poc;

import com.aerospike.client.AerospikeClient;
import com.aerospike.client.AerospikeException;
import com.aerospike.client.Host;
import com.aerospike.client.Key;
import com.aerospike.client.Record;
import com.aerospike.client.Value;
import com.aerospike.client.policy.ClientPolicy;
import com.aerospike.client.policy.Policy;
import com.aerospike.client.policy.Priority;
import com.aerospike.client.policy.QueryPolicy;
import com.aerospike.client.query.Filter;
import com.aerospike.client.query.RecordSet;
import com.aerospike.client.query.Statement;

public class SecondaryIndexTest {

	public static void main(String[] args){
		String hostname = "127.0.0.1";
		int port = 3000;
		
		String hostnames[] = new String[1];
		hostnames[0] = hostname;
		try {
			AerospikeClient client = getClient(hostnames, port);
			QueryPolicy qpolicy = new QueryPolicy();
			//qpolicy.recordQueueSize = 10;
			//qpolicy.maxConcurrentNodes = 10;
			Policy policy = new Policy();
			
			policy.priority = Priority.MEDIUM;
			long timeTaken = System.currentTimeMillis();
			Statement stmt = new Statement();
			stmt.setNamespace("cocofs");
			stmt.setSetName("seller_supc_fm_mapping_vo");
			stmt.setFilters(Filter.equal("sc", Value.get("ab28bedf-7")));
			// Execute the query
			RecordSet recordSet = client.query(qpolicy, stmt);
			
			
			Key key;
			Record record;
			int count = 0;
			try {
				   while (recordSet != null && recordSet.next()) {
				       key = recordSet.getKey();
				       record = recordSet.getRecord();

				       count ++;

				   }
				} finally {
				    recordSet.close();
				}
			
			timeTaken = System.currentTimeMillis() - timeTaken;
			
			System.out.println("Time taken = "+(double)timeTaken/1000.0 +" sec");
			
			System.out.println("Count = "+count);
			//System.out.println(recordSet.getKey());
		} catch (AerospikeException e) {
			e.printStackTrace();
			System.out.println("Not able to create the client...");
		}
		

		
	}
	
	public static AerospikeClient getClient(String[] hostnames, int port){
		
		Host[] hosts = new Host[hostnames.length];
		
		for(int i=0;i<hostnames.length;i++){
			hosts[i] = new Host(hostnames[i],port);
		}
		
		ClientPolicy cp = new ClientPolicy();
		cp.failIfNotConnected = false;
		
		try {
			return new AerospikeClient(cp, hosts);
		} catch (AerospikeException e) {
			e.printStackTrace();
		}
		return null;
		
	}
	
}
