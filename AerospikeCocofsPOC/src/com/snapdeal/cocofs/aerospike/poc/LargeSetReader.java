package com.snapdeal.cocofs.aerospike.poc;

import java.util.List;

import com.aerospike.client.AerospikeClient;
import com.aerospike.client.AerospikeException;
import com.aerospike.client.Host;
import com.aerospike.client.Key;
import com.aerospike.client.Value;
import com.aerospike.client.large.LargeSet;
import com.aerospike.client.policy.ClientPolicy;
import com.aerospike.client.policy.ScanPolicy;
import com.aerospike.client.policy.WritePolicy;

public class LargeSetReader implements Runnable{

	private String hostname;
	private int port;
	private boolean bigDataReader;
	private int id;
	
	public LargeSetReader(String hostname, int port, boolean bigDataReader,int id){
		this.hostname = hostname;
		this.port = port;
		this.bigDataReader = bigDataReader;
		this.id = id;
	}
	
	@Override
	public void run() {
		String namespace = "cocofs_ldt";
		String set = "seller_supcs_set";
		String binName = "supcs";
		String hostnames[] = new String[1];
		hostnames[0] = hostname;
		AerospikeClient client = getClient(hostnames, port);
		String[] keys = {"03f528",/*"d0f02e",*/ "eea726", "419d0d" ,"70d54d" ,"3d9c78", "13f26e", "e0565e" , "321228", "a6a809"};
		
		
		try {
			
			long timeTaken = System.currentTimeMillis();
			//String supckey = bigDataReader?keys[0]:keys[(int)(keys.length*Math.random()) % keys.length]; 
			String supckey = bigDataReader?keys[0]:keys[id % keys.length];
			Key key = new Key(namespace, set, supckey);
			System.out.println("Started thread T"+id);
			WritePolicy policy = new WritePolicy();
			policy.timeout = 100000;

			LargeSet s = client.getLargeSet(policy, key, binName, null);
			System.out.println("Fetched Large Set T"+id);
//			List<?> list = s.scan(); 
//			List<?> list2 = s.scan();
			timeTaken = System.currentTimeMillis() - timeTaken;
//			System.out.println("Time taken to read large set(seller:"+supckey+") of size:"+list.size() +" = "+(double)timeTaken/1000.0 +" sec");
//			for(int i=0;i<list.size();i++){
//				System.out.println(list.get(i));
//			}
			while(s != null);// && list2!= null);
		} catch (AerospikeException e) {
			e.printStackTrace();
		}

	}
	
	private AerospikeClient getClient(String[] hostnames, int port){
		
		Host[] hosts = new Host[hostnames.length];
		
		for(int i=0;i<hostnames.length;i++){
			hosts[i] = new Host(hostnames[i],port);
		}
		
		ClientPolicy cp = new ClientPolicy();
		cp.failIfNotConnected = true;
		
		try {
			return new AerospikeClient(cp, hosts);
		} catch (AerospikeException e) {
			e.printStackTrace();
		}
		return null;
		
	}


}
