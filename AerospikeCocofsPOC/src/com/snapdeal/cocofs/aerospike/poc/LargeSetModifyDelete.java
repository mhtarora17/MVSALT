package com.snapdeal.cocofs.aerospike.poc;


import java.util.List;

import com.aerospike.client.AerospikeClient;
import com.aerospike.client.AerospikeException;
import com.aerospike.client.Host;
import com.aerospike.client.Key;
import com.aerospike.client.Value;
import com.aerospike.client.large.LargeSet;
import com.aerospike.client.policy.ClientPolicy;
import com.aerospike.client.policy.WritePolicy;

public class LargeSetModifyDelete implements Runnable{

	//private String hostname = "54.254.241.220";
	private String hostname = "127.0.0.1";
	private int port = 3000;
	private int id;
	
	
	public LargeSetModifyDelete(int id){
		this.id = id;
	}
	
	@Override
	public void run() {
		String namespace = "cocofs_ldt";
		String set = "seller_supcs_set_new";
		String binName = "supcs";
		String hostnames[] = new String[1];
		hostnames[0] = hostname;
		AerospikeClient client = getClient(hostnames, port);
		String[] keys = {"b5cc9a", "69d82a", "6eede0" ,"70d54d" ,"3d9c78", "13f26e", "e0565e" , "321228", "a6a809"};
		
		
		try {
			
			String sellerkey = keys[0];//keys[id % keys.length];
			Key key = new Key(namespace, set, sellerkey);
			System.out.println("Started thread T"+id);
			WritePolicy policy = new WritePolicy();
			policy.timeout = 100000;
			LargeSet s = client.getLargeSet(policy, key, binName, null);
			List<?> list = s.scan();
			long timeTaken = System.currentTimeMillis();
			
			s.destroy();
			//List<?> list = s.scan(); 
			//System.out.println("Fetched Large Set(size:"+list.size()+") T"+id);
			timeTaken = System.currentTimeMillis() - timeTaken;
			
			System.out.println("Time taken to remove one largeset record = "+timeTaken+"ms ");
		} catch (AerospikeException e) {
			e.printStackTrace();
		}
	}
	
	private AerospikeClient getClient(String[] hostnames, int port){
		
		Host[] hosts = new Host[hostnames.length];
		
		for(int i=0;i<hostnames.length;i++){
			hosts[i] = new Host(hostnames[i],port);
		}
		
		ClientPolicy cp = new ClientPolicy();
		cp.failIfNotConnected = true;
		
		try {
			return new AerospikeClient(cp, hosts);
		} catch (AerospikeException e) {
			e.printStackTrace();
		}
		return null;
		
	}
	
	public static void main(String[] args){
		Thread thread = new Thread(new LargeSetModifyDelete(1));
		thread.start();
	}
}
