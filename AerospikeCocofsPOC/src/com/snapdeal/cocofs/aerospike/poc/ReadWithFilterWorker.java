package com.snapdeal.cocofs.aerospike.poc;

import com.aerospike.client.AerospikeClient;
import com.aerospike.client.AerospikeException;
import com.aerospike.client.Key;
import com.aerospike.client.Record;
import com.aerospike.client.Value;
import com.aerospike.client.policy.QueryPolicy;
import com.aerospike.client.query.Filter;
import com.aerospike.client.query.RecordSet;
import com.aerospike.client.query.Statement;

public class ReadWithFilterWorker implements Runnable{

	public  final static int READ_LIMIT=1000000;
	private int id;
	private String[] hostname;
	private int port;
	private String namespace;
	private String set;
	
	public ReadWithFilterWorker(int id, String[] hostname, int port, String namespace, String set){
		this.id = id;
		this.hostname = hostname;
		this.port = port;
		this.set = set;
		this.namespace = namespace;
	}

	@Override
	public void run() {
		try {
			AerospikeClient client = ClientInitiator.getClient(hostname, port);
			
			for(int i=0;i<READ_LIMIT;i++){
				Key key;
				Record record;
				Statement stmt = new Statement();
				stmt.setNamespace(namespace);
				stmt.setSetName(set);
				
				stmt.setFilters(Filter.equal(SELLER_SUPC_SCHEMA.SELLERC.toString(), Value.get(SELLER_SUPC_SCHEMA.SELLERC.toString()+i)));
				RecordSet recordSet = client.query(null, stmt);
				try {
				   while (recordSet != null && recordSet.next()) {
				       key = recordSet.getKey();
				       record = recordSet.getRecord();
				       if(AerospikePOC.DEBUG)
				    	   System.out.println("Record: " + record);
	
				   }
				} finally {
				    recordSet.close();
				}
			}		
			
		} catch (AerospikeException e) {
			e.printStackTrace();
			System.out.println("Not able to create the client...");
		}
		
	}
	


}
