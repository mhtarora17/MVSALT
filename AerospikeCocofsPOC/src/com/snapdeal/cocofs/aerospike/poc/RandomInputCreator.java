package com.snapdeal.cocofs.aerospike.poc;

import com.aerospike.client.AerospikeClient;
import com.aerospike.client.AerospikeException;
import com.aerospike.client.Bin;
import com.aerospike.client.Key;
import com.aerospike.client.policy.Priority;
import com.aerospike.client.policy.WritePolicy;


public class RandomInputCreator {

	private String hostname;
	private String namespace;
	private String set;
	private int port;
	private final int LIMIT = 100000;
	private final int MAXRETRIES = 100;
	
	public RandomInputCreator(String hostname, int port, String namespace, String set){
		this.hostname = hostname;
		this.set=set;
		this.port = port;
		this.namespace = namespace;
	}
	
	public void createAndInsertData(){
		try {
			AerospikeClient client = new AerospikeClient(hostname, port);
			WritePolicy wpolicy = new WritePolicy();
			wpolicy.maxRetries = MAXRETRIES;
			wpolicy.priority = Priority.MEDIUM;
			if(AerospikePOC.DEBUG)
				System.out.println("Inserting test data...");
			for(int i=1;i<LIMIT;i++){
				Key key = new Key(namespace, set, "key"+i);
				Bin bin = new Bin("name","cocofs"+i);
				client.put(wpolicy, key, bin);
			
			}
			if(AerospikePOC.DEBUG)
				System.out.println("Finished inserting test data...");
		} catch (AerospikeException e) {
			e.printStackTrace();
		}
		
		
	}
	
}
