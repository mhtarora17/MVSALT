package com.snapdeal.cocofs.aerospike.poc;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import com.aerospike.client.AerospikeClient;
import com.aerospike.client.AerospikeException;
import com.aerospike.client.Host;
import com.aerospike.client.Key;
import com.aerospike.client.Value;
import com.aerospike.client.large.LargeSet;
import com.aerospike.client.policy.ClientPolicy;
import com.aerospike.client.policy.WritePolicy;

public class LargeSetTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String hostname = "127.0.0.1";
		//String hostname = "54.254.241.220";
		
		int port = 3000;
		String namespace = "cocofs_ldt";
		String set = "seller_supcs_set";
		String binName = "supcs";
		int sellerLimit = 1;
		int randomSize[] = {1000,10000};
		String hostnames[] = new String[1];
		hostnames[0] = hostname;
		AerospikeClient client = getClient(hostnames, port);
		int bigSellers = 1;
		try{
			bigSellers = Integer.parseInt(args[0]);
		}
		catch(Exception e){
			bigSellers = 1;
		}
		int maxSupc = 1000000;
		
		try {
			WritePolicy wp = new WritePolicy();
			
			long timeTaken = System.currentTimeMillis();
			
			for(int i=0;i<sellerLimit;i++){
				String sellerCode = UUID.randomUUID().toString().substring(0,6);
				Key key = new Key(namespace, set, sellerCode);
				LargeSet largeSet = client.getLargeSet(wp, key, binName, null);
				int numSupc = bigSellers > 0? maxSupc:randomSize[i%randomSize.length];
				bigSellers--;
				
				System.out.println("Inserting "+numSupc+" objects for sellerCode= "+sellerCode);
				
				Set<String> supcSet = new HashSet<String>();
				for(int j=0;j<numSupc;j++){
					 String supc=UUID.randomUUID().toString().substring(0,12);
					 supcSet.add(supc);
				//	 if(j%1000 == 0 && bigSellers>0){
				//		 List<Value> supcList = new ArrayList<Value>(supcSet);
						 
				//		 supcSet = new HashSet<Value>();
				//	 }
					 
					 //largeSet.add(Value.get(supc));
				}
				System.out.println("Set size:"+supcSet.size());
				for(String supc:supcSet){
					largeSet.add(Value.get(supc));
				}
//				if(supcSet.size() > 0){
//					List<Value> supcList = new ArrayList<Value>(supcSet);
//					largeSet.add(supcList);
//				}
			}
			
			timeTaken = System.currentTimeMillis() - timeTaken;
			System.out.println("Time taken = "+(double)timeTaken/1000.0 +" sec");
			
		} catch (AerospikeException e) {
			e.printStackTrace();
		}

	}
	
	public static AerospikeClient getClient(String[] hostnames, int port){
		
		Host[] hosts = new Host[hostnames.length];
		
		for(int i=0;i<hostnames.length;i++){
			hosts[i] = new Host(hostnames[i],port);
		}
		
		ClientPolicy cp = new ClientPolicy();
		cp.failIfNotConnected = true;
		
		try {
			return new AerospikeClient(cp, hosts);
		} catch (AerospikeException e) {
			e.printStackTrace();
		}
		return null;
		
	}


}