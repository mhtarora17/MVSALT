package com.snapdeal.cocofs.aerospike.poc;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class LargeSetReadTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String hostname = "54.254.241.220";
		//String hostname = "127.0.0.1";
		int port = 3000;
		int MAX_READERS = 1;
		int BIG_READERS = 1;
		int MAX_CORES = 1;
			
		try{
			MAX_CORES = Integer.parseInt(args[0]);
			MAX_READERS = Integer.parseInt(args[1]);
			BIG_READERS = Integer.parseInt(args[2]);
		}
		catch(Exception ex){
			System.out.println("Not able to to parse argument parameters. Taking defaults.");
		}
		
		ExecutorService threadPool = Executors.newFixedThreadPool(MAX_CORES);
		Thread[] readers = new Thread[MAX_READERS];
		
		for(int i=0;i<MAX_READERS;i++){
			if(i<BIG_READERS){
				readers[i] = new Thread(new LargeSetReader(hostname, port, true,i));
			}
			else{
				readers[i] = new Thread(new LargeSetReader(hostname, port, false,i));
			}
		}
		
		for(int i=0;i<MAX_READERS;i++){
			threadPool.submit(readers[i]);
		}
		
		//while(!threadPool.isTerminated());
		
//		for(int i=0;i<MAX_READERS;i++){
//			try {
//				readers[i].join();
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}
//		}		
	}
}