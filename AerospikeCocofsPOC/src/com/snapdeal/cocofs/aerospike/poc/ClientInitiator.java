package com.snapdeal.cocofs.aerospike.poc;

import com.aerospike.client.AerospikeClient;
import com.aerospike.client.AerospikeException;
import com.aerospike.client.Host;
import com.aerospike.client.policy.ClientPolicy;

public class ClientInitiator {

	public static AerospikeClient getClient(String[] hostnames, int port){
		
		Host[] hosts = new Host[hostnames.length];
		
		for(int i=0;i<hostnames.length;i++){
			hosts[i] = new Host(hostnames[i],port);
		}
		
		ClientPolicy cp = new ClientPolicy();
		cp.failIfNotConnected = false;
		
		try {
			return new AerospikeClient(cp, hosts);
		} catch (AerospikeException e) {
			e.printStackTrace();
		}
		return null;
		
	}
}
