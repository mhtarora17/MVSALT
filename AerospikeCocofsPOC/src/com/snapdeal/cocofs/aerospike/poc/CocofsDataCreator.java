package com.snapdeal.cocofs.aerospike.poc;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.aerospike.client.AerospikeClient;
import com.aerospike.client.AerospikeException;
import com.aerospike.client.Bin;
import com.aerospike.client.Key;
import com.aerospike.client.policy.GenerationPolicy;
import com.aerospike.client.policy.Priority;
import com.aerospike.client.policy.WritePolicy;

public class CocofsDataCreator {

	private String[] hostname;
	private String namespace;
	private String set;
	private int port;
	private static final int LIMIT_SELLER_SUPC = 5000000;
	private static final int LIMIT_SELLER = 50000;
	private static final int LIMIT_SELLER_SUBCAT = 50000;
	private static final int MAXRETRIES = 100;
	private AerospikeClient client;
	private int sellerSupcCount = 0;
	private int sellerCount = 0;
	private int sellerSubcatCount = 0;
	private String sellerSupcSet;
	private String sellerSet;
	private String sellerSubCatSet;
	
	
	public CocofsDataCreator(String[] hostname, int port, String namespace, String sellerSupcSet, String sellerSet, String sellerSubCatSet){
		this.hostname = hostname;
		this.sellerSupcSet = sellerSupcSet;
		this.sellerSet = sellerSet;
		this.sellerSubCatSet = sellerSubCatSet;
		this.port = port;
		this.namespace = namespace;
		this.client = ClientInitiator.getClient(hostname, port);
		;
	}
	
	public void createAndInsertData(){
		insertSellerData();
		insertSellerSubcatData();
		insertSellerSupcData();
	}
	
	private void insertSellerSupcData(){
		WritePolicy wpolicy = new WritePolicy();
		wpolicy.maxRetries = MAXRETRIES;
		wpolicy.priority = Priority.MEDIUM;
		if(AerospikePOC.DEBUG)
			System.out.println("Inserting test data for seller+supc FM mappings...");
		for(int i=1;i<LIMIT_SELLER_SUPC;i++){
			Map<String, String> nextRandomInput = getNextRandomInputSellerSupc();
			String keystr = nextRandomInput.get(SELLER_SUPC_SCHEMA.SELLERC.toString())+nextRandomInput.get(SELLER_SUPC_SCHEMA.SUPC.toString());
			Key key;
			try {
				key = new Key(namespace, sellerSupcSet, keystr);
				Bin[] bins = new Bin[SELLER_SUPC_SCHEMA.values().length];
				createBins(nextRandomInput,bins);
				
				client.put(wpolicy, key, bins);
				
			} catch (AerospikeException e) {
				e.printStackTrace();
			}
			
		
		}
		if(AerospikePOC.DEBUG)
			System.out.println("Finished inserting test data...");
	}

	private void insertSellerSubcatData(){
		WritePolicy wpolicy = new WritePolicy();
		wpolicy.maxRetries = MAXRETRIES;
		wpolicy.priority = Priority.MEDIUM;
		if(AerospikePOC.DEBUG)
			System.out.println("Inserting test data for seller+subcat FM mappings...");
		for(int i=1;i<LIMIT_SELLER_SUBCAT;i++){
			Map<String, String> nextRandomInput = getNextRandomInputSellerSubcat();
			String keystr = nextRandomInput.get(SELLER_SUBCAT_SCHEMA.SELLERC.toString())+nextRandomInput.get(SELLER_SUBCAT_SCHEMA.SUBCAT.toString());
			Key key;
			try {
				key = new Key(namespace, sellerSubCatSet, keystr);

				Bin[] bins = new Bin[SELLER_SUBCAT_SCHEMA.values().length];
				createBins(nextRandomInput,bins);
				client.put(wpolicy, key, bins);
			} catch (AerospikeException e) {
				e.printStackTrace();
			}
			
		
		}
		if(AerospikePOC.DEBUG)
			System.out.println("Finished inserting test data...");
	}
	
	private void insertSellerData(){
		WritePolicy wpolicy = new WritePolicy();
		if(AerospikePOC.DEBUG)
			System.out.println("Inserting test data for seller FM mappings...");
		for(int i=1;i<LIMIT_SELLER;i++){
			Map<String, String> nextRandomInput = getNextRandomInputSeller();
			String keystr = nextRandomInput.get(SELLER_SCHEMA.SELLERC.toString());
			if(AerospikePOC.DEBUG)
				System.out.println("Inserting Key="+keystr);
			Key key;
			try {
				key = new Key(namespace, sellerSet, keystr);

				Bin[] bins = new Bin[SELLER_SCHEMA.values().length];
				
				createBins(nextRandomInput,bins);
				
				client.put(wpolicy, key, bins);
			} catch (AerospikeException e) {
				e.printStackTrace();
			}
			
		
		}
		if(AerospikePOC.DEBUG)
			System.out.println("Finished inserting test data...");
	}

	
	private void createBins(Map<String, String> map, Bin[] bins){
		
		//Bin[] bins = new Bin[map.size()];
		int i=0;
		for(Entry<String,String> entry: map.entrySet()){
			bins[i++] = new Bin(entry.getKey(),entry.getValue());
		}
		
		
		
	}
	
	
	private Map<String, String> getNextRandomInputSellerSupc(){
		Map<String, String> nextRandomInput = new HashMap<String, String>();
		nextRandomInput.put(SELLER_SUPC_SCHEMA.ID.toString(), SELLER_SUPC_SCHEMA.ID.toString()+sellerSupcCount);
		nextRandomInput.put(SELLER_SUPC_SCHEMA.SUPC.toString(), SELLER_SUPC_SCHEMA.SUPC.toString()+sellerSupcCount);
		nextRandomInput.put(SELLER_SUPC_SCHEMA.SELLERC.toString(), SELLER_SUPC_SCHEMA.SELLERC.toString()+sellerSupcCount);
		nextRandomInput.put(SELLER_SUPC_SCHEMA.FM.toString(), SELLER_SUPC_SCHEMA.FM.toString()+sellerSupcCount);
		nextRandomInput.put(SELLER_SUPC_SCHEMA.CREATED.toString(), SELLER_SUPC_SCHEMA.CREATED.toString()+sellerSupcCount);
		nextRandomInput.put(SELLER_SUPC_SCHEMA.ENABLED.toString(), SELLER_SUPC_SCHEMA.ENABLED.toString()+sellerSupcCount);
		nextRandomInput.put(SELLER_SUPC_SCHEMA.UPDATED.toString(), SELLER_SUPC_SCHEMA.UPDATED.toString()+sellerSupcCount);
		nextRandomInput.put(SELLER_SUPC_SCHEMA.UPDATED_BY.toString(), SELLER_SUPC_SCHEMA.UPDATED_BY.toString()+sellerSupcCount);
		nextRandomInput.put(SELLER_SUPC_SCHEMA.LAST_UPDATED.toString(), SELLER_SUPC_SCHEMA.LAST_UPDATED.toString()+sellerSupcCount);
		sellerSupcCount++;
		return nextRandomInput;
	}
	
	private Map<String, String> getNextRandomInputSeller(){
		Map<String, String> nextRandomInput = new HashMap<String, String>();
		nextRandomInput.put(SELLER_SCHEMA.ID.toString(), SELLER_SCHEMA.ID.toString()+sellerCount);
		nextRandomInput.put(SELLER_SCHEMA.SELLERC.toString(), SELLER_SCHEMA.SELLERC.toString()+sellerCount);
		nextRandomInput.put(SELLER_SCHEMA.FM.toString(), SELLER_SCHEMA.FM.toString()+sellerCount);
		nextRandomInput.put(SELLER_SCHEMA.CREATED.toString(), SELLER_SCHEMA.CREATED.toString()+sellerCount);
		nextRandomInput.put(SELLER_SCHEMA.ENABLED.toString(), SELLER_SCHEMA.ENABLED.toString()+sellerCount);
		nextRandomInput.put(SELLER_SCHEMA.UPDATED.toString(), SELLER_SCHEMA.UPDATED.toString()+sellerCount);
		nextRandomInput.put(SELLER_SCHEMA.UPDATED_BY.toString(), SELLER_SCHEMA.UPDATED_BY.toString()+sellerCount);
		nextRandomInput.put(SELLER_SCHEMA.LAST_UPDATED.toString(), SELLER_SCHEMA.LAST_UPDATED.toString()+sellerCount);
		sellerCount++;
		return nextRandomInput;
	}
	
	private Map<String, String> getNextRandomInputSellerSubcat(){
		Map<String, String> nextRandomInput = new HashMap<String, String>();
		nextRandomInput.put(SELLER_SUBCAT_SCHEMA.ID.toString(), SELLER_SUBCAT_SCHEMA.ID.toString()+sellerSubcatCount);
		nextRandomInput.put(SELLER_SUBCAT_SCHEMA.SELLERC.toString(), SELLER_SUBCAT_SCHEMA.SELLERC.toString()+sellerSubcatCount);
		nextRandomInput.put(SELLER_SUBCAT_SCHEMA.SUBCAT.toString(), SELLER_SUBCAT_SCHEMA.SUBCAT.toString()+sellerSubcatCount);
		nextRandomInput.put(SELLER_SUBCAT_SCHEMA.FM.toString(), SELLER_SUBCAT_SCHEMA.FM.toString()+sellerSubcatCount);
		nextRandomInput.put(SELLER_SUBCAT_SCHEMA.CREATED.toString(), SELLER_SUBCAT_SCHEMA.CREATED.toString()+sellerSubcatCount);
		nextRandomInput.put(SELLER_SUBCAT_SCHEMA.ENABLED.toString(), SELLER_SUBCAT_SCHEMA.ENABLED.toString()+sellerSubcatCount);
		nextRandomInput.put(SELLER_SUBCAT_SCHEMA.UPDATED.toString(), SELLER_SUBCAT_SCHEMA.UPDATED.toString()+sellerSubcatCount);
		nextRandomInput.put(SELLER_SUBCAT_SCHEMA.UPDATED_BY.toString(), SELLER_SUBCAT_SCHEMA.UPDATED_BY.toString()+sellerSubcatCount);
		nextRandomInput.put(SELLER_SUBCAT_SCHEMA.LAST_UPDATED.toString(), SELLER_SUBCAT_SCHEMA.LAST_UPDATED.toString()+sellerSubcatCount);
		sellerSubcatCount++;
		return nextRandomInput;
	}
}
