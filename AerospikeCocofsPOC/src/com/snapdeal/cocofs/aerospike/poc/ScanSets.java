package com.snapdeal.cocofs.aerospike.poc;

import com.aerospike.client.AerospikeClient;
import com.aerospike.client.AerospikeException;
import com.aerospike.client.Key;
import com.aerospike.client.Record;
import com.aerospike.client.ScanCallback;
import com.aerospike.client.policy.Priority;
import com.aerospike.client.policy.ScanPolicy;

public class ScanSets implements ScanCallback {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		 try {
	            new ScanSets().runTest();
	        }
	        catch (Exception e) {
	            e.printStackTrace();
	        }
	}

	private int recordCount;

    public void runTest() throws AerospikeException {   
        AerospikeClient client = new AerospikeClient("54.254.241.220", 3000);
//while(true)
        try {
        	
            ScanPolicy policy = new ScanPolicy();
            policy.concurrentNodes = true;
            policy.priority = Priority.LOW;
            policy.includeBinData = false;

            client.scanAll(policy, "cocofs_ldt", "seller_supcs_set", this);       
            System.out.println("Records " + recordCount);
        }
	catch(Exception e){}
        finally {
            client.close();
        }
    }

	
	@Override
	public void scanCallback(Key paramKey, Record paramRecord)
			throws AerospikeException {
		recordCount++;
        if ((recordCount % 10000) == 0) {
            System.out.println("Records " + recordCount);
        }
		
	}

}
