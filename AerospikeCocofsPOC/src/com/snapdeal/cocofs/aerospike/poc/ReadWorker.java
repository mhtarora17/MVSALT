package com.snapdeal.cocofs.aerospike.poc;

import com.aerospike.client.AerospikeClient;
import com.aerospike.client.AerospikeException;
import com.aerospike.client.Key;
import com.aerospike.client.Record;
import com.aerospike.client.policy.Policy;
import com.aerospike.client.policy.Priority;

public class ReadWorker implements Runnable{

	private final int READ_LIMIT=1000000;
	private final int MAXRETRIES=100;
	private long timeTaken=0;
	private int id;
	private String[] hostname;
	private int port;
	private String namespace;
	private String set;
	
	public ReadWorker(int id, String[] hostname, int port, String namespace, String set){
		this.id = id;
		this.hostname = hostname;
		this.port = port;
		this.set = set;
		this.namespace = namespace;
	}

	@Override
	public void run() {
		try {
			AerospikeClient client = ClientInitiator.getClient(hostname, port);
			Policy policy = new Policy();
			policy.maxRetries = MAXRETRIES;
			policy.priority = Priority.MEDIUM;
			timeTaken = System.currentTimeMillis();
			for(int i=1;i<READ_LIMIT;i++){
				String key = SELLER_SUPC_SCHEMA.SELLERC.toString()+i+SELLER_SUPC_SCHEMA.SUPC.toString()+i;
				if(AerospikePOC.DEBUG)
					System.out.println("Reader "+id+" reading KEY="+key);
				Record record = client.get(policy,new Key(namespace, set, key));
				if(AerospikePOC.DEBUG)
					System.out.println("Record received = "+record);
			}
			timeTaken = System.currentTimeMillis() - timeTaken;
			
		} catch (AerospikeException e) {
			e.printStackTrace();
			System.out.println("Not able to create the client...");
		}
		
	}
	
	
	public long getTimeTaken() {
		return timeTaken;
	}

}
