package com.snapdeal.cocofs.aerospike.poc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.aerospike.client.AerospikeClient;
import com.aerospike.client.AerospikeException;
import com.aerospike.client.query.IndexType;
import com.aerospike.client.task.IndexTask;


public class AerospikePOC {
	private  static String HOSTNAME1="127.0.0.1";
	private  static String HOSTNAME2="127.0.0.1";
	private static String HOSTNAME[];
	private final static int PORT=3000;
	private final static String NAMESPACE="Cocofs_Poc";
	private final static String SET="test1";
	private final static String SET_SELLER_SUPC = "seller_supc_fm_mapping";
	private final static String SET_SELLER = "seller_fm_mapping";
	private final static String SET_SELLER_SUBCAT = "seller_subcat_fm_mapping";
	private final static int MAX_READERS = 100;
	public  static boolean DEBUG = false;
	private  static int MAX_THREADS = 10;
	private  static int MAX_RTHREADS = 10;
	private  static int MAX_WTHREADS = 10;
	private static int MAX_RQTHREADS = 10;
	
	public static void main(String[] str) throws InterruptedException{
	
		if(str.length==0 || str[0].equalsIgnoreCase("help")||str[0].equalsIgnoreCase("h"))
			System.out.println("parameters required - \"hostname1\" \"hostname2\" \"readers\" \"writers\" \"readersWithQuery\" \"debug\"");

		HOSTNAME1 = str[0];
		HOSTNAME2 = str[1];
		MAX_RTHREADS = Integer.parseInt(str[2]);
		MAX_WTHREADS = Integer.parseInt(str[3]);
		MAX_RQTHREADS = Integer.parseInt(str[4]);
		DEBUG = str[5].equals("0")?false:true;
		
		HOSTNAME = new String[2];
		HOSTNAME[0] = HOSTNAME1;
		HOSTNAME[1] = HOSTNAME2;
		
		System.out.println("1.Insert test data\n2. Read/Write 90-10\n3.Effect of Secondary indices");;
		System.out.println("Enter test number:");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int test = 0;
		try {
			test = Integer.parseInt(br.readLine().trim());
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		switch(test){
		case 1:
			insertTestData();
			break;
		case 2:
			testConcurrentReadsWrites();
			break;
		case 3:
			testSecondaryIndicesEffects();
			break;
		default:
			System.out.println("Test not found..");
			break;
		}
	}
	
	private static void insertTestData(){
		if(DEBUG){
			System.out.println("******************************Setting up test data now****************************");
		}
		
		CocofsDataCreator dataCreator = new CocofsDataCreator(HOSTNAME, PORT, NAMESPACE, SET_SELLER_SUPC,SET_SELLER,SET_SELLER_SUBCAT);
		dataCreator.createAndInsertData();
		
		if(DEBUG){
			System.out.println("******************************Test data setup completed****************************");
		}
		
		
	}
	
	private static void testConcurrentReads(){
		if(DEBUG){
			System.out.println("******************************Starting concurrent reads(100%) test now****************************");
		}
		
		Thread[] readers = new Thread[MAX_READERS];
		if(DEBUG)
			System.out.println("Spawning multiple reader threads...");
		for(int i=0;i<MAX_READERS;i++){
			readers[i] = new Thread(new ReadWorker(i, HOSTNAME, PORT, NAMESPACE, SET_SELLER_SUPC));
			readers[i].start();
		}
		if(DEBUG)
			System.out.println("Spawned reader threads.");
		long timeTaken = System.currentTimeMillis();
		//wait for completion
		for(int i=0;i<MAX_READERS;i++){
			try {
				readers[i].join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		timeTaken = System.currentTimeMillis() - timeTaken;
		if(DEBUG)
			System.out.println("Total time taken = "+timeTaken);
		
		if(DEBUG){
			System.out.println("******************************COncurrent reads(100%) test completed****************************");
		}	
	}
	
	private static void testConcurrentReadsWrites(){
		if(DEBUG){
			System.out.println("******************************Starting concurrent reads/writes(90-10) test now****************************");
		}
		int readerThreadCount = (int)(MAX_THREADS * 0.9);
		int writerThreadCount = MAX_THREADS - readerThreadCount;
		
		readerThreadCount = MAX_RTHREADS;
		writerThreadCount = MAX_WTHREADS;
		
		
		Thread[] readers = new Thread[readerThreadCount];
		Thread[] writers = new Thread[writerThreadCount];
		if(DEBUG)
			System.out.println("Spawning multiple reader threads...");
		for(int i=0;i<readerThreadCount;i++){
			readers[i] = new Thread(new ReadWorker(i, HOSTNAME, PORT, NAMESPACE, SET_SELLER_SUPC));
			readers[i].start();
		}

		if(DEBUG)
			System.out.println("Spawning multiple writer threads...");
		for(int i=0;i<writerThreadCount;i++){
			writers[i] = new Thread(new WriteWorker(i, HOSTNAME, PORT, NAMESPACE, SET_SELLER_SUPC));
			writers[i].start();
		}
		
		
		if(DEBUG)
			System.out.println("Spawned reader threads.");
		long timeTaken = System.currentTimeMillis();
		//wait for completion
		for(int i=0;i<readerThreadCount;i++){
			try {
				readers[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		for(int i=0;i<writerThreadCount;i++){
			try {
				writers[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		timeTaken = System.currentTimeMillis() - timeTaken;
		if(DEBUG)
			System.out.println("Total time taken = "+timeTaken);
		
		if(DEBUG){
			System.out.println("******************************COncurrent reads/writes(90-10) test completed****************************");
		}	
	}
	
	private static void testSecondaryIndicesEffects(){
		
		//create a secondary index
		long time = System.currentTimeMillis();
		IndexTask task;
		try {
			AerospikeClient client = ClientInitiator.getClient(HOSTNAME, PORT);
			task = client.createIndex(null, NAMESPACE, SET_SELLER_SUPC, 
				    "idx_seller_code", "SELLERC", IndexType.STRING);
			task.waitTillComplete();
			time = System.currentTimeMillis() - time;
			System.out.println("Time taken to create secondary index on seller code = "+((double)time/1000.0)+"sec");
			
			Thread[] readers = new Thread[MAX_RQTHREADS];
			if(DEBUG)
				System.out.println("Spawning multiple query reader threads...");
			for(int i=0;i<MAX_RQTHREADS;i++){
				readers[i] = new Thread(new ReadWithFilterWorker(i, HOSTNAME, PORT, NAMESPACE, SET_SELLER_SUPC));
				readers[i].start();
			}
			if(DEBUG)
				System.out.println("Spawned query reader threads.");
			//wait for completion
			long start = System.currentTimeMillis();
			for(int i=0;i<MAX_RQTHREADS;i++){
				try {
					readers[i].join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			start = System.currentTimeMillis() - start;
			System.out.println("Total time taken by "+ReadWithFilterWorker.READ_LIMIT*MAX_RQTHREADS+" Reads = "+start+"ms");
			
			
		} catch (AerospikeException e) {
			if(DEBUG)
				System.out.println("Unable to create index...");
		}

		
	}
	
}
