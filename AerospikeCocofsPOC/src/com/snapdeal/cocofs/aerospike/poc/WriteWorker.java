package com.snapdeal.cocofs.aerospike.poc;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.aerospike.client.AerospikeClient;
import com.aerospike.client.AerospikeException;
import com.aerospike.client.Bin;
import com.aerospike.client.Key;
import com.aerospike.client.policy.Priority;
import com.aerospike.client.policy.WritePolicy;

public class WriteWorker implements Runnable{


	private final int WRITE_LIMIT=100000;
	private final int MAXRETRIES=100;
	private long timeTaken=0;
	private int id;
	private String[] hostname;
	private int port;
	private String namespace;
	private String set;
	
	public WriteWorker(int id, String[] hostname, int port, String namespace, String set){
		this.id = id;
		this.hostname = hostname;
		this.port = port;
		this.set = set;
		this.namespace = namespace;
	}
	
	@Override
	public void run() {
		AerospikeClient client = ClientInitiator.getClient(hostname, port);
		WritePolicy policy = new WritePolicy();
		policy.maxRetries = MAXRETRIES;
		policy.priority = Priority.MEDIUM;
		timeTaken = System.currentTimeMillis();
		for(int i=1;i<WRITE_LIMIT;i++){
			Map<String, String> nextRandomInput = getNextRandomInputSellerSupc(i);
			String keystr = nextRandomInput.get(SELLER_SUPC_SCHEMA.SELLERC.toString())+nextRandomInput.get(SELLER_SUPC_SCHEMA.SUPC.toString());
			Key key;
			try {
				key = new Key(namespace, set, keystr);
				Bin[] bins = new Bin[SELLER_SUPC_SCHEMA.values().length];
				createBins(nextRandomInput,bins);
				
				client.put(policy, key, bins);
				
			} catch (AerospikeException e) {
				e.printStackTrace();
			}
		}
		timeTaken = System.currentTimeMillis() - timeTaken;
	}
	
	private Map<String, String> getNextRandomInputSellerSupc(int sellerSupcCount){
		Map<String, String> nextRandomInput = new HashMap<String, String>();
		nextRandomInput.put(SELLER_SUPC_SCHEMA.ID.toString(), SELLER_SUPC_SCHEMA.ID.toString()+sellerSupcCount);
		nextRandomInput.put(SELLER_SUPC_SCHEMA.SUPC.toString(), SELLER_SUPC_SCHEMA.SUPC.toString()+sellerSupcCount);
		nextRandomInput.put(SELLER_SUPC_SCHEMA.SELLERC.toString(), SELLER_SUPC_SCHEMA.SELLERC.toString()+sellerSupcCount);
		nextRandomInput.put(SELLER_SUPC_SCHEMA.FM.toString(), SELLER_SUPC_SCHEMA.FM.toString()+sellerSupcCount);
		nextRandomInput.put(SELLER_SUPC_SCHEMA.CREATED.toString(), SELLER_SUPC_SCHEMA.CREATED.toString()+sellerSupcCount);
		nextRandomInput.put(SELLER_SUPC_SCHEMA.ENABLED.toString(), SELLER_SUPC_SCHEMA.ENABLED.toString()+sellerSupcCount);
		nextRandomInput.put(SELLER_SUPC_SCHEMA.UPDATED.toString(), SELLER_SUPC_SCHEMA.UPDATED.toString()+sellerSupcCount);
		nextRandomInput.put(SELLER_SUPC_SCHEMA.UPDATED_BY.toString(), SELLER_SUPC_SCHEMA.UPDATED_BY.toString()+sellerSupcCount);
		nextRandomInput.put(SELLER_SUPC_SCHEMA.LAST_UPDATED.toString(), SELLER_SUPC_SCHEMA.LAST_UPDATED.toString()+sellerSupcCount);
		return nextRandomInput;
	}
	
	private void createBins(Map<String, String> map, Bin[] bins){
		
		//Bin[] bins = new Bin[map.size()];
		int i=0;
		for(Entry<String,String> entry: map.entrySet()){
			bins[i++] = new Bin(entry.getKey(),entry.getValue());
		}
		
		
		
	}
	

}
