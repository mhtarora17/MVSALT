
1. Need to set following envioronment variable to enable PACKMAN services on tier2 tomcats -- cocofs-tier2-as-1a and cocofs-tier2-as-1b.
 JAVA_OPTS="$JAVA_OPTS -Dspring.profiles.active=PACKMAN,COCOFS"

2. Need new HA-proxy for Packman. 
2.1 Add following hosts behind the HA Proxy.
cocofs-tier2-as-1a and cocofs-tier2-as-1b
2.2 Configure HA-proxy to only allow http requests with pattern -  /service/packman/
Following configs (needs to be validated and costomized by DevOps).

haproxy.cfg
....
frontend packman_allow
        bind 127.0.0.1:6060
        acl allow_packman path_reg ^/service/packman/?.*$
        use_backend packman_server if allow_packman   =>use backend server only if url has pattern "*/service/packman/*".

backend packman_server
        server packmanserver 127.0.0.1:5050

2.3 cname for packman services (external facing, to allow hits from UC)

2.4 HTTPS needs to be enabled on packman services cname/HA-proxy.

3. For mongo upgrade from 2.4.8 to 3.0.5, follow instructions as:
https://techspace.snapdeal.in:8443/display/EN/Mongo+Upgrade+Activity



