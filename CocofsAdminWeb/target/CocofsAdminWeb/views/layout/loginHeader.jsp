<%@ include file="/tld_includes.jsp"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<style>
.headerNavCont a:HOVER {
	font-weight: bold;
	text-decoration: none;
}

.myaccount-drop {
	font-size: 13px;
	font-weight: normal;
	cursor: pointer;
	padding-top: 10px;
}

.myaccount-list-outer {
	margin-top: 21px;
	right: 0;
	width: 170px;
}

.sd-droplist-outer {
	background: none repeat scroll 0 0;
	border: 1px solid #CCCCCC;
	border-radius: 0 0 7px 7px;
	box-shadow: 0 10px 15px #939393;
	display: none;
	margin: -1px 0 0;
	padding: 10px 0;
	position: absolute;
	width: 200px;
	z-index: 500;
}

.myaccount-drop-outer {
	float: right;
}
</style>
<script type="text/javascript">
function showAccountDropDown(){
	$(".sd-droplist-outer").show();
}
function hideAccountDropDown(){
	$(".sd-droplist-outer").hide();
}
</script>
<div class="header">
	<div class="logo">
		<a href="${path.http}/login" title=""></a>
	</div>
	
	<div>
			<sec:authorize ifAnyGranted="registered">
			<div class="myaccount-drop-outer" style="float: right;">
				<div class="myaccount-drop" onmouseover="showAccountDropDown();" onmouseout="hideAccountDropDown();">My Account</div>
				<div class="sd-droplist-outer myaccount-list-outer"
					style="display: none;">
					<c:set value="${user.email}" var="userEmail"></c:set>
					<ul class="sd-droplist myaccount-list">
						<li><a href="${path.http}/logout">Sign Out</a>
						</li>
					</ul>
				</div>
			</div>
		</sec:authorize>
	</div>
</div>


