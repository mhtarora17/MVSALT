<%@ include file="/tld_includes.jsp"%>


<div id="subheader" class="subheader"
	style="background-image: url(${path.resources('img/background/bg_blue.png')});">
	<sec:authentication property="principal" var="user" />

	<div class="tabbable">
		<!-- Only required for left/right tabs -->
		<div class="subheaderTab">
			<sec:authorize ifAnyGranted="admin, tech, taxrateeditor">
				<div id="sidebar-fn-upload"
					onclick="javascript:window.location.href='/admin/taxRate/taxRateUpload'">Upload
					Tax Rate</div>
			</sec:authorize>
			<sec:authorize ifAnyGranted="admin, tech, taxclasseditor">
				<div id="sidebar-class-upload"
					onclick="javascript:window.location.href='/admin/taxClass/taxClassUpload'">Upload
					Tax Class</div>
			</sec:authorize>
		</div>
	</div>
	<div class="subheaderToggle">
		<div class="subheaderToggleContent">&lt</div>
	</div>
</div>