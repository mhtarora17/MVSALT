<%@ include file="/tld_includes.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<tiles:insertTemplate template="/views/layout/base.jsp">
	<tiles:putAttribute name="title" value="My Jobs" />

	<tiles:putAttribute name="systemMessage">
		<div class="system-message"></div>
	</tiles:putAttribute>

	<tiles:putAttribute name="extraScripts">
    <link href="${path.css('chosen/chosen.css')}" rel="stylesheet"
    type="text/css" />
    <link rel="stylesheet" type="text/css"
            href="${path.css('snapdeal/ui.jqgrid.css')}" />

    <script type="text/javascript"
    src="${path.js('chosen/chosen.jquery.min.js')}"></script>
    
    <script type="text/javascript"
    src="${path.js('jquery/jquery.ui.datepicker.min.js')}"></script>
    
        <script type="text/javascript"
            src="${path.js('jquery/grid.locale-en.js')}"></script>
        <script type="text/javascript"
            src="${path.js('jquery/jquery.jqGrid.min.js')}"></script>


	</tiles:putAttribute>

	<tiles:putAttribute name="systemMessage">
		<div class="system-message">
			<input type="hidden" value="${message}" id="messageHiddenField"/>
		</div>
	</tiles:putAttribute>
	<div class="current-user">
			<input type="hidden" value="${currentUser}" id="currentUser"/>
	</div>

	<tiles:putAttribute name="subheader">
		<tiles:insertTemplate template="/views/admin/admintasks/jobStatus-subheader.jsp">
		</tiles:insertTemplate>
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
		<div class="main-content" style="margin-left:-200px;">
			<div class="container" style="width: 90%;">
			<div>
					<form id="status-form" class="form-horizontal" method="get" action='status'>
					    <div class="control-group">
                        <label class="control-label" for="selected-job-status"> Job Status </label>
                        <div class="controls">
						<select name="selected-job-status" id="selected-job-status" class="chosen-select" onchange="initializeGrid()">
							<c:forEach
								items="${jobStatusList}"
								var="jobStatus">
                                <c:choose>
                                <c:when test="${(selectedStatusCode ne null) && (jobStatus.code eq selectedStatusCode ) }">
                                <option value="${jobStatus.code}" selected="selected">${jobStatus.description}</option>
                                </c:when>
                                <c:otherwise>
                                <option value="${jobStatus.code}">${jobStatus.description}</option>
                                </c:otherwise>
                                </c:choose>
							</c:forEach>
						</select>
                        </div>
                        </div>
                        <div class="control-group">
                        <label class="control-label" for="frm-startdate">Start Date</label>
                        <div class="controls">
                            <input type="text" id="frm-startdate" />
                        </div>
                        </div>
                        <div class="control-group">
                        <label class="control-label" for="frm-enddate">End Date</label>
                        <div class="controls">
                            <input type="text" id="frm-enddate" />
                        </div>
                        </div>
                        
                        <div class="control-group" id="org-fetch">
                        <div class="controls">
                            <button id="fetch-selected-job-status" type="button" class="btn btn-primary">Fetch</button>
                    
                        </div>
                        </div>
                          <div class="control-group" style="display: none;" id="val-fetch">
                        <div class="controls">
                            <button id="fetch-validated-job-status" type="button" class="btn btn-primary">Fetch</button>
                    
                        </div>
                        </div>
                           <div class="control-group" style="display: none;" id="held-fetch">
                        <div class="controls">
                            <button id="fetch-held-job-status" type="button" class="btn btn-primary">Fetch</button>
                    
                        </div>
                        </div>
                        
						</form>
				</div>
                <div id="job-results-div" style="display:block;">
                <table id="job-results-grid">
                </table>
                <div id="job-results-grid-pager"></div>
                </div>
   			<div id="job-results-div-validated" style="display:none;">
                <table id="job-results-grid-validated">
                </table>
                <div id="job-results-grid-pager-validated"></div>
                </div>
                <div id="job-results-div-held" style="display:none;">
                <table id="job-results-grid-held">
                </table>
                <div id="job-results-grid-pager-held"></div>
                </div>
            </div>
		</div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
	<%-- Activate relevant sidebar function   --%>
	<%-- Activate relevant subheader function --%>
		<script type="text/javascript">
			function hideSidebar() {
				$(".subheader").css({
					"left" : "-11%"
				});
				$(".functionAnchor").css({
					"display" : "none"
				});
				$(".sidebar").css({
					"left" : "1%",
					"width" : "99%"
				});
				$(".main-content").css({
					"left" : "1%",
					"width" : "98%"
				});
			}
			function showSidebar() {
				$(".subheader").css({
					"left" : "0%"
				});
				$(".functionAnchor").css({
					"display" : "block"
				});
				$(".sidebar").css({
					"left" : "12%",
					"width" : "88%"
				});
				$(".main-content").css({
					"left" : "12%",
					"width" : "87%"
				});
			}

			$(document).ready(function() {
				var subheader = true;
				$(".subheaderToggle").click(function() {
					if (subheader) {
						hideSidebar();
						$(".subheaderToggleContent").html(">");
						subheader = false;
					} else {
						showSidebar();
						$(".subheaderToggleContent").html("<");
						subheader = true;
					}

				});
				/* check sidebar jsp for relevant div ids
				 */
				$('#header-nav-myjob').addClass('active');
				$('.chosen-select').chosen();
				$('#frm-startdate').datepicker({
                    arrows: false,
                    presets:{specificDate: 'Pick a date'},
                    presetRanges:[],
                    showOn: "button",
              buttonImage: "/static/cocofs/img/calendar.jpg",
              buttonImageOnly: false,
              dateFormat:'yy-mm-dd'
                  });
				$('#frm-enddate').datepicker({
                    arrows: false,
                    presets:{specificDate: 'Pick a date'},
                    presetRanges:[],
                    showOn: "button",
              buttonImage: "/static/cocofs/img/calendar.jpg",
              buttonImageOnly: false,
              dateFormat:'yy-mm-dd'
                  });
			
				/*encodeURIComponent*/
				
			$("#job-results-grid").jqGrid({
			        url: "getMyJobInfoByStatus",
			        datatype: "json",
			        mtype: "GET",
			        colNames: ["JobCode", "FileName", "Status", "Action", "Created", "Updated"],
			        colModel: [
			            { name: "jobCode", width: 150 },
			            { name: "fileName", width: 300 },
			            { name: "status", width: 100 },
			            { name: "action", width: 150 },
			            { name: "created", width: 180 },
			            { name: "updated", width: 180 }
			        ],
			        pager: "#job-results-grid-pager",
			        rowNum: 10,
			        rowList: [5, 10, 50, 100, 200, 500, 1000, 5000],
			        viewrecords: true,
			        gridview: true,
			        autoencode: true,
			        caption: "Job Details"
			    }); 
				$("#job-results-grid").jqGrid('navGrid','#job-results-grid-pager',
	                    {edit:false,add:false,del:false,search:false,refresh:false},
	                    {},{},{},
	                    {}
	                    ); 
				 
				 $('#fetch-selected-job-status').click(function(){
					  $("#job-results-grid").jqGrid('clearGridData');
					    var start = $('#frm-startdate').val().trim();
					    var end = $('#frm-enddate').val().trim();
					    var user=$('#currentUser').val().trim();
					    if ( start.length == 0 ) {
	                        alert("Start date not specified ");
	                        return false;
	                    }
					    if ( end.length == 0 ) {
                            alert("End date not specified ");
                            return false;
                        }
	                    if ( end < start ) {
	                        alert("End date before Start date ");
	                        return false;
	                    }
	                    if (  (6 * 86400000) < ($('#frm-enddate').datepicker('getDate') - $('#frm-startdate').datepicker('getDate')) ) {
	                    	alert("Date range should not be more than a week ");
                            return false;
	                    }
	                    
	                    var status = $('select[name="selected-job-status"]').val();
	                    if ( status.length == 0 ) {
	                    	alert("Please select status to fetch first ");
	                    	return false;
	                    }
	                    var searchUrl = "getMyJobInfoByStatus?status=";
	                    searchUrl = searchUrl+status+"&start=";
	                    searchUrl = searchUrl+encodeURIComponent(start);
	                    searchUrl = searchUrl+"&end=" +encodeURIComponent(end);
	                    searchUrl = searchUrl+"&user="+user;
	                    var title = 'JobDetails ' + start + ' to ' + end + ' status ' + status;
	                    $("#job-results-grid").jqGrid('setGridParam',{ url:searchUrl,datatype: 'json'}).trigger('reloadGrid');
	                    $('#job-results-grid').setCaption(title);
					 });
				 
				 
				 
				 
				 
				 
				 
				 
				$("#fetch-validated-job-status").click(function(){
                    //Validated Grid
                    
                	grid = $('#job-results-grid-validated'), firstButtonColumnIndex=0, buttonNames={}; 
                    firstButtonColumnIndex = getColumnIndexByName(grid,'cancel');
		            buttonNames[firstButtonColumnIndex] = 'cancel';
		            buttonNames[firstButtonColumnIndex+1] = 'hold';
		            getColumnIndexByName = function(grid,columnName) {
	                    var cm = grid.jqGrid('getGridParam','colModel');
	                    for (var i=0,l=cm.length; i<l; i++) {
	                        if (cm[i].name===columnName) {
	                            return i; // return the index
	                        }
	                    }
	                    return -1;
	                }
		            $("#job-results-grid-validated").jqGrid('clearGridData');
					    var start = $('#frm-startdate').val().trim();
					    var end = $('#frm-enddate').val().trim();
					    var user=$('#currentUser').val().trim();
					    if ( start.length == 0 ) {
	                        alert("Start date not specified ");
	                        return false;
	                    }
					    if ( end.length == 0 ) {
                          alert("End date not specified ");
                          return false;
                      }
	                    if ( end < start ) {
	                        alert("End date before Start date ");
	                        return false;
	                    }
	                    if (  (6 * 86400000) < ($('#frm-enddate').datepicker('getDate') - $('#frm-startdate').datepicker('getDate')) ) {
	                    	alert("Date range should not be more than a week ");
                          return false;
	                    }
	                    
	                    var status = $('select[name="selected-job-status"]').val();
	                    if ( status.length == 0 ) {
	                    	alert("Please select status to fetch first ");
	                    	return false;
	                    }
	                    var grid=$("#job-results-grid-validated");
	                    var searchUrl = "getMyJobInfoByStatus?status=";
	                    searchUrl = searchUrl+status+"&start=";
	                    searchUrl = searchUrl+encodeURIComponent(start);
	                    searchUrl = searchUrl+"&end=" +encodeURIComponent(end);
	                    searchUrl = searchUrl+"&user="+user;
	                    var title = 'ValidatedJobDetails ' + start + ' to ' + end + ' status ' + status;
	                    $("#job-results-grid-validated").jqGrid('setGridParam',{ url:searchUrl,datatype: 'json', beforeSelectRow: function (rowid, e) {
		                    var iCol = $.jgrid.getCellIndex(e.target);
		                    if (iCol >= firstButtonColumnIndex) {
		                    	var reqId = $("#job-results-grid-validated").jqGrid('getRowData', rowid);
		                    	reqId=reqId.jobCode;
		                        //alert("rowid="+reqId +"\nButton name: "+buttonNames[iCol]);
		                		operationCall(grid,reqId,buttonNames[iCol]);
		                    }

		                    // prevent row selection if one click on the button
		                    return (iCol >= firstButtonColumnIndex)? false: true;
		                }}).trigger('reloadGrid');
	                    $('#job-results-grid-validated').setCaption(title);
				
				 
				});			 
	
				
				
				//Held Jobs Grid
						$("#fetch-held-job-status").click(function(){
                   	grid = $('#job-results-grid-held'), firstButtonColumnIndex=0, buttonNames={}; 
                    firstButtonColumnIndex = getColumnIndexByName(grid,'unhold');
		            buttonNames[firstButtonColumnIndex] = 'unhold';
		            getColumnIndexByName = function(grid,columnName) {
	                    var cm = grid.jqGrid('getGridParam','colModel');
	                    for (var i=0,l=cm.length; i<l; i++) {
	                        if (cm[i].name===columnName) {
	                            return i; // return the index
	                        }
	                    }
	                    return -1;
	                }
		            $("#job-results-grid-held").jqGrid('clearGridData');
					    var start = $('#frm-startdate').val().trim();
					    var end = $('#frm-enddate').val().trim();
					    var user=$('#currentUser').val().trim();
					    if ( start.length == 0 ) {
	                        alert("Start date not specified ");
	                        return false;
	                    }
					    if ( end.length == 0 ) {
                          alert("End date not specified ");
                          return false;
                      }
	                    if ( end < start ) {
	                        alert("End date before Start date ");
	                        return false;
	                    }
	                    if (  (6 * 86400000) < ($('#frm-enddate').datepicker('getDate') - $('#frm-startdate').datepicker('getDate')) ) {
	                    	alert("Date range should not be more than a week ");
                          return false;
	                    }
	                    
	                    var status = $('select[name="selected-job-status"]').val();
	                    if ( status.length == 0 ) {
	                    	alert("Please select status to fetch first ");
	                    	return false;
	                    }
	                    var searchUrl = "getMyJobInfoByStatus?status=";
	                    var grid=$("#job-results-grid-held");
	                    searchUrl = searchUrl+status+"&start=";
	                    searchUrl = searchUrl+encodeURIComponent(start);
	                    searchUrl = searchUrl+"&end=" +encodeURIComponent(end);
	                    searchUrl = searchUrl+"&user="+user;
	                    var title = 'ValidatedJobDetails ' + start + ' to ' + end + ' status ' + status;
	                    $("#job-results-grid-held").jqGrid('setGridParam',{ url:searchUrl,datatype: 'json', beforeSelectRow: function (rowid, e) {
		                    var iCol = $.jgrid.getCellIndex(e.target);
		                    if (iCol >= firstButtonColumnIndex) {
		                    	var reqId = $("#job-results-grid-held").jqGrid('getRowData', rowid);
		                    	reqId=reqId.jobCode;
		                        //alert("rowid="+reqId +"\nButton name: "+buttonNames[iCol]);
		                		operationCall(grid,reqId,buttonNames[iCol]);
		                    }

		                    // prevent row selection if one click on the button
		                    return (iCol >= firstButtonColumnIndex)? false: true;
		                }}).trigger('reloadGrid');
	                    $('#job-results-grid-validated').setCaption(title);
				});		
				
				
				
				/* check subheader jsp for relevant div ids
				 */
				/* $('#subheader-fn-usrmanagement').addClass('active') */
				//functions required for actions in table
	                getColumnIndexByName = function(grid,columnName) {
	                    var cm = grid.jqGrid('getGridParam','colModel');
	                    for (var i=0,l=cm.length; i<l; i++) {
	                        if (cm[i].name===columnName) {
	                            return i; // return the index
	                        }
	                    }
	                    return -1;
	                }
	                
			});
			function operationCall(grid,id,button)
			{
				//alert(id+":"+button);
				var answer = confirm ("Are you sure you want to "+button+" the job with id "+id+" ?");
if (answer)
{ 

				$.ajax({
					   url: 'updateStatus',
					   method:'GET',
					   data: {
					      id: id,
					      button:button
					   },
					   error: function() {
					     alert('Status not updated.');
					   },
					   success: function(data) {
						     alert('Status updated successfully.');
						     grid.trigger("reloadGrid");
					   },
					   type: 'GET'
					});	}
			}
			function initializeGrid(){
				var status=$('#selected-job-status').val().trim();
				if(status== 'VALD')
					{
					$("#org-fetch").hide();
					$("#held-fetch").hide();
					$("#val-fetch").show();					
					$("#job-results-div").hide();
					$("#job-results-div-held").hide();
					$("#job-results-div-validated").show();
					$("#job-results-grid-validated").jqGrid({
				        url: "getMyJobInfoByStatus",
				        datatype: "json",
				        mtype: "GET",
				        colNames: ["JobCode", "FileName", "Status", "Action", "Created", "Updated", "",""],
				        colModel: [
				            { name: "jobCode", width: 150 },
				            { name: "fileName", width: 300 },
				            { name: "status", width: 100 },
				            { name: "action", width: 150 },
				            { name: "created", width: 180 },
				            { name: "updated", width: 180 },
				            { name: 'cancel', width: 150, sortable: false, search: false,
			                      formatter:function(){
			                          return "Cancel<span class='ui-icon ui-icon-cancel'></span>"
			                      }},
			                      { name: 'hold', width: 180, sortable: false, search: false,
			                          formatter:function(){
			                              return "On Hold<span class='ui-icon ui-icon-alert'></span>"
			                          }}
			                      ],
				        pager: "#job-results-grid-pager-validated",
				        rowNum: 10,
				        rowList: [5, 10, 50, 100, 200, 500, 1000, 5000],
				        viewrecords: true,
				        gridview: true,
				        autoencode: true,
				        caption: "Job Details Validated"
		            });  				    
					$("#job-results-grid-validated").jqGrid('navGrid','#job-results-grid-pager-validated',
		                    {edit:false,add:false,del:false,search:false,refresh:false},
		                    {},{},{},
		                    {}
		                    );
					}
				else
					if(status=='HLD')
						{
						$("#org-fetch").hide();			
						$("#val-fetch").hide();
						$("#held-fetch").show();
						$("#job-results-div").hide();
						$("#job-results-div-validated").hide();
						$("#job-results-div-held").show();
						$("#job-results-grid-held").jqGrid({
					        url: "getMyJobInfoByStatus",
					        datatype: "json",
					        mtype: "GET",
					        colNames: ["JobCode", "FileName", "Status", "Action", "Created", "Updated", ""],
					        colModel: [
					            { name: "jobCode", width: 150 },
					            { name: "fileName", width: 300 },
					            { name: "status", width: 100 },
					            { name: "action", width: 150 },
					            { name: "created", width: 180 },
					            { name: "updated", width: 180 },
					            { name: 'unhold', width: 150, sortable: false, search: false,
				                      formatter:function(){
				                          return "Unhold<span class='ui-icon ui-icon-circle-check'></span>"
				                      }}				                     
				                      ],
					        pager: "#job-results-grid-pager-held",
					        rowNum: 10,
					        rowList: [5, 10, 50, 100, 200, 500, 1000, 5000],
					        viewrecords: true,
					        gridview: true,
					        autoencode: true,
					        caption: "On Hold Job Details"
			            });  				    
						$("#job-results-grid-held").jqGrid('navGrid','#job-results-grid-pager-held',
			                    {edit:false,add:false,del:false,search:false,refresh:false},
			                    {},{},{},
			                    {}
			                    );
						}
					else
					{
					$("#org-fetch").show();
					$("#held-fetch").hide();
					$("#val-fetch").hide();
					$("#job-results-div").show();
					$("#job-results-div-validated").hide();
					$("#job-results-div-held").hide();
					$("#job-results-grid").jqGrid({
				        url: "getMyJobInfoByStatus",
				        datatype: "json",
				        mtype: "GET",
				        colNames: ["JobCode", "FileName", "Status", "Action", "Created", "Updated"],
				        colModel: [
				            { name: "jobCode", width: 150 },
				            { name: "fileName", width: 300 },
				            { name: "status", width: 100 },
				            { name: "action", width: 150 },
				            { name: "created", width: 180 },
				            { name: "updated", width: 180 }
				        ],
				        pager: "#job-results-grid-pager",
				        rowNum: 10,
				        rowList: [5, 10, 50, 100, 200, 500, 1000, 5000],
				        viewrecords: true,
				        gridview: true,
				        autoencode: true,
				        caption: "Job Details"
				    }); 
					$("#job-results-grid").jqGrid('navGrid','#job-results-grid-pager',
		                    {edit:false,add:false,del:false,search:false,refresh:false},
		                    {},{},{},
		                    {}
		                    ); 
					}
				
			}
			$('#frm-startdate').change(function(){
				if($('#frm-startdate').val()!="")
					{
						d = $('#frm-startdate').datepicker('getDate');
						d.setDate(d.getDate()+6);
					
						$( "#frm-enddate" ).data().datepicker.settings.maxDate= d;
					}
			});
			
		</script>
			
		
	</tiles:putAttribute>
</tiles:insertTemplate>