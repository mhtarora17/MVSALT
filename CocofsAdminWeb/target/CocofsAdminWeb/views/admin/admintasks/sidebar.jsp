<%@ include file="/tld_includes.jsp"%>


<div id="subheader" class="subheader"
	style="background-image: url(${path.resources('img/background/bg_blue.png')});">
	<div class="tabbable">
		<!-- Only required for left/right tabs -->
		<div class="subheaderTab">
			<sec:authorize ifAnyGranted="admin,tech">
				<div id="sidebar-fn-jobview"
					onclick="javascript:window.location.href='/admin/jobs/status'">
					View Jobs</div>
			</sec:authorize>
			<sec:authorize ifAnyGranted="tech">
				<div id="sidebar-fn-cacheview"
					onclick="javascript:window.location.href='/admin/cache/cacheview'">
					Cache Control</div>
			</sec:authorize>

			<sec:authorize ifAnyGranted="tech">
				<div id="sidebar-fn-taskdetail"
					onclick="javascript:window.location.href='/admin/task/taskDetail'">
					Task Detail</div>
			</sec:authorize>
			<sec:authorize ifAnyGranted="tech">
				<div id="sidebar-fn-eventdetail"
					onclick="javascript:window.location.href='/admin/events/eventDetail'">
					Event Detail</div>
			</sec:authorize>
			<sec:authorize ifAnyGranted="tech">
				<div id="sidebar-fn-emailedit"
					onclick="javascript:window.location.href='/admin/template/email'">
					View/Edit Email Template</div>
			</sec:authorize>
			<sec:authorize ifAnyGranted="tech">
				<div id="sidebar-fn-shippingmode"
					onclick="javascript:window.location.href='/admin/shippingmode/view'">
					View Shipping Modes</div>
			</sec:authorize>
			<sec:authorize ifAnyGranted="tech">
				<div id="sidebar-fn-propertyview"
					onclick="javascript:window.location.href='/admin/property/view'">
					Cocofs Properties</div>
			</sec:authorize>
			<sec:authorize ifAnyGranted="tech">
				<div id="sidebar-fn-bloomfilter" onclick="javascript:window.location.href='/admin/bloomFilterConsole/'">
					Bloom Filter Console</div>
			</sec:authorize>

		</div>
	</div>
	<div class="subheaderToggle">
		<div class="subheaderToggleContent">&lt</div>
	</div>
</div>