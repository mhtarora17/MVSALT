<%@ include file="/tld_includes.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<tiles:insertTemplate template="/views/layout/base.jsp">
	<tiles:putAttribute name="title" value="Task Admin" />

	<tiles:putAttribute name="systemMessage">
		<div class="system-message"></div>
	</tiles:putAttribute>

	<tiles:putAttribute name="extraScripts">
		<script type="text/javascript">
		
		function doAction(taskName,action){
			if(!confirm("Do you really wanna "+action+" "+taskName )){
				return;
			}
			$.ajax({
			      type: "POST",
			      url: "/json/admin/task/"+action+"/"+taskName,
			      dataType: 'json',
			      contentType:'application/json',
			      async: false,
			      success: function(resp){
			      },
			      error: function(xhr, statusCode , error){
			    	  if(xhr.status==200){
			    		  alert("Your session has expired. Please log in again in another tab/window and then retry here.");
			    	  }
			      }
			  });
		}
		function hideSidebar(){
            $(".subheader").css({"left":"-11%"});
            $(".functionAnchor").css({"display": "none"});
            $(".sidebar").css({"left":"1%","width":"99%"});
            $(".main-content").css({"left":"1%","width":"98%"});
        }
        function showSidebar(){
            $(".subheader").css({"left":"0%"});
            $(".functionAnchor").css({"display": "block"});
            $(".sidebar").css({"left":"12%","width":"88%"});
            $(".main-content").css({"left":"12%","width":"87%"});
        }
        
		$(document).ready(function() {
			var subheader=true;
            $(".subheaderToggle").click(function(){
                    if(subheader){
                            hideSidebar();
                            $(".subheaderToggleContent").html(">");
                            subheader=false;
                    }else{
                            showSidebar();
                            $(".subheaderToggleContent").html("<");
                            subheader=true;
                    }

            });
           /* check sidebar jsp for relevant div ids
           */
           $('#sidebar-fn-taskdetail').addClass('active');
           $('#header-nav-admintasks').addClass('active');
			
		});
			
		</script>

	</tiles:putAttribute>
	<tiles:putAttribute name="subheader">
		<tiles:putAttribute name="systemMessage">
		</tiles:putAttribute>
		<tiles:insertTemplate template="/views/admin/admintasks/sidebar.jsp">
			<tiles:putAttribute name="active" value="taskAdmin" />
		</tiles:insertTemplate>
	</tiles:putAttribute>

	<tiles:putAttribute name="sidebar">
		<tiles:insertTemplate template="/views/admin/admintasks/task-subheader.jsp">
			<tiles:putAttribute name="active" value="taskInfoUpdate" />
		</tiles:insertTemplate>
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
		<div class="main-content lfloat">
			<div class="container" style="width: 100%;">
			<div style="text-align: center;"><input type="button" class="btn" id="resumeAllTasks" value="RESUME SCHEDULER" onclick='doAction("","resumeAllTask");'/>&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;<input type="button" class="btn" id="pauseAllTasks" value="PAUSE SCHEDULER" onclick='doAction("","pauseAllTask");'/>
				<br/><br/><br/></div>
				
				<table id="taskInfoTable" class="table table-hover table-condensed table-bordered ">
					<tr id="taskInfoHeaRow text-center">
						<th>Task Name</th>
						<th>Task Impl Class</th>
						<th>Previous Execution Time</th>
						<th>Next Execution Time</th>
						<th>Current Status</th>
						<th>Action</th>
						<th>Host Name</th>
					</tr>
					
						<c:forEach items="${scheduledTaskInfo}" var="taskInfo">
						<tr class="taskInfoBodyRow">
							<td>${taskInfo.name}</td>
							<td>${taskInfo.implClass }</td>
							<td>${taskInfo.lastExecutionTime}</td>
							<td>${taskInfo.nextExecutionTime}</td>
							<td>${taskInfo.currentStatus}</td>
							<td>
							<c:choose>
								<c:when test="${taskInfo.currentStatus eq 'Waiting'}">
								<input type="button" value="Start" onclick="doAction('${taskInfo.name}','startTask')"/>
								</c:when>
								<c:when test="${taskInfo.currentStatus eq 'Running'}">
								<input type="button" value="Stop" onclick="doAction('${taskInfo.name}','stopTask')"/>
								</c:when>
							</c:choose>
							</td>
							<td>${taskInfo.hostName}</td>
						</tr>
						</c:forEach>
						
				</table>
			</div>
			
		</div>
	</tiles:putAttribute>
</tiles:insertTemplate>