<%@ include file="/tld_includes.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<tiles:insertTemplate template="/views/layout/base.jsp">
	<tiles:putAttribute name="title" value="Jobs" />

	<tiles:putAttribute name="systemMessage">
		<div class="system-message"></div>
	</tiles:putAttribute>

	<tiles:putAttribute name="extraScripts">
    <link href="${path.css('chosen/chosen.css')}" rel="stylesheet"
    type="text/css" />
    <link rel="stylesheet" type="text/css"
            href="${path.css('snapdeal/ui.jqgrid.css')}" />

    <script type="text/javascript"
    src="${path.js('chosen/chosen.jquery.min.js')}"></script>
    
    <script type="text/javascript"
    src="${path.js('jquery/jquery.ui.datepicker.min.js')}"></script>
    
        <script type="text/javascript"
            src="${path.js('jquery/grid.locale-en.js')}"></script>
        <script type="text/javascript"
            src="${path.js('jquery/jquery.jqGrid.min.js')}"></script>


	</tiles:putAttribute>
	<tiles:putAttribute name="sidebar">
		<tiles:insertTemplate template="/views/admin/admintasks/sidebar.jsp">
		</tiles:insertTemplate>
	</tiles:putAttribute>
	<tiles:putAttribute name="systemMessage">
		<div class="system-message">
			<input type="hidden" value="${message }" id="messageHiddenField"/>
		</div>
	</tiles:putAttribute>


	<tiles:putAttribute name="subheader">
		<tiles:insertTemplate template="/views/admin/admintasks/jobStatus-subheader.jsp">
		</tiles:insertTemplate>
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
		<div class="main-content lfloat">
			<div class="container" style="width: 90%;">
			<div>
					<form id="status-form" class="form-horizontal" method="get" action='status'>
					    <div class="control-group">
                        <label class="control-label" for="selected-job-status"> Job Status </label>
                        <div class="controls">
						<select name="selected-job-status" id="selected-job-status" class="chosen-select" onchange="hideColumn()">
							<c:forEach
								items="${jobStatusList}"
								var="jobStatus">
                                <c:choose>
                                <c:when test="${(selectedStatusCode ne null) && (jobStatus.code eq selectedStatusCode ) }">
                                <option value="${jobStatus.code}" selected="selected">${jobStatus.description}</option>
                                </c:when>
                                <c:otherwise>
                                <option value="${jobStatus.code}">${jobStatus.description}</option>
                                </c:otherwise>
                                </c:choose>
							</c:forEach>
						</select>
                        </div>
                        </div>
                        <div class="control-group">
                        <label class="control-label" for="frm-startdate">Start Date</label>
                        <div class="controls">
                            <input type="text" id="frm-startdate" />
                        </div>
                        </div>
                        <div class="control-group">
                        <label class="control-label" for="frm-enddate">End Date</label>
                        <div class="controls">
                            <input type="text" id="frm-enddate" />
                        </div>
                        </div>
                        
                        <div class="control-group">
                        <div class="controls">
                            <button id="fetch-selected-job-status" type="button" class="btn btn-primary">Fetch</button>
                    
                        </div>
                        </div>
                        
						</form>
				</div>
                <div id="job-results-div">
                <table id="job-results-grid">
                </table>
                <div id="job-results-grid-pager"></div>
                </div>

            </div>
		</div>		
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
	<%-- Activate relevant sidebar function   --%>
	<%-- Activate relevant subheader function --%>
		<script type="text/javascript">
			function hideSidebar() {
				$(".subheader").css({
					"left" : "-11%"
				});
				$(".functionAnchor").css({
					"display" : "none"
				});
				$(".sidebar").css({
					"left" : "1%",
					"width" : "99%"
				});
				$(".main-content").css({
					"left" : "1%",
					"width" : "98%"
				});
			}
			function showSidebar() {
				$(".subheader").css({
					"left" : "0%"
				});
				$(".functionAnchor").css({
					"display" : "block"
				});
				$(".sidebar").css({
					"left" : "12%",
					"width" : "88%"
				});
				$(".main-content").css({
					"left" : "12%",
					"width" : "87%"
				});
			}
			
			function showActionButtons(cellValue, options, rowObject) {
				be = "<input style='height:22px;width:20px;' type='button' title='Edit' value='E' onclick=editGridData('"
						+ options.rowId + "');>";
				se = "<input style='height:22px;width:20px;' type='button' title='Save' value='S' onclick=saveGridData('"
						+ options.rowId + "');>";
				ce = "<input style='height:22px;width:20px;' type='button' title='Cancel' value='C' onclick=restoreGridData('"
						+ options.rowId + "');>";
				return be + se + ce;

			}
			function validateRowData(inputData) {
				if (inputData.priority == '') {
					alert("Required field is empty");
					return false;
				}
				if (inputData.priority > 100 || inputData.priority < 0) {
					alert("Please select the priority between 0 to 100");
					return false;
				}
				return true;
			}
			var lastsel2;
			function editGridData(rowId) {
				if (rowId && rowId !== lastsel2) {
					jQuery('#job-results-grid').jqGrid('restoreRow', lastsel2);
					jQuery('#job-results-grid').jqGrid('editRow', rowId, true);
					lastsel2 = rowId;
				}

			}

			function restoreGridData(rowId) {
				var row = $("#job-results-grid").jqGrid('getRowData', rowId);
				jQuery('#job-results-grid').restoreRow(rowId);
				lastsel2 = 0000000;
			}
			
			function hideColumn(){
				var status=$('#selected-job-status').val().trim();
				if(status== 'VALD'){
					$("#job-results-grid").jqGrid('showCol', 'priority');
					$("#job-results-grid").jqGrid('showCol', 'operation');
				}
				else{
					$("#job-results-grid").jqGrid('hideCol', 'priority');
					$("#job-results-grid").jqGrid('hideCol', 'operation');
				}
			}

			function saveGridData(rowId) {
				$("#job-results-grid").jqGrid('saveRow', rowId);
				var inputData = new Object();
				var row = $("#job-results-grid").jqGrid('getRowData', rowId);
				var jobDetailDTO = new Object();
				if (!validateRowData(row)) {
					$('#job-results-grid').jqGrid('editRow', rowId, true, null,
							null, 'clientArray');
					return false;
				}
				jobDetailDTO.jobCode = row.jobCode;
			
				jobDetailDTO.priority = row.priority;
				/* jobDetailDTO.updated = row.updated; */
				inputData.jobDetailDTO = jobDetailDTO;
				console.debug(inputData);
				delete row.action;
				$
						.ajax({
							type : "POST",
							url : "${path.http}/json/admin/jobs/updateJobDetail",
							dataType : 'json',
							contentType : 'application/json',
							data : JSON.stringify(inputData),
							async : false,
							success : function(resp) {
								if (resp.successful) {
/* 									var searchUrl = '${path.http}/admin/jobs/getJobInfoByStatus';
 */									 var searchUrl = "${path.http}/admin/jobs/getJobInfoByStatus?status=";
					                    searchUrl = searchUrl+$('select[name="selected-job-status"]').val()+"&start=";
					                    searchUrl = searchUrl+encodeURIComponent($("#frm-startdate").val());
					                    searchUrl = searchUrl+"&end=" +encodeURIComponent($("#frm-enddate").val());
									$("#job-results-grid").jqGrid('setGridParam', {
										url : searchUrl,
										datatype : 'json',
									}).trigger('reloadGrid');
								} else {
									alert(resp.response);

								}
								$("#job-results-grid").jqGrid('setGridParam', {
									url : searchUrl,
									datatype : 'json'
								}).trigger('reloadGrid');
								lastsel2 = 0000000;
							},
							error : function(xhr, statusCode, error) {
								if (xhr.status == 200) {
									alert("Your session has expired. Please log in again in another tab/window and then retry here.");
									$('#job-results-grid').jqGrid('editRow', rowId,
											true, null, null, 'clientArray');
								}
							}
						});
			}

			
			
			
			
			$(document).ready(function() {
								var subheader = true;
				
				$(".subheaderToggle").click(function() {
					if (subheader) {
						hideSidebar();
						$(".subheaderToggleContent").html(">");
						subheader = false;
					} else {
						showSidebar();
						$(".subheaderToggleContent").html("<");
						subheader = true;
					}

				});
				/* check sidebar jsp for relevant div ids
				 */
				$('#sidebar-fn-jobview').addClass('active');
				$('#header-nav-admintasks').addClass('active');
				$('.chosen-select').chosen();
				$('#frm-startdate').datepicker({
                    arrows: false,
                    presets:{specificDate: 'Pick a date'},
                    presetRanges:[],
                    showOn: "button",
              buttonImage: "/static/cocofs/img/calendar.jpg",
              buttonImageOnly: false,
              dateFormat:'yy-mm-dd'
                  });
				$('#frm-enddate').datepicker({
                    arrows: false,
                    presets:{specificDate: 'Pick a date'},
                    presetRanges:[],
                    showOn: "button",
              buttonImage: "/static/cocofs/img/calendar.jpg",
              buttonImageOnly: false,
              dateFormat:'yy-mm-dd'
                  });
				/*encodeURIComponent*/
				$("#job-results-grid").jqGrid({
				
					url : '',
					datatype : 'local',
					editurl : 'clientArray',
					mtype : 'POST',
					colNames : ["JobCode", "FileName", "Status", "Action", "Created", "Updated","Priority","Operation"],
					colModel : [ {
						name : 'jobCode',
						index : 'jobCode',
						width : 100,
						editable : false,
						sorttype : 'text'

					}, {
						name : 'fileName',
						index : 'fileName',
						align : 'center',
						width : 100,
						editable : false,
						sorttype : 'text'
					}, {
						name : 'status',
						index : 'status',
						align : 'center',
						width : 100,
						editable : false
					}, {
						name : 'action',
						index : 'action',
						width : 150,
						align : 'center',
						editable : false,
						search : false,
						sorttype : 'text'
					}, {
						name : 'created',
						index : 'created',
						width : 100,
						editable : false,
						align : 'center',
						sorttype : 'text',
						search : false
					}, {
						name : 'updated',
						index : 'updated',
						width : 100,
						editable : false,
						align : 'center',
						sorttype : 'text',
						search : false
					}, {
						name : 'priority',
						index : 'priority',
						width : 100,
						align : 'center',
						editable : true,
						edittype : "text",
						editrules : {
							number : true,
							required : true,
							minValue : 0,
							maxValue : 100
						},
						search : false,
						sortable : false
					},{
						name : 'operation',
						index : 'operation',
						width : 90,
						editable : false,
						formatter : showActionButtons,
						search : false
					} ],
					height : 'auto',
					autowidth : true,
					viewrecords : true,
					caption : "Job Detail",
					gridview : true,
					loadui : 'block',
					loadonce : true,
					pager : '#job-results-grid-pager',
					rowNum : 20,
					sortorder : "asc",
					shrinkToFit : true,
					rowList : [ 20, 40, 60, 100 ],
					ignoreCase : true,
					jsonReader : {
						root : "rows",
						page : "page",
						total : "total",
						records : "records",
						repeatitems : false,
						cell : "cell",
						id : "jobCode"
					},

				});

		$("#job-results-grid").jqGrid('navGrid',
				'#job-results-grid-pager', {
					edit : false,
					add : false,
					del : false,
					search : false,
					refresh : false
				}, {}, {}, {}, {});

		var searchUrl = '${path.http}/admin/jobs/getJobInfoByStatus';
		$("#job-results-grid").jqGrid('setGridParam', {
			url : searchUrl,
			datatype : 'json'
		}).trigger('reloadGrid');

				 $('#fetch-selected-job-status').click(function(){
					  $("#job-results-grid").jqGrid('clearGridData');
					    var start = $('#frm-startdate').val().trim();
					    var end = $('#frm-enddate').val().trim();
					    if ( start.length == 0 ) {
	                        alert("Start date not specified ");
	                        return false;
	                    }
					    if ( end.length == 0 ) {
                            alert("End date not specified ");
                            return false;
                        }
	                    if ( end < start ) {
	                        alert("End date before Start date ");
	                        return false;
	                    }
	                    if (  (6 * 86400000) < ($('#frm-enddate').datepicker('getDate') - $('#frm-startdate').datepicker('getDate')) ) {
	                    	alert("Date range should not be more than a week ");
                            return false;
	                    }
	                    
	                    var status = $('select[name="selected-job-status"]').val();
	                    if ( status.length == 0 ) {
	                    	alert("Please select status to fetch first ");
	                    	return false;
	                    }
	                    var searchUrl = "getJobInfoByStatus?status=";
	                    searchUrl = searchUrl+status+"&start=";
	                    searchUrl = searchUrl+encodeURIComponent(start);
	                    searchUrl = searchUrl+"&end=" +encodeURIComponent(end);
	                    var title = 'JobDetails ' + start + ' to ' + end + ' status ' + status;
	                    $("#job-results-grid").jqGrid('setGridParam',{ url:searchUrl,datatype: 'json'}).trigger('reloadGrid');
	                    $('#job-results-grid').setCaption(title);
				 });
				 

				/* check subheader jsp for relevant div ids
				 */
				/* $('#subheader-fn-usrmanagement').addClass('active') */

			});

			$('#frm-startdate').change(function(){
				if($('#frm-startdate').val()!="")
					{
						d = $('#frm-startdate').datepicker('getDate');
						d.setDate(d.getDate()+6);
					
						$( "#frm-enddate" ).data().datepicker.settings.maxDate= d;
					}
			});
			
		</script>
			
		
	</tiles:putAttribute>
</tiles:insertTemplate>