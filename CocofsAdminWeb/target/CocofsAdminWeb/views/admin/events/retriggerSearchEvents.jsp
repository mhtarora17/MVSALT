<%@ include file="/tld_includes.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<tiles:insertTemplate template="/views/layout/base.jsp">
	<tiles:putAttribute name="title" value="Retrigger search events" />

	<tiles:putAttribute name="systemMessage">
		<div class="system-message"></div>
	</tiles:putAttribute>

	<tiles:putAttribute name="extraScripts">
	<script type="text/javascript" src="${path.js('mousetrap/mousetrap.min.js')}"></script>
		<script type="text/javascript">
		
		$(document).ready(function() {
			if ($('#messageHiddenField').val() != ''){
                smoke.signal($('#messageHiddenField').val(),2000);
             }

			
			$('#sidebar-fn-usrmgmt').addClass('active');
	        $('#header-nav-usermanagementview').addClass('active');
			
		 });
		

		function retriggerSearchNotifications(){
			  document.getElementById('fullRunMessage').innerHTML  = '';

			var hours = document.getElementById("hours").value.trim();
			if(hours!=undefined && hours!='' ){
				var ajaxUrl =  "${path.http}/admin/retriggerSearchEvents/retriggerSearchNotifications?hours="+hours;
				  $ .ajax({url : ajaxUrl,type : "GET",dataType : 'json',success : function(data) {
					var output ='';
					if(data.message != null){
							  output += "message: " + data.message + "  ,";
							  
						  }
						if(data.sellerUpdateCount != null){
						  output += "Total seller updates: " +data.sellerUpdateCount + "  ,";
						  
					  }
					  if(data.sellerSupcUpdateCount != null){
						  output += "Total seller-supc updates: " +data.sellerSupcUpdateCount ;
						  
					  }
					  document.getElementById('fullRunMessage').innerHTML  = output;
				  }
			
			  });
			}else{
				smoke.alert("Please enter a valid input of number of hours");
			}
		}
		
		
			
		
			
			function getSearchNotificationNumbers(){
				  document.getElementById('dryRunMessage').innerHTML  = '';

				var dryRunHours = document.getElementById("dryRunHours").value.trim();
				if(dryRunHours!=undefined && dryRunHours!='' ){
					var ajaxUrl =  "${path.http}/admin/retriggerSearchEvents/getSearchNotificationNumbers?hours="+dryRunHours;
					  $ .ajax({url : ajaxUrl,type : "GET",dataType : 'json',success : function(data) {
						var output ='';
						if(data.message != null){
								  output += "message: " + data.message + "  ,";
								  
							  }
							if(data.sellerUpdateCount != null){
							  output += "Total seller updates: " +data.sellerUpdateCount + "  ,";
							  
						  }
						  if(data.sellerSupcUpdateCount != null){
							  output += "Total seller-supc updates: " +data.sellerSupcUpdateCount ;
							  
						  }
						  document.getElementById('dryRunMessage').innerHTML  = output;
					  }
				
				  });
				}else{
					smoke.alert("Please enter a valid input of number of hours");
				}
			}
		</script>

	</tiles:putAttribute>
	<tiles:putAttribute name="systemMessage">
	</tiles:putAttribute>
	<tiles:putAttribute name="subheader">
		<tiles:insertTemplate template="/views/admin/events/sidebar.jsp">
			<tiles:putAttribute name="active" value="retrigger-search-events" />
		</tiles:insertTemplate>
	</tiles:putAttribute>

	<tiles:putAttribute name="sidebar">
		<tiles:insertTemplate template="/views/admin/events/retrigger-subheader.jsp">
			<tiles:putAttribute name="active" value="retriggerSerachEvents" />
		</tiles:insertTemplate>
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
	<div class="main-content lfloat">
	<input type="hidden" value="${message}" id="messageHiddenField"/>

			<div class="container" style="width: 100%;">
				<div id="fetchEventNumber" class="control-group">
					<label class="control-label"><b>Fetch total seller-supc search
						push required(dry run)</b></label>
					<div class="controls">
						<input id="dryRunHours" type="text" name="dryRunHours" value=""
							tabindex="2" placeholder="enter time in hours">
					</div>
					<div class="control-group">
						<div class="controls">
							<input type="button" class="btn btn-primary" value="Fetch"
								onclick="getSearchNotificationNumbers();">

						</div>
					</div>
					<div class="control-group">
						<div class="controls">
							<div id="dryRunMessage"></div>


						</div>
					</div>
				</div>
				
				
				<div id="pushEvents" style="margin-top:100px" class="control-group">
					<label class="control-label"><b>Push  seller-supc search required</b></label>
					<div class="controls">
						<input id="hours" type="text" name="hours" value=""
							tabindex="2" placeholder="enter time in hours">
					</div>
					<div class="control-group">
						<div class="controls">
							<input type="button" class="btn btn-primary" value="Submit"
								onclick="retriggerSearchNotifications();">

						</div>
					</div>
					<div class="control-group">
						<div class="controls">
							<div id="fullRunMessage"></div>


						</div>
					</div>
				</div>

			</div>


		</div>
	</tiles:putAttribute>
</tiles:insertTemplate>