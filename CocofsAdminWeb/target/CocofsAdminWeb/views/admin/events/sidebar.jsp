<%@ include file="/tld_includes.jsp"%>

<c:set var="active">
	<tiles:getAsString name="active" />
</c:set>

<div id="subheader" class="subheader" style="background-image: url(${path.resources('img/background/bg_blue.png')});">
	<div class="tabbable">
		<!-- Only required for left/right tabs -->
		<div class="subheaderTab">
            <sec:authorize ifAnyGranted="admin,tech">
			
				<div id="sidebar-fn-retiggersearchevents" onclick="javascript:window.location.href='/admin/retriggerSearchEvents/'">
			
			Retrigger search events
			</div>
            </sec:authorize>
			
		</div>
	</div>
	<div class="subheaderToggle">
		<div class="subheaderToggleContent">&lt</div>
	</div>
</div>