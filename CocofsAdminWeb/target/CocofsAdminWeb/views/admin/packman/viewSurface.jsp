<%@ include file="/tld_includes.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<tiles:insertTemplate template="/views/layout/base.jsp">
	<tiles:putAttribute name="title" value="Surface Admin" />

	<tiles:putAttribute name="systemMessage">
		<div class="system-message"></div>
	</tiles:putAttribute>

	<tiles:putAttribute name="extraScripts">
		<link rel="stylesheet" type="text/css"
			href="${path.css('snapdeal/ui.jqgrid.css')}" />
		<script type="text/javascript"
			src="${path.js('jquery/grid.locale-en.js')}"></script>
		<script type="text/javascript"
			src="${path.js('jquery/jquery.jqGrid.min.js')}"></script>
		<script type="text/javascript">
			var grid = $('#surfaceDetail');
			function validateRowData(inputData) {
				if (inputData.surfaceName == '' ||inputData.description =='') {
					alert("One of the required field is empty");
					return false;
				}
				if(inputData.description.length >2048){
					alert("maximum allowed size of desciption is 2048 chars. input length is " + inputData.description.length);
					return false;
				}
				return true;
			}

			var lastsel2;
			function editGridData(rowId) {
				if (rowId && rowId !== lastsel2) {
					jQuery('#surfaceDetail').jqGrid('restoreRow', lastsel2);
					jQuery('#surfaceDetail').jqGrid('editRow', rowId, true);
					lastsel2 = rowId;
				}

			}

			function restoreGridData(rowId) {
				var row = $("#surfaceDetail").jqGrid('getRowData', rowId);
				jQuery('#surfaceDetail').restoreRow(rowId);
				lastsel2 = 0000000;
			}

			function saveGridData(rowId) {
				$("#surfaceDetail").jqGrid('saveRow', rowId);
				var inputData = new Object();
				var row = $("#surfaceDetail").jqGrid('getRowData', rowId);
				var surfaceDetailDTO = new Object();
				if(!validateRowData(row)){
					$('#surfaceDetail').jqGrid('editRow', rowId, true, null, null, 'clientArray');
					return false;
				}
				surfaceDetailDTO.id = row.id;
				surfaceDetailDTO.type = row.type;
				surfaceDetailDTO.description = row.description;
				inputData.surfaceDetailDTO = surfaceDetailDTO;
				delete row.action;
				$
						.ajax({
							type : "POST",
							url : "${path.http}/json/admin/packman/updateSurfaceDetail",
							dataType : 'json',
							contentType : 'application/json',
							data : JSON.stringify(inputData),
							async : false,
							success : function(resp) {
								if (resp.successful) {
									var searchUrl = '${path.http}/admin/packman/getSurfaceInfo';
									$("#surfaceDetail").jqGrid('setGridParam', {
										url : searchUrl,
										datatype : 'json'
									}).trigger('reloadGrid');
								} else {
									alert(resp.response);

								}
								$("#surfaceDetail").jqGrid('setGridParam', {
									url : searchUrl,
									datatype : 'json'
								}).trigger('reloadGrid');
								lastsel2 = 0000000;
							},
							error : function(xhr, statusCode, error) {
								if (xhr.status == 200) {
									alert("Your session has expired. Please log in again in another tab/window and then retry here.");
									$('#surfaceDetail').jqGrid('editRow', rowId,
											true, null, null, 'clientArray');
								}
							}
						});
			}

			function deleteGridData(rowId) {
				var r = confirm("Do you really want to delete it?");
				if (r == false) {
				  return;
				}
				
				var row = $("#surfaceDetail").jqGrid('getRowData', rowId);
				delete row.action;
				$
						.ajax({
							type : "POST",
							url : "${path.http}/json/admin/packman/deleteSurface",
							data : row.type,
							dataType : 'json',
							contentType : 'application/json',
							async : false,
							success : function(resp) {
								if (resp.successful) {
									var searchUrl = '${path.http}/admin/packman/getSurfaceInfo';
									$("#surfaceDetail").jqGrid('setGridParam', {
										url : searchUrl,
										datatype : 'json'
									}).trigger('reloadGrid');
								} else {
									alert(resp.response);

								}
								$("#surfaceDetail").jqGrid('setGridParam', {
									url : searchUrl,
									datatype : 'json'
								}).trigger('reloadGrid');
								lastsel2 = 0000000;
							},
							error : function(xhr, statusCode, error) {
								if (xhr.status == 200) {
									alert("Your session has expired. Please log in again in another tab/window and then retry here.");
									$('#surfaceDetail').jqGrid('editRow', rowId,
											true, null, null, 'clientArray');
								}
							}
						});

			}
			function showActionButtons(cellValue, options, rowObject) {
				be = "<input style='height:22px;width:20px;' type='button' title='Edit' value='E' onclick=editGridData('"
						+ options.rowId + "');>";
				se = "<input style='height:22px;width:20px;' type='button' title='Save' value='S' onclick=saveGridData('"
						+ options.rowId + "');>";
				ce = "<input style='height:22px;width:20px;' type='button' title='Cancel' value='C' onclick=restoreGridData('"
						+ options.rowId + "');>";
				de = "<input style='height:22px;width:20px;' type='button' title='Delete' value='D' onclick=deleteGridData('"
						+ options.rowId + "');>";
				return be + se + ce + de;

			}

			$(document)
					.ready(
							function() {

								$('#sidebar-fn-surfaceview').addClass('active');
								$('#header-nav-packman').addClass('active');

								$("#surfaceDetail").jqGrid(
										{
											url : '',
											datatype : 'local',
											editurl : 'clientArray',
											mtype : 'POST',
											colNames : [ 'S.No', 'Type',
													'Description', 'Action' ],
											colModel : [ {
												name : 'id',
												index : 'id',
												width : 20,
												editable : false,
											}, {
												name : 'type',
												index : 'type',
												align : 'center',
												width : 100,
												editable : false
											},  {
												name : 'description',
												index : 'description',
												width : 700,
												editable : true,
												edittype : "textarea",
												sortable : false,
												search : false
											}, {
												name : 'action',
												index : 'action',
												width : 100,
												editable : false,
												formatter : showActionButtons,
												search : false
											} ],
											height : 'auto',
											viewrecords : true,
											caption : "Surface Detail",
											gridview : true,
											loadui : 'block',
											loadonce : true,
											pager : '#surfaceDetail_pager',
											rowNum : 10,
											sortorder : "asc",
											shrinkToFit : true,
											rowList : [  10, 20, 30, 40 ],
											ignoreCase : true,
											jsonReader : {
												root : "rows",
												page : "page",
												total : "total",
												records : "records",
												repeatitems : false,

												cell : "cell",
												id : "id"
											},

										});

								$("#surfaceDetail").jqGrid('navGrid',
										'#surfaceDetail_pager', {
											edit : false,
											add : false,
											del : false,
											search : false,
											refresh : false
										}, {}, {}, {}, {});

								var searchUrl = '${path.http}/admin/packman/getSurfaceInfo';
								$("#surfaceDetail").jqGrid('setGridParam', {
									url : searchUrl,
									datatype : 'json'
								}).trigger('reloadGrid');

							});
		</script>

	</tiles:putAttribute>
	
	<tiles:putAttribute name="subheader">
		<tiles:putAttribute name="systemMessage">
		</tiles:putAttribute>
		<tiles:insertTemplate template="/views/admin/packman/sidebar.jsp">
			<tiles:putAttribute name="active" value="surfaceAdmin" />
		</tiles:insertTemplate>
	</tiles:putAttribute>

	<tiles:putAttribute name="sidebar">
		<tiles:insertTemplate
			template="/views/admin/packman/surface-subheader.jsp">
			<tiles:putAttribute name="active" value="cartonDetailUpdate" />
		</tiles:insertTemplate>
	</tiles:putAttribute>
	


	<tiles:putAttribute name="body">
		<div class="main-content lfloat">
			<div class="container" style="width: 100%;">
				<div id="surfaceUpdateDiv">
					<table id="surfaceDetail"></table>
					<div id="surfaceDetail_pager"></div>

					<br> <br> <br>
				</div>
			</div>

		</div>
	</tiles:putAttribute>
</tiles:insertTemplate>
