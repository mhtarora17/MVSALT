<%@ include file="/tld_includes.jsp"%>

<c:set var="active">
	<tiles:getAsString name="active" />
</c:set>

<div class="sidebar lfloat">
	<div id="functions">
		<div class="sidebarUL">
		<sec:authorize ifAnyGranted="admin,tech">
				<c:choose>
					<c:when test="${active =='viewstore'}">
						<div class="activeFunction functionAnchor_packman"
							onclick="javascript:window.location.href='/admin/packmangui/viewstore'">View/Edit Store</div>
					</c:when>
					<c:otherwise>
						<div class="functionAnchor_packman"
							onclick="javascript:window.location.href='/admin/packmangui/viewstore'">View/Edit Store</div>
					</c:otherwise>
				</c:choose>
			</sec:authorize>
			
			<sec:authorize ifAnyGranted="admin,tech">
				<c:choose>
					<c:when test="${active =='createstore'}">
						<div class="activeFunction functionAnchor_packman"
							onclick="javascript:window.location.href='/admin/packmangui/createstore'">Create Store</div>
					</c:when>
					<c:otherwise>
						<div class="functionAnchor_packman"
							onclick="javascript:window.location.href='/admin/packmangui/createstore'">Create Store</div>
					</c:otherwise>
				</c:choose>
			</sec:authorize>
			
			
		</div>

	</div>
</div>