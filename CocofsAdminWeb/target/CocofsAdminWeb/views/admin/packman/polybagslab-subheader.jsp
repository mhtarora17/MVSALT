<%@ include file="/tld_includes.jsp"%>

<c:set var="active">
	<tiles:getAsString name="active" />
</c:set>

<div class="sidebar lfloat">
	<div id="functions">
		<div class="sidebarUL">
			<sec:authorize ifAnyGranted="admin,tech,packmanslabview">
				<c:choose>
					<c:when test="${active =='viewPolybagSlab'}">
						<div class="activeFunction functionAnchor"
							onclick="javascript:window.location.href='/admin/packman/viewPolybagSlab'">View PolybagSlab</div>
					</c:when>
					<c:otherwise>
						<div class="functionAnchor"
							onclick="javascript:window.location.href='/admin/packman/viewPolybagSlab'">View PolybagSlab</div>
					</c:otherwise>
				</c:choose>
			</sec:authorize>
			<sec:authorize ifAnyGranted="admin,tech,packmanslabeditor">
				<c:choose>
					<c:when test="${active =='createPolybagSlab'}">
						<div class="activeFunction functionAnchor"
							onclick="javascript:window.location.href='/admin/packman/createPolybagSlab'">Create/Update PolybagSlab</div>
					</c:when>
					<c:otherwise>
						<div class="functionAnchor"
							onclick="javascript:window.location.href='/admin/packman/createPolybagSlab'">Create/Update PolybagSlab</div>
					</c:otherwise>
				</c:choose>
			</sec:authorize>
		</div>

	</div>
</div>