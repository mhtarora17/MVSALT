<%@ include file="/tld_includes.jsp"%>
<style>
.tb td {
	font-weight: bold;
	font-size: small;
}

select {
	width: 170px;
}
</style>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<tiles:insertTemplate template="/views/layout/base.jsp">

	<tiles:putAttribute name="subheader">
			<tiles:putAttribute name="systemMessage">		</tiles:putAttribute>
			
		<tiles:insertTemplate template="/views/admin/packman/sidebar.jsp">
			<tiles:putAttribute name="active" value="packman" />
		</tiles:insertTemplate>
	</tiles:putAttribute>
	
	

	<tiles:putAttribute name="sidebar">
		<tiles:insertTemplate template="/views/admin/packman/polybag-subheader.jsp">
			<tiles:putAttribute name="active" value="createPolybag" />
		</tiles:insertTemplate>
	</tiles:putAttribute>
	
	
	<tiles:putAttribute name="title" value="Create Polybag" />



	<tiles:putAttribute name="extraScripts">
		<link href="${path.css('chosen/chosen.css')}" rel="stylesheet"
			type="text/css" />
		<link rel="stylesheet" type="text/css"
			href="${path.css('snapdeal/ui.jqgrid.css')}" />

		<script type="text/javascript"
			src="${path.js('chosen/chosen.jquery.min.js')}"></script>

	</tiles:putAttribute>


	<tiles:putAttribute name="deferredScript">
		<script>
			if (typeof Snapdeal == 'undefined') {
				Snapdeal = {};
			};
			Snapdeal.getStaticPath = function(path) {
				return '${path.http}' + path;
			}

			Snapdeal.getResourcePath = function(path) {
				return '${path.resources("")}' + path;
			}
		</script>

		<script type="text/javascript"
			src="${path.js('snapdeal/validations.js')}"></script>
		<script>
			$(document).ready(function() {

				$('#sidebar-fn-polybagview').addClass('active');
				$('#header-nav-packman').addClass('active');

				$('#submitButton').click(function() {
					$("#polybagForm").validate({
						rules : {
							polybagCode : {
								required : true,
							},
							length : {
								required : true,
								maxlength : 8,
								positivedecimal:true,
								number : true,
							},
							breadth : {
								required : true,
								maxlength : 8,
								positivedecimal:true,
								number : true,
							},
							height : {
								required : true,
								maxlength : 8,
								positivedecimal:true,
								number : true,
							},
							polybagDescription:{
								maxlength:128,
							},
						},
						messages : {
							polybagCode : {
								required : "Please enter a Polybag Code",
							},
							length : {
								required : "Please provide length of Polybag",
								maxlength : "length can not exceed length 8",
							},
							breadth : {
								required : "Please provide breadth of Polybag",
								maxlength : "breadth can not exceed length 8",
							},
							height : {
								required : "Please provide height of Polybag",
								maxlength : "height can not exceed length 8",
							},
							polybagDescription:{
								maxlength:"description cannot exceed 128 chars",
							},
						}
					});

				});

			});
		</script>
	</tiles:putAttribute>


	<tiles:putAttribute name="body">
		<form:form commandName="polybagForm" id="polybagForm" method="POST"
			action='${path.http}/admin/packman/createNewPolybag' name="polybagForm">
			<div class="main-content lfloat">
				<div class="container subheaderDiv" style="width: 100%;">
					<table class="tb" width="54%" style="padding: 0px 0px 0px 62px;">
						<tr>
							<td><span>Polybag Code<span class=required>*</span>:
							</span></td>
							<td><input id="polybagCode"
								class="input-text validateNotEmpty" name="polybagCode" value=""
								tabindex="2"></td>
								<td>eg. PL01</td>
						</tr>
						<tr>
							<td><br /></td>
						</tr>
						<tr>
							<td><span>Length (cm)<span class=required>*</span>:
							</span></td>
							<td><input id="length" class="input-text validateNotEmpty"
								name="length" value="" tabindex="2"></td>
						</tr>
						<tr>
							<td><br /></td>
						</tr>
						<tr>
							<td><span>Breadth (cm)<span class=required>*</span>:
							</span></td>
							<td><input id="breadth" class="input-text validateNotEmpty"
								name="breadth" value="" tabindex="2"></td>
						</tr>
						<tr>
							<td><br /></td>
						</tr>
						<tr>
							<td><span>Height (cm)<span class=required>*</span>:
							</span></td>
							<td><input id="height" class="input-text validateNotEmpty"
								name="height" value="" tabindex="2"></td>
						</tr>
						<tr>
							<td><br /></td>
						</tr>
						<tr>
							<td><span>Polybag Description: </span></td>
							<td><textarea id="polybagDescription" class="input-xxlarge"
									name="polybagDescription" value="" tabindex="3"
									style="width: 256px" rows="6" cols="100"> </textarea></td>
						</tr>
					</table>

					<div id="errorMessage" class="alert-error">${message}</div>
					<div class="control-group">
						<div align="left" style="padding-top: 24px; padding-left: 184px;">
							<input type="submit" class="btn btn-primary threeD"
								id="submitButton" value="Create Polybag">

						</div>
					</div>
				</div>
			</div>
		</form:form>
	</tiles:putAttribute>
</tiles:insertTemplate>



