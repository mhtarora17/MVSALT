<%@ include file="/tld_includes.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<tiles:insertTemplate template="/views/layout/base.jsp">
	<tiles:putAttribute name="title" value="CartonSlab Admin" />


	<tiles:putAttribute name="extraScripts">
		<link rel="stylesheet" type="text/css"
			href="${path.css('snapdeal/ui.jqgrid.css')}" />
		<script type="text/javascript"
			src="${path.js('jquery/grid.locale-en.js')}"></script>
		<script type="text/javascript"
			src="${path.js('jquery/jquery.jqGrid.min.js')}"></script>
		<script type="text/javascript">
			var grid = $('#cartonSlabDetail');

			$(document)
					.ready(
							function() {

								$('#sidebar-fn-cartonSlab').addClass(
										'active');
								$('#header-nav-packman').addClass('active');

								$("#cartonSlabDetail")
										.jqGrid(
												{
													url : '${path.http}/admin/packman/getCartonSlabInfo',
													datatype : 'local',
													editurl : 'clientArray',
													mtype : 'POST',
													colNames : [ 'Slab',
															'Slab Size (gm)',
															'Slab Min (gm)',
															'Slab Max (gm)',
															'Cartons' ],
													colModel : [ {
														name : 'id',
														index : 'id',
														width : 100,
														editable : false,
													},

													{
														name : 'slabSize',
														index : 'slabSize',
														align : 'center',
														width : 150,
														editable : false,
														sortable : false
													}, {
														name : 'slabMin',
														index : 'slabMin',
														align : 'center',
														width : 150,
														editable : false,
														sortable : false
													}, {
														name : 'slabMax',
														index : 'slabMax',
														width : 150,
														align : 'center',
														width : 150,
														editable : false,
														sortable : false
													}, {
														name : 'cartonBoxes',
														index : 'cartonBoxes',
														width : 300,
														align : 'center',
														editable : false,
														sortable : false
													} ],
													height : 'auto',
													viewrecords : true,
													caption : "CartonSlab Detail",
													gridview : true,
													loadui : 'block',
													loadonce : true,
													pager : '#cartonSlabDetail_pager',
													rowNum : 20,
													sortorder : "asc",
													shrinkToFit : true,
													rowList : [ 20, 40, 60, 100],
													ignoreCase : true,
													jsonReader : {
														root : "rows",
														page : "page",
														total : "total",
														records : "records",
														repeatitems : false,

														cell : "cell",
														id : "id"
													},

												});

								$("#cartonSlabDetail").jqGrid('navGrid',
										'#cartonSlabDetail_pager', {
											edit : false,
											add : false,
											del : false,
											search : false,
											refresh : false
										}, {}, {}, {}, {});

								var searchUrl = '${path.http}/admin/packman/getCartonSlabInfo';
								$("#cartonSlabDetail").jqGrid('setGridParam', {
									url : searchUrl,
									datatype : 'json'
								}).trigger('reloadGrid');

							});
		</script>

	</tiles:putAttribute>
	
	<tiles:putAttribute name="subheader">			
		<tiles:insertTemplate template="/views/admin/packman/sidebar.jsp">
			<tiles:putAttribute name="active" value="cartonSlabAdmin" />
		</tiles:insertTemplate>
	</tiles:putAttribute>
	
	

	<tiles:putAttribute name="sidebar">
		<tiles:insertTemplate template="/views/admin/packman/cartonslab-subheader.jsp">
			<tiles:putAttribute name="active" value="viewCartonSlab" />
		</tiles:insertTemplate>
	</tiles:putAttribute>
	
	
	


	<tiles:putAttribute name="body">
		<div class="main-content lfloat">
			<div class="container subheaderDiv" style="width: 100%;">
				<div id="cartonSlabUpdateDiv">
					<table id="cartonSlabDetail"></table>
					<div id="cartonSlabDetail_pager"></div>

					<br> <br> <br>
				</div>
			</div>

		</div>
	</tiles:putAttribute>
</tiles:insertTemplate>