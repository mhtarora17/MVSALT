<%@ include file="/tld_includes.jsp"%>
<style>
.tb td {
	font-weight: bold;
	font-size: small;
}

select {
	width: 170px;
}
</style>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<tiles:insertTemplate template="/views/layout/base.jsp">


	<tiles:putAttribute name="title" value="Create Carton" />

	<tiles:putAttribute name="subheader">
			<tiles:putAttribute name="systemMessage">		</tiles:putAttribute>
			
		<tiles:insertTemplate template="/views/admin/packman/sidebar.jsp">
			<tiles:putAttribute name="active" value="packman" />
		</tiles:insertTemplate>
	</tiles:putAttribute>
	
	

	<tiles:putAttribute name="sidebar">
		<tiles:insertTemplate template="/views/admin/packman/carton-subheader.jsp">
			<tiles:putAttribute name="active" value="createCarton" />
		</tiles:insertTemplate>
	</tiles:putAttribute>
	

	<tiles:putAttribute name="extraScripts">
		<link href="${path.css('chosen/chosen.css')}" rel="stylesheet"
			type="text/css" />
		<link rel="stylesheet" type="text/css"
			href="${path.css('snapdeal/ui.jqgrid.css')}" />

		<script type="text/javascript"
			src="${path.js('chosen/chosen.jquery.min.js')}"></script>

	</tiles:putAttribute>


	<tiles:putAttribute name="deferredScript">
		<script>
			if (typeof Snapdeal == 'undefined') {
				Snapdeal = {};
			};
			Snapdeal.getStaticPath = function(path) {
				return '${path.http}' + path;
			}

			Snapdeal.getResourcePath = function(path) {
				return '${path.resources("")}' + path;
			}
		</script>

		<script type="text/javascript"
			src="${path.js('snapdeal/validations.js')}"></script>
		<script>
		
			$(document).ready(function() {

				$('#sidebar-fn-cartonview').addClass('active');
				$('#header-nav-packman').addClass('active');
				
				$('#submitButton').click(function(e) {
					
					
					$("#cartonForm").validate({
						rules : {
							cartonCode : {
								required : true,
							},
							length : {
								required : true,
								maxlength : 8,
								positivedecimal:true,
								number : true,
							},
							breadth : {
								required : true,
								maxlength : 8,
								positivedecimal:true,
								number : true,
							},
							height : {
								required : true,
								maxlength : 8,
								positivedecimal:true,
								number : true,
							},
							cartonDescription:{
								maxlength:128,
							},
						},
						messages : {
							cartonCode : {
								required : "Please enter a Carton Code",
							},
							length : {
								required : "Please provide length of Carton",
								maxlength : "length cannot exceed length 8",
							},
							breadth : {
								required : "Please provide breadth of Carton",
								maxlength : "breadth cannot exceed length 8",
							},
							height : {
								required : "Please provide height of Carton",
								maxlength : "height cannot exceed length 8",
							},
							cartonDescription:{
								maxlength:"description cannot exceed 128 chars",
							},
						}
					});
					
				

				});

			});
		</script>
	</tiles:putAttribute>


	<tiles:putAttribute name="body">
		<form:form commandName="cartonForm" id="cartonForm" method="POST"
			action='${path.http}/admin/packman/createNewCarton' name="cartonForm" onsubmit="return">
			<div class="main-content lfloat">
				<div class="container subheaderDiv" style="width: 100%;">
					<table class="tb" width="54%" style="padding: 0px 0px 0px 62px;">
						<tr>
							<td><span>Carton Code<span class=required>*</span>:
							</span></td>
							<td><input id="cartonCode"
								class="input-text validateNotEmpty" name="cartonCode" value=""
								tabindex="2"></td>
							<td>eg. CB01</td>
						</tr>
						<tr>
							<td><br /></td>
						</tr>
						<tr>
							<td><span>Length (cm)<span class=required>*</span>:
							</span></td>
							<td><input id="length" class="input-text validateNotEmpty"
								name="length" value="" tabindex="2"></td>
						</tr>
						<tr>
							<td><br /></td>
						</tr>
						<tr>
							<td><span>Breadth (cm)<span class=required>*</span>:
							</span></td>
							<td><input id="breadth" class="input-text validateNotEmpty"
								name="breadth" value="" tabindex="2"></td>
						</tr>
						<tr>
							<td><br /></td>
						</tr>
						<tr>
							<td><span>Height (cm)<span class=required>*</span>:
							</span></td>
							<td><input id="height" class="input-text validateNotEmpty"
								name="height" value="" tabindex="2"></td>
						</tr>
						<tr>
							<td><br /></td>
						</tr>
						<tr>
							<td><span>Carton Description: </span></td>
							<td><textarea id="cartonDescription" class="input-xxlarge"
									name="cartonDescription" value="" tabindex="3"
									style="width: 256px" rows="6" cols="100"> </textarea></td>
						</tr>
					</table>

					<div id="errorMessage" class="alert-error">${message}</div>
					<div class="control-group">
						<div align="left" style="padding-top: 24px; padding-left: 184px;">
							<input type="submit" class="btn btn-primary threeD"
								id="submitButton" value="Create Carton">

						</div>
					</div>
				</div>
			</div>
		</form:form>
	</tiles:putAttribute>
</tiles:insertTemplate>



