<%@ include file="/tld_includes.jsp"%>
<style>
.tb td {
	font-weight: bold;
	font-size: small;
}

select {
	width: 170px;
}
</style>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<tiles:insertTemplate template="/views/layout/base.jsp">


	<tiles:putAttribute name="title" value="Create Surface" />

	<tiles:putAttribute name="subheader">			
		<tiles:insertTemplate template="/views/admin/packman/sidebar.jsp">
		</tiles:insertTemplate>
	</tiles:putAttribute>
	
	
	<tiles:putAttribute name="sidebar">
		<tiles:insertTemplate template="/views/admin/packman/surface-subheader.jsp">
			<tiles:putAttribute name="active" value="createSurface" />
		</tiles:insertTemplate>
	</tiles:putAttribute>

	<tiles:putAttribute name="extraScripts">
		<link href="${path.css('chosen/chosen.css')}" rel="stylesheet"
			type="text/css" />
		<link rel="stylesheet" type="text/css"
			href="${path.css('snapdeal/ui.jqgrid.css')}" />

		<script type="text/javascript"
			src="${path.js('chosen/chosen.jquery.min.js')}"></script>

	</tiles:putAttribute>


	<tiles:putAttribute name="deferredScript">
		<script>
			if (typeof Snapdeal == 'undefined') {
				Snapdeal = {};
			};
			Snapdeal.getStaticPath = function(path) {
				return '${path.http}' + path;
			}

			Snapdeal.getResourcePath = function(path) {
				return '${path.resources("")}' + path;
			}
		</script>

		<script type="text/javascript"
			src="${path.js('snapdeal/validations.js')}"></script>
		<script>
			$(document).ready(function() {

				$('#sidebar-fn-surfaceview').addClass('active');
				$('#header-nav-packman').addClass('active');

				$('#submitButton').click(function() {
					$("#surfaceForm").validate({
						rules : {
							surfaceType : {
								required : true,
								maxlength : 15,
							},
							surfaceDescription : {
								required : true,
								maxlength : 2048,
							},
							
						},
						messages : {
							surfaceType : {
								required : "Please enter a Surface Type",
								maxlength : "type cannot exceed 15 chars",
							},
							surfaceDescription : {
								required : "Please enter a Instruction",
								maxlength : "description cannot exceed 2048 chars",
							},
							
						}
					});

				});

			});
		</script>
	</tiles:putAttribute>


	<tiles:putAttribute name="body">
		<form:form commandName="surfaceForm" id="surfaceForm" method="POST"
			action='${path.http}/admin/packman/createNewSurfaceType' name="surfaceForm">
			<div class="main-content lfloat">
				<div class="container" style="width: 100%;">
					<table class="tb" width="54%" style="padding: 0px 0px 0px 62px;">
						<tr>
							<td><span>Surface Type<span class=required>*</span>:
							</span></td>
							<td><input id="surfaceType"
								class="input-text validateNotEmpty" name="surfaceType" value=""
								tabindex="2"></td>
							<td>eg. SURFACE_A</td>
						</tr>
						<tr>
							<td><br /></td>
						</tr>
						
						<tr>
							<td><span>Packaging Instruction<span class=required>*</span>: </span></td>
							<td><textarea id="surfaceDescription" class="input-xxlarge validateNotEmpty"
									name="surfaceDescription" value="" tabindex="3"
									style="width: 256px" rows="6" cols="100"></textarea></td>
						</tr>
					</table>

					<div id="errorMessage" class="alert-error">${message}</div>
					<div class="control-group">
						<div align="left" style="padding-top: 24px; padding-left: 184px;">
							<input type="submit" class="btn btn-primary threeD"
								id="submitButton" value="Create Surface">

						</div>
					</div>
				</div>
			</div>
		</form:form>
	</tiles:putAttribute>
</tiles:insertTemplate>



