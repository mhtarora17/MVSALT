<%@ include file="/tld_includes.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<tiles:insertTemplate template="/views/layout/base.jsp">
	<tiles:putAttribute name="title" value="Store rule create/update" />

	<tiles:putAttribute name="systemMessage">
		<div class="system-message"></div>
	</tiles:putAttribute>

	<tiles:putAttribute name="sidebar">
		<tiles:insertTemplate template="/views/admin/packman/rules/rule-subheader.jsp">
			<tiles:putAttribute name="active" value="createrule" />
		</tiles:insertTemplate>
	</tiles:putAttribute>

	<tiles:putAttribute name="extraScripts">
<link href="${path.css('chosen/chosen.css')}" rel="stylesheet"
    type="text/css" />
    <link rel="stylesheet" type="text/css"
            href="${path.css('snapdeal/ui.jqgrid.css')}" />
            
    <script type="text/javascript"
    src="${path.js('chosen/chosen.jquery.min.js')}"></script>
    
    <script type="text/javascript"
    src="${path.js('snapdeal/validate.js')}"></script>
    
    		<script type="text/javascript"
			src="${path.js('jquery/jquery.ui.datepicker.min.js')}"></script>
			
			<script type="text/javascript"
			src="${path.js('jquery/jquery.datetimepicker.full.min.js')}"></script>
			
			<script type="text/javascript"
			src="${path.js('jquery/moment.js')}"></script>
			
			<link rel="stylesheet" type="text/css"
            href="${path.css('jquery/jquery.datetimepicker.css')}" />
            
    
	</tiles:putAttribute>
	<tiles:putAttribute name="systemMessage">
		<div class="system-message">
			<input type="hidden" value="${message }" id="messageHiddenField"/>
		</div>
		<div class="system-message">
			<input type="hidden" value="${validationResponse}" id="validationResponseHiddenField"/>
		</div>
	</tiles:putAttribute>

	<tiles:putAttribute name="subheader">
		<tiles:putAttribute name="systemMessage">
		</tiles:putAttribute>		
	 <tiles:insertTemplate template="/views/admin/packman/sidebar.jsp">
			<tiles:putAttribute name="active" value="packman" />
		</tiles:insertTemplate> 
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
		<div class="main-content_packman lfloat">
			<div class="container" style="width: 85%;">
				
			<form id="store-rule-form" class="form-horizontal" method="post" action="">
				
							  <input type="text" style="display:none" name="pageId" value="${pageId}">
				
				
			  <div class="control-group">
			    <label class="control-label" for="Rule-Type">Rule Type</label>
			    <div class="controls" id="Rule-Type">
				    <select name="RuleType"  id="RuleType" required="required" <c:if test="${StoreCreateRuleDTO.ruleName != null}"> disabled </c:if>>
 						<option value="rulesCreate">Select Rule Type</option>
								<option value="storeRuleCreate" selected>Store</option>
								<option value="categoryRuleCreate">Category</option>
								<option value="subcategoryRuleCreate">Subcategory</option>
								<option value="supcRuleCreate">Supc</option>
				    </select>
			    </div>
			    </div>
			    
			    <c:if test="${StoreCreateRuleDTO.ruleName != null}">
				    <div class="control-group">
				    <label class="control-label" for="rule-name">Rule Name</label>
				    <div class="controls">
	 			      <input type="text" id="rule-name" name="name" value="${StoreCreateRuleDTO.ruleName}" disabled>
				    </div>
				  </div>
			  </c:if>
			    
			    <div class="control-group">
			    <label class="control-label" for="store-code">Store Code</label>
			    <div class="controls">
 			      <input disabled type="text" id="store-code" name="storeCode" value="${storeCode}" >
			    </div>
			  </div>
			    
			  <div class="control-group">
                 <label class="control-label"
                     for="frm-startdate">Start Date</label>
                 <div class="controls">
                     <input type="text" name="startDate" from="startDate" id="frm-startdate" class="" />
                 </div>
              </div>
              <div class="control-group">
                 <label class="control-label"
                     for="frm-enddate">End  Date</label>
                 <div class="controls">
                     <input type="text" name="endDate" id="frm-enddate" />
                 </div>
              </div>
              
              
              <div class="control-group">
			    <label class="control-label" for="packaging-type">Packaging Type</label>
			    <div class="controls" id="packaging-type">
				    <select name="packagingType"  id="packagingType" >
					   <c:forEach var="packagingType" items="${packagingType}" >
							<option value="${packagingType}" <c:if test="${packagingType == StoreCreateRuleDTO.packagingType}"> selected </c:if> >${packagingType}</option>
						</c:forEach>
				    </select>
			    </div>
			  </div>
			  <div class="control-group">
			    		<label class="control-label" for="check-enabled">Enabled</label>

			     <div class="controls">
<input type="checkbox" name="enabled" value="true" 
	<c:choose>
			      	 <c:when test="${StoreCreateRuleDTO!=null && StoreCreateRuleDTO.enabled==true}">
			      	 	checked
			      	 </c:when>
			      	 <c:when test="${StoreCreateRuleDTO==null}">
			      	 	checked
			      	 </c:when>
			      	 </c:choose>
/>			   
			    </div>
			  </div>
			  
			  <c:if test="${StoreCreateRuleDTO.ruleName != null}">
			  <div class="control-group">
			    <div class="controls">
			      <button id="upload-form-btn" type="submit" class="btn btn-primary">Update Rule</button>
					<button class="btn" onclick="javascript:window.open('','_self').close();">close</button>			    
					</div>
			  </div>
			  </c:if>
			  <c:if test="${StoreCreateRuleDTO.ruleName == null}">
			  <div class="control-group">
			    <div class="controls">
			      <button id="upload-form-btn" type="submit" class="btn btn-primary">Create Rule</button>
					</div>
			  </div>
			  </c:if>
			</form>
			<c:if test="${message != null && message!=\"\"}">
			<div class="alert alert-info">
                <button type="button" class="close" data-dismiss="alert">×
                </button>
                ${message}
                <br>
			</div>
			</c:if>
			<c:if test="${errorfinal != null}">
			<div class="alert alert-error">
                <button type="button" class="close" data-dismiss="alert">×
                </button>
                ${errorfinal}
                <br>
			</div>
			</c:if>
			<c:if test="${errorcause != null}">
			<div class="alert alert-error">
                <button type="button" class="close" data-dismiss="alert">×
                </button>
                ${errorcause}
                <br>
			</div>
			</c:if>
			
			</div>
		</div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
	<%-- Activate relevant sidebar function   --%>
	<%-- Activate relevant subheader function --%>
		<script type="text/javascript">
		function hideSidebar(){
		    $(".subheader").css({"left":"-11%"});
		    $(".functionAnchor").css({"display": "none"});
		    $(".sidebar").css({"left":"1%","width":"99%"});
		    $(".main-content").css({"left":"1%","width":"98%"});
		}
		function showSidebar(){
		    $(".subheader").css({"left":"0%"});
		    $(".functionAnchor").css({"display": "block"});
		    $(".sidebar").css({"left":"12%","width":"88%"});
		    $(".main-content").css({"left":"12%","width":"87%"});
		}
		
			$(document).ready(function() {
				
				if("${StoreCreateRuleDTO.startDate}"!=""){
					$('#frm-startdate').val(moment("${StoreCreateRuleDTO.startDate}").format('YYYY-MM-DD HH:mm:ss'));
				}
				
				if("${StoreCreateRuleDTO.endDate}"!=""){
					$('#frm-enddate').val(moment("${StoreCreateRuleDTO.endDate}").format('YYYY-MM-DD HH:mm:ss'));
				}
				
				var subheader=true;
				var test = window.location.href;
				var lastIndex = test.lastIndexOf('/');
				var url = test.substr(0,lastIndex);
				var selected = test.substr(lastIndex+1);
				 $(".subheaderToggle").click(function(){
				         if(subheader){
				                 hideSidebar();
				                 $(".subheaderToggleContent").html(">");
				                 subheader=false;
				         }else{
				                 showSidebar();
				                 $(".subheaderToggleContent").html("<");
				                 subheader=true;
				         }
				 });
				 
				$("#RuleType").change(function(){
					if(confirm('Are you sure ? This will remove current rule data')){
					console.log(url+"/"+$(this).val());
					window.location.href= "${path.http}/admin/packmangui/${storeCode}/createrule/?page="+$(this).val()+"&pageId=${pageId}";
					}
				});
				
				
 				$("#RuleType [value='${page}']").attr('selected',true);
				
				$('#frm-startdate').datetimepicker({
                    /* arrows: false,
                    presets:{specificDate: 'Pick a date'},
                    presetRanges:[],
                    showOn: "button",
              buttonImage: "/static/cocofs/img/calendar.jpg",
              buttonImageOnly: false, */
					format:'Y-m-d H:i:00'
			});
				
				if($('#frm-startdate').val()=="")
                	$('#frm-startdate').val(moment(new Date()).format('YYYY-MM-DD HH:mm:ss'));
                
                $('#frm-enddate').datetimepicker({
                   /*  arrows: false,
                    presets:{specificDate: 'Pick a date'},
                    presetRanges:[],
                    showOn: "button",
              buttonImage: "/static/cocofs/img/calendar.jpg",
              buttonImageOnly: false, */
                	format:'Y-m-d H:i:00'
                  });
				
			
			function isInvalid(startDateObj,endDateObj,ruleCreation) {
				var start  = $("#frm-startdate").val().trim();
                var end  = $("#frm-enddate").val().trim();
                if(start==null || start==""){
                	alert("Start date should not be empty");
                	return true;
                }
	            if (! (start == moment($("#frm-startdate").val()).format('YYYY-MM-DD HH:mm:ss'))) {
	                alert("Start Date appears to be incorrect");
	                return true;
	            }
	            if ((end.length != 0) &&  (end != moment($("#frm-enddate").val()).format('YYYY-MM-DD HH:mm:ss'))) {
	                alert("End Date appears to be incorrect");
	                return true;
	            }
	            	
	            if ( ruleCreation ) {
	            	
	            	var x = $('#frm-startdate').val();
	            	/*var x = $.datepicker.formatDate('yy-mm-dd', new Date())
            		 if ( $('#frm-startdate').val() < x ) {
	                	alert('Start date before today not allowed'); 
	                	return true;
	                } */
	            	var ed = $('#frm-enddate').val();
	            	if ( ed != "" && ed <= x ) {
	                	alert('End date before start date or equal to start date is not allowed'); 
	                	return true;
	                }
	            }
	            
	            
	            return false;
	        }
			
			function valid(){
				$('.error').remove();
				if($("#packagingType").val()==null || $("#packagingType").val()=="" ){
					$("#packagingType").after("<div class='error'>" + "Please select packaging type" +  "</div>");
					return false;
				}
				return true;
			}
			
			$('#upload-form-btn').click(function() {
				if(isInvalid($("#frm-startdate").datetimepicker('getDate'), $("#frm-enddate").datepicker('getDate') , true)==false && valid() && confirm('Are you sure to save this rule ?')){
					$("#frm-startdate").prop('disabled',false);
					$("#frm-enddate").prop('disabled',false);
					$("#rule-name").prop('disabled',false);
					var urlToSubmit = "${path.http}/admin/packmangui/${storeCode}/createrule/createStoreRule/";
					$("#store-rule-form").attr('action',urlToSubmit);
					$('#store-rule-form').submit();
					return true;
				}
				return false;
			} );
				
			});
		</script>
			
		
	</tiles:putAttribute>
</tiles:insertTemplate>
