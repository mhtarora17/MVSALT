<%@ include file="/tld_includes.jsp"%>


<div id="subheader" class="subheader"
	style="background-image: url(${path.resources('img/background/bg_blue.png')});">
	<div class="tabbable">
		<!-- Only required for left/right tabs -->
		<div class="subheaderTab">
			<div id="recommendation-upload" onclick="javascript:window.location.href='recommendation'">Packman Recommendation</div>
		</div>
	</div>
	<div class="subheaderToggle">
		<div class="subheaderToggleContent">&lt</div>
	</div>
</div>