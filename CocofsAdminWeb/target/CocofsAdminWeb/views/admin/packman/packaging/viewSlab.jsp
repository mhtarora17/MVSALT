<%@ include file="/tld_includes.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<tiles:insertTemplate template="/views/layout/base.jsp">
	<tiles:putAttribute name="title" value="View Slab" />


	<tiles:putAttribute name="extraScripts">
		<link rel="stylesheet" type="text/css"
			href="${path.css('snapdeal/ui.jqgrid.css')}" />
		<script type="text/javascript"
			src="${path.js('jquery/grid.locale-en.js')}"></script>
		<script type="text/javascript"
			src="${path.js('jquery/jquery.jqGrid.min.js')}"></script>
		<script type="text/javascript">
			var grid = $('#cartonSlabDetail');

			$(document)
					.ready(
							function() {
								$('#header-nav-packman').addClass('active');

								$("#cartonSlabDetail")
										.jqGrid(
												{
													url : '${path.http}/admin/packmangui/${storeCode}/getSlabInfo/',
													datatype : 'local',
													editurl : 'clientArray',
													mtype : 'POST',
													colNames : [ 'SR No.','name',
															'Slab Size (gm)',
															'Slab Min (gm)',
															'Slab Max (gm)',
															'Packaging Type Items' ],
													colModel : [ {
														name : 'id',
														index : 'id',
														width : 100,
														editable : false,
													},{
														name : 'name',
														index : 'name',
														align : 'center',
														width : 150,
														editable : false,
														sortable : true
													},{
														name : 'size',
														index : 'size',
														align : 'center',
														width : 150,
														editable : false,
														sortable : true
													}, {
														name : 'lowerLimit',
														index : 'lowerLimit',
														align : 'center',
														width : 150,
														editable : false,
														sortable : true
													}, {
														name : 'maxSize',
														index : 'maxSize',
														width : 150,
														align : 'center',
														width : 150,
														editable : false,
														sortable : true
													}, {
														name : 'packagingTypeItems',
														index : 'packagingTypeItems',
														width : 300,
														align : 'center',
														editable : false,
														sortable : true
													} ],
													height : 'auto',
													viewrecords : true,
													caption : "CartonSlab Detail",
													gridview : true,
													loadui : 'block',
													loadonce : true,
													pager : '#cartonSlabDetail_pager',
													rowNum : 20,
													sortorder : "asc",
													shrinkToFit : true,
													rowList : [ 20, 40, 60, 100],
													ignoreCase : true,
													jsonReader : {
														root : "rows",
														page : "page",
														total : "total",
														records : "records",
														repeatitems : false,

														cell : "cell",
														id : "id"
													},

												});

								$("#cartonSlabDetail").jqGrid('navGrid',
										'#cartonSlabDetail_pager', {
											edit : false,
											add : false,
											del : false,
											search : false,
											refresh : false
										}, {}, {}, {}, {});

								var searchUrl = '${path.http}/admin/packmangui/${storeCode}/getSlabInfo/';
								$("#cartonSlabDetail").jqGrid('setGridParam', {
									url : searchUrl,
									postData : {storeCode:'${storeCode}',packagingTypeCode:'${packagingTypeCode}'},	
									datatype : 'json'
								}).trigger('reloadGrid');

							});
		</script>

	</tiles:putAttribute>
	
	<tiles:putAttribute name="subheader">			
		<tiles:insertTemplate template="/views/admin/packman/sidebar.jsp">
			<tiles:putAttribute name="active" value="packman" />
		</tiles:insertTemplate>
	</tiles:putAttribute>
	
	

	<tiles:putAttribute name="sidebar">
		<tiles:putAttribute name="systemMessage">		</tiles:putAttribute>
		<tiles:insertTemplate template="/views/admin/packman/packaging/slab-subheader.jsp">
			<tiles:putAttribute name="active" value="viewslab" />
		</tiles:insertTemplate>
	</tiles:putAttribute>
	
	
	


	<tiles:putAttribute name="body">
		<div class="main-content_packman lfloat">
			<div class="container subheaderDiv" style="width: 100%;">
				<div id="cartonSlabUpdateDiv">
					<table id="cartonSlabDetail"></table>
					<div id="cartonSlabDetail_pager"></div>

					<br> <br> <br>
				</div>
			</div>

		</div>
	</tiles:putAttribute>
</tiles:insertTemplate>