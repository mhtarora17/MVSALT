<%@ include file="/tld_includes.jsp"%>

<c:set var="active">
	<tiles:getAsString name="active" />
</c:set>

<div class="sidebar lfloat">
	<div id="functions">
		<div class="sidebarUL">
			<c:set var="url" value="${storeCode}/viewpackagingtype/" />
			<c:if test="${fn:contains(jsonvalue,url)}">
				<c:choose>
					<c:when test="${active =='viewpackagingtype'}">
						<div class="activeFunction functionAnchor" onclick=post('/admin/packmangui/${storeCode}/viewpackagingtype/',{storeCode:'${storeCode}',pageId:'${pageId}'},'post')>View
							Packaging Type</div>
					</c:when>
					<c:otherwise>
						<div class="functionAnchor" onclick=post('/admin/packmangui/${storeCode}/viewpackagingtype/',{storeCode:'${storeCode}',pageId:'${pageId}'},'post')>View
							Packaging Type</div>
					</c:otherwise>
				</c:choose>
			</c:if>

			<c:set var="url" value="${storeCode}/createpackagingtype/" />
			<c:if test="${fn:contains(jsonvalue,url)}">
				<c:choose>
					<c:when test="${active =='createpackagingtype'}">
						<div class="activeFunction functionAnchor" onclick=post('/admin/packmangui/${storeCode}/createpackagingtype/',{storeCode:'${storeCode}',pageId:'${pageId}'},'post')>Create/Update
							Packaging Type</div>
					</c:when>
					<c:otherwise>
						<div class="functionAnchor" onclick=post('/admin/packmangui/${storeCode}/createpackagingtype/',{storeCode:'${storeCode}',pageId:'${pageId}'},'post')>Create/Update
							Packaging Type</div>
					</c:otherwise>
				</c:choose>
			</c:if>

		</div>

		<script type="text/javascript" src="${path.js('snapdeal/post.js')}"></script>

	</div>
</div>