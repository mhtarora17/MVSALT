<%@ include file="/tld_includes.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<tiles:insertTemplate template="/views/layout/base.jsp">
	<tiles:putAttribute name="title" value="View Packaging Types" />

	<tiles:putAttribute name="systemMessage">
		<div class="system-message"></div>
	</tiles:putAttribute>

	<tiles:putAttribute name="subheader">
		<tiles:putAttribute name="systemMessage">
		</tiles:putAttribute>
		<tiles:insertTemplate template="/views/admin/packman/sidebar.jsp">
			<tiles:putAttribute name="active" value="packman" />
		</tiles:insertTemplate>
	</tiles:putAttribute>

	<tiles:putAttribute name="extraScripts">
		<link rel="stylesheet" type="text/css"
			href="${path.css('snapdeal/ui.jqgrid.css')}" />
		<script type="text/javascript"
			src="${path.js('jquery/grid.locale-en.js')}"></script>
		<script type="text/javascript"
			src="${path.js('jquery/jquery.jqGrid.min.js')}"></script>
		<script type="text/javascript">
			var grid = $('#packagingTypeDetail');

			function validateRowData(inputData) {
				return true;
			}

			var lastsel2;
			function editGridData(rowId) {
				var row = $("#packagingTypeDetail").jqGrid('getRowData', rowId);
				var type = row.type;
				send(type);
			}
			
			function send(type){
				post('/admin/packmangui/${storeCode}/createpackagingtype/',{storeCode:'${storeCode}',pageId:'${pageId}',packagingTypeCode:type},'post');
			}


			function showActionButtons(cellValue, options, rowObject) {
				be = "<input style='height:22px;width:20px;' type='button' title='Edit' value='E' onclick=editGridData('"
					+ options.rowId + "');>";
				return be ;

			}

			$(document)
					.ready(
							function() {

								width = $("#packagingTypeUpdateDiv").parent().width()-150;
								col1width = parseInt(width*0.10);
								col2width = parseInt(width*0.2);
								col3width = parseInt(width*0.10);
								col4width = parseInt(width*0.10);
								col5width = parseInt(width*0.3);
								
								$("#packagingTypeDetail").jqGrid(
										{
											url : '',
											datatype : 'local',
											editurl : 'clientArray',
											mtype : 'POST',
											colNames : [ 'S.No','Type' ,'Packaging Mode',
													'Store','Description','Enable',
													'Action'],
											colModel : [ {
												name : 'id',
												index : 'id',
												align : 'center',
												width : col1width,
												editable : false,
												sorttype : 'number'
											},  {
												name : 'type',
												index : 'type',
												align : 'center',
												width : col2width,
												editable : false,
												sorttype : 'text'
											}, {
												name : 'mode',
												index : 'mode',
												align : 'center',
												width : col3width,
												editable : false,
												sorttype : 'text'
											}, {
												name : 'code',
												index : 'code',
												align : 'center',
												width : col4width,
												editable : false,
												sorttype : 'text'
											},{
												name : 'description',
												index : 'description',
												align : 'center',
												width : col5width,
												editable : true
											},{
												name : 'enabled',
												index : 'enabled',
												align : 'center',
												width : 50,
												editable : false,
												edittype: 'checkbox', editoptions: { value: "true:false" }, 
												formatter: "checkbox", formatoptions: { disabled: true},
												checked : true
											}, {
												name : 'action',
												index : 'action',
												align : 'center',
												width : 50,
												editable : false,
												formatter : showActionButtons,
												search : false
											} ],
											height : 'auto',
											viewrecords : true,
											caption : "Packaging Type Detail",
											gridview : true,
											loadui : 'block',
											loadonce : true,
											pager : '#packagingTypeDetail_pager',
											rowNum : 20,
											sortorder : "asc",
											shrinkToFit : true,
											rowList : [ 20, 40, 60, 100 ],
											ignoreCase : true,
											jsonReader : {
												root : "rows",
												page : "page",
												total : "total",
												records : "records",
												repeatitems : false,

												cell : "cell",
												id : "id"
											},

										});

								$("#packagingTypeDetail").jqGrid('navGrid',
										'#packagingTypeDetail_pager', {
											edit : false,
											add : false,
											del : false,
											search : false,
											refresh : false
										}, {}, {}, {}, {});

								var searchUrl = '${path.http}/admin/packmangui/${storeCode}/getPackagingTypeInfo/';
								$("#packagingTypeDetail").jqGrid('setGridParam', {
									url : searchUrl,
									datatype : 'json',
									postData : {storeCode:'${storeCode}'}	
								}).trigger('reloadGrid');

								if('${jsonvalue}'.indexOf("admin/packmangui/${storeCode}/createpackagingtype/")==-1){
								$("#packagingTypeDetail").jqGrid('hideCol', 'action');
								}
								
							});
		</script>

	</tiles:putAttribute>
	<tiles:putAttribute name="sidebar">
		<tiles:insertTemplate
			template="/views/admin/packman/packaging/packagingtype-subheader.jsp">
			<tiles:putAttribute name="active" value="viewpackagingtype" />
		</tiles:insertTemplate>
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
		<div class="main-content_packman lfloat">
			<div class="container subheaderDiv" style="width: 100%;">
				<div id="packagingTypeUpdateDiv">
					<table id="packagingTypeDetail"></table>
					<div id="packagingTypeDetail_pager"></div>

					<br> <br> <br>
				</div>
			</div>

		</div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
		<%-- Activate relevant sidebar function   --%>
		<%-- Activate relevant subheader function --%>
		<script type="text/javascript">

		function hideSidebar(){
		    $(".subheader").css({"left":"-11%"});
		    $(".functionAnchor").css({"display": "none"});
		    $(".sidebar").css({"left":"1%","width":"99%"});
		    $(".main-content").css({"left":"1%","width":"98%"});
		}
		function showSidebar(){
		    $(".subheader").css({"left":"0%"});
		    $(".functionAnchor").css({"display": "block"});
		    $(".sidebar").css({"left":"12%","width":"88%"});
		    $(".main-content").css({"left":"12%","width":"87%"});
		}
				$('#sidebar-fn-viewpackagingtype').addClass('active');
				$('#header-nav-packman').addClass('active');
			
		</script>


	</tiles:putAttribute>
</tiles:insertTemplate>
