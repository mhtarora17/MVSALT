<%@ include file="/tld_includes.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<tiles:insertTemplate template="/views/layout/base.jsp">
	<tiles:putAttribute name="title" value="View Packaging Type Items" />

	<tiles:putAttribute name="systemMessage">
		<div class="system-message"></div>
	</tiles:putAttribute>

	<tiles:putAttribute name="sidebar">
		<tiles:insertTemplate template="/views/admin/packman/packaging/packagingtypeitem-subheader.jsp">
			<tiles:putAttribute name="active" value="viewpackagingtypeitem" />
		</tiles:insertTemplate>
	</tiles:putAttribute>

	<tiles:putAttribute name="extraScripts">
		<link rel="stylesheet" type="text/css"
			href="${path.css('snapdeal/ui.jqgrid.css')}" />
		<script type="text/javascript"
			src="${path.js('jquery/grid.locale-en.js')}"></script>
		<script type="text/javascript"
			src="${path.js('jquery/jquery.jqGrid.min.js')}"></script>
		<script type="text/javascript">
			var grid = $('#cartonDetail');

			function isValidDecimal(value, element) {
				// allow any non-whitespace characters as the host part
				if (/^(?=.*[1-9])\d*(?:\.\d{1,2})?$/.test(value)) {
					return true;
				} else {
					false;
				}
			};

			function isValid(str) { return /^[a-z0-9_ ]+$/i.test(str); }
			
			function validateRowData(inputData) {
				if(${show}==false)
					return true;
				length = inputData.length.trim();
				breadth = inputData.breadth.trim();
				height = inputData.height.trim();
				error = "";
				errorNo = 1;
				returnrequired = false;
				if(length==null || length==""){
					error += (errorNo++)+") Length is Required\n";
					returnrequired = true;
				}
				if(breadth==null || breadth==""){
					error += (errorNo++)+") Breadth is Required\n";
					returnrequired = true;
				}
				if(height==null || height==""){
					error += (errorNo++)+") Height is Required\n";
					returnrequired = true;
				}
				if(returnrequired){
					alert(error);
					return false;
				}
				if(isNaN(length)){
					error += (errorNo++)+") Length should be number only\n";
					returnrequired = true;
				}
				if(isNaN(breadth)){
					error += (errorNo++)+") Breadth should be number only\n";
					returnrequired = true;
				}
				if(isNaN(height)){
					error += (errorNo++)+") Height should be number only\n";
					returnrequired = true;
				}
				if(returnrequired){
					alert(error);
					return false;
				}
				if(length<=0){
					error += (errorNo++)+") Length should be greater than 0\n";
					returnrequired = true;

				}
				if(breadth<=0){
					error += (errorNo++)+") Breadth should be greater than 0\n";
					returnrequired = true;

				}
				if(height<=0){
					error += (errorNo++)+") Height should be greater than 0\n";
					returnrequired = true;

				}
				if(/^(?=.*[1-9])\d*(?:\.\d{1,2})?$/.test(length)==false){
					error += (errorNo++)+") Length should be positive number having atmost 2 digits after decimal\n";
					returnrequired = true;
				}
				if(/^(?=.*[1-9])\d*(?:\.\d{1,2})?$/.test(breadth)==false){
					error += (errorNo++)+") Breadth should be positive number having atmost 2 digits after decimal\n";
					returnrequired = true;
				}
				if(/^(?=.*[1-9])\d*(?:\.\d{1,2})?$/.test(height)==false){
					error += (errorNo++)+") Height should be positive number having atmost 2 digits after decimal\n";
					returnrequired = true;
				}
				if(returnrequired == false){
					return true;
				}
				alert(error);
				return false;
			}

			var lastsel2;
			function editGridData(rowId) {
				if (rowId && rowId !== lastsel2) {
					jQuery('#packagingTypeDetail').jqGrid('restoreRow', lastsel2);
					jQuery('#packagingTypeDetail').jqGrid('editRow', rowId, true);
					lastsel2 = rowId;
				}

			}

			function restoreGridData(rowId) {
				var row = $("#packagingTypeDetail").jqGrid('getRowData', rowId);
				jQuery('#packagingTypeDetail').restoreRow(rowId);
				lastsel2 = 0000000;
			}

			function saveGridData(rowId) {
				$("#packagingTypeDetail").jqGrid('saveRow', rowId);
				var inputData = new Object();
				var row = $("#packagingTypeDetail").jqGrid('getRowData', rowId);
				var packagingTypeItemDetailDTO = new Object();
				if (!validateRowData(row)) {
					$('#packagingTypeDetail').jqGrid('editRow', rowId, true, null,
							null, 'clientArray');
					return false;
				}
				packagingTypeItemDetailDTO.id = row.id;
				packagingTypeItemDetailDTO.code = row.code;
				packagingTypeItemDetailDTO.description = row.description;
				packagingTypeItemDetailDTO.packagingType = row.packagingType;
				packagingTypeItemDetailDTO.storeCode = '${storeCode}';
				packagingTypeItemDetailDTO.slab = row.slab;
				packagingTypeItemDetailDTO.length = row.length;
				packagingTypeItemDetailDTO.breadth = row.breadth;
				packagingTypeItemDetailDTO.height = row.height;
				packagingTypeItemDetailDTO.volWeight = row.volWeight;
				
				inputData.packagingTypeItemDetailDTO = packagingTypeItemDetailDTO;
				delete row.action;
				$
						.ajax({
							type : "POST",
							url : "${path.http}/admin/packmangui/${storeCode}/updatepackagingtypeitem/",
							dataType : 'json',
							postData : {storeCode:'${storeCode}'},
							contentType : 'application/json',
							data : JSON.stringify(inputData),
							async : false,
							success : function(resp) {
								if (resp.successful) {
									var searchUrl = '${path.http}/admin/packmangui/${storeCode}/getPackagingTypeItemInfo/';
									$("#packagingTypeDetail").jqGrid('setGridParam', {
										url : searchUrl,
										datatype : 'json',
										postData : {storeCode:'${storeCode}',packagingTypeCode:'${packagingTypeCode}'}	
									}).trigger('reloadGrid');
								} else {
									alert(resp.response);

								}
								$("#packagingTypeDetail").jqGrid('setGridParam', {
									url : searchUrl,
									datatype : 'json'
								}).trigger('reloadGrid');
								lastsel2 = 0000000;
							},
							error : function(xhr, statusCode, error) {
								if (xhr.status == 200) {
									alert("Your session has expired. Please log in again in another tab/window and then retry here.");
									$('#packagingTypeDetail').jqGrid('editRow', rowId,
											true, null, null, 'clientArray');
								}
							}
						});
			}

			function deleteGridData(rowId) {

				var r = confirm("Do you really want to delete it?");
				if (r == false) {
					return;
				}

				var row = $("#packagingTypeDetail").jqGrid('getRowData', rowId);
				
				var data = new Object();
				var inputData1 = new Object();
				
				data.storeCode = '${storeCode}';
				data.packagingType = '${packagingTypeCode}';
				data.code = row.code;
				data.id = row.id;
				data.description = row.description;
				data.slab = row.slab;
				data.length = row.length;
				data.breadth = row.breadth;
				data.height = row.height;
				data.volWeight = row.volWeight;
				
				inputData1.packagingTypeItemDetailDTO = data;
				
				delete row.action;
				$
						.ajax({
							type : "POST",
							url : "${path.http}/admin/packmangui/${storeCode}/deletepackagingtypeitem/",
							dataType : 'json',
							contentType : 'application/json',
							data : JSON.stringify(inputData1),
							async : false,
							success : function(resp) {
								if (resp.successful) {
									var searchUrl = '${path.http}/admin/packmangui/${storeCode}/getPackagingTypeItemInfo/';
									$("#packagingTypeDetail").jqGrid('setGridParam', {
										url : searchUrl,
										datatype : 'json',
										postData : {storeCode:'${storeCode}',packagingTypeCode:'${packagingTypeCode}'}	
									}).trigger('reloadGrid');
								} else {
									alert(resp.response);

								}
								$("#packagingTypeDetail").jqGrid('setGridParam', {
									url : searchUrl,
									datatype : 'json'
								}).trigger('reloadGrid');
								lastsel2 = 0000000;
							 },
							error : function(xhr, statusCode, error) {
								if (xhr.status == 200) {
									alert("Your session has expired. Please log in again in another tab/window and then retry here.");
									$('#packagingTypeDetail').jqGrid('editRow', rowId,
											true, null, null, 'clientArray');
								}
							}
						});

			}
			function showActionButtons(cellValue, options, rowObject) {
				be = "<input style='height:22px;width:20px;' type='button' title='Edit' value='E' onclick=editGridData('"
						+ options.rowId + "');>";
				se = "<input style='height:22px;width:20px;' type='button' title='Save' value='S' onclick=saveGridData('"
						+ options.rowId + "');>";
				ce = "<input style='height:22px;width:20px;' type='button' title='Cancel' value='C' onclick=restoreGridData('"
						+ options.rowId + "');>";
				de = "<input style='height:22px;width:20px;' type='button' title='Delete' value='D' onclick=deleteGridData('"
						+ options.rowId + "');>";
				return be + se + ce + de;

			}

			$(document)
					.ready(
							function() {

								width = $("#packagingTypeUpdateDiv").parent().width()-150;
								col1width = parseInt(width*0.05);
								col2width = parseInt(width*0.15);
								col3width = parseInt(width*0.1);
								col4width = parseInt(width*0.1);
								col5width = parseInt(width*0.10);
								col6width = parseInt(width*0.10);
								col7width = parseInt(width*0.10);
								col8width = parseInt(width*0.10);
								col9width = parseInt(width*0.10);
								
								if(${show}==false){
									col1width = 2*col1width;
									col2width = 2*col2width;
									col4width = 2*col4width;
									col9width = 2*col9width;
								}
								
								$("#packagingTypeDetail").jqGrid(
										{
											url : '',
											datatype : 'local',
											editurl : 'clientArray',
											mtype : 'POST',
											colNames : [ 'S.No','Code','Slab' ,'Packaging Type',
													'Length (in cm)','Breadth (in cm)','Height (in cm)','Vol Wt (in gms)','Description','Action'],
											colModel : [ {
												name : 'id',
												index : 'id',
												align : 'center',
												width : col1width,
												editable : false,
												sorttype : 'number'
											},  {
												name : 'code',
												index : 'code',
												align : 'center',
												width : col2width,
												editable : false,
												sorttype : 'text'
											}, {
												name : 'slab',
												index : 'slab',
												align : 'center',
												width : col3width,
												editable : false,
												hidden : true,
												sorttype : 'text'
											}, {
												name : 'packagingType',
												index : 'packagingType',
												align : 'center',
												width : col4width,
												editable : false,
												sorttype : 'text'
											},{
												name : 'length',
												index : 'length',
												align : 'center',
												width : col5width,
												editable : true,
												sorttype : 'number'
											},{
												name : 'breadth',
												index : 'breadth',
												align : 'center',
												width : col6width,
												editable : true,
												sorttype : 'number'
											},{
												name : 'height',
												index : 'height',
												align : 'center',
												width : col7width,
												editable : true,
												sorttype : 'number'
											},{
												name : 'volWeight',
												index : 'volWeight',
												align : 'center',
												width : col8width,
												editable : false,
												sorttype : 'number'
											},{
												name : 'description',
												index : 'description',
												align : 'center',
												width : col9width,
												editable : true
											}, {
												name : 'action',
												index : 'action',
												width : 90,
												editable : false,
												formatter : showActionButtons,
												search : false
											} ],
											height : 'auto',
											viewrecords : true,
											caption : "Packaging Type Detail",
											gridview : true,
											loadui : 'block',
											loadonce : true,
											pager : '#packagingTypeDetail_pager',
											rowNum : 20,
											sortorder : "asc",
											shrinkToFit : true,
											rowList : [ 20, 40, 60, 100 ],
											ignoreCase : true,
											jsonReader : {
												root : "rows",
												page : "page",
												total : "total",
												records : "records",
												repeatitems : false,

												cell : "cell",
												id : "id"
											},

										});

								$("#packagingTypeDetail").jqGrid('navGrid',
										'#packagingTypeDetail_pager', {
											edit : false,
											add : false,
											del : false,
											search : false,
											refresh : false
										}, {}, {}, {}, {});

								var searchUrl = '${path.http}/admin/packmangui/${storeCode}/getPackagingTypeItemInfo/';
								$("#packagingTypeDetail").jqGrid('setGridParam', {
									url : searchUrl,
									datatype : 'json',
									postData : {storeCode:'${storeCode}',packagingTypeCode:'${packagingTypeCode}'}	
								}).trigger('reloadGrid');
								
								 if(${show}==false){
								$("#packagingTypeDetail").jqGrid('hideCol', 'length');
								$("#packagingTypeDetail").jqGrid('hideCol', 'breadth');
								$("#packagingTypeDetail").jqGrid('hideCol', 'height');
								$("#packagingTypeDetail").jqGrid('hideCol', 'action');
								$("#packagingTypeDetail").jqGrid('hideCol', 'volWeight');
								$("#packagingTypeDetail").jqGrid('hideCol', 'slab');
								} 
								 
								 if('${jsonvalue}'.indexOf("admin/packmangui/${storeCode}/createpackagingtypeitem/")==-1){
										$("#packagingTypeDetail").jqGrid('hideCol', 'action');
										}

							});
		</script>

	</tiles:putAttribute>
	<tiles:putAttribute name="subheader">
		<tiles:putAttribute name="systemMessage">
		</tiles:putAttribute>
		<tiles:insertTemplate template="/views/admin/packman/sidebar.jsp">
			<tiles:putAttribute name="active" value="packman" />
		</tiles:insertTemplate>
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
		<div class="main-content_packman lfloat">
			<div class="container subheaderDiv" style="width: 100%;">
				<div id="packagingTypeUpdateDiv">
					<table id="packagingTypeDetail"></table>
					<div id="packagingTypeDetail_pager"></div>

					<br> <br> <br>
				</div>
			</div>
			<c:if test="${show==false}">
			<h5 style="margin-left:30px">Note : To edit PickNPack packaging type item go to <u>packaging type</u> in left sidebar</h5>
			</c:if>
		</div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
	<%-- Activate relevant sidebar function   --%>
	<%-- Activate relevant subheader function --%>
		<script type="text/javascript">

		function hideSidebar(){
		    $(".subheader").css({"left":"-11%"});
		    $(".functionAnchor").css({"display": "none"});
		    $(".sidebar").css({"left":"1%","width":"99%"});
		    $(".main-content").css({"left":"1%","width":"98%"});
		}
		function showSidebar(){
		    $(".subheader").css({"left":"0%"});
		    $(".functionAnchor").css({"display": "block"});
		    $(".sidebar").css({"left":"12%","width":"88%"});
		    $(".main-content").css({"left":"12%","width":"87%"});
		}
				$('#sidebar-fn-viewpackagingtype').addClass('active');
				$('#header-nav-packman').addClass('active');
			
		</script>
			
		
	</tiles:putAttribute>
</tiles:insertTemplate>
