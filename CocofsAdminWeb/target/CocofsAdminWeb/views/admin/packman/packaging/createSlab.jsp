<%@ include file="/tld_includes.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<tiles:insertTemplate template="/views/layout/base.jsp">
	<tiles:putAttribute name="title" value="Create Slab" />

	 <tiles:putAttribute name="systemMessage">
		<div class="system-message"></div>
	</tiles:putAttribute>
	 
	<tiles:putAttribute name="sidebar">
		<tiles:insertTemplate template="/views/admin/packman/packaging/slab-subheader.jsp">
			<tiles:putAttribute name="active" value="createslab" />
 		</tiles:insertTemplate>
 	</tiles:putAttribute>
	
	<tiles:putAttribute name="extraScripts">
<link href="${path.css('chosen/chosen.css')}" rel="stylesheet"
    type="text/css" />
    <link rel="stylesheet" type="text/css"
            href="${path.css('snapdeal/ui.jqgrid.css')}" />

    <script type="text/javascript"
    src="${path.js('chosen/chosen.jquery.min.js')}"></script>
    <script type="text/javascript"
    src="${path.js('snapdeal/validate.js')}"></script>
	</tiles:putAttribute>
	<tiles:putAttribute name="systemMessage">
		<div class="system-message">
			<input type="hidden" value="${message }" id="messageHiddenField"/>
		</div>
		<div class="system-message">
			<input type="hidden" value="${validationResponse}" id="validationResponseHiddenField"/>
		</div>
	</tiles:putAttribute>


	<tiles:putAttribute name="subheader">
	<tiles:putAttribute name="systemMessage">		</tiles:putAttribute>
		<tiles:insertTemplate template="/views/admin/packman/sidebar.jsp">
			<tiles:putAttribute name="active" value="packman" />
		</tiles:insertTemplate>
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
		<div class="main-content_packman lfloat">
			<div class="container" style="width: 85%;">
			
			<form id="slab-form" class="form-horizontal" method="post" action="">
						  <input type="text" style="display:none" name="pageId" value="${pageId}">
			
			  <div class="control-group">
			    <label class="control-label" for="store-code">Store</label>
			    <div class="controls">
				    	<input type="text" id="store-code" name="storeCode" value="${storeCode}" validation="NA" readonly="readonly" >
			    </div>
			  </div>
			  <div class="control-group">
			    <label class="control-label" for="packaging-type-code">Packaging Type Code</label>
			    <div class="controls">
				    	<input type="text" id="packaging-type-code" name="packagingTypeCode" value="${packagingTypeCode}" validation="NA" readonly="readonly" required="required">
			    </div>
			  </div>
			  <div class="control-group">
			    <label class="control-label" for="slab-size">Slab Size (in gms)</label>
			    <div class="controls">
			      <input type="text" id="slab-size" name="slabSize" validation="Integer" validationname="Slab Size" validationenabled="true" placeholder="Size">
			    </div>
			  </div>
			  <div class="control-group">
			    <div class="controls">
			      <button id="upload-form-btn" type="submit" class="btn btn-primary">Update</button>
			    </div>
			  </div>
			</form>
			
			<c:if test="${message != null && message!=\"\"}">
			<div class="alert alert-info">
                <button type="button" class="close" data-dismiss="alert">×
                </button>
                ${message}
                <br>
			</div>
			</c:if>
						
			</div>
		</div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
	<%-- Activate relevant sidebar function   --%>
	<%-- Activate relevant subheader function --%>
		<script type="text/javascript">
		$('input[id=inputfile]').change(function() {
			$('#filebrowse').val($(this).val().replace(/C:\\fakepath\\/i, ''));
			});

		function hideSidebar(){
		    $(".subheader").css({"left":"-11%"});
		    $(".functionAnchor").css({"display": "none"});
		    $(".sidebar").css({"left":"1%","width":"99%"});
		    $(".main-content").css({"left":"1%","width":"98%"});
		}
		function showSidebar(){
		    $(".subheader").css({"left":"0%"});
		    $(".functionAnchor").css({"display": "block"});
		    $(".sidebar").css({"left":"12%","width":"88%"});
		    $(".main-content").css({"left":"12%","width":"87%"});
		}
		
			$(document).ready(function() {
				 
				$('#header-nav-packman').addClass('active');
				
				function isValid(str) { return /^[0-9]+$/i.test(str); }
				
				$('#upload-form-btn').click(function() {
					if(validate()){
						$("#slab-form").attr('action','${path.http}/admin/packmangui/${storeCode}/saveslab/');
						$('#slab-form').submit();
						return true;
					}
					return false;
				} );
				
			});
			
			markRequired();
			
			
		</script>
			
		
	</tiles:putAttribute>
</tiles:insertTemplate>
