<%@ include file="/tld_includes.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<tiles:insertTemplate template="/views/layout/base.jsp">
	<tiles:putAttribute name="title" value="Create Packaging Type" />

	<tiles:putAttribute name="systemMessage">
		<div class="system-message"></div>
	</tiles:putAttribute>


	<tiles:putAttribute name="sidebar">
		<tiles:insertTemplate
			template="/views/admin/packman/packaging/packagingtype-subheader.jsp">
			<tiles:putAttribute name="active" value="createpackagingtype" />
		</tiles:insertTemplate>
	</tiles:putAttribute>

	<tiles:putAttribute name="extraScripts">
		<link href="${path.css('chosen/chosen.css')}" rel="stylesheet"
			type="text/css" />
		<link rel="stylesheet" type="text/css"
			href="${path.css('snapdeal/ui.jqgrid.css')}" />

		<script type="text/javascript"
			src="${path.js('chosen/chosen.jquery.min.js')}"></script>

		<script type="text/javascript"
			src="${path.js('snapdeal/validate.js')}"></script>

	</tiles:putAttribute>
	<tiles:putAttribute name="systemMessage">
		<div class="system-message">
			<input type="hidden" value="${message }" id="messageHiddenField" />
		</div>
		<div class="system-message">
			<input type="hidden" value="${validationResponse}"
				id="validationResponseHiddenField" />
		</div>
	</tiles:putAttribute>

	<tiles:putAttribute name="subheader">
		<tiles:putAttribute name="systemMessage">
		</tiles:putAttribute>
		<tiles:insertTemplate template="/views/admin/packman/sidebar.jsp">
			<tiles:putAttribute name="active" value="packman" />
		</tiles:insertTemplate>
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
		<div class="main-content_packman lfloat">
			<div class="container" style="width: 85%;">

				<form id="packaging-type-form" class="form-horizontal" method="post"
					action="">

					<input type="text" style="display: none" name="pageId"
						value="${pageId}"> <input type="text"
						style="display: none" name="id" value="${packagingTypeForm.id}">

					<div class="control-group">
						<label class="control-label" for="store-code">Store Code</label>
						<div class="controls">
							<input type="text" id="store-code" name="storeCode"
								validation="AlphaNumericNoSpecialCharSpace" required="required"
								value="${storeCode}" readonly="readonly">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="shipping-mode">Packaging
							Mode</label>
						<div class="controls" id="shipping-mode"
							validation="AlphaNumericNoSpecialCharSpace">
							<select name="packagingMode" id="packagingMode"
								required="required"
								<c:if test="${packagingTypeForm.id != 0}">
				    disabled
				    </c:if>>
								<c:forEach var="shippingType" items="${shippingTypes}">
									<option value="${shippingType}"
										<c:if test="${packagingTypeForm.packagingMode == shippingType}"> selected </c:if>>${shippingType}</option>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="packaging-type">Packaging
							Type Code</label>
						<div class="controls">
							<input type="text" id="packaging-type" name="packagingTypeCode"
								value="${packagingTypeForm.packagingTypeCode}"
								validation="AlphaNumericNoSpecialCharSpace" validationminlen="5"
								validationmaxlen="48" validationname="Packaging Type Code"
								validationenabled="true" placeholder="Code"
								<c:if test="${packagingTypeForm.id != 0}">
				    disabled
				    </c:if>>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="packaging-type-description">Description</label>
						<div class="controls">
							<textarea type="text" rows="3" id="packaging-type-description"
								name="packagingDescription" placeholder="Description">${packagingTypeForm.packagingDescription}</textarea>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="check-enabled">Enabled</label>

						<div class="controls">
							<input type="checkbox" name="enabled" value="true"
								<c:choose>
			      	 <c:when test="${packagingTypeForm.enabled==true}">
			      	 	checked
			      	 </c:when>
			      	 </c:choose> />
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="VALIDATION_ENABLED">Enable
							Validation<font size="3" color="red">*</font>
						</label>
						<div class="controls">
							<select name="VALIDATION_ENABLED" id="VALIDATION_ENABLED">
								<option value="true"
									<c:if test="${packagingTypeForm.VALIDATION_ENABLED == true}"> selected </c:if>>True</option>
								<option value="false"
									<c:if test="${packagingTypeForm.VALIDATION_ENABLED == false}"> selected </c:if>>False</option>
							</select>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="PACKAGING_TYPE_PREFIX">Packaging
							Type Prefix</label>
						<div class="controls">
							<input type="text" id="PACKAGING_TYPE_PREFIX"
								name="PACKAGING_TYPE_PREFIX"
								value="${packagingTypeForm.PACKAGING_TYPE_PREFIX}"
								class="AlphaNumeric" validation="AlphaNumericNoSpecialCharSpace"
								validationminlen="2" validationmaxlen="10"
								validationname="Packaging Type Prefix" validationenabled="true"
								placeholder="eg. CB">
						</div>
					</div>
					<div id="optionalProperty" style="display: none;">
						<div class="control-group">
							<label class="control-label" for="SLAB_PREFIX">Slab
								Prefix</label>
							<div class="controls">
								<input type="text" id="SLAB_PREFIX" name="SLAB_PREFIX"
									value="${packagingTypeForm.SLAB_PREFIX}" class="String"
									validation="String" validationminlen="2" validationmaxlen="10"
									validationname="Slab Prefix" validationenabled="true"
									placeholder="eg. SLAB">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="SLAB_SIZE">Slab Size (gm)</label>
							<div class="controls">
								<input type="text" id="SLAB_SIZE" name="SLAB_SIZE"
									value="${packagingTypeForm.SLAB_SIZE}" class="Integer"
									validation="Integer" minValue="100" maxValue="5000"
									validationname="Slab Size" validationenabled="true"
									placeholder="eg. 200">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="MIN_SLAB_SIZE">Minimum
								Slab Size (gm)</label>
							<div class="controls">
								<input type="text" id="MIN_SLAB_SIZE" name="MIN_SLAB_SIZE"
									value="${packagingTypeForm.MIN_SLAB_SIZE}" class="Integer"
									validation="Integer" minValue="100" maxValue="5000"
									validationname="Slab Size" validationenabled="true"
									placeholder="eg. 100">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="MAX_SLAB_SIZE">Maximum
								Slab Size (gm)</label>
							<div class="controls">
								<input type="text" id="MAX_SLAB_SIZE" name="MAX_SLAB_SIZE"
									value="${packagingTypeForm.MAX_SLAB_SIZE}" class="Integer"
									validation="Integer" minValue="100" maxValue="5000"
									validationname="Slab Size" validationenabled="true"
									placeholder="eg. 5000">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="MAX_VOL_WEIGHT">Maximum
								Volumetric Weight (gm)</label>
							<div class="controls">
								<input type="text" id="MAX_VOL_WEIGHT" name="MAX_VOL_WEIGHT"
									value="${packagingTypeForm.MAX_VOL_WEIGHT}" class="Double"
									validation="Double" minValue="100" maxValue="30000"
									validationname="Volumetric Weight" validationenabled="true"
									placeholder="eg. 30000">
							</div>
						</div>
					</div>
					<div class="control-group">
						<div class="controls">
							<button id="upload-form-btn" type="submit"
								class="btn btn-primary">Save</button>
						</div>
					</div>
				</form>

				<c:if test="${message != null && message!=\"\"}">
					<div class="alert alert-info">
						<button type="button" class="close" data-dismiss="alert">
						</button>
						${message} <br>
					</div>
				</c:if>

			</div>
		</div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
		<%-- Activate relevant sidebar function   --%>
		<%-- Activate relevant subheader function --%>
		<script type="text/javascript">
			$('input[id=inputfile]').change(
					function() {
						$('#filebrowse').val(
								$(this).val().replace(/C:\\fakepath\\/i, ''));
					});

			function hideSidebar() {
				$(".subheader").css({
					"left" : "-11%"
				});
				$(".functionAnchor").css({
					"display" : "none"
				});
				$(".sidebar").css({
					"left" : "1%",
					"width" : "99%"
				});
				$(".main-content").css({
					"left" : "1%",
					"width" : "98%"
				});
			}
			function showSidebar() {
				$(".subheader").css({
					"left" : "0%"
				});
				$(".functionAnchor").css({
					"display" : "block"
				});
				$(".sidebar").css({
					"left" : "12%",
					"width" : "88%"
				});
				$(".main-content").css({
					"left" : "12%",
					"width" : "87%"
				});
			}

			$(document)
					.ready(
							function() {
								var subheader = true;
								$(".subheaderToggle").click(function() {
									if (subheader) {
										hideSidebar();
										$(".subheaderToggleContent").html(">");
										subheader = false;
									} else {
										showSidebar();
										$(".subheaderToggleContent").html("<");
										subheader = true;
									}

								});

								$('.chosen-select').chosen();
								$('#sidebar-fn-createpackagingtype ').addClass(
										'active');
								$('#header-nav-packman').addClass('active');
								function validateExtra() {
									var slab_size = parseInt($("#SLAB_SIZE")
											.val());
									var min_slab_size = parseInt($(
											"#MIN_SLAB_SIZE").val());
									var max_slab_size = parseInt($(
											"#MAX_SLAB_SIZE").val());
									var max_vol_wt = parseInt($(
											"#MAX_VOL_WEIGHT").val());
									if (min_slab_size > max_slab_size) {
										$("#MIN_SLAB_SIZE")
												.after(
														'<div class="error">Minimum slab size must be less than maximum slab size.</div>');
										return false;
									}
									if (slab_size < min_slab_size) {
										$("#SLAB_SIZE")
												.after(
														'<div class="error">Default slab size must be greater than minimum slab size.</div>');
										return false;
									}
									if (slab_size > max_slab_size) {
										$("#SLAB_SIZE")
												.after(
														'<div class="error">Default slab size must be less than maximum slab size.</div>');
										return false;
									}
									if (max_slab_size > max_vol_wt) {
										$("#MAX_VOL_WEIGHT")
												.after(
														'<div class="error">Maximum Volumetric weight must be greater than maximum slab size.</div>');
										return false;
									}
									return true;
								}

								$('#upload-form-btn')
										.click(
												function() {
													if (validate()
															&& validateExtra()) {
														$("#packaging-type")
																.attr(
																		'disabled',
																		false);
														$("#packagingMode")
																.attr(
																		'disabled',
																		false);
														$(
																"#packaging-type-form")
																.attr('action',
																		'${path.http}/admin/packmangui/${storeCode}/savepackagingtype/');
														$(
																'#packaging-type-form')
																.submit();
														return true;
													}
													return false;
												});

								markRequired();

								function setUpForm() {
									res = false;
									if ($("#packagingMode").val() == "Normal") {
										$("#optionalProperty").show();
										res = true;
									} else {
										$("#optionalProperty").hide();
									}
									elements = $("#optionalProperty .control-group .controls [validationenabled]");
									for (i = 0; i < elements.length; i++) {
										element = elements[i];
										$(element).attr('validationenabled',
												res);
									}
								}

								setUpForm();
								$("#packagingMode").change(function() {
									setUpForm();
								});

							});
		</script>


	</tiles:putAttribute>
</tiles:insertTemplate>
