<%@ include file="/tld_includes.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<tiles:insertTemplate template="/views/layout/base.jsp">
	<tiles:putAttribute name="title" value="Create Packaging Type Items" />

	<tiles:putAttribute name="systemMessage">
		<div class="system-message"></div>
	</tiles:putAttribute>
	
	<tiles:putAttribute name="sidebar">
		<tiles:insertTemplate template="/views/admin/packman/packaging/packagingtypeitem-subheader.jsp">
			<tiles:putAttribute name="active" value="createpackagingtypeitem" />
		</tiles:insertTemplate>
	</tiles:putAttribute>

	<tiles:putAttribute name="extraScripts">
<link href="${path.css('chosen/chosen.css')}" rel="stylesheet"
    type="text/css" />
    <link rel="stylesheet" type="text/css"
            href="${path.css('snapdeal/ui.jqgrid.css')}" />

    <script type="text/javascript"
    src="${path.js('chosen/chosen.jquery.min.js')}"></script>
    <script type="text/javascript"
    src="${path.js('snapdeal/validate.js')}"></script>
	</tiles:putAttribute>
	<tiles:putAttribute name="systemMessage">
		<div class="system-message">
			<input type="hidden" value="${message }" id="messageHiddenField"/>
		</div>
		<div class="system-message">
			<input type="hidden" value="${validationResponse}" id="validationResponseHiddenField"/>
		</div>
	</tiles:putAttribute>


	<tiles:putAttribute name="subheader">
		<tiles:putAttribute name="systemMessage">
		</tiles:putAttribute>
		<tiles:insertTemplate template="/views/admin/packman/sidebar.jsp">
			<tiles:putAttribute name="active" value="packman" />
		</tiles:insertTemplate>
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
		<div class="main-content_packman lfloat">
			<div class="container" style="width: 85%;">
				
			<form id="packaging-type-form" class="form-horizontal" method="post" action="">
			  <input type="text" style="display:none" name="pageId" value="${pageId}">
			  <div class="control-group">
			    <label class="control-label" for="store-code">Store</label>
			    <div class="controls">
				    	<input type="text" id="store-code" name="storeCode" value="${storeCode}" readonly="readonly" validation="AlphaNumeric" validationminlen="2" validationmaxlen="10" validationname="Store Code" validationenabled="true">
			    </div>
			  </div>
			  <div class="control-group">
			    <label class="control-label" for="shipping-mode">Packaging Mode</label>
			    <div class="controls">
				    	<input type="text" id="shipping-mode" name="shippingMode" value="${shippingMode}" readonly="readonly" validation="AlphaNumericNoSpecialCharSpace" validationminlen="1" validationmaxlen="48" validationname="Shipping Mode" validationenabled="true">
			    </div>
			  </div>
			  <div class="control-group">
			    <label class="control-label" for="packaging-type-code">Packaging Type Code</label>
			    <div class="controls">
				    	<input type="text" id="packaging-type-code" name="packagingTypeCode" value="${packagingTypeCode}" readonly="readonly" validation="AlphaNumericNoSpecialCharSpace" validationminlen="5" validationmaxlen="48" validationname="Packaging Type Code" validationenabled="true">
			    </div>
			  </div>
			  <div class="control-group">
			    <label class="control-label" for="packaging-item-type-code">Packaging Type Item Code</label>
			    <div class="controls">
				    	<input type="text" id="packaging-item-type-code" name="packagingTypeItemCode" value="${packagingTypeItemForm.packagingTypeItemCode}" placeholder="Code" validation="AlphaNumericNoSpecialCharSpace" validationminlen="2" validationmaxlen="13" validationname="Packaging Type Item Code"  validationenabled="true">
			    </div>
			  </div>
			  <div class="control-group">
			    <label class="control-label" for="packaging-type-description">Description</label>
			    <div class="controls">
			      <textarea type="text" rows="3" id="packaging-type-description" name="description" placeholder="Description">${packagingTypeItemForm.description}</textarea>
			    </div>
			  </div>
			  <div id="lbh" style="display: none;">
			  <div class="control-group">
			    <label class="control-label" for="length">Length (in cm)</label>
			    <div class="controls">
				    	<input type="text" id="length" name="length" value="${packagingTypeItemForm.length}" placeholder="Length" validation="Decimal" validationminlen="1" validationmaxlen="10" validationname="Length" validationZeroAllowed="false" validationenabled="${show}">
			    </div>
			  </div>
			  <div class="control-group">
			    <label class="control-label" for="breadth">Breadth (in cm)</label>
			    <div class="controls">
				    	<input type="text" id="breadth" name="breadth" value="${packagingTypeItemForm.breadth}" placeholder="Breadth" validation="Decimal" validationminlen="1" validationmaxlen="10" validationname="Breadth" validationZeroAllowed="false" validationenabled="${show}">
			    </div>
			  </div>
			  <div class="control-group">
			    <label class="control-label" for="height">Height (in cm)</label>
			    <div class="controls">
				    	<input type="text" id="height" name="height" placeholder="Height" value="${packagingTypeItemForm.height}" validation="Decimal" validationminlen="1" validationmaxlen="10" validationname="Height" validationZeroAllowed="false" validationenabled="${show}">
			    </div>
			  </div>
			  </div>
			  <div class="control-group">
			    <div class="controls">
			      <button id="upload-form-btn" type="submit" class="btn btn-primary">Save</button>
			    </div>
			  </div>
			</form>
			
			<c:if test="${message != null && message!=\"\"}">
			<div class="alert alert-info">
                <button type="button" class="close" data-dismiss="alert">×
                </button>
                ${message}
                <br>
			</div>
			</c:if>
						
			</div>
		</div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
	<%-- Activate relevant sidebar function   --%>
	<%-- Activate relevant subheader function --%>
		<script type="text/javascript">
		$('input[id=inputfile]').change(function() {
			$('#filebrowse').val($(this).val().replace(/C:\\fakepath\\/i, ''));
			});

		function hideSidebar(){
		    $(".subheader").css({"left":"-11%"});
		    $(".functionAnchor").css({"display": "none"});
		    $(".sidebar").css({"left":"1%","width":"99%"});
		    $(".main-content").css({"left":"1%","width":"98%"});
		}
		function showSidebar(){
		    $(".subheader").css({"left":"0%"});
		    $(".functionAnchor").css({"display": "block"});
		    $(".sidebar").css({"left":"12%","width":"88%"});
		    $(".main-content").css({"left":"12%","width":"87%"});
		}
		
			$(document).ready(function() {
				var subheader=true;
				 $(".subheaderToggle").click(function(){
				         if(subheader){
				                 hideSidebar();
				                 $(".subheaderToggleContent").html(">");
				                 subheader=false;
				         }else{
				                 showSidebar();
				                 $(".subheaderToggleContent").html("<");
				                 subheader=true;
				         }

				 });
				 
				 function setUpForm(){
					 
					 	if(${packagingTypeItemForm.length} == 0.0){
					 		$("#length").prop('value', '');
							$("#breadth").prop('value', '');
							$("#height").prop('value', '');
					 	}
					 
						if(${show}){
							$("#lbh").show();
							$("#length").prop('validationenabled', true);
							$("#breadth").prop('validationenabled', true);
							$("#height").prop('validationenabled', true);
						}
						else{
							$("#length").prop('validationenabled', false);
							$("#breadth").prop('validationenabled', false);
							$("#height").prop('validationenabled', false);
							$("#length").prop('disabled', true);
							$("#breadth").prop('disabled', true);
							$("#height").prop('disabled', true);
							$("#lbh").hide();
						}
					}
				
				 setUpForm();
				 
				$('.chosen-select').chosen();
				$('#sidebar-fn-createpackagingtype ').addClass('active');
				$('#header-nav-packman').addClass('active');
				
				function isValid(str) { return /^[a-z0-9_ \-]+$/i.test(str); }
				
				$('#upload-form-btn').click(function() {
					if(validate()){
						$("#packaging-type-form").attr('action','${path.http}/admin/packmangui/${storeCode}/savepackagingtypeitem/');
						$('#packaging-type-form').submit();
						return true;
					}
					return false;
				} );
				
			});
			
			markRequired();
			
		</script>
			
		
	</tiles:putAttribute>
</tiles:insertTemplate>
