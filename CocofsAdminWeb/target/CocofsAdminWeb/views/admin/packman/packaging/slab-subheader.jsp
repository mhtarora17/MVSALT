<%@ include file="/tld_includes.jsp"%>

<c:set var="active">
	<tiles:getAsString name="active" />
</c:set>

<div class="sidebar lfloat">
	<div id="functions">
		<div class="sidebarUL">
			<c:set var="url" value="${storeCode}/viewslab/" />
			<c:if test="${fn:contains(jsonvalue,url)}">
				<c:choose>
					<c:when test="${active =='viewslab'}">
						<div class="activeFunction functionAnchor_packman" onclick=post('/admin/packmangui/${storeCode}/viewslab/',{storeCode:'${storeCode}',packagingTypeCode:'${packagingTypeCode}',pageId:'${pageId}'},'post')>View
							Slab</div>
					</c:when>
					<c:otherwise>
						<div class="functionAnchor_packman" onclick=post('/admin/packmangui/${storeCode}/viewslab/',{storeCode:'${storeCode}',packagingTypeCode:'${packagingTypeCode}',pageId:'${pageId}'},'post')>View
							Slab</div>
					</c:otherwise>
				</c:choose>
			</c:if>

			<c:set var="url" value="${storeCode}/createslab/" />
			<c:if test="${fn:contains(jsonvalue,url)}">
				<c:choose>
					<c:when test="${active =='createslab'}">
						<div class="activeFunction functionAnchor_packman" onclick=post('/admin/packmangui/${storeCode}/createslab/',{storeCode:'${storeCode}',packagingTypeCode:'${packagingTypeCode}',pageId:'${pageId}'},'post')>Create/Update
							Slab</div>
					</c:when>
					<c:otherwise>
						<div class="functionAnchor_packman" onclick=post('/admin/packmangui/${storeCode}/createslab/',{storeCode:'${storeCode}',packagingTypeCode:'${packagingTypeCode}',pageId:'${pageId}'},'post')>Create/Update
							Slab</div>
					</c:otherwise>
				</c:choose>
			</c:if>
			<script type="text/javascript" src="${path.js('snapdeal/post.js')}"></script>

		</div>

	</div>
</div>