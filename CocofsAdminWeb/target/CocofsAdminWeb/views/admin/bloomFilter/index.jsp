	<%@ include file="/tld_includes.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<tiles:insertTemplate template="/views/layout/base.jsp">
	<tiles:putAttribute name="title" value="Supc Bloom Filter Recalculation Panel" />

	<tiles:putAttribute name="systemMessage">
		<div class="system-message"></div>
	</tiles:putAttribute>

	<tiles:putAttribute name="extraScripts">
    <link href="${path.css('chosen/chosen.css')}" rel="stylesheet"
    type="text/css" />
    <link rel="stylesheet" type="text/css"
            href="${path.css('snapdeal/ui.jqgrid.css')}" />

    <script type="text/javascript"
    src="${path.js('chosen/chosen.jquery.min.js')}"></script>
    


	</tiles:putAttribute>
	<tiles:putAttribute name="sidebar">
		<tiles:insertTemplate template="/views/admin/admintasks/sidebar.jsp">
		</tiles:insertTemplate>
	</tiles:putAttribute>


	<tiles:putAttribute name="subheader">
		<tiles:insertTemplate template="/views/admin/admintasks/cacheview-subheader.jsp">
		</tiles:insertTemplate>
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
		<div class="main-content lfloat">
			<div class="container" style="width: 85%;">
				<form action = "" id="file-upload-form" enctype="multipart/form-data" method="post" class="form-horizontal">
					<fieldset>
						<legend>Supc Bloom Filter Sellers File Upload</legend>
 						
			
						<div class="control-group">
			<label class="control-label" for="selected-job-status"> Upload Type </label>
                        			
			<div class="controls">
			<select name="enablement" id="enablement_status" class="chosen-select">
						<option value="2">Select an Option</option>						
						<option value="1">Single Seller</option>
						<option value="0">Bulk Upload</option>
			</select> 
			</div>
			</div>
                        <div id="upload_panel">
<div class="control-group">
			<label class="control-label" for="inputfile">File To Upload </label>
                        <div class="controls">
                        <input id="inputfile" style="display:none;" name="datafile" type="file"> 
                        <div class="input-append">
                        <input id="filebrowse" class="input" type="text" readonly>
                        <a class="btn btn-info" onclick="$('input[id=inputfile]').click();">Browse</a>
                        </div>

						</div>
						</div>
						
						<%-- <span class="help-block">Please upload the file</span>  --%>
						<div class="control-group">
                        <div class="controls">
                        <button id="file-upload-form-btn" type="Upload" class="btn btn-primary">Submit</button>
                        </div>
						</div>
						</div>
						<div id="single_seller" style="display: block;">
							<div class="control-group">
								<label class="control-label">Seller Code </label>
			                        <div class="controls">
			                        <input type="text" name="sc" id="sc" placeholder="Seller Code">
			                        </div>

							</div>
						
						
						<div class="control-group">
                        <div class="controls">
                        <button id="file-upload-form-btn1" type="Upload" class="btn btn-primary">Submit</button>
                        </div>
						</div>
						
					</div>

					</fieldset>
				</form>
                <div>
                <a id="bulkUploadTemplate" href="${path.http}/admin/bloomFilterConsole/downloadTemplate"> Download Bloom Filter Bulk Upload Template</a>
                </div>
			</div>
                <c:if test="${message != null}">
                <div class="alert alert-info">
                <button type="button" class="close" data-dismiss="alert">&times;
                </button>
                ${message }
                <br/>
                <c:if test="${validationResponse != null}">
                ${validationResponse}
                </c:if>
		</div>
                </c:if>

			</div>
		</div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
	<%-- Activate relevant sidebar function   --%>
	<%-- Activate relevant subheader function --%>
		<script type="text/javascript">
		$('input[id=inputfile]').change(function() {
			$('#filebrowse').val($(this).val().replace(/C:\\fakepath\\/i, ''));
			});

		function hideSidebar(){
		    $(".subheader").css({"left":"-11%"});
		    $(".functionAnchor").css({"display": "none"});
		    $(".sidebar").css({"left":"1%","width":"99%"});
		    $(".main-content").css({"left":"1%","width":"98%"});
		}
		function showSidebar(){
		    $(".subheader").css({"left":"0%"});
		    $(".functionAnchor").css({"display": "block"});
		    $(".sidebar").css({"left":"12%","width":"88%"});
		    $(".main-content").css({"left":"12%","width":"87%"});
		}
		function setUpForm(){
								
			if($("#enablement_status").val() == 2){
				$("#upload_panel").hide();
				$("#single_seller").hide();
				$('#bulkUploadTemplate').hide();
				$("#file-upload-form").attr('action','');
			} else if($("#enablement_status").val() == 1){
				$("#upload_panel").hide();
				$("#single_seller").show();
				$('#bulkUploadTemplate').hide();
				$("#file-upload-form").attr('action','${path.http}/admin/bloomFilterConsole/Recalculate');
				$("#file-upload-form").attr('method','post');
			} else if($("#enablement_status").val() == 0){
				$("#upload_panel").show();
				$("#single_seller").hide();	
				$('#bulkUploadTemplate').show();
				$("#file-upload-form").attr('action','${path.http}/admin/bloomFilterConsole/uploadData');
				$("#file-upload-form").attr('method','post');
			}
			
		}
		
			$(document).ready(function() {
				var subheader=true;
				 $(".subheaderToggle").click(function(){
				         if(subheader){
				                 hideSidebar();
				                 $(".subheaderToggleContent").html(">");
				                 subheader=false;
				         }else{
				                 showSidebar();
				                 $(".subheaderToggleContent").html("<");
				                 subheader=true;
				         }

				 });
				$('.chosen-select').chosen();
				/* check sidebar jsp for relevant div ids
				*/
				$('#sidebar-fn-bloomfilter').addClass('active');
				$('#header-nav-admintasks').addClass('active');
				
				/* check subheader jsp for relevant div ids
				*/
				/* $('#subheader-fn-usrmanagement').addClass('active') */
				
				
				$('#file-upload-form-btn').click(function() {
					if ($("#inputfile:input").length) {
						if (! $('#inputfile').val().trim().match(/\.xls$/) ) {
                            alert("Only files with extension .xls can be uploaded ");
                            return false;
                        }
						if ($("#inputfile").val() != '') {
							$('#file-upload-form').submit();
						} else {
							alert("Please upload a file");
							return false;
						}
					} else {
						alert("class uploadJobFile not found");
						return false;
					}
				} );
				
				function isValid(str) { return /^\w+$/.test(str); }
				
				
				$('#file-upload-form-btn1').click(function() {
					sc = $('#sc').val();
					if(sc!=null && isValid(sc)){
						$('#file-upload-form').submit();
						return true;
					}
					if(sc==null || sc==""){
						alert('Please enter seller code');
					} else {
						alert('seller code can only have numbers or characters in it.');
					}
					return false;
				} );
				

				$("#upload_panel").hide();
				$("#single_seller").hide();
				setUpForm();				
				$("#enablement_status").change(function(){setUpForm();});



			});

		</script>
			
		
	</tiles:putAttribute>
</tiles:insertTemplate>

