<%@ include file="/tld_includes.jsp"%>

<%@ page session="true"%>

<tiles:insertTemplate template="/views/layout/loginBase.jsp">
    
	<tiles:putAttribute name="title" value="CoCoFS System Login"/>
	
	<tiles:putAttribute name="deferredScript">
	
		<script>
		
		$(document).ready(function() {
			$(".chzn-select").chosen();
	 		$("#forgotPasswordEmailSubmit").click(function(){
	 			$('#forgotPasswordSuccessMessage').html('');
	 			$('#forgotPasswordErrorMessage').html('');
	 			var ajaxUrl = "/forgotPassword?";
	 			
	 			ajaxUrl += "email=" + $("#forgot_email_buy_page").val();
	 			
	 			$.ajax({url:ajaxUrl, type: "POST", dataType: 'json', success: function(data) {
	 				if(data['status']=='success'){
	 					$('#forgotPasswordSuccessMessage').html('Please check email to reset your password.');	 					
	 				}else{
	 					$('#forgotPasswordErrorMessage').html('Not able reset your password, contact sysadmin.');
	 				}
	 			}});
	 		});

	 		$("#forgotPasswordLink").click(function(){
				$("#forgotPasswordEmaildiv").show();		 			
	 		});

			});
		</script>
	</tiles:putAttribute>
		
   <tiles:putAttribute name="body">
	      <c:set var="loginPath" value="${path.http}/login_security_check?"/>
	      <c:if test="${not empty signupForm.targetUrl}">
	      	<c:set var="loginPath" value="${path.http}/login_security_check?spring-security-redirect=${signupForm.targetUrl}&"/>
	      </c:if>
   		  <c:set var="encodedLoginPath" value="${path.getEncodedUrl(loginPath)}"/>
		<div class="signupBoxDiv">
			<c:if test="${passwordUpdated}">
				<div id="pwdUpdatedMessage" style="color : green">Password updated successfully, try login with new passowrd. </div>
			</c:if>
            
			<div id="prevSource" style="display:none">You have signed up thru <span id="pSource"></span>. Please click the same icon to login again. </div>
			<div class="register_head">CoCoFS System login</div>
			<form id="loginform" method="post" action="${loginPath}">
                <c:if test="${null != errorMessage}">
                <div id="loginFailedMessage" style="color : red; padding-top: 15px; " align="center"> ${errorMessage } </div>
                </c:if>
				<div class="loginformCont">
					<div class="cus_info_wrap">
						<label class="labelTop" for="email">E-mail *</label>
						<input class="input" type="text" name="j_username" id="j_username">
					</div>
					<div class="cus_info_wrap">
					<label class="labelTop" for="password">Password *</label>
					<input class="input" type="password" name="j_password">
					</div>
					<!-- <div class="rememberme">
						<input class="checkBox" id="rememberMe" name="_spring_security_remember_me" type="checkbox">
						<span>Remember me on this computer</span> 
					</div> -->
					<div align="center">
						<input id="signInButton" value="Sign in" class="button" type="submit" />
					</div>
				</div>
				<div id="forgotPasswordLink" align="center" style="cursor: pointer;" ><a>Forgot your password?</a></div> <br/>
				<div id="forgotPasswordEmaildiv" align = "center" style="display: none;">
				      <div  id="forgotPasswordSuccessMessage" style="color: green;"></div>
				      <div  id="forgotPasswordErrorMessage" style="color: red;"></div> <br/>			     
					    	<input type="text" value="Enter email to reset it" name="email" id="forgot_email_buy_page"
			            	onblur="if(this.value=='') { this.value='Enter email to reset it'; }"
			            	onfocus="if(this.value=='Enter email to reset it') { this.value=''; }"  style="width:65%" /> <br/><br/>
							<input type="button" value="Submit" class="button" id="forgotPasswordEmailSubmit" style="width:30%" /><br/><br/>
		      </div>
				</form>
		</div>
   </tiles:putAttribute>
</tiles:insertTemplate>


