<%@ include file="/tld_includes.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<tiles:insertTemplate template="/views/layout/base.jsp">
	<tiles:putAttribute name="title" value="Product Attribute Lookups" />

	<tiles:putAttribute name="systemMessage">
		<div class="system-message"></div>
	</tiles:putAttribute>

	<tiles:putAttribute name="extraScripts">


	</tiles:putAttribute>
	
	<tiles:putAttribute name="sidebar">
		<tiles:insertTemplate template="/views/admin/lookups/lookups-sidebar.jsp">
		</tiles:insertTemplate>
	</tiles:putAttribute>
	<tiles:putAttribute name="systemMessage">
		<div class="system-message">
			<input type="hidden" value="${message }" id="messageHiddenField"/>
		</div>
	</tiles:putAttribute>


	<tiles:putAttribute name="subheader">
		<tiles:insertTemplate template="/views/admin/lookups/lookups-subheader.jsp">
		</tiles:insertTemplate>
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
		<div class="main-content lfloat">
			<div class="container" style="width: 85%;">
			 <form method="post" class="form-horizontal" id = "sellerSupcFMMappingDataForm" action="${path.http}/admin/lookups/sellerSupcFMMappingData">
					<fieldset>
						<legend>Seller Supc FM Mapping Lookup</legend>
						<div class="control-group">
						<label class="control-label"> Seller-Supc Level Exception</label>
                        <div class="controls">
                        <a href="supcExceptions">Click here</a>
                        </div>
                        </div>
                       
                        <div class="control-group">
                        <label class="control-label" for="sellerCode"> Seller to download data </label>
                        <div class="controls">
                        <input id="sellerCode" type="text" name="sellerCode"> 
                        </div>
                        </div>
                        <div class="control-group">
                        <div class="controls">
                        <button id="sellerSupcFMMappingData" type="button" class="btn btn-primary">Submit</button>
                        </div>
                        </div>
                        
					</fieldset>
					</form>
                <c:if test="${message != null}">
                <div class="alert alert-info">
                <button type="button" class="close" data-dismiss="alert">&times;
                </button>
                ${message }</div>
                </c:if>

			</div>
		</div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
	<%-- Activate relevant sidebar function   --%>
	<%-- Activate relevant subheader function --%>
		<script type="text/javascript">
		
		$('input[id=inputfile]').change(function() {
			$('#filebrowse').val($(this).val().replace(/C:\\fakepath\\/i, ''));
            });

		function hideSidebar(){
		    $(".subheader").css({"left":"-11%"});
		    $(".functionAnchor").css({"display": "none"});
		    $(".sidebar").css({"left":"1%","width":"99%"});
		    $(".main-content").css({"left":"1%","width":"98%"});
		}
		function showSidebar(){
		    $(".subheader").css({"left":"0%"});
		    $(".functionAnchor").css({"display": "block"});
		    $(".sidebar").css({"left":"12%","width":"88%"});
		    $(".main-content").css({"left":"12%","width":"87%"});
		}
		
			$(document).ready(function() {
				var subheader=true;
				 $(".subheaderToggle").click(function(){
				         if(subheader){
				                 hideSidebar();
				                 $(".subheaderToggleContent").html(">");
				                 subheader=false;
				         }else{
				                 showSidebar();
				                 $(".subheaderToggleContent").html("<");
				                 subheader=true;
				         }

				 });
				/* check sidebar jsp for relevant div ids
				*/
				$('#sidebar-fm-upload ').addClass('active');
				$('#header-nav-lookups').addClass('active');
				
				/* check subheader jsp for relevant div ids
				*/
				/* $('#subheader-fn-usrmanagement').addClass('active') */
				
			$('#sellerSupcFMMappingData').click(function(){
				if($('#sellerCode').val() == ''){
					smoke.alert("Please enter seller code");
					return ;
				}else{
					$('#sellerSupcFMMappingDataForm').submit();
// 					var jsondata = {"sellerCode":$('#sellerCode').val()};
// 					$.ajax( {dataType:'json' , url:"/admin/lookups/sellerSupcFMMappingData" , 
//                         async : false ,
//                         dataType : 'json', 
//                         data: jsondata, type : 'POST' , 
//                         success: function(data, textStatus,  jqXHR ) {
//                         	alert(data);
//                         } ,
//                         error : function(jqXHR, textStatus, errorThrown) { 
//                         	if(jqXHR.status==200){
//               	    		  alert("Your session has expired. Please log in again in another tab/window and then retry here.");
//               	    	  }
//                         }  
//                         });
				}
				
			});
			
			});
		</script>
	</tiles:putAttribute>
</tiles:insertTemplate>