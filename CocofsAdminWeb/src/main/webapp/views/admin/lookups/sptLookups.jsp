<%@ include file="/tld_includes.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<tiles:insertTemplate template="/views/layout/base.jsp">
	<tiles:putAttribute name="title" value="Supc PackagingType Mapping Lookups" />

	<tiles:putAttribute name="systemMessage">
		<div class="system-message"></div>
	</tiles:putAttribute>

	<tiles:putAttribute name="extraScripts">


	</tiles:putAttribute>
	
	<tiles:putAttribute name="sidebar">
		<tiles:insertTemplate template="/views/admin/lookups/lookups-sidebar.jsp">
		</tiles:insertTemplate>
	</tiles:putAttribute>
	<tiles:putAttribute name="systemMessage">
		<div class="system-message">
			<input type="hidden" value="${errorMessage }" id="messageHiddenField"/>
		</div>
	</tiles:putAttribute>


	<tiles:putAttribute name="subheader">
		<tiles:insertTemplate template="/views/admin/lookups/lookups-subheader.jsp">
		</tiles:insertTemplate>
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
		<div class="main-content lfloat">
			<div class="container" style="width: 85%;">
				<form id="file-upload-form" enctype="multipart/form-data" method="post" class="form-horizontal">
					<fieldset>
						<legend>Supc PackagingType  Upload Lookup</legend>
						<div class="control-group">
						<label class="control-label" for="inputfile">
                        File To Upload 
                        </label>
                        <div class="controls">
						<input id="inputfile" style="display:none;" name="inputfile" type="file"> 
                        <div class="input-append">
                        <input id="filebrowse" class="input" type="text">
                        <a class="btn btn-info" onclick="$('input[id=inputfile]').click();">Browse</a>
                        </div>
                        
                        </div>
						</div>
                        <div class="control-group">
                        <div class="controls">
                        <button id="file-upload-form-btn" type="Upload" class="btn btn-primary">Submit</button>
                        </div>
                        </div>
					</fieldset>
				</form>
				
				<a href="downloadSupcTemplate"> Download Supc Template</a>
                <c:if test="${errorMessage != null}">
                <div class="alert" id ="errorMsgDiv">
                <button type="button" class="close" data-dismiss="alert">&times;
                </button>
                ${errorMessage }</div>
                </c:if>

			</div>
		</div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
	<%-- Activate relevant sidebar function   --%>
	<%-- Activate relevant subheader function --%>
		<script type="text/javascript">
		
		$('input[id=inputfile]').change(function() {
			$('#filebrowse').val($(this).val().replace(/C:\\fakepath\\/i, ''));
            });

		function hideSidebar(){
		    $(".subheader").css({"left":"-11%"});
		    $(".functionAnchor").css({"display": "none"});
		    $(".sidebar").css({"left":"1%","width":"99%"});
		    $(".main-content").css({"left":"1%","width":"98%"});
		}
		function showSidebar(){
		    $(".subheader").css({"left":"0%"});
		    $(".functionAnchor").css({"display": "block"});
		    $(".sidebar").css({"left":"12%","width":"88%"});
		    $(".main-content").css({"left":"12%","width":"87%"});
		}
		
			$(document).ready(function() {
				var subheader=true;
				 $(".subheaderToggle").click(function(){
				         if(subheader){
				                 hideSidebar();
				                 $(".subheaderToggleContent").html(">");
				                 subheader=false;
				         }else{
				                 showSidebar();
				                 $(".subheaderToggleContent").html("<");
				                 subheader=true;
				         }

				 });
				/* check sidebar jsp for relevant div ids
				*/
				$('#sidebar-spt-upload').addClass('active');
				$('#header-nav-lookups').addClass('active');
				
				/* check subheader jsp for relevant div ids
				*/
				/* $('#subheader-fn-usrmanagement').addClass('active') */
				
				
				$('#file-upload-form-btn').click(function() {
					/*var uptype = $('input[name=uploadtype]:checked').val();*/
					
					if ($("#inputfile:input").length) {
						if ($("#inputfile").val() != '') {
							if (! $('#inputfile').val().trim().match(/\.xls$/) ) {
								alert("Only files with extension .xls can be uploaded ");
								
								return false;
							}
							var posturl = "processSupcPackagingTypeMappingUpload";
							$('#file-upload-form').attr("action", posturl)
							$("#errorMsgDiv").hide();
							$('#file-upload-form').submit();
						} else {
							alert("Please upload a file");
							return false;
						}
					} else {
						alert("class uploadJobFile not found");
						return false;
					}
					
				} );

			});
		</script>
	</tiles:putAttribute>
</tiles:insertTemplate>