<%@ include file="/tld_includes.jsp"%>


<div id="subheader" class="subheader"
	style="background-image: url(${path.resources('img/background/bg_blue.png')});">
	<div class="tabbable">
		<!-- Only required for left/right tabs -->
		<sec:authentication property="principal" var="user" />
		
		<div class="subheaderTab">
			<sec:authorize ifAnyGranted="admin,tech,pincodeuploader">

				<div id="sidebar-pincode-upload"
					onclick="javascript:window.location.href='/admin/pincodeZoneMapping/'">Upload Pincodes </div>
			</sec:authorize>

		</div>
	</div>
	<div class="subheaderToggle">
		<div class="subheaderToggleContent">&lt</div>
	</div>
</div>