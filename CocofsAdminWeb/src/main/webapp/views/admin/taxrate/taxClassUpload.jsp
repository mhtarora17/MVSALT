<%@ include file="/tld_includes.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<tiles:insertTemplate template="/views/layout/base.jsp">
	<tiles:putAttribute name="title" value="Tax Class Upload" />

	<tiles:putAttribute name="systemMessage">
		<div class="system-message"></div>
	</tiles:putAttribute>


	<tiles:putAttribute name="extraScripts">


	</tiles:putAttribute>
	<tiles:putAttribute name="sidebar">
		<tiles:insertTemplate template="/views/admin/taxrate/sidebar.jsp">
		</tiles:insertTemplate>
	</tiles:putAttribute>
	<tiles:putAttribute name="systemMessage">
		<div class="system-message">
			<input type="hidden" value="${message }" id="messageHiddenField" />
		</div>
	</tiles:putAttribute>


	<tiles:putAttribute name="subheader">
		<tiles:insertTemplate
			template="/views/admin/taxrate/taxRate-subheader.jsp">
		</tiles:insertTemplate>
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
		<div class="main-content lfloat">
			<div class="container" style="width: 85%;">
				<form action="${path.http}/admin/taxClass/upload"
					id="file-upload-form" enctype="multipart/form-data" method="post"
					class="form-horizontal">
					<fieldset>
						<legend>Tax Class File Upload</legend>
						<div class="control-group">
							<label class="control-label" for="taxClassLevel">Select
								Tax Class Type </label>
							<div class="controls">

								<select id="taxClassLevel" name="taxClassLevel">
									<c:forEach items="${allTaxClassLevels}" var="taxClass">

										<option value="${taxClass}">${taxClass}</option>
									</c:forEach>
								</select>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="inputfile">File To
								Upload </label>
							<div class="controls">
								<input id="inputfile" style="display: none;" name="inputfile"
									type="file">
								<div class="input-append">
									<input id="filebrowse" class="input" type="text"> <a
										class="btn btn-info"
										onclick="$('input[id=inputfile]').click();">Browse</a>
								</div>

							</div>
						</div>

						<div class="control-group">
							<div class="controls">
								<button id="file-upload-form-btn" type="Upload"
									class="btn btn-primary">Submit</button>
							</div>
						</div>

					</fieldset>
				</form>

				<div>
					<a href="${path.http}/admin/taxClass/downloadTaxClassData">
						Download Tax Class Data</a>
				</div>

				<br>
				<div>
					<a href="${path.http}/admin/taxClass/downloadSupcTaxClassTemplate">
						Download Supc Tax Class Template</a> (Download with Data from <a
						href="${path.http}/admin/taxClass/downloadSupcTaxClassTemplateAndData">
						<b>here</b>
					</a>)
				</div>

				<br>
				<div>
					<a
						href="${path.http}/admin/taxClass/downloadSubcatTaxClassTemplate">
						Download Subcategory Tax Class Template</a> (Download with Data from <a
						href="${path.http}/admin/taxClass/downloadSubcatTaxClassTemplateAndData">
						<b>here</b>
					</a>)
				</div>

				<br>
				<c:if test="${message != null and success==true}">
					<div class="alert alert-info">
						<button type="button" class="close" data-dismiss="alert">&times;
						</button>
						${message }
					</div>
				</c:if>
				<c:if test="${message != null and success==false}">
					<div class="alert alert-error">
						<button type="button" class="close" data-dismiss="alert">&times;
						</button>
						${message }
					</div>
				</c:if>


			</div>
		</div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
		<%-- Activate relevant sidebar function   --%>
		<%-- Activate relevant subheader function --%>
		<script type="text/javascript">
			$('input[id=inputfile]').change(
					function() {
						$('#filebrowse').val(
								$(this).val().replace(/C:\\fakepath\\/i, ''));
					});

			function hideSidebar() {
				$(".subheader").css({
					"left" : "-11%"
				});
				$(".functionAnchor").css({
					"display" : "none"
				});
				$(".sidebar").css({
					"left" : "1%",
					"width" : "99%"
				});
				$(".main-content").css({
					"left" : "1%",
					"width" : "98%"
				});
			}
			function showSidebar() {
				$(".subheader").css({
					"left" : "0%"
				});
				$(".functionAnchor").css({
					"display" : "block"
				});
				$(".sidebar").css({
					"left" : "12%",
					"width" : "88%"
				});
				$(".main-content").css({
					"left" : "12%",
					"width" : "87%"
				});
			}

			$(document)
					.ready(
							function() {
								var subheader = true;
								$(".subheaderToggle").click(function() {
									if (subheader) {
										hideSidebar();
										$(".subheaderToggleContent").html(">");
										subheader = false;
									} else {
										showSidebar();
										$(".subheaderToggleContent").html("<");
										subheader = true;
									}

								});
								/* check sidebar jsp for relevant div ids
								 */
								$('#sidebar-class-upload ').addClass('active');
								$('#header-nav-taxrate').addClass('active');

								/* check subheader jsp for relevant div ids
								 */
								/* $('#subheader-fn-usrmanagement').addClass('active') */

								$('#file-upload-form-btn')
										.click(
												function() {
													if ($("#inputfile:input").length) {
														if (!$('#inputfile')
																.val()
																.trim()
																.match(/\.xls$/)
																&& !$(
																		'#inputfile')
																		.val()
																		.trim()
																		.match(
																				/\.xlsx$/)) {
															alert("Only files with extension .xls can be uploaded ");
															return false;
														}
														if ($("#inputfile")
																.val() != '') {
															$(
																	'#file-upload-form')
																	.submit();
														} else {
															alert("Please upload a file");
															return false;
														}
													} else {
														alert("class uploadJobFile not found");
														return false;
													}

												});

							});
		</script>


	</tiles:putAttribute>
</tiles:insertTemplate>