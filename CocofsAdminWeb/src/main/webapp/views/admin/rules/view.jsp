<%@ include file="/tld_includes.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<tiles:insertTemplate template="/views/layout/base.jsp">
	<tiles:putAttribute name="title" value="Rules Admin" />

	<tiles:putAttribute name="systemMessage">
		<div class="system-message"></div>
	</tiles:putAttribute>

	<tiles:putAttribute name="extraScripts">
		<link href="${path.css('chosen/chosen.css')}" rel="stylesheet"
			type="text/css" />
		
		<script type="text/javascript"
			src="${path.js('chosen/chosen.jquery.min.js')}"></script>

		<script type="text/javascript"
			src="${path.js('jquery/jquery.ui.datepicker.min.js')}"></script>
		<script type="text/javascript" src="${path.js('spin/spin.min.js')}"></script>

	</tiles:putAttribute>
	<tiles:putAttribute name="sidebar">
		<tiles:insertTemplate template="/views/admin/rules/sidebar.jsp">
		</tiles:insertTemplate>
	</tiles:putAttribute>
	<tiles:putAttribute name="systemMessage">
		<div class="system-message">
			<input type="hidden" value="${message }" id="messageHiddenField"/>
		</div>
	</tiles:putAttribute>


	<tiles:putAttribute name="subheader">
		<tiles:insertTemplate template="/views/admin/rules/view-subheader.jsp">
		</tiles:insertTemplate>
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
		<div class="main-content lfloat">
			<div class="container" style="width: 90%;">
			 <form class="form-horizontal">
                    <div class="control-group">
                        <label class="control-label" for="frm-ruletype">Rule
                            Type</label>
                        <div class="controls">
                        <input  type="text" id="frm-ruletype" name="frm-ruletype" value="${ruletype}" readonly="readonly" width="310px"></input>
                        <%-- <select id="frm-ruletype" name="frm-ruletype" class="chosen-select">
                                
                                <c:forEach items="${ruletypes}"
                                    var="ruletype">
                                    <option value="${ruletype.key}">${ruletype.value}

                                    </option>
                                </c:forEach>
                        </select> --%>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"
                            for="frm-categoryurl">Category</label>
                        <div class="controls">
                        
                        <select id="frm-categoryurl" name="frm-categoryurl" class="chosen-select" multiple>
                                <c:forEach items="${categoryUrls}"
                                    var="catUrl">
                                    
                                    <option value="${catUrl.key}">${catUrl.value}

                                    </option>
                                </c:forEach>
                        </select>
                        </div>
                    </div>
                    
                    <div class="control-group">
                    <div class="controls">
                    <button id="frm-fetchrule-btn" type="button" class="btn btn-primary">Fetch Rules </button>
                    <button id="frm-clearrule-btn" type="button" class="btn btn-primary">Clear Results </button>
                    </div>
                    </div>
                    <div id="fetchMessages">
                    </div>
                    <div id="spinnerDiv">
                    </div>
                    </form>
                    <div class="accordion" id="resultContainer">
                    
                    </div>
                    <div></div>
                    
                    <div id="creationMessages">
                    </div>
                    <div id="creationdiv">
                    
                    <form class="form-horizontal">
                    <h4>Create Rules for fetched categories</h4>
                    <div class="control-group">
                        
                        <label class="control-label"
                            for="frm-startdate">Start Date</label>
                        <div class="controls">
                            <input type="text" id="frm-startdate" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"
                            for="frm-enddate">End  Date</label>
                        <div class="controls">
                            <input type="text" id="frm-enddate" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"
                            for="frm-fragile">Fragile</label>
                        <div class="controls">
                            <label class="radio inline"> <input
                                type="radio" id="frm-fragile-1"
                                name="frm-fragile" value="" checked />
                                Any
                            </label> <label class="radio inline"> <input
                                type="radio" id="frm-fragile-2"
                                name="frm-fragile" value="fragile" />
                                Fragile
                            </label> <label class="radio inline"> <input
                                type="radio" id="frm-fragile-3"
                                name="frm-fragile" value="notfragile" />
                                Not Fragile
                            </label>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"
                            for="frm-liquid">Liquid</label>
                        <div class="controls">
                            <label class="radio inline"> <input
                                type="radio" id="frm-liquid-1"
                                name="frm-liquid" value="" checked />
                                Any
                            </label> <label class="radio inline"> <input
                                type="radio" id="frm-liquid-2"
                                name="frm-liquid" value="liquid" />
                                Liquid
                            </label> <label class="radio inline"> <input
                                type="radio" id="frm-liquid-3"
                                name="frm-liquid" value="notliquid" />
                                Not Liquid
                            </label>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"
                            for="frm-hazmat">Hazardous Material </label>
                        <div class="controls">
                            <label class="radio inline"> <input
                                type="radio" id="frm-hazmat-1"
                                name="frm-hazmat" value="" checked />
                                Any
                            </label> <label class="radio inline"> <input
                                type="radio" id="frm-hazmat-2"
                                name="frm-hazmat" value="hazmat" />
                                Hazardous
                            </label> <label class="radio inline"> <input
                                type="radio" id="frm-hazmat-3"
                                name="frm-hazmat" value="nothazmat" />
                                Not Hazardous
                            </label>
                        </div>
                    </div>
                    
                    <div class="control-group">
                        <label class="control-label"
                            for="frm-weight">Weight (more than)</label>
                        <div class="controls">
                            <input id="frm-weight" type="text" placeholder="0" /> gm.
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"
                            for="frm-volweight">Volumetric Weight (more than)</label>
                        <div class="controls">
                            <input id="frm-volweight" type="text" placeholder="0"/> gm.
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="controls">
                        <button id="frm-createrule-btn" type="button" class="btn btn-primary">Create Rule </button>
                    
                        </div>
                    </div>
                    </form>
                    </div>
			</div>
		</div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
	<%-- Activate relevant sidebar function   --%>
	<%-- Activate relevant subheader function --%>
		<script type="text/javascript">

		function hideSidebar(){
		    $(".subheader").css({"left":"-11%"});
		    $(".functionAnchor").css({"display": "none"});
		    $(".sidebar").css({"left":"1%","width":"99%"});
		    $(".main-content").css({"left":"1%","width":"98%"});
		}
		function showSidebar(){
		    $(".subheader").css({"left":"0%"});
		    $(".functionAnchor").css({"display": "block"});
		    $(".sidebar").css({"left":"12%","width":"88%"});
		    $(".main-content").css({"left":"12%","width":"87%"});
		}

        function buildCatDisplayStart(idx, catName) {
            var str = '';
            str += '<div id="result-accordian-group-'+idx+'" class="accordion-group">';
            str += '<div class="accordion-heading">';
            str += '<a class="accordion-toggle" data-toggle="collapse" data-parent="#resultContainer" href="#collapse-'+idx+'">';
            str += '<strong>View/Edit - ' + catName +  '</strong>';
            str += '</a>';
            str += '</div>';
            str += '<div id="collapse-'+idx+'" class="accordion-body collapse">';
            str += '<div class="accordion-inner">';
            str += '<span class="label label-info">'+catName+'</span>';
            
            return str;
        }
        function buildCatDisplayEnd(idx, catName) {
            var str = "</div></div></div>";
            return str;
        }
        
        
        function buildCatRuleDisplay(idx, ruleList, catName, categoryurl) {
            var str = "<div>";
            str += '<table class="table table-bordered">';
            str += '<thead>';
            str += '<tr>';
            str += '<th>Name</th><th>StartDate</th><th>EndDate</th><th>Fragile</th><th>Liquid</th>';
            str += '<th>HazMat</th><th>Weight (gm) more than </th><th>Volumetric Weight (gm) more than</th><th>Actions</th>';
            str += '</tr>';
            str += '</thead>';
            str += '<tbody>';
            var ridx = 0;
            for (ridx = 0 ; ridx < ruleList.length; ridx = ridx + 1 ) {
                str += showRuleData('idx-'+idx+'-' + ridx, ruleList[ridx] , catName, categoryurl);
            }
            str += '</tbody>';
            str += '</table>';
            str += "</div>";
            return str;
        }
        
        function buildFragileInput(idx, rule) {
            str = '<td>';
            
            str += '<div><label class="radio inline">';
            str +=' <input type="radio" id="frm-fragile-1-'+idx+'" name="frm-fragile-'+idx+'" value="" ';
            if ( rule.fragile == null) {
                str += 'checked />';
            } else {
                str += '/>';
            }
            str += 'Any';
            str += '</label></div>';
            
            str += '<div><label class="radio inline">';
            str +=' <input type="radio" id="frm-fragile-2-'+idx+'" name="frm-fragile-'+idx+'" value="fragile" ' ;
            if ( rule.fragile == true) {
                str += 'checked />';
            } else {
                str += '/>';
            }
            str += 'Fragile';
            str += '</label></div>';
            str += '<div><label class="radio inline">';
            str += '<input type="radio" id="frm-fragile-3-'+idx+'" name="frm-fragile-'+idx+'" value="notfragile" ';
            if ( rule.fragile == false) {
                str += 'checked />';
            } else {
                str += '/>';
            }
            str += 'Not Fragile';
            str += '</label></div>';
            str += '</td>';
            return str;
            
        }
        
        function buildLiquidInput(idx, rule) {
            str = '<td>';
            
            str += '<div><label class="radio inline">';
            str +=' <input type="radio" id="frm-liquid-1-'+idx+'" name="frm-liquid-'+idx+'" value="" ';
            if ( rule.liquid == null) {
                str += 'checked />';
            } else {
                str += '/>';
            }
            str += 'Any';
            str += '</label></div>';
            
            str += '<div><label class="radio inline">';
            str +=' <input type="radio" id="frm-liquid-2-'+idx+'" name="frm-liquid-'+idx+'" value="liquid" ' ;
            if ( rule.liquid == true) {
                str += 'checked />';
            } else {
                str += '/>';
            }
            str += 'Liquid';
            str += '</label></div>';
            str += '<div><label class="radio inline">';
            str += '<input type="radio" id="frm-liquid-3-'+idx+'" name="frm-liquid-'+idx+'" value="notliquid" ';
            if ( rule.liquid == false) {
                str += 'checked />';
            } else {
                str += '/>';
            }
            str += 'Not Liquid';
            str += '</label></div>';
            str += '</td>';
            return str;
        }
        
        function buildHazMatInput(idx, rule) {
            str = '<td>';
            
            str += '<div><label class="radio inline">';
            str +=' <input type="radio" id="frm-hazmat-1-'+idx+'" name="frm-hazmat-'+idx+'" value="" ';
            if ( rule.hazMat == null) {
                str += 'checked />';
            } else {
                str += '/>';
            }
            str += 'Any';
            str += '</label></div>';
            
            str += '<div><label class="radio inline">';
            str +=' <input type="radio" id="frm-hazmat-2-'+idx+'" name="frm-hazmat-'+idx+'" value="hazmat" ' ;
            if ( rule.hazMat == true) {
                str += 'checked />';
            } else {
                str += '/>';
            }
            str += 'Hazardous';
            str += '</label></div>';
            str += '<div><label class="radio inline">';
            str += '<input type="radio" id="frm-hazmat-3-'+idx+'" name="frm-hazmat-'+idx+'" value="nothazmat" ';
            if ( rule.hazMat == false) {
                str += 'checked />';
            } else {
                str += '/>';
            }
            str += 'Not Hazardous';
            str += '</label></div>';
            str += '</td>'
            return str;
        }
        
        function showRuleData(idx, rule, catName, categoryurl) {
            var str = "<tr id='row-" + idx + "'>";
            str += '<td>' + rule.name + '</td>';
            str += '<td>';
            str += '<input class="input-small" type="text" id="frm-startdate-'+idx+'" />'; 
            str += '</td><td>';
            str += '<input class="input-small" type="text" id="frm-enddate-'+idx+'" />';
            str += '</td>';
            str += buildFragileInput(idx, rule);
            str += buildLiquidInput(idx, rule);
            str += buildHazMatInput(idx, rule);
            
            str += '<td>';
            str += '<input class="input-small" id="frm-weight-'+idx+'" type="text" placeholder="0" ';
            if ( rule.weight != null) {
                str += 'value="' + rule.weight +'"';
            } 
            str += '/>';
            
            str += '</td><td>';
            str += '<input class="input-small" id="frm-volweight-'+idx+'" type="text" placeholder="0" ';
            if ( rule.volumetricWeight != null) {
                str += 'value="' + rule.volumetricWeight +'"';
            } 
            str += '/> ';
            str += '</td>';
            str += '<td>';
            str += '<input style="display:none;" type="checkbox" id="frm-enabled-'+idx+'" ';
            if ( rule.enabled == true) {
                    str += ' checked="checked" ';
                    str +=  'value="enabled" ';
            } else {
                    str +=  'value="notenabled" ';
            }
            str += '/>';
            str += '<div id="update-in-progress-msg-'+idx+'">';
            str += '</div>';
            str += '<div>';
            str += '<button id="frm-updaterule-btn-'+idx+'" type="button" class="btn" onclick="updatebtnfn(\''+idx + '\', \'' + rule.categoryUrl+ '\', \'' +  rule.name+ '\', \'' + rule.ruleType+ '\')">Update</button>';
            str += '</div>';
            str += '<br>';
            str += '<div>';
            str += '<button id="frm-removerule-btn-'+idx+'" type="button" class="btn btn-danger" onclick="removebtnfn(\''+idx + '\', \'' + rule.categoryUrl+ '\', \'' +  rule.name+ '\', \'' + rule.ruleType+ '\')">Remove</button>';
            str += '</div>';
            
            str += '</td>';
            
            
            str += '</tr>';
            str += '<tr><div id="frm-update-message-'+idx+'"></div></tr>';
           
            
            return str;
        }
        
        function updatebtnfn(idx, categoryUrl, name, ruleType ) {
            updateRuleByIndex(idx, $('#frm-updaterule-btn-'+idx), categoryUrl, name, ruleType, false);
        }
        function removebtnfn(idx, categoryUrl, name, ruleType ) {
            if ( confirm("Are you sure you want to remove the rule ? ") ) {
                $("#frm-enabled-"+idx+"").prop('checked', false);
                $('#frm-enabled-'+idx+'').attr('checked', false);
                $('#frm-enabled-'+idx+'').attr('value', 'notenabled');


                updateRuleByIndex(idx, $('#frm-removerule-btn-'+idx), categoryUrl, name, ruleType, true);
                
                
            }
            
        }
        
        function showErrorAlert(message ) {
            /*<div class="alert alert-error">*/
            console.log(message);
            if ($('#fetch-error-msg').length == 0 ) {
                $('#fetchMessages').append('<div class="alert alert-error"> <button type="button" class="close" data-dismiss="alert">&times;</button> <div id="fetch-error-msg">' + message + '</div></div>');
                
            } else {
                $('#fetch-error-msg').html($('#fetch-error-msg').html() + " <br/>" + message);
            }
        }
        
        function showCreateErrorAlert(message ) {
            /*<div class="alert alert-error">*/
            console.log(message);
        
            if ($('#create-error-msg').length == 0 ) {
                $('#creationMessages').append('<div class="alert alert-error"> <button type="button" class="close" data-dismiss="alert">&times;</button> <div id="create-error-msg">' + message + '</div></div>');
                
            } else {
                $('#create-error-msg').html($('#create-error-msg').html() + " <br/>" + message);
            }
        }
        function showCreateSuccessAlert(message) {
            /*<div class="alert alert-success">*/
            
            console.log(message);
            if ($('#create-success-msg').length == 0 ) {
                $('#creationMessages').append('<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert">&times;</button> <div id="create-success-msg">' + message + '</div></div>');
            } else {
                $('#create-success-msg').html($('#create-success-msg').html() + " <br/>" + message);
            }
        }
        function showSuccessAlert(message) {
            /*<div class="alert alert-success">*/
            
            console.log(message);
            $('#fetchMessages').append('<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert">&times;</button>' + message + '</div>');
            
        }
        function getCategoryName(categoryurl) {
            var name ;
            $('select[name="frm-categoryurl"] > option ').each(function() {if ($(this).val() == categoryurl){name =$(this).text().trim();} });
            return name;
        }
        function creationError(categoryurl, jqXHR, textStatus, errorThrown ) {
            console.log(categoryurl);
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown); 
            showCreateErrorAlert("<strong>" + getCategoryName(categoryurl) + "</strong> : Rule for category with url " + categoryurl 
                    + " not created, error : "+  errorThrown + " " );
        }
        function successHandler( categoryurl, data, textStatus,  jqXHR ) {
            console.log(categoryurl);
            console.log(data);
            console.log(textStatus);
            console.log(jqXHR); 
            if ( data.status == 'FAIL') {
                showCreateErrorAlert("<strong>" + getCategoryName(categoryurl) +  "</strong> : Rule for category with url " + categoryurl 
                        + " not created, error : " + data.message );
            } else {
                showCreateSuccessAlert("<strong>" + getCategoryName(categoryurl) +  "</strong> : Rule for category with url " + categoryurl 
                        + " created successfully " );
            }

        }
        function successEditHandler(msgBox, categoryurl, data, textStatus,  jqXHR) {
            if ( data.status == 'FAIL') {
                msgBox.append('<div class="alert alert-error"> <button type="button" class="close" data-dismiss="alert">&times;</button>Update Failed : ' + data.message + '</div>');
                return;
            }
            msgBox.append('<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert">&times;</button>Update Successfull</div>');
        }
        function editError(msgBox, categoryurl, jqXHR, textStatus, errorThrown ) {
            msgBox.append('<div class="alert alert-error"> <button type="button" class="close" data-dismiss="alert">&times;</button>Update Failed : ' + errorThrown + '</div>');
            
        }
        function fetchError(categoryurl, jqXHR, textStatus, errorThrown ) {
            console.log(categoryurl);
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown); 
            showErrorAlert("<strong>" + getCategoryName(categoryurl) + "</strong> : Rule for category with url " + categoryurl 
                    + " not found, error : "+  errorThrown + " " );
        }
        function fetchSuccessHandler( idx, categoryurl, data, textStatus,  jqXHR ) {
            console.log(categoryurl);
            console.log(data);
            console.log(textStatus);
            console.log(jqXHR); 
            var catName = getCategoryName(categoryurl);
            var cathtml = '';
            if ( data.status == 'FAIL') {
                showErrorAlert("<strong>" + catName +  "</strong> : Rule for category with url " + categoryurl 
                        + " not found, error : " + data.message );
                
                cathtml += buildCatDisplayStart(idx, catName);
                cathtml += buildCatDisplayEnd(idx, catName);
            } else {
                console.log(data.items.ruleList);
                cathtml += buildCatDisplayStart(idx, catName);
                cathtml += buildCatRuleDisplay(idx, data.items.ruleList, catName, categoryurl);
                
                cathtml += buildCatDisplayEnd(idx, catName);
                
                /* showSuccessAlert("<strong>" + getCategoryName(categoryurl) +  "</strong> : Rule for category with url " + categoryurl 
                        + " found successfully " ); */
            }

            var container = $('#resultContainer');
            container.append(cathtml);
            if ( (data.items != null) &&  (data.items != undefined)) {
                activateDateFields(idx, data.items.ruleList );
            }
            
        }
        function activateDateFields (idx, ruleList ) {
            var ridx = 0;
            for (ridx = 0 ; ridx < ruleList.length; ridx = ridx + 1 ) {
                var id = 'idx-'+idx+'-'  + ridx;
                var rule = ruleList[ridx];
                $('#frm-startdate-'+id+'').datepicker({
                    arrows: false,
                    presets:{specificDate: 'Pick a date'},
                    presetRanges:[],
                    showOn: "button",
              buttonImage: "/static/cocofs/img/calendar.jpg",
              buttonImageOnly: false,
              dateFormat:'yy-mm-dd'
                  });
                $( '#frm-startdate-'+id+'' ).datepicker( "setDate", new Date(rule.startDate));
                $('#frm-enddate-'+id+'').datepicker({
                    arrows: false,
                    presets:{specificDate: 'Pick a date'},
                    presetRanges:[],
                    showOn: "button",
              buttonImage: "/static/cocofs/img/calendar.jpg",
              buttonImageOnly: false,
              dateFormat:'yy-mm-dd'
                  });
                if ( rule.endDate != null) {
                    $( '#frm-enddate-'+id+'' ).datepicker( "setDate", new Date(rule.endDate));
                }
            }
        }
        function isInvalid(start, end, weight, volweight, startDateObj, endDateObj, ruleCreation ) {
            if ( (weight.length != 0) && !$.isNumeric(weight)) {
                alert("Enter a valid number for weight ");
                return true;
            }
            if ( (volweight.length != 0) && !$.isNumeric(volweight)) {
                alert("Enter a valid number for volumetric weight ");
                return true;
            }
            if ($.isNumeric(weight) && (weight < 0)) {
                alert("Weight needs to be >= 0 ");
                return true;
            }
            if ($.isNumeric(volweight) && (volweight < 0)) {
                alert("Volumetric Weight needs to be >= 0 ");
                return true;
            }
            
            if ( start.length == 0 ) {
                alert("Start date not specified ");
                return true;
            }
            if ( (end.length != 0) && end < start ) {
                alert("End date before Start date ");
                return true;
            }
            
            if (! (start == $.datepicker.formatDate('yy-mm-dd', startDateObj))) {
                alert("Start Date appears to be incorrect");
                return true;
            }
            if ((end.length != 0) &&  (end != $.datepicker.formatDate('yy-mm-dd', endDateObj))) {
                alert("End Date appears to be incorrect");
                return true;
            }
            
            if ( ruleCreation ) {
            	var x = $.datepicker.formatDate('yy-mm-dd', new Date())
                if ( $('#frm-startdate').val() < x ) {
                	alert('Start date before today not allowed'); 
                	return true;
                }
            }
            
            
            return false;
        }
        
        function updateRuleByIndex(idx, clickedButton, categoryUrl, ruleName, ruleType, removeAfterSuccess) {
            var fragile = $("input:radio[name=frm-fragile-"+idx+"]:checked").val();
            var liquid = $("input:radio[name=frm-liquid-"+idx+"]:checked").val();
            var hazmat = $("input:radio[name=frm-hazmat-"+idx+"]:checked").val();
            var weight = $("#frm-weight-"+idx+"").val().trim();
            var volweight = $("#frm-volweight-"+idx+"").val().trim();
            
            var start  = $("#frm-startdate-"+idx+"").val().trim();
            var end  = $("#frm-enddate-"+idx+"").val().trim();
            
            var enabled = 'notenabled';
            if ($("#frm-enabled-"+idx+"").is(":checked")) {
                enabled = "enabled";
            }
            var msgBox = $("#frm-update-message-"+idx+"");
            msgBox.html('');
            if ( !removeAfterSuccess && isInvalid(start, end, weight, volweight, $("#frm-startdate-"+idx+"").datepicker('getDate'), $("#frm-enddate-"+idx+"").datepicker('getDate') ) ) {
                /* This is require to make sure that we do not delete/update
                a rule we do not accidentally disable it in next update */
                if (!$("#frm-enabled-"+idx+"").is(":checked")) {
                    $("#frm-enabled-"+idx+"").prop('checked', true);
                    $('#frm-enabled-'+idx+'').attr('checked', true);
                    $('#frm-enabled-'+idx+'').attr('value', 'enabled');
                }
                return;
            } 
            clickedButton.hide();
            $('#update-in-progress-msg-'+idx+'').html('In Progress...');
            /**/
            var pdata = {
                    'rulename'     : ruleName,
                    'enabled'      : enabled,
                    'ruletype'     : ruleType ,
                    'categoryurl'  : categoryUrl ,
                    'fragile'      : fragile,
                    'liquid'       : liquid,
                    'hazmat'       : hazmat,
                    'weight'       : weight ,
                    'volweight'    : volweight,
                    'startdate'    : start ,
                    'enddate'      : end
            };
            console.log(pdata);
             
            $.ajax( {dataType:'json' , url:"/admin/rules/editRule" , 
                async : false ,
                data: pdata, type : 'POST' , 
                success: function(data, textStatus,  jqXHR ) {successEditHandler(msgBox, categoryUrl, data, textStatus,  jqXHR ); clickedButton.show();
                $('#update-in-progress-msg-'+idx+'').html('');
                    if ( removeAfterSuccess && ( data.status != 'FAIL')) {  $('#row-'+idx+'').remove(); }
                    else { 
                        if (!$("#frm-enabled-"+idx+"").is(":checked")) {
                            $("#frm-enabled-"+idx+"").prop('checked', true);
                            $('#frm-enabled-'+idx+'').attr('checked', true);
                            $('#frm-enabled-'+idx+'').attr('value', 'enabled');
                        }
                    }
                    
                } ,
                error : function(jqXHR, textStatus, errorThrown) { 
                	
          	    	  if(jqXHR.status==200){
          	    		  alert("Your session has expired. Please log in again in another tab/window and then retry here.");
          	    	  } else {
          	    		editError(msgBox, categoryUrl, jqXHR, textStatus, errorThrown ); 
          	    	  }
          	    	clickedButton.show();
          	    	$('#update-in-progress-msg-'+idx+'').html('');
                if (!$("#frm-enabled-"+idx+"").is(":checked")) {
                    $("#frm-enabled-"+idx+"").prop('checked', true);
                    $('#frm-enabled-'+idx+'').attr('checked', true);
                    $('#frm-enabled-'+idx+'').attr('value', 'enabled');
                }
                    }  
                });
            
        }
        
        function hideSpinnerIfDone(overallProgressList, spinner ) {
            if ( overallProgressList.length == 0 ) {
                spinner.stop();
                $('#spinnerDiv').html('');
            }
            
        }
        
        function creatRuleForCategoryAndType(ruletype, categoryurl, fragile, liquid
                , hazmat, weight, volweight, start, end, overallProgressList, spinner ) {
                console.log(ruletype + " " + categoryurl + " " + fragile + " " + liquid
                    + " " + hazmat + " " + weight + " " + volweight + " " + start
                    + " " + end );
                overallProgressList.unshift(categoryurl);
                var pdata = {
                        'ruletype' : ruletype ,
                        'categoryurl' : categoryurl ,
                        'fragile'      : fragile,
                        'liquid'       : liquid,
                        'hazmat'       : hazmat,
                        'weight'       : weight ,
                        'volweight'    : volweight,
                        'startdate'    : start ,
                        'enddate'      : end
                };
                $.ajax( {dataType:'json' , url:"/admin/rules/createRule" , 
                    data: pdata, type : 'POST' , 
                    success: function(data, textStatus,  jqXHR ) {successHandler(categoryurl, data, textStatus,  jqXHR ); overallProgressList.shift(); hideSpinnerIfDone(overallProgressList, spinner);} ,
                    error : function(jqXHR, textStatus, errorThrown) { 
                    	if(jqXHR.status==200){
            	    		  alert("Your session has expired. Please log in again in another tab/window and then retry here.");
            	    	  }
                    	creationError(categoryurl, jqXHR, textStatus, errorThrown ); overallProgressList.shift(); hideSpinnerIfDone(overallProgressList, spinner );}  
                    });
                
                
            }
        
        function fetchRuleForCategoryAndType(idx, ruletype, categoryurl, overallProgressList, spinner ) {
                console.log(ruletype + " " + categoryurl );
                overallProgressList.unshift(categoryurl);
                var pdata = {
                        'ruletype' : ruletype ,
                        'categoryurl' : categoryurl 
                };
                $.ajax( {dataType:'json' , url:"/admin/rules/findRuleForCategoryAndType" , 
                    async : false ,
                    data: pdata, type : 'POST' , 
                    success: function(data, textStatus,  jqXHR ) {fetchSuccessHandler(idx, categoryurl, data, textStatus,  jqXHR ); overallProgressList.shift(); hideSpinnerIfDone(overallProgressList, spinner);} ,
                    error : function(jqXHR, textStatus, errorThrown) { 
                    	if(jqXHR.status==200){
            	    		  alert("Your session has expired. Please log in again in another tab/window and then retry here.");
            	    	  }
                    	fetchError(categoryurl, jqXHR, textStatus, errorThrown ); overallProgressList.shift(); hideSpinnerIfDone(overallProgressList, spinner );}  
                    });
                
                
            }
        var spinopts = {
                lines: 13, // The number of lines to draw
                length: 40, // The length of each line
                width: 10, // The line thickness
                radius: 50, // The radius of the inner circle
                corners: 1, // Corner roundness (0..1)
                rotate: 0, // The rotation offset
                direction: 1, // 1: clockwise, -1: counterclockwise
                color: '#000', // #rgb or #rrggbb or array of colors
                speed: 1, // Rounds per second
                trail: 60, // Afterglow percentage
                shadow: false, // Whether to render a shadow
                hwaccel: false, // Whether to use hardware acceleration
                className: 'spinner', // The CSS class to assign to the spinner
                zIndex: 2e9, // The z-index (defaults to 2000000000)
                top: 'auto', // Top position relative to parent in px
                left: 'auto' // Left position relative to parent in px
              };
        var ruleFetchInProgress = false;
        var fetchedCats = [];
        function fetchRules() {
            var spinner ;
            if ( ruleFetchInProgress) {
                return;
            } else {
                
                
                /* clean up message */
                $('#fetchMessages').html('');
                $('#resultContainer').html('');
            }
            var ruletype = $("#frm-ruletype").val();
            
            
            ruleFetchInProgress = true;
                /* show ajax going symbol */
                /* note spin js doesn't use jqueyr element selection */
                var target = document.getElementById('spinnerDiv');
                spinner = new Spinner(spinopts).spin(target);
                var overallProgressList = []
                fetchedCats = [];
                $('select[name="frm-categoryurl"] > option:selected').each(function(idx, value) {
                fetchedCats.unshift($(this).val());
                fetchRuleForCategoryAndType( idx, ruletype, $(this).val(), overallProgressList, spinner )
                });
                ruleFetchInProgress = false;
            

            } /*End of fetchRules*/
            
            
            var ruleCreationInProgress = false;
            function createRules() {
                var spinner ;
                if ( ruleCreationInProgress) {
                    return;
                } else {
                    
                    
                    /* clean up message */
                    $('#creationMessages').html('');
                }
                var fragile = $("input:radio[name=frm-fragile]:checked").val();
                var liquid = $("input:radio[name=frm-liquid]:checked").val();
                var hazmat = $("input:radio[name=frm-hazmat]:checked").val();
                var weight = $("#frm-weight").val().trim();
                var volweight = $("#frm-volweight").val().trim();
                
                var start  = $("#frm-startdate").val().trim();
                var end  = $("#frm-enddate").val().trim();
                var ruletype = $("#frm-ruletype").val();
                
                if ( isInvalid(start, end, weight, volweight, $("#frm-startdate").datepicker('getDate'), $("#frm-enddate").datepicker('getDate') , true)  ) {
                    return;
                } else {
                    ruleCreationInProgress = true;
                    /* show ajax going symbol */
                    /* note spin js doesn't use jqueyr element selection */
                    var target = document.getElementById('spinnerDiv');
                    spinner = new Spinner(spinopts).spin(target);
                    var overallProgressList = []
                    var allFound = true;
                    $('select[name="frm-categoryurl"] > option:selected').each(function() {
                        var x = 0;
                        var found = false;
                        for ( x = 0; x < fetchedCats.length ; x++) {
                            if ( fetchedCats[x] == $(this).val() ) {
                                found = true;
                                break;
                            }
                        }
                        if ( !found ) {
                            allFound = false;
                        }
                    });
                    if ( allFound ) {
                        $('select[name="frm-categoryurl"] > option:selected').each(function() {
                            creatRuleForCategoryAndType( ruletype, $(this).val(), fragile
                                    , liquid, hazmat, weight, volweight
                                    , start , end, overallProgressList, spinner )});
                        hideAndResetCreationFormValues();
                    } else {
                        alert("Please press fetch again to fetch rules for selected categories");
                        spinner.stop();
                        $('#spinnerDiv').html('')
                    }
                    ruleCreationInProgress = false;
                }

                    }/* End of createRules() */
            
		    function resetCreationFormValues() {
		    	$('#frm-startdate').datepicker('setDate', new Date());
		    	$('#frm-enddate').datepicker('setDate',null);
		    	$('#frm-fragile-1').prop('checked',true);
		    	$('#frm-liquid-1').prop('checked',true);
		    	$('#frm-hazmat-1').prop('checked',true);
		    	$('#frm-weight').val(null);
		    	$('#frm-volweight').val(null);
		    	
            }
            function hideAndResetCreationFormValues() {
                 resetCreationFormValues();
                 $('#creationdiv').hide();
            	
            }
			$(document).ready(function() {
				var subheader=true;
				 $(".subheaderToggle").click(function(){
				         if(subheader){
				                 hideSidebar();
				                 $(".subheaderToggleContent").html(">");
				                 subheader=false;
				         }else{
				                 showSidebar();
				                 $(".subheaderToggleContent").html("<");
				                 subheader=true;
				         }

				 });
				/* check sidebar jsp for relevant div ids
				*/
				$('#sidebar-fn-ruleview').addClass('active');
				$('#header-nav-rules').addClass('active');
                
				/* check subheader jsp for relevant div ids
				*/
				/* $('#subheader-fn-usrmanagement').addClass('active') */
				/*findRuleForCategoryAndType*/
				
                
                $('#frm-fetchrule-btn').click(function() {
                	if ( $('select[name="frm-categoryurl"] > option:selected').length < 1 ) {
                		alert('Select atleast one category');
                		return ;
                	}
                    fetchRules();
                    $('#creationdiv').show();
                });
                
                $('#frm-clearrule-btn').click(function() {
                	$('#resultContainer').html('');
                	$('#fetchMessages').html('');
                	
                });
                
                
                $('#frm-createrule-btn').click(function() {
                    if ( $('select[name="frm-categoryurl"] > option:selected').length < 1 ) {
                        alert('Select atleast one category');
                        return ;
                    }
                    createRules();
                    
                });
                $('#frm-startdate').datepicker({
                    arrows: false,
                    presets:{specificDate: 'Pick a date'},
                    presetRanges:[],
                    showOn: "button",
                    minDate: 0 ,
              buttonImage: "/static/cocofs/img/calendar.jpg",
              buttonImageOnly: false,
              dateFormat:'yy-mm-dd'
                  });
                $('#frm-startdate').datepicker('setDate', new Date());
                $('#frm-enddate').datepicker({
                    arrows: false,
                    presets:{specificDate: 'Pick a date'},
                    presetRanges:[],
                    showOn: "button",
              buttonImage: "/static/cocofs/img/calendar.jpg",
              buttonImageOnly: false,
              dateFormat:'yy-mm-dd'
                  });
                $('#creationdiv').hide();
                $('.chosen-select').chosen({"display_disabled_options":false, "display_selected_options":false});
                
                


			});
			
		
		
		

		</script>
			
		
	</tiles:putAttribute>
</tiles:insertTemplate>
