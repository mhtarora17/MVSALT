<%@ include file="/tld_includes.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<tiles:insertTemplate template="/views/layout/base.jsp">
	<tiles:putAttribute name="title" value="Rules Admin" />

	<tiles:putAttribute name="systemMessage">
		<div class="system-message"></div>
	</tiles:putAttribute>

	<tiles:putAttribute name="extraScripts">
        <link href="${path.css('chosen/chosen.css')}" rel="stylesheet"
    type="text/css" />

<style>
.chosen-container{
width: 22% !important;
}


</style>

    <script type="text/javascript"
    src="${path.js('chosen/chosen.jquery.min.js')}"></script>
    
    <script type="text/javascript"
    src="${path.js('jquery/jquery.ui.datepicker.min.js')}"></script>
    <script type="text/javascript"
    src="${path.js('spin/spin.min.js')}"></script>
    
	</tiles:putAttribute>
	<tiles:putAttribute name="sidebar">
		<tiles:insertTemplate template="/views/admin/rules/sidebar.jsp">
		</tiles:insertTemplate>
	</tiles:putAttribute>
	<tiles:putAttribute name="systemMessage">
		<div class="system-message">
			<input type="hidden" value="${message }" id="messageHiddenField"/>
		</div>
	</tiles:putAttribute>


	<tiles:putAttribute name="subheader">
		<tiles:insertTemplate template="/views/admin/rules/view-subheader.jsp">
		</tiles:insertTemplate>
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
		<div class="main-content lfloat">
			<div class="container" style="width: 90%;">
			<form:form commandName="giftWrapCategoryRuleEditForm"
				name="giftWrapCategoryRuleEditForm" id="giftWrapCategoryRuleEditForm"
				method="post" path="test"
				 enctype="multipart/form-data">
			 
                    <div class="accordion" id="resultContainer">
                    
                    </div>
                    <div></div>
                    
                    <div id="creationMessages">
                    </div>
                    <div id="creationdiv">
                    
                    <form class="form-horizontal">
                    <h4>Update Rules</h4>
                    <div class="control-group">
                        
                        <label class="control-label"
                            for="frm-categoryurl">Category</label>
                        <div class="controls">
                            <form:input path="categoryUrl" type="text" id="frm-categoryurl" readonly="true" width="310px"></form:input>
                        </div>
                    </div>
                    
                    <div class="control-group">
                        
                        <label class="control-label"
                            for="frm-rulecode">Rule Code</label>
                        <div class="controls">
                            <form:input path="ruleCode" type="text" id="frm-rulecode" readonly="true" style="width:330px"></form:input>
                        </div>
                    </div>
                    
                     <div class="control-group">
                        
                        <label class="control-label"
                            for="frm-rulename">Rule Name</label>
                        <div class="controls">
                            <form:input path="name" type="text" id="frm-rulename" readonly="true" style="width:330px"></form:input>
                        </div>
                    </div>
                    
                    <div class="control-group">
                        
                        <label class="control-label"
                            for="frm-startdate">Start Date</label>
                        <div class="controls">
                            <form:input path="startDate" type="text" id="frm-startdate" ></form:input>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"
                            for="frm-enddate">End  Date</label>
                        <div class="controls">
                            <form:input path="endDate" type="text" id="frm-enddate" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"
                            for="frm-fragile">Fragile</label>
                        <div class="controls">
                        
                            <label class="radio inline"> <form:radiobutton path="fragile"
                                id="frm-fragile-1"
                                name="fragile" value="" checked="true"/>
                                Any
                            </label> <label class="radio inline"> <form:radiobutton path="fragile"
                                 id="frm-fragile-2"
                                name="fragile" value="true" />
                                Fragile
                            </label> <label class="radio inline"> <form:radiobutton path="fragile"
                                 id="frm-fragile-3"
                                name="fragile" value="false" />
                                Not Fragile
                            </label>
                        </div>
                    </div>
						<div class="control-group">
							<label class="control-label" for="frm-liquid">Dangerous
								Goods Type:</label>
							<div class="controls">
								<form:select path="dangerousGoodsType" id="dangerousGoodsType" name="dangerousGoodsType"
									class="chosen-select" multiple="true">
									
									<c:forEach items="${allDangerousGoodsType}" var="dgType">
										<form:option value="${dgType}">
											${dgType}
											</form:option>
									</c:forEach>
								</form:select>
							</div>
						</div>


						<div class="control-group">
							<label class="control-label" for="frm-weight">Weight:</label>
							<div class="controls">
								<table><tr><td colspan="3"><span> <form:select class="operator" path="weightOperator" name="weightOperator" id="weightOperator">
											<form:option value="">Select...</form:option>
											<form:option value="eq" >=</form:option>
											<form:option value="lt">&lt;</form:option>
											<form:option value="le" >&lt;=</form:option>
											<form:option value="gt">&gt;</form:option>
											<form:option value="ge" >&gt;=</form:option>
											<form:option value="bw">between</form:option>
											<form:option value="nb">not between</form:option>
										</form:select> </span> <span><form:input type="text" path="weight"
											name="weight" id="weight" /> </span> <span id="weightToField">
										and <form:input type="text"  name="weightTo" path="weightTo"
											id="weightTo" /> </span>
								</td></tr></table>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="frm-volume">Volumetric Weight:</label>
							<div class="controls">
								<table><tr><td colspan="3"><span> <form:select class="operator" 
								id ="volumeOperator" name="volumeOperator" path="volumeOperator">
											<form:option value="">Select...</form:option>
											<form:option value="eq" >=</form:option>
											<form:option value="lt">&lt;</form:option>
											<form:option value="le" >&lt;=</form:option>
											<form:option value="gt">&gt;</form:option>
											<form:option value="ge" >&gt;=</form:option>
											<form:option value="bw">between</form:option>
											<form:option value="nb">not between</form:option>
										</form:select> </span> <span><form:input  path="volume" type="text"
								name="volume" id="volume"/> </span> <span id="volumeToField"> and
								<form:input type="text"  path="volumeTo" name="volumeTo" id="volumeTo"/> </span>
								</td></tr></table>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="frm-price">Price:</label>
							<div class="controls">
								<table><tr><td colspan="3"><span> <form:select class="operator"  name="priceOperator" path="priceOperator"
								id="priceOperator">
											<form:option value="">Select...</form:option>
											<form:option value="eq" >=</form:option>
											<form:option value="lt">&lt;</form:option>
											<form:option value="le" >&lt;=</form:option>
											<form:option value="gt">&gt;</form:option>
											<form:option value="ge" >&gt;=</form:option>
											<form:option value="bw">between</form:option>
											<form:option value="nb">not between</form:option>
										</form:select> </span> <span><form:input  type="text" path="price"
								name="price" id="price"/> </span> <span id="priceToField">
								and <form:input name="priceTo" path="priceTo" type="text" id="priceTo"/> </span>
								</td></tr></table>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="frm-price">Enabled:</label>	
							<div class="controls">
								<form:checkbox path="enabled" id="enabled" name="enabled"/>
							</div>
						</div>
                    <div class="control-group">
                        <div class="controls">
                        <button id="frm-editrule-btn" type="button" class="btn btn-primary">Update Rule </button>
                    
                        </div>
                    </div>
                    </form>
                    </div>
                    </form:form>
			</div>
		</div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
	<%-- Activate relevant sidebar function   --%>
	<%-- Activate relevant subheader function --%>
	 <script type="text/javascript" src="${path.js('cocofs/rules.js')}"></script>
   
		<script type="text/javascript">

		function hideSidebar(){
		    $(".subheader").css({"left":"-11%"});
		    $(".functionAnchor").css({"display": "none"});
		    $(".sidebar").css({"left":"1%","width":"99%"});
		    $(".main-content").css({"left":"1%","width":"98%"});
		}
		function showSidebar(){
		    $(".subheader").css({"left":"0%"});
		    $(".functionAnchor").css({"display": "block"});
		    $(".sidebar").css({"left":"12%","width":"88%"});
		    $(".main-content").css({"left":"12%","width":"87%"});
		}

       
        
        
        
        function showErrorAlert(message ) {
            /*<div class="alert alert-error">*/
            console.log(message);
            if ($('#fetch-error-msg').length == 0 ) {
                $('#fetchMessages').append('<div class="alert alert-error"> <button type="button" class="close" data-dismiss="alert">&times;</button> <div id="fetch-error-msg">' + message + '</div></div>');
                
            } else {
                $('#fetch-error-msg').html($('#fetch-error-msg').html() + " <br/>" + message);
            }
        }
        
        function showCreateErrorAlert(message ) {
            /*<div class="alert alert-error">*/
            console.log(message);
        
            if ($('#create-error-msg').length == 0 ) {
                $('#creationMessages').append('<div class="alert alert-error"> <button type="button" class="close" data-dismiss="alert">&times;</button> <div id="create-error-msg">' + message + '</div></div>');
                
            } else {
                $('#create-error-msg').html($('#create-error-msg').html() + " <br/>" + message);
            }
        }
        function showCreateSuccessAlert(message) {
            /*<div class="alert alert-success">*/
            
            console.log(message);
            if ($('#create-success-msg').length == 0 ) {
                $('#creationMessages').append('<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert">&times;</button> <div id="create-success-msg">' + message + '</div></div>');
            } else {
                $('#create-success-msg').html($('#create-success-msg').html() + " <br/>" + message);
            }
        }
        function showSuccessAlert(message) {
            /*<div class="alert alert-success">*/
            
            console.log(message);
            $('#fetchMessages').append('<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert">&times;</button>' + message + '</div>');
            
        }
        
        
        function creationError(categoryurl, jqXHR, textStatus, errorThrown ) {
            console.log(categoryurl);
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown); 
            showCreateErrorAlert("<strong>" + categoryurl + "</strong> : Rule for category with url " + categoryurl 
                    + " not created, error : "+  errorThrown + " " );
        }
        function successHandler( categoryurl, data, textStatus,  jqXHR ) {
            console.log(categoryurl);
            console.log(data);
            console.log(textStatus);
            console.log(jqXHR); 
            if ( data.status == 'FAIL') {
                showCreateErrorAlert("<strong>" + categoryurl+  "</strong> : Rule for category with url " + categoryurl 
                        + " not created, error : " + data.message );
            } else {
                showCreateSuccessAlert("<strong>" + categoryurl +  "</strong> : Rule for category with url " + categoryurl 
                        + " updated successfully " );
            }

        }
        
        
        function activateDateFields (idx, ruleList ) {
            var ridx = 0;
            for (ridx = 0 ; ridx < ruleList.length; ridx = ridx + 1 ) {
                var id = 'idx-'+idx+'-'  + ridx;
                var rule = ruleList[ridx];
                $('#frm-startdate-'+id+'').datepicker({
                    arrows: false,
                    presets:{specificDate: 'Pick a date'},
                    presetRanges:[],
                    showOn: "button",
              buttonImage: "/static/cocofs/img/calendar.jpg",
              buttonImageOnly: false,
              dateFormat:'yy-mm-dd'
                  });
                $( '#frm-startdate-'+id+'' ).datepicker( "setDate", new Date(rule.startDate));
                $('#frm-enddate-'+id+'').datepicker({
                    arrows: false,
                    presets:{specificDate: 'Pick a date'},
                    presetRanges:[],
                    showOn: "button",
              buttonImage: "/static/cocofs/img/calendar.jpg",
              buttonImageOnly: false,
              dateFormat:'yy-mm-dd'
                  });
                if ( rule.endDate != null) {
                    $( '#frm-enddate-'+id+'' ).datepicker( "setDate", new Date(rule.endDate));
                }
            }
        }
        function validateQuantitativeAttr(type){
			if($('#'+type+'Operator').val().trim()!=''){
				if($('#'+type).val().trim()==''|| (($('#'+type+'Operator').val().trim()=='bw'||$('#'+type+'Operator').val().trim()=='nb') && $('#'+type+'To').val().trim()=='' )){
					alert("You have selected "+type+" operator as "+$('#'+type+'Operator'+ ' option:selected').text()+" without appropriate value.Please correct this");
					return false;
				}
				
				if($('#'+type).val().trim()=='' && !$.isNumeric($('#'+type).val().trim()) ){
					alert("Enter a valid value for " + type );
					return false;
				}
				
				if(  $('#'+type).val().trim()!=''    && $('#'+type+'To').val().trim()!=''    &&  ( eval($('#'+type).val().trim()) > eval($('#'+type+'To').val().trim()) ) ){
					alert("please input a valid input range for  " + type);
					return false;
				}
			}
			return true;
		}
        
        function isInvalid(start, end, startDateObj, endDateObj, ruleCreation ) {
        	
        	if(!validateQuantitativeAttr("price")) return true;
    		if(!validateQuantitativeAttr("weight")) return true;
    		if(!validateQuantitativeAttr("volume")) return true;

        	
            
            if ( start.length == 0 ) {
                alert("Start date not specified ");
                return true;
            }
            if ( (end.length != 0) && end < start ) {
                alert("End date before Start date ");
                return true;
            }
            
            if (! (start == $.datepicker.formatDate('yy-mm-dd', startDateObj))) {
                alert("Start Date appears to be incorrect");
                return true;
            }
            if ((end.length != 0) &&  (end != $.datepicker.formatDate('yy-mm-dd', endDateObj))) {
                alert("End Date appears to be incorrect");
                return true;
            }
            
           /*  if ( ruleCreation ) {
            	var x = $.datepicker.formatDate('yy-mm-dd', new Date())
                if ( $('#frm-startdate').val() < x ) {
                	alert('Start date before today not allowed'); 
                	return true;
                }
            } */
            
            
            return false;
        }
        
        
        
        function hideSpinnerIfDone(overallProgressList, spinner ) {
            if ( overallProgressList.length == 0 ) {
                spinner.stop();
                $('#spinnerDiv').html('');
            }
            
        }
        
        function editRuleForCategoryAndType(ruleCode, ruleName,  categoryurl , start, end, overallProgressList, spinner ) {
        	 var enabled = $("#enabled").is(":checked");
             var dangerousGoodsType = $("#dangerousGoodsType").val();
        	 var fragile = $("input:radio[name=fragile]:checked").val();
        	 var weight = $("#weight").val().trim();
        	 var weightTo = $("#weightTo").val().trim();
        	 var weightOperator = $("#weightOperator").val().trim();
             var volume = $("#volume").val().trim();
             var volumeTo = $("#volumeTo").val().trim();
             var volumeOperator = $("#volumeOperator").val().trim();
             var price = $("#price").val().trim();
             var priceTo = $("#priceTo").val().trim();
             var priceOperator = $("#priceOperator").val().trim();
             
                console.log(" categoryurl:" + categoryurl + " fragile:" + fragile + " weight:" + weight + " volume:" + volume + " price:" + price 
                		+ " weightTo:" + weightTo + " volumeTo:" + volumeTo + " priceTo:" + priceTo 
                		+ " start:" + start +  " end:" + end );
                
                overallProgressList.unshift(categoryurl);
                
			var pdata = {
					'previousRuleCode' : ruleCode,
					'ruleName' : ruleName,
					'categoryurl' : categoryurl,
					'fragile' : fragile,
					'weight' : weight,
					'volume' : volume,
					'price' : price,
					'weightTo' : weightTo,
					'volumeTo' : volumeTo,
					'priceTo' : priceTo,
					'weightOperator' : weightOperator,
					'volumeOperator' : volumeOperator,
					'priceOperator' : priceOperator,
					'dangerousGoodsType':dangerousGoodsType,
					'enabled' : enabled,
					'startdate' : start,
					'enddate' : end
				};
				$
						.ajax({
							dataType : 'json',
							url : "/admin/rules/giftwrap/category/createRule",
							data : pdata,
							type : 'POST',
							success : function(data, textStatus, jqXHR) {
								successHandler(categoryurl, data, textStatus,
										jqXHR);
								overallProgressList.shift();
								hideSpinnerIfDone(overallProgressList, spinner);
							},
							error : function(jqXHR, textStatus, errorThrown) {
								if (jqXHR.status == 200) {
									alert("Your session has expired. Please log in again in another tab/window and then retry here.");
								}
								creationError(categoryurl, jqXHR, textStatus,
										errorThrown);
								overallProgressList.shift();
								hideSpinnerIfDone(overallProgressList, spinner);
							}
						});

			}

			var spinopts = {
				lines : 13, // The number of lines to draw
				length : 40, // The length of each line
				width : 10, // The line thickness
				radius : 50, // The radius of the inner circle
				corners : 1, // Corner roundness (0..1)
				rotate : 0, // The rotation offset
				direction : 1, // 1: clockwise, -1: counterclockwise
				color : '#000', // #rgb or #rrggbb or array of colors
				speed : 1, // Rounds per second
				trail : 60, // Afterglow percentage
				shadow : false, // Whether to render a shadow
				hwaccel : false, // Whether to use hardware acceleration
				className : 'spinner', // The CSS class to assign to the spinner
				zIndex : 2e9, // The z-index (defaults to 2000000000)
				top : 'auto', // Top position relative to parent in px
				left : 'auto' // Left position relative to parent in px
			};

			var ruleCreationInProgress = false;
			function editRule() {
				var spinner;
				if (ruleCreationInProgress) {
					return;
				} else {

					/* clean up message */
					$('#creationMessages').html('');
				}

				var start = $("#frm-startdate").val().trim();
				var end = $("#frm-enddate").val().trim();
				var categoryUrl = $("#frm-categoryurl").val().trim();
				var ruleCode = $("#frm-rulecode").val().trim();
				var ruleName = $("#frm-rulename").val().trim();

				if (isInvalid(start, end, $("#frm-startdate").datepicker(
						'getDate'), $("#frm-enddate").datepicker('getDate'),
						true)) {
					console.log("invalid date");
					return;

				} else {
					ruleCreationInProgress = true;
					/* show ajax going symbol */
					/* note spin js doesn't use jqueyr element selection */
					var target = document.getElementById('spinnerDiv');
					spinner = new Spinner(spinopts).spin(target);
					var overallProgressList = []

					editRuleForCategoryAndType(ruleCode, ruleName ,
							categoryUrl, start, end, overallProgressList,
							spinner)

					hideAndResetCreationFormValues();

					ruleCreationInProgress = false;
				}

			}/* End of editRule() */

			function resetCreationFormValues() {
				$('#frm-startdate').datepicker('setDate', new Date());
				$('#frm-enddate').datepicker('setDate', null);
				$('#frm-fragile-1').prop('checked', true);
				$('#frm-weight').val(null);
				$('#frm-volume').val(null);
				$('#frm-price').val(null);

			}
			function hideAndResetCreationFormValues() {
				resetCreationFormValues();
				$('#creationdiv').hide();

			}
			$(document).ready(function() {
				var subheader = true;
				$(".subheaderToggle").click(function() {
					if (subheader) {
						hideSidebar();
						$(".subheaderToggleContent").html(">");
						subheader = false;
					} else {
						showSidebar();
						$(".subheaderToggleContent").html("<");
						subheader = true;
					}

				});
				/* check sidebar jsp for relevant div ids
				 */
				$('#sidebar-gw-ruleview').addClass('active');
				$('#header-nav-rules').addClass('active');

				

				$('#frm-clearrule-btn').click(function() {
					$('#resultContainer').html('');
					$('#fetchMessages').html('');

				});

				$('#frm-editrule-btn').click(function() {

					editRule();

				});

				$('#frm-startdate').datepicker({
					arrows : false,
					presets : {
						specificDate : 'Pick a date'
					},
					presetRanges : [],
					showOn : "button",
					minDate : 0,
					buttonImage : "/static/cocofs/img/calendar.jpg",
					buttonImageOnly : false,
					dateFormat : 'yy-mm-dd'
				});
				$('#frm-enddate').datepicker({
					arrows : false,
					presets : {
						specificDate : 'Pick a date'
					},
					presetRanges : [],
					showOn : "button",
					buttonImage : "/static/cocofs/img/calendar.jpg",
					buttonImageOnly : false,
					dateFormat : 'yy-mm-dd'
				});
				$('.chosen-select').chosen({
					"display_disabled_options" : false,
					"display_selected_options" : false
				});

			});
		</script>
			
		
	</tiles:putAttribute>
</tiles:insertTemplate>
