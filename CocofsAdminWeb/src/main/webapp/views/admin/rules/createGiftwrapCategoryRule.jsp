<%@ include file="/tld_includes.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<tiles:insertTemplate template="/views/layout/base.jsp">
	<tiles:putAttribute name="title" value="Rules Admin" />

	<tiles:putAttribute name="systemMessage">
		<div class="system-message"></div>
	</tiles:putAttribute>

	<tiles:putAttribute name="extraScripts">
        <link href="${path.css('chosen/chosen.css')}" rel="stylesheet"
    type="text/css" />

<style>
.chosen-container{
width: 22% !important;
}
.search-field .default{
width: 158px !important;
}


</style>

    <script type="text/javascript"
    src="${path.js('chosen/chosen.jquery.min.js')}"></script>
    
    <script type="text/javascript"
    src="${path.js('jquery/jquery.ui.datepicker.min.js')}"></script>
    <script type="text/javascript"
    src="${path.js('spin/spin.min.js')}"></script>
    
	</tiles:putAttribute>
	<tiles:putAttribute name="sidebar">
		<tiles:insertTemplate template="/views/admin/rules/sidebar.jsp">
		</tiles:insertTemplate>
	</tiles:putAttribute>
	<tiles:putAttribute name="systemMessage">
		<div class="system-message">
			<input type="hidden" value="${message }" id="messageHiddenField"/>
		</div>
	</tiles:putAttribute>


	<tiles:putAttribute name="subheader">
		<tiles:insertTemplate template="/views/admin/rules/view-subheader.jsp">
		</tiles:insertTemplate>
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
		<div class="main-content lfloat">
			<div class="container" style="width: 90%;">
			 <form class="form-horizontal">
                    <div class="control-group">
                        <label class="control-label"
                            for="frm-categoryurl">Category</label>
                        <div class="controls">
                        
                        <select id="frm-categoryurl" name="frm-categoryurl" class="chosen-select" multiple>
                                <c:forEach items="${categoryUrls}"
                                    var="catUrl">
                                    
                                    <option value="${catUrl.key}">${catUrl.value}

                                    </option>
                                </c:forEach>
                        </select>
                        </div>
                    </div>
                    
                    <div class="control-group">
                    <div class="controls">
                    <button id="frm-fetchrule-btn" type="button" class="btn btn-primary">Fetch Rules </button>
                    <button id="frm-clearrule-btn" type="button" class="btn btn-primary">Clear Results </button>
                    </div>
                    </div>
                    <div id="fetchMessages">
                    </div>
                    <div id="spinnerDiv">
                    </div>
                    </form>
                    <div class="accordion" id="resultContainer">
                    
                    </div>
                    <div></div>
                    
                    <div id="creationMessages">
                    </div>
                    <div id="creationdiv">
                    
                    <form class="form-horizontal">
                    <h4>Create Rules for fetched categories</h4>
                    <div class="control-group">
                        
                        <label class="control-label"
                            for="frm-startdate">Start Date</label>
                        <div class="controls">
                            <input type="text" id="frm-startdate" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"
                            for="frm-enddate">End  Date</label>
                        <div class="controls">
                            <input type="text" id="frm-enddate" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"
                            for="frm-fragile">Fragile</label>
                        <div class="controls">
                            <label class="radio inline"> <input
                                type="radio" id="frm-fragile-1"
                                name="frm-fragile" value="" checked />
                                Any
                            </label> <label class="radio inline"> <input
                                type="radio" id="frm-fragile-2"
                                name="frm-fragile" value="true" />
                                Fragile
                            </label> <label class="radio inline"> <input
                                type="radio" id="frm-fragile-3"
                                name="frm-fragile" value="false" />
                                Not Fragile
                            </label>
                        </div>
                    </div>
						<div class="control-group">
							<label class="control-label" for="frm-liquid">Dangerous
								Goods Type:</label>
							<div class="controls">
								<select id="dangerousGoodsType" name="dangerousGoodsType"
									class="chosen-select" multiple="multiple">
									<c:forEach items="${allDangerousGoodsType}" var="dgType">
										<option value="${dgType}">
											${dgType}
											</option>
									</c:forEach>
								</select>
							</div>
						</div>


						<div class="control-group">
							<label class="control-label" for="frm-weight">Weight:</label>
							<div class="controls">
								<table><tr><td colspan="3"><span> <select class="operator" name="weightOperator" id="weightOperator">
											<option value="">Select...</option>
											<option value="eq" >=</option>
											<option value="lt">&lt;</option>
											<option value="le" >&lt;=</option>
											<option value="gt">&gt;</option>
											<option value="ge" >&gt;=</option>
											<option value="bw">between</option>
											<option value="nb">not between</option>
										</select> </span> <span><input type="text" 
											name="weight" id="weight" /> </span> <span id="weightToField">
										and <input type="text"  name="weightTo"
											id="weightTo" /> </span>
								</td></tr></table>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="frm-volume">Volumetric Weight:</label>
							<div class="controls">
								<table><tr><td colspan="3"><span> <select class="operator" 
								id ="volumeOperator" name="volumeOperator">
											<option value="">Select...</option>
											<option value="eq" >=</option>
											<option value="lt">&lt;</option>
											<option value="le" >&lt;=</option>
											<option value="gt">&gt;</option>
											<option value="ge" >&gt;=</option>
											<option value="bw">between</option>
											<option value="nb">not between</option>
										</select> </span> <span><input  type="text"
								name="volume" id="volume"/> </span> <span id="volumeToField"> and
								<input type="text"  name="volumeTo" id="volumeTo"/> </span>
								</td></tr></table>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="frm-price">Price:</label>
							<div class="controls">
								<table><tr><td colspan="3"><span> <select class="operator"  name="priceOperator"
								id="priceOperator">
											<option value="">Select...</option>
											<option value="eq" >=</option>
											<option value="lt">&lt;</option>
											<option value="le" >&lt;=</option>
											<option value="gt">&gt;</option>
											<option value="ge" >&gt;=</option>
											<option value="bw">between</option>
											<option value="nb">not between</option>
										</select> </span> <span><input  type="text"
								name="price" id="price"/> </span> <span id="priceToField">
								and <input name="priceTo" type="text" id="priceTo"/> </span>
								</td></tr></table>
							</div>
						</div>
                    <div class="control-group">
                        <div class="controls">
                        <button id="frm-createrule-btn" type="button" class="btn btn-primary">Create Rule </button>
                    
                        </div>
                    </div>
                    </form>
                    </div>
			</div>
		</div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
	<%-- Activate relevant sidebar function   --%>
	<%-- Activate relevant subheader function --%>
	 <script type="text/javascript" src="${path.js('cocofs/rules.js')}"></script>
   
		<script type="text/javascript">

		function hideSidebar(){
		    $(".subheader").css({"left":"-11%"});
		    $(".functionAnchor").css({"display": "none"});
		    $(".sidebar").css({"left":"1%","width":"99%"});
		    $(".main-content").css({"left":"1%","width":"98%"});
		}
		function showSidebar(){
		    $(".subheader").css({"left":"0%"});
		    $(".functionAnchor").css({"display": "block"});
		    $(".sidebar").css({"left":"12%","width":"88%"});
		    $(".main-content").css({"left":"12%","width":"87%"});
		}

        function buildCatDisplayStart(idx, catName) {
            var str = '';
            str += '<div id="result-accordian-group-'+idx+'" class="accordion-group">';
            str += '<div class="accordion-heading">';
            str += '<a class="accordion-toggle" data-toggle="collapse" data-parent="#resultContainer" href="#collapse-'+idx+'">';
            str += '<strong>View/Edit - ' + catName +  '</strong>';
            str += '</a>';
            str += '</div>';
            str += '<div id="collapse-'+idx+'" class="accordion-body collapse">';
            str += '<div class="accordion-inner">';
            str += '<span class="label label-info">'+catName+'</span>';
            
            return str;
        }
        function buildCatDisplayEnd(idx, catName) {
            var str = "</div></div></div>";
            return str;
        }
        
        
        function buildCatRuleDisplay(idx, ruleList, catName, categoryurl) {
            var str = "<div>";
            str += '<table class="table table-bordered">';
            str += '<thead>';
            str += '<tr>';
            str += '<th>Name</th><th>StartDate</th><th>EndDate</th><th>Enabled</th>';
            str += '</tr>';
            str += '</thead>';
            str += '<tbody>';
            var ridx = 0;
            for (ridx = 0 ; ridx < ruleList.length; ridx = ridx + 1 ) {
                str += showRuleData('idx-'+idx+'-' + ridx, ruleList[ridx] , catName, categoryurl);
            }
            str += '</tbody>';
            str += '</table>';
            str += "</div>";
            return str;
        }
        
        
      
        
        function showRuleData(idx, rule, catName, categoryurl) {
            var str = "<tr id='row-" + idx + "'>";
            str += '<td>' + '<a class="grid-link" href="/admin/rules/editGiftwrapRule?ruleName=' + encodeURIComponent(rule.name) + '&ruleCode=' + encodeURIComponent(rule.ruleCode) +'" target="_blank" >' + rule.name + '</a>';
            str += '</td>';
            str += '<td>';
            str += '<input class="input-small" readonly="true" type="text" id="frm-startdate-'+idx+'" value="'+rule.startDate+'"/>'; 
            str += '</td><td>';
            str += '<input class="input-small" readonly="true" type="text" id="frm-enddate-'+idx+'" '; 
           
            //end date can be null
            if(rule.endDate != null){
            	str += 'value="'+rule.endDate+'"/>';
            }else{
            	str += '/>';
            }
            str += '</td>';
            
            
           
            str += '<td>';
            str += '<input disabled type="checkbox" id="frm-enabled-'+idx+'" ';
            if ( rule.enabled == true) {
                    str += ' checked="checked" ';
                    str +=  'value="enabled" ';
            } else {
                    str +=  'value="notenabled" ';
            }
            str += '/>';
            str += '</td>';
            str += '</tr>';
            return str;
        }
        
       
        
        function showErrorAlert(message ) {
            /*<div class="alert alert-error">*/
            console.log(message);
            if ($('#fetch-error-msg').length == 0 ) {
                $('#fetchMessages').append('<div class="alert alert-error"> <button type="button" class="close" data-dismiss="alert">&times;</button> <div id="fetch-error-msg">' + message + '</div></div>');
                
            } else {
                $('#fetch-error-msg').html($('#fetch-error-msg').html() + " <br/>" + message);
            }
        }
        
        function showCreateErrorAlert(message ) {
            /*<div class="alert alert-error">*/
            console.log(message);
        
            if ($('#create-error-msg').length == 0 ) {
                $('#creationMessages').append('<div class="alert alert-error"> <button type="button" class="close" data-dismiss="alert">&times;</button> <div id="create-error-msg">' + message + '</div></div>');
                
            } else {
                $('#create-error-msg').html($('#create-error-msg').html() + " <br/>" + message);
            }
        }
        function showCreateSuccessAlert(message) {
            /*<div class="alert alert-success">*/
            
            console.log(message);
            if ($('#create-success-msg').length == 0 ) {
                $('#creationMessages').append('<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert">&times;</button> <div id="create-success-msg">' + message + '</div></div>');
            } else {
                $('#create-success-msg').html($('#create-success-msg').html() + " <br/>" + message);
            }
        }
        function showSuccessAlert(message) {
            /*<div class="alert alert-success">*/
            
            console.log(message);
            $('#fetchMessages').append('<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert">&times;</button>' + message + '</div>');
            
        }
        
        function getCategoryName(categoryurl) {
            var name ;
            $('select[name="frm-categoryurl"] > option ').each(function() {if ($(this).val() == categoryurl){name =$(this).text().trim();} });
            return name;
        }
        
        function creationError(categoryurl, jqXHR, textStatus, errorThrown ) {
            console.log(categoryurl);
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown); 
            showCreateErrorAlert("<strong>" + getCategoryName(categoryurl) + "</strong> : Rule for category with url " + categoryurl 
                    + " not created, error : "+  errorThrown + " " );
        }
        function successHandler( categoryurl, data, textStatus,  jqXHR ) {
            console.log(categoryurl);
            console.log(data);
            console.log(textStatus);
            console.log(jqXHR); 
            if ( data.status == 'FAIL') {
                showCreateErrorAlert("<strong>" + getCategoryName(categoryurl) +  "</strong> : Rule for category with url " + categoryurl 
                        + " not created, error : " + data.message );
            } else {
                showCreateSuccessAlert("<strong>" + getCategoryName(categoryurl) +  "</strong> : Rule for category with url " + categoryurl 
                        + " created successfully " );
            }

        }
        
        function fetchError(categoryurl, jqXHR, textStatus, errorThrown ) {
            console.log(categoryurl);
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown); 
            showErrorAlert("<strong>" + getCategoryName(categoryurl) + "</strong> : Rule for category with url " + categoryurl 
                    + " not found, error : "+  errorThrown + " " );
        }
        function fetchSuccessHandler( idx, categoryurl, data, textStatus,  jqXHR ) {
            console.log(categoryurl);
            console.log(data);
            console.log(textStatus);
            console.log(jqXHR); 
            var catName = getCategoryName(categoryurl);
            var cathtml = '';
            if ( data.status == 'FAIL') {
                showErrorAlert("<strong>" + catName +  "</strong> : Rule for category with url " + categoryurl 
                        + " not found, error : " + data.message );
                
                cathtml += buildCatDisplayStart(idx, catName);
                cathtml += buildCatDisplayEnd(idx, catName);
            } else {
                console.log(data.items.ruleList);
                cathtml += buildCatDisplayStart(idx, catName);
                cathtml += buildCatRuleDisplay(idx, data.items.ruleList, catName, categoryurl);
                
                cathtml += buildCatDisplayEnd(idx, catName);
                
                /* showSuccessAlert("<strong>" + getCategoryName(categoryurl) +  "</strong> : Rule for category with url " + categoryurl 
                        + " found successfully " ); */
            }

            var container = $('#resultContainer');
            container.append(cathtml);
           
            
        }
        function activateDateFields (idx, ruleList ) {
            var ridx = 0;
            for (ridx = 0 ; ridx < ruleList.length; ridx = ridx + 1 ) {
                var id = 'idx-'+idx+'-'  + ridx;
                var rule = ruleList[ridx];
                $('#frm-startdate-'+id+'').datepicker({
                     arrows: false,
                    presets:{specificDate: 'Pick a date'},
                    presetRanges:[],
                    showOn: "button",
              buttonImage: "/static/cocofs/img/calendar.jpg",
              buttonImageOnly: false, 
              dateFormat:'yy-mm-dd'
                  }); 
                $( '#frm-startdate-'+id+'' ).datepicker( "setDate", new Date(rule.startDate));
                $('#frm-enddate-'+id+'').datepicker({
                    arrows: false,
                    presets:{specificDate: 'Pick a date'},
                    presetRanges:[],
                    showOn: "button",
              buttonImage: "/static/cocofs/img/calendar.jpg",
              buttonImageOnly: false,
              dateFormat:'yy-mm-dd'
                  });
                if ( rule.endDate != null) {
                    $( '#frm-enddate-'+id+'' ).datepicker( "setDate", new Date(rule.endDate));
                }
            }
        }
        function validateQuantitativeAttr(type){
			if($('#'+type+'Operator').val().trim()!=''){
				if($('#'+type).val().trim()==''|| (($('#'+type+'Operator').val().trim()=='bw'||$('#'+type+'Operator').val().trim()=='nb') && $('#'+type+'To').val().trim()=='' )){
					alert("You have selected "+type+" operator as "+$('#'+type+'Operator'+ ' option:selected').text()+" without appropriate value.Please correct this");
					return false;
				}
				
				if($('#'+type).val().trim()=='' && !$.isNumeric($('#'+type).val().trim()) ){
					alert("Enter a valid value for " + type );
					return false;
				}
				
				if(  $('#'+type).val().trim()!=''    && $('#'+type+'To').val().trim()!=''    &&  ( eval($('#'+type).val().trim()) > eval($('#'+type+'To').val().trim()) ) ){
					alert("please input a valid input range for  " + type);
					return false;
				}
			}
			return true;
		}
        
        function isInvalid(start, end, startDateObj, endDateObj, ruleCreation ) {
        	
        	if(!validateQuantitativeAttr("price")) return true;
    		if(!validateQuantitativeAttr("weight")) return true;
    		if(!validateQuantitativeAttr("volume")) return true;

        	/* if ( (weight.length != 0) && !$.isNumeric(weight)) {
                alert("Enter a valid number for weight ");
                return true;
            }
            if ( (volume.length != 0) && !$.isNumeric(volume)) {
                alert("Enter a valid number for volumetric weight ");
                return true;
            }
            if ($.isNumeric(weight) && (weight < 0)) {
                alert("Weight needs to be >= 0 ");
                return true;
            }
            if ($.isNumeric(volume) && (volume < 0)) {
                alert("Volumetric Weight needs to be >= 0 ");
                return true;
            } */
            
            if ( start.length == 0 ) {
                alert("Start date not specified ");
                return true;
            }
            if ( (end.length != 0) && end < start ) {
                alert("End date before Start date ");
                return true;
            }
            
            if (! (start == $.datepicker.formatDate('yy-mm-dd', startDateObj))) {
                alert("Start Date appears to be incorrect");
                return true;
            }
            if ((end.length != 0) &&  (end != $.datepicker.formatDate('yy-mm-dd', endDateObj))) {
                alert("End Date appears to be incorrect");
                return true;
            }
            
            if ( ruleCreation ) {
            	var x = $.datepicker.formatDate('yy-mm-dd', new Date())
                if ( $('#frm-startdate').val() < x ) {
                	alert('Start date before today not allowed'); 
                	return true;
                }
            }
            
            
            return false;
        }
        
     
        
        function hideSpinnerIfDone(overallProgressList, spinner ) {
            if ( overallProgressList.length == 0 ) {
                spinner.stop();
                $('#spinnerDiv').html('');
            }
            
        }
        
        function creatRuleForCategoryAndType( categoryurl , start, end, overallProgressList, spinner ) {
            var dangerousGoodsType = $("#dangerousGoodsType").val();
			
        	var fragile = $("input:radio[name=frm-fragile]:checked").val();

        	 var weight = $("#weight").val().trim();
        	 var weightTo = $("#weightTo").val().trim();
        	 var weightOperator = $("#weightOperator").val().trim();
             var volume = $("#volume").val().trim();
             var volumeTo = $("#volumeTo").val().trim();
             var volumeOperator = $("#volumeOperator").val().trim();
             var price = $("#price").val().trim();
             var priceTo = $("#priceTo").val().trim();
             var priceOperator = $("#priceOperator").val().trim();
             
                console.log(" categoryurl:" + categoryurl + " fragile:" + fragile + " weight:" + weight + " volume:" + volume + " price:" + price 
                		+ " weightTo:" + weightTo + " volumeTo:" + volumeTo + " priceTo:" + priceTo 
                		+ " start:" + start +  " end:" + end );
                
                overallProgressList.unshift(categoryurl);
                var pdata = {
                        'categoryurl' : categoryurl ,
                        'fragile'      : fragile,
                        'weight'       : weight ,
                        'volume'    : volume,
                        'price'    : price,
                        'weightTo'       : weightTo ,
                        'volumeTo'    : volumeTo,
                        'priceTo'    : priceTo,
                        'weightOperator'       : weightOperator ,
                        'volumeOperator'    : volumeOperator,
                        'priceOperator'    : priceOperator,
                        'dangerousGoodsType':dangerousGoodsType,
                        'startdate'    : start ,
                        'enddate'      : end
                };
                $.ajax( {dataType:'json' , url:"/admin/rules/giftwrap/category/createRule" , 
                    data: pdata, type : 'POST' , 
                    success: function(data, textStatus,  jqXHR ) {successHandler(categoryurl, data, textStatus,  jqXHR ); overallProgressList.shift(); hideSpinnerIfDone(overallProgressList, spinner);} ,
                    error : function(jqXHR, textStatus, errorThrown) { 
                    	if(jqXHR.status==200){
            	    		  alert("Your session has expired. Please log in again in another tab/window and then retry here.");
            	    	  }
                    	creationError(categoryurl, jqXHR, textStatus, errorThrown ); overallProgressList.shift(); hideSpinnerIfDone(overallProgressList, spinner );}  
                    });
                
                
            }
        
        function fetchRuleForCategoryAndType(idx, categoryurl, overallProgressList, spinner ) {
                console.log(categoryurl );
                overallProgressList.unshift(categoryurl);
                var pdata = {
                        'categoryurl' : categoryurl 
                };
                $.ajax( {dataType:'json' , url:"/admin/rules/findGiftWrapRuleForCategory" , 
                    async : false ,
                    data: pdata, type : 'POST' , 
                    success: function(data, textStatus,  jqXHR ) {fetchSuccessHandler(idx, categoryurl, data, textStatus,  jqXHR ); overallProgressList.shift(); hideSpinnerIfDone(overallProgressList, spinner);} ,
                    error : function(jqXHR, textStatus, errorThrown) { 
                    	if(jqXHR.status==200){
            	    		  alert("Your session has expired. Please log in again in another tab/window and then retry here.");
            	    	  }
                    	fetchError(categoryurl, jqXHR, textStatus, errorThrown ); overallProgressList.shift(); hideSpinnerIfDone(overallProgressList, spinner );}  
                    });
                
                
            }
        var spinopts = {
                lines: 13, // The number of lines to draw
                length: 40, // The length of each line
                width: 10, // The line thickness
                radius: 50, // The radius of the inner circle
                corners: 1, // Corner roundness (0..1)
                rotate: 0, // The rotation offset
                direction: 1, // 1: clockwise, -1: counterclockwise
                color: '#000', // #rgb or #rrggbb or array of colors
                speed: 1, // Rounds per second
                trail: 60, // Afterglow percentage
                shadow: false, // Whether to render a shadow
                hwaccel: false, // Whether to use hardware acceleration
                className: 'spinner', // The CSS class to assign to the spinner
                zIndex: 2e9, // The z-index (defaults to 2000000000)
                top: 'auto', // Top position relative to parent in px
                left: 'auto' // Left position relative to parent in px
              };
        var ruleFetchInProgress = false;
        var fetchedCats = [];
        function fetchRules() {
            var spinner ;
            if ( ruleFetchInProgress) {
                return;
            } else {
                
                
                /* clean up message */
                $('#fetchMessages').html('');
                $('#resultContainer').html('');
            }
            
            ruleFetchInProgress = true;
                /* show ajax going symbol */
                /* note spin js doesn't use jqueyr element selection */
                var target = document.getElementById('spinnerDiv');
                spinner = new Spinner(spinopts).spin(target);
                var overallProgressList = []
                fetchedCats = [];
                $('select[name="frm-categoryurl"] > option:selected').each(function(idx, value) {
                fetchedCats.unshift($(this).val());
                fetchRuleForCategoryAndType( idx , $(this).val(), overallProgressList, spinner )
                }); 
                ruleFetchInProgress = false;
            

            } /*End of fetchRules*/
            
            
            var ruleCreationInProgress = false;
            function createRules() {
                var spinner ;
                if ( ruleCreationInProgress) {
                    return;
                } else {
                    
                    
                    /* clean up message */
                    $('#creationMessages').html('');
                }
                //var fragile = $("input:radio[name=frm-fragile]:checked").val();
                 
                var start  = $("#frm-startdate").val().trim();
                var end  = $("#frm-enddate").val().trim();
               
                if ( isInvalid(start, end,  $("#frm-startdate").datepicker('getDate'), $("#frm-enddate").datepicker('getDate') , true)  ) {
                    return;
                } else {
                    ruleCreationInProgress = true;
                    /* show ajax going symbol */
                    /* note spin js doesn't use jqueyr element selection */
                    var target = document.getElementById('spinnerDiv');
                    spinner = new Spinner(spinopts).spin(target);
                    var overallProgressList = []
                    var allFound = true;
                    $('select[name="frm-categoryurl"] > option:selected').each(function() {
                        var x = 0;
                        var found = false;
                        for ( x = 0; x < fetchedCats.length ; x++) {
                            if ( fetchedCats[x] == $(this).val() ) {
                                found = true;
                                break;
                            }
                        }
                        if ( !found ) {
                            allFound = false;
                        }
                    });
                    if ( allFound ) {
                        $('select[name="frm-categoryurl"] > option:selected').each(function() {
                            creatRuleForCategoryAndType(  $(this).val()
                                    , start , end, overallProgressList, spinner )});
                        hideAndResetCreationFormValues();
                    } else {
                        alert("Please press fetch again to fetch rules for selected categories");
                        spinner.stop();
                        $('#spinnerDiv').html('')
                    }
                    ruleCreationInProgress = false;
                }

                    }/* End of createRules() */
            
		    function resetCreationFormValues() {
		    	$('#frm-startdate').datepicker('setDate', new Date());
		    	$('#frm-enddate').datepicker('setDate',null);
		    	$('#frm-fragile-1').prop('checked',true);
		    	$('#frm-liquid-1').prop('checked',true);
		    	$('#frm-hazmat-1').prop('checked',true);
		    	$('#frm-weight').val(null);
		    	$('#frm-volume').val(null);
		    	
            }
            function hideAndResetCreationFormValues() {
                 resetCreationFormValues();
                 $('#creationdiv').hide();
            	
            }
			$(document).ready(function() {
				var subheader=true;
				 $(".subheaderToggle").click(function(){
				         if(subheader){
				                 hideSidebar();
				                 $(".subheaderToggleContent").html(">");
				                 subheader=false;
				         }else{
				                 showSidebar();
				                 $(".subheaderToggleContent").html("<");
				                 subheader=true;
				         }

				 });
				/* check sidebar jsp for relevant div ids
				*/
				$('#sidebar-gw-ruleview').addClass('active');
				$('#header-nav-rules').addClass('active');
                
				/* check subheader jsp for relevant div ids
				*/
				/* $('#subheader-fn-usrmanagement').addClass('active') */
				/*findRuleForCategoryAndType*/
				
                
                $('#frm-fetchrule-btn').click(function() {
                	if ( $('select[name="frm-categoryurl"] > option:selected').length < 1 ) {
                		alert('Select atleast one category');
                		return ;
                	}
                    fetchRules();
                    $('#creationdiv').show();
                });
                
                $('#frm-clearrule-btn').click(function() {
                	$('#resultContainer').html('');
                	$('#fetchMessages').html('');
                	
                });
                
                
                $('#frm-createrule-btn').click(function() {
                    if ( $('select[name="frm-categoryurl"] > option:selected').length < 1 ) {
                        alert('Select atleast one category');
                        return ;
                    }
                    createRules();
                    
                });
                $('#frm-startdate').datepicker({
                    arrows: false,
                    presets:{specificDate: 'Pick a date'},
                    presetRanges:[],
                    showOn: "button",
                    minDate: 0 ,
              buttonImage: "/static/cocofs/img/calendar.jpg",
              buttonImageOnly: false,
              dateFormat:'yy-mm-dd'
                  });
                $('#frm-startdate').datepicker('setDate', new Date());
                $('#frm-enddate').datepicker({
                    arrows: false,
                    presets:{specificDate: 'Pick a date'},
                    presetRanges:[],
                    showOn: "button",
              buttonImage: "/static/cocofs/img/calendar.jpg",
              buttonImageOnly: false,
              dateFormat:'yy-mm-dd'
                  });
                $('#creationdiv').hide();
                $('.chosen-select').chosen({"display_disabled_options":false, "display_selected_options":false});
                
                


			});
			
		
		
		

		</script>
			
		
	</tiles:putAttribute>
</tiles:insertTemplate>
