<%@ include file="/tld_includes.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<tiles:insertTemplate template="/views/layout/base.jsp">
	<tiles:putAttribute name="title" value="Supc rule create/update" />

	<tiles:putAttribute name="systemMessage">
		<div class="system-message"></div>
	</tiles:putAttribute>

	<tiles:putAttribute name="sidebar">
		<tiles:insertTemplate template="/views/admin/packman/rules/rule-subheader.jsp">
			<tiles:putAttribute name="active" value="createrule" />
		</tiles:insertTemplate>
	</tiles:putAttribute>

	<tiles:putAttribute name="extraScripts">
<link href="${path.css('chosen/chosen.css')}" rel="stylesheet"
    type="text/css" />
    <link rel="stylesheet" type="text/css"
            href="${path.css('snapdeal/ui.jqgrid.css')}" />
            
    <script type="text/javascript"
    src="${path.js('chosen/chosen.jquery.min.js')}"></script>
    
    <script type="text/javascript"
    src="${path.js('snapdeal/validate.js')}"></script>
    
    		<script type="text/javascript"
			src="${path.js('jquery/jquery.ui.datepicker.min.js')}"></script>
    
	</tiles:putAttribute>
	<tiles:putAttribute name="systemMessage">
		<div class="system-message">
			<input type="hidden" value="${message }" id="messageHiddenField"/>
		</div>
		<div class="system-message">
			<input type="hidden" value="${validationResponse}" id="validationResponseHiddenField"/>
		</div>
	</tiles:putAttribute>

	<tiles:putAttribute name="subheader">
		<tiles:putAttribute name="systemMessage">
		</tiles:putAttribute>		
	 <tiles:insertTemplate template="/views/admin/packman/sidebar.jsp">
			<tiles:putAttribute name="active" value="packman" />
		</tiles:insertTemplate> 
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
		<div class="main-content_packman lfloat">
			<div class="container" style="width: 85%;">
				
			<form id="supc-rule-form" class="form-horizontal" method="post" action="">
				
							  <input type="text" style="display:none" name="pageId" value="${pageId}">
				
				
			  <div class="control-group">
			    <label class="control-label" for="Rule-Type">Rule Type</label>
			    <div class="controls" id="Rule-Type">
				    <select name="RuleType"  id="RuleType" required="required" <c:if test="${StoreCreateRuleDTO.ruleName != null}"> disabled </c:if>>
 								<option value="rulesCreate">Select Rule Type</option>
								<option value="storeRuleCreate">Store</option>
								<option value="categoryRuleCreate">Category</option>
								<option value="subcategoryRuleCreate">Subcategory</option>
								<option value="rulesCreate" selected>Supc</option>
				    </select>
			    </div>
			    </div>
			    
			    <c:if test="${StoreCreateRuleDTO.ruleName != null}">
				    <div class="control-group">
				    <label class="control-label" for="rule-name">Rule Name</label>
				    <div class="controls">
	 			      <input type="text" id="rule-name" name="name" value="${StoreCreateRuleDTO.ruleName}" disabled>
				    </div>
				  </div>
			  </c:if>
				  
			    <div class="control-group">
			    <label class="control-label" for="store-code">Store Code</label>
			    <div class="controls">
 			      <input disabled type="text" id="store-code" name="storeCode" value="${storeCode}" >
			    </div>
			  </div>
			  
			  
			    <div class="control-group">
			    <label class="control-label" for="supc">Supc</label>
			    <div class="controls">
 			      <input type="text" id="supc" name="supc"
 			      			    <c:if test="${supc!= null}"> disabled </c:if>
 			       value="${supc}">
			    </div>
			  </div>
			
			    
			
              <div class="control-group">
			    <label class="control-label" for="packaging-type">Packaging Type</label>
			    <div class="controls" id="packaging-type">
				    <select name="packagingType"  id="packagingType" >
					   <c:forEach var="packagingType" items="${packagingType}" >
							<option value="${packagingType}" <c:if test="${packagingType == packagingTypeOriginal}"> selected </c:if> >${packagingType}</option>
						</c:forEach>
				    </select>
			    </div>
			  </div>
			  <div class="control-group">
			    		<label class="control-label" for="check-enabled">Enabled</label>

			     <div class="controls">
							<input type="checkbox" name="enabled" value="true"
								<c:choose>

			      	 <c:when test="${enabled!=null && enabled!=false}">
			      	 	checked
			      	 </c:when>
			      	 </c:choose> />
						</div>
			  </div>
			  
			  <div class="control-group">
			    <div class="controls">
			      <button id="upload-form-btn" type="submit" class="btn btn-primary">Save Mapping</button>
					</div>
			  </div>
			</form>
			 <div>For SUPC-Packaging Type Bulk Upload, use  
                <a href="${path.http}/admin/supcpackagingtypemapping/supcpackagingtypemappingupload">this link.</a>
                 </div>
                
			
			<c:if test="${message != null && message!=\"\"}">
			<div class="alert alert-info">
                <button type="button" class="close" data-dismiss="alert">×
                </button>
                ${message}
                <br>
			</div>
			</c:if>
			<c:if test="${errorfinal != null}">
			<div class="alert alert-error">
                <button type="button" class="close" data-dismiss="alert">×
                </button>
                ${errorfinal}
                <br>
			</div>
			</c:if>
			<c:if test="${errorcause != null}">
			<div class="alert alert-error">
                <button type="button" class="close" data-dismiss="alert">×
                </button>
                ${errorcause}
                <br>
			</div>
			</c:if>
			
			</div>
		</div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
	<%-- Activate relevant sidebar function   --%>
	<%-- Activate relevant subheader function --%>
		<script type="text/javascript">
		function hideSidebar(){
		    $(".subheader").css({"left":"-11%"});
		    $(".functionAnchor").css({"display": "none"});
		    $(".sidebar").css({"left":"1%","width":"99%"});
		    $(".main-content").css({"left":"1%","width":"98%"});
		}
		function showSidebar(){
		    $(".subheader").css({"left":"0%"});
		    $(".functionAnchor").css({"display": "block"});
		    $(".sidebar").css({"left":"12%","width":"88%"});
		    $(".main-content").css({"left":"12%","width":"87%"});
		}
		
			$(document).ready(function() {
				var subheader=true;
				var test = window.location.href;
				var lastIndex = test.lastIndexOf('/');
				var url = test.substr(0,lastIndex);
				var selected = test.substr(lastIndex+1);
				 $(".subheaderToggle").click(function(){
				         if(subheader){
				                 hideSidebar();
				                 $(".subheaderToggleContent").html(">");
				                 subheader=false;
				         }else{
				                 showSidebar();
				                 $(".subheaderToggleContent").html("<");
				                 subheader=true;
				         }
				 });
				 
				$("#RuleType").change(function(){
					if(confirm('Are you sure ? This will remove current rule data')){
					console.log(url+"/"+$(this).val());
					window.location.href= "${path.http}/admin/packmangui/${storeCode}/createrule/?page="+$(this).val()+"&pageId=${pageId}";
					}
				});
				
				
 				$("#RuleType [value='${page}']").attr('selected',true);	            
	        
			
			function valid(){
				$('.error').remove();
				if($("#supc").val()==null || $("#supc").val()=="" ){
					$("#supc").after("<div class='error'>" + "Please enter supc" +  "</div>");
					return false;
				}
				return true;
			}
			
			$('#upload-form-btn').click(function() {
				if( valid() && confirm('Are you sure to save this rule ?')){
					var urlToSubmit = "${path.http}/admin/packmangui/${storeCode}/createrule/createSupcRule/";
					$("#supc").attr('disabled',false);
					$("#supc-rule-form").attr('action',urlToSubmit);
					$('#supc-rule-form').submit();
				return true;
			}
				return false;
				
			});
			});
		</script>
			
		
	</tiles:putAttribute>
</tiles:insertTemplate>
