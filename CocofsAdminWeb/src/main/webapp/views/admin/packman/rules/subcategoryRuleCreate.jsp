<%@ include file="/tld_includes.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<tiles:insertTemplate template="/views/layout/base.jsp">
	<tiles:putAttribute name="title" value="SubCategory rule create/update" />

	<tiles:putAttribute name="systemMessage">
		<div class="system-message"></div>
	</tiles:putAttribute>

	<tiles:putAttribute name="sidebar">
		<tiles:insertTemplate
			template="/views/admin/packman/rules/rule-subheader.jsp">
			<tiles:putAttribute name="active" value="createrule" />
		</tiles:insertTemplate>
	</tiles:putAttribute>

	<tiles:putAttribute name="extraScripts">
	<link href="${path.css('snapdeal/select2.css')}" rel="stylesheet"
			type="text/css" />
		<script type="text/javascript" src="${path.js('jquery/select2.js')}"></script>
		<link href="${path.css('chosen/chosen.css')}" rel="stylesheet"
			type="text/css" />
		<link rel="stylesheet" type="text/css"
			href="${path.css('snapdeal/ui.jqgrid.css')}" />

		<script type="text/javascript"
			src="${path.js('chosen/chosen.jquery.min.js')}"></script>

		<script type="text/javascript"
			src="${path.js('snapdeal/validate.js')}"></script>


		<script type="text/javascript"
			src="${path.js('jquery/jquery.ui.datepicker.min.js')}"></script>
		<script type="text/javascript" src="${path.js('spin/spin.min.js')}"></script>
		
		<script type="text/javascript"
			src="${path.js('jquery/jquery.datetimepicker.full.min.js')}"></script>
			
			<script type="text/javascript"
			src="${path.js('jquery/moment.js')}"></script>
			
			<link rel="stylesheet" type="text/css"
            href="${path.css('jquery/jquery.datetimepicker.css')}" />

	</tiles:putAttribute>
	<tiles:putAttribute name="systemMessage">
		<div class="system-message">
			<input type="hidden" value="${message }" id="messageHiddenField" />
		</div>
		<div class="system-message">
			<input type="hidden" value="${validationResponse}"
				id="validationResponseHiddenField" />
		</div>
	</tiles:putAttribute>

	<tiles:putAttribute name="subheader">
		<tiles:putAttribute name="systemMessage">
		</tiles:putAttribute>
		<tiles:insertTemplate template="/views/admin/packman/sidebar.jsp">
			<tiles:putAttribute name="active" value="packman" />
		</tiles:insertTemplate>
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
		<div class="main-content_packman lfloat">
			<div class="container" style="width: 85%;">

				<form id="packaging-type-form" class="form-horizontal" method="post"
					action="">

					<input type="text" style="display: none" name="pageId"
						value="${pageId}">


					<div class="control-group">
						<label class="control-label" for="Rule-Type">Rule Type</label>
						<div class="controls" id="Rule-Type">
							<select name="RuleType" id="RuleType" required="required"
								<c:if test="${SubCategoryCreateRuleDTO.ruleName != null}"> disabled </c:if>>
								<option value="rulesCreate">Select Rule Type</option>
								<option value="storeRuleCreate">Store</option>
								<option value="categoryRuleCreate">Category</option>
								<option value="subcategoryRuleCreate" selected>Subcategory</option>
								<option value="supcRuleCreate">Supc</option>
							</select>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="store-code">Store Code</label>
						<div class="controls">
							<input disabled type="text" id="store-code" name="storeCode"
								value="${storeCode}">
						</div>
					</div>

					<c:if test="${SubCategoryCreateRuleDTO.ruleName != null}">
						<div class="control-group">
							<label class="control-label" for="rule-name">Rule Name</label>
							<div class="controls">
								<input type="text" id="rule-name" disabled name="name"
									value="${SubCategoryCreateRuleDTO.ruleName}">
							</div>
						</div>
					</c:if>

					<div class="control-group">
						<label class="control-label" for="frm-subcategory">Subcategory</label>
						<div class="controls">
							<select id="frm-subcategory" name="subcategory"
								class="chosen-select"
								<c:if test="${SubCategoryCreateRuleDTO.ruleName != null}">
                	disabled
                	</c:if>>
								<c:forEach items="${subcategories}" var="subcategory">
									<option value="${subcategory.key}"
										<c:if test="${subcategory.key == SubCategoryCreateRuleDTO.subcategory}"> selected </c:if>>${subcategory.value}</option>
								</c:forEach>
							</select>
							<span id="subcategory-inline"></span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="frm-brand-type">Brand(s)</label>
						<div class="controls">
							<select id="frm-brand-type" class="chosen-select">
								<option>All</option>
								<option>In</option>
							</select>
						</div>
					</div>
					<div id="brand-hide" style="display: block;">
						<div class="control-group">
							<div class="controls">
								<input class="select2" id="frm-brand"  name="brand" value="${SubCategoryCreateRuleDTO.brand}" style="width:230px"/>
								<span id="brand-inline"></span>
							</div>
						</div>
					</div>
					<div class="tooltip-wrapper">
					<div id="supcat-label-hide" style="display: block;">
					<div class="control-group">
						<label class="control-label" for="frm-supcat-type">Label(s)</label>
						<div class="controls">
							<select id="frm-supcat-type" class="chosen-select">
								<option>All</option>
								<option>In</option>
							</select>
						</div>
					</div>
					</div>
					<div id="supcat-hide" style="display: block;">
						<div class="control-group">
							<div class="controls">
								<select id="frm-supcat" name="supercat" class="chosen-select"
									multiple>
									<c:forEach items="${supercats}" var="supercat">
										<option value="${supercat}">${supercat}</option>
									</c:forEach>
								</select> <span id="supcat-inline"></span>
							</div>
						</div>
					</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="frm-startdate">Start
							Date</label>
						<div class="controls">
							<input type="text" name="startDate"	from="startDate" id="frm-startdate" />
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="frm-enddate">End Date</label>
						<div class="controls">
							<input type="text" name="endDate" id="frm-enddate" />
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="packaging-type">Packaging
							Type</label>
						<div class="controls" id="packaging-type">
							<select name="packagingType" id="packagingType">
								<c:forEach var="packagingType" items="${packagingType}">
									<option value="${packagingType}"
										<c:if test="${packagingType == SubCategoryCreateRuleDTO.packagingType}"> selected </c:if>>${packagingType}</option>
								</c:forEach>
							</select>
							<span id="packaging-type-inline"></span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="check-enabled">Enabled</label>

						<div class="controls">
							<input type="checkbox" name="enabled" value="true"
								<c:choose>
			      	 <c:when test="${SubCategoryCreateRuleDTO!=null && SubCategoryCreateRuleDTO.enabled==true}">
			      	 	checked
			      	 </c:when>
			      	 <c:when test="${SubCategoryCreateRuleDTO==null}">
			      	 	checked
			      	 </c:when>
			      	 </c:choose> />
						</div>
					</div>
					<c:if test="${SubCategoryCreateRuleDTO.ruleName != null}">
						<div class="control-group">
							<div class="controls">
								<button id="upload-form-btn" type="submit"
									class="btn btn-primary">Update Rule</button>
								<button class="btn"
									onclick="javascript:window.open('','_self').close();">close</button>
							</div>
						</div>
					</c:if>
					<c:if test="${SubCategoryCreateRuleDTO.ruleName == null}">
						<div class="control-group">
							<div class="controls">
								<button id="upload-form-btn" type="submit"
									class="btn btn-primary">Create Rule</button>
							</div>
						</div>
					</c:if>
				</form>

				<c:if test="${message != null && message!=\"\"}">
					<div class="alert alert-info">
						<button type="button" class="close" data-dismiss="alert">×
						</button>
						${message} <br>
					</div>
				</c:if>
				<c:if test="${errorfinal != null}">
					<div class="alert alert-error">
						<button type="button" class="close" data-dismiss="alert">×
						</button>
						${errorfinal} <br>
					</div>
				</c:if>
				<c:if test="${errorcause != null}">
					<div class="alert alert-error">
						<button type="button" class="close" data-dismiss="alert">×
						</button>
						${errorcause} <br>
					</div>
				</c:if>

			</div>
		</div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
		<%-- Activate relevant sidebar function   --%>
		<%-- Activate relevant subheader function --%>
		<script type="text/javascript">
		function hideSidebar(){
		    $(".subheader").css({"left":"-11%"});
		    $(".functionAnchor").css({"display": "none"});
		    $(".sidebar").css({"left":"1%","width":"99%"});
		    $(".main-content").css({"left":"1%","width":"98%"});
		}
		function showSidebar(){
		    $(".subheader").css({"left":"0%"});
		    $(".functionAnchor").css({"display": "block"});
		    $(".sidebar").css({"left":"12%","width":"88%"});
		    $(".main-content").css({"left":"12%","width":"87%"});
		}
		
			$(document).ready(function() {
				
				if("${SubCategoryCreateRuleDTO.startDate}"!=""){
					$('#frm-startdate').val(moment("${SubCategoryCreateRuleDTO.startDate}").format('YYYY-MM-DD HH:mm:ss'));
				}
				
				if("${SubCategoryCreateRuleDTO.endDate}"!=""){
					$('#frm-enddate').val(moment("${SubCategoryCreateRuleDTO.endDate}").format('YYYY-MM-DD HH:mm:ss'));
				}
				
				$('.select2').select2({
					minimumInputLength: 1,
					multiple:true,
					quietMillis: 100,
		        	ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
		        		url: "/admin/resource/json/getBrands",
		        		dataType: 'json',
		        		data: function (term,page){
		        			return {
		        				query: term
		        			};
		        		},
		        		results: function (data) { // parse the results into the format expected by Select2.
		        			var resultData = [];
		        			$.each(data, function(ele,val){
		        				resultData.push({"id":val,"text":val});
		        			});
		        			return {results: resultData};
		        		}
		        		
		        	},
		        	initSelection: function(element,callback) {
		        		var resultData = [];
		        		var data = element.val().split(",");
	        			$.each(data, function(ele,val){
	        				resultData.push({"id":val,"text":val});
	        			});
	        			callback(resultData);
		        		
		        	},

		        dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
		        escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
					
		});
				
				var subheader=true;
				var test = window.location.href;
				var lastIndex = test.lastIndexOf('/');
				var url = test.substr(0,lastIndex);
				var selected = test.substr(lastIndex+1);
				 $(".subheaderToggle").click(function(){
				         if(subheader){
				                 hideSidebar();
				                 $(".subheaderToggleContent").html(">");
				                 subheader=false;
				         }else{
				                 showSidebar();
				                 $(".subheaderToggleContent").html("<");
				                 subheader=true;
				         }
				 });
				 
				$("#RuleType").change(function(){
					if(confirm('Are you sure ? This will remove current rule data')){
						console.log(url+"/"+$(this).val());
						window.location.href= "${path.http}/admin/packmangui/${storeCode}/createrule/?page="+$(this).val()+"&pageId=${pageId}";
						}
				});
				
				
 				$("#RuleType [value='${page}']").attr('selected',true);
 				$('#frm-startdate').datetimepicker({
                    /* arrows: false,
                    presets:{specificDate: 'Pick a date'},
                    presetRanges:[],
                    showOn: "button",
              buttonImage: "/static/cocofs/img/calendar.jpg",
              buttonImageOnly: false, */
 					format:'Y-m-d H:i:00'
			});
				
				if($('#frm-startdate').val()=="")
                	$('#frm-startdate').val(moment(new Date()).format('YYYY-MM-DD HH:mm:ss'));
                
                $('#frm-enddate').datetimepicker({
                   /*  arrows: false,
                    presets:{specificDate: 'Pick a date'},
                    presetRanges:[],
                    showOn: "button",
              buttonImage: "/static/cocofs/img/calendar.jpg",
              buttonImageOnly: false, */
                	format:'Y-m-d H:i:00'
                  });
				
			
			function isInvalid(startDateObj,endDateObj,ruleCreation) {
				var start  = $("#frm-startdate").val().trim();
                var end  = $("#frm-enddate").val().trim();
                if(start==null || start==""){
                	alert("Start date should not be empty");
                	return true;
                }
                if (! (start == moment($("#frm-startdate").val()).format('YYYY-MM-DD HH:mm:ss'))) {
	                alert("Start Date appears to be incorrect");
	                return true;
	            }
	            if ((end.length != 0) &&  (end != moment($("#frm-enddate").val()).format('YYYY-MM-DD HH:mm:ss'))) {
	                alert("End Date appears to be incorrect");
	                return true;
	            }
	            
	            if ( ruleCreation ) {
	            	var x = $('#frm-startdate').val();
	            	/* var x = $.datepicker.formatDate('yy-mm-dd', new Date())
	                if ( $('#frm-startdate').val() < x ) {
	                	alert('Start date before today not allowed'); 
	                	return true;
	                }  */
	            	var ed = $('#frm-enddate').val();
	            	if ( ed != "" && ed <= x ) {
	                	alert('End date before start date or equal to start date is not allowed'); 
	                	return true;
	                }
	            }
	            
	           /*  if($("#frm-subcategory").val()==null){
	            	alert("Please select atleast one subcategory");
	            	return true;
	            } */
	            
	            return false;
	        }
			
			
			$('#upload-form-btn').click(function() {
				if($("#frm-brand-type").val()=='In' && $('#frm-brand').val()==""){
					$("#brand-inline").html("<font color=\"red\">Select atleast one brand or select All Option</font>");
					return false;
				}
				else {
					$("#brand-inline").html("");	
				}
				if($("#frm-supcat-type").val()=='In' && $('#frm-supcat').val()==null){
					$("#supcat-inline").html("<font color=\"red\">Select atleast one supercategory or select All Option</font>")
					return false;
				}else {
					$("#supcat-inline").html("");	
				}
				
				
				function valid(){
					if($("#frm-subcategory").val()==null || $("#frm-subcategory").val()=="" ){
						$("#subcategory-inline").html("<div class='error'>" + "Please select subcategory" +  "</div>");
						return false;
					}
					$("#subcategory-inline").html("");
					if($("#packagingType").val()==null || $("#packagingType").val()=="" ){
						$("#packaging-type-inline").html("<div class='error'>" + "Please select packaging type" +  "</div>");
						return false;
					}
					$("#packaging-type-inline").html("");
					return true;
				}
				
				if(isInvalid($("#frm-startdate").datepicker('getDate'), $("#frm-enddate").datepicker('getDate') , true)==false && valid() && confirm('Are you sure to save this rule ?')){
					$("#frm-startdate").prop('disabled',false);
					$("#frm-enddate").prop('disabled',false);
					$("#rule-name").prop('disabled',false);
					$("#frm-subcategory").prop('disabled',false);
					var urlToSubmit = "${path.http}/admin/packmangui/${storeCode}/createrule/createSubCategoryRule/";
					$("#packaging-type-form").attr('action',urlToSubmit);
					$('#packaging-type-form').submit();
					
					return true;
				}
				return false;
			} );
			
            $('.chosen-select').chosen({"display_disabled_options":false, "display_selected_options":false});

           /*  <c:if test="${not empty SubCategoryCreateRuleDTO.brand}">
            var arr = [],cnt=0;
            <c:forEach items="${SubCategoryCreateRuleDTO.brand}" var="ppl">
            arr[cnt++] = "${ppl}";
            </c:forEach>
            $('#frm-brand').val(arr);
            $('#frm-brand').trigger("chosen:updated");
            $('#frm-brand-type').val('In');
            $('#frm-brand-type').trigger('chosen:updated');
        </c:if> */
        
        <c:if test="${not empty SubCategoryCreateRuleDTO.brand}">
        $('#frm-brand-type').val('In');
        $('#frm-brand-type').trigger('chosen:updated');
    </c:if>
        
        <c:if test="${not empty SubCategoryCreateRuleDTO.supercat}">
        	var arr = [],cnt=0;
            <c:forEach items="${SubCategoryCreateRuleDTO.supercat}" var="ppl">
            arr[cnt++] = "${ppl}";
            </c:forEach>
            $('#frm-supcat').val(arr);
            $('#frm-supcat').trigger("chosen:updated");
            $('#frm-supcat-type').val('In');
            $('#frm-supcat-type').trigger('chosen:updated');
    	</c:if>
        
    	$('#frm-brand-type').change(function(){
    		if($('#frm-brand-type').val() == 'All'){
    			$('#frm-brand').prop('disabled', true).trigger("chosen:updated");
    		}
    		else
    			$('#frm-brand').prop('disabled', false).trigger("chosen:updated");
    	});
    	
    	$('#frm-supcat-type').change(function(){
    		if($('#frm-supcat-type').val() == 'All'){
    			$('#frm-supcat').prop('disabled', true).trigger("chosen:updated");
    		}
    		else{
    			$('#frm-supcat').prop('disabled', false).trigger("chosen:updated");
    		}
    	});
    	
    	function setupForm(){
    		if($('#frm-brand-type').val() == 'All')
    			$('#frm-brand').prop('disabled', true).trigger("chosen:updated");
    		else
    			$('#frm-brand').prop('disabled', false).trigger("chosen:updated");
    		if($('#frm-supcat-type').val() == 'All')
    			$('#frm-supcat').prop('disabled', true).trigger("chosen:updated");
    		else
    			$('#frm-supcat').prop('disabled', false).trigger("chosen:updated");
    		
    		<c:if test="${empty supercats}">
    		$("#frm-supcat-type").prop("disabled", true).trigger('chosen:updated');
    		$(".tooltip-wrapper").attr('data-title','Labels are not specified at store level. So you can not create rules on labels');
    		</c:if>
    		
    	}
    	$(function() {
    	    $('.tooltip-wrapper').tooltip({position: "bottom"});
    	});
    	
    	setupForm();
    	
			});
				
		</script>


	</tiles:putAttribute>
</tiles:insertTemplate>
