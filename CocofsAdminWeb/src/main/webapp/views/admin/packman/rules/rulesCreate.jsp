<%@ include file="/tld_includes.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<tiles:insertTemplate template="/views/layout/base.jsp">
	<tiles:putAttribute name="title" value="Rule create" />

	<tiles:putAttribute name="systemMessage">
		<div class="system-message"></div>
	</tiles:putAttribute>

	<tiles:putAttribute name="sidebar">
		<tiles:insertTemplate template="/views/admin/packman/rules/rule-subheader.jsp">
			<tiles:putAttribute name="active" value="createrule" />
		</tiles:insertTemplate>
	</tiles:putAttribute>

	<tiles:putAttribute name="extraScripts">
<link href="${path.css('chosen/chosen.css')}" rel="stylesheet"
    type="text/css" />
    <link rel="stylesheet" type="text/css"
            href="${path.css('snapdeal/ui.jqgrid.css')}" />

    <script type="text/javascript"
    src="${path.js('chosen/chosen.jquery.min.js')}"></script>
    
    <script type="text/javascript"
    src="${path.js('snapdeal/validate.js')}"></script>
    
	</tiles:putAttribute>
	<tiles:putAttribute name="systemMessage">
		<div class="system-message">
			<input type="hidden" value="${message }" id="messageHiddenField"/>
		</div>
		<div class="system-message">
			<input type="hidden" value="${validationResponse}" id="validationResponseHiddenField"/>
		</div>
	</tiles:putAttribute>

	<tiles:putAttribute name="subheader">
		<tiles:putAttribute name="systemMessage">
		</tiles:putAttribute>		
		 <tiles:insertTemplate template="/views/admin/packman/sidebar.jsp">
			<tiles:putAttribute name="active" value="packman" />
		</tiles:insertTemplate> 
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
		<div class="main-content_packman lfloat">
			<div class="container" style="width: 85%;">
				
			<form id="packaging-type-form" class="form-horizontal" method="post" action="">
				
			  <input type="text" style="display:none" name="pageId" value="${pageId}">
				
			  <div class="control-group">
			    <label class="control-label" for="Rule-Type">Rule Level</label>
			    <div class="controls" id="Rule-Type">
							<select name="RuleType" id="RuleType" required="required">
								<option value="rulesCreate" selected>Select Rule Type</option>
								<option value="storeRuleCreate">Store</option>
								<option value="categoryRuleCreate">Category</option>
								<option value="subcategoryRuleCreate">Subcategory</option>
								<option value="supcRuleCreate">Supc</option>
							</select>
						</div>
			  </div>
			</form>
			<c:if test="${message != null && message!=\"\"}">
			<div class="alert alert-info">
                <button type="button" class="close" data-dismiss="alert">×
                </button>
                ${message}
                <br>
			</div>
			</c:if>
			</div>
		</div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
	<%-- Activate relevant sidebar function   --%>
	<%-- Activate relevant subheader function --%>
		<script type="text/javascript">
		function hideSidebar(){
		    $(".subheader").css({"left":"-11%"});
		    $(".functionAnchor").css({"display": "none"});
		    $(".sidebar").css({"left":"1%","width":"99%"});
		    $(".main-content").css({"left":"1%","width":"98%"});
		}
		function showSidebar(){
		    $(".subheader").css({"left":"0%"});
		    $(".functionAnchor").css({"display": "block"});
		    $(".sidebar").css({"left":"12%","width":"88%"});
		    $(".main-content").css({"left":"12%","width":"87%"});
		}
		
			$(document).ready(function() {
				var subheader=true;
				var test = window.location.href;
				var lastIndex = test.lastIndexOf('/');
				var url = test.substr(0,lastIndex);
				var selected = test.substr(lastIndex+1);
				 $(".subheaderToggle").click(function(){
				         if(subheader){
				                 hideSidebar();
				                 $(".subheaderToggleContent").html(">");
				                 subheader=false;
				         }else{
				                 showSidebar();
				                 $(".subheaderToggleContent").html("<");
				                 subheader=true;
				         }
				 });
				 
				$("#RuleType").change(function(){
					console.log(url+"/"+$(this).val());
					window.location.href= "${path.http}/admin/packmangui/${storeCode}/createrule/?page="+$(this).val()+"&pageId=${pageId}";
				});
				
				$("#RuleType [value="+selected+"]").attr('selected',true);
				
				
			});
				
		</script>
			
		
	</tiles:putAttribute>
</tiles:insertTemplate>
