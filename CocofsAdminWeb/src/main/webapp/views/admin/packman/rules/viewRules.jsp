<%@ include file="/tld_includes.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<tiles:insertTemplate template="/views/layout/base.jsp">
	<tiles:putAttribute name="title" value="View Rule" />

	<%-- <tiles:putAttribute name="systemMessage">
		<div class="system-message"></div>
	</tiles:putAttribute>
	 --%>
	 <tiles:putAttribute name="sidebar">
		<tiles:insertTemplate template="/views/admin/packman/rules/rule-subheader.jsp">
			<tiles:putAttribute name="active" value="viewrule" />
		</tiles:insertTemplate>
	</tiles:putAttribute> 
	
	<tiles:putAttribute name="extraScripts">
<link href="${path.css('chosen/chosen.css')}" rel="stylesheet"
    type="text/css" />
    <link rel="stylesheet" type="text/css"
            href="${path.css('snapdeal/ui.jqgrid.css')}" />
            <script type="text/javascript"
    src="${path.js('chosen/chosen.jquery.min.js')}"></script>
    <script type="text/javascript"
    src="${path.js('snapdeal/validate.js')}"></script>
    <link href="${path.css('snapdeal/select2.css')}" rel="stylesheet"
			type="text/css" />
		<script type="text/javascript" src="${path.js('jquery/select2.js')}"></script>
    <script type="text/javascript"
			src="${path.js('jquery/jquery.jqGrid.min.js')}"></script>
		<script type="text/javascript">
			var grid = $('#viewRuleDetail');
			var mydata = {};
			$(document)
					.ready(
							function() {
								$('#header-nav-packman').addClass('active');

								$("#viewRuleDetail")
										.jqGrid(
												{
													data: mydata, //insert data from the data object we created above
													datatype: 'local',
													width: 1000, 
													colNames : [ 'SR No.','Block','Rule Name',
															'Start Date',
															'End Date',
															'Enabled','Packaging Type','Remark','Edit' ],
													colModel : [ {
														name : 'id',
														index : 'id',
														align : 'center',
														width : 30,
														sortable : false,
														editable : false,
													},{
														name : 'blockName',
														index : 'blockName',
														align : 'center',
														width : 150,
														editable : false,
														sortable : false
													},{
														name : 'ruleName',
														index : 'ruleName',
														align : 'center',
														width : 150,
														editable : false,
														sortable : false
													}, {
														name : 'startDate',
														index : 'startDate',
														align : 'center',
														formatter:'date', 
														formatoptions: {srcformat:'ISO8601Long', newformat:'m/d/Y H:i:s'},
														formatter: function (cellval, opts, rowObject, action) {
															if(cellval!=null)
														    return $.fn.fmatter.call(this,"date",new Date(cellval),$.extend({}, $.jgrid.formatter.date, opts),rowObject,action);
															else
																return '';
														},
														width : 100,
														editable : false,
														sortable : false
													}, {
														name : 'endDate',
														index : 'endDate',
														formatter:'date', 
														formatoptions: {srcformat:'ISO8601Long', newformat:'m/d/Y H:i:s'},
														formatter: function (cellval, opts, rowObject, action) {
															if(cellval!=null)
														    return $.fn.fmatter.call(this,"date",new Date(cellval),$.extend({}, $.jgrid.formatter.date, opts),rowObject,action);
															else
																return '';
														},
														width : 100,
														align : 'center',
														editable : false,
														sortable : false
													}, {
														name : 'enabled',
														index : 'enabled',
														align : 'center',
														width : 60,
														editable : false,
														edittype: 'checkbox', editoptions: { value: "true:false" }, 
														formatter: "checkbox", formatoptions: { disabled: true},
														sortable : false
													},{
														name : 'packagingType',
														index : 'packagingType',
														align : 'center',
														width : 200,
														editable : false,
														sortable : false
													}, {
														name : 'remark',
														index : 'remark',
														align : 'center',
														width : 100,
														editable : false,
														sortable : false
													} ,{
														name : 'url',
														index : 'url',
														align : 'center',
														width : 50,
														editable : false,
														sortable : false
													} ],
													height : 'auto',
													viewrecords : true,
													caption : "Rules",
													gridview : true,
													loadui : 'block',
													loadonce : true,
													pager : '#viewRuleDetail_pager',
													rowNum : 200,
													sortorder : "asc",
													shrinkToFit : true,
													rowList : [ 200, 400, 600, 1000],
													ignoreCase : true,
													jsonReader : {
														root : "rows",
														page : "page",
														total : "total",
														records : "records",
														repeatitems : false,

														cell : "cell",
														id : "id"
													},

												});

								$("#viewRuleDetail").jqGrid('navGrid',
										'#viewRuleDetail_pager', {
											edit : false,
											add : false,
											del : false,
											search : false,
											refresh : false
										}, {}, {}, {}, {});

								
								
								if('${jsonvalue}'.indexOf("admin/packmangui/${storeCode}/createrule/")==-1){
									$("#viewRuleDetail").jqGrid('hideCol', 'url');
									}

							});
		</script>
    
	</tiles:putAttribute>
	
	<tiles:putAttribute name="systemMessage">
		<div class="system-message">
			<input type="hidden" value="${message }" id="messageHiddenField"/>
		</div>
		<div class="system-message">
			<input type="hidden" value="${validationResponse}" id="validationResponseHiddenField"/>
		</div>
	</tiles:putAttribute>


	<tiles:putAttribute name="subheader">
	<tiles:putAttribute name="systemMessage">		</tiles:putAttribute>
	 <tiles:insertTemplate template="/views/admin/packman/sidebar.jsp">
			<tiles:putAttribute name="active" value="packman" />
		</tiles:insertTemplate> 
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
		<div class="main-content_packman lfloat">
			<div class="container" style="width: 85%;">
				
			<form id="packaging-type-form" class="form-horizontal" method="post" action="">
				
			  <div class="control-group">
			    <label class="control-label" for="store-code">Store Code</label>
			    <div class="controls">
 			      <input disabled type="text" id="store-code" name="storeCode" value="${storeCode}" >
			    </div>
			  </div>
			  <div class="control-group">
			    <label class="control-label" for="Rule-Type">Search Rule By</label>
			    <div class="controls" id="Rule-Type">
				    <select name="RuleType"  id="RuleType" disabled>
 								<option value="nameWise" selected>Rule Name</option>
								<option value="storeRuleCreate">Store</option>
								<option value="categoryRuleCreate">Category</option>
								<option value="subcategoryRuleCreate">Subcategory</option>
								<option value="packagingTypeRuleCreate">Packaging Type</option>
								<option value="supcRuleCreate">Supc</option>
				    </select>
			    </div>
			    </div>
			  <div class="control-group" id="nameWise">
			    <label class="control-label" for="rule-name">Rule Name
			    <font size="3" color="red">*</font>
			    </label>
			    <div class="controls">
 			      <input type="text" id="rule-name" name="name" placeholder="R123-223-112" >
			    </div>
			  </div>
			  <div class="control-group" id="categoryRuleCreate">
                <label class="control-label"
                    for="frm-category-type">Category</label>
                <div class="controls">
                <select id="frm-category" name="category" class="chosen-select" >
                      <c:forEach items="${categories}" var="category">
                          <option value="${category.key}">${category.value}</option>
                      </c:forEach>
              </select>
                </div>
                </div>
			  <div class="control-group" id="subcategoryRuleCreate">
                <label class="control-label"
                    for="frm-subcategory-type">Subcategory</label>
                <div class="controls">
                <select id="frm-subcategory" name="subcategory" class="chosen-select" >
                      <c:forEach items="${subcategories}" var="subcategory">
                          <option value="${subcategory.key}">${subcategory.value}</option>
                      </c:forEach>
              </select>
                </div>
            </div>
            <div id="brand-hide" style="display: block;">
						<div class="control-group">
						 <label class="control-label"
                    for="frm-brand">Brand</label>
							<div class="controls">
								<input class="select2" id="frm-brand"  name="brand" style="width:230px"/>
								<span id="brand-inline"></span>
							</div>
						</div>
					</div>
		<div class="tooltip-wrapper" data-title="">
            <div id="supcat-label-hide" style="display: block;">
			  <div class="control-group" id="supercat">
                <label class="control-label"
                    for="frm-supercat">Label(s)</label>
               <div id="supcat-hide" style="display: block;">
              	<div class="controls">
              		<select id="frm-supercat" name="supercat" class="chosen-select">
              		<option value="">Please Select</option>
                      <c:forEach items="${supercats}" var="supercat">
                          <option value="${supercat}">${supercat}</option>
                      </c:forEach>
              		</select>
              <span id="supcat-inline"></span>
              </div>
          	</div>
          	</div>
          </div>
          </div>
          <div class="control-group" id="packagingTypeId">
			    <label class="control-label" for="packaging-type">Packaging Type
			    			    <font size="3" color="red">*</font>
			    </label>
			    <div class="controls">
<!--  			      <input type="text" id="packaging-type" name="packagingType"  placeholder="Carton">
 -->			    
 				<select id="packaging-type" name="packagingType" class="chosen-select">
              		<option value="">Please Select</option>
                      <c:forEach items="${packagingType}" var="packagingType">
                          <option value="${packagingType}">${packagingType}</option>
                      </c:forEach>
              </select>
 				</div>
			  </div>
          <div class="control-group" id="supc">
			    <label class="control-label" for="rule-supc">Supc</label>
			    <div class="controls">
 			      <input type="text" id="rule-supc" name="supc"  placeholder="SDL123676">
			    </div>
			 </div>
          <div class="control-group" id="createdById">
			    <label class="control-label" for="rule-created">Created By</label>
			    <div class="controls">
 			      <input type="text" id="rule-created" name="createdby"  placeholder="cocofs@snapdeal.com">
			    </div>
			  </div>
			  <div class="control-group" id="enabled">
		<label class="control-label" for="check-enabled">Enabled</label>

			     <div class="controls">
			      <input type="checkbox" id="check-enabled" name="enabled" checked> 
			    </div>
			  </div>
				<div class="control-group">
			    <div class="controls">
			      <button id="upload-form-btn" type="submit" class="btn btn-primary">Search Rule</button>
			    </div>
			  </div>
			</form>
 
			
			<c:if test="${message != null}">
			<div class="alert alert-info">
                <button type="button" class="close" data-dismiss="alert">×
                </button>
                ${message}
                <br>
			</div>
			</c:if>
			</div>
				<table id="viewRuleDetail"></table>		
				<div id="viewRuleDetail_pager"></div>
		</div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
	<%-- Activate relevant sidebar function   --%>
	<%-- Activate relevant subheader function --%>
		<script type="text/javascript">

		function hideSidebar(){
		    $(".subheader").css({"left":"-11%"});
		    $(".functionAnchor").css({"display": "none"});
		    $(".sidebar").css({"left":"1%","width":"99%"});
		    $(".main-content").css({"left":"1%","width":"98%"});
		}
		function showSidebar(){
		    $(".subheader").css({"left":"0%"});
		    $(".functionAnchor").css({"display": "block"});
		    $(".sidebar").css({"left":"12%","width":"88%"});
		    $(".main-content").css({"left":"12%","width":"87%"});
		}
		
			$(document).ready(function() {
				
				$("#RuleType").attr('disabled',false);
				
				$('.select2').select2({
					minimumInputLength: 1,
					multiple:false,
					quietMillis: 100,
		        	ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
		        		url: "/admin/resource/json/getBrands",
		        		dataType: 'json',
		        		data: function (term,page){
		        			return {
		        				query: term
		        			};
		        		},
		        		results: function (data) { // parse the results into the format expected by Select2.
		        			var resultData = [];
		        			$.each(data, function(ele,val){
		        				resultData.push({"id":val,"text":val});
		        			});
		        			return {results: resultData};
		        		}
		        		
		        	},
		        	initSelection: function(element,callback) {
		        		var resultData = [];
		        		var data = element.val().split(",");
	        			$.each(data, function(ele,val){
	        				resultData.push({"id":val,"text":val});
	        			});
	        			callback(resultData);
		        		
		        	},

		        dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
		        escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
					
		});
				 
				$('#sidebar-fn-createstore ').addClass('active');
				$('#header-nav-packman').addClass('active');
				
				function isValid() { 
					if(($("#rule-name").val()!="" && $("#rule-name").val().length>255)){
						$("#rule-name").after('<font color="red"><b>Rule name is too long.Maximum allowed length is 255</b></font>')
						return false;
					}
					$("#rule-name").siblings().remove();
					if(($("#rule-created").val()!="" && $("#rule-created").val().length>255)){
						$("#rule-created").after('<font color="red"><b>Created by is too long.Maximum allowed length is 255</b></font>')
						return false;
					}
					$("#rule-created").siblings().remove();
					return true;
				}
				
				$('#upload-form-btn').click(function() {
					if(!isValid())
						return false;
					var searchUrl = '${path.http}/admin/packmangui/${storeCode}/getRuleInfo/';
					var inputData = new Object();
					var searchRuleDTO = new Object();
					searchRuleDTO.storeCode = $("#store-code").val();
					if($("#RuleType :selected").attr('value')=='categoryRuleCreate'){
						if($("#frm-category").val()!="")
							searchRuleDTO.category = $("#frm-category").val();
					}
					if($("#RuleType :selected").attr('value')=='subcategoryRuleCreate'){
						if($("#frm-subcategory").val()!="")
							searchRuleDTO.subcategory = $("#frm-subcategory").val();
					}
					if($("#RuleType :selected").attr('value')=='subcategoryRuleCreate' || $("#RuleType :selected").attr('value')=='categoryRuleCreate'){
						if($("#frm-brand").val()!="")
							searchRuleDTO.brand = $("#frm-brand").val();
						if($("#frm-supercat").val()!="")
							searchRuleDTO.supercategory = $("#frm-supercat").val();
					}
					if($("#RuleType :selected").attr('value')=='nameWise'){
						if($("#rule-name").val()!=""){
							$("#rule-name").siblings().remove();
							searchRuleDTO.name = $("#rule-name").val();
						}
						else{
							$("#rule-name").after('<div class="error">Please specify rule name.</div>');
							return false;						
						}
					}
					if($("#RuleType :selected").attr('value')=='supcRuleCreate'){
						$(".error").remove();
						if($("#rule-supc").val()!=""){
						}
						else{
							$("#rule-supc").after('<div class="error">Please specify supc.</div>');
							return false;						
						}
					}
					if($("#rule-created").val()!="")
						searchRuleDTO.createdby = $("#rule-created").val();
					
					if($("#rule-created").val()!="")
						searchRuleDTO.createdby = $("#rule-created").val();
					
					
					if($("#RuleType :selected").attr('value')=='packagingTypeRuleCreate'){
						if($("#packaging-type").siblings().length!=0){
							$("#packaging-type").siblings().remove();
						}
						if($("#packaging-type").val()!=""){
							$("#packaging-type").siblings().remove();
							searchRuleDTO.packagingType = $("#packaging-type").val();
						}
						else{
							if($("#packaging-type").siblings().length==0){
								$("#packaging-type").after('<div class="error">Please select packaging type.</div>');
							}
							return false;						
						}
					}
					
					searchRuleDTO.enabled = $("#check-enabled").prop('checked');
					
					inputData.searchRuleDTO = searchRuleDTO;

					var updateUrl = '${path.http}/admin/packmangui/'+$("#store-code").val()+"/createrule/"+"?pageId=${pageId}";
					$("#viewRuleDetail").jqGrid('clearGridData');
					$.ajax({
						  url: searchUrl,
						  type: "POST",
						  data: {"storeCode" : $("#store-code").val(),
							  	 "category" : searchRuleDTO.category,
							  	 "subcategory" : searchRuleDTO.subcategory,
							  	 "brand" : searchRuleDTO.brand,
							  	 "supercategory" : searchRuleDTO.supercategory,
							  	 "name": searchRuleDTO.name,
							  	 "createdby" : searchRuleDTO.createdby,
							  	 "packagingType" : searchRuleDTO.packagingType,
							  	 "supc" : $("#rule-supc").val(),
							  	 "enabled" : searchRuleDTO.enabled},
							  	 
						  dataType : 'json',
						  headers: {'Content-Type': 'application/x-www-form-urlencoded'},
						  traditional: true,
						  async : false,
						  success: function(dataReceived1){
							$("#viewRuleDetail").jqGrid('setGridParam', {data : dataReceived1.rows}).trigger('reloadGrid');
							l = $("[aria-describedby=viewRuleDetail_url]").length;
						  	
							for(x=0;x<l;x++){
								if($("[aria-describedby=viewRuleDetail_blockName]")[x].title == 'Supc Block'){
									arr = $("[aria-describedby=viewRuleDetail_ruleName]")[x].title.split('-');
									url = '${path.http}/admin/packmangui/'+$("#store-code").val()+"/createrule/?supc="+arr[arr.length-1]+"&page=supcRuleCreate&pageId=";
									t = $("[aria-describedby=viewRuleDetail_url]")[x];
									$(t).html('<a href="'+url+'" target="_blank"><u>Click Here</u></a>');
								} else {
								_url = updateUrl+"&id="+$("[aria-describedby=viewRuleDetail_ruleName]")[x].title;
								t = $("[aria-describedby=viewRuleDetail_url]")[x];
								$(t).html('<a href="'+_url+'" target="_blank"><u>Click Here</u></a>');
								}
							}
							
							if($("#RuleType").val()=="supcRuleCreate" && "[aria-describedby=viewRuleDetail_url]".length>0){
								url = '${path.http}/admin/packmangui/'+$("#store-code").val()+"/createrule/?supc="+$("#rule-supc").val()+"&page=supcRuleCreate&pageId=";
								t = $("[aria-describedby=viewRuleDetail_url]")[0];
								$(t).html('<a href="'+url+'" target="_blank"><u>Click Here</u></a>');
							}
							
						  },
						  error: function(){
						    alert('Error occurred while getting data..');
						  }
						});
					
							return false;
				} );
				
				$("#storeRuleCreate").hide();
				$("#categoryRuleCreate").hide();
				$("#subcategoryRuleCreate").hide();
				$("#brand-hide").hide();
				$("#supercat").hide();
				$("#packagingTypeId").hide();
				$("#supc").hide();
				$("#nameWise").show();

				$(function() {
	        	    $('.tooltip-wrapper').tooltip({position: "bottom"});
	        	});

				$("#RuleType").change(function(){
					var myGrid = $("#viewRuleDetail"); // the variable you probably have already somewhere
					var gridBody = myGrid.children("tbody");
					var firstRow = gridBody.children("tr.jqgfirstrow");
					gridBody.empty().append(firstRow); 
						$("#enabled").show();
						$('#rule-name').val('');
						$('#rule-supc').val('');
						$('#frm-brand').val('').trigger('chosen:updated');
						$('#frm-supercat').val('').trigger('chosen:updated');
/* 						$("#packaging-type").val('');
 */						$('#packaging-type option[value=""]').attr('selected','selected')
 						$("#nameWise").hide();
						$("#storeRuleCreate").hide();
						$("#categoryRuleCreate").hide();
						$("#subcategoryRuleCreate").hide();
						$("#brand-hide").hide();
						$("#supercat").hide();
						$("#packagingTypeId").hide();
						$("#createdById").show();
						<c:if test="${empty supercats}">
		        		$(".tooltip-wrapper").attr('data-original-title','Labels are not specified at store level. So you can not create rules on labels');
						$("#frm-supercat").prop("disabled", true).trigger('chosen:updated');
		        		</c:if>
						
						$("#supc").hide();

						if($("#RuleType :selected").attr('value')=='nameWise'){
							$("#rule-name").siblings().remove();
							$("#nameWise").show();
						}
						else if($("#RuleType :selected").attr('value')=='categoryRuleCreate'){
							$("#categoryRuleCreate").show();
							$("#brand-hide").show();
							$("#supercat").show();
						
						}
						else if($("#RuleType :selected").attr('value')=='subcategoryRuleCreate'){
							$("#subcategoryRuleCreate").show();
							$("#brand-hide").show();
							$("#supercat").show();
							
						}
						else if($("#RuleType :selected").attr('value')=='packagingTypeRuleCreate'){
							$("#packagingTypeId").show();
							$("#packaging-type").show();
							$("#packaging-type").siblings().remove();
							$("#createdById").hide();
						}
						else if($("#RuleType :selected").attr('value')=='supcRuleCreate'){
							$("#supc").show();
							$("#enabled").hide();
							$("#created").hide();
						}
						else {
							//  Store level handling
						}
				
			});
			
			});
            $('.chosen-select').chosen({"display_disabled_options":false, "display_selected_options":false});
            
            
		</script>
			
		
	</tiles:putAttribute>
</tiles:insertTemplate>
