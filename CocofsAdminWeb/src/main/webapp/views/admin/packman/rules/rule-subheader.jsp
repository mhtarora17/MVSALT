<%@ include file="/tld_includes.jsp"%>

<c:set var="active">
	<tiles:getAsString name="active" />
</c:set>

<div class="sidebar lfloat">
	<div id="functions">
		<div class="sidebarUL">
			<c:set var="url" value="${storeCode}/viewrule/" />
			<c:if test="${fn:contains(jsonvalue,url)}">
				<c:choose>
					<c:when test="${active =='viewrule'}">
						<div class="activeFunction functionAnchor" style="margin-left:20%;"
							onclick="javascript:window.location.href='/admin/packmangui/${storeCode}/viewrule/'">View Rule</div>
					</c:when>
					<c:otherwise>
						<div class="functionAnchor" style="margin-left:20%;"
							onclick="javascript:window.location.href='/admin/packmangui/${storeCode}/viewrule/'">View Rule</div>
					</c:otherwise>
				</c:choose>
			</c:if>
			<c:set var="url" value="${storeCode}/createrule/" />
			<c:if test="${fn:contains(jsonvalue,url)}">
				<c:choose>
					<c:when test="${active =='createrule'}">
						<div class="activeFunction functionAnchor" style="margin-left:20%;"
							onclick="javascript:window.location.href='/admin/packmangui/${storeCode}/createrule/'">Create/Update Rule</div>
					</c:when>
					<c:otherwise>
						<div class="functionAnchor" style="margin-left:20%;"
							onclick="javascript:window.location.href='/admin/packmangui/${storeCode}/createrule/'">Create/Update Rule</div>
					</c:otherwise>
				</c:choose>
			</c:if>
		</div>

	</div>
</div>