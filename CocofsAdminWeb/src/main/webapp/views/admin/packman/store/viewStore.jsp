<%@ include file="/tld_includes.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<tiles:insertTemplate template="/views/layout/base.jsp">
	<tiles:putAttribute name="title" value="View Store" />

	<tiles:putAttribute name="systemMessage">
		<div class="system-message"></div>
	</tiles:putAttribute>

	
	<tiles:putAttribute name="extraScripts">
		<link rel="stylesheet" type="text/css"
			href="${path.css('snapdeal/ui.jqgrid.css')}" />
		<script type="text/javascript"
			src="${path.js('jquery/grid.locale-en.js')}"></script>
		<script type="text/javascript"
			src="${path.js('jquery/jquery.jqGrid.min.js')}"></script>
		<script type="text/javascript">
			var grid = $('#cartonDetail');


			function isValid(str) { pattern = new RegExp('^[a-zA-Z-0-9._]+$'); return pattern.test(str); }
			
			function validateRowData(inputData) {
				code = inputData.code;
				description = inputData.description;
				storeFrontId = inputData.storeFrontId.trim();
				labels = inputData.labels;

				error = "";
				errorNo = 1;
				validationFalied = false;
				if(code==null || code==""){
					error += (errorNo++)+") Store Code is Required\n";
					validationFalied = true;
				}
				
				if(storeFrontId==null || storeFrontId==''){
					error += (errorNo++)+") Store Front Id is Required\n";
					validationFalied = true;
				}
				
				if(storeFrontId.length<1 || storeFrontId.length>48){
					error += (errorNo++)+") Store Front Id length should be between 1 to 48 characters.\n";
					validationFalied = true;
				}
				
				if(!isValid(code)){
					error += (errorNo++)+") Store Code is not valid\n";
					validationFalied = true;
				}
					
					
				if(validationFalied){
					alert(error);
					return false;
				}
				
				var name = inputData.name.trim();
				if(name==null || name == "" || !isValid(name)){
					alert("Please enter valid alpha numeric store name");
					return false;
				}
				
				if (inputData.code.length > 10 || inputData.code.length <2) {
					alert("Please specify name beween 2 to 10 characters");
					return false;
				}
				if (inputData.name.length > 48 || inputData.name.length <5) {
					alert("Please specify name beween 5 to 48 characters");
					return false;
				}
				
				if(labels!=""){
				return validateLabel(labels) && noDuplicate(labels);
				}

				return true;
				
			}

			var lastsel2;
			function editGridData(rowId) {
				if (rowId && rowId !== lastsel2) {
					jQuery('#storeDetail').jqGrid('restoreRow', lastsel2);
					jQuery('#storeDetail').jqGrid('editRow', rowId, true);
					lastsel2 = rowId;
				}

			}

			function restoreGridData(rowId) {
				var row = $("#storeDetail").jqGrid('getRowData', rowId);
				jQuery('#storeDetail').restoreRow(rowId);
				lastsel2 = 0000000;
			}

			function saveGridData(rowId) {
				$("#storeDetail").jqGrid('saveRow', rowId);
				var inputData = new Object();
				var row = $("#storeDetail").jqGrid('getRowData', rowId);
				var storeDetailDTO = new Object();
				if (!validateRowData(row)) {
					$('#storeDetail').jqGrid('editRow', rowId, true, null,
							null, 'clientArray');
					return false;
				}
				storeDetailDTO.id = row.id;
				storeDetailDTO.name = row.name;
				storeDetailDTO.code = row.code;
				storeDetailDTO.description = row.description;
				storeDetailDTO.enabled = row.enabled;
				storeDetailDTO.storeFrontId = row.storeFrontId;
				if(row.labels != null && row.labels != "" ){
					data = getLabel(row.labels);
					if(validateLabel(data) && noDuplicate(data) && data != ""){
						if(!noDuplicate(data)){
							return;
						}
						storeDetailDTO.labels = data;
					}
				}
				inputData.storeDetailDTO = storeDetailDTO;
				delete row.action;
				$
						.ajax({
							type : "POST",
							url : "${path.http}/admin/packmangui/updatestore",
							dataType : 'json',
							contentType : 'application/json',
							data : JSON.stringify(inputData),
							async : false,
							success : function(resp) {
								if (resp.successful) {
									var searchUrl = '${path.http}/admin/packmangui/getStoreInfo';
									$("#storeDetail").jqGrid('setGridParam', {
										url : searchUrl,
										datatype : 'json'
									}).trigger('reloadGrid');
								} else {
									alert(resp.response);

								}
								$("#storeDetail").jqGrid('setGridParam', {
									url : searchUrl,
									datatype : 'json'
								}).trigger('reloadGrid');
								lastsel2 = 0000000;
							},
							error : function(xhr, statusCode, error) {
								if (xhr.status == 200) {
									alert("Your session has expired. Please log in again in another tab/window and then retry here.");
									$('#storeDetail').jqGrid('editRow', rowId,
											true, null, null, 'clientArray');
								}
							}
						});
			}
			
			function validateLabel(labels) { 
				if(/^([a-zA-Z0-9_ \-]+(,[a-zA-Z0-9_ \-]+)*)?$/.test(labels)){
					return true;
				}
				alert("Please enter labels separated by comma");
				return false; 
			}
			
			function noDuplicate(labels) { 
				fin = "";
				x = labels.split(",");
				for(i=0;i<x.length;i++){
					if(x[i]!="" && (","+fin+",").toLowerCase().indexOf(","+x[i].toLowerCase()+",") > -1){
						alert("Duplicate labels are not allowed");
						return false; 
					}
					if(x[i]!=""){
					fin += ","+x[i].trim(); 
					}
				}
				return true;
			}
			
			
			function getLabel(labels) { 
				var fin = "";
					x = labels.split(",");
					for(i=0;i<x.length;i++){
						if(x[i]!=""){
						fin += ","+x[i].trim(); 
						}
					}
					if(fin.length>1)
						return fin.substr(1);
					return fin;
			}

			function showActionButtons(cellValue, options, rowObject) {
				be = "<input style='height:22px;width:20px;' type='button' title='Edit' value='E' onclick=editGridData('"
						+ options.rowId + "');>";
				se = "<input style='height:22px;width:20px;' type='button' title='Save' value='S' onclick=saveGridData('"
						+ options.rowId + "');>";
				ce = "<input style='height:22px;width:20px;' type='button' title='Cancel' value='C' onclick=restoreGridData('"
						+ options.rowId + "');>";
				return be + se + ce ;

			}
			

			$(document)
					.ready(
							function() {

								$('#sidebar-fn-storeview').addClass('active');
								$('#header-nav-store').addClass('active');

								width = $("#storeUpdateDiv").parent().width()-150;
								col1width = parseInt(width*0.2);
								col2width = parseInt(width*0.2);
								col3width = parseInt(width*0.3);
								col4width = parseInt(width*0.2);
								
								$("#storeDetail").jqGrid(
										{
											url : '',
											datatype : 'local',
											editurl : 'clientArray',
											mtype : 'POST',
											colNames : [ 'S.No', 'Code','Store Front Id','Name','Enable',
													'Description','Label(s)','Action'],
											colModel : [ {
												name : 'id',
												index : 'id',
												align : 'center',
												width : 30,
												editable : false,
												sorttype : 'number'
											}, {
												name : 'code',
												index : 'code',
												align : 'center',
												width : 60,
												editable : false,
												sorttype : 'text'
											},{
												name : 'storeFrontId',
												index : 'storeFrontId',
												align : 'center',
												width : 100,
												editable : true,
												sorttype : 'text'
											},{
												name : 'name',
												index : 'name',
												align : 'center',
												width : 150,
												editable : true,
												sorttype : 'text'
											},{
												name : 'enabled',
												index : 'enabled',
												align : 'center',
												width : 50,
												editable : true,
												edittype: 'checkbox', editoptions: { value: "true:false" }, 
												formatter: "checkbox", formatoptions: { disabled: true},
												checked : true
											}, {
												name : 'description',
												index : 'description',
												align : 'center',
												width : 200,
												editable : true
											}, {
												name : 'labels',
												index : 'labels',
												align : 'center',
												width : 300,
												editable : true
											}, {
												name : 'action',
												index : 'action',
												width : 90,
												editable : false,
												formatter : showActionButtons,
												search : false
											} ],
											height : 'auto',
											viewrecords : true,
											caption : "Store Detail",
											gridview : true,
											loadui : 'block',
											loadonce : true,
											pager : '#storeDetail_pager',
											rowNum : 20,
											sortorder : "asc",
											shrinkToFit : true,
											rowList : [ 20, 40, 60, 100 ],
											ignoreCase : true,
											jsonReader : {
												root : "rows",
												page : "page",
												total : "total",
												records : "records",
												repeatitems : false,

												cell : "cell",
												id : "id"
											},

										});

								$("#storeDetail").jqGrid('navGrid',
										'#storeDetail_pager', {
											edit : false,
											add : false,
											del : false,
											search : false,
											refresh : false
										}, {}, {}, {}, {});

								var searchUrl = '${path.http}/admin/packmangui/getStoreInfo';
								$("#storeDetail").jqGrid('setGridParam', {
									url : searchUrl,
									datatype : 'json'
								}).trigger('reloadGrid');

								
							});
		</script>

	</tiles:putAttribute>
	<tiles:putAttribute name="subheader">
			<tiles:putAttribute name="systemMessage">		</tiles:putAttribute>
		
		<tiles:insertTemplate template="/views/admin/packman/sidebar.jsp">
			<tiles:putAttribute name="active" value="packman" />
		</tiles:insertTemplate>
	</tiles:putAttribute>

<tiles:putAttribute name="sidebar">
		<tiles:insertTemplate template="/views/admin/packman/store/store-subheader.jsp">
			<tiles:putAttribute name="active" value="viewstore" />
		</tiles:insertTemplate>
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
		<div class="main-content_packman lfloat">
			<div class="container subheaderDiv" style="width: 100%;">
				<div id="storeUpdateDiv">
					<table id="storeDetail"></table>
					<div id="storeDetail_pager"></div>

					<br> <br> <br>
				</div>
			</div>

		</div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
	<%-- Activate relevant sidebar function   --%>
	<%-- Activate relevant subheader function --%>
		<script type="text/javascript">

		function hideSidebar(){
		    $(".subheader").css({"left":"-11%"});
		    $(".functionAnchor").css({"display": "none"});
		    $(".sidebar").css({"left":"1%","width":"99%"});
		    $(".main-content").css({"left":"1%","width":"98%"});
		}
		function showSidebar(){
		    $(".subheader").css({"left":"0%"});
		    $(".functionAnchor").css({"display": "block"});
		    $(".sidebar").css({"left":"12%","width":"88%"});
		    $(".main-content").css({"left":"12%","width":"87%"});
		}
				$('#sidebar-fn-viewstore ').addClass('active');
				$('#header-nav-packman').addClass('active');
			
		</script>
			
		
	</tiles:putAttribute>
</tiles:insertTemplate>
