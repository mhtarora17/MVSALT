<%@ include file="/tld_includes.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<tiles:insertTemplate template="/views/layout/base.jsp">
	<tiles:putAttribute name="title" value="Create Store" />

	<%-- <tiles:putAttribute name="systemMessage">
		<div class="system-message"></div>
	</tiles:putAttribute>
	 --%>
	<tiles:putAttribute name="sidebar">
		<tiles:insertTemplate template="/views/admin/packman/store/store-subheader.jsp">
			<tiles:putAttribute name="active" value="createstore" />
		</tiles:insertTemplate>
	</tiles:putAttribute>
	
	<tiles:putAttribute name="extraScripts">
<link href="${path.css('chosen/chosen.css')}" rel="stylesheet"
    type="text/css" />
    <link rel="stylesheet" type="text/css"
            href="${path.css('snapdeal/ui.jqgrid.css')}" />
            <script type="text/javascript"
    src="${path.js('chosen/chosen.jquery.min.js')}"></script>
    <script type="text/javascript"
    src="${path.js('snapdeal/validate.js')}"></script>
	</tiles:putAttribute>
	<tiles:putAttribute name="systemMessage">
		<div class="system-message">
			<input type="hidden" value="${message }" id="messageHiddenField"/>
		</div>
		<div class="system-message">
			<input type="hidden" value="${validationResponse}" id="validationResponseHiddenField"/>
		</div>
	</tiles:putAttribute>


	<tiles:putAttribute name="subheader">
	<tiles:putAttribute name="systemMessage">		</tiles:putAttribute>
		<tiles:insertTemplate template="/views/admin/packman/sidebar.jsp">
			<tiles:putAttribute name="active" value="packman" />
		</tiles:insertTemplate>
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
		<div class="main-content_packman lfloat">
			<div class="container" style="width: 85%;">
				
			<form id="store-form" class="form-horizontal" method="post" action="">
			  <div class="control-group">
			    <label class="control-label" for="store-front-id">Store Front ID</label>
			    <div class="controls">
			      <input type="text" id="store-front-id" name="storeFrontId" value="${storeForm.storeFrontId}" validation="NoValidation" validationminlen="1" validationmaxlen="48" validationname="Store Front ID" validationenabled="true" placeholder="Id">
			    </div>
			  </div>
			  <div class="control-group">
			    <label class="control-label" for="store-name">Store Name</label>
			    <div class="controls">
			      <input type="text" id="store-name" name="storeName" value="${storeForm.storeName}" validation="AlphaNumericNoSpecialCharSpace" validationminlen="5" validationmaxlen="48" validationname="Store Name" validationenabled="true" placeholder="Name">
			    <span class="help-inline">Eg. Snapdeal,Exclusively</span>
			    </div>
			  </div>
			  <div class="control-group">
			    <label class="control-label" for="store-code">Store Code</label>
			    <div class="controls">
 			      <input type="text" id="store-code" name="storeCode" validation="AlphaNumeric" value="${storeForm.storeCode}" validationminlen="2" validationmaxlen="10" validationname="Store Code" validationenabled="true"  placeholder="Code">
			    <span class="help-inline.">Eg. SD,EX</span>
			    </div>
			  </div>
			  <div class="control-group">
			    <label class="control-label" for="store-label">Label(s)</label>
			    <div class="controls">
			      <textarea type="text" rows="3" id="store-label" name="labels" placeholder="label">${storeForm.labels}</textarea>
			    <span class="help-inline"> Provide label only if rule are required on labels.<br>Eg. To add multiple labels, please separate it by comma. <br>Like :  premium,luxury</span>
			    </div>
			  </div>
			  <div class="control-group">
			    <label class="control-label" for="store-description">Description</label>
			    <div class="controls">
			      <textarea type="text" rows="3" id="store-description" name="description" placeholder="Description">${storeForm.description}</textarea>
			    <span class="help-inline">Eg. Snapdeal Online Shopping.</span>
			    </div>
			  </div>
			  <div class="control-group">
			    <div class="controls">
			      <button id="upload-form-btn" type="submit" class="btn btn-primary">Save</button>
			    </div>
			  </div>
			</form>
			
			<c:if test="${message != null && message!=\"\"}">
			<div class="alert alert-info">
                <button type="button" class="close" data-dismiss="alert">×
                </button>
                ${message}
                <br>
			</div>
			</c:if>
						
			</div>
		</div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
	<%-- Activate relevant sidebar function   --%>
	<%-- Activate relevant subheader function --%>
		<script type="text/javascript">
		$('input[id=inputfile]').change(function() {
			$('#filebrowse').val($(this).val().replace(/C:\\fakepath\\/i, ''));
			});

		function hideSidebar(){
		    $(".subheader").css({"left":"-11%"});
		    $(".functionAnchor").css({"display": "none"});
		    $(".sidebar").css({"left":"1%","width":"99%"});
		    $(".main-content").css({"left":"1%","width":"98%"});
		}
		function showSidebar(){
		    $(".subheader").css({"left":"0%"});
		    $(".functionAnchor").css({"display": "block"});
		    $(".sidebar").css({"left":"12%","width":"88%"});
		    $(".main-content").css({"left":"12%","width":"87%"});
		}
		
			$(document).ready(function() {
				 
				$('#sidebar-fn-createstore ').addClass('active');
				$('#header-nav-packman').addClass('active');
				
				specialChars = "<>@!#$%^&*()+[]{}?:;|'\"\\,./~`=[`~,.<>;':\"$#)(!@^~)/[\]|{}()=+]/";
					check = function(string){
					    for(i = 0; i < specialChars.length;i++){
					        if(string.indexOf(specialChars[i]) > -1){
					            return true
					        }
					    }
					    return false;
					}
				
				function validateLabel() { 
					var labels = $("#store-label").val().trim();
					var fin = "";
					if(/^([a-zA-Z0-9_ \-]+(,[a-zA-Z0-9_ \-]+)*)?$/.test(labels)){
						x = labels.split(",");
						for(i=0;i<x.length;i++){
							if(x[i]!="" && (","+fin+",").toLowerCase().indexOf(","+x[i].toLowerCase()+",") > -1){
								$("#store-label").after("<div class='error'>" + "Duplicate labels are not allowed" +  "</div>");
								return false; 
							}
							if(x[i]!=""){
							fin += ","+x[i].trim(); 
							}
						}
						if(fin.length>0){
							$("#store-label").val(fin.substr(1));
						}
						return true;
					}
					$("#store-label").after("<div class='error'>" + "Please enter labels separated by comma" +  "</div>");
					return false; 
				}
				
				$('#upload-form-btn').click(function() {
					if(validate() && validateLabel()){
						$("#store-form").attr('action','${path.http}/admin/packmangui/savestore/');
						$('#store-form').submit();
						return true;
					}
					return false;
				} );
				
			});
			
			markRequired();			
		</script>
			
	</tiles:putAttribute>
</tiles:insertTemplate>
