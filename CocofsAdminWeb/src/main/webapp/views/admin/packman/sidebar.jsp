<%@ include file="/tld_includes.jsp"%>
<script src="${path.js('jquery.cookie.js')}"></script>

<script src="${path.js('jstree.js')}"></script>
<link rel="stylesheet" href="${path.css('style.css')}">
<script>
	$(document)
			.ready(
					function() {
						
						var data = ${jsonvalue};
						if('${pageId}'!=''){
							findElementById(data,'${pageId}');
						} else {
							findElementByUrl(data,window.location.pathname+'');	
						}
						
						createJSTree(data);

						function findElementById(d,id){
							if(d == undefined){
								return;
							}
							if (d.constructor === Array){

								for (var i = 0 ; i < d.length; i++) {
									if(d[i].id != undefined){
										if(d[i].id == id){
											console.log("Found");
											console.log(d[i]);
											state = new Object();
											state.opened = true;
											state.selected = true;
											d[i].state = state;
											return;
										}
									}
									if(d[i].children != undefined){
										findElementById(d[i].children,id);
									}
								}
							} else {
								if(d != undefined ){
									if(d.id == id){
											console.log("Found");
											console.log(d)
											state = new Object();
											state.opened = true;
											state.selected = true;
											d.state = state;
											return;
										}
										if(d.children != undefined){
											findElementById(d.children,id);
										}
									}
							}
						}


						function findElementByUrl(d,url){
							if(d == undefined){
								return;
							}
							if (d.constructor === Array){

								for (var i = 0 ; i < d.length; i++) {
									if(d[i]!=undefined && d[i].url != undefined){
										if(d[i].url == url){
											console.log("Found");
											console.log(d[i]);
											state = new Object();
											state.opened = true;
											state.selected = true;
											d[i].state = state;
											return;
										}
									}
									if(d[i]!=undefined && d[i].secondaryUrl != undefined && d[i].secondaryUrl.length!=undefined){
										for(var j=0;j<d[i].secondaryUrl.length;j++){
											if(d[i].secondaryUrl[j] != undefined && d[i].secondaryUrl[j] == url){
												console.log("Found");
												console.log(d[i]);
												state = new Object();
												state.opened = true;
												state.selected = true;
												d[i].state = state;
												return;
											}
										}
									}
									if(d[i].children != undefined){
										findElementByUrl(d[i].children,url);
									}
								}
							} else {
								if(d != undefined ){
									if(d.url != undefined && d.url == url){
											console.log("Found");
											console.log(d)
											state = new Object();
											state.opened = true;
											state.selected = true;
											d.state = state;
											return;
									}
									if(d.secondaryUrl != undefined && d.secondaryUrl.length != undefined){
										for(var j=0;j<d.secondaryUrl.length;j++){
											if(d.secondaryUrl[j] != undefined && d.secondaryUrl[j] == url){
												console.log("Found");
												console.log(d);
												state = new Object();
												state.opened = true;
												state.selected = true;
												d.state = state;
												return;
											}
										}
									}
									if(d.children != undefined){
											findElementByUrl(d.children,url);
									}
								}
							}
						}
						
						function createJSTree(data) {
							
							 
							 
							console.log(data);
							$('#jstree').jstree(
									{
										'core' : {
											'data' : data,
											'themes' : {
												"icons" : false
											},
										},
										'cookies' : {

											"auto_save" : true,
											"cookie_options" : {
												path : '/'
											}
										},
										"state" : {
											"key" : "samsung"
										},
										'plugins' : [ 'dnd', 'themes', 'ui',
												'cookies', 'types',
												'json_data', 'wholerow' ]
									}).bind("loaded.jstree",
									function(event, data) {
										$(this).jstree("open_all");
									});

							$("#jstree")
									.bind(
											'select_node.jstree',
											function(e, data) {
												var selectedNode = $('#jstree')
														.jstree(true)
														.get_selected('full',
																true);
												selectedNode = selectedNode[0];

												var id = selectedNode.id;
												var url = selectedNode.url;
												var parent = selectedNode.parent;
												console.log(selectedNode);
												console
														.log(selectedNode.original.url);
												if (selectedNode.original.url != null) {
													post(
															selectedNode.original.url,
															{
																pageId : id,
																storeCode : selectedNode.original.storeCode,
																packagingTypeCode : selectedNode.original.packagingTypeCode
															}, 'post');
												}
											});

						}

						createJSTree();

					});
</script>

<script type="text/javascript" src="${path.js('snapdeal/post.js')}"></script>


<div id="subheader" class="subheader_packman"
	style="background-image: url(${path.resources('img/background/bg_blue.png')}); padding-top: 50px; height:inherit">
	<div class="tabbable">
		<!-- Only required for left/right tabs -->
		<div class="subheaderTab">
			<sec:authorize ifAnyGranted="admin,tech">
				<div id="sidebar-fn-storeview"
					onclick="javascript:window.location.href='/admin/packmangui/viewstore'">
					Store</div>
			</sec:authorize>
			<sec:authorize ifAnyGranted="admin,tech">
				<div id="sidebar-fn-rearrangeSlab"
					onclick="javascript:window.location.href='/admin/packmangui/recalculateVolWt'">
					Recalculate Volumetric Weight</div>
			</sec:authorize>
			
		</div>
		<div id="jstree" class="subheader_tree"
			style="background-image: url(${path.resources('img/background/bg_blue.png')}); width:100%; height:inherit">
		</div>
	</div>

	<div class="subheaderToggle">
		<div class="subheaderToggleContent">&lt</div>
	</div>
</div>




