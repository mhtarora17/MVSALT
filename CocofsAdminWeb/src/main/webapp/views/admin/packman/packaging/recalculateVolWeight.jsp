<%@ include file="/tld_includes.jsp"%>
<style>
.tb td {
	font-weight: bold;
	font-size: small;
}

select {
	width: 170px;
}
</style>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<tiles:insertTemplate template="/views/layout/base.jsp">


	<tiles:putAttribute name="title" value="Rearrange Slabs" />

	<tiles:putAttribute name="subheader">
		<tiles:insertTemplate template="/views/admin/packman/sidebar.jsp">
			<tiles:putAttribute name="active" value="packman" />
		</tiles:insertTemplate>
	</tiles:putAttribute>



	<tiles:putAttribute name="sidebar">

	</tiles:putAttribute>


	<tiles:putAttribute name="extraScripts">
		<link href="${path.css('chosen/chosen.css')}" rel="stylesheet"
			type="text/css" />
		<link rel="stylesheet" type="text/css"
			href="${path.css('snapdeal/ui.jqgrid.css')}" />

		<script type="text/javascript"
			src="${path.js('chosen/chosen.jquery.min.js')}"></script>

	</tiles:putAttribute>


	<tiles:putAttribute name="deferredScript">
		<script>
			if (typeof Snapdeal == 'undefined') {
				Snapdeal = {};
			};
			Snapdeal.getStaticPath = function(path) {
				return '${path.http}' + path;
			}

			Snapdeal.getResourcePath = function(path) {
				return '${path.resources("")}' + path;
			}
		</script>

		<script type="text/javascript"
			src="${path.js('snapdeal/validations.js')}"></script>
		<script>
			$(document).ready(function() {
				$('#sidebar-fn-rearrangeSlab').addClass('active');
				$('#header-nav-packman').addClass('active');
			  
				$('#submitButton').click(function(e) {
					var r = confirm("Do you really want to re-arrange slabs?");
					if (r == false) {
						e.preventDefault();					 
						return;
					}
				});
			});
		</script>
	</tiles:putAttribute>


	<tiles:putAttribute name="body">
		<form:form commandName="rearrangeSlabForm" id="rearrangeSlabForm"
			method="POST"
			action='${path.http}/admin/packmangui/recalculatevolumetricweight'
			name="rearrangeSlabForm">
			<div class="main-content cfloat" style="margin-left:100px;">
				This functionality is supposed to be used in case of volumetric
				formula change; <br> Pls use below button to re-trigger
				volumetric weight formula calculations for normal packaging type items
				<div class="container subheaderDiv" style="width: 100%;">
					<div class="control-group">
						<div align="left" style="padding-top: 2px; padding-left: 184px;">
							<input type="submit" class="btn btn-primary threeD"
								id="submitButton" value="Recalculate Volumetric Weight">

						</div>
					</div>
					
					<c:if test="${message != null && message!=\"\"}">
						<div class="alert alert-info">
							<button type="button" class="close" data-dismiss="alert">×
							</button>
							${message} <br>
						</div>
					</c:if>
				</div>
			</div>
		</form:form>
	</tiles:putAttribute>
</tiles:insertTemplate>



