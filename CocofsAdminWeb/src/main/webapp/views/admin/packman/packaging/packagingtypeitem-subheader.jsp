<%@ include file="/tld_includes.jsp"%>

<c:set var="active">
	<tiles:getAsString name="active" />
</c:set>

<div class="sidebar lfloat">
	<div id="functions">
		<div class="sidebarUL">
			<c:set var="url" value="${storeCode}/viewpackagingtypeitem/" />
			<c:if test="${fn:contains(jsonvalue,url)}">
				<c:choose>
					<c:when test="${active =='viewpackagingtypeitem'}">
						<div class="activeFunction functionAnchor_packman" onclick=post('/admin/packmangui/${storeCode}/viewpackagingtypeitem/',{storeCode:'${storeCode}',packagingTypeCode:'${packagingTypeCode}',pageId:'${pageId}'},'post')>View/Edit
							Packaging Type Item</div>
					</c:when>
					<c:otherwise>
						<div class="functionAnchor_packman" onclick=post('/admin/packmangui/${storeCode}/viewpackagingtypeitem/',{storeCode:'${storeCode}',packagingTypeCode:'${packagingTypeCode}',pageId:'${pageId}'},'post')>View/Edit
							Packaging Type Item</div>
					</c:otherwise>
				</c:choose>
			</c:if>
			<c:set var="url" value="${storeCode}/createpackagingtypeitem/" />
			<c:if test="${fn:contains(jsonvalue,url) && show==true}">
				<c:choose>
					<c:when test="${active =='createpackagingtypeitem'}">
						<div class="activeFunction functionAnchor_packman" onclick=post('/admin/packmangui/${storeCode}/createpackagingtypeitem/',{storeCode:'${storeCode}',packagingTypeCode:'${packagingTypeCode}',pageId:'${pageId}'},'post')>Create
							Packaging Type Item</div>
					</c:when>
					<c:otherwise>
						<div class="functionAnchor_packman" onclick=post('/admin/packmangui/${storeCode}/createpackagingtypeitem/',{storeCode:'${storeCode}',packagingTypeCode:'${packagingTypeCode}',pageId:'${pageId}'},'post')>Create
							Packaging Type Item</div>
					</c:otherwise>
				</c:choose>
			</c:if>

			<script type="text/javascript" src="${path.js('snapdeal/post.js')}"></script>


		</div>

	</div>
</div>