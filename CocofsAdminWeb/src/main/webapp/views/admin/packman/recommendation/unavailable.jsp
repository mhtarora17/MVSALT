<%@ include file="/tld_includes.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<tiles:insertTemplate template="/views/layout/base.jsp">
	<tiles:putAttribute name="title" value="Recommendation Lookups" />

	<tiles:putAttribute name="systemMessage">
		<div class="system-message"></div>
	</tiles:putAttribute>

	<tiles:putAttribute name="extraScripts">


	</tiles:putAttribute>

	<tiles:putAttribute name="sidebar">
		<tiles:insertTemplate
			template="/views/admin/packman/recommendation/recommendation-sidebar.jsp">
		</tiles:insertTemplate>
	</tiles:putAttribute>
	<tiles:putAttribute name="systemMessage">
		<div class="system-message">
			<input type="hidden" value="${errorMessage }" id="messageHiddenField" />
		</div>
	</tiles:putAttribute>


	<tiles:putAttribute name="subheader">
		<tiles:insertTemplate
			template="/views/admin/packman/recommendation/recommendation-subheader.jsp">
		</tiles:insertTemplate>
	</tiles:putAttribute>

	<tiles:putAttribute name="body">

		<div class="main-content">
			<p align="center" style="font-size: 18px">Sorry!! but this
				feature is not available right now. Please contact your system
				administrator.</p>
		</div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
		<%-- Activate relevant sidebar function   --%>
		<%-- Activate relevant subheader function --%>
		<script type="text/javascript">

			function hideSidebar() {
				$(".subheader").css({
					"left" : "-11%"
				});
				$(".functionAnchor").css({
					"display" : "none"
				});
				$(".sidebar").css({
					"left" : "1%",
					"width" : "99%"
				});
				$(".main-content").css({
					"left" : "1%",
					"width" : "98%"
				});
			}
			function showSidebar() {
				$(".subheader").css({
					"left" : "0%"
				});
				$(".functionAnchor").css({
					"display" : "block"
				});
				$(".sidebar").css({
					"left" : "12%",
					"width" : "88%"
				});
				$(".main-content").css({
					"left" : "12%",
					"width" : "87%"
				});
			}

			$(document)
					.ready(
							function() {
								var subheader = true;
								$(".subheaderToggle").click(function() {
									if (subheader) {
										hideSidebar();
										$(".subheaderToggleContent").html(">");
										subheader = false;
									} else {
										showSidebar();
										$(".subheaderToggleContent").html("<");
										subheader = true;
									}

								});
								$('#recommendation-upload').addClass('active');
								$('#header-nav-packman').addClass('active');


							});
		</script>
	</tiles:putAttribute>
</tiles:insertTemplate>