<%@ include file="/tld_includes.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<tiles:insertTemplate template="/views/layout/base.jsp">
	<tiles:putAttribute name="title" value="ShippingModes" />

	<tiles:putAttribute name="systemMessage">
		<div class="system-message"></div>
	</tiles:putAttribute>

	<tiles:putAttribute name="extraScripts">
    <link href="${path.css('chosen/chosen.css')}" rel="stylesheet"
    type="text/css" />
    <link rel="stylesheet" type="text/css"
            href="${path.css('snapdeal/ui.jqgrid.css')}" />

    <script type="text/javascript"
    src="${path.js('chosen/chosen.jquery.min.js')}"></script>
    


	</tiles:putAttribute>
	<tiles:putAttribute name="sidebar">
		<tiles:insertTemplate template="/views/admin/admintasks/sidebar.jsp">
		</tiles:insertTemplate>
	</tiles:putAttribute>
	<tiles:putAttribute name="systemMessage">
		<div class="system-message">
			<input type="hidden" value="${message }" id="messageHiddenField"/>
		</div>
	</tiles:putAttribute>


	<tiles:putAttribute name="subheader">
		<tiles:insertTemplate template="/views/admin/admintasks/shippingmodeview-subheader.jsp">
		</tiles:insertTemplate>
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
		<div class="main-content lfloat">
			<div class="container" style="width: 85%;">
			<div>
					<form id="access-form" class="form-horizontal" method="get" action='#'>
					    
                        <div class="control-group">
                        <label class="control-label" for="frm-supc">SUPC</label>
                        <div class="controls">
                            <input type="text" id="frm-supc" />
                        </div>
                        </div>
                        <div class="control-group">
                        <label class="control-label" for="frm-cat">Category Url</label>
                        <div class="controls">
                            <input type="text" id="frm-cat" />
                        </div>
                        </div>
                        
                        <div class="control-group">
                        <div class="controls">
                            <button id="fetch-summary" type="button" class="btn btn-primary">Fetch Shipping Mode Details</button>
                    
                        </div>
                        </div>
                        
                        
					</form>
			</div>
            <div id="supc-mode-div"></div>
            <div id="cat-mode-div"></div>


            </div>
		</div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
	<%-- Activate relevant sidebar function   --%>
	<%-- Activate relevant subheader function --%>
		<script type="text/javascript">
			function hideSidebar() {
				$(".subheader").css({
					"left" : "-11%"
				});
				$(".functionAnchor").css({
					"display" : "none"
				});
				$(".sidebar").css({
					"left" : "1%",
					"width" : "99%"
				});
				$(".main-content").css({
					"left" : "1%",
					"width" : "98%"
				});
			}
			function showSidebar() {
				$(".subheader").css({
					"left" : "0%"
				});
				$(".functionAnchor").css({
					"display" : "block"
				});
				$(".sidebar").css({
					"left" : "12%",
					"width" : "88%"
				});
				$(".main-content").css({
					"left" : "12%",
					"width" : "87%"
				});
			}
			
			


			function fetchAndDisplayDetails() {
				$('#supc-mode-div').html('');
				$('#cat-mode-div').html('');
				if ( $('#frm-cat').val().trim() != "" ) { 
					
					$.ajax( {dataType:'json' , url:"/admin/shippingmode/catModes?cat="+encodeURIComponent($('#frm-cat').val().trim()) , 
		                async : true ,
		                 type : 'GET' , success: function(data, textStatus,  jqXHR ) {
		                	 console.log(data);
		                	 var d = JSON.parse(data);
		                	 var message = "<h3>Category URL  - " + d.key + "</h3>";
		                	 var i = 0;
		                	 for ( i =0 ; i < d.modes.length; i++ ) {
		                		 message += '<h4> Mode : '+  d.modes[i] + '</h4>';
		                	 }
		                	 message += "<h5>Delivery Type Based on modes "  + d.deliveryType + "</h5>"; 
		                	 
		                	 $('#cat-mode-div').html(message);
		                	 },
		                error : function(jqXHR, textStatus, errorThrown) { } } );
				}
				if ( $('#frm-supc').val().trim() != "" ) { 
					$.ajax( {dataType:'json' , url:"/admin/shippingmode/supcModes?supc="+encodeURIComponent($('#frm-supc').val().trim()) , 
		                async : true ,
		                 type : 'GET' , success: function(data, textStatus,  jqXHR ) {
		                	 console.log(data);
		                	 var d = JSON.parse(data);
                             var message = "<h3>SUPC - " + d.key + "</h3>";
                             var i = 0;
                             for ( i =0 ; i < d.modes.length; i++ ) {
                                 message += '<h4> Mode : '+  d.modes[i] + '</h4>';
                             }
                             message += "<h5>Delivery Type Based on modes "  + d.deliveryType + "</h5>"; 
                             
		                	 $('#supc-mode-div').html(message);
		                	 },
		                error : function(jqXHR, textStatus, errorThrown) { } } );
				}
				
				
				
			}
			
			
			$(document).ready(function() {
				var subheader = true;
				$(".subheaderToggle").click(function() {
					if (subheader) {
						hideSidebar();
						$(".subheaderToggleContent").html(">");
						subheader = false;
					} else {
						showSidebar();
						$(".subheaderToggleContent").html("<");
						subheader = true;
					}

				});
				/* check sidebar jsp for relevant div ids
				 */
				$('#sidebar-fn-shippingmode').addClass('active');
				$('#header-nav-admintasks').addClass('active');
				$('.chosen-select').chosen();
				
				$('#fetch-summary').click(fetchAndDisplayDetails);
				
				
				/* check subheader jsp for relevant div ids
				/cache
				 */
				/* $('#subheader-fn-usrmanagement').addClass('active') */

			});
		</script>
			
		
	</tiles:putAttribute>
</tiles:insertTemplate>