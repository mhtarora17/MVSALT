<%@ include file="/tld_includes.jsp"%>

<c:set var="active">
	<tiles:getAsString name="active" />
</c:set>

<div class="sidebar lfloat">
	<div id="functions">
		<div class="sidebarUL">
			<sec:authorize ifAnyGranted="admin,tech">
				<c:choose>
					<c:when test="${active =='eventDetail'}">
						<div class="activeFunction functionAnchor"
							onclick="javascript:window.location.href='/admin/events/eventDetail'">Event Detail</div>
					</c:when>
					<c:otherwise>
						<div class="functionAnchor"
							onclick="javascript:window.location.href='/admin/events/eventDetail'">Event Detail</div>
					</c:otherwise>
				</c:choose>
			</sec:authorize>
		</div>

	</div>
</div>
