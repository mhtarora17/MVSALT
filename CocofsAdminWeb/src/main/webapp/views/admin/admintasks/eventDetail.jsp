<%@ include file="/tld_includes.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	

<tiles:insertTemplate template="/views/layout/base.jsp">
	<tiles:putAttribute name="title" value="Event Detail" />

	<tiles:putAttribute name="systemMessage">
		<div class="system-message"></div>
	</tiles:putAttribute>


	<tiles:putAttribute name="extraScripts">

		<link rel="stylesheet" type="text/css"
			href="${path.css('snapdeal/ui.jqgrid.css')}" />
		<script type="text/javascript"
			src="${path.js('jquery/grid.locale-en.js')}"></script>
		<script type="text/javascript"
			src="${path.js('jquery/jquery.jqGrid.min.js')}"></script>
		<script type="text/javascript">
		
		function hideSidebar(){
            $(".subheader").css({"left":"-11%"});
            $(".functionAnchor").css({"display": "none"});
            $(".sidebar").css({"left":"1%","width":"99%"});
            $(".main-content").css({"left":"1%","width":"98%"});
        }
        function showSidebar(){
            $(".subheader").css({"left":"0%"});
            $(".functionAnchor").css({"display": "block"});
            $(".sidebar").css({"left":"12%","width":"88%"});
            $(".main-content").css({"left":"12%","width":"87%"});
        }
        
        function showActionButtons(cellValue, options, rowObject) {
			rt = "<input style='height:22px;width:20px;' type='button' class='button' title='Replay' value='R' onclick=replayEvent('"+ options.rowId + "');>";
			return rt;
        }

        function replayEvent(rowId) {
        	smoke.confirm("Do you want to replay the Event?",function(e) {
        		if(e){
        		var ajaxUrl = '${path.http}/admin/events/replayEvent?rowId='+rowId;
				  $ .ajax({url : ajaxUrl,type : "GET",dataType : 'json',success : function(data) {
					if(data!=null){
					  smoke.alert("Event submitted successfully.");
					  var searchUrl = '${path.http}/admin/events/getEvents?eventStatus='+encodeURIComponent($('#eventStatus').val());
					  $("#eventDetail").jqGrid('setGridParam',{url:searchUrl,datatype: 'json'}).trigger('reloadGrid');
					}else{
						smoke.alert("Failed to replay event.");
					}
				  }
				  });
        	}
        	});
		}
        
		$(document).ready(function() {
			
			var subheader=true;
            $(".subheaderToggle").click(function(){
                    if(subheader){
                            hideSidebar();
                            $(".subheaderToggleContent").html(">");
                            subheader=false;
                    }else{
                            showSidebar();
                            $(".subheaderToggleContent").html("<");
                            subheader=true;
                    }

            });
           /* check sidebar jsp for relevant div ids
           */
           //$('#sidebar-fn-taskdetail').addClass('active');
           //$('#header-nav-task').addClass('active');
			
			$('#searchEventDetail').click(function(){
				var searchUrl = '${path.http}/admin/events/getEvents?eventStatus='+encodeURIComponent($('#eventStatus').val());
				$("#eventDetail").jqGrid('setGridParam',{url:searchUrl,datatype: 'json'}).trigger('reloadGrid');
				
			});
			
			$("#eventDetail").jqGrid({
				url: '',
				datatype: 'json',
				editurl: 'clientArray',
				mtype: 'POST',
				colNames: ['Created','Id','Event Type','Param 1','Param 2','Json Arguments','Executed','Retry Count','Enabled','Replay'],
				colModel: [
				{name:'created',index:'created', width:20,editable:false,hidden:true},
				{name:'id',index:'id', width:20,editable:false,hidden:false},
				{name:'eventType',index:'eventType', width:150,editable:false},
				{name:'param1',index:'param1', width:100,editable:false},
				{name:'param2',index:'param2', width:100,editable:false, sortable:false,search:false},
				{name:'jsonArguments',index:'jsonArguments',width:400,align:'center',editable:false},
				{name:'executed',index:'executed',width:60,align:'center',editable:true,edittype:'checkbox',editoptions:{value:"true:false"},
						formatter:"checkbox",search:false, sortable:false},
				{name:'retryCount',index:'retryCount', width:70},			 
				{name:'enabled',index:'enabled',width:60,align:'center',editable:false,edittype:'checkbox',editoptions:{value:"true:false"},
						formatter:"checkbox",search:false, sortable:false},
				{name:'replay',index:'replay',align:'center', width:70, editable : false, formatter : showActionButtons, search : false},			 		
				],
				height: 'auto', viewrecords: true, caption: "Event Detail",gridview : true, loadui : 'block',align:'center',
				loadonce : true,pager: '#eventDetail_pager',rowNum:20,sortorder : "asc",shrinkToFit:true,
			      rowList:[20,50,100,200],
			      afterInsertRow: function(rowid,rowdata,rowelem){
			    	  $('#eventDetail').setCell(rowid,'eventType','','',{editable:true});
			      },
		   	ignoreCase: true,
				  jsonReader : {
		          root: "rows",
		          page: "page",
		          total: "total",
		          records: "records",  
		          repeatitems: false,
		          cell: "cell",
		          id:"id"
				  },
				  
				  subGrid: true,
				  subGridOptions: {
				        "plusicon": "ui-icon-triangle-1-e",
				        "minusicon": "ui-icon-triangle-1-s",
				        "openicon": "ui-icon-arrowreturn-1-e",
				        expandOnLoad : false,
				        selectOnExpand : false,
						reloadOnExpand : true
				  }
				  
			});
				       			
			$("#eventDetail").jqGrid('navGrid','#eventDetail_pager',
					{edit:false,add:false,del:false,search:false,refresh:false},
					{},{},{},
					{}
					);	
			
		});
		
		</script>

	</tiles:putAttribute>
	<tiles:putAttribute name="systemMessage">
		<div class="system-message">
			<input type="hidden" value="${message }" id="messageHiddenField" />
		</div>
	</tiles:putAttribute>

        <tiles:putAttribute name="subheader">
                <tiles:insertTemplate template="/views/admin/admintasks/event-subheader.jsp">
                        <tiles:putAttribute name="active" value="eventAdmin" />
                </tiles:insertTemplate>
        </tiles:putAttribute>
	
	<tiles:putAttribute name="sidebar">
		<tiles:insertTemplate template="/views/admin/admintasks/sidebar.jsp">
			<tiles:putAttribute name="active" value="eventDetail" />
		</tiles:insertTemplate>
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
	<div class="main-content lfloat">
	<div class="container" style="width: 100%;">
		<div class="error">${message}</div>
		<div class="content-bdr" style="width: 97%;">
		<div >
		<div >
			
			<div class="container" style="width: 100%;">
			<div class="form-group" style="margin-top: 49px;">
			<span> Event Status:</span>&nbsp;&nbsp;&nbsp;&nbsp;
				<select id="eventStatus">
				<c:forEach items="${eventStatus}" var="eventStatus">
					<option value="${eventStatus}">${eventStatus}</option>
				</c:forEach>	
				</select>&nbsp;&nbsp;
				<input class="btn btn-primary" id ="searchEventDetail" type="button" value="Search"> 
				</div><br>
					<div id="eventUpdateDiv" style="width:100%">
								<table id="eventDetail"  style="width:100%"></table>
								<div id="eventDetail_pager"  style="width:100%"></div>
								
								<br><br><br>
					</div>
			</div>
			</div>
		</div>
		</div>
		</div>					
				</div>
	</tiles:putAttribute>
</tiles:insertTemplate>
