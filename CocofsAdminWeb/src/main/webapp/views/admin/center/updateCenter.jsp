<%@ include file="/tld_includes.jsp"%>
<style>
.tb td{
font-weight: bold;
font-size: small;
}
select {
    width:170px;
}
</style>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<tiles:insertTemplate template="/views/layout/base.jsp">

<tiles:putAttribute name="subheader">
		<tiles:insertTemplate template="/views/admin/fmMapping/sellerSupc-subheader.jsp">
		</tiles:insertTemplate>
	</tiles:putAttribute>
	
	<tiles:putAttribute name="subheader">
		<tiles:insertTemplate template="/views/admin/fcMapping/sidebar.jsp">
			<tiles:putAttribute name="active" value="fcMapping" />
		</tiles:insertTemplate>
	</tiles:putAttribute>

<tiles:putAttribute name="title" value="Center Management" />
	<tiles:putAttribute name="deferredScript">
	<script>
	if (typeof Snapdeal == 'undefined') { Snapdeal = {};};
		Snapdeal.getStaticPath = function (path) {
		return '${path.http}' + path;
		}

		Snapdeal.getResourcePath = function (path) {
			return '${path.resources("")}' + path;
		}
	</script>
	<script type="text/javascript" src="${path.js('snapdeal/validations.js')}"></script>	
	
	<script>
	$(document).ready(function(){
	
	 
	        
			$('#sidebar-cu').addClass('active');
	        $('#header-nav-fcview').addClass('active');
		
		$("#pincode").change(function(){
			var pincode = document.getElementById("pincode").value.trim();
				
				if(pincode!=undefined && pincode!=''){
					var ajaxUrl =  "${path.http}/admin/centermanagement/getCityState?pincode="+pincode;
					  $ .ajax({url : ajaxUrl,type : "GET",dataType : 'json',success : function(data) {
						if(data!= null){
							$('#centerData').fadeIn();
							$('#city').val(data.city);
							$('#state').val(data.state);
						  }	  
					  }  
						  });
				}else{
					alert("Please enter a valid pin code");
				}
				
			});
		
		
			$('#update').click(function(){
				<%--
				$("#fulfillmentCenterForm").validate({
					rules: {
							email: {
							required: true,								 
						},
						centerName: {
							required: true,
						},
						centerCode: {
							required: true,
						},
						addressLine1:{
							required: true,
						},
						pincode :{
							required: true,
						},
						mobile :{
							required: true,
						},
						name :{
							required: true,
						},
					},
					messages: {
						useremail: {
							required:"Please enter a email address",
						},
						centerName: {
							required: "Please provide a Center name",
						},
						centerCode: {
							required: "Please provide a Center Code",
						},
						addressLine1: {
							required: "Please provide Address detail",
						},
						name: {
							required: "Please provide Shipping Detail Name",
						},
						pincode: {
							required: "Please provide pincode",
						},
						mobile: {
							required: "Please provide mobile number",
						},
					}
				});
				--%>
				$('#fulfillmentCenterForm').submit();
			});
		

		$("#searchCenter").click(function(){
			var fcCode = document.getElementById("searchFcCode").value.trim();
			
			if(fcCode!=undefined && fcCode!=''){
				var ajaxUrl =  "${path.http}/admin/centermanagement/searchCenter?fcCode="+fcCode;
				  $ .ajax({url : ajaxUrl,type : "GET",dataType : 'json',success : function(data) {
					if(data!= null){
						$('#centerData').fadeIn();
						  $('#id').val(data.id);
						  $('#centercode').val(data.centercode);
						  $('#centername').val(data.centername);
						  $('#fulfillmentType').val(data.fulfillmentType);						  
						  $('#name').val(data.name);
						  $('#addressLine1').val(data.addressLine1);
						  $('#addressLine2').val(data.addressLine2);
						  $('#city').val(data.city);
						  $('#state').val(data.state);
						  $('#pincode').val(data.pincode);
						  $('#mobile').val(data.mobile);
						  $('#landLine').val(data.landLine);
						  $('#email').val(data.email);
						  if(data.sdInstant ==true){
							  $('#sdInstant').prop('checked',true);
						  }else{
							  $('#sdInstant').prop('checked',false);
						  }
						 
					  } else {
						  alert("Center does not exists ..");
					  }	  
				  },
				   error : function(jqXHR, textStatus, errorThrown) { 
	    				alert("Center does not exist");
	    				}  
					  });
			}else{
				alert("Please enter a valid center code");
			}
			
	    });
		
		
		
	});
	</script>
	</tiles:putAttribute>
	<tiles:putAttribute name="body">
		<div class="main-content lfloat">	
		<div id="errorMessage" class="alert-error">${message}</div>	
		<div class="head-lable">Update Center Details</div>
		 
		<div class="cod-outer" style="border: 1px solid #DDDDDD;border-style: solid solid solid;">
		</div >

		<div class="cod-outer" style="border: 1px solid #DDDDDD;border-style: solid solid solid;">
            <form:form class="form-horizontal" commandName="fulfillmentCenterForm" id="fulfillmentCenterForm" method="POST" action='${path.http}/admin/centermanagement/updateCenterDetail' name="fulfillmentCenterForm">
    
			<div class="container" style="width: 100%;">
			<input type="hidden" name="id" id = "id"/> 
			<label class="control-label">FC Code</label> 
            <div class="controls">
			<input id="searchFcCode"  type="text" name="searchFcCode" value="" tabindex="2"> &nbsp;&nbsp;&nbsp;
			 <input type="button" value="Search Center" id="searchCenter" class="button">
			</div>
			
            			
           	<br><br>
			<div id="centerData" style="display: none;">
				<div class="control-group">
					  <table class="tb" width="54%" style="padding: 0px 0px 0px 62px;"> 
					  <tr><td><input type="hidden" name="centercode" id = "centercode" value="centercode"/> </td></tr>
					  <tr>
					  	<td><span>Center Name<span class=required>*</span>:</span></td>
					  	<td><input id="centername" class="input-text validateNotEmpty" name="centername" value="" tabindex="2"></td>
					  </tr>
					  <tr>
					  	<td><span>Center Type<span class=required>*</span>:</span></td>
					  	<td><input id="fulfillmentType" class="input-text validateNotEmpty" name="fulfillmentType" value="" tabindex="2" readonly></td>
					  </tr>
					  <tr>
					  	<td><span>Sd Instant:</span></td>
					  	<td><input id="sdInstant" name="sdInstant" type="checkbox"/></td>
					  </tr>
						
					  <tr>
					  	<td><span>Name<span class=required>*</span>:</span></td>
					  	<td><input id="name" class="input-text validateNotEmpty" name="name" value="" tabindex="2"></td>
					  </tr>
					  <tr>
					  <tr>
					  	<td><span>Address Line 1<span class=required>*</span>:</span></td>
					  	<td><input id="addressLine1" type="text" class="input-text validateNotEmpty" name="addressLine1" value="" tabindex="2"></td>
					  </tr>
					  <tr>
					  	<td><span>Address Line 2</span></td>
					  	<td><input id="addressLine2" type="text" class="input-text validateNotEmpty" name="addressLine2" value="" tabindex="2"></td>
					  </tr>
					  <tr>
					  	<td><span>Pincode<span class=required>*</span>:</span></td>
					  	<td><input id="pincode" type="text" class="input-text validateNotEmpty" name="pincode" value="" tabindex="2"></td>
					  	
					  </tr>
					  <tr>
					  	<td><span>City</span></td>
					  	<td><input id="city" type="text" class="input-text validateNotEmpty" name="city" tabindex="2"></td>
					  </tr>
					  <tr>
					  	<td><span>State</span></td>
					  	<td><input id="state" type="text" class="input-text validateNotEmpty" name="state" tabindex="2" ></input></td>
					  </tr>
					   <tr>
					  	<td><span>Mobile<span class=required>*</span>:</span></td>
					  	<td><input id="mobile" type="text" class="input-text validateNotEmpty" name="mobile" value="" tabindex="2"></td>
					  </tr>
					   <tr>
					  	<td><span>LandLine Number</span></td>
					  	<td><input id="landLine" type="text" class="input-text validateNotEmpty" name="landLine" value="" tabindex="2"></td>
					  </tr>
					  <tr>
					  	<td><span>Email<span class=required>*</span>:</span></td>
					  	<td><input id="email" type="text" class="input-text validateNotEmpty" name="email" value="" tabindex="2"></td>
					  </tr>
					  
					
					  </table>
				 </div>
                 
					
					   
					   <div class="control-group">
						  	<div align="center" style="padding-top: 10px;">
								<input type="button" class="button" value="Update Center" id="update">
							
							</div>
						</div>
						</div>
			</div>
		</form:form>
		</div><br/>
		</div>
		
		
	</tiles:putAttribute>
</tiles:insertTemplate>

