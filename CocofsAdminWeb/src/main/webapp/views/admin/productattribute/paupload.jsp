<%@ include file="/tld_includes.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<tiles:insertTemplate template="/views/layout/base.jsp">
	<tiles:putAttribute name="title" value="Product Attribute Admin" />

	<tiles:putAttribute name="systemMessage">
		<div class="system-message"></div>
	</tiles:putAttribute>

	<tiles:putAttribute name="extraScripts">


	</tiles:putAttribute>
	<tiles:putAttribute name="sidebar">
		<tiles:insertTemplate template="/views/admin/productattribute/sidebar.jsp">
		</tiles:insertTemplate>
	</tiles:putAttribute>
	<tiles:putAttribute name="systemMessage">
		<div class="system-message">
			<input type="hidden" value="${message }" id="messageHiddenField"/>
		</div>
	</tiles:putAttribute>


	<tiles:putAttribute name="subheader">
		<tiles:insertTemplate template="/views/admin/productattribute/paupload-subheader.jsp">
		</tiles:insertTemplate>
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
		<div class="main-content lfloat">
			<div class="container" style="width: 85%;">
				<form id="file-upload-form" enctype="multipart/form-data" method="post" class="form-horizontal">
					<fieldset>
						<legend>Product Attribute File Upload</legend>
						<div class="control-group">
						<label class="control-label" for="inputfile">
                        File To Upload 
                        </label>
                        <div class="controls">
						<input id="inputfile" style="display:none;" name="inputfile" type="file"> 
                        <div class="input-append">
                        <input id="filebrowse" class="input" type="text">
                        <a class="btn btn-info" onclick="$('input[id=inputfile]').click();">Browse</a>
                        </div>
                        
                        </div>
						</div>
                        <sec:authorize ifAnyGranted="tech,superuploader">
                        <div class="control-group">
                        <label class="control-label" for="superupload">Upload As Super Uploader
                        </label>
                        <div class="controls">
                        <input id="superupload" name="superupload" type="checkbox"/> 
                        </div>
                        </div>
                        </sec:authorize>
                        <div class="control-group">
                        <div class="controls">
                        <button id="file-upload-form-btn" type="Upload" class="btn btn-primary">Submit</button>
                        </div>
                        </div>
                       
						
						<%-- <span class="help-block">Please upload the file</span>  --%>
						<%--
                        <div class="control-group">
						<label class="radio inline"> <input
							type="radio" name="uploadtype" id="attributetype" value="attribute" checked> Product Attribute Upload
						</label>
						<label class="radio inline"> <input
							type="radio" name="uploadtype" id="weighttype" value="weight" checked> Product Weight Upload
						</label>
						</div>
                        --%>
						
					</fieldset>
				</form>
                <div>
                <a href="downloadProductAttributeTemplate"> Download Product Attribute Template</a>
                </div>
                <%--
                <div>
                <a href="downloadProductWeightTemplate"> Download Product Weight Template</a>
                </div>
                 --%>
                <c:if test="${message != null}">
                <div class="alert alert-info">
                <button type="button" class="close" data-dismiss="alert">&times;
                </button>
                ${message }</div>
                </c:if>

			</div>
		</div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
	<%-- Activate relevant sidebar function   --%>
	<%-- Activate relevant subheader function --%>
		<script type="text/javascript">
		
		$('input[id=inputfile]').change(function() {
			$('#filebrowse').val($(this).val().replace(/C:\\fakepath\\/i, ''));
            });

		function hideSidebar(){
		    $(".subheader").css({"left":"-11%"});
		    $(".functionAnchor").css({"display": "none"});
		    $(".sidebar").css({"left":"1%","width":"99%"});
		    $(".main-content").css({"left":"1%","width":"98%"});
		}
		function showSidebar(){
		    $(".subheader").css({"left":"0%"});
		    $(".functionAnchor").css({"display": "block"});
		    $(".sidebar").css({"left":"12%","width":"88%"});
		    $(".main-content").css({"left":"12%","width":"87%"});
		}
		
			$(document).ready(function() {
				var subheader=true;
				 $(".subheaderToggle").click(function(){
				         if(subheader){
				                 hideSidebar();
				                 $(".subheaderToggleContent").html(">");
				                 subheader=false;
				         }else{
				                 showSidebar();
				                 $(".subheaderToggleContent").html("<");
				                 subheader=true;
				         }

				 });
				/* check sidebar jsp for relevant div ids
				*/
				$('#sidebar-fn-upload ').addClass('active');
				$('#header-nav-paupload').addClass('active');
				
				/* check subheader jsp for relevant div ids
				*/
				/* $('#subheader-fn-usrmanagement').addClass('active') */
				
				
				$('#file-upload-form-btn').click(function() {
					/*var uptype = $('input[name=uploadtype]:checked').val();*/
					
					if ($("#inputfile:input").length) {
						if ($("#inputfile").val() != '') {
							if (! $('#inputfile').val().trim().match(/\.xls$/) ) {
								alert("Only files with extension .xls can be uploaded ");
								return false;
							}
							var posturl = "processWeightUpload"
								/*if ( uptype == 'attribute') { */
									posturl = "processAttributeUpload"
								/*}*/
								var issuper = false;
			                    if ( ($("#superupload").length > 0 ) && $("#superupload").is(":checked")) {
			                    	issuper = true;
			                    }
			                    console.log(issuper);
								$('#file-upload-form').attr("action", posturl)
								$('#file-upload-form').submit();
						} else {
							alert("Please upload a file");
							return false;
						}
					} else {
						alert("class uploadJobFile not found");
						return false;
					}
					
				} );

			});
			
		
		
		

		</script>
			
		
	</tiles:putAttribute>
</tiles:insertTemplate>