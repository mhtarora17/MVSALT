<%@ include file="/tld_includes.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="${path.css('snapdeal/jquery-ui-1.8.17.custom.css')}" />
<tiles:insertAttribute name="headImmediate" defaultValue="" />
<title><tiles:getAsString name="title" />
</title>
<meta name="google-site-verification" content="8v8zAJic2w2XirLf5cTcaMB3A6sWQHQ_pv2EubyTtZc" />
<meta name="msvalidate.01" content="C3D2C75EC8D7389BDB21FE494808B39D" />
<meta name="y_key" content="b8b20c3f440781c8" />
<link href="${path.css('snapdeal/jqueryUi.css')}" rel="stylesheet" type="text/css" />
<link href="${path.css('snapdeal/style.css')}" rel="stylesheet" type="text/css" />
<link href="${path.css('bootstrap/bootstrap.min.css')}" rel="stylesheet"
	type="text/css" />
<link href="${path.css('chosen/chosen.css')}" rel="stylesheet"
            type="text/css" />           
<!-- <link rel="shortcut icon" href="${path.resources('img/favicon.ico')}" type="image/x-icon" />
<link rel=icon type=image/ico href="${path.resources('img/favicon.ico')}" /> -->
<script>
	if (typeof Snapdeal == 'undefined') {
		Snapdeal = {};
	};
	Snapdeal.getStaticPath = function(path) {
		return '${path.http}' + path;
	}

	Snapdeal.getResourcePath = function(path) {
		return '${path.resources("")}' + path;
	}
</script>
<tiles:insertAttribute name="head" defaultValue="" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

</head>
<body style="width: 100%; height: 100%; min-width: 962px;">
	<div id="system_message">
		<div class="sytem-msj-cont">
			<c:choose>
				<c:when test="${systemMessage != null}">
					<div class="system_${systemMessage.status}">
						<div class="message_inner">
							<c:out value="${systemMessage.message}"></c:out>
							<div class="close"></div>
						</div>
					</div>
				</c:when>
				<c:otherwise>
					<c:choose>
						<c:when test="${param['systemcode'] != null}">
							<c:set var="sysMessage" value="${cache.getCache('systemMessage').getSystemMessageByCode(param['systemcode'])}"></c:set>
							<div class="system_${sysMessage.status}">
								<div class="message_inner">
									<c:out value="${sysMessage.message}"></c:out>
									<div class="close"></div>
								</div>
							</div>
						</c:when>
						<c:otherwise>
							<sec:authorize ifAllGranted="unverified">
								<div class="system_fail">
									<div class="message_inner">
										Please verify your account to activate. Click <a href="${path.http}/sendVerificationEmail">here</a> to send verification email.
										<div class="close"></div>
									</div>
								</div>
							</sec:authorize>
						</c:otherwise>
					</c:choose>
				</c:otherwise>
			</c:choose>
		</div>
	</div>
	<tiles:insertAttribute name="header" defaultValue="/views/layout/loginHeader.jsp" />
		<tiles:insertAttribute name="body" />
	<tiles:insertAttribute name="footer" defaultValue="/views/layout/footer.jsp" />
	<div id="newSubscriber"></div>
	<script type="text/javascript" src="${path.js('jquery/jquery.all.min.js')}"></script>
	<script type="text/javascript"
            src="${path.js('chosen/chosen.jquery.min.js')}"></script>
            
	<tiles:insertAttribute name="deferredScript" defaultValue="" />
	<tiles:insertAttribute name="bodyEnd" defaultValue="" />
</body>
</html>


