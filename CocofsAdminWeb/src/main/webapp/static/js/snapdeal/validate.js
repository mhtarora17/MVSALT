function validate() {
	$('.error').remove();
	validationElement = $("[validation]");
	elements = $(validationElement).length;
	error = "";
	result = false;
	formValid = true;
	for (index = 0; index < elements; index++) {
		
		localFlag = true;
		
		tempElement = $(validationElement)[index];
		element = {
				"validation" : $(tempElement).attr('validation'),
				"validationminlen" : $(tempElement).attr('validationminlen'),
				"validationmaxlen" : $(tempElement).attr('validationmaxlen'),
				"validationname" : $(tempElement).attr('validationname'),
				"validationenabled" : $(tempElement).attr('validationenabled'),
				"minValue" : $(tempElement).attr('minValue'),
				"maxValue" : $(tempElement).attr('maxValue'),
				"zeroAllowed" : $(tempElement).attr('validationZeroAllowed'),
				"startsWith" : $(tempElement).attr('validationStartsWith'),
				"value" : $(tempElement).val()
		};
		console.log(element);
		if(element.validationenabled != null && element.validationenabled == "true"){
			if(typeof element.value == 'undefined' || element.value == null || element.value.trim() == ''){
				result = false;
				localFlag = false;
				error += element.validationname+" is required\n";
			}
			
			if(localFlag){
				
				if(element.validation == "AlphaNumeric"){
					pattern = new RegExp('^[a-zA-Z0-9]+$');
					if(!pattern.test(element.value)){
						result = false;
						localFlag = false;
						error += element.validationname+" should only contain numbers and alphabets\n";
					}
				} else if(element.validation == "AlphaNumericNoSpecialCharSpace"){
					pattern = new RegExp('^[a-zA-Z-0-9_]+$');
					if(!pattern.test(element.value)){
						result = false;
						localFlag = false;
						error += element.validationname+" should only contain numbers or alphabets or underscore\n";
					}
				} else if(element.validation == "AlphaNumericNoSpecialChar"){
					pattern = new RegExp('^[a-zA-Z-0-9.]+$');
					if(!pattern.test(element.value)){
						result = false;
						localFlag = false;
						error += element.validationname+" should only contain numbers and alphabets. no special characters are allowed\n";
					}
				} else if(element.validation == "Alpha" || element.validation == "String"){
					pattern = new RegExp('^[a-zA-Z_]+$');
					if(!pattern.test(element.value)){
						result = false;
						localFlag = false;
						error += element.validationname+" should only contain alphabets\n";
					}
				} else if(element.validation == "Numbers" || element.validation == "Integer"){
					pattern = new RegExp('^[0-9]+$');
					if(!pattern.test(element.value)){
						result = false;
						localFlag = false;
						error += element.validationname+" should only contain Numbers\n";
					}
				} else if(element.validation == "Decimal" || element.validation == "Double"){
					pattern = new RegExp('^(?=.*[1-9])\d*(?:\.\d{1,2})?$');
					if(/^(?=.*[1-9])\d*(?:\.\d{1,2})?$/.test(element.value)==false){
						result = false;
						localFlag = false;
						error += "Please enter valid positive number having atmost 2 digits after decimal.\n";
					}
				} else if(element.validation == "Boolean"){
					if(!(element.value == "true" || element.value == "false")){
						result = false;
						localFlag = false;
						error += element.validationname+" is either true or false\n";
					}
				} else if(element.validation == "NoValidation");
				
			}
			
			if(localFlag){
				if(element.startsWith != null && !strStartsWith(element.value,element.startsWith)){
					result = false;
					localFlag = false;
					error += element.validationname+" must starts with prefix "+element.startsWith+"\n";
				}
			}
			
			if(localFlag){
				if(element.validationminlen != null && element.value.length<element.validationminlen){
					result = false;
					localFlag = false;
					error += element.validationname+" is too short. minimum required length is "+element.validationminlen+"\n";
				}
				if(element.validationmaxlen != null && element.value.length>element.validationmaxlen){
					result = false;
					localFlag = false;
					error += element.validationname+" is too long. maximum Allowed length is "+element.validationmaxlen+"\n";
				}
			}
			
			if(localFlag){
				if(element.minValue != null && parseFloat(element.value) < parseFloat(element.minValue)){
					result = false;
					localFlag = false;
					error += "Minimum "+element.validationname+" should be "+element.minValue+"\n";
				}
				else if(element.maxValue != null && parseFloat(element.value) > parseFloat(element.maxValue)){
					result = false;
					localFlag = false;
					error += "Maximum "+element.validationname+" can be "+element.maxValue+"\n";
				}
				if(element.zeroAllowed != null && element.zeroAllowed == "false" && parseFloat(element.value)==0){
					result = false;
					localFlag = false;
					error += element.validationname+" can not be zero\n";
				}
			}
		}
		if(!localFlag){
			formValid = false;
			$(tempElement).after("<div class='error'>" + error +  "</div>");
		}
		error = "";
	}
	
	
	
	if(formValid){
		result = true;
	}
	
	return result;
};

function strStartsWith(str, prefix) {
    return str.indexOf(prefix) === 0;
}

function markRequired(){
	value = $("[validation]");
	for(i=0;i<value.size();i++){
	temp = value[i].id;
	textt = $( "label[for="+temp+"]" ).text();
	$( "label[for="+temp+"]" ).html(textt+"\<font size=\"3\" color=\"red\"\>*\</font\>");
	}
};