package com.snapdeal.cocofs.adminweb.data.population;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.model.request.AddOrUpdateSellerFulfilmentModelMappingRequest;
import com.snapdeal.cocofs.model.request.AddOrUpdateSellerSubcatFMMappingRequest;
import com.snapdeal.cocofs.model.request.SetProductFulfilmentAttributeRequest;
import com.snapdeal.cocofs.model.response.AddOrUpdateSellerFulfilmentModelMappingResponse;
import com.snapdeal.cocofs.model.response.AddOrUpdateSellerSubcatFMMappingResponse;
import com.snapdeal.cocofs.model.response.SetProductAttributeFulfilmentResponse;
import com.snapdeal.cocofs.sro.FulfillmentModel;
import com.snapdeal.cocofs.sro.ProductFulfilmentAttributeSRO;
import com.snapdeal.cocofs.sro.SellerSubcatFulfilmentModelMappingSRO;
import com.snapdeal.cocofs.utils.ExcelReader;
//import com.snapdeal.cocofs.web.landing.layer.ICocofsFMLandingService;
//import com.snapdeal.cocofs.web.landing.layer.ICocofsLandingService;
import com.snapdeal.shipping.enums.ShippingFulfillmentModelConstants;
import com.snapdeal.vendor.core.model.GetAllVendorFulfillmentModelMappingsRequest;
import com.snapdeal.vendor.core.model.GetAllVendorFulfillmentModelMappingsResponse;
import com.snapdeal.vendor.service.IVendorClientService;
import com.snapdeal.vendor.sro.FulfillmentModelSRO;
import com.snapdeal.vendor.sro.VendorFulfillmentSRO;

@Component
public class DataPopulator {

    private List<ProductFulfilmentAttributeSRO>    attributeList                   = new ArrayList<ProductFulfilmentAttributeSRO>();

    private List<SellerFMMapping>                  sellerFmMappingDTO              = new ArrayList<SellerFMMapping>();

    private List<SellerSubCatFMMapping>            sellerSubCatFmMappingDTO        = new ArrayList<SellerSubCatFMMapping>();

    private static final Logger                    LOG                             = LoggerFactory.getLogger(DataPopulator.class);

    private Map<String, List<FulfillmentModelSRO>> sellerToFulfilmentModel         = new HashMap<String, List<FulfillmentModelSRO>>();

    private static Map<String, FulfillmentModel>   shippingToCocofsFulfilmentModel = new HashMap<String, FulfillmentModel>();

   /* @Autowired
    ICocofsLandingService                          cocofsLandingService;

    @Autowired
    ICocofsFMLandingService                        cocofsFMLandingService;
*/
    @Autowired
    IVendorClientService                           vendorClientService;

    static {
        shippingToCocofsFulfilmentModel.put(ShippingFulfillmentModelConstants.FC_MODEL.getCode(), FulfillmentModel.FC);
        shippingToCocofsFulfilmentModel.put(ShippingFulfillmentModelConstants.FCVOI_MODEL.getCode(), FulfillmentModel.FC_VOI);
        shippingToCocofsFulfilmentModel.put(ShippingFulfillmentModelConstants.FCBOOKS_MODEL.getCode(), FulfillmentModel.FC_BOOKS);
        shippingToCocofsFulfilmentModel.put(ShippingFulfillmentModelConstants.ONESHIP_MODEL.getCode(), FulfillmentModel.ONESHIP);
    }

    public void populateProductFulfilmentAttribute(String filePath, int batchSize) {
        int uploadSize = 0;
        int count = 0;
        try {
            attributeList = ExcelReader.readFile(filePath, ProductFulfilmentAttributeSRO.class);
            uploadSize = attributeList.size();
        } catch (Exception e) {
            LOG.error("Error in reading file", e);
        }

        while (count < uploadSize) {
            int batchNumber = count / batchSize;
            int lastIndex = (count + batchSize - 1 > uploadSize) ? uploadSize : count + batchSize - 1;

            List<ProductFulfilmentAttributeSRO> batch = attributeList.subList(count, lastIndex);
            count += batchSize;
            SetProductFulfilmentAttributeRequest request = new SetProductFulfilmentAttributeRequest();
            for (ProductFulfilmentAttributeSRO supcData : batch) {
                request.addSupcInRequest(supcData.getSupc(), supcData);
            }
            LOG.info("Processing data population for batch {} and supcs {} ", batchNumber, request.toString());

            SetProductAttributeFulfilmentResponse response = new SetProductAttributeFulfilmentResponse();

            //SetProductAttributeFulfilmentResponse response = cocofsLandingService.setProductFulfilmentAttributes(request);
            if (response.isSuccessful()) {
                if (response.getFailures() != null && response.getFailures().size() > 0) {
                    LOG.warn("Failure found  {}", response.getFailures().keySet());
                }
                if (response.getSuccessCount() == batch.size()) {
                    LOG.info("All the request of batch {} are processed successfully", batchNumber);
                }
            } else {
                LOG.error("Error found in persisting data for batchNumber {} and supcs {}", batchNumber, request.getRequestMap().keySet());
            }
        }
    }

    public void populateSellerFMMapping(String filePath, int batchSize) {

        try {
            sellerFmMappingDTO = ExcelReader.readFile(filePath, SellerFMMapping.class);
        } catch (Exception e) {
            LOG.error("Error in reading file", e);
        }

        for (SellerFMMapping mapping : sellerFmMappingDTO) {

            if (validateFromShipping(mapping.getSellerCode(), mapping.getFmCode())) {
                AddOrUpdateSellerFulfilmentModelMappingRequest request = new AddOrUpdateSellerFulfilmentModelMappingRequest(mapping.getSellerCode(),
                        FulfillmentModel.getFulfilmentModelByCode(mapping.getFmCode()));

                LOG.info("Processing sellerFMMapping population for request {} ", request);
                AddOrUpdateSellerFulfilmentModelMappingResponse response = new AddOrUpdateSellerFulfilmentModelMappingResponse();
//                AddOrUpdateSellerFulfilmentModelMappingResponse response = cocofsFMLandingService.addOrUpdateVendorFulfilmentModelMapping(request);
                if (response.isSuccessful()) {
                    LOG.info("sellerFMMapping processed succesfully for request {}", request);
                } else {
                    LOG.info(" Error found in processing sellerFmMapping for requset {}", request);
                }
            } else {
                LOG.warn("FM data is not validate from shipping for sellerCode {}", mapping.getSellerCode());
            }
        }
    }

    public void populateSellerSubCatFMMapping(String filePath, int batchSize) {

        try {
            sellerSubCatFmMappingDTO = ExcelReader.readFile(filePath, SellerSubCatFMMapping.class);
        } catch (Exception e) {
            LOG.error("Error in reading file", e);
        }

        Map<String, List<SellerSubCatFMMapping>> sellerToMappingListMap = prepareSellerToSubCatMapping(sellerSubCatFmMappingDTO);

        for (Entry<String, List<SellerSubCatFMMapping>> entry : sellerToMappingListMap.entrySet()) {

            if (valdiateSubCategoryMappingFromShipping(entry.getKey(), entry.getValue())) {
                List<SellerSubcatFulfilmentModelMappingSRO> mappingSros = new ArrayList<SellerSubcatFulfilmentModelMappingSRO>();
                AddOrUpdateSellerSubcatFMMappingRequest request = new AddOrUpdateSellerSubcatFMMappingRequest();

                for (SellerSubCatFMMapping mapping : entry.getValue()) {
                    SellerSubcatFulfilmentModelMappingSRO sro = new SellerSubcatFulfilmentModelMappingSRO();
                    sro.setFulfilmentModel(FulfillmentModel.getFulfilmentModelByCode(mapping.getFmCode()));
                    sro.setSellerCode(mapping.getSellerCode());
                    sro.setSubcat(mapping.getSubCategory());
                    mappingSros.add(sro);
                }
                request.setSellerSubcatFulfilmentModelMappingList(mappingSros);
                LOG.info("Processing sellerFMSubCatMapping population for sellerCode {} request {} ", entry.getKey(), request);

                AddOrUpdateSellerSubcatFMMappingResponse response = new AddOrUpdateSellerSubcatFMMappingResponse();
//                    cocofsFMLandingService.addOrUpdateVendorSubcatFulfilmentModelMapping(request);

                if (response.isSuccessful()) {
                    if (response.getSellerSubcatToFailureReasonMap() != null && response.getSellerSubcatToFailureReasonMap().size() > 0) {
                        LOG.error("Error in persisiting data for seller {} categories {}", response.getSellerSubcatToFailureReasonMap().keySet());
                    }
                } else {
                    LOG.info(" Error found in processing sellerFmMapping for sellerCode {}", entry.getKey());
                }

            } else {
                LOG.error("Error in validating data for seller {}", entry.getKey());
            }
        }
    }

    private Map<String, List<SellerSubCatFMMapping>> prepareSellerToSubCatMapping(List<SellerSubCatFMMapping> mappings) {
        Map<String, List<SellerSubCatFMMapping>> toReturn = new HashMap<String, List<SellerSubCatFMMapping>>();

        for (SellerSubCatFMMapping mapping : mappings) {
            List<SellerSubCatFMMapping> list = null;
            if (!toReturn.containsKey(mapping.getSellerCode())) {
                list = new ArrayList<SellerSubCatFMMapping>();
            } else {
                list = toReturn.get(mapping.getSellerCode());
            }
            list.add(mapping);
            toReturn.put(mapping.getSellerCode(), list);
        }

        return toReturn;
    }

    public void reloadVendorInfoFormationFromShipping() {
        vendorClientService.setVendorWebServiceURL(ConfigUtils.getStringScalar(Property.SHIPPING_WEB_SERVICE_URL));
        GetAllVendorFulfillmentModelMappingsResponse response = vendorClientService.getAllVendorsFullfillmentMappings(new GetAllVendorFulfillmentModelMappingsRequest());
        if (response.isSuccessful() && response.getVendorFulfillmentMappings() != null && response.getVendorFulfillmentMappings().size() > 0) {
            for (VendorFulfillmentSRO mapping : response.getVendorFulfillmentMappings()) {
                LOG.info("Mapping found for vendor {}", mapping);
                sellerToFulfilmentModel.put(mapping.getVendorCode(), mapping.getVendorFulfillmentMappings());
            }
        } else {
            LOG.error("Error in fetching information from shipping");
        }
    }

    private boolean validateFromShipping(String sellerCode, String fulfilmentModel) {

        List<FulfillmentModelSRO> mappings = sellerToFulfilmentModel.get(sellerCode);

        if (mappings != null && mappings.size() > 0) {
            for (FulfillmentModelSRO sro : mappings) {
                if (shippingToCocofsFulfilmentModel.containsKey(sro.getFulfillmentType())) {
                    FulfillmentModel fm = shippingToCocofsFulfilmentModel.get(sro.getFulfillmentType());
                    if (fm.getCode().equals(fulfilmentModel)) {
                        return true;
                    } else {
                        LOG.warn("didn't match {} with {}", fm.getCode(), fulfilmentModel);
                    }
                } else {
                    LOG.warn("not found in the map");
                }
            }
        }

        return false;
    }

    private boolean valdiateSubCategoryMappingFromShipping(String sellerCode, List<SellerSubCatFMMapping> mappings) {
        for (SellerSubCatFMMapping mapping : mappings) {
            if (!validateFromShipping(sellerCode, mapping.getFmCode())) {
                LOG.warn("Could not validate category {} for seller {} ", mapping.getSubCategory(), mapping.getSellerCode());
                return false;
            }
        }
        return true;
    }
}
