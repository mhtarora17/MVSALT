package com.snapdeal.cocofs.adminweb.data.population;

final class SellerFMMapping {

    private String sellerCode;

    private String fmCode;

    public SellerFMMapping() {

    }

    public SellerFMMapping(String sellerCode, String fmCode) {
        this.sellerCode = sellerCode;
        this.fmCode = fmCode;
    }

    public String getSellerCode() {
        return sellerCode;
    }

    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }

    public String getFmCode() {
        return fmCode;
    }

    public void setFmCode(String fmCode) {
        this.fmCode = fmCode;
    }

}
