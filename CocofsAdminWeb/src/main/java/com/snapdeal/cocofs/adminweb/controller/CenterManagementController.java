/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.adminweb.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.base.utils.ValidatorUtils;
import com.snapdeal.cocofs.adminweb.controller.form.FulfillmentCenterForm;
import com.snapdeal.cocofs.adminweb.dto.CenterTypeDTO;
import com.snapdeal.cocofs.adminweb.dto.CityAndStateDTO;
import com.snapdeal.cocofs.commonweb.utils.WebContextUtils;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.dto.FCUpdateDTO;
import com.snapdeal.cocofs.entity.FCAddress;
import com.snapdeal.cocofs.entity.FulfillmentCentre;
import com.snapdeal.cocofs.generic.persister.exception.GenericPersisterException;
import com.snapdeal.cocofs.services.IFCDetails;
import com.snapdeal.cocofs.services.events.dto.FCAddressUpdateEventObj;
import com.snapdeal.cocofs.services.events.dto.FCAttributeUpdateEventObj;
import com.snapdeal.cocofs.services.events.dto.SellerFMFCUpdateEventObj;
import com.snapdeal.cocofs.services.fm.IFulfillmentCenterService;
import com.snapdeal.cocofs.services.fm.ISellerSUPCFMMappingProcessor;
import com.snapdeal.cocofs.services.fm.ISellerSupcSdInstantMappingProcessor;
import com.snapdeal.cocofs.services.fm.ISellerSupcShipFromMappingProcessor;
import com.snapdeal.cocofs.services.fm.dto.AddressDTO;
import com.snapdeal.score.client.service.IScoreClientService;
import com.snapdeal.score.model.request.GetAllCityAndStateFromPincodeRequest;
import com.snapdeal.score.model.response.GetAllCityAndStateFromPincodeResponse;
import com.snapdeal.score.sro.CityStateTuple;

/**
 * add and update FullFillment Cetnters
 * 
 * @author indrajit
 */
@Controller
@RequestMapping(CenterManagementController.URL)
public class CenterManagementController {

    @Autowired
    private IFCDetails                fcService;

    @Autowired
    private IFulfillmentCenterService fulfillmentService;

    @Autowired
    private IScoreClientService       scoreService;
    
    @Autowired
    @Qualifier("failSafeSellerSUPCFMNotificationSendor")
    private ISellerSupcShipFromMappingProcessor           failSafeShipFromMappingProcessor;

    @Autowired
    @Qualifier("failSafeSellerSUPCFMNotificationSendor")
    private ISellerSupcSdInstantMappingProcessor           failSafeSdInstantMappingProcessor;


    public static final String        URL           = "/admin/centermanagement";

    private static final Logger       LOG           = LoggerFactory.getLogger(CenterManagementController.class);

    private static final String       ATOM          = "[^\\x00-\\x1F^\\(^\\)^\\<^\\>^\\@^\\,^\\;^\\:^\\\\^\\\"^\\.^\\[^\\]^\\s]";
    private static final String       DOMAIN        = "(" + ATOM + "+(\\." + ATOM + "+)+";
    private static final Pattern      EMAIL_PATTERN = java.util.regex.Pattern.compile("^" + ATOM + "+(\\." + ATOM + "+)*@" + DOMAIN + ")$",
                                                            java.util.regex.Pattern.CASE_INSENSITIVE);

    @RequestMapping("/createCenter")
    public String createCenter(ModelMap modelMap) {

        Property p = Property.SELLER_SUPC_FM_FC_UPDATE_PUSH_ENABLED;
        if (!ConfigUtils.getBooleanScalar(p)) {
            modelMap.addAttribute("message", "The page you were looking is not live at this moment.");
            return "admin/fcMapping/message";
        }

        modelMap.addAttribute("fullfillmentCenterForm", new FulfillmentCenterForm());
        return "/admin/center/createCenter";
    }

    @RequestMapping("/updateCenter")
    public String updateCenter(ModelMap modelMap) {
        Property p = Property.SELLER_SUPC_FM_FC_UPDATE_PUSH_ENABLED;
        if (!ConfigUtils.getBooleanScalar(p)) {
            modelMap.addAttribute("message", "The page you were looking is not live at this moment.");
            return "admin/fcMapping/message";
        }
        return "/admin/center/updateCenter";
    }

    @RequestMapping("/createNewCenter")
    public String createNewCenter(@Valid FulfillmentCenterForm centerForm, ModelMap modelMap) {
        try {

            if (validateForm(centerForm, modelMap) && centerForm.getCentercode() != null && centerForm.getCentername() != null) {
                if (centerForm.getCentername().isEmpty()) {
                    modelMap.addAttribute("message", "Center Name can not be left empty");

                } else {
                    if (validateFcCode(centerForm.getCentercode(), modelMap)) {
                        saveCenterDetail(centerForm);
                        modelMap.addAttribute("message", "Successfully Created Center.");
                    }
                }
            }

            modelMap.addAttribute("fulfillmentCenterForm", new FulfillmentCenterForm());
            return "/admin/center/createCenter";

        } catch(GenericPersisterException ex){
            LOG.error("Exception while persisting center details {} " + centerForm.getCentername(), ex);
        }catch (Exception e) {
            LOG.error("Exception while creating center{} " + centerForm.getCentername(), e);
        }
        modelMap.put("fulfillmentCenterForm", new FulfillmentCenterForm());
        return "/admin/center/createCenter";
    }

    private boolean validateForm(FulfillmentCenterForm form, ModelMap modelMap) {
        if (form != null) {
            if (form.getAddressLine1().isEmpty() || form.getAddressLine1() == null || form.getPincode().isEmpty() || form.getPincode() == null || form.getEmail().isEmpty()
                    || form.getEmail() == null || form.getName().isEmpty() || form.getName() == null || form.getMobile().isEmpty() || form.getMobile() == null
                    || form.getCentername() == null || form.getCentername().isEmpty()) {
                modelMap.addAttribute("message", "Please fill all mandatory field correctly");
                return false;

            } /*else if (!validateCityAndState(form.getPincode(), form.getCity(), form.getState())) {
                modelMap.addAttribute("message", "Please choose correct PIN No...");
                return false;
            } */else if (!isEmailPatternValid(form.getEmail())) {

                modelMap.addAttribute("message", "Please enter correct email...");
                return false;

            } else if (!StringUtils.isNotEmpty(ValidatorUtils.getValidMobileOrNull(form.getMobile()))) {

                modelMap.addAttribute("message", "Please enter correct mobile number...");
                return false;

            } else if (null != form.getLandLine() && !form.getLandLine().isEmpty() && !validatelandLineNumber(form.getLandLine().trim())) {
                modelMap.addAttribute("message", "land line number should have only digits and not more then 11 digits");
                return false;

            } else if (form.getCentercode() != null) {
                Pattern pattern = Pattern.compile("^[a-zA-Z 0-9\\_\\-]*$");
                Matcher matcher = pattern.matcher(form.getCentercode());
                boolean found = matcher.find();

                if (!found || form.getCentercode().contains(" ")) {
                    modelMap.addAttribute("message", "Center Code does not allow any special character and whitespace...");
                    return false;
                }
            }
        }
        return true;
    }

    private boolean validateCityAndState(String pincode, String city, String state) {
        GetAllCityAndStateFromPincodeResponse response = getCityAndStateFromPincode(pincode);
        if (null != response && null != response.getcitystateList()) {
            for (CityStateTuple cityStatePair : response.getcitystateList()) {
                if (cityStatePair.getCity().equals(city) && cityStatePair.getState().equals(state)) {
                    return true;
                }

            }
        }
        return false;
    }

    private boolean validatelandLineNumber(String landLineNo) {
        Integer validLandLineNumberLength = ConfigUtils.getIntegerScalar(Property.VALID_LAND_LINE_NUMBER_LENGTH);
        if (landLineNo.length() == validLandLineNumberLength) {
            Pattern pattern = Pattern.compile("^(\\d)++$");
            return pattern.matcher(landLineNo).matches();
        }
        return false;
    }

    private boolean validateFcCode(String fcCode, ModelMap modelMap) {

        if (fcCode.trim().length() > 16) {
            modelMap.addAttribute("message", "FC Code can not be more than 16 Characters");
            LOG.error("FC Code length is more than 16 character");
            return false;
        }
        if (fcCode != null && !fcCode.isEmpty()) {
            FCAddress shippingDetail = fulfillmentService.getFulfillmentAddressByCode(fcCode);
            if (shippingDetail != null) {
                modelMap.addAttribute("message", "FCCode already Exist");
                return false;
            }
        } else {
            LOG.error("fcCode is found null ..");
            modelMap.addAttribute("message", "FCCode can not be empty");
            return false;
        }

        return true;
    }

    private void saveCenterDetail(FulfillmentCenterForm form) throws GenericPersisterException {
        String username = WebContextUtils.getCurrentUserEmail();
        FulfillmentCentre center = new FulfillmentCentre();
        FCAddress fcAddress = getShippingDetailFromForm(form);
        fcAddress.setUpdatedBy(username);
        center.setFcAddress(fcAddress);
        center.setCode(form.getCentercode());
        center.setName(form.getCentername());
        center.setType(form.getFulfillmentType());
        center.setEnabled(true);
        center.setSdinstant(form.isSdInstant());
        center.setUpdatedBy(username);
       fulfillmentService.saveOrUpdateFulfilmentCenterWithAerospike(center);

    }

    @RequestMapping("/getAllCenterTypes")
    public @ResponseBody CenterTypeDTO getAllCenterTypes(ModelMap modelMap) {

        Property p = Property.COCOFS_CENTER_TYPES;
        String centerTypes = ConfigUtils.getStringScalar(p);
        String[] centers = centerTypes.split(",");
        List<String> listOfCenters = Arrays.asList(centers);
        CenterTypeDTO centerType = new CenterTypeDTO(listOfCenters);
        return centerType;

    }

    @RequestMapping("/searchCenter")
    public @ResponseBody FCUpdateDTO searchCenter(@RequestParam("fcCode") String fcCode, ModelMap modelMap) {
        FCUpdateDTO fcUpdateDTO = null;
        FulfillmentCentre fmDetails = null;
        if (StringUtils.isNotEmpty(fcCode) && fcCode != null) {
            fmDetails = fulfillmentService.getFulfillmentCentreByCode(fcCode);
            if (fmDetails == null) {
                modelMap.put("message", "Either Center does not exist or it is disabled.");
            } else {
                fcUpdateDTO = buildFCDetailsDTO(fmDetails);
            }
        } else {

            modelMap.put("message", "Invalid FC Code ..");
            fcUpdateDTO = new FCUpdateDTO();
        }

        return fcUpdateDTO;

    }

    @RequestMapping("/updateCenterDetail")
    public String updateCenter(FulfillmentCenterForm form, ModelMap modelMap) {
        try {
            if (validateForm(form, modelMap)) {
                updateCenterInfo(form, modelMap);
                LOG.info("Center {} is saved successfully");
                modelMap.put("message", "Center Updated Successfully");
            }
        } catch (Exception e) {
            LOG.error("Exception while updating center detail with email " + form.getEmail(), e);
        }
        modelMap.put("fulfillmentCenterForm", new FulfillmentCenterForm());
        return "/admin/center/updateCenter";
    }

    private void updateCenterInfo(FulfillmentCenterForm form, ModelMap modelMap) throws Exception {
        try {
            String username = WebContextUtils.getCurrentUserEmail();
            FulfillmentCentre fc = fulfillmentService.getFulfillmentCentreByCode(form.getCentercode());
            String oldPincode = fc.getFcAddress().getPincode();
            boolean oldSdInstant = (fc.getSdinstant() == null) ? false : fc.getSdinstant();
            fc.setName(form.getCentername());
            fc.setUpdatedBy(username);
            fc.setSdinstant(form.isSdInstant());
                        
            fc.getFcAddress().setAddressLine1(form.getAddressLine1());
            fc.getFcAddress().setAddressLine2(form.getAddressLine2());
            fc.getFcAddress().setCity(form.getCity());
            fc.getFcAddress().setEmail(form.getEmail());
            fc.getFcAddress().setLandLine(form.getLandLine());
            fc.getFcAddress().setState(form.getState());
            fc.getFcAddress().setMobile(form.getMobile());
            fc.getFcAddress().setName(form.getName());
            fc.getFcAddress().setPincode(form.getPincode());
            fc.getFcAddress().setUpdatedBy(username);
            fc.getFcAddress().setUpdated(DateUtils.getCurrentTime());
            fulfillmentService.saveOrUpdateFulfilmentCenterWithAerospike(fc);
            
            if(oldPincode != form.getPincode()){
                pushUpdatesForFCAddressMappingUpdate(fc.getCode(), oldPincode, form.getPincode());
            }
            
            if( oldSdInstant ^ form.isSdInstant()){
                pushUpdatesForFCSdInstantUpdate(fc.getCode(), form.isSdInstant());
            }
        } catch (Exception ex) {
            LOG.error("Error while updating the Center info " + ex);
            modelMap.addAttribute("message", "FC update has some error");
            throw ex;

        }

    }

    private void pushUpdatesForFCAddressMappingUpdate(String fcCode, String oldPincode, String newPincode) {
        //fulfilment mode does not change here 
        try{
            failSafeShipFromMappingProcessor.processChangeOfFCAddressForExternalNotification(new FCAddressUpdateEventObj(fcCode, oldPincode, newPincode));
        }catch (Exception gpe) {
            LOG.error("Could not processChangeOfFCAddressForExternalNotification for fcCode:{}  ", fcCode);
        }
    }
    
    private void pushUpdatesForFCSdInstantUpdate(String fcCode, boolean sdInstant) {
        try{
            failSafeSdInstantMappingProcessor.processChangeOfFCAttributesForExternalNotification(new FCAttributeUpdateEventObj(fcCode, sdInstant));
        }catch (Exception gpe) {
            LOG.error("Could not process sdInstant update for External notification for fcCode:{}  ", fcCode);
        }
    }
    
    
    private FCAddress getShippingDetailFromForm(FulfillmentCenterForm form) {
        FCAddress shippingDetail = null;
        if (form != null) {
            shippingDetail = new FCAddress();
            shippingDetail.setId(form.getId());
            shippingDetail.setAddressLine1(form.getAddressLine1());
            shippingDetail.setAddressLine2(form.getAddressLine2());
            shippingDetail.setCity(form.getCity());
            shippingDetail.setEmail(form.getEmail());
            shippingDetail.setLandLine(form.getLandLine());
            shippingDetail.setState(form.getState());
            shippingDetail.setMobile(form.getMobile());
            shippingDetail.setName(form.getName());
            shippingDetail.setPincode(form.getPincode());

        }
        return shippingDetail;

    }

    @RequestMapping("/getCityState")
    public @ResponseBody CityAndStateDTO getCityAndState(@RequestParam("pincode") String pincode, ModelMap modelMap) {
        CityAndStateDTO cityAndStateDTO = null;
        List<String> city = new ArrayList<String>();
        List<String> state = new ArrayList<String>();
        try {

            Pattern p = Pattern.compile("[1-9][0-9]{5}");
            Matcher m = p.matcher(pincode);
            if (!m.matches()) {
                modelMap.put("message", "pincode does not exist");
            } else {
                GetAllCityAndStateFromPincodeResponse response = getCityAndStateFromPincode(pincode);

                if (response != null && response.isSuccessful()) {
                    if (response.getcitystateList() != null) {
                        LOG.debug("NO of items = " + response.getcitystateList().size());
                        for (CityStateTuple cityAndState : response.getcitystateList()) {
                            LOG.debug("City: " + cityAndState.getCity() + " State : " + cityAndState.getState());
                            city.add(cityAndState.getCity());
                            state.add(cityAndState.getState());
                            break;
                        }
                        cityAndStateDTO = new CityAndStateDTO(city, state);
                    }
                    LOG.info("response recieved from score ..:");
                } else {
                    LOG.info("Unable to get city and state for pincode" + " " + pincode);
                    modelMap.put("message", "pincode does not exist");
                }
            }

        } catch (Exception e) {
            LOG.error("exception while calling score to fetch city and state from pincode {}", e);
        }

        return cityAndStateDTO;

    }

    private GetAllCityAndStateFromPincodeResponse getCityAndStateFromPincode(String pincode) {
        LOG.info("calling score to fetch fetch city and state from pincode:");
        GetAllCityAndStateFromPincodeRequest request = new GetAllCityAndStateFromPincodeRequest(pincode);
        GetAllCityAndStateFromPincodeResponse response = scoreService.getAllCityAndStateFromPincode(request);
        return response;
    }

    private boolean isEmailPatternValid(String value) {
        if (value == null || value.length() == 0) {
            return false;
        }
        return EMAIL_PATTERN.matcher(value).matches();
    }

    private FCUpdateDTO buildFCDetailsDTO(FulfillmentCentre fmCenter) {
        FCUpdateDTO fcUpdateDTO = new FCUpdateDTO();
        fcUpdateDTO.setCentername(fmCenter.getName());
        fcUpdateDTO.setCentercode(fmCenter.getCode());
        fcUpdateDTO.setFulfillmentType(fmCenter.getType());
        fcUpdateDTO.setSdInstant(fmCenter.getSdinstant());
        FCAddress fcAddress = fmCenter.getFcAddress();
        if (null == fcAddress) {
            LOG.info("FCAddress is coming null for fcCode:");
            return null;
        }
        fcUpdateDTO.setAddressLine1(fcAddress.getAddressLine1());
        fcUpdateDTO.setAddressLine2(fcAddress.getAddressLine2());
        fcUpdateDTO.setName(fcAddress.getName());
        fcUpdateDTO.setCity(fcAddress.getCity());
        fcUpdateDTO.setEmail(fcAddress.getEmail());
        fcUpdateDTO.setLandLine(fcAddress.getLandLine());
        fcUpdateDTO.setMobile(fcAddress.getMobile());
        fcUpdateDTO.setPincode(fcAddress.getPincode());
        fcUpdateDTO.setState(fcAddress.getState());
        fcUpdateDTO.setId(fcAddress.getId());
        
        return fcUpdateDTO;
    }

}
