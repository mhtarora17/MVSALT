/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 21-Aug-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.adminweb.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.snapdeal.cocofs.commonweb.utils.dto.JqgridResponse;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.services.task.dto.TaskDetailDTO;
import com.snapdeal.cocofs.services.task.dto.TaskParamDTO;
import com.snapdeal.cocofs.services.task.update.ITaskUpdateService;

@RequestMapping("/admin/task/")
@Controller("TaskUpdateController")
public class TaskUpdateController {

    @Autowired
    ITaskUpdateService taskUpdateService;

    @RequestMapping("taskDetail")
    public String getUpdateTaskDetailPage(ModelMap modelMap) {
        modelMap.addAttribute("implClasses", ConfigUtils.getMap(Property.TASK_IMPL_CLASSES));
        modelMap.addAttribute("hostNames", ConfigUtils.getStringList(Property.HOST_NAMES));
        return "/admin/admintasks/updateTaskDetail";
    }

    @RequestMapping("taskInfo")
    public String getUpdateTaskPage(ModelMap modelMap) {
        modelMap.addAttribute("scheduledTaskInfo", taskUpdateService.getAllEnabledTaskInfo());
        return "/admin/admintasks/updateTask";
    }

    @RequestMapping("/getTaskInfo")
    public @ResponseBody
    JqgridResponse<TaskDetailDTO> getTaskInfo(ModelMap modelMap, @RequestParam(value = "implClass") String implClass,
            @RequestParam(value = "page", required = false, defaultValue = "1") int page, @RequestParam(value = "rows", required = false, defaultValue = "40") int rows) {
        JqgridResponse<TaskDetailDTO> resp = new JqgridResponse<TaskDetailDTO>();
        List<TaskDetailDTO> dtoList = null;
        if (StringUtils.isNotEmpty(implClass) && ConfigUtils.getMap(Property.TASK_IMPL_CLASSES).containsValue(implClass)) {
            dtoList = taskUpdateService.getAllTaskByClass(implClass);
        } else {
            dtoList = taskUpdateService.getAllTasks();
        }
        int records = dtoList.size();
        resp.setRecords(String.valueOf(records));
        Integer pages = records == rows ? records / rows : records / rows + 1;
        resp.setTotal(pages);
        resp.setPage(String.valueOf(page));
        resp.setRows(dtoList);
        return resp;
    }

    @RequestMapping("/getTaskParamInfo")
    public @ResponseBody
    JqgridResponse<TaskParamDTO> getTaskParamInfo(ModelMap modelMap, @RequestParam(value = "taskName") String taskName,
            @RequestParam(value = "page", required = false, defaultValue = "1") int page, @RequestParam(value = "rows", required = false, defaultValue = "40") int rows) {
        JqgridResponse<TaskParamDTO> resp = new JqgridResponse<TaskParamDTO>();
        List<TaskParamDTO> dtoList = null;
        dtoList = new ArrayList<TaskParamDTO>(taskUpdateService.getTaskParamsByTaskName(taskName));
        int records = dtoList.size();
        resp.setRecords(String.valueOf(records));
        Integer pages = records == rows ? records / rows : records / rows + 1;
        resp.setTotal(pages);
        resp.setPage(String.valueOf(page));
        resp.setRows(dtoList);
        return resp;
    }

}
