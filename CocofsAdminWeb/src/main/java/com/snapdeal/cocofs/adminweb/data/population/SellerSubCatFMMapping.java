package com.snapdeal.cocofs.adminweb.data.population;

final class SellerSubCatFMMapping {

    private String sellerCode;

    private String fmCode;

    private String subCategory;

    public SellerSubCatFMMapping() {

    }

    public SellerSubCatFMMapping(String sellerCode, String fmCode, String subCategory) {
        super();
        this.sellerCode = sellerCode;
        this.fmCode = fmCode;
        this.subCategory = subCategory;
    }

    public String getSellerCode() {
        return sellerCode;
    }

    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }

    public String getFmCode() {
        return fmCode;
    }

    public void setFmCode(String fmCode) {
        this.fmCode = fmCode;
    }

    public String getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }

}
