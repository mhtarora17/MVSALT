package com.snapdeal.cocofs.adminweb.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.jms.JMSException;

import org.apache.activemq.command.ActiveMQObjectMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.snapdeal.base.activemq.IActiveMQManager;
import com.snapdeal.base.activemq.exception.ActiveMQException;
import com.snapdeal.base.cache.CacheManager;
import com.snapdeal.base.model.common.ServiceResponse;
import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.catalog.base.model.GetAllCategoryListRequest;
import com.snapdeal.catalog.base.model.GetCategoryListResponse;
import com.snapdeal.catalog.base.model.GetMinProductInfoBySupcRequest;
import com.snapdeal.catalog.base.model.GetMinProductInfoResponse;
import com.snapdeal.cocofs.adminweb.data.population.DataPopulator;
import com.snapdeal.cocofs.cache.loader.impl.SellerFMMappingCache;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.core.aerospike.vo.SellerToSupcListVO;
import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.external.service.factory.IExternalServiceFactory;
import com.snapdeal.cocofs.services.consumer.INewSellerSupcUpdateConsumer;
import com.snapdeal.cocofs.services.fm.dto.SellerFmFcDTO;
import com.snapdeal.ipms.base.queue.update.SUPCSellerUpdateEventSRO;
import com.snapdeal.ipms.base.queue.update.SUPCSellerUpdateEventsSRO;
import com.snapdeal.score.data.aerospike.exception.GetFailedException;
import com.snapdeal.score.data.aerospike.exception.PutFailedException;
import com.snapdeal.score.data.aerospike.service.impl.ScoreAerospikeLDTService;
import com.snapdeal.score.data.aerospike.service.impl.ScoreAerospikeService;

@Controller
@RequestMapping("/test")
public class TestController {

    @Autowired
    IExternalServiceFactory          externalServiceFactory;

    @Autowired
    @Qualifier("newSellerSupcUpdateConsumer")
    INewSellerSupcUpdateConsumer     newSellerSupcUpdateConsumer;

    @Autowired
    @Qualifier("aerospikeLDTService")
    private ScoreAerospikeLDTService aerospikeLDTService;

    @Autowired
    @Qualifier("aerospikeService")
    private ScoreAerospikeService    aerospikeService;
 
    private static final Logger      LOG = LoggerFactory.getLogger(TestController.class);

    @Autowired
    DataPopulator                    dataPopulator;

    @RequestMapping("/testAS")
    public void testAS(@RequestParam String seller, @RequestParam String supc) throws GetFailedException, PutFailedException,
            com.snapdeal.score.data.aerospike.exception.PutFailedException, com.snapdeal.score.data.aerospike.exception.GetFailedException {

        Set<String> s1 = new java.util.HashSet<String>();

        SellerToSupcListVO vo = new SellerToSupcListVO();

        vo = (SellerToSupcListVO) aerospikeLDTService.get(seller, SellerToSupcListVO.class);

        if (vo != null) {
            LOG.info("found");
            LOG.info("1_: " + vo);
            List<String> list = (List<String>) aerospikeLDTService.getLDTSet(seller, SellerToSupcListVO.class, "ssls");
            LOG.info("(1_list). " + list);

        }

        if (vo == null) {
            LOG.info("not found");
            vo = new SellerToSupcListVO();
            vo.setSellerCode(seller);
        }

        if (!aerospikeLDTService.searchInLDTSet(seller, SellerToSupcListVO.class, "ssls", supc)) {
            LOG.info("supc: " + supc + "does not exists alreay, hence putting it");
            s1.add(supc);
        } else {
            LOG.info("supc: " + supc + "d exists alreay, hence skipping it");

        }

        aerospikeLDTService.putWithLDTSet(vo, "ssls", s1);

        vo = (SellerToSupcListVO) aerospikeLDTService.get(seller, SellerToSupcListVO.class);
        if (vo != null) {
            LOG.info("2_: " + vo);
            List<String> list = (List<String>) aerospikeLDTService.getLDTSet(seller, SellerToSupcListVO.class, "ssls");
            LOG.info("(2_list). " + list);

        }

    }

    @RequestMapping(value="/testActiveMQPush", produces = "application/json")
    @ResponseBody
    public ServiceResponse testActiveMQPush(@RequestParam String sellerCode, @RequestParam String supc) {

        //register dummy producer
        registerProducer(ConfigUtils.getStringScalar(Property.SELLER_SUPC_ASSOCIATION_QUEUE), ConfigUtils.getStringScalar(Property.ACTIVE_MQ_URL),
                ConfigUtils.getStringScalar(Property.ACTIVE_MQ_USERNAME), ConfigUtils.getStringScalar(Property.ACTIVE_MQ_PASSWORD));

        //create message and send

        SUPCSellerUpdateEventSRO o = new SUPCSellerUpdateEventSRO(supc, sellerCode, new Date());
        List<SUPCSellerUpdateEventSRO> lst = new ArrayList<SUPCSellerUpdateEventSRO>();
        lst.add(o);

        SUPCSellerUpdateEventsSRO msg = new SUPCSellerUpdateEventsSRO(lst);

        simulateIPMSPushForSellerSUPCMapping(msg);

        //unregister dummy producer
        unregisterProducer();
        
        return new ServiceResponse();
    }

    @RequestMapping("/fmCache")
    public void fmCache(@RequestParam String seller) {
        SellerFMMappingCache sellerFMMappingCache = CacheManager.getInstance().getCache(SellerFMMappingCache.class);
        
        SellerFmFcDTO dto;
        try {
            dto = sellerFMMappingCache.getSellerFmFcMapping(seller);
            LOG.info("dto : {}",String.valueOf(dto));

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    
    @RequestMapping("/categories")
    public void getCamsCategories() {
        try {
            GetCategoryListResponse response = externalServiceFactory.getCAMSExternalService().getAllCategories(new GetAllCategoryListRequest());
            LOG.info(String.valueOf(response.getProductCategories().size()));
        } catch (ExternalDataNotFoundException e) {
            LOG.error("ExternalDataNotFoundException ecnountered {}", e);
        } catch (Exception e) {
            LOG.error("exception ecnountered {}", e);
        }

    }

    @RequestMapping("/product")
    public void getCamsProductInfo(@RequestParam String supc) {
        try {
            GetMinProductInfoResponse response = externalServiceFactory.getCAMSExternalService().getMinProductInfo(new GetMinProductInfoBySupcRequest(supc));
        } catch (ExternalDataNotFoundException e) {
            LOG.error("ExternalDataNotFoundException ecnountered {}", e);
        } catch (Exception e) {
            LOG.error("exception ecnountered {}", e);
        }

    }
    
    

    @RequestMapping("/onMessage")
    public void testOnMessage(@RequestParam String sellerCode, @RequestParam String supc) throws JMSException {
        SUPCSellerUpdateEventsSRO sro = new SUPCSellerUpdateEventsSRO();
        List<SUPCSellerUpdateEventSRO> eventList = new ArrayList<SUPCSellerUpdateEventSRO>();
        SUPCSellerUpdateEventSRO event = new SUPCSellerUpdateEventSRO(sellerCode, supc, DateUtils.getCurrentTime());
        eventList.add(event);
        sro.setSucpSellerUpdateEventList(eventList);
        ActiveMQObjectMessage message = new ActiveMQObjectMessage();
        message.setObject(sro);
        newSellerSupcUpdateConsumer.onMessage(message);
    }

    @RequestMapping("/supcData")
    public void populateSupcData(@RequestParam String fileName, @RequestParam String batchSize) {
        dataPopulator.populateProductFulfilmentAttribute(prepareAbsolutePath(fileName), Integer.parseInt(batchSize));
    }

    @RequestMapping("/loadVendorInfo")
    public void loadVendorInfo() {
        LOG.info("Loading vendor info");
        dataPopulator.reloadVendorInfoFormationFromShipping();
    }

    @RequestMapping("/sellerFMMapping")
    public void populateSellerFMMapping(@RequestParam String fileName, @RequestParam String batchSize) {
        dataPopulator.populateSellerFMMapping(prepareAbsolutePath(fileName), Integer.parseInt(batchSize));
    }

    @RequestMapping("/sellerSubcatFMMapping")
    public void populateSellerSubCatFMMapping(@RequestParam String fileName, @RequestParam String batchSize) {
        dataPopulator.populateSellerSubCatFMMapping(prepareAbsolutePath(fileName), Integer.parseInt(batchSize));
    }

    private String prepareAbsolutePath(String fileName) {
        return "/tmp/" + fileName;
    }

    @Autowired
    private IActiveMQManager activeMQManager;
    private static Long      activeMqToken = null;
    private String           queueName     = null;
    private String           queueURL      = null;

    private void registerProducer(String queueName, String networkPath, String username, String password) {

        this.queueName = queueName;
        this.queueURL = networkPath;

        try {
            if (activeMqToken == null) {
                activeMqToken = activeMQManager.registerPublisher(queueName, networkPath, username, password);
                LOG.info("Active mq producer registered successfully  ...");
            } else {
                LOG.info("Active mq producer was already registered ... not attempting registration");
            }
        } catch (Exception e) {
            LOG.error("unable to register external-notification-service producer to activemq. " + "attempted with queueName:" + queueName + ", queueURL:" + queueURL + ", username"
                    + username, e);
        }
    }

    private void unregisterProducer() {
        try {
            if (activeMqToken != null) {
                activeMQManager.unregisterPublisher(activeMqToken);
                LOG.info("Active mq producer unregistered successfully  ...");
            }
            activeMqToken = null;
        } catch (Exception e) {
            LOG.error("unable to unregister external-notification-service producer from activemq", e);
        }
    }

    private void simulateIPMSPushForSellerSUPCMapping(SUPCSellerUpdateEventsSRO event) {

        LOG.info("publishing to coco consumer for ipms push handler started ... item:" + event);

        List<SUPCSellerUpdateEventSRO> itms = event.getSucpSellerUpdateEventList();
        if (itms != null && !itms.isEmpty()) {

            try {
                LOG.debug("Publishing updates on url: " + queueURL + " with update event: " + event + " on queue: " + queueName + ", size:" + itms.size());
                activeMQManager.publish(activeMqToken, event);
                LOG.info("Published updates on url: " + queueURL + " with update event: " + event + " on queue: " + queueName);
            } catch (ActiveMQException e) {
                LOG.error("Error publishing post update on url: " + queueURL + " with update event: " + event + " on queue: " + queueName);
                LOG.error("Exception", e);
            } catch (JMSException e) {
                LOG.error("Error publishing on url: " + queueURL + " with update event: " + event + " on queue: " + queueName);
                LOG.error("Exception", e);
            } catch (Exception e) {
                LOG.error("Unable to publish on url: " + queueURL + " with update event: " + event + " on queue: " + queueName);
                LOG.error("Exception", e);
            }
        }
        LOG.info("publishing to coco consumer for ipms push handler done ...");

    }

    public void setActiveMQManager(IActiveMQManager activeMQManager) {
        this.activeMQManager = activeMQManager;
    }

}
