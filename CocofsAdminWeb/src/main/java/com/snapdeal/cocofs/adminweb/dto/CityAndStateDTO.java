/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.adminweb.dto;

import java.util.List;

/**
 *  
 *  
 *  @author indrajit
 */
public class CityAndStateDTO {
    
    
    private List<String> city;
    
    private List<String>  state;

    public CityAndStateDTO(List<String> city, List<String> state) {
        this.city=city;
        this.state=state;
       
    }

    public List<String> getCity() {
        return city;
    }

    public void setCity(List<String> city) {
        this.city = city;
    }

    public List<String> getState() {
        return state;
    }

    public void setState(List<String> state) {
        this.state = state;
    }
    
    

}
