/**
 
*  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.adminweb.controller;

/**
 *  
 *  @version     1.0, 09-Dec-2015
 *  @author vikas sharma
 */

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.snapdeal.base.fileutils.SDFileUploadUtils;
import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.cocofs.common.JobValidationException;
import com.snapdeal.cocofs.commonweb.security.CocofsUser;
import com.snapdeal.cocofs.commonweb.utils.ExportToExcelUtils;
import com.snapdeal.cocofs.commonweb.utils.WebContextUtils;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.db.users.IUserService;
import com.snapdeal.cocofs.entity.JobDetail;
import com.snapdeal.cocofs.enums.JobActionCode;
import com.snapdeal.cocofs.excelupload.SupcPackagingTypeUploadDTO;
import com.snapdeal.cocofs.services.IJobSchedulerService;
import com.snapdeal.cocofs.services.ISupcToPackagingTypeMappingService;
import com.snapdeal.cocofs.services.fm.IFulfillmentCenterService;
import com.snapdeal.cocofs.utils.ExcelReader;
import com.snapdeal.packaman.storecodeandpt.dto.ScAndPtDownloadDto;
import com.snapdeal.packman.services.packaging.IPackmanExclusiveService;

@Controller
@RequestMapping(SupcPackagingTypeMappingController.URL)
public class SupcPackagingTypeMappingController {
    public static final String                 URL = "/admin/supcpackagingtypemapping/";

    private static final Logger                LOG = LoggerFactory.getLogger(SupcPackagingTypeMappingController.class);

    @Autowired
    private IJobSchedulerService               jobSchedulerService;

    @Autowired
    private ISupcToPackagingTypeMappingService supcToPackagingTypeMappingService;

    @Autowired
    private IPackmanExclusiveService           packmanService;

    @Autowired
    IFulfillmentCenterService                  fulfillmentCenterService;

    @Autowired
    IUserService                               userService;

    @RequestMapping(method = { RequestMethod.GET }, value = "supcpackagingtypemappingupload")
    public String uploadPage(ModelMap map) {
        return "admin/supcpackagingtypemapping/supcpackagingtypemappingupload";
    }

    @RequestMapping(method = { RequestMethod.GET }, value = "downloadsupcpackagingtypemappingTemplate")
    public void downloadSupcPackagingTypeMappingTemplate(HttpServletRequest req, HttpServletResponse resp) {
        try {
            ExportToExcelUtils.downloadTemplate(resp, SupcPackagingTypeUploadDTO.class, "SupcPackagingTypeMappingTemplate");
        } catch (IOException | ClassNotFoundException e) {
            LOG.info("Errr while exporting template for attributes ", e);
        }
    }

    @RequestMapping("/downloadAllStoreCodPackagingTypeList")
    public void downloadAllStoreCodeList(HttpServletRequest req, HttpServletResponse resp) throws ClassNotFoundException, IOException {

        List<ScAndPtDownloadDto> sclist = supcToPackagingTypeMappingService.getAllSCs();

        ExportToExcelUtils.exportToExcel(resp, ScAndPtDownloadDto.class, "List", sclist, true, false);

    }

    @RequestMapping(method = { RequestMethod.POST }, value = "processSupcPackagingTypeMappingUpload")
    public String upload(ModelMap map, @RequestParam(value = "inputfile") MultipartFile inputFile) {
        boolean fileupload = validateUploadedFile(map, inputFile);
        String message = "Supc PackagingTypeMapping Upload Could not be done. ";
        if (fileupload) {
            try {
                String userEmail = WebContextUtils.getCurrentUserEmail();

                JobDetail jd = jobSchedulerService.saveJob(inputFile, JobActionCode.SUPC_PACKAGINGTYPE_MAPPING.getCode(), userEmail);

                String actionCodeMSG = "";
                if (jd.getAction() != null) {
                    actionCodeMSG = ", Action code:" + jd.getAction().getCode();
                }
                map.addAttribute("message", "Supc Packaging Type Mapping upload done. <br>Job code:" + jd.getFileCode() + actionCodeMSG + ", saved at  " + jd.getFilePath()
                        + File.separator + jd.getFileName());

            } catch (IOException e) {
                LOG.info(message, e);
                map.addAttribute("message", message);
            } catch (JobValidationException e) {
                LOG.error(message, e);
                map.addAttribute("message", message + e.getMessage() + " Message:" + e.getResponse());
                map.addAttribute("validationResponse", e.getResponse());
            } catch (Exception e) {
                LOG.info(message, e);
                map.addAttribute("message", "Supc Packaging Type Mapping Upload could not be done , something bad happened.");
            }
        }
        return "admin/supcpackagingtypemapping/supcpackagingtypemappingupload";
    }

    public boolean validateUploadedFile(ModelMap map, MultipartFile inputFile) {
        boolean fileupload = true;
        String uploadFilePath = ConfigUtils.getStringScalar(Property.TEMP_DIRECTORY) + inputFile.getOriginalFilename();
        List<String> errors = new ArrayList<>();
        try {
            SDFileUploadUtils.upload(inputFile.getInputStream(), inputFile.getContentType(), uploadFilePath);
            LOG.info("FILE upload at " + uploadFilePath);
            List<SupcPackagingTypeUploadDTO> supcPackagingMappingList = ExcelReader.readFile(uploadFilePath, SupcPackagingTypeUploadDTO.class, true, 0, false, true);
            if (supcPackagingMappingList != null && supcPackagingMappingList.size() > 0) {
                LOG.info("FILE have content to read.");
                int count = 0;
                Set<String> storeCodeSet = getUserAccessibleStores();

                for (SupcPackagingTypeUploadDTO pTRL : supcPackagingMappingList) {
                    count++;
                    if (pTRL.getSupc() == null) {
                        fileupload = false;
                        String msg = "FILE does not contain valid SUPC at row " + count + ".";
                        LOG.error(msg);
                        errors.add(msg);
                    }
                    if (pTRL.getStoreCode() == null) {
                        fileupload = false;
                        String msg = "FILE does not contain valid STORE CODE at row " + count + ".";
                        LOG.error(msg);
                        errors.add(msg);
                    }
                    if (!storeCodeSet.contains(pTRL.getStoreCode())) {
                        fileupload = false;
                        String msg = "User not have permission to create Supc Mapping at Store " + pTRL.getStoreCode() + " at row " + count + ".";
                        LOG.error(msg);
                        errors.add(msg);
                    }

                }
            } else {
                fileupload = false;
                String msg = "FILE uploaded is empty or contain invalid data.";
                LOG.error(msg);
                errors.add(msg);
            }
        } catch (Exception e) {
            fileupload = false;
            LOG.error("Error in reading file {}", inputFile, e);
            String msg = "Error in reading file, please check the content and size. " + e.getMessage();
            errors.add(msg);
        }

        StringBuilder sb = new StringBuilder();
        String BR = "<br/>";
        if (!errors.isEmpty()) {
            sb.append("<pre>");
            sb.append("Total errors: " + errors.size());
            sb.append(BR);
            int count = 1;
            for (String o : errors) {
                sb.append((count++) + " [" + o + "]");
                sb.append(BR);
            }
            sb.append("</pre>");
            String allErrors = sb.toString();
            map.addAttribute("message", allErrors);
            LOG.error("All errors {}", allErrors);
        }
        return fileupload;
    }

    private Set<String> getUserAccessibleStores() {

        String email = WebContextUtils.getCurrentUserEmail();
        CocofsUser user = WebContextUtils.getCurrentUser();
        boolean superUser = false;
        if (user != null) {
            superUser = user.hasRole("admin") || user.hasRole("tech");
        }

        String storeLevelRoleNames = ConfigUtils.getStringScalar(Property.ALLOWED_STORE_ROLES_FOR_SUPC_PACKAGING_MAPPING);

        List<String> storeLevelRoles = StringUtils.split(storeLevelRoleNames);

        Set<String> storeCodeSet = packmanService.getAccessibleStoresByUserAndUserRole(email, storeLevelRoles, superUser);
        return storeCodeSet;
    }

}
