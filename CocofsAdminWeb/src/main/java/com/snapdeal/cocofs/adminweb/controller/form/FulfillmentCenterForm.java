/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.adminweb.controller.form;

import java.util.Date;

/**
 * @author indrajit
 */
public class FulfillmentCenterForm {

    private Integer id;
    private String  name;
    private String  addressLine1;
    private String  addressLine2;
    private String  city;
    private String  state;
    private String  pincode;
    private String  mobile;
    private String  landLine;
    private String  email;
    private Date    created;

    private String  centername;
    private String  centercode;
    private String  fulfillmentType;
    private boolean enable;
    private boolean sdInstant;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getLandLine() {
        return landLine;
    }

    public void setLandLine(String landLine) {
        this.landLine = landLine;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getCentername() {
        return centername;
    }

    public void setCentername(String centerName) {
        this.centername = centerName;
    }

    public String getCentercode() {
        return centercode;
    }

    public void setCentercode(String centerCode) {
        this.centercode = centerCode;
    }

    public String getFulfillmentType() {
        return fulfillmentType;
    }

    public void setFulfillmentType(String fulfillmentType) {
        this.fulfillmentType = fulfillmentType;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public boolean isSdInstant() {
        return sdInstant;
    }

    public void setSdInstant(boolean sdInstant) {
        this.sdInstant = sdInstant;
    }

}
