package com.snapdeal.cocofs.adminweb.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.snapdeal.base.fileutils.SDFileUploadUtils;
import com.snapdeal.cocofs.adminweb.dto.SellerCodeDTO;
import com.snapdeal.cocofs.adminweb.dto.SupcUploadDTO;
import com.snapdeal.cocofs.commonweb.utils.ExportToExcelUtils;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.services.IProductAttributeLookupService;
import com.snapdeal.cocofs.services.ISupcPackagingTypeMappingLookupService;
import com.snapdeal.cocofs.services.ITaxRateLookupService;
import com.snapdeal.cocofs.services.common.dto.ProductAttributeDownloadData;
import com.snapdeal.cocofs.services.common.dto.SupcPackagingTypeMappingDownloadData;
import com.snapdeal.cocofs.services.common.dto.TaxRateDownloadData;
import com.snapdeal.cocofs.services.fm.IFMLookupService;
import com.snapdeal.cocofs.services.fm.dto.SellerDto;
import com.snapdeal.cocofs.services.fm.dto.SellerSupcFMMappingData;
import com.snapdeal.cocofs.utils.ExcelReader;

/**
 * @author nikhil
 */

@Controller
@RequestMapping(LookupsController.URL)
public class LookupsController {

    public static final String     URL = "/admin/lookups/";

    private static final Logger    LOG = LoggerFactory.getLogger(LookupsController.class);

    @Autowired
    ITaxRateLookupService          taxRateLookupService;

    @Autowired
    IProductAttributeLookupService productAttributeLookupService;

    @Autowired
    IFMLookupService               fmLookupService;
    
    @Autowired
    ISupcPackagingTypeMappingLookupService supcPackagingTypeMappingLookupService;
    
    @RequestMapping("")
    public String lookupsPage(ModelMap map) {
        return "admin/lookups/paLookups";
    }

    @RequestMapping("pa")
    public String lookupsPage2(ModelMap map) {
        return "admin/lookups/paLookups";
    }

    @RequestMapping("fm")
    public String lookupsFmPage(ModelMap map) {
        return "admin/lookups/fmLookups";
    }
    
    @RequestMapping("spt")
    public String lookupsSPTPage(ModelMap map) {
        return "admin/lookups/sptLookups";
    }

    @RequestMapping("tx")
    public String lookupsTxPage(ModelMap map) {
        return "admin/lookups/txLookups";
    }

    @RequestMapping(method = { RequestMethod.POST }, value = "processAttributeUpload")
    public String processPALookup(ModelMap map, @RequestParam(required = true, value = "inputfile") MultipartFile inputFile, HttpServletResponse response, ModelMap modelMap) {
        String uploadFilePath = ConfigUtils.getStringScalar(Property.TEMP_DIRECTORY) + "/" + inputFile.getOriginalFilename();

        try {
            SDFileUploadUtils.upload(inputFile.getInputStream(), inputFile.getContentType(), uploadFilePath);

            List<SupcUploadDTO> supcList = ExcelReader.readFile(uploadFilePath, SupcUploadDTO.class, false, ConfigUtils.getIntegerScalar(Property.MAX_SIZE_ALLOWED_LOOKUP), true,
                    false);
            if (supcList != null && supcList.size() > 0) {
                List<ProductAttributeDownloadData> dtos = new ArrayList<ProductAttributeDownloadData>();

                for (SupcUploadDTO dto : supcList) {
                    try {
                        ProductAttributeDownloadData data = productAttributeLookupService.getProductAttributeDownloadData(dto.getSupc());
                        if (data != null) {
                            LOG.info("Got the downloaded data for supc {}", dto.getSupc());
                            dtos.add(data);
                        }
                    } catch (Exception e) {
                        LOG.error("Error found in reading information for supc {}, exception {}", dto.getSupc(), e);
                        LOG.error("Response for supc {} would be empty", dto.getSupc());
                        ProductAttributeDownloadData o = new ProductAttributeDownloadData();
                        o.setSupc(dto.getSupc());
                        dtos.add(o);
                    }
                }

                if (dtos.size() > 0) {
                    ExportToExcelUtils.exportToExcel(response, ProductAttributeDownloadData.class, inputFile.getOriginalFilename() + "_output", dtos);
                } else {
                    modelMap.addAttribute("errorMessage", "Can't find data for any record uploaded");
                }
            } else {
                modelMap.addAttribute("errorMessage", "No content found in the file uploaded");
            }

        } catch (Exception e) {
            LOG.error("Error in reading file {}", inputFile, e);
            modelMap.addAttribute("errorMessage", "Error in reading file, please check the content and size");
        }

        return "admin/lookups/paLookups";
    }

    @RequestMapping(method = { RequestMethod.POST }, value = "processSellerUploadForTaxRate")
    public String processTXLookup(ModelMap map, @RequestParam(required = true, value = "inputfile") MultipartFile inputFile, HttpServletResponse response, ModelMap modelMap) {
        String uploadFilePath = ConfigUtils.getStringScalar(Property.TEMP_DIRECTORY) + "/" + inputFile.getOriginalFilename();

        try {
            SDFileUploadUtils.upload(inputFile.getInputStream(), inputFile.getContentType(), uploadFilePath);

            List<SellerCodeDTO> sellerList = ExcelReader.readFile(uploadFilePath, SellerCodeDTO.class, false, ConfigUtils.getIntegerScalar(Property.MAX_SIZE_ALLOWED_LOOKUP), true,
                    false);
            if (sellerList != null && sellerList.size() > 0) {
                List<TaxRateDownloadData> dtos = new ArrayList<TaxRateDownloadData>();
                List<TaxRateDownloadData> resultChunkList = null;

                for (SellerCodeDTO dto : sellerList) {
                    try {
                        resultChunkList = taxRateLookupService.getTaxRateForSellerForAllSupcs(dto.getSellerCode());
                        if (resultChunkList != null) {
                            LOG.info("Got the downloaded data for seller code {}", dto.getSellerCode());
                            dtos.addAll(resultChunkList);

                        }
                    } catch (Exception e) {
                        LOG.error("Error found in reading information for seller code {}, exception {}", dto.getSellerCode(), e);
                        LOG.error("Response for seller code {} would be empty", dto.getSellerCode());
                        TaxRateDownloadData o = new TaxRateDownloadData();
                        o.setSellerCode(dto.getSellerCode());
                        dtos.add(o);
                    }
                }

                if (dtos.size() > 0) {
                    ExportToExcelUtils.exportToExcel(response, TaxRateDownloadData.class, inputFile.getOriginalFilename() + "_output", dtos);
                } else {
                    modelMap.addAttribute("errorMessage", "Can't find data for any record uploaded");
                }
            } else {
                modelMap.addAttribute("errorMessage", "No content found in the file uploaded");
            }

        } catch (Exception e) {
            LOG.error("Error in reading file {}", inputFile, e);
            modelMap.addAttribute("errorMessage", "Error in reading file, please check the content and size");
        }

        return "admin/lookups/txLookups";
    }

    private void addDataInListFromSet(List<TaxRateDownloadData> list, Set<TaxRateDownloadData> set) {
        if (null != list || null != set) {
            for (TaxRateDownloadData data : set) {
                list.add(data);
            }
        }
    }

    @RequestMapping(method = { RequestMethod.GET }, value = "downloadSupcTemplate")
    public void downloadProductAttributeTemplate(HttpServletRequest req, HttpServletResponse resp) {
        try {
            ExportToExcelUtils.downloadTemplate(resp, SupcUploadDTO.class, "SupcUploadTemplate");
        } catch (IOException e) {
            LOG.info("Errr while exporting template for attributes ", e);
        } catch (ClassNotFoundException e) {
            LOG.info("Errr while exporting template for attributes ", e);
        }
    }

    @RequestMapping(method = { RequestMethod.GET }, value = "downloadSellerCodeTemplate")
    public void downloadTaxrateAttributeTemplate(HttpServletRequest req, HttpServletResponse resp) {
        try {
            ExportToExcelUtils.downloadTemplate(resp, SellerCodeDTO.class, "SellerCodeUploadTemplate");
        } catch (IOException e) {
            LOG.info("Errr while exporting template for attributes ", e);
        } catch (ClassNotFoundException e) {
            LOG.info("Errr while exporting template for attributes ", e);
        }
    }

    @RequestMapping(method = { RequestMethod.GET }, value = "downloadSellerCodeData")
    public void downloadTaxrateAttributeData(HttpServletRequest req, HttpServletResponse resp) {
        try {
            List<SellerDto> sellersDTO = taxRateLookupService.getSellersCodeswithTaxRate();
            if (sellersDTO.size() <= ConfigUtils.getIntegerScalar(Property.EXCEL_SHEET_MAX_VALUE)) {
                ExportToExcelUtils.exportToExcel(resp, SellerDto.class, "Sellers", sellersDTO);
            } else {
                ExportToExcelUtils.exportToCsv(resp, SellerDto.class, "Sellers", sellersDTO, true, false);
            }
        } catch (IOException e) {
            LOG.info("Errr while exporting template for attributes ", e);
        } catch (ClassNotFoundException e) {
            LOG.info("Errr while exporting template for attributes ", e);
        }
    }

    @RequestMapping("supcExceptions")
    public void downloadSellersWithSupcException(HttpServletResponse resp) {
        try {
            List<SellerDto> sellersDTO = fmLookupService.getSellersWithExceptionAtSupcLevel();
            ExportToExcelUtils.exportToExcel(resp, SellerDto.class, "Sellers", sellersDTO);
        } catch (IOException e) {
            LOG.info("Errr while exporting template for attributes ", e);
        } catch (ClassNotFoundException e) {
            LOG.info("Errr while exporting template for attributes ", e);
        }
    }

    @RequestMapping(value = "sellerSupcFMMappingData")
    public void downloadSellerSupcFMMappingData(@RequestParam("sellerCode") String sellerCode, ModelMap modelMap, HttpServletResponse resp) {
        try {
            List<SellerSupcFMMappingData> sellerSupcFMMappingDTO = fmLookupService.getSellerSupcFMMappingData(sellerCode);
            ExportToExcelUtils.exportToExcel(resp, SellerSupcFMMappingData.class, "SellerSupcFMMapping", sellerSupcFMMappingDTO);
        } catch (IOException e) {
            LOG.error("Error while exporting template for attributes ", e);
        } catch (ClassNotFoundException e) {
            LOG.error("Error while exporting template for attributes ", e);
        }
    }
    
    
  /*  @RequestMapping(value = "processSupcPackagingTypeMappingUpload")
    public void processSupcPackagingTypeLookup(@RequestParam("supc") String supc, ModelMap modelMap, HttpServletResponse resp) {
        try {
            List<SupcPackagingTypeMappingDownloadData> supcPackagingTypeMappingDTO = supcPackagingTypeMappingLookupService.getSupcPackagingTypeDownloadData(supc);
           ExportToExcelUtils.exportToExcel(resp, SupcPackagingTypeMappingDownloadData.class, "SupcPackagingTypeMapping", supcPackagingTypeMappingDTO);
        } catch (IOException e) {
            LOG.error("Error while exporting template for attributes ", e);
        } catch (ClassNotFoundException e) {
            LOG.error("Error while exporting template for attributes ", e);
        } 
    }*/
    
    @RequestMapping(method = { RequestMethod.POST }, value = "processSupcPackagingTypeMappingUpload")
    public String processSupcPackagingTypeLookup(ModelMap map, @RequestParam(required = true, value = "inputfile") MultipartFile inputFile, HttpServletResponse response, ModelMap modelMap) {
        String uploadFilePath = ConfigUtils.getStringScalar(Property.TEMP_DIRECTORY) + "/" + inputFile.getOriginalFilename();

        try {
            SDFileUploadUtils.upload(inputFile.getInputStream(), inputFile.getContentType(), uploadFilePath);

            List<SupcUploadDTO> supcList = ExcelReader.readFile(uploadFilePath, SupcUploadDTO.class, false, ConfigUtils.getIntegerScalar(Property.MAX_SIZE_ALLOWED_LOOKUP), true,
                    false);
            if (supcList != null && supcList.size() > 0) {
                List<SupcPackagingTypeMappingDownloadData> dtos = new ArrayList<SupcPackagingTypeMappingDownloadData>();

                for (SupcUploadDTO dto : supcList) {
                    try {
                        List<SupcPackagingTypeMappingDownloadData> data = supcPackagingTypeMappingLookupService.getSupcPackagingTypeDownloadData(dto.getSupc());
                        if (data != null) {
                            LOG.info("Got the downloaded data for supc {}", dto.getSupc());
                            for(SupcPackagingTypeMappingDownloadData supcPackagingTypeMappingDownloadData : data)
                            {
                            dtos.add(supcPackagingTypeMappingDownloadData);
                            }
                        }
                    } catch (Exception e) {
                        LOG.error("Error found in reading information for supc {}, exception {}", dto.getSupc(), e);
                        LOG.error("Response for supc {} would be empty", dto.getSupc());
                        //SupcPackagingTypeMappingDownloadData o = new SupcPackagingTypeMappingDownloadData();
                        SupcPackagingTypeMappingDownloadData o = new SupcPackagingTypeMappingDownloadData(dto.getSupc(), null, null, null, null, null,null); 
                        dtos.add(o);
                    }
                }

                if (dtos.size() > 0) {
                    ExportToExcelUtils.exportToExcel(response, SupcPackagingTypeMappingDownloadData.class, inputFile.getOriginalFilename() + "_output", dtos);
                } else {
                    modelMap.addAttribute("errorMessage", "Can't find data for any record uploaded");
                }
            } else {
                modelMap.addAttribute("errorMessage", "No content found in the file uploaded");
            }

        } catch (Exception e) {
            LOG.error("Error in reading file {}", inputFile, e);
            modelMap.addAttribute("errorMessage", "Error in reading file, please check the content and size");
        }

        return "admin/lookups/sptLookups";
    }

}
