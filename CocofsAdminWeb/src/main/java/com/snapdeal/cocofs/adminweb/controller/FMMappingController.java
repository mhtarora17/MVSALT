/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 21-Oct-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.adminweb.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.snapdeal.cocofs.adminweb.dto.FCDownloadDTO;
import com.snapdeal.cocofs.common.JobValidationException;
import com.snapdeal.cocofs.commonweb.utils.ExportToExcelUtils;
import com.snapdeal.cocofs.commonweb.utils.WebContextUtils;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.core.aerospike.vo.FCDetailVO;
import com.snapdeal.cocofs.entity.FulfillmentCentre;
import com.snapdeal.cocofs.entity.JobDetail;
import com.snapdeal.cocofs.enums.JobActionCode;
import com.snapdeal.cocofs.services.IJobSchedulerService;
import com.snapdeal.cocofs.services.fm.IFulfillmentCenterService;

@Controller
@RequestMapping("/admin/sellerSupcFMMapping")
public class FMMappingController {

    private static final Logger  LOG = LoggerFactory.getLogger(FMMappingController.class);

    @Autowired
    private IJobSchedulerService jobSchedulerService;

    @Autowired
    IFulfillmentCenterService    fulfillmentCenterService;

    @RequestMapping("")
    public String uploadPage(ModelMap map) {
        map.addAttribute("fcEnabled", ConfigUtils.getBooleanScalar(Property.SELLER_SUPC_FM_FC_UPDATE_PUSH_ENABLED));
        return "admin/fmMapping/sellerSupc";
    }

    @RequestMapping("/upload")
    public String upload(ModelMap map, @RequestParam(value = "inputfile") MultipartFile inputFile, @RequestParam(value = "enabled", required = true) int enabled) {

        try {
            String userEmail = WebContextUtils.getCurrentUserEmail();
            JobDetail jd = jobSchedulerService.saveJob(inputFile, JobActionCode.SELLER_SUPC_FM_MAPPING.getCode(), userEmail, "" + enabled);

            String actionCodeMSG = "";
            if (jd.getAction() != null) {
                actionCodeMSG = ", Action code:" + jd.getAction().getCode();
            }
            map.addAttribute("message", "Seller Supc FM Mapping upload done. <br>Job code:" + jd.getFileCode() + actionCodeMSG + ", saved at  " + jd.getFilePath() + File.separator
                    + jd.getFileName());
        } catch (IOException e) {
            LOG.info("Seller Supc FM Mapping Upload Could not be done. ", e);
            map.addAttribute("message", "Seller Supc FM Mapping upload could not be done.");
        } catch (JobValidationException e) {
            LOG.error("Attribute Upload Could not be done ", e);
            map.addAttribute("message", "Seller Supc FM Mapping upload could not be done. " + e.getMessage());
            map.addAttribute("validationResponse", e.getResponse());
        } catch (Exception e) {
            LOG.info("Seller Supc FM Mapping upload could not be done. ", e);
            map.addAttribute("message", "Seller Supc FM Mapping upload could not be done, something bad happened.");
        }
        return "admin/fmMapping/sellerSupc";
    }

    @RequestMapping("/downloadTemplate")
    public void downloadSellerSupcFmMappingTemplate(HttpServletRequest req, HttpServletResponse resp) {
        try {
            //temp fix to allow only old format template download.
            Class<?> clazz = TempSellerFMMappingUpdateDto.class;
            if (isSellerSupcFMFCUpdatePushEnabled()) {
                clazz = TempSellerFMFCMappingUpdateDto.class;
            }
            ExportToExcelUtils.downloadTemplate(resp, clazz, "SellerSupcFmMapping");
        } catch (IOException e) {
            LOG.error("Exception", e);
        } catch (ClassNotFoundException e) {
            LOG.error("Exception", e);
        }
    }

    private boolean isSellerSupcFMFCUpdatePushEnabled() {
        Property p = Property.SELLER_SUPC_FM_FC_UPDATE_PUSH_ENABLED;
        if (!ConfigUtils.getBooleanScalar(p)) {
            LOG.warn(p + " for push of seller-supc-fm-fcList is disabled ... ");
            return false;
        }
        return true;
    }

    @RequestMapping("/downloadAllFCList")
    public void downloadAllFCList(HttpServletRequest req, HttpServletResponse resp) {
        try {
            List<FulfillmentCentre> allFCs = fulfillmentCenterService.getAllFCs();
            List<FCDownloadDTO> fcDTOList = new ArrayList<FCDownloadDTO>();
            for (FulfillmentCentre fc : allFCs) {
                FCDownloadDTO dto = new FCDownloadDTO(fc.getCode(), fc.getName(), fc.getType());
                fcDTOList.add(dto);
            }
            ExportToExcelUtils.exportToExcel(resp, FCDownloadDTO.class, "FCList", fcDTOList, true, false);
        } catch (IOException e) {
            LOG.error("Exception", e);
        } catch (ClassNotFoundException e) {
            LOG.error("Exception", e);
        }
    }

    //TODO - remove in next release
    static class TempSellerFMMappingUpdateDto {
        private String sellerCode;
        private String supc;
        private String fulfilmentModel;
    }

    // NOTE - do not remove this class, This class has to be in sync with if it needs to be removed, pls modify the table - upload_sheet_field_domain for relevant changes
    // ensure that this class is in sync with SellerSupcFMMappingUploadDto
    static class TempSellerFMFCMappingUpdateDto {
        private String sellerCode;
        private String supc;
        private String fulfilmentModel;
        private String fcCenters;
    }
}
