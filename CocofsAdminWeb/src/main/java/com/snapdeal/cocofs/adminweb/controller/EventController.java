/*
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 07-July-2014
 *  
 *  @Desc	Added to control Event Detail and Search Event pages from Admin
 */
package com.snapdeal.cocofs.adminweb.controller;

import java.util.List;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.snapdeal.base.model.common.ServiceResponse;
import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.services.events.IEventHandlerService;
import com.snapdeal.cocofs.services.events.dto.EventDetailDTO;
import com.snapdeal.cocofs.services.events.dto.SnapDealJsonData;
import com.snapdeal.cocofs.services.events.impl.EventStatus;

@RequestMapping("/admin/events/")
@Controller("EventController")
public class EventController {

    @Autowired
    IEventHandlerService        eventHandlerService;

    private static final Logger LOG = LoggerFactory.getLogger(EventController.class);

    @RequestMapping("eventDetail")
    public String getUpdateTaskDetailPage(ModelMap modelMap) {
        modelMap.addAttribute("eventStatus", ConfigUtils.getStringList(Property.EVENT_INSTANCE_STATUS));
        return "/admin/admintasks/eventDetail";
    }

    @RequestMapping("searchEvent")
    public String getUpdateTaskPage(ModelMap modelMap) {
        modelMap.addAttribute("searchCriteria", ConfigUtils.getStringList(Property.EVENT_INSTANCE_SEARCH_CRITERIA));
        return "/admin/admintasks/searchEvent";
    }

    /**
     * Will be called from Event Details page from Shipping Admin
     * 
     * @param modelMap
     * @param eventStatus
     * @param page
     * @param rows
     * @return
     */
    @RequestMapping("/getEvents")
    public @ResponseBody SnapDealJsonData getEvents(ModelMap modelMap, @RequestParam(value = "eventStatus") String eventStatus,
            @RequestParam(value = "page", required = false, defaultValue = "1") int page, @RequestParam(value = "rows", required = false, defaultValue = "40") int rows) {
        LOG.info("Request receieved to get events for event Status {}", eventStatus);
        SnapDealJsonData resp = new SnapDealJsonData();
        List<EventDetailDTO> dtoList = null;
        if (StringUtils.isNotEmpty(eventStatus) && EventStatus.PENDING.getCode().equals(eventStatus)) {
            LOG.info("Getting PENDING Events Only");
            dtoList = eventHandlerService.getEvents(false, true);
        } else {
            LOG.info("Getting ALL Events");
            dtoList = eventHandlerService.getEvents(true, true);
        }
        LOG.info("Got following events {}", dtoList);
        int records = dtoList.size();
        resp.setRecords(String.valueOf(records));
        Double pages = (double) (records == rows ? records / rows : records / rows + 1);
        resp.setTotal(pages);
        resp.setPage(String.valueOf(page));
        resp.setRows(dtoList);
        return resp;
    }

    /**
     * Will be called from Event Details and Search Event pages from ShipTrack Admin
     * 
     * @param rowId
     * @return
     */
    @RequestMapping("/replayEvent")
    @Produces(MediaType.APPLICATION_JSON)
    @ResponseBody
    public String replayEvent(@RequestParam(value = "rowId") String rowId) {
        LOG.info("Request receieved to replay Event with id {}", rowId);
        ServiceResponse resp = new ServiceResponse();
        try {
            eventHandlerService.callEventForcefully(Integer.parseInt(rowId));
            resp.setSuccessful(true);
            return new Gson().toJson(resp);
        } catch (Throwable t) {
            LOG.error("Error in replaying the event with id {}", rowId);
            LOG.error("", t);
            resp.setSuccessful(false);
            return new Gson().toJson(resp);
        }
    }

}
