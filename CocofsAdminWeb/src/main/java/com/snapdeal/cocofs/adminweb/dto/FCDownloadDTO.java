/**
 * Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.adminweb.dto;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *  
 *  @version     1.0, 08-Apr-2015
 *  @author ankur
 */

@XmlRootElement
public class FCDownloadDTO {

    private String code;
    private String name;
    private String type;

    public FCDownloadDTO(){
        
    }
    
    public FCDownloadDTO(String code, String name, String type) {
        super();
        this.code = code;
        this.name = name;
        this.type = type;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "FCDownloadDTO [code=" + code + ", name=" + name + ", type=" + type + "]";
    }

}
