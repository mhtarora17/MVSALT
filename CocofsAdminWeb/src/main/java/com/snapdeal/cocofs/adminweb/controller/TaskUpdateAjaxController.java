/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 02-Sep-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.adminweb.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.gson.Gson;
import com.snapdeal.cocofs.commonweb.utils.WebContextUtils;
import com.snapdeal.cocofs.enums.TaskDetailAction;
import com.snapdeal.cocofs.services.task.dto.TaskDetailDTO;
import com.snapdeal.cocofs.services.task.dto.TaskDetailUpdateResponseDTO;
import com.snapdeal.cocofs.services.task.update.ITaskUpdateService;
import com.snapdeal.task.exception.TaskException;

@Path("/json/admin/task/")
@Controller("TaskUpdateAjaxController")
public class TaskUpdateAjaxController {

    private static final Logger LOG         = LoggerFactory.getLogger(TaskUpdateAjaxController.class);

    @Autowired
    ITaskUpdateService          taskUpdateService;

    private final Gson          GSON_OBJECT = new Gson();

    @Path("updateTaskDetail")
    @Produces(MediaType.APPLICATION_JSON)
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public String saveTaskDetail(@RequestParam(value = "taskDetailDTO") TaskDetailDTO taskDetailDTO) {
        TaskDetailUpdateResponseDTO resp = null;
        try {
            LOG.info("Request receieved to update task info {}", taskDetailDTO);
            String currentUser = WebContextUtils.getCurrentUserEmail();
            resp = taskUpdateService.validateRequest(taskDetailDTO);
            if (resp.isSuccessful()) {
                taskUpdateService.updateTaskDetailAndParams(taskDetailDTO, currentUser != null ? currentUser : "anonymous");
            }
        } catch (TaskException e) {
            LOG.error("Exception while updating task detail for {} ", e, taskDetailDTO);
        }
        return GSON_OBJECT.toJson(resp);
    }

    @Path("resumeAllTask")
    @Produces(MediaType.APPLICATION_JSON)
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void startAllTask() {
        String currentUser = WebContextUtils.getCurrentUserEmail();
        LOG.info("Request received to start all tasks by {}", currentUser);
        try {
            taskUpdateService.createTaskDetailUpdateForAllHosts(TaskDetailAction.RESUME_ALL.getCode(), null, currentUser != null ? currentUser : "anonymous");
        } catch (Exception e) {
            LOG.error("Exception while starting all tasks", e);
        }
    }

    @Path("pauseAllTask")
    @Produces(MediaType.APPLICATION_JSON)
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void pauseAllTask() {
        String currentUser = WebContextUtils.getCurrentUserEmail();
        LOG.info("Request received to pause all tasks By {}", currentUser);
        try {
            taskUpdateService.createTaskDetailUpdateForAllHosts(TaskDetailAction.PAUSE_ALL.getCode(), null, currentUser != null ? currentUser : "anonymous");
        } catch (Exception e) {
            LOG.error("Exception while pausing all tasks", e);
        }

    }

    @Path("stopTask/{taskName}")
    @Produces(MediaType.APPLICATION_JSON)
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void stopTask(@PathParam(value = "taskName") String taskName) {
        String currentUser = WebContextUtils.getCurrentUserEmail();
        LOG.info("Request received to stop task {} by {}", taskName, currentUser);
        try {
            taskUpdateService.createTaskDetailUpdate(TaskDetailAction.STOP_TASK.getCode(), taskName, currentUser != null ? currentUser : "anonymous");
        } catch (Exception e) {
            LOG.error("Exception while stopping tasks {}", e, taskName);
        }
    }

    @Path("startTask/{taskName}")
    @Produces(MediaType.APPLICATION_JSON)
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void startTask(@PathParam(value = "taskName") String taskName) {
        String currentUser = WebContextUtils.getCurrentUserEmail();
        LOG.info("Request received to start task {} By {} ", taskName, currentUser);
        try {
            taskUpdateService.createTaskDetailUpdate(TaskDetailAction.START_TASK.getCode(), taskName, currentUser != null ? currentUser : "anonymous");
        } catch (Exception e) {
            LOG.error("Exception while starting tasks {}", e, taskName);
        }
    }

}
