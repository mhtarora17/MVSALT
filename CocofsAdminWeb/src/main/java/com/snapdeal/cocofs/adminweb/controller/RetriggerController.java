/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.adminweb.controller;

import java.util.Date;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.annotations.NotNull;
import com.snapdeal.base.model.common.ServiceRequest;
import com.snapdeal.base.model.common.ServiceResponse;
import com.snapdeal.base.transport.service.ITransportService.Protocol;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.services.IPendingPAUPlayService;

/**
 * @version 1.0, 8-Sep-2015
 * @author shiv
 */

@Controller
@RequestMapping(RetriggerController.URL)
public class RetriggerController {

    private static final Logger    LOG = LoggerFactory.getLogger(RetriggerController.class);

    public static final String     URL = "/retrigger/manual/";

    @Autowired
    private IPendingPAUPlayService pendingPAUPlayService;

    @RequestMapping(value = "playPendingPAUByDateRange", produces = "application/sd-service")
    @ResponseBody
    public ServiceResponse playPendingPAUByDateRange(@RequestBody final PlayPendingPAUByDateRange request) {
        ServiceResponse response = new ServiceResponse();
        try {

            LOG.info("playPendingPAUByDateRange started for request:{}", request);
            if (!org.apache.commons.lang.StringUtils.trimToEmpty(request.getKey()).equals(ConfigUtils.getStringScalar(Property.PENDING_PAU_REPLAY_SHARED_KEY))) {
                throw new Exception("not authorized with key:[" + request.getKey() + "]. Key must be valid.");
            }

            if (request.getStartDate() == null || request.getEndDate() == null) {
                throw new Exception("startDate and endDate must be valid.");
            }

            Runnable r = new Runnable() {
                @Override
                public void run() {

                    try {
                        LOG.info("playPendingPAUByDateRange started for sDate:{} to eDate:{}", request.getStartDate(), request.getEndDate());
                        // persist was successful
                        pendingPAUPlayService.promotePendingPAUs(request.getStartDate(), request.getEndDate());
                        LOG.info("playPendingPAUByDateRange finished successfully.");
                    } catch (Exception e) {
                        LOG.error("Exception while processing the request: {} . Exception {}", request, e);
                    }
                }
            };

            LOG.info("Processing asynchronously - playPendingPAUByDateRange for request {}", request);
            new Thread(r, Thread.currentThread().getName() + "--forked").start();

            LOG.info("playPendingPAUByDateRange submitted successfully.");
        } catch (Exception e) {
            LOG.error("Exception while playing pending PAU promotion.", e);
            response = new ServiceResponse(false, e.getMessage());
        }

        Protocol p = request.getResponseProtocol();
        if (request.getResponseProtocol() == null) {
            p = Protocol.PROTOCOL_JSON;
        }
        response.setProtocol(p);

        return response;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    static class PlayPendingPAUByDateRange extends ServiceRequest {

        /**
         * 
         */
        private static final long serialVersionUID = -4814048545277870152L;

        @Tag(21)
        @NotNull
        private Date              startDate;

        @Tag(22)
        @NotNull
        private Date              endDate;

        private String            key;

        public Date getStartDate() {
            return startDate;
        }

        public void setStartDate(Date startDate) {
            this.startDate = startDate;
        }

        public Date getEndDate() {
            return endDate;
        }

        public void setEndDate(Date endDate) {
            this.endDate = endDate;
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        @Override
        public String toString() {
            return "PlayPendingPAUByDateRange [startDate=" + startDate + ", endDate=" + endDate + ", key=" + key + ", getResponseProtocol()=" + getResponseProtocol()
                    + ", getRequestProtocol()=" + getRequestProtocol() + ", getUserTrackingId()=" + getUserTrackingId() + ", getContextSRO()=" + getContextSRO()
                    + ", getUniqueLoggingCode()=" + getUniqueLoggingCode() + "]";
        }

    }

}
