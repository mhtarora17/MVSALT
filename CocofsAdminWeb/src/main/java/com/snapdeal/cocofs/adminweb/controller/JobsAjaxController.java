package com.snapdeal.cocofs.adminweb.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.gson.Gson;
import com.snapdeal.cocofs.adminweb.dto.JobDTOResponse;
import com.snapdeal.cocofs.common.JobDetailDTO;
import com.snapdeal.cocofs.commonweb.utils.WebContextUtils;
import com.snapdeal.cocofs.entity.JobDetail;
import com.snapdeal.cocofs.services.IJobService;
import com.snapdeal.cocofs.adminweb.controller.JobsAjaxController;

@Path("/json/admin/jobs/")
@Controller("JobsAjaxController")
public class JobsAjaxController {
	private static final Logger LOG = LoggerFactory.getLogger(JobsAjaxController.class);

	@Autowired
	private IJobService jobService;

	private final Gson GSON_OBJECT = new Gson();

	@Path("updateJobDetail")
	@Produces(MediaType.APPLICATION_JSON)
	@POST
	// @Consumes(MediaType.APPLICATION_JSON)
	public String savejobDetail(
			@RequestParam(value = "jobDetailDTO") JobDetailDTO jobDetailDTO) {
		JobDTOResponse resp = new JobDTOResponse();
		try {
			LOG.info("Request received to update Priority info {}",jobDetailDTO);
			if (updatePriority(jobDetailDTO)) {
				LOG.info("Success: Successfully Updated Priority"+ jobDetailDTO);
			} else {
				LOG.error("Error: Error while Updated Priority" + jobDetailDTO);
			}

		}
		catch (NumberFormatException e) {
			LOG.error("Exception while updating Priority for " + jobDetailDTO,
					e);
			resp.setSuccessful(false);
			resp.setMessage("Failure :: Please check the Entered Priority");
			resp.setResponse("Failure :: Please check the Entered Priority");
		} 
		catch (Exception e) {
			LOG.error("Exception while updating Priority for " + jobDetailDTO, e);
			resp.setSuccessful(false);
			resp.setMessage("Failure :: Please check your configuration. Message:"+ e.getMessage());
			resp.setResponse("Failure :: Please check your configuration. Message:"+ e.getMessage());
		}

		return GSON_OBJECT.toJson(resp);
	}

	private Boolean updatePriority(JobDetailDTO jobDetailDTO) {

		JobDetail job = jobService.getJobDetailByCode(jobDetailDTO.getJobCode());
		job.setPriority(jobDetailDTO.getPriority());
		String username = WebContextUtils.getCurrentUserEmail();
		return jobService.updateJob(job, username);
	}

}
