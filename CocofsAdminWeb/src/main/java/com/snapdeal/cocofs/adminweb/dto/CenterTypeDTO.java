package com.snapdeal.cocofs.adminweb.dto;

import java.util.List;

public class CenterTypeDTO {

    private List<String> centers;

    public CenterTypeDTO(List<String> fms) {
        super();
        this.centers = fms;
    }

    public List<String> getFms() {
        return centers;
    }

    public void setFms(List<String> fms) {
        this.centers = fms;
    }

}
