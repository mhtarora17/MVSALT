/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 01-Jan-2016
 *  @author Suryansh
 */
package com.snapdeal.cocofs.adminweb.controller;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.snapdeal.base.cache.CacheManager;
import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.cocofs.cache.JobCache;
import com.snapdeal.cocofs.common.MyJobDetailDTO;
import com.snapdeal.cocofs.commonweb.utils.WebContextUtils;
import com.snapdeal.cocofs.commonweb.utils.dto.JqgridResponse;
import com.snapdeal.cocofs.entity.JobStatus;
import com.snapdeal.cocofs.services.IJobSchedulerService;
import com.snapdeal.cocofs.services.IJobService;
import com.snapdeal.cocofs.services.IJobsConvertorService;

@Controller
@RequestMapping(MyJobsController.URL)
public class MyJobsController {
    public static final String    URL = "/user/jobs/";

    @Autowired
    private IJobSchedulerService  jobSchedulerService;

    @Autowired
    private IJobService           jobService;
    @Autowired
    private IJobsConvertorService jobsConvertorService;

    private static final Logger   LOG = LoggerFactory.getLogger(MyJobsController.class);

    @RequestMapping("")
    public String defaultView() {
        return "redirect:myJobs";
    }

    private String getSortByFieldFromSortByColumn(String sortByCol) {
        if ("jobCode".equals(sortByCol)) {
            return "fileCode";
        }
        if ("fileName".equals(sortByCol)) {
            return "fileName";
        }
        if ("status".equals(sortByCol)) {
            return "status.code";
        }
        if ("action".equals(sortByCol)) {
            return "action.code";
        }
        if ("created".equals(sortByCol)) {
            return "created";
        }
        if ("updated".equals(sortByCol)) {
            return "updated";
        }
        if ("uploadedBy".equals(sortByCol)) {
            return "uploadedBy";
        }
        return "";
    }

    @RequestMapping("myJobs")
    public String showMyJobStatus(ModelMap model) {
        try {
            model.addAttribute("jobStatusList", CacheManager.getInstance().getCache(JobCache.class).getAllJobStatus());
        }

        catch (Exception e) {
            LOG.error(null != e.getMessage() ? e.getMessage() : "Error cannot be determined");
        }

        return "user/jobs/myJobStatus";
    }

    @RequestMapping("getMyJobInfoByStatus")
    public @ResponseBody JqgridResponse<MyJobDetailDTO> getMyTaskInfo(ModelMap modelMap, @RequestParam(value = "status", required = true) String status,
            @RequestParam(value = "page", required = false, defaultValue = "1") int page, @RequestParam(value = "rows", required = false, defaultValue = "40") int rows,
            @RequestParam(value = "sidx", required = false, defaultValue = "created") String sortByColumn,
            @RequestParam(value = "sord", required = false, defaultValue = "desc") String sortOrder,
            @RequestParam(value = "start", required = false, defaultValue = "") String sDate, @RequestParam(value = "end", required = false, defaultValue = "") String eDate,
            @RequestParam(value = "user", required = false, defaultValue = "") String user) {
        JqgridResponse<MyJobDetailDTO> resp = new JqgridResponse<MyJobDetailDTO>();
        List<MyJobDetailDTO> dtoList = null;

        int startRow = (page - 1) * rows;
        int rowCount = rows;
        JobStatus jobStatus = CacheManager.getInstance().getCache(JobCache.class).getJobStatusByCode(status);
        Date startDate;
        Date endDate;
        if (StringUtils.isEmpty(sDate)) {
            // Weird way to make sure that we get todays date with time stamp
            // portion
            // at 00:00:00
            startDate = DateUtils.stringToDate(DateUtils.dateToString(new Date(), "yyyy-MM-dd"), "yyyy-MM-dd");
        } else {
            startDate = DateUtils.stringToDate(sDate, "yyyy-MM-dd");
        }
        if (StringUtils.isEmpty(eDate)) {
            endDate = startDate;
        } else {
            endDate = DateUtils.stringToDate(eDate, "yyyy-MM-dd");
        }

        Calendar cal = new GregorianCalendar();
        cal.setTime(endDate);
        cal.add(Calendar.DATE, 1);
        endDate = cal.getTime();
        int totalRows = jobService.getMyJobDetailsRowCountPagination(jobStatus, startDate, endDate, user);
        String sortByField = getSortByFieldFromSortByColumn(sortByColumn);
        dtoList = jobsConvertorService.getMyJobDetailDTOList(jobService.getMyJobDetailsWithPagination(jobStatus, startRow, rowCount, startDate, endDate, sortByField, sortOrder,
                user));
        resp.setRecords(String.valueOf(totalRows));
        Integer pages = ((totalRows % rows) == 0) ? (totalRows / rows) : (totalRows / rows) + 1;
        resp.setTotal(pages);
        resp.setPage(String.valueOf(page));
        resp.setRows(dtoList);
        return resp;
    }

    @RequestMapping("updateStatus")
    public @ResponseBody String updateStatus(ModelMap modelMap, @RequestParam(value = "id", required = true) String id,
            @RequestParam(value = "button", required = true) String button) throws ParseException {
        String update=jobService.updateStatus(id, button,WebContextUtils.getCurrentUserEmail());
        return update;
    }
}
