/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 10-Nov-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.adminweb.controller.form;

import java.util.ArrayList;
import java.util.List;

public class UserUpdateForm {

    private Integer      userId;
    private String       password;
    private String       userEmail;
    private String       displayName;
    private String       firstName;
    private String       middleName;
    private String       lastName;
    private boolean      enabled;
    private List<String> userRoles = new ArrayList<String>();

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public List<String> getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(List<String> userRoles) {
        this.userRoles = userRoles;
    }

    @Override
    public String toString() {
        return "UserCreationForm [userId=" + userId + ", password=" + password + ", userEmail=" + userEmail + ", displayName=" + displayName + ", firstName=" + firstName
                + ", middleName=" + middleName + ", lastName=" + lastName + ", enabled=" + enabled + ", userRoles=" + userRoles + "]";
    }

}
