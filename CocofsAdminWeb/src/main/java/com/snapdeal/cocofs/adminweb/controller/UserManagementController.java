/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 19-Mar-2013
 *  @author prateek
 */
package com.snapdeal.cocofs.adminweb.controller;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.cocofs.adminweb.controller.form.UserUpdateForm;
import com.snapdeal.cocofs.commonweb.utils.WebContextUtils;
import com.snapdeal.cocofs.db.users.IUserService;
import com.snapdeal.cocofs.entity.User;
import com.snapdeal.cocofs.services.IEmailService;
import com.snapdeal.cocofs.services.usermanagement.IUserManagementService;
import com.snapdeal.cocofs.services.usermanagement.impl.UserDTO;
import com.snapdeal.cocofs.utils.EncryptionUtils;

@Controller
@RequestMapping(UserManagementController.URL)
public class UserManagementController {

    @Autowired
    private IUserService           userService;

    @Autowired
    private IUserManagementService userManagementService;

    @Autowired
    IEmailService                  emailService;

    public static final String     URL             = "/admin/usermanagement";

    private static final int       PASSWORD_LENGTH = 8;

    private static final Pattern   pattern         = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");

    private static final Logger    LOG             = LoggerFactory.getLogger(UserManagementController.class);

    @RequestMapping("")
    public String defaultView(ModelMap modelMap) {
        modelMap.put("userCreationForm", new UserUpdateForm());
        return "/admin/user/updateUser";
    }

    @RequestMapping("/updateUser")
    public String updateUser(UserUpdateForm form, BindingResult result, ModelMap modelMap) {
        try {
            LOG.info("form {} by user {}", form, WebContextUtils.getCurrentUserEmail());
            if (StringUtils.isNotEmpty(form.getUserEmail()) && validateEmail(form.getUserEmail())) {
                String password = RandomStringUtils.randomAlphanumeric(PASSWORD_LENGTH);
                User u = getUserFromForm(form, password);
                userManagementService.updateUser(u, password);
                LOG.info("User {} is saved successfully", u);
                modelMap.put("message", "User Updated Successfully");
            }
        } catch (Exception e) {
            LOG.error("Exception while creating user with email " + form.getUserEmail(), e);
            modelMap.put("message", "Something bad happened while updating");
        }
        modelMap.put("userCreationForm", new UserUpdateForm());
        return "/admin/user/updateUser";
    }

    private User getUserFromForm(UserUpdateForm form, String password) {
        if (form != null) {
            User user = null;
            if (form.getUserId() != null) {
                user = userService.getUserByEmailForEdit(form.getUserEmail());
            } else {
                user = new User();
                user.setCreated(DateUtils.getCurrentTime());
                user.setPassword(EncryptionUtils.getMD5EncodedPassword(password));
            }
            user.setDisplayName(form.getDisplayName());
            user.setEmail(form.getUserEmail());
            user.setEmailVerified(true);
            user.setEnabled(form.isEnabled());
            user.setFirstName(form.getFirstName());
            user.setLastName(form.getLastName());
            user.setMiddleName(form.getMiddleName());
            userManagementService.enrichUserWithUserRole(user, form.getUserRoles());
            return user;
        }
        return null;
    }

    @RequestMapping("/searchUser")
    public @ResponseBody
    UserDTO getUserByEmail(@RequestParam("email") String email) {
        UserDTO userDTO = null;
        if (StringUtils.isNotEmpty(email) && validateEmail(email)) {
            userDTO = userManagementService.getUserInfo(email);
        } else {
            LOG.info("invalid email ");
            userDTO = new UserDTO();
        }
        return userDTO;
    }

    private boolean validateEmail(String email) {
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

}
