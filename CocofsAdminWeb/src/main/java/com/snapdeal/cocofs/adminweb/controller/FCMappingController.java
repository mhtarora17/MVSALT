/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.adminweb.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.naming.OperationNotSupportedException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.snapdeal.base.utils.CollectionUtils;
import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.cocofs.commonweb.response.SystemResponse;
import com.snapdeal.cocofs.commonweb.utils.WebContextUtils;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.core.aerospike.vo.FCDetailVO;
import com.snapdeal.cocofs.core.aerospike.vo.SellerFMMappingVO;
import com.snapdeal.cocofs.dataaccess.fc.IFCAerospikeDataReadService;
import com.snapdeal.cocofs.dataaccess.fm.IFMDBDataReadService;
import com.snapdeal.cocofs.entity.FulfillmentCentre;
import com.snapdeal.cocofs.entity.SellerFCCodeMapping;
import com.snapdeal.cocofs.entity.SellerFMMapping;
import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.generic.persister.exception.GenericPersisterException;
import com.snapdeal.cocofs.generic.persister.response.GenericPersisterWithAerospikeResponse;
import com.snapdeal.cocofs.services.IDataReaderWithAerospike;
import com.snapdeal.cocofs.services.IDataUpdaterWithAerospike;
import com.snapdeal.cocofs.services.ISellerDetailsService;
import com.snapdeal.cocofs.services.common.dto.UserInfo;
import com.snapdeal.cocofs.services.data.engine.IDataEngineWithAerospike;
import com.snapdeal.cocofs.services.events.dto.SellerFMFCUpdateEventObj;
import com.snapdeal.cocofs.services.fc.dto.SellerFCMappingDataDTO;
import com.snapdeal.cocofs.services.fm.IFulfillmentCenterService;
import com.snapdeal.cocofs.services.fm.IFulfilmentModelService;
import com.snapdeal.cocofs.services.fm.ISellerSUPCFMMappingProcessor;
import com.snapdeal.cocofs.services.fm.dto.AddressDTO;
import com.snapdeal.cocofs.services.fm.dto.SellerFMMappingUpdateDto;
import com.snapdeal.cocofs.sro.FulfillmentModel;

/**
 * @version 1.0, 23-Mar-2015
 * @author ankur
 */

@Controller
@RequestMapping(FCMappingController.URL)
public class FCMappingController {

    public static final String                                                                     URL = "/admin/fcMapping";

    private static final Logger                                                                    LOG = LoggerFactory.getLogger(FCMappingController.class);

    @Autowired
    IFulfilmentModelService                                                                        fulfilmentModelService;

    @Autowired
    IFulfillmentCenterService                                                                      fulfilmentCentreService;
    
    @Autowired
    ISellerDetailsService                                                                          sellerDetailsService; 

    @Autowired
    private IDataUpdaterWithAerospike                                                              aerospikedataUpdater;

    @Autowired
    @Qualifier("SellerFMMappingDataEngine")
    private IDataEngineWithAerospike<SellerFMMappingUpdateDto, SellerFMMapping, SellerFMMappingVO> sellerFMMappingDataEngine;

    @Autowired
    @Qualifier("SellerFMMappingDataReader")
    private IDataReaderWithAerospike<SellerFMMappingUpdateDto, SellerFMMapping, SellerFMMappingVO> sellerFMMappingDataReader;

    @Autowired
    private IFMDBDataReadService                                                                   fmDBDataReadService;

    @Autowired
    private com.snapdeal.cocofs.services.converter.IConverterService                               convertorService;

    @Autowired
    @Qualifier("failSafeSellerSUPCFMNotificationSendor")
    private ISellerSUPCFMMappingProcessor                                                          failSafeFMFCMappingProcessor;
    
    @Autowired
    IFCAerospikeDataReadService        fcAerospikeService;
  

    @RequestMapping("")
    public String uploadPage(ModelMap map) {
        Property p = Property.SELLER_SUPC_FM_FC_UPDATE_PUSH_ENABLED;
        if (!ConfigUtils.getBooleanScalar(p)) {
            map.addAttribute("message", "The page you were looking is not live at this moment.");
            return "admin/fcMapping/message";
        }
        return "admin/fcMapping/sellerCentreMapping";
    }

    @RequestMapping("/searchSeller")
    public @ResponseBody SellerFCMappingDataDTO getFCMappingForSeller(@RequestParam("sellerCode") String sellerCode) {
        SellerFCMappingDataDTO sellerFCDTO = new SellerFCMappingDataDTO();

        try {
            if (StringUtils.isNotEmpty(sellerCode)) {

                SellerFMMapping sellerFMMapping = fmDBDataReadService.getEnabledSellerFMMapping(sellerCode);
                if (sellerFMMapping != null && StringUtils.isNotEmpty(sellerFMMapping.getFulfilmentModel())) {
                    if (sellerCode.equals(sellerFMMapping.getSellerCode())) {

                        sellerFCDTO.setSellerCode(sellerFMMapping.getSellerCode());
                        String fmModel = sellerFMMapping.getFulfilmentModel();
                        sellerFCDTO.setFulfilmentModel(fmModel);
                        
                        updateSellerFCMappingDataDTO(sellerFMMapping, sellerFCDTO);

                        if (isFCEnabledFM(fmModel)) {
                            Map<String, String> fmFCMapping = ConfigUtils.getMap(Property.FM_CODE_TO_FC_TYPE_MAP);
                            String type = fmFCMapping.get(fmModel);
                            List<FulfillmentCentre> allFCs = fulfilmentCentreService.getAllFCsByType(type);

                            List<String> eligibleFCCodesList = new ArrayList<String>();
                            for (FulfillmentCentre fc : allFCs) {
                                eligibleFCCodesList.add(fc.getCode());
                            }
                            sellerFCDTO.setEligibleFCCodesList(eligibleFCCodesList);
                        }
                    } else {
                        sellerFCDTO.setErrorMessage("invalid sellerCode " + sellerCode + ", possibly case do not match. Did you mean " + sellerFMMapping.getSellerCode());
                        LOG.info("invalid sellerCode {}, possibly case do not match", sellerCode);
                    }
                } else {
                    sellerFCDTO.setErrorMessage("no sellerCode fulfilment mapping found for " + sellerCode);
                    LOG.info("invalid sellerCode {}, no sellerCode fulfilment mapping found", sellerCode);
                }

            } else {
                sellerFCDTO.setErrorMessage("invalid sellerCode " + sellerCode);
                LOG.info("invalid sellerCode ");
            }
        } catch (Exception e) {
            LOG.error("error while fetching fc/fm data for seller " + sellerCode, e);
        }
        LOG.info("SellerFCMappingDataDTO is :{}", sellerFCDTO);

        return sellerFCDTO;
    }

    private void updateSellerFCMappingDataDTO(SellerFMMapping sellerFMMapping, SellerFCMappingDataDTO dto) {
        if (sellerFMMapping != null) {
            Set<SellerFCCodeMapping> fcMappingList = sellerFMMapping.getFcCenters();
            if (fcMappingList != null && fcMappingList.size() > 0) {
                for (SellerFCCodeMapping fcCodeMapping : fcMappingList) {
                    if (fcCodeMapping.isEnabled()) {
                        dto.setExistingFC(fcCodeMapping.getFcCode());
                        break;
                    }
                }
            }
        }
    }

    @RequestMapping(value = "updateSellerFC", produces = "application/json", method = { RequestMethod.POST })
    @ResponseBody
    public SystemResponse addOrUpdateSellerFCMapping(@RequestParam(value = "sellerCode", required = true) String sellerCode, @RequestParam(value = "fcCode") String fcCode,
            @RequestParam(value = "fulfilmentModel") String fulfilmentModel) {
        SystemResponse response = new SystemResponse();
        response.setStatus(SystemResponse.ResponseStatus.FAIL.name());
        try {

            if (validateFmModel(fulfilmentModel, sellerCode)) {
                String userEmail = WebContextUtils.getCurrentUserEmail();
                UserInfo info = new UserInfo(userEmail, false);

                Date lastUpdated = DateUtils.getCurrentDate();
                List<String> existingFCCenters = null;
                SellerFMMapping mapping = null;

                mapping = fmDBDataReadService.getEnabledSellerFMMapping(sellerCode);

                if (mapping != null && mapping.isEnabled()) {
                    Set<SellerFCCodeMapping> existingFCCentersData = mapping.getFcCenters();
                    lastUpdated = mapping.getLastUpdated();

                    if (!CollectionUtils.isEmpty(existingFCCentersData)) {
                        existingFCCenters = new ArrayList<String>();
                        for (SellerFCCodeMapping o : existingFCCentersData) {
                            if (o != null && o.isEnabled()) {
                                existingFCCenters.add(o.getFcCode());
                            }
                        }
                    }
                }

                GenericPersisterWithAerospikeResponse<SellerFMMapping, SellerFMMappingVO> resp = aerospikedataUpdater.updateDataWithDTO(new SellerFMMappingUpdateDto(sellerCode,
                        fulfilmentModel, fcCode, lastUpdated), sellerFMMappingDataReader, sellerFMMappingDataEngine, info, true);

                if (!resp.isSuccessful()) {
                    response.setMessage(resp.getMessage());
                } else {
                    response.setStatus(SystemResponse.ResponseStatus.SUCCESS.name());
                    response.setMessage("success");

                    pushUpdatesForSellerFCMappingUpdate(sellerCode, fulfilmentModel, fcCode, existingFCCenters);
                }
            } else {
                LOG.info("Invalid fulfilment model was passed from gui for sellerCode {}", sellerCode);
                response.setMessage("fulfilment model for seller " + sellerCode + " has been changed , please refresh the page");
            }

        } catch (OperationNotSupportedException e) {
            LOG.info("error while saving data", e);
        } catch (GenericPersisterException e) {
            LOG.info("exception while saving data", e);

        }
        return response;
    }

    private boolean validateFmModel(String fmModel, String sellerCode) {
        SellerFMMapping sellerFMMapping = fmDBDataReadService.getEnabledSellerFMMapping(sellerCode);
        if (sellerFMMapping != null && sellerFMMapping.getFulfilmentModel().equals(fmModel)) {
            return true;
        }

        return false;

    }

    private List<String> getExistingFCCenters(String sellerCode) {
        List<String> existingFCCenters = null;
        SellerFMMappingVO vo = null;
        try {
            vo = fulfilmentModelService.getFulfilmentModelBySellerFromAerospike(sellerCode);
            if (vo != null && vo.isEnabled()) {
                existingFCCenters = vo.getFcCenters();
            }
        } catch (ExternalDataNotFoundException e) {
            LOG.info("exception while getting data from aerospike ", e);
        }
        return existingFCCenters;
    }

    private boolean isFCEnabledFM(String fmType) {
        return (FulfillmentModel.FC_VOI.getCode().equals(fmType) || FulfillmentModel.ONESHIP.getCode().equals(fmType) || FulfillmentModel.OCPLUS.getCode().equals(fmType));
    }

    private void pushUpdatesForSellerFCMappingUpdate(String sellerCode, String fulfilmentModel, String fcCode, List<String> existingFCCenters) {
        List<String> newFCCenters = convertorService.convertToFCCentreList(fcCode);
        //fulfilment mode does not change here 
        
            AddressDTO addressDTO = sellerDetailsService.getSellerDetailsFromBaseSource(sellerCode);
            String sellerPincode = addressDTO.getPincode();

        failSafeFMFCMappingProcessor.processChangeOfSellerFMMappingForExternalNotification(new SellerFMFCUpdateEventObj(sellerCode, fulfilmentModel, fulfilmentModel, newFCCenters,
                existingFCCenters,sellerPincode,sellerPincode));
    }
    
    
}
