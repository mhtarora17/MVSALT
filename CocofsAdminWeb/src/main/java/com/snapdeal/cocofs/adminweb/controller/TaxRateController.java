/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.adminweb.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.snapdeal.base.cache.CacheManager;
import com.snapdeal.cocofs.cache.StateCache;
import com.snapdeal.cocofs.common.JobValidationException;
import com.snapdeal.cocofs.commonweb.utils.ExportToExcelUtils;
import com.snapdeal.cocofs.commonweb.utils.WebContextUtils;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.dataaccess.taxclass.ITaxClassDBDataReadService;
import com.snapdeal.cocofs.dataaccess.taxrate.ITaxRateDBDataReadService;
import com.snapdeal.cocofs.entity.JobDetail;
import com.snapdeal.cocofs.entity.SellerCategoryTaxRate;
import com.snapdeal.cocofs.entity.SellerSupcTaxRate;
import com.snapdeal.cocofs.entity.StateCategoryPriceTaxRate;
import com.snapdeal.cocofs.enums.JobActionCode;
import com.snapdeal.cocofs.services.IJobSchedulerService;
import com.snapdeal.cocofs.services.converter.IConverterService;
import com.snapdeal.cocofs.services.taxrate.dto.SellerCategoryTaxRateUploadDTO;
import com.snapdeal.cocofs.services.taxrate.dto.SellerSUPCTaxRateUploadDTO;
import com.snapdeal.cocofs.services.taxrate.dto.StateCategoryPriceTaxRateUploadDTO;
import com.snapdeal.cocofs.services.taxrate.dto.StateDTO;

/**
 * @version 1.0, 13-Aug-2014
 * @author ankur
 */

@Controller
@RequestMapping("/admin/taxRate")
public class TaxRateController {

    private static final Logger        LOG = LoggerFactory.getLogger(TaxRateController.class);

    @Autowired
    private IJobSchedulerService       jobSchedulerService;

    @Autowired
    private ITaxRateDBDataReadService  taxRateDBService;

    @Autowired
    private ITaxClassDBDataReadService taxClassDBService;

    @Autowired
    private IConverterService          converterService;

    @RequestMapping("taxRateUpload")
    public String uploadPage(ModelMap map) {
        addTaxRatetypesInModelMap(map);
        return "admin/taxrate/taxRateUpload";
    }

    @RequestMapping("/upload")
    public String upload(ModelMap map, @RequestParam(value = "inputfile") MultipartFile inputFile, @RequestParam(value = "taxRateLevel", required = true) String taxRateLevel) {

        if (!taxRateLevel.equals(JobActionCode.STATE_CATEGORY_PRICE_TAX_RATE.getCode()) && !taxRateLevel.equals(JobActionCode.SELLER_CATEGORY_TAX_RATE.getCode())
                && !taxRateLevel.equals(JobActionCode.SELLER_SUPC_TAX_RATE.getCode())) {

            LOG.info("TaxRate upload  for " + taxRateLevel + " could not be done ");
            map.addAttribute("message", "Invalid taxRateLevel " + taxRateLevel);
            map.addAttribute("success", "false");

        } else {
            try {
                String userEmail = WebContextUtils.getCurrentUserEmail();
                JobDetail jd = jobSchedulerService.saveJob(inputFile, taxRateLevel, userEmail);
                map.addAttribute("success", "true");

                String actionCodeMSG = "";
                if (jd.getAction() != null) {
                    actionCodeMSG = ", Action code:" + jd.getAction().getCode();
                }
                map.addAttribute("message", "TaxRate upload for " + taxRateLevel + " done. <br>Job code:" + jd.getFileCode() + actionCodeMSG + ", saved at  " + jd.getFilePath()
                        + File.separator + jd.getFileName());

            } catch (IOException e) {
                LOG.info("TaxRate upload  for " + taxRateLevel + " could not be done ", e);
                map.addAttribute("success", "false");
                map.addAttribute("message", "TaxRate upload  for " + taxRateLevel + " could not be done.");
            } catch (JobValidationException e) {
                LOG.error("TaxRate upload  for " + taxRateLevel + " upload could not be done ", e);
                map.addAttribute("success", "false");
                map.addAttribute("message", "TaxRate upload  for " + taxRateLevel + " could not be done. Message: <br>" + e.getMessage());
                map.addAttribute("validationResponse", e.getResponse());
            } catch (Exception e) {
                LOG.info("TaxRate upload  for " + taxRateLevel + " could not be done ", e);
                map.addAttribute("success", "false");
                map.addAttribute("message", "TaxRate upload  for " + taxRateLevel + " could not be done, Something bad happened");
            }
        }
        addTaxRatetypesInModelMap(map);
        return "admin/taxrate/taxRateUpload";
    }

    private void addTaxRatetypesInModelMap(ModelMap map) {
        List<String> allTaxRateLevels = new ArrayList<String>();
        allTaxRateLevels.add(JobActionCode.STATE_CATEGORY_PRICE_TAX_RATE.getCode());
        allTaxRateLevels.add(JobActionCode.SELLER_CATEGORY_TAX_RATE.getCode());
        allTaxRateLevels.add(JobActionCode.SELLER_SUPC_TAX_RATE.getCode());
        map.addAttribute("allTaxRateLevels", allTaxRateLevels);

    }

    @RequestMapping("/downloadSCPTRTemplate")
    public void downloadStateCategoryPriceTaxRateTemplate(HttpServletRequest req, HttpServletResponse resp) {
        try {
            ExportToExcelUtils.downloadTemplate(resp, StateCategoryPriceTaxRateUploadDTO.class, "StateCategoryPriceTaxRate");

        } catch (IOException e) {
            LOG.error("Exception", e);
        } catch (ClassNotFoundException e) {
            LOG.error("Exception", e);
        }
    }

    @RequestMapping("/downloadSCPTRTemplateAndData")
    public void downloadStateCategoryPriceTaxRateTemplateAndData(HttpServletRequest req, HttpServletResponse resp) {
        try {
            List<StateCategoryPriceTaxRateUploadDTO> dtoList = new ArrayList<StateCategoryPriceTaxRateUploadDTO>();

            List<StateCategoryPriceTaxRate> entityList = taxRateDBService.getAllEnabledStateCategoryPriceTaxRates();

            if (entityList != null) {
                for (StateCategoryPriceTaxRate entity : entityList) {
                    StateCategoryPriceTaxRateUploadDTO dto = converterService.getStateCategoryPriceTaxRateUploadDTO(entity);
                    if (dto != null) {
                        dtoList.add(dto);
                    }
                }
            }
            if (dtoList.size() <= ConfigUtils.getIntegerScalar(Property.EXCEL_SHEET_MAX_VALUE)) {
                ExportToExcelUtils.exportToExcel(resp, StateCategoryPriceTaxRateUploadDTO.class, "StateCategoryPriceTaxRate", dtoList);

            } else {
                ExportToExcelUtils.exportToCsv(resp, StateCategoryPriceTaxRateUploadDTO.class, "StateCategoryPriceTaxRate", dtoList, true, false);
            }
        } catch (IOException e) {
            LOG.error("Exception", e);
        } catch (ClassNotFoundException e) {
            LOG.error("Exception", e);
        }
    }

    @RequestMapping("/downloadSCTRTemplate")
    public void downloadSellerCategoryTaxRateTemplate(HttpServletRequest req, HttpServletResponse resp) {
        try {
            ExportToExcelUtils.downloadTemplate(resp, SellerCategoryTaxRateUploadDTO.class, "SellerCategoryTaxRate");
        } catch (IOException e) {
            LOG.error("Exception", e);
        } catch (ClassNotFoundException e) {
            LOG.error("Exception", e);
        }
    }

    @RequestMapping("/downloadSCTRTemplateAndData")
    public void downloadSellerCategoryTaxRateTemplateAndData(HttpServletRequest req, HttpServletResponse resp) {
        try {
            List<SellerCategoryTaxRateUploadDTO> dtoList = new ArrayList<SellerCategoryTaxRateUploadDTO>();

            List<SellerCategoryTaxRate> entityList = taxRateDBService.getAllEnabledSellerCategoryTaxRates();

            if (entityList != null) {
                for (SellerCategoryTaxRate entity : entityList) {
                    SellerCategoryTaxRateUploadDTO dto = converterService.getSellerCategoryTaxRateUploadDTO(entity);
                    if (dto != null) {
                        dtoList.add(dto);
                    }
                }
            }
            if (dtoList.size() <= ConfigUtils.getIntegerScalar(Property.EXCEL_SHEET_MAX_VALUE)) {
                ExportToExcelUtils.exportToExcel(resp, SellerCategoryTaxRateUploadDTO.class, "SellerCategoryTaxRate", dtoList);

            } else {
                ExportToExcelUtils.exportToCsv(resp, SellerCategoryTaxRateUploadDTO.class, "SellerCategoryTaxRate", dtoList, true, false);
            }
        } catch (IOException e) {
            LOG.error("Exception", e);
        } catch (ClassNotFoundException e) {
            LOG.error("Exception", e);
        }
    }

    @RequestMapping("/downloadSSTRData")
    public void downloadSellerSupcTaxRateData(HttpServletRequest req, HttpServletResponse resp) {

        try {

            if (!ConfigUtils.getBooleanScalar(Property.ENABLE_SUPC_TAX_RATE_TEMPLATE_DOWNLOAD)) {
                resp.sendError(HttpStatus.SC_FORBIDDEN, "download of data is not allowed at present");
                resp.setContentType("text/html");
                return;
            }

            List<SellerSUPCTaxRateUploadDTO> dtoList = new ArrayList<SellerSUPCTaxRateUploadDTO>();

            List<SellerSupcTaxRate> entityList = taxRateDBService.getAllEnabledSellerSupcTaxRates();

            if (entityList != null) {
                for (SellerSupcTaxRate entity : entityList) {
                    SellerSUPCTaxRateUploadDTO dto = converterService.getSellerSUPCTaxRateUploadDTO(entity);
                    if (dto != null) {
                        dtoList.add(dto);
                    }
                }
            }

            if (dtoList.size() <= ConfigUtils.getIntegerScalar(Property.EXCEL_SHEET_MAX_VALUE)) {
                ExportToExcelUtils.exportToExcel(resp, SellerSUPCTaxRateUploadDTO.class, "SellerSupcTaxRate", dtoList);

            } else {
                ExportToExcelUtils.exportToCsv(resp, SellerSUPCTaxRateUploadDTO.class, "SellerSupcTaxRate", dtoList, true, false);
            }
        } catch (IOException e) {
            LOG.error("Exception", e);
        } catch (Exception e) {
            LOG.error("Exception", e);
        }
    }

    @RequestMapping("/downloadSSTRTemplate")
    public void downloadSellerSupcTaxRateTemplate(HttpServletRequest req, HttpServletResponse resp) {

        try {
            ExportToExcelUtils.downloadTemplate(resp, SellerSUPCTaxRateUploadDTO.class, "SellerSupcTaxRate");
        } catch (IOException e) {
            LOG.error("Exception", e);
        } catch (Exception e) {
            LOG.error("Exception", e);
        }
    }

    @RequestMapping("/downloadStateLists")
    public void downloadStatesList(HttpServletRequest req, HttpServletResponse resp) {
        try {
            List<String> statesList = CacheManager.getInstance().getCache(StateCache.class).getStatesList();
            List<StateDTO> stateList = new ArrayList<StateDTO>();
            for (String stateName : statesList) {
                StateDTO dto = new StateDTO(stateName);
                stateList.add(dto);
            }
            ExportToExcelUtils.exportToExcel(resp, StateDTO.class, "StatesList", stateList, true, false);
        } catch (IOException e) {
            LOG.error("Exception", e);
        } catch (ClassNotFoundException e) {
            LOG.error("Exception", e);
        }
    }

}
