/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.adminweb.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.cocofs.commonweb.response.SystemResponse;
import com.snapdeal.cocofs.core.aerospike.vo.SellerFMMappingVO;
import com.snapdeal.cocofs.core.aerospike.vo.SellerSubcatFMMappingVO;
import com.snapdeal.cocofs.core.aerospike.vo.SellerSupcFMMappingVO;
import com.snapdeal.cocofs.dataaccess.fm.IFMAerospikeDataReadService;
import com.snapdeal.cocofs.dataaccess.fm.IFMDBDataReadService;
import com.snapdeal.cocofs.entity.SellerFCCodeMapping;
import com.snapdeal.cocofs.entity.SellerFMMapping;
import com.snapdeal.cocofs.entity.SellerSubcatFMMapping;
import com.snapdeal.cocofs.entity.SellerSupcFCCodeMapping;
import com.snapdeal.cocofs.entity.SellerSupcFMMapping;
import com.snapdeal.cocofs.utils.AerospikeKeyHelper;
import com.snapdeal.score.data.aerospike.AerospikeSet;
import com.snapdeal.score.data.aerospike.service.impl.ScoreAerospikeService;

/**
 * @version 1.0, 18-Sep-2014
 * @author ankur
 */
@Controller
@RequestMapping(manageFMAerospikeController.URL)
public class manageFMAerospikeController<R extends AerospikeSet> {
    private static final Logger         LOG  = LoggerFactory.getLogger(manageFMAerospikeController.class);

    public static final String          URL  = "/admin/manageFMAerospike/";

    private static Gson                 gson = new Gson();

    @Autowired
    private IFMAerospikeDataReadService aerospikeReadService;

    @Autowired
    private IFMDBDataReadService        dbReadService;

    @Autowired
    @Qualifier("aerospikeService")
    private ScoreAerospikeService       aerospikeService;

    @RequestMapping("/get/sellerFMMapping")
    @ResponseBody
    public String getSellerFMMapping(@RequestParam(value = ("sellerCode")) String sellerCode) {
        SystemResponse response = new SystemResponse();
        SellerFMMappingVO sellerFMMappingVO = null;
        try {
            sellerFMMappingVO = (SellerFMMappingVO) aerospikeReadService.getFMMapping(sellerCode, SellerFMMappingVO.class);
            if (sellerFMMappingVO != null) {
                Map<String, Object> items = new HashMap<String, Object>();
                items.put("sellerFMMapping", sellerFMMappingVO);
                response.setItems(items);
            }
        } catch (Exception e) {
            response.setMessage(e.getMessage());
            LOG.error("failed to get from Aerospike:", e);
        }
        return response.toString();
    }

    @RequestMapping("/sync/sellerFMMapping")
    @ResponseBody
    public String syncSellerFMMapping(@RequestParam(value = ("sellerCode")) String sellerCode) {
        SystemResponse response = new SystemResponse();
        SellerFMMappingVO record = null;

        try {
            SellerFMMapping entity = dbReadService.getSellerFMMapping(sellerCode);
            record = (SellerFMMappingVO) aerospikeReadService.getFMMapping(sellerCode, SellerFMMappingVO.class);
            boolean syncRequired = false;
            StringBuilder message = new StringBuilder();
            if (entity != null && record != null) {

                if (!record.getFulfillmentModel().equals(entity.getFulfilmentModel())) {
                    record.setFulfillmentModel(entity.getFulfilmentModel());
                    syncRequired = true;
                    message.append("set FM to :" + entity.getFulfilmentModel());
                }
                if (record.isEnabled() ^ entity.isEnabled()) {
                    record.setEnabled(entity.isEnabled() ? 1 : 0);
                    syncRequired = true;
                    message.append("marked Enabled to :" + entity.isEnabled());
                }
                if (record.isSupcExist() ^ entity.isSupcExist()) {
                    record.setSupcExist(entity.isSupcExist() ? 1 : 0);
                    syncRequired = true;
                    message.append("marked SupcExist to :" + entity.isSupcExist());

                }
                if (record.isSubcatExist() ^ entity.isSubcatExist()) {
                    record.setSubcatExist(entity.isSubcatExist() ? 1 : 0);
                    syncRequired = true;
                    message.append("marked SubcatExist to :" + entity.isSubcatExist());
                }

            } else if (entity != null && record == null) {
                syncRequired = true;
                record = new SellerFMMappingVO();
                record.setSellerCode(entity.getSellerCode());
                record.setEnabled(entity.isEnabled() ? 1 : 0);
                record.setCreatedStr(entity.getCreated().getTime() + "");
                record.setUpdatedStr(DateUtils.getCurrentTime().getTime() + "");
                record.setFulfillmentModel(entity.getFulfilmentModel());
                record.setSupcExist(entity.isSupcExist() ? 1 : 0);
                record.setSubcatExist(entity.isSubcatExist() ? 1 : 0);
                List<String> fcCodeList = new ArrayList<String>();
                for(SellerFCCodeMapping fcMapping:entity.getFcCenters() ){
                    if(fcMapping != null && fcMapping.isEnabled()){
                        fcCodeList.add(fcMapping.getFcCode());
                    }
                } 
                if(fcCodeList.size() > 0){
                    record.setFcCenters(fcCodeList);
                }
            }

            if (syncRequired) {
                aerospikeService.put(record);
                Map<String, Object> items = new HashMap<String, Object>();
                items.put("sellerFMMapping", record);
                response.setItems(items);
                response.setMessage(message.toString());
            } else {
                response.setMessage("No sync is required, aerospike record has same value as in mysql ");
            }

        } catch (Exception e) {
            response.setMessage(e.getMessage());
            LOG.error("failed to sync object with Aerospike:", e);
        }
        return response.toString();
    }

    @RequestMapping("/get/sellerSupcFMMapping")
    @ResponseBody
    public String getSellerSupcFMMapping(@RequestParam(value = ("sellerCode")) String sellerCode, @RequestParam(value = ("supc")) String supc) {
        SystemResponse response = new SystemResponse();
        SellerSupcFMMappingVO sellerSupcFMMappingVO = null;
        try {
            sellerSupcFMMappingVO = (SellerSupcFMMappingVO) aerospikeReadService.getFMMapping(AerospikeKeyHelper.getKey(sellerCode, supc), SellerSupcFMMappingVO.class);
            if (sellerSupcFMMappingVO != null) {
                Map<String, Object> items = new HashMap<String, Object>();
                items.put("sellerSupcFMMapping", sellerSupcFMMappingVO);
                response.setItems(items);
            }
        } catch (Exception e) {
            response.setMessage(e.getMessage());
            LOG.error("failed to get from Aerospike:", e);
        }
        return response.toString();
    }

    @RequestMapping("/sync/sellerSupcFMMapping")
    @ResponseBody
    public String syncSellerSupcFMMapping(@RequestParam(value = ("sellerCode")) String sellerCode, @RequestParam(value = ("supc")) String supc) {
        SystemResponse response = new SystemResponse();
        SellerSupcFMMappingVO record = null;

        try {
            SellerSupcFMMapping entity = dbReadService.getSellerSupcFMMapping(sellerCode, supc);
            record = (SellerSupcFMMappingVO) aerospikeReadService.getFMMapping(AerospikeKeyHelper.getKey(sellerCode, supc), SellerSupcFMMappingVO.class);
            boolean syncRequired = false;
            StringBuilder message = new StringBuilder();
            if (entity != null && record != null) {

                if (!record.getFulfillmentModel().equals(entity.getFulfilmentModel())) {
                    record.setFulfillmentModel(entity.getFulfilmentModel());
                    syncRequired = true;
                    message.append("set FM to :" + entity.getFulfilmentModel());
                }
                if (record.isEnabled() ^ entity.isEnabled()) {
                    record.setEnabled(entity.isEnabled() ? 1 : 0);
                    syncRequired = true;
                    message.append("marked Enabled to :" + entity.isEnabled());
                }

            } else if (entity != null && record == null) {
                syncRequired = true;
                record = new SellerSupcFMMappingVO();
                record.setSellerCode(entity.getSellerCode());
                record.setSupc(entity.getSupc());
                record.setEnabled(entity.isEnabled() ? 1 : 0);
                record.setCreatedStr(entity.getCreated().getTime() + "");
                record.setUpdatedStr(DateUtils.getCurrentTime().getTime() + "");
                record.setFulfillmentModel(entity.getFulfilmentModel());
                record.setSupcSellerCode(AerospikeKeyHelper.getKey(entity.getSellerCode(), entity.getSupc()));
                
                List<String> fcCodeList = new ArrayList<String>();
                for(SellerSupcFCCodeMapping fcMapping:entity.getFcCenters() ){
                    if(fcMapping != null && fcMapping.isEnabled()){
                        fcCodeList.add(fcMapping.getFcCode());
                    }
                } 
                if(fcCodeList.size() > 0){
                    record.setFcCenters(fcCodeList);
                }
                
            }

            if (syncRequired) {
                aerospikeService.put(record);
                Map<String, Object> items = new HashMap<String, Object>();
                items.put("sellerSupcFMMapping", record);
                response.setItems(items);
                response.setMessage(message.toString());
            } else {
                response.setMessage("No sync is required, aerospike record has same value as in mysql ");
            }

        } catch (Exception e) {
            response.setMessage(e.getMessage());
            LOG.error("failed to sync object with Aerospike:", e);
        }
        return response.toString();
    }

    @RequestMapping("/get/sellerSubcatFMMapping")
    @ResponseBody
    public String getSellerSubcatFMMapping(@RequestParam(value = ("sellerCode")) String sellerCode, @RequestParam(value = ("subcat")) String subcat) {
        SystemResponse response = new SystemResponse();
        SellerSubcatFMMappingVO sellerSubcatFMMappingVO = null;
        try {
            sellerSubcatFMMappingVO = (SellerSubcatFMMappingVO) aerospikeReadService.getFMMapping(AerospikeKeyHelper.getKey(sellerCode, subcat), SellerSubcatFMMappingVO.class);
            if (sellerSubcatFMMappingVO != null) {
                Map<String, Object> items = new HashMap<String, Object>();
                items.put("sellerSubcatFMMapping", sellerSubcatFMMappingVO);
                response.setItems(items);
            }
        } catch (Exception e) {
            response.setMessage(e.getMessage());
            LOG.error("failed to get from Aerospike:", e);
        }
        return response.toString();
    }

    @RequestMapping("/sync/sellerSubcatFMMapping")
    @ResponseBody
    public String syncSellerSubcatFMMapping(@RequestParam(value = ("sellerCode")) String sellerCode, @RequestParam(value = ("subcat")) String subcat) {
        SystemResponse response = new SystemResponse();
        SellerSubcatFMMappingVO record = null;

        try {
            SellerSubcatFMMapping entity = dbReadService.getSellerSubcatFMMapping(sellerCode, subcat);
            record = (SellerSubcatFMMappingVO) aerospikeReadService.getFMMapping(AerospikeKeyHelper.getKey(sellerCode, subcat), SellerSubcatFMMappingVO.class);
            boolean syncRequired = false;
            StringBuilder message = new StringBuilder();
            if (entity != null && record != null) {

                if (!record.getFulfillmentModel().equals(entity.getFulfilmentModel())) {
                    record.setFulfillmentModel(entity.getFulfilmentModel());
                    syncRequired = true;
                    message.append("set FM to :" + entity.getFulfilmentModel());
                }
                if (record.isEnabled() ^ entity.isEnabled()) {
                    record.setEnabled(entity.isEnabled() ? 1 : 0);
                    syncRequired = true;
                    message.append("marked Enabled to :" + entity.isEnabled());
                }

            } else if (entity != null && record == null) {
                syncRequired = true;
                record = new SellerSubcatFMMappingVO();
                record.setSellerCode(entity.getSellerCode());
                record.setSubcat(entity.getSubcat());
                record.setEnabled(entity.isEnabled() ? 1 : 0);
                record.setCreatedStr(entity.getCreated().getTime() + "");
                record.setUpdatedStr(DateUtils.getCurrentTime().getTime() + "");
                record.setFulfillmentModel(entity.getFulfilmentModel());
                record.setSubcatSellerCode(AerospikeKeyHelper.getKey(entity.getSellerCode(), entity.getSubcat()));
            }

            if (syncRequired) {
                aerospikeService.put(record);
                Map<String, Object> items = new HashMap<String, Object>();
                items.put("sellerSubcatFMMapping", record);
                response.setItems(items);
                response.setMessage(message.toString());
            } else {
                response.setMessage("No sync is required, aerospike record has same value as in mysql ");
            }

        } catch (Exception e) {
            response.setMessage(e.getMessage());
            LOG.error("failed to sync object with Aerospike:", e);
        }
        return response.toString();
    }

}
