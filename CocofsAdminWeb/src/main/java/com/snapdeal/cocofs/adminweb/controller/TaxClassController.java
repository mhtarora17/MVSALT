/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.adminweb.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.snapdeal.base.cache.CacheManager;
import com.snapdeal.cocofs.adminweb.controller.form.FulfillmentCenterForm;
import com.snapdeal.cocofs.cache.TaxClassCache;
import com.snapdeal.cocofs.common.JobValidationException;
import com.snapdeal.cocofs.commonweb.utils.ExportToExcelUtils;
import com.snapdeal.cocofs.commonweb.utils.WebContextUtils;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.dataaccess.taxclass.ITaxClassDBDataReadService;
import com.snapdeal.cocofs.entity.JobDetail;
import com.snapdeal.cocofs.entity.SubcatTaxClassMapping;
import com.snapdeal.cocofs.entity.SupcTaxClassMapping;
import com.snapdeal.cocofs.enums.JobActionCode;
import com.snapdeal.cocofs.services.IJobSchedulerService;
import com.snapdeal.cocofs.services.converter.IConverterService;
import com.snapdeal.cocofs.services.taxclass.dto.SubcatTaxClassMappingDTO;
import com.snapdeal.cocofs.services.taxclass.dto.SupcTaxClassMappingDTO;
import com.snapdeal.cocofs.services.taxclass.dto.TaxClassDTO;

/**
 * @version 1.0, 13-Aug-2014
 * @author nitish
 */

@Controller
@RequestMapping("/admin/taxClass")
public class TaxClassController {

    private static final Logger        LOG = LoggerFactory.getLogger(TaxClassController.class);

    @Autowired
    private IJobSchedulerService       jobSchedulerService;

    @Autowired
    private ITaxClassDBDataReadService taxClassDBService;

    @Autowired
    private IConverterService          converterService;

    @RequestMapping("taxClassUpload")
    public String uploadPage(ModelMap map) {

        Property p = Property.ENABLE_TAX_CLASS;
        if (!ConfigUtils.getBooleanScalar(p)) {
            map.addAttribute("message", "The page you were looking is not live at this moment.");
            return "admin/taxrate/message";
        }
        addTaxClasstypesInModelMap(map);
        return "admin/taxrate/taxClassUpload";
    }

    @RequestMapping("/upload")
    public String upload(ModelMap map, @RequestParam(value = "inputfile") MultipartFile inputFile, @RequestParam(value = "taxClassLevel", required = true) String taxClassLevel) {

        if (!taxClassLevel.equals(JobActionCode.SUPC_TAX_CLASS.getCode()) && !taxClassLevel.equals(JobActionCode.SUBCAT_TAX_CLASS.getCode())) {

            LOG.info("TaxClass upload  for " + taxClassLevel + " could not be done ");
            map.addAttribute("message", "Invalid taxClassLevel " + taxClassLevel);
            map.addAttribute("success", "false");

        } else {
            try {
                String userEmail = WebContextUtils.getCurrentUserEmail();
                JobDetail jd = jobSchedulerService.saveJob(inputFile, taxClassLevel, userEmail);
                if (jd != null) {
                    map.addAttribute("success", "true");

                    String actionCodeMSG = "";
                    if (jd.getAction() != null) {
                        actionCodeMSG = ", Action code:" + jd.getAction().getCode();
                    }
                    map.addAttribute("message",
                            "TaxClass upload for " + taxClassLevel + " done. <br>Job code:" + jd.getFileCode() + actionCodeMSG + ", saved at  " + jd.getFilePath() + File.separator
                                    + jd.getFileName());
                }
            } catch (IOException e) {
                LOG.info("TaxClass upload  for " + taxClassLevel + " could not be done ", e);
                map.addAttribute("success", "false");
                map.addAttribute("message", "TaxClass upload  for " + taxClassLevel + " could not be done.");
            } catch (JobValidationException e) {
                LOG.error("TaxClass upload  for " + taxClassLevel + " upload could not be done ", e);
                map.addAttribute("success", "false");
                map.addAttribute("message", "TaxClass upload  for " + taxClassLevel + " could not be done. Message: <br>" + e.getMessage());
                map.addAttribute("validationResponse", e.getResponse());
            } catch (Exception e) {
                LOG.info("TaxClass upload  for " + taxClassLevel + " could not be done ", e);
                map.addAttribute("success", "false");
                map.addAttribute("message", "TaxClass upload  for " + taxClassLevel + " could not be done, Something bad happened");
            }
        }
        addTaxClasstypesInModelMap(map);
        return "admin/taxrate/taxClassUpload";
    }

    private void addTaxClasstypesInModelMap(ModelMap map) {
        List<String> allTaxClassLevels = new ArrayList<String>();
        allTaxClassLevels.add(JobActionCode.SUPC_TAX_CLASS.getCode());
        allTaxClassLevels.add(JobActionCode.SUBCAT_TAX_CLASS.getCode());

        map.addAttribute("allTaxClassLevels", allTaxClassLevels);

    }

    @RequestMapping("/downloadSupcTaxClassTemplate")
    public void downloadSupcTaxClassTemplate(HttpServletRequest req, HttpServletResponse resp) {
        try {
            ExportToExcelUtils.downloadTemplate(resp, SupcTaxClassMappingDTO.class, "SupcTaxClassMapping");
        } catch (IOException e) {
            LOG.error("Exception", e);
        } catch (ClassNotFoundException e) {
            LOG.error("Exception", e);
        }
    }

    @RequestMapping("/downloadTaxClassData")
    public void downloadTaxClassData(HttpServletRequest req, HttpServletResponse resp) {

        TaxClassCache taxClassCache = CacheManager.getInstance().getCache(TaxClassCache.class);
        List<TaxClassDTO> dtoList = new ArrayList<TaxClassDTO>();
        try {
            List<String> list = taxClassCache.getTaxClassList();
            if (list != null) {
                for (String entity : list) {
                    TaxClassDTO dto = new TaxClassDTO(entity);
                    if (dto != null) {
                        dtoList.add(dto);
                    }
                }
            }
            if (dtoList.size() <= ConfigUtils.getIntegerScalar(Property.EXCEL_SHEET_MAX_VALUE)) {
                ExportToExcelUtils.exportToExcel(resp, TaxClassDTO.class, "TaxClass", dtoList);

            } else {
                ExportToExcelUtils.exportToCsv(resp, TaxClassDTO.class, "TaxClass", dtoList, true, false);
            }
        } catch (IOException e) {
            LOG.error("Exception", e);
        } catch (ClassNotFoundException e) {
            LOG.error("Exception", e);
        } catch (Exception e) {
            LOG.error("Exception", e);
        }
    }

    @RequestMapping("/downloadSupcTaxClassTemplateAndData")
    public void downloadSupcTaxClassTemplateAndData(HttpServletRequest req, HttpServletResponse resp) {
        try {
            List<SupcTaxClassMappingDTO> dtoList = new ArrayList<SupcTaxClassMappingDTO>();

            List<SupcTaxClassMapping> entityList = taxClassDBService.getAllEnabledSupcTaxClassMapping();

            if (entityList != null) {
                for (SupcTaxClassMapping entity : entityList) {
                    SupcTaxClassMappingDTO dto = converterService.getSupcTaxClassMappingUploadDTO(entity);
                    if (dto != null) {
                        dtoList.add(dto);
                    }
                }
            }
            if (dtoList.size() <= ConfigUtils.getIntegerScalar(Property.EXCEL_SHEET_MAX_VALUE)) {
                ExportToExcelUtils.exportToExcel(resp, SupcTaxClassMappingDTO.class, "SupcTaxClassMapping", dtoList);

            } else {
                ExportToExcelUtils.exportToCsv(resp, SupcTaxClassMappingDTO.class, "SupcTaxClassMapping", dtoList, true, false);
            }
        } catch (IOException e) {
            LOG.error("Exception", e);
        } catch (ClassNotFoundException e) {
            LOG.error("Exception", e);
        }
    }

    @RequestMapping("/downloadSubcatTaxClassTemplate")
    public void downloadSubcatTaxClassTemplate(HttpServletRequest req, HttpServletResponse resp) {
        try {
            ExportToExcelUtils.downloadTemplate(resp, SubcatTaxClassMappingDTO.class, "SubcatTaxClassMapping");
        } catch (IOException e) {
            LOG.error("Exception", e);
        } catch (ClassNotFoundException e) {
            LOG.error("Exception", e);
        }
    }

    @RequestMapping("/downloadSubcatTaxClassTemplateAndData")
    public void downloadSubcatTaxClassTemplateAndData(HttpServletRequest req, HttpServletResponse resp) {
        try {
            List<SubcatTaxClassMappingDTO> dtoList = new ArrayList<SubcatTaxClassMappingDTO>();

            List<SubcatTaxClassMapping> entityList = taxClassDBService.getAllEnabledSubcatTaxClassMapping();

            if (entityList != null) {
                for (SubcatTaxClassMapping entity : entityList) {
                    SubcatTaxClassMappingDTO dto = converterService.getSubcatTaxClassMappingUploadDTO(entity);
                    if (dto != null) {
                        dtoList.add(dto);
                    }
                }
            }
            if (dtoList.size() <= ConfigUtils.getIntegerScalar(Property.EXCEL_SHEET_MAX_VALUE)) {
                ExportToExcelUtils.exportToExcel(resp, SubcatTaxClassMappingDTO.class, "SubcatTaxClassMapping", dtoList);

            } else {
                ExportToExcelUtils.exportToCsv(resp, SubcatTaxClassMappingDTO.class, "SubcatTaxClassMapping", dtoList, true, false);
            }
        } catch (IOException e) {
            LOG.error("Exception", e);
        } catch (ClassNotFoundException e) {
            LOG.error("Exception", e);
        }
    }

}
