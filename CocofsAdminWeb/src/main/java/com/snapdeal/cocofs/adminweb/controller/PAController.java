/*

 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 02-Sep-2013
 *  @author himanshum
 */
package com.snapdeal.cocofs.adminweb.controller;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.snapdeal.base.fileutils.SDFileUploadUtils;
import com.snapdeal.cocofs.common.JobValidationException;
import com.snapdeal.cocofs.commonweb.utils.ExportToExcelUtils;
import com.snapdeal.cocofs.commonweb.utils.WebContextUtils;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.entity.JobDetail;
import com.snapdeal.cocofs.excelupload.ProductAttributeUploadDTO;
import com.snapdeal.cocofs.excelupload.ProductWeightUploadDTO;
import com.snapdeal.cocofs.services.IJobSchedulerService;
import com.snapdeal.cocofs.services.IJobService;
import com.snapdeal.cocofs.utils.ExcelReader;

@Controller
@RequestMapping(PAController.URL)
public class PAController {

    public static final String   URL = "/admin/productattribute/";

    private static final Logger  LOG = LoggerFactory.getLogger(PAController.class);

    @Autowired
    private IJobService          jobService;

    @Autowired
    private IJobSchedulerService jobSchedulerService;

    @RequestMapping(method = { RequestMethod.GET }, value = "paupload")
    public String uploadPage(ModelMap map) {
        setSuperFlagIfRequired(map);
        return "admin/productattribute/paupload";
    }

    private void setSuperFlagIfRequired(ModelMap map) {
        if (WebContextUtils.getCurrentUser().hasRole("superuploader")) {
            map.addAttribute("isSuper", true);
        } else {
            map.addAttribute("isSuper", false);
        }

    }

    @RequestMapping(method = { RequestMethod.POST }, value = "processAttributeUpload")
    public String processAttributeUpload(ModelMap map, @RequestParam(required = true, value = "inputfile") MultipartFile inputFile,
            @RequestParam(required = false, value = "superupload", defaultValue = "false") boolean isSuper) {

        boolean fileupload = ValidateFileContent(map, inputFile);

        if (fileupload == true) {
            try {
                String userEmail = WebContextUtils.getCurrentUserEmail();
                LOG.info("USER " + userEmail + " Super upload : " + isSuper);
                String actionCode = "AttributeUpdate";
                if (isSuper && (WebContextUtils.getCurrentUser().hasRole("superuploader") || (WebContextUtils.getCurrentUser().hasRole("tech")))) {
                    LOG.info("USER " + userEmail + " Super upload : " + isSuper + " verified ");
                    actionCode = "SuperAttributeUpdate";
                }
                JobDetail jd = jobSchedulerService.saveJob(inputFile, actionCode, userEmail);
                String actionCodeMSG = "";
                if (jd.getAction() != null) {
                    actionCodeMSG = ", Action code:" + jd.getAction().getCode();
                }
                map.addAttribute("message",
                        "Attribute upload done. <br>Job code:" + jd.getFileCode() + actionCodeMSG + ", saved at  " + jd.getFilePath() + File.separator + jd.getFileName());
            } catch (IOException e) {
                LOG.error("Attribute upload could not be done ", e);
                map.addAttribute("message", "Attribute upload could not be done, " + e.getMessage());
            } catch (JobValidationException e) {
                LOG.error("Attribute upload could not be done ", e);
                map.addAttribute("message", "Attribute upload could not be done, " + e.getMessage());
                map.addAttribute("validationResponse", e.getResponse());
            } catch (Exception e) {
                LOG.error("Attribute upload could not be done ", e);
                map.addAttribute("message", "Attribute upload could not be done, " + e.getMessage());
            }

        }
        setSuperFlagIfRequired(map);
        return "admin/productattribute/paupload";
    }

    @RequestMapping(method = { RequestMethod.POST }, value = "processWeightUpload")
    public String processWeightUpload(ModelMap map, @RequestParam(required = true, value = "inputfile") MultipartFile inputFile) throws ClassNotFoundException {

        try {
            String userEmail = WebContextUtils.getCurrentUserEmail();
            JobDetail jd = jobSchedulerService.saveJob(inputFile, "WeightUpdate", userEmail);
            map.addAttribute("message", "Weight Upload Done, saved at  " + jd.getFilePath() + File.separator + jd.getFileName());
        } catch (IOException e) {
            LOG.info("Weight Upload Could not be done ", e);
            map.addAttribute("message", "Weight Upload Could not be done, " + e.getMessage());
        } catch (JobValidationException e) {
            LOG.error("Attribute Upload Could not be done ", e);
            map.addAttribute("message", "Attribute Upload Could not be done, " + e.getMessage());
        } catch (Exception e) {
            LOG.error("Attribute Upload Could not be done ", e);
            map.addAttribute("message", "Attribute Upload Could not be done, " + e.getMessage());
        }
        setSuperFlagIfRequired(map);
        return "admin/productattribute/paupload";
    }

    @RequestMapping(method = { RequestMethod.GET }, value = "downloadProductAttributeTemplate")
    public void downloadProductAttributeTemplate(HttpServletRequest req, HttpServletResponse resp) {
        try {
            ExportToExcelUtils.downloadTemplate(resp, ProductAttributeUploadDTO.class, "ProductAttributeUploadTemplate");
        } catch (IOException e) {
            LOG.info("Errr while exporting template for attributes ", e);
        } catch (ClassNotFoundException e) {
            LOG.info("Errr while exporting template for attributes ", e);
        }
    }

    @RequestMapping(method = { RequestMethod.GET }, value = "downloadProductWeightTemplate")
    public void downloadProductWeightTemplate(HttpServletRequest req, HttpServletResponse resp) {
        try {
            ExportToExcelUtils.downloadTemplate(resp, ProductWeightUploadDTO.class, "ProductWeightUploadTemplate");
        } catch (IOException e) {
            LOG.info("Errr while exporting template for weight", e);
        } catch (ClassNotFoundException e) {
            LOG.info("Errr while exporting template for weight", e);
        }
    }

    public boolean ValidateFileContent(ModelMap map, MultipartFile inputFile) {
        boolean fileupload = true;
        String uploadFilePath = ConfigUtils.getStringScalar(Property.TEMP_DIRECTORY) + inputFile.getOriginalFilename();
        try {
            SDFileUploadUtils.upload(inputFile.getInputStream(), inputFile.getContentType(), uploadFilePath);
            LOG.error("FILE upload at " + uploadFilePath);
            List<ProductAttributeUploadDTO> proAttributeList = ExcelReader.readFile(uploadFilePath, ProductAttributeUploadDTO.class, true, 0, false, true);
            if (proAttributeList != null && proAttributeList.size() > 0) {
                LOG.info("FILE have content to read.");
                int count = 0;
                for (ProductAttributeUploadDTO pAL : proAttributeList) {
                    count++;

                    if (pAL.getSupc() == null) {
                        fileupload = false;
                        LOG.error("FILE does not contain a valid SUPC at row " + count + ".");
                        map.addAttribute("message", "FILE does not contain a valid SUPC at row " + count + ".");
                        break;
                    }

                    if (pAL.getBreadth() != null) {
                        if (pAL.getBreadth() <= 0) {
                            fileupload = false;
                            LOG.error("FILE contain data below 1 for column Breadth in row " + count + " is not valid.");
                            map.addAttribute("message", "FILE contain data below 1 for column Breadth in row " + count + " is not valid.");
                            break;
                        }
                    }
                    if (pAL.getHeight() != null) {
                        if (pAL.getHeight() <= 0) {
                            fileupload = false;
                            LOG.error("FILE contain data below 1 for column Height in row " + count + " is not valid.");
                            map.addAttribute("message", "FILE contain data below 1 for column Height in row " + count + " is not valid.");
                            break;
                        }
                    }

                    if (pAL.getLength() != null) {
                        if (pAL.getLength() <= 0) {
                            fileupload = false;
                            LOG.error("FILE contain data below 1 for column Length in row " + count + " is not valid.");
                            map.addAttribute("message", "FILE contain data below 1 for column Length in row " + count + " is not valid.");
                            break;
                        }
                    }
                    
                    if (pAL.getPrimaryLength() != null) {
                        if (pAL.getPrimaryLength() <= 0) {
                            fileupload = false;
                            String msg = "FILE contain data below 1 for column PrimaryLength in row " + count + " is not valid.";
                            LOG.error(msg);
                            map.addAttribute("message", msg);
                            break;
                        }
                    }

                    if (pAL.getPrimaryBreadth() != null) {
                        if (pAL.getPrimaryBreadth() <= 0) {
                            fileupload = false;
                            LOG.error("FILE contain data below 1 for column PrimaryBreadth in row " + count + " is not valid.");
                            map.addAttribute("message", "FILE contain data below 1 for column PrimaryBreadth in row " + count + " is not valid.");
                            break;
                        }
                    }
                    if (pAL.getPrimaryHeight() != null) {
                        if (pAL.getPrimaryHeight() <= 0) {
                            fileupload = false;
                            LOG.error("FILE contain data below 1 for column PrimaryHeight in row " + count + " is not valid.");
                            map.addAttribute("message", "FILE contain data below 1 for column PrimaryHeight in row " + count + " is not valid.");
                            break;
                        }
                    }
                 
                    if (pAL.getWeight() != null) {
                        if (pAL.getWeight() <= 0) {
                            fileupload = false;
                            String msg = "FILE contain data below 1 for column Weight in row " + count + " is not valid.";
                            LOG.error(msg);
                            map.addAttribute("message", msg);
                            break;
                        }
                    }
                    if (pAL.getProductParts() != null) {
                        if (pAL.getProductParts() <= 0) {
                            fileupload = false;
                            LOG.error("FILE contain data below 1 for column Product Part in row " + count + " is not valid.");
                            map.addAttribute("message", "FILE contain data below 1 for column Product Part in row " + count + " is not valid.");
                            break;
                        } else if (pAL.getProductParts() > ConfigUtils.getIntegerScalar(Property.MAX_PRODUCT_PARTS)) {
                            fileupload = false;
                            LOG.error("FILE contain data greater than max allowed size for column Product Part in row " + count + " is not valid.");
                            map.addAttribute("message", "FILE contain data greater than max allowed size for column Product Part in row " + count + " is not valid.");
                            break;
                        }
                    }
                }
            } else {
                fileupload = false;
                LOG.error("FILE uploaded is empty or contain invalid data.");
                map.addAttribute("message", "FILE uploaded is empty or contain invalid data.");
            }
        } catch (Exception e) {
            fileupload = false;
            LOG.error("Error in reading file {}", inputFile, e);
            map.addAttribute("message", "Error in reading file, please check the content and size" + e.getMessage());
        }
        return fileupload;
    }

}
