
package com.snapdeal.cocofs.adminweb.controller;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.snapdeal.base.fileutils.SDFileUploadUtils;
import com.snapdeal.cocofs.common.JobValidationException;
import com.snapdeal.cocofs.commonweb.utils.ExportToExcelUtils;
import com.snapdeal.cocofs.commonweb.utils.WebContextUtils;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.entity.JobDetail;
import com.snapdeal.cocofs.enums.JobActionCode;
import com.snapdeal.cocofs.excelupload.PincodeUploadDTO;
import com.snapdeal.cocofs.services.IJobSchedulerService;
import com.snapdeal.cocofs.utils.ExcelReader;


@Controller
@RequestMapping("/admin/pincodeZoneMapping")
public class PinCodeZoneMappingController {


    private static final Logger  LOG = LoggerFactory.getLogger(FMMappingController.class);

    @Autowired
    private IJobSchedulerService jobSchedulerService;
    
    
    

    @RequestMapping("")
    public String uploadPage(ModelMap map) {
        Property p = Property.SHIP_NEAR;
        if (!ConfigUtils.getBooleanScalar(p)) {
            map.addAttribute("message", "The page you were looking is not live at this moment.");
            return "admin/pzMapping/pageNotLive";
        }
        return "admin/pzMapping/pincodeUpload";
    }

    @RequestMapping("/upload")
    public String upload(ModelMap map, @RequestParam(value = "inputfile") MultipartFile inputFile) {
        boolean fileupload=validateUploadedFile(map, inputFile);
        if (fileupload) {
            try {
                String userEmail = WebContextUtils.getCurrentUserEmail();

                JobDetail jd = jobSchedulerService.saveJob(inputFile, JobActionCode.P_Z_UPDATE.getCode(), userEmail);

                String actionCodeMSG = "";
                if (jd.getAction() != null) {
                    actionCodeMSG = ", Action code:" + jd.getAction().getCode();
                }
                map.addAttribute("message",
                        "Pincode upload done. <br>Job code:" + jd.getFileCode() + actionCodeMSG + ", saved at  " + jd.getFilePath() + File.separator + jd.getFileName());
            } catch (IOException e) {
                LOG.info("Pincode Upload Could not be done. ", e);
                map.addAttribute("message", "Pincode upload could not be done.");
            } catch (JobValidationException e) {
                LOG.error("Pincode Upload Could not be done ", e);
                map.addAttribute("message", "Pincode upload could not be done. " + e.getMessage());
                map.addAttribute("validationResponse", e.getResponse());
            } catch (Exception e) {
                LOG.info("Pincode upload could not be done. ", e);
                map.addAttribute("message", "Pincode upload could not be done, something bad happened.");
            }
        }
        return "admin/pzMapping/pincodeUpload";
    }

    @RequestMapping("/downloadTemplate")
    public void downloadSellerSupcFmMappingTemplate(HttpServletRequest req, HttpServletResponse resp) {
        try {          
            ExportToExcelUtils.downloadTemplate(resp, PincodeUploadDTO.class, "Pincodes");
        } catch (IOException e) {
            LOG.error("Exception", e);
        } catch (ClassNotFoundException e) {
            LOG.error("Exception", e);
        }
    }
    
    public boolean validateUploadedFile(ModelMap map,MultipartFile inputFile){
        boolean fileupload=true;
        String uploadFilePath = ConfigUtils.getStringScalar(Property.TEMP_DIRECTORY) + inputFile.getOriginalFilename();
        try {
            SDFileUploadUtils.upload(inputFile.getInputStream(), inputFile.getContentType(), uploadFilePath);
            LOG.error("FILE upload at "+uploadFilePath);
            List<PincodeUploadDTO> dtoList = ExcelReader.readFile(uploadFilePath, PincodeUploadDTO.class,true, 0, false,true);
            if (dtoList != null && dtoList.size() > 0) {
                LOG.info("FILE have content to read.");
                int fileSizeLimit = ConfigUtils.getIntegerScalar(Property.PINCODE_UPLOAD_LIMIT);
                if(dtoList.size() > fileSizeLimit){
                    fileupload=false;
                    LOG.error("File uploaded  exceeded pincode upload limit, you can upload a maximum of  " + fileSizeLimit + " in single upload");
                    map.addAttribute("message", "File uploaded  exceeded pincode upload limit, you can upload a maximum of " + fileSizeLimit + " in single upload");
                }
            }
            else{
                fileupload=false;
                LOG.error("FILE uploaded is empty or contain invalid data.");
                map.addAttribute("message", "FILE uploaded is empty or contain invalid data.");
            }
        }catch (Exception e) {
            fileupload=false;
            LOG.error("Error in reading file {}", inputFile, e);
            map.addAttribute("message", "Error in reading file, please check the content and size" + e.getMessage());
        }
        return fileupload;
    }

}
