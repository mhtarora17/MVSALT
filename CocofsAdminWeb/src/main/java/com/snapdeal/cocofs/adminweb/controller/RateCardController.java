/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 16, 2013
 *  @author himanshu
 
package com.snapdeal.cocofs.adminweb.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.snapdeal.base.cache.CacheManager;
import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.base.validation.ValidationError;
import com.snapdeal.cocofs.cache.ProductCategoryCache;
import com.snapdeal.cocofs.common.FulfillmentModelCost;
import com.snapdeal.cocofs.common.RateCardRowDisplayDTO;
import com.snapdeal.cocofs.common.RateCardUpdateDTO;
import com.snapdeal.cocofs.common.RateCardUpdateRequest;
import com.snapdeal.cocofs.commonweb.response.SystemResponse;
import com.snapdeal.cocofs.commonweb.utils.WebContextUtils;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.dto.ProductCategoryDTO;
import com.snapdeal.cocofs.external.service.IOPSExternalService;
import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.external.service.exception.ExternalDataUpdateFailedException;
import com.snapdeal.cocofs.external.service.factory.IExternalServiceFactory;
import com.snapdeal.cocofs.sro.DeliveryType;
import com.snapdeal.ops.base.api.getRateCardByCategory.FulfillmentModelCostingByCategoryConfigurationSRO;
import com.snapdeal.ops.base.api.getRateCardByCategory.GetRateCardByCategoryRequest;
import com.snapdeal.ops.base.api.getRateCardByCategory.GetRateCardByCategoryResponse;
import com.snapdeal.ops.base.api.getRateCardByCategory.WeightCostingByCategorySRO;
import com.snapdeal.ops.base.api.model.CostHeadRCCode;
import com.snapdeal.ops.base.api.updateRateCardByCategory.UpdateRateCardByCategoryIdSRO;
import com.snapdeal.ops.base.api.updateRateCardByCategory.UpdateRateCardByCategoryRequest;
import com.snapdeal.ops.base.api.updateRateCardByCategory.UpdateRateCardByCategoryResponse;
import com.snapdeal.ops.base.exception.OPSException;

@Controller
@RequestMapping(RateCardController.URL)
public class RateCardController {

    private static final Logger                                    LOG            = LoggerFactory.getLogger(RateCardController.class);
    private static final Gson                                      GSON_CONVERTOR = new Gson();
    public static final String                                     URL            = "/admin/ratecard/";
    private static final Comparator<? super RateCardRowDisplayDTO> ROW_COMPARATOR = new Comparator<RateCardRowDisplayDTO>() {

                                                                                      @Override
                                                                                      public int compare(RateCardRowDisplayDTO o1, RateCardRowDisplayDTO o2) {
                                                                                          if (!o1.getCostHead().equals(o2.getCostHead())) {
                                                                                              return o1.getCostHead().compareTo(o2.getCostHead());
                                                                                          }
                                                                                          if (!o1.getDeliveryType().get(0).equals(o2.getDeliveryType().get(0))) {
                                                                                              return o1.getDeliveryType().get(0).compareTo(o2.getDeliveryType().get(0));
                                                                                          }
                                                                                          return o1.getSlabIndex() - o2.getSlabIndex();
                                                                                      }
                                                                                  };

    @Autowired
    private IExternalServiceFactory                                factory;

    @RequestMapping("")
    public String defaultView() {
        return "redirect:" + URL + "view";
    }

    @RequestMapping("/view")
    public String showRateCard(ModelMap modelMap) {
        IOPSExternalService opsService = factory.getOPSExternalService();

        try {
            GetRateCardByCategoryRequest request = new GetRateCardByCategoryRequest();
            GetRateCardByCategoryResponse resp = opsService.getRateCardByCategory(request);
            if (!resp.isSuccessful()) {
                String msg = "OPS didn't give rate card info " + resp.getMessage();
                LOG.info("Possible validation errors {} ", resp.getValidationErrors());
                LOG.info(msg);
            } else {
                populateModelMapWithOPSdata(resp, modelMap);
            }
        } catch (ExternalDataNotFoundException e) {
            String message = "Unable to get RateCard Info from OPS ";
            LOG.info(message, e);
            modelMap.addAttribute("opsError", message);
        }
        return "/admin/ratecard/view";
    }

    @RequestMapping(value = "/get", method = RequestMethod.POST)
    @ResponseBody
    public SystemResponse getRateCard() {
        IOPSExternalService opsService = factory.getOPSExternalService();
        Map<String, List<RateCardRowDisplayDTO>> categoryRowMap = new HashMap<String, List<RateCardRowDisplayDTO>>();

        try {
            GetRateCardByCategoryRequest request = new GetRateCardByCategoryRequest();
            GetRateCardByCategoryResponse resp = opsService.getRateCardByCategory(request);
            if (!resp.isSuccessful()) {
                String msg = "OPS didn't give rate card info " + resp.getMessage();
                LOG.info("Possible validation errors {} ", resp.getValidationErrors());
                LOG.info(msg);
            } else {

                for (Integer categoryId : resp.getConfigSROMap().keySet()) {

                    List<RateCardRowDisplayDTO> rowList = initializeRows(resp.getConfigSROMap().get(categoryId));
                    LOG.debug("Get Rate Card Response " + GSON_CONVERTOR.toJson(resp));
                    for (FulfillmentModelCostingByCategoryConfigurationSRO f : resp.getConfigSROMap().get(categoryId)) {
                        updateRowWithRateInfo(rowList, f);

                    }
                    LOG.info("RowList for categoryId:" + categoryId + " RateCard " + rowList);

                    String catId = categoryId.toString(); //chaning it to String as in JSP, Integer key of Map does not work 
                    categoryRowMap.put(catId, rowList);
                }
            }
        } catch (ExternalDataNotFoundException e) {
            String message = "Unable to get RateCard Info from OPS ";
            LOG.info(message, e);
        }
        SystemResponse resp = new SystemResponse();
        resp.setStatus(SystemResponse.ResponseStatus.SUCCESS.name());
        resp.setMessage("successful");

        Map<String, Object> items = new HashMap<String, Object>();
        items.put("ruleList", categoryRowMap);

        resp.setItems(items);
        return resp;

    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public SystemResponse updateRateCard(@RequestBody RateCardUpdateRequest req) {
        LOG.info("Got Rate Card Update Request {} ", req);
        String user = WebContextUtils.getCurrentUserEmail();
        SystemResponse resp = new SystemResponse();

        IOPSExternalService opsService = factory.getOPSExternalService();

        try {
            UpdateRateCardByCategoryRequest request = getOPSRateCardUpdateRequest(req, user);
            LOG.debug("Form Request For Update " + GSON_CONVERTOR.toJson(req));
            UpdateRateCardByCategoryResponse opsResp = opsService.updateRateCardByCategory(request);
            if (opsResp.isSuccessful()) {
                resp.setStatus(SystemResponse.ResponseStatus.SUCCESS.name());
                LOG.info("Rate Card updated successfuly " + req.toString());
                return resp;
            } else {
                resp.setStatus(SystemResponse.ResponseStatus.FAIL.name());
                for (ValidationError err : opsResp.getValidationErrors()) {
                    LOG.info("Validation error while updating rate card " + err.getMessage());
                }

                resp.setMessage(opsResp.getMessage());
                return resp;
            }
        } catch (ExternalDataUpdateFailedException e) {
            String message = "Unable to update RateCard Info to OPS ";
            LOG.info(message, e);
            resp = new SystemResponse();
            resp.setStatus(SystemResponse.ResponseStatus.FAIL.name());
            resp.setMessage(message);
            return resp;
        } catch (OPSException e) {
            String message = "Unable to update RateCard Info to OPS ";
            LOG.info(message, e);
            resp = new SystemResponse();
            resp.setStatus(SystemResponse.ResponseStatus.FAIL.name());
            resp.setMessage(message);
            return resp;
        }
    }

    private UpdateRateCardByCategoryRequest getOPSRateCardUpdateRequest(RateCardUpdateRequest req, String user) throws OPSException {
        UpdateRateCardByCategoryRequest request = new UpdateRateCardByCategoryRequest();
        request.setUpdatedBy(user);
        Set<UpdateRateCardByCategoryIdSRO> updateRateCardSROSet = new HashSet<UpdateRateCardByCategoryIdSRO>();
        for (RateCardUpdateDTO r : req.getRows()) {
            String costHead = r.getCostHead();
            double weight = r.getWeight();
            List<String> dTypes = r.getDeliveryTypes();
            List<FulfillmentModelCost> fmCosts = r.getFmCosts();
            int slabIndex = r.getSlabIndex();
            for (String dType : dTypes) {
                if (StringUtils.isEmpty(dType)) {
                    continue;
                }

                for (FulfillmentModelCost c : fmCosts) {

                    double cost = c.getCost();
                    String fulfillmentModel = c.getFulfillmentModel();
                    UpdateRateCardByCategoryIdSRO s = new UpdateRateCardByCategoryIdSRO();
                    s.setCocofsCategoryId(req.getCategoryId());
                    s.setCostHeadRCCode(CostHeadRCCode.valueOf(costHead));
                    s.setCostPerSlab((float) cost);
                    s.setDeliveryType(dType);
                    s.setFullFulfillmentModel(fulfillmentModel);
                    s.setSlabIndex(slabIndex);
                    s.setWeightSlab(weight);
                    s.setCount(1);
                    if (isSubsequentSlab(slabIndex, s.getCostHeadRCCode())) {
                        // when we call this function , ops internally set count to -1 or 1 based on  its value
                        s.setSubsequent(true);

                    } else {
                        s.setSubsequent(false);
                    }
                    updateRateCardSROSet.add(s);
                }
            }
        }
        request.setUpdateRateCardSROs(updateRateCardSROSet);
        LOG.info(GSON_CONVERTOR.toJson(request));
        return request;
    }

    private boolean isSubsequentSlab(int slabIndex, CostHeadRCCode costHeadCode) {
        if (costHeadCode.equals(CostHeadRCCode.LOGISTIC_COST) && (slabIndex == 0)) {
            return false;
        } else if (costHeadCode.equals(CostHeadRCCode.HANDLING_AND_PACKAGING) && (slabIndex != 3)) {
            return false;
        } else {
            return true;
        }
    }

    private void populateModelMapWithOPSdata(GetRateCardByCategoryResponse resp, ModelMap modelMap) {
        // prepare all fullfillment models
        List<String> fmList = new ArrayList<String>();
        Set<String> allFMs = new HashSet<String>();

        for (FulfillmentModelCostingByCategoryConfigurationSRO f : resp.getConfigSROMap().values().iterator().next()) {
            allFMs.add(f.getFulfillmentModel());
        }
        fmList.addAll(allFMs);
        Collections.sort(fmList);

        // prepare all categories
        Set<Integer> allCategories = new HashSet<Integer>();
        for (Integer categoryId : resp.getConfigSROMap().keySet()) {
            // skipping -1 category, is added seperately in productCategoryDTOs
            if (categoryId != -1) {
                allCategories.add(categoryId);
            }
        }

        List<ProductCategoryDTO> productCategoryDTOs = getProductCategories(allCategories, modelMap);
        modelMap.addAttribute("fmList", fmList);
        modelMap.addAttribute("categoryList", productCategoryDTOs);

    }

    private List<ProductCategoryDTO> getProductCategories(Set<Integer> allCategories, ModelMap modelMap) {
        List<ProductCategoryDTO> productCategoryDTOs = new ArrayList<ProductCategoryDTO>();
        List<Integer> categoryList = new ArrayList<Integer>();
        categoryList.addAll(allCategories);

        try {
            Map<String, ProductCategoryDTO> map = CacheManager.getInstance().getCache(ProductCategoryCache.class).getAllMappings();

            ProductCategoryDTO allCaterogiesHead = new ProductCategoryDTO();
            allCaterogiesHead.setId(-1);
            allCaterogiesHead.setName("All Categories");
            productCategoryDTOs.add(allCaterogiesHead);
            for (Entry<String, ProductCategoryDTO> o : map.entrySet()) {
                ProductCategoryDTO item = o.getValue();
                if (allCategories.contains(item.getId())) {
                    productCategoryDTOs.add(item);
                }
            }

        } catch (Exception e) {
            String message = "Unable to get Category Info from Cache ";
            LOG.info(message, e);
            modelMap.addAttribute("opsError", message);
        }
        return productCategoryDTOs;

    }

    private void updateRowWithRateInfo(List<RateCardRowDisplayDTO> rowList, FulfillmentModelCostingByCategoryConfigurationSRO f) {
        String costHeadName = f.getCostHeadRCCode().name();
        String deliveryType = f.getDeliveryType();
        for (WeightCostingByCategorySRO w : f.getWeightCostingSROList()) {
            double weight = w.getWeightSlab();
            float c = w.getCostPerSlab();
            double cost = Double.valueOf(String.valueOf(c));
            int idx = w.getSlabIndex();
            RateCardRowDisplayDTO dto = findRow(rowList, idx, costHeadName, deliveryType);
            dto.setSlabWeight(weight);
            dto.getFmCost().put(f.getFulfillmentModel(), cost);
            dto.getFmEditable().put(f.getFulfillmentModel(), true);

        }

    }

    private RateCardRowDisplayDTO findRow(List<RateCardRowDisplayDTO> rowList, int idx, String costHeadName, String deliveryType) {
        for (RateCardRowDisplayDTO dto : rowList) {
            if ((idx == dto.getSlabIndex()) && (costHeadName.equals(dto.getCostHead()) && (dto.getDeliveryType().contains(deliveryType)))) {
                return dto;
            }
        }
        return null;
    }

    private List<RateCardRowDisplayDTO> initializeRows(List<FulfillmentModelCostingByCategoryConfigurationSRO> list) {
        List<RateCardRowDisplayDTO> rowList = new ArrayList<RateCardRowDisplayDTO>();
        Map<String, Integer> costHeadToTotalSlabCount = new HashMap<String, Integer>();
        for (FulfillmentModelCostingByCategoryConfigurationSRO f : list) {
            String costHead = f.getCostHeadRCCode().name();
            int slabCount = f.getWeightCostingSROList().size();
            costHeadToTotalSlabCount.put(costHead, slabCount);
        }
        List<String> costHeadWithSplitDelivery = ConfigUtils.getStringList(Property.COSTHEAD_WITH_SPLIT_DELIVERY);
        if (null == costHeadWithSplitDelivery) {
            costHeadWithSplitDelivery = new ArrayList<String>();
        }
        for (Entry<String, Integer> e : costHeadToTotalSlabCount.entrySet()) {
            String costHead = e.getKey();
            int slabCount = e.getValue();
            if (costHeadWithSplitDelivery.contains(costHead)) {
                List<String> dTypes = new ArrayList<String>();
                dTypes.add(DeliveryType.EXPRESS.name());
                rowList.addAll(getRowsForCostHeadAndDeliverTypes(costHead, dTypes, slabCount));
                dTypes.clear();
                dTypes.add(DeliveryType.NORMAL.name());
                rowList.addAll(getRowsForCostHeadAndDeliverTypes(costHead, dTypes, slabCount));
            } else {
                List<String> dTypes = new ArrayList<String>();
                dTypes.add(DeliveryType.EXPRESS.name());
                dTypes.add(DeliveryType.NORMAL.name());
                rowList.addAll(getRowsForCostHeadAndDeliverTypes(costHead, dTypes, slabCount));

            }

        }
        Collections.sort(rowList, ROW_COMPARATOR);
        return rowList;
    }

    private Collection<? extends RateCardRowDisplayDTO> getRowsForCostHeadAndDeliverTypes(String costHead, List<String> dTypes, int slabCount) {
        List<RateCardRowDisplayDTO> rowList = new ArrayList<RateCardRowDisplayDTO>();
        for (int i = 0; i < slabCount; i++) {
            rowList.add(getRowForCostHeadSlabIndexWithDeliveryTypes(costHead, dTypes, i, slabCount));
        }
        return rowList;
    }

    private RateCardRowDisplayDTO getRowForCostHeadSlabIndexWithDeliveryTypes(String costHead, List<String> dTypes, int slabIndex, int slabCount) {
        RateCardRowDisplayDTO dto = new RateCardRowDisplayDTO();
        dto.setCostHead(costHead);
        for (String dt : dTypes) {
            dto.getDeliveryType().add(dt);
        }

        dto.setSlabIndex(slabIndex);
        StringBuilder sb = new StringBuilder(costHead);
        StringBuilder suffix = new StringBuilder("");
        if (dTypes.size() == 1) {
            suffix.append(" " + dTypes.get(0) + " Delivery");
        }

        if (slabCount > 1) {
             If there are more than one slabs for given cost head, we need to append
             * if its first , next or subsequent  slab
             
            if (suffix.length() > 0) {
                // append - if we already have delivery type in suffix
                // to make sure suffix is of format 'deliveryType - slabName'
                // or just 'deliveryType' or just 'slabName'
                suffix.append(" -");
            }

            if (slabIndex == 0) {

                suffix.append(" First Slab");
            } else {
                 If its not first slab , then if its last one its subsequent slab 
                if (slabIndex == (slabCount - 1)) {
                    suffix.append(" Subsequent Slab");
                } else {
                    suffix.append(" Next Slab");
                }

            }
        }
        if (suffix.length() > 0) {
            sb.append(" (");
            sb.append(suffix.toString());
            sb.append(" )");
        }
        dto.setName(sb.toString());
        return dto;
    }
}
*/
