/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 16, 2013
 *  @author himanshu
 */
package com.snapdeal.cocofs.adminweb.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.snapdeal.base.cache.CacheManager;
import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.cocofs.cache.ProductCategoryCache;
import com.snapdeal.cocofs.common.CocofsRuleBlock;
import com.snapdeal.cocofs.common.RuleCreationDTO;
import com.snapdeal.cocofs.common.RuleEditDTO;
import com.snapdeal.cocofs.common.RuleUpdateResponse;
import com.snapdeal.cocofs.commonweb.response.SystemResponse;
import com.snapdeal.cocofs.commonweb.utils.WebContextUtils;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.external.service.factory.IExternalServiceFactory;
import com.snapdeal.cocofs.giftwrap.converter.GiftWrapRuleCreateFormConverter;
import com.snapdeal.cocofs.giftwrap.dto.GiftWrapCategoryRuleCreateForm;
import com.snapdeal.cocofs.giftwrap.dto.GiftWrapCategoryRuleEditForm;
import com.snapdeal.cocofs.giftwrap.dto.GiftWrapRuleUpdateDTO;
import com.snapdeal.cocofs.mongo.model.CocofsRuleUnit.CocofsRuleUnitType;
import com.snapdeal.cocofs.services.IGiftWrapRuleWrapperService;
import com.snapdeal.cocofs.services.IRuleManagement;
import com.snapdeal.rule.cache.BlocksCache;
import com.snapdeal.rule.engine.entity.Block;
import com.snapdeal.rule.engine.entity.Rule;
import com.snapdeal.rule.engine.services.IRulesService;

@Controller
@RequestMapping(RulesController.URL)
public class RulesController {

    private static final Logger         LOG = LoggerFactory.getLogger(RulesController.class);
    public static final String          URL = "/admin/rules/";

    @Autowired
    private IExternalServiceFactory     factory;

    @Autowired
    private IRuleManagement             ruleManager;

    @Autowired
    private IGiftWrapRuleWrapperService giftWrapRuleWrapperService;

    @Autowired
    private IRulesService               ruleService;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }

    @RequestMapping("")
    public String defaultView() {
        return "redirect:" + URL + "view";
    }

    @RequestMapping("/view")
    public String showRules(ModelMap modelMap) {
        modelMap.addAttribute("categoryUrls", getChildCategories());
        modelMap.addAttribute("ruletype", CocofsRuleBlock.PACKAGING_TYPE.getName());
        return "/admin/rules/view";
    }

    @RequestMapping("/giftwrap")
    public String showGiftwrapRules(ModelMap modelMap) {
        modelMap.addAttribute("categoryUrls", getChildCategories());
        modelMap.addAttribute("allDangerousGoodsType", ConfigUtils.getStringList(Property.DANGEROUS_GOODS_TYPE));
        return "/admin/rules/createGiftwrapCategoryRule";
    }

    @RequestMapping(value = "findRuleForCategoryAndType", produces = "application/json", method = { RequestMethod.POST })
    @ResponseBody
    public SystemResponse findRuleForCategoryAndType(@RequestParam(value = "ruletype", required = true) String ruletype,
            @RequestParam(value = "categoryurl", required = true) String categoryUrl) {
        List<Rule> rules = ruleManager.getEnabledRulesForCategoryAndType(categoryUrl, ruletype);
        if ((null == rules) || (rules.isEmpty())) {
            SystemResponse resp = new SystemResponse();
            resp.setStatus(SystemResponse.ResponseStatus.FAIL.name());
            resp.setMessage("No enabled rule found for category " + categoryUrl + " type : " + ruletype);

            return resp;
        }
        List<RuleEditDTO> ruleList = new ArrayList<RuleEditDTO>();
        for (Rule r : rules) {
            RuleEditDTO dto = ruleManager.getRuleEditDTOFromRule(r);
            ruleList.add(dto);
        }

        SystemResponse resp = new SystemResponse();
        resp.setStatus(SystemResponse.ResponseStatus.SUCCESS.name());
        Map<String, Object> items = new HashMap<String, Object>();
        items.put("ruleList", ruleList);

        resp.setItems(items);
        return resp;
    }

    @RequestMapping(value = "findGiftWrapRuleForCategory", produces = "application/json", method = { RequestMethod.POST })
    @ResponseBody
    public SystemResponse findGiftWrapRuleForCategory(@RequestParam(value = "categoryurl", required = true) String categoryUrl) {
        String ruleCode = GiftWrapRuleCreateFormConverter.getGiftWrapRuleCodeForCategoryAndType(categoryUrl, CocofsRuleUnitType.GIFTWRAP_CATEGORY.getCode());
        List<Rule> rules = giftWrapRuleWrapperService.getGiftWrapRulesByCode(ruleCode);
        if ((null == rules) || (rules.isEmpty())) {
            SystemResponse resp = new SystemResponse();
            resp.setStatus(SystemResponse.ResponseStatus.FAIL.name());
            resp.setMessage("No enabled rule found for category " + categoryUrl + " type : " + CocofsRuleUnitType.GIFTWRAP_CATEGORY.getCode());

            return resp;
        }
        List<GiftWrapCategoryRuleEditForm> ruleList = new ArrayList<GiftWrapCategoryRuleEditForm>();
        for (Rule r : rules) {
            GiftWrapCategoryRuleEditForm dto = giftWrapRuleWrapperService.getGiftWrapRuleEditDTOFromRule(r);
            ruleList.add(dto);
        }

        SystemResponse resp = new SystemResponse();
        resp.setStatus(SystemResponse.ResponseStatus.SUCCESS.name());
        Map<String, Object> items = new HashMap<String, Object>();
        items.put("ruleList", ruleList);

        resp.setItems(items);
        return resp;
    }

    @RequestMapping(value = "createRule", produces = "application/json", method = { RequestMethod.POST })
    @ResponseBody
    public SystemResponse createRule(@RequestParam(value = "ruletype", required = true) String ruleType, @RequestParam(value = "categoryurl", required = true) String categoryUrl,
            @RequestParam(value = "fragile", required = false) String fragile, @RequestParam(value = "hazmat", required = false) String hazmat,
            @RequestParam(value = "liquid", required = false) String liquid, @RequestParam(value = "weight", required = false) String weight,
            @RequestParam(value = "volweight", required = false) String volweight, @RequestParam(value = "enddate", required = false) String enddate,
            @RequestParam(value = "startdate", required = true) String startdate, @RequestParam(value = "user", required = false) String user) {
        if (StringUtils.isEmpty(user)) {
            user = WebContextUtils.getCurrentUserEmail();
        }

        RuleCreationDTO creationDTO = ruleManager.getRuleCreationDTO(ruleType, categoryUrl, fragile, hazmat, liquid, weight, volweight, enddate, startdate, user);

        try {
            SystemResponse resp = new SystemResponse();

            Rule r = ruleManager.createRule(creationDTO);
            resp.setStatus(SystemResponse.ResponseStatus.SUCCESS.name());
            LOG.info("Rule created " + creationDTO.toString());

            Map<String, Object> items = new HashMap<String, Object>();
            items.put("createdRule", creationDTO);

            resp.setItems(items);
            return resp;
        } catch (Exception e) {
            SystemResponse resp = new SystemResponse();
            resp.setStatus(SystemResponse.ResponseStatus.FAIL.name());
            resp.setMessage(" Rule creation failded for category " + categoryUrl + " and type " + ruleType + " reason  : " + e.getMessage());
            LOG.info("Rule creation failed " + creationDTO.toString(), e);
            return resp;
        }

    }

    @RequestMapping(value = "giftwrap/category/createRule", produces = "application/json", method = { RequestMethod.POST })
    @ResponseBody
    public SystemResponse createGiftWrapCategoryRule(@RequestParam(value = "previousRuleCode", required = false) String previousRuleCode,
            @RequestParam(value = "ruleName", required = false) String ruleName, @RequestParam(value = "categoryurl", required = true) String categoryUrl,
            @RequestParam(value = "fragile", required = false) String fragile, @RequestParam(value = "weight", required = false) String weight,
            @RequestParam(value = "volume", required = false) String volume, @RequestParam(value = "price", required = false) String price,
            @RequestParam(value = "weightTo", required = false) String weightTo, @RequestParam(value = "volumeTo", required = false) String volumeTo,
            @RequestParam(value = "priceTo", required = false) String priceTo, @RequestParam(value = "weightOperator", required = false) String weightOperator,
            @RequestParam(value = "volumeOperator", required = false) String volumeOperator, @RequestParam(value = "priceOperator", required = false) String priceOperator,
            @RequestParam(value = "dangerousGoodsType[]", required = false) String[] dangerousGoodsType, @RequestParam(value = "enabled", required = false) String enabled,
            @RequestParam(value = "enddate", required = false) String enddate, @RequestParam(value = "startdate", required = true) String startdate,
            @RequestParam(value = "user", required = false) String user) {
        if (StringUtils.isEmpty(user)) {
            user = WebContextUtils.getCurrentUserEmail();
        }

        GiftWrapCategoryRuleCreateForm categoryRuleCreateForm = getGiftWrapCategoryRuleCreateForm(previousRuleCode, ruleName, categoryUrl, fragile, weight, volume, price,
                weightTo, volumeTo, priceTo, weightOperator, volumeOperator, priceOperator, dangerousGoodsType, enddate, startdate, enabled, user);

        List<Rule> rules = null;

        // if it is an update request.
        if (StringUtils.isNotEmpty(categoryRuleCreateForm.getPreviousRuleCode())) {
            List<Rule> existingRules = giftWrapRuleWrapperService.getGiftWrapRulesByCode(categoryRuleCreateForm.getPreviousRuleCode());
            for (Rule existingRule : existingRules) {
                if (existingRule.getName().equalsIgnoreCase(categoryRuleCreateForm.getRuleName())) {
                    rules = new ArrayList<Rule>();
                    rules.add(existingRule);
                    break;
                }

            }
            LOG.info("Previous rules {} for rule code {}", existingRules, categoryRuleCreateForm.getPreviousRuleCode());
        }

        List<GiftWrapRuleUpdateDTO> creationDTOList = GiftWrapRuleCreateFormConverter.getRuleDTOFromCategoryRuleCreateForm(categoryRuleCreateForm, rules);
        try {
            SystemResponse resp = new SystemResponse();
            Map<String, Object> items = new HashMap<String, Object>();

            for (GiftWrapRuleUpdateDTO creationDTO : creationDTOList) {
                if (null == rules) {
                    Rule r = giftWrapRuleWrapperService.saveServiceabilityRules(creationDTO);
                    resp.setStatus(SystemResponse.ResponseStatus.SUCCESS.name());
                    LOG.info("Rule created " + creationDTO.toString());

                } else {
                    RuleUpdateResponse updateResponse = giftWrapRuleWrapperService.upadteGiftWrapRules(creationDTO);
                    if (updateResponse.isSuccessful()) {
                        resp.setStatus(SystemResponse.ResponseStatus.SUCCESS.name());
                        LOG.info("Rule updated " + creationDTO.toString());
                    } else {
                        resp.setStatus(SystemResponse.ResponseStatus.FAIL.name());
                        resp.setMessage(updateResponse.getFailureReason());
                    }
                }

                items.put("createdRule", creationDTO);
            }

            resp.setItems(items);
            return resp;
        } catch (Exception e) {
            SystemResponse resp = new SystemResponse();
            resp.setStatus(SystemResponse.ResponseStatus.FAIL.name());
            resp.setMessage(" Rule creation failded for category " + categoryUrl + " and type " + "gif  twrap-categorywise" + " reason  : " + e.getMessage());
            LOG.info("Rule creation failed " + creationDTOList.toString(), e);
            return resp;
        }

    }

    @RequestMapping("editGiftwrapRule")
    public String editGiftWrapRule(ModelMap modelMap, @RequestParam(value = "ruleName") String ruleName, @RequestParam(value = "ruleCode") String ruleCode) {
        List<Rule> rules = giftWrapRuleWrapperService.getGiftWrapRulesByCode(ruleCode);
        String view = null;
        Rule r = null;

        if (null != rules && !rules.isEmpty()) {
            for (Rule rule : rules) {
                if (rule.getName().equalsIgnoreCase(ruleName)) {
                    r = rule;
                    break;
                }
            }
        }
        if (r != null) {
            GiftWrapCategoryRuleEditForm form = giftWrapRuleWrapperService.getGiftWrapRuleEditDTOFromRule(r);
            modelMap.addAttribute("giftWrapCategoryRuleEditForm", form);
            modelMap.addAttribute("allDangerousGoodsType", ConfigUtils.getStringList(Property.DANGEROUS_GOODS_TYPE));
            view = "/admin/rules/editGiftwrapCategoryRule";
        } else {
            modelMap.addAttribute("message", "No such rule exists. Please update the search criteria.");
            view = "/admin/rules/editGiftwrapCategoryRule";
        }
        return view;
    }

    private GiftWrapCategoryRuleCreateForm getGiftWrapCategoryRuleCreateForm(String previousRuleCode, String ruleName, String categoryUrl, String fragile, String weight,
            String volume, String price, String weightTo, String volumeTo, String priceTo, String weightOperator, String volumeOperator, String priceOperator,
            String[] dangerousGoodsType, String endDate, String startDate, String enabled, String user) {

        if (StringUtils.isEmpty(ruleName)) {
            ruleName = "R" + DateUtils.dateToString(DateUtils.getCurrentTime(), "yyyyMMddHHmmssSSS") + "-" + com.snapdeal.base.utils.StringUtils.getRandomAlphaNumeric(4) + "-"
                    + categoryUrl;
        }
        GiftWrapCategoryRuleCreateForm form = new GiftWrapCategoryRuleCreateForm();
        form.setRuleName(ruleName);
        form.setRuleType(CocofsRuleUnitType.GIFTWRAP_CATEGORY.getCode());
        form.setCategoryUrl(categoryUrl);
        form.setFragile(getBooleanFromString(fragile));
        form.setWeight(weight);
        form.setVolume(volume);
        form.setPrice(price);
        form.setWeightTo(weightTo);
        form.setVolumeTo(volumeTo);
        form.setPriceTo(priceTo);
        form.setWeightOperator(weightOperator);
        form.setVolumeOperator(volumeOperator);
        form.setPriceOperator(priceOperator);
        if (null != dangerousGoodsType) {
            form.setDangerousGoodsType(Arrays.asList(dangerousGoodsType));
        }
        form.setEndDate(endDate);
        form.setStartDate(startDate);
        if (StringUtils.isNotEmpty(enabled)) {
            form.setEnabled(Boolean.parseBoolean(enabled));
        } else {
            form.setEnabled(true);
        }
        //TODO dangeGoodType
        form.setPreviousRuleCode(previousRuleCode);
        form.setUserEmail(user);
        return form;

    }

    private Boolean getBooleanFromString(String fragile) {
        if (StringUtils.isEmpty(fragile)) {
            return null;
        }
        return !fragile.startsWith("false");
    }

    @RequestMapping(value = "editRule", produces = "application/json", method = { RequestMethod.POST })
    @ResponseBody
    public SystemResponse editRule(@RequestParam(value = "ruletype", required = true) String ruleType, @RequestParam(value = "categoryurl", required = true) String categoryUrl,
            @RequestParam(value = "fragile", required = false) String fragile, @RequestParam(value = "hazmat", required = false) String hazmat,
            @RequestParam(value = "liquid", required = false) String liquid, @RequestParam(value = "weight", required = false) String weight,
            @RequestParam(value = "volweight", required = false) String volweight, @RequestParam(value = "enddate", required = false) String enddate,
            @RequestParam(value = "startdate", required = true) String startdate, @RequestParam(value = "user", required = false) String user,
            @RequestParam(value = "enabled", required = true) String enabled, @RequestParam(value = "rulename", required = true) String ruleName) {
        if (StringUtils.isEmpty(user)) {
            user = WebContextUtils.getCurrentUserEmail();
        }
        List<Rule> existingRules = ruleManager.getEnabledRulesForCategoryAndType(categoryUrl, ruleType);
        if ((null == existingRules) || existingRules.isEmpty()) {
            SystemResponse resp = new SystemResponse();
            resp.setStatus(SystemResponse.ResponseStatus.FAIL.name());
            resp.setMessage(" No enabled rule exists for category " + categoryUrl + " and type " + ruleType);
            return resp;
        }
        RuleEditDTO editDTO = ruleManager.getRuleEditDTO(ruleType, categoryUrl, fragile, hazmat, liquid, weight, volweight, enddate, startdate, user, enabled);

        try {
            SystemResponse resp = new SystemResponse();

            RuleUpdateResponse updateResp = ruleManager.updateRule(editDTO, ruleName);
            if (updateResp.isSuccessful()) {
                resp.setStatus(SystemResponse.ResponseStatus.SUCCESS.name());
                LOG.info("Rule edited " + editDTO.toString());

                Map<String, Object> items = new HashMap<String, Object>();
                items.put("editedRule", editDTO);

                resp.setItems(items);
            } else {
                resp.setStatus(SystemResponse.ResponseStatus.FAIL.name());
                resp.setMessage(updateResp.getFailureReason());
            }

            return resp;
        } catch (Exception e) {
            SystemResponse resp = new SystemResponse();
            resp.setStatus(SystemResponse.ResponseStatus.FAIL.name());
            resp.setMessage(" Rule edit failed for category " + categoryUrl + " and type " + ruleType + " reason  : " + e.getMessage());
            LOG.info("Rule edit failed " + editDTO.toString(), e);
            return resp;
        }

    }

    private Map<String, String> getChildCategories() {
        Map<String, String> map = (Map<String, String>) Collections.EMPTY_MAP;
        ProductCategoryCache cache = CacheManager.getInstance().getCache(ProductCategoryCache.class);
        if (cache != null) {
            map = cache.getAllSubCategories();
        }
        return map;
    }

    private Map<String, String> getBlocks() {
        List<Block> bList = CacheManager.getInstance().getCache(BlocksCache.class).getAllBlocks();

        Map<String, String> bMap = new HashMap<String, String>();
        for (Block b : bList) {
            bMap.put(b.getName(), b.getName());
        }
        return bMap;
    }

}
