/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.adminweb.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.cocofs.commonweb.utils.WebContextUtils;
import com.snapdeal.cocofs.entity.CocofsProperty;
import com.snapdeal.cocofs.services.ICocofsPropertyService;

@Controller
@RequestMapping(PropertyController.URL)
public class PropertyController {

    public static final String     URL = "/admin/property/";

    private static final Logger    LOG = LoggerFactory.getLogger(PropertyController.class);

    @Autowired
    private ICocofsPropertyService propertyService;

    @RequestMapping("view")
    public String showView(ModelMap model) {
        return "admin/admintasks/propertyview";
    }

    /**
     * This method will give value of property against its name.
     * 
     * @param propName
     * @return
     */
    @RequestMapping(value = "getPropertyValue", produces = "application/json")
    @ResponseBody
    public String getValueForProperty(@RequestParam(value = "propName", required = true) String propName) {
        LOG.info("Checking for property with name {} in cocofs_property table", propName);
        CocofsProperty prop = propertyService.getPropertyFromName(propName);
        if (prop == null) {
            LOG.info("Property does not exist in DB");
            return new Gson().toJson("Property does not exist in DB");
        } else {

            LOG.info("Property does exist in DB and its value is {}", prop.getValue());
            return new Gson().toJson(prop.getValue());
        }
    }

    /**
     * This method will clear the Property from Database against a Property Name provided.
     * 
     * @param propName
     * @return
     */

    @RequestMapping(value = "clearPropertyWithName", produces = "application/json")
    @ResponseBody
    public String clearPropertyWithName(@RequestParam(value = "propName", required = true) String propName) {
        LOG.info("Checking for property with name {} in cocofs_property table", propName);
        CocofsProperty prop = propertyService.getPropertyFromName(propName);
        if (prop == null) {
            LOG.info("Property {} does not exist in DB", propName);
            return new Gson().toJson("Property does not exist in DB");
        } else {
            prop.setLastUpdated(DateUtils.getCurrentTime());
            prop.setUpdated_by(WebContextUtils.getCurrentUserEmail());
            LOG.info("Property Found "+prop);
            propertyService.remove(prop);
            LOG.info("Property {} deleted from the Database", propName);
            return new Gson().toJson("Property " + propName + " deleted from the Database");
        }
    }

    /**
     * This method will create/update the Property in Database against Property Name and Value provided.
     * 
     * @param propName
     * @param propValue
     * @return
     */

    @RequestMapping(value = "saveProperty", produces = "application/json")
    @ResponseBody
    public String saveProperty(@RequestParam(value = "propName", required = true) String propName, @RequestParam(value = "propValue", required = true) String propValue) {
        LOG.info("Checking for property with name {} in cocofs_property table", propName);
        CocofsProperty prop = propertyService.getPropertyFromName(propName);
        String userName = WebContextUtils.getCurrentUserEmail();
        if (prop == null) {
            prop = new CocofsProperty();
            prop.setName(propName);
            prop.setValue(propValue);
            prop.setCreated(DateUtils.getCurrentTime());
            prop.setLastUpdated(DateUtils.getCurrentTime());
            prop.setUpdated_by(userName);
            propertyService.save(prop);
            LOG.info("Property {} does not exist in DB, Created the same", propName);
            return new Gson().toJson("Property did not exist in DB, Created the same");
        } else {
            LOG.info("Property {} exists in the Database, Updated the same", propName);
            prop.setValue(propValue);
            prop.setLastUpdated(DateUtils.getCurrentTime());
            prop.setUpdated_by(userName);
            propertyService.merge(prop);
            return new Gson().toJson("Property " + propName + " exists in the Database, Updated the same");
        }
    }
}
