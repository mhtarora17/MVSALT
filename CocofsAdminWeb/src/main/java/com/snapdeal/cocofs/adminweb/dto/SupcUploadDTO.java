package com.snapdeal.cocofs.adminweb.dto;

public class SupcUploadDTO {

    private String supc;

    public String getSupc() {
        return supc;
    }

    public void setSupc(String supc) {
        this.supc = supc;
    }

}
