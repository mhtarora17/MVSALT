/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 16, 2013
 *  @author himanshu
 */
package com.snapdeal.cocofs.adminweb.dto;

import java.util.ArrayList;
import java.util.List;

public class ShippingModeInfoDTO {
    
    private String key;
    private List<String> modes = new ArrayList<String>();
    private String deliveryType ;
    public String getKey() {
        return key;
    }
    public void setKey(String key) {
        this.key = key;
    }
    public List<String> getModes() {
        return modes;
    }
    public void setModes(List<String> modes) {
        this.modes = modes;
    }
    public String getDeliveryType() {
        return deliveryType;
    }
    public void setDeliveryType(String deliveryType) {
        this.deliveryType = deliveryType;
    }
    
    
    

}
