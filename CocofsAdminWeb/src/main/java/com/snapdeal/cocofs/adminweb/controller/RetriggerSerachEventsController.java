/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.adminweb.controller;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.cocofs.adminweb.controller.form.UserUpdateForm;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.dataaccess.fm.IFMDBDataReadService;
import com.snapdeal.cocofs.dto.SearchEventDTO;
import com.snapdeal.cocofs.entity.SellerFMMapping;
import com.snapdeal.cocofs.entity.SellerSupcFMMapping;
import com.snapdeal.cocofs.services.fm.IManualPushOfSellerFMUpdateForExternalNotification;

/**
 * @version 1.0, 19-Dec-2014
 * @author ankur
 */

@Controller
@RequestMapping(RetriggerSerachEventsController.URL)
public class RetriggerSerachEventsController {

    private static final Logger                                LOG = LoggerFactory.getLogger(RetriggerSerachEventsController.class);

    public static final String                                 URL = "/admin/retriggerSearchEvents";

    @Autowired
    private IFMDBDataReadService                               fmService;

    
    @Autowired
    @Qualifier("sellerSUPCFMMappingProcessor")
    private IManualPushOfSellerFMUpdateForExternalNotification searchNotificationProcessor;

    @RequestMapping("")
    public String defaultView(ModelMap modelMap) {
        modelMap.put("userCreationForm", new UserUpdateForm());
        return "/admin/events/retriggerSearchEvents";
    }

    @RequestMapping("/getSearchNotificationNumbers")
    public @ResponseBody SearchEventDTO getSearchNotificationNumbers(@RequestParam("hours") String strHours) {
        SearchEventDTO searchEventDTO = new SearchEventDTO();
        try {
            if (StringUtils.isNotEmpty(strHours)) {
                Integer hours = Integer.parseInt(strHours);
                Date lastUpdatedTime = DateUtils.addToDate(DateUtils.getCurrentTime(), Calendar.HOUR, -hours);
                LOG.info("Fetching numbers from last Updated time {}", lastUpdatedTime);
                long sellerCount = fmService.getSellerFMMappingCountByLastUpdated(lastUpdatedTime);
                long sellerSupcCount = fmService.getSellerSupcFMMappingCountByLastUpdated(lastUpdatedTime);
                searchEventDTO.setSellerSupcUpdateCount(sellerSupcCount);
                searchEventDTO.setSellerUpdateCount(sellerCount);
                searchEventDTO.setMessage("success");

            } else {
                LOG.info("invalid hour ");
                searchEventDTO.setMessage("invalid hour");

            }
        } catch (NumberFormatException e) {
            LOG.info("invalid hour ");
            searchEventDTO.setMessage("invalid hour");
        }

        return searchEventDTO;
    }

    @RequestMapping("/retriggerSearchNotifications")
    public @ResponseBody SearchEventDTO retriggerSearchNotifications(@RequestParam("hours") String strHours) {
        SearchEventDTO searchEventDTO = new SearchEventDTO();
        try {
            if (StringUtils.isNotEmpty(strHours)) {
                Integer hours = Integer.parseInt(strHours);
                Date lastUpdatedTime = DateUtils.addToDate(DateUtils.getCurrentTime(), Calendar.HOUR, -hours);

                processNotifications(lastUpdatedTime, searchEventDTO);

            } else {
                LOG.info("invalid hour ");
                searchEventDTO.setMessage("invalid hour");

            }
        } catch (NumberFormatException e) {
            LOG.info("invalid hour ");
            searchEventDTO.setMessage("invalid hour");
        }

        return searchEventDTO;
    }

    private void processNotifications(Date lastUpdatedTime, SearchEventDTO searchEventDTO) {

        LOG.info("Fetching seller and seller-supc numbers from last Updated time {}", lastUpdatedTime);
        StringBuilder message = new StringBuilder();
        long sellerCount = fmService.getSellerFMMappingCountByLastUpdated(lastUpdatedTime);
        if (sellerCount >= 0 && sellerCount <= ConfigUtils.getIntegerScalar(Property.MANUAL_SELLER_UPDATES_PUSH_LIMIT)) {
            processSellerUpdateNotifications(lastUpdatedTime);
            searchEventDTO.setSellerUpdateCount(sellerCount);
            message.append("seller Updates pushed. ");

        } else {
            LOG.info("sellerCount {} is more than configure limit, hence not pushing messages to search for this time range ", sellerCount);
            searchEventDTO.setSellerUpdateCount(0);
            message.append("sellerCount " + sellerCount + " is more than configure limit. ");
        }

        long sellersupcCount = fmService.getSellerSupcFMMappingCountByLastUpdated(lastUpdatedTime);
        if (sellersupcCount >= 0 && sellersupcCount <= ConfigUtils.getIntegerScalar(Property.MANUAL_SELLER_SUPC_UPDATES_PUSH_LIMIT)) {
            processSellerSupcUpdateNotifications(lastUpdatedTime);
            searchEventDTO.setSellerSupcUpdateCount(sellersupcCount);
            message.append("seller-supc Updates pushed. ");

        } else {
            LOG.info("sellersupc Count {} is more than configure limit, hence not pushing messages to search for this time range ", sellersupcCount);
            searchEventDTO.setSellerSupcUpdateCount(0);
            message.append("seller-Supc Count " + sellersupcCount + " is more than configure limit. ");
        }

        searchEventDTO.setMessage(message.toString());

    }

    @Async
    private void processSellerUpdateNotifications(Date lastUpdatedTime) {

        LOG.info("fetching seller list to pushed to search from DB for lastUpdatedTime {}", lastUpdatedTime);
        List<SellerFMMapping> sellerFMMappings = fmService.getAllSellerFMMappingByLastUpdated(lastUpdatedTime);

        for (SellerFMMapping sellerfm : sellerFMMappings) {
            String existingFulfilmentModel = sellerfm.getFulfilmentModel();
            searchNotificationProcessor.processManualPushOfExistingSellerFMMappingForExternalNotification(sellerfm.getSellerCode(), existingFulfilmentModel);
        }

    }

    @Async
    private void processSellerSupcUpdateNotifications(Date lastUpdatedTime) {
        LOG.info("fetching seller-supc list to pushed to search from DB for lastUpdatedTime {}", lastUpdatedTime);
        List<SellerSupcFMMapping> sellersupcFMMappings = fmService.getAllSellerSupcFMMappingByLastUpdated(lastUpdatedTime);

        for (SellerSupcFMMapping sellersupcfm : sellersupcFMMappings) {
            String existingFulfilmentModel = sellersupcfm.getFulfilmentModel();
            searchNotificationProcessor.processExistingSellerSUPCFMMappingExceptionForExternalNotification(sellersupcfm.getSellerCode(), sellersupcfm.getSupc(),
                    existingFulfilmentModel);
        }
    }

}
