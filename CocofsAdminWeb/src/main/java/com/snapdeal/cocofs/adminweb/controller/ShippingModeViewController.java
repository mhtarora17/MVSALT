/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Nov 7, 2013
 *  @author himanshu
 */
package com.snapdeal.cocofs.adminweb.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.snapdeal.cocofs.access.IShippingModeService;
import com.snapdeal.cocofs.adminweb.dto.ShippingModeInfoDTO;
import com.snapdeal.cocofs.services.IDeliveryTypeService;
import com.snapdeal.cocofs.sro.CategoryShippingModeSRO;
import com.snapdeal.cocofs.sro.DeliveryType;
import com.snapdeal.cocofs.sro.SupcShippingModeSRO;


@Controller
@RequestMapping(ShippingModeViewController.URL)
public class ShippingModeViewController {
    public static final String    URL = "/admin/shippingmode/";
    
    private static final Gson GSON_BUILDER = new Gson();
    
    @Autowired
    private IShippingModeService shippingModeService;
    @Autowired
    private IDeliveryTypeService deliveryTypeService;
    
    @RequestMapping("")
    public String defaultView() {
        return "redirect:view";
    }
    
    @RequestMapping("view")
    public String showView( ModelMap model) {
        return "admin/admintasks/shippingmodeview";
    }
    @RequestMapping(value = "supcModes", produces="application/json")
    @ResponseBody
    public String fetchSupcModeInfo(@RequestParam(value = "supc", required = true) String supc) {
        List<String> supcs = new ArrayList<String>();
        supcs.add(supc);
        Map<String, SupcShippingModeSRO> map = shippingModeService.getSupcModeMapping(supcs );
        List<String> modes = new ArrayList<String>();
        if ( map.containsKey(supc)) {
            modes = map.get(supc).getShippingMode();
        }
        DeliveryType dt = deliveryTypeService.getDeliverTypeForShippingModes(modes);
        ShippingModeInfoDTO dto = new ShippingModeInfoDTO();
        dto.setKey(supc);
        dto.setModes(modes);
        dto.setDeliveryType(dt.getCode());
        return GSON_BUILDER.toJson(dto);
    }
    
    @RequestMapping(value = "catModes", produces="application/json")
    @ResponseBody
    public String fetchCategoryModeInfo(@RequestParam(value = "cat", required = true) String cat) {
        List<String> categories = new ArrayList<String>();
        categories.add(cat);
        Map<String, CategoryShippingModeSRO> map = shippingModeService.getCategoryModeMapping(categories);
        List<String> modes = new ArrayList<String>();
        if ( map.containsKey(cat)) {
            modes = map.get(cat).getShippingMode();
        }
        DeliveryType dt = deliveryTypeService.getDeliverTypeForShippingModes(modes);
        ShippingModeInfoDTO dto = new ShippingModeInfoDTO();
        
        dto.setKey(cat);
        dto.setModes(modes);
        dto.setDeliveryType(dt.getCode());
        return GSON_BUILDER.toJson(dto);
    }

}
