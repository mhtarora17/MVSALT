package com.snapdeal.cocofs.adminweb.dto;

import com.snapdeal.base.model.common.ServiceResponse;

public class JobDTOResponse extends ServiceResponse {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String response;

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}
	
	

}