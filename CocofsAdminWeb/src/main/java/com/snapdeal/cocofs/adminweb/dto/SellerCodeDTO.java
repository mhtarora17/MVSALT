package com.snapdeal.cocofs.adminweb.dto;

public class SellerCodeDTO {
	
	private String sellerCode;

	public String getSellerCode() {
		return sellerCode;
	}

	public void setSellerCode(String sellerCode) {
		this.sellerCode = sellerCode;
	}
	

}
