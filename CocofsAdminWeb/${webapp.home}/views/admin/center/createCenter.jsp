<%@ include file="/tld_includes.jsp"%>
<style>
.tb td {
	font-weight: bold;
	font-size: small;
}

select {
	width: 170px;
}
</style>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<tiles:insertTemplate template="/views/layout/base.jsp">

	<tiles:putAttribute name="subheader">
		<tiles:insertTemplate
			template="/views/admin/fmMapping/sellerSupc-subheader.jsp">
		</tiles:insertTemplate>
	</tiles:putAttribute>

	<tiles:putAttribute name="title" value="Center Management" />

	<tiles:putAttribute name="subheader">
		<tiles:insertTemplate template="/views/admin/fcMapping/sidebar.jsp">
			<tiles:putAttribute name="active" value="fcMapping" />
		</tiles:insertTemplate>
	</tiles:putAttribute>

	<tiles:putAttribute name="extraScripts">
		<link href="${path.css('chosen/chosen.css')}" rel="stylesheet"
			type="text/css" />
		<link rel="stylesheet" type="text/css"
			href="${path.css('snapdeal/ui.jqgrid.css')}" />

		<script type="text/javascript"
			src="${path.js('chosen/chosen.jquery.min.js')}"></script>
	</tiles:putAttribute>


	<tiles:putAttribute name="deferredScript">
		<script>
			if (typeof Snapdeal == 'undefined') {
				Snapdeal = {};
			};
			Snapdeal.getStaticPath = function(path) {
				return '${path.http}' + path;
			}

			Snapdeal.getResourcePath = function(path) {
				return '${path.resources("")}' + path;
			}
		</script>
		<script type="text/javascript"
			src="${path.js('snapdeal/validations.js')}"></script>

		<script>
			$(document)
					.ready(
							function() {

								$('#sidebar-cc').addClass('active');
								$('#header-nav-fcview').addClass('active');

								$
										.ajax({
											url : "${path.http}/admin/centermanagement/getAllCenterTypes",
											type : "GET",
											dataType : 'json',
											success : function(data) {

												if (data != null) {
													var options = '';
													for (var i = 0; i < data.fms.length; i++) {
														options += '<option value="' + data.fms[i] + '">'
																+ data.fms[i]
																+ '</option>';
													}
													$("#fulfillmentType").html(
															options);
												}

											}
										});

								$("#pincode")
										.change(
												function() {
													var pincode = document
															.getElementById("pincode").value
															.trim();

													if (pincode != undefined
															&& pincode != '') {
														var ajaxUrl = "${path.http}/admin/centermanagement/getCityState?pincode="
																+ pincode;
														$
																.ajax({
																	url : ajaxUrl,
																	type : "GET",
																	dataType : 'json',
																	success : function(
																			data) {
																		if (data != null) {
																			$(
																					'#centerData')
																					.fadeIn();
																			var options = '';
																			for (var i = 0; i < data.city.length; i++) {
																				options += '<option value="' + data.city[i] + '">'
																						+ data.city[i]
																						+ '</option>';
																			}
																			$(
																					"#city")
																					.html(
																							options);

																			var div_data = '';
																			for (var i = 0; i < data.state.length; i++) {
																				div_data += '<option value="'+ data.state[i]+'">'
																						+ data.state[i]
																						+ '</option>';
																			}
																			$(
																					"#state")
																					.html(
																							div_data);

																		}
																	}
																});
													} else {
														alert("Please enter a valid pin code");
													}

												});

								$('#submitButton')
										.click(
												function() {
													$("#fulfillmentCenterForm")
															.validate(
																	{
																		rules : {
																			email : {
																				required : true,
																			},
																			centername : {
																				required : true,
																			},
																			centercode : {
																				required : true,
																				maxlength : 16,
																			},
																			addressLine1 : {
																				required : true,
																			},
																			pincode : {
																				required : true,
																				number : true,
																			},
																			mobile : {
																				required : true,
																				number : true,
																				maxlength : 10,
																			},
																			name : {
																				required : true,
																			},
																			city : {
																				required : true,
																			},
																			state : {
																				required : true,
																			},
																		},
																		messages : {
																			email : {
																				required : "Please enter a email address",
																			},
																			centername : {
																				required : "Please provide a Center name",
																			},
																			centercode : {
																				required : "Please provide a Center Code",
																				maxlength : "Center code can not exceed length 16",
																			},
																			addressLine1 : {
																				required : "Please provide Address detail",
																			},
																			name : {
																				required : "Please provide Name",
																			},
																			pincode : {
																				required : "Please provide pincode",
																				number : "Pincode should be numeric",
																			},
																			mobile : {
																				required : "Please provide mobile number",
																				number : "mobile number should be numeric",
																				maxlength : "Mobile number length can not exceed 10",
																			},
																			city : {
																				required : "Please provide city",
																			},
																			state : {
																				required : "Please provide state",
																			},
																		}
																	});
													$('#fulfillmentCenterForm')
															.submit();
												});

							});
		</script>
	</tiles:putAttribute>


	<tiles:putAttribute name="body">
		<div class="cod-dash-menu">
			<ul style="float: none;">


				<li><a href="${path.http}/admin/centermanagement/createCenter"
					class="active">Create Center</a></li>
				<li><a href="${path.http}/admin/centermanagement/updateCenter">Update
						Center </a></li>
			</ul>
		</div>


		<form:form commandName="fulfillmentCenterForm"
			id="fulfillmentCenterForm" method="POST"
			action='${path.http}/admin/centermanagement/createNewCenter'
			name="fulfillmentCenterForm">
			<div class="main-content lfloat">
				<div class="head-lable">Create Center</div>
				<div class="container" style="width: 100%;">

					<div class="control-group">
						<table class="tb" width="54%" style="padding: 0px 0px 0px 62px;">
							<tr>
								<td><span>Center Name<span class=required>*</span>:
								</span></td>
								<td><input id="centername"
									class="input-text validateNotEmpty" name="centername" value=""
									tabindex="2"></td>
							</tr>
							<tr>
								<td><span>Center Code<span class=required>*</span>:
								</span></td>
								<td><input id="centercode"
									class="input-text validateNotEmpty" name="centercode" value=""
									tabindex="2"></td>
							</tr>

							<tr>
								<td><label class="control-label">Center Type</label></td>

								<td><select id="fulfillmentType" name="fulfillmentType"
									tabindex="2" style="width: 206px">
								</select></td>
							</tr>
							<tr>
								<td><span>Name<span class=required>*</span>:
								</span></td>
								<td><input id="name" class="input-text validateNotEmpty"
									name="name" value="" tabindex="2"></td>
							</tr>
							<tr>
							<tr>
								<td><span>Address Line 1<span class=required>*</span>:
								</span></td>
								<td><input id="addressLine1" type="text"
									class="input-text validateNotEmpty" name="addressLine1"
									value="" tabindex="2"></td>
							</tr>
							<tr>
								<td><label class="control-label">Address Line 2</label></td>
								<td><input id="addressLine2" type="text"
									class="input-text validateNotEmpty" name="addressLine2"
									value="" tabindex="2"></td>
							</tr>
							<tr>
								<td><span>Pincode<span class=required>*</span>:
								</span></td>
								<td><input id="pincode" type="text"
									class="input-text validateNotEmpty" name="pincode" value=""
									tabindex="2"></td>

							</tr>
							<tr>
								<td><label class="control-label">City</label></td>
								<td><select id="city" type="text"
									class="input-text validateNotEmpty" name="city" tabindex="2"
									style="width: 206px"></select></td>
							</tr>
							<tr>
								<td><label class="control-label">State</label></td>
								<td><select id="state" type="text"
									class="input-text validateNotEmpty" name="state" tabindex="2"
									style="width: 206px"></select></td>
							</tr>
							<tr>
								<td><span>Mobile<span class=required>*</span>:
								</span></td>
								<td><input id="mobile" type="text"
									class="input-text validateNotEmpty" name="mobile" value=""
									tabindex="2"></td>
							</tr>
							<tr>
								<td><label class="control-label">LandLine Number</label></td>
								<td><input id="landLine" type="text"
									class="input-text validateNotEmpty" name="landLine" value=""
									tabindex="2"></td>
							</tr>
							<tr>
								<td><span>Email<span class=required>*</span>:
								</span></td>
								<td><input id="email" type="text"
									class="input-text validateNotEmpty" name="email" value=""
									tabindex="2"></td>
							</tr>


						</table>
					</div>

					<div id="errorMessage" class="alert-error">${message}</div>
					<div class="control-group">
						<div align="left" style="padding-top: 24px; padding-left: 184px;">
							<input type="submit" class="btn btn-primary threeD"
								id="submitButton" value="Create Center">

						</div>
					</div>
				</div>
			</div>
		</form:form>
	</tiles:putAttribute>
</tiles:insertTemplate>



