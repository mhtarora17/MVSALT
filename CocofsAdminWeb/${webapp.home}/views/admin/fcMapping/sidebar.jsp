<%@ include file="/tld_includes.jsp"%>


<div id="subheader" class="subheader"
	style="background-image: url(${path.resources('img/background/bg_blue.png')});">
	<div class="tabbable">
		<!-- Only required for left/right tabs -->
		<sec:authentication property="principal" var="user" />
		
		<div class="subheaderTab">
			<sec:authorize ifAnyGranted="admin,tech,fcmappingeditor">

				<div id="sidebar-fn-fc"
					onclick="javascript:window.location.href='/admin/fcMapping/'">Seller
					Center Mapping</div>
			</sec:authorize>
			<sec:authorize ifAnyGranted="admin,tech, fceditor">

				<div id="sidebar-cc"
					onclick="javascript:window.location.href='/admin/centermanagement/createCenter'">Create
					FC</div>
			</sec:authorize>
			<sec:authorize ifAnyGranted="admin,tech, fceditor">

				<div id="sidebar-cu"
					onclick="javascript:window.location.href='/admin/centermanagement/updateCenter'">Update
					FC Details</div>

			</sec:authorize>

		</div>
	</div>
	<div class="subheaderToggle">
		<div class="subheaderToggleContent">&lt</div>
	</div>
</div>