<%@ include file="/tld_includes.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<tiles:insertTemplate template="/views/layout/base.jsp">
	<tiles:putAttribute name="title" value="Seller Center Mapping" />

	<tiles:putAttribute name="systemMessage">
		<div class="system-message"></div>
	</tiles:putAttribute>

	<tiles:putAttribute name="extraScripts">
	<script type="text/javascript" src="${path.js('mousetrap/mousetrap.min.js')}"></script>
		<script type="text/javascript">
		
		$(document).ready(function() {
			if ($('#messageHiddenField').val() != ''){
                smoke.signal($('#messageHiddenField').val(),2000);
             }

			
			$('#sidebar-fn-fc').addClass('active');
	        $('#header-nav-fcview').addClass('active');
			
		 });
		
		
		
		Mousetrap.stopCallback= function(e, element, combo) {

            // if the element has the class "mousetrap" then no need to stop
            if ((' ' + element.className + ' ').indexOf(' mousetrap ') > -1) {
                return false;
            }

            // stop unless enter is pressed from with in scan text box
            return (element.id != 'searchEmail');
        }
		
		Mousetrap.bind('enter', function() { 
    		searchSeller();
   	 });

		/* function checkIfAlphaNumeric(){
				$('#sellerCode').val($('#sellerCode').val().replace(/[^a-zA-Z0-9]+/i, ''));
		} */
		
		function checkIfSellerAlphaNumeric(val) {
	        var regularExpression = /^[a-zA-Z0-9]+$/;
	        var valid = regularExpression.test(val);
	        return valid;
	    }
		
			function fixArray(array){
				if ($.isArray(array)) {
					return array;
				} else {
					return [ array ];
				}
			}
			
			function showErrorAlert(message ) {
	            /*<div class="alert alert-error">*/
	            console.log(message);
	            if ($('#fetch-error-msg').length == 0 ) {
	                $('#fetchMessages').append('<div class="alert alert-error"> <button type="button" class="close" data-dismiss="alert">&times;</button> <div id="fetch-error-msg">' + message + '</div></div>');
	                
	            } else {
	                $('#fetch-error-msg').html($('#fetch-error-msg').html() + " <br/>" + message);
	            }
	        }
			
			function showSuccessAlert(message) {
	            
	            console.log(message);
	            $('#fetchMessages').append('<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert">&times;</button>' + message + '</div>');
	            
	        }
			
			function resetOldForm(){
				$('#userData').hide();
				  $('#seller').val('');
				  $('#fulfilmentModel').val('');
				 
			}
			
			
			function submitForm(){
				var sellerCode = document.getElementById("sellerCode").value.trim();
				var fcCode = $('input[name=radio_fulfillment]:checked', '#sellerFCForm').val();
				var fmModel = document.getElementById("fulfilmentModel").value.trim();
				
				if(fcCode =='' || fcCode == undefined){
					smoke.alert("Please select a fulfilment center");
					return;
				}
				
				if(sellerCode!=undefined && sellerCode!='' ){
					var data = {
							'sellerCode' : sellerCode, // catalog ids as csv
							'fcCode' : fcCode,
							'fulfilmentModel' :  fmModel
						};
					var ajaxUrl =  "${path.http}/admin/fcMapping/updateSellerFC";
					  $ .ajax({url : ajaxUrl,async : false ,type : "POST", data : data, dataType:'json',success : function(data) {
						  fetchSuccessHandler(data,sellerCode );
						  
					  }
				
				  });
				}else{
					smoke.alert("Please enter a valid seller code");
				}
			}
			
			 function fetchSuccessHandler( data,sellerCode ) {
					$('#fetchMessages').html('');
		            console.log(data);
		           
		            if ( data.status == 'FAIL') {
		                showErrorAlert( "For seller " + "<strong>" + sellerCode +  "</strong> :" + " error : " + data.message );           
		            } else {
						resetOldForm();
		                showSuccessAlert("<strong>" + sellerCode +  "</strong>:  updated successfully " ); 
		            }
		            
		        }
			 
			 function getFCListHtml(allFCList, fcCode){
				 var html ='';
				 console.log(allFCList.length);
				if(allFCList.length > 0){ 
				 	html = '<table width="50%" border="1" cellspacing="3"><thead>\
						<tr><th>#</th><th>Fulfilment Center Code</th></tr></thead><tbody>';
				
					for(var i=0;i<allFCList.length;i++){
					 	 html+= '<tr>\
							<td><input type="radio" name="radio_fulfillment"\
							id="radio_fulfillment" class="radio_type" value="'+ allFCList[i] + '"' ;
					
					  	if(fcCode != null && fcCode !='' && fcCode!= undefined &&  fcCode == allFCList[i] )	{
						  html += 'checked="checked"';
					  	}	
							
					  	html+= '/></td>\
							<td>' + allFCList[i] +'</td>\
							</tr>';
				  	}	
					html += '</thead></tbody>';
					
					$('#saveButton').show();
					
				}else{
					$('#saveButton').hide();
				}
				
				return html;
			 }
			
			function searchSeller(){
				$('#fetchMessages').html('');
				$('#fcList').html('');
				resetOldForm();
				var sellerCode = document.getElementById("sellerCode").value.trim();
				
				if(!checkIfSellerAlphaNumeric(sellerCode)){
					smoke.alert("invalid value for sellerCode");
					return;
				}
				
				if(sellerCode!=undefined && sellerCode!='' ){
					var ajaxUrl =  "${path.http}/admin/fcMapping/searchSeller?sellerCode="+sellerCode;
					  $ .ajax({url : ajaxUrl,type : "GET",dataType : 'json',success : function(data) {
						
						  if(data.sellerCode == null){
						
							 smoke.alert(data.errorMessage);
						 
						 } else if(data.fulfilmentModel != null){
							 
							 $('#userData').fadeIn();
							  $('#seller').val(data.sellerCode);
							  $('#fulfilmentModel').val(data.fulfilmentModel);
							  var fcCode = data.existingFC;
							  var allFCList = fixArray(data.eligibleFCCodesList);
						      $('#fcList').html(getFCListHtml(allFCList, fcCode));									  
								
						  }else{
							  
							  smoke.alert("no fulfilment model found for this seller");
						  
						  }
						  
					  }
				
				  });
				}else{
					smoke.alert("Please enter a valid seller code");
				}
			}
			
			
		</script>

	</tiles:putAttribute>
	<tiles:putAttribute name="systemMessage">
	</tiles:putAttribute>
	<tiles:putAttribute name="subheader">
		<tiles:insertTemplate template="/views/admin/fcMapping/sidebar.jsp">
			<tiles:putAttribute name="active" value="fcMapping" />
		</tiles:insertTemplate>
	</tiles:putAttribute>

	

	<tiles:putAttribute name="body">
	<div class="main-content lfloat">
	<input type="hidden" value="${message}" id="messageHiddenField"/>
            <form class="form-horizontal"  id="sellerFCForm" method="POST" action='${path.http}/admin/fcMapping/update' name="sellerFCForm">
    
			<div class="container" style="width: 100%;">
			<div id="searchSeller" class="control-group" >
			<label class="control-label">Seller Code</label> 
            <div class="controls">
			<input id="sellerCode"  type="text" name="sellerCode" value="" tabindex="2">
			</div>
            </div>
            <div  class="control-group" >
            <div class="controls">
            <input type="button" class="btn btn-primary" value="Search Seller" onclick="searchSeller();">
            
            </div>
            </div>
            <div id="fetchMessages">
                    </div>			
			<div id="userData" style="display: none;">
						<div class="control-group">
							<table>
								<thead>
									<tr class="controls">
									<th>Seller Code</th>
									<th>Fulfilment Model</th>
									</tr>
								</thead>
								<tbody>
								<tr class="controls">
									<td><input id="seller" readonly="readonly" type="text"
										name="seller" value="" tabindex="2">
									</td>
									<td><input id="fulfilmentModel" type="text"
										name="fulfilmentModel" value="" tabindex="2" readonly="readonly">
									</td>
								</tr>
								</tbody>

							</table>
						</div>

						<div class="control-group" id="fcList">

							
						</div>

						<div class="control-group" id="saveButton">
						  	<div align="center" style="padding-top: 10px;padding-right: 220px;">
								<input type="button" class="btn btn-primary threeD" value="Save" onclick="submitForm()">
							
							</div>
						</div>
						</div>
			</div>
			</form>
	</div>
	</tiles:putAttribute>
</tiles:insertTemplate>
