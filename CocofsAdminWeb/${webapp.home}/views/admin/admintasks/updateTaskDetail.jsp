<%@ include file="/tld_includes.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<tiles:insertTemplate template="/views/layout/base.jsp">
	<tiles:putAttribute name="title" value="Task Admin" />

	<tiles:putAttribute name="systemMessage">
		<div class="system-message"></div>
	</tiles:putAttribute>

	<tiles:putAttribute name="extraScripts">
	<link rel="stylesheet" type="text/css"
			href="${path.css('snapdeal/ui.jqgrid.css')}" />
		<script type="text/javascript"
			src="${path.js('jquery/grid.locale-en.js')}"></script>
		<script type="text/javascript"
			src="${path.js('jquery/jquery.jqGrid.min.js')}"></script>
		<script type="text/javascript">
		
		
		var data ='${hostNames}';
		var data = data.substring(1,data.length-1);
		var hostNames = data.split(",");
		var hostName_data='';
		for(var i=0;i<hostNames.length;i++){
			hostName_data +=  hostNames[i].trim() + ':' + hostNames[i].trim() + ';';
		}
		hostName_data = hostName_data.substr(0,hostName_data.length-1);	
		
		
		
		
		var grid = $('#taskDetail');
		function validateRowData(inputData){
			if(inputData.taskName == '' || inputData.implClass =='' || inputData.hostName =='' || inputData.cronExpression ==''){
				alert("One of the required field is empty");
				return false;
			}
			return true;
		}
		
		function addRow(){
			jQuery('#taskDetail').jqGrid('addRow',{'position':'last'});
		}
		
		function editGridData(rowId) {
			var prop_name = $('#taskDetail').jqGrid('getColProp','taskName');
			var prop_impl = $('#taskDetail').jqGrid('getColProp','implClass');
			var prop_clustered = $('#taskDetail').jqGrid('getColProp','clustered');
			var prop_concurrent = $('#taskDetail').jqGrid('getColProp','concurrent');
			var prop_hostName = $('#taskDetail').jqGrid('getColProp','hostName');
			prop_name.editable=false;
			prop_impl.editable=false;
			prop_clustered.editable=false;
			prop_concurrent.editable=false;
			prop_hostName.editable=false;
			$('#taskDetail').jqGrid('editRow', rowId, true, null, null, 'clientArray');
			prop_name.editable=true;
			prop_impl.editable=true;
			prop_clustered.editable=true;
			prop_concurrent.editable=true;
			prop_hostName.editable=true;
		}
		
		
		function restoreGridData(rowId){
			var row = $("#taskDetail").jqGrid('getRowData',rowId);
			if(row.taskDetailId == ''){
				$('#taskDetail').jqGrid("collapseSubGridRow",rowId);
				$("#taskDetail").jqGrid('delRowData',rowId);
			}else{
				jQuery('#taskDetail').restoreRow(rowId);
			}
		}
		
		function validateSubGridRowData(rowData){
			if((rowData.paramName != '' &&rowData.paramValue != '')){
				return true;
			}else{
				alert('Param Value/Name cannt be empty');
				return false;
			}
		}
		
		function saveGridData(rowId) {
			$("#taskDetail").jqGrid('saveRow',rowId);
			var inputData = new Object();
			var row = $("#taskDetail").jqGrid('getRowData',rowId);
			inputData.taskDetailDTO = row;
			if(!validateRowData(row)){
				$('#taskDetail').jqGrid('editRow', rowId, true, null, null, 'clientArray');
				return false;
			}
				var t = jQuery("#taskDetail_" + rowId + "_t");
				var crit_ids = t.jqGrid('getDataIDs');
				var paramsDTO = [];
				for(i=0;i<crit_ids.length;i++){
					t.jqGrid('saveRow',crit_ids[i]);
					var rowData = t.jqGrid('getRowData',crit_ids[i]);
					if(!(rowData.paramName == '' &&rowData.paramValue == '')){
						if(!validateSubGridRowData(rowData)){
							$('#taskDetail').jqGrid('editRow', rowId, true, null, null, 'clientArray');
							return false;
						}
						delete rowData.act;
					
						paramsDTO.push(rowData);
					}
				}
				row.taskParams = paramsDTO;
				delete row.action;
			  $.ajax({
			      type: "POST",
			      url: "/json/admin/task/updateTaskDetail",
			      dataType: 'json',
			      contentType:'application/json',
			      data: JSON.stringify(inputData),
			      async: false,
			      success: function(resp){
			    	  if(resp.successful){
			    	  	var searchUrl = '${path.http}/admin/task/getTaskInfo?implClass='+encodeURIComponent($('#taskImplClass').val());
						$("#taskDetail").jqGrid('setGridParam',{url:searchUrl,datatype: 'json'}).trigger('reloadGrid');
			    	  }else{
			    			var failedData= "Please correct the data. \n";
			    			for (i = 0; i < resp.failedList.length; i++) {
			    				failedData += resp.failedList[i]+"\n";
			    			}
			    			alert(failedData);
			    			if(row.taskDetailId==''){
			    		  		$('#taskDetail').jqGrid('editRow', rowId, true, null, null, 'clientArray');
			    			}else{
			    				editGridData(rowId);
			    			}
			    	  }
			      },
			      error: function(xhr, statusCode , error){
			    	  if(xhr.status==200){
			    		  alert("Your session has expired. Please log in again in another tab/window and then retry here.");
			    		  $('#taskDetail').jqGrid('editRow', rowId, true, null, null, 'clientArray');
			    	  }
			      }
			  });
		}
		
		function hideSidebar(){
            $(".subheader").css({"left":"-11%"});
            $(".functionAnchor").css({"display": "none"});
            $(".sidebar").css({"left":"1%","width":"99%"});
            $(".main-content").css({"left":"1%","width":"98%"});
        }
        function showSidebar(){
            $(".subheader").css({"left":"0%"});
            $(".functionAnchor").css({"display": "block"});
            $(".sidebar").css({"left":"12%","width":"88%"});
            $(".main-content").css({"left":"12%","width":"87%"});
        }
		
		$(document).ready(function() {
			
			var subheader=true;
            $(".subheaderToggle").click(function(){
                    if(subheader){
                            hideSidebar();
                            $(".subheaderToggleContent").html(">");
                            subheader=false;
                    }else{
                            showSidebar();
                            $(".subheaderToggleContent").html("<");
                            subheader=true;
                    }

            });
           /* check sidebar jsp for relevant div ids
           */
           $('#sidebar-fn-taskdetail').addClass('active');
           $('#header-nav-admintasks').addClass('active');
			
			$('#searchTaskDetail').click(function(){
				var searchUrl = '${path.http}/admin/task/getTaskInfo?implClass='+encodeURIComponent($('#taskImplClass').val());
				$("#taskDetail").jqGrid('setGridParam',{url:searchUrl,datatype: 'json'}).trigger('reloadGrid');
				
			});
			
			function showAddRemoveButtons(cellValue,options,rowObject,gridId){
				var addRow = "jQuery('#"+gridId+"').jqGrid('addRow',{'position':'last'})";
				var removeRow = "jQuery('#"+gridId+"').jqGrid('delRowData','"+options.rowId+"')";
				ae =  "<input style='height:22px;width:20px;' type='button' title='Add' value='A' onclick=\""+addRow+";\" />"; 
				re = "<input style='height:22px;width:20px;' type='button' title='Remove' value='R' onclick=\""+removeRow+";\" />"; 
				return ae+re;
			}
			
			 function showActionButtons(cellValue, options, rowObject) {
				ae =  "<input style='height:22px;width:30px;' type='button' title='Add' value='A' onclick=addRow();>";
				be = "<input style='height:22px;width:20px;' type='button' title='Edit' value='E' onclick=editGridData('"+ options.rowId + "');>";
				se = "<input style='height:22px;width:20px;' type='button' title='Save' value='S' onclick=saveGridData('"+ options.rowId + "');>";
				ce = "<input style='height:22px;width:20px;' type='button' title='Cancel' value='C' onclick=restoreGridData('"+ options.rowId + "');>";
				return ae+be+se+ce;
                
            }
			 var impl_class='';
			 $("#taskImplClass option").each(function(){
					if($(this).val().length >0){
						impl_class +=  $(this).val() + ':' + $(this).text() + ';';
					}	
					
				});	
			 impl_class=impl_class.substr(0,impl_class.length-1);
			 var lastSel_sub =-1 ;
			
			$("#taskDetail").jqGrid({
				url: '',
				datatype: 'local',
				editurl: 'clientArray',
				mtype: 'POST',
				colNames: ['Created','Id','Name','Impl Class','Cron Expression','Concurrent','Clustered','HOST NAME','Enabled','Action'],
				colModel: [
				{name:'created',index:'created', width:20,editable:false,hidden:true},
				{name:'taskDetailId',index:'taskDetailId', width:20,editable:false,hidden:true},
				{name:'taskName',index:'taskName', width:150,editable:true},
				{name:'implClass',index:'implClass', width:150,editable:true, edittype:"select",formatter:'select', 
					 editoptions:{value:impl_class}, sortable:true,search:false},
				{name:'cronExpression',index:'cronExpression', width:150,editable:true, sortable:false,search:false},
				{ name:'concurrent',index:'concurrent',width:60,align:'center',editable:true,edittype:'checkbox',editoptions:{value:"true:false"},
						formatter:"checkbox",search:false, sortable:false},
				{ name:'clustered',index:'clustered',width:60,align:'center',editable:true,edittype:'checkbox',editoptions:{value:"true:false"},
						formatter:"checkbox",search:false, sortable:false},
				{name:'hostName',index:'hostName', width:150,editable:true,edittype:"select",formatter:'select', 
							 editoptions:{value:":"+"Select"+";"+hostName_data}, sortable:true,search:false},
				{ name:'enabled',index:'enabled',width:60,align:'center',editable:true,edittype:'checkbox',editoptions:{value:"true:false"},
						formatter:"checkbox",search:false, sortable:false},
				{name : 'action', index : 'action', width : 90, editable : false, formatter : showActionButtons, search : false}
				],
				height: 'auto', viewrecords: true, caption: "Task Detail",gridview : true, loadui : 'block',
				loadonce : true,pager: '#taskDetail_pager',rowNum:20,sortorder : "asc",shrinkToFit:true,
			      rowList:[20,50,100,200],
			      afterInsertRow: function(rowid,rowdata,rowelem){
			    	  $('#taskDetail').setCell(rowid,'taskName','','',{editable:true});
			      },
		   	ignoreCase: true,
				  jsonReader : {
		          root: "rows",
		          page: "page",
		          total: "total",
		          records: "records",  
		          repeatitems: false,
		          
		          cell: "cell",
		          id:"id"
				  },
				  
				  subGrid: true,
				  subGridOptions: {
				        "plusicon": "ui-icon-triangle-1-e",
				        "minusicon": "ui-icon-triangle-1-s",
				        "openicon": "ui-icon-arrowreturn-1-e",
				        "expandOnLoad" : true,
				  },
				  subGridRowExpanded: function (subgrid_id, row_id) {
					  	lastSel = -1
				        var subgrid_table_id = subgrid_id + "_t";
				        $("#" + subgrid_id).html("<table id='" + subgrid_table_id + "' class='scroll'></table>");
				        var rowData = $('#taskDetail').jqGrid('getRowData',row_id);
				        
				        $("#" + subgrid_table_id).jqGrid({
				        	url: '${path.http}/admin/task/getTaskParamInfo?taskName='+encodeURIComponent(rowData.taskName),
							datatype: 'json',
							editurl: 'clientArray',
							mtype: 'POST',
							colNames: ['Created','Id','Param Name','Param Value',''],
							colModel: [
							{name:'created',index:'created', width:20,editable:false,hidden:true},
							{name:'taskParamId',index:'taskParamId', width:20,editable:false,hidden:true},
							{name:'paramName',index:'paramName', width:100,editable:true},
							{name:'paramValue',index:'paramValue', width:100,editable:true},
							{name:'act',index:'act', width:50,editable:false,formatter: function (cellvalue, options, rowObject) { 
								return  showAddRemoveButtons(cellvalue, options, rowObject,subgrid_table_id);}
								,sortable:false,search:false},
							],
							loadComplete: function(data){
								if(rowData.taskDetailId == ''){
									$("#" + subgrid_table_id).showCol('act');
									$('#'+subgrid_table_id).jqGrid('addRow',{'position':'last'});
									return;
								}
								$("#" + subgrid_table_id).hideCol('act');
								if ( data.records == 0) {
									$('#taskDetail').jqGrid("collapseSubGridRow",row_id);
								}
							},
							 onSelectRow: function (id) {
								   grid = jQuery("#"+subgrid_table_id);
							        if (id && id !== lastSel_sub ) {
							        	if(lastSel_sub != -1){
							        		 grid.jqGrid('saveRow', lastSel_sub);
							        	}
							            lastSel_sub = id;
							        }
							        if(rowData.taskDetailId != ''){
								        var c_paramName = grid.jqGrid('getColProp','paramName');
								        c_paramName.editable=false;
								        grid.jqGrid('editRow', id, {
								            keys: true,
								        });
								        c_paramName.editable=true;
							        }else{
							        	 grid.jqGrid('editRow', id, {
									            keys: true,
									        });
							        }
							        
							    },
							jsonReader : {
						          root: 'rows',
						          page: "page",
						          total: "total",
						          records: "records",  
						          repeatitems: false,
								  },
							height: 'auto', viewrecords: true,gridview : true, loadui : 'block',
							loadonce : true,rowNum:5,sortorder : "asc",shrinkToFit:true,
						      rowList:[5, 10, 50, 100, 200, 500, 1000, 5000],
					   	ignoreCase: true,
				        });
				    }
				});
			
			$("#taskDetail").jqGrid('navGrid','#taskDetail_pager',
					{edit:false,add:false,del:false,search:false,refresh:false,width: auto},
					{},{},{},
					{}
					);	
			
		});
			
		</script>

	</tiles:putAttribute>
	<tiles:putAttribute name="subheader">
		<tiles:putAttribute name="systemMessage">
		</tiles:putAttribute>
		<tiles:insertTemplate template="/views/admin/admintasks/sidebar.jsp">
			<tiles:putAttribute name="active" value="taskAdmin" />
		</tiles:insertTemplate>
	</tiles:putAttribute>

	<tiles:putAttribute name="sidebar">
		<tiles:insertTemplate template="/views/admin/admintasks/task-subheader.jsp">
			<tiles:putAttribute name="active" value="taskDetailUpdate" />
		</tiles:insertTemplate>
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
		<div class="main-content lfloat">
			<div class="container" style="width: 85%;">
			<div class="form-group" style="padding-top: 25px; padding-bottom: 25px;">
			<span> Select Task Impl Class:</span>&nbsp;&nbsp;&nbsp;&nbsp;
				<select id="taskImplClass">
				<option value="">All</option>
					<c:forEach items="${implClasses}" var = "implClass">
						<option value="${implClass.value}">${implClass.key}</option>
					</c:forEach>	
				</select>&nbsp;&nbsp;
				<input class="btn btn-primary" id ="searchTaskDetail" type="button" value="Search"> 
				</div>
					<div id="taskUpdateDiv">
								<table id="taskDetail"></table>
								<div id="taskDetail_pager"></div>
								
								<br><br><br>
					</div>
			</div>
			
		</div>
	</tiles:putAttribute>
</tiles:insertTemplate>