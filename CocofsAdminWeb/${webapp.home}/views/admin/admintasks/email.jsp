<%@ include file="/tld_includes.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<tiles:insertTemplate template="/views/layout/base.jsp">
	<tiles:putAttribute name="title" value="Template Admin" />

	<tiles:putAttribute name="systemMessage">
		<div class="system-message"></div>
	</tiles:putAttribute>

	<tiles:putAttribute name="extraScripts">


	</tiles:putAttribute>
	<tiles:putAttribute name="sidebar">
		<tiles:insertTemplate template="/views/admin/admintasks/sidebar.jsp">
		</tiles:insertTemplate>
	</tiles:putAttribute>
	<tiles:putAttribute name="systemMessage">
		<div class="system-message">
			<input type="hidden" value="${message }" id="messageHiddenField" />
		</div>
	</tiles:putAttribute>


	<tiles:putAttribute name="subheader">
		<tiles:insertTemplate
			template="/views/admin/admintasks/emailtemplate-subheader.jsp">
		</tiles:insertTemplate>
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
		<div class="main-content lfloat">
			<div class="container" style="width: 85%;">


				<form:form commandName="emailTemplateAdminForm" method="POST"
					action="${path.http}/admin/template/updateEmailTemplate"
					name="emailTemplateAdminForm" class="form-horizontal">
					<div class="control-group">
						<label class="control-label" for="emailTemplate.name"> <b>Email
								Template Name:<form:errors path="emailTemplate.name"
									class="errorMessage" />
						</b>
						</label>
						<div class="controls">
							<form:select path="emailTemplate.name" id="emailTemp"
								class="input-xxlarge">
								<c:forEach
									items="${cache.getCache('emailTemplateCache').getEmailTemplates()}"
									var="emailTemp">
									<form:option value="${emailTemp.name}">${emailTemp.name}</form:option>
								</c:forEach>
							</form:select>
							<input type="button" name="fetchData" id="fetchEmailData"
								value="Fetch" class="btn" />
						</div>
					</div>

					<c:if test="${emailTemplateAdminForm.emailTemplate.id != null}">
						<form:hidden path="emailTemplate.id" />
						<form:hidden path="emailTemplate.created" />
						<div class="control-group">
							<label class="control-label" for="emailTemplate.subjectTemplate">
								<b>Subject Template:<form:errors
										path="emailTemplate.subjectTemplate" class="errorMessage" /></b>
							</label>
							<div class="controls">
								<form:input path="emailTemplate.subjectTemplate"
									class="input-xxlarge" />
							</div>
						</div>

						<div class="control-group">
							<label class="control-label" for="emailTemplate.bodyTemplate">
								<b>Body Template:<form:errors
										path="emailTemplate.bodyTemplate" class="errorMessage" /></b>
							</label>
							<div class="controls">
								<form:textarea path="emailTemplate.bodyTemplate"
									class="input-xxlarge" />
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="emailTemplate.from"> <b>From:<form:errors
										path="emailTemplate.from" class="errorMessage" /></b>
							</label>
							<div class="controls">
								<form:input path="emailTemplate.from" class="input-xxlarge" />
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="emailTemplate.to"> <b>To:<form:errors
										path="emailTemplate.to" class="errorMessage" /></b>
							</label>
							<div class="controls">
								<form:input path="emailTemplate.to" class="input-xxlarge" />
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="emailTemplate.cc"> <b>CC:<form:errors
										path="emailTemplate.cc" class="errorMessage" /></b>
							</label>
							<div class="controls">
								<form:input path="emailTemplate.cc" class="input-xxlarge" />
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="emailTemplate.bcc"> <b>BCC:<form:errors
										path="emailTemplate.bcc" class="errorMessage" /></b>
							</label>
							<div class="controls">
								<form:input path="emailTemplate.bcc" class="input-xxlarge" />
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="emailTemplate.replyTo">
								<b>Reply To:<form:errors path="emailTemplate.replyTo"
										class="errorMessage" /></b>
							</label>
							<div class="controls">
								<form:input path="emailTemplate.replyTo" class="input-xxlarge" />
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="emailTemplate.emailChannelId">
								<b>Email Channel:</b>
							</label>
							<div class="controls">
								<form:select path="emailTemplate.emailChannelId"
									class="input-xxlarge">
									<c:forEach items="${emailTemplateAdminForm.channels}"
										var="emailChan">
										<form:option value="${emailChan.channelID}">${emailChan.channelName}</form:option>
									</c:forEach>

								</form:select>
							</div>
						</div>
						<div align="center">
							<input type="submit" name="submit" value="Submit"
								class="submit btn btn-primary" />
						</div>
					</c:if>
				</form:form>
			</div>
			<div id="errorMessage" class="alert-error">${message}</div>
		</div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
		<%-- Activate relevant sidebar function   --%>
		<%-- Activate relevant subheader function --%>
		<script type="text/javascript">
			function hideSidebar() {
				$(".subheader").css({
					"left" : "-11%"
				});
				$(".functionAnchor").css({
					"display" : "none"
				});
				$(".sidebar").css({
					"left" : "1%",
					"width" : "99%"
				});
				$(".main-content").css({
					"left" : "1%",
					"width" : "98%"
				});
			}
			function showSidebar() {
				$(".subheader").css({
					"left" : "0%"
				});
				$(".functionAnchor").css({
					"display" : "block"
				});
				$(".sidebar").css({
					"left" : "12%",
					"width" : "88%"
				});
				$(".main-content").css({
					"left" : "12%",
					"width" : "87%"
				});
			}

			$(document)
					.ready(
							function() {
								var subheader = true;
								$(".subheaderToggle").click(function() {
									if (subheader) {
										hideSidebar();
										$(".subheaderToggleContent").html(">");
										subheader = false;
									} else {
										showSidebar();
										$(".subheaderToggleContent").html("<");
										subheader = true;
									}

								});
								/* check sidebar jsp for relevant div ids
								 */
								$('#sidebar-fn-emailedit').addClass('active');
								$('#header-nav-admintasks').addClass('active');
								$('#fetchEmailData')
										.click(
												function() {
													window.location.href = "/admin/template/fetchEmailData?name="
															+ $('#emailTemp')
																	.val();
												});
								/* check subheader jsp for relevant div ids
								 */
								/* $('#subheader-fn-usrmanagement').addClass('active') */

							});
		</script>


	</tiles:putAttribute>
</tiles:insertTemplate>