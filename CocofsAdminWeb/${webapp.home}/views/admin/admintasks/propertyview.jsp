	<%@ include file="/tld_includes.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<tiles:insertTemplate template="/views/layout/base.jsp">
	<tiles:putAttribute name="title" value="Cocofs Properties" />

	<tiles:putAttribute name="systemMessage">
		<div class="system-message"></div>
	</tiles:putAttribute>

	<tiles:putAttribute name="extraScripts">
    <link href="${path.css('chosen/chosen.css')}" rel="stylesheet"
    type="text/css" />
    <link rel="stylesheet" type="text/css"
            href="${path.css('snapdeal/ui.jqgrid.css')}" />

    <script type="text/javascript"
    src="${path.js('chosen/chosen.jquery.min.js')}"></script>
    


	</tiles:putAttribute>
	<tiles:putAttribute name="sidebar">
		<tiles:insertTemplate template="/views/admin/admintasks/sidebar.jsp">
		</tiles:insertTemplate>
	</tiles:putAttribute>
	<tiles:putAttribute name="systemMessage">
		<div class="system-message">
			<input type="hidden" value="${message }" id="messageHiddenField"/>
		</div>
	</tiles:putAttribute>


	<tiles:putAttribute name="subheader">
		<tiles:insertTemplate template="/views/admin/admintasks/cacheview-subheader.jsp">
		</tiles:insertTemplate>
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
		<div class="main-content lfloat">
			<div class="container" style="width: 85%;">
			<div>
			<form id="access-form" class="form-horizontal" method="get"
								action='#'>

								<br />

								<div class="control-group">
									<div class="controls">
										Property Name : <input type="text" id="frm-propertyname" />
										Property Value : <input type="text" id="frm-propertyvalue" />
									</div>
								</div>

								<div class="control-group">
									<div class="controls">
										<button id="fetch-getproperty" type="button"
											class="btn btn-primary">Get Property</button>
										<button id="fetch-updateproperty" type="button" 
												class="btn btn-primary">Update Property</button>	
										<button id="fetch-clearproperty" type="button"
											class="btn btn-primary">Delete Property </button>
									</div>
								</div>
								
								<br />

							</form>

			</div>
            <div id="all-stats-div"></div>
            


            </div>
		</div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
	<%-- Activate relevant sidebar function   --%>
	<%-- Activate relevant subheader function --%>
		<script type="text/javascript">
			function hideSidebar() {
				$(".subheader").css({
					"left" : "-11%"
				});
				$(".functionAnchor").css({
					"display" : "none"
				});
				$(".sidebar").css({
					"left" : "1%",
					"width" : "99%"
				});
				$(".main-content").css({
					"left" : "1%",
					"width" : "98%"
				});
			}
			function showSidebar() {
				$(".subheader").css({
					"left" : "0%"
				});
				$(".functionAnchor").css({
					"display" : "block"
				});
				$(".sidebar").css({
					"left" : "12%",
					"width" : "88%"
				});
				$(".main-content").css({
					"left" : "12%",
					"width" : "87%"
				});
			}
			
			function getPropertyData(urltoget) {
				var responseObj = {
					success : false,
					message : 'Dummy message',
					obj : null
				};
				$.ajax({
					dataType : 'json',
					url : urltoget,
					async : false,
					type : 'GET',
					success : function(data, textStatus, jqXHR) {
						if(data == null){
							responseObj.message = "Requested data doesn't exists";
							return responseObj; 
						}
						if ((!data.code) || (data.code == 200)) {
							/* We got valid response */
							responseObj.success = true;
							responseObj.obj = data;
							responseObj.message = data.message;
						} else {
							responseObj.message = data.message;
						}
					},
					error : function(jqXHR, textStatus, errorThrown) {
						responseObj.message = textStatus + errorThrown
					},
				});
				return responseObj;
			}
			
			function getProperty(propName) {
				var url = '/admin/property/getPropertyValue?propName=' + encodeURIComponent(propName);
				var resp = getPropertyData(url);
				
				if (resp.success) {
 						alert(JSON.stringify(resp.obj));
				} else {
					alert("Could not get property data : " + resp.message);
				}
			}
			
			function updateProperty(propName,propValue) {
				var url = '/admin/property/saveProperty?propName=' + encodeURIComponent(propName) + "&propValue="+encodeURIComponent(propValue);
				var resp = getPropertyData(url);
				if (resp.success) {
 						alert(JSON.stringify(resp.obj));
				} else {
					alert("Could not Save Property data : " + resp.message);
				}
			}

			function clearProperty(propName) {
				var url = '/admin/property/clearPropertyWithName?propName=' + encodeURIComponent(propName);
				
				var resp = getPropertyData(url);
				if (resp.success) {
					alert(JSON.stringify(resp.obj));
				} else {
					alert("Could not clear property data : " + resp.message);
				}
			}
			

			$(document).ready(function() {
				var subheader = true;
				$(".subheaderToggle").click(function() {
					if (subheader) {
						hideSidebar();
						$(".subheaderToggleContent").html(">");
						subheader = false;
					} else {
						showSidebar();
						$(".subheaderToggleContent").html("<");
						subheader = true;
					}

				});
				/* check sidebar jsp for relevant div ids
				 */
				$('#sidebar-fn-propertyview').addClass('active');
	
				$('#fetch-getproperty').click(function() {
					var propName = $("#frm-propertyname").val().trim();
					if(propName==null || propName==''){
						alert("Please specify the property name");
						return;
					}
					$("#frm-propertyname").val("");
					getProperty(propName);
				});
				
				$('#fetch-updateproperty').click(function() {
					var propName = $("#frm-propertyname").val().trim();
					var propValue = $("#frm-propertyvalue").val().trim();
					if(propName==null || propName==''){
						alert("Please specify the property name");
						return;
					}
					if(propValue==null || propValue==''){
						alert("Please specify the property value");
						return;
					}
					$("#frm-propertyname").val("");
					$("#frm-propertyvalue").val("");
					updateProperty(propName,propValue);
				});

				$('#fetch-clearproperty').click(function() {
					var propName = $("#frm-propertyname").val().trim();
					if(propName==null || propName==''){
						alert("Please specify the property name");
						return;
					}
					$("#frm-propertyname").val("");					
					clearProperty(propName);
				});

				$('#sidebar-fn-propertyview').addClass('active');

			});
		</script>		
	</tiles:putAttribute>
</tiles:insertTemplate>