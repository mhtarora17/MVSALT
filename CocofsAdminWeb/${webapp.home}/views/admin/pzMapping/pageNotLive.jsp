<%@ include file="/tld_includes.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<tiles:insertTemplate template="/views/layout/base.jsp">
<style>
.error-text {
	font-size: 30px;
	font-weight: normal;
	cursor: pointer;
	padding-left: 100px;
    padding-top: 100px;
}

</style>
	<tiles:putAttribute name="title" value="Page not found" />

	
	
	<tiles:putAttribute name="systemMessage">
	</tiles:putAttribute>
	<tiles:putAttribute name="subheader">
	</tiles:putAttribute>

	

	<tiles:putAttribute name="body">
	<div class="main-content lfloat">
					
					<div class="error-cont">
						<div class="error-text">
							${message}
						</div>
						<div class="error-text-line">&nbsp;</div>
						<div class="clear"></div>
					</div>
	</div>

	</tiles:putAttribute>
</tiles:insertTemplate>
