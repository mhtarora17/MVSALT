<%@ include file="/tld_includes.jsp"%>


<div id="subheader" class="subheader"
	style="background-image: url(${path.resources('img/background/bg_blue.png')});">
	<div class="tabbable">
		<!-- Only required for left/right tabs -->
		<div class="subheaderTab">
		    <div id="sidebar-pa-upload" onclick="javascript:window.location.href='pa'">Product Attributes</div>
			<div id="sidebar-fm-upload" onclick="javascript:window.location.href='fm'">Fulfilment Model</div>
			<div id="sidebar-tx-upload" onclick="javascript:window.location.href='tx'">Tax Rate</div>
		</div>
	</div>
	<div class="subheaderToggle">
		<div class="subheaderToggleContent">&lt</div>
	</div>
</div>