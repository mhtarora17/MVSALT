<%@ include file="/tld_includes.jsp"%>


<div id="subheader" class="subheader"
	style="background-image: url(${path.resources('img/background/bg_blue.png')});">
	<div class="tabbable">
		<!-- Only required for left/right tabs -->
		<div class="subheaderTab">
			<sec:authorize
				ifAnyGranted="admin,tech,packmanboxcreator,packmanboxeditor">
				
				<sec:authorize ifAnyGranted="admin,tech,packmanboxeditor">
					<div id="sidebar-fn-cartonview"
						onclick="javascript:window.location.href='/admin/packman/viewCarton'">Carton</div>
				</sec:authorize>
				
				<sec:authorize ifNotGranted="admin,tech,packmanboxeditor">
					<div id="sidebar-fn-cartonview"
						onclick="javascript:window.location.href='/admin/packman/createCarton'">Carton</div>
				</sec:authorize>
				
			</sec:authorize>
			
			<sec:authorize
				ifAnyGranted="admin,tech,packmanboxcreator,packmanboxeditor">
				
				<sec:authorize ifAnyGranted="admin,tech,packmanboxeditor">
					<div id="sidebar-fn-polybagview"
						onclick="javascript:window.location.href='/admin/packman/viewPolybag'">Polybag</div>
				</sec:authorize>
				
				<sec:authorize ifNotGranted="admin,tech,packmanboxeditor">
					<div id="sidebar-fn-polybagview"
						onclick="javascript:window.location.href='/admin/packman/createPolybag'">Polybag</div>
				</sec:authorize>
				
				
			</sec:authorize>

			<sec:authorize
				ifAnyGranted="admin,tech,packmanslabeditor,packmanslabview">
					<sec:authorize ifAnyGranted="admin,tech,packmanslabview">
						<div id="sidebar-fn-cartonSlab"
							 onclick="javascript:window.location.href='/admin/packman/viewCartonSlab'">Carton Slabs</div>
					</sec:authorize>
					<sec:authorize ifNotGranted="admin,tech,packmanslabview">
						<div id="sidebar-fn-cartonSlab"
							 onclick="javascript:window.location.href='/admin/packman/createCartonSlab'">Carton Slabs</div>
					</sec:authorize>
					
			</sec:authorize>

			<sec:authorize
				ifAnyGranted="admin,tech,packmanslabeditor,packmanslabview">
				<sec:authorize ifAnyGranted="admin,tech,packmanslabview">
					<div id="sidebar-fn-polybagSlab"
						onclick="javascript:window.location.href='/admin/packman/viewPolybagSlab'">Polybag Slabs</div>
				</sec:authorize>
				
				<sec:authorize ifNotGranted="admin,tech,packmanslabview">
						<div id="sidebar-fn-polybagSlab" 
							onclick="javascript:window.location.href='/admin/packman/createPolybagSlab'">Polybag Slabs</div>
				</sec:authorize>
					
			</sec:authorize>
			
			
			
			<sec:authorize ifAnyGranted="admin,tech,packmansurfaceeditor,packmansurfacecreator">
				
				<sec:authorize ifAnyGranted="admin,tech,packmansurfaceeditor">
					<div id="sidebar-fn-surfaceview"
						onclick="javascript:window.location.href='/admin/packman/viewSurface'">Surface</div>
				</sec:authorize>
				
				<sec:authorize ifNotGranted="admin,tech,packmansurfaceeditor">
					<div id="sidebar-fn-surfaceview"
						onclick="javascript:window.location.href='/admin/packman/createSurface'">Surface</div>
				</sec:authorize>
			</sec:authorize>	
			
			
			<sec:authorize ifAnyGranted="admin,tech">
				<div id="sidebar-fn-rearrangeSlab"
					onclick="javascript:window.location.href='/admin/packman/rearrangeSlabs'">
					Re-arrange Slabs</div>
			</sec:authorize>

		</div>
	</div>
	<div class="subheaderToggle">
		<div class="subheaderToggleContent">&lt</div>
	</div>
</div>