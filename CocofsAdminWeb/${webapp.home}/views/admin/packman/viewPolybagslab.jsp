<%@ include file="/tld_includes.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<tiles:insertTemplate template="/views/layout/base.jsp">
	<tiles:putAttribute name="title" value="PolybagSlab Admin" />

	<tiles:putAttribute name="extraScripts">
		<link rel="stylesheet" type="text/css"
			href="${path.css('snapdeal/ui.jqgrid.css')}" />
		<script type="text/javascript"
			src="${path.js('jquery/grid.locale-en.js')}"></script>
		<script type="text/javascript"
			src="${path.js('jquery/jquery.jqGrid.min.js')}"></script>
		<script type="text/javascript">
			var grid = $('#polybagSlabDetail');

			$(document)
					.ready(
							function() {

								$('#sidebar-fn-polybagSlab').addClass(
										'active');
								$('#header-nav-packman').addClass('active');

								$("#polybagSlabDetail")
										.jqGrid(
												{
													url : '${path.http}/admin/packman/getPolybagSlabInfo',
													datatype : 'local',
													editurl : 'clientArray',
													mtype : 'POST',
													colNames : [ 'Slab',
															'Slab Size (gm)',
															'Slab Min (gm)',
															'Slab Max (gm)',
															'Polybags' ],
													colModel : [ {
														name : 'id',
														index : 'id',
														width : 100,
														editable : false,
													},

													{
														name : 'slabSize',
														index : 'slabSize',
														align : 'center',
														width : 150,
														editable : false,
														sortable : false
													}, {
														name : 'slabMin',
														index : 'slabMin',
														align : 'center',
														width : 150,
														editable : false,
														sortable : false
													}, {
														name : 'slabMax',
														index : 'slabMax',
														width : 150,
														align : 'center',
														width : 150,
														editable : false,
														sortable : false
													}, {
														name : 'polybagBoxes',
														index : 'polybagBoxes',
														width : 300,
														align : 'center',
														editable : false,
														sortable : false
													} ],
													height : 'auto',
													viewrecords : true,
													caption : "PolybagSlab Detail",
													gridview : true,
													loadui : 'block',
													loadonce : true,
													pager : '#polybagSlabDetail_pager',
													rowNum : 20,
													sortorder : "asc",
													shrinkToFit : true,
													rowList : [20, 40, 60, 100],
													ignoreCase : true,
													jsonReader : {
														root : "rows",
														page : "page",
														total : "total",
														records : "records",
														repeatitems : false,

														cell : "cell",
														id : "id"
													},

												});

								$("#polybagSlabDetail").jqGrid('navGrid',
										'#polybagSlabDetail_pager', {
											edit : false,
											add : false,
											del : false,
											search : false,
											refresh : false
										}, {}, {}, {}, {});

								var searchUrl = '${path.http}/admin/packman/getPolybagSlabInfo';
								$("#polybagSlabDetail").jqGrid('setGridParam', {
									url : searchUrl,
									datatype : 'json'
								}).trigger('reloadGrid');

							});
		</script>

	</tiles:putAttribute>
	<tiles:putAttribute name="subheader">			
		<tiles:insertTemplate template="/views/admin/packman/sidebar.jsp">
			<tiles:putAttribute name="active" value="polybagSlabAdmin" />
		</tiles:insertTemplate>
	</tiles:putAttribute>
	
	

	<tiles:putAttribute name="sidebar">
		<tiles:insertTemplate template="/views/admin/packman/polybagslab-subheader.jsp">
			<tiles:putAttribute name="active" value="viewPolybagSlab" />
		</tiles:insertTemplate>
	</tiles:putAttribute>


	<tiles:putAttribute name="body">
		<div class="main-content lfloat">
			<div class="container subheaderDiv" style="width: 100%;">
				<div id="polybagSlabUpdateDiv">
					<table id="polybagSlabDetail"></table>
					<div id="polybagSlabDetail_pager"></div>

					<br> <br> <br>
				</div>
			</div>

		</div>
	</tiles:putAttribute>
</tiles:insertTemplate>