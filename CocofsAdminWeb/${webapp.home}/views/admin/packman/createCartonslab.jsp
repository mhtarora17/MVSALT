<%@ include file="/tld_includes.jsp"%>
<style>
.tb td {
	font-weight: bold;
	font-size: small;
}

select {
	width: 170px;
}
</style>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<tiles:insertTemplate template="/views/layout/base.jsp">


	<tiles:putAttribute name="title" value="Create CartonSlab" />

	<tiles:putAttribute name="subheader">
			
		<tiles:insertTemplate template="/views/admin/packman/sidebar.jsp">
			<tiles:putAttribute name="active" value="packman" />
		</tiles:insertTemplate>
	</tiles:putAttribute>
	
	

	<tiles:putAttribute name="sidebar">
		<tiles:insertTemplate template="/views/admin/packman/cartonslab-subheader.jsp">
			<tiles:putAttribute name="active" value="createCartonSlab" />
		</tiles:insertTemplate>
	</tiles:putAttribute>
	
	

	<tiles:putAttribute name="extraScripts">
		<link href="${path.css('chosen/chosen.css')}" rel="stylesheet"
			type="text/css" />
		<link rel="stylesheet" type="text/css"
			href="${path.css('snapdeal/ui.jqgrid.css')}" />

		<script type="text/javascript"
			src="${path.js('chosen/chosen.jquery.min.js')}"></script>

	</tiles:putAttribute>


	<tiles:putAttribute name="deferredScript">
		<script>
			if (typeof Snapdeal == 'undefined') {
				Snapdeal = {};
			};
			Snapdeal.getStaticPath = function(path) {
				return '${path.http}' + path;
			}

			Snapdeal.getResourcePath = function(path) {
				return '${path.resources("")}' + path;
			}
		</script>

		<script type="text/javascript"
			src="${path.js('snapdeal/validations.js')}"></script>
		<script>
			$(document).ready(function() {

 				$('#sidebar-fn-cartonSlab').addClass('active');
 				$('#header-nav-packman').addClass('active');

				$('#submitButton').click(function() {
					$("#cartonSlabForm").validate({
						rules : {
							slabCode : {
								required : true,
							},
							slabSize : {
								required : true,
								maxlength : 6,
								number : true,
							},
						},
						messages : {
							slabCode : {
								required : "Please enter a slab Code",
							},
							slabSize : {
								required : "Please provide size of Slab",
								maxlength : "length can not exceed length 6",
							},
						}
					});

				});

			});
		</script>
	</tiles:putAttribute>


	<tiles:putAttribute name="body">
		<form:form commandName="cartonSlabForm" id="cartonSlabForm"
			method="POST" action='${path.http}/admin/packman/createNewCartonSlab'
			name="cartonSlabForm">
			<div class="main-content lfloat">
				<div class="container subheaderDiv" style="width: 100%;">
					<table class="tb" width="54%" style="padding: 0px 0px 0px 62px;">
						<tr>
							<td><span>Slab Size (gm)<span class=required>*</span>:
							</span></td>
							<td><input id="slabSize" class="input-text validateNotEmpty"
								name="slabSize" value="" tabindex="2"></td>
						</tr>
						<tr>
							<td><br /></td>
						</tr>
					</table>

					<div id="errorMessage" class="alert-error">${message}</div>
					<div class="control-group">
						<div align="left" style="padding-top: 24px; padding-left: 184px;">
							<input type="submit" class="btn btn-primary threeD"
								id="submitButton" value="Create CartonSlab">

						</div>
					</div>
				</div>
			</div>
		</form:form>
	</tiles:putAttribute>
</tiles:insertTemplate>



