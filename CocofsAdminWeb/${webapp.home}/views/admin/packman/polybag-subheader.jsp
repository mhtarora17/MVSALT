<%@ include file="/tld_includes.jsp"%>

<c:set var="active">
	<tiles:getAsString name="active" />
</c:set>

<div class="sidebar lfloat">
	<div id="functions">
		<div class="sidebarUL">
		<sec:authorize ifAnyGranted="admin,tech,packmanboxeditor">
				<c:choose>
					<c:when test="${active =='polybagDetailUpdate'}">
						<div class="activeFunction functionAnchor"
							onclick="javascript:window.location.href='/admin/packman/viewPolybag'">View/Edit Polybag</div>
					</c:when>
					<c:otherwise>
						<div class="functionAnchor"
							onclick="javascript:window.location.href='/admin/packman/viewPolybag'">View/Edit Polybag</div>
					</c:otherwise>
				</c:choose>
			</sec:authorize>
			
			<sec:authorize ifAnyGranted="admin,tech,packmanboxcreator">
				<c:choose>
					<c:when test="${active =='createPolybag'}">
						<div class="activeFunction functionAnchor"
							onclick="javascript:window.location.href='/admin/packman/createPolybag'">Create Polybag</div>
					</c:when>
					<c:otherwise>
						<div class="functionAnchor"
							onclick="javascript:window.location.href='/admin/packman/createPolybag'">Create Polybag</div>
					</c:otherwise>
				</c:choose>
		</sec:authorize>
			
			
		</div>

	</div>
</div>