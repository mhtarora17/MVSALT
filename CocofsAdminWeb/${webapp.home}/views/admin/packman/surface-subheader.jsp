<%@ include file="/tld_includes.jsp"%>

<c:set var="active">
	<tiles:getAsString name="active" />
</c:set>

<div class="sidebar lfloat">
	<div id="functions">
		<div class="sidebarUL">
		<sec:authorize ifAnyGranted="admin,tech,packmansurfaceeditor">
				<c:choose>
					<c:when test="${active =='SurfaceDetailUpdate'}">
						<div class="activeFunction functionAnchor"
							onclick="javascript:window.location.href='/admin/packman/viewSurface'">View/Edit Surface</div>
					</c:when>
					<c:otherwise>
						<div class="functionAnchor"
							onclick="javascript:window.location.href='/admin/packman/viewSurface'">View/Edit Surface</div>
					</c:otherwise>
				</c:choose>
			</sec:authorize>
			
			<sec:authorize ifAnyGranted="admin,tech,packmansurfacecreator">
				<c:choose>
					<c:when test="${active =='createSurface'}">
						<div class="activeFunction functionAnchor"
							onclick="javascript:window.location.href='/admin/packman/createSurface'">Create Surface</div>
					</c:when>
					<c:otherwise>
						<div class="functionAnchor"
							onclick="javascript:window.location.href='/admin/packman/createSurface'">Create Surface</div>
					</c:otherwise>
				</c:choose>
			</sec:authorize>
			
			
		</div>

	</div>
</div>