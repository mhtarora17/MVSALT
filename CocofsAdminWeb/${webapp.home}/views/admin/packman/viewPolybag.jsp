<%@ include file="/tld_includes.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<tiles:insertTemplate template="/views/layout/base.jsp">
	<tiles:putAttribute name="title" value="Polybag Admin" />

	<tiles:putAttribute name="systemMessage">
		<div class="system-message"></div>
	</tiles:putAttribute>

	<tiles:putAttribute name="extraScripts">
		<link rel="stylesheet" type="text/css"
			href="${path.css('snapdeal/ui.jqgrid.css')}" />
		<script type="text/javascript"
			src="${path.js('jquery/grid.locale-en.js')}"></script>
		<script type="text/javascript"
			src="${path.js('jquery/jquery.jqGrid.min.js')}"></script>
		<script type="text/javascript">
			var grid = $('#polybagDetail');
			function isValidDecimal(value, element) {
				  // allow any non-whitespace characters as the host part
				if(/^(?=.*[1-9])\d*(?:\.\d{1,2})?$/.test(value)){
					return true;
				}else{
					false;
				}
			};
			
			
			function validateRowData(inputData) {
				if (inputData.length == '' || inputData.breadth == ''
						|| inputData.height == '') {
					alert("One of the required field is empty");
					return false;
				}
				if(!isValidDecimal(inputData.length) || !isValidDecimal(inputData.breadth)  || !isValidDecimal(inputData.height) ){
					alert("Please specify a valid positive number for L,B,H having atmost 2 digits after decimal ");
					return false;
				}
				if(inputData.description.length >128){
					alert("maximum allowed size of desciption is 128");
					return false;
				}
				
				return true;
			}
			
			function restoreGridData(rowId) {
				var row = $("#polybagDetail").jqGrid('getRowData', rowId);
				if (row.polybagDetailId == '') {
					$('#polybagDetail').jqGrid("collapseSubGridRow", rowId);
					$("#polybagDetail").jqGrid('delRowData', rowId);
				} else {
					jQuery('#polybagDetail').restoreRow(rowId);
				}
				lastsel2 = 0000000;
			}

			var lastsel2;
			function editGridData(rowId) {
				if (rowId && rowId !== lastsel2) {
					jQuery('#polybagDetail').jqGrid('restoreRow', lastsel2);
					jQuery('#polybagDetail').jqGrid('editRow', rowId, true);
					lastsel2 = rowId;
				}

			}

			function saveGridData(rowId) {
				$("#polybagDetail").jqGrid('saveRow', rowId);
				var inputData = new Object();
				var row = $("#polybagDetail").jqGrid('getRowData', rowId);
				var polybagDetailDTO = new Object();
				if(!validateRowData(row)){
					$('#polybagDetail').jqGrid('editRow', rowId, true, null, null, 'clientArray');
					return false;
				}
				polybagDetailDTO.id = row.id;
				polybagDetailDTO.code = row.code;
				polybagDetailDTO.description = row.description;
				polybagDetailDTO.length = row.length;
				polybagDetailDTO.breadth = row.breadth;
				polybagDetailDTO.height = row.height;
				inputData.polybagDetailDTO = polybagDetailDTO;
				delete row.action;
				$
						.ajax({
							type : "POST",
							url : "${path.http}/json/admin/packman/updatePolybagDetail",
							dataType : 'json',
							contentType : 'application/json',
							data : JSON.stringify(inputData),
							async : false,
							success : function(resp) {
								if (resp.successful) {
									var searchUrl = '${path.http}/admin/packman/getPolybagInfo';
									$("#polybagDetail").jqGrid('setGridParam',
											{
												url : searchUrl,
												datatype : 'json'
											}).trigger('reloadGrid');
								} else {
									alert(resp.response);

								}
								$("#polybagDetail").jqGrid('setGridParam', {
									url : searchUrl,
									datatype : 'json'
								}).trigger('reloadGrid');
								lastsel2 = 0000000;
							},
							error : function(xhr, statusCode, error) {
								if (xhr.status == 200) {
									alert("Your session has expired. Please log in again in another tab/window and then retry here.");
									$('#polybagDetail').jqGrid('editRow',
											rowId, true, null, null,
											'clientArray');
								}
							}
						});
			}

			function deleteGridData(rowId) {
				var r = confirm("Do you really want to delete it?");
				if (r == false) {
				  return;
				}
				
				var row = $("#polybagDetail").jqGrid('getRowData', rowId);
				delete row.action;
				$
						.ajax({
							type : "POST",
							url : "${path.http}/json/admin/packman/deletePolybag",
							data : row.code,
							dataType : 'json',
							contentType : 'application/json',
							async : false,
							success : function(resp) {
								if (resp.successful) {
									var searchUrl = '${path.http}/admin/packman/getPolybagInfo';
									$("#polybagDetail").jqGrid('setGridParam',
											{
												url : searchUrl,
												datatype : 'json'
											}).trigger('reloadGrid');
								} else {
									alert(resp.response);

								}
								$("#polybagDetail").jqGrid('setGridParam', {
									url : searchUrl,
									datatype : 'json'
								}).trigger('reloadGrid');
								lastsel2 = 0000000;
							},
							error : function(xhr, statusCode, error) {
								if (xhr.status == 200) {
									alert("Your session has expired. Please log in again in another tab/window and then retry here.");
									$('#polybagDetail').jqGrid('editRow',
											rowId, true, null, null,
											'clientArray');
								}
							}
						});

			}
			function showActionButtons(cellValue, options, rowObject) {
				be = "<input style='height:22px;width:20px;' type='button' title='Edit' value='E' onclick=editGridData('"
						+ options.rowId + "');>";
				se = "<input style='height:22px;width:20px;' type='button' title='Save' value='S' onclick=saveGridData('"
						+ options.rowId + "');>";
				ce = "<input style='height:22px;width:20px;' type='button' title='Cancel' value='C' onclick=restoreGridData('"
						+ options.rowId + "');>";
				de = "<input style='height:22px;width:20px;' type='button' title='Delete' value='D' onclick=deleteGridData('"
						+ options.rowId + "');>";
				return be + se + ce + de;

			}

			$(document)
					.ready(
							function() {

								$('#sidebar-fn-polybagview').addClass('active');
								$('#header-nav-packman').addClass('active');

								$("#polybagDetail").jqGrid(
										{
											url : '',
											datatype : 'local',
											editurl : 'clientArray',
											mtype : 'POST',
											colNames : [ 'S.No', 'Code',
													'Slab', 'Vol.Weight(gm)',
													'Length(cm)',
													'Breadth(cm)',
													'Height(cm)',
													'Description', 'Action' ],
											colModel : [ {
												name : 'id',
												index : 'id',
												width : 20,
												editable : false,
											}, {
												name : 'code',
												index : 'code',
												align : 'center',
												width : 100,
												editable : false
											}, {
												name : 'slab',
												index : 'slab',
												align : 'center',
												width : 100,
												editable : false
											}, {
												name : 'volweight',
												index : 'volweight',
												width : 150,
												align : 'center',
												editable : false,
												search : false,
												sorttype:'number'
											}, {
												name : 'length',
												index : 'length',
												width : 100,
												editable : true,
												align : 'center',
												edittype : "text",
												editrules : {
													number : true,
													required:true,
													minValue : 0.01
												},
												sortable : false,
												search : false
											}, {
												name : 'breadth',
												index : 'breadth',
												width : 100,
												align : 'center',
												editable : true,
												edittype : "text",
												editrules : {
													number : true,
													required:true,
													minValue : 0.01
												},
												search : false,
												sortable : false
											}, {
												name : 'height',
												index : 'height',
												width : 100,
												align : 'center',
												editable : true,
												edittype : "text",
												editrules : {
													number : true,
													required:true,
													minValue : 0.01
												},
												search : false,
												sortable : false
											}, {
												name : 'description',
												index : 'description',
												width : 300,
												editable : true,
												edittype : "textarea",
												sortable : false,
												search : false
											}, {
												name : 'action',
												index : 'action',
												width : 90,
												editable : false,
												formatter : showActionButtons,
												search : false
											} ],
											height : 'auto',
											viewrecords : true,
											caption : "Polybag Detail",
											gridview : true,
											loadui : 'block',
											loadonce : true,
											pager : '#polybagDetail_pager',
											rowNum : 20,
											sortorder : "asc",
											shrinkToFit : true,
											rowList : [ 20, 40, 60, 100],
											ignoreCase : true,
											jsonReader : {
												root : "rows",
												page : "page",
												total : "total",
												records : "records",
												repeatitems : false,

												cell : "cell",
												id : "id"
											},

										});

								$("#polybagDetail").jqGrid('navGrid',
										'#polybagDetail_pager', {
											edit : false,
											add : false,
											del : false,
											search : false,
											refresh : false
										}, {}, {}, {}, {});

								var searchUrl = '${path.http}/admin/packman/getPolybagInfo';
								$("#polybagDetail").jqGrid('setGridParam', {
									url : searchUrl,
									datatype : 'json'
								}).trigger('reloadGrid');

							});
		</script>

	</tiles:putAttribute>
	<tiles:putAttribute name="subheader">
		<tiles:putAttribute name="systemMessage">
		</tiles:putAttribute>
		<tiles:insertTemplate template="/views/admin/packman/sidebar.jsp">
			<tiles:putAttribute name="active" value="polybagAdmin" />
		</tiles:insertTemplate>
	</tiles:putAttribute>

	<tiles:putAttribute name="sidebar">
		<tiles:insertTemplate
			template="/views/admin/packman/polybag-subheader.jsp">
			<tiles:putAttribute name="active" value="polybagDetailUpdate" />
		</tiles:insertTemplate>
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
		<div class="main-content lfloat">
			<div class="container subheaderDiv" style="width: 100%;">
				<div id="polybagUpdateDiv">
					<table id="polybagDetail"></table>
					<div id="polybagDetail_pager"></div>

					<br> <br> <br>
				</div>
			</div>

		</div>
	</tiles:putAttribute>
</tiles:insertTemplate>