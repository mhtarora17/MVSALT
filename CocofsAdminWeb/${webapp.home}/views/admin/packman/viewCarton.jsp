<%@ include file="/tld_includes.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<tiles:insertTemplate template="/views/layout/base.jsp">
	<tiles:putAttribute name="title" value="Carton Admin" />

	<tiles:putAttribute name="systemMessage">
		<div class="system-message"></div>
	</tiles:putAttribute>

	<tiles:putAttribute name="extraScripts">
		<link rel="stylesheet" type="text/css"
			href="${path.css('snapdeal/ui.jqgrid.css')}" />
		<script type="text/javascript"
			src="${path.js('jquery/grid.locale-en.js')}"></script>
		<script type="text/javascript"
			src="${path.js('jquery/jquery.jqGrid.min.js')}"></script>
		<script type="text/javascript">
			var grid = $('#cartonDetail');

			function isValidDecimal(value, element) {
				// allow any non-whitespace characters as the host part
				if (/^(?=.*[1-9])\d*(?:\.\d{1,2})?$/.test(value)) {
					return true;
				} else {
					false;
				}
			};

			function validateRowData(inputData) {
				if (inputData.length == '' || inputData.breadth == ''
						|| inputData.height == '') {
					alert("One of the required field is empty");
					return false;
				}
				if (!isValidDecimal(inputData.length)
						|| !isValidDecimal(inputData.breadth)
						|| !isValidDecimal(inputData.height)) {
					alert("Please specify a valid positive number for L,B,H having atmost 2 digits after decimal ");
					return false;
				}

				if (inputData.description.length > 128) {
					alert("maximum allowed size of desciption is 128");
					return false;
				}
				return true;
			}

			var lastsel2;
			function editGridData(rowId) {
				if (rowId && rowId !== lastsel2) {
					jQuery('#cartonDetail').jqGrid('restoreRow', lastsel2);
					jQuery('#cartonDetail').jqGrid('editRow', rowId, true);
					lastsel2 = rowId;
				}

			}

			function restoreGridData(rowId) {
				var row = $("#cartonDetail").jqGrid('getRowData', rowId);
				jQuery('#cartonDetail').restoreRow(rowId);
				lastsel2 = 0000000;
			}

			function saveGridData(rowId) {
				$("#cartonDetail").jqGrid('saveRow', rowId);
				var inputData = new Object();
				var row = $("#cartonDetail").jqGrid('getRowData', rowId);
				var cartonDetailDTO = new Object();
				if (!validateRowData(row)) {
					$('#cartonDetail').jqGrid('editRow', rowId, true, null,
							null, 'clientArray');
					return false;
				}
				cartonDetailDTO.id = row.id;
				cartonDetailDTO.code = row.code;
				cartonDetailDTO.description = row.description;
				cartonDetailDTO.length = row.length;
				cartonDetailDTO.breadth = row.breadth;
				cartonDetailDTO.height = row.height;
				inputData.cartonDetailDTO = cartonDetailDTO;
				delete row.action;
				$
						.ajax({
							type : "POST",
							url : "${path.http}/json/admin/packman/updateCartonDetail",
							dataType : 'json',
							contentType : 'application/json',
							data : JSON.stringify(inputData),
							async : false,
							success : function(resp) {
								if (resp.successful) {
									var searchUrl = '${path.http}/admin/packman/getCartonInfo';
									$("#cartonDetail").jqGrid('setGridParam', {
										url : searchUrl,
										datatype : 'json'
									}).trigger('reloadGrid');
								} else {
									alert(resp.response);

								}
								$("#cartonDetail").jqGrid('setGridParam', {
									url : searchUrl,
									datatype : 'json'
								}).trigger('reloadGrid');
								lastsel2 = 0000000;
							},
							error : function(xhr, statusCode, error) {
								if (xhr.status == 200) {
									alert("Your session has expired. Please log in again in another tab/window and then retry here.");
									$('#cartonDetail').jqGrid('editRow', rowId,
											true, null, null, 'clientArray');
								}
							}
						});
			}

			function deleteGridData(rowId) {

				var r = confirm("Do you really want to delete it?");
				if (r == false) {
					return;
				}

				var row = $("#cartonDetail").jqGrid('getRowData', rowId);
				delete row.action;
				$
						.ajax({
							type : "POST",
							url : "${path.http}/json/admin/packman/deleteCarton",
							data : row.code,
							dataType : 'json',
							contentType : 'application/json',
							async : false,
							success : function(resp) {
								if (resp.successful) {
									var searchUrl = '${path.http}/admin/packman/getCartonInfo';
									$("#cartonDetail").jqGrid('setGridParam', {
										url : searchUrl,
										datatype : 'json'
									}).trigger('reloadGrid');
								} else {
									alert(resp.response);

								}
								$("#cartonDetail").jqGrid('setGridParam', {
									url : searchUrl,
									datatype : 'json'
								}).trigger('reloadGrid');
								lastsel2 = 0000000;
							},
							error : function(xhr, statusCode, error) {
								if (xhr.status == 200) {
									alert("Your session has expired. Please log in again in another tab/window and then retry here.");
									$('#cartonDetail').jqGrid('editRow', rowId,
											true, null, null, 'clientArray');
								}
							}
						});

			}
			function showActionButtons(cellValue, options, rowObject) {
				be = "<input style='height:22px;width:20px;' type='button' title='Edit' value='E' onclick=editGridData('"
						+ options.rowId + "');>";
				se = "<input style='height:22px;width:20px;' type='button' title='Save' value='S' onclick=saveGridData('"
						+ options.rowId + "');>";
				ce = "<input style='height:22px;width:20px;' type='button' title='Cancel' value='C' onclick=restoreGridData('"
						+ options.rowId + "');>";
				de = "<input style='height:22px;width:20px;' type='button' title='Delete' value='D' onclick=deleteGridData('"
						+ options.rowId + "');>";
				return be + se + ce + de;

			}

			$(document)
					.ready(
							function() {

								$('#sidebar-fn-cartonview').addClass('active');
								$('#header-nav-packman').addClass('active');

								$("#cartonDetail").jqGrid(
										{
											url : '',
											datatype : 'local',
											editurl : 'clientArray',
											mtype : 'POST',
											colNames : [ 'S.No', 'Code',
													'Slab', 'Vol.Weight(gm)',
													'Length(cm)',
													'Breadth(cm)',
													'Height(cm)',
													'Description', 'Action' ],
											colModel : [ {
												name : 'id',
												index : 'id',
												width : 20,
												editable : false,
												sorttype : 'number'

											}, {
												name : 'code',
												index : 'code',
												align : 'center',
												width : 100,
												editable : false,
												sorttype : 'text'
											}, {
												name : 'slab',
												index : 'slab',
												align : 'center',
												width : 100,
												editable : false
											}, {
												name : 'volweight',
												index : 'volweight',
												width : 150,
												align : 'center',
												editable : false,
												search : false,
												sorttype : 'number'
											}, {
												name : 'length',
												index : 'length',
												width : 100,
												editable : true,
												align : 'center',
												edittype : "text",
												editrules : {
													number : true,
													required : true,
													minValue : 0.01,
												},
												sortable : false,
												search : false
											}, {
												name : 'breadth',
												index : 'breadth',
												width : 100,
												align : 'center',
												editable : true,
												edittype : "text",
												editrules : {
													number : true,
													required : true,
													minValue : 0.01
												},
												search : false,
												sortable : false
											}, {
												name : 'height',
												index : 'height',
												width : 100,
												align : 'center',
												editable : true,
												edittype : "text",
												editrules : {
													number : true,
													required : true,
													minValue : 0.01
												},
												search : false,
												sortable : false
											}, {
												name : 'description',
												index : 'description',
												width : 300,
												editable : true,
												edittype : "textarea",
												sortable : false,
												search : false
											}, {
												name : 'action',
												index : 'action',
												width : 90,
												editable : false,
												formatter : showActionButtons,
												search : false
											} ],
											height : 'auto',
											viewrecords : true,
											caption : "Carton Detail",
											gridview : true,
											loadui : 'block',
											loadonce : true,
											pager : '#cartonDetail_pager',
											rowNum : 20,
											sortorder : "asc",
											shrinkToFit : true,
											rowList : [ 20, 40, 60, 100 ],
											ignoreCase : true,
											jsonReader : {
												root : "rows",
												page : "page",
												total : "total",
												records : "records",
												repeatitems : false,

												cell : "cell",
												id : "id"
											},

										});

								$("#cartonDetail").jqGrid('navGrid',
										'#cartonDetail_pager', {
											edit : false,
											add : false,
											del : false,
											search : false,
											refresh : false
										}, {}, {}, {}, {});

								var searchUrl = '${path.http}/admin/packman/getCartonInfo';
								$("#cartonDetail").jqGrid('setGridParam', {
									url : searchUrl,
									datatype : 'json'
								}).trigger('reloadGrid');

							});
		</script>

	</tiles:putAttribute>
	<tiles:putAttribute name="subheader">
		<tiles:putAttribute name="systemMessage">
		</tiles:putAttribute>
		<tiles:insertTemplate template="/views/admin/packman/sidebar.jsp">
			<tiles:putAttribute name="active" value="cartonAdmin" />
		</tiles:insertTemplate>
	</tiles:putAttribute>

	<tiles:putAttribute name="sidebar">
		<tiles:insertTemplate
			template="/views/admin/packman/carton-subheader.jsp">
			<tiles:putAttribute name="active" value="cartonDetailUpdate" />
		</tiles:insertTemplate>
	</tiles:putAttribute>


	<tiles:putAttribute name="body">
		<div class="main-content lfloat">
			<div class="container subheaderDiv" style="width: 100%;">
				<div id="cartonUpdateDiv">
					<table id="cartonDetail"></table>
					<div id="cartonDetail_pager"></div>

					<br> <br> <br>
				</div>
			</div>

		</div>
	</tiles:putAttribute>
</tiles:insertTemplate>