<%@ include file="/tld_includes.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<tiles:insertTemplate template="/views/layout/base.jsp">
	<tiles:putAttribute name="title" value="Update User" />

	<tiles:putAttribute name="systemMessage">
		<div class="system-message"></div>
	</tiles:putAttribute>

	<tiles:putAttribute name="extraScripts">
	<script type="text/javascript" src="${path.js('mousetrap/mousetrap.min.js')}"></script>
		<script type="text/javascript">
		
		$(document).ready(function() {
			if ($('#messageHiddenField').val() != ''){
                smoke.signal($('#messageHiddenField').val(),2000);
             }

			
			$('#sidebar-fn-usrmgmt').addClass('active');
	        $('#header-nav-usermanagementview').addClass('active');
			
		 });
		
		Mousetrap.stopCallback= function(e, element, combo) {

            // if the element has the class "mousetrap" then no need to stop
            if ((' ' + element.className + ' ').indexOf(' mousetrap ') > -1) {
                return false;
            }

            // stop unless enter is pressed from with in scan text box
            return (element.id != 'searchEmail');
        }
		
		Mousetrap.bind('enter', function() { 
    		searchUser();
   	 });
		
		function checkEmail(str){
			var filter=/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
			if (filter.test(str)){
				testresults=true
			}
			else{
				testresults=false
			}
			return testresults
			}
		
			function fixArray(array){
				if ($.isArray(array)) {
					return array;
				} else {
					return [ array ];
				}
			}
			
			function resetOldForm(){
				  $('#userId').val('');
				  $('#userEmail').val('');
				  $('#password').val('');
				  $('#displayName').val('');
				  $('#firstName').val('');
				  $('#middleName').val('');
				  $('#lastName').val('');
				  document.getElementById("enabled").checked=false;
				  $('input[class="userRoles"]').attr('checked', false);
			}
			
			function submitForm(){
				$('#userCreationForm').submit();
			}
			
			function searchUser(){
				resetOldForm();
				var email = document.getElementById("searchEmail").value.trim();
				if(email!=undefined && email!='' && checkEmail(email)){
					var ajaxUrl =  "${path.http}/admin/usermanagement/searchUser?email="+email;
					  $ .ajax({url : ajaxUrl,type : "GET",dataType : 'json',success : function(data) {
						if(data.email != null){
							  $('#userData').fadeIn();
							  $('#userId').val(data.id);
							  $('#userEmail').val(data.email);
							  $('#password').val(data.password);
							  $('#displayName').val(data.displayName);
							  $('#firstName').val(data.firstName);
							  $('#middleName').val(data.middleName);
							  $('#lastName').val(data.lastName);
							  $('#fcCode').val(data.fcCode);
							  if(data.enabled){
								  document.getElementById("enabled").checked=true;
							  }else{
								  document.getElementById("enabled").checked=false;
							  }
							  var roles = fixArray(data.userRoles);
							  if(roles!=""){
								  for(var i=0;i<roles.length;i++){
									  document.getElementById(roles[i]).checked=true;
								  }
						 	 }
						  }else{
							  smoke.confirm("Do you wanna create a new user with email id "+ email, function(e){
								  if(e){
									  $('#userEmail').val(email);
									  $('#userData').fadeIn();
									  document.getElementById('enabled').checked=true;
								  }else{
									  return;
								  }
								  
								  
							  });
						  }
						  
					  }
				
				  });
				}else{
					smoke.alert("Please enter a valid email");
				}
			}
		</script>

	</tiles:putAttribute>
	<tiles:putAttribute name="systemMessage">
	</tiles:putAttribute>
	<tiles:putAttribute name="subheader">
		<tiles:insertTemplate template="/views/admin/user/sidebar.jsp">
			<tiles:putAttribute name="active" value="usermanagement" />
		</tiles:insertTemplate>
	</tiles:putAttribute>

	<tiles:putAttribute name="sidebar">
		<tiles:insertTemplate template="/views/admin/user/user-subheader.jsp">
			<tiles:putAttribute name="active" value="editUser" />
		</tiles:insertTemplate>
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
	<div class="main-content lfloat">
	<input type="hidden" value="${message}" id="messageHiddenField"/>
            <form:form class="form-horizontal" commandName="userCreationForm" id="userCreationForm" method="POST" action='${path.http}/admin/usermanagement/updateUser' name="userCreationForm">
    
			<div class="container" style="width: 100%;">
			<input type="hidden" name="userId" id = "userId"/> 
			<div id="searchUser" class="control-group" >
			<label class="control-label">EMAIL</label> 
            <div class="controls">
			<input id="searchEmail"  type="text" name="searchEmail" value="" tabindex="2">
			</div>
            </div>
            <div  class="control-group" >
            <div class="controls">
            <input type="button" class="btn btn-primary" value="Search User" onclick="searchUser();">
            
            </div>
            </div>
            			
			<div id="userData" style="display: none;">
				 <div class="control-group" >
				 <label class="control-label">EMAIL</label>
                 <div class="controls">
                 <input id="userEmail" readonly="readonly"  type="text" name="userEmail" value="" tabindex="2">
                 </div>
                 </div>
                 <div class="control-group" >
                 <label class="control-label">DISPLAY NAME</label>
                 <div class="controls">
                 <input id="displayName" type="text" name="displayName" value="" tabindex="2">
                 </div>
                 </div>
                 <div class="control-group" >
                 <label class="control-label">FIRST NAME</label>
                 <div class="controls">
                 <input id="firstName" type="text" name="firstName" value="" tabindex="2">
                 </div>
                 </div>
                 <div class="control-group" >
                 <label class="control-label">MIDDLE NAME</label>
                 <div class="controls">
                 <input id="middleName" type="text" name="middleName" value="" tabindex="2">
                 </div>
                 </div>
                 <div class="control-group" >
                 <label class="control-label">LAST NAME</label>
                 <div class="controls">
                 <input id="lastName" type="text" name="lastName" value="" tabindex="2">
                 </div>
                 </div>
                 <div class="control-group" >
                 <label class="control-label">ENABLED</label>
                 <div class="controls"><input id="enabled" type="checkbox" name="enabled" tabindex="2">
                 </div>
			     </div>
					 <div class="control-group">
					 
					  <table width="100%" border="1" cellspacing="3"> 
					  <thead>
					  <c:forEach items="${cache.getCacheByName('roleCache').roles}" var="roleDTO"  >
					  <th> <c:out value="${roleDTO.description }" /></th>
					  </c:forEach>
					  </thead>
					  <tbody>
					  <tr>	
					   <c:forEach items="${cache.getCacheByName('roleCache').roles}" var="roleDTO"  >
					   <td style="text-align: center;">
					   <span><input id = "${roleDTO.code}" style="vertical-align: super;" class="userRoles" name= "userRoles" type = "checkbox" value="${roleDTO.code}"/></span>
					   </td>
					   </c:forEach>
					  </tr>
					  </tbody>
					</table>
					 </div>
					   
					   <div class="control-group">
						  	<div align="center" style="padding-top: 10px;">
								<input type="button" class="btn btn-primary threeD" value="Update User" onclick="submitForm()">
							
							</div>
						</div>
						</div>
			</div>
	</div>
		</form:form>
	</tiles:putAttribute>
</tiles:insertTemplate>