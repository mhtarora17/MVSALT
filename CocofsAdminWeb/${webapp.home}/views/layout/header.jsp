<%@ include file="/tld_includes.jsp"%>
<style type="text/css">
.navbar .nav > li > a {
    
    padding: 5px !important;
    
}
}
</style>

<div class=" navbar navbar-inverse  cfloat">
	<div class="navbar-inner">
		<a class="brand" href="#">CoCoFS</a>
		<div id="header" class="header lpadding-normal wp82 lfloat"></div>

		<sec:authentication property="principal" var="user" />
		<ul class="nav">
			<sec:authorize ifAnyGranted="admin,tech,uploader,superuploader">
				<li id="header-nav-paupload"><a
					href="${path.http}/admin/productattribute/paupload"> Product
						Attribute</a>
				</li>
				<li class="divider-vertical"></li>
			</sec:authorize>
			<sec:authorize ifAnyGranted="admin,tech,ruleeditor">
				<li id="header-nav-rules"><a href="${path.http}/admin/rules/">
						Rules</a>
				</li>
				<li class="divider-vertical"></li>
			</sec:authorize>
			
			<sec:authorize ifAnyGranted="admin,tech,fmMappingUploader">
				<li id="header-nav-sellerSupcFMMapping"><a
					href="${path.http}/admin/sellerSupcFMMapping/"> Seller Supc FM
						Mapping</a>
				</li>
				<li class="divider-vertical"></li>
			</sec:authorize>

			<sec:authorize ifAnyGranted="admin, tech">
				<li id="header-nav-usermanagementview"><a
					href="${path.http}/admin/usermanagement/"> User Management</a>
				</li>
				<li class="divider-vertical"></li>
			</sec:authorize>
			<sec:authorize ifAnyGranted="admin,tech">
				<li id="header-nav-admintasks"><a
					href="${path.http}/admin/jobs/"> Admin Tasks</a>
				</li>
				<li class="divider-vertical"></li>
			</sec:authorize>
			<li id="header-nav-lookups"><a
				href="${path.http}/admin/lookups/"> Lookups</a>
			</li>
			<li class="divider-vertical"></li>
			<sec:authorize ifAnyGranted="admin,tech, taxrateeditor">
				<li id="header-nav-taxrate"><a
					href="${path.http}/admin/taxRate/"> TaxRate</a>
				</li>
				<li class="divider-vertical"></li>
			</sec:authorize>
			
			
			<sec:authorize ifAnyGranted="admin,tech, fceditor,fcmappingeditor">
				<li id="header-nav-fcview"><a
					href="${path.http}/admin/fcMapping/"> Fulfilment Center</a>
				</li>
				<li class="divider-vertical"></li>
			</sec:authorize>
			
			<sec:authorize ifAnyGranted="admin,tech, pincodeuploader">
				<li id="header-nav-pzMapping"><a
					href="${path.http}/admin/pincodeZoneMapping/">Pincode Zone Mapping </a>
				</li>
				<li class="divider-vertical"></li>
			</sec:authorize>
			<sec:authorize ifAnyGranted="admin,tech,packmanboxcreator,packmanboxeditor,packmanslabeditor,packmanslabview,packmansurfaceeditor,packmansurfacecreator">
				<li id="header-nav-packman">
				 <a href="${path.http}/admin/packman/main">Pack Man </a>
				</li>
				<li class="divider-vertical"></li>
			</sec:authorize>
			<sec:authorize ifAnyGranted="admin,tech,uploader,superuploader,fceditor,fcmappingeditor,taxrateeditor,fmMappingUploader,ruleeditor,packmanboxcreator,packmanboxeditor,packmanslabeditor,packmanslabview,packmansurfaceeditor,packmansurfacecreator">
				<li id="header-nav-myjob">
					<a href="${path.http}/admin/jobs/myJobs">
					My Jobs</a>
				</li>
				<li class="divider-vertical"></li>
			</sec:authorize>
			
		</ul>
		<ul class="nav pull-right">
			<li class="dropdown"><a class="dropdown-toggle"
				data-toggle="dropdown" href="#"> Hello, <c:choose>
						<c:when test="${currentUser == null}">
				      User 
				     </c:when>
						<c:otherwise>
				     ${currentUser}
				     </c:otherwise>
					</c:choose> <span class="caret"></span> </a>
				<ul class="dropdown-menu">
					<li><a href="${path.http}/updatePasswordLanding">Change
							Password</a>
					</li>
					<li><a href="${path.http}/logout"> Logout</a>
					</li>
				</ul></li>
		</ul>

	</div>
</div>
