$(document).ready(function() {
	
			$(function() {
							$("#endDate").datepicker({
								dateFormat:'dd/mm/yy',minDate:0, changeMonth: true
							});
						});
			
			$(function() {
				$("#startDate").datepicker({
					dateFormat:'dd/mm/yy',minDate:0, changeMonth: true
				});
			});
			
			$("#startDate").keypress(function(event) { event.preventDefault();});
			$("#endDate").keypress(function(event) { if(event.which!=8) event.preventDefault();});
			
			
			$('#rulesRemove').click(function(){
				if(confirm("Do you really want to remove the rule?")){
					if($('#remarks').val()==''){
						alert("Please provider a  reason in remarks column for removing the rule.");
						return false;
					}
					$('#ruleCodeToRemove').val($('#ruleCode').val());
					$('#removalRemarks').val($('#remarks').val());
					$('#ruleRemoveForm').submit();
					
				}
			});
			
			
			
			$('#ruleName').change(function(){
				
				if($(this).val()!=''){
					if($(this).val().trim().split(",").length>1 || $(this).val().trim().split("-").length>1){
						alert("Please input a valid rule name (without any comma/hyphen)");
						return;
					}
				}
				
			});
			
			$('.rulesCreate').click(function(){
				if(confirm("Are you sure? This will remove the current rule data")){
				window.location.href= $(this).val();
				}
				else{
					return false;
				}
			});
			
			$('.rulesEdit').click(function(){
				if(confirm("Are you sure? This will remove the current rule data")){
				window.location.href= $(this).val();
				}
				else{
					return false ;
				}
			});
				
						$('.chzn-select').chosen();
						
						$( '.chzn-results .group-result' ).each( function () {
						    var self      = $( this )
						        , options = $( '~li', self )
						        , next    = options.filter( '.group-result' ).get( 0 )
						    ;
						    self.data( 'chzn-options', options.slice( 0, options.index( next ) ) );
						} )
						.click( function () { 
						    $( this ).data( 'chzn-options' ).mouseup()
						 } )
						.hover( function () { 
						    $( this ).data( 'chzn-options' ).addClass( 'highlighted' );
						 }, function () { 
						    $( this ).data( 'chzn-options' ).removeClass( 'highlighted' );
						 } )
						.css( { cursor: 'pointer' } )
	

						
						var statesCache=null;
						var cityCache=null;
						
						function fixArray(array){
							if ($.isArray(array)) {
								return array;
							} else {
								return [ array ];
							}
						}
						
						

						

					});

		

$(document).ready(function() {
	
	if($('#volumeOperator').val()==''){
		$('#volume').hide();
	}
	if ($('#volumeOperator').val() != 'bw' && $('#volumeOperator').val() != 'nb') {
		$('#volumeToField').hide();
	}

	if($('#weightOperator').val()==''){
		$('#weight').hide();
	}
	if ($('#weightOperator').val() != 'bw' && $('#weightOperator').val() != 'nb') {
		$('#weightToField').hide();
	}
	if($('#priceOperator').val()==''){
		$('#price').hide();
	}
	
	if ($('#priceOperator').val() != 'bw'&& $('#priceOperator').val() != 'nb') {
		$('#priceToField').hide();
	}

	$('#volumeOperator').change(function() {
		$('#volumeTo').val('');
		$('#volume').val('');
		if ($(this).val() == 'bw'||$(this).val() == 'nb') {
			$('#volumeToField').show();
		} else {
			if($(this).val()==''){
				$('#volume').hide();
			}
			$('#volumeToField').hide();
		}
		$('#volume').show();
	});

	$('#weightOperator').change(function() {
		$('#weightTo').val('');
		$('#weight').val('');
		if ($(this).val() == 'bw'||$(this).val() == 'nb') {
			$('#weightToField').show();
		} else {
			$('#weightToField').hide();
			if($(this).val()==''){
				$('#weight').hide();
				return;
			}
			
		}
		$('#weight').show();
		
	});

	$('#priceOperator').change(function() {
		$('#priceTo').val('');
		$('#price').val('');
		if ($(this).val() == 'bw'||$(this).val() == 'nb') {
			$('#priceToField').show();
		} else {
			$('#priceToField').hide();
			if($(this).val()==''){
				$('#price').hide();
				return 
			}
		}
		$('#price').show();
	});
	
	$('#price').change(function(){
		if($('#price').val()!='' && !$.isNumeric($('#price').val())){
			alert("please input a number ");
			$('#price').val('');
		}
	});
	$('#weight').change(function(){
		if($('#weight').val()!='' && !$.isNumeric($('#weight').val())){
			alert("please input a number ");
			$('#weight').val('');
		}
	});
	
	
	$('#volume').change(function(){
		if($('#volume').val()!='' && !$.isNumeric($('#volume').val())){
			alert("please input a number ");
			$('#volume').val('');
		}
	});
	
	$('#priceTo').change(function(){
		if($('#priceTo').val()!='' && !$.isNumeric($('#priceTo').val())){
			alert("please input a number ");
			$('#priceTo').val('');
		}
	});
	
	$('#volumeTo').change(function(){
		if($('#volumeTo').val()!='' && !$.isNumeric($('#volumeTo').val())){
			alert("please input a number ");
			$('#volumeTo').val('');
		}
	});
	$('#weightTo').change(function(){
		if($('#weightTo').val()!='' && !$.isNumeric($('#weightTo').val())){
			alert("please input a number ");
			$('#weightTo').val('');
		}
	});
	
	
	
	
	
	
	
	$('#categoryOperator').change(function(){
		var operator = $('#categoryOperator').val();
		document.getElementById("categories_chzn").style.width="630px";
		if(operator!=""){
			$('#categoryDiv').show();
		}else{
			$('#categoryDiv').hide();
		}
			
	});
	
});