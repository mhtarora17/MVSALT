/**
 *  Copyright 2016 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.packman.dto;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.snapdeal.base.utils.DateUtils;

/**
 * @version 1.0, 25-Feb-2016
 * @author brijesh
 */
public class ThreeDimensionalPTItemDTO extends PackagingTypeItemDTO implements Comparator<ThreeDimensionalPTItemDTO>, Comparable<ThreeDimensionalPTItemDTO> {

    private Double length;

    private Double breadth;

    private Double height;

    private Double volWeight;

    public ThreeDimensionalPTItemDTO() {
    }
    
    public ThreeDimensionalPTItemDTO(Double length,Double breadth, Double height, String name) {
        this.length = length;
        this.breadth = breadth;
        this.height = height;
        super.setBoxName(name);
        // Dummy Vol Weight 
        // Used only for sorting
        this.volWeight = length * breadth * height;
    }


    public ThreeDimensionalPTItemDTO(PackagingTypeItemDTO dto) {
        super(dto.getBoxName(), dto.getPackagingMode(), dto.getDescription());
        ThreeDimensionalPTItemDTO temp = (ThreeDimensionalPTItemDTO) dto;
        this.length = temp.getLength();
        this.breadth = temp.getBreadth();
        this.height = temp.getHeight();
        this.volWeight = temp.getVolWeight();
    }

    public ThreeDimensionalPTItemDTO(Double length, Double breadth, Double height, Double volWeight, String boxName, String packagingMode, String description) {
        super(boxName, packagingMode, description);
        this.length = length;
        this.breadth = breadth;
        this.height = height;
        this.volWeight = volWeight;
    }

    public ThreeDimensionalPTItemDTO(Double volWeight) {
        this.volWeight = volWeight;
    }

    public Double getLength() {
        return length;
    }

    public void setLength(Double length) {
        this.length = length;
    }

    public Double getBreadth() {
        return breadth;
    }

    public void setBreadth(Double breadth) {
        this.breadth = breadth;
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public Double getVolWeight() {
        return volWeight;
    }

    public void setVolWeight(Double volWeight) {
        this.volWeight = volWeight;
    }

    @Override
    public String toString() {
        return "ThreeDimensionalPTDTO [length=" + length + ", breadth=" + breadth + ", height=" + height + ", volWeight=" + volWeight + "]";
    }

    // Remove element whose name are same.
    @Override
    public boolean equals(Object obj) {
        if(this.getBoxName().equals(((ThreeDimensionalPTItemDTO)obj).getBoxName())){
            return true;
        }
        return false;
    }

    @Override
    public int compareTo(ThreeDimensionalPTItemDTO object) {
        return compare(this, object);
    }

    @Override
    public int compare(ThreeDimensionalPTItemDTO object1, ThreeDimensionalPTItemDTO object2) {
        return Double.compare(object1.getVolWeight(),object2.getVolWeight());
    }
    
    public boolean isValid3dDto(){
        if(length<=0 || breadth<=0 || height<=0){
            return false;
        }
        return true;
    }

    public PackagingTypeItemDTO getAppropriateBox(RecommendationParamsDTO dto, List<ThreeDimensionalPTItemDTO> boxes) {
        if (boxes != null) {
            Double[] itemDimensions = { dto.getLength(), dto.getBreadth(), dto.getHeight() };
            Arrays.sort(itemDimensions, Collections.reverseOrder());
            Collections.sort(boxes);//TODO
            for (ThreeDimensionalPTItemDTO item : boxes) {
                Double[] boxDimensions = { item.getLength(), item.getBreadth(), item.getHeight() };
                Arrays.sort(itemDimensions, Collections.reverseOrder());
                if (itemDimensions[0] + dto.getRecommendationLBHMargin() <= boxDimensions[0] && itemDimensions[1] + dto.getRecommendationLBHMargin() <= boxDimensions[1]
                        && itemDimensions[2] + dto.getRecommendationLBHMargin() <= boxDimensions[2]) {
                    return item;
                }
            }
        }
        return new PackagingTypeItemDTO();
    }

    /**
     * 
     * Check whether item is fittable into particular box or not
     * Considers rotation also
     * 
     * @param productDimensions
     * @param boxDimensions
     * @param sortRequired
     * @return
     */
    public static boolean isProductFittableIntoBox(Double[] productDimensions, Double[] boxDimensions, boolean sortRequired) {
        if (sortRequired) {
            Arrays.sort(productDimensions, Collections.reverseOrder());
            Arrays.sort(boxDimensions, Collections.reverseOrder());
        }
        if (productDimensions[0] <= boxDimensions[0] && productDimensions[1] <= boxDimensions[1] && productDimensions[2] <= boxDimensions[2]) {
            return true;
        }
        return false;
    }

}
