/**
 *  Copyright 2016 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.packman.dto;

import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;

/**
 * @version 1.0, 10-Feb-2016
 * @author brijesh
 */
public class RecommendationParamsDTO {

    private String      storeCode;

    private String      packagingType;
    
    private String      orderCode;

    private String      supc;

    private Double      length;

    private Double      breadth;

    private Double      height;

    private Double      volWeight;

    private Boolean     recommendationBasedOnLBH;

    private Boolean     recommendationBasedOnDeltaVolWt;

    private Double      recommendationLBHMargin;

    private Double      recommendationDeltaVolWtOffset;

    private Double      recommendationDeltaMultFactor;

    private IDependency dep = new Dependency();

    public RecommendationParamsDTO() {
        init(dep);
    }

    public RecommendationParamsDTO(String dummy) {
        // This constructor is useful when we want to avoid reading properties from ConfigUtils...
    }

    public RecommendationParamsDTO(IDependency dep) {
        init(dep);
    }

    private void init(IDependency dep) {
        this.recommendationBasedOnLBH = dep.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_LBH);
        this.recommendationBasedOnDeltaVolWt = dep.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_DELTA_VOL_WT);
        this.recommendationLBHMargin = dep.getDoubleScalar(Property.PACKMAN_RECO_LBH_MARGIN);
        this.recommendationDeltaMultFactor = dep.getDoubleScalar(Property.PACKMAN_RECO_DELTA_VOL_WT_MULT_FACTOR);
        this.recommendationDeltaVolWtOffset = dep.getDoubleScalar(Property.PACKMAN_RECO_DELTA_VOL_WT_OFFSET);
    }

    public String getStoreCode() {
        return storeCode;
    }

    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }

    public String getPackagingType() {
        return packagingType;
    }

    public void setPackagingType(String packagingType) {
        this.packagingType = packagingType;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public String getSupc() {
        return supc;
    }

    public void setSupc(String supc) {
        this.supc = supc;
    }

    public Double getVolWeight() {
        return volWeight;
    }

    public void setVolWeight(Double volWeight) {
        this.volWeight = volWeight;
    }

    public Double getLength() {
        return length;
    }

    public void setLength(Double length) {
        this.length = length;
    }

    public Double getBreadth() {
        return breadth;
    }

    public void setBreadth(Double breadth) {
        this.breadth = breadth;
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public Boolean isRecommendationBasedOnLBH() {
        return recommendationBasedOnLBH;
    }

    public void setRecommendationBasedOnLBH(Boolean recommendationBasedOnLBH) {
        this.recommendationBasedOnLBH = recommendationBasedOnLBH;
    }

    public Boolean isRecommendationBasedOnDeltaVolWt() {
        return recommendationBasedOnDeltaVolWt;
    }

    public void setRecommendationBasedOnDeltaVolWt(Boolean recommendationBasedOnDeltaVolWt) {
        this.recommendationBasedOnDeltaVolWt = recommendationBasedOnDeltaVolWt;
    }

    public Double getRecommendationLBHMargin() {
        return recommendationLBHMargin;
    }

    public void setRecommendationLBHMargin(Double recommendationLBHMargin) {
        this.recommendationLBHMargin = recommendationLBHMargin;
    }

    public Double getRecommendationDeltaVolWtOffset() {
        return recommendationDeltaVolWtOffset;
    }

    public void setRecommendationDeltaVolWtOffset(Double recommendationDeltaVolWtOffset) {
        this.recommendationDeltaVolWtOffset = recommendationDeltaVolWtOffset;
    }

    public Double getRecommendationDeltaMultFactor() {
        return recommendationDeltaMultFactor;
    }

    public void setRecommendationDeltaMultFactor(Double recommendationDeltaMultFactor) {
        this.recommendationDeltaMultFactor = recommendationDeltaMultFactor;
    }

    @Override
    public String toString() {
        return "RecommendationParamsDTO [storeCode=" + storeCode + ", packagingType=" + packagingType + ", orderCode=" + orderCode + ", supc=" + supc + ", length=" + length
                + ", breadth=" + breadth + ", height=" + height + ", volWeight=" + volWeight + ", recommendationBasedOnLBH=" + recommendationBasedOnLBH
                + ", recommendationBasedOnDeltaVolWt=" + recommendationBasedOnDeltaVolWt + ", recommendationLBHMargin=" + recommendationLBHMargin
                + ", recommendationDeltaVolWtOffset=" + recommendationDeltaVolWtOffset + ", recommendationDeltaMultFactor=" + recommendationDeltaMultFactor + ", dep=" + dep + "]";
    }

    public interface IDependency {

        Double getDoubleScalar(Property packmanRecoLbhMargin);

        Boolean getBooleanScalar(Property packmanRecoBasedOnLbh);

    }

    static class Dependency implements IDependency {

        @Override
        public Double getDoubleScalar(Property property) {
            return ConfigUtils.getDoubleScalar(property);
        }

        @Override
        public Boolean getBooleanScalar(Property property) {
            return ConfigUtils.getBooleanScalar(property);
        }

    }

}
