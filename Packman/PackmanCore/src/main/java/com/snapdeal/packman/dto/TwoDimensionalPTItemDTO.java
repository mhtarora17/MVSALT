/**
 *  Copyright 2016 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.packman.dto;

import com.snapdeal.base.utils.StringUtils;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * @version 1.0, 25-Feb-2016
 * @author brijesh
 */
public class TwoDimensionalPTItemDTO extends PackagingTypeItemDTO implements Comparator<TwoDimensionalPTItemDTO>, Comparable<TwoDimensionalPTItemDTO> {

    private Double length;

    private Double breadth;

    public TwoDimensionalPTItemDTO() {
    }

    public TwoDimensionalPTItemDTO(Double length, Double breadth) {
        this.length = length;
        this.breadth = breadth;
    }

    public TwoDimensionalPTItemDTO(PackagingTypeItemDTO dto) {
        super(dto.getBoxName(), dto.getPackagingMode(), dto.getDescription());
        TwoDimensionalPTItemDTO temp = (TwoDimensionalPTItemDTO) dto;
        this.length = temp.getLength();
        this.breadth = temp.getBreadth();
    }

    public TwoDimensionalPTItemDTO(Double length, Double breadth, String boxName, String packagingMode, String description) {
        super(boxName, packagingMode, description);
        this.length = length;
        this.breadth = breadth;
    }

    public Double getLength() {
        return length;
    }

    public void setLength(Double length) {
        this.length = length;
    }

    public Double getBreadth() {
        return breadth;
    }

    public void setBreadth(Double breadth) {
        this.breadth = breadth;
    }

    @Override
    public String toString() {
        return "TwoDimensionalPTItemDTO [length=" + length + ", breadth=" + breadth + "]";
    }

    @Override
    public int compareTo(TwoDimensionalPTItemDTO object) {
        return compare(this, object);
    }

    @Override
    public int compare(TwoDimensionalPTItemDTO obj1, TwoDimensionalPTItemDTO obj2) {
        return Double.compare(obj1.getLength() + obj1.getBreadth(), obj2.getLength() + obj2.getBreadth());
    }

    public PackagingTypeItemDTO getAppropriateBox(RecommendationParamsDTO dto, List<TwoDimensionalPTItemDTO> boxes) {

        double productL = dto.getLength();
        double productB = dto.getBreadth();
        double productH = dto.getHeight();

        // There are total six ways in which we can fit three dimensional product into two dimensional box..

        if (boxes != null) {
            for (TwoDimensionalPTItemDTO box : boxes) {
                double boxL = box.getLength();
                double boxB = box.getBreadth();
                if((productB+productH)<=boxB && (productL+productH)<=boxL){
                    return box;
                } else if ((productB+productH)<=boxL && (productL+productH)<=boxB){
                    return box;
                } else if ((productB+productL)<=boxB && (productH+productL)<=boxL){
                    return box;
                } else if ((productB+productL)<=boxL && (productH+productL)<=boxB){
                    return box;
                } else if ((productL+productB)<=boxB && (productH+productB)<=boxL){
                    return box;
                } else if ((productL+productB)<=boxL && (productH+productB)<=boxB){
                    return box;
                }
            }
        }
        return new PackagingTypeItemDTO();
    }

    public static boolean isProductFittableIntoBox(Double[] productDimensions, TwoDimensionalPTItemDTO box) {

        RecommendationParamsDTO dto = new RecommendationParamsDTO("Dummy");
        dto.setLength(productDimensions[0]);
        dto.setBreadth(productDimensions[1]);
        dto.setHeight(productDimensions[2]);

        List<TwoDimensionalPTItemDTO> boxList = new ArrayList<>();
        boxList.add(box);

        PackagingTypeItemDTO result = box.getAppropriateBox(dto, boxList);

        return StringUtils.isNotEmpty(result.getBoxName());

    }

}
