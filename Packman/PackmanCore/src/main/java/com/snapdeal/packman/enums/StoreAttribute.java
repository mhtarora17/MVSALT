/**
 *  Copyright 2016 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.packman.enums;

/**
 * @version 1.0, 08-Jan-2016
 * @author brijesh
 */
public enum StoreAttribute {

    LABEL("LABEL");

    private String name;

    StoreAttribute(String name) {
        this.name = name;
    }

    public String getCode() {
        return this.name;
    }

}
