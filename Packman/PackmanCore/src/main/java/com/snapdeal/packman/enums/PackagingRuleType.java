/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.enums;

/**
 *  
 *  @version     1.0, 21-Dec-2015
 *  @author brijesh
 */
public enum PackagingRuleType {

    StorePackagingRuleBlock("Store","storeRuleCreate"),SupcPackagingRuleBlock("supc","supcRuleCreate"), CategoryPackagingRuleBlock("Category","categoryRuleCreate"), SubcategoryPackagingRuleBlock("Subcategory","subcategoryRuleCreate");

    private String code;
    
    private String url;

    private PackagingRuleType(String code,String url) {
        this.code = code;
        this.url = url;
    }

    public String getCode() {
        return code;
    }

    public String getUrl() {
        return url;
    }
    
    
}
