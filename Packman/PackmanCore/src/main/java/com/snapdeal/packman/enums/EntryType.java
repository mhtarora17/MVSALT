/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.enums;

/**
 *  
 *  @version     1.0, 01-Dec-2015
 *  @author indrajit
 */
public enum EntryType {
    
  
    
    STORE("Store"),
    SDSTORE("Snapdeal"),
    EXCSTORE("Exclusively"),
    ROLE("Role"),
    SDROLE("SDRole"),
    EXCROLE("ExcRole");
    
    String name ;
    
    private EntryType(String roleName){
	this.name = roleName;
    }
    
    public String role()
    {
	return this.name;
    }
    
    public static EntryType getEntryTypeByType(String key) {
	EntryType result = null;
        for (EntryType k : EntryType.values()) {
            if (k.role().equals(key)) {
                result = k;
                break;
            }
        }
        return result;
    }
    
    
    

}
