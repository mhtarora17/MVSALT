/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.packman.dto;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * @version 1.0, 08-Dec-2015
 * @author indrajit
 */
public class URLPatternDto implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 123523523L;

    private String url;

    private List<String> roleCodeList;

    private boolean url_enable;

    private boolean role_enable;

    public String getUrl() {
	return url;
    }

    public void setUrl(String url) {
	this.url = url;
    }

    
    

    public List<String> getRoleCodeList() {
        return roleCodeList;
    }

    public void setRoleCodeList(List<String> roleCodeList) {
        this.roleCodeList = roleCodeList;
    }

    public boolean isUrl_enable() {
	return url_enable;
    }

    public void setUrl_enable(boolean url_enable) {
	this.url_enable = url_enable;
    }

    public boolean isRole_enable() {
	return role_enable;
    }

    public void setRole_enable(boolean role_enable) {
	this.role_enable = role_enable;
    }

    @Override
    public String toString() {
	return "URLPatternDto [url=" + url + ", roleCodeList=" + roleCodeList
		+ ", url_enable=" + url_enable + ", role_enable=" + role_enable
		+ "]";
    }

    

   

}
