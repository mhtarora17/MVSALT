/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.rule.dto;

import org.hibernate.validator.constraints.NotEmpty;



/**
 *  
 *  @version     1.0, 24-Dec-2015
 *  @author brijesh
 */
public class StoreRuleCreateForm {
    
    String storeCode;
    
    String ruleName;

    @NotEmpty
    String packagingType;
    
    @NotEmpty
    String startDate;
    
    String endDate;
    
    boolean enabled = false;

    public String getStoreCode() {
        return storeCode;
    }

    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }

    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    public String getPackagingType() {
        return packagingType;
    }

    public void setPackagingType(String packagingType) {
        this.packagingType = packagingType;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public String toString() {
        return "StoreCreateRuleDTO [storeCode=" + storeCode + ", ruleName=" + ruleName + ", packagingType=" + packagingType + ", startDate=" + startDate + ", endDate=" + endDate
                + ", enabled=" + enabled + "]";
    }
    
}

