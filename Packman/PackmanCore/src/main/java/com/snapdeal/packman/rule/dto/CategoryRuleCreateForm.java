/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.rule.dto;

import java.util.Arrays;

import org.hibernate.validator.constraints.NotEmpty;

/**
 *  
 *  @version     1.0, 24-Dec-2015
 *  @author brijesh
 */
public class CategoryRuleCreateForm extends StoreRuleCreateForm{

    @NotEmpty
    private String category;
    
    private String brand;
    
    private String[] supercat;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String[] getSupercat() {
        return supercat;
    }

    public void setSupercat(String[] supercat) {
        this.supercat = supercat;
    }

    @Override
    public String toString() {
        return "CategoryCreateRuleDTO [category=" + category + ", brand=" + brand + ", supercat=" + Arrays.toString(supercat) + ", packagingType=" + packagingType
                + ", startDate=" + startDate + ", endDate=" + endDate + ", enabled=" + enabled + "]";
    }
    
    
    
}
