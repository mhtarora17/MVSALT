package com.snapdeal.packman.enums;

public enum RulePriorityType {

    SUPC("SUPC",100),
    SUBCAT_BRAND_SUPERCAT("SUBCAT_BRAND_SUPERCAT",90),
    SUBCAT_BRAND("SUBCAT_BRAND",80),
    SUBCAT_SUPERCAT("SUBCAT_SUPERCAT",70),
    SUBCAT("SUBCAT",60),
    CATEGORY_BRAND_SUPERCAT("CATEGORY_BRAND_SUPERCAT",50),
    CATEGORY_BRAND("CATEGORY_BRAND",40),
    CATEGORY_SUPERCAT("CATEGORY_SUPERCAT",30),
    CATEGORY("CATEGORY",20),
    STORE("STORE",10);




    private String type;
    private int priority;

    private RulePriorityType(String type,int priority) {
        this.type = type;
        this.priority = priority;
    }

    public String getType() {
        return type;
    }

    public int getPriority() {
        return priority;
    }
    
    
}
