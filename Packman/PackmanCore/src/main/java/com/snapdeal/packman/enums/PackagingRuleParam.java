package com.snapdeal.packman.enums;

public enum PackagingRuleParam {
     
    CATEGORY("CATEGORY"), SUBCATEGORY("SUBCATEGORY"), STORECODE("STORECODE"),RETURN_VALUE("RETURN_VALUE");

    private String code;

    private PackagingRuleParam(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
