/**
 *  Copyright 2016 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

/**
 *  
 *  @version     1.0, 19-Jan-2016
 *  @author brijesh
 */

@Entity
@Table(name = "packaging_type_item_properties")
@Audited
public class PackagingTypeItemProperties implements Serializable{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private Integer               id;
    private String                name;
    private String                value;
    private PackagingTypeItem     packagingTypeItem;
    private Boolean               enabled;
    private Date                  created;
    private Date                  lastUpdated;
    private String                updatedBy;
    
    public PackagingTypeItemProperties() {
    }
    
    public PackagingTypeItemProperties(String name, PackagingTypeItem packagingTypeItem) {
        this.name = name;
        this.packagingTypeItem = packagingTypeItem;
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "name",nullable = false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "value", nullable = false)
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "packaging_type_item_id")
    public PackagingTypeItem getPackagingTypeItem() {
        return packagingTypeItem;
    }
    
    public void setPackagingTypeItem(PackagingTypeItem packagingTypeItem) {
        this.packagingTypeItem = packagingTypeItem;
    }
    
    @Column(name = "enabled")
    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created", nullable = false)
    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_updated", length = 19)
    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    @Column(name = "updated_by", nullable = false)
    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Override
    public String toString() {
        return "PackagingTypeItemProperties [id=" + id + ", name=" + name + ", value=" + value + ", packagingTypeItem=" + packagingTypeItem + ", enabled=" + enabled + ", created="
                + created + ", lastUpdated=" + lastUpdated + ", updatedBy=" + updatedBy + "]";
    }
    
    @Override
    public boolean equals(Object obj) {
        if(obj instanceof PackagingTypeItemProperties){
            PackagingTypeItemProperties prop = (PackagingTypeItemProperties) obj;
            if(prop.getName()==this.getName()){
                if(prop.getPackagingTypeItem()==null && this.getPackagingTypeItem()==null){
                    return true;
                } else if (prop.getPackagingTypeItem()!=null && this.getPackagingTypeItem()!=null){
                    if(prop.getPackagingTypeItem().getId()==this.getPackagingTypeItem().getId()){
                        return true;
                    }
                }
            }
        }
        return false;
    }

}
