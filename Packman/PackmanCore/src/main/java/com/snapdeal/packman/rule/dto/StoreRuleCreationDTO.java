/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.rule.dto;

import java.util.Date;

/**
 *  
 *  @version     1.0, 22-Dec-2015
 *  @author brijesh
 */
public class StoreRuleCreationDTO {
    
    private String ruleType;
    
    private String ruleName;
    
    private String storeCode;
    
    private String updatedBy;
    
    private Date updateTime;

    private String returnValue;
    
    private Date startDate;
    
    private Date endDate;
    
    private int priority;
    
    private int block;
    
    private boolean enabled;
    
    public String getRuleType() {
        return ruleType;
    }

    public void setRuleType(String ruleType) {
        this.ruleType = ruleType;
    }

    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    public String getStoreCode() {
        return storeCode;
    }

    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getReturnValue() {
        return returnValue;
    }

    public void setReturnValue(String returnValue) {
        this.returnValue = returnValue;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public int getBlock() {
        return block;
    }

    public void setBlock(int block) {
        this.block = block;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public String toString() {
        return "StoreRuleCreationDTO [ruleType=" + ruleType + ", ruleName=" + ruleName + ", storeCode=" + storeCode + ", updatedBy=" + updatedBy + ", updateTime=" + updateTime
                + ", returnValue=" + returnValue + ", startDate=" + startDate + ", endDate=" + endDate + ", priority=" + priority + ", block=" + block + ", enabled=" + enabled
                + "]";
    }
    
}
