/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 23-Aug-2013
 *  @author abhinav singhal
 */
package com.snapdeal.packman.mongo.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class PackmanRuleUnit {

    public enum PackmanRuleUnitFieldName {
        ID("id"), TYPE("type"), PARAM_VALUE("paramValue"), RULE_CODE("ruleCode"), START_DATE("startDate"), END_DATE("endDate"),  PRIORITY("priority"), ENABLED("enabled"), 
            CREATED("created"), UPDATED("updated"),  SYNCHED("synched"), NAME("name"), STORECODE("storeCode");
        private String name;
        private PackmanRuleUnitFieldName(String name ) {
            this.name = name;
        }
        public String getName() {
            return name;
        }
        
    }
    
    public static enum PackmanRuleUnitType {
        CATEGORY_LEVEL_PACKAGING("category-packaging"),SUBCATEGORY_LEVEL_PACKAGING("subcategory-packaging"),STORE_LEVEL_PACKAGING("store-packaging");

        private String code;

        PackmanRuleUnitType(String code) {
            this.code = code;
        }

        public String getCode() {
            return code;
        }

    };
    private Integer            id;
    
    private String             name;
    
    private String             storeCode;

    private String             type;
        
    private String             paramValue; 
    
    private String             returnValue; 

    private Date               startDate;

    private Date               endDate;

    private boolean            enabled;

    private Integer            priority;

    private List<CriteriaUnit> criteria = new ArrayList<CriteriaUnit>(0);

    private Date               created;

    private Date               updated;
    
    private String             ruleCode;
    
    private String             createdBy;
    
    private String             updatedby;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    

    public String getStoreCode() {
        return storeCode;
    }

    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    
    public String getParamValue() {
        return paramValue;
    }

    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }

    
    public String getReturnValue() {
        return returnValue;
    }

    public void setReturnValue(String returnValue) {
        this.returnValue = returnValue;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public List<CriteriaUnit> getCriteria() {
        return criteria;
    }

    public void setCriteria(List<CriteriaUnit> criteria) {
        this.criteria = criteria;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

   

    
    public String getRuleCode() {
        return ruleCode;
    }

    public void setRuleCode(String ruleCode) {
        this.ruleCode = ruleCode;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedby() {
        return updatedby;
    }

    public void setUpdatedby(String updatedby) {
        this.updatedby = updatedby;
    }

    @Override
    public String toString() {
        return "PackmanRuleUnit [id=" + id + ", type=" + type + ", name=" + name + ", startDate=" + startDate + ", endDate=" + endDate + ", enabled=" + enabled + ", priority="
                 + priority +  ", returnValue=" + returnValue + ", criteria=" + criteria + ", created=" + created + ", updated=" + updated + ", ruleCode=" + ruleCode + ", createdBy=" + createdBy + ", updatedBy=" + updatedby + "]";
    }

   
    

}
