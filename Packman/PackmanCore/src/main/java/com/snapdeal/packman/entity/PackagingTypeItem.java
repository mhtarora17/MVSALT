/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.packman.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import com.snapdeal.packman.configuration.PackagingTypeItemProperty;

/**
 * @version 1.0, 25-Nov-2015
 * @author ankur
 */

@Entity
@Table(name = "packaging_type_item")
@Audited
public class PackagingTypeItem implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -6345499630375236425L;
    private Integer                               id;
    private String                                code;
    private String                                description;
    private PackagingType                         packagingType;
    private List<PackagingTypeItemProperties>     properties;
    private Date                                  created;
    private Date                                  lastUpdated;
    private String                                updatedBy;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "code", nullable = false, length = 48)
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

   
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "packaging_type_id")
    public PackagingType getPackagingType() {
        return packagingType;
    }

    public void setPackagingType(PackagingType packagingType) {
        this.packagingType = packagingType;
    }
    
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "packagingTypeItem")
    public List<PackagingTypeItemProperties> getProperties() {
        return properties;
    }

    public void setProperties(List<PackagingTypeItemProperties> properties) {
        this.properties = properties;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created", nullable = false)
    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_updated", length = 19)
    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    @Column(name = "updated_by", nullable = false)
    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Override
    public String toString() {
        return "PackagingTypeItem [id=" + id + ", code=" + code + ", description=" + description + ", created=" + created + ", lastUpdated=" + lastUpdated + ", updatedBy=" + updatedBy + "]";
    }
    
    public String getPackagingTypeItemProperty(PackagingTypeItemProperty prop){
        if(properties!=null){
            for(PackagingTypeItemProperties p : properties){
                if(p.getName().equals(prop.getName())){
                    return p.getValue();
                }
            }
        }
        return null;
    }

}
