/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.dto;

/**
 *  
 *  @version     1.0, 10-Dec-2015
 *  @author indrajit
 */
public class UrlPatternDto {
    private String url;
    private Boolean enanble;
    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }
    public Boolean getEnanble() {
        return enanble;
    }
    public void setEnanble(Boolean enanble) {
        this.enanble = enanble;
    }
    @Override
    public String toString() {
	return "UrlPatternDto [url=" + url + ", enanble=" + enanble + "]";
    }
    

}
