/**
 *  Copyright 2016 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.dto;


/**
 *  
 *  @version     1.0, 25-Feb-2016
 *  @author brijesh
 */
public class PackagingTypeItemDTO {

    private String boxName;
    
    private String packagingMode;
    
    private String description;
    
    public PackagingTypeItemDTO() {
    }

    public PackagingTypeItemDTO(String boxName, String packagingMode, String description) {
        this.boxName = boxName;
        this.packagingMode = packagingMode;
        this.description = description;
    }

    public String getBoxName() {
        return boxName;
    }

    public void setBoxName(String boxName) {
        this.boxName = boxName;
    }

    public String getPackagingMode() {
        return packagingMode;
    }

    public void setPackagingMode(String packagingMode) {
        this.packagingMode = packagingMode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "PackagingTypeItemDTO [boxName=" + boxName + ", packagingMode=" + packagingMode + ", description=" + description + "]";
    }
    
}
