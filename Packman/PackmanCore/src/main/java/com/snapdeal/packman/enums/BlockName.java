/**
 * 
 */
package com.snapdeal.packman.enums;

/**
 * @author ankurgarg
 */
public enum BlockName {

    StorePackagingRuleBlock("StorePackagingRuleBlock"), CategoryPackagingRuleBlock("CategoryPackagingRuleBlock"), SubcategoryPackagingRuleBlock("SubcategoryPackagingRuleBlock");

    private String code;

    private BlockName(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
