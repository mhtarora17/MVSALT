/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.rule.dto;

import java.util.List;

import org.hibernate.validator.constraints.NotEmpty;

/**
 *  
 *  @version     1.0, 25-Dec-2015
 *  @author brijesh
 */
public class SearchRuleDTO {
    
    private String rulename;

    @NotEmpty
    private String storeCode;
    
    private String category;
    
    private String subcategory;
    
    private List<String> brand;
    
    private List<String> supercategory;
    
    private String createdby;
    
    private boolean enabled = true;
    
    private String packagingType;

    public String getRulename() {
        return rulename;
    }

    public void setRulename(String rulename) {
        this.rulename = rulename;
    }

    public String getStoreCode() {
        return storeCode;
    }

    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(String subcategory) {
        this.subcategory = subcategory;
    }

    public List<String> getBrand() {
        return brand;
    }

    public void setBrand(List<String> brand) {
        this.brand = brand;
    }

    public List<String> getSupercategory() {
        return supercategory;
    }

    public void setSupercategory(List<String> supercategory) {
        this.supercategory = supercategory;
    }

    public String getCreatedby() {
        return createdby;
    }

    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getPackagingType() {
        return packagingType;
    }

    public void setPackagingType(String packagingType) {
        this.packagingType = packagingType;
    }

    @Override
    public String toString() {
        return "SearchRuleDTO [rulename=" + rulename + ", storeCode=" + storeCode + ", category=" + category + ", subcategory=" + subcategory + ", brand=" + brand
                + ", supercategory=" + supercategory + ", createdby=" + createdby + ", enabled=" + enabled + ", packagingType=" + packagingType + "]";
    }

}
