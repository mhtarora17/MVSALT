/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.dto;

import java.util.List;

/**
 *  
 *  @version     1.0, 08-Sep-2015
 *  @author ankur
 */
public class AirPackagingRecommendationDTO {

    String packagingType;
    
    List<String> recommedationList;
    
    String mostPreferredBox;

    public AirPackagingRecommendationDTO(){
        
    }
    
    public AirPackagingRecommendationDTO(String packagingType, List<String> recommedationList, String mostPreferredBox) {
        super();
        this.packagingType = packagingType;
        this.recommedationList = recommedationList;
        this.mostPreferredBox = mostPreferredBox;
    }

    public String getPackagingType() {
        return packagingType;
    }

    public void setPackagingType(String packagingType) {
        this.packagingType = packagingType;
    }

    public List<String> getRecommedationList() {
        return recommedationList;
    }

    public void setRecommedationList(List<String> recommedationList) {
        this.recommedationList = recommedationList;
    }

    public String getMostPreferredBox() {
        return mostPreferredBox;
    }

    public void setMostPreferredBox(String mostPreferredBox) {
        this.mostPreferredBox = mostPreferredBox;
    }
    
    
}
