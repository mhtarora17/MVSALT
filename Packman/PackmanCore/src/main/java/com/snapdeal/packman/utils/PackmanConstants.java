/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.utils;

/**
 *  
 *  @version     1.0, 09-Sep-2015
 *  @author ankur
 */
public interface PackmanConstants {
    public static final String PACKMAN_PROFILE_NAME = "PACKMAN";
    
    public static final String BREACH_CASE = "Mismatch/Breach Case : ";

}
