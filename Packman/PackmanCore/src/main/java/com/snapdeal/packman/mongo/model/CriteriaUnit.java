/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 10, 2013
 *  @author himanshu
 */
package com.snapdeal.packman.mongo.model;

import java.util.Date;

public class CriteriaUnit {
    private Integer                 id;
    
    private String                  name;
    
    private String                  value;
    
    private String                  valueType;
    
    private String                  operator;
     
    private Date                    created;
    
    private Date                    updated;
    
    private boolean                 enabled;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getValueType() {
        return valueType;
    }

    public void setValueType(String valueType) {
        this.valueType = valueType;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public String toString() {
        return "CriteriaUnit [id=" + id + ", name=" + name + ", value=" + value + ", valueType=" + valueType + ", operator=" + operator + ", created=" + created + ", updated="
                + updated + ", enabled=" + enabled + "]";
    }
    
    
    

}
