/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.dto;

/**
 *  
 *  @version     1.0, 09-Dec-2015
 *  @author indrajit
 */
public class RoleStoreMappingDto {
    
    private String roleCode;
    
    private String storeCode;
    
    private String updatedBy;
    
    private boolean enabled;

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    public String getStoreCode() {
        return storeCode;
    }

    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public String toString() {
	return "RoleStoreMappingDto [roleCode=" + roleCode + ", storeCode="
		+ storeCode + ", updatedBy=" + updatedBy + ", enabled="
		+ enabled + "]";
    }
    
    

}
