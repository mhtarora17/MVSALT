/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.packman.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.Audited;

/**
 * @version 1.0, 04-Sep-2015
 * @author ankur
 */

@Entity
@Table(name = "packaging_type")
@Audited
public class PackagingType implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3620023462600249990L;

    private Integer id;
    private String  type;
    private Store   store;
    private boolean defaultType;
    private String  packagingMode; 
    private List<PackagingTypeProperties>     properties;
    private String  description;
    private boolean enabled;
    private Date    created;
    private Date    lastUpdated;
    private String  updatedBy;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "type",  nullable = false, length = 48)
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
    
    @Column(name = "packaging_mode", length = 48)
    public String getPackagingMode() {
        return packagingMode;
    }

    public void setPackagingMode(String shippingMode) {
        this.packagingMode = shippingMode;
    }

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "packagingType")
    @Fetch(value = FetchMode.SUBSELECT)
    public List<PackagingTypeProperties> getProperties() {
        return properties;
    }

    public void setProperties(List<PackagingTypeProperties> properties) {
        this.properties = properties;
    }
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "store_id")
    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    
    @Column(name = "default_type")
    public boolean isDefaultType() {
        return defaultType;
    }

    public void setDefaultType(boolean defaultType) {
        this.defaultType = defaultType;
    }

    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "enabled")
    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created", nullable = false)
    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_updated", length = 19)
    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    @Column(name = "updated_by", nullable = false)
    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Override
    public String toString() {
        return "PackagingType [id=" + id + ", type=" + type + ", store=" + store + ", defaultType=" + defaultType + ", packagingMode=" + packagingMode + ", description="
                + description + ", enabled=" + enabled + ", created=" + created + ", lastUpdated=" + lastUpdated + ", updatedBy=" + updatedBy + "]";
    }

}
