/**
 *  Copyright 2016 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.annotations;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.METHOD;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *  
 *  @version     1.0, 04-Jan-2016
 *  @author brijesh
 */

@Retention(RetentionPolicy.RUNTIME)
@Target({ ANNOTATION_TYPE, METHOD })
public @interface GenerateSidebarData {

}
