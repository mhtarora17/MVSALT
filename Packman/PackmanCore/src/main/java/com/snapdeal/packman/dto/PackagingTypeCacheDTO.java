/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.packman.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * @version 1.0, 10-Sep-2015
 * @author ankur
 */
public class PackagingTypeCacheDTO {

    private String                     type;

    private String                     packagingMode;

    private String                     description;

    private String                     storeCode;

    private List<PackagingTypeItemDTO> packagingTypeItems = new ArrayList<PackagingTypeItemDTO>(); ;

    public PackagingTypeCacheDTO() {
        super();
    }

    public PackagingTypeCacheDTO(String type, String packagingMode, String description, String storeCode) {
        super();
        this.type = type;
        this.setPackagingMode(packagingMode);
        this.description = description;
        this.storeCode = storeCode;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPackagingMode() {
        return packagingMode;
    }

    public void setPackagingMode(String packagingMode) {
        this.packagingMode = packagingMode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStoreCode() {
        return storeCode;
    }

    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }

    public List<PackagingTypeItemDTO> getPackagingTypeItems() {
        return packagingTypeItems;
    }

    public void setPackagingTypeItems(List<PackagingTypeItemDTO> packagingTypeItems) {
        this.packagingTypeItems = packagingTypeItems;
    }

    @Override
    public String toString() {
        return "PackagingTypeCacheDTO [type=" + type + ", packagingMode=" + packagingMode + ", description=" + description + ", storeCode=" + storeCode + ", packagingTypeItems="
                + packagingTypeItems + "]";
    }

    public PackagingTypeItemDTO getRecommendation(RecommendationParamsDTO dto) {
        if (packagingTypeItems.size() != 0) {
            PackagingTypeItemDTO packagingTypeItemDTO = packagingTypeItems.get(0);
            if (packagingTypeItemDTO instanceof TwoDimensionalPTItemDTO) {
                List<TwoDimensionalPTItemDTO> list = new ArrayList<TwoDimensionalPTItemDTO>();
                for (PackagingTypeItemDTO obj : packagingTypeItems) {
                    list.add((TwoDimensionalPTItemDTO) obj);
                }
                return ((TwoDimensionalPTItemDTO) packagingTypeItemDTO).getAppropriateBox(dto, list);
            }
            if (packagingTypeItemDTO instanceof ThreeDimensionalPTItemDTO) {
                List<ThreeDimensionalPTItemDTO> list = new ArrayList<ThreeDimensionalPTItemDTO>();
                for (PackagingTypeItemDTO obj : packagingTypeItems) {
                    list.add((ThreeDimensionalPTItemDTO) obj);
                }
                return ((ThreeDimensionalPTItemDTO) packagingTypeItemDTO).getAppropriateBox(dto, list);
            }
            if (packagingTypeItemDTO instanceof SurfaceTypePTItemDTO) {
                return ((SurfaceTypePTItemDTO) packagingTypeItemDTO).getAppropriateBox(dto, packagingTypeItems);
            }
        }
        return new PackagingTypeItemDTO();
    }

}
