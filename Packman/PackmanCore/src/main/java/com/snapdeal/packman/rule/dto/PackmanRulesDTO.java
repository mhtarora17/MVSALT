/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.rule.dto;

import java.util.Date;

/**
 *  
 *  @version     1.0, 25-Dec-2015
 *  @author brijesh
 */
public class PackmanRulesDTO {
    
    private int id;
    
    private String ruleCode;
    
    private String ruleName;
    
    private String blockName;
    
    private Date startDate;
    
    private Date endDate;
    
    private String packagingType;
    
    private boolean synched;
    
    private boolean banned;
    
    private String remark;
    
    private boolean enabled;
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRuleCode() {
        return ruleCode;
    }

    public void setRuleCode(String ruleCode) {
        this.ruleCode = ruleCode;
    }

    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    public String getBlockName() {
        return blockName;
    }

    public void setBlockName(String blockName) {
        this.blockName = blockName;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getPackagingType() {
        return packagingType;
    }

    public void setPackagingType(String packagingType) {
        this.packagingType = packagingType;
    }

    public boolean isSynched() {
        return synched;
    }

    public void setSynched(boolean synched) {
        this.synched = synched;
    }

    public boolean isBanned() {
        return banned;
    }

    public void setBanned(boolean banned) {
        this.banned = banned;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public String toString() {
        return "PackmanRulesDTO [id=" + id + ", ruleCode=" + ruleCode + ", ruleName=" + ruleName + ", blockName=" + blockName + ", startDate=" + startDate + ", endDate=" + endDate
                + ", packagingType=" + packagingType + ", synched=" + synched + ", banned=" + banned + ", remark=" + remark + ", enabled=" + enabled + "]";
    }

}
