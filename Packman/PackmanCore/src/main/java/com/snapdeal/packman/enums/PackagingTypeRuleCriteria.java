/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.enums;


/**
 *  
 *  @version     1.0, 23-Dec-2015
 *  @author brijesh
 */
public enum PackagingTypeRuleCriteria {

    BRAND("BRAND"), 
    LABEL("LABEL");
    private String     name;

    private PackagingTypeRuleCriteria(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}



