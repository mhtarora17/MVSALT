/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.rule.dto;

/**
 *  
 *  @version     1.0, 23-Dec-2015
 *  @author brijesh
 */
public class SubCategoryRuleCreationDTO extends StoreRuleCreationDTO{
    
    private String subCategory;
    
    private String brand ;
    
    private String superCat ;
    
    private boolean enabled;

    public String getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getSuperCat() {
        return superCat;
    }

    public void setSuperCat(String superCat) {
        this.superCat = superCat;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public String toString() {
        return "CategoryRuleCreationDTO [brand=" + brand + ", superCat=" + superCat +", SubCategory=" + subCategory +" ,Enabled = "+enabled+", common="+super.toString()+ "]";
    }
    
    
}
