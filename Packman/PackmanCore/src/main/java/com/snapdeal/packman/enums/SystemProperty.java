/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.packman.enums;

/**
 * 
 * @version 1.0, 08-Dec-2015
 * @author indrajit
 */

public enum SystemProperty {
    HOST_NAME("hostName");

    private String code;

    private SystemProperty(String code) {
	this.code = code;
    }

    public String getCode() {
	return this.code;
    }

}
