/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.enums;

/**
 *  
 *  @version     1.0, 25-Dec-2015
 *  @author indrajit
 */
public enum PackagingRuleOrder {
    
    SUPCLEVEL("supclevel",  1),
    SUBCATAGEORYLEVEL("subcatagorylevel", 2),
    CATAGORYLEVEL("catagorylevel", 3),
    STORELEVEL("storelevel", 4);
    
    String name;
    int sequence;
    
    private PackagingRuleOrder(String name, int sequence ){
	this.name = name;
	this.sequence = sequence;
    }
    
    public String getName(){
	return name;
    }
    
    public int getSequence(){
	return sequence;
    }
    
    public static PackagingRuleOrder getPackagingRuleOrder(String s){
	PackagingRuleOrder[] arr = PackagingRuleOrder.values();
	for(PackagingRuleOrder p: arr){
	    if(p.getName().equals(s)){
		return p;
	    }
	}
	return null;
    }

}
