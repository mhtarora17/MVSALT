/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.dto;

/**
 *  
 *  @version     1.0, 09-Dec-2015
 *  @author indrajit
 */
public class RoleDto {
    
    private String code;
    
    private String description;
    
    private String parentRole;
    
    private boolean enabled;
    
    private String defaultUrl;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getParentRole() {
        return parentRole;
    }

    public void setParentRole(String parentRole) {
        this.parentRole = parentRole;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getDefaultUrl() {
        return defaultUrl;
    }

    public void setDefaultUrl(String defaultUrl) {
        this.defaultUrl = defaultUrl;
    }

    @Override
    public String toString() {
	return "RoleDto [code=" + code + ", description=" + description
		+ ", parentRole=" + parentRole + ", enabled=" + enabled
		+ ", defaultUrl=" + defaultUrl + "]";
    }
    
    

}
