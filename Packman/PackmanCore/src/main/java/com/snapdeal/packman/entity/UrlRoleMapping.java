/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

/**
 *  
 *  @version     1.0, 07-Dec-2015
 *  @author indrajit
 */
@Entity
@Table(name = "url_role_mapping")
@Audited
public class UrlRoleMapping implements Serializable{

    /**
     * 
     */
    private static final long serialVersionUID = 1632562365923L;
    
    private Integer         id;
    private Integer	    url_id;
    private String	    role_code;
    private Boolean 	    enabled;
    private Date            created;
    private Date            lastUpdated;
    private String          updatedBy;
    
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    
    @Column(name = "role_code", nullable = false)
    public String getRole_code() {
        return role_code;
    }
    public void setRole_code(String role_code) {
        this.role_code = role_code;
    }
    @Column(name = "url_id", nullable = false)
    public Integer getUrl_id() {
        return url_id;
    }
    public void setUrl_id(Integer url_id) {
        this.url_id = url_id;
    }
    @Column(name = "enabled", nullable = false)
    public Boolean getEnabled() {
        return enabled;
    }
    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
    
   
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created", nullable = false)
    public Date getCreated() {
        return created;
    }
    public void setCreated(Date created) {
        this.created = created;
    }
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_updated", length = 19)
    public Date getLastUpdated() {
        return lastUpdated;
    }
    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }
    @Column(name = "updated_by", nullable = false)
    public String getUpdatedBy() {
        return updatedBy;
    }
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }
    @Override
    public String toString() {
	return "UrlRoleMapping [url_id=" + url_id + ", role_code=" + role_code
		+ ", enabled=" + enabled + ", created=" + created
		+ ", lastUpdated=" + lastUpdated + ", updatedBy=" + updatedBy
		+ "]";
    }
    
    
    

}
