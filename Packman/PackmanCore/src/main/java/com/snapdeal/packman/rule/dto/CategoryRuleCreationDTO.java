/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.rule.dto;


/**
 *  
 *  @version     1.0, 22-Dec-2015
 *  @author brijesh
 */
public class CategoryRuleCreationDTO extends StoreRuleCreationDTO{

    private String category;
    
    private String brand ;
    
    private String superCat ;
    
    private boolean enabled;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getSuperCat() {
        return superCat;
    }

    public void setSuperCat(String superCat) {
        this.superCat = superCat;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public String toString() {
        return "CategoryRuleCreationDTO [brand=" + brand + ", superCat=" + superCat +", Category=" + category +", Enabled = "+enabled+", common="+super.toString()+ "]";
    }
    
}
