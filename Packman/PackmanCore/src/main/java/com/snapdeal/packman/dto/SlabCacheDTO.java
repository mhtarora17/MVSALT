/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.packman.dto;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;

/**
 * @version 1.0, 07-Sep-2015
 * @author ankur/shiv
 */
public class SlabCacheDTO {

    private IDependency              dep                   = new Dependency();

    public static final SlabCacheDTO DEFAULT_SLAB_SENTINAL = new SlabCacheDTO();
    private static final Logger      LOG                   = LoggerFactory.getLogger(SlabCacheDTO.class);
    private Integer                  slabSize;
    private Integer                  lowerLimit;
    private List<SlabItemDTO>        boxes;

    public Integer getSlabSize() {
        return slabSize;
    }

    public void setSlabSize(Integer slabSize) {
        this.slabSize = slabSize;
    }

    public Integer getLowerLimit() {
        return lowerLimit;
    }

    public void setLowerLimit(Integer lowerLimit) {
        this.lowerLimit = lowerLimit;
    }

    public List<SlabItemDTO> getBoxes() {
        return boxes;
    }

    public void setBoxes(List<SlabItemDTO> boxes) {
        this.boxes = boxes;
    }

    /**
     * Most preferred box has vol. weight higher or equal to the supc vol. weight <br>
     * In future this algo can be extended to use the dimensions of box (L,B,H) to recommend most appropriate box.
     * 
     * @param volWeight
     * @param length
     * @param breadth
     * @param height
     * @return
     */
    public SlabItemDTO getMostPreferredBox(RecommendationParamsDTO recoDto) {
        SlabItemDTO mostPreferredBox = new SlabItemDTO();

        if (this == DEFAULT_SLAB_SENTINAL) {
            mostPreferredBox.setCode(ConfigUtils.getStringScalar(Property.PACKMAN_DEFAULT_RECO_AIR)); //TODO we might have to stop recommending default.
            return mostPreferredBox;
        }

        if (CollectionUtils.isEmpty(boxes)) {
            LOG.warn("empty list of boxes received , hence retuning null for mostPreferred box");
            return mostPreferredBox;
        }

        if (boxes.size() == 1) {
            return boxes.get(0);
        }
        double minPositiveDiff = Double.MAX_VALUE;

        Double[] item = { recoDto.getLength(), recoDto.getBreadth(), recoDto.getHeight() };


        boolean useLBHForRecommendation = recoDto.isRecommendationBasedOnLBH();


        //double 
        //String mostPreferredBox = null;
        for (SlabItemDTO dto : boxes) {
            if (dto != null) {
                double localdiff = dto.getVolWeight() - recoDto.getVolWeight();
                if (localdiff >= 0 && localdiff < minPositiveDiff) {
                    if (!useLBHForRecommendation || (isBoxOptimal(recoDto, dto.getVolWeight()) && canItemFitIntoBox(item,recoDto, dto.getLength(), dto.getBreadth(), dto.getHeight()))) {
                        minPositiveDiff = localdiff;
                        mostPreferredBox = dto;
                    }
                }
            }
        }

        if (mostPreferredBox.getCode() == null) {
            LOG.warn("This should not happen, could not find any nearest box in list ");
        }
        return mostPreferredBox;
    }

    /**
     * Check whether item is fittable or not into given box with allowable delta
     * 
     * @param recoDto
     * @param boxVolWt
     * @return
     */

    private boolean isBoxOptimal(RecommendationParamsDTO recoDto, Double boxVolWt) {
        if (recoDto.isRecommendationBasedOnDeltaVolWt()) {
            double volWtCrit1 = recoDto.getVolWeight() * recoDto.getRecommendationDeltaMultFactor();
            double volWtCrit2 = recoDto.getVolWeight() + recoDto.getRecommendationDeltaVolWtOffset();
            Double volWtAllowed = Math.min(volWtCrit1, volWtCrit2);
            if (boxVolWt >= recoDto.getVolWeight() && boxVolWt <= volWtAllowed) {
                return true;
            }
            LOG.info("This box is too large. Item's Vol wt [{}] and vol wt with delta [{}] and Box's Vol Wt [{}]", recoDto.getVolWeight(), volWtAllowed, boxVolWt);
            return false;
        }
        return true;
    }

    /**
     * Provide all boxes of the slab. if default provide, default
     * 
     * @return
     */
    public List<String> getRecommedationList() {
        if (this == DEFAULT_SLAB_SENTINAL) {
            return Arrays.asList(new String[] { ConfigUtils.getStringScalar(Property.PACKMAN_DEFAULT_RECO_AIR) });
        }

        List<String> boxCodes = new ArrayList<String>();
        for (SlabItemDTO dto : boxes) {
            if (dto != null) {
                boxCodes.add(dto.getCode());
            }
        }
        return boxCodes;
    }

    /**
     * Slab can adjust SUPC's col. weight if slab has minimum one box such that the box has vol. weight equal or higher
     * than of SUPC. <br>
     * In future this algo can be extended to use the dimensions of box (L,B,H) to recommend most appropriate box.
     * 
     * @param volWeight
     * @param pauSRO
     * @return
     */
    public boolean canSlabAdjustVolWeight(RecommendationParamsDTO recoDto) {

        if (com.snapdeal.base.utils.CollectionUtils.isEmpty(getBoxes())) {
            return false;
        }

        // No need to check for null since exception is thrown while calculating
        // vol. weight using lbh if needed
        Double item[] = { recoDto.getLength(), recoDto.getBreadth(), recoDto.getHeight() };

        double weight = recoDto.getVolWeight().doubleValue();
        boolean bFoundHigherBox = false;
        if (isSlabOptimal(recoDto)) {
            for (SlabItemDTO o : getBoxes()) {
                if (o.getVolWeight() >= weight) {

                    if (recoDto.isRecommendationBasedOnLBH()) {
                        if (isBoxOptimal(recoDto, o.getVolWeight()) && canItemFitIntoBox(item,recoDto, o.getLength(), o.getBreadth(), o.getHeight())) {
                            bFoundHigherBox = true;
                            break;
                        }
                    } else {
                        bFoundHigherBox = true;
                        break;
                    }
                }
            }
        }

        return bFoundHigherBox;
    }

    private boolean canItemFitIntoBox(Double[] item, RecommendationParamsDTO recoDto, Double length, Double breadth, Double height) {
        Double[] box = { length, breadth, height };
        Arrays.sort(item, Collections.reverseOrder());
        Arrays.sort(box, Collections.reverseOrder());

        Double margin = recoDto.getRecommendationLBHMargin();

        if ((item[0] + margin) <= box[0] && (item[1] + margin) <= box[1] && (item[2] + margin) <= box[2]) {
            LOG.info("box is found with margin...");
            return true;
        }
        return false;
    }

    public static class SlabItemDTO implements Comparable<SlabItemDTO> {

        private String code;
        private Double length;
        private Double breadth;
        private Double height;
        private double volWeight;
        private String description;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public Double getLength() {
            return length;
        }

        public void setLength(Double length) {
            this.length = length;
        }

        public Double getBreadth() {
            return breadth;
        }

        public void setBreadth(Double breadth) {
            this.breadth = breadth;
        }

        public Double getHeight() {
            return height;
        }

        public void setHeight(Double height) {
            this.height = height;
        }

        public double getVolWeight() {
            return volWeight;
        }

        public void setVolWeight(double volWeight) {
            this.volWeight = volWeight;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        @Override
        public int compareTo(SlabItemDTO o) {
            return Double.compare(getVolWeight(), o.getVolWeight());
        }

        @Override
        public String toString() {
            return "SlabItemDTO [code=" + code + ", length=" + length + ", breadth=" + breadth + ", height=" + height + ", volWeight=" + volWeight + ", description=" + description
                    + "]";
        }

    }

    @Override
    public String toString() {
        return "SlabCacheDTO [slabSize=" + slabSize + ", lowerLimit=" + lowerLimit + ", boxes=" + boxes + "]";
    }

    /**
     * check whether vol. weight of product with allowed delta is lying in the slab or not.
     * 
     * @param recoDto
     * @return
     */
    public boolean isSlabOptimal(RecommendationParamsDTO recoDto) {
        if (recoDto.isRecommendationBasedOnDeltaVolWt()) {
            double volWtCrit1 = recoDto.getVolWeight() * recoDto.getRecommendationDeltaMultFactor();
            double volWtCrit2 = recoDto.getVolWeight() + recoDto.getRecommendationDeltaVolWtOffset();
            Double volWtWithDelta = Math.min(volWtCrit1, volWtCrit2);
            if (!((recoDto.getVolWeight() > (getLowerLimit() + getSlabSize()) || (volWtWithDelta < getLowerLimit())))) {
                return true;
            } else {
                LOG.info("Not looking into this slab since vol wt [{}] and vol wt with delta [{}] is not lying in slab range [{}-{}]", recoDto.getVolWeight(), volWtWithDelta, getLowerLimit(),
                        (getLowerLimit() + getSlabSize()));
                return false;
            }
        } else {
            return true;
        }
    }

    public interface IDependency {

        boolean getBooleanScalar(Property property);

        Object reverseOrder();

        Double getDoubleScalar(Property packmanRecoDeltaVolWtMultFactor);

    }

    static class Dependency implements IDependency {
        @Override
        public boolean getBooleanScalar(Property property) {
            return ConfigUtils.getBooleanScalar(property);
        }

        @Override
        public Double getDoubleScalar(Property packmanRecoDeltaVolWtMultFactor) {
            return ConfigUtils.getDoubleScalar(packmanRecoDeltaVolWtMultFactor);
        }

        @Override
        public Object reverseOrder() {
            return Collections.reverseOrder();
        }
    }

}
