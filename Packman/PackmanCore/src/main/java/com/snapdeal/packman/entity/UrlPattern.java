/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

/**
 *  
 *  @version     1.0, 07-Dec-2015
 *  @author indrajit
 */
@Entity
@Table(name = "url_pattern")
@Audited
public class UrlPattern implements Serializable{

    /**
     * 
     */
    private static final long serialVersionUID = 1253325325L;
    
    private Integer id;
    private String url;
    private Boolean enanble;
    private Date created;
    private Date lastUpdated;
    private String updatedBy;
    private Set<UrlRoleMapping> urlRoalMappingSet;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
	return id;
    }

    public void setId(Integer id) {
	this.id = id;
    }
    
    @Column(name = "url", nullable = false)
    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }
    
    @Column(name = "enabled", nullable = false)
    public Boolean getEnanble() {
        return enanble;
    }
    public void setEnanble(Boolean enanble) {
        this.enanble = enanble;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created", nullable = false)
    public Date getCreated() {
        return created;
    }
    public void setCreated(Date created) {
        this.created = created;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_updated", length = 19)
    public Date getLastUpdated() {
        return lastUpdated;
    }
    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }
    
    @Column(name = "updated_by", nullable = false)
    public String getUpdatedBy() {
        return updatedBy;
    }
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }
    
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "url_id")
    public Set<UrlRoleMapping> getUrlRoalMappingSet() {
        return urlRoalMappingSet;
    }

    public void setUrlRoalMappingSet(Set<UrlRoleMapping> urlRoalMappingSet) {
        this.urlRoalMappingSet = urlRoalMappingSet;
    }

    @Override
    public String toString() {
	return "UrlPattern [id=" + id + ", url=" + url + ", enanble=" + enanble
		+ ", created=" + created + ", lastUpdated=" + lastUpdated
		+ ", updatedBy=" + updatedBy + "]";
    }
    
    
    

}
