/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.packman.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

/**
 * @version 1.0, 04-Sep-2015
 * @author ankur
 */

@Entity
@Table(name = "slab")
@Audited
public class Slab implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1114178081651439699L;

    private Integer                id;
    private String                 name;
    private Integer                slabSize;
    private Integer                lowerLimit;
    private PackagingType          packagingType;
    private Date                   created;
    private Date                   lastUpdated;
    private String                 updatedBy;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "name",  nullable = false, length = 48)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "slab_size", nullable = false)
    public Integer getSlabSize() {
        return slabSize;
    }

    public void setSlabSize(Integer slabSize) {
        this.slabSize = slabSize;
    }

    @Column(name = "lower_limit", nullable = false)
    public Integer getLowerLimit() {
        return lowerLimit;
    }

    public void setLowerLimit(Integer lowerLimit) {
        this.lowerLimit = lowerLimit;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "packaging_type_id")
    public PackagingType getPackagingType() {
        return packagingType;
    }

    public void setPackagingType(PackagingType packagingType) {
        this.packagingType = packagingType;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created", nullable = false)
    public Date getCreated() {
        return created;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_updated", length = 19)
    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    @Column(name = "updated_by", nullable = false)
    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Override
    public String toString() {
        return "Slab [id=" + id + ", name=" + name + ", slabSize=" + slabSize + ", lowerLimit=" + lowerLimit 
                + ", packagingType=" + packagingType + ", created=" + created + ", lastUpdated=" + lastUpdated + ", updatedBy=" + updatedBy + "]";
    }

}
