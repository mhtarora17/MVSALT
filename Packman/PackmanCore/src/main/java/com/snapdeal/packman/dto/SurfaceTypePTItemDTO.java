/**
 *  Copyright 2016 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.dto;

import java.util.List;

/**
 *  
 *  @version     1.0, 25-Feb-2016
 *  @author brijesh
 */
public class SurfaceTypePTItemDTO extends PackagingTypeItemDTO{

    public PackagingTypeItemDTO getAppropriateBox(RecommendationParamsDTO dto, List<PackagingTypeItemDTO> packagingTypeItems){
        return this;
    }
}
