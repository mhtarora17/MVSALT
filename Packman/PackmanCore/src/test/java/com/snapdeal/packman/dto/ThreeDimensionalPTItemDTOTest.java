/**
 *  Copyright 2016 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.dto;

import static org.junit.Assert.*;

import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 *  
 *  @version     1.0, 06-Mar-2016
 *  @author brijesh
 */
public class ThreeDimensionalPTItemDTOTest {

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void testCompareTo() {

        ThreeDimensionalPTItemDTO dto1 = new ThreeDimensionalPTItemDTO(1.0,1.0,1.0,"CB01");
        ThreeDimensionalPTItemDTO dto2 = new ThreeDimensionalPTItemDTO(1.0,1.0,1.0,"CB02");

        Assert.assertTrue(dto1.compareTo(dto2) == 0);

    }

    @Test
    public void testCompare() {

        ThreeDimensionalPTItemDTO dto1 = new ThreeDimensionalPTItemDTO(1.0,1.0,1.0,"CB01");
        ThreeDimensionalPTItemDTO dto2 = new ThreeDimensionalPTItemDTO(1.0,1.0,1.0,"CB02");

        Assert.assertTrue(dto1.compare(dto1, dto2) == 0);

    }

    @Test
    public void testCompareV1() {

        ThreeDimensionalPTItemDTO dto1 = new ThreeDimensionalPTItemDTO(1.0,1.0,1.0,"CB01");
        ThreeDimensionalPTItemDTO dto2 = new ThreeDimensionalPTItemDTO(0.5,1.0,2.0,"CB02");

        Assert.assertTrue(dto1.compare(dto1,dto2)==0);

    }

    @Test
    public void testCompareV3() {

        ThreeDimensionalPTItemDTO dto1 = new ThreeDimensionalPTItemDTO(1.0,1.0,1.0,"CB01");
        ThreeDimensionalPTItemDTO dto2 = new ThreeDimensionalPTItemDTO(0.5,2.0,2.0,"CB02");

        Assert.assertTrue(dto1.compare(dto1,dto2)==-1);

    }

    @Test
    public void testCompareV4() {

        ThreeDimensionalPTItemDTO dto1 = new ThreeDimensionalPTItemDTO(1.0,10.0,1.0,"CB01");
        ThreeDimensionalPTItemDTO dto2 = new ThreeDimensionalPTItemDTO(0.5,2.0,2.0,"CB02");

        Assert.assertTrue(dto1.compare(dto1,dto2)==1);

    }

    @Test
    public void testIsValid3dDto() {

        ThreeDimensionalPTItemDTO dto = new ThreeDimensionalPTItemDTO(1.0,1.0,1.0,"CB01");
        Assert.assertTrue(dto.isValid3dDto());

    }

    @Test
    public void testIsValid3dDtoV1() {

        ThreeDimensionalPTItemDTO dto = new ThreeDimensionalPTItemDTO(1.0,-1.0,1.0,"CB01");
        Assert.assertFalse(dto.isValid3dDto());

    }

    @Test
    public void testIsValid3dDtoV2() {

        ThreeDimensionalPTItemDTO dto = new ThreeDimensionalPTItemDTO(1.0,1.0,0.0,"CB01");
        Assert.assertFalse(dto.isValid3dDto());

    }

    @Test
    public void testGetAppropriateBox() {

        RecommendationParamsDTO dto;
        ThreeDimensionalPTItemDTO box ;
        List<ThreeDimensionalPTItemDTO> boxes = new ArrayList<>();

        dto = new RecommendationParamsDTO("Dummy");
        dto.setLength(1.0);
        dto.setBreadth(1.0);
        dto.setHeight(1.0);
        dto.setRecommendationLBHMargin(0.0);

        box = new ThreeDimensionalPTItemDTO(1.0,1.0,1.0,"CB01");
        boxes.add(box);

        box = new ThreeDimensionalPTItemDTO(2.0,2.0,2.0,"CB02");
        boxes.add(box);

        PackagingTypeItemDTO result = box.getAppropriateBox(dto,boxes);

        Assert.assertEquals(result.getBoxName(),"CB01");

    }

    @Test
    public void testGetAppropriateBoxV1() {

        RecommendationParamsDTO dto;
        ThreeDimensionalPTItemDTO box ;
        List<ThreeDimensionalPTItemDTO> boxes = new ArrayList<>();

        dto = new RecommendationParamsDTO("Dummy");
        dto.setLength(1.0);
        dto.setBreadth(1.0);
        dto.setHeight(1.0);
        dto.setRecommendationLBHMargin(0.1);

        box = new ThreeDimensionalPTItemDTO(1.0,1.0,1.0,"CB01");
        boxes.add(box);

        box = new ThreeDimensionalPTItemDTO(2.0,2.0,2.0,"CB02");
        boxes.add(box);

        PackagingTypeItemDTO result = box.getAppropriateBox(dto,boxes);

        Assert.assertEquals(result.getBoxName(),"CB02");

    }

    @Test
    public void testGetAppropriateBoxV2() {

        RecommendationParamsDTO dto;
        ThreeDimensionalPTItemDTO box ;
        List<ThreeDimensionalPTItemDTO> boxes = new ArrayList<>();

        dto = new RecommendationParamsDTO("Dummy");
        dto.setLength(1.0);
        dto.setBreadth(1.0);
        dto.setHeight(1.0);
        dto.setRecommendationLBHMargin(1.1);

        box = new ThreeDimensionalPTItemDTO(1.0,1.0,1.0,"CB01");
        boxes.add(box);

        box = new ThreeDimensionalPTItemDTO(2.0,2.0,2.0,"CB02");
        boxes.add(box);

        PackagingTypeItemDTO result = box.getAppropriateBox(dto,boxes);

        Assert.assertNull(result.getBoxName());

    }

    @Test
    public void testIsProductFittableIntoBox() {

        Double[] productDimensions = new Double[]{1.0,1.0,1.0};
        Double[] boxDimensions = new Double[]{1.0,1.0,1.0};
        boolean sortRequired = false;

        Assert.assertTrue(ThreeDimensionalPTItemDTO.isProductFittableIntoBox(productDimensions, boxDimensions, sortRequired));

    }

    @Test
    public void testIsProductFittableIntoBoxV1() {

        Double[] productDimensions = new Double[]{1.0,1.0,1.0};
        Double[] boxDimensions = new Double[]{1.0,1.0,0.9999};
        boolean sortRequired = false;

        Assert.assertFalse(ThreeDimensionalPTItemDTO.isProductFittableIntoBox(productDimensions, boxDimensions, sortRequired));

    }

    @Test
    public void testIsProductFittableIntoBoxV2() {

        Double[] productDimensions = new Double[]{1.0,1.0,1.0};
        Double[] boxDimensions = new Double[]{1.0,1.0,0.9999};
        boolean sortRequired = true;

        Assert.assertFalse(ThreeDimensionalPTItemDTO.isProductFittableIntoBox(productDimensions, boxDimensions, sortRequired));

    }

    @Test
    public void testIsProductFittableIntoBoxV3() {

        Double[] productDimensions = new Double[]{1.0,12.0,3.0};
        Double[] boxDimensions = new Double[]{12.0,1.0,3.0};
        boolean sortRequired = true;

        Assert.assertTrue(ThreeDimensionalPTItemDTO.isProductFittableIntoBox(productDimensions, boxDimensions, sortRequired));

    }

    @Test
    public void testIsProductFittableIntoBoxV4() {

        Double[] productDimensions = new Double[]{1.0,12.0,3.0};
        Double[] boxDimensions = new Double[]{12.0,1.0,3.0};
        boolean sortRequired = false;

        Assert.assertFalse(ThreeDimensionalPTItemDTO.isProductFittableIntoBox(productDimensions, boxDimensions, sortRequired));

    }

}
