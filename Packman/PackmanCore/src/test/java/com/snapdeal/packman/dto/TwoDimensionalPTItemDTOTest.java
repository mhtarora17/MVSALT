/**
 *  Copyright 2016 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.dto;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 *  
 *  @version     1.0, 04-Mar-2016
 *  @author brijesh
 */
public class TwoDimensionalPTItemDTOTest {
    
    TwoDimensionalPTItemDTO twoDimensionalPackagingTypeDto;

    @Before
    public void setUp() throws Exception {
        twoDimensionalPackagingTypeDto = new TwoDimensionalPTItemDTO();
    }

    @Test
    public void testCompareToV1() {
        
        TwoDimensionalPTItemDTO dto1 = new TwoDimensionalPTItemDTO();
        dto1.setLength(1.0);
        dto1.setBreadth(1.0);

        TwoDimensionalPTItemDTO dto = new TwoDimensionalPTItemDTO();
        dto.setLength(1.0);
        dto.setBreadth(1.0);
        
        Assert.assertTrue(dto1.compareTo(dto)==0);
        
    }
    
    @Test
    public void testCompareToV2() {
        twoDimensionalPackagingTypeDto.setLength(1.0);
        twoDimensionalPackagingTypeDto.setBreadth(1.0);

        TwoDimensionalPTItemDTO dto = new TwoDimensionalPTItemDTO();
        dto.setLength(1.0);
        dto.setBreadth(2.0);
        
        Assert.assertTrue(twoDimensionalPackagingTypeDto.compareTo(dto)<0);
        
    }
    
    @Test
    public void testCompareToV3() {
        twoDimensionalPackagingTypeDto.setLength(1.0);
        twoDimensionalPackagingTypeDto.setBreadth(1.0);

        TwoDimensionalPTItemDTO dto = new TwoDimensionalPTItemDTO();
        dto.setLength(1.0);
        dto.setBreadth(0.7);
        
        Assert.assertTrue(twoDimensionalPackagingTypeDto.compareTo(dto)>0);
        
    }

    @Test
    public void testCompareV1() {

        TwoDimensionalPTItemDTO dto1 = new TwoDimensionalPTItemDTO(1.0,1.0);
        TwoDimensionalPTItemDTO dto2 = new TwoDimensionalPTItemDTO(2.0,0.0);

        Assert.assertTrue(dto1.compare(dto1, dto2)==0);

    }
    @Test
    public void testCompareV2() {

        TwoDimensionalPTItemDTO dto1 = new TwoDimensionalPTItemDTO(1.0,1.01);
        TwoDimensionalPTItemDTO dto2 = new TwoDimensionalPTItemDTO(2.0,0.1);

        Assert.assertTrue(dto1.compare(dto1,dto2)==-1);

    }

    @Test
    public void testCompareV3() {

        TwoDimensionalPTItemDTO dto1 = new TwoDimensionalPTItemDTO(1.0,1.1);
        TwoDimensionalPTItemDTO dto2 = new TwoDimensionalPTItemDTO(2.0,0.01);

        Assert.assertTrue(dto1.compare(dto1,dto2)==1);

    }

    @Test
    public void testGetAppropriateBox() {

        TwoDimensionalPTItemDTO box;
        RecommendationParamsDTO dto;
        List<TwoDimensionalPTItemDTO> boxes = new ArrayList<>();

        dto = new RecommendationParamsDTO("Dummy");
        dto.setLength(7.0);
        dto.setBreadth(3.0);
        dto.setHeight(15.0);

        box = new TwoDimensionalPTItemDTO(5.0,5.0,"PB01","NORMAL","5X5");
        boxes.add(box);

        box = new TwoDimensionalPTItemDTO(18.0,10.0,"PB02","NORMAL","18X22");
        boxes.add(box);

        box = new TwoDimensionalPTItemDTO(30.3,24.0,"PB03","NORMAL","30.3X24");
        boxes.add(box);

        PackagingTypeItemDTO result = box.getAppropriateBox(dto,boxes);

        Assert.assertEquals(result.getBoxName(),"PB02");

    }

    @Test
    public void testGetAppropriateBoxV1() {

        TwoDimensionalPTItemDTO box;
        RecommendationParamsDTO dto;
        List<TwoDimensionalPTItemDTO> boxes = new ArrayList<>();

        dto = new RecommendationParamsDTO("Dummy");
        dto.setLength(7.0);
        dto.setBreadth(3.0);
        dto.setHeight(15.0);

        box = new TwoDimensionalPTItemDTO(5.0,5.0,"PB01","NORMAL","5X5");
        boxes.add(box);

        box = new TwoDimensionalPTItemDTO(17.99,10.0,"PB02","NORMAL","17.99X22");
        boxes.add(box);

        box = new TwoDimensionalPTItemDTO(30.3,24.0,"PB03","NORMAL","30.3X24");
        boxes.add(box);

        PackagingTypeItemDTO result = box.getAppropriateBox(dto,boxes);

        Assert.assertEquals(result.getBoxName(),"PB03");

    }

    @Test
    public void testGetAppropriateBoxV2() {

        TwoDimensionalPTItemDTO box;
        RecommendationParamsDTO dto;
        List<TwoDimensionalPTItemDTO> boxes = new ArrayList<>();

        dto = new RecommendationParamsDTO("Dummy");
        dto.setLength(23.5);
        dto.setBreadth(17.2);
        dto.setHeight(6.8);

        box = new TwoDimensionalPTItemDTO(5.0,5.0,"PB01","NORMAL","5X5");
        boxes.add(box);

        box = new TwoDimensionalPTItemDTO(17.99,10.0,"PB02","NORMAL","17.99X22");
        boxes.add(box);

        box = new TwoDimensionalPTItemDTO(30.3,24.0,"PB03","NORMAL","30.3X24");
        boxes.add(box);

        PackagingTypeItemDTO result = box.getAppropriateBox(dto,boxes);

        Assert.assertEquals(result.getBoxName(),"PB03");

    }

    @Test
    public void testGetAppropriateBoxV3() {

        TwoDimensionalPTItemDTO box;
        RecommendationParamsDTO dto;
        List<TwoDimensionalPTItemDTO> boxes = new ArrayList<>();

        dto = new RecommendationParamsDTO("Dummy");
        dto.setLength(23.5);
        dto.setBreadth(17.2);
        dto.setHeight(6.8);

        box = new TwoDimensionalPTItemDTO(5.0,5.0,"PB01","NORMAL","5X5");
        boxes.add(box);

        box = new TwoDimensionalPTItemDTO(17.99,10.0,"PB02","NORMAL","17.99X22");
        boxes.add(box);

        box = new TwoDimensionalPTItemDTO(30.2999,24.0,"PB03","NORMAL","30.3X24");
        boxes.add(box);

        PackagingTypeItemDTO result = box.getAppropriateBox(dto,boxes);

        Assert.assertNull(result.getBoxName());

    }

    @Test
    public void testIsProductFittableIntoBoxV1() {

        Double[] productDimensions = new Double[]{15.0,7.0,3.0};
        TwoDimensionalPTItemDTO box = new TwoDimensionalPTItemDTO(10.0,18.0);
        box.setBoxName("PB01");

        Assert.assertTrue(TwoDimensionalPTItemDTO.isProductFittableIntoBox(productDimensions,box));

    }

    @Test
    public void testIsProductFittableIntoBoxV2() {

        Double[] productDimensions = new Double[]{15.0,7.0,3.0};
        TwoDimensionalPTItemDTO box = new TwoDimensionalPTItemDTO(18.0,10.0);
        box.setBoxName("PB01");

        Assert.assertTrue(TwoDimensionalPTItemDTO.isProductFittableIntoBox(productDimensions, box));

    }

    @Test
    public void testIsProductFittableIntoBoxV3() {

        Double[] productDimensions = new Double[]{3.0,15.0,7.0};
        TwoDimensionalPTItemDTO box = new TwoDimensionalPTItemDTO(18.0,10.0);
        box.setBoxName("PB01");

        Assert.assertTrue(TwoDimensionalPTItemDTO.isProductFittableIntoBox(productDimensions, box));

    }

    @Test
    public void testIsProductFittableIntoBoxV4() {

        Double[] productDimensions = new Double[]{3.0,15.0,7.0};
        TwoDimensionalPTItemDTO box = new TwoDimensionalPTItemDTO(17.0,10.0);
        box.setBoxName("PB01");

        Assert.assertFalse(TwoDimensionalPTItemDTO.isProductFittableIntoBox(productDimensions, box));

    }

    @Test
    public void testIsProductFittableIntoBoxV5() {

        Double[] productDimensions = new Double[]{3.0,15.0,7.0};
        TwoDimensionalPTItemDTO box = new TwoDimensionalPTItemDTO(17.99,10.0);
        box.setBoxName("PB01");

        Assert.assertFalse(TwoDimensionalPTItemDTO.isProductFittableIntoBox(productDimensions,box));

    }

    @Test
    public void testIsProductFittableIntoBoxV6() {

        Double[] productDimensions = new Double[]{3.0,15.0,7.0};
        TwoDimensionalPTItemDTO box = new TwoDimensionalPTItemDTO(17.99,10.01);
        box.setBoxName("PB01");

        Assert.assertFalse(TwoDimensionalPTItemDTO.isProductFittableIntoBox(productDimensions,box));

    }

    @Test
    public void testIsProductFittableIntoBoxV7() {

        Double[] productDimensions = new Double[]{17.2,6.8,23.5};
        TwoDimensionalPTItemDTO box = new TwoDimensionalPTItemDTO(30.3,24.0);
        box.setBoxName("PB01");

        Assert.assertTrue(TwoDimensionalPTItemDTO.isProductFittableIntoBox(productDimensions, box));

    }

    // Testing all the six use case
    @Test
    public void testIsProductFittableIntoBoxV1_1() {

        Double[] productDimensions = new Double[]{7.0,3.0,15.0};
        TwoDimensionalPTItemDTO box = new TwoDimensionalPTItemDTO(10.0,18.0);
        box.setBoxName("PB01");

        Assert.assertTrue(TwoDimensionalPTItemDTO.isProductFittableIntoBox(productDimensions, box));

    }

    // Testing all the six use case
    @Test
    public void testIsProductFittableIntoBoxV1_2() {

        Double[] productDimensions = new Double[]{7.0,15.0,3.0};
        TwoDimensionalPTItemDTO box = new TwoDimensionalPTItemDTO(10.0,18.0);
        box.setBoxName("PB01");

        Assert.assertTrue(TwoDimensionalPTItemDTO.isProductFittableIntoBox(productDimensions, box));

    }

    // Testing all the six use case
    @Test
    public void testIsProductFittableIntoBoxV1_3() {

        Double[] productDimensions = new Double[]{3.0,7.0,15.0};
        TwoDimensionalPTItemDTO box = new TwoDimensionalPTItemDTO(10.0,18.0);
        box.setBoxName("PB01");

        Assert.assertTrue(TwoDimensionalPTItemDTO.isProductFittableIntoBox(productDimensions, box));

    }

    // Testing all the six use case
    @Test
    public void testIsProductFittableIntoBoxV1_4() {

        Double[] productDimensions = new Double[]{3.0,15.0,7.0};
        TwoDimensionalPTItemDTO box = new TwoDimensionalPTItemDTO(10.0,18.0);
        box.setBoxName("PB01");

        Assert.assertTrue(TwoDimensionalPTItemDTO.isProductFittableIntoBox(productDimensions, box));

    }

    // Testing all the six use case
    @Test
    public void testIsProductFittableIntoBoxV1_5() {

        Double[] productDimensions = new Double[]{15.0,7.0,3.0};
        TwoDimensionalPTItemDTO box = new TwoDimensionalPTItemDTO(10.0,18.0);
        box.setBoxName("PB01");

        Assert.assertTrue(TwoDimensionalPTItemDTO.isProductFittableIntoBox(productDimensions, box));

    }

    // Testing all the six use case
    @Test
    public void testIsProductFittableIntoBoxV1_6() {

        Double[] productDimensions = new Double[]{15.0,3.0,7.0};
        TwoDimensionalPTItemDTO box = new TwoDimensionalPTItemDTO(10.0,18.0);
        box.setBoxName("PB01");

        Assert.assertTrue(TwoDimensionalPTItemDTO.isProductFittableIntoBox(productDimensions, box));

    }

}
