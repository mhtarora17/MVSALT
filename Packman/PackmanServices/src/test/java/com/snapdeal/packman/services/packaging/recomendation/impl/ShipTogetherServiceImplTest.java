/**
 * Copyright 2016 Jasper Infotech (P) Limited . All Rights Reserved.
 * JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.packman.services.packaging.recomendation.impl;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import com.snapdeal.cocofs.configuration.ConfigUtils;
import junit.framework.Assert;

import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import com.snapdeal.base.cache.CacheManager;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.packman.cache.PackagingTypeCache;
import com.snapdeal.packman.configuration.PackagingProperty;
import com.snapdeal.packman.configuration.PackagingTypeItemProperty;
import com.snapdeal.packman.dto.PackagingTypeItemDTO;
import com.snapdeal.packman.dto.ThreeDimensionalPTItemDTO;
import com.snapdeal.packman.entity.PackagingType;
import com.snapdeal.packman.entity.PackagingTypeItem;
import com.snapdeal.packman.entity.PackagingTypeItemProperties;
import com.snapdeal.packman.entity.PackagingTypeProperties;
import com.snapdeal.packman.entity.Store;
import com.snapdeal.packman.enums.PackagingMode;
import com.snapdeal.packman.enums.PackagingTypeCategory;
import com.snapdeal.packman.services.packaging.dto.ProductDetailDTO;
import com.snapdeal.packman.services.packaging.dto.ShipTogetherSearchDTO;

/**
 * @author brijesh
 * @version 1.0, 03-Mar-2016
 */
public class ShipTogetherServiceImplTest {

    static ShipTogetherServiceImpl shipTogetherImpl;
    static Method isFittable3D;
    static Method findAppropriateBox;

    @BeforeClass
    public static void setUp() throws Exception {
        shipTogetherImpl = new ShipTogetherServiceImpl();

        isFittable3D = shipTogetherImpl.getClass().getDeclaredMethod("isFittable3D", new Class[]{List.class, List.class});
        isFittable3D.setAccessible(true);

        findAppropriateBox = shipTogetherImpl.getClass().getDeclaredMethod("findAppropriateBox", new Class[]{ProductMinDetailDTO.class, List.class, boolean.class});
        findAppropriateBox.setAccessible(true);

    }

    private void initializePackagingTypeCache() {
        Store store;
        PackagingType packagingType;
        PackagingTypeItem items;
        PackagingTypeProperties packagingTypeProperty;
        PackagingTypeItemProperties packagingTypeItemProperty;
        List<PackagingTypeItem> packagingTypeItems = new ArrayList<>();
        List<PackagingTypeProperties> ptProperties = new ArrayList<>();
        List<PackagingTypeItemProperties> ptiProperties = new ArrayList<PackagingTypeItemProperties>();

        store = new Store();
        store.setCode("SD");
        store.setName("Snapdeal");
        store.setStoreFrontId("SDL");

        packagingTypeProperty = new PackagingTypeProperties();
        packagingTypeProperty.setName(PackagingProperty.PACKAGING_TYPE_CATEGORY.mode());
        packagingTypeProperty.setValue(PackagingTypeCategory.THREE_DIMENSIONAL.type());

        packagingType = new PackagingType();
        packagingType.setType("CARTON");
        packagingType.setPackagingMode(PackagingMode.NORMAL.mode());
        packagingType.setStore(store);

        packagingTypeProperty.setPackagingType(packagingType);
        ptProperties.add(packagingTypeProperty);
        packagingType.setProperties(ptProperties);

        // CB01 [10X10X10]

        items = new PackagingTypeItem();
        items.setCode("CB01");
        items.setPackagingType(packagingType);

        packagingTypeItemProperty = new PackagingTypeItemProperties();
        packagingTypeItemProperty.setName(PackagingTypeItemProperty.LENGTH.getName());
        packagingTypeItemProperty.setValue("10.0");
        packagingTypeItemProperty.setPackagingTypeItem(items);
        ptiProperties.add(packagingTypeItemProperty);

        packagingTypeItemProperty = new PackagingTypeItemProperties();
        packagingTypeItemProperty.setName(PackagingTypeItemProperty.BREADTH.getName());
        packagingTypeItemProperty.setValue("10.0");
        packagingTypeItemProperty.setPackagingTypeItem(items);
        ptiProperties.add(packagingTypeItemProperty);

        packagingTypeItemProperty = new PackagingTypeItemProperties();
        packagingTypeItemProperty.setName(PackagingTypeItemProperty.HEIGHT.getName());
        packagingTypeItemProperty.setValue("10.0");
        packagingTypeItemProperty.setPackagingTypeItem(items);
        ptiProperties.add(packagingTypeItemProperty);

        packagingTypeItemProperty = new PackagingTypeItemProperties();
        packagingTypeItemProperty.setName(PackagingTypeItemProperty.VOL_WEIGHT.getName());
        packagingTypeItemProperty.setValue("200.0");
        packagingTypeItemProperty.setPackagingTypeItem(items);
        ptiProperties.add(packagingTypeItemProperty);

        items.setProperties(new ArrayList<>(ptiProperties));
        packagingTypeItems.add(items);

        // CB02 [20X20X20]
        items = new PackagingTypeItem();
        items.setCode("CB02");
        items.setPackagingType(packagingType);

        ptiProperties.clear();

        packagingTypeItemProperty = new PackagingTypeItemProperties();
        packagingTypeItemProperty.setName(PackagingTypeItemProperty.LENGTH.getName());
        packagingTypeItemProperty.setValue("20.0");
        packagingTypeItemProperty.setPackagingTypeItem(items);
        ptiProperties.add(packagingTypeItemProperty);

        packagingTypeItemProperty = new PackagingTypeItemProperties();
        packagingTypeItemProperty.setName(PackagingTypeItemProperty.BREADTH.getName());
        packagingTypeItemProperty.setValue("20.0");
        packagingTypeItemProperty.setPackagingTypeItem(items);
        ptiProperties.add(packagingTypeItemProperty);

        packagingTypeItemProperty = new PackagingTypeItemProperties();
        packagingTypeItemProperty.setName(PackagingTypeItemProperty.HEIGHT.getName());
        packagingTypeItemProperty.setValue("20.0");
        packagingTypeItemProperty.setPackagingTypeItem(items);
        ptiProperties.add(packagingTypeItemProperty);

        packagingTypeItemProperty = new PackagingTypeItemProperties();
        packagingTypeItemProperty.setName(PackagingTypeItemProperty.VOL_WEIGHT.getName());
        packagingTypeItemProperty.setValue("1600.0");
        packagingTypeItemProperty.setPackagingTypeItem(items);
        ptiProperties.add(packagingTypeItemProperty);

        items.setProperties(new ArrayList<>(ptiProperties));

        packagingTypeItems.add(items);

        // CB03 [30X30X30]
        items = new PackagingTypeItem();
        items.setCode("CB03");
        items.setPackagingType(packagingType);

        ptiProperties.clear();

        packagingTypeItemProperty = new PackagingTypeItemProperties();
        packagingTypeItemProperty.setName(PackagingTypeItemProperty.LENGTH.getName());
        packagingTypeItemProperty.setValue("30.0");
        packagingTypeItemProperty.setPackagingTypeItem(items);
        ptiProperties.add(packagingTypeItemProperty);

        packagingTypeItemProperty = new PackagingTypeItemProperties();
        packagingTypeItemProperty.setName(PackagingTypeItemProperty.BREADTH.getName());
        packagingTypeItemProperty.setValue("30.0");
        packagingTypeItemProperty.setPackagingTypeItem(items);
        ptiProperties.add(packagingTypeItemProperty);

        packagingTypeItemProperty = new PackagingTypeItemProperties();
        packagingTypeItemProperty.setName(PackagingTypeItemProperty.HEIGHT.getName());
        packagingTypeItemProperty.setValue("30.0");
        packagingTypeItemProperty.setPackagingTypeItem(items);
        ptiProperties.add(packagingTypeItemProperty);

        packagingTypeItemProperty = new PackagingTypeItemProperties();
        packagingTypeItemProperty.setName(PackagingTypeItemProperty.VOL_WEIGHT.getName());
        packagingTypeItemProperty.setValue("5400.0");
        packagingTypeItemProperty.setPackagingTypeItem(items);
        ptiProperties.add(packagingTypeItemProperty);

        items.setProperties(new ArrayList<>(ptiProperties));

        packagingTypeItems.add(items);

        PackagingTypeCache cache = new PackagingTypeCache();
        cache.addPackagingTypeByPTI(packagingTypeItems);
        CacheManager.getInstance().setCache(cache);
    }

    @Test
    public void testGetTotalVolWtOfProductsPosV1() {
        List<ProductDetailDTO> productDetailList = new ArrayList<ProductDetailDTO>();
        Assert.assertEquals(shipTogetherImpl.getTotalVolWtOfProducts(productDetailList), 0.0);
    }

    @Test
    public void testGetTotalVolWtOfProductsNegV1() {
        List<ProductDetailDTO> productDetailList = new ArrayList<ProductDetailDTO>();
        Assert.assertNotSame(shipTogetherImpl.getTotalVolWtOfProducts(productDetailList), 0.1);
    }

    @Test
    public void testGetTotalVolWtOfProductsPosV2() {

        ProductDetailDTO dto = null;
        List<ProductDetailDTO> productDetailList = new ArrayList<ProductDetailDTO>();

        dto = new ProductDetailDTO();
        dto.setQuantity(1);
        dto.setVolWt(0.0);
        productDetailList.add(dto);

        Assert.assertEquals(shipTogetherImpl.getTotalVolWtOfProducts(productDetailList), 0.0);

    }

    @Test
    public void testGetTotalVolWtOfProductsPosV3() {

        ProductDetailDTO dto = null;
        List<ProductDetailDTO> productDetailList = new ArrayList<ProductDetailDTO>();

        dto = new ProductDetailDTO();
        dto.setQuantity(1);
        dto.setVolWt(0.00001);
        productDetailList.add(dto);

        Assert.assertEquals(shipTogetherImpl.getTotalVolWtOfProducts(productDetailList), 0.00001);

    }

    @Test
    public void testGetTotalVolWtOfProductsPosV4() {

        ProductDetailDTO dto = null;
        List<ProductDetailDTO> productDetailList = new ArrayList<ProductDetailDTO>();

        dto = new ProductDetailDTO();
        dto.setQuantity(1);
        dto.setVolWt(1.0);
        productDetailList.add(dto);

        dto = new ProductDetailDTO();
        dto.setQuantity(2);
        dto.setVolWt(2.1);
        productDetailList.add(dto);

        Assert.assertEquals(shipTogetherImpl.getTotalVolWtOfProducts(productDetailList), 5.2);

    }

    @Test
    public void getBreakedBoxesPosV0() {

        ThreeDimensionalPTItemDTO box = new ThreeDimensionalPTItemDTO();
        ProductMinDetailDTO rotatedDto = new ProductMinDetailDTO();
        List<ThreeDimensionalPTItemDTO> result = new ArrayList<ThreeDimensionalPTItemDTO>();

        box.setLength(3.0);
        box.setBreadth(3.0);
        box.setHeight(3.0);

        rotatedDto.setProductDimensions(new Double[]{3.0, 3.0, 3.1});

        for (int i = 1; i <= 6; i++) {
            result = shipTogetherImpl.getBreakedBoxes(null, box, rotatedDto, i);
            Assert.assertTrue(result.size() == 0);
        }

    }

    @Test
    public void getBreakedBoxesPosV0_0() {

        ThreeDimensionalPTItemDTO box = new ThreeDimensionalPTItemDTO();
        ProductMinDetailDTO rotatedDto = new ProductMinDetailDTO();
        int caseToExecute;
        List<ThreeDimensionalPTItemDTO> result = new ArrayList<ThreeDimensionalPTItemDTO>();

        caseToExecute = 1;
        box.setLength(3.0);
        box.setBreadth(3.0);
        box.setHeight(3.0);

        rotatedDto.setProductDimensions(new Double[]{3.0, 3.0, 2.0});

        result = shipTogetherImpl.getBreakedBoxes(null, box, rotatedDto, caseToExecute);
        Assert.assertTrue(result.size() == 1);

        Assert.assertEquals(result.get(0).getLength(), 3.0);
        Assert.assertEquals(result.get(0).getBreadth(), 3.0);
        Assert.assertEquals(result.get(0).getHeight(), 1.0);
    }

    @Test
    public void getBreakedBoxesPosV0_1() {

        ThreeDimensionalPTItemDTO box = new ThreeDimensionalPTItemDTO();
        ProductMinDetailDTO rotatedDto = new ProductMinDetailDTO();
        int caseToExecute;
        List<ThreeDimensionalPTItemDTO> result = new ArrayList<ThreeDimensionalPTItemDTO>();

        caseToExecute = 1;
        box.setLength(3.0);
        box.setBreadth(3.0);
        box.setHeight(3.0);

        rotatedDto.setProductDimensions(new Double[]{3.0, 2.5, 3.0});

        result = shipTogetherImpl.getBreakedBoxes(null, box, rotatedDto, caseToExecute);
        Assert.assertTrue(result.size() == 1);

        Assert.assertEquals(result.get(0).getLength(), 3.0);
        Assert.assertEquals(result.get(0).getBreadth(), 0.5);
        Assert.assertEquals(result.get(0).getHeight(), 3.0);
    }

    @Test
    public void getBreakedBoxesPosV0_2() {

        ThreeDimensionalPTItemDTO box = new ThreeDimensionalPTItemDTO();
        ProductMinDetailDTO rotatedDto = new ProductMinDetailDTO();
        int caseToExecute;
        List<ThreeDimensionalPTItemDTO> result = new ArrayList<ThreeDimensionalPTItemDTO>();

        caseToExecute = 1;
        box.setLength(3.0);
        box.setBreadth(3.0);
        box.setHeight(3.0);

        rotatedDto.setProductDimensions(new Double[]{3.0, 3.0, 2.0});

        result = shipTogetherImpl.getBreakedBoxes(null, box, rotatedDto, caseToExecute);
        Assert.assertTrue(result.size() == 1);

        Assert.assertEquals(result.get(0).getLength(), 3.0);
        Assert.assertEquals(result.get(0).getBreadth(), 3.0);
        Assert.assertEquals(result.get(0).getHeight(), 1.0);
    }

    @Test
    public void getBreakedBoxesPosV0_3() {

        ThreeDimensionalPTItemDTO box = new ThreeDimensionalPTItemDTO();
        ProductMinDetailDTO rotatedDto = new ProductMinDetailDTO();
        int caseToExecute;
        List<ThreeDimensionalPTItemDTO> result = new ArrayList<ThreeDimensionalPTItemDTO>();

        caseToExecute = 1;
        box.setLength(3.0);
        box.setBreadth(3.0);
        box.setHeight(3.0);

        rotatedDto.setProductDimensions(new Double[]{3.0, 2.0, 3.0});

        result = shipTogetherImpl.getBreakedBoxes(null, box, rotatedDto, caseToExecute);
        Assert.assertTrue(result.size() == 1);

        Assert.assertEquals(result.get(0).getLength(), 3.0);
        Assert.assertEquals(result.get(0).getBreadth(), 1.0);
        Assert.assertEquals(result.get(0).getHeight(), 3.0);
    }

    @Test
    public void getBreakedBoxesPosV1() {

        ThreeDimensionalPTItemDTO box = new ThreeDimensionalPTItemDTO();
        ProductMinDetailDTO rotatedDto = new ProductMinDetailDTO();
        int caseToExecute;
        List<ThreeDimensionalPTItemDTO> result = new ArrayList<ThreeDimensionalPTItemDTO>();

        caseToExecute = 1;
        box.setLength(3.0);
        box.setBreadth(3.0);
        box.setHeight(3.0);

        rotatedDto.setProductDimensions(new Double[]{2.0, 2.0, 2.0});

        result = shipTogetherImpl.getBreakedBoxes(null, box, rotatedDto, caseToExecute);
        Assert.assertTrue(result.size() == 3);

        Assert.assertEquals(result.get(0).getLength(), 1.0);
        Assert.assertEquals(result.get(0).getBreadth(), 3.0);
        Assert.assertEquals(result.get(0).getHeight(), 2.0);

        Assert.assertEquals(result.get(1).getLength(), 2.0);
        Assert.assertEquals(result.get(1).getBreadth(), 1.0);
        Assert.assertEquals(result.get(1).getHeight(), 2.0);

        Assert.assertEquals(result.get(2).getLength(), 3.0);
        Assert.assertEquals(result.get(2).getBreadth(), 3.0);
        Assert.assertEquals(result.get(2).getHeight(), 1.0);
    }

    @Test
    public void getBreakedBoxesPosV2() {

        ThreeDimensionalPTItemDTO box = new ThreeDimensionalPTItemDTO();
        ProductMinDetailDTO rotatedDto = new ProductMinDetailDTO();
        int caseToExecute;
        List<ThreeDimensionalPTItemDTO> result = new ArrayList<ThreeDimensionalPTItemDTO>();

        caseToExecute = 2;
        box.setLength(3.0);
        box.setBreadth(3.0);
        box.setHeight(3.0);

        rotatedDto.setProductDimensions(new Double[]{2.0, 2.0, 2.0});

        result = shipTogetherImpl.getBreakedBoxes(null, box, rotatedDto, caseToExecute);
        Assert.assertTrue(result.size() == 3);

        Assert.assertEquals(result.get(0).getLength(), 1.0);
        Assert.assertEquals(result.get(0).getBreadth(), 2.0);
        Assert.assertEquals(result.get(0).getHeight(), 2.0);

        Assert.assertEquals(result.get(1).getLength(), 3.0);
        Assert.assertEquals(result.get(1).getBreadth(), 1.0);
        Assert.assertEquals(result.get(1).getHeight(), 2.0);

        Assert.assertEquals(result.get(2).getLength(), 3.0);
        Assert.assertEquals(result.get(2).getBreadth(), 3.0);
        Assert.assertEquals(result.get(2).getHeight(), 1.0);
    }

    @Test
    public void getBreakedBoxesPosV3() {

        ThreeDimensionalPTItemDTO box = new ThreeDimensionalPTItemDTO();
        ProductMinDetailDTO rotatedDto = new ProductMinDetailDTO();
        int caseToExecute;
        List<ThreeDimensionalPTItemDTO> result = new ArrayList<ThreeDimensionalPTItemDTO>();

        caseToExecute = 3;
        box.setLength(3.0);
        box.setBreadth(3.0);
        box.setHeight(3.0);

        rotatedDto.setProductDimensions(new Double[]{2.0, 2.0, 2.0});

        result = shipTogetherImpl.getBreakedBoxes(null, box, rotatedDto, caseToExecute);
        Assert.assertTrue(result.size() == 3);

        Assert.assertEquals(result.get(0).getLength(), 1.0);
        Assert.assertEquals(result.get(0).getBreadth(), 3.0);
        Assert.assertEquals(result.get(0).getHeight(), 3.0);

        Assert.assertEquals(result.get(1).getLength(), 2.0);
        Assert.assertEquals(result.get(1).getBreadth(), 1.0);
        Assert.assertEquals(result.get(1).getHeight(), 3.0);

        Assert.assertEquals(result.get(2).getLength(), 2.0);
        Assert.assertEquals(result.get(2).getBreadth(), 2.0);
        Assert.assertEquals(result.get(2).getHeight(), 1.0);
    }

    @Test
    public void getBreakedBoxesPosV4() {

        ThreeDimensionalPTItemDTO box = new ThreeDimensionalPTItemDTO();
        ProductMinDetailDTO rotatedDto = new ProductMinDetailDTO();
        int caseToExecute;
        List<ThreeDimensionalPTItemDTO> result = new ArrayList<ThreeDimensionalPTItemDTO>();

        caseToExecute = 4;
        box.setLength(3.0);
        box.setBreadth(3.0);
        box.setHeight(3.0);

        rotatedDto.setProductDimensions(new Double[]{2.0, 2.0, 2.0});

        result = shipTogetherImpl.getBreakedBoxes(null, box, rotatedDto, caseToExecute);
        Assert.assertTrue(result.size() == 3);

        Assert.assertEquals(result.get(0).getLength(), 1.0);
        Assert.assertEquals(result.get(0).getBreadth(), 2.0);
        Assert.assertEquals(result.get(0).getHeight(), 3.0);

        Assert.assertEquals(result.get(1).getLength(), 3.0);
        Assert.assertEquals(result.get(1).getBreadth(), 1.0);
        Assert.assertEquals(result.get(1).getHeight(), 3.0);

        Assert.assertEquals(result.get(2).getLength(), 2.0);
        Assert.assertEquals(result.get(2).getBreadth(), 2.0);
        Assert.assertEquals(result.get(2).getHeight(), 1.0);
    }

    @Test
    public void getBreakedBoxesPosV5() {

        ThreeDimensionalPTItemDTO box = new ThreeDimensionalPTItemDTO();
        ProductMinDetailDTO rotatedDto = new ProductMinDetailDTO();
        int caseToExecute;
        List<ThreeDimensionalPTItemDTO> result = new ArrayList<ThreeDimensionalPTItemDTO>();

        caseToExecute = 5;
        box.setLength(3.0);
        box.setBreadth(3.0);
        box.setHeight(3.0);

        rotatedDto.setProductDimensions(new Double[]{2.0, 2.0, 2.0});

        result = shipTogetherImpl.getBreakedBoxes(null, box, rotatedDto, caseToExecute);
        Assert.assertTrue(result.size() == 3);

        Assert.assertEquals(result.get(0).getLength(), 1.0);
        Assert.assertEquals(result.get(0).getBreadth(), 3.0);
        Assert.assertEquals(result.get(0).getHeight(), 3.0);

        Assert.assertEquals(result.get(1).getLength(), 2.0);
        Assert.assertEquals(result.get(1).getBreadth(), 1.0);
        Assert.assertEquals(result.get(1).getHeight(), 2.0);

        Assert.assertEquals(result.get(2).getLength(), 2.0);
        Assert.assertEquals(result.get(2).getBreadth(), 3.0);
        Assert.assertEquals(result.get(2).getHeight(), 1.0);
    }

    @Test
    public void getBreakedBoxesPosV6() {

        ThreeDimensionalPTItemDTO box = new ThreeDimensionalPTItemDTO();
        ProductMinDetailDTO rotatedDto = new ProductMinDetailDTO();
        int caseToExecute;
        List<ThreeDimensionalPTItemDTO> result = new ArrayList<ThreeDimensionalPTItemDTO>();

        caseToExecute = 6;
        box.setLength(3.0);
        box.setBreadth(3.0);
        box.setHeight(3.0);

        rotatedDto.setProductDimensions(new Double[]{2.0, 2.0, 2.0});

        result = shipTogetherImpl.getBreakedBoxes(null, box, rotatedDto, caseToExecute);
        Assert.assertTrue(result.size() == 3);

        Assert.assertEquals(result.get(0).getLength(), 1.0);
        Assert.assertEquals(result.get(0).getBreadth(), 2.0);
        Assert.assertEquals(result.get(0).getHeight(), 2.0);

        Assert.assertEquals(result.get(1).getLength(), 3.0);
        Assert.assertEquals(result.get(1).getBreadth(), 1.0);
        Assert.assertEquals(result.get(1).getHeight(), 3.0);

        Assert.assertEquals(result.get(2).getLength(), 3.0);
        Assert.assertEquals(result.get(2).getBreadth(), 2.0);
        Assert.assertEquals(result.get(2).getHeight(), 1.0);
    }

    // Trying to fit item [10X10X10] into box [10X10X10] 
    @Test
    public void shipTogetherForNormal() {

        List<ProductDetailDTO> productDetailList = new ArrayList<>();
        ProductDetailDTO pdDto;

        ShipTogetherSearchDTO dto = new ShipTogetherSearchDTO();
        dto.setStoreCode("SD");
        dto.setPackagingType("CARTON");

        pdDto = new ProductDetailDTO();
        pdDto.setLength(10.0);
        pdDto.setBreadth(10.0);
        pdDto.setHeight(10.0);
        pdDto.setMargin(0.0);
        pdDto.setVolWt(200.0);
        pdDto.setQuantity(1);
        productDetailList.add(pdDto);

        dto.setProductDetailList(productDetailList);

        initializePackagingTypeCache();

        com.snapdeal.packman.services.packaging.recomendation.impl.ShipTogetherServiceImpl.IDependency dep;

        dep = mock(com.snapdeal.packman.services.packaging.recomendation.impl.ShipTogetherServiceImpl.IDependency.class);
        when(dep.getIntegerScalar(Property.PACKMAN_BRUTE_FORCE_APPROACH_TIME_OUT)).thenReturn(Integer.parseInt("100"));
        ReflectionTestUtils.setField(shipTogetherImpl, "dependency", dep);

        PackagingTypeItemDTO result = shipTogetherImpl.shipTogether(dto);
        Assert.assertEquals(result.getBoxName(), "CB01");
        Assert.assertEquals(result.getPackagingMode(), PackagingMode.NORMAL.mode());

    }

    // Trying to fit item [10X10X5]=2 into box [10X10X10] 
    @Test
    public void shipTogetherForNormalV1() {

        List<ProductDetailDTO> productDetailList = new ArrayList<>();
        ProductDetailDTO pdDto;

        ShipTogetherSearchDTO dto = new ShipTogetherSearchDTO();
        dto.setStoreCode("SD");
        dto.setPackagingType("CARTON");

        pdDto = new ProductDetailDTO();
        pdDto.setLength(10.0);
        pdDto.setBreadth(10.0);
        pdDto.setHeight(5.0);
        pdDto.setMargin(0.0);
        pdDto.setVolWt(100.0);
        pdDto.setQuantity(2);
        productDetailList.add(pdDto);

        dto.setProductDetailList(productDetailList);

        initializePackagingTypeCache();

        com.snapdeal.packman.services.packaging.recomendation.impl.ShipTogetherServiceImpl.IDependency dep;

        dep = mock(com.snapdeal.packman.services.packaging.recomendation.impl.ShipTogetherServiceImpl.IDependency.class);
        when(dep.getIntegerScalar(Property.PACKMAN_BRUTE_FORCE_APPROACH_TIME_OUT)).thenReturn(Integer.parseInt("100"));
        ReflectionTestUtils.setField(shipTogetherImpl, "dependency", dep);

        PackagingTypeItemDTO result = shipTogetherImpl.shipTogether(dto);
        Assert.assertEquals(result.getBoxName(), "CB01");
        Assert.assertEquals(result.getPackagingMode(), PackagingMode.NORMAL.mode());

    }

    // Trying to fit item [1X1X1]=1000 into box [10X10X10] 
    @Test
    public void shipTogetherForNormalV2() {

        List<ProductDetailDTO> productDetailList = new ArrayList<>();
        ProductDetailDTO pdDto;

        ShipTogetherSearchDTO dto = new ShipTogetherSearchDTO();
        dto.setStoreCode("SD");
        dto.setPackagingType("CARTON");

        pdDto = new ProductDetailDTO();
        pdDto.setLength(1.0);
        pdDto.setBreadth(1.0);
        pdDto.setHeight(1.0);
        pdDto.setMargin(0.0);
        pdDto.setVolWt(0.2);
        pdDto.setQuantity(1000);
        productDetailList.add(pdDto);

        dto.setProductDetailList(productDetailList);

        initializePackagingTypeCache();

        com.snapdeal.packman.services.packaging.recomendation.impl.ShipTogetherServiceImpl.IDependency dep;

        dep = mock(com.snapdeal.packman.services.packaging.recomendation.impl.ShipTogetherServiceImpl.IDependency.class);
        when(dep.getIntegerScalar(Property.PACKMAN_BRUTE_FORCE_APPROACH_TIME_OUT)).thenReturn(Integer.parseInt("100"));
        ReflectionTestUtils.setField(shipTogetherImpl, "dependency", dep);

        PackagingTypeItemDTO result = shipTogetherImpl.shipTogether(dto);
        Assert.assertEquals(result.getBoxName(), "CB01");
        Assert.assertEquals(result.getPackagingMode(), PackagingMode.NORMAL.mode());

    }

    // Trying to fit item [1X1X1]=1000 into box [10X10X10] with timeout = 300 ms
    @Test
    public void shipTogetherForNormalV3() {

        List<ProductDetailDTO> productDetailList = new ArrayList<>();
        ProductDetailDTO pdDto;

        ShipTogetherSearchDTO dto = new ShipTogetherSearchDTO();
        dto.setStoreCode("SD");
        dto.setPackagingType("CARTON");

        pdDto = new ProductDetailDTO();
        pdDto.setLength(1.0);
        pdDto.setBreadth(1.0);
        pdDto.setHeight(1.0);
        pdDto.setMargin(0.0);
        pdDto.setVolWt(0.2);
        pdDto.setQuantity(1000);
        productDetailList.add(pdDto);

        dto.setProductDetailList(productDetailList);

        initializePackagingTypeCache();

        com.snapdeal.packman.services.packaging.recomendation.impl.ShipTogetherServiceImpl.IDependency dep;

        dep = mock(com.snapdeal.packman.services.packaging.recomendation.impl.ShipTogetherServiceImpl.IDependency.class);
        when(dep.getIntegerScalar(Property.PACKMAN_BRUTE_FORCE_APPROACH_TIME_OUT)).thenReturn(Integer.parseInt("300"));
        ReflectionTestUtils.setField(shipTogetherImpl, "dependency", dep);

        PackagingTypeItemDTO result = shipTogetherImpl.shipTogether(dto);
        Assert.assertEquals(result.getBoxName(), "CB01");
        Assert.assertEquals(result.getPackagingMode(), PackagingMode.NORMAL.mode());

    }

    // Trying to fit item [1X1X1]=1000 into box [10X10X10] with timeout = 300 ms
    @Test
    public void shipTogetherForNormalV4() {

        List<ProductDetailDTO> productDetailList = new ArrayList<>();
        ProductDetailDTO pdDto;

        pdDto = new ProductDetailDTO();
        pdDto.setLength(1.0);
        pdDto.setBreadth(1.0);
        pdDto.setHeight(1.0);
        pdDto.setMargin(0.0);
        pdDto.setVolWt(0.2);
        pdDto.setQuantity(1000);
        productDetailList.add(pdDto);

        ShipTogetherSearchDTO dto = new ShipTogetherSearchDTO();
        dto.setStoreCode("SD");
        dto.setPackagingType("CARTON");
        dto.setProductDetailList(productDetailList);

        initializePackagingTypeCache();

        com.snapdeal.packman.services.packaging.recomendation.impl.ShipTogetherServiceImpl.IDependency dep;

        dep = mock(com.snapdeal.packman.services.packaging.recomendation.impl.ShipTogetherServiceImpl.IDependency.class);
        when(dep.getIntegerScalar(Property.PACKMAN_BRUTE_FORCE_APPROACH_TIME_OUT)).thenReturn(Integer.parseInt("300"));
        ReflectionTestUtils.setField(shipTogetherImpl, "dependency", dep);

        PackagingTypeItemDTO result = shipTogetherImpl.shipTogether(dto);
        Assert.assertEquals(result.getBoxName(), "CB01");
        Assert.assertEquals(result.getPackagingMode(), PackagingMode.NORMAL.mode());

    }

    // Trying to fit item [5X5X5]=8 into box [10X10X10] with timeout = 50 ms
    @Test
    public void shipTogetherForNormalV5() {

        List<ProductDetailDTO> productDetailList = new ArrayList<>();
        ProductDetailDTO pdDto;

        pdDto = new ProductDetailDTO();
        pdDto.setLength(5.0);
        pdDto.setBreadth(5.0);
        pdDto.setHeight(5.0);
        pdDto.setMargin(0.0);
        pdDto.setVolWt(25.0);
        pdDto.setQuantity(8);
        productDetailList.add(pdDto);

        ShipTogetherSearchDTO dto = new ShipTogetherSearchDTO();
        dto.setStoreCode("SD");
        dto.setPackagingType("CARTON");
        dto.setProductDetailList(productDetailList);

        initializePackagingTypeCache();

        com.snapdeal.packman.services.packaging.recomendation.impl.ShipTogetherServiceImpl.IDependency dep;

        dep = mock(com.snapdeal.packman.services.packaging.recomendation.impl.ShipTogetherServiceImpl.IDependency.class);
        when(dep.getIntegerScalar(Property.PACKMAN_BRUTE_FORCE_APPROACH_TIME_OUT)).thenReturn(Integer.parseInt("50"));
        ReflectionTestUtils.setField(shipTogetherImpl, "dependency", dep);

        PackagingTypeItemDTO result = shipTogetherImpl.shipTogether(dto);
        Assert.assertEquals(result.getBoxName(), "CB01");
        Assert.assertEquals(result.getPackagingMode(), PackagingMode.NORMAL.mode());

    }

    // Trying to fit item [5X5X5]=7 and [1X1X1]=125 into box [10X10X10]
    @Test
    public void shipTogetherNegativeForNormalV5() {

        List<ProductDetailDTO> productDetailList = new ArrayList<>();
        ProductDetailDTO pdDto;

        pdDto = new ProductDetailDTO();
        pdDto.setLength(5.0);
        pdDto.setBreadth(5.0);
        pdDto.setHeight(5.0);
        pdDto.setMargin(0.0);
        pdDto.setVolWt(25.0);
        pdDto.setQuantity(7);
        productDetailList.add(pdDto);

        pdDto = new ProductDetailDTO();
        pdDto.setLength(1.0);
        pdDto.setBreadth(1.0);
        pdDto.setHeight(1.0);
        pdDto.setMargin(0.0);
        pdDto.setVolWt(0.2);
        pdDto.setQuantity(125);
        productDetailList.add(pdDto);

        ShipTogetherSearchDTO dto = new ShipTogetherSearchDTO();
        dto.setStoreCode("SD");
        dto.setPackagingType("CARTON");
        dto.setProductDetailList(productDetailList);

        initializePackagingTypeCache();

        com.snapdeal.packman.services.packaging.recomendation.impl.ShipTogetherServiceImpl.IDependency dep;

        dep = mock(com.snapdeal.packman.services.packaging.recomendation.impl.ShipTogetherServiceImpl.IDependency.class);
        when(dep.getIntegerScalar(Property.PACKMAN_BRUTE_FORCE_APPROACH_TIME_OUT)).thenReturn(Integer.parseInt("1000"));
        ReflectionTestUtils.setField(shipTogetherImpl, "dependency", dep);

        PackagingTypeItemDTO result = shipTogetherImpl.shipTogether(dto);
        Assert.assertEquals(result.getBoxName(), "CB01");
        Assert.assertEquals(result.getPackagingMode(), PackagingMode.NORMAL.mode());
    }

    // Trying to fit item [5X5X5]=7 and [1X1X1]=117 and [2X2X2]=1 into box [10X10X10] 
    @Test
    public void shipTogetherNegativeForNormalV6() {

        List<ProductDetailDTO> productDetailList = new ArrayList<>();
        ProductDetailDTO pdDto;

        pdDto = new ProductDetailDTO();
        pdDto.setLength(5.0);
        pdDto.setBreadth(5.0);
        pdDto.setHeight(5.0);
        pdDto.setMargin(0.0);
        pdDto.setVolWt(25.0);
        pdDto.setQuantity(7);
        productDetailList.add(pdDto);

        pdDto = new ProductDetailDTO();
        pdDto.setLength(1.0);
        pdDto.setBreadth(1.0);
        pdDto.setHeight(1.0);
        pdDto.setMargin(0.0);
        pdDto.setVolWt(0.2);
        pdDto.setQuantity(117);
        productDetailList.add(pdDto);

        pdDto = new ProductDetailDTO();
        pdDto.setLength(2.0);
        pdDto.setBreadth(2.0);
        pdDto.setHeight(2.0);
        pdDto.setMargin(0.0);
        pdDto.setVolWt(1.6);
        pdDto.setQuantity(1);
        productDetailList.add(pdDto);

        ShipTogetherSearchDTO dto = new ShipTogetherSearchDTO();
        dto.setStoreCode("SD");
        dto.setPackagingType("CARTON");
        dto.setProductDetailList(productDetailList);

        initializePackagingTypeCache();

        com.snapdeal.packman.services.packaging.recomendation.impl.ShipTogetherServiceImpl.IDependency dep;

        dep = mock(com.snapdeal.packman.services.packaging.recomendation.impl.ShipTogetherServiceImpl.IDependency.class);
        when(dep.getIntegerScalar(Property.PACKMAN_BRUTE_FORCE_APPROACH_TIME_OUT)).thenReturn(Integer.parseInt("1000"));
        ReflectionTestUtils.setField(shipTogetherImpl, "dependency", dep);

        PackagingTypeItemDTO result = shipTogetherImpl.shipTogether(dto);
        Assert.assertEquals(result.getBoxName(), "CB01");
        Assert.assertEquals(result.getPackagingMode(), PackagingMode.NORMAL.mode());
    }

    @Test
    public void isFittable3DV1() throws Exception {

        List<ProductMinDetailDTO> productMinDetailDtoList = new ArrayList<>();
        ArrayList<ThreeDimensionalPTItemDTO> boxList = new ArrayList<>();

        ProductMinDetailDTO productMinDetailDTO = new ProductMinDetailDTO();
        productMinDetailDTO.setProductDimensions(new Double[]{1.0, 1.0, 1.0});
        productMinDetailDtoList.add(productMinDetailDTO);

        Assert.assertEquals((boolean) isFittable3D.invoke(shipTogetherImpl, productMinDetailDtoList, boxList), false);
    }

    @Test
    public void isFittable3DV2() throws Exception {

        List<ProductMinDetailDTO> productMinDetailDtoList = new ArrayList<>();
        ArrayList<ThreeDimensionalPTItemDTO> boxList = new ArrayList<>();
        Assert.assertEquals((boolean) isFittable3D.invoke(shipTogetherImpl, productMinDetailDtoList, boxList), true);

    }

    // Try to exact fit one item into one box
    @Test
    public void isFittable3DV3() throws Exception {

        ProductMinDetailDTO product;
        ThreeDimensionalPTItemDTO box;
        List<ProductMinDetailDTO> productMinDetailDtoList = new ArrayList<>();
        ArrayList<ThreeDimensionalPTItemDTO> boxList = new ArrayList<>();

        product = new ProductMinDetailDTO(10.0, 10.0, 10.0);
        productMinDetailDtoList.add(product);

        box = new ThreeDimensionalPTItemDTO(10.0, 10.0, 10.0,"CB01");
        boxList.add(box);

        Assert.assertEquals((boolean) isFittable3D.invoke(shipTogetherImpl, productMinDetailDtoList, boxList), true);

    }

    @Test
    public void isFittable3DV4() throws Exception {

        ProductMinDetailDTO product;
        ThreeDimensionalPTItemDTO box;
        List<ProductMinDetailDTO> productMinDetailDtoList = new ArrayList<>();
        ArrayList<ThreeDimensionalPTItemDTO> boxList = new ArrayList<>();

        product = new ProductMinDetailDTO(10.0, 10.0, 10.0);
        productMinDetailDtoList.add(product);

        box = new ThreeDimensionalPTItemDTO(20.0, 20.0, 20.0,"CB01");
        boxList.add(box);

        Assert.assertEquals((boolean) isFittable3D.invoke(shipTogetherImpl, productMinDetailDtoList, boxList), true);

    }

    @Test
    public void isFittable3DV5() throws Exception {

        ProductMinDetailDTO product;
        ThreeDimensionalPTItemDTO box;
        List<ProductMinDetailDTO> productMinDetailDtoList = new ArrayList<>();
        ArrayList<ThreeDimensionalPTItemDTO> boxList = new ArrayList<>();

        product = new ProductMinDetailDTO(1.0, 1.0, 20.1);
        productMinDetailDtoList.add(product);

        box = new ThreeDimensionalPTItemDTO(20.0, 20.0, 20.0,"CB01");
        boxList.add(box);

        Assert.assertEquals((boolean) isFittable3D.invoke(shipTogetherImpl, productMinDetailDtoList, boxList), false);

    }

    // Try to fit two items into container
    @Test
    public void isFittable3DV6() throws Exception {

        ProductMinDetailDTO product;
        ThreeDimensionalPTItemDTO box;
        List<ProductMinDetailDTO> productMinDetailDtoList = new ArrayList<>();
        ArrayList<ThreeDimensionalPTItemDTO> boxList = new ArrayList<>();

        product = new ProductMinDetailDTO(10.0, 10.0, 1.0);
        productMinDetailDtoList.add(product);

        product = new ProductMinDetailDTO(10.0, 10.0, 9.0);
        productMinDetailDtoList.add(product);

        box = new ThreeDimensionalPTItemDTO(10.0, 10.0, 10.0,"CB01");
        boxList.add(box);

        Assert.assertEquals((boolean) isFittable3D.invoke(shipTogetherImpl, productMinDetailDtoList, boxList), true);

    }

    // Try to fit smaller product multiple times into container
    @Test
    public void isFittable3DV7() throws Exception {

        ProductMinDetailDTO product;
        ThreeDimensionalPTItemDTO box;
        List<ProductMinDetailDTO> productMinDetailDtoList = new ArrayList<>();
        ArrayList<ThreeDimensionalPTItemDTO> boxList = new ArrayList<>();

        for (int i = 0; i < 1000; i++) {
            product = new ProductMinDetailDTO(1.0, 1.0, 1.0);
            productMinDetailDtoList.add(product);
        }

        box = new ThreeDimensionalPTItemDTO(10.0, 10.0, 10.0,"CB01");
        boxList.add(box);

        Assert.assertEquals((boolean) isFittable3D.invoke(shipTogetherImpl, productMinDetailDtoList, boxList), true);

    }

    // Try to fit smaller product multiple times into container
    @Test
    public void isFittable3DV8() throws Exception {

        List<ProductDetailDTO> productDetailList = new ArrayList<>();
        ProductDetailDTO pdDto;

        ShipTogetherSearchDTO dto = new ShipTogetherSearchDTO();
        dto.setStoreCode("SD");
        dto.setPackagingType("CARTON");

        pdDto = new ProductDetailDTO();
        pdDto.setLength(1.0);
        pdDto.setBreadth(1.0);
        pdDto.setHeight(1.0);
        pdDto.setMargin(0.0);
        pdDto.setVolWt(0.2);
        pdDto.setQuantity(2000);
        productDetailList.add(pdDto);

        dto.setProductDetailList(productDetailList);

        initializePackagingTypeCache();

        com.snapdeal.packman.services.packaging.recomendation.impl.ShipTogetherServiceImpl.IDependency dep;

        dep = mock(com.snapdeal.packman.services.packaging.recomendation.impl.ShipTogetherServiceImpl.IDependency.class);
        when(dep.getIntegerScalar(Property.PACKMAN_BRUTE_FORCE_APPROACH_TIME_OUT)).thenReturn(Integer.parseInt("10000"));
        ReflectionTestUtils.setField(shipTogetherImpl, "dependency", dep);

        PackagingTypeItemDTO result = shipTogetherImpl.shipTogether(dto);
        Assert.assertEquals(result.getBoxName(), "CB02");
        Assert.assertEquals(result.getPackagingMode(), PackagingMode.NORMAL.mode());

    }

    // Try to fit smaller product multiple times into container
    @Test
    public void isFittable3DV9() throws Exception {

        List<ProductDetailDTO> productDetailList = new ArrayList<>();
        ProductDetailDTO pdDto;

        ShipTogetherSearchDTO dto = new ShipTogetherSearchDTO();
        dto.setStoreCode("SD");
        dto.setPackagingType("CARTON");

        pdDto = new ProductDetailDTO();
        pdDto.setLength(1.0);
        pdDto.setBreadth(1.0);
        pdDto.setHeight(1.0);
        pdDto.setMargin(0.0);
        pdDto.setVolWt(0.2);
        pdDto.setQuantity(1001);
        productDetailList.add(pdDto);

        dto.setProductDetailList(productDetailList);

        initializePackagingTypeCache();

        com.snapdeal.packman.services.packaging.recomendation.impl.ShipTogetherServiceImpl.IDependency dep;

        dep = mock(com.snapdeal.packman.services.packaging.recomendation.impl.ShipTogetherServiceImpl.IDependency.class);
        when(dep.getIntegerScalar(Property.PACKMAN_BRUTE_FORCE_APPROACH_TIME_OUT)).thenReturn(Integer.parseInt("50000"));
        ReflectionTestUtils.setField(shipTogetherImpl, "dependency", dep);

        PackagingTypeItemDTO result = shipTogetherImpl.shipTogether(dto);
        Assert.assertEquals(result.getBoxName(), "CB02");
        Assert.assertEquals(result.getPackagingMode(), PackagingMode.NORMAL.mode());

    }

    // Try to fit smaller product multiple times into container
    @Test
    public void isFittable3DV10() throws Exception {

        List<ProductDetailDTO> productDetailList = new ArrayList<>();
        ProductDetailDTO pdDto;

        ShipTogetherSearchDTO dto = new ShipTogetherSearchDTO();
        dto.setStoreCode("SD");
        dto.setPackagingType("CARTON");

        pdDto = new ProductDetailDTO();
        pdDto.setLength(1.0);
        pdDto.setBreadth(1.0);
        pdDto.setHeight(1.0);
        pdDto.setMargin(0.0);
        pdDto.setVolWt(0.2);
        pdDto.setQuantity(500);
        productDetailList.add(pdDto);

        pdDto = new ProductDetailDTO();
        pdDto.setLength(2.0);
        pdDto.setBreadth(2.0);
        pdDto.setHeight(2.0);
        pdDto.setMargin(0.0);
        pdDto.setVolWt(1.6);
        pdDto.setQuantity(50);
        productDetailList.add(pdDto);

        pdDto = new ProductDetailDTO();
        pdDto.setLength(10.0);
        pdDto.setBreadth(10.0);
        pdDto.setHeight(1.0);
        pdDto.setMargin(0.0);
        pdDto.setVolWt(20.0);
        pdDto.setQuantity(1);
        productDetailList.add(pdDto);

        dto.setProductDetailList(productDetailList);

        initializePackagingTypeCache();

        com.snapdeal.packman.services.packaging.recomendation.impl.ShipTogetherServiceImpl.IDependency dep;

        dep = mock(com.snapdeal.packman.services.packaging.recomendation.impl.ShipTogetherServiceImpl.IDependency.class);
        when(dep.getIntegerScalar(Property.PACKMAN_BRUTE_FORCE_APPROACH_TIME_OUT)).thenReturn(Integer.parseInt("50000"));
        ReflectionTestUtils.setField(shipTogetherImpl, "dependency", dep);

        PackagingTypeItemDTO result = shipTogetherImpl.shipTogether(dto);
        Assert.assertEquals(result.getBoxName(), "CB01");
        Assert.assertEquals(result.getPackagingMode(), PackagingMode.NORMAL.mode());

    }

    // Try to fit smaller product multiple times into container
    @Test
    public void isFittable3DV11() throws Exception {

        List<ProductDetailDTO> productDetailList = new ArrayList<>();
        ProductDetailDTO pdDto;

        ShipTogetherSearchDTO dto = new ShipTogetherSearchDTO();
        dto.setStoreCode("SD");
        dto.setPackagingType("CARTON");

        pdDto = new ProductDetailDTO();
        pdDto.setLength(10.0);
        pdDto.setBreadth(10.0);
        pdDto.setHeight(10.0);
        pdDto.setMargin(0.0);
        pdDto.setVolWt(200.0);
        pdDto.setQuantity(1);
        productDetailList.add(pdDto);

        dto.setProductDetailList(productDetailList);

        initializePackagingTypeCache();

        com.snapdeal.packman.services.packaging.recomendation.impl.ShipTogetherServiceImpl.IDependency dep;

        dep = mock(com.snapdeal.packman.services.packaging.recomendation.impl.ShipTogetherServiceImpl.IDependency.class);

        when(dep.getIntegerScalar(Property.PACKMAN_BRUTE_FORCE_APPROACH_TIME_OUT)).thenReturn(Integer.parseInt("50000"));
        ReflectionTestUtils.setField(shipTogetherImpl, "dependency", dep);

        PackagingTypeItemDTO result = shipTogetherImpl.shipTogether(dto);
        Assert.assertEquals(result.getBoxName(), "CB01");
        Assert.assertEquals(result.getPackagingMode(), PackagingMode.NORMAL.mode());

    }

    // Try to fit smaller product multiple times into container

    @Test
    public void isFittable3DV12() throws Exception {

        List<ProductDetailDTO> productDetailList = new ArrayList<>();
        ProductDetailDTO pdDto;

        ShipTogetherSearchDTO dto = new ShipTogetherSearchDTO();
        dto.setStoreCode("SD");
        dto.setPackagingType("CARTON");

        pdDto = new ProductDetailDTO();
        pdDto.setLength(1.0);
        pdDto.setBreadth(1.0);
        pdDto.setHeight(1.0);
        pdDto.setMargin(0.0);
        pdDto.setVolWt(0.2);
        pdDto.setQuantity(8000);
        productDetailList.add(pdDto);

        dto.setProductDetailList(productDetailList);

        initializePackagingTypeCache();

        com.snapdeal.packman.services.packaging.recomendation.impl.ShipTogetherServiceImpl.IDependency dep;

        dep = mock(com.snapdeal.packman.services.packaging.recomendation.impl.ShipTogetherServiceImpl.IDependency.class);
        when(dep.getIntegerScalar(Property.PACKMAN_BRUTE_FORCE_APPROACH_TIME_OUT)).thenReturn(Integer.parseInt("50000"));
        ReflectionTestUtils.setField(shipTogetherImpl, "dependency", dep);

        PackagingTypeItemDTO result = shipTogetherImpl.shipTogether(dto);
        Assert.assertEquals(result.getBoxName(), "CB02");
        Assert.assertEquals(result.getPackagingMode(), PackagingMode.NORMAL.mode());

    }

    // Checking if stack overflow occurs or not

    @Test
    public void isFittable3DV13() throws Exception {

        List<ProductDetailDTO> productDetailList = new ArrayList<>();
        ProductDetailDTO pdDto;

        ShipTogetherSearchDTO dto = new ShipTogetherSearchDTO();
        dto.setStoreCode("SD");
        dto.setPackagingType("CARTON");

        pdDto = new ProductDetailDTO();
        pdDto.setLength(1.0);
        pdDto.setBreadth(1.0);
        pdDto.setHeight(1.0);
        pdDto.setMargin(0.0);
        pdDto.setVolWt(0.2);
        pdDto.setQuantity(7000);
        productDetailList.add(pdDto);

        dto.setProductDetailList(productDetailList);

        initializePackagingTypeCache();

        com.snapdeal.packman.services.packaging.recomendation.impl.ShipTogetherServiceImpl.IDependency dep;

        dep = mock(com.snapdeal.packman.services.packaging.recomendation.impl.ShipTogetherServiceImpl.IDependency.class);
        when(dep.getIntegerScalar(Property.PACKMAN_BRUTE_FORCE_APPROACH_TIME_OUT)).thenReturn(Integer.parseInt("50000"));
        ReflectionTestUtils.setField(shipTogetherImpl, "dependency", dep);

        PackagingTypeItemDTO result = shipTogetherImpl.shipTogether(dto);
        Assert.assertEquals(result.getBoxName(), "CB03");
        Assert.assertEquals(result.getPackagingMode(), PackagingMode.NORMAL.mode());

    }

    @Test
    public void findAppropriateBoxV1() throws Exception {

        ProductMinDetailDTO productMinDetailDTO = new ProductMinDetailDTO();
        ArrayList<ThreeDimensionalPTItemDTO> boxList = new ArrayList<>();
        boolean sortRequired = false;

        productMinDetailDTO.setProductDimensions(new Double[]{1.0, 1.0, 1.0});
        Assert.assertNull(findAppropriateBox.invoke(shipTogetherImpl, productMinDetailDTO, boxList, sortRequired));

    }

    @Test
    public void findAppropriateBoxV2() throws Exception {

        ThreeDimensionalPTItemDTO box;
        ProductMinDetailDTO productMinDetailDTO = new ProductMinDetailDTO();
        ArrayList<ThreeDimensionalPTItemDTO> boxList = new ArrayList<>();
        boolean sortRequired = false;

        box = new ThreeDimensionalPTItemDTO(1.0, 2.0, 3.0,"CB01");
        box.setBoxName("CB01");
        boxList.add(box);

        productMinDetailDTO.setProductDimensions(new Double[]{1.0, 2.0, 3.0});
        ThreeDimensionalPTItemDTO result = (ThreeDimensionalPTItemDTO) findAppropriateBox.invoke(shipTogetherImpl, productMinDetailDTO, boxList, sortRequired);
        Assert.assertEquals(result.getBoxName(), "CB01");

    }

    @Test
    public void findAppropriateBoxV3() throws Exception {

        ThreeDimensionalPTItemDTO box;
        ProductMinDetailDTO productMinDetailDTO = new ProductMinDetailDTO();
        ArrayList<ThreeDimensionalPTItemDTO> boxList = new ArrayList<>();
        boolean sortRequired = false;

        box = new ThreeDimensionalPTItemDTO(1.0, 2.0, 3.0,"CB01");
        box.setBoxName("CB01");
        boxList.add(box);

        productMinDetailDTO.setProductDimensions(new Double[]{1.0, 2.0, 4.0});
        ThreeDimensionalPTItemDTO result = (ThreeDimensionalPTItemDTO) findAppropriateBox.invoke(shipTogetherImpl, productMinDetailDTO, boxList, sortRequired);
        Assert.assertNull(result);

    }

    @Test
    public void findAppropriateBoxV4() throws Exception {

        ThreeDimensionalPTItemDTO box;
        ProductMinDetailDTO productMinDetailDTO = new ProductMinDetailDTO();
        ArrayList<ThreeDimensionalPTItemDTO> boxList = new ArrayList<>();
        boolean sortRequired = false;

        box = new ThreeDimensionalPTItemDTO(1.0, 2.0, 3.0,"CB01");
        box.setBoxName("CB01");
        boxList.add(box);

        box = new ThreeDimensionalPTItemDTO(2.0, 2.0, 3.0,"CB01");
        box.setBoxName("CB02");
        boxList.add(box);

        box = new ThreeDimensionalPTItemDTO(2.0, 2.0, 4.0,"CB01");
        box.setBoxName("CB03");
        boxList.add(box);

        productMinDetailDTO.setProductDimensions(new Double[]{1.0, 2.0, 4.0});
        ThreeDimensionalPTItemDTO result = (ThreeDimensionalPTItemDTO) findAppropriateBox.invoke(shipTogetherImpl, productMinDetailDTO, boxList, sortRequired);
        Assert.assertEquals(result.getBoxName(), "CB03");

    }

}
