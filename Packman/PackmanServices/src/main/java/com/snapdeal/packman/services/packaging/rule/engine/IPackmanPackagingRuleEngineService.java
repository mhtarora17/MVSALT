/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.services.packaging.rule.engine;

import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.packman.services.exception.PackagingTypeNotFoundException;
import com.snapdeal.packman.services.packaging.dto.SUPCDetailDTO;

/**
 * This class will calculate the packaging type bases on catageoryURL and Store.
 *  
 *  @version     1.0, 23-Dec-2015
 *  @author indrajit
 */
public interface IPackmanPackagingRuleEngineService {
    
    /**
     * @param storeCode
     * @param catagoryUrl
     * @return
     * @throws PackagingTypeNotFoundException
     */
    String calculatePackagingType(SUPCDetailDTO dto)throws PackagingTypeNotFoundException, ExternalDataNotFoundException;

}
