/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.packman.services.packaging;

import com.snapdeal.packman.common.sro.AddressSRO;
import com.snapdeal.packman.common.sro.SurfacePackagingSRO;
import com.snapdeal.packman.dto.AirPackagingRecommendationDTO;
import com.snapdeal.packman.external.service.exception.ExternalDataNotFoundException;

/**
 * @version 1.0, 07-Sep-2015
 * @author ankur
 */
public interface IPackagingRecommendation {

    SurfacePackagingSRO getRecommendationsForSurfaceShipMode(String supc, String courierCode, AddressSRO toAddressSRO, AddressSRO fromAddressSRO)
            throws ExternalDataNotFoundException;

    AirPackagingRecommendationDTO getRecommendationsForAirShipMode(String supc) throws ExternalDataNotFoundException;

}
