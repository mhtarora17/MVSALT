/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.services.external;


import com.snapdeal.cocofs.model.request.GetProductFulfilmentAttributeRequest;
import com.snapdeal.cocofs.sro.ProductFulfilmentAttributeSRO;

/**
 *  
 *  @version     1.0, 07-Sep-2015
 *  @author ankur
 */
public interface ICocofsExternalService {

    double getVolumetricWeightFromLBH(double l, double b, double h);

    ProductFulfilmentAttributeSRO getProductFulfilmentAttributes(GetProductFulfilmentAttributeRequest request);

}
