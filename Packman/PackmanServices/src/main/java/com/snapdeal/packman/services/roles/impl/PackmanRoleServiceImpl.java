/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.services.roles.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.base.utils.CollectionUtils;
import com.snapdeal.cocofs.db.dao.IPackmanRoleDao;
import com.snapdeal.cocofs.entity.Role;
import com.snapdeal.packman.dto.RoleDto;
import com.snapdeal.packman.services.roles.IPackmanRoleService;

/**
 *  
 *  @version     1.0, 09-Dec-2015
 *  @author indrajit
 */
@Service("packmanRoleService")
public class PackmanRoleServiceImpl implements IPackmanRoleService{
    
    @Autowired
    IPackmanRoleDao packmanRoleDao;

    @Override
    public List<String> persisteRole(List<RoleDto> roleDtoList) {
	// TODO Auto-generated method stub
	if (null == roleDtoList || CollectionUtils.isEmpty(roleDtoList))
	    return null;
	List<String> persistedRoleList = new ArrayList<String>();
	for (RoleDto roleDto : roleDtoList) {
	    if (null == roleDto || null == roleDto.getCode())
		continue;
	    Role roleEntity = createRoleEntity(roleDto);
	    roleEntity = packmanRoleDao.persistRole(roleEntity);
	    if (roleEntity.getId() != 0)
		persistedRoleList.add(roleEntity.getCode());
	}
	return persistedRoleList;
    }

   private Role createRoleEntity(RoleDto dto){
       Role role = new Role();
       role.setCode(dto.getCode());
       role.setDefaultUrl(dto.getDefaultUrl());
       role.setDescription(dto.getDescription());
       role.setEnabled(dto.isEnabled());
       return role;
   }

}
