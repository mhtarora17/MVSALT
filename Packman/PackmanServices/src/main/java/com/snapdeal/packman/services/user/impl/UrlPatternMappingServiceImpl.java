/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.services.user.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.packman.dao.IUrlPatternDao;
import com.snapdeal.packman.dto.URLPatternDto;
import com.snapdeal.packman.entity.UrlPattern;
import com.snapdeal.packman.entity.UrlRoleMapping;
import com.snapdeal.packman.enums.SystemProperty;
import com.snapdeal.packman.services.user.IUrlPatternMappingService;

/**
 *  
 *  @version     1.0, 08-Dec-2015
 *  @author indrajit
 */
@Service("urlPatternMappingService")
public class UrlPatternMappingServiceImpl implements IUrlPatternMappingService{
    
    @Autowired
    IUrlPatternDao urlPatternDao;

    @Override
    public boolean saveUrlPatternMappingWithDto(URLPatternDto dto) {
	if(null != dto && null != dto.getUrl() && null != dto.getRoleCodeList()){
	    UrlPattern urlEntity = createUrlPatternEntity(dto);
	    urlEntity = saveUrlPattern(urlEntity);
	    if(null == urlEntity.getId())
		return false;
	    UrlRoleMapping urlRoleMappingEntity = null;
	    for(String role  : dto.getRoleCodeList()){
		if(null == role)
		    continue;
		urlRoleMappingEntity = createUrlRoleMappingEntity(urlEntity.getId(), role, true);
		saveURLRoleMapping(urlRoleMappingEntity);
	    }
	    return true;
	}
	// TODO Auto-generated method stub
	return false;
    }

    @Override
    public UrlPattern saveUrlPattern(UrlPattern urlPattern) {
	// TODO Auto-generated method stub
	return urlPatternDao.persistURLPattern(urlPattern);
    }

    @Override
    public UrlRoleMapping saveURLRoleMapping(UrlRoleMapping urlRoleMapping) {
	// TODO Auto-generated method stub
	return urlPatternDao.persistURLRoleMapping(urlRoleMapping);
    }
    
    private UrlPattern createUrlPatternEntity(URLPatternDto dto){
	UrlPattern url = new UrlPattern();
	url.setUrl(dto.getUrl());
	url.setEnanble(dto.isUrl_enable());
	url.setUpdatedBy(System.getProperty(SystemProperty.HOST_NAME.getCode()));
	return url;
    }
    
    private UrlRoleMapping createUrlRoleMappingEntity(Integer url_id, String roleCode, boolean enabled){
	UrlRoleMapping urlroleMapping = new UrlRoleMapping();
	urlroleMapping.setUrl_id(url_id);
	urlroleMapping.setRole_code(roleCode);
	urlroleMapping.setEnabled(enabled);
	urlroleMapping.setUpdatedBy(System.getProperty(SystemProperty.HOST_NAME.getCode()));
	return urlroleMapping;
	
    }

    @Override
    public Map<String, List<String>> getAllUrlPattern() {
	// TODO Auto-generated method stub
	Map<String, List<String>> finalResultMap = new HashMap<String, List<String>>();
	
	List<UrlPattern> urlPatternList = urlPatternDao.getAllUrlPattern();
	for(UrlPattern u: urlPatternList){
	    List<String> roleList = new ArrayList<String>();
	    Set<UrlRoleMapping> urlRole = u.getUrlRoalMappingSet();
	    for(UrlRoleMapping ur : urlRole)
		roleList.add(ur.getRole_code());
	    finalResultMap.put(u.getUrl(), roleList);
	}
	
	return finalResultMap;
    }

   

}
