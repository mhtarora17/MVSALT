/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.services.roles;


/**This class will reload the caches internally. 
 * It loads roles and urlRoleMap cache
 *  
 *  @version     1.0, 22-Dec-2015
 *  @author indrajit
 */
public interface IPackmanInternalCacheReloadService {
    
    public void reloadPackmanCaches();

}
