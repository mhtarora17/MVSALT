/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.services.packaging.dto;

import java.util.ArrayList;
import java.util.List;

/**
 *  
 *  @version     1.0, 25-Dec-2015
 *  @author indrajit
 */
public class CatageoryEvaluationParamDto {
    
    private  String brand;
 // Please keep it list as Rule Engine require non null object in hasRulePassed Method
    private  List<String> label = new ArrayList<>();

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public List<String> getLabel() {
        return label;
    }

    public void setLabel(List<String> label) {
        this.label = label;
    }

    @Override
    public String toString() {
	return "CatageoryEvaluationParamDto [brand=" + brand + ", label="
		+ label + "]";
    }

    
   
    
    

}
