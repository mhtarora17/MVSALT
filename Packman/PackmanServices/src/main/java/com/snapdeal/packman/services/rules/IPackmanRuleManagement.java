/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.services.rules;

import com.snapdeal.packman.rule.dto.CategoryRuleCreateForm;
import com.snapdeal.packman.rule.dto.StoreRuleCreateForm;
import com.snapdeal.packman.rule.dto.StoreRuleCreationDTO;
import com.snapdeal.packman.rule.dto.SubCategoryRuleCreateForm;
import com.snapdeal.rule.engine.entity.Rule;

/**
 *  
 *  @version     1.0, 22-Dec-2015
 *  @author brijesh
 */
public interface IPackmanRuleManagement {

    Object getRuleCreationDTO(String ruleType,String storeCode,String catOrSubcat,String packagingType,String string, String[] supercats,String user,String startDate,String endDate,boolean enabled);

    Rule createRule(StoreRuleCreationDTO creationDTO);

    void updateRule(StoreRuleCreationDTO dto) throws Exception;
    
    StoreRuleCreateForm getStoreCreateRuleDTO(Rule r, StoreRuleCreateForm dto);

    CategoryRuleCreateForm getCategoryCreateRuleDTO(Rule r, CategoryRuleCreateForm dto);

    SubCategoryRuleCreateForm getSubCategoryCreateRuleDTO(Rule r, SubCategoryRuleCreateForm dto);

    void enrichDto(StoreRuleCreateForm dto);
    
}
