/**
 *  Copyright 2016 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.packman.services.packaging.recomendation.impl;

import com.snapdeal.base.cache.CacheManager;
import com.snapdeal.base.utils.CollectionUtils;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.packman.cache.PackagingTypeCache;
import com.snapdeal.packman.dto.PackagingTypeCacheDTO;
import com.snapdeal.packman.dto.PackagingTypeItemDTO;
import com.snapdeal.packman.dto.ThreeDimensionalPTItemDTO;
import com.snapdeal.packman.dto.TwoDimensionalPTItemDTO;
import com.snapdeal.packman.services.packaging.dto.ProductDetailDTO;
import com.snapdeal.packman.services.packaging.dto.ShipTogetherSearchDTO;
import com.snapdeal.packman.services.packaging.recomendation.IShipTogetherService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.*;

/**
 * @version 1.0, 26-Feb-2016
 * @author brijesh
 */
@Service("shipTogetherService")
public class ShipTogetherServiceImpl implements IShipTogetherService {

    private static final Logger LOG        = LoggerFactory.getLogger(ShipTogetherServiceImpl.class);

    private IDependency         dependency = new Dependency();

    @Override
    public PackagingTypeItemDTO shipTogether(ShipTogetherSearchDTO dto) {

        PackagingTypeCacheDTO packagingTypeCacheDto = CacheManager.getInstance().getCache(PackagingTypeCache.class).getPackagingTypeDtoV1(dto.getStoreCode(),
                dto.getPackagingType());

        List<PackagingTypeItemDTO> packagingTypeItems = packagingTypeCacheDto.getPackagingTypeItems();

        if (!CollectionUtils.isEmpty(packagingTypeItems)) {

            boolean startThread = useThreadApproach(dto);

            // Right now ship together support is only for 3d packaging types
            if (packagingTypeItems.get(0) instanceof ThreeDimensionalPTItemDTO) {

                double totalVolWtOfProduct = getTotalVolWtOfProducts(dto.getProductDetailList());

                List<ThreeDimensionalPTItemDTO> threeDBoxes = new ArrayList<ThreeDimensionalPTItemDTO>();
                for (PackagingTypeItemDTO box : packagingTypeItems) {
                    ThreeDimensionalPTItemDTO threeDimensionalBox = new ThreeDimensionalPTItemDTO(box);
                    threeDBoxes.add(threeDimensionalBox);
                }


                for (ThreeDimensionalPTItemDTO box : threeDBoxes) {
                    List<ProductMinDetailDTO> minProductDtoList = getMinProductDtoList(dto);
                    final List<ProductMinDetailDTO> productMinDetailDtoList = new ArrayList<>(minProductDtoList);
                    Collections.sort(productMinDetailDtoList,Collections.reverseOrder());
//                    LOG.info("Trying to fit [{}] items {} in box [{}]", productMinDetailDtoList.size(),productMinDetailDtoList,box.getBoxName());
                    if (totalVolWtOfProduct <= box.getVolWeight()) {

                        final List<ThreeDimensionalPTItemDTO> boxList = new ArrayList<ThreeDimensionalPTItemDTO>(threeDBoxes);
                        boxList.add(box);

                        if(startThread) {
                            ExecutorService executor = Executors.newCachedThreadPool();
                            Callable<Boolean> task = new Callable<Boolean>() {
                                public Boolean call() {
                                    return isFittable3D(productMinDetailDtoList, boxList);
                                }
                            };
                            Future<Boolean> future = executor.submit(task);
                            try {
                                Boolean result = future.get(dependency.getIntegerScalar(Property.PACKMAN_BRUTE_FORCE_APPROACH_TIME_OUT), TimeUnit.MILLISECONDS);
                                if (result) {
                                    return box;
                                }
                            } catch (Exception e) {
                                LOG.info("### Exception : {}", e);
                                // handle other exceptions
                            } finally {
                                future.cancel(true); // may or may not desire this
                            }
                        } else {
                            if(isFittable3D(productMinDetailDtoList,boxList)){
                                return box;
                            }
                        }
                    }
                }

            } else if (packagingTypeItems.get(0) instanceof TwoDimensionalPTItemDTO) {

                List<ProductMinDetailDTO> productMinDetailDtoList = getMinProductDtoList(dto);

                List<TwoDimensionalPTItemDTO> twoDBoxes = new ArrayList<TwoDimensionalPTItemDTO>();
                for (PackagingTypeItemDTO box : packagingTypeItems) {
                    TwoDimensionalPTItemDTO twoDimensionalBox = new TwoDimensionalPTItemDTO(box);
                    twoDBoxes.add(twoDimensionalBox);
                }

                Collections.sort(twoDBoxes);

                for (TwoDimensionalPTItemDTO box : twoDBoxes) {
                    LOG.info("Checking for Box [{}] .. ", box.getBoxName());
                    ArrayList<TwoDimensionalPTItemDTO> boxList = new ArrayList<TwoDimensionalPTItemDTO>();
                    boxList.add(box);
                    if (isFittable2D(productMinDetailDtoList, boxList)) {
                        LOG.info("Ship together is applicable. Recommended box [{}]. Items [{}]", box.getBoxName(), dto.getProductDetailList());
                        return box;
                    }
                }

            }

        }

        return null;

    }

    /**
     *
     * This methods decide whether to call isFittable3D with timeout or not..
     * Return false if total quantity of items > 0
     * else returns true
     *
     * @param dto
     * @return
     */
    private boolean useThreadApproach(ShipTogetherSearchDTO dto) {
        int totalProducts = 0;
        for(ProductDetailDTO pdDto : dto.getProductDetailList()){
            totalProducts += pdDto.getQuantity();
        }
        return totalProducts>1;
    }

    private boolean isFittable2D(List<ProductMinDetailDTO> productMinDetailDtoList, ArrayList<TwoDimensionalPTItemDTO> boxList) {
        return false;
    }

    private boolean isFittable3D(List<ProductMinDetailDTO> productMinDetailDtoList, List<ThreeDimensionalPTItemDTO> boxList) {
        if (CollectionUtils.isEmpty(productMinDetailDtoList)) {
            return true;
        }
        //TODO See posibility of returning accumulated result
        if (CollectionUtils.isEmpty(boxList)) {
            return false;
        }

        // Selecting box with maximum volumetric weight first
        ProductMinDetailDTO productMinDetailDTO = productMinDetailDtoList.get(0);

        // Sorted box so we can try to fit item into box such that unused space is minimum
        Collections.sort(boxList);
        ThreeDimensionalPTItemDTO box = findAppropriateBox(productMinDetailDTO, boxList, true);

        if (box == null) {
            return false;
        } else {
            // Total six rotations are possible
            for (int i = 1; i <= 6; i++) {

                // box!= null . It means there is some way by which we can fit item into box. Maybe using rotation
                ProductMinDetailDTO rotatedDto = getRotatedDto(productMinDetailDTO, i);

                // We can break box by eight types..
                for (int j = 1; j <= 6; j++) {

                    List<ThreeDimensionalPTItemDTO> breakedBoxes = null;

                    // get breaked boxes from rotated dto
                    breakedBoxes = getBreakedBoxes(boxList, box, rotatedDto, j);

                    int pos = findAppropriateBoxPosition(productMinDetailDTO, boxList, false);
                    boxList.remove(box);
                    productMinDetailDtoList.remove(0);
                    boxList.addAll(breakedBoxes);
                    if (isFittable3D(productMinDetailDtoList, boxList)) {
                        return true;
                    }
                    boxList.removeAll(breakedBoxes);
                    productMinDetailDtoList.add(0, productMinDetailDTO);

                }

            }
        }
        return false;
    }

    public List<ThreeDimensionalPTItemDTO> getBreakedBoxes(List<ThreeDimensionalPTItemDTO> boxList, ThreeDimensionalPTItemDTO box, ProductMinDetailDTO rotatedDto, int j) {

        List<ThreeDimensionalPTItemDTO> breakedBoxes = null;

        // We will pass rotated dto in this function so boxes will be breaked accordingly..
        switch (j) {
            case 1:
                breakedBoxes = breakBoxIntoPartsHorizontallyV1(boxList, box, rotatedDto);
                break;
            case 2:
                breakedBoxes = breakBoxIntoPartsHorizontallyV2(boxList, box, rotatedDto);
                break;
            case 3:
                breakedBoxes = breakBoxIntoPartsVerticallyV1(boxList, box, rotatedDto);
                break;
            case 4:
                breakedBoxes = breakBoxIntoPartsVerticallyV2(boxList, box, rotatedDto);
                break;
            case 5:
                breakedBoxes = breakBoxIntoPartsHorizontallyAndVerticallyV1(boxList, box, rotatedDto);
                break;
            case 6:
                breakedBoxes = breakBoxIntoPartsHorizontallyAndVerticallyV2(boxList, box, rotatedDto);
                break;
        }
        return breakedBoxes;
    }

    public boolean canBoxFitTheItem(ThreeDimensionalPTItemDTO box, Double[] productDimensions) {
        return box.getLength() >= productDimensions[0] && box.getBreadth() >= productDimensions[1] && box.getHeight() >= productDimensions[2];
    }

    private List<ThreeDimensionalPTItemDTO> breakBoxIntoPartsHorizontallyAndVerticallyV2(List<ThreeDimensionalPTItemDTO> boxList, ThreeDimensionalPTItemDTO box,
            ProductMinDetailDTO productMinDetailDTO) {
        List<ThreeDimensionalPTItemDTO> result = new ArrayList<ThreeDimensionalPTItemDTO>();

        Double[] productDimensions = productMinDetailDTO.getProductDimensions();
        if (canBoxFitTheItem(box, productDimensions)) {

            ThreeDimensionalPTItemDTO dto = null;

            dto = new ThreeDimensionalPTItemDTO(box.getLength() - productDimensions[0], productDimensions[1], productDimensions[2], box.getBoxName() +"_1");
            validateAndAddIntoLIst(result, dto);

            dto = new ThreeDimensionalPTItemDTO(box.getLength(), box.getBreadth() - productDimensions[1], box.getHeight(), box.getBoxName() +"_2");
            validateAndAddIntoLIst(result, dto);

            dto = new ThreeDimensionalPTItemDTO(box.getLength(), productDimensions[1], box.getHeight() - productDimensions[2], box.getBoxName() +"_3");
            validateAndAddIntoLIst(result, dto);

        }

        return result;
    }

    private List<ThreeDimensionalPTItemDTO> breakBoxIntoPartsHorizontallyAndVerticallyV1(List<ThreeDimensionalPTItemDTO> boxList, ThreeDimensionalPTItemDTO box,
            ProductMinDetailDTO productMinDetailDTO) {
        List<ThreeDimensionalPTItemDTO> result = new ArrayList<ThreeDimensionalPTItemDTO>();

        Double[] productDimensions = productMinDetailDTO.getProductDimensions();
        if (canBoxFitTheItem(box, productDimensions)) {

            ThreeDimensionalPTItemDTO dto = null;

            dto = new ThreeDimensionalPTItemDTO(box.getLength() - productDimensions[0], box.getBreadth(), box.getHeight(), box.getBoxName() +"_1");
            validateAndAddIntoLIst(result, dto);

            dto = new ThreeDimensionalPTItemDTO(productDimensions[0], box.getBreadth() - productDimensions[1], productDimensions[2], box.getBoxName() +"_2");
            validateAndAddIntoLIst(result, dto);

            dto = new ThreeDimensionalPTItemDTO(productDimensions[0], box.getBreadth(), box.getHeight() - productDimensions[2], box.getBoxName() +"_3");
            validateAndAddIntoLIst(result, dto);

        }

        return result;
    }

    private List<ThreeDimensionalPTItemDTO> breakBoxIntoPartsVerticallyV2(List<ThreeDimensionalPTItemDTO> boxList, ThreeDimensionalPTItemDTO box,
            ProductMinDetailDTO productMinDetailDTO) {
        List<ThreeDimensionalPTItemDTO> result = new ArrayList<ThreeDimensionalPTItemDTO>();

        Double[] productDimensions = productMinDetailDTO.getProductDimensions();
        if (canBoxFitTheItem(box, productDimensions)) {

            ThreeDimensionalPTItemDTO dto = null;

            dto = new ThreeDimensionalPTItemDTO(box.getLength() - productDimensions[0], productDimensions[1], box.getHeight(), box.getBoxName() +"_1");
            validateAndAddIntoLIst(result, dto);

            dto = new ThreeDimensionalPTItemDTO(box.getLength(), box.getBreadth() - productDimensions[1], box.getHeight(), box.getBoxName() +"_2");
            validateAndAddIntoLIst(result, dto);

            dto = new ThreeDimensionalPTItemDTO(productDimensions[0], productDimensions[1], box.getHeight() - productDimensions[2], box.getBoxName() +"_3");
            validateAndAddIntoLIst(result, dto);

        }

        return result;
    }

    private List<ThreeDimensionalPTItemDTO> breakBoxIntoPartsVerticallyV1(List<ThreeDimensionalPTItemDTO> boxList, ThreeDimensionalPTItemDTO box,
            ProductMinDetailDTO productMinDetailDTO) {
        List<ThreeDimensionalPTItemDTO> result = new ArrayList<ThreeDimensionalPTItemDTO>();

        Double[] productDimensions = productMinDetailDTO.getProductDimensions();
        if (canBoxFitTheItem(box, productDimensions)) {

            ThreeDimensionalPTItemDTO dto = null;

            dto = new ThreeDimensionalPTItemDTO(box.getLength() - productDimensions[0], box.getBreadth(), box.getHeight(), box.getBoxName() +"_1");
            validateAndAddIntoLIst(result, dto);

            dto = new ThreeDimensionalPTItemDTO(productDimensions[0], box.getBreadth() - productDimensions[1], box.getHeight(), box.getBoxName() +"_2");
            validateAndAddIntoLIst(result, dto);

            dto = new ThreeDimensionalPTItemDTO(productDimensions[0], productDimensions[1], box.getHeight() - productDimensions[2], box.getBoxName() +"_2");
            validateAndAddIntoLIst(result, dto);

        }

        return result;
    }

    private List<ThreeDimensionalPTItemDTO> breakBoxIntoPartsHorizontallyV2(List<ThreeDimensionalPTItemDTO> boxList, ThreeDimensionalPTItemDTO box,
            ProductMinDetailDTO productMinDetailDTO) {
        List<ThreeDimensionalPTItemDTO> result = new ArrayList<ThreeDimensionalPTItemDTO>();

        Double[] productDimensions = productMinDetailDTO.getProductDimensions();
        if (canBoxFitTheItem(box, productDimensions)) {

            ThreeDimensionalPTItemDTO dto = null;

            dto = new ThreeDimensionalPTItemDTO(box.getLength() - productDimensions[0], productDimensions[1], productDimensions[2], box.getBoxName() +"_1");
            validateAndAddIntoLIst(result, dto);

            dto = new ThreeDimensionalPTItemDTO(box.getLength(), box.getBreadth() - productDimensions[1], productDimensions[2], box.getBoxName() +"_2");
            validateAndAddIntoLIst(result, dto);

            dto = new ThreeDimensionalPTItemDTO(box.getLength(), box.getBreadth(), box.getHeight() - productDimensions[2], box.getBoxName() +"_3");
            validateAndAddIntoLIst(result, dto);

        }

        return result;
    }

    private ProductMinDetailDTO getRotatedDto(ProductMinDetailDTO productMinDetailDTO, int i) {
        ProductMinDetailDTO rotatedDto = new ProductMinDetailDTO();
        switch (i) {
            case 1:
                // Case 1 : Put item into box such that side with maximum surface area touch box at bottom. Version 1 - 
                rotatedDto = new ProductMinDetailDTO(productMinDetailDTO.getProductDimensions()[0], productMinDetailDTO.getProductDimensions()[1],
                        productMinDetailDTO.getProductDimensions()[2]);
                break;
            case 2:
                // Case 2 : Put item into box such that side with maximum surface area touch box at bottom. Version 2
                rotatedDto = new ProductMinDetailDTO(productMinDetailDTO.getProductDimensions()[1], productMinDetailDTO.getProductDimensions()[0],
                        productMinDetailDTO.getProductDimensions()[2]);
                break;
            case 3:
                // Case 3 : Put item into box such that side with minimum surface area touch box at bottom. Version 1
                rotatedDto = new ProductMinDetailDTO(productMinDetailDTO.getProductDimensions()[2], productMinDetailDTO.getProductDimensions()[1],
                        productMinDetailDTO.getProductDimensions()[0]);
                break;
            case 4:
                // Case 4 : Put item into box such that side with minimum surface area touch box at bottom. Version 2
                rotatedDto = new ProductMinDetailDTO(productMinDetailDTO.getProductDimensions()[1], productMinDetailDTO.getProductDimensions()[2],
                        productMinDetailDTO.getProductDimensions()[0]);
                break;
            case 5:
                rotatedDto = new ProductMinDetailDTO(productMinDetailDTO.getProductDimensions()[0], productMinDetailDTO.getProductDimensions()[2],
                        productMinDetailDTO.getProductDimensions()[1]);
                break;
            case 6:
                rotatedDto = new ProductMinDetailDTO(productMinDetailDTO.getProductDimensions()[2], productMinDetailDTO.getProductDimensions()[0],
                        productMinDetailDTO.getProductDimensions()[1]);
                break;
        }
        return rotatedDto;
    }

    private List<ThreeDimensionalPTItemDTO> breakBoxIntoPartsHorizontallyV1(List<ThreeDimensionalPTItemDTO> boxList, ThreeDimensionalPTItemDTO box,
            ProductMinDetailDTO productMinDetailDTO) {
        List<ThreeDimensionalPTItemDTO> result = new ArrayList<ThreeDimensionalPTItemDTO>();

        Double[] productDimensions = productMinDetailDTO.getProductDimensions();
        if (canBoxFitTheItem(box, productDimensions)) {

            ThreeDimensionalPTItemDTO dto = null;

            dto = new ThreeDimensionalPTItemDTO(box.getLength() - productDimensions[0], box.getBreadth(), productDimensions[2], box.getBoxName() +"_1");
            validateAndAddIntoLIst(result, dto);

            dto = new ThreeDimensionalPTItemDTO(productDimensions[0], box.getBreadth() - productDimensions[1], productDimensions[2], box.getBoxName() +"_2");
            validateAndAddIntoLIst(result, dto);

            dto = new ThreeDimensionalPTItemDTO(box.getLength(), box.getBreadth(), box.getHeight() - productDimensions[2], box.getBoxName() +"_3");
            validateAndAddIntoLIst(result, dto);

        }

        return result;
    }

    private void validateAndAddIntoLIst(List<ThreeDimensionalPTItemDTO> result, ThreeDimensionalPTItemDTO dto) {
        if (dto.isValid3dDto()) {
            result.add(dto);
        }
    }

    /**
     * This method should not sort the dimensions. Need to do without it
     * 
     * @param productMinDetailDTO
     * @param boxList
     * @return
     */
    private ThreeDimensionalPTItemDTO findAppropriateBox(ProductMinDetailDTO productMinDetailDTO, List<ThreeDimensionalPTItemDTO> boxList, boolean needToSort) {
        for (ThreeDimensionalPTItemDTO dto : boxList) {
            Double[] boxDimension = { dto.getLength(), dto.getBreadth(), dto.getHeight() };
            if (ThreeDimensionalPTItemDTO.isProductFittableIntoBox(productMinDetailDTO.getProductDimensions(), boxDimension, needToSort)) {
                return dto;
            }
        }
        return null;
    }

    private int findAppropriateBoxPosition(ProductMinDetailDTO productMinDetailDTO, List<ThreeDimensionalPTItemDTO> boxList, boolean needToSort) {
        Collections.sort(boxList);
        int i = 0;
        for (ThreeDimensionalPTItemDTO dto : boxList) {
            Double[] boxDimension = { dto.getLength(), dto.getBreadth(), dto.getHeight() };
            if (ThreeDimensionalPTItemDTO.isProductFittableIntoBox(productMinDetailDTO.getProductDimensions(), boxDimension, needToSort)) {
                return i;
            }
            i++;
        }
        return i;
    }

    public double getTotalVolWtOfProducts(List<ProductDetailDTO> productDetailList) {
        Double total = 0.0;
        if (!CollectionUtils.isEmpty(productDetailList)) {
            for (ProductDetailDTO dto : productDetailList) {
                total += dto.getVolWt() * dto.getQuantity();
            }
        }
        return total;
    }

    private List<ProductMinDetailDTO> getMinProductDtoList(ShipTogetherSearchDTO dto) {

        List<ProductMinDetailDTO> productMinDetailDtoList = new ArrayList<ProductMinDetailDTO>();

        for (ProductDetailDTO productDetailDto : dto.getProductDetailList()) {
            for (int i = 0; i < productDetailDto.getQuantity(); i++) {
                productMinDetailDtoList.add(new ProductMinDetailDTO(productDetailDto.getLength() + productDetailDto.getMargin(), productDetailDto.getBreadth()
                        + productDetailDto.getMargin(), productDetailDto.getHeight() + productDetailDto.getMargin()));
            }
        }
        return productMinDetailDtoList;
    }

    public interface IDependency {
        int getIntegerScalar(Property property);
    }

    static class Dependency implements IDependency {
        @Override
        public int getIntegerScalar(Property property) {
            return ConfigUtils.getIntegerScalar(property);
        }
    }

}
