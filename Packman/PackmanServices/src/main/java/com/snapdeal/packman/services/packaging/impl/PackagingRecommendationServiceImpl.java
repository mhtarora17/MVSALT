/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.packman.services.packaging.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.base.cache.CacheManager;
import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.model.request.GetProductFulfilmentAttributeRequest;
import com.snapdeal.cocofs.sro.ProductFulfilmentAttributeSRO;
import com.snapdeal.packman.cache.PackagingTypeCache;
import com.snapdeal.packman.cache.PincodeCityStateMappingCache;
import com.snapdeal.packman.cache.ShippingProviderCache;
import com.snapdeal.packman.cache.SlabMappingCache;
import com.snapdeal.packman.common.PackagingType;
import com.snapdeal.packman.common.sro.AddressSRO;
import com.snapdeal.packman.common.sro.SurfacePackagingSRO;
import com.snapdeal.packman.dto.AirPackagingRecommendationDTO;
import com.snapdeal.packman.dto.PackagingTypeCacheDTO;
import com.snapdeal.packman.dto.PincodeCityStateTupleDTO;
import com.snapdeal.packman.dto.RecommendationParamsDTO;
import com.snapdeal.packman.dto.SlabCacheDTO;
import com.snapdeal.packman.enums.PackagingMode;
import com.snapdeal.packman.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.packman.services.external.ICocofsExternalService;
import com.snapdeal.packman.services.packaging.IPackagingRecommendation;
import com.snapdeal.packman.utils.PackmanConstants;

/**
 * @version 1.0, 07-Sep-2015
 * @author ankur
 */

@Service("packagingRecommendationService")
public class PackagingRecommendationServiceImpl implements IPackagingRecommendation {

    private static final Logger LOG = LoggerFactory.getLogger(PackagingRecommendationServiceImpl.class);

    @Autowired
    ICocofsExternalService      cocofsExternalService;

    @Override
    public AirPackagingRecommendationDTO getRecommendationsForAirShipMode(String supc) throws ExternalDataNotFoundException {

        ProductFulfilmentAttributeSRO pauSRO = cocofsExternalService.getProductFulfilmentAttributes(new GetProductFulfilmentAttributeRequest(supc));
        if (pauSRO == null) {
            throw new ExternalDataNotFoundException("null ProductFulfilmentAttribute received for supc " + supc);
        }
        String storeCode = ConfigUtils.getStringScalar(Property.SNAPDEAL_STORE_CODE);
        AirPackagingRecommendationDTO dto = new AirPackagingRecommendationDTO();
        PackagingType pkgingType = PackagingType.CARTON;
        if (PackagingType.POLYBAG.getCode().equals(pauSRO.getPackagingType())) {
            pkgingType = PackagingType.POLYBAG;
        } else if (PackagingType.CARTON.getCode().equals(pauSRO.getPackagingType())) {
            pkgingType = PackagingType.CARTON;
        } else {
            LOG.error(PackmanConstants.BREACH_CASE + "packaging type received from cocofs: {} is not valid for AIR mode, hence returning carton type for supc {}",
                    pauSRO.getPackagingType(), supc);
            pkgingType = PackagingType.CARTON;
        }
        double volWeight = cocofsExternalService.getVolumetricWeightFromLBH(pauSRO.getLength(), pauSRO.getBreadth(), pauSRO.getHeight());
        String pkgingCode = pkgingType.getCode();
        RecommendationParamsDTO recoDto = getRecommendationParamsForSearchDto(supc, pauSRO, storeCode, pkgingType, volWeight);
        SlabCacheDTO recommmendedSlab = CacheManager.getInstance().getCache(SlabMappingCache.class).getSlab(recoDto);
        dto.setPackagingType(pkgingCode);
        if (recommmendedSlab != null) {
            dto.setMostPreferredBox(recommmendedSlab.getMostPreferredBox(recoDto).getCode());
            dto.setRecommedationList(recommmendedSlab.getRecommedationList());
        } else {
            LOG.info("no data exists for store {} and packagingType {}", storeCode, pkgingType);
        }
        return dto;
    }

    private RecommendationParamsDTO getRecommendationParamsForSearchDto(String supc, ProductFulfilmentAttributeSRO pauSRO, String storeCode, PackagingType pkgingType, double volWeight) {
        RecommendationParamsDTO recoDto = new RecommendationParamsDTO();
        recoDto.setStoreCode(storeCode);
        recoDto.setPackagingType(pkgingType.getCode());
        recoDto.setSupc(supc);
        recoDto.setVolWeight(volWeight);
        recoDto.setLength(pauSRO.getLength());
        recoDto.setBreadth(pauSRO.getBreadth());
        recoDto.setHeight(pauSRO.getHeight());
        return recoDto;
    }

    @Override
    public SurfacePackagingSRO getRecommendationsForSurfaceShipMode(String supc, String courierCode, AddressSRO toAddressSRO, AddressSRO fromAddressSRO)
            throws ExternalDataNotFoundException {

        SurfacePackagingSRO surfaceSRO = new SurfacePackagingSRO();
        String storeCode = ConfigUtils.getStringScalar(Property.SNAPDEAL_STORE_CODE);

        try {

            Boolean specialPackaging = CacheManager.getInstance().getCache(ShippingProviderCache.class).getMapping(courierCode);
            LOG.info("specialPackaging found as {} in shippingProvider cache for courier {} ", specialPackaging, courierCode);

            if (specialPackaging == null) {
                LOG.info("no specialPackaging details found in shippingProvider cache for courier {} , using false by default", courierCode);
                specialPackaging = false;
            }

            if (specialPackaging || shipmentInDifferentCityStates(toAddressSRO, fromAddressSRO)) {

                ProductFulfilmentAttributeSRO pauSRO = cocofsExternalService.getProductFulfilmentAttributes(new GetProductFulfilmentAttributeRequest(supc));
                if (pauSRO == null) {
                    throw new ExternalDataNotFoundException("null ProductFulfilmentAttribute received for supc " + supc);
                }

                String packagingType = pauSRO.getPackagingType();

                if (isNotSurfaceType(packagingType)) {
                    LOG.error(PackmanConstants.BREACH_CASE + "packaging type received from cocofs: {} is not valid for SURFACE mode, hence using SURFACE_DEFAULT for supc {}",
                            packagingType, supc);
                    setSurfaceDefault(surfaceSRO);
                    return surfaceSRO;
                }

                List<PackagingTypeCacheDTO> ptDTOList = CacheManager.getInstance().getCache(PackagingTypeCache.class).getPackagingTypeForStoreAndPackagingMode(storeCode,
                        PackagingMode.PICKNPACK.mode());
                PackagingTypeCacheDTO ptCacheDTO = null;
                for (PackagingTypeCacheDTO ptDTO : ptDTOList) {
                    if (ptDTO.getType().equals(packagingType)) {
                        LOG.info("for packagingType {} found {} in cache", packagingType, ptDTO);
                        ptCacheDTO = ptDTO;
                        break;
                    }
                }

                if (ptCacheDTO != null) {
                    surfaceSRO.setPackagingInstruction(ptCacheDTO.getDescription());
                    surfaceSRO.setType(packagingType);
                } else {
                    LOG.error(PackmanConstants.BREACH_CASE + "no surface type found in packman for packagingType {} received for supc {} using SURFACE_DEFAULT", packagingType,
                            supc);
                    setSurfaceDefault(surfaceSRO);
                }
            } else {
                LOG.info("for specialPackaging false and same city hence using SURFACE_DEFAULT");
                setSurfaceDefault(surfaceSRO);
            }
        } catch (Exception e) {
            LOG.error("Error while getting recommendations for surface mode and supc: " + supc + " using SURFACE_DEFAULT. error:", e);
            setSurfaceDefault(surfaceSRO);
        }

        return surfaceSRO;

    }

    private void setSurfaceDefault(SurfacePackagingSRO surfaceSRO) {
        String surfaceType = ConfigUtils.getStringScalar(Property.PACKMAN_DEFAULT_RECO_SURFACE_TYPE);
        String surfaceInst = ConfigUtils.getStringScalar(Property.PACKMAN_DEFAULT_RECO_SURFACE_INST);
        String storeCode = ConfigUtils.getStringScalar(Property.SNAPDEAL_STORE_CODE);
        PackagingTypeCacheDTO stCacheDTO = CacheManager.getInstance().getCache(PackagingTypeCache.class).getPackagingTypeDto(storeCode, PackagingMode.PICKNPACK.mode(), surfaceType);

        if (stCacheDTO != null) {
            surfaceInst = stCacheDTO.getDescription();
        }

        surfaceSRO.setType(surfaceType);
        surfaceSRO.setPackagingInstruction(surfaceInst);
        LOG.info("setSurfaceDefault returning " + surfaceSRO);
    }

    private boolean isNotSurfaceType(String packagingType) {
        return StringUtils.isEmpty(packagingType) || "NONE".equals(packagingType) || PackagingType.CARTON.getCode().equals(packagingType)
                || PackagingType.POLYBAG.getCode().equals(packagingType);
    }

    private boolean shipmentInDifferentCityStates(AddressSRO toAddressSRO, AddressSRO fromAddressSRO) {
        PincodeCityStateMappingCache cache = CacheManager.getInstance().getCache(PincodeCityStateMappingCache.class);
        List<PincodeCityStateTupleDTO> toCityStateList = cache.getMapping(toAddressSRO.getPincode());
        List<PincodeCityStateTupleDTO> fromCityStateList = cache.getMapping(fromAddressSRO.getPincode());
        LOG.info("for toAddressSRO {} found toCityStateList {}", toAddressSRO, toCityStateList);
        LOG.info("for fromAddressSRO {} found fromCityStateList {}", fromAddressSRO, fromCityStateList);

        // if no address mapping found, need to send SURFACE_DEFAULT
        if (com.snapdeal.base.utils.CollectionUtils.isEmpty(fromCityStateList)) {
            LOG.info("shipmentInDifferentCityStates returning false as could not locate city state mapping from SCORE, for {} found {}", fromAddressSRO, fromCityStateList);
            return false;
        }

        if (com.snapdeal.base.utils.CollectionUtils.isEmpty(toCityStateList)) {
            LOG.info("shipmentInDifferentCityStates returning false as could not locate city state mapping from SCORE, for {} found {}", toAddressSRO, toCityStateList);
            return false;
        }

        for (PincodeCityStateTupleDTO toCityStateTuple : toCityStateList) {
            for (PincodeCityStateTupleDTO fromCityStateTuple : fromCityStateList) {
                if (toCityStateTuple.getState().equals(fromCityStateTuple.getState()) && toCityStateTuple.getCity().equals(fromCityStateTuple.getCity())) {
                    LOG.info("shipmentInDifferentCityStates returning false as city and state matches for pinodes {} and {}", toAddressSRO.getPincode(),
                            fromAddressSRO.getPincode());
                    return false;
                }
            }
        }

        LOG.info("shipmentInDifferentCityStates returning true as city and state does not match for pinodes {} and {}", toAddressSRO.getPincode(), fromAddressSRO.getPincode());
        return true;
    }
}
