/**
 *  Copyright 2016 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.packman.services.packaging.recomendation.impl;

import static com.snapdeal.packman.common.ApiErrorCode.INVALID_SUPC_CODE;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.base.cache.CacheManager;
import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.cocofs.model.request.GetProductFulfilmentAttributeRequest;
import com.snapdeal.cocofs.sro.ProductFulfilmentAttributeSRO;
import com.snapdeal.packman.cache.PackagingTypeCache;
import com.snapdeal.packman.cache.SlabMappingCache;
import com.snapdeal.packman.common.sro.PackagingTypeItemSRO;
import com.snapdeal.packman.dto.PackagingTypeCacheDTO;
import com.snapdeal.packman.dto.PackagingTypeItemDTO;
import com.snapdeal.packman.dto.RecommendationParamsDTO;
import com.snapdeal.packman.dto.SlabCacheDTO;
import com.snapdeal.packman.dto.SlabCacheDTO.SlabItemDTO;
import com.snapdeal.packman.services.exception.PackagingRecomendationException;
import com.snapdeal.packman.services.external.ICocofsExternalService;
import com.snapdeal.packman.services.packaging.dto.RecommendationResponseDTO;
import com.snapdeal.packman.services.packaging.recomendation.IPackmanRecommendationService;

/**
 * @version 1.0, 10-Feb-2016
 * @author brijesh
 */

@Service("packmanRecommendationService")
public class PackmanRecommendationServiceImpl implements IPackmanRecommendationService {

    private static final Logger LOG = LoggerFactory.getLogger(PackmanRecommendationServiceImpl.class);

    @Autowired
    ICocofsExternalService      cocofsExternalService;

    @Override
    public RecommendationResponseDTO getPackagingRecommendation(RecommendationParamsDTO dto) {
        RecommendationResponseDTO resp = convertResponse(dto);
        PackagingTypeCacheDTO stCacheDTO = CacheManager.getInstance().getCache(PackagingTypeCache.class).getPackagingTypeDtoV1(dto.getStoreCode(), dto.getPackagingType());
        if (stCacheDTO != null) {
            PackagingTypeItemDTO resp1 = stCacheDTO.getRecommendation(dto);
            resp.setPrefferedBox(resp1.getBoxName());
        }
        return resp;
    }

    private RecommendationResponseDTO convertResponse(RecommendationParamsDTO dto) {
        RecommendationResponseDTO res = null;
        if (dto != null) {
            res = new RecommendationResponseDTO(dto.getStoreCode(), dto.getPackagingType(), dto.getSupc(), dto.getVolWeight(), dto.getLength(), dto.getBreadth(), dto.getHeight(),
                    dto.isRecommendationBasedOnLBH(), dto.isRecommendationBasedOnDeltaVolWt(), dto.getRecommendationLBHMargin(), dto.getRecommendationDeltaVolWtOffset(),
                    dto.getRecommendationDeltaMultFactor(), null, null, null);
        }
        return res;
    }

    @Override
    public RecommendationParamsDTO getEnrichedRecommendationDto(RecommendationParamsDTO dto) throws PackagingRecomendationException {
        RecommendationParamsDTO out = new RecommendationParamsDTO();
        out.setStoreCode(dto.getStoreCode());
        out.setPackagingType(dto.getPackagingType());
        if (dto.isRecommendationBasedOnDeltaVolWt() != null) {
            out.setRecommendationBasedOnDeltaVolWt(dto.isRecommendationBasedOnDeltaVolWt());
        }
        if (dto.isRecommendationBasedOnLBH() != null) {
            out.setRecommendationBasedOnLBH(dto.isRecommendationBasedOnLBH());
        }
        if (dto.getRecommendationLBHMargin() != null) {
            out.setRecommendationLBHMargin(dto.getRecommendationLBHMargin());
        }
        if (dto.getRecommendationDeltaMultFactor() != null) {
            out.setRecommendationDeltaMultFactor(dto.getRecommendationDeltaMultFactor());
        }
        if (dto.getRecommendationDeltaVolWtOffset() != null) {
            out.setRecommendationDeltaVolWtOffset(dto.getRecommendationDeltaVolWtOffset());
        }
        if (StringUtils.isNotEmpty(dto.getSupc())) {
            out.setSupc(dto.getSupc());
            ProductFulfilmentAttributeSRO pauSRO = getProductAttributes(dto.getSupc());
            out.setLength(pauSRO.getLength());
            out.setBreadth(pauSRO.getBreadth());
            out.setHeight(pauSRO.getHeight());
            out.setVolWeight(cocofsExternalService.getVolumetricWeightFromLBH(pauSRO.getLength(), pauSRO.getBreadth(), pauSRO.getHeight()));
        } else if (dto.getLength() != null && dto.getBreadth() != null && dto.getHeight() != null) {
            out.setSupc("Test-Supc");
            out.setLength(dto.getLength());
            out.setBreadth(dto.getBreadth());
            out.setHeight(dto.getHeight());
            out.setVolWeight(cocofsExternalService.getVolumetricWeightFromLBH(dto.getLength(), dto.getBreadth(), dto.getHeight()));
        } else {
            // Considering vol. wt only
            out.setSupc("Test-Supc");
            out.setVolWeight(dto.getVolWeight());
            out.setRecommendationBasedOnLBH(false);
            out.setRecommendationBasedOnDeltaVolWt(false);
        }
        return out;
    }

    private ProductFulfilmentAttributeSRO getProductAttributes(String supc) throws PackagingRecomendationException {
        ProductFulfilmentAttributeSRO pauSRO = cocofsExternalService.getProductFulfilmentAttributes(new GetProductFulfilmentAttributeRequest(supc));
        if (null == pauSRO) {
            LOG.error("Product fullfillment attributes not found for supc :" + supc);
            throw new PackagingRecomendationException("SUPC does not exist, supc:" + supc, INVALID_SUPC_CODE);
        }
        return pauSRO;
    }

}
