/**
 *  Copyright 2016 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.services.packaging.dto;

import java.util.List;

import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;

/**
 *  
 *  @version     1.0, 10-Feb-2016
 *  @author brijesh
 */
public class RecommendationResponseDTO {

    private String  storeCode;

    private String  packagingType;

    private String  supc;

    private Double  length;

    private Double  breadth;

    private Double  height;

    private Double  volWeight;

    private boolean recommendationBasedOnLBH;

    private boolean recommendationBasedOnDeltaVolWt;

    private double  recommendationLBHMargin;

    private double  recommendationDeltaVolWtOffset;

    private double  recommendationDeltaMultFactor;

    private String  prefferedBox;
    
    private List<String> recommendedBox;
    
    private String  error;
    
    public RecommendationResponseDTO() {
        this.recommendationBasedOnLBH = ConfigUtils.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_LBH);
        this.recommendationBasedOnDeltaVolWt = ConfigUtils.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_DELTA_VOL_WT);
        this.recommendationLBHMargin = ConfigUtils.getDoubleScalar(Property.PACKMAN_RECO_LBH_MARGIN);
        this.recommendationDeltaMultFactor = ConfigUtils.getDoubleScalar(Property.PACKMAN_RECO_DELTA_VOL_WT_MULT_FACTOR);
        this.recommendationDeltaVolWtOffset = ConfigUtils.getDoubleScalar(Property.PACKMAN_RECO_DELTA_VOL_WT_OFFSET);
    }

    
    public RecommendationResponseDTO(String storeCode, String packagingType, String supc, Double volWeight, Double length, Double breadth, Double height,
            boolean recommendationBasedOnLBH, boolean recommendationBasedOnDeltaVolWt, double recommendationLBHMargin, double recommendationDeltaVolWtOffset,
            double recommendationDeltaMultFactor, String prefferedBox, List<String> recommendedBox, String error) {
        super();
        this.storeCode = storeCode;
        this.packagingType = packagingType;
        this.supc = supc;
        this.volWeight = volWeight;
        this.length = length;
        this.breadth = breadth;
        this.height = height;
        this.recommendationBasedOnLBH = recommendationBasedOnLBH;
        this.recommendationBasedOnDeltaVolWt = recommendationBasedOnDeltaVolWt;
        this.recommendationLBHMargin = recommendationLBHMargin;
        this.recommendationDeltaVolWtOffset = recommendationDeltaVolWtOffset;
        this.recommendationDeltaMultFactor = recommendationDeltaMultFactor;
        this.prefferedBox = prefferedBox;
        this.recommendedBox = recommendedBox;
        this.error = error;
    }


    public String getStoreCode() {
        return storeCode;
    }

    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }

    public String getPackagingType() {
        return packagingType;
    }

    public void setPackagingType(String packagingType) {
        this.packagingType = packagingType;
    }

    public String getSupc() {
        return supc;
    }

    public void setSupc(String supc) {
        this.supc = supc;
    }

    public Double getVolWeight() {
        return volWeight;
    }

    public void setVolWeight(Double volWeight) {
        this.volWeight = volWeight;
    }

    public Double getLength() {
        return length;
    }

    public void setLength(Double length) {
        this.length = length;
    }

    public Double getBreadth() {
        return breadth;
    }

    public void setBreadth(Double breadth) {
        this.breadth = breadth;
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public boolean isRecommendationBasedOnLBH() {
        return recommendationBasedOnLBH;
    }

    public void setRecommendationBasedOnLBH(boolean recommendationBasedOnLBH) {
        this.recommendationBasedOnLBH = recommendationBasedOnLBH;
    }

    public boolean isRecommendationBasedOnDeltaVolWt() {
        return recommendationBasedOnDeltaVolWt;
    }

    public void setRecommendationBasedOnDeltaVolWt(boolean recommendationBasedOnDeltaVolWt) {
        this.recommendationBasedOnDeltaVolWt = recommendationBasedOnDeltaVolWt;
    }

    public double getRecommendationLBHMargin() {
        return recommendationLBHMargin;
    }

    public void setRecommendationLBHMargin(double recommendationLBHMargin) {
        this.recommendationLBHMargin = recommendationLBHMargin;
    }

    public double getRecommendationDeltaVolWtOffset() {
        return recommendationDeltaVolWtOffset;
    }

    public void setRecommendationDeltaVolWtOffset(double recommendationDeltaVolWtOffset) {
        this.recommendationDeltaVolWtOffset = recommendationDeltaVolWtOffset;
    }

    public double getRecommendationDeltaMultFactor() {
        return recommendationDeltaMultFactor;
    }

    public void setRecommendationDeltaMultFactor(double recommendationDeltaMultFactor) {
        this.recommendationDeltaMultFactor = recommendationDeltaMultFactor;
    }

    public String getPrefferedBox() {
        return prefferedBox;
    }

    public void setPrefferedBox(String prefferedBox) {
        this.prefferedBox = prefferedBox;
    }

    public List<String> getRecommendedBox() {
        return recommendedBox;
    }

    public void setRecommendedBox(List<String> recommendedBox) {
        this.recommendedBox = recommendedBox;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return "RecommendationResponseDTO [storeCode=" + storeCode + ", packagingType=" + packagingType + ", supc=" + supc + ", volWeight=" + volWeight + ", length=" + length
                + ", breadth=" + breadth + ", height=" + height + ", recommendationBasedOnLBH=" + recommendationBasedOnLBH + ", recommendationBasedOnDeltaVolWt="
                + recommendationBasedOnDeltaVolWt + ", recommendationLBHMargin=" + recommendationLBHMargin + ", recommendationDeltaVolWtOffset=" + recommendationDeltaVolWtOffset
                + ", recommendationDeltaMultFactor=" + recommendationDeltaMultFactor + ", prefferedBox=" + prefferedBox + ", recommendedBox=" + recommendedBox + ", error=" + error
                + "]";
    }
    
    
    
}
