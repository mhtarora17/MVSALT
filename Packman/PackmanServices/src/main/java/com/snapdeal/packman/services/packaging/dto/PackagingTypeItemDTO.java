/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.services.packaging.dto;

import java.util.Date;

import com.snapdeal.packman.entity.PackagingType;

/**
 *  
 *  @version     1.0, 04-Dec-2015
 *  @author brijesh
 */
public class PackagingTypeItemDTO {
    
    private String            code;
    private String            description;
    private PackagingType     packagingType;
    private String            packagingTypeCategory;
//    private Slab              slab;
    private Date              created;
    private Date              lastUpdated;
    private String            updatedBy;
    private Float             length;
    private Float             breadth;
    private Float             height;
    private Double            volWeight;
    
    public String getCode() {
        return code;
    }
    
    public void setCode(String code) {
        this.code = code;
    }
    
    public String getDescription() {
        return description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    
    public PackagingType getPackagingType() {
        return packagingType;
    }
    
    public void setPackagingType(PackagingType packagingType) {
        this.packagingType = packagingType;
    }
    
//    public Slab getSlab() {
//        return slab;
//    }
//    
//    public void setSlab(Slab slab) {
//        this.slab = slab;
//    }
    
    public String getPackagingTypeCategory() {
        return packagingTypeCategory;
    }

    public void setPackagingTypeCategory(String packagingTypeCategory) {
        this.packagingTypeCategory = packagingTypeCategory;
    }

    public Date getCreated() {
        return created;
    }
    
    public void setCreated(Date created) {
        this.created = created;
    }
    
    public Date getLastUpdated() {
        return lastUpdated;
    }
    
    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }
    
    public String getUpdatedBy() {
        return updatedBy;
    }
    
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }
    
    public Float getLength() {
        return length;
    }
    
    public void setLength(Float length) {
        this.length = length;
    }
    
    public Float getBreadth() {
        return breadth;
    }
    
    public void setBreadth(Float breadth) {
        this.breadth = breadth;
    }
    
    public Float getHeight() {
        return height;
    }
    
    public void setHeight(Float height) {
        this.height = height;
    }
    
    public Double getVolWeight() {
        return volWeight;
    }
    
    public void setVolWeight(Double volWeight) {
        this.volWeight = volWeight;
    }

    @Override
    public String toString() {
        return "PackagingTypeItemDTO [code=" + code + ", description=" + description + ", packagingType=" + packagingType + ", packagingTypeCategory=" + packagingTypeCategory
                + ", created=" + created + ", lastUpdated=" + lastUpdated + ", updatedBy=" + updatedBy + ", length=" + length + ", breadth=" + breadth + ", height=" + height
                + ", volWeight=" + volWeight + "]";
    }
    
}
