/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.services.rules.impl;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.snapdeal.base.cache.CacheManager;
import com.snapdeal.cocofs.db.dao.IPackmanRuleDao;
import com.snapdeal.packman.enums.BlockName;
import com.snapdeal.packman.enums.PackagingRuleParam;
import com.snapdeal.packman.enums.PackagingTypeRuleCriteria;
import com.snapdeal.packman.rule.dto.SearchRuleDTO;
import com.snapdeal.packman.services.rules.IPackmanRuleService;
import com.snapdeal.rule.cache.BlocksCache;
import com.snapdeal.rule.engine.entity.Criteria;
import com.snapdeal.rule.engine.entity.Rule;
import com.snapdeal.rule.engine.entity.RuleParams;
import com.snapdeal.rule.engine.services.IRulesService;

/**
 *  
 *  @version     1.0, 23-Dec-2015
 *  @author brijesh
 */

@Service("packmanRuleService")
public class PackmanRuleServiceImpl implements IPackmanRuleService {

    private static final Logger LOG = LoggerFactory.getLogger(PackmanRuleManagementImpl.class);

    @Autowired
    IPackmanRuleDao    packmanRuleDao;
    
    @Autowired
    IRulesService       ruleService;
    
    @Override
    @Transactional(readOnly = true)//TODO create dto instead of function args
    public Object[] getSameRules(String ruleLevel,String storeCode, String subtype, int priority, int blockId, String category, String subCategory,Date startDate,Date endDate,String brand,String supercategory,boolean bypassPriority,String ruleName) {
        List<Rule> rules = getAllRulesForCategoryAndType(subtype, ruleLevel);
        List<Rule> sameRules = new ArrayList<Rule>();
        StringBuffer exceptionList = new StringBuffer();
        String[] brands = null;
        if(null != brand)
            brands = brand.split(",");
        String supercategorys[] = null;
        if(null != supercategory)
            supercategorys = supercategory.split(",");
        if(null == endDate)
            endDate = new Date(Long.MAX_VALUE);//TODO use null as maximum endDate
        for(Rule r : rules){
            if(!(ruleName!= null && r.getName().equals(ruleName))){
                StringBuffer exceptions = new StringBuffer();
                Date startValue = r.getStartDate();
                Date startdtdb = startValue;
                Date endDateValue = r.getEndDate();
                Date enddtdb = endDateValue;
                exceptions.append("{ Rule for ");
                if(null == enddtdb)
                    enddtdb = new Date(Long.MAX_VALUE);
                if((bypassPriority || r.getPriority()==priority) && r.getBlockId()==blockId){
                    boolean flagParam = true, flagCriteria = true;//TODO check default value of flags to be false
                    Set<RuleParams> ruleParams = r.getRuleParams();
                    for(RuleParams rp : ruleParams){
                        if(null != storeCode && rp.getParamName().equals(PackagingRuleParam.STORECODE.getCode())){
                            if(rp.getParamValue().equals(storeCode)){
                                flagParam = true;
                                exceptions.append("store ["+storeCode+"] ");
                            } else {
                                flagParam = false;
                                break;
                            }
                        }
                        if(null != category && rp.getParamName().equals(PackagingRuleParam.CATEGORY.getCode())){
                            if(rp.getParamValue().equals(category)){
                                flagParam = true;
                                exceptions.append("category ["+category+"] ");
                            } else {
                                flagParam = false;
                                break;
                            }
                        }
                        if(null != subCategory && rp.getParamName().equals(PackagingRuleParam.SUBCATEGORY.getCode())){
                            if(rp.getParamValue().equals(subCategory)){
                                flagParam = true;
                                exceptions.append("subcategory ["+subCategory+"] ");
                            } else {
                                flagParam = false;
                                break;
                            }
                        }
                    }
                    //TODO add check if flagParam is true 
                    Set<Criteria> ruleCriteria = r.getCriterion();
                    if(ruleCriteria.size()==0 && (null != brand || null != supercategory))
                        flagCriteria = false;
                    int brandsMatched = 0,supercatMatched=0;
                    for(Criteria c : ruleCriteria){
                        if(null !=  brand && (c.getName().equals(PackagingTypeRuleCriteria.BRAND.name()))){
                            exceptions.append("brand [");
                            for(String b : brands){
                                String brandFromDb = addSeparator(c.getValue()); //TODO make a funct to get brands list and check contains there
                                if(brandFromDb.contains(addSeparator(b))){
                                    flagCriteria = true;
                                    exceptions.append(b+",");
                                    brandsMatched++;
                                }
                            }
                            if(brandsMatched>0)
                                exceptions.deleteCharAt(exceptions.length()-1);
                            exceptions.append("] ");
                        }
                        if(null != supercategory && c.getName().equals(PackagingTypeRuleCriteria.LABEL.name())){
                            exceptions.append("supercategory [");
                            for(String s : supercategorys){
                                String supcatFromDb = ","+c.getValue()+",";
                                if(supcatFromDb.contains(addSeparator(s))){
                                    supercatMatched++;
                                    flagCriteria = true;
                                    exceptions.append(s+",");
                                }
                            }
                            if(supercatMatched>0)
                                exceptions.deleteCharAt(exceptions.length()-1);
                            exceptions.append("] ");
                        }
                    }
                    if(brandsMatched==0 && supercatMatched==0){
                        if(brand!=null || supercategory!=null){
                            flagCriteria = false;
                        }
                    }
                    
                    if((brandsMatched==0 && brand!=null && supercatMatched!=0) || (brandsMatched!=0 && supercatMatched==0 && supercategory!=null)){
                        flagCriteria = false;
                    }
                    
                    if( flagParam && !(beforeOrEqual(endDate, startdtdb) || beforeOrEqual(enddtdb, startDate))){
                        flagParam = true;
                    } else {
                        flagParam = false;
                    }
                    if (flagParam && flagCriteria) {
                        sameRules.add(r);
                        String endTime = "",startTime="";
                        if(startValue != null){
                            String ed = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG).format(startValue);
                            startTime = "from "+ ed;
                        }
                        if(endDateValue != null){
                            String ed = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG).format(endDateValue);
                            endTime = " to "+ ed;
                        }
                        exceptions.append(startTime+endTime+ " already exists. } : Rule Name - "+r.getName()+"<br>");
                        exceptionList.append(exceptions);
                        LOG.info(exceptions.toString());
                    }
                }
            }
        }
        return new Object[] {exceptionList.toString(),sameRules};
    }
    
    private boolean beforeOrEqual(Date date1,Date date2){
        return date1.getTime()<=date2.getTime();
    }
    
    private List<Rule> getAllRulesForCategoryAndType(String subtype, String type) {
        List<Rule> rules = ruleService.getRuleByCode(getRuleCodeForCategoryAndType(subtype, type));
        List<Rule> enabledRules = new ArrayList<Rule>();
        initializeCriteria(rules);
        initializeRuleParams(rules);
        for (Rule r : rules) {
            if(r.isEnabled()){
                enabledRules.add(r);
            }
        }
        return enabledRules;
    }
    
    private String getRuleCodeForCategoryAndType(String subtype, String type) {
        return type + "|" + subtype;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Rule> getMatchingRules(SearchRuleDTO dto) {

        boolean cat = dto.getCategory()!=null?true:false;
        boolean subcat = dto.getSubcategory()!=null?true:false;
        boolean supcat = dto.getSupercategory()!=null?true:false;
        boolean brand = dto.getBrand()!=null?true:false;
        
        List<Rule> ruleList = new ArrayList<Rule>();
        
        // Fetch all rules with given packaging type
        if(dto.getPackagingType()!=null){
            return getRuleByRuleParams(PackagingRuleParam.STORECODE.getCode(), dto.getStoreCode(), PackagingRuleParam.RETURN_VALUE.getCode(), dto.getPackagingType());
        }
        
        if(dto.getRulename()!=null){
                return getRuleFromName(dto.getRulename(),PackagingRuleParam.STORECODE.getCode(),dto.getStoreCode());
        }
        
        // Fetch store rules only if cat,subcat,supcat and brand all are null
        if(!(cat || subcat || supcat || brand)){
            List<Rule> rule = packmanRuleDao.getRuleByRuleParamsAndBlockId(PackagingRuleParam.STORECODE.getCode(), dto.getStoreCode(), CacheManager.getInstance().getCache(BlocksCache.class).getBlockIDByName(BlockName.StorePackagingRuleBlock.getCode()));
            for(Rule r : rule){
                boolean flagParam = true;
                for(RuleParams b : r.getRuleParams()){
                    if(b.getParamName().equals(PackagingRuleParam.STORECODE.getCode()) && !b.getParamValue().equals(dto.getStoreCode()))
                        flagParam = false;
                }
                if(flagParam)
                    ruleList.add(r);
            }
        }
        
        if(cat){
            List<Rule> rule = getRuleByRuleParams(PackagingRuleParam.STORECODE.getCode(), dto.getStoreCode(), PackagingRuleParam.CATEGORY.getCode(), dto.getCategory());
            for (Rule r : rule) {
                rulesWithMatchingCriterias(dto, supcat, brand, ruleList, r);
            }
        }

        if(subcat){
            List<Rule> rule = getRuleByRuleParams(PackagingRuleParam.STORECODE.getCode(), dto.getStoreCode(), PackagingRuleParam.SUBCATEGORY.getCode(), dto.getSubcategory());
            for (Rule r : rule) {
                rulesWithMatchingCriterias(dto, supcat, brand, ruleList, r);
            }
        }
        
        if(!(cat || subcat) && (brand || supcat)){
            List<Rule> rule = getAllRulesByStore(dto.getStoreCode());
            for(Rule r : rule){
                rulesWithMatchingCriterias(dto, supcat, brand, ruleList, r);
            }
        }
        
        return ruleList;
    }

    private void rulesWithMatchingCriterias(SearchRuleDTO dto, boolean supcat, boolean brand, List<Rule> ruleList, Rule r) {
        boolean flagCriteria = true;
        boolean brandExist = false, supcatExist = false;
        for(Criteria c : r.getCriterion()){
            if(c.getName().equals(PackagingTypeRuleCriteria.BRAND.name())){
                brandExist = true;
                if(brand){
                    String brandDb = addSeparator(c.getValue());
                    for(String s : dto.getBrand()){
                        if(!brandDb.contains(addSeparator(s))){
                            flagCriteria = false;
                            break;
                        }
                    }
                }
            }
            if(c.getName().equals(PackagingTypeRuleCriteria.LABEL.name())){
                supcatExist = true;
                if(supcat){
                    String supcatDb = addSeparator(c.getValue());
                    for(String s : dto.getSupercategory()){
                        if(!supcatDb.contains(addSeparator(s))){
                            flagCriteria = false;
                            break;
                        }
                    }
                }
            }
        }
        if(flagCriteria && (!brand || (brand && brandExist)) && (!supcat || (supcat && supcatExist)))
            if(dto.getCreatedby()==null || (r.getCreatedBy().equals(dto.getCreatedby())))
                ruleList.add(r);
    }

    private String addSeparator(String s) {
        return ","+s+",";
    }

    @Override
    public List<Rule> getAllRulesByStore(String storeCode) {
        List<Rule> rules = packmanRuleDao.getAllRuleForStore(PackagingRuleParam.STORECODE.getCode(),storeCode);
        initializeRuleParams(rules);
        return rules;
    }
    
    private List<Rule> getRuleByRuleParams(String p1,String v1,String p2,String v2){
        List<Rule> rules =  packmanRuleDao.getAllRuleByRuleParams(p1, v1, p2, v2);
        initializeCriteria(rules);
        initializeRuleParams(rules);
        return rules;
    }

    @Override
    @Transactional
    public List<Rule> getRuleFromName(String name,String key,String value) {
        List<Rule> rules = packmanRuleDao.getRuleByName(name,key,value);
        initializeCriteria(rules);
        initializeRuleParams(rules);
        return rules;
    }

    private void initializeCriteria(List<Rule> rules) {
        if(null != rules)
            for (Rule r : rules) {
                if (!Hibernate.isInitialized(r.getCriterion())) {
                    Hibernate.initialize(r.getCriterion());
                }
            }
    }

    private void initializeRuleParams(List<Rule> rules) {
        if(null != rules)
            for (Rule r : rules) {
                if (!Hibernate.isInitialized(r.getRuleParams())) {
                    Hibernate.initialize(r.getRuleParams());
                }
            }
    }

    @Override
    public List<Rule> getRuleByStoreAndPackagingType(String storeCode, String packagingType) {
        return packmanRuleDao.getAllRuleByRuleParams(PackagingRuleParam.STORECODE.getCode(), storeCode, PackagingRuleParam.RETURN_VALUE.getCode(), packagingType);
    }
}
