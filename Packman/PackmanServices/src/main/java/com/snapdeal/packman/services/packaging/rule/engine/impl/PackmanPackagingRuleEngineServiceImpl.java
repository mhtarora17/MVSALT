/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.packman.services.packaging.rule.engine.impl;

import static com.snapdeal.packman.common.ApiErrorCode.EXTERNAL_SERVICE_CALL_FAILED;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.base.tenant.model.StoreFront;
import com.snapdeal.base.utils.CollectionUtils;
import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.catalog.base.model.GetPOGDetailListBySUPCListRequest;
import com.snapdeal.catalog.base.model.GetPOGDetailListResponse;
import com.snapdeal.catalog.base.sro.BucketSRO;
import com.snapdeal.catalog.base.sro.FilterValueSRO;
import com.snapdeal.catalog.base.sro.POGDetailSRO;
import com.snapdeal.catalog.base.sro.POGDetailSRO.ProductCategoryDetailSRO;
import com.snapdeal.catalog.base.sro.POGDetailSRO.ProductOfferDetailSRO;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.external.service.factory.IExternalServiceFactory;
import com.snapdeal.cocofs.mongo.CocofsMongoQuery;
import com.snapdeal.cocofs.mongo.CocofsMongoQuery.Operator;
import com.snapdeal.cocofs.mongo.Criteria;
import com.snapdeal.cocofs.mongo.mao.IGenericMao;
import com.snapdeal.cocofs.services.impl.RuleEvaluationHelper;
import com.snapdeal.packman.dataaccess.supcpt.ISupcPackagingTypeMongoService;
import com.snapdeal.packman.enums.BlockName;
import com.snapdeal.packman.enums.PackagingRuleOrder;
import com.snapdeal.packman.enums.PackagingRuleParam;
import com.snapdeal.packman.mongo.model.PackmanRuleUnit;
import com.snapdeal.packman.mongo.model.PackmanRuleUnit.PackmanRuleUnitFieldName;
import com.snapdeal.packman.services.exception.PackagingRecomendationException;
import com.snapdeal.packman.services.exception.PackagingTypeNotFoundException;
import com.snapdeal.packman.services.packaging.IPackmanExclusiveService;
import com.snapdeal.packman.services.packaging.dto.CatageoryEvaluationParamDto;
import com.snapdeal.packman.services.packaging.dto.SUPCDetailDTO;
import com.snapdeal.packman.services.packaging.dto.SubCatagoryEvaluationParamDto;
import com.snapdeal.packman.services.packaging.rule.engine.IPackmanPackagingRuleEngineService;
import com.snapdeal.rule.engine.entity.BlockParams;
import com.snapdeal.rule.engine.entity.Rule;
import com.snapdeal.rule.engine.entity.RuleParams;
import com.snapdeal.rule.engine.evaluator.impl.RuleEvaluator;
import com.snapdeal.rule.engine.services.IRulesService;

/**
 * @version 1.0, 23-Dec-2015
 * @author indrajit
 */
@Service("ruleEngineService")
public class PackmanPackagingRuleEngineServiceImpl extends RuleEvaluator implements IPackmanPackagingRuleEngineService {

    private static final Logger     LOG = LoggerFactory.getLogger(PackmanPackagingRuleEngineServiceImpl.class);

    @Autowired
    private IExternalServiceFactory factory;

    @Autowired
    private IGenericMao             genericMao;

    @Autowired
    private IRulesService           ruleService;

    @Autowired
    ISupcPackagingTypeMongoService  supcPackagingTypeMongoService;

    @Autowired
    IPackmanExclusiveService        exclusiveService;
    
    private IDependency               dep = new Dependency();

    @Override
    public String calculatePackagingType(SUPCDetailDTO dto) throws PackagingTypeNotFoundException, ExternalDataNotFoundException {
        String pacakagingType = null;
        GetPOGDetailListResponse response = null;


        try {
  
            GetPOGDetailListBySUPCListRequest request = new GetPOGDetailListBySUPCListRequest();
            List<String> supcList = new ArrayList<String>();
            supcList.add(dto.getSupc());
            request.setSupcs(supcList);
            if (dto.isCatalogDetailsRequired()) {

                response = factory.getCAMSExternalService().getPOGDetailsBbySupc(request);
                LOG.info("CAMS response for SUPC {} : {}", dto.getSupc(), response);
                if (null != response && response.isSuccessful()) {
                    prepareCompleteDTO(dto, response);
                } else {
                    throw new PackagingRecomendationException("External server call failed", EXTERNAL_SERVICE_CALL_FAILED);
                }
             //   LOG.info("SUPC dto: {}", dto);
            }
            List<String> ruleExecutionOrderList = dep.getStringList(Property.PACKAGING_RULE_EXECUTION_ORDER);
            for (String str : ruleExecutionOrderList) {
                pacakagingType = callRuleBasedOnSequence(PackagingRuleOrder.getPackagingRuleOrder(str).getSequence(), dto);
                if (null != pacakagingType && StringUtils.isNotEmpty(pacakagingType)) {
                    LOG.info("Rule found at level :" + str);
                    LOG.info("Packaging type found from rule is " + pacakagingType);
                    break;
                }
            }

        } catch (ExternalDataNotFoundException ex) {
            LOG.error("External calls to CAMS failed : " + ex);
            throw ex;
        } catch (Exception ex) {
            LOG.error("Packaging type not found exception: "  ,ex);
            throw new PackagingTypeNotFoundException(ex.getMessage());
        }

        return pacakagingType;

    }

    private String callRuleBasedOnSequence(int seq, SUPCDetailDTO dto) {
        String packagingType = null;
        List<Rule> ruleList = null;
        switch (seq) {
            case 1:
                packagingType = supcPackagingTypeMongoService.getEnabledAndActivePackagingTypeBySupcAndStoreCode(dto.getStoreCode(), dto.getSupc());
                break;
            case 2:
                ruleList = fetchRuleUnitsForSubCatageory(dto.getSubCatageory(), dto.getStoreCode());
                if (!CollectionUtils.isEmpty(ruleList)) {
                    packagingType = evaluateRuleOnSubCatageory(ruleList, dto);
                }
                break;
            case 3:
                ruleList = fetchRuleUnitsForCatageory(dto.getCatageory(), dto.getStoreCode());
                if (!CollectionUtils.isEmpty(ruleList)) {
                    packagingType = evaluateRuleOnCatageory(ruleList, dto);
                }
                break;
            case 4:
                ruleList = fetchRuleUnitsForStore(dto.getStoreCode());
                packagingType = evaluateStoreRule(ruleList);
                break;
            default:
                break;
        }
        return packagingType;

    }




    private List<Rule> fetchRuleUnitsForCatageory(String catageory, String storeCode) {
        List<Rule> outList = null;
        if (null == catageory || StringUtils.isEmpty(catageory)) {
            outList = new ArrayList<Rule>();
            return outList;
        }

        List<PackmanRuleUnit> ruleList = fetchRuleUnit(PackmanRuleUnit.PackmanRuleUnitType.CATEGORY_LEVEL_PACKAGING, PackmanRuleUnit.PackmanRuleUnitFieldName.STORECODE, storeCode,
                PackmanRuleUnit.PackmanRuleUnitFieldName.PARAM_VALUE, catageory);

        
        if (null == ruleList || ruleList.isEmpty()) {

            return new ArrayList<Rule>();
        }

        List<Rule> list = new ArrayList<Rule>();
        for (PackmanRuleUnit unit : ruleList) {
            Rule r = RuleEvaluationHelper.getRuleFromCocofsRuleUnit(unit);
            list.add(r);
        }

        return list;
    }

    private List<Rule> fetchRuleUnitsForSubCatageory(String subCatageory, String storeCode) {
        List<Rule> outList = null;
        if (null == subCatageory || StringUtils.isEmpty(subCatageory)) {
            outList = new ArrayList<Rule>();
            return outList;
        }

        List<PackmanRuleUnit> ruleList = fetchRuleUnit(PackmanRuleUnit.PackmanRuleUnitType.SUBCATEGORY_LEVEL_PACKAGING, PackmanRuleUnit.PackmanRuleUnitFieldName.STORECODE,
                storeCode, PackmanRuleUnit.PackmanRuleUnitFieldName.PARAM_VALUE, subCatageory);

        if (null == ruleList || ruleList.isEmpty()) {
            return new ArrayList<Rule>();
        }

        List<Rule> list = new ArrayList<Rule>();
        for (PackmanRuleUnit unit : ruleList) {
            Rule r = RuleEvaluationHelper.getRuleFromCocofsRuleUnit(unit);
            list.add(r);
        }
        return list;
    }

    private List<Rule> fetchRuleUnitsForStore(String storeCode) {
        List<Rule> outList = null;
        if (null == storeCode || StringUtils.isEmpty(storeCode)) {
            outList = new ArrayList<Rule>();
            return outList;
        }

        List<PackmanRuleUnit> ruleList = fetchRuleUnit(PackmanRuleUnit.PackmanRuleUnitType.STORE_LEVEL_PACKAGING, PackmanRuleUnit.PackmanRuleUnitFieldName.STORECODE, storeCode,
                null, null);
        
        if (null == ruleList || ruleList.isEmpty()) {

            return new ArrayList<Rule>();
        }

        List<Rule> list = new ArrayList<Rule>();
        for (PackmanRuleUnit unit : ruleList) {
            Rule r = RuleEvaluationHelper.getRuleFromCocofsRuleUnit(unit);
            list.add(r);
        }
        return list;
    }

    private List<PackmanRuleUnit> fetchRuleUnit(PackmanRuleUnit.PackmanRuleUnitType type, PackmanRuleUnit.PackmanRuleUnitFieldName fieldName, String value,
            PackmanRuleUnit.PackmanRuleUnitFieldName optionalFieldName, String optionalValue) {
        List<PackmanRuleUnit> outList = new ArrayList<PackmanRuleUnit>();
        List<PackmanRuleUnit> ruleList = null;

        List<Criteria> queryCriterias = new ArrayList<Criteria>();
        Criteria criteria1 = new Criteria(PackmanRuleUnitFieldName.TYPE.getName(), type.getCode(), Operator.EQUAL);
        Criteria criteria2 = new Criteria(fieldName.getName(), value, Operator.EQUAL);
        queryCriterias.add(criteria1);
        queryCriterias.add(criteria2);
        if (null != optionalFieldName && null != optionalValue) {
            Criteria criteria3 = new Criteria(optionalFieldName.getName(), optionalValue, Operator.EQUAL);
            queryCriterias.add(criteria3);
        }
        // This is to fetch only enabled rules from mongo
        Criteria criteria4 = new Criteria(PackmanRuleUnitFieldName.ENABLED.getName(), Boolean.TRUE, Operator.EQUAL);
        queryCriterias.add(criteria4);
        ruleList = genericMao.getDocumentList(new CocofsMongoQuery(queryCriterias), PackmanRuleUnit.class);


        if (null == ruleList) {
            ruleList = new ArrayList<PackmanRuleUnit>();
        }
        Date now = DateUtils.getCurrentTime();
        LOG.debug("Fetched " + ruleList.size() + " rules for packaging  with criteria " + queryCriterias);
        outList = RuleEvaluationHelper.getRuleValidForDate(ruleList, now);

        return outList;
    }

    private String evaluateRuleOnCatageory(List<Rule> rules, SUPCDetailDTO dto) {
        String packagingType = null;
        List<BlockParams> blockParams = ruleService.getBlockParamsByBlockName(BlockName.CategoryPackagingRuleBlock.getCode());
        CatageoryEvaluationParamDto paramDTO = new CatageoryEvaluationParamDto();
        //FIXME - any of the param can be null, other param must be evaluated
        if (null == dto.getBrand() && CollectionUtils.isEmpty(dto.getLabels())) {
            return evaluateRuleWithoutCriteria(rules);
        } else {
            paramDTO.setBrand(dto.getBrand());
            if (!CollectionUtils.isEmpty(dto.getLabels()) && dto.getLabels().size() > 0) {
                paramDTO.setLabel(dto.getLabels());
            }

        }
        Map<String, Object> ruleParam = populateRuleParamsFromBlockObject(paramDTO, blockParams);
        for (Entry<String, Object> e : ruleParam.entrySet()) {
            LOG.info("For Category Rules: Parameter " + e.getKey() + " Value " + e.getValue());
        }
        sortRuleBbyPriority(rules);
        for (Rule rule : rules) {
            List<Rule> tempList = new ArrayList<Rule>();
            tempList.add(rule);
            boolean isRuleSatisfied = evaluateRules(tempList, ruleParam);
            if (isRuleSatisfied) {
                LOG.info("Catageory Rule satisfied:" + rule.getName());
                packagingType = getParamReturnType(rule);
                break;
            }
        }

        return packagingType;

    }

    private String evaluateRuleOnSubCatageory(List<Rule> rules, SUPCDetailDTO dto) {
        String packagingType = null;

        List<BlockParams> blockParams =  ruleService.getBlockParamsByBlockName(BlockName.SubcategoryPackagingRuleBlock.getCode());

        SubCatagoryEvaluationParamDto paramDTO = new SubCatagoryEvaluationParamDto();
        if (null == dto.getBrand() && CollectionUtils.isEmpty(dto.getLabels())) {
            return evaluateRuleWithoutCriteria(rules);
        } else {
            paramDTO.setBrand(dto.getBrand());
            if (!CollectionUtils.isEmpty(dto.getLabels()) && dto.getLabels().size() > 0) {
                paramDTO.setLabel(dto.getLabels());
            }

        }

        Map<String, Object> ruleParam = populateRuleParamsFromBlockObject(paramDTO, blockParams);

        for (Entry<String, Object> e : ruleParam.entrySet()) {
            LOG.info("Subcateory Parameter " + e.getKey() + " Value " + e.getValue());
        }
        sortRuleBbyPriority(rules);
        for (Rule rule : rules) {
            List<Rule> tempList = new ArrayList<Rule>();
            tempList.add(rule);
            boolean isRuleSatisfied = evaluateRules(tempList, ruleParam);
            if (isRuleSatisfied) {
                packagingType = getParamReturnType(rule);
                LOG.info("Sub-catageory Rule satisfied:" + rule.getName());
                break;
            }
        }
        
        return packagingType;

    }
    private String evaluateRuleWithoutCriteria(List<Rule> rules) {
        sortRuleBbyPriority(rules);
        for (Rule rule : rules) {
            if (CollectionUtils.isEmpty(rule.getCriterion()))
            {
                return getParamReturnType(rule);
            }
        }
        return null;
    }

    private String evaluateStoreRule(List<Rule> rules) {
        return evaluateRuleWithoutCriteria(rules);
    }

    private String getParamReturnType(Rule r) {
        if (null == r.getRuleParams()) {
            return null;
        }
        for (RuleParams param : r.getRuleParams()) {
            if (PackagingRuleParam.RETURN_VALUE.getCode().equals(param.getParamName()))
            {
                 return param.getParamValue();
            }
        }
        return null;
    }
    
    private void prepareCompleteDTO(SUPCDetailDTO dto,
	    GetPOGDetailListResponse response) throws Exception {
	 // TODO set brand, catageory and other features from response
	 try {
	    for (POGDetailSRO sro : response.getPogDetailSROs()) {
	 for (ProductCategoryDetailSRO catSRO : sro.getCategories()) {
	    LOG.info("CAMS category response: {}", catSRO);

	    if (StringUtils.isEmpty(dto.getCatageory()))
	 dto.setCatageory(catSRO.getParentCategoryPageUrl());
	    if (StringUtils.isEmpty(dto.getSubCatageory()))
	 dto.setSubCatageory(catSRO.getPageUrl());
	 }
	 if (StringUtils.isEmpty(dto.getBrand()))
	    dto.setBrand(sro.getBrand().getName());

	 String filterName = ConfigUtils.getMapProperty(Property.PACKMAN_STORE_FRONT_TO_FILTER_VALUE_MAP, dto.getStoreFrontId());
	 List<String> labelList = exclusiveService.fetchLabelsForStore(dto.getStoreCode());
	 if (CollectionUtils.isEmpty(dto.getLabels())
	 && null != filterName
	 && !CollectionUtils.isEmpty(labelList)) {

	    for (ProductOfferDetailSRO offerDetailSRO : sro.getOffers()) {
	 for (FilterValueSRO filterSRO : offerDetailSRO
	 .getFilterValues())
	    // TODO need to read it from Property
	    if (filterName
	    .equalsIgnoreCase(filterSRO.getName())
	    && labelList.contains(filterSRO.getValue())) {
	 dto.getLabels().add(filterSRO.getValue());
	    }

	    }
	 }
	    }
	 } catch (Exception ex) {
	    LOG.info("SUPCDetailDto is not prepared. Exception : ", ex);
	    throw ex;
	 }

	 LOG.info("Prepared SUPCDetailedDto::" +dto.toString());
	    }

    private void sortRuleBbyPriority(List<Rule> rules) {
        Collections.sort(rules, new PriorityBasedComparator());
    }

    class PriorityBasedComparator implements Comparator<Rule> {
        @Override
        public int compare(Rule o1, Rule o2) {
            // TODO Auto-generated method stub
            return o2.getPriority() - o1.getPriority();
        }

    }
    public interface IDependency {
        String getStringScalar(Property property);
        
        boolean isEmpty(List<String> labels);

        List<String> getStringList(Property property);


        int getIntegerScalar(Property property);
    }

    static class Dependency implements IDependency {
        @Override
        public String getStringScalar(Property property) {
            return ConfigUtils.getStringScalar(property);
        }

        @Override
        public int getIntegerScalar(Property property) {
            return ConfigUtils.getIntegerScalar(property);
        }


        @Override
        public List<String> getStringList(Property property) {
            // TODO Auto-generated method stub
            return ConfigUtils.getStringList(property);
        }

        @Override
        public boolean isEmpty(List<String> labels) {
            // TODO Auto-generated method stub
            return CollectionUtils.isEmpty(labels);
        }
    }

}
