/**
 *  Copyright 2016 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.services.packaging.dto;

import java.util.ArrayList;
import java.util.List;

/**
 *  
 *  @version     1.0, 26-Feb-2016
 *  @author brijesh
 */
public class ShipTogetherSearchDTO {

    private String storeCode;
    
    private String packagingType;
    
    private List<ProductDetailDTO> productDetailList = new ArrayList<>();

    public ShipTogetherSearchDTO() {
    }

    public ShipTogetherSearchDTO(String storeCode,String packagingType) {
        this.storeCode = storeCode;
        this.packagingType = packagingType;
    }
    
    public String getStoreCode() {
        return storeCode;
    }
    
    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }

    public String getPackagingType() {
        return packagingType;
    }

    public void setPackagingType(String packagingType) {
        this.packagingType = packagingType;
    }

    public List<ProductDetailDTO> getProductDetailList() {
        return productDetailList;
    }

    public void setProductDetailList(List<ProductDetailDTO> productDetailList) {
        this.productDetailList = productDetailList;
    }

    @Override
    public String toString() {
        return "ShipTogetherSearchDTO [storeCode=" + storeCode + ", packagingType=" + packagingType + ", productDetailList=" + productDetailList + "]";
    }
    
}
