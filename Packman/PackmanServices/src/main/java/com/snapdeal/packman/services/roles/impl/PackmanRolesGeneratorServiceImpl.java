/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.services.roles.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.packman.dto.RoleDto;
import com.snapdeal.packman.dto.RoleStoreMappingDto;
import com.snapdeal.packman.dto.URLPatternDto;
import com.snapdeal.packman.enums.SystemProperty;
import com.snapdeal.packman.services.packaging.IPackmanExclusiveService;
import com.snapdeal.packman.services.roles.IPackmanRoleService;
import com.snapdeal.packman.services.roles.IPackmanRolesGeneratorService;
import com.snapdeal.packman.services.user.IUrlPatternMappingService;

/**
 *  
 *  @version     1.0, 08-Dec-2015
 *  @author indrajit
 */
@Service("packmanRolesGeneratorServiceImpl")
public class PackmanRolesGeneratorServiceImpl implements IPackmanRolesGeneratorService{
    
    private static final Logger       LOG           = LoggerFactory.getLogger(PackmanRolesGeneratorServiceImpl.class);
    
    @Autowired
    IUrlPatternMappingService urlPatternMappingService;
    
    @Autowired
    IPackmanRoleService roleService;
    
    @Autowired
    IPackmanExclusiveService packmanExclusiveService;
    
    

    @Override
    public void generateRolesAndUrlMapping(Integer storeId, String storeCode) throws Exception{

	String rolePrefix = ConfigUtils.getStringScalar(Property.PACKMAN_ROLE_PREFIX);
	Map<String,String> roleToGeneratedUrlMap = new HashMap<String, String>();
	Map<String, String> roleToGeneratedRolemap = new HashMap<String, String>();
	Map<String,List<String>> roleToUrlMap = new HashMap<String, List<String>>();
	try {
	    List<RoleDto> roleDtoList = createRoleDtoList(rolePrefix,storeCode, roleToGeneratedUrlMap, roleToGeneratedRolemap);
	    List<String> persistedRoleCodeList = roleService.persisteRole(roleDtoList);
	    List<RoleStoreMappingDto> roleStoreMappingList = createRoleStoreMappinglist(persistedRoleCodeList, storeCode);
	    packmanExclusiveService.saveRoleStoremapping(roleStoreMappingList);
	    for(Entry<String, String> entry : roleToGeneratedUrlMap.entrySet()){
	        String role = entry.getKey();
	        List<String> urlList = getUrlFromCSV(storeCode,entry.getValue());
	        roleToUrlMap.put(role, urlList);
	    }
	    createUrlandGeneratedRoleMapping(roleToUrlMap, roleToGeneratedRolemap);
	    
	} catch (Exception ex) {
	    LOG.error("Error has come while making dynamic roles " + ex);
	    throw ex;
	}

	// TODO Auto-generated method stub

    }
    
    private List<String> getUrlFromCSV(String storeCode,String url){
        List<String> list = new ArrayList<String>();
        if(url==null){
            return list;
        }
        String[] urlList = url.split(",");
        for(String u:urlList){
            list.add(prepareUrl(storeCode, u));
        }
        return list;
    }
    
    private List<RoleDto> createRoleDtoList(String rolePreFix, String storeCode, Map<String, String> roleToGeneratedUrlMap, Map<String, String> roleToGeneratedRolemap) {
	List<String> roleList = ConfigUtils.getStringList(Property.PACKMAN_DEFAULT_ROLE_LIST);
	Map<String,String> defaultRoleUrlmap = ConfigUtils.getMap(Property.PACKMAN_DEFAULT_ROLE_URLS_MAP);
	String defaultUrl = ConfigUtils.getStringScalar(Property.PACKMAN_DEFAULT_URL_MAIN);
	List<RoleDto> roleDtoList = new ArrayList<RoleDto>();
	String generatedRole = null;
	String generatedUrl  = null;

	for (String role : roleList) {
	    RoleDto dto = new RoleDto();
	    generatedRole = prepareRoleCode(role, rolePreFix, storeCode);
	    generatedUrl = defaultRoleUrlmap.get(role);
	    dto.setCode(generatedRole);	 
	    dto.setDefaultUrl(defaultUrl);
	    dto.setDescription(generatedRole);
	    roleToGeneratedUrlMap.put(role, generatedUrl);
	    roleToGeneratedRolemap.put(role, generatedRole);
	    dto.setEnabled(true);
	    roleDtoList.add(dto);
	    
	}

	return roleDtoList;

    }
    
    private List<RoleStoreMappingDto> createRoleStoreMappinglist(List<String> roleCodeList, String storeCode){
	List<RoleStoreMappingDto> finalList = new ArrayList<RoleStoreMappingDto>();
	for(String role : roleCodeList){
	    RoleStoreMappingDto dto = new RoleStoreMappingDto();
	    dto.setEnabled(true);
	    dto.setRoleCode(role);
	    dto.setStoreCode(storeCode);
	    dto.setUpdatedBy(System.getProperty(SystemProperty.HOST_NAME.getCode()));
	    finalList.add(dto);
	}
	return finalList;
    }
    private String prepareRoleCode(String baseRole, String prefix, String storeCode){
	return prefix+storeCode+"_"+baseRole;
    }
    
    private String prepareUrl(String storeCode, String url){
	String[] urls = url.split("/");
	StringBuilder finalUrl = new StringBuilder();
	for(String u : urls){
	    if("$STORE".equals(u)){
		finalUrl.append(storeCode+"/");
		continue;
	    }
	    finalUrl.append(u+"/");
	}
	return finalUrl.toString();
    }
    
   private void createUrlandGeneratedRoleMapping(Map<String, List<String>> roleToGeneratedUrlMap, Map<String, String> roleToGeneratedRolemap){
       Map<String, String> defaultRoleToRoleMapping = ConfigUtils.getMap(Property.PACKMAN_DEFAULT_ROLE_MAP);
       for(String role : defaultRoleToRoleMapping.keySet()){
       List<String> urlList = roleToGeneratedUrlMap.get(role);
       for(String url : urlList){
           URLPatternDto dto = new URLPatternDto();
           dto.setUrl(url);
           dto.setUrl_enable(true);
           dto.setRoleCodeList(getRoles(defaultRoleToRoleMapping.get(role), roleToGeneratedRolemap));
           dto.setRole_enable(true);
           urlPatternMappingService.saveUrlPatternMappingWithDto(dto);
       }
       }
   }
   
   private List<String> getRoles(String roles, Map<String, String> roleToGeneratedRoleMap){
       String[] roleTokens = roles.split(",");
       List<String> finalList = new ArrayList<String>();
       for(String str : roleTokens){
	   finalList.add(roleToGeneratedRoleMap.get(str));
       }
       return finalList;
   }
   

}
