/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.packman.services.packaging;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.snapdeal.packman.dto.RoleStoreMappingDto;
import com.snapdeal.packman.entity.PackagingType;
import com.snapdeal.packman.entity.PackagingTypeItem;
import com.snapdeal.packman.entity.PackagingTypeItemProperties;
import com.snapdeal.packman.entity.PackagingTypeProperties;
import com.snapdeal.packman.entity.Store;
import com.snapdeal.packman.entity.StoreProperties;
import com.snapdeal.packman.services.packaging.dto.SideBar;



/**
 * @version 1.0, 01-Dec-2015
 * @author indrajit
 */
public interface IPackmanExclusiveService {

    Store saveStore(Store store);

    Store createStore(Store store) throws Exception;

    List<Store> getAllStoreDetails();

    Store getStore(String code);

    void saveRoleStoremapping(List<RoleStoreMappingDto> dto);

    void savePackagingType(PackagingType packagingType);

    List<PackagingType> getAllPackagingType(String storeCode);

    PackagingType getPackagingType(String type, String storeCode);

    void savePackagingTypeItem(PackagingTypeItem packagingTypeItem,List<PackagingTypeItemProperties> properties);

    void validateSurfacePackagingType(PackagingTypeItem packagingTypeItem);

    PackagingTypeItem getPackagingTypeItem(String type, int packagingTypeId);

    List<PackagingTypeItem> getAllPackagingTypeItemDetails(String storeCode, String packagingTypeCode);

    void deletePackagingTypeItem(String type, int packagingTypeId);

    List<SideBar> getSideBar(String email, boolean specialUser);

    boolean getBooleanScalar(String input);

    String getStringScalar(String input);

    Double getDoubleScalar(String input);

    String getValueFromPackagingTypeProperty(PackagingTypeProperties packagingTypeProperties);

    String getPackagingTypeProperties(String name, PackagingType packagingType);

    Integer getIntegerScalar(String input);

    void updateStore(Store store, StoreProperties property);

    Store getStoreByStoreFrontId(String id);

    void createPackagingTypeItem(PackagingTypeItem packagingTypeItem, String user, String packagingTypeCode, String storeCode, List<PackagingTypeItemProperties> property);

    List<PackagingType> getAllEnabledPackagingType(String storeCode);

    void createPackagingType(PackagingType packagingType, List<PackagingTypeProperties> packagingTypeProperties, String user);

    void saveStoreProperty(StoreProperties property);

    void createStoreAndStoreProperties(Store store, StoreProperties property, String user) throws Exception;

    StoreProperties fetchStoreProperty(String storeCode, String name);

    void deleteStoreProperty(StoreProperties property);

    Map<String, String> getAllPackagingTypeProperties(PackagingType p);

    List<PackagingTypeProperties> getAllPackagingTypeProperty(PackagingType p);

    void updatePackagingType(PackagingType packagingType, List<PackagingTypeProperties> packagingTypeProperties, String packagingTypeCategory, String user);

    List<String> fetchLabelsForStore(String storeCode);

    Set<String> getAccessibleStoresByUserAndUserRole(String email, List<String> roles, boolean superUser);

    PackagingTypeItem getPackagingTypeItem(String type);

    void validateAirPackagingType(PackagingTypeItem packagingTypeItem);

    List<PackagingTypeItemProperties> getPackagingTypeItemProperties(PackagingTypeItem packagingTypeItem);

    Double getMaxVolumetricWeight(int packagingTypeId);
    
    List<String> getAllLabels(String storeCode);

    void reCalculateVolWt(String currentUserEmail);

}
