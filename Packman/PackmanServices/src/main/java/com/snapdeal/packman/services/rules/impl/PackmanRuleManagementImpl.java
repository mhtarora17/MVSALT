/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.services.rules.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.math.RandomUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.aerospike.client.Log;
import com.snapdeal.base.cache.CacheManager;
import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.cocofs.mongo.CocofsMongoQuery;
import com.snapdeal.cocofs.mongo.CocofsMongoQuery.Operator;
import com.snapdeal.cocofs.mongo.mao.IGenericMao;
import com.snapdeal.packman.dao.IPackmanExclusivelyDao;
import com.snapdeal.packman.entity.PackagingType;
import com.snapdeal.packman.enums.BlockName;
import com.snapdeal.packman.enums.PackagingRuleParam;
import com.snapdeal.packman.enums.PackagingTypeRuleCriteria;
import com.snapdeal.packman.enums.RulePriorityType;
import com.snapdeal.packman.mongo.model.CriteriaUnit;
import com.snapdeal.packman.mongo.model.PackmanRuleUnit;
import com.snapdeal.packman.mongo.model.PackmanRuleUnit.PackmanRuleUnitFieldName;
import com.snapdeal.packman.mongo.model.PackmanRuleUnit.PackmanRuleUnitType;
import com.snapdeal.packman.rule.dto.CategoryRuleCreateForm;
import com.snapdeal.packman.rule.dto.CategoryRuleCreationDTO;
import com.snapdeal.packman.rule.dto.StoreRuleCreateForm;
import com.snapdeal.packman.rule.dto.StoreRuleCreationDTO;
import com.snapdeal.packman.rule.dto.SubCategoryRuleCreateForm;
import com.snapdeal.packman.rule.dto.SubCategoryRuleCreationDTO;
import com.snapdeal.packman.services.rules.IPackmanRuleManagement;
import com.snapdeal.packman.services.rules.IPackmanRuleService;
import com.snapdeal.rule.cache.BlocksCache;
import com.snapdeal.rule.engine.entity.Criteria;
import com.snapdeal.rule.engine.entity.Operators;
import com.snapdeal.rule.engine.entity.Rule;
import com.snapdeal.rule.engine.entity.RuleParams;
import com.snapdeal.rule.engine.entity.ValueTypes;
import com.snapdeal.rule.engine.services.IRulesService;

/**
 *  
 *  @version     1.0, 22-Dec-2015
 *  @author brijesh
 */

@Service("packmanRuleManager")
public class PackmanRuleManagementImpl implements IPackmanRuleManagement {

    private static final Logger LOG = LoggerFactory.getLogger(PackmanRuleManagementImpl.class);

    @Autowired
    private IRulesService                      ruleService;

    @Autowired
    private IGenericMao                        genericMao;
    
    @Autowired
    private IPackmanRuleService                packmanRuleService;
    
    @Autowired
    private IPackmanExclusivelyDao             packmanExclusivelyDao;
    
    @Override
    public Object getRuleCreationDTO(String ruleType,String storeCode,String catorsubcat,String packagingType, String brands, String[] supercats,String user, String startDate, String endDate,boolean enabled) {
        
        StoreRuleCreationDTO dto = null;
        if(ruleType.equals(BlockName.StorePackagingRuleBlock.getCode())){
            dto = new StoreRuleCreationDTO();
        }
        else if(ruleType.equals(BlockName.CategoryPackagingRuleBlock.getCode())){
            dto = new CategoryRuleCreationDTO();
            CategoryRuleCreationDTO dtoSpecific = (CategoryRuleCreationDTO)dto;
            dtoSpecific.setBrand(brands);
            dtoSpecific.setSuperCat(convertToCsv(supercats));
            
            dtoSpecific.setCategory(catorsubcat);
        }
        else if(ruleType.equals(BlockName.SubcategoryPackagingRuleBlock.getCode())){
            dto = new SubCategoryRuleCreationDTO();
            SubCategoryRuleCreationDTO dtoSpecific = (SubCategoryRuleCreationDTO)dto;
            dtoSpecific.setBrand(brands);
            dtoSpecific.setSuperCat(convertToCsv(supercats));
            dtoSpecific.setSubCategory(catorsubcat);
        }
        
        dto.setPriority(getRulePriority(dto));
        Date date = getDateFromString(endDate);
        if (null != date) {
            dto.setEndDate(date);
        } else {
            dto.setEndDate(null);
        }
        
        dto.setBlock(CacheManager.getInstance().getCache(BlocksCache.class).getBlockIDByName(ruleType));
        dto.setRuleType(ruleType);
        dto.setStoreCode(storeCode);
        dto.setReturnValue(packagingType);
        dto.setUpdatedBy(user);
        dto.setStartDate(getDateFromString(startDate));
        dto.setUpdateTime(DateUtils.getCurrentTime());
        dto.setEnabled(enabled);
        
        Log.info(dto.toString());
        return dto;
    }

    private String convertToCsv(String[] brands) {
        StringBuilder sbBrand = null;
        if(brands != null){
            sbBrand = new StringBuilder();
            for(String brand : brands) {
                sbBrand.append(brand).append(",");
            }
        }
        if (sbBrand != null && sbBrand.length() > 0) {
            return sbBrand.substring(0, sbBrand.length() - 1);
        }
        return sbBrand != null ? sbBrand.toString() : null;
    }

    private Date getDateFromString(String enddate) {
        if (StringUtils.isEmpty(enddate)) {
            return null;
        }
        return DateUtils.stringToDate(enddate, "yyyy-MM-dd HH:mm:ss");
    }
    
    private String getRuleCodeForCategoryAndType(String subtype, String type) {
        return type + "|" + subtype;
    }
    
    private Set<RuleParams> getRuleParamFromRuleCreationDTO(Object dto, Rule r) {
        
        Set<RuleParams> params = new HashSet<>();
        StoreRuleCreationDTO storeRuleCreationDTO = (StoreRuleCreationDTO)dto;
        
        RuleParams p1 = new RuleParams();
        p1.setParamValue(storeRuleCreationDTO.getReturnValue());
        p1.setParamName(PackagingRuleParam.RETURN_VALUE.getCode());
        p1.setRule(r);
        params.add(p1);
        
        RuleParams p2 = new RuleParams();
        p2.setParamValue(storeRuleCreationDTO.getStoreCode());
        p2.setParamName(PackagingRuleParam.STORECODE.getCode());
        p2.setRule(r);
        params.add(p2);
        
        if(dto instanceof CategoryRuleCreationDTO){
            RuleParams p3 = new RuleParams();
            p3.setParamValue(((CategoryRuleCreationDTO)dto).getCategory());
            p3.setParamName(PackagingRuleParam.CATEGORY.getCode());
            p3.setRule(r);
            params.add(p3);
        }
        
        if(dto instanceof SubCategoryRuleCreationDTO){
            RuleParams p4 = new RuleParams();
            p4.setParamValue(((SubCategoryRuleCreationDTO)dto).getSubCategory());
            p4.setParamName(PackagingRuleParam.SUBCATEGORY.getCode());
            p4.setRule(r);
            params.add(p4);
        }

        return params;
    }

    private PackmanRuleUnit getRuleUnitFromRuleCreationDTO(Object dto, String ruleName) {
        PackmanRuleUnit unit = new PackmanRuleUnit();
       
        StoreRuleCreationDTO storeRuleCreationDTO = (StoreRuleCreationDTO)dto;
        unit.setType(PackmanRuleUnitType.STORE_LEVEL_PACKAGING.getCode());
        unit.setRuleCode(getRuleCodeForCategoryAndType(storeRuleCreationDTO.getStoreCode(), PackmanRuleUnitType.STORE_LEVEL_PACKAGING.getCode()));
        unit.setParamValue(storeRuleCreationDTO.getStoreCode());
        
        if(dto instanceof CategoryRuleCreationDTO){
            unit.setType(PackmanRuleUnitType.CATEGORY_LEVEL_PACKAGING.getCode());
            unit.setRuleCode(getRuleCodeForCategoryAndType(((CategoryRuleCreationDTO)dto).getCategory(), PackmanRuleUnitType.CATEGORY_LEVEL_PACKAGING.getCode()));
            unit.setParamValue(((CategoryRuleCreationDTO)dto).getCategory());
        }
        
        if(dto instanceof SubCategoryRuleCreationDTO){
            unit.setType(PackmanRuleUnitType.SUBCATEGORY_LEVEL_PACKAGING.getCode());
            unit.setRuleCode(getRuleCodeForCategoryAndType(((SubCategoryRuleCreationDTO)dto).getSubCategory(), PackmanRuleUnitType.SUBCATEGORY_LEVEL_PACKAGING.getCode()));
            unit.setParamValue(((SubCategoryRuleCreationDTO)dto).getSubCategory());
        }
        
        List<CriteriaUnit> criteria = getRuleCriteriaUnitFromRuleCreationDTO(dto);
        unit.setCriteria(criteria);
        unit.setReturnValue(storeRuleCreationDTO.getReturnValue());
        unit.setCreated(storeRuleCreationDTO.getUpdateTime());
        unit.setEndDate(storeRuleCreationDTO.getEndDate());
        unit.setStoreCode(storeRuleCreationDTO.getStoreCode());
        unit.setName(ruleName);
        unit.setPriority(storeRuleCreationDTO.getPriority());
        unit.setStartDate(storeRuleCreationDTO.getStartDate());
        unit.setUpdated(storeRuleCreationDTO.getUpdateTime());
        unit.setEnabled(storeRuleCreationDTO.isEnabled());
        unit.setId(RandomUtils.nextInt());
        unit.setCreatedBy(storeRuleCreationDTO.getUpdatedBy());
        unit.setUpdatedby(storeRuleCreationDTO.getUpdatedBy());
        return unit;
    }
    
    @Override
    @Transactional
    public Rule createRule(StoreRuleCreationDTO dto) {
        String ruleName = "R"+DateUtils.dateToString(DateUtils.getCurrentTime(), "yyyyMMddHHmmssSSS")+"-"+StringUtils.getRandomAlphaNumeric(4);
        
        PackmanRuleUnit unit = getRuleUnitFromRuleCreationDTO(dto, ruleName);
        Rule entity = getRuleFromRuleCreationDTO(dto, ruleName );
        
        ruleService.createRule(entity);
        genericMao.saveDocument(unit);
        ruleService.updateRuleWithLog(entity);
        return entity;
    }

    private List<CriteriaUnit> getRuleCriteriaUnitFromRuleCreationDTO(Object dto) {
        List<CriteriaUnit> list = new ArrayList<>();
        boolean cat=false,subcat=false;
        if(dto instanceof CategoryRuleCreationDTO)
            cat = true;
        if(dto instanceof SubCategoryRuleCreationDTO)
            subcat = true;
        if( (cat && null != ((CategoryRuleCreationDTO)dto).getBrand()) ||
            (subcat && null != ((SubCategoryRuleCreationDTO)dto).getBrand())){
            CriteriaUnit unit = new CriteriaUnit();
            unit.setCreated(DateUtils.getCurrentTime());
            unit.setEnabled(true);
            unit.setName(PackagingTypeRuleCriteria.BRAND.getName());
            unit.setOperator(Operators.IN.getCode());
            unit.setUpdated(((StoreRuleCreationDTO)dto).getUpdateTime());
            if(cat)
                unit.setValue(((CategoryRuleCreationDTO)dto).getBrand());
            if(subcat)
                unit.setValue(((SubCategoryRuleCreationDTO)dto).getBrand());
            unit.setValueType(ValueTypes.STRING.name());
            unit.setId(RandomUtils.nextInt());
            list.add(unit);
        }
        
        if((cat && null != ((CategoryRuleCreationDTO)dto).getSuperCat()) ||
                (subcat && null != ((SubCategoryRuleCreationDTO)dto).getSuperCat())){
            CriteriaUnit unit = new CriteriaUnit();
            unit.setCreated(DateUtils.getCurrentTime());
            unit.setEnabled(true);
            unit.setName(PackagingTypeRuleCriteria.LABEL.getName());
            unit.setOperator(Operators.ANY_MATCHES.getCode());
            unit.setUpdated(((StoreRuleCreationDTO)dto).getUpdateTime());
            if(cat)
                unit.setValue(((CategoryRuleCreationDTO)dto).getSuperCat());
            if(subcat)
                unit.setValue(((SubCategoryRuleCreationDTO)dto).getSuperCat());
            unit.setValueType(ValueTypes.STRING.name());
            unit.setId(RandomUtils.nextInt());
            list.add(unit);
        }
        return list;
    }

    private Set<Criteria> getRuleCriteriaFromRuleCreationDTO(StoreRuleCreationDTO dto) {
        Set<Criteria> criteria = new HashSet<>();
        boolean cat=false, subcat=false;
        if(dto instanceof CategoryRuleCreationDTO)
            cat = true;
        if(dto instanceof SubCategoryRuleCreationDTO)
            subcat = true;
        if((cat && null != ((CategoryRuleCreationDTO)dto).getBrand()) ||
            (subcat && null != ((SubCategoryRuleCreationDTO)dto).getBrand())){
            Criteria crit = new Criteria();
            crit.setCreated(DateUtils.getCurrentTime());
            crit.setEnabled(true);
            crit.setName(PackagingTypeRuleCriteria.BRAND.getName());
            crit.setOperator(Operators.IN);
            if(cat)
                crit.setValue(((CategoryRuleCreationDTO)dto).getBrand());
            if(subcat)
                crit.setValue(((SubCategoryRuleCreationDTO)dto).getBrand());
            crit.setValueType(ValueTypes.STRING);
            criteria.add(crit);
        }
        
        if((cat && null != ((CategoryRuleCreationDTO)dto).getSuperCat()) ||
                (subcat && null != ((SubCategoryRuleCreationDTO)dto).getSuperCat())){
            Criteria crit = new Criteria();
            crit.setCreated(DateUtils.getCurrentTime());
            crit.setEnabled(true);
            crit.setName(PackagingTypeRuleCriteria.LABEL.getName());
            crit.setOperator(Operators.ANY_MATCHES);
            if(cat)
                crit.setValue(((CategoryRuleCreationDTO)dto).getSuperCat());
            if(subcat)
                crit.setValue(((SubCategoryRuleCreationDTO)dto).getSuperCat());
            crit.setValueType(ValueTypes.STRING);
            criteria.add(crit);
        }
        
        return criteria;
    }
    
    private Rule getRuleFromRuleCreationDTO(StoreRuleCreationDTO dto, String ruleName) {
        Rule r = new Rule();
        r.setBlockId(CacheManager.getInstance().getCache(BlocksCache.class).getBlockIDByName(dto.getRuleType()));
        r.setCreated(dto.getUpdateTime());
        r.setCreatedBy(dto.getUpdatedBy());
        r.setEnabled(dto.isEnabled());
        r.setEndDate(dto.getEndDate());
        r.setName(ruleName);
        r.setPriority(dto.getPriority());
        r.setStartDate(dto.getStartDate());
        r.setUpdated(dto.getUpdateTime());
        r.setUpdatedBy(dto.getUpdatedBy());
        r.setSynched(true);
        
        Set<Criteria> criterias = getRuleCriteriaFromRuleCreationDTO(dto);
        r.setCriterion(criterias);
        Set<RuleParams> ruleParams = getRuleParamFromRuleCreationDTO(dto, r);
        r.setRuleParams(ruleParams);
        
        if(dto instanceof CategoryRuleCreationDTO){
            CategoryRuleCreationDTO categoryRuleCreationDTO = (CategoryRuleCreationDTO)dto;
            r.setCode(getRuleCodeForCategoryAndType(categoryRuleCreationDTO.getCategory(), PackmanRuleUnitType.CATEGORY_LEVEL_PACKAGING.getCode()));
            r.setRemark("Creating rule for " + categoryRuleCreationDTO.getRuleType() + " and " + categoryRuleCreationDTO.getCategory() + " by user " + categoryRuleCreationDTO.getUpdatedBy());
        }
        else if(dto instanceof SubCategoryRuleCreationDTO){
            SubCategoryRuleCreationDTO subCategoryRuleCreationDTO = (SubCategoryRuleCreationDTO)dto;
            r.setCode(getRuleCodeForCategoryAndType(subCategoryRuleCreationDTO.getSubCategory(), PackmanRuleUnitType.SUBCATEGORY_LEVEL_PACKAGING.getCode()));
            r.setRemark("Creating rule for " + subCategoryRuleCreationDTO.getRuleType() + " and " + subCategoryRuleCreationDTO.getSubCategory() + " by user " + subCategoryRuleCreationDTO.getUpdatedBy());
        }
        else {
            r.setCode(getRuleCodeForCategoryAndType(dto.getStoreCode(), PackmanRuleUnitType.STORE_LEVEL_PACKAGING.getCode()));
            r.setRemark("Creating rule for " + dto.getRuleType() + " and " + dto.getStoreCode() + " by user " + dto.getUpdatedBy());
        }
        
        return r;
    }


    
    private Integer getRulePriority(StoreRuleCreationDTO dto) {
        String brands,superCats;
        if(dto instanceof CategoryRuleCreationDTO){
            brands = ((CategoryRuleCreationDTO)dto).getBrand();
            superCats = ((CategoryRuleCreationDTO)dto).getSuperCat();
            if(brands==null && superCats==null)
                return RulePriorityType.CATEGORY.getPriority();
            else if(brands!=null && superCats==null)
                return RulePriorityType.CATEGORY_BRAND.getPriority();
            else if(brands==null)
                return RulePriorityType.CATEGORY_SUPERCAT.getPriority();
            else
                return RulePriorityType.CATEGORY_BRAND_SUPERCAT.getPriority();
        }
        if(dto instanceof SubCategoryRuleCreationDTO){
            brands = ((SubCategoryRuleCreationDTO)dto).getBrand();
            superCats = ((SubCategoryRuleCreationDTO)dto).getSuperCat();
            if(brands==null && superCats==null)
                return RulePriorityType.SUBCAT.getPriority();
            else if(brands!=null && superCats==null)
                return RulePriorityType.SUBCAT_BRAND.getPriority();
            else if(brands==null)
                return RulePriorityType.SUBCAT_SUPERCAT.getPriority();
            else
                return RulePriorityType.SUBCAT_BRAND_SUPERCAT.getPriority();
        }
        return RulePriorityType.STORE.getPriority();
    }
    
    @Override
    @Transactional
    public void updateRule(StoreRuleCreationDTO dto) throws Exception {
        String storeCode = dto.getStoreCode();
        List<Rule> rules = packmanRuleService.getRuleFromName(dto.getRuleName(), PackagingRuleParam.STORECODE.getCode(), storeCode);
        Rule previousRule;
        if(rules!=null){
            if(rules.size()==0){
                throw new Exception("No rules are found for provided rule name : "+dto.getRuleName());
            }
            if(rules.size()>1){
                throw new Exception("Multiple rules are found for provided rule name. Cancelling update for rule : "+dto.getRuleName());
            }
            Rule r = rules.get(0);
            previousRule = r;

            String packagingTypeCode = dto.getReturnValue();
            PackagingType p = packmanExclusivelyDao.getPackagingType(storeCode, packagingTypeCode);
            
            if(p==null){
                throw new IllegalArgumentException("Could not find packaging type ["+packagingTypeCode+"] under store ["+storeCode+"] is disabled");
            }
            
            if(!p.isEnabled()){
                throw new IllegalArgumentException("Could not update rule since packaging type ["+p.getType()+"] for store ["+storeCode+"] is disabled");
            }
            
            r.setUpdatedBy(dto.getUpdatedBy());
            r.setUpdated(DateUtils.getCurrentTime());
            r.setStartDate(dto.getStartDate());
            r.setEndDate(dto.getEndDate());
            r.setEnabled(dto.isEnabled());
            r.setPriority(dto.getPriority());
            
            List<com.snapdeal.cocofs.mongo.Criteria> crit = new ArrayList<>();
            com.snapdeal.cocofs.mongo.Criteria criteria1 = new com.snapdeal.cocofs.mongo.Criteria(PackmanRuleUnitFieldName.NAME.getName(), r.getName() , Operator.EQUAL);
            crit.add(criteria1);
            CocofsMongoQuery query = new CocofsMongoQuery(crit);
            
            PackmanRuleUnit document = genericMao.getDocument(query, PackmanRuleUnit.class);
            
            if(null == document)
                throw new Exception("Unable to update rule : mongo and mysql are not in sync");
            
            document.setUpdated(DateUtils.getCurrentTime());
            document.setStartDate(dto.getStartDate());
            document.setEndDate(dto.getEndDate());
            document.setEnabled(dto.isEnabled());
            document.setPriority(dto.getPriority());
            document.setReturnValue(packagingTypeCode);
            document.setUpdatedby(dto.getUpdatedBy());

            // Set Rule Params
            Set<RuleParams> ruleParams = r.getRuleParams();
            for(RuleParams rp : ruleParams){
                String name = rp.getParamName();
                if(name.equals(PackagingRuleParam.RETURN_VALUE.getCode())){
                    rp.setParamValue(packagingTypeCode);
                    rp.setUpdated(DateUtils.getCurrentTime());
                }
            }
            
            r.setRuleParams(ruleParams);
            
            // Set Criteria In Db
            Set<Criteria> criteria = r.getCriterion();
            Set<Criteria> criteriaUpdated = new HashSet<>();
            Criteria cBrand = null, cSuperCat = null;
            for(Criteria c : criteria){
                String name = c.getName();
                if(name.equals(PackagingTypeRuleCriteria.BRAND.getName())){
                    cBrand = c;
                }
                if(name.equals(PackagingTypeRuleCriteria.LABEL.getName())){
                    cSuperCat = c;
                }
            }
            
            boolean typecat = dto instanceof CategoryRuleCreationDTO;
            boolean typesubcat = dto instanceof SubCategoryRuleCreationDTO;
            if(typecat || typesubcat){
                CategoryRuleCreationDTO temp =null;
                SubCategoryRuleCreationDTO temp1 =null;
                if(typecat)
                    temp = (CategoryRuleCreationDTO) dto;
                if(typesubcat)
                    temp1 = (SubCategoryRuleCreationDTO) dto;
                if((temp!=null && temp.getBrand()==null) || 
                    (temp1!=null && temp1.getBrand()==null)){
                    if(cBrand!=null){
                        // Delete from db
                        ruleService.deleteCriteriaById(cBrand.getId());
                        LOG.info("Deleting criteria brand from mysql");
                    } 
                } else {
                    if(cBrand==null){
                        // Create new criteria
                        Criteria c = new Criteria();
                        c.setCreated(DateUtils.getCurrentTime());
                        c.setEnabled(true);
                        c.setName(PackagingTypeRuleCriteria.BRAND.getName());
                        c.setOperator(Operators.IN);
                        c.setRule(r);
                        if(typecat){//TODO
                            c.setValue(temp.getBrand());
                        }else{                        
                            c.setValue(temp1.getBrand());
                        }
                        c.setValueType(ValueTypes.STRING);
                        criteriaUpdated.add(c);
                        LOG.info("Adding new criteria brand into mysql");
                    } else {
                        // Update Criteria
                        if(typecat){
                            cBrand.setValue(temp.getBrand());
                        }
                        else{
                            cBrand.setValue(temp1.getBrand());
                        }
                        criteriaUpdated.add(cBrand);
                        LOG.info("Updating criteria brand into mysql");
                    }
                }
                
                if((temp!=null && temp.getSuperCat()==null) ||
                    (temp1!=null && temp1.getSuperCat()==null)){
                    if(cSuperCat!=null){
                        // Delete from Db
                        ruleService.deleteCriteriaById(cSuperCat.getId());
                        LOG.info("Deleting criteria supercat from mysql");
                    }
                } else {
                    if(cSuperCat==null){
                        // Create new criteria
                        Criteria c = new Criteria();
                        c.setCreated(DateUtils.getCurrentTime());
                        c.setEnabled(true);
                        c.setName(PackagingTypeRuleCriteria.LABEL.getName());
                        c.setOperator(Operators.ANY_MATCHES);
                        c.setRule(r);
                        if(typecat){
                            c.setValue(temp.getSuperCat());
                        }
                        else{
                            c.setValue(temp1.getSuperCat());
                        }
                        c.setValueType(ValueTypes.STRING);
                        criteriaUpdated.add(c);
                        LOG.info("Adding new criteria supercat into mysql");
                    } else {
                        // update criteria
                        if(typecat){
                            cSuperCat.setValue(temp.getSuperCat());
                        }
                        else {
                            cSuperCat.setValue(temp1.getSuperCat());
                        }
                        criteriaUpdated.add(cSuperCat);
                        LOG.info("Updating criteria supercat into mysql");
                    }
                }
                
                // update Criteria in mongo
                List<CriteriaUnit> unit = getRuleCriteriaUnitFromRuleCreationDTO(dto);
                
                document.setCriteria(unit);
                
                for(Criteria c : criteriaUpdated){
                    c.setUpdated(DateUtils.getCurrentTime());
                }
                
                r.setCriterion(criteriaUpdated);
            }
            
            ruleService.updateRule(r);
            LOG.info("Rule updated successfully in Mysql");
            genericMao.saveDocument(document);
            LOG.info("Rule updated successfully in mongo");
            ruleService.updateRuleWithLog(previousRule);
            LOG.info("Modified rule is logged");
        }
    }

    private void enrichDtoFromRule(Rule r, Object dto) {
        Set<Criteria> crit = r.getCriterion();
        for (Criteria c : crit) {
            if (c.getName().equals(PackagingTypeRuleCriteria.BRAND.name())) {
                String brands = c.getValue();
                if (null != brands) {
                    if (dto instanceof SubCategoryRuleCreateForm)
                        ((SubCategoryRuleCreateForm) dto).setBrand(brands);
                    if (dto instanceof CategoryRuleCreateForm)
                        ((CategoryRuleCreateForm) dto).setBrand(brands);
                }
            }
            if (c.getName().equals(PackagingTypeRuleCriteria.LABEL.name())) {
                String supercats = c.getValue();
                if (null != supercats) {
                    if (dto instanceof SubCategoryRuleCreateForm)
                        ((SubCategoryRuleCreateForm) dto).setSupercat(supercats.split(","));
                    if (dto instanceof CategoryRuleCreateForm)
                        ((CategoryRuleCreateForm) dto).setSupercat(supercats.split(","));
                }
            }
        }
        Set<RuleParams> ruleParams = r.getRuleParams();
        for (RuleParams rp : ruleParams) {
            String paramName = rp.getParamName();
            String paramValue = rp.getParamValue();
            if (paramName.equals(PackagingRuleParam.STORECODE.getCode())) {
                ((StoreRuleCreateForm) dto).setStoreCode(paramValue);
            }
            if (paramName.equals(PackagingRuleParam.CATEGORY.getCode())) {
                if (dto instanceof CategoryRuleCreateForm)
                    ((CategoryRuleCreateForm) dto).setCategory(paramValue);
            }
            if (paramName.equals(PackagingRuleParam.SUBCATEGORY.getCode())) {
                if (dto instanceof SubCategoryRuleCreateForm)
                    ((SubCategoryRuleCreateForm) dto).setSubcategory(paramValue);
            }
            if (paramName.equals(PackagingRuleParam.RETURN_VALUE.getCode())) {
                ((StoreRuleCreateForm) dto).setPackagingType(paramValue);
            }

        }
    }

    @Override
    public StoreRuleCreateForm getStoreCreateRuleDTO(Rule r, StoreRuleCreateForm dto) {
        enrichDtoFromRule(r, dto);
        dto.setEnabled(r.isEnabled());
        if (null != r.getStartDate())
            dto.setStartDate(getDate(r.getStartDate()));
        if (null != r.getEndDate())
            dto.setEndDate(getDate(r.getEndDate()));
        dto.setRuleName(r.getName());
        return dto;
    }
    
    private String getDate(Date d) {
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdfDate.format(d);
    }
    
    @Override
    public CategoryRuleCreateForm getCategoryCreateRuleDTO(Rule r, CategoryRuleCreateForm dto) {
        enrichDtoFromRule(r, dto);
        getStoreCreateRuleDTO(r, dto);
        return dto;
    }
    
    @Override
    public SubCategoryRuleCreateForm getSubCategoryCreateRuleDTO(Rule r, SubCategoryRuleCreateForm dto) {
        enrichDtoFromRule(r, dto);
        getStoreCreateRuleDTO(r, dto);
        return dto;
    }
    
    @Override
    public void enrichDto(StoreRuleCreateForm dto) {
        boolean cat = false, subcat = false;
        if (dto instanceof CategoryRuleCreateForm)
            cat = true;
        if (dto instanceof SubCategoryRuleCreateForm)
            subcat = true;
        List<Rule> rule = null;
        if (cat){
            rule = packmanRuleService.getRuleFromName(dto.getRuleName(), PackagingRuleParam.STORECODE.getCode(),
                    dto.getStoreCode());
        }
        if (subcat){
            rule = packmanRuleService.getRuleFromName(dto.getRuleName(), PackagingRuleParam.STORECODE.getCode(),
                    dto.getStoreCode());
        }
        if (null != rule) {
            Rule r = rule.get(0);
            Set<Criteria> crit = r.getCriterion();
            String brands = null;
            String[] supercats = null;
            for (Criteria c : crit) {
                if (c.getName().equals(PackagingTypeRuleCriteria.BRAND.getName()))
                    brands = c.getValue();
                if (c.getName().equals(PackagingTypeRuleCriteria.LABEL.getName()))
                    supercats = c.getValue().split(",");
            }
            if (cat) {
                ((CategoryRuleCreateForm) dto).setBrand(brands);
                ((CategoryRuleCreateForm) dto).setSupercat(supercats);
            }
            if (subcat) {
                ((SubCategoryRuleCreateForm) dto).setBrand(brands);
                ((SubCategoryRuleCreateForm) dto).setSupercat(supercats);
            }
        }
    }
}
