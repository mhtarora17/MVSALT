/**
 *  Copyright 2016 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.packman.services.packaging.recomendation.impl;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

import org.springframework.util.CollectionUtils;

/**
 * @version 1.0, 26-Feb-2016
 * @author brijesh
 */
public class ProductMinDetailDTO implements Comparator<ProductMinDetailDTO>, Comparable<ProductMinDetailDTO> {

    private Double[] productDimensions = new Double[3];
    
    public ProductMinDetailDTO() {
    }
    
/*
    public ProductMinDetailDTO(Double length, Double breadth, Double height) {
        // Sorts internally. So setting index 1 last is mandatory
        this.productDimensions[0] = Math.max(Math.max(length, breadth), height);
        this.productDimensions[2] = Math.min(Math.min(length, breadth), height);
        this.productDimensions[1] = Double.longBitsToDouble(Double.doubleToRawLongBits(length) ^ Double.doubleToRawLongBits(breadth) ^ Double.doubleToRawLongBits(height)
                ^ Double.doubleToRawLongBits(productDimensions[0]) ^ Double.doubleToRawLongBits(productDimensions[2]));
    }
*/
    
    public ProductMinDetailDTO(Double length, Double breadth, Double height) {
        this.productDimensions[0] = length;
        this.productDimensions[1] = breadth;
        this.productDimensions[2] = height;
        Arrays.sort(productDimensions,Collections.reverseOrder());
    }
    
    public Double[] getProductDimensions() {
        return productDimensions;
    }

    public void setProductDimensions(Double[] productDimensions) {
        this.productDimensions = productDimensions;
    }

    @Override
    public int compareTo(ProductMinDetailDTO object) {
        return compare(this, object);
    }

    @Override
    public int compare(ProductMinDetailDTO object1, ProductMinDetailDTO object2) {
        Double volWt1 = object1.getProductDimensions()[0] * object1.getProductDimensions()[1] * object1.getProductDimensions()[2];
        Double volWt2 = object2.getProductDimensions()[0] * object2.getProductDimensions()[1] * object2.getProductDimensions()[2];
        if (volWt1 == volWt2) {
            return 0;
        } else if (volWt1 > volWt2) {
            return 1;
        }
        return -1;
    }

    @Override
    public String toString() {
        return "ProductMinDetailDTO [productDimensions=" + Arrays.toString(productDimensions) + "]";
    }

}
