/**

 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.packman.services.external.impl;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.access.IProductAttributeService;
import com.snapdeal.cocofs.model.request.GetProductFulfilmentAttributeRequest;
import com.snapdeal.cocofs.mongo.model.ProductAttributeUnit;
import com.snapdeal.cocofs.services.converter.impl.ConverterServiceImpl;
import com.snapdeal.cocofs.sro.ProductFulfilmentAttributeSRO;
import com.snapdeal.cocofs.utils.UtilHelper;
import com.snapdeal.packman.services.external.ICocofsExternalService;

/**
 * @version 1.0, 07-Sep-2015
 * @author ankur
 */

@Service("cocofsExternalService")
public class CocofsExternalServiceImpl implements ICocofsExternalService {

    private static final Logger      LOG = LoggerFactory.getLogger(CocofsExternalServiceImpl.class);

    @Autowired
    private IProductAttributeService productAttributeService;

    @Override
    public ProductFulfilmentAttributeSRO getProductFulfilmentAttributes(GetProductFulfilmentAttributeRequest request) {
        ProductFulfilmentAttributeSRO paSro = null;
        LOG.info("getting product attributes in Cocofs for request {}", request);
        try {
            ProductAttributeUnit productAttributeUnit = productAttributeService.getProductAttributeUnitBySUPC(request.getSupc());
            LOG.info("Response {}", productAttributeUnit);
            paSro = ConverterServiceImpl.getProductAttributeSro(productAttributeUnit);//TODO  it is not require while separating packman.
        } catch (Exception ex) {
            LOG.error("Unable to get product attributes from Cocofs for request {}. Exception {}", request, ex);
        }

        return paSro;
    }

    @Override
    public double getVolumetricWeightFromLBH(double l, double b, double h) {
        return UtilHelper.getVolumetricWeightFromLBH(l, b, h); //TODO use CocofsClient with packman separation.
    }
}
