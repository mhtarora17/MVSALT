/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.packman.services.packaging.recomendation.impl;

import static com.snapdeal.packman.common.ApiErrorCode.EXTERNAL_SERVICE_CALL_FAILED;
import static com.snapdeal.packman.common.ApiErrorCode.INVALID_STORE_FRONT_ID;
import static com.snapdeal.packman.common.ApiErrorCode.INVALID_SUPC_CODE;
import static com.snapdeal.packman.common.ApiErrorCode.PACKAGING_TYPE_NOT_FOUND;
import static com.snapdeal.packman.common.ApiErrorCode.PRIMARY_LBH_NOT_AVAILABLE;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ExecutorService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.snapdeal.base.cache.CacheManager;
import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.model.request.GetProductFulfilmentAttributeRequest;
import com.snapdeal.cocofs.sro.ProductFulfilmentAttributeSRO;
import com.snapdeal.packman.cache.PackagingTypeCache;
import com.snapdeal.packman.cache.StoreFrontToStoreCodeMapCache;
import com.snapdeal.packman.common.sro.PackagableItemSRO;
import com.snapdeal.packman.common.sro.PackageDetailSRO;
import com.snapdeal.packman.common.sro.PackagingRecommendationSRO;
import com.snapdeal.packman.common.sro.PackagingTypeItemSRO;
import com.snapdeal.packman.common.sro.PackagingTypeSRO;
import com.snapdeal.packman.common.sro.SupcQuantitySRO;
import com.snapdeal.packman.dto.PackagingTypeCacheDTO;
import com.snapdeal.packman.dto.PackagingTypeItemDTO;
import com.snapdeal.packman.dto.RecommendationParamsDTO;
import com.snapdeal.packman.enums.PackagingMode;
import com.snapdeal.packman.services.exception.PackagingRecomendationException;
import com.snapdeal.packman.services.exception.PackagingTypeNotFoundException;
import com.snapdeal.packman.services.external.ICocofsExternalService;
import com.snapdeal.packman.services.packaging.IPackagingRecommendation;
import com.snapdeal.packman.services.packaging.dto.ProductDetailDTO;
import com.snapdeal.packman.services.packaging.dto.SUPCDetailDTO;
import com.snapdeal.packman.services.packaging.dto.ShipTogetherSearchDTO;
import com.snapdeal.packman.services.packaging.recomendation.IPackagingRecommendationService;
import com.snapdeal.packman.services.packaging.recomendation.IShipTogetherService;
import com.snapdeal.packman.services.packaging.rule.engine.IPackmanPackagingRuleEngineService;

/**
 * @version 1.0, 24-Dec-2015
 * @author indrajit
 */
@Service("packagingRecomendationService")
public class PackagingRecomendationService implements IPackagingRecommendationService {

    private static final Logger        LOG = LoggerFactory.getLogger(PackagingRecomendationService.class);

    @Autowired
    IPackmanPackagingRuleEngineService ruleEngineService;

    @Autowired
    ICocofsExternalService             cocofsExternalService;

    @Autowired
    IShipTogetherService               shipTogetherService;

    IPackagingRecommendation           packmanOldService;

    private IDependency                dep = new Dependency();

    @Override
    public List<PackagingRecommendationSRO> getRecommendation(String storeFrontId, Boolean isShipTogether, PackageDetailSRO packageDetail) throws PackagingRecomendationException {

        String storeCode = getStoreCodeFromStoreFrontId(storeFrontId);
        if (null == storeCode) {
            LOG.error("Store front id {} is not mapped to any store code", storeFrontId);
            throw new PackagingRecomendationException("No Store code found for Store front id: " + storeFrontId, INVALID_STORE_FRONT_ID);
        }

        List<PackagingRecommendationSRO> recomendationList = new ArrayList<PackagingRecommendationSRO>();

        if (null != isShipTogether && isShipTogether.booleanValue() == true) {
            LOG.info("Processing request for ship together.");

            List<PackagableItemSRO> itemList = packageDetail.getPackagableItemList();
            List<ShipTogetherSearchDTO> unevaluatedDtos = new ArrayList<ShipTogetherSearchDTO>();
            List<ShipTogetherSearchDTO> dtoList = getShipTogetherSearchDto(storeCode, storeFrontId, itemList);

            evaluateUsingShipTogether(recomendationList, unevaluatedDtos, dtoList);

            if (!CollectionUtils.isEmpty(unevaluatedDtos)) {
                // Break by supc count
                List<ShipTogetherSearchDTO> dtoModifiedList = getDtoGroupedBySupc(unevaluatedDtos);
                unevaluatedDtos.clear();
                evaluateUsingShipTogether(recomendationList, unevaluatedDtos, dtoModifiedList);
            }

            if (!CollectionUtils.isEmpty(unevaluatedDtos)) {
                // Break individually
                List<ShipTogetherSearchDTO> dtoModifiedList = getDtoIndividually(unevaluatedDtos);
                unevaluatedDtos.clear();
                evaluateUsingShipTogether(recomendationList, unevaluatedDtos, dtoModifiedList);
            }

            return recomendationList;

        }

        List<PackagableItemSRO> itemList = packageDetail.getPackagableItemList();
        for (PackagableItemSRO sro : itemList) {
            PackagingRecommendationSRO resultSRO = getPackagingRecommendationItem(storeCode, sro, storeFrontId);
            recomendationList.add(resultSRO);
        }

        return recomendationList;

    }

    private List<ShipTogetherSearchDTO> getDtoIndividually(List<ShipTogetherSearchDTO> dtoList) {
        List<ShipTogetherSearchDTO> individualList = new ArrayList<ShipTogetherSearchDTO>();
        for (ShipTogetherSearchDTO dto : dtoList) {
            for (ProductDetailDTO pdDto : dto.getProductDetailList()) {
                for (int count = 0; count < pdDto.getQuantity(); count++) {
                    ShipTogetherSearchDTO d = new ShipTogetherSearchDTO(dto.getStoreCode(), dto.getPackagingType());
                    ProductDetailDTO productDetailDTO = new ProductDetailDTO(pdDto);
                    productDetailDTO.setQuantity(1);
                    d.getProductDetailList().add(productDetailDTO);
                    individualList.add(d);
                }
            }
        }
        return individualList;
    }

    private void evaluateUsingShipTogether(List<PackagingRecommendationSRO> recomendationList, List<ShipTogetherSearchDTO> unevaluatedDtos,
            List<ShipTogetherSearchDTO> dtoModifiedList) {
        for (ShipTogetherSearchDTO dto : dtoModifiedList) {
            int totalQuantity = getQuantity(dto);
            int supportCount = ConfigUtils.getIntegerScalar(Property.PACKMAN_SHIP_TOGETHER_BRUTE_FORCE_SUPPORT_COUNT);
            if (totalQuantity <= supportCount) {
                PackagingTypeItemDTO result = shipTogetherService.shipTogether(dto);
                if (result != null) {
                    LOG.info("Ship together is applied. Recommended box [{}] for Items [{}]", result.getBoxName(), dto.getProductDetailList());
                    PackagingRecommendationSRO resultSRO = getResultSro(dto, result);
                    recomendationList.add(resultSRO);
                } else {
                    unevaluatedDtos.add(dto);
                    LOG.info("Unable to find box using ship together.. need to use another approach for product[" + dto + "]");
                }
            } else {
                LOG.info("Item count [{}] while supported count [{}]. So need to break.. ", totalQuantity, supportCount);
                unevaluatedDtos.add(dto);
            }
        }
    }

    private int getQuantity(ShipTogetherSearchDTO dto) {
        int count = 0;
        List<ProductDetailDTO> productDetailList = dto.getProductDetailList();
        for (int i = 0; i < productDetailList.size(); i++) {
            count += productDetailList.get(i).getQuantity();
        }
        return count;
    }

    private List<ShipTogetherSearchDTO> getDtoGroupedBySupc(List<ShipTogetherSearchDTO> dtoList) {
        List<ShipTogetherSearchDTO> groupedList = new ArrayList<ShipTogetherSearchDTO>();
        for (ShipTogetherSearchDTO dto : dtoList) {
            for (ProductDetailDTO pdDto : dto.getProductDetailList()) {
                ShipTogetherSearchDTO d = new ShipTogetherSearchDTO(dto.getStoreCode(), dto.getPackagingType());
                d.getProductDetailList().add(pdDto);
                groupedList.add(d);
            }
        }
        return groupedList;
    }

    private PackagingRecommendationSRO getResultSro(ShipTogetherSearchDTO dto, PackagingTypeItemDTO result) {
        PackagingRecommendationSRO response = new PackagingRecommendationSRO();

        PackagingTypeSRO ptSro = new PackagingTypeSRO();
        PackagingTypeCacheDTO packagingTypeCacheDto = CacheManager.getInstance().getCache(PackagingTypeCache.class).getPackagingTypeDtoV1(dto.getStoreCode(),
                dto.getPackagingType());
        ptSro.setType(packagingTypeCacheDto.getType());
        ptSro.setDescription(packagingTypeCacheDto.getDescription());

        PackagingTypeItemSRO ptiSro = new PackagingTypeItemSRO();
        ptiSro.setCode(result.getBoxName());
        ptiSro.setDescription(result.getDescription());

        List<SupcQuantitySRO> supcQtySroList = new ArrayList<SupcQuantitySRO>();

        for (ProductDetailDTO pdDto : dto.getProductDetailList()) {
            SupcQuantitySRO supcQtySro = new SupcQuantitySRO();
            supcQtySro.setSupc(pdDto.getSupc());
            supcQtySro.setQuantity(pdDto.getQuantity());
            supcQtySroList.add(supcQtySro);
        }

        response.setPackagingMode(packagingTypeCacheDto.getPackagingMode());
        response.setPackagingType(ptSro);
        response.setPreferredPackagingTypeItem(ptiSro);
        response.setSupcList(supcQtySroList);

        return response;
    }

    private PackagingRecommendationSRO getPackagingRecommendationItem(String storeCode, PackagableItemSRO sro, String storeFrontId) throws PackagingRecomendationException {

        SUPCDetailDTO dto = prepareDto(sro, storeCode, storeFrontId);
        String supc = dto.getSupc();
        ProductFulfilmentAttributeSRO pauSRO = getProductAttributes(supc);
        String packagingType = deducePackagingType(storeCode, dto, pauSRO);
        PackagingTypeCacheDTO ptDto = CacheManager.getInstance().getCache(PackagingTypeCache.class).getPackagingTypeByTypeAndStore(packagingType, storeCode);

        String description = null;
        PackagingTypeItemResultHolder out = null;
        String packagingMode = null;
        if (null != ptDto) {
            packagingMode = ptDto.getPackagingMode();
            description = ptDto.getDescription();
            LOG.info("Packaging type and packaging mode found are :" + packagingType + "," + packagingMode);
            if (PackagingMode.PICKNPACK.mode().equals(packagingMode)) {
                out = getPreferredAndRecommendedBoxesForPickNPackMode(storeCode, packagingType);
            } else {
                //for NORMAL and others
                out = getPreferredAndRecommendedBoxesForNormalMode(storeCode, packagingType, supc, pauSRO);
            }
        } else {
            LOG.info("No such packagingType {} found in system for store {}", packagingType, storeCode);
        }

        PackagingRecommendationSRO resultObj = createPackagingRecommendationSRO(dto, packagingType, description, packagingMode, out);
        return resultObj;
    }

    private List<ShipTogetherSearchDTO> getShipTogetherSearchDto(String storeCode, String storeFrontId, List<PackagableItemSRO> sroList) throws PackagingRecomendationException {

        Map<String, ShipTogetherSearchDTO> packagingTypeMap = new HashMap<String, ShipTogetherSearchDTO>();

        for (PackagableItemSRO sro : sroList) {
            SUPCDetailDTO supcDetaiDto = prepareDto(sro, storeCode, storeFrontId);
            String supc = supcDetaiDto.getSupc();
            ProductFulfilmentAttributeSRO pauSRO = getProductAttributes(supc);
            String packagingType = deducePackagingType(storeCode, supcDetaiDto, pauSRO);
            Double length, breadth, height;
            if (ConfigUtils.getBooleanScalar(Property.USE_PRIMARY_LBH)) {
                length = pauSRO.getPrimaryLength();
                breadth = pauSRO.getPrimaryBreadth();
                height = pauSRO.getPrimaryHeight();
            } else {
                length = pauSRO.getLength();
                breadth = pauSRO.getBreadth();
                height = pauSRO.getHeight();
            }
            Double margin = ConfigUtils.getDoubleScalar(Property.PACKMAN_RECO_LBH_MARGIN);
            Double volWt = cocofsExternalService.getVolumetricWeightFromLBH(length, breadth, height);
            ShipTogetherSearchDTO dto = packagingTypeMap.get(packagingType);
            if (dto == null) {
                dto = new ShipTogetherSearchDTO(storeCode, packagingType);
                packagingTypeMap.put(packagingType, dto);
            }
            dto.getProductDetailList().add(new ProductDetailDTO(supcDetaiDto.getSupc(), length, breadth, height, margin, volWt, sro.getQuantity(), packagingType));
        }

        List<ShipTogetherSearchDTO> dtoList = new ArrayList<ShipTogetherSearchDTO>();
        for (Entry<String, ShipTogetherSearchDTO> entry : packagingTypeMap.entrySet()) {
            dtoList.add(entry.getValue());
        }
        return dtoList;

    }

    private PackagingRecommendationSRO createPackagingRecommendationSRO(SUPCDetailDTO dto, String packagingType, String description, String packagingMode,
            PackagingTypeItemResultHolder out) {
        PackagingRecommendationSRO resultSRO = new PackagingRecommendationSRO();
        resultSRO.setPackagingMode(packagingMode);
        resultSRO.setPackagingType(createPackagingType(packagingType, description));
        if (out != null) {
            resultSRO.setPreferredPackagingTypeItem(out.getPreferred());
        }
        resultSRO.setSupcList(createSUPCQuantitySROs(dto));
        return resultSRO;
    }

    private PackagingTypeSRO createPackagingType(String packagingType, String description) {
        PackagingTypeSRO pt = new PackagingTypeSRO();
        pt.setType(packagingType);
        pt.setDescription(description);
        return pt;
    }

    private List<SupcQuantitySRO> createSUPCQuantitySROs(SUPCDetailDTO dto) {
        SupcQuantitySRO supcSRO = new SupcQuantitySRO();
        supcSRO.setSupc(dto.getSupc());

        List<SupcQuantitySRO> supcList = new ArrayList<SupcQuantitySRO>();
        supcList.add(supcSRO);
        return supcList;
    }

    private ProductFulfilmentAttributeSRO getProductAttributes(String supc) throws PackagingRecomendationException {
        ProductFulfilmentAttributeSRO pauSRO = cocofsExternalService.getProductFulfilmentAttributes(new GetProductFulfilmentAttributeRequest(supc));
        if (null == pauSRO) {
            LOG.error("Product fullfillment attributes not found for supc :" + supc);
            throw new PackagingRecomendationException("pauSRO is found null. SUPC does not exist, supc:" + supc, INVALID_SUPC_CODE);
        }
        return pauSRO;
    }

    private String deducePackagingType(String storeCode, SUPCDetailDTO dto, ProductFulfilmentAttributeSRO pauSRO) throws PackagingRecomendationException {
        String packagingType = null;
        if (dep.getBooleanScalar(Property.PACKMAN_PACKAGING_RULE_EVALUATION_REQ)) {
            try {
                packagingType = ruleEngineService.calculatePackagingType(dto);
            } catch (ExternalDataNotFoundException ex) {
                LOG.error("Packaging type could not be evaluated from rules" + ex);
                throw new PackagingRecomendationException("External server call failed", EXTERNAL_SERVICE_CALL_FAILED);
            } catch (PackagingTypeNotFoundException ex) {
                LOG.error("Packaging type could not be evaluated from rules" + ex);
                throw new PackagingRecomendationException("Packaging type not found", PACKAGING_TYPE_NOT_FOUND);
            }

        } else {
            //TODO - check PA/packagingType only for SD specific store code, rest all it should read null from CoCoFS
            if (storeCode.equals(dep.getStringScalar(Property.SNAPDEAL_STORE_CODE))) {
                packagingType = pauSRO.getPackagingType();
            }

        }
        return packagingType;
    }

    private PackagingTypeItemResultHolder getPreferredAndRecommendedBoxesForNormalMode(String storeCode, String packagingType, String supc, ProductFulfilmentAttributeSRO pauSRO)
            throws PackagingRecomendationException {
        PackagingTypeItemResultHolder out = null;
        double volWeight = getVolWeight(pauSRO);
        Double length, breadth, height;
        if (dep.getBooleanScalar(Property.USE_PRIMARY_LBH)) {
            length = pauSRO.getPrimaryLength();
            breadth = pauSRO.getPrimaryBreadth();
            height = pauSRO.getPrimaryHeight();
        } else {
            length = pauSRO.getLength();
            breadth = pauSRO.getBreadth();
            height = pauSRO.getHeight();
        }
        RecommendationParamsDTO recoDto = getRecommendationParamsForSearchDto(supc, length, breadth, height, storeCode, packagingType, volWeight);
        PackagingTypeCacheDTO stCacheDTO = CacheManager.getInstance().getCache(PackagingTypeCache.class).getPackagingTypeDtoV1(storeCode, packagingType);

        if (stCacheDTO != null) {
            PackagingTypeItemSRO ptiSRO = new PackagingTypeItemSRO();
            PackagingTypeItemDTO dto = stCacheDTO.getRecommendation(recoDto);
            ptiSRO.setDescription(dto.getDescription());
            ptiSRO.setCode(dto.getBoxName());
            out = new PackagingTypeItemResultHolder(ptiSRO);
        }
        return out;
    }

    public RecommendationParamsDTO getRecommendationParamsForSearchDto(String supc, double length, double breadth, double height, String storeCode, String pkgingType,
            double volWeight) {
        RecommendationParamsDTO recoDto = createRecParamDTO();
        recoDto.setStoreCode(storeCode);
        recoDto.setPackagingType(pkgingType);
        recoDto.setSupc(supc);
        recoDto.setVolWeight(volWeight);
        recoDto.setLength(length);
        recoDto.setBreadth(breadth);
        recoDto.setHeight(height);
        return recoDto;
    }

    private double getVolWeight(ProductFulfilmentAttributeSRO pauSRO) throws PackagingRecomendationException {
        double volWeight = 0.0d;
        if (dep.getBooleanScalar(Property.USE_PRIMARY_LBH)) {
            if (null == pauSRO.getPrimaryLength() || null == pauSRO.getPrimaryBreadth() || null == pauSRO.getPrimaryHeight()) {
                LOG.error("using primary LBH, but one or more attrib in LBH is NULL in pauSRO {}", pauSRO);
                throw new PackagingRecomendationException("Primary LBH is null", PRIMARY_LBH_NOT_AVAILABLE);
            }
            volWeight = cocofsExternalService.getVolumetricWeightFromLBH(pauSRO.getPrimaryLength(), pauSRO.getPrimaryBreadth(), pauSRO.getPrimaryHeight());
        } else {
            volWeight = cocofsExternalService.getVolumetricWeightFromLBH(pauSRO.getLength(), pauSRO.getBreadth(), pauSRO.getHeight());
        }
        return volWeight;
    }

    private PackagingTypeItemResultHolder getPreferredAndRecommendedBoxesForPickNPackMode(String storeCode, String packagingType) throws PackagingRecomendationException {

        PackagingTypeItemResultHolder out = null;
        try {
            PackagingTypeCacheDTO stCacheDTO = CacheManager.getInstance().getCache(PackagingTypeCache.class).getPackagingTypeDtoV1(storeCode, packagingType);

            if (stCacheDTO != null) {
                PackagingTypeItemSRO ptiSRO = new PackagingTypeItemSRO();
                PackagingTypeItemDTO dto = stCacheDTO.getRecommendation(null);
                ptiSRO.setDescription(dto.getDescription());
                ptiSRO.setCode(dto.getBoxName());
                out = new PackagingTypeItemResultHolder(ptiSRO);
            }
        } catch (Exception ex) {
            LOG.error("Surface packaging isue" + ex);
            throw new PackagingRecomendationException(ex.getMessage(), ex);
        }
        return out;
    }

    private String getStoreCodeFromStoreFrontId(String storeFrontId) {
        String storeCode = dep.getStringScalar(Property.SNAPDEAL_STORE_CODE);
        if (StringUtils.isNotEmpty(storeFrontId)) {
            storeCode = getStoreFrontToStoreCodeMapCache().getStoreFrontToStoreCodeMap().get(storeFrontId);
        }
        return storeCode;
    }

    private StoreFrontToStoreCodeMapCache getStoreFrontToStoreCodeMapCache() {
        return CacheManager.getInstance().getCache(StoreFrontToStoreCodeMapCache.class);
    }

    private SUPCDetailDTO prepareDto(PackagableItemSRO sro, String storeCode, String storeFrontId) {
        SUPCDetailDTO dto = new SUPCDetailDTO();
        dto.setSupc(sro.getSupc());
        dto.setStoreCode(storeCode);
        dto.setStoreFrontId(storeFrontId);
        dto.setBrand(sro.getBrand());
        dto.setCatageory(sro.getCategory());
        dto.setSubCatageory(sro.getSubCategory());
        if (!CollectionUtils.isEmpty(sro.getLabels())) {
            dto.setLabels(sro.getLabels());
        }
        return dto;
    }

    private RecommendationParamsDTO createRecParamDTO() {
        return dep.createRecParamDTO();
    }

    static class PackagingTypeItemResultHolder {
        private PackagingTypeItemSRO preferred;

        public PackagingTypeItemResultHolder(PackagingTypeItemSRO preferred) {
            this.preferred = preferred;
        }

        public PackagingTypeItemSRO getPreferred() {
            return preferred;
        }

        public void setPreferred(PackagingTypeItemSRO preferred) {
            this.preferred = preferred;
        }

    }

    public interface IDependency {
        String getStringScalar(Property property);

        boolean getBooleanScalar(Property property);

        int getIntegerScalar(Property property);

        RecommendationParamsDTO createRecParamDTO();
    }

    static class Dependency implements IDependency {
        @Override
        public String getStringScalar(Property property) {
            return ConfigUtils.getStringScalar(property);
        }

        @Override
        public int getIntegerScalar(Property property) {
            return ConfigUtils.getIntegerScalar(property);
        }

        @Override
        public boolean getBooleanScalar(Property property) {
            return ConfigUtils.getBooleanScalar(property);

        }

        @Override
        public RecommendationParamsDTO createRecParamDTO() {
            return new RecommendationParamsDTO();
        }
    }

}
