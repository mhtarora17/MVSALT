/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.packman.services.packaging.impl;

import java.text.DecimalFormat;
//import java.util.ArrayList;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.snapdeal.base.cache.CacheManager;
import com.snapdeal.base.utils.CollectionUtils;
import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.db.users.IUserRoleService;
import com.snapdeal.cocofs.db.users.IUserService;
import com.snapdeal.cocofs.entity.UserRole;
import com.snapdeal.packman.cache.StoreFrontToStoreCodeMapCache;
import com.snapdeal.packman.configuration.PackagingProperty;
import com.snapdeal.packman.configuration.PackagingTypeItemProperty;
import com.snapdeal.packman.dao.IPackmanExclusivelyDao;
import com.snapdeal.packman.dto.RoleStoreMappingDto;
import com.snapdeal.packman.entity.PackagingType;
import com.snapdeal.packman.entity.PackagingTypeItem;
import com.snapdeal.packman.entity.PackagingTypeItemProperties;
import com.snapdeal.packman.entity.PackagingTypeProperties;
import com.snapdeal.packman.entity.RoleStoreMapping;
import com.snapdeal.packman.entity.Store;
import com.snapdeal.packman.entity.StoreProperties;
import com.snapdeal.packman.enums.PackagingMode;
import com.snapdeal.packman.enums.StoreAttribute;
import com.snapdeal.packman.services.external.ICocofsExternalService;
import com.snapdeal.packman.services.packaging.IPackmanExclusiveService;
import com.snapdeal.packman.services.packaging.dto.RolesHelper;
import com.snapdeal.packman.services.packaging.dto.SideBar;
import com.snapdeal.packman.services.roles.IPackmanRolesGeneratorService;
import com.snapdeal.packman.url.Url;

/**
 * @version 1.0, 01-Dec-2015
 * @author indrajit
 */
@Service("packmanExclusiveService")
@Transactional
public class PackmanExclusiveServiceImpl implements IPackmanExclusiveService {

    private static final Logger   LOG = LoggerFactory.getLogger(PackmanExclusiveServiceImpl.class);

    private static final String LABEL_SEPARATOR = ",";

    @Autowired
    IPackmanExclusivelyDao        packmanExclusiveDao;

    @Autowired
    ICocofsExternalService        cocofsExternalService;

    @Autowired
    IUserService                  userService;

    @Autowired
    IUserRoleService              userRoleService;

    @Autowired
    IPackmanRolesGeneratorService rolecreationService;
    
    @Override
    public Store saveStore(Store store) {
        store.setLastUpdated(DateUtils.getCurrentTime());
        return packmanExclusiveDao.persistStore(store);
    }

    @Override
    public List<Store> getAllStoreDetails() {
        return packmanExclusiveDao.fetchAllStores();
    }

    @Override
    public Store getStore(String code) {
        return packmanExclusiveDao.fetchStore(code);
    }

    @Override
    public void savePackagingType(PackagingType packagingType) {
        packagingType.setLastUpdated(DateUtils.getCurrentTime());
        packmanExclusiveDao.persistPackagingType(packagingType);
    }

    @Override
    public PackagingType getPackagingType(String type, String storeCode) {
        return packmanExclusiveDao.fetchPackagingType(type, storeCode);
    }

    @Override
    public void createPackagingTypeItem(PackagingTypeItem packagingTypeItem, String user, String packagingTypeCode, String storeCode,List<PackagingTypeItemProperties> property) {
        PackagingType packagingType = getPackagingType(packagingTypeCode, storeCode);
        String packagingTypeItemCode = packagingTypeItem.getCode();
        if (packagingType == null) {
            LOG.error("Unable to find packaging type for provided packaging type item [{}], with packaging type [{}] and store [{}]", packagingTypeItemCode, packagingTypeCode,
                    storeCode);
            throw new IllegalArgumentException("Unable to find packaging type for packaging type item [" + packagingTypeItemCode + "]" + " with packaging type ["
                    + packagingTypeCode + "]" + " and store [" + storeCode + "].");
        }
        if(ConfigUtils.getBooleanScalar(Property.PACKMAN_UNIQUE_PACKAGING_TYPE_ITEM)){
            PackagingTypeItem p = getPackagingTypeItem(packagingTypeItemCode);
            if (p != null) {
                PackagingType packagingTypeExisted = p.getPackagingType();
                throw new IllegalArgumentException("Packaging type item [" + packagingTypeItemCode + "] already exist with packaging type [" + packagingTypeExisted.getType()
                        + "] under store [" + packagingTypeExisted.getStore().getCode() + "]");
            }
        }
        packagingTypeItem.setPackagingType(packagingType);
        packagingTypeItem.setCreated(DateUtils.getCurrentTime());
        packagingTypeItem.setUpdatedBy(user);
        
        if(property != null){
            for(PackagingTypeItemProperties prop : property){
                prop.setEnabled(true);
                prop.setUpdatedBy(user);
                prop.setCreated(DateUtils.getCurrentTime());
                prop.setLastUpdated(DateUtils.getCurrentTime());
            }
        }
        
        savePackagingTypeItem(packagingTypeItem,property);
    }

    @Override
    @Transactional
    public void savePackagingTypeItem(PackagingTypeItem packagingTypeItem,List<PackagingTypeItemProperties> properties) {
        packagingTypeItem.setLastUpdated(DateUtils.getCurrentTime());
        if (packagingTypeItem.getPackagingType().getPackagingMode().equals(PackagingMode.NORMAL.mode())) {
            PackagingTypeItemProperties VolWtProp = null;
            Float length = null,breadth=null,height=null;
            for(PackagingTypeItemProperties prop : properties){
                if(prop.getName().equals(PackagingTypeItemProperty.LENGTH.getName())){
                    length = Float.parseFloat(prop.getValue());
                } else if(prop.getName().equals(PackagingTypeItemProperty.BREADTH.getName())){
                    breadth = Float.parseFloat(prop.getValue());
                } else if(prop.getName().equals(PackagingTypeItemProperty.HEIGHT.getName())){
                    height = Float.parseFloat(prop.getValue());
                } else if(prop.getName().equals(PackagingTypeItemProperty.VOL_WEIGHT.getName())){
                    VolWtProp = prop;
                }
            }
            if (getBooleanScalar(getPackagingTypeProperties(PackagingProperty.VALIDATION_ENABLED.mode(), packagingTypeItem.getPackagingType()))) {
                LOG.info("pre-persist validation start.");
                validateAirPackagingType(packagingTypeItem);
                LOG.info("pre-persist validation done.");
            }
            // Height is null for 2D packaging type
            if(height!=null){
                double volumetricWeightFromLBH = cocofsExternalService.getVolumetricWeightFromLBH(length, breadth,height);
                if (!validateVolWeight(volumetricWeightFromLBH)) {
                    LOG.info(" vol. weight cannot be zero  for " + packagingTypeItem);
                    throw new IllegalArgumentException("Packaging Type Item's vol. weight should be greater than zero ");
                }
                double maxVolWeight = ConfigUtils.getDoubleScalar(Property.PACKMAN_MAX_VOL_WEIGHT);
                if (volumetricWeightFromLBH > maxVolWeight) {
                    LOG.info("pre-persist validation failed for max vol. weight allowed for " + packagingTypeItem);
                    throw new IllegalArgumentException(
                            "Packaging Type Item's vol. weight [" + volumetricWeightFromLBH + " gms] exceeds max allowed vol. weight [" + maxVolWeight + " gms].");
                }
                // Assign Vol Wt
                VolWtProp.setValue(Double.toString(round(volumetricWeightFromLBH,2)));
            }
        } else {
            if (getBooleanScalar(getPackagingTypeProperties(PackagingProperty.VALIDATION_ENABLED.mode(), packagingTypeItem.getPackagingType()))) {
                LOG.info("pre-persist validation start.");
                validateSurfacePackagingType(packagingTypeItem);
                LOG.info("pre-persist validation done.");
            }
        }
        LOG.info("Persisting Packaging Type Item {}", packagingTypeItem);
        PackagingTypeItem p = packmanExclusiveDao.persistPackagingTypeItem(packagingTypeItem);
        LOG.info("Persisted Packaging Type Item");
        
        if(properties != null){
            for(PackagingTypeItemProperties prop : properties){
                prop.setPackagingTypeItem(p);
            }
            LOG.info("Persisting packaging type item properties {}",properties);
            packmanExclusiveDao.persistPackagingTypeItem(properties);
            LOG.info("Presisted packaging type item properties");
        }
    }

    @Override
    public void validateAirPackagingType(PackagingTypeItem packagingTypeItem) {
        String prefix = getStringScalar(getPackagingTypeProperties(PackagingProperty.PACKAGING_TYPE_PREFIX.mode(), packagingTypeItem.getPackagingType()));
        if (prefix == null) {
            LOG.info("Packaging type prefix does not exists for " + packagingTypeItem.getPackagingType().getType());
            throw new IllegalArgumentException("Packaging type prefix does not exist.");
        }
        String code = packagingTypeItem.getCode();
        if (!code.startsWith(prefix)) {
            LOG.info("pre-persist validation failed for code prefix for " + packagingTypeItem);
            throw new IllegalArgumentException("Packaging Type Item's code must start with prefix [" + prefix + "].");
        } else {
            if (!isNumeric(code.replace(prefix, ""))) {
                LOG.info("pre-persist validation failed for code prefix for " + packagingTypeItem);
                throw new IllegalArgumentException("Packaging Type code [" + code + "] is not valid. It must be numeric after prefix " + prefix);
            }
        }

    }
    
    public static boolean isNumeric(String str) {
        if(StringUtils.isEmpty(str)){
            return false;
        }
        for (char c : str.toCharArray()) {
            if (!Character.isDigit(c))
                return false;
        }
        return true;
    }


    /**
     * Validate volweight such that its conversion to decimal two places should be greater than zero
     * 
     * @param volWeight
     * @return
     */
    private boolean validateVolWeight(Double volWeight) {
        DecimalFormat df = new DecimalFormat("0.00");
        String angleFormated = df.format(volWeight);
        if (Double.parseDouble(angleFormated) > 0) {
            return true;
        }

        return false;
    }

    @Override
    public void validateSurfacePackagingType(PackagingTypeItem packagingTypeItem) {
        String prefix = getStringScalar(getPackagingTypeProperties(PackagingProperty.PACKAGING_TYPE_PREFIX.mode(), packagingTypeItem.getPackagingType()));
        if (prefix == null) {
            LOG.info("Packaging type prefix does not exists for " + packagingTypeItem.getPackagingType().getType());
            throw new IllegalArgumentException("Packaging type prefix does not exist.");
        }
        if (!packagingTypeItem.getCode().startsWith(prefix)) {
            LOG.info("pre-persist validation failed for code prefix. " + packagingTypeItem);
            throw new IllegalArgumentException("Surface type Item Name must start with prefix [" + prefix + "].");
        }
    }

    @Override
    public PackagingTypeItem getPackagingTypeItem(String type, int packagingTypeId) {
        return packmanExclusiveDao.getPackagingTypeItem(type, packagingTypeId);
    }

    @Override
    public PackagingTypeItem getPackagingTypeItem(String type) {
        return packmanExclusiveDao.getPackagingTypeItem(type);
    }

    @Override
    public List<PackagingType> getAllPackagingType(String storeCode) {
        return packmanExclusiveDao.getAllPackagingType(storeCode);
    }

    @Override
    public List<PackagingType> getAllEnabledPackagingType(String storeCode) {
        return packmanExclusiveDao.getAllEnabledPackagingTypeByStore(storeCode);
    }

    @Override
    public void saveRoleStoremapping(List<RoleStoreMappingDto> dtoList) {
        List<RoleStoreMapping> listToBePersisted = new ArrayList<>();
        for (RoleStoreMappingDto dto : dtoList) {
            RoleStoreMapping entity = new RoleStoreMapping();
            entity.setRoleCode(dto.getRoleCode());
            entity.setStoreCode(dto.getStoreCode());
            entity.setEnable(dto.isEnabled());
            entity.setCreated(DateUtils.getCurrentTime());
            entity.setLastUpdated(DateUtils.getCurrentTime());
            entity.setUpdatedBy(dto.getUpdatedBy());
            listToBePersisted.add(entity);
        }
        packmanExclusiveDao.persistRoleStoreMapping(listToBePersisted);
    }

    @Override
    public List<PackagingTypeItem> getAllPackagingTypeItemDetails(String storeCode, String packagingTypeCode) {
        PackagingType packagingType = getPackagingType(packagingTypeCode, storeCode);
        return packmanExclusiveDao.getAllPackagingTypeItem(packagingType);
    }

    @Override
    @Transactional
    public void deletePackagingTypeItem(String type, int packagingTypeId) {
        PackagingTypeItem packagingTypeItem = getPackagingTypeItem(type, packagingTypeId);
        List<PackagingTypeItemProperties> properties = packagingTypeItem.getProperties();
        if(properties != null){
            packmanExclusiveDao.deletePackagingTypeItemProperties(properties);
            LOG.info("Deleting packaging type properties for packaging type item {}",type);
        }
        packmanExclusiveDao.deletePackagingTypeItem(packagingTypeItem);
        LOG.info("Deletion is successful..");
    }


    @Override
    public List<SideBar> getSideBar(String email, boolean specialRole) {

        // Check if special user is there
        // Admin or tech

        Map<String, RolesHelper> storeWiseRoles = new HashMap<String, RolesHelper>();

        if (specialRole) {

            // fetch store wise url

            Map<String, List<String>> storeWiseUrlMap = new HashMap<String, List<String>>();
            storeWiseUrlMap = getStoreWiseUrlMap(storeWiseUrlMap);

            String storeCode;
            RolesHelper roleHelper;
            Set<Entry<String, List<String>>> entrySet = storeWiseUrlMap.entrySet();
            for (Entry<String, List<String>> entry : entrySet) {
                storeCode = entry.getKey();
                List<String> urlList = entry.getValue();
                roleHelper = new RolesHelper();
                if (urlList != null) {
                    for (String url : urlList) {
                        String[] urlParts = url.split("/");
                        setRoles(roleHelper, urlParts[urlParts.length - 1], url);
                    }
                }
                storeWiseRoles.put(storeCode, roleHelper);
            }

        } else {

            Map<String, Map<String, List<String>>> storeWiseRoleUrlMap = getStoreWiseRoleUrlMap(email);
            Map<String, List<String>> storeWiseRoleMap = getStoreWiseRoleMap(storeWiseRoleUrlMap);

            if (storeWiseRoleMap.size() > 0) {
                Set<Entry<String, List<String>>> storeWiseRoleMapEntrySet = storeWiseRoleMap.entrySet();
                for (Entry<String, List<String>> storeRoles : storeWiseRoleMapEntrySet) {
                    String storeCode = storeRoles.getKey();
                    RolesHelper roleHelper = new RolesHelper();
                    List<String> storeRolesList = storeRoles.getValue();
                    if (null != storeRolesList) {
                        for (String role : storeRolesList) {
                            List<String> urlList = storeWiseRoleUrlMap.get(storeCode).get(role);
                            for (String url : urlList) {
                                String[] urlParts = url.split("/");
                                setRoles(roleHelper, urlParts[urlParts.length - 1], url);
                            }
                        }
                    }
                    storeWiseRoles.put(storeCode, roleHelper);
                }
            }
        }

        Map<String, List<PackagingType>> storeWisePackagingTypeMap = getStoreWisePackagingTypeMap();
        List<SideBar> levelOne = generateSideBar(storeWiseRoles, storeWisePackagingTypeMap);

        return levelOne;
    }

    private Map<String, List<String>> getStoreWiseUrlMap(Map<String, List<String>> storeWiseUrlMap) {
        List<Object[]> lt = packmanExclusiveDao.getStoreUrlMapping();
        for (Object[] object : lt) {
            String storeCode = (String) object[0];
            String url = (String) object[1];
            List<String> urlList = storeWiseUrlMap.get(storeCode);
            if (urlList == null) {
                urlList = new ArrayList<String>();
                storeWiseUrlMap.put(storeCode, urlList);
            }
            urlList.add(url);
        }
        return storeWiseUrlMap;
    }

    private List<SideBar> generateSideBar(Map<String, RolesHelper> storeWiseRoles, Map<String, List<PackagingType>> storeWisePackagingTypeMap) {
        List<SideBar> levelOne = new ArrayList<SideBar>();
        if (storeWiseRoles.size() > 0) {
            for (Entry<String, RolesHelper> storeRoles : storeWiseRoles.entrySet()) {
                List<SideBar> levelTwo = new ArrayList<SideBar>();
                List<SideBar> levelThree = new ArrayList<SideBar>();
                List<SideBar> levelFour = null;
                RolesHelper roleHelper = storeRoles.getValue();
                String storeCode = storeRoles.getKey();
                if (roleHelper.isViewPackagingTypeItem()) {
                    List<PackagingType> packagingTypes = storeWisePackagingTypeMap.get(storeCode);
                    if (null != packagingTypes) {
                        for (PackagingType pt : packagingTypes) {
                            levelFour = new ArrayList<SideBar>();
                            if (roleHelper.isViewSlab() && pt.getPackagingMode().contains(PackagingMode.NORMAL.mode())) {
                                SideBar slabElement = new SideBar(storeCode + "_SLAB_" + pt.getType(), storeCode, pt.getType(), pt.getType() + " Slab", pt.getPackagingMode(),
                                        roleHelper.getViewSlabDefaultURL(), null, null);
                                if (roleHelper.isCreateSlab())
                                    slabElement.getSecondaryUrl().add(roleHelper.getCreateSlabDefaultURL());
                                //levelFour.add(slabElement);
                            }
                            if (levelFour.size() == 0)
                                levelFour = null;
                            SideBar packagingTypeItemElement = new SideBar(storeCode + "_PTI_" + pt.getType(), storeCode, pt.getType(), pt.getType(), pt.getPackagingMode(),
                                    roleHelper.getViewPackagingTypeItemDefaultURL(), null, levelFour);
                            if (roleHelper.isCreatePackagingTypeItem())
                                packagingTypeItemElement.getSecondaryUrl().add(roleHelper.getCreatePackagingTypeItemDefaultURL());
                            levelThree.add(packagingTypeItemElement);
                        }
                    }
                }
                if (levelThree.size() == 0)
                    levelThree = null;
                SideBar packagingTypeElement = new SideBar(storeCode + "_PT_", storeCode, null, "Packaging Type", null,
                        roleHelper.isViewPackagingType() == true ? roleHelper.getViewPackagingTypeDefaultURL() : null, null, levelThree);
                if (roleHelper.isCreatePackagingType())
                    packagingTypeElement.getSecondaryUrl().add(roleHelper.getCreatePackagingTypeDefaultURL());
                if (levelThree != null || roleHelper.isCreatePackagingType() || roleHelper.isViewPackagingType()) {
                    levelTwo.add(packagingTypeElement);
                }
                SideBar ruleElelment = new SideBar(storeCode + "_R_", storeCode, null, "Rules", null, roleHelper.isViewrule() == true ? roleHelper.getViewRuleDefaultURL() : null,
                        null, null);
                if (roleHelper.isCreaterule())
                    ruleElelment.getSecondaryUrl().add(roleHelper.getCreateRuleDefaultURL());
                if (roleHelper.isCreaterule() || roleHelper.isViewrule()) {
                    levelTwo.add(ruleElelment);
                }
                if (levelTwo.size() == 0)
                    levelTwo = null;
                levelOne.add(new SideBar(storeCode, null, null, storeCode, null, null, null, levelTwo));
            }
        }
        if (levelOne.size() == 0)
            levelOne = null;
        return levelOne;
    }

    private Map<String, List<String>> getStoreWiseRoleMap(Map<String, Map<String, List<String>>> storeWiseRoleUrlMap) {
        Map<String, List<String>> map = new HashMap<String, List<String>>();
        for (Entry<String, Map<String, List<String>>> m : storeWiseRoleUrlMap.entrySet()) {
            String storeCode = m.getKey();
            List<String> roleList = map.get(storeCode);
            if (roleList == null) {
                roleList = new ArrayList<String>();
                map.put(storeCode, roleList);
            }
            for (Entry<String, List<String>> roleMap : m.getValue().entrySet()) {
                roleList.add(roleMap.getKey());
            }
        }
        return map;
    }

    private Map<String, List<PackagingType>> getStoreWisePackagingTypeMap() {
        Map<String, List<PackagingType>> map = new HashMap<String, List<PackagingType>>();
        List<PackagingType> packagingTypes = packmanExclusiveDao.getAllEnabledPackagingType();
        for (PackagingType p : packagingTypes) {
            String storeCode = p.getStore().getCode();
            List<PackagingType> ptList = map.get(storeCode);
            if (ptList == null) {
                ptList = new ArrayList<PackagingType>();
                map.put(storeCode, ptList);
            }
            ptList.add(p);
        }
        return map;
    }

    private Map<String, Map<String, List<String>>> getStoreWiseRoleUrlMap(String email) {
        Map<String, Map<String, List<String>>> map = new HashMap<String, Map<String, List<String>>>();
        List<Object[]> lt = packmanExclusiveDao.getUrlRoleStoreFromEmail(email);
        for (Object[] object : lt) {
            String url = (String) object[0];
            String roleCode = (String) object[1];
            String storeCode = (String) object[2];
            Map<String, List<String>> roleWiseUrlMap = map.get(storeCode);
            if (null == roleWiseUrlMap) {
                roleWiseUrlMap = new HashMap<String, List<String>>();
                map.put(storeCode, roleWiseUrlMap);
            }
            List<String> urlList = roleWiseUrlMap.get(roleCode);
            if (null == urlList) {
                urlList = new ArrayList<String>();
                roleWiseUrlMap.put(roleCode, urlList);
            }
            urlList.add(url);
        }
        return map;
    }

    private void setRoles(RolesHelper roleHelper, String code, String url) {
        String roleCodeLower = code.toLowerCase();
        if (roleCodeLower.endsWith(Url.ViewPackagingType.getCode())) {
            roleHelper.setViewPackagingType(true);
            roleHelper.setViewPackagingTypeDefaultURL(url);
        } else if (roleCodeLower.endsWith(Url.CreatePackagingType.getCode())) {
            roleHelper.setCreatePackagingType(true);
            roleHelper.setCreatePackagingTypeDefaultURL(url);
        } else if (roleCodeLower.endsWith(Url.ViewPackagingTypeItem.getCode())) {
            roleHelper.setViewPackagingTypeItem(true);
            roleHelper.setViewPackagingTypeItemDefaultURL(url);
        } else if (roleCodeLower.endsWith(Url.CreatePackagingTypeItem.getCode())) {
            roleHelper.setCreatePackagingTypeItem(true);
            roleHelper.setCreatePackagingTypeItemDefaultURL(url);
        } else if (roleCodeLower.endsWith(Url.ViewSlab.getCode())) {
            roleHelper.setViewSlab(true);
            roleHelper.setViewSlabDefaultURL(url);
        } else if (roleCodeLower.endsWith(Url.CreateSlab.getCode())) {
            roleHelper.setCreateSlab(true);
            roleHelper.setCreateSlabDefaultURL(url);
        } else if (roleCodeLower.endsWith(Url.CreateRule.getCode())) {
            roleHelper.setCreaterule(true);
            roleHelper.setCreateRuleDefaultURL(url);
        } else if (roleCodeLower.endsWith(Url.ViewRule.getCode())) {
            roleHelper.setViewrule(true);
            roleHelper.setViewRuleDefaultURL(url);
        }
    }

    @Override
    public boolean getBooleanScalar(String input) {
        if ("1".equals(input) || "true".equalsIgnoreCase(input)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String getStringScalar(String input) {
        return input;
    }

    @Override
    public Double getDoubleScalar(String input) {
        return Double.parseDouble(input);
    }

    @Override
    public Integer getIntegerScalar(String input) {
        return Integer.parseInt(input);
    }

    @Override
    public String getValueFromPackagingTypeProperty(PackagingTypeProperties packagingTypeProperties) {
        if (null != packagingTypeProperties)
            return packagingTypeProperties.getValue();
        return null;
    }

    public String getPackagingTypeProperties(String name, PackagingType packagingType) {
        PackagingTypeProperties packagingTypeProperties = packmanExclusiveDao.getPackagingTypePropertyByPackagingType(name, packagingType);
        return getValueFromPackagingTypeProperty(packagingTypeProperties);
    }

    @Transactional
    @Override
    public void createPackagingType(PackagingType packagingType, List<PackagingTypeProperties> packagingTypeProperties, String user) {
        String packagingTypeCode = packagingType.getType();
        String storeCode = packagingType.getStore().getCode();
        PackagingType pt = getPackagingType(packagingTypeCode, storeCode);
        if (pt != null) {
            LOG.error("Packaging type {} already exists for store {}", packagingTypeCode, storeCode);
            throw new IllegalArgumentException("Packaging type " + packagingTypeCode + " for store " + storeCode + " already exists.");
        }
        packagingType.setLastUpdated(DateUtils.getCurrentTime());
        packagingType.setCreated(DateUtils.getCurrentTime());
        packagingType.setUpdatedBy(user);
        PackagingType packagingTypeResult = packmanExclusiveDao.persistPackagingType(packagingType);

        // Special case when packaging type is picknpack - packaging type item is generated automatically
        if (packagingTypeResult.getPackagingMode().equals(PackagingMode.PICKNPACK.mode())) {
            PackagingTypeItem item = packagingType(packagingTypeResult, user);
            LOG.info("Auto generating packaging type item {}", item);
            packmanExclusiveDao.persistPackagingTypeItem(item);
        }

        for (PackagingTypeProperties p : packagingTypeProperties) {
            p.setPackagingType(packagingTypeResult);
            p.setCreated(DateUtils.getCurrentTime());
            p.setUpdatedBy(user);
            p.setLastUpdated(DateUtils.getCurrentTime());
        }
        packmanExclusiveDao.createPackagingTypeProperties(packagingTypeProperties, packagingTypeResult);
    }

    private PackagingTypeItem packagingType(PackagingType type, String user) {
        PackagingTypeItem item = new PackagingTypeItem();
        item.setCode(type.getType());
        item.setPackagingType(type);
        item.setDescription(type.getDescription());
        item.setUpdatedBy(user);
        item.setLastUpdated(DateUtils.getCurrentTime());
        item.setCreated(DateUtils.getCurrentTime());
        return item;
    }

    @Override
    @Transactional
    public Store createStore(Store store) throws Exception {
        String storeCode = store.getCode();
        String storeFrontId = store.getStoreFrontId();
        Store st = getStore(storeCode);
        if (st != null) {
            LOG.error("Store with code {} already exists", storeCode);
            throw new IllegalArgumentException("Store with code " + storeCode + " Already exists.");
        }
        st = getStoreByStoreFrontId(storeFrontId);
        if (st != null) {
            LOG.error("Store with store front id {} already exists", storeFrontId);
            throw new IllegalArgumentException("Store with store front id " + storeFrontId + " Already exists.");
        }
        Store savedStore = saveStore(store);
        rolecreationService.generateRolesAndUrlMapping(savedStore.getId(), savedStore.getCode());
        return savedStore;
    }

    @Transactional
    @Override
    public void updateStore(Store store, StoreProperties property) {
        store.setLastUpdated(DateUtils.getCurrentTime());
        LOG.info("Updating store :" + store);
        packmanExclusiveDao.persistStore(store);
        List<String> roleList = new ArrayList<String>();
        List<RoleStoreMapping> roleStoreMappingList = packmanExclusiveDao.fetchRoleStoreMappingByStoreCode(store.getCode());
        for (RoleStoreMapping roleStoreMapping : roleStoreMappingList) {
            roleList.add(roleStoreMapping.getRoleCode());
        }
        if (property != null) {
            LOG.info("Updating store property ");
            packmanExclusiveDao.saveStoreProperty(property);
        }
        LOG.info("Updating roles for store:" + store.getCode());
        packmanExclusiveDao.updateRole(roleList, store.isEnabled());
        LOG.info("Updating user roles for store:" + store.getCode());
        packmanExclusiveDao.updateRoleStoreMapping(store.getCode(), store.isEnabled());
    }

    @Override
    public Store getStoreByStoreFrontId(String id) {
        return packmanExclusiveDao.fetchStoreByStoreFrontId(id);
    }


    @Override
    @Transactional
    public void createStoreAndStoreProperties(Store store, StoreProperties property, String user) throws Exception {

        store.setUpdatedBy(user);
        store.setCreated(DateUtils.getCurrentTime());
        store.setLastUpdated(DateUtils.getCurrentTime());
        Store st = createStore(store);

        if (property != null) {
            property.setStore(st);
            property.setCreated(DateUtils.getCurrentTime());
            property.setUpdatedBy(user);
            saveStoreProperty(property);
        }

    }

    @Override
    @Transactional
    public void saveStoreProperty(StoreProperties property) {
        property.setLastUpdated(DateUtils.getCurrentTime());
        packmanExclusiveDao.saveStoreProperty(property);
    }

    @Override
    public StoreProperties fetchStoreProperty(String storeCode, String name) {
        return packmanExclusiveDao.fetchStoreProperty(storeCode, name);
    }

    @Override
    public List<String> fetchLabelsForStore(String storeCode) {
       return CacheManager.getInstance().getCache(StoreFrontToStoreCodeMapCache.class).getStoreToLabelMap().get(storeCode);
    }

    @Override
    public void deleteStoreProperty(StoreProperties property) {
        LOG.info("Deleting Store Property {} ", property);
        packmanExclusiveDao.deleteStoreProperty(property);
    }

    @Override
    public Map<String, String> getAllPackagingTypeProperties(PackagingType p) {
        Map<String, String> map = new HashMap<String, String>();
        List<PackagingTypeProperties> properties = packmanExclusiveDao.getAllPackagingTypeProperties(p);
        if (properties != null) {
            for (PackagingTypeProperties pro : properties) {
                map.put(pro.getName(), pro.getValue());
            }
        }
        return map;
    }

    @Override
    public List<PackagingTypeProperties> getAllPackagingTypeProperty(PackagingType p) {
        return packmanExclusiveDao.getAllPackagingTypeProperties(p);
    }

    @Override
    @Transactional
    public void updatePackagingType(PackagingType packagingType, List<PackagingTypeProperties> packagingTypeProperties, String packagingTypeCat, String user) {
        packagingType.setLastUpdated(DateUtils.getCurrentTime());
        packagingType.setUpdatedBy(user);
        packmanExclusiveDao.persistPackagingType(packagingType);
        LOG.info("packaging type updated successfully");
        for (PackagingTypeProperties p : packagingTypeProperties) {
            p.setLastUpdated(DateUtils.getCurrentTime());
            p.setUpdatedBy(user);
            packmanExclusiveDao.persistPackagingTypeProperty(p);
        }
        LOG.info("Packaging type property updated successfully");
        String packagingMode = packagingType.getPackagingMode();
        if (packagingMode.equals(PackagingMode.PICKNPACK.mode())) {
            PackagingTypeItem item = packmanExclusiveDao.getPackagingTypeItem(packagingType.getType(), packagingType.getId());
            item.setDescription(packagingType.getDescription());
            item.setLastUpdated(DateUtils.getCurrentTime());
            item.setUpdatedBy(user);
            LOG.info("Auto updating packaging type item {}", item);
            packmanExclusiveDao.persistPackagingTypeItem(item);
        }
    }

    @Override
    public Set<String> getAccessibleStoresByUserAndUserRole(String email, List<String> roles, boolean superUser) {

        Set<String> scSet = new HashSet<String>();
        if (superUser) {
            List<Store> stores = packmanExclusiveDao.fetchAllEnabledStores();
            if (!CollectionUtils.isEmpty(stores)) {
                for (Store store : stores) {
                    scSet.add(store.getCode());
                }
            }
        } else if (!CollectionUtils.isEmpty(roles)) {
            List<UserRole> eUserRole = new ArrayList<UserRole>();
            List<UserRole> lUserRole = userService.getAllUserRoleMappings(userService.getUserByEmail(email));
            for (UserRole us : lUserRole) {
                if (us.isEnabled() == true) {
                    eUserRole.add(us);
                }
            }
            List<String> roleCodes = new ArrayList<String>();
            for (UserRole lur : eUserRole) {
                for (String role : roles) {
                    if (lur.getRole().getCode().endsWith(role)) {
                        roleCodes.add(lur.getRole().getCode());
                    }
                }
            }
            for (String rc : roleCodes) {
                String sc = packmanExclusiveDao.getStoreCodeForRoleCode(rc);
                scSet.add(sc);
            }
        }
        return scSet;
    }

    @Override
    public List<PackagingTypeItemProperties> getPackagingTypeItemProperties(PackagingTypeItem packagingTypeItem) {
        return packmanExclusiveDao.getPackagingTypeItemProperties(packagingTypeItem.getId());
    }

    @Override
    public Double getMaxVolumetricWeight(int packagingTypeId) {
        List<PackagingTypeItemProperties> prop = packmanExclusiveDao.getMaxPackagingTypeSize(packagingTypeId);
        if(prop != null){
            Double max = 0.0;
            for(PackagingTypeItemProperties p : prop){
                max = Math.max(max, Double.parseDouble(p.getValue()));
            }
            if(max!=0.0){
                return max;
            }
        }
        return null;
    }

    private double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();
        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    @Override
    public List<String> getAllLabels(String storeCode) {
        StoreProperties labelProperty = fetchStoreProperty(storeCode, StoreAttribute.LABEL.getCode());
        if(labelProperty != null){
            String label = labelProperty.getValue();
            if(label!=null){
                List<String> labelList = Arrays.asList(label.split(LABEL_SEPARATOR));
                return labelList;
            }
        }
        return Collections.EMPTY_LIST;
    }

    @Override
    public void reCalculateVolWt(String currentUserEmail) {
        List<PackagingTypeItem> ptiList = packmanExclusiveDao.getAllPackagingTypeItem();
        List<PackagingTypeItemProperties> propertiesToPersist = new ArrayList<PackagingTypeItemProperties>();
        for(PackagingTypeItem pti : ptiList){
            List<PackagingTypeItemProperties> prop = pti.getProperties();
            PackagingTypeItemProperties volWtProp = null;
            Double length=null,breadth=null,height=null;
            for(PackagingTypeItemProperties p : prop){
                if(p.getName().equals(PackagingTypeItemProperty.LENGTH.getName())){
                    length = Double.parseDouble(p.getValue());
                } else if(p.getName().equals(PackagingTypeItemProperty.BREADTH.getName())){
                    breadth = Double.parseDouble(p.getValue());
                } else if(p.getName().equals(PackagingTypeItemProperty.HEIGHT.getName())){
                    height = Double.parseDouble(p.getValue());
                } else if(p.getName().equals(PackagingTypeItemProperty.VOL_WEIGHT.getName())){
                    volWtProp = p;
                }
            }
            if(volWtProp!=null){
                volWtProp.setValue(Double.toString(round(cocofsExternalService.getVolumetricWeightFromLBH(length, breadth, height),2)));
                volWtProp.setUpdatedBy(currentUserEmail);
                volWtProp.setLastUpdated(DateUtils.getCurrentTime());
                propertiesToPersist.add(volWtProp);
            }
        }
        if(!CollectionUtils.isEmpty(propertiesToPersist)){
           packmanExclusiveDao.persistPackagingTypeItem(propertiesToPersist);
        }
    }

}
