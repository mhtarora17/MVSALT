/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.services.user;

import java.util.List;
import java.util.Map;

import com.snapdeal.packman.dto.URLPatternDto;
import com.snapdeal.packman.entity.UrlPattern;
import com.snapdeal.packman.entity.UrlRoleMapping;

/**
 *  
 *  @version     1.0, 08-Dec-2015
 *  @author indrajit
 */
public interface IUrlPatternMappingService {
    
    boolean saveUrlPatternMappingWithDto(URLPatternDto dto);
    
    UrlPattern saveUrlPattern(UrlPattern urlPattern);
    
    UrlRoleMapping saveURLRoleMapping(UrlRoleMapping urlRoleMapping);
    
    Map<String, List<String>> getAllUrlPattern();
    
    
    

}
