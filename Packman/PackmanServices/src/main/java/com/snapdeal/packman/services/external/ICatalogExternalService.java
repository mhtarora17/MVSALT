package com.snapdeal.packman.services.external;

import com.snapdeal.catalog.base.model.GetAllCategoryListRequest;
import com.snapdeal.catalog.base.model.GetCategoryListResponse;
import com.snapdeal.catalog.base.model.GetMinProductInfoBySupcRequest;
import com.snapdeal.catalog.base.model.GetMinProductInfoResponse;
import com.snapdeal.packman.external.service.exception.ExternalDataNotFoundException;

public interface ICatalogExternalService {

    GetMinProductInfoResponse getMinProductInfo(GetMinProductInfoBySupcRequest request) throws ExternalDataNotFoundException;

    GetCategoryListResponse getAllCategories(GetAllCategoryListRequest request) throws ExternalDataNotFoundException;

}
