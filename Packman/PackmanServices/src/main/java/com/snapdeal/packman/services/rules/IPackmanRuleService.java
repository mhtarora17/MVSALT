/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.services.rules;

import java.util.Date;
import java.util.List;

import com.snapdeal.packman.rule.dto.SearchRuleDTO;
import com.snapdeal.rule.engine.entity.Rule;

/**
 *  
 *  @version     1.0, 23-Dec-2015
 *  @author brijesh
 */
public interface IPackmanRuleService {

    Object[] getSameRules(String type,String store, String subType,int priority,int blockI,String category, String subCategory,Date startDate, Date endDate,String brand,String supercategory,boolean bypassPriority,String ruleName);

    List<Rule> getMatchingRules(SearchRuleDTO dto);
    
    List<Rule> getRuleFromName(String name,String key,String value);
    
    List<Rule> getRuleByStoreAndPackagingType(String storeCode,String packagingType);

    List<Rule> getAllRulesByStore(String storeCode);
    
}
