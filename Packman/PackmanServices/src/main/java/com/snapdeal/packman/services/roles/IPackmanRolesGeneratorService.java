/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.services.roles;

/**
 *  
 *  @version     1.0, 08-Dec-2015
 *  @author indrajit
 */
public interface IPackmanRolesGeneratorService {
    
    void generateRolesAndUrlMapping(Integer storeId, String storeCode) throws Exception;
    
    

}
