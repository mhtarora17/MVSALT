/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.packman.services.packaging.recomendation;

import java.util.List;

import com.snapdeal.packman.common.sro.PackageDetailSRO;
import com.snapdeal.packman.common.sro.PackagingRecommendationSRO;
import com.snapdeal.packman.services.exception.PackagingRecomendationException;

/**
 * @version 1.0, 24-Dec-2015
 * @author indrajit
 */
public interface IPackagingRecommendationService {

    List<PackagingRecommendationSRO> getRecommendation(String storeFrontId, Boolean isShipTogather, PackageDetailSRO packageDetail) throws PackagingRecomendationException;

 }
