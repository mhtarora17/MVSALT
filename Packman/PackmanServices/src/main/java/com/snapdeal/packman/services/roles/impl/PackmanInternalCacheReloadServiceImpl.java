/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.services.roles.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.cache.loader.ICacheLoader;
import com.snapdeal.cocofs.configurations.ICacheLoadService;
import com.snapdeal.packman.services.roles.IPackmanInternalCacheReloadService;

/**
 *  
 *  @version     1.0, 22-Dec-2015
 *  @author indrajit
 */
@Service("packmanInternalCacheReloadService")
public class PackmanInternalCacheReloadServiceImpl implements IPackmanInternalCacheReloadService{
    
    private static final Logger       LOG           = LoggerFactory.getLogger(PackmanRolesGeneratorServiceImpl.class);
    
    @Autowired
    ICacheLoadService                cacheLoadService;
    
    @Autowired
    ICacheLoader                     cacheLoader;


    @Override
    public void reloadPackmanCaches() {
	// TODO Auto-generated method stub

	List<String> methodNameList = new ArrayList<String>();
	try {
	    methodNameList.add("loadRoles");
	    methodNameList.add("loadUrlToRoleMap");
	    cacheLoadService.loadCacheByMethodName(cacheLoader, methodNameList,null, true, "host");
	} catch (IllegalArgumentException | IllegalAccessException
		| InvocationTargetException e) {
	    // TODO Auto-generated catch block
	    LOG.error("Cache internal reload has failed:" + e);
	}

    }

}
