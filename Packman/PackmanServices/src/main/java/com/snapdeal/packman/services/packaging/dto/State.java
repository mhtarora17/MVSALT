/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.packman.services.packaging.dto;

/**
 * @version 1.0, 10-Dec-2015
 * @author nitish
 */
public class State {
    private boolean opened;
    private boolean selected;

    public State(boolean opened, boolean selected) {
        super();
        this.opened = opened;
        this.selected = selected;
    }

    public boolean isOpened() {
        return opened;
    }

    public void setOpened(boolean opened) {
        this.opened = opened;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

}