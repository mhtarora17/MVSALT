/**
 *  Copyright 2016 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.services.packaging.dto;

/**
 *  
 *  @version     1.0, 26-Feb-2016
 *  @author brijesh
 */
public class ProductDetailDTO {

    private String supc;
    
    private Double length;
    
    private Double breadth;
    
    private Double height;
    
    private Double margin;
    
    private Double volWt;
    
    private Double perimeter;
    
    private Integer quantity;
    
    private String packagingType;
    
    public ProductDetailDTO() {
    }
    
    public ProductDetailDTO(String supc, Double length, Double breadth, Double height, Double margin, Double volWt, Integer quantity, String packagingType) {
        super();
        this.supc = supc;
        this.length = length;
        this.breadth = breadth;
        this.height = height;
        this.margin = margin;
        this.volWt = volWt;
        this.quantity = quantity;
        this.packagingType = packagingType;
        this.perimeter = length + breadth;
    }
    
    public ProductDetailDTO(ProductDetailDTO obj){
        super();
        this.supc = obj.getSupc();
        this.length = obj.getLength();
        this.breadth = obj.getBreadth();
        this.height = obj.getHeight();
        this.margin = obj.getMargin();
        this.volWt = obj.getVolWt();
        this.quantity = obj.getQuantity();
        this.packagingType = obj.getPackagingType();
        this.perimeter = obj.getLength() + obj.getBreadth();
    }

    public String getSupc() {
        return supc;
    }

    public void setSupc(String supc) {
        this.supc = supc;
    }

    public Double getLength() {
        return length;
    }

    public void setLength(Double length) {
        this.length = length;
    }

    public Double getBreadth() {
        return breadth;
    }

    public void setBreadth(Double breadth) {
        this.breadth = breadth;
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public Double getMargin() {
        return margin;
    }

    public void setMargin(Double margin) {
        this.margin = margin;
    }

    public Double getVolWt() {
        return volWt;
    }

    public void setVolWt(Double volWt) {
        this.volWt = volWt;
    }

    public Double getPerimeter() {
        return perimeter;
    }

    public void setPerimeter(Double perimeter) {
        this.perimeter = perimeter;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getPackagingType() {
        return packagingType;
    }

    public void setPackagingType(String packagingType) {
        this.packagingType = packagingType;
    }

    @Override
    public String toString() {
        return "ProductDetailDTO [supc=" + supc + ", length=" + length + ", breadth=" + breadth + ", height=" + height + ", margin=" + margin + ", volWt=" + volWt + ", perimeter="
                + perimeter + ", quantity=" + quantity + ", packagingType=" + packagingType + "]";
    }
    
}
