/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.packman.services.exception;

import com.snapdeal.packman.common.ApiErrorCode;

/**
 * @version 1.0, 24-Dec-2015
 * @author indrajit
 */
public class PackagingRecomendationException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 123523623L;

    private ApiErrorCode      errorCode;

    public PackagingRecomendationException() {
    }

    //    public PackagingRecomendationException(String msg) {
    //        super(msg);
    //    }

    public PackagingRecomendationException(String msg, Throwable t) {
        super(msg, t);
    }

    public PackagingRecomendationException(String msg, ApiErrorCode errorCode) {
        super(msg);
        this.errorCode = errorCode;
    }

    public PackagingRecomendationException(String msg, ApiErrorCode errorCode, Throwable t) {
        super(msg, t);
        this.errorCode = errorCode;
    }

    public ApiErrorCode getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(ApiErrorCode errorCode) {
        this.errorCode = errorCode;
    }

}
