/**
 *  Copyright 2016 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.services.packaging.recomendation;

import com.snapdeal.packman.dto.PackagingTypeItemDTO;
import com.snapdeal.packman.services.packaging.dto.ShipTogetherSearchDTO;

/**
 *  
 *  @version     1.0, 26-Feb-2016
 *  @author brijesh
 */
public interface IShipTogetherService {

    PackagingTypeItemDTO shipTogether(ShipTogetherSearchDTO dto);
    
}
