/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.packman.services.packaging.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * @version 1.0, 09-Dec-2015
 * @author brijesh
 */
public class SideBar {

    private String        id;
    private String        text;
    private String        storeCode;
    private String        packagingTypeCode;
    private String        mode;
    private String        url;
    private List<String>  secondaryUrl = new ArrayList<String>();
    private State         state;
    private List<SideBar> children;

    public SideBar(String id, String storeCode, String packagingTypeCode, String text, String mode, String url, State state, List<SideBar> children) {
        super();
        this.id = id;
        this.text = text;
        this.storeCode = storeCode;
        this.packagingTypeCode = packagingTypeCode;
        this.mode = mode;
        this.url = url;
        this.state = state;
        this.children = children;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getStoreCode() {
        return storeCode;
    }

    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }

    public String getPackagingTypeCode() {
        return packagingTypeCode;
    }

    public void setPackagingTypeCode(String packagingTypeCode) {
        this.packagingTypeCode = packagingTypeCode;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<String> getSecondaryUrl() {
        return secondaryUrl;
    }

    public void setSecondaryUrl(List<String> secondaryUrl) {
        this.secondaryUrl = secondaryUrl;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public List<SideBar> getChildren() {
        return children;
    }

    public void setChildren(List<SideBar> children) {
        this.children = children;
    }

    @Override
    public String toString() {
        return "SideBar [id=" + id + ", text=" + text + ", storeCode=" + storeCode + ", packagingTypeCode=" + packagingTypeCode + ", mode=" + mode + ", url=" + url
                + ", secondaryUrl=" + secondaryUrl + ", state=" + state + ", children=" + children + "]";
    }

}
