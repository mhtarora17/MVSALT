/**
 *  Copyright 2016 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.services.packaging.recomendation;

import com.snapdeal.packman.dto.RecommendationParamsDTO;
import com.snapdeal.packman.services.exception.PackagingRecomendationException;
import com.snapdeal.packman.services.packaging.dto.RecommendationResponseDTO;

/**
 *  
 *  @version     1.0, 10-Feb-2016
 *  @author brijesh
 */
public interface IPackmanRecommendationService {

    RecommendationResponseDTO getPackagingRecommendation(RecommendationParamsDTO dto);
    
    RecommendationParamsDTO getEnrichedRecommendationDto(RecommendationParamsDTO dto) throws PackagingRecomendationException;
    
}
