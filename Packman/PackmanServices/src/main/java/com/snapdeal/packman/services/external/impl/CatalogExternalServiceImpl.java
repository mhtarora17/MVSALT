package com.snapdeal.packman.services.external.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.snapdeal.base.exception.TransportException;
import com.snapdeal.catalog.base.model.GetAllCategoryListRequest;
import com.snapdeal.catalog.base.model.GetCategoryListResponse;
import com.snapdeal.catalog.base.model.GetMinProductInfoBySupcRequest;
import com.snapdeal.catalog.base.model.GetMinProductInfoResponse;
import com.snapdeal.packman.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.packman.services.external.ICatalogExternalService;
import com.snapdeal.product.client.service.IProductClientService;

/**
 * @author nikhil
 */
@Service("catalogExternalService")
public class CatalogExternalServiceImpl implements ICatalogExternalService {

    @Autowired
    private IProductClientService productClientService;

    private static final Logger   LOG = LoggerFactory.getLogger(CatalogExternalServiceImpl.class);

    @Override
    @Cacheable(value = "productInfoCache", key = "#request.supc")
    public GetMinProductInfoResponse getMinProductInfo(GetMinProductInfoBySupcRequest request) throws ExternalDataNotFoundException {
        try {
            return productClientService.getMinProductInfoBySupc(request);
        } catch (TransportException e) {
            LOG.error("Error in getting min product info from CAMS, ex message {}, ex cause {}", e.getMessage(), e.getCause());
            throw new ExternalDataNotFoundException(ExternalDataNotFoundException.Type.DEFAULT, "Error in getting min product info from CAMS for supc " + request.getSupc());
        }
    }

    @Override
    public GetCategoryListResponse getAllCategories(GetAllCategoryListRequest request) throws ExternalDataNotFoundException {
        try {
            return productClientService.getAllCategories(request);
        } catch (TransportException e) {
            LOG.error("Error in getting categories from CAMS, ex message {}, ex cause {}", e.getMessage(), e.getCause());
            throw new ExternalDataNotFoundException(ExternalDataNotFoundException.Type.DEFAULT, "Error in getting categories from CAMS");
        }
    }

}
