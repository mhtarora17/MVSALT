/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.services.packaging.dto;

/**
 *  
 *  @version     1.0, 10-Dec-2015
 *  @author brijesh
 */
public class RolesHelper {

    private boolean viewPackagingType;
    
    private String viewPackagingTypeDefaultURL;
    
    private boolean createPackagingType;
    
    private String createPackagingTypeDefaultURL;
    
    private boolean viewPackagingTypeItem;
    
    private String viewPackagingTypeItemDefaultURL;
    
    private boolean createPackagingTypeItem;
    
    private String createPackagingTypeItemDefaultURL;
    
    private boolean viewSlab;
    
    private String viewSlabDefaultURL;
    
    private boolean createSlab;
    
    private String createSlabDefaultURL;
    
    private boolean createrule;
    
    private boolean viewrule;
    
    private String createRuleDefaultURL;
    
    private String viewRuleDefaultURL;
    

    public String getCreateRuleDefaultURL() {
        return createRuleDefaultURL;
    }

    public void setCreateRuleDefaultURL(String createRuleDefaultURL) {
        this.createRuleDefaultURL = createRuleDefaultURL;
    }

    public String getViewRuleDefaultURL() {
        return viewRuleDefaultURL;
    }

    public void setViewRuleDefaultURL(String viewRuleDefaultURL) {
        this.viewRuleDefaultURL = viewRuleDefaultURL;
    }

    public boolean isViewPackagingType() {
        return viewPackagingType;
    }

    public void setViewPackagingType(boolean viewPackagingType) {
        this.viewPackagingType = viewPackagingType;
    }

    public String getViewPackagingTypeDefaultURL() {
        return viewPackagingTypeDefaultURL;
    }

    public void setViewPackagingTypeDefaultURL(String viewPackagingTypeDefaultURL) {
        this.viewPackagingTypeDefaultURL = viewPackagingTypeDefaultURL;
    }

    public boolean isCreatePackagingType() {
        return createPackagingType;
    }

    public void setCreatePackagingType(boolean createPackagingType) {
        this.createPackagingType = createPackagingType;
    }

    public String getCreatePackagingTypeDefaultURL() {
        return createPackagingTypeDefaultURL;
    }

    public void setCreatePackagingTypeDefaultURL(String createPackagingTypeDefaultURL) {
        this.createPackagingTypeDefaultURL = createPackagingTypeDefaultURL;
    }

    public boolean isViewPackagingTypeItem() {
        return viewPackagingTypeItem;
    }

    public void setViewPackagingTypeItem(boolean viewPackagingTypeItem) {
        this.viewPackagingTypeItem = viewPackagingTypeItem;
    }

    public String getViewPackagingTypeItemDefaultURL() {
        return viewPackagingTypeItemDefaultURL;
    }

    public void setViewPackagingTypeItemDefaultURL(String viewPackagingTypeItemDefaultURL) {
        this.viewPackagingTypeItemDefaultURL = viewPackagingTypeItemDefaultURL;
    }

    public boolean isCreatePackagingTypeItem() {
        return createPackagingTypeItem;
    }

    public void setCreatePackagingTypeItem(boolean createPackagingTypeItem) {
        this.createPackagingTypeItem = createPackagingTypeItem;
    }

    public String getCreatePackagingTypeItemDefaultURL() {
        return createPackagingTypeItemDefaultURL;
    }

    public void setCreatePackagingTypeItemDefaultURL(String createPackagingTypeItemDefaultURL) {
        this.createPackagingTypeItemDefaultURL = createPackagingTypeItemDefaultURL;
    }

    public boolean isViewSlab() {
        return viewSlab;
    }

    public void setViewSlab(boolean viewSlab) {
        this.viewSlab = viewSlab;
    }

    public String getViewSlabDefaultURL() {
        return viewSlabDefaultURL;
    }

    public void setViewSlabDefaultURL(String viewSlabDefaultURL) {
        this.viewSlabDefaultURL = viewSlabDefaultURL;
    }

    public boolean isCreateSlab() {
        return createSlab;
    }

    public void setCreateSlab(boolean createSlab) {
        this.createSlab = createSlab;
    }

    public String getCreateSlabDefaultURL() {
        return createSlabDefaultURL;
    }

    public boolean isCreaterule() {
        return createrule;
    }

    public void setCreaterule(boolean createrule) {
        this.createrule = createrule;
    }

    public boolean isViewrule() {
        return viewrule;
    }

    public void setViewrule(boolean viewrule) {
        this.viewrule = viewrule;
    }

    public void setCreateSlabDefaultURL(String createSlabDefaultURL) {
        this.createSlabDefaultURL = createSlabDefaultURL;
    }

    @Override
    public String toString() {
        return "RolesHelper [viewPackagingType=" + viewPackagingType + ", viewPackagingTypeDefaultURL=" + viewPackagingTypeDefaultURL + ", createPackagingType="
                + createPackagingType + ", createPackagingTypeDefaultURL=" + createPackagingTypeDefaultURL + ", viewPackagingTypeItem=" + viewPackagingTypeItem
                + ", viewPackagingTypeItemDefaultURL=" + viewPackagingTypeItemDefaultURL + ", createPackagingTypeItem=" + createPackagingTypeItem
                + ", createPackagingTypeItemDefaultURL=" + createPackagingTypeItemDefaultURL + ", viewSlab=" + viewSlab + ", viewSlabDefaultURL=" + viewSlabDefaultURL
                + ", createSlab=" + createSlab + ", createSlabDefaultURL=" + createSlabDefaultURL + ", createrule=" + createrule + ", viewrule=" + viewrule
                + ", createRuleDefaultURL=" + createRuleDefaultURL + ", viewRuleDefaultURL=" + viewRuleDefaultURL + "]";
    }
}
