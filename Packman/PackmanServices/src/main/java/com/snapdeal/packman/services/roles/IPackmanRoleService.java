/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.services.roles;

import java.util.List;

import com.snapdeal.packman.dto.RoleDto;

/**
 *  
 *  @version     1.0, 09-Dec-2015
 *  @author indrajit
 */
public interface IPackmanRoleService {
    
    List<String> persisteRole(List<RoleDto> roleDtoList);

}
