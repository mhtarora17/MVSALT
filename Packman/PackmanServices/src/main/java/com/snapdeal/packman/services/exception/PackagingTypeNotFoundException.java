/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.services.exception;

/**
 *  
 *  @version     1.0, 23-Dec-2015
 *  @author indrajit
 */
public class PackagingTypeNotFoundException extends Exception{

    /**
     * 
     */
    private static final long serialVersionUID = 1234235L;
    
    public PackagingTypeNotFoundException(){
	
    }
    
    public PackagingTypeNotFoundException(String msg){
	super(msg);
    }

}
