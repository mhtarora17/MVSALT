/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.packman.services.packaging.dto;

import java.util.ArrayList;
import java.util.List;

import com.snapdeal.base.utils.CollectionUtils;
import com.snapdeal.base.utils.StringUtils;

/**
 * @version 1.0, 24-Dec-2015
 * @author indrajit
 */
public class SUPCDetailDTO {

    private String       supc;

    private String       storeCode;

    private String       brand;

    private String       catageory;

    private List<String> labels = new ArrayList<>();

    private String       subCatageory;
    
    private String      storeFrontId;

    public SUPCDetailDTO() {
    }

    public String getSupc() {
        return supc;
    }

    public void setSupc(String supc) {
        this.supc = supc;
    }

    public String getStoreCode() {
        return storeCode;
    }

    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getCatageory() {
        return catageory;
    }

    public void setCatageory(String catageory) {
        this.catageory = catageory;
    }

    public List<String> getLabels() {
        return labels;
    }

    public void setLabels(List<String> labels) {
        this.labels = labels;
    }

    public String getSubCatageory() {
        return subCatageory;
    }

    public void setSubCatageory(String subCatageory) {
        this.subCatageory = subCatageory;
    }
    
    

    public String getStoreFrontId() {
        return storeFrontId;
    }

    public void setStoreFrontId(String storeFrontId) {
        this.storeFrontId = storeFrontId;
    }

    public boolean isCatalogDetailsRequired() {
        if (StringUtils.isEmpty(brand) || StringUtils.isEmpty(catageory) || CollectionUtils.isEmpty(labels) || StringUtils.isEmpty(subCatageory)) {
            return true;
        }
        return false;
    }

  
    public String toString() {
    return "SUPCDetailDTO [supc=" + supc + ", storeCode=" + storeCode
        + ", brand=" + brand + ", catageory=" + catageory + ", labels="
        + labels + ", subCatageory=" + subCatageory + ", storeFrontId="
        + storeFrontId + "]";
    }

   

}
