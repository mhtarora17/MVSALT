/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.packman.common.sro;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;

/**
 * @version 1.0, 16-Dec-2015
 * @author indrajit
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SupcQuantitySRO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 8232530612044686284L;

    @Tag(21)
    private String            supc;

    @Tag(22)
    private Integer           quantity;

    public String getSupc() {
        return supc;
    }

    public void setSupc(String supc) {
        this.supc = supc;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "SupcQuantitySRO [supc=" + supc + ", quantity=" + quantity + "]";
    }

}
