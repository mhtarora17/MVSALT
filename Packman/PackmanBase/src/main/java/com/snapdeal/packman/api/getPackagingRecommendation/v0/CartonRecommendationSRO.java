package com.snapdeal.packman.api.getPackagingRecommendation.v0;

import java.io.Serializable;
import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.packman.common.sro.CartonSRO;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CartonRecommendationSRO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -724180139242940987L;

    @Tag(11)
    private List<CartonSRO> recommendedCartons;

    @Tag(12)
    private CartonSRO preferredCarton;

    public CartonRecommendationSRO() {
    
    }
    
    public CartonRecommendationSRO(List<CartonSRO> recommendedCartons, CartonSRO preferredCarton) {
        super();
        this.recommendedCartons = recommendedCartons;
        this.preferredCarton = preferredCarton;
    }

    public List<CartonSRO> getRecommendedCartons() {
        return recommendedCartons;
    }

    public void setRecommendedCartons(List<CartonSRO> recommendedCartons) {
        this.recommendedCartons = recommendedCartons;
    }

    public CartonSRO getPreferredCarton() {
        return preferredCarton;
    }

    public void setPreferredCarton(CartonSRO preferredCarton) {
        this.preferredCarton = preferredCarton;
    }

    @Override
    public String toString() {
        return "CartonRecommendationSRO [recommendedCartons=" + recommendedCartons + ", preferredCarton=" + preferredCarton + "]";
    }

}
