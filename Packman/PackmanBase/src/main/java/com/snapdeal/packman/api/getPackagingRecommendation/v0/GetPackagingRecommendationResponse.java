package com.snapdeal.packman.api.getPackagingRecommendation.v0;


import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.model.common.ServiceResponse;
import com.snapdeal.packman.common.PackagingType;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetPackagingRecommendationResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 4811968395869117102L;

    /**
     * refer {@link PackagingType} enum for all possible values;
     */
    @Tag(11)
    private String packagingType;

    @Tag(12)
    private CartonRecommendationSRO cartonRecommendation;

    @Tag(13)
    private PolybagRecommendationSRO polybagRecommendation;

    @Tag(14)
    private SurfaceRecommendationSRO surfaceRecommendation;

    /**
     * refer {@link PackagingType} enum for all possible values;
     * 
     * @return
     */
    public String getPackagingType() {
        return packagingType;
    }

    public void setPackagingType(String packagingType) {
        this.packagingType = packagingType;
    }

    public CartonRecommendationSRO getCartonRecommendation() {
        return cartonRecommendation;
    }

    public void setCartonRecommendation(CartonRecommendationSRO cartonRecommendation) {
        this.cartonRecommendation = cartonRecommendation;
    }

    public PolybagRecommendationSRO getPolybagRecommendation() {
        return polybagRecommendation;
    }

    public void setPolybagRecommendation(PolybagRecommendationSRO polybagRecommendation) {
        this.polybagRecommendation = polybagRecommendation;
    }

    public SurfaceRecommendationSRO getSurfaceRecommendation() {
        return surfaceRecommendation;
    }

    public void setSurfaceRecommendation(SurfaceRecommendationSRO surfaceRecommendation) {
        this.surfaceRecommendation = surfaceRecommendation;
    }

    @Override
    public String toString() {
        return "GetPackagingRecommendationResponse [packagingType=" + packagingType + ", cartonRecommendation=" + cartonRecommendation + ", polybagRecommendation="
                + polybagRecommendation + ", surfaceRecommendation=" + surfaceRecommendation + ", isSuccessful()=" + isSuccessful() + ", getCode()=" + getCode()
                + ", getMessage()=" + getMessage() + ", getValidationErrors()=" + getValidationErrors() + ", getProtocol()=" + getProtocol() + "]";
    }

}
