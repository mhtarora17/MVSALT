/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.packman.common.sro;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;

/**
 * @version 1.0, 24-Dec-2015
 * @author indrajit/shiv
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PackagingTypeItemSRO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3125903134557163653L;

    @Tag(21)
    private String            code;

    @Tag(22)
    private String            description;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "PackagingTypeItemSRO [code=" + code + ", description=" + description + "]";
    }

}
