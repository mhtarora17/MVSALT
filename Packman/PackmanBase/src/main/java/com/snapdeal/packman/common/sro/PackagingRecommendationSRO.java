/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.packman.common.sro;

import java.io.Serializable;
import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;

/**
 * @version 1.0, 16-Dec-2015
 * @author indrajit/shiv
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PackagingRecommendationSRO implements Serializable {

    /**
     * 
     */
    private static final long          serialVersionUID = -6308482752124807240L;

    // it should be referred by enum PackagingMode
    @Tag(21)
    private String                     packagingMode;

    @Tag(22)
    private PackagingTypeSRO           packagingType;

    @Tag(23)
    private PackagingTypeItemSRO       preferredPackagingTypeItem;

    @Tag(24)
    private List<PackagingTypeItemSRO> recommendedPackagingTypeItemList;

    @Tag(25)
    private List<SupcQuantitySRO>      supcList;

    public String getPackagingMode() {
        return packagingMode;
    }

    public void setPackagingMode(String packagingMode) {
        this.packagingMode = packagingMode;
    }

    public PackagingTypeSRO getPackagingType() {
        return packagingType;
    }

    public void setPackagingType(PackagingTypeSRO packagingType) {
        this.packagingType = packagingType;
    }

    public PackagingTypeItemSRO getPreferredPackagingTypeItem() {
        return preferredPackagingTypeItem;
    }

    public void setPreferredPackagingTypeItem(PackagingTypeItemSRO preferredPackagingTypeItem) {
        this.preferredPackagingTypeItem = preferredPackagingTypeItem;
    }

    public List<PackagingTypeItemSRO> getRecommendedPackagingTypeItemList() {
        return recommendedPackagingTypeItemList;
    }

    public void setRecommendedPackagingTypeItemList(List<PackagingTypeItemSRO> recommendedPackagingTypeItemList) {
        this.recommendedPackagingTypeItemList = recommendedPackagingTypeItemList;
    }

    public List<SupcQuantitySRO> getSupcList() {
        return supcList;
    }

    public void setSupcList(List<SupcQuantitySRO> supcList) {
        this.supcList = supcList;
    }

    @Override
    public String toString() {
        return "PackagingRecommendationSRO [packagingMode=" + packagingMode + ", packagingType=" + packagingType + ", preferredPackagingTypeItem=" + preferredPackagingTypeItem
                + ", recommendedPackagingTypeItemList=" + recommendedPackagingTypeItemList + ", supcList=" + supcList + "]";
    }

}
