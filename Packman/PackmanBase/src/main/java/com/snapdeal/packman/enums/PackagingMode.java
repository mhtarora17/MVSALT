/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.enums;

/**
 *  
 *  @version     1.0, 24-Dec-2015
 *  @author indrajit
 */
public enum PackagingMode {
    
    NORMAL("Normal"), PICKNPACK("Picknpack");
    String name;
    
    private PackagingMode(String name){
	this.name = name;
    }
    
    public String mode(){
	return name;
    }
    
    public static PackagingMode getShippingTypeByType(String key){
        PackagingMode result = null;
        for(PackagingMode s : PackagingMode.values()){
            if(s.mode().equals(key)){
                result = s;
                break;
            }
        }
        return result;
    }

}
