/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.packman.response;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.model.common.ServiceResponse;
import com.snapdeal.packman.common.sro.PackagingRecommendationSRO;

/**
 * @version 1.0, 16-Dec-2015
 * @author indrajit/shiv
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class GetPackagingRecommendationsResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long                serialVersionUID = -2150834562859479346L;
    
    @Tag(21)
    private List<PackagingRecommendationSRO> packagingRecommendationList;

    public List<PackagingRecommendationSRO> getPackagingRecommendationList() {
        return packagingRecommendationList;
    }

    public void setPackagingRecommendationList(List<PackagingRecommendationSRO> packagingRecommendationList) {
        this.packagingRecommendationList = packagingRecommendationList;
    }

    @Override
    public String toString() {
	return "GetPackagingRecommendationsResponse [packagingRecommendationList="
		+ packagingRecommendationList
		+ ", isSuccessful()="
		+ isSuccessful()
		+ ", getCode()="
		+ getCode()
		+ ", getMessage()="
		+ getMessage()
		+ ", getValidationErrors()="
		+ getValidationErrors() + "]";
    }

   

}
