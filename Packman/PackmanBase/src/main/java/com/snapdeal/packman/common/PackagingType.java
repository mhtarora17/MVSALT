package com.snapdeal.packman.common;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;

@JsonIgnoreProperties(ignoreUnknown = true)
public enum PackagingType {

	CARTON("CARTON"), //
	POLYBAG("POLYBAG"), //
	SURFACE("SURFACE");

	@Tag(1)
	private String code;

	private PackagingType(String code) {
		this.code = code;

	}

	public String getCode() {
		return code;
	}

}
