/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.packman.common.sro;

import java.io.Serializable;
import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.annotations.NotNull;

/**
 * @version 1.0, 16-Dec-2015
 * @author indrajit/shiv
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PackagableItemSRO implements Serializable {


    /**
     * 
     */
    private static final long serialVersionUID = 1266461645700855907L;

    @Tag(21)
    @NotNull
    private String            supc;

    @Tag(22)
    @NotNull
    private String            sellerCode;

    @Tag(23)
    private Integer           quantity;

    @Tag(24)
    private String            brand;

    @Tag(25)
    private List<String>      labels;

    @Tag(26)
    private String            subCategory;

    @Tag(27)
    private String            category;

    public String getSupc() {
        return supc;
    }

    public void setSupc(String supc) {
        this.supc = supc;
    }

    public String getSellerCode() {
        return sellerCode;
    }

    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public List<String> getLabels() {
        return labels;
    }

    public void setLabels(List<String> labels) {
        this.labels = labels;
    }

    public String getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "PackagableItemSRO [supc=" + supc + ", sellerCode=" + sellerCode + ", quantity=" + quantity + ", brand=" + brand + ", labels=" + labels + ", subCategory="
                + subCategory + ", category=" + category + "]";
    }

}
