/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.packman.common.sro;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;

/**
 * @version 1.0, 21-Dec-2015
 * @author indrajit/shiv
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PackagingTypeSRO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 2344758567711344321L;

    @Tag(21)
    private String            type;

    @Tag(22)
    private String            description;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "PackagingTypeSRO [type=" + type + ", description=" + description + "]";
    }

}
