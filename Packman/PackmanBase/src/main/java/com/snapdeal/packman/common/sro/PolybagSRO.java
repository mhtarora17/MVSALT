package com.snapdeal.packman.common.sro;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PolybagSRO extends PackagingSRO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -8746199672354793741L;

    @Tag(11)
    private String code;

    public PolybagSRO(){
        
    }
    
    public PolybagSRO(String code) {
        super();
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "PolybagSRO [code=" + code + "]";
    }

}
