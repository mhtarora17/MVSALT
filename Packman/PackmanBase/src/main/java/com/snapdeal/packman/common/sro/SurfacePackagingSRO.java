package com.snapdeal.packman.common.sro;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SurfacePackagingSRO extends PackagingSRO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -441644386873906639L;

    @Tag(11)
    private String type;

    @Tag(12)
    private String packagingInstruction;

    public SurfacePackagingSRO(){
        
    }
    
    public SurfacePackagingSRO(String type, String packagingInstruction) {
        super();
        this.type = type;
        this.packagingInstruction = packagingInstruction;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPackagingInstruction() {
        return packagingInstruction;
    }

    public void setPackagingInstruction(String packagingInstruction) {
        this.packagingInstruction = packagingInstruction;
    }

    @Override
    public String toString() {
        return "SurfacePackagingSRO [type=" + type + ", packagingInstruction=" + packagingInstruction + "]";
    }

}
