package com.snapdeal.packman.api.getAllSurfacePackagingTypes.v0;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.snapdeal.base.model.common.ServiceRequest;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetAllSurfacePackagingTypesRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -3091626827654694816L;

    public GetAllSurfacePackagingTypesRequest() {

    }

    @Override
    public String toString() {
        return "GetAllSurfacePackagingTypesRequest [getResponseProtocol()=" + getResponseProtocol() + ", getRequestProtocol()=" + getRequestProtocol() + ", getUserTrackingId()="
                + getUserTrackingId() + ", getContextSRO()=" + getContextSRO() + ", getUniqueLoggingCode()=" + getUniqueLoggingCode() + "]";
    }

}
