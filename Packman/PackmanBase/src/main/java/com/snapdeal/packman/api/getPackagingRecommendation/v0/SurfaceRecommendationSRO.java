package com.snapdeal.packman.api.getPackagingRecommendation.v0;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.packman.common.sro.SurfacePackagingSRO;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SurfaceRecommendationSRO implements Serializable {

    /**
     * 
     */
    private static final long   serialVersionUID = -98240291002262661L;

    @Tag(11)
    private SurfacePackagingSRO surfaceType;

    public SurfacePackagingSRO getSurfaceType() {
        return surfaceType;
    }

    public void setSurfaceType(SurfacePackagingSRO surfaceType) {
        this.surfaceType = surfaceType;
    }

    @Override
    public String toString() {
        return "SurfaceRecommendationSRO [surfaceType=" + surfaceType + "]";
    }

}
