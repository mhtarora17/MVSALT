package com.snapdeal.packman.api.getPackagingRecommendation.v0;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.annotations.NotNull;
import com.snapdeal.base.model.common.ServiceRequest;
import com.snapdeal.packman.common.sro.AddressSRO;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetPackagingRecommendationRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -2609206433406103290L;
    
    @Tag(10)
    @NotNull
    private String          apiKey;

    @Tag(11)
    @NotNull
    private String            supc;

    @Tag(12)
    @NotNull
    private String            sellerCode;

    @Tag(13)
    @NotNull
    private String            courierCode;

    @Tag(14)
    @NotNull
    private String            shipMode;

    @Tag(15)
    @NotNull
    private AddressSRO        shipToAddress;

    @Tag(16)
    @NotNull
    private AddressSRO        shipFromAddress;

    
    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getSupc() {
        return supc;
    }

    public void setSupc(String supc) {
        this.supc = supc;
    }

    public String getSellerCode() {
        return sellerCode;
    }

    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }

    public String getShipMode() {
        return shipMode;
    }

    public void setShipMode(String shipMode) {
        this.shipMode = shipMode;
    }

    public String getCourierCode() {
        return courierCode;
    }

    public void setCourierCode(String courierCode) {
        this.courierCode = courierCode;
    }

    public AddressSRO getShipToAddress() {
        return shipToAddress;
    }

    public void setShipToAddress(AddressSRO shipToAddress) {
        this.shipToAddress = shipToAddress;
    }

    public AddressSRO getShipFromAddress() {
        return shipFromAddress;
    }

    public void setShipFromAddress(AddressSRO shipFromAddress) {
        this.shipFromAddress = shipFromAddress;
    }

    @Override
    public String toString() {
        return "GetPackagingRecommendationRequest [supc=" + supc + ", sellerCode=" + sellerCode + ", courierCode=" + courierCode + ", shipMode=" + shipMode + ", shipToAddress="
                + shipToAddress + ", shipFromAddress=" + shipFromAddress + ", getResponseProtocol()=" + getResponseProtocol() + ", getRequestProtocol()=" + getRequestProtocol()
                + ", getUserTrackingId()=" + getUserTrackingId() + ", getContextSRO()=" + getContextSRO() + ", getUniqueLoggingCode()=" + getUniqueLoggingCode() + "]";
    }

}
