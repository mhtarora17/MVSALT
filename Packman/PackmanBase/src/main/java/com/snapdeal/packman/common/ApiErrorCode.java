package com.snapdeal.packman.common;

import com.snapdeal.packman.enums.ServiceResponseCode;

/**
 * @author nikhil
 */
public enum ApiErrorCode {

    /**
     * if a required field is found to be null in the request/requestItem
     **/
    REQUIRED_FIELD_NULL(100, "Required fields are null", ServiceResponseCode.NON_RETRIBALE_FAILURE),

    /**
     * if the request is a batch request, and contains more than the allowed request items inside it
     **/
    REQUEST_BATCH_SIZE_LIMIT_EXCEEDED(102, "Batch size is out of allowed limit", ServiceResponseCode.NON_RETRIBALE_FAILURE),

    /**
     * if requested data do not exists at the serverend
     **/
    REQUESTED_DATA_DOES_NOT_EXISTS(104),

    /**
     * if requested data is out of the allowed range
     **/
    DATA_OUT_OF_RANGE(101),

    /**
     * unknown error
     **/
    UNKNOWN_ERROR(103),

    /**
     * System's internal error, probably due to external services or due to failed data-source interaction
     **/
    INTERNAL_ERROR(105),

    /**
     * will be used only for those API's which are supposed to create new data at server
     **/
    DATA_ALREADY_EXISTS(106),

    /**
     * Requested data cannot be persisted
     **/
    DATA_NOT_PERSISTED(107),

    DATA_NOT_DISABLED(108),

    /**
     * Value of some of the fields are other than the pre-configured values e.g.
     */
    UNEXPECTED_DATA_ENCOUNTERED(109),

    /**
     * The value provided is not in the configured list
     */
    INVALID_FULFILMENT_MODEL(110),

    /**
     * when no fillment model exist for requested supc-seller.This should not happen
     */
    NO_FULFILLMENT_MODEL_FOUND(111),

    /**
     * when a feature is not supported any more and it has been deprecated
     */
    NOT_SUPPORTED(112),
    /**
     *
     */
    INSUFFICIENT_DATA(113),

    /**
     * Invalid api authentication key
     */
    INVALID_API_KEY(114, "Invalid api key", ServiceResponseCode.NON_RETRIBALE_FAILURE),

    INVALID_STORE_FRONT_ID(115, " Invalid store front id", ServiceResponseCode.NON_RETRIBALE_FAILURE),

    PRIMARY_LBH_NOT_AVAILABLE(116, "Primary LBH is null", ServiceResponseCode.NON_RETRIBALE_FAILURE),

    INVALID_SUPC_CODE(117, "Supc is not valid", ServiceResponseCode.NON_RETRIBALE_FAILURE),

    SHIP_TOGETHER_NOT_SUPPORTED(118, "Ship together is not supported", ServiceResponseCode.NON_RETRIBALE_FAILURE),

    PACKAGING_TYPE_NOT_FOUND(119, "Packaging type not found", ServiceResponseCode.NON_RETRIBALE_FAILURE),

    EXTERNAL_SERVICE_CALL_FAILED(120, "Call to  external servers failed", ServiceResponseCode.RETRIABLE_FAILURE),

    INVALID_PARAMETERS(121, "Invalid/Missing parameters", ServiceResponseCode.NON_RETRIBALE_FAILURE);

    private final int code;

    private String msg;

    private ServiceResponseCode responseCode;

    private ApiErrorCode(int errorCode) {
        code = errorCode;
    }

    private ApiErrorCode(int errorCode, String msg, ServiceResponseCode responseCode) {
        code = errorCode;
        this.msg = msg;
        this.responseCode = responseCode;
    }

    public String getMsg() {
        return msg;
    }

    public int code() {
        return this.code;
    }

    public ServiceResponseCode getResponseCode() {
        return responseCode;
    }
}
