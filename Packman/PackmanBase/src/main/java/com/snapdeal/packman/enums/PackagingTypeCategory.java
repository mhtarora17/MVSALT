/**
 *  Copyright 2016 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.enums;

/**
 *  
 *  @version     1.0, 24-Feb-2016
 *  @author brijesh
 */
public enum PackagingTypeCategory {

    TWO_DIMENSIONAL("2D"),
    THREE_DIMENSIONAL("3D");
    
    String name;
    
    private PackagingTypeCategory(String type) {
        this.name = type;
    }
    
    public String type(){
        return name;
    }
    
    public static PackagingTypeCategory getPackagingTypeCat(String key){
        PackagingTypeCategory result = null;
        for(PackagingTypeCategory s : PackagingTypeCategory.values()){
            if(s.name().equals(key)){
                result = s;
                break;
            }
        }
        return result;
    }
    
}
