/**
 *  Copyright 2016 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.enums;

/**
 *  
 *  @version     1.0, 11-Jan-2016
 *  @author indrajit
 */
public enum ServiceResponseCode {
    
    SUCCESSFUL("0","Successful"),
    RETRIABLE_FAILURE("1","Error: Please Re-try"),
    NON_RETRIBALE_FAILURE("2","Error: Non triable error");
    
    String code;
    
    String msg;
    
    private ServiceResponseCode(String code, String msg){
	this.code = code;
	this.msg = msg;
    }

    public String getCode() {
        return code;
    }

     public String getMsg() {
        return msg;
    }

      

}
