package com.snapdeal.packman.common.sro;

import java.io.Serializable;
import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.annotations.NotNull;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PackageDetailSRO implements Serializable {

    /**
     * 
     */
    private static final long       serialVersionUID = 601029227719409753L;

    @Tag(21)
    @NotNull
    private List<PackagableItemSRO> packagableItemList;

    @Tag(22)
    @NotNull
    private AddressSRO              shipToAddress;

    @Tag(23)
    @NotNull
    private AddressSRO              shipFromAddress;

    @Tag(24)
    private String                  courierCode;

    @Tag(25)
    private String                  shippingMode;

    public List<PackagableItemSRO> getPackagableItemList() {
        return packagableItemList;
    }

    public void setPackagableItemList(List<PackagableItemSRO> packagableItemList) {
        this.packagableItemList = packagableItemList;
    }

    public AddressSRO getShipToAddress() {
        return shipToAddress;
    }

    public void setShipToAddress(AddressSRO shipToAddress) {
        this.shipToAddress = shipToAddress;
    }

    public AddressSRO getShipFromAddress() {
        return shipFromAddress;
    }

    public void setShipFromAddress(AddressSRO shipFromAddress) {
        this.shipFromAddress = shipFromAddress;
    }

    public String getCourierCode() {
        return courierCode;
    }

    public void setCourierCode(String courierCode) {
        this.courierCode = courierCode;
    }

    public String getShippingMode() {
        return shippingMode;
    }

    @Deprecated
    public void setShippingMode(String shippingMode) {
        this.shippingMode = shippingMode;
    }

    @Override
    public String toString() {
        return "PackageDetailSRO [packagableItemList=" + packagableItemList + ", shipToAddress=" + shipToAddress + ", shipFromAddress=" + shipFromAddress + ", courierCode="
                + courierCode + ", shippingMode=" + shippingMode + "]";
    }

}
