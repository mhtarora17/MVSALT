package com.snapdeal.packman.api.getPackagingRecommendation.v0;

import java.io.Serializable;
import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.packman.common.sro.PolybagSRO;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PolybagRecommendationSRO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4832986205232152010L;

    @Tag(11)
    private List<PolybagSRO>  recommendedPolybags;

    @Tag(12)
    private PolybagSRO        preferredPolybag;

    public PolybagRecommendationSRO() {

    }

    public PolybagRecommendationSRO(List<PolybagSRO> recommendedPolybags, PolybagSRO preferredPolybag) {
        super();
        this.recommendedPolybags = recommendedPolybags;
        this.preferredPolybag = preferredPolybag;
    }

    public List<PolybagSRO> getRecommendedPolybags() {
        return recommendedPolybags;
    }

    public void setRecommendedPolybags(List<PolybagSRO> recommendedPolybags) {
        this.recommendedPolybags = recommendedPolybags;
    }

    public PolybagSRO getPreferredPolybag() {
        return preferredPolybag;
    }

    public void setPreferredPolybag(PolybagSRO preferredPolybag) {
        this.preferredPolybag = preferredPolybag;
    }

    @Override
    public String toString() {
        return "PolybagRecommendationSRO [recommendedPolybags=" + recommendedPolybags + ", preferredPolybag=" + preferredPolybag + "]";
    }

}
