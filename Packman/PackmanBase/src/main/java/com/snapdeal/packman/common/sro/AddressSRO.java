package com.snapdeal.packman.common.sro;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.annotations.NotNull;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AddressSRO implements Serializable {

    /**
	 * 
	 */
    private static final long serialVersionUID = -6782534056427289096L;

    @Tag(11)
    private String            state;

    @Tag(12)
    private String            city;

    @Tag(13)
    private String            country;

    @Tag(14)
    @NotNull
    private String            pincode;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    @Override
    public String toString() {
        return "AddressSRO [state=" + state + ", city=" + city + ", country=" + country + ", pincode=" + pincode + "]";
    }

}
