/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.packman.request;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.annotations.NotNull;
import com.snapdeal.base.model.common.ServiceRequest;
import com.snapdeal.packman.common.sro.PackageDetailSRO;

/**
 * @version 1.0, 16-Dec-2015
 * @author indrajit/shiv
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class GetPackagingRecommendationsRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 2109744133001962234L;

    @Tag(11)
    @NotNull
    private String            apiKey;

    /**
     *   This field will accept valid store front id. If it comes null then default store (snapdeal)
     *   will be considered for packman processing.
     */
    @Tag(12)
    private String            storeFrontId;

    @Tag(13)
    @NotNull
    private PackageDetailSRO  packageDetail;

    @Tag(14)
    private Boolean           isShipTogether;

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getStoreFrontId() {
        return storeFrontId;
    }

    public void setStoreFrontId(String storeFrontId) {
        this.storeFrontId = storeFrontId;
    }

    public PackageDetailSRO getPackageDetail() {
        return packageDetail;
    }

    public void setPackageDetail(PackageDetailSRO packageDetail) {
        this.packageDetail = packageDetail;
    }

    public Boolean getIsShipTogether() {
        return isShipTogether;
    }

    public void setIsShipTogether(Boolean isShipTogether) {
        this.isShipTogether = isShipTogether;
    }

    @Override
    public String toString() {
        return "GetPackagingRecommendationsRequest [storeFrontId=" + storeFrontId + ", packageDetail=" + packageDetail + ", isShipTogether=" + isShipTogether + "]";
    }

}
