package com.snapdeal.packman.api.getAllSurfacePackagingTypes.v0;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.model.common.ServiceResponse;
import com.snapdeal.packman.common.sro.SurfacePackagingSRO;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetAllSurfacePackagingTypesResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long         serialVersionUID = -7515487701719715563L;

    @Tag(11)
    private List<SurfacePackagingSRO> surfacePackagingTypes;

    public List<SurfacePackagingSRO> getSurfacePackagingTypes() {
        return surfacePackagingTypes;
    }

    public void setSurfacePackagingTypes(List<SurfacePackagingSRO> surfacePackagingTypes) {
        this.surfacePackagingTypes = surfacePackagingTypes;
    }

    @Override
    public String toString() {
        return "GetAllSurfacePackagingTypesResponse [surfacePackagingTypes=" + surfacePackagingTypes + ", isSuccessful()=" + isSuccessful() + ", getCode()=" + getCode()
                + ", getMessage()=" + getMessage() + ", getValidationErrors()=" + getValidationErrors() + ", getProtocol()=" + getProtocol() + "]";
    }

}
