/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.packman.cache;

import java.util.HashSet;
import java.util.Set;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.snapdeal.base.cache.CacheManager;
import com.snapdeal.cocofs.configuration.CocofsPropertiesCache;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.packman.common.PackagingType;
import com.snapdeal.packman.dto.SlabCacheDTO;
import com.snapdeal.packman.entity.CartonSlab;
import com.snapdeal.packman.entity.CartonType;

/**
 * @version 1.0, 19-Sep-2015
 * @author shiv
 */
public class SlabMappingCacheTest {

    SlabMappingCache sut         = null;
    static int       slabCount   = 0;
    static int       cartonCount = 0;

    @Before
    public void setUp() throws Exception {
        sut = new SlabMappingCache();

        CocofsPropertiesCache propertiesCache = new CocofsPropertiesCache();
        propertiesCache.addProperty(Property.PACKMAN_DEFAULT_RECO_AIR.getName(), Property.PACKMAN_DEFAULT_RECO_AIR.getValue());

        CacheManager.getInstance().setCache(propertiesCache);
    }

    @After
    public void tearDown() throws Exception {
        sut = null;
        slabCount = 0;
        cartonCount = 0;
    }

    @Test
    public void testGetSlab_001() {
        Double volWeight = Double.valueOf(10);
        SlabCacheDTO dto = sut.getSlab(volWeight, PackagingType.CARTON, "ABC");

        System.out.println("dto:" + dto);
        Assert.assertEquals(dto.getMostPreferredBox(volWeight), Property.PACKMAN_DEFAULT_RECO_AIR.getValue());
        Assert.assertTrue(dto.getRecommedationList().contains(Property.PACKMAN_DEFAULT_RECO_AIR.getValue()));

        sut.addCartonSlab(getSlab(0, 100, cartons("CB", 50, 80, 90, 100)));
        sut.addCartonSlab(getSlab(200, 100, cartons("CB", 250)));
        volWeight = Double.valueOf(100);
        dto = sut.getSlab(volWeight, PackagingType.CARTON, "ABC");

        Assert.assertEquals("l. limit  ", dto.getLowerLimit(), Integer.valueOf(0));
        Assert.assertEquals("vol. weight ", dto.getMostPreferredBox(volWeight), "CB4");

    }

    @Test
    public void testGetSlab_002() {
        sut.addCartonSlab(getSlab(0, 100, cartons("CB", 50, 80, 90, 100)));
        sut.addCartonSlab(getSlab(200, 100, cartons("CB")));
        sut.addCartonSlab(getSlab(300, 100, cartons("CB", 350)));
        Double volWeight = Double.valueOf(10);
        SlabCacheDTO dto = sut.getSlab(volWeight, PackagingType.CARTON, "ABC");

        Assert.assertEquals("l. limit  ", dto.getLowerLimit(), Integer.valueOf(0));
        Assert.assertEquals("vol. weight ", dto.getMostPreferredBox(volWeight), "CB1");

        volWeight = Double.valueOf(75);
        dto = sut.getSlab(volWeight, PackagingType.CARTON, "ABC");

        Assert.assertEquals("l. limit  ", dto.getLowerLimit(), Integer.valueOf(0));
        Assert.assertEquals("vol. weight ", dto.getMostPreferredBox(volWeight), "CB2");

        volWeight = Double.valueOf(90);
        dto = sut.getSlab(volWeight, PackagingType.CARTON, "ABC");

        Assert.assertEquals("l. limit  ", dto.getLowerLimit(), Integer.valueOf(0));
        Assert.assertEquals("vol. weight ", dto.getMostPreferredBox(volWeight), "CB3");

        volWeight = Double.valueOf(95);
        dto = sut.getSlab(volWeight, PackagingType.CARTON, "ABC");

        Assert.assertEquals("l. limit  ", dto.getLowerLimit(), Integer.valueOf(0));
        Assert.assertEquals("vol. weight ", dto.getMostPreferredBox(volWeight), "CB4");

        volWeight = Double.valueOf(255);
        dto = sut.getSlab(volWeight, PackagingType.CARTON, "ABC");
        //        Assert.assertEquals("l. limit  ", dto.getLowerLimit(), Integer.valueOf(201));
        Assert.assertEquals("vol. weight ", dto.getMostPreferredBox(volWeight), "CB5");

        volWeight = Double.valueOf(355);
        dto = sut.getSlab(volWeight, PackagingType.CARTON, "ABC");
        //        Assert.assertEquals("l. limit  ", dto.getLowerLimit(), Integer.valueOf(201));
        Assert.assertEquals("vol. weight ", dto.getMostPreferredBox(volWeight), "AIR_DEF");

        volWeight = Double.valueOf(100);
        dto = sut.getSlab(volWeight, PackagingType.CARTON, "ABC");
        //        Assert.assertEquals("l. limit  ", dto.getLowerLimit(), Integer.valueOf(0));
        Assert.assertEquals("vol. weight ", dto.getMostPreferredBox(volWeight), "CB4");

    }

    private CartonSlab getSlab(int lowerLmt, int slbSize, Set<CartonType> cartons) {
        CartonSlab s = new CartonSlab();
        s.setName("SLAB" + (++slabCount));
        s.setLowerLimit(lowerLmt);
        s.setSlabSize(slbSize);
        s.setCartonTypes(cartons);
        return s;
    }

    private Set<CartonType> cartons(String prefix, int... cartons) {
        if (cartons == null || cartons.length == 0) {
            return new HashSet<CartonType>();
        }
        Set<CartonType> cList = new HashSet<CartonType>();
        for (int i : cartons) {
            CartonType o = new CartonType();
            o.setCode(prefix + (++cartonCount));
            o.setVolWeight(Double.valueOf(i));
            cList.add(o);
        }
        return cList;
    }

}
