/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.packman.cache;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.snapdeal.base.annotations.Cache;
import com.snapdeal.base.utils.CollectionUtils;
import com.snapdeal.packman.configuration.PackagingProperty;
import com.snapdeal.packman.configuration.PackagingTypeItemProperty;
import com.snapdeal.packman.dto.PackagingTypeCacheDTO;
import com.snapdeal.packman.dto.PackagingTypeItemDTO;
import com.snapdeal.packman.dto.ThreeDimensionalPTItemDTO;
import com.snapdeal.packman.dto.TwoDimensionalPTItemDTO;
import com.snapdeal.packman.entity.PackagingType;
import com.snapdeal.packman.entity.PackagingTypeItem;
import com.snapdeal.packman.entity.PackagingTypeProperties;
import com.snapdeal.packman.enums.PackagingMode;
import com.snapdeal.packman.enums.PackagingTypeCategory;


/**
 * @version 1.0, 10-Sep-2015
 * @author ankur
 */

@Cache(name = "PackagingTypeCache")
public class PackagingTypeCache {
    
    private static final Logger LOG = LoggerFactory.getLogger(PackagingTypeCache.class);

    private Map<String, Map<String, List<PackagingTypeCacheDTO>>> storeToShippimgModePTMap   = new HashMap<String, Map<String, List<PackagingTypeCacheDTO>>>();

    private Map<String, List<PackagingTypeCacheDTO>>              typeToPackagingTypeDTOsMap = new HashMap<String, List<PackagingTypeCacheDTO>>();

    // [{SD,[Carton,DTO]}]
    private Map<String, Map<String,PackagingTypeCacheDTO>>        storeTopackagingTypeDTOMap = new HashMap<String, Map<String,PackagingTypeCacheDTO>>();
    
    public void addPackagingType(List<PackagingType> packagingList) {
        for (PackagingType pt : packagingList) {
            String storeCode = pt.getStore().getCode();
            PackagingTypeCacheDTO dto = new PackagingTypeCacheDTO(pt.getType(), pt.getPackagingMode(), pt.getDescription(), storeCode);

            Map<String, List<PackagingTypeCacheDTO>> packagingModeToPTMap = storeToShippimgModePTMap.get(storeCode);
            if (packagingModeToPTMap == null) {
                packagingModeToPTMap = new HashMap<String, List<PackagingTypeCacheDTO>>();
            }

            List<PackagingTypeCacheDTO> ptList = packagingModeToPTMap.get(pt.getPackagingMode());
            if (ptList == null) {
                ptList = new ArrayList<PackagingTypeCacheDTO>();
            }
            ptList.add(dto);

            packagingModeToPTMap.put(pt.getPackagingMode(), ptList);

            storeToShippimgModePTMap.put(storeCode, packagingModeToPTMap);

            List<PackagingTypeCacheDTO> packagingTypeCacheDTOList = typeToPackagingTypeDTOsMap.get(pt.getType());
            if (packagingTypeCacheDTOList == null) {
                packagingTypeCacheDTOList = new ArrayList<PackagingTypeCacheDTO>();
            }
            packagingTypeCacheDTOList.add(dto);
            typeToPackagingTypeDTOsMap.put(pt.getType(), packagingTypeCacheDTOList);
        }
    }

    @SuppressWarnings("unchecked")
    public List<PackagingTypeCacheDTO> getPackagingTypeForStoreAndPackagingMode(String storeCode, String packagingMode) {
        Map<String, List<PackagingTypeCacheDTO>> map = storeToShippimgModePTMap.get(storeCode);
        if (map != null) {
            return map.get(packagingMode);
        }

        return Collections.EMPTY_LIST;
    }

    public PackagingTypeCacheDTO getPackagingTypeDto(String storeCode, String packagingMode, String packagingType) {
        PackagingTypeCacheDTO result = null;
        if (null == storeCode || null == packagingMode || null == packagingType)
            return null;
        Map<String, List<PackagingTypeCacheDTO>> packagingModeToPTMap = storeToShippimgModePTMap.get(storeCode);
        if (null == packagingModeToPTMap) {
            return null;
        }
        List<PackagingTypeCacheDTO> ptList = packagingModeToPTMap.get(packagingMode);
        if (null == ptList || ptList.size() == 0) {
            return null;
        }
        for (PackagingTypeCacheDTO dto : ptList) {
            if (packagingType.equals(dto.getType()))
                result = dto;
        }
        return result;
    }

    public PackagingTypeCacheDTO getPackagingTypeByTypeAndStore(String type, String storeCode) {
        if (null == storeCode || type == null)
        {
            return null;
        }
        List<PackagingTypeCacheDTO> dtoList = typeToPackagingTypeDTOsMap.get(type);
        if(dtoList!=null){
            for (PackagingTypeCacheDTO dto : dtoList) {
                if (storeCode.equals(dto.getStoreCode())) {
                    return dto;
                }
            }
        }
        return null;
    }

    public void addPackagingTypeByPTI(List<PackagingTypeItem> typeItemList) {
        storeTopackagingTypeDTOMap.clear();
        if(typeItemList!=null){
            for(PackagingTypeItem item : typeItemList){
                String storeCode = item.getPackagingType().getStore().getCode(); 
                PackagingType pt = item.getPackagingType();
                String packagingTypeCode = pt.getType();
                Map<String,PackagingTypeCacheDTO> innerMap = storeTopackagingTypeDTOMap.get(storeCode);
                if(innerMap==null){
                    innerMap = new HashMap<String, PackagingTypeCacheDTO>();
                    storeTopackagingTypeDTOMap.put(storeCode, innerMap);
                }
                PackagingTypeCacheDTO dto = innerMap.get(packagingTypeCode);
                if(dto==null){
                    dto = new PackagingTypeCacheDTO(packagingTypeCode,pt.getPackagingMode(),pt.getDescription(),storeCode);
                    innerMap.put(packagingTypeCode, dto);
                }
                PackagingTypeItemDTO itemDto = null;
                if(pt.getPackagingMode().equals(PackagingMode.NORMAL.mode())){
                    Double length = Double.parseDouble(item.getPackagingTypeItemProperty(PackagingTypeItemProperty.LENGTH));
                    Double breadth = Double.parseDouble(item.getPackagingTypeItemProperty(PackagingTypeItemProperty.BREADTH));
                    if(PackagingTypeCategory.TWO_DIMENSIONAL.type().equals(getDimension(pt))){
                        itemDto = new TwoDimensionalPTItemDTO(length,breadth,item.getCode(),pt.getPackagingMode(),item.getDescription());
                    } else if(PackagingTypeCategory.THREE_DIMENSIONAL.type().equals(getDimension(pt))){
                        Double height = Double.parseDouble(item.getPackagingTypeItemProperty(PackagingTypeItemProperty.HEIGHT));
                        Double volWeight = Double.parseDouble(item.getPackagingTypeItemProperty(PackagingTypeItemProperty.VOL_WEIGHT));
                        itemDto = new ThreeDimensionalPTItemDTO(length,breadth,height,volWeight,item.getCode(),pt.getPackagingMode(),item.getDescription());
                    }
                } else {
                    itemDto = new PackagingTypeItemDTO(item.getCode(),pt.getPackagingMode(),item.getDescription());
                }
                dto.getPackagingTypeItems().add(itemDto);
            }
            sortBoxes();
            LOG.debug("Boxes Cache {}",storeTopackagingTypeDTOMap.toString());
        }
    }
    
    private void sortBoxes() {
        for(Entry<String, Map<String, PackagingTypeCacheDTO>> outer : storeTopackagingTypeDTOMap.entrySet()){
            for(Entry<String, PackagingTypeCacheDTO> inner : outer.getValue().entrySet()){
                
                List<PackagingTypeItemDTO> packagingTypeItems = inner.getValue().getPackagingTypeItems();
                if(!CollectionUtils.isEmpty(packagingTypeItems)){
                    PackagingTypeItemDTO packagingTypeItemDTO = packagingTypeItems.get(0);
                    if(packagingTypeItemDTO instanceof ThreeDimensionalPTItemDTO){
                        List<ThreeDimensionalPTItemDTO> list = new ArrayList<ThreeDimensionalPTItemDTO>();
                        for(PackagingTypeItemDTO p : packagingTypeItems){
                            list.add((ThreeDimensionalPTItemDTO)p);
                        }
                        Collections.sort(list);
                        inner.getValue().setPackagingTypeItems(new ArrayList<PackagingTypeItemDTO>(list));
                    }
                    
                    if(packagingTypeItemDTO instanceof TwoDimensionalPTItemDTO){
                        List<TwoDimensionalPTItemDTO> list = new ArrayList<TwoDimensionalPTItemDTO>();
                        for(PackagingTypeItemDTO p : packagingTypeItems){
                            list.add((TwoDimensionalPTItemDTO)p);
                        }
                        Collections.sort(list);
                        inner.getValue().setPackagingTypeItems(new ArrayList<PackagingTypeItemDTO>(list));
                    }
                }
                
            }
        }
    }

    public PackagingTypeCacheDTO  getPackagingTypeDtoV1(String storeCode,String packagingTypeCode){
        PackagingTypeCacheDTO dto = null ;
        if(storeTopackagingTypeDTOMap.get(storeCode)!=null){
            dto = storeTopackagingTypeDTOMap.get(storeCode).get(packagingTypeCode);
        }
        if(dto==null){
            dto = new PackagingTypeCacheDTO();
        }
        return dto;
    }

    private String getDimension(PackagingType pt) {
        List<PackagingTypeProperties> prop = pt.getProperties();
        for(PackagingTypeProperties p : prop){
            if(p.getName().equals(PackagingProperty.PACKAGING_TYPE_CATEGORY.mode())){
                return p.getValue();
            }
        }
        return null;
    }

}
