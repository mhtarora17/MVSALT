/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *  
 *  @version     1.0, 01-Dec-2015
 *  @author brijesh
 */
public class RolesDTO {
    
    private String userMailId;
    
    private Map<String,List<String>> storeWiseRoles  = new HashMap<String,List<String>>();

    public String getUserMailId() {
        return userMailId;
    }

    public void setUserMailId(String userMailId) {
        this.userMailId = userMailId;
    }

    public Map<String, List<String>> getStoreWiseRoles() {
        return storeWiseRoles;
    }

    public void setStoreWiseRoles(Map<String, List<String>> storeWiseRoles) {
        this.storeWiseRoles = storeWiseRoles;
    }
    
    public void addRoles(String key,String value) {
        List<String> list = storeWiseRoles.get(key);
        if(list == null){
            list = new ArrayList<String>();
            storeWiseRoles.put(key, list);
        }
        list.add(value);
    }

    @Override
    public String toString() {
        return "RolesDTO [userMailId=" + userMailId + ", storeWiseRoles=" + storeWiseRoles + "]";
    }

}
