/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.snapdeal.packman.dao.IPackmanRulesDao;

/**
 *  
 *  @version     1.0, 23-Dec-2015
 *  @author brijesh
 */

@Repository("PackmanRulesDao")
public class IPackmanRulesDaoImpl implements IPackmanRulesDao{

    @Autowired
    private SessionFactory sessionFactory;
    
}
