/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.packman.dao.IUrlPatternDao;
import com.snapdeal.packman.entity.UrlPattern;
import com.snapdeal.packman.entity.UrlRoleMapping;

/**
 *  
 *  @version     1.0, 07-Dec-2015
 *  @author indrajit
 */
@Repository("urlPatternDaoImpl")
@Transactional
public class UrlPatternDaoImpl implements IUrlPatternDao{
    
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @Transactional
    public UrlPattern persistURLPattern(UrlPattern urlPattern) {
	urlPattern.setLastUpdated(DateUtils.getCurrentTime());
	urlPattern.setCreated(DateUtils.getCurrentTime());
	return (UrlPattern) sessionFactory.getCurrentSession().merge(urlPattern);
	
    }

    @Override
    @Transactional
    public UrlRoleMapping persistURLRoleMapping(UrlRoleMapping urlRoleMapping) {
	urlRoleMapping.setLastUpdated(DateUtils.getCurrentTime());
	urlRoleMapping.setCreated(DateUtils.getCurrentTime());
	return (UrlRoleMapping) sessionFactory.getCurrentSession().merge(urlRoleMapping);
	
    }

    @Override
    @Transactional
    public List<UrlPattern> getAllUrlPattern() {
	 Query q = sessionFactory.getCurrentSession().createQuery("from UrlPattern ");
	 return q.list();
    }

}
