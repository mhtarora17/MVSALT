package com.snapdeal.packman.cache;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.snapdeal.base.annotations.Cache;
import com.snapdeal.packman.dto.PincodeCityStateTupleDTO;
import com.snapdeal.score.sro.PostalCodeSRO;

@Cache(name = "pincodeCityStateMappingCache")
public class PincodeCityStateMappingCache {

    private Map<String, List<PincodeCityStateTupleDTO>> pincodeToCityStateMapping = new HashMap<String, List<PincodeCityStateTupleDTO>>();

    public List<PincodeCityStateTupleDTO> getMapping(String pincode) {
        return pincodeToCityStateMapping.get(pincode);
    }

    public void addMappings(List<PostalCodeSRO> postalCodeSROs) {
        int size = 16; //java.util.HashMap.DEFAULT_INITIAL_CAPACITY
        if (postalCodeSROs != null) {
            size = postalCodeSROs.size();
        }
        Map<String, List<PincodeCityStateTupleDTO>> pincodeToCityStateMapping = new HashMap<String, List<PincodeCityStateTupleDTO>>(size);
        for (PostalCodeSRO o : postalCodeSROs) {
            List<PincodeCityStateTupleDTO> dtos = pincodeToCityStateMapping.get(o.getPincode());
            if (dtos == null) {
                dtos = new ArrayList<PincodeCityStateTupleDTO>();
                pincodeToCityStateMapping.put(o.getPincode(), dtos);
            }
            dtos.add(convert(o));
        }
        this.pincodeToCityStateMapping = pincodeToCityStateMapping;
    }

    private PincodeCityStateTupleDTO convert(PostalCodeSRO in) {
        return new PincodeCityStateTupleDTO(in);
    }

}
