/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.dao;

import java.util.List;

import com.snapdeal.packman.entity.UrlPattern;
import com.snapdeal.packman.entity.UrlRoleMapping;

/**
 *  
 *  @version     1.0, 07-Dec-2015
 *  @author indrajit
 */
public interface IUrlPatternDao {
    
    UrlPattern persistURLPattern(UrlPattern urlPattern);
    
    UrlRoleMapping persistURLRoleMapping(UrlRoleMapping urlRoleMapping);
    
    List<UrlPattern> getAllUrlPattern();

}
