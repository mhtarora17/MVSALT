/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.packman.cache;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.Set;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aerospike.client.Log;
import com.snapdeal.base.annotations.Cache;
import com.snapdeal.packman.configuration.PackagingTypeItemProperty;
import com.snapdeal.packman.dto.RecommendationParamsDTO;
import com.snapdeal.packman.dto.SlabCacheDTO;
import com.snapdeal.packman.dto.SlabCacheDTO.SlabItemDTO;
import com.snapdeal.packman.entity.PackagingTypeItem;
import com.snapdeal.packman.entity.PackagingTypeItemProperties;
import com.snapdeal.packman.entity.Slab;
import com.snapdeal.packman.enums.PackagingMode;
import com.snapdeal.packman.utils.PackmanConstants;

/**
 * @version 1.0, 07-Sep-2015
 * @author ankur
 */

@Cache(name = "SlabMappingCache")
@Deprecated
public class SlabMappingCache {

    private static final Logger                             LOG       = LoggerFactory.getLogger(SlabMappingCache.class);

    private Map<String, NavigableMap<Double, SlabCacheDTO>> slabCache = new HashMap<String, NavigableMap<Double, SlabCacheDTO>>();

    public void addSlab(Slab slab, Set<PackagingTypeItem> packagingTypeItems) {
        if (slab != null && slab.getLowerLimit() != null) {
            com.snapdeal.packman.entity.PackagingType packagingType = slab.getPackagingType();
            if (packagingType != null && packagingType.getStore() != null && PackagingMode.NORMAL.mode().equals(packagingType.getPackagingMode())) {
                SlabCacheDTO dto = new SlabCacheDTO();

                dto.setLowerLimit(slab.getLowerLimit()); // if slab is defined from 0(lower limit) to 500, it will contain boxes from 1 to 500[both inclusive]
                dto.setSlabSize(slab.getSlabSize());

                List<SlabItemDTO> boxes = new ArrayList<SlabItemDTO>();

                //TODO get packaging type items
                for (PackagingTypeItem item : packagingTypeItems) {
                    if (item != null) {
                        SlabItemDTO o = new SlabItemDTO();
                        o.setCode(item.getCode());
                        enrichSlabItemDto(item, o);
                        o.setDescription(item.getDescription());
                        boxes.add(o);
                    }
                }
                Collections.sort(boxes);
                dto.setBoxes(boxes);

                //put slab in cache map
                String key = prepareKey(packagingType.getStore().getCode(), packagingType.getType());
                NavigableMap<Double, SlabCacheDTO> lowerLimitToSlabMap = slabCache.get(key);
                if (lowerLimitToSlabMap == null) {
                    lowerLimitToSlabMap = new TreeMap<Double, SlabCacheDTO>();
                }

                lowerLimitToSlabMap.put(slab.getLowerLimit().doubleValue(), dto);// if slab is defined from 0(lower limit) to 500, it will contain boxes from 1 to 500[both inclusive]

                slabCache.put(key, lowerLimitToSlabMap);
            }
        }
    }

    private void enrichSlabItemDto(PackagingTypeItem item, SlabItemDTO o) {
        if (item != null) {
            List<PackagingTypeItemProperties> prop = item.getProperties();
            if (prop != null) {
                for (PackagingTypeItemProperties p : prop) {
                    if (p.getName().equals(PackagingTypeItemProperty.LENGTH.getName())) {
                        o.setLength(Double.parseDouble(p.getValue()));
                    } else if (p.getName().equals(PackagingTypeItemProperty.BREADTH.getName())) {
                        o.setBreadth(Double.parseDouble(p.getValue()));
                    } else if (p.getName().equals(PackagingTypeItemProperty.HEIGHT.getName())) {
                        o.setHeight(Double.parseDouble(p.getValue()));
                    } else if (p.getName().equals(PackagingTypeItemProperty.VOL_WEIGHT.getName())) {
                        o.setVolWeight(Double.parseDouble(p.getValue()));
                    }
                }
            }
        }
    }

    public SlabCacheDTO getSlab(RecommendationParamsDTO recoDto) {
        String key = prepareKey(recoDto.getStoreCode(), recoDto.getPackagingType());

        NavigableMap<Double, SlabCacheDTO> slabMap = slabCache.get(key);
        if (slabMap == null) {
            return null;
        }
        return getAppropriateSlab(recoDto, slabMap);

    }

    /**
     * @Logic Slabs are defined from x to y. where only y is inclusive. If there is no such slab for input volWeight ,
     *        we try to find next available higher slab.
     * @param volWeight
     * @param slabMap
     * @param supc
     * @param pauSRO
     * @return
     */
    private SlabCacheDTO getAppropriateSlab(RecommendationParamsDTO recoDto, NavigableMap<Double, SlabCacheDTO> slabMap) {

        SlabCacheDTO recommendedSlab = null;
        Entry<Double, SlabCacheDTO> slab = slabMap.lowerEntry(recoDto.getVolWeight());

        //compare it with very first key in map if no range founded.
        if (slab == null) {
            LOG.warn("for supc {} and vol. weight {} no slab is found, recommending SENTINAL", recoDto.getSupc(), recoDto.getVolWeight());
            recommendedSlab = SlabCacheDTO.DEFAULT_SLAB_SENTINAL;
        } else {
            SlabCacheDTO slabDto = slab.getValue();
            // if slab has at 
            if (slabDto.canSlabAdjustVolWeight(recoDto)) {
                LOG.info("for supc {} and vol. weight {} found slab {} ", recoDto.getSupc(), recoDto.getVolWeight(), slabDto);
                recommendedSlab = slabDto;
            } else {
                LOG.warn("for supc {} and vol. weight {} discarding slab {} as this can not adjust vol. weight; looking further", recoDto.getSupc(), recoDto.getVolWeight(), slabDto);

                // Find the next slab which contains atleast one box .
                Entry<Double, SlabCacheDTO> lastSlab = null;
                Entry<Double, SlabCacheDTO> nextHigherSlab = slabMap.ceilingEntry(recoDto.getVolWeight());
                while (nextHigherSlab != null && !nextHigherSlab.getValue().canSlabAdjustVolWeight(recoDto)) {
                    nextHigherSlab = slabMap.higherEntry(nextHigherSlab.getKey());
                    lastSlab = nextHigherSlab;
                }

                //If no slab found means input vol. weight is higher than all slabs, return default
                if (nextHigherSlab == null) {
                    //vol. weight is greater than maxslab range
                    LOG.error(PackmanConstants.BREACH_CASE + "for supc {}, vol. weight {} is greater than last slab {}, recommending SENTINAL", recoDto.getSupc(), recoDto.getVolWeight(), lastSlab);
                    recommendedSlab = SlabCacheDTO.DEFAULT_SLAB_SENTINAL;
                } else {
                    LOG.info("for supc {} and vol. weight {} found higher slab {} ", recoDto.getSupc(), recoDto.getVolWeight(), nextHigherSlab.getValue());
                    recommendedSlab = nextHigherSlab.getValue();
                }
            }
        }

        return recommendedSlab;
    }

    public static String prepareKey(String... subkeys) {
        if (subkeys == null)
            return null;

        StringBuilder key = new StringBuilder("");

        key.append(subkeys[0]);
        for (int i = 1; i < subkeys.length; i++) {
            key.append("-");
            key.append(subkeys[i]);
        }

        return key.toString();
    }

}
