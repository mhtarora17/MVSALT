/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.packman.cache;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.snapdeal.base.annotations.Cache;

/**
 * @version 1.0, 23-Dec-2015
 * @author indrajit
 */
@Cache(name = "storeFrontToStoreCodeMapCache")
public class StoreFrontToStoreCodeMapCache {

    private Map<String, String> storeFrontToStoreCodeMap = new HashMap<String, String>();
    
    private Map<String, List<String>> storeToLabelMap = new HashMap<String, List<String>>();

    public Map<String, String> getStoreFrontToStoreCodeMap() {
        return storeFrontToStoreCodeMap;
    }

    public void setStoreFrontToStoreCodeMap(Map<String, String> storeFrontToStoreCodeMap) {
        this.storeFrontToStoreCodeMap = storeFrontToStoreCodeMap;
    }
    
    
    public Map<String, List<String>> getStoreToLabelMap() {
        return storeToLabelMap;
    }

    public void setStoreToLabelMap(Map<String, List<String>> storeToLabelMap) {
        this.storeToLabelMap = storeToLabelMap;
    }

    public boolean isStoreValid(String storeCode) {
        return storeFrontToStoreCodeMap.containsValue(storeCode);
    }

}
