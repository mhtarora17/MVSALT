/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.cache;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.snapdeal.base.annotations.Cache;

/**
 *  
 *  @version     1.0, 07-Dec-2015
 *  @author indrajit
 */

@Cache(name = "urlRoleMappingCache")
public class UrlRoleMappingCache {
    
    private Map<String, List<String>> urlRoleMap = new HashMap<String, List<String>>();
    
    
    public void addMappings(String url, List<String> roleList){
	if(null != url && null != roleList){
	    this.urlRoleMap.put(url, roleList);
	}
    }

    public Map<String, List<String>> getUrlRoleMap() {
        return urlRoleMap;
    }

    public void setUrlRoleMap(Map<String, List<String>> urlRoleMap) {
        this.urlRoleMap = urlRoleMap;
    }

}
