/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.packman.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.packman.configuration.PackagingTypeItemProperty;
import com.snapdeal.packman.dao.IPackmanExclusivelyDao;
import com.snapdeal.packman.dto.RoleStoreMappingDto;
import com.snapdeal.packman.entity.PackagingType;
import com.snapdeal.packman.entity.PackagingTypeItem;
import com.snapdeal.packman.entity.PackagingTypeItemProperties;
import com.snapdeal.packman.entity.PackagingTypeProperties;
import com.snapdeal.packman.entity.RoleStoreMapping;
import com.snapdeal.packman.entity.Slab;
import com.snapdeal.packman.entity.Store;
import com.snapdeal.packman.entity.StoreProperties;
import com.snapdeal.packman.entity.UrlPattern;
import com.snapdeal.packman.entity.UrlRoleMapping;

/**
 * @version 1.0, 26-Nov-2015
 * @author indrajit
 */
@Repository("PackmanExclusivelyDao")
public class PackmanExclusivelyDaoImpl implements IPackmanExclusivelyDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Store persistStore(Store store) {
        return (Store) sessionFactory.getCurrentSession().merge(store);

    }

    @Override
    public PackagingType persistPackagingType(PackagingType packagingType) {
        return (PackagingType) sessionFactory.getCurrentSession().merge(packagingType);
    }

    @Override
    public PackagingTypeItem persistPackagingTypeItem(PackagingTypeItem packagingTypeItem) {
        PackagingTypeItem p = (PackagingTypeItem) sessionFactory.getCurrentSession().merge(packagingTypeItem);
        return p;
    }

    @Override
    public void persistPackagingTypeProperty(PackagingTypeProperties packagingTypeProperties) {
        sessionFactory.getCurrentSession().merge(packagingTypeProperties);
    }

    @Override
    public Store fetchStore(String storeCode) {
        Query q = sessionFactory.getCurrentSession().createQuery("FROM Store where code =:storeCode");
        q.setParameter("storeCode", storeCode);
        return (Store) q.uniqueResult();
    }

    @Override
    public PackagingType fetchPackagingType(String type, String storeCode) {
        Query q = sessionFactory.getCurrentSession().createQuery("FROM PackagingType where type =:type and store.code =:storeCode");
        q.setParameter("type", type);
        q.setParameter("storeCode", storeCode);
        return (PackagingType) q.uniqueResult();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Store> fetchAllStores() {
        Query q = sessionFactory.getCurrentSession().createQuery("FROM Store");
        return (List<Store>) q.list();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Store> fetchAllEnabledStores() {
        Query q = sessionFactory.getCurrentSession().createQuery("FROM Store where enabled = true");
        return (List<Store>) q.list();
    }

    @Override
    @Transactional
    public void persistRoleStoreMapping(List<RoleStoreMapping> list) {
        for (RoleStoreMapping entity : list)
            sessionFactory.getCurrentSession().merge(entity);
    }

    @Override
    public PackagingTypeItem getPackagingTypeItem(String code, int packagingTypeId) {
        Query q = sessionFactory.getCurrentSession().createQuery("From PackagingTypeItem where code =:code and packagingType.id =:packagingTypeId");
        q.setParameter("code", code);
        q.setParameter("packagingTypeId", packagingTypeId);
        return (PackagingTypeItem) q.uniqueResult();
    }

    @Override
    @Transactional
    @SuppressWarnings("unchecked")
    public List<PackagingType> getAllPackagingType(String storeCode) {
        Query q = sessionFactory.getCurrentSession().createQuery("From PackagingType where store.code =:storeCode");
        q.setParameter("storeCode", storeCode);
        return (List<PackagingType>) q.list();
    }

    @Override
    public PackagingType getPackagingType(String storeCode, String packagingTypeCode) {
        Query q = sessionFactory.getCurrentSession().createQuery("From PackagingType where type =:packagingTypeCode and store.code =:storeCode ");
        q.setParameter("packagingTypeCode", packagingTypeCode);
        q.setParameter("storeCode", storeCode);
        return (PackagingType) q.uniqueResult();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<PackagingTypeItem> getAllPackagingTypeItem(PackagingType packagingType) {
        Query q = sessionFactory.getCurrentSession().createQuery("From PackagingTypeItem where packagingType.id =:packagingTypeID");
        q.setParameter("packagingTypeID", packagingType.getId());
        return (List<PackagingTypeItem>) q.list();
    }

    @Override
    public void deletePackagingTypeItem(PackagingTypeItem packagingTypeItem) {
        sessionFactory.getCurrentSession().delete(packagingTypeItem);
    }

    @Override
    public RoleStoreMapping fetchRoleStoreMappingByRoleCode(String roleCode) {
        Query q = sessionFactory.getCurrentSession().createQuery("From RoleStoreMapping where roleCode =:roleCode and enabled = true");
        q.setParameter("roleCode", roleCode);
        return (RoleStoreMapping) q.uniqueResult();
    }

    @Override
    public PackagingTypeProperties getPackagingTypePropertyByPackagingType(String name, PackagingType packagingType) {
        Query q = sessionFactory.getCurrentSession().createQuery("From PackagingTypeProperties where name =:name and packagingType =:packagingType");
        q.setParameter("name", name);
        q.setParameter("packagingType", packagingType);
        return (PackagingTypeProperties) q.uniqueResult();
    }

    @Override
    public void persistRoleStoreMapping(RoleStoreMapping roleStoremap) {
        roleStoremap.setLastUpdated(DateUtils.getCurrentTime());
        roleStoremap.setCreated(DateUtils.getCurrentTime());
        sessionFactory.getCurrentSession().merge(roleStoremap);

    }

    @Override
    public void persistRoleStoreMapping(RoleStoreMappingDto dto) {
        RoleStoreMapping entity = new RoleStoreMapping();
        entity.setRoleCode(dto.getRoleCode());
        entity.setStoreCode(dto.getStoreCode());
        entity.setEnable(dto.isEnabled());
        sessionFactory.getCurrentSession().merge(entity);

    }

    @Override
    public void createPackagingTypeProperties(List<PackagingTypeProperties> packagingTypeProperties, PackagingType packagingType) {
        for (PackagingTypeProperties property : packagingTypeProperties) {
            sessionFactory.getCurrentSession().merge(property);
        }

    }

    @Override
    @SuppressWarnings("unchecked")
    public List<RoleStoreMapping> fetchRoleStoreMappingByStoreCode(String storeCode) {
        Query q = sessionFactory.getCurrentSession().createQuery("From RoleStoreMapping where storeCode =:storeCode");
        q.setParameter("storeCode", storeCode);
        return (List<RoleStoreMapping>) q.list();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<UrlRoleMapping> getAllUrlRoleMappingByRoleCodes(List<String> roleCodeList) {
        Query q = sessionFactory.getCurrentSession().createQuery("From UrlRoleMapping where role_code in (:roleCodeList)");
        q.setParameterList("roleCodeList", roleCodeList);
        return (List<UrlRoleMapping>) q.list();
    }

    @Override
    public void updateRole(List<String> roleList, boolean enabled) {
        for (String roleCode : roleList) {
            Query q = sessionFactory.getCurrentSession().createQuery("update Role set enabled =:enabled where code =:roleCode");
            q.setParameter("roleCode", roleCode);
            q.setParameter("enabled", enabled);
            q.executeUpdate();
        }
    }

    @Override
    public void updateRoleStoreMapping(String storeCode, boolean enabled) {
        Query q = sessionFactory.getCurrentSession().createQuery("update RoleStoreMapping set enabled =:enabled where storeCode =:storeCode");
        q.setParameter("storeCode", storeCode);
        q.setParameter("enabled", enabled);
        q.executeUpdate();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<UrlRoleMapping> getUrlRoleMappingByRoleCode(String code) {
        Query q = sessionFactory.getCurrentSession().createQuery("from UrlRoleMapping where role_code =:code and enabled = true");
        q.setParameter("code", code);
        return (List<UrlRoleMapping>) q.list();
    }

    @Override
    public UrlPattern getUrlByID(int id) {
        Query q = sessionFactory.getCurrentSession().createQuery("from UrlPattern where id=:id");
        q.setParameter("id", id);
        return (UrlPattern) q.uniqueResult();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<PackagingTypeItemProperties> getMaxPackagingTypeSize(int packagingTypeId) {
        Criteria cr = sessionFactory.getCurrentSession().createCriteria(PackagingTypeItemProperties.class);
        cr.add(Restrictions.eq("name", PackagingTypeItemProperty.VOL_WEIGHT.getName()));
        cr.createAlias("packagingTypeItem", "pti");
        cr.createAlias("pti.packagingType", "pt");
        cr.add(Restrictions.eq("pt.id", packagingTypeId));
        return cr.list();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Object[]> getUrlRoleStoreFromEmail(String email) {
        Query q = sessionFactory.getCurrentSession().createQuery(
                "select up.url,urm.role_code,rsm.storeCode from UrlPattern up,UrlRoleMapping urm,UserRole ur,User u,RoleStoreMapping rsm where up.id = urm.url_id and urm.role_code = ur.role.code and ur.user.id = u.id and u.email = '"
                        + email + "' and rsm.roleCode = ur.role.code and ur.enabled=true and rsm.enable=true");
        return (List<Object[]>) q.list();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<PackagingType> getAllEnabledPackagingType() {
        Query q = sessionFactory.getCurrentSession().createQuery("From PackagingType where enabled = true");
        return (List<PackagingType>) q.list();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<PackagingType> getAllEnabledPackagingTypeByStore(String storeCode) {
        Query q = sessionFactory.getCurrentSession().createQuery("From PackagingType where store.code =:storeCode and enabled = true");
        q.setParameter("storeCode", storeCode);
        return (List<PackagingType>) q.list();
    }

    @Override
    public Store fetchStoreByStoreFrontId(String id) {
        Query q = sessionFactory.getCurrentSession().createQuery("From Store where storeFrontId =:id");
        q.setParameter("id", id);
        return (Store) q.uniqueResult();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<PackagingType> getAllPackagingType() {
        Query q = sessionFactory.getCurrentSession().createQuery("From PackagingType");
        return (List<PackagingType>) q.list();
    }

    @Override
    public void saveStoreProperty(StoreProperties property) {
        sessionFactory.getCurrentSession().merge(property);
    }

    @Override
    public StoreProperties fetchStoreProperty(String storeCode, String name) {
        Query q = sessionFactory.getCurrentSession().createQuery("From StoreProperties where name =:name and store.code =:storeCode");
        q.setParameter("name", name);
        q.setParameter("storeCode", storeCode);
        return (StoreProperties) q.uniqueResult();
    }

    @Override
    public void deleteStoreProperty(StoreProperties property) {
        sessionFactory.getCurrentSession().delete(property);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<PackagingTypeProperties> getAllPackagingTypeProperties(PackagingType p) {
        Query q = sessionFactory.getCurrentSession().createQuery("From PackagingTypeProperties where packagingType.type =:type and packagingType.store.code =:storeCode");
        q.setParameter("type", p.getType());
        q.setParameter("storeCode", p.getStore().getCode());
        return (List<PackagingTypeProperties>) q.list();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Object[]> getStoreUrlMapping() {
        Query q = sessionFactory.getCurrentSession().createQuery(
                "select rsm.storeCode,up.url from RoleStoreMapping rsm,UrlPattern up,UrlRoleMapping urm where urm.role_code = rsm.roleCode and urm.url_id = up.id group by up.url,rsm.storeCode");
        return (List<Object[]>) q.list();
    }

    @Override
    public PackagingTypeItem getPackagingTypeItem(String type) {
        Query q = sessionFactory.getCurrentSession().createQuery("From PackagingTypeItem where code =:type");
        q.setParameter("type", type);
        return (PackagingTypeItem) q.uniqueResult();
    }

    @Override
    @Transactional(readOnly = true)
    public String getStoreCodeForRoleCode(String roleCode) {
        Query query = sessionFactory.getCurrentSession().createQuery("FROM RoleStoreMapping WHERE roleCode = :roleCode AND enabled = true");
        query.setParameter("roleCode", roleCode);
        RoleStoreMapping roleStoreMapping = (RoleStoreMapping) query.uniqueResult();
        return roleStoreMapping.getStoreCode();
    }

    @Override
    public void persistPackagingTypeItem(List<PackagingTypeItemProperties> properties) {
        for (PackagingTypeItemProperties prop : properties) {
            sessionFactory.getCurrentSession().merge(prop);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<PackagingTypeItemProperties> getPackagingTypeItemProperties(Integer id) {
        Query q = sessionFactory.getCurrentSession().createQuery("From PackagingTypeItemProperties where packagingTypeItem.id=:id");
        q.setParameter("id", id);
        return q.list();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<PackagingTypeItem> getAllPackagingTypeItem(int id) {
        Query q = sessionFactory.getCurrentSession().createQuery("From PackagingTypeItem where packagingType.id=:id");
        q.setParameter("id", id);
        return q.list();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<PackagingTypeItem> getAllPackagingTypeItem(){
        Query q = sessionFactory.getCurrentSession().createQuery("From PackagingTypeItem");
        return q.list();
    }
    
    @Override
    public void deletePackagingTypeItemProperties(List<PackagingTypeItemProperties> properties) {
        for(PackagingTypeItemProperties p : properties){
            sessionFactory.getCurrentSession().delete(p);
        }
    }

}
