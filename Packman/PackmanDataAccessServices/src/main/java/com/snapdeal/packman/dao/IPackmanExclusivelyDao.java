/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.packman.dao;

import java.util.List;

import com.snapdeal.packman.dto.RoleStoreMappingDto;
import com.snapdeal.packman.entity.PackagingType;
import com.snapdeal.packman.entity.PackagingTypeItem;
import com.snapdeal.packman.entity.PackagingTypeItemProperties;
import com.snapdeal.packman.entity.PackagingTypeProperties;
import com.snapdeal.packman.entity.RoleStoreMapping;
import com.snapdeal.packman.entity.Slab;
import com.snapdeal.packman.entity.Store;
import com.snapdeal.packman.entity.StoreProperties;
import com.snapdeal.packman.entity.UrlPattern;
import com.snapdeal.packman.entity.UrlRoleMapping;

/**
 * This DAO class is for Packman Exclusively.
 * 
 * @version 1.0, 26-Nov-2015
 * @author indrajit
 */
public interface IPackmanExclusivelyDao {

    Store persistStore(Store store);

    Store fetchStore(String storeCode);

    List<Store> fetchAllStores();
    
    List<Store> fetchAllEnabledStores();

    PackagingType persistPackagingType(PackagingType packagingType);

    PackagingType fetchPackagingType(String type, String storeCode);

    List<PackagingType> getAllPackagingType(String storeCode);

    PackagingType getPackagingType(String storeCode, String packagingTypeCode);

    PackagingTypeItem persistPackagingTypeItem(PackagingTypeItem packagingTypeItem);

    PackagingTypeItem getPackagingTypeItem(String code, int packagingTypeId);

    List<PackagingTypeItem> getAllPackagingTypeItem(PackagingType packagingType);

    void deletePackagingTypeItem(PackagingTypeItem packagingTypeItem);

    void persistPackagingTypeProperty(PackagingTypeProperties packagingTypeProperties);

    PackagingTypeProperties getPackagingTypePropertyByPackagingType(String name, PackagingType packagingType);

    void createPackagingTypeProperties(List<PackagingTypeProperties> packagingTypeProperties, PackagingType packagingType);

    RoleStoreMapping fetchRoleStoreMappingByRoleCode(String roleCode);

    void persistRoleStoreMapping(RoleStoreMappingDto dto);

    List<RoleStoreMapping> fetchRoleStoreMappingByStoreCode(String storeCode);

    void persistRoleStoreMapping(RoleStoreMapping roleStoremap);

    void persistRoleStoreMapping(List<RoleStoreMapping> list);

    List<UrlRoleMapping> getAllUrlRoleMappingByRoleCodes(List<String> roleCodeList);

    List<UrlRoleMapping> getUrlRoleMappingByRoleCode(String code);

    UrlPattern getUrlByID(int id);

    void updateRole(List<String> roleList, boolean enablement);

    void updateRoleStoreMapping(String storeCode, boolean enabled);
    
    List<PackagingTypeItemProperties> getMaxPackagingTypeSize(int packagingTypeId);
    
    List<Object[]> getUrlRoleStoreFromEmail(String email);

    List<PackagingType> getAllEnabledPackagingType();

    Store fetchStoreByStoreFrontId(String id);

    List<PackagingType> getAllEnabledPackagingTypeByStore(String storeCode);

    List<PackagingType> getAllPackagingType();

    void saveStoreProperty(StoreProperties property);

    StoreProperties fetchStoreProperty(String storeCode, String name);

    void deleteStoreProperty(StoreProperties property);

    List<PackagingTypeProperties> getAllPackagingTypeProperties(PackagingType p);

    List<Object[]> getStoreUrlMapping();
    
    String getStoreCodeForRoleCode(String roleCode);


    PackagingTypeItem getPackagingTypeItem(String type);

    void persistPackagingTypeItem(List<PackagingTypeItemProperties> properties);

    List<PackagingTypeItemProperties> getPackagingTypeItemProperties(Integer id);

    List<PackagingTypeItem> getAllPackagingTypeItem(int packagingTypeId);

    void deletePackagingTypeItemProperties(List<PackagingTypeItemProperties> properties);

    List<PackagingTypeItem> getAllPackagingTypeItem();

}
