package com.snapdeal.packman.dto;

import java.io.Serializable;

import com.snapdeal.score.sro.PostalCodeSRO;

/**
 * @author shiv
 */
public class PincodeCityStateTupleDTO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1780926199906642959L;

    private String            pincode;
    private String            city;
    private String            state;

    public PincodeCityStateTupleDTO() {
    }

    public PincodeCityStateTupleDTO(PostalCodeSRO in) {
        setPincode(in.getPincode());
        setCity(in.getCity());
        setState(in.getState());
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "PincodeCityStateTupleDTO [pincode=" + pincode + ", city=" + city + ", state=" + state + "]";
    }

}
