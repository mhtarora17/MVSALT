package com.snapdeal.packman.cache;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.snapdeal.base.annotations.Cache;
import com.snapdeal.score.sro.ShippingProviderDetailsSRO;

@Cache(name = "shippingProviderCache")
public class ShippingProviderCache {

    private Map<String, Boolean> shippingProviderToSpecialPackagingCache = new HashMap<String, Boolean>();

    public Boolean getMapping(String courierCode) {
        return shippingProviderToSpecialPackagingCache.get(courierCode);
    }

    public void addMappings(List<ShippingProviderDetailsSRO> shippingProviderSROs) {
        int size = 16; //java.util.HashMap.DEFAULT_INITIAL_CAPACITY
        if (shippingProviderSROs != null) {
            size = shippingProviderSROs.size();
        }
        Map<String, Boolean> cache = new HashMap<String, Boolean>(size);
        for (ShippingProviderDetailsSRO o : shippingProviderSROs) {
            cache.put(o.getCode(), o.isSpecialPackagingRequired());
        }
        this.shippingProviderToSpecialPackagingCache = cache;
    }

}
