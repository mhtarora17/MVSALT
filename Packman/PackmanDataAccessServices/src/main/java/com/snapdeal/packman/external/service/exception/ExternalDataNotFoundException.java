package com.snapdeal.packman.external.service.exception;

public class ExternalDataNotFoundException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public enum Type {
        DEFAULT("default");

        private String code;

        private Type(String code) {
            this.code = code;
        }

        public String code() {
            return this.code;
        }
    }

    Type errorType;

    public ExternalDataNotFoundException(Type type, String message) {
        super(message);
        errorType = type;
    }

    public ExternalDataNotFoundException(String message) {
        super(message);
    }

    public Type getErrorType() {
        return errorType;
    }

    public void setErrorType(Type errorType) {
        this.errorType = errorType;
    }

}
