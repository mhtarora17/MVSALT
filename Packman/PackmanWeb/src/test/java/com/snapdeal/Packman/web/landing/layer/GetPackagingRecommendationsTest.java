/**
 *  Copyright 2016 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
 *  
 *  @version     1.0, 20-Jan-2016
 *  @author vikassharma03
 */
package com.snapdeal.Packman.web.landing.layer;

import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;

import javax.naming.OperationNotSupportedException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.test.util.ReflectionTestUtils;

import com.snapdeal.base.cache.CacheManager;
import com.snapdeal.base.transport.service.ITransportService.Protocol;
import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.catalog.base.model.GetPOGDetailListBySUPCListRequest;
import com.snapdeal.catalog.base.model.GetPOGDetailListResponse;
import com.snapdeal.catalog.base.sro.POGDetailSRO;
import com.snapdeal.catalog.base.sro.POGDetailSRO.ProductBrandDetailSRO;
import com.snapdeal.cocofs.access.IProductAttributeService;
import com.snapdeal.cocofs.configuration.CocofsPropertiesCache;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.db.dao.IStartupDao;
import com.snapdeal.cocofs.enums.Labels;
import com.snapdeal.cocofs.external.service.ICAMSExternalService;
import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.external.service.factory.ExternalServiceFactory;
import com.snapdeal.cocofs.external.service.factory.IExternalServiceFactory;
import com.snapdeal.cocofs.generic.persister.exception.GenericPersisterException;
import com.snapdeal.cocofs.model.request.GetProductFulfilmentAttributeRequest;
import com.snapdeal.cocofs.mongo.CocofsMongoQuery;
import com.snapdeal.cocofs.mongo.Criteria;
import com.snapdeal.cocofs.mongo.mao.IGenericMao;
import com.snapdeal.cocofs.mongo.model.ProductAttributeUnit;
import com.snapdeal.cocofs.sro.ProductFulfilmentAttributeSRO;
import com.snapdeal.packman.cache.PackagingTypeCache;
import com.snapdeal.packman.cache.SlabMappingCache;
import com.snapdeal.packman.cache.StoreFrontToStoreCodeMapCache;
import com.snapdeal.packman.common.ApiErrorCode;
import com.snapdeal.packman.common.sro.PackagableItemSRO;
import com.snapdeal.packman.common.sro.PackageDetailSRO;
import com.snapdeal.packman.common.sro.PackagingRecommendationSRO;
import com.snapdeal.packman.dataaccess.supcpt.ISupcPackagingTypeMongoService;
import com.snapdeal.packman.dto.PackagingTypeCacheDTO;
import com.snapdeal.packman.dto.RecommendationParamsDTO;
import com.snapdeal.packman.dto.SlabCacheDTO;
import com.snapdeal.packman.dto.SlabCacheDTO.SlabItemDTO;
import com.snapdeal.packman.entity.PackagingType;
import com.snapdeal.packman.entity.Store;
import com.snapdeal.packman.enums.PackagingRuleOrder;
import com.snapdeal.packman.mongo.model.CriteriaUnit;
import com.snapdeal.packman.mongo.model.PackmanRuleUnit;
import com.snapdeal.packman.request.GetPackagingRecommendationsRequest;
import com.snapdeal.packman.response.GetPackagingRecommendationsResponse;
import com.snapdeal.packman.services.exception.PackagingRecomendationException;
import com.snapdeal.packman.services.external.ICocofsExternalService;
import com.snapdeal.packman.services.packaging.IPackmanExclusiveService;
import com.snapdeal.packman.services.packaging.dto.CatageoryEvaluationParamDto;
import com.snapdeal.packman.services.packaging.impl.PackmanExclusiveServiceImpl;
import com.snapdeal.packman.services.packaging.recomendation.IPackagingRecommendationService;
import com.snapdeal.packman.services.packaging.recomendation.impl.PackagingRecomendationService;
import com.snapdeal.packman.services.packaging.rule.engine.IPackmanPackagingRuleEngineService;
import com.snapdeal.packman.services.packaging.rule.engine.impl.PackmanPackagingRuleEngineServiceImpl;
import com.snapdeal.packman.web.builder.IResponseBuilder;
import com.snapdeal.packman.web.builder.impl.ResponseBuilderImpl;
import com.snapdeal.packman.web.landing.layer.IPackmanLandingService;
import com.snapdeal.packman.web.landing.layer.IRequestValidationHelper;
import com.snapdeal.packman.web.landing.layer.IRequestValidationService;
import com.snapdeal.packman.web.landing.layer.impl.PackmanLandingServiceImpl;
import com.snapdeal.packman.web.landing.layer.impl.PackmanLandingServiceImpl.IDependency;
import com.snapdeal.packman.web.landing.layer.impl.RequestValidationHelper;
import com.snapdeal.packman.web.landing.layer.impl.RequestValidationServiceImpl;
import com.snapdeal.rule.engine.entity.Block;
import com.snapdeal.rule.engine.entity.BlockParams;
import com.snapdeal.rule.engine.services.IRulesService;

public class GetPackagingRecommendationsTest {
    IDependency                                                                                                mDependencyProcessor             = null;
    IPackmanLandingService                                                                                     packmanLandingService            = null;
    IRequestValidationService                                                                                  mrequestValidationService        = null;
    IPackagingRecommendationService                                                                            mPackagingRecommendationService;
    PackageDetailSRO                                                                                           packageDetailSRO;
    PackagableItemSRO                                                                                          packagableItemSRO;
    List<PackagableItemSRO>                                                                                    lPackagableItemSRO;
    CocofsPropertiesCache                                                                                      cocofsPropertiesCache;
    ConfigUtils                                                                                                configUtils;
    ApiErrorCode                                                                                               apiErrorCode;
    PackagingRecommendationSRO                                                                                 packagingRecommendationSRO;
    PackagingRecomendationService                                                                              packagingRecomendationService    = null;
    com.snapdeal.packman.web.landing.layer.impl.PackmanLandingServiceImpl.IDependency                          mDependencyCoCo                  = null;
    com.snapdeal.packman.services.packaging.recomendation.impl.PackagingRecomendationService.IDependency       mDependencyCoCoForPRS            = null;
    com.snapdeal.packman.services.packaging.rule.engine.impl.PackmanPackagingRuleEngineServiceImpl.IDependency mDepedencyForPPRES               = null;
    com.snapdeal.packman.dto.RecommendationParamsDTO.IDependency                                               mDmDepedencyForRecommendationDto = null;
    com.snapdeal.packman.dto.SlabCacheDTO.IDependency                                                          mDepedencyForDTO                 = null;
    List<PackagingRecommendationSRO>                                                                           lPackagingRecommendationSRO      = null;
    IResponseBuilder                                                                                           responseBuilder                  = null;
    GetPackagingRecommendationsResponse                                                                        response                         = null;

    ProductFulfilmentAttributeSRO                                                                              pauSRO;
    ICocofsExternalService                                                                                     mcocofsExternalService           = null;
    IProductAttributeService                                                                                   productAttributeService          = null;
    ProductAttributeUnit                                                                                       productAttributeUnit             = null;
    IPackmanPackagingRuleEngineService                                                                         mruleEngineService               = null;
    PackagingRuleOrder                                                                                         mPackagingRuleOrder;
    ISupcPackagingTypeMongoService                                                                             mSupcPackagingTypeMongoService   = null;
    IRequestValidationHelper                                                                                   mrequestValidationHelper         = null;
    IRulesService                                                                                              mruleService                     = null;
    IGenericMao                                                                                                genericMao                       = null;
    List<PackmanRuleUnit>                                                                                      ruleList                         = null;
    SlabCacheDTO                                                                                               recommmendedSlab                 = null;
    BlockParams                                                                                                mblockParams                     = null;
    Block                                                                                                      mBlock                           = null;
    IExternalServiceFactory                                                                                    mExternalServiceFactory          = null;
    IPackmanExclusiveService                                                                                   mexclusiveService                = null;
    IStartupDao                                                                                                mstartupDao                      = null;

    @Before
    public void setUp() throws OperationNotSupportedException, GenericPersisterException, PackagingRecomendationException {

        Map<String, String> map = new HashMap<String, String>();
        map.put("SD", "Snapdeal");

        mrequestValidationHelper = new RequestValidationHelper();
        packmanLandingService = new PackmanLandingServiceImpl();

        StoreFrontToStoreCodeMapCache cache = new StoreFrontToStoreCodeMapCache();

        cache.setStoreFrontToStoreCodeMap(map);

        CacheManager.getInstance().setCache(cache);

        mPackagingRecommendationService = new PackagingRecomendationService();

        responseBuilder = new ResponseBuilderImpl();
        ReflectionTestUtils.setField(packmanLandingService, "responseBuilder", responseBuilder);
        packagingRecommendationSRO = new PackagingRecommendationSRO();
        packagingRecommendationSRO.setPackagingMode("normal");
        lPackagingRecommendationSRO = new ArrayList<PackagingRecommendationSRO>();
        lPackagingRecommendationSRO.add(packagingRecommendationSRO);
        lPackagableItemSRO = new ArrayList<PackagableItemSRO>();

        packageDetailSRO = new PackageDetailSRO();

        mrequestValidationService = new RequestValidationServiceImpl();
        ReflectionTestUtils.setField(packmanLandingService, "requestValidationService", mrequestValidationService);

        ReflectionTestUtils.setField(mrequestValidationService, "requestValidationHelper", mrequestValidationHelper);

        mDependencyCoCo = mock(com.snapdeal.packman.web.landing.layer.impl.PackmanLandingServiceImpl.IDependency.class);
        when(mDependencyCoCo.getIntegerScalar(Property.PACKMAN_API_BATCH_SIZE)).thenReturn(Integer.parseInt("10"));
        when(mDependencyCoCo.getStringScalar(Property.PACKAGING_RECOMMENDATION_API_KEY)).thenReturn("p@(km@n");
        ReflectionTestUtils.setField(packmanLandingService, "dep", mDependencyCoCo);

        mDependencyCoCoForPRS = mock(com.snapdeal.packman.services.packaging.recomendation.impl.PackagingRecomendationService.IDependency.class);
        when(mDependencyCoCoForPRS.getStringScalar(Property.SNAPDEAL_STORE_CODE)).thenReturn("Snapdeal");
        when(mDependencyCoCoForPRS.getBooleanScalar(Property.PACKMAN_PACKAGING_RULE_EVALUATION_REQ)).thenReturn(true);
        when(mDependencyCoCoForPRS.getBooleanScalar(Property.USE_PRIMARY_LBH)).thenReturn(false);
        ReflectionTestUtils.setField(mPackagingRecommendationService, "dep", mDependencyCoCoForPRS);

        mcocofsExternalService = mock(ICocofsExternalService.class);

        List<String> OrderList = new ArrayList<String>();
        OrderList.add("supclevel");
        OrderList.add("subcatagorylevel");
        OrderList.add("catagorylevel");
        OrderList.add("storelevel");

        mruleEngineService = new PackmanPackagingRuleEngineServiceImpl();
        mDepedencyForPPRES = mock(com.snapdeal.packman.services.packaging.rule.engine.impl.PackmanPackagingRuleEngineServiceImpl.IDependency.class);
        when(mDepedencyForPPRES.getStringList(Property.PACKAGING_RULE_EXECUTION_ORDER)).thenReturn(OrderList);
        ReflectionTestUtils.setField(mruleEngineService, "dep", mDepedencyForPPRES);

        ReflectionTestUtils.setField(mPackagingRecommendationService, "ruleEngineService", mruleEngineService);

        mDmDepedencyForRecommendationDto = mock(com.snapdeal.packman.dto.RecommendationParamsDTO.IDependency.class);
        when(mDmDepedencyForRecommendationDto.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_LBH)).thenReturn(false);
        when(mDmDepedencyForRecommendationDto.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_DELTA_VOL_WT)).thenReturn(false);
        when(mDmDepedencyForRecommendationDto.getDoubleScalar(Property.PACKMAN_RECO_LBH_MARGIN)).thenReturn(0.00);
        when(mDmDepedencyForRecommendationDto.getDoubleScalar(Property.PACKMAN_RECO_DELTA_VOL_WT_MULT_FACTOR)).thenReturn(2.00);
        when(mDmDepedencyForRecommendationDto.getDoubleScalar(Property.PACKMAN_RECO_DELTA_VOL_WT_OFFSET)).thenReturn(1000.00);

        doAnswer(new Answer<RecommendationParamsDTO>() {
            public RecommendationParamsDTO answer(InvocationOnMock invocation) {
                return new RecommendationParamsDTO(mDmDepedencyForRecommendationDto);
            }
        }).when(mDependencyCoCoForPRS).createRecParamDTO();
        

    }

    @After
    public void tearDown() {
        packmanLandingService = null;
        packageDetailSRO = null;
        mDependencyCoCo = null;
        responseBuilder = null;
        mrequestValidationHelper = null;
        mPackagingRecommendationService = null;
        mruleEngineService = null;
        mrequestValidationService = null;
        packagingRecommendationSRO = null;
        mrequestValidationService = null;
    }

    //test 24
    @Test
    public void testGetPackagingRecommendationsPicknpackAtSubcatLabelAndBrandBothExist() throws PackagingRecomendationException, ExternalDataNotFoundException ,NullPointerException
    {
    
        List<String> labels = new ArrayList<String>();
        labels.add(Labels.Premium.toString());
        mExternalServiceFactory = mock(ExternalServiceFactory.class);
        ICAMSExternalService miCAMSExternalService = mock(ICAMSExternalService.class);
        GetPOGDetailListResponse response = new GetPOGDetailListResponse();
        POGDetailSRO pogDetailSROs = new POGDetailSRO();
        pogDetailSROs.setEnabled(true);
    
        List<POGDetailSRO> lpogDetailSROs = new ArrayList<POGDetailSRO>();
        lpogDetailSROs.add(pogDetailSROs);
        response.setPogDetailSROs(lpogDetailSROs);
        when(mExternalServiceFactory.getCAMSExternalService()).thenReturn(miCAMSExternalService);
        when(miCAMSExternalService.getPOGDetailsBbySupc(Matchers.<GetPOGDetailListBySUPCListRequest> any())).thenReturn(response);
        ReflectionTestUtils.setField(mruleEngineService, "factory", mExternalServiceFactory);

        PackagableItemSRO packagableItemSRO = createPackagableItemSRO(labels);
        lPackagableItemSRO.add(packagableItemSRO);
        packageDetailSRO.setPackagableItemList(lPackagableItemSRO);
        genericMao = mock(IGenericMao.class);
        ruleList = new ArrayList<>();
        List<CriteriaUnit> lCriteriaUnit = new ArrayList<CriteriaUnit>();        
        CriteriaUnit criteriaUnit = createCriteriaUnit(1,"LABELS","premium");
        lCriteriaUnit.add(criteriaUnit);
        
        PackmanRuleUnit packmanRuleUnit = createPackmanRuleUnit(2,"ru03",7,"SURFACE_B", lCriteriaUnit);
        ruleList.add(packmanRuleUnit);
        doAnswer(new Answer<List<PackmanRuleUnit>>() {
            public List<PackmanRuleUnit> answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                CocofsMongoQuery cocofsMongoQuery = (CocofsMongoQuery) args[0];
                List<Criteria> lCriteria = cocofsMongoQuery.getCriteria();
                Criteria criteria1 = lCriteria.get(0);
                String rule_type = (String) criteria1.getObj();
                if (rule_type.equals("subcategory-packaging"))
                    return ruleList;
                return null;
    
            }
    
        }).when(genericMao).getDocumentList(Matchers.<CocofsMongoQuery> any(), Matchers.<Class<PackmanRuleUnit>> any());
        ReflectionTestUtils.setField(mruleEngineService, "genericMao", genericMao);
    
        PackmanPackagingRuleEngineServiceImpl packmanPackagingRuleEngineServiceImpl = Mockito.spy(new PackmanPackagingRuleEngineServiceImpl());
        Map<String, Object> ruleParam = new HashMap<String, Object>();
        CatageoryEvaluationParamDto paramDTO = new CatageoryEvaluationParamDto();
        paramDTO.setLabel(labels);
        paramDTO.setBrand("puma");
    
        SlabMappingCache mslabMappingCache = new SlabMappingCache();
        Map<String, NavigableMap<Double, SlabCacheDTO>> slabCache = new HashMap<String, NavigableMap<Double, SlabCacheDTO>>();
        List<SlabItemDTO> boxes = new ArrayList<SlabItemDTO>();       
        SlabItemDTO slabItemDTO = createSlabItemDto("CB01",20.0,10.0,10.0,200.00,"BOX 1");
        boxes.add(slabItemDTO);        
        SlabItemDTO slabItemDTO1 = createSlabItemDto("CB02",40.0,10.0,10.0,400.00,"BOX 2");
        boxes.add(slabItemDTO1);       
        SlabCacheDTO slabCacheDTO = createSlabCacheDTO(boxes,0,200);    
        NavigableMap<Double, SlabCacheDTO> navigableMap = new ConcurrentSkipListMap<Double, SlabCacheDTO>();
        navigableMap.put(99.0, slabCacheDTO);
        slabCache.put("Snapdeal-SURFACE_B", navigableMap);
    
        Map<String, List<PackagingTypeCacheDTO>> mtypeToPackagingTypeDTOsMap = new HashMap<String, List<PackagingTypeCacheDTO>>();
        PackagingTypeCache packagingTypeCache = new PackagingTypeCache();
        
        List<PackagingTypeCacheDTO> lPackagingTypeCacheDTO = new ArrayList<PackagingTypeCacheDTO>();        
        PackagingTypeCacheDTO packagingTypeCacheDTO = createPackagingTypeCacheDTO("Snapdeal","SURFACE_B","Picknpack");
        lPackagingTypeCacheDTO.add(packagingTypeCacheDTO);    
        mtypeToPackagingTypeDTOsMap.put("SURFACE_B", lPackagingTypeCacheDTO);
    
        PackagingType packagingType = createPackagingType("Snapdeal","picknpack");
        pauSRO = createPauSro("990001",10.0,10.0,10.0,"CARTON");
     
        List<PackagingType> lpackagingList = new ArrayList<PackagingType>();
        lpackagingList.add(packagingType);
        packagingTypeCache.addPackagingType(lpackagingList);
        CacheManager.getInstance().setCache(packagingTypeCache);
        when(mcocofsExternalService.getProductFulfilmentAttributes(Matchers.<GetProductFulfilmentAttributeRequest> any())).thenReturn(pauSRO);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(10.0, 10.0, 10.0)).thenReturn(100.0);
        ReflectionTestUtils.setField(mPackagingRecommendationService, "cocofsExternalService", mcocofsExternalService);
    
        mSupcPackagingTypeMongoService = mock(ISupcPackagingTypeMongoService.class);
        when(mSupcPackagingTypeMongoService.getEnabledAndActivePackagingTypeBySupcAndStoreCode("Snapdeal", "990001")).thenReturn(null);
        ReflectionTestUtils.setField(mruleEngineService, "supcPackagingTypeMongoService", mSupcPackagingTypeMongoService);
    
        ReflectionTestUtils.setField(packagingTypeCache, "typeToPackagingTypeDTOsMap", mtypeToPackagingTypeDTOsMap);
    
        GetPackagingRecommendationsRequest request = mock(GetPackagingRecommendationsRequest.class);
        ReflectionTestUtils.setField(packmanLandingService, "packagingRecomendationService", mPackagingRecommendationService);
        Protocol proto = Protocol.PROTOCOL_JSON;
        when(request.getRequestProtocol()).thenReturn(proto);
        when(request.getApiKey()).thenReturn("p@(km@n");
        when(request.getStoreFrontId()).thenReturn("SD");
        when(request.getIsShipTogether()).thenReturn(false);
        when(request.getPackageDetail()).thenReturn(packageDetailSRO);
    
        CacheManager.getInstance().setCache(mslabMappingCache);
    
        ReflectionTestUtils.setField(mslabMappingCache, "slabCache", slabCache);
        mruleService = mock(IRulesService.class);
        List<BlockParams> lblockParams = new ArrayList<BlockParams>();
        Block block = createBlock("CategoryPackagingRuleBlock");
        BlockParams blockParams = createBlockParams(block,"LABEL","Label");
        ruleParam.put("abc", "CategoryPackagingRuleBlock");
        Mockito.doReturn(ruleParam).when(packmanPackagingRuleEngineServiceImpl).populateRuleParamsFromBlockObject(Matchers.any(), (List<BlockParams>) Matchers.any());
        
        lblockParams.add(blockParams);
        mexclusiveService = mock(PackmanExclusiveServiceImpl.class);
        when(mruleService.getBlockParamsByBlockName("CategoryPackagingRuleBlock")).thenReturn(lblockParams);
        ReflectionTestUtils.setField(mruleEngineService, "ruleService", mruleService);
        ReflectionTestUtils.setField(mruleEngineService, "exclusiveService", mexclusiveService);
        mDepedencyForDTO = mock(com.snapdeal.packman.dto.SlabCacheDTO.IDependency.class);
    
        when(mDepedencyForDTO.getBooleanScalar(Property.USE_PRIMARY_LBH)).thenReturn(false);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_LBH)).thenReturn(false);
        GetPackagingRecommendationsResponse resp = packmanLandingService.getPackagingRecommendations(request);        
        assertTrue(resp.isSuccessful());
        assertEquals("SURFACE_B",resp.getPackagingRecommendationList().get(0).getPackagingType().getType());        
    }

    

    //TEST 25
    @Test
    public void testGetPackagingRecommendationsNormalAtSubcatLabelAndBrandBothExist()
            throws PackagingRecomendationException, ExternalDataNotFoundException, NullPointerException, NullPointerException {

        List<String> labels = new ArrayList<String>();
        labels.add(Labels.Premium.toString());
        mExternalServiceFactory = mock(ExternalServiceFactory.class);
        ICAMSExternalService miCAMSExternalService = mock(ICAMSExternalService.class);
        GetPOGDetailListResponse response = new GetPOGDetailListResponse();
        POGDetailSRO pogDetailSROs = new POGDetailSRO();
        pogDetailSROs.setEnabled(true);

        List<POGDetailSRO> lpogDetailSROs = new ArrayList<POGDetailSRO>();
        lpogDetailSROs.add(pogDetailSROs);
        response.setPogDetailSROs(lpogDetailSROs);
        when(mExternalServiceFactory.getCAMSExternalService()).thenReturn(miCAMSExternalService);
        when(miCAMSExternalService.getPOGDetailsBbySupc(Matchers.<GetPOGDetailListBySUPCListRequest> any())).thenReturn(response);
        ReflectionTestUtils.setField(mruleEngineService, "factory", mExternalServiceFactory);
        PackagableItemSRO packagableItemSRO = createPackagableItemSRO(labels);
        lPackagableItemSRO.add(packagableItemSRO);
        packageDetailSRO.setPackagableItemList(lPackagableItemSRO);
        genericMao = mock(IGenericMao.class);
        ruleList = new ArrayList<>();
        List<CriteriaUnit> lCriteriaUnit = new ArrayList<CriteriaUnit>();
        CriteriaUnit criteriaUnit = createCriteriaUnit(1,"LABELS","premium");
        lCriteriaUnit.add(criteriaUnit);
        
        PackmanRuleUnit packmanRuleUnit = createPackmanRuleUnit(2,"ru03",7,"CARTON", lCriteriaUnit);
        packmanRuleUnit.setCriteria(lCriteriaUnit);
        ruleList.add(packmanRuleUnit);
        doAnswer(new Answer<List<PackmanRuleUnit>>() {
            public List<PackmanRuleUnit> answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                CocofsMongoQuery cocofsMongoQuery = (CocofsMongoQuery) args[0];
                List<Criteria> lCriteria = cocofsMongoQuery.getCriteria();
                Criteria criteria1 = lCriteria.get(0);
                String rule_type = (String) criteria1.getObj();
                if (rule_type.equals("subcategory-packaging"))
                    return ruleList;
                return null;

            }

        }).when(genericMao).getDocumentList(Matchers.<CocofsMongoQuery> any(), Matchers.<Class<PackmanRuleUnit>> any());
        ReflectionTestUtils.setField(mruleEngineService, "genericMao", genericMao);

        PackmanPackagingRuleEngineServiceImpl packmanPackagingRuleEngineServiceImpl = Mockito.spy(new PackmanPackagingRuleEngineServiceImpl());
        Map<String, Object> ruleParam = new HashMap<String, Object>();
        CatageoryEvaluationParamDto paramDTO = new CatageoryEvaluationParamDto();
        paramDTO.setLabel(labels);
        paramDTO.setBrand("puma");

        SlabMappingCache mslabMappingCache = new SlabMappingCache();
        Map<String, NavigableMap<Double, SlabCacheDTO>> slabCache = new HashMap<String, NavigableMap<Double, SlabCacheDTO>>();
        SlabCacheDTO slabCacheDTO = new SlabCacheDTO();
        List<SlabItemDTO> boxes = new ArrayList<SlabItemDTO>();
        
        SlabItemDTO slabItemDTO = createSlabItemDto("CB01",20.0,10.0,10.0,200.00,"BOX 1");
        boxes.add(slabItemDTO);  
        SlabItemDTO slabItemDTO1 = createSlabItemDto("CB02",40.0,10.0,10.0,400.00,"BOX 2");
        boxes.add(slabItemDTO1);
        slabCacheDTO = createSlabCacheDTO(boxes,0,200); 

        NavigableMap<Double, SlabCacheDTO> navigableMap = new ConcurrentSkipListMap<Double, SlabCacheDTO>();
        navigableMap.put(99.0, slabCacheDTO);
        slabCache.put("Snapdeal-CARTON", navigableMap);

        Map<String, List<PackagingTypeCacheDTO>> mtypeToPackagingTypeDTOsMap = new HashMap<String, List<PackagingTypeCacheDTO>>();
        PackagingTypeCache packagingTypeCache = new PackagingTypeCache();
        
        List<PackagingTypeCacheDTO> lPackagingTypeCacheDTO = new ArrayList<PackagingTypeCacheDTO>();        
        PackagingTypeCacheDTO packagingTypeCacheDTO = createPackagingTypeCacheDTO("Snapdeal","CARTON","Normal");
        lPackagingTypeCacheDTO.add(packagingTypeCacheDTO);
        mtypeToPackagingTypeDTOsMap.put("CARTON", lPackagingTypeCacheDTO);
        PackagingType packagingType = createPackagingType("Snapdeal","Normal");
        pauSRO = createPauSro("990001",10.0,10.0,10.0,"CARTON");

        List<PackagingType> lpackagingList = new ArrayList<PackagingType>();
        lpackagingList.add(packagingType);
        packagingTypeCache.addPackagingType(lpackagingList);
        CacheManager.getInstance().setCache(packagingTypeCache);
        when(mcocofsExternalService.getProductFulfilmentAttributes(Matchers.<GetProductFulfilmentAttributeRequest> any())).thenReturn(pauSRO);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(10.0, 10.0, 10.0)).thenReturn(100.0);
        ReflectionTestUtils.setField(mPackagingRecommendationService, "cocofsExternalService", mcocofsExternalService);

        mSupcPackagingTypeMongoService = mock(ISupcPackagingTypeMongoService.class);
        when(mSupcPackagingTypeMongoService.getEnabledAndActivePackagingTypeBySupcAndStoreCode("Snapdeal", "990001")).thenReturn(null);
        ReflectionTestUtils.setField(mruleEngineService, "supcPackagingTypeMongoService", mSupcPackagingTypeMongoService);

        ReflectionTestUtils.setField(packagingTypeCache, "typeToPackagingTypeDTOsMap", mtypeToPackagingTypeDTOsMap);

        GetPackagingRecommendationsRequest request = mock(GetPackagingRecommendationsRequest.class);
        ReflectionTestUtils.setField(packmanLandingService, "packagingRecomendationService", mPackagingRecommendationService);
        Protocol proto = Protocol.PROTOCOL_JSON;
        when(request.getRequestProtocol()).thenReturn(proto);
        when(request.getApiKey()).thenReturn("p@(km@n");
        when(request.getStoreFrontId()).thenReturn("SD");
        when(request.getIsShipTogether()).thenReturn(false);
        when(request.getPackageDetail()).thenReturn(packageDetailSRO);

        CacheManager.getInstance().setCache(mslabMappingCache);
        ReflectionTestUtils.setField(mslabMappingCache, "slabCache", slabCache);
        mruleService = mock(IRulesService.class);
        List<BlockParams> lblockParams = new ArrayList<BlockParams>();
        Block block = createBlock("CategoryPackagingRuleBlock");
        BlockParams blockParams = createBlockParams(block,"LABEL","Label");

        ruleParam.put("abc", "CategoryPackagingRuleBlock");
        Mockito.doReturn(ruleParam).when(packmanPackagingRuleEngineServiceImpl).populateRuleParamsFromBlockObject(Matchers.any(), (List<BlockParams>) Matchers.any());
        lblockParams.add(blockParams);
        mexclusiveService = mock(PackmanExclusiveServiceImpl.class);
        when(mruleService.getBlockParamsByBlockName("CategoryPackagingRuleBlock")).thenReturn(lblockParams);
        ReflectionTestUtils.setField(mruleEngineService, "ruleService", mruleService);
        ReflectionTestUtils.setField(mruleEngineService, "exclusiveService", mexclusiveService);

        mDepedencyForDTO = mock(com.snapdeal.packman.dto.SlabCacheDTO.IDependency.class);
        when(mDepedencyForDTO.getBooleanScalar(Property.USE_PRIMARY_LBH)).thenReturn(false);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_LBH)).thenReturn(false);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_DELTA_VOL_WT)).thenReturn(false);

        ReflectionTestUtils.setField(slabCacheDTO, "dep", mDepedencyForDTO);
        GetPackagingRecommendationsResponse resp = packmanLandingService.getPackagingRecommendations(request);
        assertTrue(resp.isSuccessful());
        assertEquals("CARTON", resp.getPackagingRecommendationList().get(0).getPackagingType().getType());
        assertEquals("Normal", resp.getPackagingRecommendationList().get(0).getPackagingMode());
    }

      //test 6.a.1
    @Test
    public void testGetPackagingRecommendationsNormalAtSupcLabelOldFeatureWithPrimaryLBHAndMargin() throws PackagingRecomendationException, ExternalDataNotFoundException,NullPointerException{
    
        List<String> labels = new ArrayList<String>();
        labels.add(Labels.Premium.toString());
        mExternalServiceFactory = mock(ExternalServiceFactory.class);
        ICAMSExternalService miCAMSExternalService = mock(ICAMSExternalService.class);
        GetPOGDetailListResponse response = new GetPOGDetailListResponse();
        POGDetailSRO pogDetailSROs = new POGDetailSRO();
        pogDetailSROs.setEnabled(true);
    
        List<POGDetailSRO> lpogDetailSROs = new ArrayList<POGDetailSRO>();
        lpogDetailSROs.add(pogDetailSROs);
        response.setPogDetailSROs(lpogDetailSROs);
        when(mExternalServiceFactory.getCAMSExternalService()).thenReturn(miCAMSExternalService);
        when(miCAMSExternalService.getPOGDetailsBbySupc(Matchers.<GetPOGDetailListBySUPCListRequest> any())).thenReturn(response);
        ReflectionTestUtils.setField(mruleEngineService, "factory", mExternalServiceFactory);
        PackagableItemSRO packagableItemSRO = createPackagableItemSRO(labels);
        lPackagableItemSRO.add(packagableItemSRO);
        packageDetailSRO.setPackagableItemList(lPackagableItemSRO);
        genericMao = mock(IGenericMao.class);
        ruleList = new ArrayList<>();
        List<CriteriaUnit> lCriteriaUnit = new ArrayList<CriteriaUnit>();
       
        PackmanRuleUnit packmanRuleUnit = createPackmanRuleUnit(2,"ru03",7,"CARTON", lCriteriaUnit);
        ruleList.add(packmanRuleUnit);
        doAnswer(new Answer<List<PackmanRuleUnit>>() {
            public List<PackmanRuleUnit> answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                CocofsMongoQuery cocofsMongoQuery = (CocofsMongoQuery) args[0];
                List<Criteria> lCriteria = cocofsMongoQuery.getCriteria();
                Criteria criteria1 = lCriteria.get(0);
                String rule_type = (String) criteria1.getObj();
                if (rule_type.equals("store-packaging"))
                    return ruleList;
                return null;
    
            }
    
        }).when(genericMao).getDocumentList(Matchers.<CocofsMongoQuery> any(), Matchers.<Class<PackmanRuleUnit>> any());
        ReflectionTestUtils.setField(mruleEngineService, "genericMao", genericMao);
    
        PackmanPackagingRuleEngineServiceImpl packmanPackagingRuleEngineServiceImpl = Mockito.spy(new PackmanPackagingRuleEngineServiceImpl());
        Map<String, Object> ruleParam = new HashMap<String, Object>();
        SlabMappingCache mslabMappingCache = new SlabMappingCache();
        Map<String, NavigableMap<Double, SlabCacheDTO>> slabCache = new HashMap<String, NavigableMap<Double, SlabCacheDTO>>();
        
        List<SlabItemDTO> boxes = new ArrayList<SlabItemDTO>();
        
        SlabItemDTO slabItemDTO = createSlabItemDto("CB101",10.0,10.0,10.0,100.00,"CB101");
        boxes.add(slabItemDTO);      
        slabItemDTO = createSlabItemDto("CB102",10.1,10.1,10.1,103.03,"CB102");
        boxes.add(slabItemDTO);
        SlabCacheDTO slabCacheDTO = createSlabCacheDTO(boxes,0,500);
        NavigableMap<Double, SlabCacheDTO> navigableMap = new ConcurrentSkipListMap<Double, SlabCacheDTO>();
        navigableMap.put(99.0, slabCacheDTO);
        slabCache.put("Snapdeal-CARTON", navigableMap);
    
        Map<String, List<PackagingTypeCacheDTO>> mtypeToPackagingTypeDTOsMap = new HashMap<String, List<PackagingTypeCacheDTO>>();
        PackagingTypeCache packagingTypeCache = new PackagingTypeCache();
        PackagingTypeCacheDTO packagingTypeCacheDTO = createPackagingTypeCacheDTO("Snapdeal","CARTON","Normal");
        List<PackagingTypeCacheDTO> lPackagingTypeCacheDTO = new ArrayList<PackagingTypeCacheDTO>();
        lPackagingTypeCacheDTO.add(packagingTypeCacheDTO);
        mtypeToPackagingTypeDTOsMap.put("CARTON", lPackagingTypeCacheDTO);
    
        PackagingType packagingType = createPackagingType("Snapdeal","Normal");
        pauSRO = createPauSro("990001",10.0,10.0,10.0,"CARTON");
        List<PackagingType> lpackagingList = new ArrayList<PackagingType>();
        lpackagingList.add(packagingType);
        packagingTypeCache.addPackagingType(lpackagingList);
        CacheManager.getInstance().setCache(packagingTypeCache);
        when(mcocofsExternalService.getProductFulfilmentAttributes(Matchers.<GetProductFulfilmentAttributeRequest> any())).thenReturn(pauSRO);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(10.0, 10.0, 10.0)).thenReturn(100.0);
        ReflectionTestUtils.setField(mPackagingRecommendationService, "cocofsExternalService", mcocofsExternalService);
    
        mSupcPackagingTypeMongoService = mock(ISupcPackagingTypeMongoService.class);
        when(mSupcPackagingTypeMongoService.getEnabledAndActivePackagingTypeBySupcAndStoreCode("Snapdeal", "990001")).thenReturn("CARTON");
        ReflectionTestUtils.setField(mruleEngineService, "supcPackagingTypeMongoService", mSupcPackagingTypeMongoService);
    
        ReflectionTestUtils.setField(packagingTypeCache, "typeToPackagingTypeDTOsMap", mtypeToPackagingTypeDTOsMap);
    
        GetPackagingRecommendationsRequest request = mock(GetPackagingRecommendationsRequest.class);
        ReflectionTestUtils.setField(packmanLandingService, "packagingRecomendationService", mPackagingRecommendationService);
        Protocol proto = Protocol.PROTOCOL_JSON;
        when(request.getRequestProtocol()).thenReturn(proto);
        when(request.getApiKey()).thenReturn("p@(km@n");
        when(request.getStoreFrontId()).thenReturn("SD");
        when(request.getIsShipTogether()).thenReturn(false);
        when(request.getPackageDetail()).thenReturn(packageDetailSRO);
    
        CacheManager.getInstance().setCache(mslabMappingCache);   
        ReflectionTestUtils.setField(mslabMappingCache, "slabCache", slabCache);
        mruleService = mock(IRulesService.class);
        List<BlockParams> lblockParams = new ArrayList<BlockParams>();
        Block block = createBlock("StorePackagingRuleBlock");
        BlockParams blockParams = createBlockParams(block,"LABEL","Label");
        ruleParam.put("BRAND", "StorePackagingRuleBlock");
        Mockito.doReturn(ruleParam).when(packmanPackagingRuleEngineServiceImpl).populateRuleParamsFromBlockObject(Matchers.any(), (List<BlockParams>) Matchers.any());
        blockParams.setId(5);
        lblockParams.add(blockParams);
        mexclusiveService = mock(PackmanExclusiveServiceImpl.class);
        when(mruleService.getBlockParamsByBlockName("StorePackagingRuleBlock")).thenReturn(lblockParams);
        ReflectionTestUtils.setField(mruleEngineService, "ruleService", mruleService);
        ReflectionTestUtils.setField(mruleEngineService, "exclusiveService", mexclusiveService);
        mDepedencyForDTO = mock(com.snapdeal.packman.dto.SlabCacheDTO.IDependency.class);
    
        when(mDepedencyForDTO.getBooleanScalar(Property.USE_PRIMARY_LBH)).thenReturn(false);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_LBH)).thenReturn(true);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_DELTA_VOL_WT_MULT_FACTOR)).thenReturn(2.0);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_LBH_MARGIN)).thenReturn(0.1);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_DELTA_VOL_WT)).thenReturn(false);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_DELTA_VOL_WT_OFFSET)).thenReturn(1000.0);
        ReflectionTestUtils.setField(slabCacheDTO, "dep", mDepedencyForDTO);
        GetPackagingRecommendationsResponse resp = packmanLandingService.getPackagingRecommendations(request);
        assertTrue(resp.isSuccessful());
        assertEquals("CARTON", resp.getPackagingRecommendationList().get(0).getPackagingType().getType());
        assertEquals("Normal", resp.getPackagingRecommendationList().get(0).getPackagingMode());
        assertEquals("CB101", resp.getPackagingRecommendationList().get(0).getPreferredPackagingTypeItem().getCode());
    }
    
    //test 6.a.2
    @Test
    public void testGetPackagingRecommendationsNormalAtSupcLabelOldFeature() throws PackagingRecomendationException, ExternalDataNotFoundException {
    
        List<String> labels = new ArrayList<String>();
        labels.add(Labels.Premium.toString());
        mExternalServiceFactory = mock(ExternalServiceFactory.class);
        ICAMSExternalService miCAMSExternalService = mock(ICAMSExternalService.class);
        GetPOGDetailListResponse response = new GetPOGDetailListResponse();
        POGDetailSRO pogDetailSROs = new POGDetailSRO();
        pogDetailSROs.setEnabled(true);
    
        List<POGDetailSRO> lpogDetailSROs = new ArrayList<POGDetailSRO>();
        lpogDetailSROs.add(pogDetailSROs);
        response.setPogDetailSROs(lpogDetailSROs);
        when(mExternalServiceFactory.getCAMSExternalService()).thenReturn(miCAMSExternalService);
        when(miCAMSExternalService.getPOGDetailsBbySupc(Matchers.<GetPOGDetailListBySUPCListRequest> any())).thenReturn(response);
        ReflectionTestUtils.setField(mruleEngineService, "factory", mExternalServiceFactory);
        PackagableItemSRO packagableItemSRO = createPackagableItemSRO(labels);
        lPackagableItemSRO.add(packagableItemSRO);
        packageDetailSRO.setPackagableItemList(lPackagableItemSRO);
        genericMao = mock(IGenericMao.class);
        ruleList = new ArrayList<>();
        List<CriteriaUnit> lCriteriaUnit = new ArrayList<CriteriaUnit>();
        CriteriaUnit criteriaUnit = createCriteriaUnit(1,"","");
        lCriteriaUnit.add(criteriaUnit);
        PackmanRuleUnit packmanRuleUnit = createPackmanRuleUnit(2,"ru03",7,"CARTON", lCriteriaUnit);
        ruleList.add(packmanRuleUnit);
        doAnswer(new Answer<List<PackmanRuleUnit>>() {
            public List<PackmanRuleUnit> answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                CocofsMongoQuery cocofsMongoQuery = (CocofsMongoQuery) args[0];
                List<Criteria> lCriteria = cocofsMongoQuery.getCriteria();
                Criteria criteria1 = lCriteria.get(0);
                String rule_type = (String) criteria1.getObj();
                if (rule_type.equals("store-packaging"))
                    return ruleList;
                return null;
    
            }
    
        }).when(genericMao).getDocumentList(Matchers.<CocofsMongoQuery> any(), Matchers.<Class<PackmanRuleUnit>> any());
        ReflectionTestUtils.setField(mruleEngineService, "genericMao", genericMao);
    
        PackmanPackagingRuleEngineServiceImpl packmanPackagingRuleEngineServiceImpl = Mockito.spy(new PackmanPackagingRuleEngineServiceImpl());
        Map<String, Object> ruleParam = new HashMap<String, Object>();
        SlabMappingCache mslabMappingCache = new SlabMappingCache();
        Map<String, NavigableMap<Double, SlabCacheDTO>> slabCache = new HashMap<String, NavigableMap<Double, SlabCacheDTO>>();
        SlabCacheDTO slabCacheDTO = new SlabCacheDTO();
        List<SlabItemDTO> boxes = new ArrayList<SlabItemDTO>();
        
        SlabItemDTO slabItemDTO = createSlabItemDto("CB101",10.0,10.0,10.0,100.00,"CB101");
        boxes.add(slabItemDTO);        
        slabItemDTO = createSlabItemDto("CB102",10.1,10.1,10.1,103.03,"CB102");
        boxes.add(slabItemDTO);        
        slabCacheDTO = createSlabCacheDTO(boxes,0,500);

        NavigableMap<Double, SlabCacheDTO> navigableMap = new ConcurrentSkipListMap<Double, SlabCacheDTO>();
        navigableMap.put(99.0, slabCacheDTO);
        slabCache.put("Snapdeal-CARTON", navigableMap);
    
        Map<String, List<PackagingTypeCacheDTO>> mtypeToPackagingTypeDTOsMap = new HashMap<String, List<PackagingTypeCacheDTO>>();
        PackagingTypeCache packagingTypeCache = new PackagingTypeCache();
        List<PackagingTypeCacheDTO> lPackagingTypeCacheDTO = new ArrayList<PackagingTypeCacheDTO>();
        
        PackagingTypeCacheDTO packagingTypeCacheDTO = createPackagingTypeCacheDTO("Snapdeal","CARTON","Normal");
        lPackagingTypeCacheDTO.add(packagingTypeCacheDTO);
    
        mtypeToPackagingTypeDTOsMap.put("CARTON", lPackagingTypeCacheDTO);
    
        PackagingType packagingType = createPackagingType("Snapdeal","Normal");
        pauSRO = createPauSro("990001",10.0,10.0,10.0,"CARTON");
        List<PackagingType> lpackagingList = new ArrayList<PackagingType>();
        lpackagingList.add(packagingType);
        packagingTypeCache.addPackagingType(lpackagingList);
        CacheManager.getInstance().setCache(packagingTypeCache);
        when(mcocofsExternalService.getProductFulfilmentAttributes(Matchers.<GetProductFulfilmentAttributeRequest> any())).thenReturn(pauSRO);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(10.0, 10.0, 10.0)).thenReturn(100.0);
        ReflectionTestUtils.setField(mPackagingRecommendationService, "cocofsExternalService", mcocofsExternalService);
    
        mSupcPackagingTypeMongoService = mock(ISupcPackagingTypeMongoService.class);
        when(mSupcPackagingTypeMongoService.getEnabledAndActivePackagingTypeBySupcAndStoreCode("Snapdeal", "990001")).thenReturn("CARTON");
        ReflectionTestUtils.setField(mruleEngineService, "supcPackagingTypeMongoService", mSupcPackagingTypeMongoService);
    
        ReflectionTestUtils.setField(packagingTypeCache, "typeToPackagingTypeDTOsMap", mtypeToPackagingTypeDTOsMap);
    
        GetPackagingRecommendationsRequest request = mock(GetPackagingRecommendationsRequest.class);
        ReflectionTestUtils.setField(packmanLandingService, "packagingRecomendationService", mPackagingRecommendationService);
        Protocol proto = Protocol.PROTOCOL_JSON;
        when(request.getRequestProtocol()).thenReturn(proto);
        when(request.getApiKey()).thenReturn("p@(km@n");
        when(request.getStoreFrontId()).thenReturn("SD");
        when(request.getIsShipTogether()).thenReturn(false);
        when(request.getPackageDetail()).thenReturn(packageDetailSRO);
    
        CacheManager.getInstance().setCache(mslabMappingCache);
    
        ReflectionTestUtils.setField(mslabMappingCache, "slabCache", slabCache);
        mruleService = mock(IRulesService.class);
        List<BlockParams> lblockParams = new ArrayList<BlockParams>();
        Block block = createBlock("StorePackagingRuleBlock");
        BlockParams blockParams = createBlockParams(block,"LABEL","Label");
        ruleParam.put("BRAND", "StorePackagingRuleBlock");
        Mockito.doReturn(ruleParam).when(packmanPackagingRuleEngineServiceImpl).populateRuleParamsFromBlockObject(Matchers.any(), (List<BlockParams>) Matchers.any());
        lblockParams.add(blockParams);
        mexclusiveService = mock(PackmanExclusiveServiceImpl.class);
        when(mruleService.getBlockParamsByBlockName("StorePackagingRuleBlock")).thenReturn(lblockParams);
        ReflectionTestUtils.setField(mruleEngineService, "ruleService", mruleService);
        ReflectionTestUtils.setField(mruleEngineService, "exclusiveService", mexclusiveService);
        mDepedencyForDTO = mock(com.snapdeal.packman.dto.SlabCacheDTO.IDependency.class);
    
        when(mDepedencyForDTO.getBooleanScalar(Property.USE_PRIMARY_LBH)).thenReturn(false);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_LBH)).thenReturn(false);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_DELTA_VOL_WT_MULT_FACTOR)).thenReturn(2.0);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_LBH_MARGIN)).thenReturn(0.1);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_DELTA_VOL_WT)).thenReturn(false);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_DELTA_VOL_WT_OFFSET)).thenReturn(1000.0);
        ReflectionTestUtils.setField(slabCacheDTO, "dep", mDepedencyForDTO);
        GetPackagingRecommendationsResponse resp = packmanLandingService.getPackagingRecommendations(request);
        assertTrue(resp.isSuccessful());
        assertEquals("CARTON", resp.getPackagingRecommendationList().get(0).getPackagingType().getType());
        assertEquals("Normal", resp.getPackagingRecommendationList().get(0).getPackagingMode());
        assertEquals("CB101", resp.getPackagingRecommendationList().get(0).getPreferredPackagingTypeItem().getCode());
    }
    
    //test 6.a.3
    @Test
    public void testGetPackagingRecommendationsNormalAtSupcLabelOldAndDeltafeature() throws PackagingRecomendationException, ExternalDataNotFoundException {
    
        List<String> labels = new ArrayList<String>();
        labels.add(Labels.Premium.toString());
        mExternalServiceFactory = mock(ExternalServiceFactory.class);
        ICAMSExternalService miCAMSExternalService = mock(ICAMSExternalService.class);
        GetPOGDetailListResponse response = new GetPOGDetailListResponse();
        POGDetailSRO pogDetailSROs = new POGDetailSRO();
        pogDetailSROs.setEnabled(true);
    
        List<POGDetailSRO> lpogDetailSROs = new ArrayList<POGDetailSRO>();
        lpogDetailSROs.add(pogDetailSROs);
        response.setPogDetailSROs(lpogDetailSROs);
        when(mExternalServiceFactory.getCAMSExternalService()).thenReturn(miCAMSExternalService);
        when(miCAMSExternalService.getPOGDetailsBbySupc(Matchers.<GetPOGDetailListBySUPCListRequest> any())).thenReturn(response);
        ReflectionTestUtils.setField(mruleEngineService, "factory", mExternalServiceFactory);
        PackagableItemSRO packagableItemSRO = createPackagableItemSRO(labels);
        lPackagableItemSRO.add(packagableItemSRO);
        packageDetailSRO.setPackagableItemList(lPackagableItemSRO);
        genericMao = mock(IGenericMao.class);
        ruleList = new ArrayList<>();
        List<CriteriaUnit> lCriteriaUnit = new ArrayList<CriteriaUnit>();
        CriteriaUnit criteriaUnit = createCriteriaUnit(1,"","");
        lCriteriaUnit.add(criteriaUnit);
        PackmanRuleUnit packmanRuleUnit = createPackmanRuleUnit(2,"ru03",7,"CARTON", lCriteriaUnit);

        ruleList.add(packmanRuleUnit);
        doAnswer(new Answer<List<PackmanRuleUnit>>() {
            public List<PackmanRuleUnit> answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                CocofsMongoQuery cocofsMongoQuery = (CocofsMongoQuery) args[0];
                List<Criteria> lCriteria = cocofsMongoQuery.getCriteria();
                Criteria criteria1 = lCriteria.get(0);
                String rule_type = (String) criteria1.getObj();
                if (rule_type.equals("store-packaging"))
                    return ruleList;
                return null;
    
            }
    
        }).when(genericMao).getDocumentList(Matchers.<CocofsMongoQuery> any(), Matchers.<Class<PackmanRuleUnit>> any());
        ReflectionTestUtils.setField(mruleEngineService, "genericMao", genericMao);
    
        PackmanPackagingRuleEngineServiceImpl packmanPackagingRuleEngineServiceImpl = Mockito.spy(new PackmanPackagingRuleEngineServiceImpl());
        Map<String, Object> ruleParam = new HashMap<String, Object>();
        SlabMappingCache mslabMappingCache = new SlabMappingCache();
        Map<String, NavigableMap<Double, SlabCacheDTO>> slabCache = new HashMap<String, NavigableMap<Double, SlabCacheDTO>>();
        SlabCacheDTO slabCacheDTO = new SlabCacheDTO();
        List<SlabItemDTO> boxes = new ArrayList<SlabItemDTO>();        
        SlabItemDTO slabItemDTO = createSlabItemDto("CB101",10.0,10.0,10.0,100.00,"CB101");
        boxes.add(slabItemDTO);        
        slabItemDTO = createSlabItemDto("CB102",10.1,10.1,10.1,103.03,"CB102");
        boxes.add(slabItemDTO);        
        slabCacheDTO = createSlabCacheDTO(boxes,0,500);
        NavigableMap<Double, SlabCacheDTO> navigableMap = new ConcurrentSkipListMap<Double, SlabCacheDTO>();
        navigableMap.put(99.0, slabCacheDTO);
        slabCache.put("Snapdeal-CARTON", navigableMap);
    
        Map<String, List<PackagingTypeCacheDTO>> mtypeToPackagingTypeDTOsMap = new HashMap<String, List<PackagingTypeCacheDTO>>();
        PackagingTypeCache packagingTypeCache = new PackagingTypeCache();
        List<PackagingTypeCacheDTO> lPackagingTypeCacheDTO = new ArrayList<PackagingTypeCacheDTO>();
        
        PackagingTypeCacheDTO packagingTypeCacheDTO = createPackagingTypeCacheDTO("Snapdeal","CARTON","Normal");
        lPackagingTypeCacheDTO.add(packagingTypeCacheDTO);
        mtypeToPackagingTypeDTOsMap.put("CARTON", lPackagingTypeCacheDTO);
    
        PackagingType packagingType = createPackagingType("Snapdeal","Normal");
        pauSRO = createPauSro("990001",10.0,10.0,10.0,"CARTON");
        List<PackagingType> lpackagingList = new ArrayList<PackagingType>();
        lpackagingList.add(packagingType);
        packagingTypeCache.addPackagingType(lpackagingList);
        CacheManager.getInstance().setCache(packagingTypeCache);
        when(mcocofsExternalService.getProductFulfilmentAttributes(Matchers.<GetProductFulfilmentAttributeRequest> any())).thenReturn(pauSRO);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(10.0, 10.0, 10.0)).thenReturn(100.0);
        ReflectionTestUtils.setField(mPackagingRecommendationService, "cocofsExternalService", mcocofsExternalService);
    
        mSupcPackagingTypeMongoService = mock(ISupcPackagingTypeMongoService.class);
        when(mSupcPackagingTypeMongoService.getEnabledAndActivePackagingTypeBySupcAndStoreCode("Snapdeal", "990001")).thenReturn("CARTON");
        ReflectionTestUtils.setField(mruleEngineService, "supcPackagingTypeMongoService", mSupcPackagingTypeMongoService);
    
        ReflectionTestUtils.setField(packagingTypeCache, "typeToPackagingTypeDTOsMap", mtypeToPackagingTypeDTOsMap);
    
        GetPackagingRecommendationsRequest request = mock(GetPackagingRecommendationsRequest.class);
        ReflectionTestUtils.setField(packmanLandingService, "packagingRecomendationService", mPackagingRecommendationService);
        Protocol proto = Protocol.PROTOCOL_JSON;
        when(request.getRequestProtocol()).thenReturn(proto);
        when(request.getApiKey()).thenReturn("p@(km@n");
        when(request.getStoreFrontId()).thenReturn("SD");
        when(request.getIsShipTogether()).thenReturn(false);
        when(request.getPackageDetail()).thenReturn(packageDetailSRO);
    
        CacheManager.getInstance().setCache(mslabMappingCache);
    
        ReflectionTestUtils.setField(mslabMappingCache, "slabCache", slabCache);
        mruleService = mock(IRulesService.class);
        List<BlockParams> lblockParams = new ArrayList<BlockParams>();
        ruleParam.put("BRAND", "StorePackagingRuleBlock");
        Mockito.doReturn(ruleParam).when(packmanPackagingRuleEngineServiceImpl).populateRuleParamsFromBlockObject(Matchers.any(), (List<BlockParams>) Matchers.any());
        Block block = createBlock("StorePackagingRuleBlock");
        BlockParams blockParams = createBlockParams(block,"LABEL","Label");
        lblockParams.add(blockParams);
        mexclusiveService = mock(PackmanExclusiveServiceImpl.class);
        when(mruleService.getBlockParamsByBlockName("StorePackagingRuleBlock")).thenReturn(lblockParams);
        ReflectionTestUtils.setField(mruleEngineService, "ruleService", mruleService);
        ReflectionTestUtils.setField(mruleEngineService, "exclusiveService", mexclusiveService);
        mDepedencyForDTO = mock(com.snapdeal.packman.dto.SlabCacheDTO.IDependency.class);
    
        when(mDepedencyForDTO.getBooleanScalar(Property.USE_PRIMARY_LBH)).thenReturn(false);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_LBH)).thenReturn(false);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_DELTA_VOL_WT_MULT_FACTOR)).thenReturn(2.0);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_LBH_MARGIN)).thenReturn(0.1);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_DELTA_VOL_WT)).thenReturn(true);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_DELTA_VOL_WT_OFFSET)).thenReturn(1000.0);
    
        ReflectionTestUtils.setField(slabCacheDTO, "dep", mDepedencyForDTO);
        GetPackagingRecommendationsResponse resp = packmanLandingService.getPackagingRecommendations(request);
        assertTrue(resp.isSuccessful());
        assertEquals("CARTON", resp.getPackagingRecommendationList().get(0).getPackagingType().getType());
        assertEquals("Normal", resp.getPackagingRecommendationList().get(0).getPackagingMode());
        assertEquals("CB101", resp.getPackagingRecommendationList().get(0).getPreferredPackagingTypeItem().getCode());
    }
    
    //6.a.4
    @Test
    public void testGetPackagingRecommendationsNormalAtSupcLabelOldFeatureAndLBHTrue() throws PackagingRecomendationException, ExternalDataNotFoundException {
    
        List<String> labels = new ArrayList<String>();
        labels.add(Labels.Premium.toString());
        mExternalServiceFactory = mock(ExternalServiceFactory.class);
        ICAMSExternalService miCAMSExternalService = mock(ICAMSExternalService.class);
        GetPOGDetailListResponse response = new GetPOGDetailListResponse();
        POGDetailSRO pogDetailSROs = new POGDetailSRO();
        pogDetailSROs.setEnabled(true);
    
        List<POGDetailSRO> lpogDetailSROs = new ArrayList<POGDetailSRO>();
        lpogDetailSROs.add(pogDetailSROs);
        response.setPogDetailSROs(lpogDetailSROs);
        when(mExternalServiceFactory.getCAMSExternalService()).thenReturn(miCAMSExternalService);
        when(miCAMSExternalService.getPOGDetailsBbySupc(Matchers.<GetPOGDetailListBySUPCListRequest> any())).thenReturn(response);
        ReflectionTestUtils.setField(mruleEngineService, "factory", mExternalServiceFactory);
        PackagableItemSRO packagableItemSRO = createPackagableItemSRO(labels);
        lPackagableItemSRO.add(packagableItemSRO);
        packageDetailSRO.setPackagableItemList(lPackagableItemSRO);
        genericMao = mock(IGenericMao.class);
        ruleList = new ArrayList<>();
        List<CriteriaUnit> lCriteriaUnit = new ArrayList<CriteriaUnit>();
        CriteriaUnit criteriaUnit = createCriteriaUnit(1,"","");
        lCriteriaUnit.add(criteriaUnit);
        PackmanRuleUnit packmanRuleUnit = createPackmanRuleUnit(2,"ru03",7,"CARTON", lCriteriaUnit);
        ruleList.add(packmanRuleUnit);
        doAnswer(new Answer<List<PackmanRuleUnit>>() {
            public List<PackmanRuleUnit> answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                CocofsMongoQuery cocofsMongoQuery = (CocofsMongoQuery) args[0];
                List<Criteria> lCriteria = cocofsMongoQuery.getCriteria();
                Criteria criteria1 = lCriteria.get(0);
                String rule_type = (String) criteria1.getObj();
                if (rule_type.equals("store-packaging"))
                    return ruleList;
                return null;
    
            }
    
        }).when(genericMao).getDocumentList(Matchers.<CocofsMongoQuery> any(), Matchers.<Class<PackmanRuleUnit>> any());
        ReflectionTestUtils.setField(mruleEngineService, "genericMao", genericMao);
    
        PackmanPackagingRuleEngineServiceImpl packmanPackagingRuleEngineServiceImpl = Mockito.spy(new PackmanPackagingRuleEngineServiceImpl());
        Map<String, Object> ruleParam = new HashMap<String, Object>();
        SlabMappingCache mslabMappingCache = new SlabMappingCache();
        Map<String, NavigableMap<Double, SlabCacheDTO>> slabCache = new HashMap<String, NavigableMap<Double, SlabCacheDTO>>();
        SlabCacheDTO slabCacheDTO = new SlabCacheDTO();
        List<SlabItemDTO> boxes = new ArrayList<SlabItemDTO>();
        
        SlabItemDTO slabItemDTO = createSlabItemDto("CB101",10.0,10.0,10.0,100.00,"CB101");
        boxes.add(slabItemDTO);
        slabItemDTO = createSlabItemDto("CB102",10.1,10.1,10.1,103.03,"CB102");
        boxes.add(slabItemDTO);
        slabCacheDTO = createSlabCacheDTO(boxes,0,500);
    
        NavigableMap<Double, SlabCacheDTO> navigableMap = new ConcurrentSkipListMap<Double, SlabCacheDTO>();
        navigableMap.put(99.0, slabCacheDTO);
        slabCache.put("Snapdeal-CARTON", navigableMap);
    
        Map<String, List<PackagingTypeCacheDTO>> mtypeToPackagingTypeDTOsMap = new HashMap<String, List<PackagingTypeCacheDTO>>();
        PackagingTypeCache packagingTypeCache = new PackagingTypeCache();
        List<PackagingTypeCacheDTO> lPackagingTypeCacheDTO = new ArrayList<PackagingTypeCacheDTO>();
        PackagingTypeCacheDTO packagingTypeCacheDTO = createPackagingTypeCacheDTO("Snapdeal","CARTON","Normal");
        lPackagingTypeCacheDTO.add(packagingTypeCacheDTO);
    
        mtypeToPackagingTypeDTOsMap.put("CARTON", lPackagingTypeCacheDTO);
    
        PackagingType packagingType = createPackagingType("Snapdeal","Normal");
        pauSRO = createPauSro("990001",10.0,10.0,10.0,"CARTON");
        List<PackagingType> lpackagingList = new ArrayList<PackagingType>();
        lpackagingList.add(packagingType);
        packagingTypeCache.addPackagingType(lpackagingList);
        CacheManager.getInstance().setCache(packagingTypeCache);
        when(mcocofsExternalService.getProductFulfilmentAttributes(Matchers.<GetProductFulfilmentAttributeRequest> any())).thenReturn(pauSRO);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(10.0, 10.0, 10.0)).thenReturn(100.0);
        ReflectionTestUtils.setField(mPackagingRecommendationService, "cocofsExternalService", mcocofsExternalService);
    
        mSupcPackagingTypeMongoService = mock(ISupcPackagingTypeMongoService.class);
        when(mSupcPackagingTypeMongoService.getEnabledAndActivePackagingTypeBySupcAndStoreCode("Snapdeal", "990001")).thenReturn("CARTON");
        ReflectionTestUtils.setField(mruleEngineService, "supcPackagingTypeMongoService", mSupcPackagingTypeMongoService);
    
        ReflectionTestUtils.setField(packagingTypeCache, "typeToPackagingTypeDTOsMap", mtypeToPackagingTypeDTOsMap);
    
        GetPackagingRecommendationsRequest request = mock(GetPackagingRecommendationsRequest.class);
        ReflectionTestUtils.setField(packmanLandingService, "packagingRecomendationService", mPackagingRecommendationService);
        Protocol proto = Protocol.PROTOCOL_JSON;
        when(request.getRequestProtocol()).thenReturn(proto);
        when(request.getApiKey()).thenReturn("p@(km@n");
        when(request.getStoreFrontId()).thenReturn("SD");
        when(request.getIsShipTogether()).thenReturn(false);
        when(request.getPackageDetail()).thenReturn(packageDetailSRO);
    
        CacheManager.getInstance().setCache(mslabMappingCache);
    
        ReflectionTestUtils.setField(mslabMappingCache, "slabCache", slabCache);
        mruleService = mock(IRulesService.class);
        List<BlockParams> lblockParams = new ArrayList<BlockParams>();
        Block block = createBlock("StorePackagingRuleBlock");
        BlockParams blockParams = createBlockParams(block,"LABEL","Label");
        ruleParam.put("BRAND", "StorePackagingRuleBlock");
        Mockito.doReturn(ruleParam).when(packmanPackagingRuleEngineServiceImpl).populateRuleParamsFromBlockObject(Matchers.any(), (List<BlockParams>) Matchers.any());
        lblockParams.add(blockParams);
        mexclusiveService = mock(PackmanExclusiveServiceImpl.class);
        when(mruleService.getBlockParamsByBlockName("StorePackagingRuleBlock")).thenReturn(lblockParams);
        ReflectionTestUtils.setField(mruleEngineService, "ruleService", mruleService);
        ReflectionTestUtils.setField(mruleEngineService, "exclusiveService", mexclusiveService);
        mDepedencyForDTO = mock(com.snapdeal.packman.dto.SlabCacheDTO.IDependency.class);
    
        when(mDepedencyForDTO.getBooleanScalar(Property.USE_PRIMARY_LBH)).thenReturn(false);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_LBH)).thenReturn(true);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_DELTA_VOL_WT_MULT_FACTOR)).thenReturn(2.0);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_LBH_MARGIN)).thenReturn(0.0);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_DELTA_VOL_WT)).thenReturn(false);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_DELTA_VOL_WT_OFFSET)).thenReturn(1000.0);
    
        ReflectionTestUtils.setField(slabCacheDTO, "dep", mDepedencyForDTO);
        GetPackagingRecommendationsResponse resp = packmanLandingService.getPackagingRecommendations(request);
        assertTrue(resp.isSuccessful());
        assertEquals("CARTON", resp.getPackagingRecommendationList().get(0).getPackagingType().getType());
        assertEquals("Normal", resp.getPackagingRecommendationList().get(0).getPackagingMode());
        assertEquals("CB101", resp.getPackagingRecommendationList().get(0).getPreferredPackagingTypeItem().getCode());
    }
    
    //6.a.5
    @Test
    public void testGetPackagingRecommendationsNormalAtSupcLabelOldFeatureAndDeltaAndLBHTrue() throws PackagingRecomendationException, ExternalDataNotFoundException {
    
        List<String> labels = new ArrayList<String>();
        labels.add(Labels.Premium.toString());
        mExternalServiceFactory = mock(ExternalServiceFactory.class);
        ICAMSExternalService miCAMSExternalService = mock(ICAMSExternalService.class);
        GetPOGDetailListResponse response = new GetPOGDetailListResponse();
        POGDetailSRO pogDetailSROs = new POGDetailSRO();
        pogDetailSROs.setEnabled(true);
    
        List<POGDetailSRO> lpogDetailSROs = new ArrayList<POGDetailSRO>();
        lpogDetailSROs.add(pogDetailSROs);
        response.setPogDetailSROs(lpogDetailSROs);
        when(mExternalServiceFactory.getCAMSExternalService()).thenReturn(miCAMSExternalService);
        when(miCAMSExternalService.getPOGDetailsBbySupc(Matchers.<GetPOGDetailListBySUPCListRequest> any())).thenReturn(response);
        ReflectionTestUtils.setField(mruleEngineService, "factory", mExternalServiceFactory);
        PackagableItemSRO packagableItemSRO = createPackagableItemSRO(labels);
        lPackagableItemSRO.add(packagableItemSRO);
        packageDetailSRO.setPackagableItemList(lPackagableItemSRO);
        genericMao = mock(IGenericMao.class);
        ruleList = new ArrayList<>();
        List<CriteriaUnit> lCriteriaUnit = new ArrayList<CriteriaUnit>();
        CriteriaUnit criteriaUnit = createCriteriaUnit(1,"","");
        lCriteriaUnit.add(criteriaUnit);
        PackmanRuleUnit packmanRuleUnit = createPackmanRuleUnit(2,"ru03",7,"CARTON", lCriteriaUnit);
        packmanRuleUnit.setCriteria(lCriteriaUnit);
        ruleList.add(packmanRuleUnit);
        
        doAnswer(new Answer<List<PackmanRuleUnit>>() {
            public List<PackmanRuleUnit> answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                CocofsMongoQuery cocofsMongoQuery = (CocofsMongoQuery) args[0];
                List<Criteria> lCriteria = cocofsMongoQuery.getCriteria();
                Criteria criteria1 = lCriteria.get(0);
                String rule_type = (String) criteria1.getObj();
                if (rule_type.equals("store-packaging"))
                    return ruleList;
                return null;
    
            }
    
        }).when(genericMao).getDocumentList(Matchers.<CocofsMongoQuery> any(), Matchers.<Class<PackmanRuleUnit>> any());
        ReflectionTestUtils.setField(mruleEngineService, "genericMao", genericMao);
    
        PackmanPackagingRuleEngineServiceImpl packmanPackagingRuleEngineServiceImpl = Mockito.spy(new PackmanPackagingRuleEngineServiceImpl());
        Map<String, Object> ruleParam = new HashMap<String, Object>();
        SlabMappingCache mslabMappingCache = new SlabMappingCache();
        Map<String, NavigableMap<Double, SlabCacheDTO>> slabCache = new HashMap<String, NavigableMap<Double, SlabCacheDTO>>();
        SlabCacheDTO slabCacheDTO = new SlabCacheDTO();
        List<SlabItemDTO> boxes = new ArrayList<SlabItemDTO>();
        
        SlabItemDTO slabItemDTO = createSlabItemDto("CB101",10.0,10.0,10.0,100.00,"CB101");
        boxes.add(slabItemDTO);
        slabItemDTO = createSlabItemDto("CB103",10.0,51.0,2.0,102.0,"CB103");
        boxes.add(slabItemDTO);
        slabItemDTO = createSlabItemDto("CB102",10.1,10.1,10.1,103.03,"CB102");
        boxes.add(slabItemDTO);
        slabItemDTO = createSlabItemDto("CB18",11.43,11.43,11.43,149.33,"CB18");
        boxes.add(slabItemDTO);
        slabCacheDTO = createSlabCacheDTO(boxes,0,500);

        NavigableMap<Double, SlabCacheDTO> navigableMap = new ConcurrentSkipListMap<Double, SlabCacheDTO>();
        navigableMap.put(99.0, slabCacheDTO);
        slabCache.put("Snapdeal-CARTON", navigableMap);
    
        Map<String, List<PackagingTypeCacheDTO>> mtypeToPackagingTypeDTOsMap = new HashMap<String, List<PackagingTypeCacheDTO>>();
        PackagingTypeCache packagingTypeCache = new PackagingTypeCache();        
        List<PackagingTypeCacheDTO> lPackagingTypeCacheDTO = new ArrayList<PackagingTypeCacheDTO>();
    
        PackagingTypeCacheDTO packagingTypeCacheDTO = createPackagingTypeCacheDTO("Snapdeal","CARTON","Normal");
        lPackagingTypeCacheDTO.add(packagingTypeCacheDTO);
    
        mtypeToPackagingTypeDTOsMap.put("CARTON", lPackagingTypeCacheDTO);
    
        PackagingType packagingType = createPackagingType("Snapdeal","Normal");
        pauSRO = createPauSro("990001",10.0,10.0,10.0,"CARTON");
        List<PackagingType> lpackagingList = new ArrayList<PackagingType>();
        lpackagingList.add(packagingType);
        packagingTypeCache.addPackagingType(lpackagingList);
        CacheManager.getInstance().setCache(packagingTypeCache);
        when(mcocofsExternalService.getProductFulfilmentAttributes(Matchers.<GetProductFulfilmentAttributeRequest> any())).thenReturn(pauSRO);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(10.0, 10.0, 10.0)).thenReturn(100.0);
        ReflectionTestUtils.setField(mPackagingRecommendationService, "cocofsExternalService", mcocofsExternalService);
    
        mSupcPackagingTypeMongoService = mock(ISupcPackagingTypeMongoService.class);
        when(mSupcPackagingTypeMongoService.getEnabledAndActivePackagingTypeBySupcAndStoreCode("Snapdeal", "990001")).thenReturn("CARTON");
        ReflectionTestUtils.setField(mruleEngineService, "supcPackagingTypeMongoService", mSupcPackagingTypeMongoService);
    
        ReflectionTestUtils.setField(packagingTypeCache, "typeToPackagingTypeDTOsMap", mtypeToPackagingTypeDTOsMap);
    
        GetPackagingRecommendationsRequest request = mock(GetPackagingRecommendationsRequest.class);
        ReflectionTestUtils.setField(packmanLandingService, "packagingRecomendationService", mPackagingRecommendationService);
        Protocol proto = Protocol.PROTOCOL_JSON;
        when(request.getRequestProtocol()).thenReturn(proto);
        when(request.getApiKey()).thenReturn("p@(km@n");
        when(request.getStoreFrontId()).thenReturn("SD");
        when(request.getIsShipTogether()).thenReturn(false);
        when(request.getPackageDetail()).thenReturn(packageDetailSRO);
    
        CacheManager.getInstance().setCache(mslabMappingCache);
    
        ReflectionTestUtils.setField(mslabMappingCache, "slabCache", slabCache);
        mruleService = mock(IRulesService.class);
        List<BlockParams> lblockParams = new ArrayList<BlockParams>();
        Block block = createBlock("StorePackagingRuleBlock");
        BlockParams blockParams = createBlockParams(block,"LABEL","Label");
        ruleParam.put("BRAND", "StorePackagingRuleBlock");
        Mockito.doReturn(ruleParam).when(packmanPackagingRuleEngineServiceImpl).populateRuleParamsFromBlockObject(Matchers.any(), (List<BlockParams>) Matchers.any());
        lblockParams.add(blockParams);
        mexclusiveService = mock(PackmanExclusiveServiceImpl.class);
        when(mruleService.getBlockParamsByBlockName("StorePackagingRuleBlock")).thenReturn(lblockParams);
        ReflectionTestUtils.setField(mruleEngineService, "ruleService", mruleService);
        ReflectionTestUtils.setField(mruleEngineService, "exclusiveService", mexclusiveService);
        mDepedencyForDTO = mock(com.snapdeal.packman.dto.SlabCacheDTO.IDependency.class);
    
        when(mDepedencyForDTO.getBooleanScalar(Property.USE_PRIMARY_LBH)).thenReturn(false);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_LBH)).thenReturn(true);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_DELTA_VOL_WT_MULT_FACTOR)).thenReturn(2.0);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_LBH_MARGIN)).thenReturn(0.0);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_DELTA_VOL_WT)).thenReturn(true);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_DELTA_VOL_WT_OFFSET)).thenReturn(1000.0);
        ReflectionTestUtils.setField(slabCacheDTO, "dep", mDepedencyForDTO);
        GetPackagingRecommendationsResponse resp = packmanLandingService.getPackagingRecommendations(request);
        assertTrue(resp.isSuccessful());
        assertEquals("CARTON", resp.getPackagingRecommendationList().get(0).getPackagingType().getType());
        assertEquals("Normal", resp.getPackagingRecommendationList().get(0).getPackagingMode());
        assertEquals("CB101", resp.getPackagingRecommendationList().get(0).getPreferredPackagingTypeItem().getCode());
    }
    
    //6.a.5
    @Test
    public void testGetPackagingRecommendationsNormalAtSupcLabelOldFeatureAndDeltaAndLBHTrueWithMargin() throws PackagingRecomendationException, ExternalDataNotFoundException {
    
        List<String> labels = new ArrayList<String>();
        labels.add(Labels.Premium.toString());
        mExternalServiceFactory = mock(ExternalServiceFactory.class);
        ICAMSExternalService miCAMSExternalService = mock(ICAMSExternalService.class);
        GetPOGDetailListResponse response = new GetPOGDetailListResponse();
        POGDetailSRO pogDetailSROs = new POGDetailSRO();
        pogDetailSROs.setEnabled(true);
        List<POGDetailSRO> lpogDetailSROs = new ArrayList<POGDetailSRO>();
        lpogDetailSROs.add(pogDetailSROs);
        response.setPogDetailSROs(lpogDetailSROs);
        when(mExternalServiceFactory.getCAMSExternalService()).thenReturn(miCAMSExternalService);
        when(miCAMSExternalService.getPOGDetailsBbySupc(Matchers.<GetPOGDetailListBySUPCListRequest> any())).thenReturn(response);
        ReflectionTestUtils.setField(mruleEngineService, "factory", mExternalServiceFactory);
        PackagableItemSRO packagableItemSRO = createPackagableItemSRO(labels);
        lPackagableItemSRO.add(packagableItemSRO);
        packageDetailSRO.setPackagableItemList(lPackagableItemSRO);
        genericMao = mock(IGenericMao.class);
        ruleList = new ArrayList<>();
        List<CriteriaUnit> lCriteriaUnit = new ArrayList<CriteriaUnit>();
        CriteriaUnit criteriaUnit = createCriteriaUnit(1,"","");
        lCriteriaUnit.add(criteriaUnit);
        PackmanRuleUnit packmanRuleUnit = createPackmanRuleUnit(2,"ru03",7,"CARTON", lCriteriaUnit);

        packmanRuleUnit.setCriteria(lCriteriaUnit);
        ruleList.add(packmanRuleUnit);
        doAnswer(new Answer<List<PackmanRuleUnit>>() {
            public List<PackmanRuleUnit> answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                CocofsMongoQuery cocofsMongoQuery = (CocofsMongoQuery) args[0];
                List<Criteria> lCriteria = cocofsMongoQuery.getCriteria();
                Criteria criteria1 = lCriteria.get(0);
                String rule_type = (String) criteria1.getObj();
                if (rule_type.equals("store-packaging"))
                    return ruleList;
                return null;
    
            }
    
        }).when(genericMao).getDocumentList(Matchers.<CocofsMongoQuery> any(), Matchers.<Class<PackmanRuleUnit>> any());
        ReflectionTestUtils.setField(mruleEngineService, "genericMao", genericMao);
    
        PackmanPackagingRuleEngineServiceImpl packmanPackagingRuleEngineServiceImpl = Mockito.spy(new PackmanPackagingRuleEngineServiceImpl());
        Map<String, Object> ruleParam = new HashMap<String, Object>();
        CatageoryEvaluationParamDto paramDTO = new CatageoryEvaluationParamDto();
        SlabMappingCache mslabMappingCache = new SlabMappingCache();
        Map<String, NavigableMap<Double, SlabCacheDTO>> slabCache = new HashMap<String, NavigableMap<Double, SlabCacheDTO>>();
        SlabCacheDTO slabCacheDTO = new SlabCacheDTO();
        List<SlabItemDTO> boxes = new ArrayList<SlabItemDTO>();
    
        SlabItemDTO slabItemDTO = createSlabItemDto("CB101",10.0,10.0,10.0,100.00,"CB101");
        boxes.add(slabItemDTO);
        slabItemDTO = createSlabItemDto("CB103",10.0,51.0,2.0,102.0,"CB103");
        boxes.add(slabItemDTO);
        slabItemDTO = createSlabItemDto("CB102",10.1,10.1,10.1,103.03,"CB102");
        boxes.add(slabItemDTO);
        slabItemDTO = createSlabItemDto("CB18",11.43,11.43,11.43,149.33,"CB18");
        boxes.add(slabItemDTO);
    
        slabCacheDTO = createSlabCacheDTO(boxes,0,500);
        NavigableMap<Double, SlabCacheDTO> navigableMap = new ConcurrentSkipListMap<Double, SlabCacheDTO>();
        navigableMap.put(99.0, slabCacheDTO);
        slabCache.put("Snapdeal-CARTON", navigableMap);
    
        Map<String, List<PackagingTypeCacheDTO>> mtypeToPackagingTypeDTOsMap = new HashMap<String, List<PackagingTypeCacheDTO>>();
        PackagingTypeCache packagingTypeCache = new PackagingTypeCache();
        List<PackagingTypeCacheDTO> lPackagingTypeCacheDTO = new ArrayList<PackagingTypeCacheDTO>();
        PackagingTypeCacheDTO packagingTypeCacheDTO = createPackagingTypeCacheDTO("Snapdeal","CARTON","Normal");
       lPackagingTypeCacheDTO.add(packagingTypeCacheDTO);
    
        mtypeToPackagingTypeDTOsMap.put("CARTON", lPackagingTypeCacheDTO);
    
        PackagingType packagingType = createPackagingType("Snapdeal","Normal");
        pauSRO = createPauSro("990001",10.0,10.0,10.0,"CARTON");
        List<PackagingType> lpackagingList = new ArrayList<PackagingType>();
        lpackagingList.add(packagingType);
        packagingTypeCache.addPackagingType(lpackagingList);
        CacheManager.getInstance().setCache(packagingTypeCache);
        when(mcocofsExternalService.getProductFulfilmentAttributes(Matchers.<GetProductFulfilmentAttributeRequest> any())).thenReturn(pauSRO);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(10.0, 10.0, 10.0)).thenReturn(100.0);
        ReflectionTestUtils.setField(mPackagingRecommendationService, "cocofsExternalService", mcocofsExternalService);
    
        mSupcPackagingTypeMongoService = mock(ISupcPackagingTypeMongoService.class);
        when(mSupcPackagingTypeMongoService.getEnabledAndActivePackagingTypeBySupcAndStoreCode("Snapdeal", "990001")).thenReturn("CARTON");
        ReflectionTestUtils.setField(mruleEngineService, "supcPackagingTypeMongoService", mSupcPackagingTypeMongoService);
    
        ReflectionTestUtils.setField(packagingTypeCache, "typeToPackagingTypeDTOsMap", mtypeToPackagingTypeDTOsMap);
    
        GetPackagingRecommendationsRequest request = mock(GetPackagingRecommendationsRequest.class);
        ReflectionTestUtils.setField(packmanLandingService, "packagingRecomendationService", mPackagingRecommendationService);
        Protocol proto = Protocol.PROTOCOL_JSON;
        when(request.getRequestProtocol()).thenReturn(proto);
        when(request.getApiKey()).thenReturn("p@(km@n");
        when(request.getStoreFrontId()).thenReturn("SD");
        when(request.getIsShipTogether()).thenReturn(false);
        when(request.getPackageDetail()).thenReturn(packageDetailSRO);
    
        CacheManager.getInstance().setCache(mslabMappingCache);
    
        ReflectionTestUtils.setField(mslabMappingCache, "slabCache", slabCache);
        mruleService = mock(IRulesService.class);
        List<BlockParams> lblockParams = new ArrayList<BlockParams>();
        Block block = createBlock("StorePackagingRuleBlock");
        BlockParams blockParams = createBlockParams(block,"LABEL","Label");
        ruleParam.put("BRAND", "StorePackagingRuleBlock");
        Mockito.doReturn(ruleParam).when(packmanPackagingRuleEngineServiceImpl).populateRuleParamsFromBlockObject(Matchers.any(), (List<BlockParams>) Matchers.any());
        lblockParams.add(blockParams);
        mexclusiveService = mock(PackmanExclusiveServiceImpl.class);
        when(mruleService.getBlockParamsByBlockName("StorePackagingRuleBlock")).thenReturn(lblockParams);
        ReflectionTestUtils.setField(mruleEngineService, "ruleService", mruleService);
        ReflectionTestUtils.setField(mruleEngineService, "exclusiveService", mexclusiveService);
        mDepedencyForDTO = mock(com.snapdeal.packman.dto.SlabCacheDTO.IDependency.class);
    
        when(mDepedencyForDTO.getBooleanScalar(Property.USE_PRIMARY_LBH)).thenReturn(false);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_LBH)).thenReturn(true);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_DELTA_VOL_WT_MULT_FACTOR)).thenReturn(2.0);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_LBH_MARGIN)).thenReturn(0.1);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_DELTA_VOL_WT)).thenReturn(true);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_DELTA_VOL_WT_OFFSET)).thenReturn(1000.0);
    
        ReflectionTestUtils.setField(slabCacheDTO, "dep", mDepedencyForDTO);
        GetPackagingRecommendationsResponse resp = packmanLandingService.getPackagingRecommendations(request);
        assertTrue(resp.isSuccessful());
        assertEquals("CARTON", resp.getPackagingRecommendationList().get(0).getPackagingType().getType());
        assertEquals("Normal", resp.getPackagingRecommendationList().get(0).getPackagingMode());
        assertEquals("CB101", resp.getPackagingRecommendationList().get(0).getPreferredPackagingTypeItem().getCode());
    }
    
    //6.b.1
    @Test
    public void testGetPackagingRecommendationsNormalAtSupcLabelOldFeatureNewLBH() throws PackagingRecomendationException, ExternalDataNotFoundException {
    
        List<String> labels = new ArrayList<String>();
        labels.add(Labels.Premium.toString());
        mExternalServiceFactory = mock(ExternalServiceFactory.class);
        ICAMSExternalService miCAMSExternalService = mock(ICAMSExternalService.class);
        GetPOGDetailListResponse response = new GetPOGDetailListResponse();
        POGDetailSRO pogDetailSROs = new POGDetailSRO();
        pogDetailSROs.setEnabled(true);
    
        List<POGDetailSRO> lpogDetailSROs = new ArrayList<POGDetailSRO>();
        lpogDetailSROs.add(pogDetailSROs);
        response.setPogDetailSROs(lpogDetailSROs);
        when(mExternalServiceFactory.getCAMSExternalService()).thenReturn(miCAMSExternalService);
        when(miCAMSExternalService.getPOGDetailsBbySupc(Matchers.<GetPOGDetailListBySUPCListRequest> any())).thenReturn(response);
        ReflectionTestUtils.setField(mruleEngineService, "factory", mExternalServiceFactory);
        PackagableItemSRO packagableItemSRO = createPackagableItemSRO(labels);
        lPackagableItemSRO.add(packagableItemSRO);
        packageDetailSRO.setPackagableItemList(lPackagableItemSRO);
        genericMao = mock(IGenericMao.class);
        ruleList = new ArrayList<>();
        List<CriteriaUnit> lCriteriaUnit = new ArrayList<CriteriaUnit>();
        CriteriaUnit criteriaUnit = createCriteriaUnit(1,"","");
        lCriteriaUnit.add(criteriaUnit);
        PackmanRuleUnit packmanRuleUnit = createPackmanRuleUnit(2,"ru03",7,"CARTON", lCriteriaUnit);

        packmanRuleUnit.setCriteria(lCriteriaUnit);
        ruleList.add(packmanRuleUnit);
        doAnswer(new Answer<List<PackmanRuleUnit>>() {
            public List<PackmanRuleUnit> answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                CocofsMongoQuery cocofsMongoQuery = (CocofsMongoQuery) args[0];
                List<Criteria> lCriteria = cocofsMongoQuery.getCriteria();
                Criteria criteria1 = lCriteria.get(0);
                String rule_type = (String) criteria1.getObj();
                if (rule_type.equals("store-packaging"))
                    return ruleList;
                return null;
    
            }
    
        }).when(genericMao).getDocumentList(Matchers.<CocofsMongoQuery> any(), Matchers.<Class<PackmanRuleUnit>> any());
        ReflectionTestUtils.setField(mruleEngineService, "genericMao", genericMao);
    
        PackmanPackagingRuleEngineServiceImpl packmanPackagingRuleEngineServiceImpl = Mockito.spy(new PackmanPackagingRuleEngineServiceImpl());
        Map<String, Object> ruleParam = new HashMap<String, Object>();
        SlabMappingCache mslabMappingCache = new SlabMappingCache();
        Map<String, NavigableMap<Double, SlabCacheDTO>> slabCache = new HashMap<String, NavigableMap<Double, SlabCacheDTO>>();
        SlabCacheDTO slabCacheDTO = new SlabCacheDTO();
        List<SlabItemDTO> boxes = new ArrayList<SlabItemDTO>();
    
        SlabItemDTO slabItemDTO = createSlabItemDto("CB101",10.0,10.0,10.0,100.00,"CB101");
        boxes.add(slabItemDTO);
        slabItemDTO = createSlabItemDto("CB103",10.0,51.0,2.0,102.0,"CB103");
        boxes.add(slabItemDTO);
        slabItemDTO = createSlabItemDto("CB102",10.1,10.1,10.1,103.03,"CB102");
        boxes.add(slabItemDTO);
        slabItemDTO = createSlabItemDto("CB18",11.43,11.43,11.43,149.33,"CB18");
        boxes.add(slabItemDTO);
        slabCacheDTO = createSlabCacheDTO(boxes,0,500);
        NavigableMap<Double, SlabCacheDTO> navigableMap = new ConcurrentSkipListMap<Double, SlabCacheDTO>();
        navigableMap.put(99.0, slabCacheDTO);
        slabCache.put("Snapdeal-CARTON", navigableMap);
    
        Map<String, List<PackagingTypeCacheDTO>> mtypeToPackagingTypeDTOsMap = new HashMap<String, List<PackagingTypeCacheDTO>>();
        PackagingTypeCache packagingTypeCache = new PackagingTypeCache();
        List<PackagingTypeCacheDTO> lPackagingTypeCacheDTO = new ArrayList<PackagingTypeCacheDTO>();
    
        PackagingTypeCacheDTO packagingTypeCacheDTO = createPackagingTypeCacheDTO("Snapdeal","CARTON","Normal");
        lPackagingTypeCacheDTO.add(packagingTypeCacheDTO);
        mtypeToPackagingTypeDTOsMap.put("CARTON", lPackagingTypeCacheDTO);
    
        PackagingType packagingType = createPackagingType("Snapdeal","Normal");
        pauSRO = createPauSro("990001",10.0,10.0,10.1,"CARTON");
        List<PackagingType> lpackagingList = new ArrayList<PackagingType>();
        lpackagingList.add(packagingType);
        packagingTypeCache.addPackagingType(lpackagingList);
        CacheManager.getInstance().setCache(packagingTypeCache);
        when(mcocofsExternalService.getProductFulfilmentAttributes(Matchers.<GetProductFulfilmentAttributeRequest> any())).thenReturn(pauSRO);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(10.0, 10.0, 10.1)).thenReturn(101.0);
        ReflectionTestUtils.setField(mPackagingRecommendationService, "cocofsExternalService", mcocofsExternalService);
    
        mSupcPackagingTypeMongoService = mock(ISupcPackagingTypeMongoService.class);
        when(mSupcPackagingTypeMongoService.getEnabledAndActivePackagingTypeBySupcAndStoreCode("Snapdeal", "990001")).thenReturn("CARTON");
        ReflectionTestUtils.setField(mruleEngineService, "supcPackagingTypeMongoService", mSupcPackagingTypeMongoService);
    
        ReflectionTestUtils.setField(packagingTypeCache, "typeToPackagingTypeDTOsMap", mtypeToPackagingTypeDTOsMap);
    
        GetPackagingRecommendationsRequest request = mock(GetPackagingRecommendationsRequest.class);
        ReflectionTestUtils.setField(packmanLandingService, "packagingRecomendationService", mPackagingRecommendationService);
        Protocol proto = Protocol.PROTOCOL_JSON;
        when(request.getRequestProtocol()).thenReturn(proto);
        when(request.getApiKey()).thenReturn("p@(km@n");
        when(request.getStoreFrontId()).thenReturn("SD");
        when(request.getIsShipTogether()).thenReturn(false);
        when(request.getPackageDetail()).thenReturn(packageDetailSRO);
    
        CacheManager.getInstance().setCache(mslabMappingCache);
    
        ReflectionTestUtils.setField(mslabMappingCache, "slabCache", slabCache);
        mruleService = mock(IRulesService.class);
        List<BlockParams> lblockParams = new ArrayList<BlockParams>();
        Block block = createBlock("StorePackagingRuleBlock");
        BlockParams blockParams = createBlockParams(block,"LABEL","Label");
        ruleParam.put("BRAND", "StorePackagingRuleBlock");
        Mockito.doReturn(ruleParam).when(packmanPackagingRuleEngineServiceImpl).populateRuleParamsFromBlockObject(Matchers.any(), (List<BlockParams>) Matchers.any());
        lblockParams.add(blockParams);
        mexclusiveService = mock(PackmanExclusiveServiceImpl.class);
        when(mruleService.getBlockParamsByBlockName("StorePackagingRuleBlock")).thenReturn(lblockParams);
        ReflectionTestUtils.setField(mruleEngineService, "ruleService", mruleService);
        ReflectionTestUtils.setField(mruleEngineService, "exclusiveService", mexclusiveService);
        mDepedencyForDTO = mock(com.snapdeal.packman.dto.SlabCacheDTO.IDependency.class);
    
        when(mDepedencyForDTO.getBooleanScalar(Property.USE_PRIMARY_LBH)).thenReturn(false);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_LBH)).thenReturn(false);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_DELTA_VOL_WT_MULT_FACTOR)).thenReturn(2.0);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_LBH_MARGIN)).thenReturn(0.0);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_DELTA_VOL_WT)).thenReturn(false);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_DELTA_VOL_WT_OFFSET)).thenReturn(1000.0);
    
        ReflectionTestUtils.setField(slabCacheDTO, "dep", mDepedencyForDTO);
        GetPackagingRecommendationsResponse resp = packmanLandingService.getPackagingRecommendations(request);
        assertTrue(resp.isSuccessful());
        assertEquals("CARTON", resp.getPackagingRecommendationList().get(0).getPackagingType().getType());
        assertEquals("Normal", resp.getPackagingRecommendationList().get(0).getPackagingMode());
        assertEquals("CB103", resp.getPackagingRecommendationList().get(0).getPreferredPackagingTypeItem().getCode());
    }
    
    //6.b.2
    @Test
    public void testGetPackagingRecommendationsNormalAtSupcLabelOldFeatureNewLBHWithDeltaFeature() throws PackagingRecomendationException, ExternalDataNotFoundException {
    
        List<String> labels = new ArrayList<String>();
        labels.add(Labels.Premium.toString());
        mExternalServiceFactory = mock(ExternalServiceFactory.class);
        ICAMSExternalService miCAMSExternalService = mock(ICAMSExternalService.class);
        GetPOGDetailListResponse response = new GetPOGDetailListResponse();
        POGDetailSRO pogDetailSROs = new POGDetailSRO();
        pogDetailSROs.setEnabled(true);
    
        List<POGDetailSRO> lpogDetailSROs = new ArrayList<POGDetailSRO>();
        lpogDetailSROs.add(pogDetailSROs);
        response.setPogDetailSROs(lpogDetailSROs);
        when(mExternalServiceFactory.getCAMSExternalService()).thenReturn(miCAMSExternalService);
        when(miCAMSExternalService.getPOGDetailsBbySupc(Matchers.<GetPOGDetailListBySUPCListRequest> any())).thenReturn(response);
        ReflectionTestUtils.setField(mruleEngineService, "factory", mExternalServiceFactory);
        PackagableItemSRO packagableItemSRO = createPackagableItemSRO(labels);
        lPackagableItemSRO.add(packagableItemSRO);
        packageDetailSRO.setPackagableItemList(lPackagableItemSRO);
        genericMao = mock(IGenericMao.class);
        ruleList = new ArrayList<>();
        List<CriteriaUnit> lCriteriaUnit = new ArrayList<CriteriaUnit>();
        CriteriaUnit criteriaUnit = createCriteriaUnit(1,"","");
        lCriteriaUnit.add(criteriaUnit);
        PackmanRuleUnit packmanRuleUnit = createPackmanRuleUnit(2,"ru03",7,"CARTON", lCriteriaUnit);
        packmanRuleUnit.setCriteria(lCriteriaUnit);
        ruleList.add(packmanRuleUnit);
        doAnswer(new Answer<List<PackmanRuleUnit>>() {
            public List<PackmanRuleUnit> answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                CocofsMongoQuery cocofsMongoQuery = (CocofsMongoQuery) args[0];
                List<Criteria> lCriteria = cocofsMongoQuery.getCriteria();
                Criteria criteria1 = lCriteria.get(0);
                String rule_type = (String) criteria1.getObj();
                if (rule_type.equals("store-packaging"))
                    return ruleList;
                return null;
    
            }
    
        }).when(genericMao).getDocumentList(Matchers.<CocofsMongoQuery> any(), Matchers.<Class<PackmanRuleUnit>> any());
        ReflectionTestUtils.setField(mruleEngineService, "genericMao", genericMao);
    
        PackmanPackagingRuleEngineServiceImpl packmanPackagingRuleEngineServiceImpl = Mockito.spy(new PackmanPackagingRuleEngineServiceImpl());
        Map<String, Object> ruleParam = new HashMap<String, Object>();
        SlabMappingCache mslabMappingCache = new SlabMappingCache();
        Map<String, NavigableMap<Double, SlabCacheDTO>> slabCache = new HashMap<String, NavigableMap<Double, SlabCacheDTO>>();
        SlabCacheDTO slabCacheDTO = new SlabCacheDTO();
        List<SlabItemDTO> boxes = new ArrayList<SlabItemDTO>();
    
        SlabItemDTO slabItemDTO = createSlabItemDto("CB101",10.0,10.0,10.0,100.00,"CB101");
        boxes.add(slabItemDTO);
        slabItemDTO = createSlabItemDto("CB103",10.0,51.0,2.0,102.0,"CB103");
        boxes.add(slabItemDTO);
        slabItemDTO = createSlabItemDto("CB102",10.1,10.1,10.1,103.03,"CB102");
        boxes.add(slabItemDTO);
        slabItemDTO = createSlabItemDto("CB18",11.43,11.43,11.43,149.33,"CB18");
        boxes.add(slabItemDTO);
    
        slabCacheDTO = createSlabCacheDTO(boxes,0,500);
        NavigableMap<Double, SlabCacheDTO> navigableMap = new ConcurrentSkipListMap<Double, SlabCacheDTO>();
        navigableMap.put(99.0, slabCacheDTO);
        slabCache.put("Snapdeal-CARTON", navigableMap);
    
        Map<String, List<PackagingTypeCacheDTO>> mtypeToPackagingTypeDTOsMap = new HashMap<String, List<PackagingTypeCacheDTO>>();
        PackagingTypeCache packagingTypeCache = new PackagingTypeCache();
        List<PackagingTypeCacheDTO> lPackagingTypeCacheDTO = new ArrayList<PackagingTypeCacheDTO>();
        PackagingTypeCacheDTO packagingTypeCacheDTO = createPackagingTypeCacheDTO("Snapdeal","CARTON","Normal");
        lPackagingTypeCacheDTO.add(packagingTypeCacheDTO);
        mtypeToPackagingTypeDTOsMap.put("CARTON", lPackagingTypeCacheDTO);
    
        PackagingType packagingType = createPackagingType("Snapdeal","Normal");
        pauSRO = createPauSro("990001",10.0,10.0,10.1,"CARTON");
        List<PackagingType> lpackagingList = new ArrayList<PackagingType>();
        lpackagingList.add(packagingType);
        packagingTypeCache.addPackagingType(lpackagingList);
        CacheManager.getInstance().setCache(packagingTypeCache);
        when(mcocofsExternalService.getProductFulfilmentAttributes(Matchers.<GetProductFulfilmentAttributeRequest> any())).thenReturn(pauSRO);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(10.0, 10.0, 10.0)).thenReturn(101.0);
        ReflectionTestUtils.setField(mPackagingRecommendationService, "cocofsExternalService", mcocofsExternalService);
    
        mSupcPackagingTypeMongoService = mock(ISupcPackagingTypeMongoService.class);
        when(mSupcPackagingTypeMongoService.getEnabledAndActivePackagingTypeBySupcAndStoreCode("Snapdeal", "990001")).thenReturn("CARTON");
        ReflectionTestUtils.setField(mruleEngineService, "supcPackagingTypeMongoService", mSupcPackagingTypeMongoService);
    
        ReflectionTestUtils.setField(packagingTypeCache, "typeToPackagingTypeDTOsMap", mtypeToPackagingTypeDTOsMap);
    
        GetPackagingRecommendationsRequest request = mock(GetPackagingRecommendationsRequest.class);
        ReflectionTestUtils.setField(packmanLandingService, "packagingRecomendationService", mPackagingRecommendationService);
        Protocol proto = Protocol.PROTOCOL_JSON;
        when(request.getRequestProtocol()).thenReturn(proto);
        when(request.getApiKey()).thenReturn("p@(km@n");
        when(request.getStoreFrontId()).thenReturn("SD");
        when(request.getIsShipTogether()).thenReturn(false);
        when(request.getPackageDetail()).thenReturn(packageDetailSRO);
    
        CacheManager.getInstance().setCache(mslabMappingCache);
    
        ReflectionTestUtils.setField(mslabMappingCache, "slabCache", slabCache);
        mruleService = mock(IRulesService.class);
        List<BlockParams> lblockParams = new ArrayList<BlockParams>();
        Block block = createBlock("StorePackagingRuleBlock");
        BlockParams blockParams = createBlockParams(block,"LABEL","Label");
        ruleParam.put("BRAND", "StorePackagingRuleBlock");
        Mockito.doReturn(ruleParam).when(packmanPackagingRuleEngineServiceImpl).populateRuleParamsFromBlockObject(Matchers.any(), (List<BlockParams>) Matchers.any());
        lblockParams.add(blockParams);
        mexclusiveService = mock(PackmanExclusiveServiceImpl.class);
        when(mruleService.getBlockParamsByBlockName("StorePackagingRuleBlock")).thenReturn(lblockParams);
        ReflectionTestUtils.setField(mruleEngineService, "ruleService", mruleService);
        ReflectionTestUtils.setField(mruleEngineService, "exclusiveService", mexclusiveService);
        mDepedencyForDTO = mock(com.snapdeal.packman.dto.SlabCacheDTO.IDependency.class);
    
        when(mDepedencyForDTO.getBooleanScalar(Property.USE_PRIMARY_LBH)).thenReturn(false);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_LBH)).thenReturn(true);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_DELTA_VOL_WT_MULT_FACTOR)).thenReturn(2.0);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_LBH_MARGIN)).thenReturn(0.0);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_DELTA_VOL_WT)).thenReturn(true);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_DELTA_VOL_WT_OFFSET)).thenReturn(1000.0);
    
        ReflectionTestUtils.setField(slabCacheDTO, "dep", mDepedencyForDTO);
        GetPackagingRecommendationsResponse resp = packmanLandingService.getPackagingRecommendations(request);
        assertTrue(resp.isSuccessful());
        assertEquals("CARTON", resp.getPackagingRecommendationList().get(0).getPackagingType().getType());
        assertEquals("Normal", resp.getPackagingRecommendationList().get(0).getPackagingMode());
    }
    
    //6.b.3
    @Test
    public void testGetPackagingRecommendationsNormalAtSupcLabelOldFeatureNewLBHandLBHFlagOn() throws PackagingRecomendationException, ExternalDataNotFoundException {
    
        List<String> labels = new ArrayList<String>();
        labels.add(Labels.Premium.toString());
        mExternalServiceFactory = mock(ExternalServiceFactory.class);
        ICAMSExternalService miCAMSExternalService = mock(ICAMSExternalService.class);
        GetPOGDetailListResponse response = new GetPOGDetailListResponse();
        POGDetailSRO pogDetailSROs = new POGDetailSRO();
        pogDetailSROs.setEnabled(true);
    
        List<POGDetailSRO> lpogDetailSROs = new ArrayList<POGDetailSRO>();
        lpogDetailSROs.add(pogDetailSROs);
        response.setPogDetailSROs(lpogDetailSROs);
        when(mExternalServiceFactory.getCAMSExternalService()).thenReturn(miCAMSExternalService);
        when(miCAMSExternalService.getPOGDetailsBbySupc(Matchers.<GetPOGDetailListBySUPCListRequest> any())).thenReturn(response);
        ReflectionTestUtils.setField(mruleEngineService, "factory", mExternalServiceFactory);
        PackagableItemSRO packagableItemSRO = createPackagableItemSRO(labels);
        lPackagableItemSRO.add(packagableItemSRO);
        packageDetailSRO.setPackagableItemList(lPackagableItemSRO);
        genericMao = mock(IGenericMao.class);
        ruleList = new ArrayList<>();
        List<CriteriaUnit> lCriteriaUnit = new ArrayList<CriteriaUnit>();
        CriteriaUnit criteriaUnit = createCriteriaUnit(1,"","");
        lCriteriaUnit.add(criteriaUnit);
        PackmanRuleUnit packmanRuleUnit = createPackmanRuleUnit(2,"ru03",7,"CARTON", lCriteriaUnit);

        packmanRuleUnit.setCriteria(lCriteriaUnit);
        ruleList.add(packmanRuleUnit);
        doAnswer(new Answer<List<PackmanRuleUnit>>() {
            public List<PackmanRuleUnit> answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                CocofsMongoQuery cocofsMongoQuery = (CocofsMongoQuery) args[0];
                List<Criteria> lCriteria = cocofsMongoQuery.getCriteria();
                Criteria criteria1 = lCriteria.get(0);
                String rule_type = (String) criteria1.getObj();
                if (rule_type.equals("store-packaging"))
                    return ruleList;
                return null;
    
            }
    
        }).when(genericMao).getDocumentList(Matchers.<CocofsMongoQuery> any(), Matchers.<Class<PackmanRuleUnit>> any());
        ReflectionTestUtils.setField(mruleEngineService, "genericMao", genericMao);
    
        PackmanPackagingRuleEngineServiceImpl packmanPackagingRuleEngineServiceImpl = Mockito.spy(new PackmanPackagingRuleEngineServiceImpl());
        Map<String, Object> ruleParam = new HashMap<String, Object>();
        SlabMappingCache mslabMappingCache = new SlabMappingCache();
        Map<String, NavigableMap<Double, SlabCacheDTO>> slabCache = new HashMap<String, NavigableMap<Double, SlabCacheDTO>>();
        SlabCacheDTO slabCacheDTO = new SlabCacheDTO();
        List<SlabItemDTO> boxes = new ArrayList<SlabItemDTO>();    
        SlabItemDTO slabItemDTO = createSlabItemDto("CB101",10.0,10.0,10.0,100.00,"CB101");
        boxes.add(slabItemDTO);
        slabItemDTO = createSlabItemDto("CB103",10.0,51.0,2.0,102.0,"CB103");
        boxes.add(slabItemDTO);
        slabItemDTO = createSlabItemDto("CB102",10.1,10.1,10.1,103.03,"CB102");
        boxes.add(slabItemDTO);
        slabItemDTO = createSlabItemDto("CB18",11.43,11.43,11.43,149.33,"CB18");
        boxes.add(slabItemDTO);
        slabCacheDTO = createSlabCacheDTO(boxes,0,500);
        NavigableMap<Double, SlabCacheDTO> navigableMap = new ConcurrentSkipListMap<Double, SlabCacheDTO>();
        navigableMap.put(99.0, slabCacheDTO);
        slabCache.put("Snapdeal-CARTON", navigableMap);
    
        Map<String, List<PackagingTypeCacheDTO>> mtypeToPackagingTypeDTOsMap = new HashMap<String, List<PackagingTypeCacheDTO>>();
        PackagingTypeCache packagingTypeCache = new PackagingTypeCache();
        List<PackagingTypeCacheDTO> lPackagingTypeCacheDTO = new ArrayList<PackagingTypeCacheDTO>();
    
        PackagingTypeCacheDTO packagingTypeCacheDTO = createPackagingTypeCacheDTO("Snapdeal","CARTON","Normal");
        lPackagingTypeCacheDTO.add(packagingTypeCacheDTO);
        mtypeToPackagingTypeDTOsMap.put("CARTON", lPackagingTypeCacheDTO);
    
        PackagingType packagingType = createPackagingType("Snapdeal","Normal");
        pauSRO = createPauSro("990001",10.0,10.0,10.1,"CARTON");
        List<PackagingType> lpackagingList = new ArrayList<PackagingType>();
        lpackagingList.add(packagingType);
        packagingTypeCache.addPackagingType(lpackagingList);
        CacheManager.getInstance().setCache(packagingTypeCache);
        when(mcocofsExternalService.getProductFulfilmentAttributes(Matchers.<GetProductFulfilmentAttributeRequest> any())).thenReturn(pauSRO);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(10.0, 10.0, 10.1)).thenReturn(101.0);
        ReflectionTestUtils.setField(mPackagingRecommendationService, "cocofsExternalService", mcocofsExternalService);
    
        mSupcPackagingTypeMongoService = mock(ISupcPackagingTypeMongoService.class);
        when(mSupcPackagingTypeMongoService.getEnabledAndActivePackagingTypeBySupcAndStoreCode("Snapdeal", "990001")).thenReturn("CARTON");
        ReflectionTestUtils.setField(mruleEngineService, "supcPackagingTypeMongoService", mSupcPackagingTypeMongoService);
    
        ReflectionTestUtils.setField(packagingTypeCache, "typeToPackagingTypeDTOsMap", mtypeToPackagingTypeDTOsMap);
    
        GetPackagingRecommendationsRequest request = mock(GetPackagingRecommendationsRequest.class);
        ReflectionTestUtils.setField(packmanLandingService, "packagingRecomendationService", mPackagingRecommendationService);
        Protocol proto = Protocol.PROTOCOL_JSON;
        when(request.getRequestProtocol()).thenReturn(proto);
        when(request.getApiKey()).thenReturn("p@(km@n");
        when(request.getStoreFrontId()).thenReturn("SD");
        when(request.getIsShipTogether()).thenReturn(false);
        when(request.getPackageDetail()).thenReturn(packageDetailSRO);
    
        CacheManager.getInstance().setCache(mslabMappingCache);
    
        ReflectionTestUtils.setField(mslabMappingCache, "slabCache", slabCache);
        mruleService = mock(IRulesService.class);
        List<BlockParams> lblockParams = new ArrayList<BlockParams>();
        Block block = createBlock("StorePackagingRuleBlock");
        BlockParams blockParams = createBlockParams(block,"LABEL","Label");
        ruleParam.put("BRAND", "StorePackagingRuleBlock");
        Mockito.doReturn(ruleParam).when(packmanPackagingRuleEngineServiceImpl).populateRuleParamsFromBlockObject(Matchers.any(), (List<BlockParams>) Matchers.any());
        lblockParams.add(blockParams);
        mexclusiveService = mock(PackmanExclusiveServiceImpl.class);
        when(mruleService.getBlockParamsByBlockName("StorePackagingRuleBlock")).thenReturn(lblockParams);
        ReflectionTestUtils.setField(mruleEngineService, "ruleService", mruleService);
        ReflectionTestUtils.setField(mruleEngineService, "exclusiveService", mexclusiveService);
        mDepedencyForDTO = mock(com.snapdeal.packman.dto.SlabCacheDTO.IDependency.class);
    
        when(mDepedencyForDTO.getBooleanScalar(Property.USE_PRIMARY_LBH)).thenReturn(false);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_LBH)).thenReturn(true);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_DELTA_VOL_WT_MULT_FACTOR)).thenReturn(2.0);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_LBH_MARGIN)).thenReturn(0.0);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_DELTA_VOL_WT)).thenReturn(false);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_DELTA_VOL_WT_OFFSET)).thenReturn(1000.0);
        ReflectionTestUtils.setField(slabCacheDTO, "dep", mDepedencyForDTO);
        GetPackagingRecommendationsResponse resp = packmanLandingService.getPackagingRecommendations(request);
        assertTrue(resp.isSuccessful());
        assertEquals("CARTON", resp.getPackagingRecommendationList().get(0).getPackagingType().getType());
        assertEquals("Normal", resp.getPackagingRecommendationList().get(0).getPackagingMode());
    }
    
    //6.3.4
    @Test
    public void testGetPackagingRecommendationsNormalAtSupcLabelOldFeatureNewLBHandLBHFlagOnWithMargin() throws PackagingRecomendationException, ExternalDataNotFoundException {
    
        List<String> labels = new ArrayList<String>();
        labels.add(Labels.Premium.toString());
        mExternalServiceFactory = mock(ExternalServiceFactory.class);
        ICAMSExternalService miCAMSExternalService = mock(ICAMSExternalService.class);
        GetPOGDetailListResponse response = new GetPOGDetailListResponse();
        POGDetailSRO pogDetailSROs = new POGDetailSRO();
        pogDetailSROs.setEnabled(true);
    
        List<POGDetailSRO> lpogDetailSROs = new ArrayList<POGDetailSRO>();
        lpogDetailSROs.add(pogDetailSROs);
        response.setPogDetailSROs(lpogDetailSROs);
        when(mExternalServiceFactory.getCAMSExternalService()).thenReturn(miCAMSExternalService);
        when(miCAMSExternalService.getPOGDetailsBbySupc(Matchers.<GetPOGDetailListBySUPCListRequest> any())).thenReturn(response);
        ReflectionTestUtils.setField(mruleEngineService, "factory", mExternalServiceFactory);
        PackagableItemSRO packagableItemSRO = createPackagableItemSRO(labels);
        lPackagableItemSRO.add(packagableItemSRO);
        packageDetailSRO.setPackagableItemList(lPackagableItemSRO);
        genericMao = mock(IGenericMao.class);
        ruleList = new ArrayList<>();
        List<CriteriaUnit> lCriteriaUnit = new ArrayList<CriteriaUnit>();
        CriteriaUnit criteriaUnit = createCriteriaUnit(1,"","");
        lCriteriaUnit.add(criteriaUnit);
         PackmanRuleUnit packmanRuleUnit = createPackmanRuleUnit(2,"ru03",7,"CARTON", lCriteriaUnit);

        packmanRuleUnit.setCriteria(lCriteriaUnit);
        ruleList.add(packmanRuleUnit);
        doAnswer(new Answer<List<PackmanRuleUnit>>() {
            public List<PackmanRuleUnit> answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                CocofsMongoQuery cocofsMongoQuery = (CocofsMongoQuery) args[0];
                List<Criteria> lCriteria = cocofsMongoQuery.getCriteria();
                Criteria criteria1 = lCriteria.get(0);
                String rule_type = (String) criteria1.getObj();
                if (rule_type.equals("store-packaging"))
                    return ruleList;
                return null;
    
            }
    
        }).when(genericMao).getDocumentList(Matchers.<CocofsMongoQuery> any(), Matchers.<Class<PackmanRuleUnit>> any());
        ReflectionTestUtils.setField(mruleEngineService, "genericMao", genericMao);
    
        PackmanPackagingRuleEngineServiceImpl packmanPackagingRuleEngineServiceImpl = Mockito.spy(new PackmanPackagingRuleEngineServiceImpl());
        Map<String, Object> ruleParam = new HashMap<String, Object>();
        SlabMappingCache mslabMappingCache = new SlabMappingCache();
        Map<String, NavigableMap<Double, SlabCacheDTO>> slabCache = new HashMap<String, NavigableMap<Double, SlabCacheDTO>>();
        SlabCacheDTO slabCacheDTO = new SlabCacheDTO();
        List<SlabItemDTO> boxes = new ArrayList<SlabItemDTO>();
    
        SlabItemDTO slabItemDTO = createSlabItemDto("CB101",10.0,10.0,10.0,100.00,"CB101");
        boxes.add(slabItemDTO);
        slabItemDTO = createSlabItemDto("CB103",10.0,51.0,2.0,102.0,"CB103");
        boxes.add(slabItemDTO);
        slabItemDTO = createSlabItemDto("CB102",10.1,10.1,10.1,103.03,"CB102");
        boxes.add(slabItemDTO);
        slabItemDTO = createSlabItemDto("CB18",11.43,11.43,11.43,149.33,"CB18");
        boxes.add(slabItemDTO);
        slabCacheDTO = createSlabCacheDTO(boxes,0,500);
        NavigableMap<Double, SlabCacheDTO> navigableMap = new ConcurrentSkipListMap<Double, SlabCacheDTO>();
        navigableMap.put(99.0, slabCacheDTO);
        slabCache.put("Snapdeal-CARTON", navigableMap);
    
        Map<String, List<PackagingTypeCacheDTO>> mtypeToPackagingTypeDTOsMap = new HashMap<String, List<PackagingTypeCacheDTO>>();
        PackagingTypeCache packagingTypeCache = new PackagingTypeCache();
        List<PackagingTypeCacheDTO> lPackagingTypeCacheDTO = new ArrayList<PackagingTypeCacheDTO>();
    
        PackagingTypeCacheDTO packagingTypeCacheDTO = createPackagingTypeCacheDTO("Snapdeal","CARTON","Normal");
        lPackagingTypeCacheDTO.add(packagingTypeCacheDTO);
        mtypeToPackagingTypeDTOsMap.put("CARTON", lPackagingTypeCacheDTO);
    
        PackagingType packagingType = createPackagingType("Snapdeal","Normal");
        pauSRO = createPauSro("990001",10.0,10.0,10.1,"CARTON");
        List<PackagingType> lpackagingList = new ArrayList<PackagingType>();
        lpackagingList.add(packagingType);
        packagingTypeCache.addPackagingType(lpackagingList);
        CacheManager.getInstance().setCache(packagingTypeCache);
        when(mcocofsExternalService.getProductFulfilmentAttributes(Matchers.<GetProductFulfilmentAttributeRequest> any())).thenReturn(pauSRO);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(10.0, 10.0, 10.1)).thenReturn(101.0);
        ReflectionTestUtils.setField(mPackagingRecommendationService, "cocofsExternalService", mcocofsExternalService);
    
        mSupcPackagingTypeMongoService = mock(ISupcPackagingTypeMongoService.class);
        when(mSupcPackagingTypeMongoService.getEnabledAndActivePackagingTypeBySupcAndStoreCode("Snapdeal", "990001")).thenReturn("CARTON");
        ReflectionTestUtils.setField(mruleEngineService, "supcPackagingTypeMongoService", mSupcPackagingTypeMongoService);
    
        ReflectionTestUtils.setField(packagingTypeCache, "typeToPackagingTypeDTOsMap", mtypeToPackagingTypeDTOsMap);
    
        GetPackagingRecommendationsRequest request = mock(GetPackagingRecommendationsRequest.class);
        ReflectionTestUtils.setField(packmanLandingService, "packagingRecomendationService", mPackagingRecommendationService);
        Protocol proto = Protocol.PROTOCOL_JSON;
        when(request.getRequestProtocol()).thenReturn(proto);
        when(request.getApiKey()).thenReturn("p@(km@n");
        when(request.getStoreFrontId()).thenReturn("SD");
        when(request.getIsShipTogether()).thenReturn(false);
        when(request.getPackageDetail()).thenReturn(packageDetailSRO);
    
        CacheManager.getInstance().setCache(mslabMappingCache);
    
        ReflectionTestUtils.setField(mslabMappingCache, "slabCache", slabCache);
        mruleService = mock(IRulesService.class);
        List<BlockParams> lblockParams = new ArrayList<BlockParams>();
        Block block = createBlock("StorePackagingRuleBlock");
        BlockParams blockParams = createBlockParams(block,"LABEL","Label");
        ruleParam.put("BRAND", "StorePackagingRuleBlock");
        Mockito.doReturn(ruleParam).when(packmanPackagingRuleEngineServiceImpl).populateRuleParamsFromBlockObject(Matchers.any(), (List<BlockParams>) Matchers.any());
        lblockParams.add(blockParams);
        mexclusiveService = mock(PackmanExclusiveServiceImpl.class);
        when(mruleService.getBlockParamsByBlockName("StorePackagingRuleBlock")).thenReturn(lblockParams);
        ReflectionTestUtils.setField(mruleEngineService, "ruleService", mruleService);
        ReflectionTestUtils.setField(mruleEngineService, "exclusiveService", mexclusiveService);
        mDepedencyForDTO = mock(com.snapdeal.packman.dto.SlabCacheDTO.IDependency.class);
    
        when(mDepedencyForDTO.getBooleanScalar(Property.USE_PRIMARY_LBH)).thenReturn(false);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_LBH)).thenReturn(true);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_DELTA_VOL_WT_MULT_FACTOR)).thenReturn(2.0);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_LBH_MARGIN)).thenReturn(0.1);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_DELTA_VOL_WT)).thenReturn(false);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_DELTA_VOL_WT_OFFSET)).thenReturn(1000.0);
    
        ReflectionTestUtils.setField(slabCacheDTO, "dep", mDepedencyForDTO);
        GetPackagingRecommendationsResponse resp = packmanLandingService.getPackagingRecommendations(request);
        assertTrue(resp.isSuccessful());
        assertEquals("CARTON", resp.getPackagingRecommendationList().get(0).getPackagingType().getType());
        assertEquals("Normal", resp.getPackagingRecommendationList().get(0).getPackagingMode());
    }    
    
  @Test
    public void test_getPackagingRecommendations_Normal_At_SUPC_LABEL_Oldfeature2() throws PackagingRecomendationException, ExternalDataNotFoundException {
    
        List<String> labels = new ArrayList<String>();
        labels.add(Labels.Premium.toString());
        mExternalServiceFactory = mock(ExternalServiceFactory.class);
        ICAMSExternalService miCAMSExternalService = mock(ICAMSExternalService.class);
        GetPOGDetailListResponse response = new GetPOGDetailListResponse();
        POGDetailSRO pogDetailSROs = new POGDetailSRO();
        pogDetailSROs.setEnabled(true);
    
        List<POGDetailSRO> lpogDetailSROs = new ArrayList<POGDetailSRO>();
        lpogDetailSROs.add(pogDetailSROs);
        response.setPogDetailSROs(lpogDetailSROs);
        when(mExternalServiceFactory.getCAMSExternalService()).thenReturn(miCAMSExternalService);
        when(miCAMSExternalService.getPOGDetailsBbySupc(Matchers.<GetPOGDetailListBySUPCListRequest> any())).thenReturn(response);
        ReflectionTestUtils.setField(mruleEngineService, "factory", mExternalServiceFactory);
        PackagableItemSRO packagableItemSRO = createPackagableItemSRO(labels);
        lPackagableItemSRO.add(packagableItemSRO);
        packageDetailSRO.setPackagableItemList(lPackagableItemSRO);
        genericMao = mock(IGenericMao.class);
        ruleList = new ArrayList<>();
        List<CriteriaUnit> lCriteriaUnit = new ArrayList<CriteriaUnit>();
        CriteriaUnit criteriaUnit = createCriteriaUnit(1,"","");
        lCriteriaUnit.add(criteriaUnit);
        PackmanRuleUnit packmanRuleUnit = createPackmanRuleUnit(2,"ru03",7,"CARTON", lCriteriaUnit);

        packmanRuleUnit.setCriteria(lCriteriaUnit);
        ruleList.add(packmanRuleUnit);
        doAnswer(new Answer<List<PackmanRuleUnit>>() {
            public List<PackmanRuleUnit> answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                CocofsMongoQuery cocofsMongoQuery = (CocofsMongoQuery) args[0];
                List<Criteria> lCriteria = cocofsMongoQuery.getCriteria();
                Criteria criteria1 = lCriteria.get(0);
                String rule_type = (String) criteria1.getObj();
                if (rule_type.equals("store-packaging"))
                    return ruleList;
                return null;
    
            }
    
        }).when(genericMao).getDocumentList(Matchers.<CocofsMongoQuery> any(), Matchers.<Class<PackmanRuleUnit>> any());
        ReflectionTestUtils.setField(mruleEngineService, "genericMao", genericMao);
    
        PackmanPackagingRuleEngineServiceImpl packmanPackagingRuleEngineServiceImpl = Mockito.spy(new PackmanPackagingRuleEngineServiceImpl());
        Map<String, Object> ruleParam = new HashMap<String, Object>();
        SlabMappingCache mslabMappingCache = new SlabMappingCache();
        Map<String, NavigableMap<Double, SlabCacheDTO>> slabCache = new HashMap<String, NavigableMap<Double, SlabCacheDTO>>();
        SlabCacheDTO slabCacheDTO = new SlabCacheDTO();
        List<SlabItemDTO> boxes = new ArrayList<SlabItemDTO>();    
        SlabItemDTO slabItemDTO = createSlabItemDto("CB101",10.0,10.0,10.0,100.00,"CB101");
        boxes.add(slabItemDTO);    
        slabItemDTO = createSlabItemDto("CB103",10.0,51.0,2.0,102.0,"CB103");
        boxes.add(slabItemDTO);    
        slabItemDTO = createSlabItemDto("CB102",10.1,10.1,10.1,103.03,"CB102");
        boxes.add(slabItemDTO);    
        slabItemDTO = createSlabItemDto("CB18",11.43,11.43,11.43,149.33,"CB18");
        boxes.add(slabItemDTO);    
        slabCacheDTO = createSlabCacheDTO(boxes,0,500);
        NavigableMap<Double, SlabCacheDTO> navigableMap = new ConcurrentSkipListMap<Double, SlabCacheDTO>();
        navigableMap.put(99.0, slabCacheDTO);
        slabCache.put("Snapdeal-CARTON", navigableMap);
    
        Map<String, List<PackagingTypeCacheDTO>> mtypeToPackagingTypeDTOsMap = new HashMap<String, List<PackagingTypeCacheDTO>>();
        PackagingTypeCache packagingTypeCache = new PackagingTypeCache();
        List<PackagingTypeCacheDTO> lPackagingTypeCacheDTO = new ArrayList<PackagingTypeCacheDTO>();
    
        PackagingTypeCacheDTO packagingTypeCacheDTO = createPackagingTypeCacheDTO("Snapdeal","CARTON","Normal");
        lPackagingTypeCacheDTO.add(packagingTypeCacheDTO);
        mtypeToPackagingTypeDTOsMap.put("CARTON", lPackagingTypeCacheDTO);
    
        PackagingType packagingType = createPackagingType("Snapdeal","Normal");
        pauSRO = createPauSro("990001",10.0,10.0,10.1,"CARTON");
        List<PackagingType> lpackagingList = new ArrayList<PackagingType>();
        lpackagingList.add(packagingType);
        packagingTypeCache.addPackagingType(lpackagingList);
        CacheManager.getInstance().setCache(packagingTypeCache);
        when(mcocofsExternalService.getProductFulfilmentAttributes(Matchers.<GetProductFulfilmentAttributeRequest> any())).thenReturn(pauSRO);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(10.0, 10.0, 10.1)).thenReturn(101.0);
        ReflectionTestUtils.setField(mPackagingRecommendationService, "cocofsExternalService", mcocofsExternalService);
    
        mSupcPackagingTypeMongoService = mock(ISupcPackagingTypeMongoService.class);
        when(mSupcPackagingTypeMongoService.getEnabledAndActivePackagingTypeBySupcAndStoreCode("Snapdeal", "990001")).thenReturn("CARTON");
        ReflectionTestUtils.setField(mruleEngineService, "supcPackagingTypeMongoService", mSupcPackagingTypeMongoService);    
        ReflectionTestUtils.setField(packagingTypeCache, "typeToPackagingTypeDTOsMap", mtypeToPackagingTypeDTOsMap);
        GetPackagingRecommendationsRequest request = mock(GetPackagingRecommendationsRequest.class);
        ReflectionTestUtils.setField(packmanLandingService, "packagingRecomendationService", mPackagingRecommendationService);
        Protocol proto = Protocol.PROTOCOL_JSON;
        when(request.getRequestProtocol()).thenReturn(proto);
        when(request.getApiKey()).thenReturn("p@(km@n");
        when(request.getStoreFrontId()).thenReturn("SD");
        when(request.getIsShipTogether()).thenReturn(false);
        when(request.getPackageDetail()).thenReturn(packageDetailSRO);
    
        CacheManager.getInstance().setCache(mslabMappingCache);
    
        ReflectionTestUtils.setField(mslabMappingCache, "slabCache", slabCache);
        mruleService = mock(IRulesService.class);
        List<BlockParams> lblockParams = new ArrayList<BlockParams>();
        Block block = createBlock("StorePackagingRuleBlock");
        BlockParams blockParams = createBlockParams(block,"LABEL","Label");
        ruleParam.put("BRAND", "StorePackagingRuleBlock");
        Mockito.doReturn(ruleParam).when(packmanPackagingRuleEngineServiceImpl).populateRuleParamsFromBlockObject(Matchers.any(), (List<BlockParams>) Matchers.any());
        lblockParams.add(blockParams);
        mexclusiveService = mock(PackmanExclusiveServiceImpl.class);
        when(mruleService.getBlockParamsByBlockName("StorePackagingRuleBlock")).thenReturn(lblockParams);
        ReflectionTestUtils.setField(mruleEngineService, "ruleService", mruleService);
        ReflectionTestUtils.setField(mruleEngineService, "exclusiveService", mexclusiveService);
        mDepedencyForDTO = mock(com.snapdeal.packman.dto.SlabCacheDTO.IDependency.class);
    
        when(mDepedencyForDTO.getBooleanScalar(Property.USE_PRIMARY_LBH)).thenReturn(false);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_LBH)).thenReturn(true);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_DELTA_VOL_WT_MULT_FACTOR)).thenReturn(2.0);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_LBH_MARGIN)).thenReturn(0.1);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_DELTA_VOL_WT)).thenReturn(false);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_DELTA_VOL_WT_OFFSET)).thenReturn(1000.0);
        ReflectionTestUtils.setField(slabCacheDTO, "dep", mDepedencyForDTO);
        GetPackagingRecommendationsResponse resp = packmanLandingService.getPackagingRecommendations(request);
        assertTrue(resp.isSuccessful());
        assertEquals("CARTON", resp.getPackagingRecommendationList().get(0).getPackagingType().getType());
        assertEquals("Normal", resp.getPackagingRecommendationList().get(0).getPackagingMode());
    }
    
    //6.b.5
    @Test
    public void testGetPackagingRecommendationsNormalAtSupcLabelOldFeatureNewLBHandLBHFlagOnWithMarginWithDelta()
            throws PackagingRecomendationException, ExternalDataNotFoundException {
    
        List<String> labels = new ArrayList<String>();
        labels.add(Labels.Premium.toString());
        mExternalServiceFactory = mock(ExternalServiceFactory.class);
        ICAMSExternalService miCAMSExternalService = mock(ICAMSExternalService.class);
        GetPOGDetailListResponse response = new GetPOGDetailListResponse();
        POGDetailSRO pogDetailSROs = new POGDetailSRO();
        pogDetailSROs.setEnabled(true);
    
        List<POGDetailSRO> lpogDetailSROs = new ArrayList<POGDetailSRO>();
        lpogDetailSROs.add(pogDetailSROs);
        response.setPogDetailSROs(lpogDetailSROs);
        when(mExternalServiceFactory.getCAMSExternalService()).thenReturn(miCAMSExternalService);
        when(miCAMSExternalService.getPOGDetailsBbySupc(Matchers.<GetPOGDetailListBySUPCListRequest> any())).thenReturn(response);
        ReflectionTestUtils.setField(mruleEngineService, "factory", mExternalServiceFactory);
        PackagableItemSRO packagableItemSRO = createPackagableItemSRO(labels);
        lPackagableItemSRO.add(packagableItemSRO);
        packageDetailSRO.setPackagableItemList(lPackagableItemSRO);
        genericMao = mock(IGenericMao.class);
        ruleList = new ArrayList<>();
        List<CriteriaUnit> lCriteriaUnit = new ArrayList<CriteriaUnit>();
        CriteriaUnit criteriaUnit = createCriteriaUnit(1,"","");
        lCriteriaUnit.add(criteriaUnit);
        PackmanRuleUnit packmanRuleUnit = createPackmanRuleUnit(2,"ru03",7,"CARTON", lCriteriaUnit);

        packmanRuleUnit.setCriteria(lCriteriaUnit);
        ruleList.add(packmanRuleUnit);
        doAnswer(new Answer<List<PackmanRuleUnit>>() {
            public List<PackmanRuleUnit> answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                CocofsMongoQuery cocofsMongoQuery = (CocofsMongoQuery) args[0];
                List<Criteria> lCriteria = cocofsMongoQuery.getCriteria();
                Criteria criteria1 = lCriteria.get(0);
                String rule_type = (String) criteria1.getObj();
                if (rule_type.equals("store-packaging"))
                    return ruleList;
                return null;
    
            }
    
        }).when(genericMao).getDocumentList(Matchers.<CocofsMongoQuery> any(), Matchers.<Class<PackmanRuleUnit>> any());
        ReflectionTestUtils.setField(mruleEngineService, "genericMao", genericMao);
    
        PackmanPackagingRuleEngineServiceImpl packmanPackagingRuleEngineServiceImpl = Mockito.spy(new PackmanPackagingRuleEngineServiceImpl());
        Map<String, Object> ruleParam = new HashMap<String, Object>();
        SlabMappingCache mslabMappingCache = new SlabMappingCache();
        Map<String, NavigableMap<Double, SlabCacheDTO>> slabCache = new HashMap<String, NavigableMap<Double, SlabCacheDTO>>();
        SlabCacheDTO slabCacheDTO = new SlabCacheDTO();
        List<SlabItemDTO> boxes = new ArrayList<SlabItemDTO>();
    
        SlabItemDTO slabItemDTO = createSlabItemDto("CB101",10.0,10.0,10.0,100.00,"CB101");
        boxes.add(slabItemDTO);
        slabItemDTO = createSlabItemDto("CB103",10.0,51.0,2.0,102.0,"CB103");
        boxes.add(slabItemDTO);
        slabItemDTO = createSlabItemDto("CB102",10.1,10.1,10.1,103.03,"CB102");
        boxes.add(slabItemDTO);
        slabItemDTO = createSlabItemDto("CB18",11.43,11.43,11.43,149.33,"CB18");
        boxes.add(slabItemDTO);
    
        slabCacheDTO = createSlabCacheDTO(boxes,0,500);
        NavigableMap<Double, SlabCacheDTO> navigableMap = new ConcurrentSkipListMap<Double, SlabCacheDTO>();
        navigableMap.put(99.0, slabCacheDTO);
        slabCache.put("Snapdeal-CARTON", navigableMap);
    
        Map<String, List<PackagingTypeCacheDTO>> mtypeToPackagingTypeDTOsMap = new HashMap<String, List<PackagingTypeCacheDTO>>();
        PackagingTypeCache packagingTypeCache = new PackagingTypeCache();
        List<PackagingTypeCacheDTO> lPackagingTypeCacheDTO = new ArrayList<PackagingTypeCacheDTO>();
    
         PackagingTypeCacheDTO packagingTypeCacheDTO = createPackagingTypeCacheDTO("Snapdeal","CARTON","Normal");
        lPackagingTypeCacheDTO.add(packagingTypeCacheDTO);
        mtypeToPackagingTypeDTOsMap.put("CARTON", lPackagingTypeCacheDTO);
    
        PackagingType packagingType = createPackagingType("Snapdeal","Normal");
        pauSRO = createPauSro("990001",10.0,10.0,10.1,"CARTON");
        List<PackagingType> lpackagingList = new ArrayList<PackagingType>();
        lpackagingList.add(packagingType);
        packagingTypeCache.addPackagingType(lpackagingList);
        CacheManager.getInstance().setCache(packagingTypeCache);
        when(mcocofsExternalService.getProductFulfilmentAttributes(Matchers.<GetProductFulfilmentAttributeRequest> any())).thenReturn(pauSRO);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(10.0, 10.0, 10.1)).thenReturn(101.0);
        ReflectionTestUtils.setField(mPackagingRecommendationService, "cocofsExternalService", mcocofsExternalService);
    
        mSupcPackagingTypeMongoService = mock(ISupcPackagingTypeMongoService.class);
        when(mSupcPackagingTypeMongoService.getEnabledAndActivePackagingTypeBySupcAndStoreCode("Snapdeal", "990001")).thenReturn("CARTON");
        ReflectionTestUtils.setField(mruleEngineService, "supcPackagingTypeMongoService", mSupcPackagingTypeMongoService);
    
        ReflectionTestUtils.setField(packagingTypeCache, "typeToPackagingTypeDTOsMap", mtypeToPackagingTypeDTOsMap);
    
        GetPackagingRecommendationsRequest request = mock(GetPackagingRecommendationsRequest.class);
        ReflectionTestUtils.setField(packmanLandingService, "packagingRecomendationService", mPackagingRecommendationService);
        Protocol proto = Protocol.PROTOCOL_JSON;
        when(request.getRequestProtocol()).thenReturn(proto);
        when(request.getApiKey()).thenReturn("p@(km@n");
        when(request.getStoreFrontId()).thenReturn("SD");
        when(request.getIsShipTogether()).thenReturn(false);
        when(request.getPackageDetail()).thenReturn(packageDetailSRO);
    
        CacheManager.getInstance().setCache(mslabMappingCache);
    
        ReflectionTestUtils.setField(mslabMappingCache, "slabCache", slabCache);
        mruleService = mock(IRulesService.class);
        List<BlockParams> lblockParams = new ArrayList<BlockParams>();
        Block block = createBlock("StorePackagingRuleBlock");
        BlockParams blockParams = createBlockParams(block,"LABEL","Label");
        ruleParam.put("BRAND", "StorePackagingRuleBlock");
        Mockito.doReturn(ruleParam).when(packmanPackagingRuleEngineServiceImpl).populateRuleParamsFromBlockObject(Matchers.any(), (List<BlockParams>) Matchers.any());
        lblockParams.add(blockParams);
        mexclusiveService = mock(PackmanExclusiveServiceImpl.class);
        when(mruleService.getBlockParamsByBlockName("StorePackagingRuleBlock")).thenReturn(lblockParams);
        ReflectionTestUtils.setField(mruleEngineService, "ruleService", mruleService);
        ReflectionTestUtils.setField(mruleEngineService, "exclusiveService", mexclusiveService);
        mDepedencyForDTO = mock(com.snapdeal.packman.dto.SlabCacheDTO.IDependency.class);
    
        when(mDepedencyForDTO.getBooleanScalar(Property.USE_PRIMARY_LBH)).thenReturn(false);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_LBH)).thenReturn(true);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_DELTA_VOL_WT_MULT_FACTOR)).thenReturn(2.0);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_LBH_MARGIN)).thenReturn(0.1);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_DELTA_VOL_WT)).thenReturn(true);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_DELTA_VOL_WT_OFFSET)).thenReturn(1000.0);
    
        ReflectionTestUtils.setField(slabCacheDTO, "dep", mDepedencyForDTO);
        GetPackagingRecommendationsResponse resp = packmanLandingService.getPackagingRecommendations(request);
        assertTrue(resp.isSuccessful());
        assertEquals("CARTON", resp.getPackagingRecommendationList().get(0).getPackagingType().getType());
        assertEquals("Normal", resp.getPackagingRecommendationList().get(0).getPackagingMode());
    }
    
    //test 6.4.1
    @Test
    public void testGetPackagingRecommendationsNormalAtSupcLabelOldFeatureNewLBHSet2() throws PackagingRecomendationException, ExternalDataNotFoundException {
    
        List<String> labels = new ArrayList<String>();
        labels.add(Labels.Premium.toString());
        mExternalServiceFactory = mock(ExternalServiceFactory.class);
        ICAMSExternalService miCAMSExternalService = mock(ICAMSExternalService.class);
        GetPOGDetailListResponse response = new GetPOGDetailListResponse();
        POGDetailSRO pogDetailSROs = new POGDetailSRO();
        pogDetailSROs.setEnabled(true);
    
        List<POGDetailSRO> lpogDetailSROs = new ArrayList<POGDetailSRO>();
        lpogDetailSROs.add(pogDetailSROs);
        response.setPogDetailSROs(lpogDetailSROs);
        when(mExternalServiceFactory.getCAMSExternalService()).thenReturn(miCAMSExternalService);
        when(miCAMSExternalService.getPOGDetailsBbySupc(Matchers.<GetPOGDetailListBySUPCListRequest> any())).thenReturn(response);
        ReflectionTestUtils.setField(mruleEngineService, "factory", mExternalServiceFactory);
        PackagableItemSRO packagableItemSRO = createPackagableItemSRO(labels);
        lPackagableItemSRO.add(packagableItemSRO);
        packageDetailSRO.setPackagableItemList(lPackagableItemSRO);
        genericMao = mock(IGenericMao.class);
        ruleList = new ArrayList<>();
        CriteriaUnit criteriaUnit = createCriteriaUnit(1,"","");
        List<CriteriaUnit> lCriteriaUnit = new ArrayList<CriteriaUnit>();
        lCriteriaUnit.add(criteriaUnit);
        PackmanRuleUnit packmanRuleUnit = createPackmanRuleUnit(2,"ru03",7,"CARTON", lCriteriaUnit);

        ruleList.add(packmanRuleUnit);
        doAnswer(new Answer<List<PackmanRuleUnit>>() {
            public List<PackmanRuleUnit> answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                CocofsMongoQuery cocofsMongoQuery = (CocofsMongoQuery) args[0];
                List<Criteria> lCriteria = cocofsMongoQuery.getCriteria();
                Criteria criteria1 = lCriteria.get(0);
                String rule_type = (String) criteria1.getObj();
                if (rule_type.equals("store-packaging"))
                    return ruleList;
                return null;
    
            }
    
        }).when(genericMao).getDocumentList(Matchers.<CocofsMongoQuery> any(), Matchers.<Class<PackmanRuleUnit>> any());
        ReflectionTestUtils.setField(mruleEngineService, "genericMao", genericMao);
    
        PackmanPackagingRuleEngineServiceImpl packmanPackagingRuleEngineServiceImpl = Mockito.spy(new PackmanPackagingRuleEngineServiceImpl());
        Map<String, Object> ruleParam = new HashMap<String, Object>();
        SlabMappingCache mslabMappingCache = new SlabMappingCache();
        Map<String, NavigableMap<Double, SlabCacheDTO>> slabCache = new HashMap<String, NavigableMap<Double, SlabCacheDTO>>();
        SlabCacheDTO slabCacheDTO = new SlabCacheDTO();
        List<SlabItemDTO> boxes = new ArrayList<SlabItemDTO>();
        SlabItemDTO slabItemDTO = createSlabItemDto("CB101",10.0,10.0,10.0,100.00,"CB101");
        boxes.add(slabItemDTO);
        slabItemDTO = createSlabItemDto("CB103",10.0,51.0,2.0,102.0,"CB103");
        boxes.add(slabItemDTO);    
        slabItemDTO = createSlabItemDto("CB102",10.1,10.1,10.1,103.03,"CB102");
        boxes.add(slabItemDTO);
        slabItemDTO = createSlabItemDto("CB18",11.43,11.43,11.43,149.33,"CB18");
        boxes.add(slabItemDTO);
        List<SlabItemDTO> boxes2 = new ArrayList<SlabItemDTO>();
    
        SlabCacheDTO slabCacheDTO2 = new SlabCacheDTO();
        SlabItemDTO slabItemDTO2 = createSlabItemDto("CB25",32.0,22.0,13.0,915.2,"FOOT WEAR");
        boxes2.add(slabItemDTO2);
        List<SlabItemDTO> boxes1 = new ArrayList<SlabItemDTO>();    
        SlabCacheDTO slabCacheDTO1 = new SlabCacheDTO();
        SlabItemDTO slabItemDTO1 = createSlabItemDto("CB07",38.1,25.4,15.24,1474.84,"cb07");
        boxes1.add(slabItemDTO1);
        List<SlabItemDTO> boxes3 = new ArrayList<SlabItemDTO>();    
        SlabCacheDTO slabCacheDTO3 = new SlabCacheDTO();
        SlabItemDTO slabItemDTO3 = createSlabItemDto("CB09",48.26,35.4,15.24,2615.38,"cb09");
        boxes3.add(slabItemDTO3);
        List<SlabItemDTO> boxes4 = new ArrayList<SlabItemDTO>();    
        SlabCacheDTO slabCacheDTO4 = new SlabCacheDTO();
        SlabItemDTO slabItemDTO4 = createSlabItemDto("CB16",63.5,30.48,48.26,7846.13,"cb16");
        boxes4.add(slabItemDTO4);
        
        slabCacheDTO = createSlabCacheDTO(boxes,0,500);
        slabCacheDTO2 = createSlabCacheDTO(boxes2,500,500);
        slabCacheDTO1 = createSlabCacheDTO(boxes1,1000,500);
        slabCacheDTO3 = createSlabCacheDTO(boxes3,2500,500);
        slabCacheDTO4 = createSlabCacheDTO(boxes4,7000,500);
    
        NavigableMap<Double, SlabCacheDTO> navigableMap = new ConcurrentSkipListMap<Double, SlabCacheDTO>();
        navigableMap.put(0.0, slabCacheDTO);
        navigableMap.put(500.0, slabCacheDTO2);
        navigableMap.put(1000.0, slabCacheDTO1);
        navigableMap.put(2500.0, slabCacheDTO3);
        navigableMap.put(7000.0, slabCacheDTO4);
        slabCache.put("Snapdeal-CARTON", navigableMap);
    
        Map<String, List<PackagingTypeCacheDTO>> mtypeToPackagingTypeDTOsMap = new HashMap<String, List<PackagingTypeCacheDTO>>();
        PackagingTypeCache packagingTypeCache = new PackagingTypeCache();
        List<PackagingTypeCacheDTO> lPackagingTypeCacheDTO = new ArrayList<PackagingTypeCacheDTO>();        
        PackagingTypeCacheDTO packagingTypeCacheDTO = createPackagingTypeCacheDTO("Snapdeal","CARTON","Normal");
        lPackagingTypeCacheDTO.add(packagingTypeCacheDTO);
        mtypeToPackagingTypeDTOsMap.put("CARTON", lPackagingTypeCacheDTO);    
        PackagingType packagingType = createPackagingType("Snapdeal","Normal");
        pauSRO = createPauSro("990001",48.26,15.24,35.56,"CARTON");
        List<PackagingType> lpackagingList = new ArrayList<PackagingType>();
        lpackagingList.add(packagingType);
        packagingTypeCache.addPackagingType(lpackagingList);
        CacheManager.getInstance().setCache(packagingTypeCache);
        when(mcocofsExternalService.getProductFulfilmentAttributes(Matchers.<GetProductFulfilmentAttributeRequest> any())).thenReturn(pauSRO);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(48.26,15.24,35.56)).thenReturn(2615.38);
        ReflectionTestUtils.setField(mPackagingRecommendationService, "cocofsExternalService", mcocofsExternalService);
    
        mSupcPackagingTypeMongoService = mock(ISupcPackagingTypeMongoService.class);
        when(mSupcPackagingTypeMongoService.getEnabledAndActivePackagingTypeBySupcAndStoreCode("Snapdeal", "990001")).thenReturn("CARTON");
        ReflectionTestUtils.setField(mruleEngineService, "supcPackagingTypeMongoService", mSupcPackagingTypeMongoService);
    
        ReflectionTestUtils.setField(packagingTypeCache, "typeToPackagingTypeDTOsMap", mtypeToPackagingTypeDTOsMap);   
        GetPackagingRecommendationsRequest request = mock(GetPackagingRecommendationsRequest.class);
        ReflectionTestUtils.setField(packmanLandingService, "packagingRecomendationService", mPackagingRecommendationService);
        Protocol proto = Protocol.PROTOCOL_JSON;
        when(request.getRequestProtocol()).thenReturn(proto);
        when(request.getApiKey()).thenReturn("p@(km@n");
        when(request.getStoreFrontId()).thenReturn("SD");
        when(request.getIsShipTogether()).thenReturn(false);
        when(request.getPackageDetail()).thenReturn(packageDetailSRO);
    
        CacheManager.getInstance().setCache(mslabMappingCache);    
        ReflectionTestUtils.setField(mslabMappingCache, "slabCache", slabCache);
        mruleService = mock(IRulesService.class);
        List<BlockParams> lblockParams = new ArrayList<BlockParams>();
        Block block = createBlock("StorePackagingRuleBlock");
        BlockParams blockParams = createBlockParams(block,"LABEL","Label");
        ruleParam.put("BRAND", "StorePackagingRuleBlock");
        Mockito.doReturn(ruleParam).when(packmanPackagingRuleEngineServiceImpl).populateRuleParamsFromBlockObject(Matchers.any(), (List<BlockParams>) Matchers.any());
        lblockParams.add(blockParams);
        mexclusiveService = mock(PackmanExclusiveServiceImpl.class);
        when(mruleService.getBlockParamsByBlockName("StorePackagingRuleBlock")).thenReturn(lblockParams);
        ReflectionTestUtils.setField(mruleEngineService, "ruleService", mruleService);
        ReflectionTestUtils.setField(mruleEngineService, "exclusiveService", mexclusiveService);
        mDepedencyForDTO = mock(com.snapdeal.packman.dto.SlabCacheDTO.IDependency.class);
    
        when(mDepedencyForDTO.getBooleanScalar(Property.USE_PRIMARY_LBH)).thenReturn(false);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_LBH)).thenReturn(false);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_DELTA_VOL_WT_MULT_FACTOR)).thenReturn(2.0);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_LBH_MARGIN)).thenReturn(0.1);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_DELTA_VOL_WT)).thenReturn(false);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_DELTA_VOL_WT_OFFSET)).thenReturn(1000.0);
    
        ReflectionTestUtils.setField(slabCacheDTO, "dep", mDepedencyForDTO);
        ReflectionTestUtils.setField(slabCacheDTO1, "dep", mDepedencyForDTO);
        ReflectionTestUtils.setField(slabCacheDTO2, "dep", mDepedencyForDTO);
        ReflectionTestUtils.setField(slabCacheDTO3, "dep", mDepedencyForDTO);
        ReflectionTestUtils.setField(slabCacheDTO4, "dep", mDepedencyForDTO);
        GetPackagingRecommendationsResponse resp = packmanLandingService.getPackagingRecommendations(request);
        assertTrue(resp.isSuccessful());
        assertEquals("CARTON", resp.getPackagingRecommendationList().get(0).getPackagingType().getType());
        assertEquals("Normal", resp.getPackagingRecommendationList().get(0).getPackagingMode());
        assertEquals("CB09", resp.getPackagingRecommendationList().get(0).getPreferredPackagingTypeItem().getCode());
    }
    
    //test 6.4.2
    @Test
    public void testGetPackagingRecommendationsNormalAtSupcLabelOldFeatureNewLBHSet3WithDeltaFeature() throws PackagingRecomendationException, ExternalDataNotFoundException {
    
        List<String> labels = new ArrayList<String>();
        labels.add(Labels.Premium.toString());
        mExternalServiceFactory = mock(ExternalServiceFactory.class);
        ICAMSExternalService miCAMSExternalService = mock(ICAMSExternalService.class);
        GetPOGDetailListResponse response = new GetPOGDetailListResponse();
        POGDetailSRO pogDetailSROs = new POGDetailSRO();
        pogDetailSROs.setEnabled(true);
    
        List<POGDetailSRO> lpogDetailSROs = new ArrayList<POGDetailSRO>();
        lpogDetailSROs.add(pogDetailSROs);
        response.setPogDetailSROs(lpogDetailSROs);
        when(mExternalServiceFactory.getCAMSExternalService()).thenReturn(miCAMSExternalService);
        when(miCAMSExternalService.getPOGDetailsBbySupc(Matchers.<GetPOGDetailListBySUPCListRequest> any())).thenReturn(response);
        ReflectionTestUtils.setField(mruleEngineService, "factory", mExternalServiceFactory);
        PackagableItemSRO packagableItemSRO = createPackagableItemSRO(labels);
        lPackagableItemSRO.add(packagableItemSRO);
        packageDetailSRO.setPackagableItemList(lPackagableItemSRO);
        genericMao = mock(IGenericMao.class);
        ruleList = new ArrayList<>();
        List<CriteriaUnit> lCriteriaUnit = new ArrayList<CriteriaUnit>();
        CriteriaUnit criteriaUnit = createCriteriaUnit(1,"","");
        lCriteriaUnit.add(criteriaUnit);
        PackmanRuleUnit packmanRuleUnit = createPackmanRuleUnit(2,"ru03",7,"CARTON", lCriteriaUnit);
        packmanRuleUnit.setCriteria(lCriteriaUnit);
        ruleList.add(packmanRuleUnit);
        doAnswer(new Answer<List<PackmanRuleUnit>>() {
            public List<PackmanRuleUnit> answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                CocofsMongoQuery cocofsMongoQuery = (CocofsMongoQuery) args[0];
                List<Criteria> lCriteria = cocofsMongoQuery.getCriteria();
                Criteria criteria1 = lCriteria.get(0);
                String rule_type = (String) criteria1.getObj();
                if (rule_type.equals("store-packaging"))
                    return ruleList;
                return null;
    
            }
    
        }).when(genericMao).getDocumentList(Matchers.<CocofsMongoQuery> any(), Matchers.<Class<PackmanRuleUnit>> any());
        ReflectionTestUtils.setField(mruleEngineService, "genericMao", genericMao);
    
        PackmanPackagingRuleEngineServiceImpl packmanPackagingRuleEngineServiceImpl = Mockito.spy(new PackmanPackagingRuleEngineServiceImpl());
        Map<String, Object> ruleParam = new HashMap<String, Object>();
        SlabMappingCache mslabMappingCache = new SlabMappingCache();
        Map<String, NavigableMap<Double, SlabCacheDTO>> slabCache = new HashMap<String, NavigableMap<Double, SlabCacheDTO>>();
        List<SlabItemDTO> boxes = new ArrayList<SlabItemDTO>();
    
        SlabItemDTO slabItemDTO = createSlabItemDto("CB101",10.0,10.0,10.0,100.00,"CB101");
        boxes.add(slabItemDTO);
        slabItemDTO = createSlabItemDto("CB103",10.0,51.0,2.0,102.0,"CB103");
        boxes.add(slabItemDTO);
        slabItemDTO = createSlabItemDto("CB102",10.1,10.1,10.1,103.03,"CB102");
        boxes.add(slabItemDTO);
        slabItemDTO = createSlabItemDto("CB18",11.43,11.43,11.43,149.33,"CB18");
        boxes.add(slabItemDTO);
        List<SlabItemDTO> boxes2 = new ArrayList<SlabItemDTO>();
        SlabItemDTO slabItemDTO2 = createSlabItemDto("CB25",32.0,22.0,13.0,915.2,"FOOT WEAR");
        boxes2.add(slabItemDTO2);       
        List<SlabItemDTO> boxes1 = new ArrayList<SlabItemDTO>();
        SlabItemDTO slabItemDTO1 = createSlabItemDto("CB07",38.1,25.4,15.24,1474.84,"cb07");
        boxes1.add(slabItemDTO1);
        List<SlabItemDTO> boxes3 = new ArrayList<SlabItemDTO>();
    
        SlabItemDTO slabItemDTO3 = createSlabItemDto("CB09",48.26,35.4,15.24,2615.38,"cb09");
        boxes3.add(slabItemDTO3);
        List<SlabItemDTO> boxes4 = new ArrayList<SlabItemDTO>();    
        SlabItemDTO slabItemDTO4 = createSlabItemDto("CB16",63.5,30.48,48.26,7846.13,"cb16");
        boxes4.add(slabItemDTO4);
    
        SlabCacheDTO slabCacheDTO = createSlabCacheDTO(boxes,0,500);
        SlabCacheDTO slabCacheDTO2 = createSlabCacheDTO(boxes2,500,500);
        SlabCacheDTO slabCacheDTO1 = createSlabCacheDTO(boxes1,1000,500);
        SlabCacheDTO slabCacheDTO3 = createSlabCacheDTO(boxes3,2500,500);
        SlabCacheDTO slabCacheDTO4 = createSlabCacheDTO(boxes4,7000,500);
    
        NavigableMap<Double, SlabCacheDTO> navigableMap = new ConcurrentSkipListMap<Double, SlabCacheDTO>();
        navigableMap.put(0.0, slabCacheDTO);
        navigableMap.put(500.0, slabCacheDTO2);
        navigableMap.put(1000.0, slabCacheDTO1);
        navigableMap.put(2500.0, slabCacheDTO3);
        navigableMap.put(7000.0, slabCacheDTO4);
        slabCache.put("Snapdeal-CARTON", navigableMap);
    
        Map<String, List<PackagingTypeCacheDTO>> mtypeToPackagingTypeDTOsMap = new HashMap<String, List<PackagingTypeCacheDTO>>();
        PackagingTypeCache packagingTypeCache = new PackagingTypeCache();
        List<PackagingTypeCacheDTO> lPackagingTypeCacheDTO = new ArrayList<PackagingTypeCacheDTO>();
    
        PackagingTypeCacheDTO packagingTypeCacheDTO = createPackagingTypeCacheDTO("Snapdeal","CARTON","Normal");
        lPackagingTypeCacheDTO.add(packagingTypeCacheDTO);
        mtypeToPackagingTypeDTOsMap.put("CARTON", lPackagingTypeCacheDTO);
    
        PackagingType packagingType = createPackagingType("Snapdeal","Normal");
        pauSRO = createPauSro("990001",48.26,15.24,35.56,"CARTON");
        List<PackagingType> lpackagingList = new ArrayList<PackagingType>();
        lpackagingList.add(packagingType);
        packagingTypeCache.addPackagingType(lpackagingList);
        CacheManager.getInstance().setCache(packagingTypeCache);
        when(mcocofsExternalService.getProductFulfilmentAttributes(Matchers.<GetProductFulfilmentAttributeRequest> any())).thenReturn(pauSRO);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(48.26, 15.24, 35.56)).thenReturn(2615.38);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(32.0, 20.0, 14.0)).thenReturn(896.0);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(32.0, 22.0, 13.0)).thenReturn(915.2);
        ReflectionTestUtils.setField(mPackagingRecommendationService, "cocofsExternalService", mcocofsExternalService);
    
        mSupcPackagingTypeMongoService = mock(ISupcPackagingTypeMongoService.class);
        when(mSupcPackagingTypeMongoService.getEnabledAndActivePackagingTypeBySupcAndStoreCode("Snapdeal", "990001")).thenReturn("CARTON");
        ReflectionTestUtils.setField(mruleEngineService, "supcPackagingTypeMongoService", mSupcPackagingTypeMongoService);
    
        ReflectionTestUtils.setField(packagingTypeCache, "typeToPackagingTypeDTOsMap", mtypeToPackagingTypeDTOsMap);
    
        GetPackagingRecommendationsRequest request = mock(GetPackagingRecommendationsRequest.class);
        ReflectionTestUtils.setField(packmanLandingService, "packagingRecomendationService", mPackagingRecommendationService);
        Protocol proto = Protocol.PROTOCOL_JSON;
        when(request.getRequestProtocol()).thenReturn(proto);
        when(request.getApiKey()).thenReturn("p@(km@n");
        when(request.getStoreFrontId()).thenReturn("SD");
        when(request.getIsShipTogether()).thenReturn(false);
        when(request.getPackageDetail()).thenReturn(packageDetailSRO);
    
        CacheManager.getInstance().setCache(mslabMappingCache);
    
        ReflectionTestUtils.setField(mslabMappingCache, "slabCache", slabCache);
        mruleService = mock(IRulesService.class);
        List<BlockParams> lblockParams = new ArrayList<BlockParams>();
        Block block = createBlock("StorePackagingRuleBlock");
        BlockParams blockParams = createBlockParams(block,"LABEL","Label");
        ruleParam.put("BRAND", "StorePackagingRuleBlock");
        Mockito.doReturn(ruleParam).when(packmanPackagingRuleEngineServiceImpl).populateRuleParamsFromBlockObject(Matchers.any(), (List<BlockParams>) Matchers.any());
        lblockParams.add(blockParams);
        mexclusiveService = mock(PackmanExclusiveServiceImpl.class);
        when(mruleService.getBlockParamsByBlockName("StorePackagingRuleBlock")).thenReturn(lblockParams);
        ReflectionTestUtils.setField(mruleEngineService, "ruleService", mruleService);
        ReflectionTestUtils.setField(mruleEngineService, "exclusiveService", mexclusiveService);
        mDepedencyForDTO = mock(com.snapdeal.packman.dto.SlabCacheDTO.IDependency.class);
    
        when(mDepedencyForDTO.getBooleanScalar(Property.USE_PRIMARY_LBH)).thenReturn(false);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_LBH)).thenReturn(true);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_DELTA_VOL_WT_MULT_FACTOR)).thenReturn(2.0);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_LBH_MARGIN)).thenReturn(0.1);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_DELTA_VOL_WT)).thenReturn(true);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_DELTA_VOL_WT_OFFSET)).thenReturn(1000.0);
      
        ReflectionTestUtils.setField(slabCacheDTO, "dep", mDepedencyForDTO);
        ReflectionTestUtils.setField(slabCacheDTO1, "dep", mDepedencyForDTO);
        ReflectionTestUtils.setField(slabCacheDTO2, "dep", mDepedencyForDTO);
        ReflectionTestUtils.setField(slabCacheDTO3, "dep", mDepedencyForDTO);
        ReflectionTestUtils.setField(slabCacheDTO4, "dep", mDepedencyForDTO);
        GetPackagingRecommendationsResponse resp = packmanLandingService.getPackagingRecommendations(request);
        assertTrue(resp.isSuccessful());
        assertEquals("CARTON", resp.getPackagingRecommendationList().get(0).getPackagingType().getType());
        assertEquals("Normal", resp.getPackagingRecommendationList().get(0).getPackagingMode());
        assertEquals("CB09", resp.getPackagingRecommendationList().get(0).getPreferredPackagingTypeItem().getCode());
    } 

   // @Test
   public void testGetPackagingRecommendationsNormalAtSupcLabelOldFeatureNewLBHSet3() throws PackagingRecomendationException, ExternalDataNotFoundException {
    
        List<String> labels = new ArrayList<String>();
        labels.add(Labels.Premium.toString());
        mExternalServiceFactory = mock(ExternalServiceFactory.class);
        ICAMSExternalService miCAMSExternalService = mock(ICAMSExternalService.class);
        GetPOGDetailListResponse response = new GetPOGDetailListResponse();
        POGDetailSRO pogDetailSROs = new POGDetailSRO();
        pogDetailSROs.setEnabled(true);
    
        List<POGDetailSRO> lpogDetailSROs = new ArrayList<POGDetailSRO>();
        lpogDetailSROs.add(pogDetailSROs);
        response.setPogDetailSROs(lpogDetailSROs);
        when(mExternalServiceFactory.getCAMSExternalService()).thenReturn(miCAMSExternalService);
        when(miCAMSExternalService.getPOGDetailsBbySupc(Matchers.<GetPOGDetailListBySUPCListRequest> any())).thenReturn(response);
        ReflectionTestUtils.setField(mruleEngineService, "factory", mExternalServiceFactory);
        PackagableItemSRO packagableItemSRO = createPackagableItemSRO(labels);
        lPackagableItemSRO.add(packagableItemSRO);
        packageDetailSRO.setPackagableItemList(lPackagableItemSRO);
        genericMao = mock(IGenericMao.class);
        ruleList = new ArrayList<>();
        List<CriteriaUnit> lCriteriaUnit = new ArrayList<CriteriaUnit>();
        CriteriaUnit criteriaUnit = createCriteriaUnit(1,"","");
        lCriteriaUnit.add(criteriaUnit);
        PackmanRuleUnit packmanRuleUnit = createPackmanRuleUnit(2,"ru03",7,"CARTON", lCriteriaUnit);

        packmanRuleUnit.setCriteria(lCriteriaUnit);
        ruleList.add(packmanRuleUnit);
        doAnswer(new Answer<List<PackmanRuleUnit>>() {
            public List<PackmanRuleUnit> answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                CocofsMongoQuery cocofsMongoQuery = (CocofsMongoQuery) args[0];
                List<Criteria> lCriteria = cocofsMongoQuery.getCriteria();
                Criteria criteria1 = lCriteria.get(0);
                String rule_type = (String) criteria1.getObj();
                if (rule_type.equals("store-packaging"))
                    return ruleList;
                return null;
    
            }
    
        }).when(genericMao).getDocumentList(Matchers.<CocofsMongoQuery> any(), Matchers.<Class<PackmanRuleUnit>> any());
        ReflectionTestUtils.setField(mruleEngineService, "genericMao", genericMao);
    
        PackmanPackagingRuleEngineServiceImpl packmanPackagingRuleEngineServiceImpl = Mockito.spy(new PackmanPackagingRuleEngineServiceImpl());
        Map<String, Object> ruleParam = new HashMap<String, Object>();
        SlabMappingCache mslabMappingCache = new SlabMappingCache();
        Map<String, NavigableMap<Double, SlabCacheDTO>> slabCache = new HashMap<String, NavigableMap<Double, SlabCacheDTO>>();
        List<SlabItemDTO> boxes = new ArrayList<SlabItemDTO>();
    
        SlabItemDTO slabItemDTO = createSlabItemDto("CB101",10.0,10.0,10.0,100.00,"CB101");
        boxes.add(slabItemDTO);
        slabItemDTO = createSlabItemDto("CB103",10.0,51.0,2.0,102.0,"CB103");
        boxes.add(slabItemDTO);
        slabItemDTO = createSlabItemDto("CB102",10.1,10.1,10.1,103.03,"CB102");
        boxes.add(slabItemDTO);
        slabItemDTO = createSlabItemDto("CB18",11.43,11.43,11.43,149.33,"CB18");
        boxes.add(slabItemDTO);
        List<SlabItemDTO> boxes2 = new ArrayList<SlabItemDTO>();

        SlabItemDTO slabItemDTO2 = createSlabItemDto("CB25",32.0,22.0,13.0,915.2,"FOOT WEAR");
        boxes2.add(slabItemDTO2);      
        List<SlabItemDTO> boxes1 = new ArrayList<SlabItemDTO>();

        SlabItemDTO slabItemDTO1 = createSlabItemDto("CB07",38.1,25.4,15.24,1474.84,"cb07");
        boxes1.add(slabItemDTO1);
        List<SlabItemDTO> boxes3 = new ArrayList<SlabItemDTO>();    
    
        SlabItemDTO slabItemDTO3 = createSlabItemDto("CB09",48.26,35.4,15.24,2615.38,"cb09");
        boxes3.add(slabItemDTO3);
        List<SlabItemDTO> boxes4 = new ArrayList<SlabItemDTO>();
    
        SlabItemDTO slabItemDTO4 = createSlabItemDto("CB16",63.5,30.48,48.26,7846.13,"cb16");
        boxes4.add(slabItemDTO4);
    
        SlabCacheDTO slabCacheDTO = createSlabCacheDTO(boxes,0,500);
        SlabCacheDTO slabCacheDTO2 = createSlabCacheDTO(boxes2,500,500);    
        SlabCacheDTO slabCacheDTO1 = createSlabCacheDTO(boxes1,1000,500);    
        SlabCacheDTO slabCacheDTO3 = createSlabCacheDTO(boxes3,2500,500);    
        SlabCacheDTO slabCacheDTO4 = createSlabCacheDTO(boxes4,7000,500);
    
        NavigableMap<Double, SlabCacheDTO> navigableMap = new ConcurrentSkipListMap<Double, SlabCacheDTO>();
        navigableMap.put(0.0, slabCacheDTO);
        navigableMap.put(500.0, slabCacheDTO2);
        navigableMap.put(1000.0, slabCacheDTO1);
        navigableMap.put(2500.0, slabCacheDTO3);
        navigableMap.put(7000.0, slabCacheDTO4);
        slabCache.put("Snapdeal-CARTON", navigableMap);
    
        Map<String, List<PackagingTypeCacheDTO>> mtypeToPackagingTypeDTOsMap = new HashMap<String, List<PackagingTypeCacheDTO>>();
        PackagingTypeCache packagingTypeCache = new PackagingTypeCache();
        List<PackagingTypeCacheDTO> lPackagingTypeCacheDTO = new ArrayList<PackagingTypeCacheDTO>();
    
        PackagingTypeCacheDTO packagingTypeCacheDTO = createPackagingTypeCacheDTO("Snapdeal","CARTON","Normal");
        lPackagingTypeCacheDTO.add(packagingTypeCacheDTO);
        mtypeToPackagingTypeDTOsMap.put("CARTON", lPackagingTypeCacheDTO);
    
        PackagingType packagingType = createPackagingType("Snapdeal","Normal");
        pauSRO = createPauSro("990001",10.0,10.0,10.1,"CARTON");
        List<PackagingType> lpackagingList = new ArrayList<PackagingType>();
        lpackagingList.add(packagingType);
        packagingTypeCache.addPackagingType(lpackagingList);
        CacheManager.getInstance().setCache(packagingTypeCache);
        when(mcocofsExternalService.getProductFulfilmentAttributes(Matchers.<GetProductFulfilmentAttributeRequest> any())).thenReturn(pauSRO);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(48.26, 15.24, 35.56)).thenReturn(2615.38);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(32.0, 20.0, 14.0)).thenReturn(896.0);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(32.0, 22.0, 13.0)).thenReturn(915.2);
        ReflectionTestUtils.setField(mPackagingRecommendationService, "cocofsExternalService", mcocofsExternalService);
    
        mSupcPackagingTypeMongoService = mock(ISupcPackagingTypeMongoService.class);
        when(mSupcPackagingTypeMongoService.getEnabledAndActivePackagingTypeBySupcAndStoreCode("Snapdeal", "990001")).thenReturn("CARTON");
        ReflectionTestUtils.setField(mruleEngineService, "supcPackagingTypeMongoService", mSupcPackagingTypeMongoService);
    
        ReflectionTestUtils.setField(packagingTypeCache, "typeToPackagingTypeDTOsMap", mtypeToPackagingTypeDTOsMap);
    
        GetPackagingRecommendationsRequest request = mock(GetPackagingRecommendationsRequest.class);
        ReflectionTestUtils.setField(packmanLandingService, "packagingRecomendationService", mPackagingRecommendationService);
        Protocol proto = Protocol.PROTOCOL_JSON;
        when(request.getRequestProtocol()).thenReturn(proto);
        when(request.getApiKey()).thenReturn("p@(km@n");
        when(request.getStoreFrontId()).thenReturn("SD");
        when(request.getIsShipTogether()).thenReturn(false);
        when(request.getPackageDetail()).thenReturn(packageDetailSRO);
    
        CacheManager.getInstance().setCache(mslabMappingCache);
    
        ReflectionTestUtils.setField(mslabMappingCache, "slabCache", slabCache);
        mruleService = mock(IRulesService.class);
        List<BlockParams> lblockParams = new ArrayList<BlockParams>();
        Block block = createBlock("StorePackagingRuleBlock");
        BlockParams blockParams = createBlockParams(block,"LABEL","Label");
        ruleParam.put("BRAND", "StorePackagingRuleBlock");
        Mockito.doReturn(ruleParam).when(packmanPackagingRuleEngineServiceImpl).populateRuleParamsFromBlockObject(Matchers.any(), (List<BlockParams>) Matchers.any());
        lblockParams.add(blockParams);
        mexclusiveService = mock(PackmanExclusiveServiceImpl.class);
        when(mruleService.getBlockParamsByBlockName("StorePackagingRuleBlock")).thenReturn(lblockParams);
        ReflectionTestUtils.setField(mruleEngineService, "ruleService", mruleService);
        ReflectionTestUtils.setField(mruleEngineService, "exclusiveService", mexclusiveService);
        mDepedencyForDTO = mock(com.snapdeal.packman.dto.SlabCacheDTO.IDependency.class);
    
        when(mDepedencyForDTO.getBooleanScalar(Property.USE_PRIMARY_LBH)).thenReturn(false);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_LBH)).thenReturn(false);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_DELTA_VOL_WT_MULT_FACTOR)).thenReturn(2.0);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_LBH_MARGIN)).thenReturn(0.0);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_DELTA_VOL_WT)).thenReturn(false);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_DELTA_VOL_WT_OFFSET)).thenReturn(1000.0);
    
        ReflectionTestUtils.setField(slabCacheDTO, "dep", mDepedencyForDTO);
        ReflectionTestUtils.setField(slabCacheDTO1, "dep", mDepedencyForDTO);
        ReflectionTestUtils.setField(slabCacheDTO2, "dep", mDepedencyForDTO);
        ReflectionTestUtils.setField(slabCacheDTO3, "dep", mDepedencyForDTO);
        ReflectionTestUtils.setField(slabCacheDTO4, "dep", mDepedencyForDTO);
        GetPackagingRecommendationsResponse resp = packmanLandingService.getPackagingRecommendations(request);
        assertTrue(resp.isSuccessful());
        assertEquals("CARTON", resp.getPackagingRecommendationList().get(0).getPackagingType().getType());
        assertEquals("Normal", resp.getPackagingRecommendationList().get(0).getPackagingMode());
        assertEquals("CB09", resp.getPackagingRecommendationList().get(0).getPreferredPackagingTypeItem().getCode());
    }
    
   //test 6.4.4
    @Test
    public void testGetPackagingRecommendationsNormalAtSupcLabelOldFeatureNewLBHSet3andLBHFlagOn() throws PackagingRecomendationException, ExternalDataNotFoundException {
    
        List<String> labels = new ArrayList<String>();
        labels.add(Labels.Premium.toString());
        mExternalServiceFactory = mock(ExternalServiceFactory.class);
        ICAMSExternalService miCAMSExternalService = mock(ICAMSExternalService.class);
        GetPOGDetailListResponse response = new GetPOGDetailListResponse();
        POGDetailSRO pogDetailSROs = new POGDetailSRO();
        pogDetailSROs.setEnabled(true);
    
        List<POGDetailSRO> lpogDetailSROs = new ArrayList<POGDetailSRO>();
        lpogDetailSROs.add(pogDetailSROs);
        response.setPogDetailSROs(lpogDetailSROs);
        when(mExternalServiceFactory.getCAMSExternalService()).thenReturn(miCAMSExternalService);
        when(miCAMSExternalService.getPOGDetailsBbySupc(Matchers.<GetPOGDetailListBySUPCListRequest> any())).thenReturn(response);
        ReflectionTestUtils.setField(mruleEngineService, "factory", mExternalServiceFactory);
        PackagableItemSRO packagableItemSRO = createPackagableItemSRO(labels);
        lPackagableItemSRO.add(packagableItemSRO);
        packageDetailSRO.setPackagableItemList(lPackagableItemSRO);
        genericMao = mock(IGenericMao.class);
        ruleList = new ArrayList<>();
        List<CriteriaUnit> lCriteriaUnit = new ArrayList<CriteriaUnit>();
        CriteriaUnit criteriaUnit = createCriteriaUnit(1,"","");
        lCriteriaUnit.add(criteriaUnit);
        PackmanRuleUnit packmanRuleUnit = createPackmanRuleUnit(2,"ru03",7,"CARTON", lCriteriaUnit);

        packmanRuleUnit.setCriteria(lCriteriaUnit);
        ruleList.add(packmanRuleUnit);
        doAnswer(new Answer<List<PackmanRuleUnit>>() {
            public List<PackmanRuleUnit> answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                CocofsMongoQuery cocofsMongoQuery = (CocofsMongoQuery) args[0];
                List<Criteria> lCriteria = cocofsMongoQuery.getCriteria();
                Criteria criteria1 = lCriteria.get(0);
                String rule_type = (String) criteria1.getObj();
                if (rule_type.equals("store-packaging"))
                    return ruleList;
                return null;
    
            }
    
        }).when(genericMao).getDocumentList(Matchers.<CocofsMongoQuery> any(), Matchers.<Class<PackmanRuleUnit>> any());
        ReflectionTestUtils.setField(mruleEngineService, "genericMao", genericMao);
    
        PackmanPackagingRuleEngineServiceImpl packmanPackagingRuleEngineServiceImpl = Mockito.spy(new PackmanPackagingRuleEngineServiceImpl());
        Map<String, Object> ruleParam = new HashMap<String, Object>();
        SlabMappingCache mslabMappingCache = new SlabMappingCache();
        Map<String, NavigableMap<Double, SlabCacheDTO>> slabCache = new HashMap<String, NavigableMap<Double, SlabCacheDTO>>();
        SlabCacheDTO slabCacheDTO = new SlabCacheDTO();
        List<SlabItemDTO> boxes = new ArrayList<SlabItemDTO>();
 
        SlabItemDTO slabItemDTO = createSlabItemDto("CB101",10.0,10.0,10.0,100.00,"CB101");
        boxes.add(slabItemDTO);
        slabItemDTO = createSlabItemDto("CB103",10.0,51.0,2.0,102.0,"CB103");
        boxes.add(slabItemDTO);
        slabItemDTO = createSlabItemDto("CB102",10.1,10.1,10.1,103.03,"CB102");
        boxes.add(slabItemDTO);
        slabItemDTO = createSlabItemDto("CB18",11.43,11.43,11.43,149.33,"CB18");
        boxes.add(slabItemDTO);
        List<SlabItemDTO> boxes2 = new ArrayList<SlabItemDTO>();
    
        SlabCacheDTO slabCacheDTO2 = new SlabCacheDTO();
        SlabItemDTO slabItemDTO2 = createSlabItemDto("CB25",32.0,22.0,13.0,915.2,"FOOT WEAR");
        boxes2.add(slabItemDTO2);       
        List<SlabItemDTO> boxes1 = new ArrayList<SlabItemDTO>();
    
        SlabCacheDTO slabCacheDTO1 = new SlabCacheDTO();
        SlabItemDTO slabItemDTO1 = createSlabItemDto("CB07",38.1,25.4,15.24,1474.84,"cb07");
        boxes1.add(slabItemDTO1);
        List<SlabItemDTO> boxes3 = new ArrayList<SlabItemDTO>();
    
        SlabCacheDTO slabCacheDTO3 = new SlabCacheDTO();
        SlabItemDTO slabItemDTO3 = createSlabItemDto("CB09",48.26,35.4,15.24,2615.38,"cb09");
        boxes3.add(slabItemDTO3);
        List<SlabItemDTO> boxes4 = new ArrayList<SlabItemDTO>();
    
        SlabCacheDTO slabCacheDTO4 = new SlabCacheDTO();
        SlabItemDTO slabItemDTO4 = createSlabItemDto("CB16",63.5,30.48,48.26,7846.13,"cb16");
        boxes4.add(slabItemDTO4);
    
        slabCacheDTO = createSlabCacheDTO(boxes,0,500);    
        slabCacheDTO2 = createSlabCacheDTO(boxes2,500,500);    
        slabCacheDTO1 = createSlabCacheDTO(boxes1,1000,500);    
        slabCacheDTO3 = createSlabCacheDTO(boxes3,2500,500);    
        slabCacheDTO4 = createSlabCacheDTO(boxes4,7000,500);
    
        NavigableMap<Double, SlabCacheDTO> navigableMap = new ConcurrentSkipListMap<Double, SlabCacheDTO>();
        navigableMap.put(0.0, slabCacheDTO);
        navigableMap.put(500.0, slabCacheDTO2);
        navigableMap.put(1000.0, slabCacheDTO1);
        navigableMap.put(2500.0, slabCacheDTO3);
        navigableMap.put(7000.0, slabCacheDTO4);
        slabCache.put("Snapdeal-CARTON", navigableMap);
    
        Map<String, List<PackagingTypeCacheDTO>> mtypeToPackagingTypeDTOsMap = new HashMap<String, List<PackagingTypeCacheDTO>>();
        PackagingTypeCache packagingTypeCache = new PackagingTypeCache();
        List<PackagingTypeCacheDTO> lPackagingTypeCacheDTO = new ArrayList<PackagingTypeCacheDTO>();
    
        PackagingTypeCacheDTO packagingTypeCacheDTO = createPackagingTypeCacheDTO("Snapdeal","CARTON","Normal");
        lPackagingTypeCacheDTO.add(packagingTypeCacheDTO);
        mtypeToPackagingTypeDTOsMap.put("CARTON", lPackagingTypeCacheDTO);
    
        PackagingType packagingType = createPackagingType("Snapdeal","Normal");
        pauSRO = createPauSro("990001",48.26,15.24,35.56,"CARTON");
        List<PackagingType> lpackagingList = new ArrayList<PackagingType>();
        lpackagingList.add(packagingType);
        packagingTypeCache.addPackagingType(lpackagingList);
        CacheManager.getInstance().setCache(packagingTypeCache);
        when(mcocofsExternalService.getProductFulfilmentAttributes(Matchers.<GetProductFulfilmentAttributeRequest> any())).thenReturn(pauSRO);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(48.26, 15.24, 35.56)).thenReturn(2615.38);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(32.0, 20.0, 14.0)).thenReturn(896.0);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(32.0, 22.0, 13.0)).thenReturn(915.2);
        ReflectionTestUtils.setField(mPackagingRecommendationService, "cocofsExternalService", mcocofsExternalService);
    
        mSupcPackagingTypeMongoService = mock(ISupcPackagingTypeMongoService.class);
        when(mSupcPackagingTypeMongoService.getEnabledAndActivePackagingTypeBySupcAndStoreCode("Snapdeal", "990001")).thenReturn("CARTON");
        ReflectionTestUtils.setField(mruleEngineService, "supcPackagingTypeMongoService", mSupcPackagingTypeMongoService);
    
        ReflectionTestUtils.setField(packagingTypeCache, "typeToPackagingTypeDTOsMap", mtypeToPackagingTypeDTOsMap);
    
        GetPackagingRecommendationsRequest request = mock(GetPackagingRecommendationsRequest.class);
        ReflectionTestUtils.setField(packmanLandingService, "packagingRecomendationService", mPackagingRecommendationService);
        Protocol proto = Protocol.PROTOCOL_JSON;
        when(request.getRequestProtocol()).thenReturn(proto);
        when(request.getApiKey()).thenReturn("p@(km@n");
        when(request.getStoreFrontId()).thenReturn("SD");
        when(request.getIsShipTogether()).thenReturn(false);
        when(request.getPackageDetail()).thenReturn(packageDetailSRO);
    
        CacheManager.getInstance().setCache(mslabMappingCache);
    
        ReflectionTestUtils.setField(mslabMappingCache, "slabCache", slabCache);
        mruleService = mock(IRulesService.class);
        List<BlockParams> lblockParams = new ArrayList<BlockParams>();
        Block block = createBlock("StorePackagingRuleBlock");
        BlockParams blockParams = createBlockParams(block,"LABEL","Label");
        ruleParam.put("BRAND", "StorePackagingRuleBlock");
        Mockito.doReturn(ruleParam).when(packmanPackagingRuleEngineServiceImpl).populateRuleParamsFromBlockObject(Matchers.any(), (List<BlockParams>) Matchers.any());
        lblockParams.add(blockParams);
        mexclusiveService = mock(PackmanExclusiveServiceImpl.class);
        when(mruleService.getBlockParamsByBlockName("StorePackagingRuleBlock")).thenReturn(lblockParams);
        ReflectionTestUtils.setField(mruleEngineService, "ruleService", mruleService);
        ReflectionTestUtils.setField(mruleEngineService, "exclusiveService", mexclusiveService);
        mDepedencyForDTO = mock(com.snapdeal.packman.dto.SlabCacheDTO.IDependency.class);
    
        when(mDepedencyForDTO.getBooleanScalar(Property.USE_PRIMARY_LBH)).thenReturn(false);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_LBH)).thenReturn(true);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_DELTA_VOL_WT_MULT_FACTOR)).thenReturn(2.0);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_LBH_MARGIN)).thenReturn(0.1);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_DELTA_VOL_WT)).thenReturn(false);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_DELTA_VOL_WT_OFFSET)).thenReturn(1000.0);
    
        ReflectionTestUtils.setField(slabCacheDTO, "dep", mDepedencyForDTO);
        ReflectionTestUtils.setField(slabCacheDTO1, "dep", mDepedencyForDTO);
        ReflectionTestUtils.setField(slabCacheDTO2, "dep", mDepedencyForDTO);
        ReflectionTestUtils.setField(slabCacheDTO3, "dep", mDepedencyForDTO);
        ReflectionTestUtils.setField(slabCacheDTO4, "dep", mDepedencyForDTO);
        GetPackagingRecommendationsResponse resp = packmanLandingService.getPackagingRecommendations(request);
        
        assertTrue(resp.isSuccessful());
        assertEquals("CARTON", resp.getPackagingRecommendationList().get(0).getPackagingType().getType());
        assertEquals("Normal", resp.getPackagingRecommendationList().get(0).getPackagingMode());
        assertEquals("CB09", resp.getPackagingRecommendationList().get(0).getPreferredPackagingTypeItem().getCode());
        
    }
    
    //6.4.5
    @Test
    public void testGetPackagingRecommendationsNormalAtSupcLabelOldFeatureNewLBHSet3andLBHFlagOnWithMargin() throws PackagingRecomendationException, ExternalDataNotFoundException {
    
        List<String> labels = new ArrayList<String>();
        labels.add(Labels.Premium.toString());
        mExternalServiceFactory = mock(ExternalServiceFactory.class);
        ICAMSExternalService miCAMSExternalService = mock(ICAMSExternalService.class);
        GetPOGDetailListResponse response = new GetPOGDetailListResponse();
        POGDetailSRO pogDetailSROs = new POGDetailSRO();
        pogDetailSROs.setEnabled(true);
    
        List<POGDetailSRO> lpogDetailSROs = new ArrayList<POGDetailSRO>();
        lpogDetailSROs.add(pogDetailSROs);
        response.setPogDetailSROs(lpogDetailSROs);
        when(mExternalServiceFactory.getCAMSExternalService()).thenReturn(miCAMSExternalService);
        when(miCAMSExternalService.getPOGDetailsBbySupc(Matchers.<GetPOGDetailListBySUPCListRequest> any())).thenReturn(response);
        ReflectionTestUtils.setField(mruleEngineService, "factory", mExternalServiceFactory);
        PackagableItemSRO packagableItemSRO = createPackagableItemSRO(labels);
        lPackagableItemSRO.add(packagableItemSRO);
        packageDetailSRO.setPackagableItemList(lPackagableItemSRO);
        genericMao = mock(IGenericMao.class);
        ruleList = new ArrayList<>();
        List<CriteriaUnit> lCriteriaUnit = new ArrayList<CriteriaUnit>();
        CriteriaUnit criteriaUnit = createCriteriaUnit(1,"","");
        lCriteriaUnit.add(criteriaUnit);
        PackmanRuleUnit packmanRuleUnit = createPackmanRuleUnit(2,"ru03",7,"CARTON", lCriteriaUnit);

        packmanRuleUnit.setCriteria(lCriteriaUnit);
        ruleList.add(packmanRuleUnit);
        doAnswer(new Answer<List<PackmanRuleUnit>>() {
            public List<PackmanRuleUnit> answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                CocofsMongoQuery cocofsMongoQuery = (CocofsMongoQuery) args[0];
                List<Criteria> lCriteria = cocofsMongoQuery.getCriteria();
                Criteria criteria1 = lCriteria.get(0);
                String rule_type = (String) criteria1.getObj();
                if (rule_type.equals("store-packaging"))
                    return ruleList;
                return null;
    
            }
    
        }).when(genericMao).getDocumentList(Matchers.<CocofsMongoQuery> any(), Matchers.<Class<PackmanRuleUnit>> any());
        ReflectionTestUtils.setField(mruleEngineService, "genericMao", genericMao);
    
        PackmanPackagingRuleEngineServiceImpl packmanPackagingRuleEngineServiceImpl = Mockito.spy(new PackmanPackagingRuleEngineServiceImpl());
        Map<String, Object> ruleParam = new HashMap<String, Object>();
        SlabMappingCache mslabMappingCache = new SlabMappingCache();
        Map<String, NavigableMap<Double, SlabCacheDTO>> slabCache = new HashMap<String, NavigableMap<Double, SlabCacheDTO>>();
        SlabCacheDTO slabCacheDTO = new SlabCacheDTO();
        List<SlabItemDTO> boxes = new ArrayList<SlabItemDTO>();
    
        SlabItemDTO slabItemDTO = createSlabItemDto("CB101",10.0,10.0,10.0,100.00,"CB101");
        boxes.add(slabItemDTO);
        slabItemDTO = createSlabItemDto("CB103",10.0,51.0,2.0,102.0,"CB103");
        boxes.add(slabItemDTO);
        slabItemDTO = createSlabItemDto("CB102",10.1,10.1,10.1,103.03,"CB102");
        boxes.add(slabItemDTO);
        slabItemDTO = createSlabItemDto("CB18",11.43,11.43,11.43,149.33,"CB18");
        boxes.add(slabItemDTO);
        List<SlabItemDTO> boxes2 = new ArrayList<SlabItemDTO>();
        SlabCacheDTO slabCacheDTO2 = new SlabCacheDTO();
        SlabItemDTO slabItemDTO2 = createSlabItemDto("CB25",32.0,22.0,13.0,915.00,"FOOT WEAR");
        boxes2.add(slabItemDTO2);
    
        List<SlabItemDTO> boxes1 = new ArrayList<SlabItemDTO>();
        SlabCacheDTO slabCacheDTO1 = new SlabCacheDTO();
        SlabItemDTO slabItemDTO1 = createSlabItemDto("CB07",38.1,25.4,15.24,1474.84,"cb07");
        boxes1.add(slabItemDTO1);
        List<SlabItemDTO> boxes3 = new ArrayList<SlabItemDTO>();    
        SlabCacheDTO slabCacheDTO3 = new SlabCacheDTO();    
        SlabItemDTO slabItemDTO3 = createSlabItemDto("CB09",48.26,35.4,15.24,2615.38,"cb09");
        boxes3.add(slabItemDTO3);    
        List<SlabItemDTO> boxes4 = new ArrayList<SlabItemDTO>();    
        SlabCacheDTO slabCacheDTO4 = new SlabCacheDTO();
    
        SlabItemDTO slabItemDTO4 = createSlabItemDto("CB16",63.5,30.48,48.26,7846.13,"cb16");
        boxes4.add(slabItemDTO4);
    
        slabCacheDTO = createSlabCacheDTO(boxes,0,500);
        slabCacheDTO2 = createSlabCacheDTO(boxes2,500,500);    
        slabCacheDTO1 = createSlabCacheDTO(boxes1,1000,500);    
        slabCacheDTO3 = createSlabCacheDTO(boxes3,2500,500);    
        slabCacheDTO4 = createSlabCacheDTO(boxes4,7000,500);
    
        NavigableMap<Double, SlabCacheDTO> navigableMap = new ConcurrentSkipListMap<Double, SlabCacheDTO>();
        navigableMap.put(0.0, slabCacheDTO);
        navigableMap.put(500.0, slabCacheDTO2);
        navigableMap.put(1000.0, slabCacheDTO1);
        navigableMap.put(2500.0, slabCacheDTO3);
        navigableMap.put(7000.0, slabCacheDTO4);
        slabCache.put("Snapdeal-CARTON", navigableMap);
    
        Map<String, List<PackagingTypeCacheDTO>> mtypeToPackagingTypeDTOsMap = new HashMap<String, List<PackagingTypeCacheDTO>>();
        PackagingTypeCache packagingTypeCache = new PackagingTypeCache();
        List<PackagingTypeCacheDTO> lPackagingTypeCacheDTO = new ArrayList<PackagingTypeCacheDTO>();
    
        PackagingTypeCacheDTO packagingTypeCacheDTO = createPackagingTypeCacheDTO("Snapdeal","CARTON","Normal");
        lPackagingTypeCacheDTO.add(packagingTypeCacheDTO);
        mtypeToPackagingTypeDTOsMap.put("CARTON", lPackagingTypeCacheDTO);
    
        PackagingType packagingType = createPackagingType("Snapdeal","Normal");
        pauSRO = createPauSro("990001",48.26,15.24,35.56,"CARTON");
        List<PackagingType> lpackagingList = new ArrayList<PackagingType>();
        lpackagingList.add(packagingType);
        packagingTypeCache.addPackagingType(lpackagingList);
        CacheManager.getInstance().setCache(packagingTypeCache);
        when(mcocofsExternalService.getProductFulfilmentAttributes(Matchers.<GetProductFulfilmentAttributeRequest> any())).thenReturn(pauSRO);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(48.26, 15.24, 35.56)).thenReturn(2615.38);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(32.0, 20.0, 14.0)).thenReturn(896.0);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(32.0, 22.0, 13.0)).thenReturn(915.2);
        ReflectionTestUtils.setField(mPackagingRecommendationService, "cocofsExternalService", mcocofsExternalService);
    
        mSupcPackagingTypeMongoService = mock(ISupcPackagingTypeMongoService.class);
        when(mSupcPackagingTypeMongoService.getEnabledAndActivePackagingTypeBySupcAndStoreCode("Snapdeal", "990001")).thenReturn("CARTON");
        ReflectionTestUtils.setField(mruleEngineService, "supcPackagingTypeMongoService", mSupcPackagingTypeMongoService);
    
        ReflectionTestUtils.setField(packagingTypeCache, "typeToPackagingTypeDTOsMap", mtypeToPackagingTypeDTOsMap);
    
        GetPackagingRecommendationsRequest request = mock(GetPackagingRecommendationsRequest.class);
        ReflectionTestUtils.setField(packmanLandingService, "packagingRecomendationService", mPackagingRecommendationService);
        Protocol proto = Protocol.PROTOCOL_JSON;
        when(request.getRequestProtocol()).thenReturn(proto);
        when(request.getApiKey()).thenReturn("p@(km@n");
        when(request.getStoreFrontId()).thenReturn("SD");
        when(request.getIsShipTogether()).thenReturn(false);
        when(request.getPackageDetail()).thenReturn(packageDetailSRO);
    
        CacheManager.getInstance().setCache(mslabMappingCache);
    
        ReflectionTestUtils.setField(mslabMappingCache, "slabCache", slabCache);
        mruleService = mock(IRulesService.class);
        List<BlockParams> lblockParams = new ArrayList<BlockParams>();
        Block block = createBlock("StorePackagingRuleBlock");
        BlockParams blockParams = createBlockParams(block,"LABEL","Label");
        ruleParam.put("BRAND", "StorePackagingRuleBlock");
        Mockito.doReturn(ruleParam).when(packmanPackagingRuleEngineServiceImpl).populateRuleParamsFromBlockObject(Matchers.any(), (List<BlockParams>) Matchers.any());
        lblockParams.add(blockParams);
        mexclusiveService = mock(PackmanExclusiveServiceImpl.class);
        when(mruleService.getBlockParamsByBlockName("StorePackagingRuleBlock")).thenReturn(lblockParams);
        ReflectionTestUtils.setField(mruleEngineService, "ruleService", mruleService);
        ReflectionTestUtils.setField(mruleEngineService, "exclusiveService", mexclusiveService);
        mDepedencyForDTO = mock(com.snapdeal.packman.dto.SlabCacheDTO.IDependency.class);
    
        when(mDepedencyForDTO.getBooleanScalar(Property.USE_PRIMARY_LBH)).thenReturn(false);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_LBH)).thenReturn(true);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_DELTA_VOL_WT_MULT_FACTOR)).thenReturn(2.0);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_LBH_MARGIN)).thenReturn(0.1);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_DELTA_VOL_WT)).thenReturn(true);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_DELTA_VOL_WT_OFFSET)).thenReturn(1000.0);
    
        ReflectionTestUtils.setField(slabCacheDTO, "dep", mDepedencyForDTO);
        ReflectionTestUtils.setField(slabCacheDTO1, "dep", mDepedencyForDTO);
        ReflectionTestUtils.setField(slabCacheDTO2, "dep", mDepedencyForDTO);
        ReflectionTestUtils.setField(slabCacheDTO3, "dep", mDepedencyForDTO);
        ReflectionTestUtils.setField(slabCacheDTO4, "dep", mDepedencyForDTO);
        GetPackagingRecommendationsResponse resp = packmanLandingService.getPackagingRecommendations(request);
        assertTrue(resp.isSuccessful());
        assertEquals("CARTON", resp.getPackagingRecommendationList().get(0).getPackagingType().getType());
        assertEquals("Normal", resp.getPackagingRecommendationList().get(0).getPackagingMode());
        assertEquals("CB09", resp.getPackagingRecommendationList().get(0).getPreferredPackagingTypeItem().getCode());
    }
    
    //6.4.5
    @Test
    public void testGetPackagingRecommendationsNormalAtSupcLabelOldFeatureNewLBHandLBHFlagOnWithMarginWithMF3()
            throws PackagingRecomendationException, ExternalDataNotFoundException {
    
        List<String> labels = new ArrayList<String>();
        labels.add(Labels.Premium.toString());
        mExternalServiceFactory = mock(ExternalServiceFactory.class);
        ICAMSExternalService miCAMSExternalService = mock(ICAMSExternalService.class);
        GetPOGDetailListResponse response = new GetPOGDetailListResponse();
        POGDetailSRO pogDetailSROs = new POGDetailSRO();
        pogDetailSROs.setEnabled(true);
    
        List<POGDetailSRO> lpogDetailSROs = new ArrayList<POGDetailSRO>();
        lpogDetailSROs.add(pogDetailSROs);
        response.setPogDetailSROs(lpogDetailSROs);
        when(mExternalServiceFactory.getCAMSExternalService()).thenReturn(miCAMSExternalService);
        when(miCAMSExternalService.getPOGDetailsBbySupc(Matchers.<GetPOGDetailListBySUPCListRequest> any())).thenReturn(response);
        ReflectionTestUtils.setField(mruleEngineService, "factory", mExternalServiceFactory);
        PackagableItemSRO packagableItemSRO = createPackagableItemSRO(labels);
        lPackagableItemSRO.add(packagableItemSRO);
        packageDetailSRO.setPackagableItemList(lPackagableItemSRO);
        genericMao = mock(IGenericMao.class);
        ruleList = new ArrayList<>();
        List<CriteriaUnit> lCriteriaUnit = new ArrayList<CriteriaUnit>();
        CriteriaUnit criteriaUnit = createCriteriaUnit(1,"","");
        lCriteriaUnit.add(criteriaUnit);
        PackmanRuleUnit packmanRuleUnit = createPackmanRuleUnit(2,"ru03",7,"CARTON", lCriteriaUnit);

        packmanRuleUnit.setCriteria(lCriteriaUnit);
        ruleList.add(packmanRuleUnit);
        doAnswer(new Answer<List<PackmanRuleUnit>>() {
            public List<PackmanRuleUnit> answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                CocofsMongoQuery cocofsMongoQuery = (CocofsMongoQuery) args[0];
                List<Criteria> lCriteria = cocofsMongoQuery.getCriteria();
                Criteria criteria1 = lCriteria.get(0);
                String rule_type = (String) criteria1.getObj();
                if (rule_type.equals("store-packaging"))
                    return ruleList;
                return null;
    
            }
    
        }).when(genericMao).getDocumentList(Matchers.<CocofsMongoQuery> any(), Matchers.<Class<PackmanRuleUnit>> any());
        ReflectionTestUtils.setField(mruleEngineService, "genericMao", genericMao);
    
        PackmanPackagingRuleEngineServiceImpl packmanPackagingRuleEngineServiceImpl = Mockito.spy(new PackmanPackagingRuleEngineServiceImpl());
        Map<String, Object> ruleParam = new HashMap<String, Object>();
        SlabMappingCache mslabMappingCache = new SlabMappingCache();
        Map<String, NavigableMap<Double, SlabCacheDTO>> slabCache = new HashMap<String, NavigableMap<Double, SlabCacheDTO>>();
        SlabCacheDTO slabCacheDTO = new SlabCacheDTO();
        List<SlabItemDTO> boxes = new ArrayList<SlabItemDTO>();
    
        SlabItemDTO slabItemDTO = createSlabItemDto("CB101",10.0,10.0,10.0,100.00,"CB101");
        boxes.add(slabItemDTO);
        slabItemDTO = createSlabItemDto("CB103",10.0,51.0,2.0,102.0,"CB103");
        boxes.add(slabItemDTO);
        slabItemDTO = createSlabItemDto("CB102",10.1,10.1,10.1,103.03,"CB102");
        boxes.add(slabItemDTO);
        slabItemDTO = createSlabItemDto("CB18",11.43,11.43,11.43,149.33,"CB18");
        boxes.add(slabItemDTO);
    
        List<SlabItemDTO> boxes2 = new ArrayList<SlabItemDTO>();
    
        SlabCacheDTO slabCacheDTO2 = new SlabCacheDTO();
    
        SlabItemDTO slabItemDTO2 = createSlabItemDto("CB25",32.0,22.0,13.0,915.2,"FOOT WEAR");
        boxes2.add(slabItemDTO2);      
    
        List<SlabItemDTO> boxes1 = new ArrayList<SlabItemDTO>();    
        SlabCacheDTO slabCacheDTO1 = new SlabCacheDTO();
        SlabItemDTO slabItemDTO1 = createSlabItemDto("CB07",38.1,25.4,15.24,1474.84,"cb07");
        boxes1.add(slabItemDTO1);
    
        List<SlabItemDTO> boxes3 = new ArrayList<SlabItemDTO>();    
        SlabCacheDTO slabCacheDTO3 = new SlabCacheDTO();    
        SlabItemDTO slabItemDTO3 = createSlabItemDto("CB09",48.26,35.4,15.24,2615.38,"cb09");
        boxes3.add(slabItemDTO3);
    
        List<SlabItemDTO> boxes4 = new ArrayList<SlabItemDTO>();    
        SlabCacheDTO slabCacheDTO4 = new SlabCacheDTO();    
        SlabItemDTO slabItemDTO4 = createSlabItemDto("CB16",63.5,30.48,48.26,7846.13,"cb16");
        boxes4.add(slabItemDTO4);
    
        slabCacheDTO = createSlabCacheDTO(boxes,0,500);    
        slabCacheDTO2 = createSlabCacheDTO(boxes2,500,500);    
        slabCacheDTO1 = createSlabCacheDTO(boxes1,1000,500);    
        slabCacheDTO3 = createSlabCacheDTO(boxes3,2500,500);    
        slabCacheDTO4 = createSlabCacheDTO(boxes4,7000,500);
    
        NavigableMap<Double, SlabCacheDTO> navigableMap = new ConcurrentSkipListMap<Double, SlabCacheDTO>();
        navigableMap.put(0.0, slabCacheDTO);
        navigableMap.put(500.0, slabCacheDTO2);
        navigableMap.put(1000.0, slabCacheDTO1);
        navigableMap.put(2500.0, slabCacheDTO3);
        navigableMap.put(7000.0, slabCacheDTO4);
        slabCache.put("Snapdeal-CARTON", navigableMap);
    
        Map<String, List<PackagingTypeCacheDTO>> mtypeToPackagingTypeDTOsMap = new HashMap<String, List<PackagingTypeCacheDTO>>();
        PackagingTypeCache packagingTypeCache = new PackagingTypeCache();
        List<PackagingTypeCacheDTO> lPackagingTypeCacheDTO = new ArrayList<PackagingTypeCacheDTO>();
    
        PackagingTypeCacheDTO packagingTypeCacheDTO = createPackagingTypeCacheDTO("Snapdeal","CARTON","Normal");
        lPackagingTypeCacheDTO.add(packagingTypeCacheDTO);
        mtypeToPackagingTypeDTOsMap.put("CARTON", lPackagingTypeCacheDTO);
    
        PackagingType packagingType = createPackagingType("Snapdeal","Normal");
        pauSRO = createPauSro("990001",48.26,15.24,35.56,"CARTON");
        List<PackagingType> lpackagingList = new ArrayList<PackagingType>();
        lpackagingList.add(packagingType);
        packagingTypeCache.addPackagingType(lpackagingList);
        CacheManager.getInstance().setCache(packagingTypeCache);
        
        when(mcocofsExternalService.getProductFulfilmentAttributes(Matchers.<GetProductFulfilmentAttributeRequest> any())).thenReturn(pauSRO);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(48.26, 15.24, 35.56)).thenReturn(2615.38);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(32.0, 20.0, 14.0)).thenReturn(896.0);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(32.0, 22.0, 13.0)).thenReturn(915.2);
        ReflectionTestUtils.setField(mPackagingRecommendationService, "cocofsExternalService", mcocofsExternalService);
    
        mSupcPackagingTypeMongoService = mock(ISupcPackagingTypeMongoService.class);
        when(mSupcPackagingTypeMongoService.getEnabledAndActivePackagingTypeBySupcAndStoreCode("Snapdeal", "990001")).thenReturn("CARTON");
        ReflectionTestUtils.setField(mruleEngineService, "supcPackagingTypeMongoService", mSupcPackagingTypeMongoService);
    
        ReflectionTestUtils.setField(packagingTypeCache, "typeToPackagingTypeDTOsMap", mtypeToPackagingTypeDTOsMap);
    
        GetPackagingRecommendationsRequest request = mock(GetPackagingRecommendationsRequest.class);
        ReflectionTestUtils.setField(packmanLandingService, "packagingRecomendationService", mPackagingRecommendationService);
        Protocol proto = Protocol.PROTOCOL_JSON;
        when(request.getRequestProtocol()).thenReturn(proto);
        when(request.getApiKey()).thenReturn("p@(km@n");
        when(request.getStoreFrontId()).thenReturn("SD");
        when(request.getIsShipTogether()).thenReturn(false);
        when(request.getPackageDetail()).thenReturn(packageDetailSRO);
    
        CacheManager.getInstance().setCache(mslabMappingCache);
    
        ReflectionTestUtils.setField(mslabMappingCache, "slabCache", slabCache);
        mruleService = mock(IRulesService.class);
        List<BlockParams> lblockParams = new ArrayList<BlockParams>();
        Block block = createBlock("StorePackagingRuleBlock");
        BlockParams blockParams = createBlockParams(block,"LABEL","Label");
        ruleParam.put("BRAND", "StorePackagingRuleBlock");
        Mockito.doReturn(ruleParam).when(packmanPackagingRuleEngineServiceImpl).populateRuleParamsFromBlockObject(Matchers.any(), (List<BlockParams>) Matchers.any());
        lblockParams.add(blockParams);
        mexclusiveService = mock(PackmanExclusiveServiceImpl.class);
        when(mruleService.getBlockParamsByBlockName("StorePackagingRuleBlock")).thenReturn(lblockParams);
        ReflectionTestUtils.setField(mruleEngineService, "ruleService", mruleService);
        ReflectionTestUtils.setField(mruleEngineService, "exclusiveService", mexclusiveService);
        mDepedencyForDTO = mock(com.snapdeal.packman.dto.SlabCacheDTO.IDependency.class);
    
        when(mDepedencyForDTO.getBooleanScalar(Property.USE_PRIMARY_LBH)).thenReturn(false);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_LBH)).thenReturn(true);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_DELTA_VOL_WT_MULT_FACTOR)).thenReturn(3.0);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_LBH_MARGIN)).thenReturn(0.1);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_DELTA_VOL_WT)).thenReturn(false);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_DELTA_VOL_WT_OFFSET)).thenReturn(5000.0);
    
        ReflectionTestUtils.setField(slabCacheDTO, "dep", mDepedencyForDTO);
        ReflectionTestUtils.setField(slabCacheDTO1, "dep", mDepedencyForDTO);
        ReflectionTestUtils.setField(slabCacheDTO2, "dep", mDepedencyForDTO);
        ReflectionTestUtils.setField(slabCacheDTO3, "dep", mDepedencyForDTO);
        ReflectionTestUtils.setField(slabCacheDTO4, "dep", mDepedencyForDTO);
        GetPackagingRecommendationsResponse resp = packmanLandingService.getPackagingRecommendations(request);
     
        assertTrue(resp.isSuccessful());
        assertEquals("CARTON", resp.getPackagingRecommendationList().get(0).getPackagingType().getType());
        assertEquals("Normal", resp.getPackagingRecommendationList().get(0).getPackagingMode());
        assertEquals("CB09", resp.getPackagingRecommendationList().get(0).getPreferredPackagingTypeItem().getCode());
    }
    
    //6.5.1
    @Test
    public void testGetPackagingRecommendationsNormalAtSupcLabelOldFeatureNewLBHSet4() throws PackagingRecomendationException, ExternalDataNotFoundException {
    
        List<String> labels = new ArrayList<String>();
        labels.add(Labels.Premium.toString());
        mExternalServiceFactory = mock(ExternalServiceFactory.class);
        ICAMSExternalService miCAMSExternalService = mock(ICAMSExternalService.class);
        GetPOGDetailListResponse response = new GetPOGDetailListResponse();
        POGDetailSRO pogDetailSROs = new POGDetailSRO();
        pogDetailSROs.setEnabled(true);
    
        List<POGDetailSRO> lpogDetailSROs = new ArrayList<POGDetailSRO>();
        lpogDetailSROs.add(pogDetailSROs);
        response.setPogDetailSROs(lpogDetailSROs);
        when(mExternalServiceFactory.getCAMSExternalService()).thenReturn(miCAMSExternalService);
        when(miCAMSExternalService.getPOGDetailsBbySupc(Matchers.<GetPOGDetailListBySUPCListRequest> any())).thenReturn(response);
        ReflectionTestUtils.setField(mruleEngineService, "factory", mExternalServiceFactory);
        PackagableItemSRO packagableItemSRO = createPackagableItemSRO(labels);
        lPackagableItemSRO.add(packagableItemSRO);
        packageDetailSRO.setPackagableItemList(lPackagableItemSRO);
        genericMao = mock(IGenericMao.class);
        ruleList = new ArrayList<>();
        List<CriteriaUnit> lCriteriaUnit = new ArrayList<CriteriaUnit>();
        CriteriaUnit criteriaUnit = createCriteriaUnit(1,"","");
        lCriteriaUnit.add(criteriaUnit);
       PackmanRuleUnit packmanRuleUnit = createPackmanRuleUnit(2,"ru03",7,"CARTON", lCriteriaUnit);

        packmanRuleUnit.setCriteria(lCriteriaUnit);
        ruleList.add(packmanRuleUnit);
        doAnswer(new Answer<List<PackmanRuleUnit>>() {
            public List<PackmanRuleUnit> answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                CocofsMongoQuery cocofsMongoQuery = (CocofsMongoQuery) args[0];
                List<Criteria> lCriteria = cocofsMongoQuery.getCriteria();
                Criteria criteria1 = lCriteria.get(0);
                String rule_type = (String) criteria1.getObj();
                if (rule_type.equals("store-packaging"))
                    return ruleList;
                return null;
    
            }
    
        }).when(genericMao).getDocumentList(Matchers.<CocofsMongoQuery> any(), Matchers.<Class<PackmanRuleUnit>> any());
        ReflectionTestUtils.setField(mruleEngineService, "genericMao", genericMao);
    
        PackmanPackagingRuleEngineServiceImpl packmanPackagingRuleEngineServiceImpl = Mockito.spy(new PackmanPackagingRuleEngineServiceImpl());
        Map<String, Object> ruleParam = new HashMap<String, Object>();
        SlabMappingCache mslabMappingCache = new SlabMappingCache();
        Map<String, NavigableMap<Double, SlabCacheDTO>> slabCache = new HashMap<String, NavigableMap<Double, SlabCacheDTO>>();
        List<SlabItemDTO> boxes = new ArrayList<SlabItemDTO>();
    
        SlabItemDTO slabItemDTO = createSlabItemDto("CB101",10.0,10.0,10.0,100.00,"CB101");
        boxes.add(slabItemDTO);
        slabItemDTO = createSlabItemDto("CB103",10.0,51.0,2.0,102.0,"CB103");
        boxes.add(slabItemDTO);
        slabItemDTO = createSlabItemDto("CB102",10.1,10.1,10.1,103.03,"CB102");
        boxes.add(slabItemDTO);
        slabItemDTO = createSlabItemDto("CB18",11.43,11.43,11.43,149.33,"CB18");
        boxes.add(slabItemDTO);
    
        List<SlabItemDTO> boxes2 = new ArrayList<SlabItemDTO>();
        SlabItemDTO slabItemDTO2 = createSlabItemDto("CB25",32.0,22.0,13.0,915.2,"FOOT WEAR");
        boxes2.add(slabItemDTO2);      
        List<SlabItemDTO> boxes1 = new ArrayList<SlabItemDTO>();
        SlabItemDTO slabItemDTO1 = createSlabItemDto("CB07",38.1,25.4,15.24,1474.84,"cb07");
        boxes1.add(slabItemDTO1);    
        List<SlabItemDTO> boxes3 = new ArrayList<SlabItemDTO>();
        SlabItemDTO slabItemDTO3 = createSlabItemDto("CB09",48.26,35.4,15.24,2615.38,"cb09");
        boxes3.add(slabItemDTO3);
        List<SlabItemDTO> boxes4 = new ArrayList<SlabItemDTO>();
        SlabItemDTO slabItemDTO4 = createSlabItemDto("CB16",63.5,30.48,48.26,7846.13,"cb16");
        boxes4.add(slabItemDTO4);
    
        List<SlabItemDTO> boxes5 = new ArrayList<SlabItemDTO>();    
        SlabCacheDTO slabCacheDTO = createSlabCacheDTO(boxes,0,500);    
        SlabCacheDTO slabCacheDTO2 = createSlabCacheDTO(boxes2,500,500);    
        SlabCacheDTO slabCacheDTO1 = createSlabCacheDTO(boxes1,1000,500);    
        SlabCacheDTO slabCacheDTO3 = createSlabCacheDTO(boxes3,2500,500);    
        SlabCacheDTO slabCacheDTO4 = createSlabCacheDTO(boxes4,7000,500);        
        SlabCacheDTO slabCacheDTO5 = createSlabCacheDTO(boxes5,1500,500);
        
        NavigableMap<Double, SlabCacheDTO> navigableMap = new ConcurrentSkipListMap<Double, SlabCacheDTO>();
        navigableMap.put(0.0, slabCacheDTO);
        navigableMap.put(500.0, slabCacheDTO2);
        navigableMap.put(1000.0, slabCacheDTO1);
        navigableMap.put(2500.0, slabCacheDTO3);
        navigableMap.put(7000.0, slabCacheDTO4);
        navigableMap.put(7000.0, slabCacheDTO5);
        slabCache.put("Snapdeal-CARTON", navigableMap);
    
        Map<String, List<PackagingTypeCacheDTO>> mtypeToPackagingTypeDTOsMap = new HashMap<String, List<PackagingTypeCacheDTO>>();
        PackagingTypeCache packagingTypeCache = new PackagingTypeCache();
        List<PackagingTypeCacheDTO> lPackagingTypeCacheDTO = new ArrayList<PackagingTypeCacheDTO>();
    
        PackagingTypeCacheDTO packagingTypeCacheDTO = createPackagingTypeCacheDTO("Snapdeal","CARTON","Normal");
        lPackagingTypeCacheDTO.add(packagingTypeCacheDTO);
        mtypeToPackagingTypeDTOsMap.put("CARTON", lPackagingTypeCacheDTO);
    
        PackagingType packagingType = createPackagingType("Snapdeal","Normal");
        pauSRO = createPauSro("990001",40.01,20.0,9.98,"CARTON");
        List<PackagingType> lpackagingList = new ArrayList<PackagingType>();
        lpackagingList.add(packagingType);
        packagingTypeCache.addPackagingType(lpackagingList);
        CacheManager.getInstance().setCache(packagingTypeCache);
        when(mcocofsExternalService.getProductFulfilmentAttributes(Matchers.<GetProductFulfilmentAttributeRequest> any())).thenReturn(pauSRO);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(40.01, 20.0, 9.98)).thenReturn(800.199);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(48.26, 15.24, 35.56)).thenReturn(2615.38);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(32.0, 20.0, 14.0)).thenReturn(896.0);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(32.0, 22.0, 13.0)).thenReturn(915.2);
        ReflectionTestUtils.setField(mPackagingRecommendationService, "cocofsExternalService", mcocofsExternalService);
    
        mSupcPackagingTypeMongoService = mock(ISupcPackagingTypeMongoService.class);
        when(mSupcPackagingTypeMongoService.getEnabledAndActivePackagingTypeBySupcAndStoreCode("Snapdeal", "990001")).thenReturn("CARTON");
        ReflectionTestUtils.setField(mruleEngineService, "supcPackagingTypeMongoService", mSupcPackagingTypeMongoService);
    
        ReflectionTestUtils.setField(packagingTypeCache, "typeToPackagingTypeDTOsMap", mtypeToPackagingTypeDTOsMap);
    
        GetPackagingRecommendationsRequest request = mock(GetPackagingRecommendationsRequest.class);
        ReflectionTestUtils.setField(packmanLandingService, "packagingRecomendationService", mPackagingRecommendationService);
        Protocol proto = Protocol.PROTOCOL_JSON;
        when(request.getRequestProtocol()).thenReturn(proto);
        when(request.getApiKey()).thenReturn("p@(km@n");
        when(request.getStoreFrontId()).thenReturn("SD");
        when(request.getIsShipTogether()).thenReturn(false);
        when(request.getPackageDetail()).thenReturn(packageDetailSRO);
    
        CacheManager.getInstance().setCache(mslabMappingCache);
    
        ReflectionTestUtils.setField(mslabMappingCache, "slabCache", slabCache);
        mruleService = mock(IRulesService.class);
        List<BlockParams> lblockParams = new ArrayList<BlockParams>();
        Block block = createBlock("StorePackagingRuleBlock");
        BlockParams blockParams = createBlockParams(block,"LABEL","Label");
        ruleParam.put("BRAND", "StorePackagingRuleBlock");
        Mockito.doReturn(ruleParam).when(packmanPackagingRuleEngineServiceImpl).populateRuleParamsFromBlockObject(Matchers.any(), (List<BlockParams>) Matchers.any());
        lblockParams.add(blockParams);
        mexclusiveService = mock(PackmanExclusiveServiceImpl.class);
        when(mruleService.getBlockParamsByBlockName("StorePackagingRuleBlock")).thenReturn(lblockParams);
        ReflectionTestUtils.setField(mruleEngineService, "ruleService", mruleService);
        ReflectionTestUtils.setField(mruleEngineService, "exclusiveService", mexclusiveService);
        mDepedencyForDTO = mock(com.snapdeal.packman.dto.SlabCacheDTO.IDependency.class);
    
        when(mDepedencyForDTO.getBooleanScalar(Property.USE_PRIMARY_LBH)).thenReturn(false);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_LBH)).thenReturn(false);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_DELTA_VOL_WT_MULT_FACTOR)).thenReturn(2.0);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_LBH_MARGIN)).thenReturn(0.1);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_DELTA_VOL_WT)).thenReturn(false);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_DELTA_VOL_WT_OFFSET)).thenReturn(5000.0);
    
        ReflectionTestUtils.setField(slabCacheDTO, "dep", mDepedencyForDTO);
        ReflectionTestUtils.setField(slabCacheDTO1, "dep", mDepedencyForDTO);
        ReflectionTestUtils.setField(slabCacheDTO2, "dep", mDepedencyForDTO);
        ReflectionTestUtils.setField(slabCacheDTO3, "dep", mDepedencyForDTO);
        ReflectionTestUtils.setField(slabCacheDTO4, "dep", mDepedencyForDTO);
        GetPackagingRecommendationsResponse resp = packmanLandingService.getPackagingRecommendations(request);
        assertTrue(resp.isSuccessful());
        assertEquals("CARTON", resp.getPackagingRecommendationList().get(0).getPackagingType().getType());
        assertEquals("Normal", resp.getPackagingRecommendationList().get(0).getPackagingMode());
        assertEquals("CB25", resp.getPackagingRecommendationList().get(0).getPreferredPackagingTypeItem().getCode());
    }
    
    //6.5.2
    @Test
    public void testGetPackagingRecommendationsNormalAtSupcLabelOldFeatureNewLBHSet4andLBHFlagOn() throws PackagingRecomendationException, ExternalDataNotFoundException {
    
        List<String> labels = new ArrayList<String>();
        labels.add(Labels.Premium.toString());
        mExternalServiceFactory = mock(ExternalServiceFactory.class);
        ICAMSExternalService miCAMSExternalService = mock(ICAMSExternalService.class);
        GetPOGDetailListResponse response = new GetPOGDetailListResponse();
        POGDetailSRO pogDetailSROs = new POGDetailSRO();
        pogDetailSROs.setEnabled(true);
    
        List<POGDetailSRO> lpogDetailSROs = new ArrayList<POGDetailSRO>();
        lpogDetailSROs.add(pogDetailSROs);
        response.setPogDetailSROs(lpogDetailSROs);
        when(mExternalServiceFactory.getCAMSExternalService()).thenReturn(miCAMSExternalService);
        when(miCAMSExternalService.getPOGDetailsBbySupc(Matchers.<GetPOGDetailListBySUPCListRequest> any())).thenReturn(response);
        ReflectionTestUtils.setField(mruleEngineService, "factory", mExternalServiceFactory);
        PackagableItemSRO packagableItemSRO = createPackagableItemSRO(labels);
        lPackagableItemSRO.add(packagableItemSRO);
        packageDetailSRO.setPackagableItemList(lPackagableItemSRO);
        genericMao = mock(IGenericMao.class);
        ruleList = new ArrayList<>();
        List<CriteriaUnit> lCriteriaUnit = new ArrayList<CriteriaUnit>();
        CriteriaUnit criteriaUnit = createCriteriaUnit(1,"","");
        lCriteriaUnit.add(criteriaUnit);
        PackmanRuleUnit packmanRuleUnit = createPackmanRuleUnit(2,"ru03",7,"CARTON", lCriteriaUnit);

        packmanRuleUnit.setCriteria(lCriteriaUnit);
        ruleList.add(packmanRuleUnit);
        doAnswer(new Answer<List<PackmanRuleUnit>>() {
            public List<PackmanRuleUnit> answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                CocofsMongoQuery cocofsMongoQuery = (CocofsMongoQuery) args[0];
                List<Criteria> lCriteria = cocofsMongoQuery.getCriteria();
                Criteria criteria1 = lCriteria.get(0);
                String rule_type = (String) criteria1.getObj();
                if (rule_type.equals("store-packaging"))
                    return ruleList;
                return null;
    
            }
    
        }).when(genericMao).getDocumentList(Matchers.<CocofsMongoQuery> any(), Matchers.<Class<PackmanRuleUnit>> any());
        ReflectionTestUtils.setField(mruleEngineService, "genericMao", genericMao);
    
        PackmanPackagingRuleEngineServiceImpl packmanPackagingRuleEngineServiceImpl = Mockito.spy(new PackmanPackagingRuleEngineServiceImpl());
        Map<String, Object> ruleParam = new HashMap<String, Object>();
        SlabMappingCache mslabMappingCache = new SlabMappingCache();
        Map<String, NavigableMap<Double, SlabCacheDTO>> slabCache = new HashMap<String, NavigableMap<Double, SlabCacheDTO>>();
        SlabCacheDTO slabCacheDTO = new SlabCacheDTO();
        List<SlabItemDTO> boxes = new ArrayList<SlabItemDTO>();
    
        SlabItemDTO slabItemDTO = createSlabItemDto("CB101",10.0,10.0,10.0,100.00,"CB101");
        boxes.add(slabItemDTO);
        slabItemDTO = createSlabItemDto("CB103",10.0,51.0,2.0,102.0,"CB103");
        boxes.add(slabItemDTO);
        slabItemDTO = createSlabItemDto("CB102",10.1,10.1,10.1,103.03,"CB102");
        boxes.add(slabItemDTO);
        slabItemDTO = createSlabItemDto("CB18",11.43,11.43,11.43,149.33,"CB18");
        boxes.add(slabItemDTO);
    
        List<SlabItemDTO> boxes2 = new ArrayList<SlabItemDTO>();
        SlabItemDTO slabItemDTO2 = createSlabItemDto("CB25",32.0,22.0,13.0,915.2,"FOOT WEAR");
        boxes2.add(slabItemDTO2);
        SlabCacheDTO slabCacheDTO2 = new SlabCacheDTO();
        List<SlabItemDTO> boxes1 = new ArrayList<SlabItemDTO>();
        SlabCacheDTO slabCacheDTO1 = new SlabCacheDTO();
        SlabItemDTO slabItemDTO1 = createSlabItemDto("CB07",38.1,25.4,15.24,1474.84,"cb07");
        boxes1.add(slabItemDTO1);    
        List<SlabItemDTO> boxes3 = new ArrayList<SlabItemDTO>();
        SlabCacheDTO slabCacheDTO3 = new SlabCacheDTO();
        SlabItemDTO slabItemDTO3 = createSlabItemDto("CB09",48.26,35.4,15.24,2615.38,"cb09");
        boxes3.add(slabItemDTO3);
        slabItemDTO3 = createSlabItemDto("CB11",55.88,45.72,10.16,4090.21,"CB11");
        boxes3.add(slabItemDTO3);    
        List<SlabItemDTO> boxes4 = new ArrayList<SlabItemDTO>();    
        SlabCacheDTO slabCacheDTO4 = new SlabCacheDTO();
        SlabItemDTO slabItemDTO4 = createSlabItemDto("CB16",63.5,30.48,48.26,7846.13,"cb16");
        boxes4.add(slabItemDTO4);    
        List<SlabItemDTO> boxes5 = new ArrayList<SlabItemDTO>();

        SlabCacheDTO slabCacheDTO5 = new SlabCacheDTO();
        slabCacheDTO = createSlabCacheDTO(boxes,0,500);    
        slabCacheDTO2 = createSlabCacheDTO(boxes2,500,500);    
        slabCacheDTO1 = createSlabCacheDTO(boxes1,1000,500);    
        slabCacheDTO3 = createSlabCacheDTO(boxes3,2500,500);    
        slabCacheDTO4 = createSlabCacheDTO(boxes4,7000,500);    
        slabCacheDTO5.setBoxes(boxes5);
        slabCacheDTO5.setLowerLimit(1500);
        slabCacheDTO5.setSlabSize(500);
    
        NavigableMap<Double, SlabCacheDTO> navigableMap = new ConcurrentSkipListMap<Double, SlabCacheDTO>();
        navigableMap.put(0.0, slabCacheDTO);
        navigableMap.put(500.0, slabCacheDTO2);
        navigableMap.put(1000.0, slabCacheDTO1);
        navigableMap.put(2500.0, slabCacheDTO3);
        navigableMap.put(7000.0, slabCacheDTO4);
        navigableMap.put(1500.0, slabCacheDTO5);
        slabCache.put("Snapdeal-CARTON", navigableMap);
    
        Map<String, List<PackagingTypeCacheDTO>> mtypeToPackagingTypeDTOsMap = new HashMap<String, List<PackagingTypeCacheDTO>>();
        PackagingTypeCache packagingTypeCache = new PackagingTypeCache();
        List<PackagingTypeCacheDTO> lPackagingTypeCacheDTO = new ArrayList<PackagingTypeCacheDTO>();
    
        PackagingTypeCacheDTO packagingTypeCacheDTO = createPackagingTypeCacheDTO("Snapdeal","CARTON","Normal");
        lPackagingTypeCacheDTO.add(packagingTypeCacheDTO);
        mtypeToPackagingTypeDTOsMap.put("CARTON", lPackagingTypeCacheDTO);
    
        PackagingType packagingType = createPackagingType("Snapdeal","Normal");
        pauSRO = createPauSro("990001",40.01,20.0,9.98,"CARTON");
        List<PackagingType> lpackagingList = new ArrayList<PackagingType>();
        lpackagingList.add(packagingType);
        packagingTypeCache.addPackagingType(lpackagingList);
        CacheManager.getInstance().setCache(packagingTypeCache);
        when(mcocofsExternalService.getProductFulfilmentAttributes(Matchers.<GetProductFulfilmentAttributeRequest> any())).thenReturn(pauSRO);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(40.01, 20.0, 9.98)).thenReturn(800.199);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(48.26, 15.24, 35.56)).thenReturn(2615.38);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(32.0, 20.0, 14.0)).thenReturn(896.0);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(32.0, 22.0, 13.0)).thenReturn(915.2);
        ReflectionTestUtils.setField(mPackagingRecommendationService, "cocofsExternalService", mcocofsExternalService);
    
        mSupcPackagingTypeMongoService = mock(ISupcPackagingTypeMongoService.class);
        when(mSupcPackagingTypeMongoService.getEnabledAndActivePackagingTypeBySupcAndStoreCode("Snapdeal", "990001")).thenReturn("CARTON");
        ReflectionTestUtils.setField(mruleEngineService, "supcPackagingTypeMongoService", mSupcPackagingTypeMongoService);
    
        ReflectionTestUtils.setField(packagingTypeCache, "typeToPackagingTypeDTOsMap", mtypeToPackagingTypeDTOsMap);
    
        GetPackagingRecommendationsRequest request = mock(GetPackagingRecommendationsRequest.class);
        ReflectionTestUtils.setField(packmanLandingService, "packagingRecomendationService", mPackagingRecommendationService);
        Protocol proto = Protocol.PROTOCOL_JSON;
        when(request.getRequestProtocol()).thenReturn(proto);
        when(request.getApiKey()).thenReturn("p@(km@n");
        when(request.getStoreFrontId()).thenReturn("SD");
        when(request.getIsShipTogether()).thenReturn(false);
        when(request.getPackageDetail()).thenReturn(packageDetailSRO);
    
        CacheManager.getInstance().setCache(mslabMappingCache);
    
        ReflectionTestUtils.setField(mslabMappingCache, "slabCache", slabCache);
        mruleService = mock(IRulesService.class);
        List<BlockParams> lblockParams = new ArrayList<BlockParams>();
        Block block = createBlock("StorePackagingRuleBlock");
        BlockParams blockParams = createBlockParams(block,"LABEL","Label");
        ruleParam.put("BRAND", "StorePackagingRuleBlock");
        Mockito.doReturn(ruleParam).when(packmanPackagingRuleEngineServiceImpl).populateRuleParamsFromBlockObject(Matchers.any(), (List<BlockParams>) Matchers.any());
        lblockParams.add(blockParams);
        mexclusiveService = mock(PackmanExclusiveServiceImpl.class);
        when(mruleService.getBlockParamsByBlockName("StorePackagingRuleBlock")).thenReturn(lblockParams);
        ReflectionTestUtils.setField(mruleEngineService, "ruleService", mruleService);
        ReflectionTestUtils.setField(mruleEngineService, "exclusiveService", mexclusiveService);
        mDepedencyForDTO = mock(com.snapdeal.packman.dto.SlabCacheDTO.IDependency.class);
    
        when(mDepedencyForDTO.getBooleanScalar(Property.USE_PRIMARY_LBH)).thenReturn(false);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_LBH)).thenReturn(true);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_DELTA_VOL_WT_MULT_FACTOR)).thenReturn(2.0);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_LBH_MARGIN)).thenReturn(0.0);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_DELTA_VOL_WT)).thenReturn(false);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_DELTA_VOL_WT_OFFSET)).thenReturn(5000.0);
    
        ReflectionTestUtils.setField(slabCacheDTO, "dep", mDepedencyForDTO);
        ReflectionTestUtils.setField(slabCacheDTO1, "dep", mDepedencyForDTO);
        ReflectionTestUtils.setField(slabCacheDTO2, "dep", mDepedencyForDTO);
        ReflectionTestUtils.setField(slabCacheDTO3, "dep", mDepedencyForDTO);
        ReflectionTestUtils.setField(slabCacheDTO4, "dep", mDepedencyForDTO);
        GetPackagingRecommendationsResponse resp = packmanLandingService.getPackagingRecommendations(request);
        assertTrue(resp.isSuccessful());
        assertEquals("CARTON", resp.getPackagingRecommendationList().get(0).getPackagingType().getType());
        assertEquals("Normal", resp.getPackagingRecommendationList().get(0).getPackagingMode());
        assertEquals("CB25", resp.getPackagingRecommendationList().get(0).getPreferredPackagingTypeItem().getCode());
    }
    
    @Test
    public void testGetPackagingRecommendationsNormalAtSupcLabelOldFeatureNewLBHSet4WithDeltaFeature() throws PackagingRecomendationException, ExternalDataNotFoundException {
    
        List<String> labels = new ArrayList<String>();
        labels.add(Labels.Premium.toString());
        mExternalServiceFactory = mock(ExternalServiceFactory.class);
        ICAMSExternalService miCAMSExternalService = mock(ICAMSExternalService.class);
        GetPOGDetailListResponse response = new GetPOGDetailListResponse();
        POGDetailSRO pogDetailSROs = new POGDetailSRO();
        pogDetailSROs.setEnabled(true);
    
        List<POGDetailSRO> lpogDetailSROs = new ArrayList<POGDetailSRO>();
        lpogDetailSROs.add(pogDetailSROs);
        response.setPogDetailSROs(lpogDetailSROs);
        when(mExternalServiceFactory.getCAMSExternalService()).thenReturn(miCAMSExternalService);
        when(miCAMSExternalService.getPOGDetailsBbySupc(Matchers.<GetPOGDetailListBySUPCListRequest> any())).thenReturn(response);
        ReflectionTestUtils.setField(mruleEngineService, "factory", mExternalServiceFactory);
        PackagableItemSRO packagableItemSRO = createPackagableItemSRO(labels);
        lPackagableItemSRO.add(packagableItemSRO);
        packageDetailSRO.setPackagableItemList(lPackagableItemSRO);
        genericMao = mock(IGenericMao.class);
        ruleList = new ArrayList<>();
        List<CriteriaUnit> lCriteriaUnit = new ArrayList<CriteriaUnit>();
        CriteriaUnit criteriaUnit = createCriteriaUnit(1,"","");
        lCriteriaUnit.add(criteriaUnit);
        PackmanRuleUnit packmanRuleUnit = createPackmanRuleUnit(2,"ru03",7,"CARTON", lCriteriaUnit);

        packmanRuleUnit.setCriteria(lCriteriaUnit);
        ruleList.add(packmanRuleUnit);
        doAnswer(new Answer<List<PackmanRuleUnit>>() {
            public List<PackmanRuleUnit> answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                CocofsMongoQuery cocofsMongoQuery = (CocofsMongoQuery) args[0];
                List<Criteria> lCriteria = cocofsMongoQuery.getCriteria();
                Criteria criteria1 = lCriteria.get(0);
                String rule_type = (String) criteria1.getObj();
                if (rule_type.equals("store-packaging"))
                    return ruleList;
                return null;
    
            }
    
        }).when(genericMao).getDocumentList(Matchers.<CocofsMongoQuery> any(), Matchers.<Class<PackmanRuleUnit>> any());
        ReflectionTestUtils.setField(mruleEngineService, "genericMao", genericMao);
    
        PackmanPackagingRuleEngineServiceImpl packmanPackagingRuleEngineServiceImpl = Mockito.spy(new PackmanPackagingRuleEngineServiceImpl());
        Map<String, Object> ruleParam = new HashMap<String, Object>();
        SlabMappingCache mslabMappingCache = new SlabMappingCache();
        Map<String, NavigableMap<Double, SlabCacheDTO>> slabCache = new HashMap<String, NavigableMap<Double, SlabCacheDTO>>();
        List<SlabItemDTO> boxes = new ArrayList<SlabItemDTO>();

        SlabItemDTO slabItemDTO = createSlabItemDto("CB101",10.0,10.0,10.0,100.00,"CB101");
        boxes.add(slabItemDTO);
        slabItemDTO = new SlabItemDTO();
        slabItemDTO = createSlabItemDto("CB103",10.0,51.0,2.0,102.0,"CB103");
        boxes.add(slabItemDTO);
        slabItemDTO = createSlabItemDto("CB102",10.1,10.1,10.1,103.03,"CB102");
        boxes.add(slabItemDTO);
        slabItemDTO = createSlabItemDto("CB18",11.43,11.43,11.43,149.33,"CB18");
        boxes.add(slabItemDTO);
        List<SlabItemDTO> boxes2 = new ArrayList<SlabItemDTO>();        
        SlabItemDTO slabItemDTO2 = createSlabItemDto("CB07",38.1,25.4,15.24,1474.84,"cb07");
        boxes2.add(slabItemDTO2);  
        List<SlabItemDTO> boxes1 = new ArrayList<SlabItemDTO>();
        SlabItemDTO slabItemDTO1 = createSlabItemDto("CB10",30.48,15.24,17.78,825.91,"cb10");
        boxes1.add(slabItemDTO1);
        slabItemDTO1 = createSlabItemDto("CB25",32.0,22.0,13.0,915.2,"FOOT WEAR");
        boxes1.add(slabItemDTO1);   
        List<SlabItemDTO> boxes5 = new ArrayList<SlabItemDTO>();    
        SlabItemDTO slabItemDTO5 = new SlabItemDTO();    
        slabItemDTO = createSlabItemDto("CB105",50.0,35.0,10.0,1750.00,"CB105");
        boxes5.add(slabItemDTO5);    
        List<SlabItemDTO> boxes3 = new ArrayList<SlabItemDTO>();
        SlabItemDTO slabItemDTO3 = createSlabItemDto("CB09",48.26,35.4,15.24,2615.38,"cb09");
        boxes3.add(slabItemDTO3);        
        slabItemDTO3 = createSlabItemDto("CB11",55.88,45.72,10.16,2595.75,"CB11");
        boxes3.add(slabItemDTO3);    
        List<SlabItemDTO> boxes4 = new ArrayList<SlabItemDTO>();    
        SlabItemDTO slabItemDTO4 = createSlabItemDto("CB16",63.5,30.48,48.26,7846.13,"cb16");
        boxes4.add(slabItemDTO4);

        SlabCacheDTO slabCacheDTO = createSlabCacheDTO(boxes,0,500);
        SlabCacheDTO slabCacheDTO1 = createSlabCacheDTO(boxes1,500,500);
        SlabCacheDTO slabCacheDTO2 = createSlabCacheDTO(boxes2,1000,500);
        SlabCacheDTO slabCacheDTO5 = createSlabCacheDTO(boxes5,1500,500);    
        SlabCacheDTO slabCacheDTO3 = createSlabCacheDTO(boxes3,2500,500);
        SlabCacheDTO slabCacheDTO4 = createSlabCacheDTO(boxes4,7000,500);
    
        NavigableMap<Double, SlabCacheDTO> navigableMap = new ConcurrentSkipListMap<Double, SlabCacheDTO>();
        navigableMap.put(0.0, slabCacheDTO);
        navigableMap.put(500.0, slabCacheDTO1);
        navigableMap.put(1000.0, slabCacheDTO2);
        navigableMap.put(1500.0, slabCacheDTO5);
        navigableMap.put(2500.0, slabCacheDTO3);
        navigableMap.put(7000.0, slabCacheDTO4);
    
        slabCache.put("Snapdeal-CARTON", navigableMap);
    
        Map<String, List<PackagingTypeCacheDTO>> mtypeToPackagingTypeDTOsMap = new HashMap<String, List<PackagingTypeCacheDTO>>();
        PackagingTypeCache packagingTypeCache = new PackagingTypeCache();
        List<PackagingTypeCacheDTO> lPackagingTypeCacheDTO = new ArrayList<PackagingTypeCacheDTO>();
    
        PackagingTypeCacheDTO packagingTypeCacheDTO = createPackagingTypeCacheDTO("Snapdeal","CARTON","Normal");
        lPackagingTypeCacheDTO.add(packagingTypeCacheDTO);
        mtypeToPackagingTypeDTOsMap.put("CARTON", lPackagingTypeCacheDTO);
    
        PackagingType packagingType = createPackagingType("Snapdeal","Normal");
        pauSRO = createPauSro("990001",120.00,10.00,10.00,"CARTON");
        List<PackagingType> lpackagingList = new ArrayList<PackagingType>();
        lpackagingList.add(packagingType);
        packagingTypeCache.addPackagingType(lpackagingList);
        CacheManager.getInstance().setCache(packagingTypeCache);
        when(mcocofsExternalService.getProductFulfilmentAttributes(Matchers.<GetProductFulfilmentAttributeRequest> any())).thenReturn(pauSRO);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(120.00, 10.0, 10.00)).thenReturn(1200.00);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(40.01, 20.0, 9.98)).thenReturn(800.1999999999999);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(48.26, 15.24, 35.56)).thenReturn(2615.38);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(32.0, 20.0, 14.0)).thenReturn(896.0);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(32.0, 22.0, 13.0)).thenReturn(915.2);
        ReflectionTestUtils.setField(mPackagingRecommendationService, "cocofsExternalService", mcocofsExternalService);
    
        mSupcPackagingTypeMongoService = mock(ISupcPackagingTypeMongoService.class);
        when(mSupcPackagingTypeMongoService.getEnabledAndActivePackagingTypeBySupcAndStoreCode("Snapdeal", "990001")).thenReturn("CARTON");
        ReflectionTestUtils.setField(mruleEngineService, "supcPackagingTypeMongoService", mSupcPackagingTypeMongoService);
    
        ReflectionTestUtils.setField(packagingTypeCache, "typeToPackagingTypeDTOsMap", mtypeToPackagingTypeDTOsMap);
    
        GetPackagingRecommendationsRequest request = mock(GetPackagingRecommendationsRequest.class);
        ReflectionTestUtils.setField(packmanLandingService, "packagingRecomendationService", mPackagingRecommendationService);
        Protocol proto = Protocol.PROTOCOL_JSON;
        when(request.getRequestProtocol()).thenReturn(proto);
        when(request.getApiKey()).thenReturn("p@(km@n");
        when(request.getStoreFrontId()).thenReturn("SD");
        when(request.getIsShipTogether()).thenReturn(false);
        when(request.getPackageDetail()).thenReturn(packageDetailSRO);
    
        CacheManager.getInstance().setCache(mslabMappingCache);
    
        ReflectionTestUtils.setField(mslabMappingCache, "slabCache", slabCache);
        mruleService = mock(IRulesService.class);
        List<BlockParams> lblockParams = new ArrayList<BlockParams>();
        Block block = createBlock("StorePackagingRuleBlock");
        BlockParams blockParams = createBlockParams(block,"LABEL","Label");
        ruleParam.put("BRAND", "StorePackagingRuleBlock");
        Mockito.doReturn(ruleParam).when(packmanPackagingRuleEngineServiceImpl).populateRuleParamsFromBlockObject(Matchers.any(), (List<BlockParams>) Matchers.any());
        lblockParams.add(blockParams);
        mexclusiveService = mock(PackmanExclusiveServiceImpl.class);
        when(mruleService.getBlockParamsByBlockName("StorePackagingRuleBlock")).thenReturn(lblockParams);
        ReflectionTestUtils.setField(mruleEngineService, "ruleService", mruleService);
        ReflectionTestUtils.setField(mruleEngineService, "exclusiveService", mexclusiveService);
        mDepedencyForDTO = mock(com.snapdeal.packman.dto.SlabCacheDTO.IDependency.class);
    
        when(mDepedencyForDTO.getBooleanScalar(Property.USE_PRIMARY_LBH)).thenReturn(false);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_LBH)).thenReturn(true);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_DELTA_VOL_WT_MULT_FACTOR)).thenReturn(2.0);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_LBH_MARGIN)).thenReturn(0.0);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_DELTA_VOL_WT)).thenReturn(true);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_DELTA_VOL_WT_OFFSET)).thenReturn(200.0);
    
        ReflectionTestUtils.setField(slabCacheDTO, "dep", mDepedencyForDTO);
        ReflectionTestUtils.setField(slabCacheDTO1, "dep", mDepedencyForDTO);
        ReflectionTestUtils.setField(slabCacheDTO2, "dep", mDepedencyForDTO);
        ReflectionTestUtils.setField(slabCacheDTO3, "dep", mDepedencyForDTO);
        ReflectionTestUtils.setField(slabCacheDTO4, "dep", mDepedencyForDTO);
        ReflectionTestUtils.setField(slabCacheDTO5, "dep", mDepedencyForDTO);
        GetPackagingRecommendationsResponse resp = packmanLandingService.getPackagingRecommendations(request);
        assertTrue(resp.isSuccessful());
        assertEquals("CARTON", resp.getPackagingRecommendationList().get(0).getPackagingType().getType());
        assertEquals("Normal", resp.getPackagingRecommendationList().get(0).getPackagingMode());
        assertEquals("CB07", resp.getPackagingRecommendationList().get(0).getPreferredPackagingTypeItem().getCode());
    }
    
    //boundary condition supc size equal to box size
    @Test
    public void testGetPackagingRecommendationsNormalAtSUPCLabelOldfeatureBoundaryCondition() throws PackagingRecomendationException, ExternalDataNotFoundException {
    
        List<String> labels = new ArrayList<String>();
        labels.add(Labels.Premium.toString());
        mExternalServiceFactory = mock(ExternalServiceFactory.class);
        ICAMSExternalService miCAMSExternalService = mock(ICAMSExternalService.class);
        GetPOGDetailListResponse response = new GetPOGDetailListResponse();
        POGDetailSRO pogDetailSROs = new POGDetailSRO();
        pogDetailSROs.setEnabled(true);
    
        List<POGDetailSRO> lpogDetailSROs = new ArrayList<POGDetailSRO>();
        lpogDetailSROs.add(pogDetailSROs);
        response.setPogDetailSROs(lpogDetailSROs);
        when(mExternalServiceFactory.getCAMSExternalService()).thenReturn(miCAMSExternalService);
        when(miCAMSExternalService.getPOGDetailsBbySupc(Matchers.<GetPOGDetailListBySUPCListRequest> any())).thenReturn(response);
        ReflectionTestUtils.setField(mruleEngineService, "factory", mExternalServiceFactory);
        PackagableItemSRO packagableItemSRO = createPackagableItemSRO(labels);
        lPackagableItemSRO.add(packagableItemSRO);
        packageDetailSRO.setPackagableItemList(lPackagableItemSRO);
        genericMao = mock(IGenericMao.class);
        ruleList = new ArrayList<>();
        List<CriteriaUnit> lCriteriaUnit = new ArrayList<CriteriaUnit>();
        CriteriaUnit criteriaUnit = createCriteriaUnit(1,"","");
        lCriteriaUnit.add(criteriaUnit);
        PackmanRuleUnit packmanRuleUnit = createPackmanRuleUnit(2,"ru03",7,"CARTON", lCriteriaUnit);
        packmanRuleUnit.setCriteria(lCriteriaUnit);
        ruleList.add(packmanRuleUnit);
        doAnswer(new Answer<List<PackmanRuleUnit>>() {
            public List<PackmanRuleUnit> answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                CocofsMongoQuery cocofsMongoQuery = (CocofsMongoQuery) args[0];
                List<Criteria> lCriteria = cocofsMongoQuery.getCriteria();
                Criteria criteria1 = lCriteria.get(0);
                String rule_type = (String) criteria1.getObj();
                if (rule_type.equals("store-packaging"))
                    return ruleList;
                return null;
    
            }
    
        }).when(genericMao).getDocumentList(Matchers.<CocofsMongoQuery> any(), Matchers.<Class<PackmanRuleUnit>> any());
        ReflectionTestUtils.setField(mruleEngineService, "genericMao", genericMao);
    
        PackmanPackagingRuleEngineServiceImpl packmanPackagingRuleEngineServiceImpl = Mockito.spy(new PackmanPackagingRuleEngineServiceImpl());
        Map<String, Object> ruleParam = new HashMap<String, Object>();
        CatageoryEvaluationParamDto paramDTO = new CatageoryEvaluationParamDto();
        SlabMappingCache mslabMappingCache = new SlabMappingCache();
        Map<String, NavigableMap<Double, SlabCacheDTO>> slabCache = new HashMap<String, NavigableMap<Double, SlabCacheDTO>>();
        List<SlabItemDTO> boxes = new ArrayList<SlabItemDTO>();
 
        SlabItemDTO slabItemDTO = createSlabItemDto("CB101",10.0,10.0,10.0,100.00,"CB101");
        boxes.add(slabItemDTO);
    
        slabItemDTO = new SlabItemDTO();
        slabItemDTO = createSlabItemDto("CB103",10.0,51.0,2.0,102.0,"CB103");
        boxes.add(slabItemDTO);   
        slabItemDTO = createSlabItemDto("CB102",10.1,10.1,10.1,103.03,"CB102");
        boxes.add(slabItemDTO);    
        slabItemDTO = createSlabItemDto("CB18",11.43,11.43,11.43,149.33,"CB18");
        boxes.add(slabItemDTO);    
        List<SlabItemDTO> boxes5 = new ArrayList<SlabItemDTO>();    
        SlabItemDTO slabItemDTO5 = new SlabItemDTO();
        slabItemDTO = createSlabItemDto("CB105",50.0,35.0,10.0,1750.00,"CB105");
        boxes5.add(slabItemDTO5);

    
        List<SlabItemDTO> boxes2 = new ArrayList<SlabItemDTO>();    
        SlabItemDTO slabItemDTO2 = createSlabItemDto("CB25",32.0,22.0,13.0,915.2,"FOOT WEAR");
        boxes2.add(slabItemDTO2);               
        List<SlabItemDTO> boxes1 = new ArrayList<SlabItemDTO>();
        SlabItemDTO slabItemDTO1 = createSlabItemDto("CB07",38.1,25.4,15.24,1474.84,"cb07");
        boxes1.add(slabItemDTO1);      
        List<SlabItemDTO> boxes3 = new ArrayList<SlabItemDTO>();  
        SlabItemDTO slabItemDTO3 = createSlabItemDto("CB09",48.26,35.4,15.24,2615.38,"cb09");
        boxes3.add(slabItemDTO3);  
        slabItemDTO3 = createSlabItemDto("CB11",55.88,45.72,10.16,4090.21,"CB11");
        boxes3.add(slabItemDTO3);    
        List<SlabItemDTO> boxes4 = new ArrayList<SlabItemDTO>();  
        SlabItemDTO slabItemDTO4 = createSlabItemDto("CB16",63.5,30.48,48.26,7846.13,"cb16");
        boxes4.add(slabItemDTO4);

        SlabCacheDTO slabCacheDTO = createSlabCacheDTO(boxes,0,500);   
        SlabCacheDTO slabCacheDTO2 = createSlabCacheDTO(boxes2,500,500);    
        SlabCacheDTO slabCacheDTO1 = createSlabCacheDTO(boxes1,1000,500);        
        SlabCacheDTO slabCacheDTO5 = createSlabCacheDTO(boxes5,1500,500);   
        SlabCacheDTO slabCacheDTO3 = createSlabCacheDTO(boxes3,2500,500);
        SlabCacheDTO slabCacheDTO4 = createSlabCacheDTO(boxes4,7000,500);
    
        NavigableMap<Double, SlabCacheDTO> navigableMap = new ConcurrentSkipListMap<Double, SlabCacheDTO>();
        navigableMap.put(0.0, slabCacheDTO);
        navigableMap.put(500.0, slabCacheDTO2);
        navigableMap.put(1000.0, slabCacheDTO1);
        navigableMap.put(1500.0, slabCacheDTO5);
        navigableMap.put(2500.0, slabCacheDTO3);
        navigableMap.put(7000.0, slabCacheDTO4);
    
        slabCache.put("Snapdeal-CARTON", navigableMap);
    
        Map<String, List<PackagingTypeCacheDTO>> mtypeToPackagingTypeDTOsMap = new HashMap<String, List<PackagingTypeCacheDTO>>();
        PackagingTypeCache packagingTypeCache = new PackagingTypeCache();
        List<PackagingTypeCacheDTO> lPackagingTypeCacheDTO = new ArrayList<PackagingTypeCacheDTO>();
    
        PackagingTypeCacheDTO packagingTypeCacheDTO = createPackagingTypeCacheDTO("Snapdeal","CARTON","Normal");
        lPackagingTypeCacheDTO.add(packagingTypeCacheDTO);
        mtypeToPackagingTypeDTOsMap.put("CARTON", lPackagingTypeCacheDTO);
    
        PackagingType packagingType = createPackagingType("Snapdeal","Normal");
        pauSRO = createPauSro("990001",10.00,10.00,10.00,"CARTON");
        List<PackagingType> lpackagingList = new ArrayList<PackagingType>();
        lpackagingList.add(packagingType);
        packagingTypeCache.addPackagingType(lpackagingList);
        CacheManager.getInstance().setCache(packagingTypeCache);
        when(mcocofsExternalService.getProductFulfilmentAttributes(Matchers.<GetProductFulfilmentAttributeRequest> any())).thenReturn(pauSRO);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(10.00, 10.00, 10.00)).thenReturn(100.00);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(40.01, 20.0, 9.98)).thenReturn(800.199);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(48.26, 15.24, 35.56)).thenReturn(2615.38);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(32.0, 20.0, 14.0)).thenReturn(896.0);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(32.0, 22.0, 13.0)).thenReturn(915.2);
        ReflectionTestUtils.setField(mPackagingRecommendationService, "cocofsExternalService", mcocofsExternalService);
    
        mSupcPackagingTypeMongoService = mock(ISupcPackagingTypeMongoService.class);
        when(mSupcPackagingTypeMongoService.getEnabledAndActivePackagingTypeBySupcAndStoreCode("Snapdeal", "990001")).thenReturn("CARTON");
        ReflectionTestUtils.setField(mruleEngineService, "supcPackagingTypeMongoService", mSupcPackagingTypeMongoService);
    
        ReflectionTestUtils.setField(packagingTypeCache, "typeToPackagingTypeDTOsMap", mtypeToPackagingTypeDTOsMap);
    
        GetPackagingRecommendationsRequest request = mock(GetPackagingRecommendationsRequest.class);
        ReflectionTestUtils.setField(packmanLandingService, "packagingRecomendationService", mPackagingRecommendationService);
        Protocol proto = Protocol.PROTOCOL_JSON;
        when(request.getRequestProtocol()).thenReturn(proto);
        when(request.getApiKey()).thenReturn("p@(km@n");
        when(request.getStoreFrontId()).thenReturn("SD");
        when(request.getIsShipTogether()).thenReturn(false);
        when(request.getPackageDetail()).thenReturn(packageDetailSRO);
    
        CacheManager.getInstance().setCache(mslabMappingCache);
    
        ReflectionTestUtils.setField(mslabMappingCache, "slabCache", slabCache);
        mruleService = mock(IRulesService.class);
        List<BlockParams> lblockParams = new ArrayList<BlockParams>();
        Block block = createBlock("StorePackagingRuleBlock");
        BlockParams blockParams = createBlockParams(block,"LABEL","Label");
        ruleParam.put("BRAND", "StorePackagingRuleBlock");
        Mockito.doReturn(ruleParam).when(packmanPackagingRuleEngineServiceImpl).populateRuleParamsFromBlockObject(Matchers.any(), (List<BlockParams>) Matchers.any());
        lblockParams.add(blockParams);
        mexclusiveService = mock(PackmanExclusiveServiceImpl.class);
        when(mruleService.getBlockParamsByBlockName("StorePackagingRuleBlock")).thenReturn(lblockParams);
        ReflectionTestUtils.setField(mruleEngineService, "ruleService", mruleService);
        ReflectionTestUtils.setField(mruleEngineService, "exclusiveService", mexclusiveService);
        mDepedencyForDTO = mock(com.snapdeal.packman.dto.SlabCacheDTO.IDependency.class);
    
        when(mDepedencyForDTO.getBooleanScalar(Property.USE_PRIMARY_LBH)).thenReturn(false);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_LBH)).thenReturn(false);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_DELTA_VOL_WT_MULT_FACTOR)).thenReturn(2.0);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_LBH_MARGIN)).thenReturn(0.0);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_DELTA_VOL_WT)).thenReturn(false);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_DELTA_VOL_WT_OFFSET)).thenReturn(1000.0);
    
        ReflectionTestUtils.setField(slabCacheDTO, "dep", mDepedencyForDTO);
        ReflectionTestUtils.setField(slabCacheDTO1, "dep", mDepedencyForDTO);
        ReflectionTestUtils.setField(slabCacheDTO2, "dep", mDepedencyForDTO);
        ReflectionTestUtils.setField(slabCacheDTO3, "dep", mDepedencyForDTO);
        ReflectionTestUtils.setField(slabCacheDTO4, "dep", mDepedencyForDTO);
        ReflectionTestUtils.setField(slabCacheDTO5, "dep", mDepedencyForDTO);
        GetPackagingRecommendationsResponse resp = packmanLandingService.getPackagingRecommendations(request);
        assertTrue(resp.isSuccessful());
        assertEquals("CARTON", resp.getPackagingRecommendationList().get(0).getPackagingType().getType());
        assertEquals("Normal", resp.getPackagingRecommendationList().get(0).getPackagingMode());
        assertEquals("CB101", resp.getPackagingRecommendationList().get(0).getPreferredPackagingTypeItem().getCode());
    }
    
    //test 7
    @Test
    public void testGetPackagingRecommendationsPicknpackAtSupcLabel() throws PackagingRecomendationException, ExternalDataNotFoundException {
    
        List<String> labels = new ArrayList<String>();
        labels.add(Labels.Premium.toString());
        mExternalServiceFactory = mock(ExternalServiceFactory.class);
        ICAMSExternalService miCAMSExternalService = mock(ICAMSExternalService.class);
        GetPOGDetailListResponse response = new GetPOGDetailListResponse();
        POGDetailSRO pogDetailSROs = new POGDetailSRO();
        pogDetailSROs.setEnabled(true);
    
        List<POGDetailSRO> lpogDetailSROs = new ArrayList<POGDetailSRO>();
        lpogDetailSROs.add(pogDetailSROs);
        response.setPogDetailSROs(lpogDetailSROs);
        when(mExternalServiceFactory.getCAMSExternalService()).thenReturn(miCAMSExternalService);
        when(miCAMSExternalService.getPOGDetailsBbySupc(Matchers.<GetPOGDetailListBySUPCListRequest> any())).thenReturn(response);
        ReflectionTestUtils.setField(mruleEngineService, "factory", mExternalServiceFactory);
        PackagableItemSRO packagableItemSRO = createPackagableItemSRO(labels);
        lPackagableItemSRO.add(packagableItemSRO);
        packageDetailSRO.setPackagableItemList(lPackagableItemSRO);
        genericMao = mock(IGenericMao.class);
        ruleList = new ArrayList<>();
        List<CriteriaUnit> lCriteriaUnit = new ArrayList<CriteriaUnit>();
        CriteriaUnit criteriaUnit = createCriteriaUnit(1,"","");
        lCriteriaUnit.add(criteriaUnit);
        
        PackmanRuleUnit packmanRuleUnit = createPackmanRuleUnit(2,"ru03",7,"SURFACE_P", lCriteriaUnit);
        ruleList.add(packmanRuleUnit);
        doAnswer(new Answer<List<PackmanRuleUnit>>() {
            public List<PackmanRuleUnit> answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                CocofsMongoQuery cocofsMongoQuery = (CocofsMongoQuery) args[0];
                List<Criteria> lCriteria = cocofsMongoQuery.getCriteria();
                Criteria criteria1 = lCriteria.get(0);
                String rule_type = (String) criteria1.getObj();
                if (rule_type.equals("store-packaging"))
                    return ruleList;
                return null;
    
            }
    
        }).when(genericMao).getDocumentList(Matchers.<CocofsMongoQuery> any(), Matchers.<Class<PackmanRuleUnit>> any());
    
        ReflectionTestUtils.setField(mruleEngineService, "genericMao", genericMao);
    
        PackmanPackagingRuleEngineServiceImpl packmanPackagingRuleEngineServiceImpl = Mockito.spy(new PackmanPackagingRuleEngineServiceImpl());
        Map<String, Object> ruleParam = new HashMap<String, Object>();
    
        SlabMappingCache mslabMappingCache = new SlabMappingCache();
        Map<String, NavigableMap<Double, SlabCacheDTO>> slabCache = new HashMap<String, NavigableMap<Double, SlabCacheDTO>>();
        List<SlabItemDTO> boxes = new ArrayList<SlabItemDTO>();
        SlabItemDTO slabItemDTO = createSlabItemDto("CB101",10.0,10.0,10.0,100.00,"CB101");
        boxes.add(slabItemDTO);
    
        slabItemDTO = createSlabItemDto("CB02",40.0,10.0,10.0,400.00,"CB02");
        boxes.add(slabItemDTO);        
        SlabCacheDTO slabCacheDTO = createSlabCacheDTO(boxes,0,200);
        NavigableMap<Double, SlabCacheDTO> navigableMap = new ConcurrentSkipListMap<Double, SlabCacheDTO>();
        navigableMap.put(99.0, slabCacheDTO);
        slabCache.put("Snapdeal-SURFACE_P", navigableMap);
    
        Map<String, List<PackagingTypeCacheDTO>> mtypeToPackagingTypeDTOsMap = new HashMap<String, List<PackagingTypeCacheDTO>>();
        PackagingTypeCache packagingTypeCache = new PackagingTypeCache();
        List<PackagingTypeCacheDTO> lPackagingTypeCacheDTO = new ArrayList<PackagingTypeCacheDTO>();
        
        PackagingTypeCacheDTO packagingTypeCacheDTO = createPackagingTypeCacheDTO("Snapdeal","SURFACE_P","Picknpack");
        lPackagingTypeCacheDTO.add(packagingTypeCacheDTO);
        mtypeToPackagingTypeDTOsMap.put("SURFACE_P", lPackagingTypeCacheDTO);
    
        PackagingType packagingType = createPackagingType("Snapdeal","picknpack");
        pauSRO = createPauSro("990001",10.0,10.0,10.0,"CARTON");
        List<PackagingType> lpackagingList = new ArrayList<PackagingType>();
        lpackagingList.add(packagingType);
        packagingTypeCache.addPackagingType(lpackagingList);
        CacheManager.getInstance().setCache(packagingTypeCache);
        when(mcocofsExternalService.getProductFulfilmentAttributes(Matchers.<GetProductFulfilmentAttributeRequest> any())).thenReturn(pauSRO);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(10.0, 10.0, 10.0)).thenReturn(100.0);
        ReflectionTestUtils.setField(mPackagingRecommendationService, "cocofsExternalService", mcocofsExternalService);
    
        mSupcPackagingTypeMongoService = mock(ISupcPackagingTypeMongoService.class);
        when(mSupcPackagingTypeMongoService.getEnabledAndActivePackagingTypeBySupcAndStoreCode("Snapdeal", "990001")).thenReturn("SURFACE_P");
        ReflectionTestUtils.setField(mruleEngineService, "supcPackagingTypeMongoService", mSupcPackagingTypeMongoService);
    
        ReflectionTestUtils.setField(packagingTypeCache, "typeToPackagingTypeDTOsMap", mtypeToPackagingTypeDTOsMap);
    
        GetPackagingRecommendationsRequest request = mock(GetPackagingRecommendationsRequest.class);
        ReflectionTestUtils.setField(packmanLandingService, "packagingRecomendationService", mPackagingRecommendationService);
        Protocol proto = Protocol.PROTOCOL_JSON;
        when(request.getRequestProtocol()).thenReturn(proto);
        when(request.getApiKey()).thenReturn("p@(km@n");
        when(request.getStoreFrontId()).thenReturn("SD");
        when(request.getIsShipTogether()).thenReturn(false);
        when(request.getPackageDetail()).thenReturn(packageDetailSRO);
    
        CacheManager.getInstance().setCache(mslabMappingCache);
    
        ReflectionTestUtils.setField(mslabMappingCache, "slabCache", slabCache);
        mruleService = mock(IRulesService.class);
        List<BlockParams> lblockParams = new ArrayList<BlockParams>();
        Block block = createBlock("StorePackagingRuleBlock");
        BlockParams blockParams = createBlockParams(block,"LABEL","Label");
        ruleParam.put("BRAND", "StorePackagingRuleBlock");
        Mockito.doReturn(ruleParam).when(packmanPackagingRuleEngineServiceImpl).populateRuleParamsFromBlockObject(Matchers.any(), (List<BlockParams>) Matchers.any());
        lblockParams.add(blockParams);
        mexclusiveService = mock(PackmanExclusiveServiceImpl.class);
        when(mruleService.getBlockParamsByBlockName("StorePackagingRuleBlock")).thenReturn(lblockParams);
        ReflectionTestUtils.setField(mruleEngineService, "ruleService", mruleService);
        ReflectionTestUtils.setField(mruleEngineService, "exclusiveService", mexclusiveService);
    
        mDepedencyForDTO = mock(com.snapdeal.packman.dto.SlabCacheDTO.IDependency.class);
    
        when(mDepedencyForDTO.getBooleanScalar(Property.USE_PRIMARY_LBH)).thenReturn(false);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_LBH)).thenReturn(false);
    
        ReflectionTestUtils.setField(slabCacheDTO, "dep", mDepedencyForDTO);
        GetPackagingRecommendationsResponse resp = packmanLandingService.getPackagingRecommendations(request);
        assertTrue(resp.isSuccessful());
        assertEquals("SURFACE_P", resp.getPackagingRecommendationList().get(0).getPackagingType().getType());
        assertEquals("Picknpack", resp.getPackagingRecommendationList().get(0).getPackagingMode());
    }
    
    @Test
    public void testGetPackagingRecommendationsNormalAtSUPCLabelOldfeatureDeltafeatureSet5() throws PackagingRecomendationException, ExternalDataNotFoundException {
    
        List<String> labels = new ArrayList<String>();
        labels.add(Labels.Premium.toString());
        mExternalServiceFactory = mock(ExternalServiceFactory.class);
        ICAMSExternalService miCAMSExternalService = mock(ICAMSExternalService.class);
    
        GetPOGDetailListResponse response = new GetPOGDetailListResponse();
        POGDetailSRO pogDetailSROs = new POGDetailSRO();
        pogDetailSROs.setEnabled(true);
    
        List<POGDetailSRO> lpogDetailSROs = new ArrayList<POGDetailSRO>();
        lpogDetailSROs.add(pogDetailSROs);
        response.setPogDetailSROs(lpogDetailSROs);
        when(mExternalServiceFactory.getCAMSExternalService()).thenReturn(miCAMSExternalService);
        when(miCAMSExternalService.getPOGDetailsBbySupc(Matchers.<GetPOGDetailListBySUPCListRequest> any())).thenReturn(response);
        ReflectionTestUtils.setField(mruleEngineService, "factory", mExternalServiceFactory);
        PackagableItemSRO packagableItemSRO = createPackagableItemSRO(labels);
        lPackagableItemSRO.add(packagableItemSRO);
        packageDetailSRO.setPackagableItemList(lPackagableItemSRO);
        genericMao = mock(IGenericMao.class);
        ruleList = new ArrayList<>();
        List<CriteriaUnit> lCriteriaUnit = new ArrayList<CriteriaUnit>();
        CriteriaUnit criteriaUnit = createCriteriaUnit(1,"","");
        lCriteriaUnit.add(criteriaUnit);
        PackmanRuleUnit packmanRuleUnit = createPackmanRuleUnit(2,"ru03",7,"CARTON", lCriteriaUnit);

        packmanRuleUnit.setCriteria(lCriteriaUnit);
        ruleList.add(packmanRuleUnit);
        doAnswer(new Answer<List<PackmanRuleUnit>>() {
            public List<PackmanRuleUnit> answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                CocofsMongoQuery cocofsMongoQuery = (CocofsMongoQuery) args[0];
                List<Criteria> lCriteria = cocofsMongoQuery.getCriteria();
                Criteria criteria1 = lCriteria.get(0);
                String rule_type = (String) criteria1.getObj();
                if (rule_type.equals("store-packaging"))
                    return ruleList;
                return null;
    
            }
    
        }).when(genericMao).getDocumentList(Matchers.<CocofsMongoQuery> any(), Matchers.<Class<PackmanRuleUnit>> any());
        ReflectionTestUtils.setField(mruleEngineService, "genericMao", genericMao);
    
        PackmanPackagingRuleEngineServiceImpl packmanPackagingRuleEngineServiceImpl = Mockito.spy(new PackmanPackagingRuleEngineServiceImpl());
        Map<String, Object> ruleParam = new HashMap<String, Object>();
        SlabMappingCache mslabMappingCache = new SlabMappingCache();
        Map<String, NavigableMap<Double, SlabCacheDTO>> slabCache = new HashMap<String, NavigableMap<Double, SlabCacheDTO>>();
        SlabCacheDTO slabCacheDTO = new SlabCacheDTO();
        List<SlabItemDTO> boxes = new ArrayList<SlabItemDTO>();
        
        SlabItemDTO slabItemDTO = createSlabItemDto("CB101",10.0,10.0,10.0,100.00,"CB101");
        boxes.add(slabItemDTO);    
        slabItemDTO = createSlabItemDto("CB103",10.0,51.0,2.0,102.0,"CB103");
        boxes.add(slabItemDTO);    
        slabItemDTO = createSlabItemDto("CB102",10.1,10.1,10.1,103.03,"CB102");
        boxes.add(slabItemDTO);    
        slabItemDTO = createSlabItemDto("CB18",11.43,11.43,11.43,149.33,"CB18");
        boxes.add(slabItemDTO);
    
        slabCacheDTO = createSlabCacheDTO(boxes,0,500);
        NavigableMap<Double, SlabCacheDTO> navigableMap = new ConcurrentSkipListMap<Double, SlabCacheDTO>();
        navigableMap.put(99.0, slabCacheDTO);
        slabCache.put("Snapdeal-CARTON", navigableMap);
    
        Map<String, List<PackagingTypeCacheDTO>> mtypeToPackagingTypeDTOsMap = new HashMap<String, List<PackagingTypeCacheDTO>>();
        PackagingTypeCache packagingTypeCache = new PackagingTypeCache();
        List<PackagingTypeCacheDTO> lPackagingTypeCacheDTO = new ArrayList<PackagingTypeCacheDTO>();
    
        PackagingTypeCacheDTO packagingTypeCacheDTO = createPackagingTypeCacheDTO("Snapdeal","CARTON","Normal");
        lPackagingTypeCacheDTO.add(packagingTypeCacheDTO);
        mtypeToPackagingTypeDTOsMap.put("CARTON", lPackagingTypeCacheDTO);
    
        PackagingType packagingType = createPackagingType("Snapdeal","Normal");
        pauSRO = createPauSro("990001",10.0,10.0,10.0,"CARTON");
        List<PackagingType> lpackagingList = new ArrayList<PackagingType>();
        lpackagingList.add(packagingType);
        packagingTypeCache.addPackagingType(lpackagingList);
        CacheManager.getInstance().setCache(packagingTypeCache);
        when(mcocofsExternalService.getProductFulfilmentAttributes(Matchers.<GetProductFulfilmentAttributeRequest> any())).thenReturn(pauSRO);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(10.0, 10.0, 10.0)).thenReturn(100.0);
        ReflectionTestUtils.setField(mPackagingRecommendationService, "cocofsExternalService", mcocofsExternalService);
    
        mSupcPackagingTypeMongoService = mock(ISupcPackagingTypeMongoService.class);
        when(mSupcPackagingTypeMongoService.getEnabledAndActivePackagingTypeBySupcAndStoreCode("Snapdeal", "990001")).thenReturn("CARTON");
        ReflectionTestUtils.setField(mruleEngineService, "supcPackagingTypeMongoService", mSupcPackagingTypeMongoService);
    
        ReflectionTestUtils.setField(packagingTypeCache, "typeToPackagingTypeDTOsMap", mtypeToPackagingTypeDTOsMap);
    
        GetPackagingRecommendationsRequest request = mock(GetPackagingRecommendationsRequest.class);
        ReflectionTestUtils.setField(packmanLandingService, "packagingRecomendationService", mPackagingRecommendationService);
        Protocol proto = Protocol.PROTOCOL_JSON;
        when(request.getRequestProtocol()).thenReturn(proto);
        when(request.getApiKey()).thenReturn("p@(km@n");
        when(request.getStoreFrontId()).thenReturn("SD");
        when(request.getIsShipTogether()).thenReturn(false);
        when(request.getPackageDetail()).thenReturn(packageDetailSRO);
    
        CacheManager.getInstance().setCache(mslabMappingCache);
    
        ReflectionTestUtils.setField(mslabMappingCache, "slabCache", slabCache);
        mruleService = mock(IRulesService.class);
        List<BlockParams> lblockParams = new ArrayList<BlockParams>();
        Block block = createBlock("StorePackagingRuleBlock");
        BlockParams blockParams = createBlockParams(block,"LABEL","Label");
        ruleParam.put("BRAND", "StorePackagingRuleBlock");
        Mockito.doReturn(ruleParam).when(packmanPackagingRuleEngineServiceImpl).populateRuleParamsFromBlockObject(Matchers.any(), (List<BlockParams>) Matchers.any());
        lblockParams.add(blockParams);
        mexclusiveService = mock(PackmanExclusiveServiceImpl.class);
        when(mruleService.getBlockParamsByBlockName("StorePackagingRuleBlock")).thenReturn(lblockParams);
        ReflectionTestUtils.setField(mruleEngineService, "ruleService", mruleService);
        ReflectionTestUtils.setField(mruleEngineService, "exclusiveService", mexclusiveService);
        mDepedencyForDTO = mock(com.snapdeal.packman.dto.SlabCacheDTO.IDependency.class);
    
        when(mDepedencyForDTO.getBooleanScalar(Property.USE_PRIMARY_LBH)).thenReturn(false);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_LBH)).thenReturn(false);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_DELTA_VOL_WT_MULT_FACTOR)).thenReturn(2.0);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_LBH_MARGIN)).thenReturn(0.0);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_DELTA_VOL_WT)).thenReturn(true);
        when(mDepedencyForDTO.getDoubleScalar(Property.PACKMAN_RECO_DELTA_VOL_WT_OFFSET)).thenReturn(1000.0);
    
        ReflectionTestUtils.setField(slabCacheDTO, "dep", mDepedencyForDTO);
        GetPackagingRecommendationsResponse resp = packmanLandingService.getPackagingRecommendations(request);

        assertTrue(resp.isSuccessful());
        assertEquals("CARTON", resp.getPackagingRecommendationList().get(0).getPackagingType().getType());
        assertEquals("Normal", resp.getPackagingRecommendationList().get(0).getPackagingMode());
        assertEquals("CB101", resp.getPackagingRecommendationList().get(0).getPreferredPackagingTypeItem().getCode());
    }
    
    //test 8
    @Test
    public void testGetPackagingRecommendationsPicknpackAtStoreLabel() throws PackagingRecomendationException, ExternalDataNotFoundException {
        List<String> labels = new ArrayList<String>();
        labels.add(Labels.Premium.toString());
        mExternalServiceFactory = mock(ExternalServiceFactory.class);
        ICAMSExternalService miCAMSExternalService = mock(ICAMSExternalService.class);
        GetPOGDetailListResponse response = new GetPOGDetailListResponse();
        POGDetailSRO pogDetailSROs = new POGDetailSRO();
        pogDetailSROs.setEnabled(true);
    
        List<POGDetailSRO> lpogDetailSROs = new ArrayList<POGDetailSRO>();
        lpogDetailSROs.add(pogDetailSROs);
        response.setPogDetailSROs(lpogDetailSROs);
        when(mExternalServiceFactory.getCAMSExternalService()).thenReturn(miCAMSExternalService);
        when(miCAMSExternalService.getPOGDetailsBbySupc(Matchers.<GetPOGDetailListBySUPCListRequest> any())).thenReturn(response);
        ReflectionTestUtils.setField(mruleEngineService, "factory", mExternalServiceFactory);
        PackagableItemSRO packagableItemSRO = createPackagableItemSRO(labels);
        lPackagableItemSRO.add(packagableItemSRO);
        packageDetailSRO.setPackagableItemList(lPackagableItemSRO);
        genericMao = mock(IGenericMao.class);
        ruleList = new ArrayList<>();
        List<CriteriaUnit> lCriteriaUnit = new ArrayList<CriteriaUnit>();
        
        PackmanRuleUnit packmanRuleUnit = createPackmanRuleUnit(2,"ru03",7,"SURFACE_P", lCriteriaUnit);

        ruleList.add(packmanRuleUnit);
        doAnswer(new Answer<List<PackmanRuleUnit>>() {
            public List<PackmanRuleUnit> answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
    
                CocofsMongoQuery cocofsMongoQuery = (CocofsMongoQuery) args[0];
                List<Criteria> lCriteria = cocofsMongoQuery.getCriteria();
                Criteria criteria1 = lCriteria.get(0);
                String rule_type = (String) criteria1.getObj();
                if (rule_type.equals("store-packaging"))
                    return ruleList;
                return null;
    
            }
    
        }).when(genericMao).getDocumentList(Matchers.<CocofsMongoQuery> any(), Matchers.<Class<PackmanRuleUnit>> any());
    
        ReflectionTestUtils.setField(mruleEngineService, "genericMao", genericMao);
    
        PackmanPackagingRuleEngineServiceImpl packmanPackagingRuleEngineServiceImpl = Mockito.spy(new PackmanPackagingRuleEngineServiceImpl());
        Map<String, Object> ruleParam = new HashMap<String, Object>();
        SlabMappingCache mslabMappingCache = new SlabMappingCache();
        Map<String, NavigableMap<Double, SlabCacheDTO>> slabCache = new HashMap<String, NavigableMap<Double, SlabCacheDTO>>();
        SlabCacheDTO slabCacheDTO = new SlabCacheDTO();
        List<SlabItemDTO> boxes = new ArrayList<SlabItemDTO>();
        SlabItemDTO slabItemDTO = createSlabItemDto("CB01",20.0,10.0,10.0,200.00,"BOX 1");
        boxes.add(slabItemDTO);
    
        slabItemDTO = createSlabItemDto("CB02",40.0,10.0,10.0,400.0,"BOX 2");
        boxes.add(slabItemDTO);    
        slabCacheDTO = createSlabCacheDTO(boxes,0,200);

        NavigableMap<Double, SlabCacheDTO> navigableMap = new ConcurrentSkipListMap<Double, SlabCacheDTO>();
        navigableMap.put(99.0, slabCacheDTO);
        slabCache.put("Snapdeal-SURFACE_P", navigableMap);
    
        Map<String, List<PackagingTypeCacheDTO>> mtypeToPackagingTypeDTOsMap = new HashMap<String, List<PackagingTypeCacheDTO>>();
        PackagingTypeCache packagingTypeCache = new PackagingTypeCache();
        List<PackagingTypeCacheDTO> lPackagingTypeCacheDTO = new ArrayList<PackagingTypeCacheDTO>();
        
        PackagingTypeCacheDTO packagingTypeCacheDTO = createPackagingTypeCacheDTO("Snapdeal","SURFACE_P","Picknpack"); 
        lPackagingTypeCacheDTO.add(packagingTypeCacheDTO);
        mtypeToPackagingTypeDTOsMap.put("SURFACE_P", lPackagingTypeCacheDTO);
    
        PackagingType packagingType = createPackagingType("Snapdeal","picknpack");     
        pauSRO = createPauSro("990001",10.0,10.0,10.0,"CARTON");

        List<PackagingType> lpackagingList = new ArrayList<PackagingType>();
        lpackagingList.add(packagingType);
        packagingTypeCache.addPackagingType(lpackagingList);
        CacheManager.getInstance().setCache(packagingTypeCache);
        when(mcocofsExternalService.getProductFulfilmentAttributes(Matchers.<GetProductFulfilmentAttributeRequest> any())).thenReturn(pauSRO);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(10.0, 10.0, 10.0)).thenReturn(100.0);
        ReflectionTestUtils.setField(mPackagingRecommendationService, "cocofsExternalService", mcocofsExternalService);
    
        mSupcPackagingTypeMongoService = mock(ISupcPackagingTypeMongoService.class);
        when(mSupcPackagingTypeMongoService.getEnabledAndActivePackagingTypeBySupcAndStoreCode("Snapdeal", "990001")).thenReturn(null);
        ReflectionTestUtils.setField(mruleEngineService, "supcPackagingTypeMongoService", mSupcPackagingTypeMongoService);
    
        ReflectionTestUtils.setField(packagingTypeCache, "typeToPackagingTypeDTOsMap", mtypeToPackagingTypeDTOsMap);
    
        GetPackagingRecommendationsRequest request = mock(GetPackagingRecommendationsRequest.class);
        ReflectionTestUtils.setField(packmanLandingService, "packagingRecomendationService", mPackagingRecommendationService);
        Protocol proto = Protocol.PROTOCOL_JSON;
        when(request.getRequestProtocol()).thenReturn(proto);
        when(request.getApiKey()).thenReturn("p@(km@n");
        when(request.getStoreFrontId()).thenReturn("SD");
        when(request.getIsShipTogether()).thenReturn(false);
        when(request.getPackageDetail()).thenReturn(packageDetailSRO);
    
        CacheManager.getInstance().setCache(mslabMappingCache);
    
        ReflectionTestUtils.setField(mslabMappingCache, "slabCache", slabCache);
        mruleService = mock(IRulesService.class);
        List<BlockParams> lblockParams = new ArrayList<BlockParams>();
        Block block = createBlock("StorePackagingRuleBlock");
        BlockParams blockParams = createBlockParams(block,"LABEL","Label");
        ruleParam.put("BRAND", "StorePackagingRuleBlock");
        Mockito.doReturn(ruleParam).when(packmanPackagingRuleEngineServiceImpl).populateRuleParamsFromBlockObject(Matchers.any(), (List<BlockParams>) Matchers.any());
        lblockParams.add(blockParams);
        mexclusiveService = mock(PackmanExclusiveServiceImpl.class);
        when(mruleService.getBlockParamsByBlockName("StorePackagingRuleBlock")).thenReturn(lblockParams);
        ReflectionTestUtils.setField(mruleEngineService, "ruleService", mruleService);
        ReflectionTestUtils.setField(mruleEngineService, "exclusiveService", mexclusiveService);
        mDepedencyForDTO = mock(com.snapdeal.packman.dto.SlabCacheDTO.IDependency.class);
    
        when(mDepedencyForDTO.getBooleanScalar(Property.USE_PRIMARY_LBH)).thenReturn(false);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_LBH)).thenReturn(false);
    
        ReflectionTestUtils.setField(slabCacheDTO, "dep", mDepedencyForDTO);
        GetPackagingRecommendationsResponse resp = packmanLandingService.getPackagingRecommendations(request);
        assertTrue(resp.isSuccessful());
        assertEquals("SURFACE_P", resp.getPackagingRecommendationList().get(0).getPackagingType().getType());
        assertEquals("Picknpack", resp.getPackagingRecommendationList().get(0).getPackagingMode());
    }
    
    //test 9
    @Test
    public void testGetPackagingRecommendationsPicknpackAtCatOnlyLabelExist() throws PackagingRecomendationException, ExternalDataNotFoundException {
    
        List<String> labels = new ArrayList<String>();
        labels.add(Labels.Premium.toString());
        mExternalServiceFactory = mock(ExternalServiceFactory.class);
        ICAMSExternalService miCAMSExternalService = mock(ICAMSExternalService.class);
        GetPOGDetailListResponse response = new GetPOGDetailListResponse();
        POGDetailSRO pogDetailSROs = new POGDetailSRO();
        pogDetailSROs.setEnabled(true);
    
        List<POGDetailSRO> lpogDetailSROs = new ArrayList<POGDetailSRO>();
        lpogDetailSROs.add(pogDetailSROs);
        response.setPogDetailSROs(lpogDetailSROs);
        when(mExternalServiceFactory.getCAMSExternalService()).thenReturn(miCAMSExternalService);
        when(miCAMSExternalService.getPOGDetailsBbySupc(Matchers.<GetPOGDetailListBySUPCListRequest> any())).thenReturn(response);
        ReflectionTestUtils.setField(mruleEngineService, "factory", mExternalServiceFactory);
        PackagableItemSRO packagableItemSRO = createPackagableItemSRO(labels);
        lPackagableItemSRO.add(packagableItemSRO);
        packageDetailSRO.setPackagableItemList(lPackagableItemSRO);
        genericMao = mock(IGenericMao.class);
        ruleList = new ArrayList<>();
        List<CriteriaUnit> lCriteriaUnit = new ArrayList<CriteriaUnit>();
        CriteriaUnit criteriaUnit = createCriteriaUnit(1,"BRAND","puma");
        lCriteriaUnit.add(criteriaUnit);
        PackmanRuleUnit packmanRuleUnit = createPackmanRuleUnit(2,"ru03",7,"SURFACE_2", lCriteriaUnit);
        ruleList.add(packmanRuleUnit);
        doAnswer(new Answer<List<PackmanRuleUnit>>() {
            public List<PackmanRuleUnit> answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                CocofsMongoQuery cocofsMongoQuery = (CocofsMongoQuery) args[0];
                List<Criteria> lCriteria = cocofsMongoQuery.getCriteria();
                Criteria criteria1 = lCriteria.get(0);
                String rule_type = (String) criteria1.getObj();
                if (rule_type.equals("category-packaging"))
                    return ruleList;
                return null;
    
            }
    
        }).when(genericMao).getDocumentList(Matchers.<CocofsMongoQuery> any(), Matchers.<Class<PackmanRuleUnit>> any());
        ReflectionTestUtils.setField(mruleEngineService, "genericMao", genericMao);
    
        PackmanPackagingRuleEngineServiceImpl packmanPackagingRuleEngineServiceImpl = Mockito.spy(new PackmanPackagingRuleEngineServiceImpl());
        Map<String, Object> ruleParam = new HashMap<String, Object>();
        SlabMappingCache mslabMappingCache = new SlabMappingCache();
        Map<String, NavigableMap<Double, SlabCacheDTO>> slabCache = new HashMap<String, NavigableMap<Double, SlabCacheDTO>>();
        SlabCacheDTO slabCacheDTO = new SlabCacheDTO();
        List<SlabItemDTO> boxes = new ArrayList<SlabItemDTO>();
        SlabItemDTO slabItemDTO = createSlabItemDto("CB101",10.0,10.0,10.0,100.00,"CB101");
        boxes.add(slabItemDTO);
    
        slabItemDTO = createSlabItemDto("CB02",40.0,10.0,10.0,400.0,"BOX 2");
        boxes.add(slabItemDTO);    
        slabCacheDTO = createSlabCacheDTO(boxes,0,200);

        NavigableMap<Double, SlabCacheDTO> navigableMap = new ConcurrentSkipListMap<Double, SlabCacheDTO>();
        navigableMap.put(99.0, slabCacheDTO);
        slabCache.put("Snapdeal-SURFACE_2", navigableMap);
    
        Map<String, List<PackagingTypeCacheDTO>> mtypeToPackagingTypeDTOsMap = new HashMap<String, List<PackagingTypeCacheDTO>>();
        PackagingTypeCache packagingTypeCache = new PackagingTypeCache();
        List<PackagingTypeCacheDTO> lPackagingTypeCacheDTO = new ArrayList<PackagingTypeCacheDTO>();
        PackagingTypeCacheDTO packagingTypeCacheDTO = createPackagingTypeCacheDTO("Snapdeal","SURFACE_2","Picknpack");
        lPackagingTypeCacheDTO.add(packagingTypeCacheDTO);
    
        mtypeToPackagingTypeDTOsMap.put("SURFACE_2", lPackagingTypeCacheDTO);
    
        PackagingType packagingType = createPackagingType("Snapdeal","picknpack");
        pauSRO = createPauSro("990001",10.0,10.0,10.0,"CARTON");
        List<PackagingType> lpackagingList = new ArrayList<PackagingType>();
        lpackagingList.add(packagingType);
        packagingTypeCache.addPackagingType(lpackagingList);
        CacheManager.getInstance().setCache(packagingTypeCache);
        when(mcocofsExternalService.getProductFulfilmentAttributes(Matchers.<GetProductFulfilmentAttributeRequest> any())).thenReturn(pauSRO);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(10.0, 10.0, 10.0)).thenReturn(100.0);
        ReflectionTestUtils.setField(mPackagingRecommendationService, "cocofsExternalService", mcocofsExternalService);
    
        mSupcPackagingTypeMongoService = mock(ISupcPackagingTypeMongoService.class);
        when(mSupcPackagingTypeMongoService.getEnabledAndActivePackagingTypeBySupcAndStoreCode("Snapdeal", "990001")).thenReturn(null);
        ReflectionTestUtils.setField(mruleEngineService, "supcPackagingTypeMongoService", mSupcPackagingTypeMongoService);
    
        ReflectionTestUtils.setField(packagingTypeCache, "typeToPackagingTypeDTOsMap", mtypeToPackagingTypeDTOsMap);
    
        GetPackagingRecommendationsRequest request = mock(GetPackagingRecommendationsRequest.class);
        ReflectionTestUtils.setField(packmanLandingService, "packagingRecomendationService", mPackagingRecommendationService);
        Protocol proto = Protocol.PROTOCOL_JSON;
        when(request.getRequestProtocol()).thenReturn(proto);
        when(request.getApiKey()).thenReturn("p@(km@n");
        when(request.getStoreFrontId()).thenReturn("SD");
        when(request.getIsShipTogether()).thenReturn(false);
        when(request.getPackageDetail()).thenReturn(packageDetailSRO);
    
        CacheManager.getInstance().setCache(mslabMappingCache);
    
        ReflectionTestUtils.setField(mslabMappingCache, "slabCache", slabCache);
        mruleService = mock(IRulesService.class);
        List<BlockParams> lblockParams = new ArrayList<BlockParams>();
        Block block = createBlock("CategoryPackagingRuleBlock");
        BlockParams blockParams = createBlockParams(block,"BRAND","Brand");
        ruleParam.put("abc", "CategoryPackagingRuleBlock");
        Mockito.doReturn(ruleParam).when(packmanPackagingRuleEngineServiceImpl).populateRuleParamsFromBlockObject(Matchers.any(), (List<BlockParams>) Matchers.any());
        lblockParams.add(blockParams);
        mexclusiveService = mock(PackmanExclusiveServiceImpl.class);
        when(mruleService.getBlockParamsByBlockName("CategoryPackagingRuleBlock")).thenReturn(lblockParams);
        ReflectionTestUtils.setField(mruleEngineService, "ruleService", mruleService);
        ReflectionTestUtils.setField(mruleEngineService, "exclusiveService", mexclusiveService);
    
        mDepedencyForDTO = mock(com.snapdeal.packman.dto.SlabCacheDTO.IDependency.class);
    
        when(mDepedencyForDTO.getBooleanScalar(Property.USE_PRIMARY_LBH)).thenReturn(false);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_LBH)).thenReturn(false);    
        ReflectionTestUtils.setField(slabCacheDTO, "dep", mDepedencyForDTO);    
        GetPackagingRecommendationsResponse resp = packmanLandingService.getPackagingRecommendations(request);
        assertTrue(resp.isSuccessful());
        assertEquals("SURFACE_2", resp.getPackagingRecommendationList().get(0).getPackagingType().getType());
        assertEquals("Picknpack", resp.getPackagingRecommendationList().get(0).getPackagingMode());
    }
    
    //test 10
   @Test
    public void testGetPackagingRecommendationsPicknpackAtCatLabelAndBrandBothUnexist() throws PackagingRecomendationException, ExternalDataNotFoundException {
    
        List<String> labels = new ArrayList<String>();
        labels.add(Labels.Premium.toString());
        mExternalServiceFactory = mock(ExternalServiceFactory.class);
        ICAMSExternalService miCAMSExternalService = mock(ICAMSExternalService.class);
        GetPOGDetailListResponse response = new GetPOGDetailListResponse();
        POGDetailSRO pogDetailSROs = new POGDetailSRO();
        pogDetailSROs.setEnabled(true);
    
        List<POGDetailSRO> lpogDetailSROs = new ArrayList<POGDetailSRO>();
        lpogDetailSROs.add(pogDetailSROs);
        response.setPogDetailSROs(lpogDetailSROs);
        when(mExternalServiceFactory.getCAMSExternalService()).thenReturn(miCAMSExternalService);
        when(miCAMSExternalService.getPOGDetailsBbySupc(Matchers.<GetPOGDetailListBySUPCListRequest> any())).thenReturn(response);
        ReflectionTestUtils.setField(mruleEngineService, "factory", mExternalServiceFactory);
        PackagableItemSRO packagableItemSRO = createPackagableItemSRO(labels);
        lPackagableItemSRO.add(packagableItemSRO);
        packageDetailSRO.setPackagableItemList(lPackagableItemSRO);
        genericMao = mock(IGenericMao.class);
        ruleList = new ArrayList<>();
        List<CriteriaUnit> lCriteriaUnit = new ArrayList<CriteriaUnit>();
        PackmanRuleUnit packmanRuleUnit = createPackmanRuleUnit(2,"ru03",7,"SURFACE_S", lCriteriaUnit);
        ruleList.add(packmanRuleUnit);
        doAnswer(new Answer<List<PackmanRuleUnit>>() {
            public List<PackmanRuleUnit> answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                CocofsMongoQuery cocofsMongoQuery = (CocofsMongoQuery) args[0];
                List<Criteria> lCriteria = cocofsMongoQuery.getCriteria();
                Criteria criteria1 = lCriteria.get(0);
                String rule_type = (String) criteria1.getObj();
                if (rule_type.equals("category-packaging"))
                    return ruleList;
                return null;
    
            }
    
        }).when(genericMao).getDocumentList(Matchers.<CocofsMongoQuery> any(), Matchers.<Class<PackmanRuleUnit>> any());
    
        ReflectionTestUtils.setField(mruleEngineService, "genericMao", genericMao);
    
        PackmanPackagingRuleEngineServiceImpl packmanPackagingRuleEngineServiceImpl = Mockito.spy(new PackmanPackagingRuleEngineServiceImpl());
        Map<String, Object> ruleParam = new HashMap<String, Object>();
        SlabMappingCache mslabMappingCache = new SlabMappingCache();
        Map<String, NavigableMap<Double, SlabCacheDTO>> slabCache = new HashMap<String, NavigableMap<Double, SlabCacheDTO>>();
        SlabCacheDTO slabCacheDTO = new SlabCacheDTO();
        List<SlabItemDTO> boxes = new ArrayList<SlabItemDTO>();
        SlabItemDTO slabItemDTO = createSlabItemDto("CB101",10.0,10.0,10.0,100.00,"CB101");
        boxes.add(slabItemDTO);
    
        slabItemDTO = createSlabItemDto("CB02",40.0,10.0,10.0,400.0,"BOX 2");
        boxes.add(slabItemDTO);

        slabCacheDTO = createSlabCacheDTO(boxes,0,200);
        NavigableMap<Double, SlabCacheDTO> navigableMap = new ConcurrentSkipListMap<Double, SlabCacheDTO>();
        navigableMap.put(99.0, slabCacheDTO);
        slabCache.put("Snapdeal-SURFACE_S", navigableMap);
    
        Map<String, List<PackagingTypeCacheDTO>> mtypeToPackagingTypeDTOsMap = new HashMap<String, List<PackagingTypeCacheDTO>>();
        PackagingTypeCache packagingTypeCache = new PackagingTypeCache();
        List<PackagingTypeCacheDTO> lPackagingTypeCacheDTO = new ArrayList<PackagingTypeCacheDTO>();
        PackagingTypeCacheDTO packagingTypeCacheDTO = createPackagingTypeCacheDTO("Snapdeal","SURFACE_S","Picknpack");
        lPackagingTypeCacheDTO.add(packagingTypeCacheDTO);
    
        mtypeToPackagingTypeDTOsMap.put("SURFACE_S", lPackagingTypeCacheDTO);  
        PackagingType packagingType = createPackagingType("Snapdeal","picknpack");
    
        pauSRO = createPauSro("990001",10.0,10.0,10.0,"CARTON");
        List<PackagingType> lpackagingList = new ArrayList<PackagingType>();
        lpackagingList.add(packagingType);
        packagingTypeCache.addPackagingType(lpackagingList);
        CacheManager.getInstance().setCache(packagingTypeCache);
        when(mcocofsExternalService.getProductFulfilmentAttributes(Matchers.<GetProductFulfilmentAttributeRequest> any())).thenReturn(pauSRO);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(10.0, 10.0, 10.0)).thenReturn(100.0);
        ReflectionTestUtils.setField(mPackagingRecommendationService, "cocofsExternalService", mcocofsExternalService);
    
        mSupcPackagingTypeMongoService = mock(ISupcPackagingTypeMongoService.class);
        when(mSupcPackagingTypeMongoService.getEnabledAndActivePackagingTypeBySupcAndStoreCode("Snapdeal", "990001")).thenReturn(null);
        ReflectionTestUtils.setField(mruleEngineService, "supcPackagingTypeMongoService", mSupcPackagingTypeMongoService);
    
        ReflectionTestUtils.setField(packagingTypeCache, "typeToPackagingTypeDTOsMap", mtypeToPackagingTypeDTOsMap);
    
        GetPackagingRecommendationsRequest request = mock(GetPackagingRecommendationsRequest.class);
        ReflectionTestUtils.setField(packmanLandingService, "packagingRecomendationService", mPackagingRecommendationService);
        Protocol proto = Protocol.PROTOCOL_JSON;
        when(request.getRequestProtocol()).thenReturn(proto);
        when(request.getApiKey()).thenReturn("p@(km@n");
        when(request.getStoreFrontId()).thenReturn("SD");
        when(request.getIsShipTogether()).thenReturn(false);
        when(request.getPackageDetail()).thenReturn(packageDetailSRO);
    
        CacheManager.getInstance().setCache(mslabMappingCache);
    
        ReflectionTestUtils.setField(mslabMappingCache, "slabCache", slabCache);
        mruleService = mock(IRulesService.class);
        List<BlockParams> lblockParams = new ArrayList<BlockParams>();
        Block block = createBlock("CategoryPackagingRuleBlock");
        BlockParams blockParams = createBlockParams(block,"","");
        Mockito.doReturn(ruleParam).when(packmanPackagingRuleEngineServiceImpl).populateRuleParamsFromBlockObject(Matchers.any(), (List<BlockParams>) Matchers.any());
        lblockParams.add(blockParams);
        mexclusiveService = mock(PackmanExclusiveServiceImpl.class);
        when(mruleService.getBlockParamsByBlockName("CategoryPackagingRuleBlock")).thenReturn(lblockParams);
        ReflectionTestUtils.setField(mruleEngineService, "ruleService", mruleService);
        ReflectionTestUtils.setField(mruleEngineService, "exclusiveService", mexclusiveService);
    
        mDepedencyForDTO = mock(com.snapdeal.packman.dto.SlabCacheDTO.IDependency.class);
    
        when(mDepedencyForDTO.getBooleanScalar(Property.USE_PRIMARY_LBH)).thenReturn(false);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_LBH)).thenReturn(false);
    
        ReflectionTestUtils.setField(slabCacheDTO, "dep", mDepedencyForDTO);
    
        GetPackagingRecommendationsResponse resp = packmanLandingService.getPackagingRecommendations(request);

        assertEquals("SURFACE_S", resp.getPackagingRecommendationList().get(0).getPackagingType().getType());
        assertEquals("Picknpack", resp.getPackagingRecommendationList().get(0).getPackagingMode());
        
    }
    
    //test 11
    @Test
    public void testGetPackagingRecommendationsNormalAtCatOnlyLabelExist() throws PackagingRecomendationException, ExternalDataNotFoundException {
    
        List<String> labels = new ArrayList<String>();
        labels.add(Labels.Premium.toString());
        mExternalServiceFactory = mock(ExternalServiceFactory.class);
        ICAMSExternalService miCAMSExternalService = mock(ICAMSExternalService.class);
        GetPOGDetailListResponse response = new GetPOGDetailListResponse();
        POGDetailSRO pogDetailSROs = new POGDetailSRO();
        pogDetailSROs.setEnabled(true);
    
        List<POGDetailSRO> lpogDetailSROs = new ArrayList<POGDetailSRO>();
        lpogDetailSROs.add(pogDetailSROs);
        response.setPogDetailSROs(lpogDetailSROs);
        when(mExternalServiceFactory.getCAMSExternalService()).thenReturn(miCAMSExternalService);
        when(miCAMSExternalService.getPOGDetailsBbySupc(Matchers.<GetPOGDetailListBySUPCListRequest> any())).thenReturn(response);
        ReflectionTestUtils.setField(mruleEngineService, "factory", mExternalServiceFactory);
        PackagableItemSRO packagableItemSRO = createPackagableItemSRO(labels);
        lPackagableItemSRO.add(packagableItemSRO);
        packageDetailSRO.setPackagableItemList(lPackagableItemSRO);
        genericMao = mock(IGenericMao.class);
        ruleList = new ArrayList<>();
        List<CriteriaUnit> lCriteriaUnit = new ArrayList<CriteriaUnit>();
        CriteriaUnit criteriaUnit = createCriteriaUnit(1,"BRAND","puma");
        lCriteriaUnit.add(criteriaUnit);
        PackmanRuleUnit packmanRuleUnit = createPackmanRuleUnit(2,"ru03",7,"CARTON", lCriteriaUnit);
        ruleList.add(packmanRuleUnit);
        doAnswer(new Answer<List<PackmanRuleUnit>>() {
            public List<PackmanRuleUnit> answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                CocofsMongoQuery cocofsMongoQuery = (CocofsMongoQuery) args[0];
                List<Criteria> lCriteria = cocofsMongoQuery.getCriteria();
                Criteria criteria1 = lCriteria.get(0);
                String rule_type = (String) criteria1.getObj();
                if (rule_type.equals("category-packaging"))
                    return ruleList;
                return null;
    
            }
    
        }).when(genericMao).getDocumentList(Matchers.<CocofsMongoQuery> any(), Matchers.<Class<PackmanRuleUnit>> any());
    
        ReflectionTestUtils.setField(mruleEngineService, "genericMao", genericMao);
    
        PackmanPackagingRuleEngineServiceImpl packmanPackagingRuleEngineServiceImpl = Mockito.spy(new PackmanPackagingRuleEngineServiceImpl());
        Map<String, Object> ruleParam = new HashMap<String, Object>();
        SlabMappingCache mslabMappingCache = new SlabMappingCache();
        Map<String, NavigableMap<Double, SlabCacheDTO>> slabCache = new HashMap<String, NavigableMap<Double, SlabCacheDTO>>();
        SlabCacheDTO slabCacheDTO = new SlabCacheDTO();
        List<SlabItemDTO> boxes = new ArrayList<SlabItemDTO>();
        SlabItemDTO slabItemDTO = createSlabItemDto("CB101",10.0,10.0,10.0,100.00,"CB101");
        boxes.add(slabItemDTO);
    
        slabItemDTO = createSlabItemDto("CB02",40.0,10.0,10.0,400.0,"BOX 2");
        boxes.add(slabItemDTO);
    
        slabCacheDTO = createSlabCacheDTO(boxes,0,200);
        NavigableMap<Double, SlabCacheDTO> navigableMap = new ConcurrentSkipListMap<Double, SlabCacheDTO>();
        navigableMap.put(99.0, slabCacheDTO);
        slabCache.put("Snapdeal-CARTON", navigableMap);
    
        Map<String, List<PackagingTypeCacheDTO>> mtypeToPackagingTypeDTOsMap = new HashMap<String, List<PackagingTypeCacheDTO>>();
        PackagingTypeCache packagingTypeCache = new PackagingTypeCache();
        List<PackagingTypeCacheDTO> lPackagingTypeCacheDTO = new ArrayList<PackagingTypeCacheDTO>();
    
        PackagingTypeCacheDTO packagingTypeCacheDTO = createPackagingTypeCacheDTO("Snapdeal","CARTON","Normal");
        lPackagingTypeCacheDTO.add(packagingTypeCacheDTO);
        mtypeToPackagingTypeDTOsMap.put("CARTON", lPackagingTypeCacheDTO);
    
        PackagingType packagingType = createPackagingType("Snapdeal","Normal");
        pauSRO = createPauSro("990001",10.0,10.0,10.0,"CARTON");
        List<PackagingType> lpackagingList = new ArrayList<PackagingType>();
        lpackagingList.add(packagingType);
        packagingTypeCache.addPackagingType(lpackagingList);
        CacheManager.getInstance().setCache(packagingTypeCache);
        when(mcocofsExternalService.getProductFulfilmentAttributes(Matchers.<GetProductFulfilmentAttributeRequest> any())).thenReturn(pauSRO);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(10.0, 10.0, 10.0)).thenReturn(100.0);
        ReflectionTestUtils.setField(mPackagingRecommendationService, "cocofsExternalService", mcocofsExternalService);
    
        mSupcPackagingTypeMongoService = mock(ISupcPackagingTypeMongoService.class);
        when(mSupcPackagingTypeMongoService.getEnabledAndActivePackagingTypeBySupcAndStoreCode("Snapdeal", "990001")).thenReturn(null);
        ReflectionTestUtils.setField(mruleEngineService, "supcPackagingTypeMongoService", mSupcPackagingTypeMongoService);
    
        ReflectionTestUtils.setField(packagingTypeCache, "typeToPackagingTypeDTOsMap", mtypeToPackagingTypeDTOsMap);
    
        GetPackagingRecommendationsRequest request = mock(GetPackagingRecommendationsRequest.class);
        ReflectionTestUtils.setField(packmanLandingService, "packagingRecomendationService", mPackagingRecommendationService);
        Protocol proto = Protocol.PROTOCOL_JSON;
        when(request.getRequestProtocol()).thenReturn(proto);
        when(request.getApiKey()).thenReturn("p@(km@n");
        when(request.getStoreFrontId()).thenReturn("SD");
        when(request.getIsShipTogether()).thenReturn(false);
        when(request.getPackageDetail()).thenReturn(packageDetailSRO);
    
        CacheManager.getInstance().setCache(mslabMappingCache);
    
        ReflectionTestUtils.setField(mslabMappingCache, "slabCache", slabCache);
        mruleService = mock(IRulesService.class);
        List<BlockParams> lblockParams = new ArrayList<BlockParams>();
        Block block = createBlock("CategoryPackagingRuleBlock");
        BlockParams blockParams = createBlockParams(block,"BRAND","Brand");
        ruleParam.put("abc", "CategoryPackagingRuleBlock");
        Mockito.doReturn(ruleParam).when(packmanPackagingRuleEngineServiceImpl).populateRuleParamsFromBlockObject(Matchers.any(), (List<BlockParams>) Matchers.any());
        blockParams.setId(5);
        lblockParams.add(blockParams);
        mexclusiveService = mock(PackmanExclusiveServiceImpl.class);
        when(mruleService.getBlockParamsByBlockName("CategoryPackagingRuleBlock")).thenReturn(lblockParams);
        ReflectionTestUtils.setField(mruleEngineService, "ruleService", mruleService);
        ReflectionTestUtils.setField(mruleEngineService, "exclusiveService", mexclusiveService);
    
        mDepedencyForDTO = mock(com.snapdeal.packman.dto.SlabCacheDTO.IDependency.class);
    
        when(mDepedencyForDTO.getBooleanScalar(Property.USE_PRIMARY_LBH)).thenReturn(false);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_LBH)).thenReturn(false);
    
        ReflectionTestUtils.setField(slabCacheDTO, "dep", mDepedencyForDTO);   
        GetPackagingRecommendationsResponse resp = packmanLandingService.getPackagingRecommendations(request);
        assertTrue(resp.isSuccessful());
        assertEquals("CARTON", resp.getPackagingRecommendationList().get(0).getPackagingType().getType());
        assertEquals("Normal", resp.getPackagingRecommendationList().get(0).getPackagingMode());
        assertEquals("CB101", resp.getPackagingRecommendationList().get(0).getPreferredPackagingTypeItem().getCode());
    }
    
    //test 13
    @Test
    public void testGetPackagingRecommendationsPicknpackAtCatLabelAndBrandBothExist() throws PackagingRecomendationException, ExternalDataNotFoundException {
    
        List<String> labels = new ArrayList<String>();
        labels.add(Labels.Premium.toString());
        mExternalServiceFactory = mock(ExternalServiceFactory.class);
        ICAMSExternalService miCAMSExternalService = mock(ICAMSExternalService.class);
        GetPOGDetailListResponse response = new GetPOGDetailListResponse();
        POGDetailSRO pogDetailSROs = new POGDetailSRO();
        pogDetailSROs.setEnabled(true);
    
        List<POGDetailSRO> lpogDetailSROs = new ArrayList<POGDetailSRO>();
        lpogDetailSROs.add(pogDetailSROs);
        response.setPogDetailSROs(lpogDetailSROs);
        when(mExternalServiceFactory.getCAMSExternalService()).thenReturn(miCAMSExternalService);
        when(miCAMSExternalService.getPOGDetailsBbySupc(Matchers.<GetPOGDetailListBySUPCListRequest> any())).thenReturn(response);
        ReflectionTestUtils.setField(mruleEngineService, "factory", mExternalServiceFactory);
        PackagableItemSRO packagableItemSRO = createPackagableItemSRO(labels);
        lPackagableItemSRO.add(packagableItemSRO);
        packageDetailSRO.setPackagableItemList(lPackagableItemSRO);
        genericMao = mock(IGenericMao.class);
        ruleList = new ArrayList<>();
        List<CriteriaUnit> lCriteriaUnit = new ArrayList<CriteriaUnit>();
        CriteriaUnit criteriaUnit = createCriteriaUnit(1,"BRAND","puma");
        lCriteriaUnit.add(criteriaUnit);
        PackmanRuleUnit packmanRuleUnit = createPackmanRuleUnit(2,"ru03",7,"SURFACE_B", lCriteriaUnit);
        ruleList.add(packmanRuleUnit);
        doAnswer(new Answer<List<PackmanRuleUnit>>() {
            public List<PackmanRuleUnit> answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                CocofsMongoQuery cocofsMongoQuery = (CocofsMongoQuery) args[0];
                List<Criteria> lCriteria = cocofsMongoQuery.getCriteria();
                Criteria criteria1 = lCriteria.get(0);
                String rule_type = (String) criteria1.getObj();
                if (rule_type.equals("category-packaging"))
                    return ruleList;
                return null;
    
            }
    
        }).when(genericMao).getDocumentList(Matchers.<CocofsMongoQuery> any(), Matchers.<Class<PackmanRuleUnit>> any());
    
        ReflectionTestUtils.setField(mruleEngineService, "genericMao", genericMao);
    
        PackmanPackagingRuleEngineServiceImpl packmanPackagingRuleEngineServiceImpl = Mockito.spy(new PackmanPackagingRuleEngineServiceImpl());
        Map<String, Object> ruleParam = new HashMap<String, Object>();
        CatageoryEvaluationParamDto paramDTO = new CatageoryEvaluationParamDto();
        paramDTO.setLabel(labels);
        paramDTO.setBrand("puma");
    
        SlabMappingCache mslabMappingCache = new SlabMappingCache();
        Map<String, NavigableMap<Double, SlabCacheDTO>> slabCache = new HashMap<String, NavigableMap<Double, SlabCacheDTO>>();
        SlabCacheDTO slabCacheDTO = new SlabCacheDTO();
        List<SlabItemDTO> boxes = new ArrayList<SlabItemDTO>();
        SlabItemDTO slabItemDTO = createSlabItemDto("CB101",10.0,10.0,10.0,100.00,"CB101");
        boxes.add(slabItemDTO);
    
        slabItemDTO = createSlabItemDto("CB02",40.0,10.0,10.0,400.0,"BOX 2");
        boxes.add(slabItemDTO);
    
        slabCacheDTO = createSlabCacheDTO(boxes,0,200);
        NavigableMap<Double, SlabCacheDTO> navigableMap = new ConcurrentSkipListMap<Double, SlabCacheDTO>();
        navigableMap.put(99.0, slabCacheDTO);
        slabCache.put("Snapdeal-SURFACE_B", navigableMap);
    
        Map<String, List<PackagingTypeCacheDTO>> mtypeToPackagingTypeDTOsMap = new HashMap<String, List<PackagingTypeCacheDTO>>();
        PackagingTypeCache packagingTypeCache = new PackagingTypeCache();
        List<PackagingTypeCacheDTO> lPackagingTypeCacheDTO = new ArrayList<PackagingTypeCacheDTO>();
        PackagingTypeCacheDTO packagingTypeCacheDTO = createPackagingTypeCacheDTO("Snapdeal","SURFACE_B","Picknpack");
        lPackagingTypeCacheDTO.add(packagingTypeCacheDTO);    
        mtypeToPackagingTypeDTOsMap.put("SURFACE_B", lPackagingTypeCacheDTO);
    
        PackagingType packagingType = createPackagingType("Snapdeal","picknpack");
    
        pauSRO = createPauSro("990001",10.0,10.0,10.0,"CARTON");
        List<PackagingType> lpackagingList = new ArrayList<PackagingType>();
        lpackagingList.add(packagingType);
        packagingTypeCache.addPackagingType(lpackagingList);
        CacheManager.getInstance().setCache(packagingTypeCache);
        when(mcocofsExternalService.getProductFulfilmentAttributes(Matchers.<GetProductFulfilmentAttributeRequest> any())).thenReturn(pauSRO);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(10.0, 10.0, 10.0)).thenReturn(100.0);
        ReflectionTestUtils.setField(mPackagingRecommendationService, "cocofsExternalService", mcocofsExternalService);
    
        mSupcPackagingTypeMongoService = mock(ISupcPackagingTypeMongoService.class);
        when(mSupcPackagingTypeMongoService.getEnabledAndActivePackagingTypeBySupcAndStoreCode("Snapdeal", "990001")).thenReturn(null);
        ReflectionTestUtils.setField(mruleEngineService, "supcPackagingTypeMongoService", mSupcPackagingTypeMongoService);
    
        ReflectionTestUtils.setField(packagingTypeCache, "typeToPackagingTypeDTOsMap", mtypeToPackagingTypeDTOsMap);
    
        GetPackagingRecommendationsRequest request = mock(GetPackagingRecommendationsRequest.class);
        ReflectionTestUtils.setField(packmanLandingService, "packagingRecomendationService", mPackagingRecommendationService);
        Protocol proto = Protocol.PROTOCOL_JSON;
        when(request.getRequestProtocol()).thenReturn(proto);
        when(request.getApiKey()).thenReturn("p@(km@n");
        when(request.getStoreFrontId()).thenReturn("SD");
        when(request.getIsShipTogether()).thenReturn(false);
        when(request.getPackageDetail()).thenReturn(packageDetailSRO);
    
        CacheManager.getInstance().setCache(mslabMappingCache);
    
        ReflectionTestUtils.setField(mslabMappingCache, "slabCache", slabCache);
        mruleService = mock(IRulesService.class);
        List<BlockParams> lblockParams = new ArrayList<BlockParams>();
        Block block = createBlock("CategoryPackagingRuleBlock");
        BlockParams blockParams = createBlockParams(block,"LABEL","Label");
        ruleParam.put("abc", "CategoryPackagingRuleBlock");
        Mockito.doReturn(ruleParam).when(packmanPackagingRuleEngineServiceImpl).populateRuleParamsFromBlockObject(Matchers.any(), (List<BlockParams>) Matchers.any());
        lblockParams.add(blockParams);
        mexclusiveService = mock(PackmanExclusiveServiceImpl.class);
        when(mruleService.getBlockParamsByBlockName("CategoryPackagingRuleBlock")).thenReturn(lblockParams);
        ReflectionTestUtils.setField(mruleEngineService, "ruleService", mruleService);
        ReflectionTestUtils.setField(mruleEngineService, "exclusiveService", mexclusiveService);
    
        mDepedencyForDTO = mock(com.snapdeal.packman.dto.SlabCacheDTO.IDependency.class);
    
        when(mDepedencyForDTO.getBooleanScalar(Property.USE_PRIMARY_LBH)).thenReturn(false);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_LBH)).thenReturn(false);  
        ReflectionTestUtils.setField(slabCacheDTO, "dep", mDepedencyForDTO);
        GetPackagingRecommendationsResponse resp = packmanLandingService.getPackagingRecommendations(request);

        assertTrue(resp.isSuccessful());
        assertEquals("SURFACE_B", resp.getPackagingRecommendationList().get(0).getPackagingType().getType());
        assertEquals("Picknpack", resp.getPackagingRecommendationList().get(0).getPackagingMode());
    }
    
    //test 14
    @Test
    public void testGetPackagingRecommendationsPicknpackAtSubcatOnlyLabelsExist() throws PackagingRecomendationException, ExternalDataNotFoundException {
    
        List<String> labels = new ArrayList<String>();
        labels.add(Labels.Premium.toString());
        mExternalServiceFactory = mock(ExternalServiceFactory.class);
        ICAMSExternalService miCAMSExternalService = mock(ICAMSExternalService.class);
        GetPOGDetailListResponse response = new GetPOGDetailListResponse();
        POGDetailSRO pogDetailSROs = new POGDetailSRO();
        pogDetailSROs.setEnabled(true);
    
        List<POGDetailSRO> lpogDetailSROs = new ArrayList<POGDetailSRO>();
        lpogDetailSROs.add(pogDetailSROs);
        response.setPogDetailSROs(lpogDetailSROs);
        when(mExternalServiceFactory.getCAMSExternalService()).thenReturn(miCAMSExternalService);
        when(miCAMSExternalService.getPOGDetailsBbySupc(Matchers.<GetPOGDetailListBySUPCListRequest> any())).thenReturn(response);
        ReflectionTestUtils.setField(mruleEngineService, "factory", mExternalServiceFactory);
        PackagableItemSRO packagableItemSRO = createPackagableItemSRO(labels);
        lPackagableItemSRO.add(packagableItemSRO);
        packageDetailSRO.setPackagableItemList(lPackagableItemSRO);
        genericMao = mock(IGenericMao.class);
        ruleList = new ArrayList<>();
        List<CriteriaUnit> lCriteriaUnit = new ArrayList<CriteriaUnit>();
        CriteriaUnit criteriaUnit = createCriteriaUnit(1,"LABELS","premium");
        lCriteriaUnit.add(criteriaUnit);
        PackmanRuleUnit packmanRuleUnit = createPackmanRuleUnit(2,"ru02",7,"SURFACE_D", lCriteriaUnit);
        ruleList.add(packmanRuleUnit);
        when(genericMao.getDocumentList(Matchers.<CocofsMongoQuery> any(), Matchers.<Class<PackmanRuleUnit>> any())).thenReturn(ruleList);
        ReflectionTestUtils.setField(mruleEngineService, "genericMao", genericMao);
    
        SlabMappingCache mslabMappingCache = new SlabMappingCache();
        Map<String, NavigableMap<Double, SlabCacheDTO>> slabCache = new HashMap<String, NavigableMap<Double, SlabCacheDTO>>();
        SlabCacheDTO slabCacheDTO = new SlabCacheDTO();
        List<SlabItemDTO> boxes = new ArrayList<SlabItemDTO>();
        SlabItemDTO slabItemDTO = createSlabItemDto("CB101",10.0,10.0,10.0,100.00,"CB101");
        boxes.add(slabItemDTO);
    
        slabItemDTO = createSlabItemDto("CB02",40.0,10.0,10.0,400.0,"BOX 2");
        boxes.add(slabItemDTO);
    
        slabCacheDTO = createSlabCacheDTO(boxes,0,200);
        NavigableMap<Double, SlabCacheDTO> navigableMap = new ConcurrentSkipListMap<Double, SlabCacheDTO>();
        navigableMap.put(99.0, slabCacheDTO);
        slabCache.put("Snapdeal-SURFACE_D", navigableMap);
    
        Map<String, List<PackagingTypeCacheDTO>> mtypeToPackagingTypeDTOsMap = new HashMap<String, List<PackagingTypeCacheDTO>>();
        PackagingTypeCache packagingTypeCache = new PackagingTypeCache();
        List<PackagingTypeCacheDTO> lPackagingTypeCacheDTO = new ArrayList<PackagingTypeCacheDTO>();
        PackagingTypeCacheDTO packagingTypeCacheDTO = createPackagingTypeCacheDTO("Snapdeal","SURFACE_D","Picknpack");

        lPackagingTypeCacheDTO.add(packagingTypeCacheDTO);
    
        mtypeToPackagingTypeDTOsMap.put("SURFACE_D", lPackagingTypeCacheDTO);
    
        PackagingType packagingType = createPackagingType("Snapdeal","picknpack");
        pauSRO = createPauSro("990001",10.0,10.0,10.0,"CARTON");
        List<PackagingType> lpackagingList = new ArrayList<PackagingType>();
        lpackagingList.add(packagingType);
        packagingTypeCache.addPackagingType(lpackagingList);
        CacheManager.getInstance().setCache(packagingTypeCache);
        when(mcocofsExternalService.getProductFulfilmentAttributes(Matchers.<GetProductFulfilmentAttributeRequest> any())).thenReturn(pauSRO);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(10.0, 10.0, 10.0)).thenReturn(100.0);
        ReflectionTestUtils.setField(mPackagingRecommendationService, "cocofsExternalService", mcocofsExternalService);
    
        mSupcPackagingTypeMongoService = mock(ISupcPackagingTypeMongoService.class);
        when(mSupcPackagingTypeMongoService.getEnabledAndActivePackagingTypeBySupcAndStoreCode("Snapdeal", "990001")).thenReturn(null);
        ReflectionTestUtils.setField(mruleEngineService, "supcPackagingTypeMongoService", mSupcPackagingTypeMongoService);
    
        ReflectionTestUtils.setField(packagingTypeCache, "typeToPackagingTypeDTOsMap", mtypeToPackagingTypeDTOsMap);
    
        GetPackagingRecommendationsRequest request = mock(GetPackagingRecommendationsRequest.class);
        ReflectionTestUtils.setField(packmanLandingService, "packagingRecomendationService", mPackagingRecommendationService);
        Protocol proto = Protocol.PROTOCOL_JSON;
        when(request.getRequestProtocol()).thenReturn(proto);
        when(request.getApiKey()).thenReturn("p@(km@n");
        when(request.getStoreFrontId()).thenReturn("SD");
        when(request.getIsShipTogether()).thenReturn(false);
        when(request.getPackageDetail()).thenReturn(packageDetailSRO);
    
        CacheManager.getInstance().setCache(mslabMappingCache);
    
        ReflectionTestUtils.setField(mslabMappingCache, "slabCache", slabCache);
        mruleService = mock(IRulesService.class);
        List<BlockParams> lblockParams = new ArrayList<BlockParams>();
        Block block = createBlock("SubcategoryPackagingRuleBlock");
        BlockParams blockParams = createBlockParams(block,"LABEL","Label");
        lblockParams.add(blockParams);
        mexclusiveService = mock(PackmanExclusiveServiceImpl.class);
        when(mruleService.getBlockParamsByBlockName("SubcategoryPackagingRuleBlock")).thenReturn(lblockParams);
        ReflectionTestUtils.setField(mruleEngineService, "ruleService", mruleService);
        when(mexclusiveService.fetchLabelsForStore("Snapdeal")).thenReturn(labels);
        ReflectionTestUtils.setField(mruleEngineService, "exclusiveService", mexclusiveService);
    
        mDepedencyForDTO = mock(com.snapdeal.packman.dto.SlabCacheDTO.IDependency.class);
    
        when(mDepedencyForDTO.getBooleanScalar(Property.USE_PRIMARY_LBH)).thenReturn(false);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_LBH)).thenReturn(false);
    
        ReflectionTestUtils.setField(slabCacheDTO, "dep", mDepedencyForDTO);
    
        GetPackagingRecommendationsResponse resp = packmanLandingService.getPackagingRecommendations(request);
        assertTrue(resp.isSuccessful());
        assertEquals("SURFACE_D", resp.getPackagingRecommendationList().get(0).getPackagingType().getType());
        assertEquals("Picknpack", resp.getPackagingRecommendationList().get(0).getPackagingMode());
    }
    
    //test  15
    @Test
    public void testGetPackagingRecommendationsNormalAtCatLabelAndBrandBothExist() throws PackagingRecomendationException, ExternalDataNotFoundException {
    
        List<String> labels = new ArrayList<String>();
        labels.add(Labels.Premium.toString());
        mExternalServiceFactory = mock(ExternalServiceFactory.class);
        ICAMSExternalService miCAMSExternalService = mock(ICAMSExternalService.class);
        GetPOGDetailListResponse response = new GetPOGDetailListResponse();
        POGDetailSRO pogDetailSROs = new POGDetailSRO();
        pogDetailSROs.setEnabled(true);
    
        List<POGDetailSRO> lpogDetailSROs = new ArrayList<POGDetailSRO>();
        lpogDetailSROs.add(pogDetailSROs);
        response.setPogDetailSROs(lpogDetailSROs);
        when(mExternalServiceFactory.getCAMSExternalService()).thenReturn(miCAMSExternalService);
        when(miCAMSExternalService.getPOGDetailsBbySupc(Matchers.<GetPOGDetailListBySUPCListRequest> any())).thenReturn(response);
        ReflectionTestUtils.setField(mruleEngineService, "factory", mExternalServiceFactory);
        PackagableItemSRO packagableItemSRO = createPackagableItemSRO(labels);
        lPackagableItemSRO.add(packagableItemSRO);
        packageDetailSRO.setPackagableItemList(lPackagableItemSRO);
        genericMao = mock(IGenericMao.class);
        ruleList = new ArrayList<>();
        List<CriteriaUnit> lCriteriaUnit = new ArrayList<CriteriaUnit>();
        CriteriaUnit criteriaUnit = createCriteriaUnit(1,"LABELS","premium");
        lCriteriaUnit.add(criteriaUnit);
        PackmanRuleUnit packmanRuleUnit = createPackmanRuleUnit(2,"ru03",7,"CARTON", lCriteriaUnit);
        ruleList.add(packmanRuleUnit);
        doAnswer(new Answer<List<PackmanRuleUnit>>() {
            public List<PackmanRuleUnit> answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                CocofsMongoQuery cocofsMongoQuery = (CocofsMongoQuery) args[0];
                List<Criteria> lCriteria = cocofsMongoQuery.getCriteria();
                Criteria criteria1 = lCriteria.get(0);
                String rule_type = (String) criteria1.getObj();
                if (rule_type.equals("category-packaging"))
                    return ruleList;
                return null;
    
            }
    
        }).when(genericMao).getDocumentList(Matchers.<CocofsMongoQuery> any(), Matchers.<Class<PackmanRuleUnit>> any());
        ReflectionTestUtils.setField(mruleEngineService, "genericMao", genericMao);
    
        PackmanPackagingRuleEngineServiceImpl packmanPackagingRuleEngineServiceImpl = Mockito.spy(new PackmanPackagingRuleEngineServiceImpl());
        Map<String, Object> ruleParam = new HashMap<String, Object>();
        CatageoryEvaluationParamDto paramDTO = new CatageoryEvaluationParamDto();
        paramDTO.setLabel(labels);
        paramDTO.setBrand("puma");
    
        SlabMappingCache mslabMappingCache = new SlabMappingCache();
        Map<String, NavigableMap<Double, SlabCacheDTO>> slabCache = new HashMap<String, NavigableMap<Double, SlabCacheDTO>>();
        SlabCacheDTO slabCacheDTO = new SlabCacheDTO();
        List<SlabItemDTO> boxes = new ArrayList<SlabItemDTO>();
        SlabItemDTO slabItemDTO = createSlabItemDto("CB101",10.0,10.0,10.0,100.00,"CB101");
        boxes.add(slabItemDTO);
    
        slabItemDTO = createSlabItemDto("CB02",40.0,10.0,10.0,400.0,"BOX 2");
        boxes.add(slabItemDTO);
    
        slabCacheDTO = createSlabCacheDTO(boxes,0,200);
        NavigableMap<Double, SlabCacheDTO> navigableMap = new ConcurrentSkipListMap<Double, SlabCacheDTO>();
        navigableMap.put(99.0, slabCacheDTO);
        slabCache.put("Snapdeal-CARTON", navigableMap);
    
        Map<String, List<PackagingTypeCacheDTO>> mtypeToPackagingTypeDTOsMap = new HashMap<String, List<PackagingTypeCacheDTO>>();
        PackagingTypeCache packagingTypeCache = new PackagingTypeCache();
        List<PackagingTypeCacheDTO> lPackagingTypeCacheDTO = new ArrayList<PackagingTypeCacheDTO>();
    
        PackagingTypeCacheDTO packagingTypeCacheDTO = createPackagingTypeCacheDTO("Snapdeal","CARTON","Normal");
        lPackagingTypeCacheDTO.add(packagingTypeCacheDTO);
        mtypeToPackagingTypeDTOsMap.put("CARTON", lPackagingTypeCacheDTO);
    
        PackagingType packagingType = createPackagingType("Snapdeal","Normal");
    
        pauSRO = createPauSro("990001",10.0,10.0,10.0,"CARTON");
        List<PackagingType> lpackagingList = new ArrayList<PackagingType>();
        lpackagingList.add(packagingType);
        packagingTypeCache.addPackagingType(lpackagingList);
        CacheManager.getInstance().setCache(packagingTypeCache);
        when(mcocofsExternalService.getProductFulfilmentAttributes(Matchers.<GetProductFulfilmentAttributeRequest> any())).thenReturn(pauSRO);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(10.0, 10.0, 10.0)).thenReturn(100.0);
        ReflectionTestUtils.setField(mPackagingRecommendationService, "cocofsExternalService", mcocofsExternalService);
    
        mSupcPackagingTypeMongoService = mock(ISupcPackagingTypeMongoService.class);
        when(mSupcPackagingTypeMongoService.getEnabledAndActivePackagingTypeBySupcAndStoreCode("Snapdeal", "990001")).thenReturn(null);
        ReflectionTestUtils.setField(mruleEngineService, "supcPackagingTypeMongoService", mSupcPackagingTypeMongoService);
    
        ReflectionTestUtils.setField(packagingTypeCache, "typeToPackagingTypeDTOsMap", mtypeToPackagingTypeDTOsMap);
    
        GetPackagingRecommendationsRequest request = mock(GetPackagingRecommendationsRequest.class);
        ReflectionTestUtils.setField(packmanLandingService, "packagingRecomendationService", mPackagingRecommendationService);
        Protocol proto = Protocol.PROTOCOL_JSON;
        when(request.getRequestProtocol()).thenReturn(proto);
        when(request.getApiKey()).thenReturn("p@(km@n");
        when(request.getStoreFrontId()).thenReturn("SD");
        when(request.getIsShipTogether()).thenReturn(false);
        when(request.getPackageDetail()).thenReturn(packageDetailSRO);
    
        CacheManager.getInstance().setCache(mslabMappingCache);
    
        ReflectionTestUtils.setField(mslabMappingCache, "slabCache", slabCache);
        mruleService = mock(IRulesService.class);
        List<BlockParams> lblockParams = new ArrayList<BlockParams>();
        Block block = createBlock("CategoryPackagingRuleBlock");
        BlockParams blockParams = createBlockParams(block,"LABEL","Label");
        ruleParam.put("abc", "CategoryPackagingRuleBlock");
        Mockito.doReturn(ruleParam).when(packmanPackagingRuleEngineServiceImpl).populateRuleParamsFromBlockObject(Matchers.any(), (List<BlockParams>) Matchers.any());
        lblockParams.add(blockParams);
        mexclusiveService = mock(PackmanExclusiveServiceImpl.class);
        when(mruleService.getBlockParamsByBlockName("CategoryPackagingRuleBlock")).thenReturn(lblockParams);
        ReflectionTestUtils.setField(mruleEngineService, "ruleService", mruleService);
        ReflectionTestUtils.setField(mruleEngineService, "exclusiveService", mexclusiveService);
    
        mDepedencyForDTO = mock(com.snapdeal.packman.dto.SlabCacheDTO.IDependency.class);
    
        when(mDepedencyForDTO.getBooleanScalar(Property.USE_PRIMARY_LBH)).thenReturn(false);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_LBH)).thenReturn(false);  
        ReflectionTestUtils.setField(slabCacheDTO, "dep", mDepedencyForDTO);
        GetPackagingRecommendationsResponse resp = packmanLandingService.getPackagingRecommendations(request);
        assertTrue(resp.isSuccessful());
        assertEquals("CARTON", resp.getPackagingRecommendationList().get(0).getPackagingType().getType());
        assertEquals("Normal", resp.getPackagingRecommendationList().get(0).getPackagingMode());
        assertEquals("CB101", resp.getPackagingRecommendationList().get(0).getPreferredPackagingTypeItem().getCode());
    }
    
    //test 16
    //LABEL AND BRAND BOTH ARE UNEXIST AT SUB CAT LAVEL
    @Test
    public void testGetPackagingRecommendationsPicknpackAtSubcatLabelAndBrandUnexist() throws PackagingRecomendationException, ExternalDataNotFoundException {
    
        mExternalServiceFactory = mock(ExternalServiceFactory.class);
        ICAMSExternalService miCAMSExternalService = mock(ICAMSExternalService.class);
        GetPOGDetailListResponse response = new GetPOGDetailListResponse();
        POGDetailSRO pogDetailSROs = new POGDetailSRO();
        pogDetailSROs.setEnabled(true);
    
        List<POGDetailSRO> lpogDetailSROs = new ArrayList<POGDetailSRO>();
        lpogDetailSROs.add(pogDetailSROs);
        response.setPogDetailSROs(lpogDetailSROs);
        when(mExternalServiceFactory.getCAMSExternalService()).thenReturn(miCAMSExternalService);
        when(miCAMSExternalService.getPOGDetailsBbySupc(Matchers.<GetPOGDetailListBySUPCListRequest> any())).thenReturn(response);
        ReflectionTestUtils.setField(mruleEngineService, "factory", mExternalServiceFactory);
        lPackagableItemSRO.add(packagableItemSRO);
        packageDetailSRO.setPackagableItemList(lPackagableItemSRO);
        genericMao = mock(IGenericMao.class);
        ruleList = new ArrayList<>();
        List<CriteriaUnit> lCriteriaUnit = new ArrayList<CriteriaUnit>();
        CriteriaUnit criteriaUnit = createCriteriaUnit(1,"","");
        lCriteriaUnit.add(criteriaUnit);
        PackmanRuleUnit packmanRuleUnit = createPackmanRuleUnit(2,"ru02",4,"SURFACE_C", lCriteriaUnit);
        packmanRuleUnit.setCriteria(lCriteriaUnit);
        ruleList.add(packmanRuleUnit);
        when(genericMao.getDocumentList(Matchers.<CocofsMongoQuery> any(), Matchers.<Class<PackmanRuleUnit>> any())).thenReturn(ruleList);
        ReflectionTestUtils.setField(mruleEngineService, "genericMao", genericMao);
    
        SlabMappingCache mslabMappingCache = new SlabMappingCache();
        Map<String, NavigableMap<Double, SlabCacheDTO>> slabCache = new HashMap<String, NavigableMap<Double, SlabCacheDTO>>();
        SlabCacheDTO slabCacheDTO = new SlabCacheDTO();
        List<SlabItemDTO> boxes = new ArrayList<SlabItemDTO>();
        SlabItemDTO slabItemDTO = createSlabItemDto("CB101",10.0,10.0,10.0,100.00,"CB101");
        boxes.add(slabItemDTO);
    
        slabItemDTO = createSlabItemDto("CB02",40.0,10.0,10.0,400.0,"BOX 2");
        boxes.add(slabItemDTO);
    
        slabCacheDTO = createSlabCacheDTO(boxes,0,200);
        NavigableMap<Double, SlabCacheDTO> navigableMap = new ConcurrentSkipListMap<Double, SlabCacheDTO>();
        navigableMap.put(99.0, slabCacheDTO);
        slabCache.put("Snapdeal-SURFACE_C", navigableMap);
    
        Map<String, List<PackagingTypeCacheDTO>> mtypeToPackagingTypeDTOsMap = new HashMap<String, List<PackagingTypeCacheDTO>>();
        PackagingTypeCache packagingTypeCache = new PackagingTypeCache();
        List<PackagingTypeCacheDTO> lPackagingTypeCacheDTO = new ArrayList<PackagingTypeCacheDTO>();
        PackagingTypeCacheDTO packagingTypeCacheDTO = createPackagingTypeCacheDTO("Snapdeal","SURFACE_C","Picknpack");
        lPackagingTypeCacheDTO.add(packagingTypeCacheDTO);
    
        mtypeToPackagingTypeDTOsMap.put("SURFACE_C", lPackagingTypeCacheDTO);
    
        PackagingType packagingType = createPackagingType("Snapdeal","picknpack");
        pauSRO = createPauSro("990001",10.0,10.0,10.0,"SURFACE_C");
        List<PackagingType> lpackagingList = new ArrayList<PackagingType>();
        lpackagingList.add(packagingType);
        packagingTypeCache.addPackagingType(lpackagingList);
        CacheManager.getInstance().setCache(packagingTypeCache);
        when(mcocofsExternalService.getProductFulfilmentAttributes(Matchers.<GetProductFulfilmentAttributeRequest> any())).thenReturn(pauSRO);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(10.0, 10.0, 10.0)).thenReturn(100.0);
        ReflectionTestUtils.setField(mPackagingRecommendationService, "cocofsExternalService", mcocofsExternalService);
    
        mSupcPackagingTypeMongoService = mock(ISupcPackagingTypeMongoService.class);
        when(mSupcPackagingTypeMongoService.getEnabledAndActivePackagingTypeBySupcAndStoreCode("Snapdeal", "990001")).thenReturn(null);
        ReflectionTestUtils.setField(mruleEngineService, "supcPackagingTypeMongoService", mSupcPackagingTypeMongoService);
    
        ReflectionTestUtils.setField(packagingTypeCache, "typeToPackagingTypeDTOsMap", mtypeToPackagingTypeDTOsMap);
    
        GetPackagingRecommendationsRequest request = mock(GetPackagingRecommendationsRequest.class);
        ReflectionTestUtils.setField(packmanLandingService, "packagingRecomendationService", mPackagingRecommendationService);
        Protocol proto = Protocol.PROTOCOL_JSON;
        when(request.getRequestProtocol()).thenReturn(proto);
        when(request.getApiKey()).thenReturn("p@(km@n");
        when(request.getStoreFrontId()).thenReturn("SD");
        when(request.getIsShipTogether()).thenReturn(false);
        when(request.getPackageDetail()).thenReturn(packageDetailSRO);
    
        CacheManager.getInstance().setCache(mslabMappingCache);
    
        ReflectionTestUtils.setField(mslabMappingCache, "slabCache", slabCache);
        mruleService = mock(IRulesService.class);
        List<BlockParams> lblockParams = new ArrayList<BlockParams>();
        Block block = createBlock("SubcategoryPackagingRuleBlock");
        BlockParams blockParams = createBlockParams(block,"","");
        lblockParams.add(blockParams);
        mexclusiveService = mock(PackmanExclusiveServiceImpl.class);
        when(mruleService.getBlockParamsByBlockName("SubcategoryPackagingRuleBlock")).thenReturn(lblockParams);
        ReflectionTestUtils.setField(mruleEngineService, "ruleService", mruleService);
        ReflectionTestUtils.setField(mruleEngineService, "exclusiveService", mexclusiveService);
    
        mDepedencyForDTO = mock(com.snapdeal.packman.dto.SlabCacheDTO.IDependency.class);
    
        when(mDepedencyForDTO.getBooleanScalar(Property.USE_PRIMARY_LBH)).thenReturn(false);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_LBH)).thenReturn(false);   
        ReflectionTestUtils.setField(slabCacheDTO, "dep", mDepedencyForDTO);
        GetPackagingRecommendationsResponse resp = packmanLandingService.getPackagingRecommendations(request);
        assertFalse(resp.isSuccessful());
    }
    
    //test 17
    @Test
    public void testGetPackagingRecommendationsPicknpackAtSubcatOnlyBrandExist() throws PackagingRecomendationException, ExternalDataNotFoundException {
    
        List<String> labels = new ArrayList<String>();
        labels.add(Labels.Premium.toString());
        labels.add(Labels.Designer.toString());
        labels.add(Labels.Luxury.toString());
        mExternalServiceFactory = mock(ExternalServiceFactory.class);
    
        ICAMSExternalService miCAMSExternalService = mock(ICAMSExternalService.class);
        GetPOGDetailListResponse response = new GetPOGDetailListResponse();
    
        ProductBrandDetailSRO brand = new ProductBrandDetailSRO();
        brand.setEnabled(true);
        brand.setName("Puma");
    
        POGDetailSRO pogDetailSROs = new POGDetailSRO();
        pogDetailSROs.setEnabled(true);
    
        List<POGDetailSRO> lpogDetailSROs = new ArrayList<POGDetailSRO>();
        lpogDetailSROs.add(pogDetailSROs);
        response.setPogDetailSROs(lpogDetailSROs);
        response.setSuccessful(true);
        when(mExternalServiceFactory.getCAMSExternalService()).thenReturn(miCAMSExternalService);
        when(miCAMSExternalService.getPOGDetailsBbySupc(Matchers.<GetPOGDetailListBySUPCListRequest> any())).thenReturn(response);
        ReflectionTestUtils.setField(mruleEngineService, "factory", mExternalServiceFactory);
    
        packageDetailSRO.setPackagableItemList(lPackagableItemSRO);
        genericMao = mock(IGenericMao.class);
        ruleList = new ArrayList<>();
        List<CriteriaUnit> lCriteriaUnit = new ArrayList<CriteriaUnit>();
        CriteriaUnit criteriaUnit = createCriteriaUnit(1,"BRAND","puma");
        lCriteriaUnit.add(criteriaUnit);
        PackmanRuleUnit packmanRuleUnit = createPackmanRuleUnit(1,"ru01",4,"Surface_a", lCriteriaUnit);
      
        ruleList.add(packmanRuleUnit);
        when(genericMao.getDocumentList(Matchers.<CocofsMongoQuery> any(), Matchers.<Class<PackmanRuleUnit>> any())).thenReturn(ruleList);
        ReflectionTestUtils.setField(mruleEngineService, "genericMao", genericMao);
    
        SlabMappingCache mslabMappingCache = new SlabMappingCache();
        Map<String, NavigableMap<Double, SlabCacheDTO>> slabCache = new HashMap<String, NavigableMap<Double, SlabCacheDTO>>();
        SlabCacheDTO slabCacheDTO = new SlabCacheDTO();
        List<SlabItemDTO> boxes = new ArrayList<SlabItemDTO>();
        SlabItemDTO slabItemDTO = createSlabItemDto("CB01",20.0,10.0,10.0,200.0,"BOX 1");
        boxes.add(slabItemDTO);
        
        slabItemDTO = createSlabItemDto("CB02",40.0,10.0,10.0,400.0,"BOX 2");
        boxes.add(slabItemDTO);
    
        slabCacheDTO = createSlabCacheDTO(boxes,0,200);

        NavigableMap<Double, SlabCacheDTO> navigableMap = new ConcurrentSkipListMap<Double, SlabCacheDTO>();
        navigableMap.put(99.0, slabCacheDTO);
        slabCache.put("Snapdeal-Surface_a", navigableMap);
    
        Map<String, List<PackagingTypeCacheDTO>> mtypeToPackagingTypeDTOsMap = new HashMap<String, List<PackagingTypeCacheDTO>>();
        PackagingTypeCache packagingTypeCache = new PackagingTypeCache();
        List<PackagingTypeCacheDTO> lPackagingTypeCacheDTO = new ArrayList<PackagingTypeCacheDTO>();
        PackagingTypeCacheDTO packagingTypeCacheDTO = createPackagingTypeCacheDTO("Snapdeal","Surface_a","Picknpack");
        lPackagingTypeCacheDTO.add(packagingTypeCacheDTO);
    
        mtypeToPackagingTypeDTOsMap.put("Surface_a", lPackagingTypeCacheDTO);
        PackagingType packagingType = createPackagingType("Snapdeal","Picknpack");
        pauSRO = createPauSro("990001",10.0,10.0,10.0,"CARTON");
        List<PackagingType> lpackagingList = new ArrayList<PackagingType>();
        lpackagingList.add(packagingType);
        packagingTypeCache.addPackagingType(lpackagingList);
        CacheManager.getInstance().setCache(packagingTypeCache);
        when(mcocofsExternalService.getProductFulfilmentAttributes(Matchers.<GetProductFulfilmentAttributeRequest> any())).thenReturn(pauSRO);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(10.0, 10.0, 10.0)).thenReturn(100.0);
        ReflectionTestUtils.setField(mPackagingRecommendationService, "cocofsExternalService", mcocofsExternalService);
    
        mSupcPackagingTypeMongoService = mock(ISupcPackagingTypeMongoService.class);
        when(mSupcPackagingTypeMongoService.getEnabledAndActivePackagingTypeBySupcAndStoreCode("Snapdeal", "990001")).thenReturn(null);
        ReflectionTestUtils.setField(mruleEngineService, "supcPackagingTypeMongoService", mSupcPackagingTypeMongoService);
    
        ReflectionTestUtils.setField(packagingTypeCache, "typeToPackagingTypeDTOsMap", mtypeToPackagingTypeDTOsMap);
    
        GetPackagingRecommendationsRequest request = mock(GetPackagingRecommendationsRequest.class);
        ReflectionTestUtils.setField(packmanLandingService, "packagingRecomendationService", mPackagingRecommendationService);
        Protocol proto = Protocol.PROTOCOL_JSON;
        when(request.getRequestProtocol()).thenReturn(proto);
        when(request.getApiKey()).thenReturn("p@(km@n");
        when(request.getStoreFrontId()).thenReturn("SD");
        when(request.getIsShipTogether()).thenReturn(false);
        when(request.getPackageDetail()).thenReturn(packageDetailSRO);
    
        CacheManager.getInstance().setCache(mslabMappingCache);
    
        ReflectionTestUtils.setField(mslabMappingCache, "slabCache", slabCache);
        mruleService = mock(IRulesService.class);
        List<BlockParams> lblockParams = new ArrayList<BlockParams>();
        Block block = createBlock("SubcategoryPackagingRuleBlock");
        BlockParams blockParams = createBlockParams(block,"BRAND","Brand");
        lblockParams.add(blockParams);
        mexclusiveService = mock(PackmanExclusiveServiceImpl.class);
        when(mruleService.getBlockParamsByBlockName("SubcategoryPackagingRuleBlock")).thenReturn(lblockParams);
        ReflectionTestUtils.setField(mruleEngineService, "ruleService", mruleService);
        when(mexclusiveService.fetchLabelsForStore("Snapdeal")).thenReturn(labels);
        ReflectionTestUtils.setField(mruleEngineService, "exclusiveService", mexclusiveService);
    
        mDepedencyForDTO = mock(com.snapdeal.packman.dto.SlabCacheDTO.IDependency.class);
    
        when(mDepedencyForDTO.getBooleanScalar(Property.USE_PRIMARY_LBH)).thenReturn(false);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_LBH)).thenReturn(false);
    
        ReflectionTestUtils.setField(slabCacheDTO, "dep", mDepedencyForDTO);
        GetPackagingRecommendationsResponse resp = packmanLandingService.getPackagingRecommendations(request);
        assertTrue(resp.isSuccessful());
    }
    
    //test 18
    @Test
    public void testGetPackagingRecommendationsNormalAtStorewLabel() throws PackagingRecomendationException, ExternalDataNotFoundException {
    
        List<String> labels = new ArrayList<String>();
        labels.add(Labels.Premium.toString());
        mExternalServiceFactory = mock(ExternalServiceFactory.class);
        ICAMSExternalService miCAMSExternalService = mock(ICAMSExternalService.class);
        GetPOGDetailListResponse response = new GetPOGDetailListResponse();
        POGDetailSRO pogDetailSROs = new POGDetailSRO();
        pogDetailSROs.setEnabled(true);
    
        List<POGDetailSRO> lpogDetailSROs = new ArrayList<POGDetailSRO>();
        lpogDetailSROs.add(pogDetailSROs);
        response.setPogDetailSROs(lpogDetailSROs);
        when(mExternalServiceFactory.getCAMSExternalService()).thenReturn(miCAMSExternalService);
        when(miCAMSExternalService.getPOGDetailsBbySupc(Matchers.<GetPOGDetailListBySUPCListRequest> any())).thenReturn(response);
        ReflectionTestUtils.setField(mruleEngineService, "factory", mExternalServiceFactory);
        PackagableItemSRO packagableItemSRO = createPackagableItemSRO(labels);
        lPackagableItemSRO.add(packagableItemSRO);
        packageDetailSRO.setPackagableItemList(lPackagableItemSRO);
        genericMao = mock(IGenericMao.class);
        ruleList = new ArrayList<>();
        List<CriteriaUnit> lCriteriaUnit = new ArrayList<CriteriaUnit>();
        PackmanRuleUnit packmanRuleUnit = createPackmanRuleUnit(2,"ru03",7,"CARTON", lCriteriaUnit);
        ruleList.add(packmanRuleUnit);
        doAnswer(new Answer<List<PackmanRuleUnit>>() {
            public List<PackmanRuleUnit> answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                CocofsMongoQuery cocofsMongoQuery = (CocofsMongoQuery) args[0];
                List<Criteria> lCriteria = cocofsMongoQuery.getCriteria();
                Criteria criteria1 = lCriteria.get(0);
                String rule_type = (String) criteria1.getObj();
                if (rule_type.equals("store-packaging"))
                    return ruleList;
                return null;
    
            }
    
        }).when(genericMao).getDocumentList(Matchers.<CocofsMongoQuery> any(), Matchers.<Class<PackmanRuleUnit>> any());
        ReflectionTestUtils.setField(mruleEngineService, "genericMao", genericMao);
    
        PackmanPackagingRuleEngineServiceImpl packmanPackagingRuleEngineServiceImpl = Mockito.spy(new PackmanPackagingRuleEngineServiceImpl());
        Map<String, Object> ruleParam = new HashMap<String, Object>();
        SlabMappingCache mslabMappingCache = new SlabMappingCache();
        Map<String, NavigableMap<Double, SlabCacheDTO>> slabCache = new HashMap<String, NavigableMap<Double, SlabCacheDTO>>();
        SlabCacheDTO slabCacheDTO = new SlabCacheDTO();
        List<SlabItemDTO> boxes = new ArrayList<SlabItemDTO>();
        SlabItemDTO slabItemDTO = createSlabItemDto("CB01",20.0,10.0,10.0,200.0,"BOX 1");
        boxes.add(slabItemDTO);
        
        slabItemDTO = createSlabItemDto("CB02",40.0,10.0,10.0,400.0,"BOX 2");
        boxes.add(slabItemDTO);    
        slabCacheDTO = createSlabCacheDTO(boxes,0,200);
        NavigableMap<Double, SlabCacheDTO> navigableMap = new ConcurrentSkipListMap<Double, SlabCacheDTO>();
        navigableMap.put(99.0, slabCacheDTO);
        slabCache.put("Snapdeal-CARTON", navigableMap);
    
        Map<String, List<PackagingTypeCacheDTO>> mtypeToPackagingTypeDTOsMap = new HashMap<String, List<PackagingTypeCacheDTO>>();
        PackagingTypeCache packagingTypeCache = new PackagingTypeCache();
        List<PackagingTypeCacheDTO> lPackagingTypeCacheDTO = new ArrayList<PackagingTypeCacheDTO>();
        PackagingTypeCacheDTO packagingTypeCacheDTO = createPackagingTypeCacheDTO("Snapdeal","CARTON","Normal");
        lPackagingTypeCacheDTO.add(packagingTypeCacheDTO);
    
        mtypeToPackagingTypeDTOsMap.put("CARTON", lPackagingTypeCacheDTO);       
        PackagingType packagingType = createPackagingType("Snapdeal","Normal");
        pauSRO = createPauSro("990001",10.0,10.0,10.0,"CARTON");
        List<PackagingType> lpackagingList = new ArrayList<PackagingType>();
        lpackagingList.add(packagingType);
        packagingTypeCache.addPackagingType(lpackagingList);
        CacheManager.getInstance().setCache(packagingTypeCache);
        when(mcocofsExternalService.getProductFulfilmentAttributes(Matchers.<GetProductFulfilmentAttributeRequest> any())).thenReturn(pauSRO);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(10.0, 10.0, 10.0)).thenReturn(100.0);
        ReflectionTestUtils.setField(mPackagingRecommendationService, "cocofsExternalService", mcocofsExternalService);
    
        mSupcPackagingTypeMongoService = mock(ISupcPackagingTypeMongoService.class);
        when(mSupcPackagingTypeMongoService.getEnabledAndActivePackagingTypeBySupcAndStoreCode("Snapdeal", "990001")).thenReturn(null);
        ReflectionTestUtils.setField(mruleEngineService, "supcPackagingTypeMongoService", mSupcPackagingTypeMongoService);
        ReflectionTestUtils.setField(packagingTypeCache, "typeToPackagingTypeDTOsMap", mtypeToPackagingTypeDTOsMap);    
        GetPackagingRecommendationsRequest request = mock(GetPackagingRecommendationsRequest.class);
        ReflectionTestUtils.setField(packmanLandingService, "packagingRecomendationService", mPackagingRecommendationService);
        Protocol proto = Protocol.PROTOCOL_JSON;
        when(request.getRequestProtocol()).thenReturn(proto);
        when(request.getApiKey()).thenReturn("p@(km@n");
        when(request.getStoreFrontId()).thenReturn("SD");
        when(request.getIsShipTogether()).thenReturn(false);
        when(request.getPackageDetail()).thenReturn(packageDetailSRO);
    
        CacheManager.getInstance().setCache(mslabMappingCache);
    
        ReflectionTestUtils.setField(mslabMappingCache, "slabCache", slabCache);
        mruleService = mock(IRulesService.class);
        List<BlockParams> lblockParams = new ArrayList<BlockParams>();
        Block block = createBlock("StorePackagingRuleBlock");
        BlockParams blockParams = createBlockParams(block,"","");
        ruleParam.put("BRAND", "StorePackagingRuleBlock");
        Mockito.doReturn(ruleParam).when(packmanPackagingRuleEngineServiceImpl).populateRuleParamsFromBlockObject(Matchers.any(), (List<BlockParams>) Matchers.any());
        lblockParams.add(blockParams);
        mexclusiveService = mock(PackmanExclusiveServiceImpl.class);
        when(mruleService.getBlockParamsByBlockName("StorePackagingRuleBlock")).thenReturn(lblockParams);
        ReflectionTestUtils.setField(mruleEngineService, "ruleService", mruleService);
        ReflectionTestUtils.setField(mruleEngineService, "exclusiveService", mexclusiveService);
    
        mDepedencyForDTO = mock(com.snapdeal.packman.dto.SlabCacheDTO.IDependency.class);
    
        when(mDepedencyForDTO.getBooleanScalar(Property.USE_PRIMARY_LBH)).thenReturn(false);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_LBH)).thenReturn(false);
    
        ReflectionTestUtils.setField(slabCacheDTO, "dep", mDepedencyForDTO);
        GetPackagingRecommendationsResponse resp = packmanLandingService.getPackagingRecommendations(request);
        assertTrue(resp.isSuccessful());
        assertEquals("CARTON", resp.getPackagingRecommendationList().get(0).getPackagingType().getType());
        assertEquals("Normal", resp.getPackagingRecommendationList().get(0).getPackagingMode());
        assertEquals("CB01", resp.getPackagingRecommendationList().get(0).getPreferredPackagingTypeItem().getCode());
    }
    
    //test 19 
    @Test
    public void testGetPackagingRecommendationsNormalAtCatLabelAndBrandBothUnexist() throws PackagingRecomendationException, ExternalDataNotFoundException {
    
        List<String> labels = new ArrayList<String>();
        labels.add(Labels.Premium.toString());
        mExternalServiceFactory = mock(ExternalServiceFactory.class);
        ICAMSExternalService miCAMSExternalService = mock(ICAMSExternalService.class);
        GetPOGDetailListResponse response = new GetPOGDetailListResponse();
        POGDetailSRO pogDetailSROs = new POGDetailSRO();
        pogDetailSROs.setEnabled(true);
    
        List<POGDetailSRO> lpogDetailSROs = new ArrayList<POGDetailSRO>();
        lpogDetailSROs.add(pogDetailSROs);
        response.setPogDetailSROs(lpogDetailSROs);
        when(mExternalServiceFactory.getCAMSExternalService()).thenReturn(miCAMSExternalService);
        when(miCAMSExternalService.getPOGDetailsBbySupc(Matchers.<GetPOGDetailListBySUPCListRequest> any())).thenReturn(response);
        ReflectionTestUtils.setField(mruleEngineService, "factory", mExternalServiceFactory);
        PackagableItemSRO packagableItemSRO = createPackagableItemSRO(labels);
        lPackagableItemSRO.add(packagableItemSRO);
        packageDetailSRO.setPackagableItemList(lPackagableItemSRO);
        genericMao = mock(IGenericMao.class);
        ruleList = new ArrayList<>();
        List<CriteriaUnit> lCriteriaUnit = new ArrayList<CriteriaUnit>();
        PackmanRuleUnit packmanRuleUnit = createPackmanRuleUnit(2,"ru03",7,"SURFACE_D", lCriteriaUnit);
        ruleList.add(packmanRuleUnit);
        doAnswer(new Answer<List<PackmanRuleUnit>>() {
            public List<PackmanRuleUnit> answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                CocofsMongoQuery cocofsMongoQuery = (CocofsMongoQuery) args[0];
                List<Criteria> lCriteria = cocofsMongoQuery.getCriteria();
                Criteria criteria1 = lCriteria.get(0);
                String rule_type = (String) criteria1.getObj();
                if (rule_type.equals("category-packaging"))
                    return ruleList;
                return null;
    
            }
    
        }).when(genericMao).getDocumentList(Matchers.<CocofsMongoQuery> any(), Matchers.<Class<PackmanRuleUnit>> any());
        ReflectionTestUtils.setField(mruleEngineService, "genericMao", genericMao);
    
        PackmanPackagingRuleEngineServiceImpl packmanPackagingRuleEngineServiceImpl = Mockito.spy(new PackmanPackagingRuleEngineServiceImpl());
        Map<String, Object> ruleParam = new HashMap<String, Object>();
        SlabMappingCache mslabMappingCache = new SlabMappingCache();
        Map<String, NavigableMap<Double, SlabCacheDTO>> slabCache = new HashMap<String, NavigableMap<Double, SlabCacheDTO>>();
        SlabCacheDTO slabCacheDTO = new SlabCacheDTO();
        List<SlabItemDTO> boxes = new ArrayList<SlabItemDTO>();
        SlabItemDTO slabItemDTO = createSlabItemDto("CB101",10.0,10.0,10.0,100.00,"CB101");
        boxes.add(slabItemDTO);
    
        slabItemDTO = createSlabItemDto("CB02",40.0,10.0,10.0,400.0,"BOX 2");
        boxes.add(slabItemDTO);
    
        slabCacheDTO = createSlabCacheDTO(boxes,0,200);
        NavigableMap<Double, SlabCacheDTO> navigableMap = new ConcurrentSkipListMap<Double, SlabCacheDTO>();
        navigableMap.put(99.0, slabCacheDTO);
        slabCache.put("Snapdeal-CARTON", navigableMap);
    
        Map<String, List<PackagingTypeCacheDTO>> mtypeToPackagingTypeDTOsMap = new HashMap<String, List<PackagingTypeCacheDTO>>();
        PackagingTypeCache packagingTypeCache = new PackagingTypeCache();
        List<PackagingTypeCacheDTO> lPackagingTypeCacheDTO = new ArrayList<PackagingTypeCacheDTO>();
    
        PackagingTypeCacheDTO packagingTypeCacheDTO = createPackagingTypeCacheDTO("Snapdeal","CARTON","Normal");
        lPackagingTypeCacheDTO.add(packagingTypeCacheDTO);
        mtypeToPackagingTypeDTOsMap.put("CARTON", lPackagingTypeCacheDTO);
        PackagingType packagingType = createPackagingType("Snapdeal","Normal");
        pauSRO = createPauSro("990001",10.0,10.0,10.0,"CARTON");
        List<PackagingType> lpackagingList = new ArrayList<PackagingType>();
        lpackagingList.add(packagingType);
        packagingTypeCache.addPackagingType(lpackagingList);
        CacheManager.getInstance().setCache(packagingTypeCache);
        when(mcocofsExternalService.getProductFulfilmentAttributes(Matchers.<GetProductFulfilmentAttributeRequest> any())).thenReturn(pauSRO);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(10.0, 10.0, 10.0)).thenReturn(100.0);
        ReflectionTestUtils.setField(mPackagingRecommendationService, "cocofsExternalService", mcocofsExternalService);
    
        mSupcPackagingTypeMongoService = mock(ISupcPackagingTypeMongoService.class);
        when(mSupcPackagingTypeMongoService.getEnabledAndActivePackagingTypeBySupcAndStoreCode("Snapdeal", "990001")).thenReturn(null);
        ReflectionTestUtils.setField(mruleEngineService, "supcPackagingTypeMongoService", mSupcPackagingTypeMongoService);
    
        ReflectionTestUtils.setField(packagingTypeCache, "typeToPackagingTypeDTOsMap", mtypeToPackagingTypeDTOsMap);
    
        GetPackagingRecommendationsRequest request = mock(GetPackagingRecommendationsRequest.class);
        ReflectionTestUtils.setField(packmanLandingService, "packagingRecomendationService", mPackagingRecommendationService);
        Protocol proto = Protocol.PROTOCOL_JSON;
        when(request.getRequestProtocol()).thenReturn(proto);
        when(request.getApiKey()).thenReturn("p@(km@n");
        when(request.getStoreFrontId()).thenReturn("SD");
        when(request.getIsShipTogether()).thenReturn(false);
        when(request.getPackageDetail()).thenReturn(packageDetailSRO);
    
        CacheManager.getInstance().setCache(mslabMappingCache);
    
        ReflectionTestUtils.setField(mslabMappingCache, "slabCache", slabCache);
        mruleService = mock(IRulesService.class);
        List<BlockParams> lblockParams = new ArrayList<BlockParams>();
        Block block = createBlock("CategoryPackagingRuleBlock");
        BlockParams blockParams = createBlockParams(block,"","");
        Mockito.doReturn(ruleParam).when(packmanPackagingRuleEngineServiceImpl).populateRuleParamsFromBlockObject(Matchers.any(), (List<BlockParams>) Matchers.any());
        lblockParams.add(blockParams);
        mexclusiveService = mock(PackmanExclusiveServiceImpl.class);
        when(mruleService.getBlockParamsByBlockName("CategoryPackagingRuleBlock")).thenReturn(lblockParams);
        ReflectionTestUtils.setField(mruleEngineService, "ruleService", mruleService);
        ReflectionTestUtils.setField(mruleEngineService, "exclusiveService", mexclusiveService);
    
        mDepedencyForDTO = mock(com.snapdeal.packman.dto.SlabCacheDTO.IDependency.class);    
        when(mDepedencyForDTO.getBooleanScalar(Property.USE_PRIMARY_LBH)).thenReturn(false);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_LBH)).thenReturn(false);
    
        ReflectionTestUtils.setField(slabCacheDTO, "dep", mDepedencyForDTO);  
        GetPackagingRecommendationsResponse resp = packmanLandingService.getPackagingRecommendations(request);        
        assertTrue(resp.isSuccessful());
    }
    
    //test 20
    //LABEL AND BRAND BOTH ARE UNEXIST AT SUB CAT LAVEL
    @Test
    public void testGetPackagingRecommendationsNormalAtSubcatLabelAndBrandUnexist() throws PackagingRecomendationException, ExternalDataNotFoundException {
    
        mExternalServiceFactory = mock(ExternalServiceFactory.class);
        ICAMSExternalService miCAMSExternalService = mock(ICAMSExternalService.class);
        GetPOGDetailListResponse response = new GetPOGDetailListResponse();
        POGDetailSRO pogDetailSROs = new POGDetailSRO();
        pogDetailSROs.setEnabled(true);
    
        List<POGDetailSRO> lpogDetailSROs = new ArrayList<POGDetailSRO>();
        lpogDetailSROs.add(pogDetailSROs);
        response.setPogDetailSROs(lpogDetailSROs);
        when(mExternalServiceFactory.getCAMSExternalService()).thenReturn(miCAMSExternalService);
        when(miCAMSExternalService.getPOGDetailsBbySupc(Matchers.<GetPOGDetailListBySUPCListRequest> any())).thenReturn(response);
        ReflectionTestUtils.setField(mruleEngineService, "factory", mExternalServiceFactory);
    
        lPackagableItemSRO.add(packagableItemSRO);
        packageDetailSRO.setPackagableItemList(lPackagableItemSRO);
        genericMao = mock(IGenericMao.class);
        ruleList = new ArrayList<>();
        List<CriteriaUnit> lCriteriaUnit = new ArrayList<CriteriaUnit>();
        CriteriaUnit criteriaUnit = createCriteriaUnit(1,"","");
        lCriteriaUnit.add(criteriaUnit);      
        PackmanRuleUnit packmanRuleUnit = createPackmanRuleUnit(2,"ru02",4,"CARTON", lCriteriaUnit);   
        ruleList.add(packmanRuleUnit);
        when(genericMao.getDocumentList(Matchers.<CocofsMongoQuery> any(), Matchers.<Class<PackmanRuleUnit>> any())).thenReturn(ruleList);
        ReflectionTestUtils.setField(mruleEngineService, "genericMao", genericMao);
    
        SlabMappingCache mslabMappingCache = new SlabMappingCache();
        Map<String, NavigableMap<Double, SlabCacheDTO>> slabCache = new HashMap<String, NavigableMap<Double, SlabCacheDTO>>();
        SlabCacheDTO slabCacheDTO = new SlabCacheDTO();
        List<SlabItemDTO> boxes = new ArrayList<SlabItemDTO>();
        SlabItemDTO slabItemDTO = createSlabItemDto("CB101",10.0,10.0,10.0,100.00,"CB101");
        boxes.add(slabItemDTO);
    
        slabItemDTO = createSlabItemDto("CB02",40.0,10.0,10.0,400.0,"BOX 2");
        boxes.add(slabItemDTO);   
        slabCacheDTO = createSlabCacheDTO(boxes,0,200);
        NavigableMap<Double, SlabCacheDTO> navigableMap = new ConcurrentSkipListMap<Double, SlabCacheDTO>();
        navigableMap.put(99.0, slabCacheDTO);
        slabCache.put("Snapdeal-CARTON", navigableMap);
    
        Map<String, List<PackagingTypeCacheDTO>> mtypeToPackagingTypeDTOsMap = new HashMap<String, List<PackagingTypeCacheDTO>>();
        PackagingTypeCache packagingTypeCache = new PackagingTypeCache();
        List<PackagingTypeCacheDTO> lPackagingTypeCacheDTO = new ArrayList<PackagingTypeCacheDTO>();
    
        PackagingTypeCacheDTO packagingTypeCacheDTO = createPackagingTypeCacheDTO("Snapdeal","CARTON","Normal");
        lPackagingTypeCacheDTO.add(packagingTypeCacheDTO);
        mtypeToPackagingTypeDTOsMap.put("CARTON", lPackagingTypeCacheDTO);
        PackagingType packagingType = createPackagingType("Snapdeal","Normal");

        pauSRO = createPauSro("990001",10.0,10.0,10.0,"CARTON");
        List<PackagingType> lpackagingList = new ArrayList<PackagingType>();
        lpackagingList.add(packagingType);
        packagingTypeCache.addPackagingType(lpackagingList);
        CacheManager.getInstance().setCache(packagingTypeCache);
        when(mcocofsExternalService.getProductFulfilmentAttributes(Matchers.<GetProductFulfilmentAttributeRequest> any())).thenReturn(pauSRO);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(10.0, 10.0, 10.0)).thenReturn(100.0);
        ReflectionTestUtils.setField(mPackagingRecommendationService, "cocofsExternalService", mcocofsExternalService);
    
        mSupcPackagingTypeMongoService = mock(ISupcPackagingTypeMongoService.class);
        when(mSupcPackagingTypeMongoService.getEnabledAndActivePackagingTypeBySupcAndStoreCode("Snapdeal", "990001")).thenReturn(null);
        ReflectionTestUtils.setField(mruleEngineService, "supcPackagingTypeMongoService", mSupcPackagingTypeMongoService);
    
        ReflectionTestUtils.setField(packagingTypeCache, "typeToPackagingTypeDTOsMap", mtypeToPackagingTypeDTOsMap);
    
        GetPackagingRecommendationsRequest request = mock(GetPackagingRecommendationsRequest.class);
        ReflectionTestUtils.setField(packmanLandingService, "packagingRecomendationService", mPackagingRecommendationService);
        Protocol proto = Protocol.PROTOCOL_JSON;
        when(request.getRequestProtocol()).thenReturn(proto);
        when(request.getApiKey()).thenReturn("p@(km@n");
        when(request.getStoreFrontId()).thenReturn("SD");
        when(request.getIsShipTogether()).thenReturn(false);
        when(request.getPackageDetail()).thenReturn(packageDetailSRO);
    
        CacheManager.getInstance().setCache(mslabMappingCache);
    
        ReflectionTestUtils.setField(mslabMappingCache, "slabCache", slabCache);
        mruleService = mock(IRulesService.class);
        List<BlockParams> lblockParams = new ArrayList<BlockParams>();
        Block block = createBlock("SubcategoryPackagingRuleBlock");
        BlockParams blockParams = createBlockParams(block,"","");
        lblockParams.add(blockParams);
        mexclusiveService = mock(PackmanExclusiveServiceImpl.class);
        when(mruleService.getBlockParamsByBlockName("SubcategoryPackagingRuleBlock")).thenReturn(lblockParams);
        ReflectionTestUtils.setField(mruleEngineService, "ruleService", mruleService);
        ReflectionTestUtils.setField(mruleEngineService, "exclusiveService", mexclusiveService);
        mDepedencyForDTO = mock(com.snapdeal.packman.dto.SlabCacheDTO.IDependency.class);
    
        when(mDepedencyForDTO.getBooleanScalar(Property.USE_PRIMARY_LBH)).thenReturn(false);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_LBH)).thenReturn(false);    
        ReflectionTestUtils.setField(slabCacheDTO, "dep", mDepedencyForDTO);    
        GetPackagingRecommendationsResponse resp = packmanLandingService.getPackagingRecommendations(request);
        assertFalse(resp.isSuccessful());
    }
    
    //test 21
    @Test
    public void testGetPackagingRecommendationsNormalAtSubcatOnlyLabelsExist() throws PackagingRecomendationException, ExternalDataNotFoundException {
    
        List<String> labels = new ArrayList<String>();
        labels.add(Labels.Premium.toString());
        mExternalServiceFactory = mock(ExternalServiceFactory.class);
        ICAMSExternalService miCAMSExternalService = mock(ICAMSExternalService.class);
        GetPOGDetailListResponse response = new GetPOGDetailListResponse();
        POGDetailSRO pogDetailSROs = new POGDetailSRO();
        pogDetailSROs.setEnabled(true);
    
        List<POGDetailSRO> lpogDetailSROs = new ArrayList<POGDetailSRO>();
        lpogDetailSROs.add(pogDetailSROs);
        response.setPogDetailSROs(lpogDetailSROs);
        when(mExternalServiceFactory.getCAMSExternalService()).thenReturn(miCAMSExternalService);
        when(miCAMSExternalService.getPOGDetailsBbySupc(Matchers.<GetPOGDetailListBySUPCListRequest> any())).thenReturn(response);
        ReflectionTestUtils.setField(mruleEngineService, "factory", mExternalServiceFactory);
        PackagableItemSRO packagableItemSRO = createPackagableItemSRO(labels);
        lPackagableItemSRO.add(packagableItemSRO);
        packageDetailSRO.setPackagableItemList(lPackagableItemSRO);
        genericMao = mock(IGenericMao.class);
        ruleList = new ArrayList<>();
        List<CriteriaUnit> lCriteriaUnit = new ArrayList<CriteriaUnit>();
        CriteriaUnit criteriaUnit = createCriteriaUnit(1,"LABELS","premium");
        lCriteriaUnit.add(criteriaUnit);
        PackmanRuleUnit packmanRuleUnit = createPackmanRuleUnit(2,"ru02",4,"CARTON", lCriteriaUnit);
        ruleList.add(packmanRuleUnit);
        when(genericMao.getDocumentList(Matchers.<CocofsMongoQuery> any(), Matchers.<Class<PackmanRuleUnit>> any())).thenReturn(ruleList);
        ReflectionTestUtils.setField(mruleEngineService, "genericMao", genericMao);
    
        SlabMappingCache mslabMappingCache = new SlabMappingCache();
        Map<String, NavigableMap<Double, SlabCacheDTO>> slabCache = new HashMap<String, NavigableMap<Double, SlabCacheDTO>>();
        SlabCacheDTO slabCacheDTO = new SlabCacheDTO();
        List<SlabItemDTO> boxes = new ArrayList<SlabItemDTO>();
        SlabItemDTO slabItemDTO = createSlabItemDto("CB101",10.0,10.0,10.0,100.00,"CB101");
        boxes.add(slabItemDTO);
    
        slabItemDTO = createSlabItemDto("CB02",40.0,10.0,10.0,400.0,"BOX 2");
        boxes.add(slabItemDTO);
    
        slabCacheDTO = createSlabCacheDTO(boxes,0,200);
        NavigableMap<Double, SlabCacheDTO> navigableMap = new ConcurrentSkipListMap<Double, SlabCacheDTO>();
        navigableMap.put(99.0, slabCacheDTO);
        slabCache.put("Snapdeal-CARTON", navigableMap);
    
        Map<String, List<PackagingTypeCacheDTO>> mtypeToPackagingTypeDTOsMap = new HashMap<String, List<PackagingTypeCacheDTO>>();
        PackagingTypeCache packagingTypeCache = new PackagingTypeCache();
        List<PackagingTypeCacheDTO> lPackagingTypeCacheDTO = new ArrayList<PackagingTypeCacheDTO>();
    
        PackagingTypeCacheDTO packagingTypeCacheDTO = createPackagingTypeCacheDTO("Snapdeal","CARTON","Normal");
        lPackagingTypeCacheDTO.add(packagingTypeCacheDTO);
        mtypeToPackagingTypeDTOsMap.put("CARTON", lPackagingTypeCacheDTO);
    
        PackagingType packagingType = createPackagingType("Snapdeal","Normal");
        pauSRO = createPauSro("990001",10.0,10.0,10.0,"CARTON");
        List<PackagingType> lpackagingList = new ArrayList<PackagingType>();
        lpackagingList.add(packagingType);
        packagingTypeCache.addPackagingType(lpackagingList);
        CacheManager.getInstance().setCache(packagingTypeCache);
        when(mcocofsExternalService.getProductFulfilmentAttributes(Matchers.<GetProductFulfilmentAttributeRequest> any())).thenReturn(pauSRO);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(10.0, 10.0, 10.0)).thenReturn(100.0);
        ReflectionTestUtils.setField(mPackagingRecommendationService, "cocofsExternalService", mcocofsExternalService);
    
        mSupcPackagingTypeMongoService = mock(ISupcPackagingTypeMongoService.class);
        when(mSupcPackagingTypeMongoService.getEnabledAndActivePackagingTypeBySupcAndStoreCode("Snapdeal", "990001")).thenReturn(null);
        ReflectionTestUtils.setField(mruleEngineService, "supcPackagingTypeMongoService", mSupcPackagingTypeMongoService);
    
        ReflectionTestUtils.setField(packagingTypeCache, "typeToPackagingTypeDTOsMap", mtypeToPackagingTypeDTOsMap);
    
        GetPackagingRecommendationsRequest request = mock(GetPackagingRecommendationsRequest.class);
        ReflectionTestUtils.setField(packmanLandingService, "packagingRecomendationService", mPackagingRecommendationService);
        Protocol proto = Protocol.PROTOCOL_JSON;
        when(request.getRequestProtocol()).thenReturn(proto);
        when(request.getApiKey()).thenReturn("p@(km@n");
        when(request.getStoreFrontId()).thenReturn("SD");
        when(request.getIsShipTogether()).thenReturn(false);
        when(request.getPackageDetail()).thenReturn(packageDetailSRO);
    
        CacheManager.getInstance().setCache(mslabMappingCache);
    
        ReflectionTestUtils.setField(mslabMappingCache, "slabCache", slabCache);
        mruleService = mock(IRulesService.class);
        List<BlockParams> lblockParams = new ArrayList<BlockParams>();
        Block block = createBlock("SubcategoryPackagingRuleBlock");
        BlockParams blockParams = createBlockParams(block,"","");
        lblockParams.add(blockParams);
        mexclusiveService = mock(PackmanExclusiveServiceImpl.class);
        when(mruleService.getBlockParamsByBlockName("SubcategoryPackagingRuleBlock")).thenReturn(lblockParams);
        ReflectionTestUtils.setField(mruleEngineService, "ruleService", mruleService);
        when(mexclusiveService.fetchLabelsForStore("Snapdeal")).thenReturn(labels);
        ReflectionTestUtils.setField(mruleEngineService, "exclusiveService", mexclusiveService);
        mDepedencyForDTO = mock(com.snapdeal.packman.dto.SlabCacheDTO.IDependency.class);
    
        when(mDepedencyForDTO.getBooleanScalar(Property.USE_PRIMARY_LBH)).thenReturn(false);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_LBH)).thenReturn(false);
    
        ReflectionTestUtils.setField(slabCacheDTO, "dep", mDepedencyForDTO);
    
        GetPackagingRecommendationsResponse resp = packmanLandingService.getPackagingRecommendations(request);
        assertTrue(resp.isSuccessful());
        assertEquals("CARTON", resp.getPackagingRecommendationList().get(0).getPackagingType().getType());
        assertEquals("Normal", resp.getPackagingRecommendationList().get(0).getPackagingMode());
        assertEquals("CB101", resp.getPackagingRecommendationList().get(0).getPreferredPackagingTypeItem().getCode());
    }
    
    //TEST 22
    @Test
    public void testGetPackagingRecommendationsNormalAtSubcatOnlyBrandExist() throws PackagingRecomendationException, ExternalDataNotFoundException {
    
        List<String> labels = new ArrayList<String>();
        labels.add(Labels.Premium.toString());
        labels.add(Labels.Designer.toString());
        labels.add(Labels.Luxury.toString());
        mExternalServiceFactory = mock(ExternalServiceFactory.class);
        ICAMSExternalService miCAMSExternalService = mock(ICAMSExternalService.class);
        GetPOGDetailListResponse response = new GetPOGDetailListResponse();
        ProductBrandDetailSRO brand = new ProductBrandDetailSRO();
        brand.setEnabled(true);
        brand.setName("Puma");
    
        POGDetailSRO pogDetailSROs = new POGDetailSRO();
        pogDetailSROs.setEnabled(true);
        List<POGDetailSRO> lpogDetailSROs = new ArrayList<POGDetailSRO>();
        lpogDetailSROs.add(pogDetailSROs);
        response.setPogDetailSROs(lpogDetailSROs);
        response.setSuccessful(true);
        when(mExternalServiceFactory.getCAMSExternalService()).thenReturn(miCAMSExternalService);
        when(miCAMSExternalService.getPOGDetailsBbySupc(Matchers.<GetPOGDetailListBySUPCListRequest> any())).thenReturn(response);
        ReflectionTestUtils.setField(mruleEngineService, "factory", mExternalServiceFactory);
        PackagableItemSRO packagableItemSRO = createPackagableItemSRO(labels);
        lPackagableItemSRO.add(packagableItemSRO);
        packageDetailSRO.setPackagableItemList(lPackagableItemSRO);
        genericMao = mock(IGenericMao.class);
        ruleList = new ArrayList<>();
        List<CriteriaUnit> lCriteriaUnit = new ArrayList<CriteriaUnit>();
        CriteriaUnit criteriaUnit = createCriteriaUnit(1,"BRAND","puma");
        lCriteriaUnit.add(criteriaUnit);
        PackmanRuleUnit packmanRuleUnit = createPackmanRuleUnit(1,"ru01",4,"CARTON", lCriteriaUnit);
        ruleList.add(packmanRuleUnit);
        when(genericMao.getDocumentList(Matchers.<CocofsMongoQuery> any(), Matchers.<Class<PackmanRuleUnit>> any())).thenReturn(ruleList);
        ReflectionTestUtils.setField(mruleEngineService, "genericMao", genericMao);
    
        SlabMappingCache mslabMappingCache = new SlabMappingCache();
        Map<String, NavigableMap<Double, SlabCacheDTO>> slabCache = new HashMap<String, NavigableMap<Double, SlabCacheDTO>>();
        SlabCacheDTO slabCacheDTO = new SlabCacheDTO();
        List<SlabItemDTO> boxes = new ArrayList<SlabItemDTO>();
        SlabItemDTO slabItemDTO = createSlabItemDto("CB01",20.0,10.0,10.0,200.0,"BOX 1");
        boxes.add(slabItemDTO);
        
        slabItemDTO = createSlabItemDto("CB02",40.0,10.0,10.0,400.0,"BOX 2");
        boxes.add(slabItemDTO);
        slabCacheDTO = createSlabCacheDTO(boxes,0,200);

        NavigableMap<Double, SlabCacheDTO> navigableMap = new ConcurrentSkipListMap<Double, SlabCacheDTO>();
        navigableMap.put(99.0, slabCacheDTO);
        slabCache.put("Snapdeal-CARTON", navigableMap);
    
        Map<String, List<PackagingTypeCacheDTO>> mtypeToPackagingTypeDTOsMap = new HashMap<String, List<PackagingTypeCacheDTO>>();
        PackagingTypeCache packagingTypeCache = new PackagingTypeCache();
        List<PackagingTypeCacheDTO> lPackagingTypeCacheDTO = new ArrayList<PackagingTypeCacheDTO>();
        PackagingTypeCacheDTO packagingTypeCacheDTO = createPackagingTypeCacheDTO("Snapdeal","CARTON","Normal");

        lPackagingTypeCacheDTO.add(packagingTypeCacheDTO);
    
        mtypeToPackagingTypeDTOsMap.put("CARTON", lPackagingTypeCacheDTO);
        PackagingType packagingType = createPackagingType("Snapdeal","Normal");
  
        pauSRO = createPauSro("990001",10.0,10.0,10.0,"CARTON");
        List<PackagingType> lpackagingList = new ArrayList<PackagingType>();
        lpackagingList.add(packagingType);
        packagingTypeCache.addPackagingType(lpackagingList);
        CacheManager.getInstance().setCache(packagingTypeCache);
        when(mcocofsExternalService.getProductFulfilmentAttributes(Matchers.<GetProductFulfilmentAttributeRequest> any())).thenReturn(pauSRO);
        when(mcocofsExternalService.getVolumetricWeightFromLBH(10.0, 10.0, 10.0)).thenReturn(100.0);
        ReflectionTestUtils.setField(mPackagingRecommendationService, "cocofsExternalService", mcocofsExternalService);
    
        mSupcPackagingTypeMongoService = mock(ISupcPackagingTypeMongoService.class);
        when(mSupcPackagingTypeMongoService.getEnabledAndActivePackagingTypeBySupcAndStoreCode("Snapdeal", "990001")).thenReturn(null);
        ReflectionTestUtils.setField(mruleEngineService, "supcPackagingTypeMongoService", mSupcPackagingTypeMongoService);
    
        ReflectionTestUtils.setField(packagingTypeCache, "typeToPackagingTypeDTOsMap", mtypeToPackagingTypeDTOsMap);
    
        GetPackagingRecommendationsRequest request = mock(GetPackagingRecommendationsRequest.class);
        ReflectionTestUtils.setField(packmanLandingService, "packagingRecomendationService", mPackagingRecommendationService);
        Protocol proto = Protocol.PROTOCOL_JSON;
        when(request.getRequestProtocol()).thenReturn(proto);
        when(request.getApiKey()).thenReturn("p@(km@n");
        when(request.getStoreFrontId()).thenReturn("SD");
        when(request.getIsShipTogether()).thenReturn(false);
        when(request.getPackageDetail()).thenReturn(packageDetailSRO);
    
        CacheManager.getInstance().setCache(mslabMappingCache);
    
        ReflectionTestUtils.setField(mslabMappingCache, "slabCache", slabCache);
        mruleService = mock(IRulesService.class);
        List<BlockParams> lblockParams = new ArrayList<BlockParams>();
        Block block = createBlock("SubcategoryPackagingRuleBlock");
        BlockParams blockParams = createBlockParams(block,"BRAND","Brand");

        lblockParams.add(blockParams);
        mexclusiveService = mock(PackmanExclusiveServiceImpl.class);
        when(mruleService.getBlockParamsByBlockName("SubcategoryPackagingRuleBlock")).thenReturn(lblockParams);
        ReflectionTestUtils.setField(mruleEngineService, "ruleService", mruleService);
        when(mexclusiveService.fetchLabelsForStore("Snapdeal")).thenReturn(labels);
        ReflectionTestUtils.setField(mruleEngineService, "exclusiveService", mexclusiveService);
        mDepedencyForDTO = mock(com.snapdeal.packman.dto.SlabCacheDTO.IDependency.class);
    
        when(mDepedencyForDTO.getBooleanScalar(Property.USE_PRIMARY_LBH)).thenReturn(false);
        when(mDepedencyForDTO.getBooleanScalar(Property.PACKMAN_RECO_BASED_ON_LBH)).thenReturn(false);
    
        ReflectionTestUtils.setField(slabCacheDTO, "dep", mDepedencyForDTO);    
        ReflectionTestUtils.setField(slabCacheDTO, "dep", mDepedencyForDTO);
    
        GetPackagingRecommendationsResponse resp = packmanLandingService.getPackagingRecommendations(request);
        assertTrue(resp.isSuccessful());
        assertEquals("CARTON", resp.getPackagingRecommendationList().get(0).getPackagingType().getType());
        assertEquals("Normal", resp.getPackagingRecommendationList().get(0).getPackagingMode());
        assertEquals("CB01", resp.getPackagingRecommendationList().get(0).getPreferredPackagingTypeItem().getCode());
    }
    
    // BatchSizeLessThenAllowd
    @Test
    public void testGetPackagingRecommendationsWhenBatchSizeLessThenAllowd() throws PackagingRecomendationException {
        
        List<String> labels = new ArrayList<String>();        
        PackagableItemSRO packagableItemSRO = createPackagableItemSRO(labels);
        lPackagableItemSRO.add(packagableItemSRO);
        packageDetailSRO.setPackagableItemList(lPackagableItemSRO);    
        GetPackagingRecommendationsRequest request = mock(GetPackagingRecommendationsRequest.class);
        ReflectionTestUtils.setField(packmanLandingService, "packagingRecomendationService", mPackagingRecommendationService);
        when(mDependencyCoCo.getIntegerScalar(Property.PACKMAN_API_BATCH_SIZE)).thenReturn(Integer.parseInt("0"));
        ReflectionTestUtils.setField(packmanLandingService, "dep", mDependencyCoCo);
        Protocol proto = Protocol.PROTOCOL_JSON;
        when(request.getRequestProtocol()).thenReturn(proto);
        when(request.getApiKey()).thenReturn("p@(km@n");
        when(request.getStoreFrontId()).thenReturn("SD");
        when(request.getIsShipTogether()).thenReturn(true);
        when(request.getPackageDetail()).thenReturn(packageDetailSRO);
        GetPackagingRecommendationsResponse resp = packmanLandingService.getPackagingRecommendations(request);
        assertFalse(resp.isSuccessful());
    }
    
    //IsShipTogetherTrue
    @Test
    public void testGetPackagingRecommendationsIsShipTogetherTrue() throws PackagingRecomendationException {
        List<String> labels = new ArrayList<String>();        
        PackagableItemSRO packagableItemSRO = createPackagableItemSRO(labels);
        lPackagableItemSRO.add(packagableItemSRO);
        packageDetailSRO.setPackagableItemList(lPackagableItemSRO);    
        GetPackagingRecommendationsRequest request = mock(GetPackagingRecommendationsRequest.class);
        ReflectionTestUtils.setField(packmanLandingService, "packagingRecomendationService", mPackagingRecommendationService);    
        Protocol proto = Protocol.PROTOCOL_JSON;
        when(request.getRequestProtocol()).thenReturn(proto);
        when(request.getApiKey()).thenReturn("p@(km@n");
        when(request.getStoreFrontId()).thenReturn("SD");
        when(request.getIsShipTogether()).thenReturn(true);
        when(request.getPackageDetail()).thenReturn(packageDetailSRO);
        GetPackagingRecommendationsResponse resp = packmanLandingService.getPackagingRecommendations(request);
        assertFalse(resp.isSuccessful());
    }
    //IsShipTogetherNULL
    @Test
    public void testGetPackagingRecommendationsIsShipTogetherNULL() throws PackagingRecomendationException {
        List<String> labels = new ArrayList<String>();        
        PackagableItemSRO packagableItemSRO = createPackagableItemSRO(labels);
        lPackagableItemSRO.add(packagableItemSRO);
        packageDetailSRO.setPackagableItemList(lPackagableItemSRO);    
        GetPackagingRecommendationsRequest request = mock(GetPackagingRecommendationsRequest.class);
        ReflectionTestUtils.setField(packmanLandingService, "packagingRecomendationService", mPackagingRecommendationService);    
        Protocol proto = Protocol.PROTOCOL_JSON;
        when(request.getRequestProtocol()).thenReturn(proto);
        when(request.getApiKey()).thenReturn("p@(km@n");
        when(request.getStoreFrontId()).thenReturn("SD");
        when(request.getIsShipTogether()).thenReturn(null);
        when(request.getPackageDetail()).thenReturn(packageDetailSRO);
        GetPackagingRecommendationsResponse resp = packmanLandingService.getPackagingRecommendations(request);
        assertFalse(resp.isSuccessful());
    }
    
    //IsShipTogethereNotSet
    @Test
    public void testGetPackagingRecommendationsIsShipTogethereNotSet() {
        List<String> labels = new ArrayList<String>();        
        PackagableItemSRO packagableItemSRO = createPackagableItemSRO(labels);
        lPackagableItemSRO.add(packagableItemSRO);
        packageDetailSRO.setPackagableItemList(lPackagableItemSRO);   
        GetPackagingRecommendationsRequest request = mock(GetPackagingRecommendationsRequest.class);
        Protocol proto = Protocol.PROTOCOL_JSON;
        when(request.getRequestProtocol()).thenReturn(proto);
        when(request.getApiKey()).thenReturn("p@(km@n");
        when(request.getStoreFrontId()).thenReturn("SD");
        when(request.getPackageDetail()).thenReturn(packageDetailSRO);
        GetPackagingRecommendationsResponse resp = packmanLandingService.getPackagingRecommendations(request);
        assertFalse("resp ", resp.isSuccessful());
    }
    //With Wrong Api Key
    @Test
    public void testGetPackagingRecommendationsApiKeyWrong() throws PackagingRecomendationException {
        List<String> labels = new ArrayList<String>();        
        PackagableItemSRO packagableItemSRO = createPackagableItemSRO(labels);
        lPackagableItemSRO.add(packagableItemSRO);
        packageDetailSRO.setPackagableItemList(lPackagableItemSRO);    
        GetPackagingRecommendationsRequest request = mock(GetPackagingRecommendationsRequest.class);
        ReflectionTestUtils.setField(packmanLandingService, "packagingRecomendationService", mPackagingRecommendationService);
        Protocol proto = Protocol.PROTOCOL_JSON;
        when(request.getRequestProtocol()).thenReturn(proto);
        when(request.getApiKey()).thenReturn("p@(km@");
        when(request.getStoreFrontId()).thenReturn("SD");
        when(request.getIsShipTogether()).thenReturn(false);
        when(request.getPackageDetail()).thenReturn(packageDetailSRO);
        GetPackagingRecommendationsResponse resp = packmanLandingService.getPackagingRecommendations(request);
        assertFalse(resp.isSuccessful());
    }  
    
    private SlabItemDTO createSlabItemDto(String code,Double length,Double breadth,Double height,Double volWeight,String description) 
    {
        SlabItemDTO slabItemDTO = new SlabItemDTO(); 
        slabItemDTO.setCode(code);
        slabItemDTO.setLength(length);
        slabItemDTO.setBreadth(breadth);
        slabItemDTO.setHeight(height);
        slabItemDTO.setVolWeight(volWeight);
        slabItemDTO.setDescription(description);
        return slabItemDTO;
        
    }
    
    private PackmanRuleUnit createPackmanRuleUnit(Integer id,String ruleName,Integer priority,String returnType,List<CriteriaUnit> lCriteriaUnit)
    {
        PackmanRuleUnit packmanRuleUnit = new PackmanRuleUnit();
        packmanRuleUnit.setCreated(DateUtils.getCurrentDate());
        packmanRuleUnit.setStartDate(DateUtils.getCurrentDate());
        packmanRuleUnit.setCreatedBy("tech");
        packmanRuleUnit.setEnabled(true);
        packmanRuleUnit.setEndDate(DateUtils.getDateAfterGivenWorkingDays(DateUtils.getCurrentDate(), 2));
        packmanRuleUnit.setId(id);
        packmanRuleUnit.setName(ruleName);
        packmanRuleUnit.setPriority(priority);
        packmanRuleUnit.setReturnValue(returnType);
        packmanRuleUnit.setParamValue(returnType);
        packmanRuleUnit.setCriteria(lCriteriaUnit);
        return packmanRuleUnit;
        
    }
    
    private CriteriaUnit createCriteriaUnit(Integer i, String name, String value) {
        CriteriaUnit criteriaUnit = new CriteriaUnit();
        criteriaUnit.setId(1);
        criteriaUnit.setEnabled(true);
        criteriaUnit.setName(name);
        criteriaUnit.setOperator("eq");
        criteriaUnit.setValue(value);
        criteriaUnit.setValueType("STRING");
        return criteriaUnit;
    }
    
   private PackagingTypeCacheDTO createPackagingTypeCacheDTO(String storeCode,String packagingType,String packagingMode)
   {
       PackagingTypeCacheDTO packagingTypeCacheDTO = new PackagingTypeCacheDTO();
       packagingTypeCacheDTO.setStoreCode(storeCode);
       packagingTypeCacheDTO.setType(packagingType);
       packagingTypeCacheDTO.setPackagingMode(packagingMode);
       return packagingTypeCacheDTO;
       
   }
   
   private ProductFulfilmentAttributeSRO createPauSro(String supc,Double length,Double breadth,Double height,String packagingType)
   {
       ProductFulfilmentAttributeSRO pauSRO = new ProductFulfilmentAttributeSRO();
       pauSRO.setSupc(supc);
       pauSRO.setBreadth(breadth);
       pauSRO.setLength(length);
       pauSRO.setHeight(height);
       pauSRO.setPackagingType(packagingType);
       return pauSRO;
       
   }
   
   private Block createBlock(String type)
   {
       Block block = new Block();
       block.setName(type);
       block.setCreated(DateUtils.getCurrentDate());
       return block;
       
   }
   
   private BlockParams createBlockParams(Block block,String type,String value)
   {
       BlockParams blockParams = new BlockParams();
       blockParams.setBlock(block);
       blockParams.setParamName(type);
       blockParams.setFieldValue(value);
       blockParams.setId(5);
       return blockParams;
       
   }
   
   private SlabCacheDTO createSlabCacheDTO(List<SlabItemDTO> boxes,Integer lowerLimit,Integer slabSize)
   {
       SlabCacheDTO slabCacheDTO = new SlabCacheDTO();
       slabCacheDTO.setBoxes(boxes);
       slabCacheDTO.setLowerLimit(lowerLimit);
       slabCacheDTO.setSlabSize(slabSize);
       return slabCacheDTO;
   }
   
   private PackagableItemSRO createPackagableItemSRO(List<String> labels)
   {
       PackagableItemSRO packagableItemSRO = new PackagableItemSRO();
       packagableItemSRO.setSupc("990001");
       packagableItemSRO.setBrand("puma");
       packagableItemSRO.setQuantity(10);
       packagableItemSRO.setSellerCode("SC1");
       packagableItemSRO.setLabels(labels);
       packagableItemSRO.setSubCategory("Subcat");
       packagableItemSRO.setCategory("Category");
      return packagableItemSRO;
   }
   
   private PackagingType createPackagingType(String storeCode,String packagingMode)
   {
       PackagingType packagingType = new PackagingType();
       Store store = new Store();
       store.setCode(storeCode);
       packagingType.setPackagingMode(packagingMode);
       packagingType.setStore(store);
       return packagingType;
       
   }     
}
