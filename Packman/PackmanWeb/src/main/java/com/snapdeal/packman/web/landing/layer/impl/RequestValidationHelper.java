package com.snapdeal.packman.web.landing.layer.impl;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.snapdeal.base.annotations.NotNull;
import com.snapdeal.base.model.common.ServiceRequest;
import com.snapdeal.base.utils.CollectionUtils;
import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.packman.web.landing.layer.IRequestValidationHelper;

/**
 * @author ankur
 */
@Service("packmanRequestValidationHelper")
public class RequestValidationHelper implements IRequestValidationHelper {

    private static final Logger LOG = LoggerFactory.getLogger(RequestValidationHelper.class);

    @Override
    public <T> List<String> checkForNullFields(T t) throws IllegalArgumentException, IllegalAccessException {

        List<String> nullFieldList = new ArrayList<String>();
        if (t == null) {
            nullFieldList.add(ServiceRequest.class.getName());
        }

        for (Field field : t.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            if (field.isAnnotationPresent(NotNull.class)) {
                if (!field.getType().isPrimitive()) {

                    if (field.get(t) == null || (field.get(t) != null && field.getType().equals(String.class) && StringUtils.isEmpty((String) field.get(t)))) {

                        nullFieldList.add(field.getName());
                    } else {
                        nullFieldList.addAll(checkForInnerObjects(field.get(t), field));
                    }
                }
            }
        }
        return nullFieldList;
    }

    private <T> List<String> checkForInnerObjects(T t, Field field) throws IllegalArgumentException, IllegalAccessException {
        List<String> nullFieldList = new ArrayList<String>();
        if (t instanceof List) {
            if (CollectionUtils.isEmpty((List<?>) t)) {
                nullFieldList.add(field.getName());
                return nullFieldList;
            } else {
                for (Object obj : (List<?>) t) {
                    if (null == obj) {
                        nullFieldList.add(field.getName());
                        return nullFieldList;
                    }
                    nullFieldList.addAll(checkForNullFields(obj));
                }
            }
        } else if (t instanceof Set) {
            if (CollectionUtils.isEmpty((Set<?>) t)) {
                nullFieldList.add(field.getName());
                return nullFieldList;
            } else {
                for (Object obj : (Set<?>) t) {
                    if (nullFieldList == obj) {
                        nullFieldList.add(field.getName());
                        return nullFieldList;
                    }
                    nullFieldList.addAll(checkForNullFields(obj));
                }
            }
        } else if (t instanceof Map) {
            if (((Map<?, ?>) t).isEmpty()) {
                nullFieldList.add(field.getName());
                return nullFieldList;
            } else {
                for (Entry<?, ?> entry : ((Map<?, ?>) t).entrySet()) {
                    nullFieldList.addAll(checkForNullFields(entry.getKey()));
                    if (null != entry.getValue()) {
                        nullFieldList.addAll(checkForNullFields(entry.getValue()));
                    }
                }
            }
        } else {
            nullFieldList.addAll(checkForNullFields(t));
        }
        return nullFieldList;

    }

}
