/**
 * *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.packman.web.landing.layer.impl;

import java.util.ArrayList;
import java.util.List;

import com.snapdeal.packman.common.sro.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.base.cache.CacheManager;
import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.base.validation.ValidationError;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.packman.api.getAllSurfacePackagingTypes.v0.GetAllSurfacePackagingTypesRequest;
import com.snapdeal.packman.api.getAllSurfacePackagingTypes.v0.GetAllSurfacePackagingTypesResponse;
import com.snapdeal.packman.api.getPackagingRecommendation.v0.CartonRecommendationSRO;
import com.snapdeal.packman.api.getPackagingRecommendation.v0.GetPackagingRecommendationRequest;
import com.snapdeal.packman.api.getPackagingRecommendation.v0.GetPackagingRecommendationResponse;
import com.snapdeal.packman.api.getPackagingRecommendation.v0.PolybagRecommendationSRO;
import com.snapdeal.packman.api.getPackagingRecommendation.v0.SurfaceRecommendationSRO;
import com.snapdeal.packman.cache.PackagingTypeCache;
import com.snapdeal.packman.common.ApiErrorCode;
import com.snapdeal.packman.common.PackagingType;
import com.snapdeal.packman.dto.AirPackagingRecommendationDTO;
import com.snapdeal.packman.dto.PackagingTypeCacheDTO;
import com.snapdeal.packman.enums.PackagingMode;
import com.snapdeal.packman.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.packman.request.GetPackagingRecommendationsRequest;
import com.snapdeal.packman.response.GetPackagingRecommendationsResponse;
import com.snapdeal.packman.services.exception.PackagingRecomendationException;
import com.snapdeal.packman.services.packaging.IPackagingRecommendation;
import com.snapdeal.packman.services.packaging.impl.PackagingRecommendationServiceImpl;
import com.snapdeal.packman.services.packaging.recomendation.IPackagingRecommendationService;
import com.snapdeal.packman.web.builder.IResponseBuilder;
import com.snapdeal.packman.web.landing.layer.IPackmanLandingService;
import com.snapdeal.packman.web.landing.layer.IRequestValidationService;
import com.snapdeal.score.model.ShippingModeType;

/**
 * @version 1.0, 08-Sep-2015
 * @author ankur/indrajit
 */

@Service("packmanLandingService")
public class PackmanLandingServiceImpl implements IPackmanLandingService {

    @Autowired
    private IResponseBuilder          responseBuilder;

    @Autowired
    private IRequestValidationService requestValidationService;

    private static final Logger       LOG = LoggerFactory.getLogger(PackagingRecommendationServiceImpl.class);

    @Autowired
    IPackagingRecommendation          packagingRecommendationService;

    @Autowired
    IPackagingRecommendationService   packagingRecomendationService;


    private IDependency               dep = new Dependency();

    @Override
    public GetPackagingRecommendationResponse getPackagingRecommendations(GetPackagingRecommendationRequest request) {
        LOG.info("Request received {}", request);
        GetPackagingRecommendationResponse response = new GetPackagingRecommendationResponse();
        response.setProtocol(request.getResponseProtocol());

        ValidationError error = requestValidationService.requestBasicFieldValidation(request);
        if (error != null) {
            LOG.error("Validation error found request for supc {}", request != null ? request.getSupc() : null);
            responseBuilder.buildFailedResponse(error, response);
            return response;
        }

        String apiKey = dep.getStringScalar(Property.PACKAGING_RECOMMENDATION_API_KEY);
        if (!apiKey.equals(request.getApiKey())) {
            LOG.error("Invalid apiKey {} passed in request for supc {}", request.getApiKey(), request.getSupc());
            responseBuilder.buildFailedResponse(new ValidationError(ApiErrorCode.INVALID_API_KEY.code(), "invalid api authentication key "), response);
            return response;
        }

        try {
            response.setSuccessful(true);
            if (ShippingModeType.AIR.getCode().equalsIgnoreCase(request.getShipMode())) {
                getRecommendationsForAir(request, response);
            } else if (ShippingModeType.SURFACE.getCode().equalsIgnoreCase(request.getShipMode())) {

                SurfacePackagingSRO surfaceSRO = packagingRecommendationService.getRecommendationsForSurfaceShipMode(request.getSupc(), request.getCourierCode(),
                        request.getShipToAddress(), request.getShipFromAddress());
                SurfaceRecommendationSRO surfaceRecommendation = new SurfaceRecommendationSRO();
                surfaceRecommendation.setSurfaceType(surfaceSRO);

                response.setPackagingType(PackagingType.SURFACE.getCode());
                response.setSurfaceRecommendation(surfaceRecommendation);

            } else {

                LOG.error("invalid shipMode :" + request.getShipMode() + "for supc :" + request.getSupc());
                responseBuilder.buildFailedResponse(ApiErrorCode.REQUESTED_DATA_DOES_NOT_EXISTS, response);

            }

        } catch (ExternalDataNotFoundException e) {
            LOG.error("No packaging recommnedations found for request supc:" + request.getSupc());
            responseBuilder.buildFailedResponse(ApiErrorCode.REQUESTED_DATA_DOES_NOT_EXISTS, response);
        } catch (Exception e) {
            LOG.error("unexpected error occured while trying to get recommendations info  for request supc:" + request.getSupc() + " shipMode:" + request.getShipMode(), e);
            responseBuilder.buildFailedResponse(ApiErrorCode.INTERNAL_ERROR, response);
        }

        LOG.info("Sending Response {}", response);

        return response;

    }

    private void getRecommendationsForAir(GetPackagingRecommendationRequest request, GetPackagingRecommendationResponse response) throws ExternalDataNotFoundException {

        AirPackagingRecommendationDTO dto = packagingRecommendationService.getRecommendationsForAirShipMode(request.getSupc());

        if (dto == null || StringUtils.isEmpty(dto.getPackagingType())) {
            LOG.error("No info found for requested supc {}", request != null ? request.getSupc() : null);
            responseBuilder.buildFailedResponse(ApiErrorCode.REQUESTED_DATA_DOES_NOT_EXISTS, response);
            return;
        }

        response.setPackagingType(dto.getPackagingType());

        if (PackagingType.CARTON.getCode().equals(dto.getPackagingType())) {
            CartonRecommendationSRO cartonRecommendation = new CartonRecommendationSRO();
            List<CartonSRO> cartonList = new ArrayList<CartonSRO>();
            for (String code : dto.getRecommedationList()) {
                cartonList.add(new CartonSRO(code));
            }
            cartonRecommendation.setRecommendedCartons(cartonList);
            cartonRecommendation.setPreferredCarton(new CartonSRO(dto.getMostPreferredBox()));
            response.setCartonRecommendation(cartonRecommendation);

        } else if (PackagingType.POLYBAG.getCode().equals(dto.getPackagingType())) {
            PolybagRecommendationSRO polybagRecommendation = new PolybagRecommendationSRO();
            List<PolybagSRO> polybagList = new ArrayList<PolybagSRO>();
            for (String code : dto.getRecommedationList()) {
                polybagList.add(new PolybagSRO(code));
            }
            polybagRecommendation.setRecommendedPolybags(polybagList);
            polybagRecommendation.setPreferredPolybag(new PolybagSRO(dto.getMostPreferredBox()));
            response.setPolybagRecommendation(polybagRecommendation);
        } else {
            LOG.info("invalid packaging type {} for supc {}", dto.getPackagingType(), request.getSupc());
            responseBuilder.buildFailedResponse(ApiErrorCode.REQUESTED_DATA_DOES_NOT_EXISTS, response);
        }

    }

    @Override
    public GetAllSurfacePackagingTypesResponse getAllSurfacePackagingTypes(GetAllSurfacePackagingTypesRequest req) {
        LOG.info("Request received {}", req);

        GetAllSurfacePackagingTypesResponse response = new GetAllSurfacePackagingTypesResponse();

        String storeCode = ConfigUtils.getStringScalar(Property.SNAPDEAL_STORE_CODE);
        List<PackagingTypeCacheDTO> dtoList = CacheManager.getInstance().getCache(PackagingTypeCache.class).getPackagingTypeForStoreAndPackagingMode(storeCode,
                PackagingMode.PICKNPACK.mode());
        List<SurfacePackagingSRO> sroList = new ArrayList<SurfacePackagingSRO>();
        for (PackagingTypeCacheDTO ptType : dtoList) {
            SurfacePackagingSRO sro = new SurfacePackagingSRO(ptType.getType(), ptType.getDescription());
            sroList.add(sro);
        }
        response.setSurfacePackagingTypes(sroList);
        response.setSuccessful(true);
        response.setProtocol(req.getResponseProtocol());
        LOG.info("Returned SurfacePackagingTypes List  of size {}", (sroList != null) ? sroList.size() : 0);
        LOG.info("Sending Response {}", response);
        return response;
    }

    @Override
    public GetPackagingRecommendationsResponse getPackagingRecommendations(GetPackagingRecommendationsRequest request) {
        LOG.info("Request received {}", request);
        List<PackagingRecommendationSRO> packagingRecommendationList = null;
        GetPackagingRecommendationsResponse response = new GetPackagingRecommendationsResponse();
        response.setProtocol(request.getResponseProtocol());
        Integer allowedBatchSize = dep.getIntegerScalar(Property.PACKMAN_API_BATCH_SIZE);
        ValidationError error = requestValidationService.requestBasicFieldValidation(request);
        if (error != null) {
            LOG.error("Validation error found for request " + request.toString());

            responseBuilder.buildFailedResponse(error, response);
            LOG.info("Sending Response {}", response);
            return response;
        }
        String apiKey = dep.getStringScalar(Property.PACKAGING_RECOMMENDATION_API_KEY);
        if (!apiKey.equals(request.getApiKey())) {
            LOG.error("Invalid apiKey {} passed in request " + request.toString(), request.getApiKey());
            responseBuilder.buildFailedResponse(new ValidationError(ApiErrorCode.INVALID_API_KEY.code(), "invalid api authentication key "), response);
            LOG.info("Sending Response {}", response);
            return response;
        }
        if (request.getPackageDetail().getPackagableItemList().size() > allowedBatchSize) {
            LOG.error("batch size limit crossed in request " + request.toString());
            responseBuilder.buildFailedResponse(ApiErrorCode.REQUEST_BATCH_SIZE_LIMIT_EXCEEDED, response);
            LOG.info("Sending Response {}", response);
            responseBuilder.buildFailedResponse(new ValidationError(ApiErrorCode.REQUEST_BATCH_SIZE_LIMIT_EXCEEDED.code(), "Allowed batch size limit is breached "), response);
            return response;

        }
        if(!isQuantityFieldValid(request)){
            LOG.error("quanity specified is invalid in request " + request.toString());
            responseBuilder.buildFailedResponse(ApiErrorCode.INVALID_PARAMETERS, response);
            LOG.info("Sending Response {}", response);
            return response;
        }
        try {
            packagingRecommendationList = packagingRecomendationService.getRecommendation(request.getStoreFrontId(), request.getIsShipTogether(), request.getPackageDetail());
            response.setPackagingRecommendationList(packagingRecommendationList);

            responseBuilder.buildSuccessfulResponse(response);

        } catch (PackagingRecomendationException ex) {
            LOG.error("Error has come", ex);
            responseBuilder.buildFailedResponse(new ValidationError(ex.getErrorCode().code(), ex.getErrorCode().getMsg()), response);
        } catch (Exception ex) {
            LOG.error("Error has come", ex);
            responseBuilder.buildFailedResponse(new ValidationError(ApiErrorCode.UNKNOWN_ERROR.code(), "Internal Error "), response);

        }
        LOG.info("Sending Response {}", response);

        return response;
    }

    private boolean isQuantityFieldValid(GetPackagingRecommendationsRequest request) {
        for(PackagableItemSRO product : request.getPackageDetail().getPackagableItemList()){
            if(product.getQuantity()<1){
                return false;
            }
        }
        return true;
    }

    public interface IDependency {
        String getStringScalar(Property property);

        int getIntegerScalar(Property property);
    }

    static class Dependency implements IDependency {
        @Override
        public String getStringScalar(Property property) {
            return ConfigUtils.getStringScalar(property);
        }

        @Override
        public int getIntegerScalar(Property property) {
            return ConfigUtils.getIntegerScalar(property);
        }
    }

}
