package com.snapdeal.packman.web.landing.layer;

import com.snapdeal.base.validation.ValidationError;

public interface IRequestValidationService {

    public <T> ValidationError requestBasicFieldValidation(T request);

    public <T, Z> ValidationError requestBatchSizeValidation(T request, Z collection);
}