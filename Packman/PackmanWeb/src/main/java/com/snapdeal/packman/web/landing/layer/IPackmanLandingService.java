/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.web.landing.layer;

import com.snapdeal.packman.api.getAllSurfacePackagingTypes.v0.GetAllSurfacePackagingTypesRequest;
import com.snapdeal.packman.api.getAllSurfacePackagingTypes.v0.GetAllSurfacePackagingTypesResponse;
import com.snapdeal.packman.api.getPackagingRecommendation.v0.GetPackagingRecommendationRequest;
import com.snapdeal.packman.api.getPackagingRecommendation.v0.GetPackagingRecommendationResponse;
import com.snapdeal.packman.request.GetPackagingRecommendationsRequest;
import com.snapdeal.packman.response.GetPackagingRecommendationsResponse;

/**
 *  
 *  @version     1.0, 08-Sep-2015
 *  @author ankur
 */
public interface IPackmanLandingService {

    GetPackagingRecommendationResponse getPackagingRecommendations(GetPackagingRecommendationRequest request);

    GetAllSurfacePackagingTypesResponse getAllSurfacePackagingTypes(GetAllSurfacePackagingTypesRequest req);
    
    GetPackagingRecommendationsResponse getPackagingRecommendations(GetPackagingRecommendationsRequest request);

}
