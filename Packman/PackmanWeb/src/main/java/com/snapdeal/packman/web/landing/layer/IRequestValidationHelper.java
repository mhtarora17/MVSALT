package com.snapdeal.packman.web.landing.layer;

import java.util.List;

public interface IRequestValidationHelper {

    public <T> List<String> checkForNullFields(T t) throws IllegalArgumentException, IllegalAccessException;

    

}
