package com.snapdeal.packman.web.landing.layer.impl;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.base.validation.ValidationError;
import com.snapdeal.packman.common.ApiErrorCode;
import com.snapdeal.packman.web.landing.layer.IRequestValidationHelper;
import com.snapdeal.packman.web.landing.layer.IRequestValidationService;

/**
 * @author ankur
 */
@Service("packmanRequestValidationService")
public class RequestValidationServiceImpl implements IRequestValidationService {

    private static final Logger LOG = LoggerFactory.getLogger(RequestValidationServiceImpl.class);

    @Autowired
    IRequestValidationHelper    requestValidationHelper;

    @Override
    public <T> ValidationError requestBasicFieldValidation(T request) {
        try {
            List<String> fieldList = requestValidationHelper.checkForNullFields(request);
            if (fieldList.size() > 0) {
                LOG.warn("Null fields {} found in the " + (request != null ? request.getClass().getName() : "request"), fieldList.toString());
                return new ValidationError(ApiErrorCode.REQUIRED_FIELD_NULL.code(), makeup(fieldList) + " found to be null");
            }


        } catch (Exception e) {
            LOG.error("Error in validating request for {}", request.toString());
            return new ValidationError(ApiErrorCode.UNKNOWN_ERROR.code(), "Unknown error");
        }
        // no validation error found
        return null;
    }

    @Override
    public <T, Z> ValidationError requestBatchSizeValidation(T request, Z collectionType) {
        /**
         * under construction, will be built soon :)
         */
        return null;
    }

    private String makeup(List<String> fieldList) {

        if (fieldList != null) {
            switch (fieldList.size()) {
                case 1:
                    return fieldList.get(0).toString();
                default:
                    return fieldList.toString();
            }
        } else {
            return "";
        }
    }

}
