package com.snapdeal.packman.web.builder;

import com.snapdeal.base.model.common.ServiceResponse;
import com.snapdeal.base.validation.ValidationError;
import com.snapdeal.packman.common.ApiErrorCode;

/**
 * @author nikhil
 */
public interface IResponseBuilder {

    
    public <T extends ServiceResponse> void buildFailedResponse(ApiErrorCode errorCode, T response);

  
    public <T extends ServiceResponse> void buildFailedResponse(ValidationError error, T response);
    
    public <T extends ServiceResponse> void buildSuccessfulResponse(T response);
    
    public <T extends ServiceResponse> void buildFailedResponse(T response, ApiErrorCode errorCode);
    
  
}
