package com.snapdeal.packman.web.builder.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.snapdeal.base.model.common.ServiceResponse;
import com.snapdeal.base.validation.ValidationError;
import com.snapdeal.packman.common.ApiErrorCode;
import com.snapdeal.packman.enums.ServiceResponseCode;
import com.snapdeal.packman.web.builder.IResponseBuilder;

/**
 * @author ankur
 */
@Service("packmanResponseBuilder")
public class ResponseBuilderImpl implements IResponseBuilder {

   

   
    @Override
    public <T extends ServiceResponse> void buildFailedResponse(ValidationError error, T response) {
        List<ValidationError> validationErrors = new ArrayList<ValidationError>();
        response.setSuccessful(false);
        response.setMessage(error.getMessage());
        validationErrors.add(error);
        response.setValidationErrors(validationErrors);
    }

    @Override
    public <T extends ServiceResponse> void buildFailedResponse(ApiErrorCode errorCode, T response) {
        response.setSuccessful(false);
        List<ValidationError> validationErrors = new ArrayList<ValidationError>();
        if (errorCode.equals(ApiErrorCode.REQUESTED_DATA_DOES_NOT_EXISTS)) {
            String responseMessage = "No information exists for the requested params";
            response.setMessage(responseMessage);
            validationErrors.add(new ValidationError(ApiErrorCode.REQUESTED_DATA_DOES_NOT_EXISTS.code(), responseMessage));
            response.setValidationErrors(validationErrors);
        }else if(errorCode.equals(ApiErrorCode.REQUEST_BATCH_SIZE_LIMIT_EXCEEDED)){
            String responseMessage = "requested items are more than the  allowed batch size";
            response.setMessage(responseMessage);
            validationErrors.add(new ValidationError(ApiErrorCode.REQUEST_BATCH_SIZE_LIMIT_EXCEEDED.code(), responseMessage));
            response.setValidationErrors(validationErrors);
        }
        
    }

    @Override
    public <T extends ServiceResponse> void buildSuccessfulResponse(T response) {
    // TODO Auto-generated method stub
    response.setSuccessful(true);
        response.setCode(ServiceResponseCode.SUCCESSFUL.getCode());
        response.setMessage(ServiceResponseCode.SUCCESSFUL.getMsg());
    
    }

    @Override
    public <T extends ServiceResponse> void buildFailedResponse(T response, ApiErrorCode errorCode) {
    // TODO Auto-generated method stub
    List<ValidationError> validationErrors = new ArrayList<ValidationError>();
    ValidationError error = new ValidationError(errorCode.code(), errorCode.getMsg());
        response.setSuccessful(false);
        validationErrors.add(error);
        response.setValidationErrors(validationErrors);
        response.setCode(errorCode.getResponseCode().getCode());
        response.setMessage(errorCode.getResponseCode().getMsg());
       
    
    }
}