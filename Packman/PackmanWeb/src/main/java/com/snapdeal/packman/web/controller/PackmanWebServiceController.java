/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.packman.web.controller;



import javax.ws.rs.Path;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.snapdeal.packman.api.getAllSurfacePackagingTypes.v0.GetAllSurfacePackagingTypesRequest;
import com.snapdeal.packman.api.getAllSurfacePackagingTypes.v0.GetAllSurfacePackagingTypesResponse;
import com.snapdeal.packman.api.getPackagingRecommendation.v0.GetPackagingRecommendationRequest;
import com.snapdeal.packman.api.getPackagingRecommendation.v0.GetPackagingRecommendationResponse;
import com.snapdeal.packman.request.GetPackagingRecommendationsRequest;
import com.snapdeal.packman.response.GetPackagingRecommendationsResponse;
import com.snapdeal.packman.utils.PackmanConstants;
import com.snapdeal.packman.web.landing.layer.IPackmanLandingService;

/**
 * @version 1.0, 08-Sep-2015
 * @author ankur/indrajit
 */

@Controller
@RequestMapping(PackmanWebServiceController.URL)
@Path(PackmanWebServiceController.URL)
@Profile(PackmanConstants.PACKMAN_PROFILE_NAME)
public class PackmanWebServiceController {

    public static final String URL = "/service/packman/";

    private static final Logger LOG = LoggerFactory.getLogger(PackmanWebServiceController.class);
    @Autowired
    IPackmanLandingService      packmanLandingService;

    @RequestMapping(value = "getPackagingRecommendation", produces = "application/sd-service")
    @ResponseBody
    public GetPackagingRecommendationResponse getPackagingRecommendation(@RequestBody GetPackagingRecommendationRequest request) {
        GetPackagingRecommendationResponse response = null;
        try {
            response = packmanLandingService.getPackagingRecommendations(request);
        } catch (RuntimeException e) {
            LOG.error("getPackagingRecommendation", e);
            throw e;
        }

        return response;
    }
    @RequestMapping(value = "getAllSurfacePackagingTypes", produces = "application/sd-service")
    @ResponseBody
    public GetAllSurfacePackagingTypesResponse getAllSurfacePackagingTypes(@RequestBody GetAllSurfacePackagingTypesRequest req) {
        
        GetAllSurfacePackagingTypesResponse response = null;
        try {
            response = packmanLandingService.getAllSurfacePackagingTypes(req);
        } catch (RuntimeException e) {
            LOG.error("getAllSurfacePackagingTypes", e);
            throw e;
        }

        return response;
    }
    
    @RequestMapping(value = "getPackagingRecommendations", produces = "application/sd-service")
    @ResponseBody
    public GetPackagingRecommendationsResponse getPackagingRecommendations(@RequestBody GetPackagingRecommendationsRequest request) {
	GetPackagingRecommendationsResponse response = null;
        try {
            response = packmanLandingService.getPackagingRecommendations(request);
        } catch (RuntimeException e) {
            LOG.error("getPackagingRecommendations", e);
            throw e;
        }

        return response;
    }

}
