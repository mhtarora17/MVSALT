package com.snapdeal.packman.adminweb.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class PolybagSlabDTO {

    private String id;

    private String slabSize;

    private String slabMin;

    private String slabMax;

    private String polybagBoxes;

    public PolybagSlabDTO() {
        super();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSlabSize() {
        return slabSize;
    }

    public void setSlabSize(String slabSize) {
        this.slabSize = slabSize;
    }

    public String getSlabMin() {
        return slabMin;
    }

    public void setSlabMin(String slabMin) {
        this.slabMin = slabMin;
    }

    public String getSlabMax() {
        return slabMax;
    }

    public void setSlabMax(String slabMax) {
        this.slabMax = slabMax;
    }

    public String getPolybagBoxes() {
        return polybagBoxes;
    }

    public void setPolybagBoxes(String polybagBoxes) {
        this.polybagBoxes = polybagBoxes;
    }

    @Override
    public String toString() {
        return "PolybagSlabDTO [id=" + id + ", slabSize=" + slabSize + ", slabMin=" + slabMin + ", slabMax=" + slabMax + ", polybagBoxes=" + polybagBoxes + "]";
    }

    

}
