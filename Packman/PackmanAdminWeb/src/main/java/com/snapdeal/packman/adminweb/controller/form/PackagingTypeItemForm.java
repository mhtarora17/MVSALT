/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.adminweb.controller.form;


/**
 *  
 *  @version     1.0, 07-Dec-2015
 *  @author brijesh
 */
public class PackagingTypeItemForm {

    private String storeCode;
    
    private String shippingMode;
    
    private String packagingTypeCat;
    
    private String packagingTypeCode;
    
    private String packagingTypeItemCode;
    
    private String description;
    
    private float length;
    
    private float breadth;
    
    private float height;

    public String getStoreCode() {
        return storeCode;
    }

    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }

    public String getShippingMode() {
        return shippingMode;
    }

    public void setShippingMode(String shippingMode) {
        this.shippingMode = shippingMode;
    }

    public String getPackagingTypeCat() {
        return packagingTypeCat;
    }

    public void setPackagingTypeCat(String packagingTypeCat) {
        this.packagingTypeCat = packagingTypeCat;
    }

    public String getPackagingTypeCode() {
        return packagingTypeCode;
    }

    public void setPackagingTypeCode(String packagingTypeCode) {
        this.packagingTypeCode = packagingTypeCode;
    }

    public String getPackagingTypeItemCode() {
        return packagingTypeItemCode;
    }

    public void setPackagingTypeItemCode(String packagingTypeItemCode) {
        this.packagingTypeItemCode = packagingTypeItemCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getLength() {
        return length;
    }

    public void setLength(float length) {
        this.length = length;
    }

    public float getBreadth() {
        return breadth;
    }

    public void setBreadth(float breadth) {
        this.breadth = breadth;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    @Override
    public String toString() {
        return "PackagingTypeItemForm [storeCode=" + storeCode + ", shippingMode=" + shippingMode + ", packagingTypeCat=" + packagingTypeCat + ", packagingTypeCode="
                + packagingTypeCode + ", packagingTypeItemCode=" + packagingTypeItemCode + ", description=" + description + ", length=" + length + ", breadth=" + breadth
                + ", height=" + height + "]";
    }

}
