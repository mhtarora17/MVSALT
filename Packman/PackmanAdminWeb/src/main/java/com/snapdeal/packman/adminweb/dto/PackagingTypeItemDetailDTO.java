/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.adminweb.dto;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *  
 *  @version     1.0, 08-Dec-2015
 *  @author brijesh
 */

@XmlRootElement
public class PackagingTypeItemDetailDTO {
    
    private int id;
    
    private String code;
    
    private String description;
    
    private String packagingType;
    
    private String storeCode;
    
    private String slab;
    
    private Float length;
    
    private Float breadth;
    
    private Float height;
    
    private Double volWeight;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPackagingType() {
        return packagingType;
    }

    public void setPackagingType(String packagingType) {
        this.packagingType = packagingType;
    }

    public String getSlab() {
        return slab;
    }

    public void setSlab(String slab) {
        this.slab = slab;
    }

    public Float getLength() {
        return length;
    }

    public void setLength(Float length) {
        this.length = length;
    }

    public Float getBreadth() {
        return breadth;
    }

    public void setBreadth(Float breadth) {
        this.breadth = breadth;
    }

    public Float getHeight() {
        return height;
    }

    public void setHeight(Float height) {
        this.height = height;
    }

    public Double getVolWeight() {
        return volWeight;
    }

    public void setVolWeight(Double volWeight) {
        this.volWeight = volWeight;
    }

    public String getStoreCode() {
        return storeCode;
    }

    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }

    @Override
    public String toString() {
        return "PackagingTypeItemDetailDTO [id=" + id + ", code=" + code + ", description=" + description + ", packagingType=" + packagingType + ", storeCode=" + storeCode
                + ", slab=" + slab + ", length=" + length + ", breadth=" + breadth + ", height=" + height + ", volWeight=" + volWeight + "]";
    }
    
}
