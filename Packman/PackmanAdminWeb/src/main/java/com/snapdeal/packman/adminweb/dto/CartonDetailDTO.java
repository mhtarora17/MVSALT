package com.snapdeal.packman.adminweb.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CartonDetailDTO {

    private String id;

    private String code;

    private String description;

    private String length;

    private String breadth;

    private String height;

    private String volweight;

    private String slab;

    public CartonDetailDTO() {
        super();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getBreadth() {
        return breadth;
    }

    public void setBreadth(String breadth) {
        this.breadth = breadth;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getVolweight() {
        return volweight;
    }

    public void setVolweight(String volweight) {
        this.volweight = volweight;
    }

    public String getSlab() {
        return slab;
    }

    public void setSlab(String slab) {
        this.slab = slab;
    }

    @Override
    public String toString() {
        return "CartonDetailDTO [id=" + id + ", code=" + code + ", description=" + description + ", length=" + length + ", breadth=" + breadth + ", height=" + height
                + ", volweight=" + volweight + ", slab=" + slab + "]";
    }

}
