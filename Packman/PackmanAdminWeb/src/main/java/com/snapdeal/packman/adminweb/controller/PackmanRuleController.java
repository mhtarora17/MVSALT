/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.packman.adminweb.controller;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.naming.OperationNotSupportedException;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import antlr.Utils;

import com.snapdeal.base.cache.CacheManager;
import com.snapdeal.base.utils.CollectionUtils;
import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.cocofs.access.IProductAttributeService;
import com.snapdeal.cocofs.cache.ProductCategoryCache;
import com.snapdeal.cocofs.commonweb.security.CocofsUser;
import com.snapdeal.cocofs.commonweb.utils.WebContextUtils;
import com.snapdeal.cocofs.commonweb.utils.dto.JqgridResponse;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.db.users.IUserService;
import com.snapdeal.cocofs.entity.SupcPackagingTypeMapping;
import com.snapdeal.cocofs.excelupload.SupcPackagingTypeUploadDTO;
import com.snapdeal.cocofs.generic.persister.exception.GenericPersisterException;
import com.snapdeal.cocofs.generic.persister.response.GenericPersisterResponse;
import com.snapdeal.cocofs.mongo.model.SupcPackagingTypeMappingUnit;
import com.snapdeal.cocofs.services.IDataReader;
import com.snapdeal.cocofs.services.IDataUpdater;
import com.snapdeal.cocofs.services.common.dto.UserInfo;
import com.snapdeal.cocofs.services.data.engine.IDataEngine;
import com.snapdeal.packman.adminweb.converter.IPackmanConverterService;
import com.snapdeal.packman.annotations.GenerateSidebarData;
import com.snapdeal.packman.dataaccess.supcpt.ISupcPackagingTypeMongoService;
import com.snapdeal.packman.entity.PackagingType;
import com.snapdeal.packman.enums.BlockName;
import com.snapdeal.packman.enums.PackagingRuleParam;
import com.snapdeal.packman.enums.PackagingRuleType;
import com.snapdeal.packman.mongo.model.PackmanRuleUnit.PackmanRuleUnitType;
import com.snapdeal.packman.rule.dto.CategoryRuleCreateForm;
import com.snapdeal.packman.rule.dto.CategoryRuleCreationDTO;
import com.snapdeal.packman.rule.dto.PackmanRulesDTO;
import com.snapdeal.packman.rule.dto.SearchRuleDTO;
import com.snapdeal.packman.rule.dto.StoreRuleCreateForm;
import com.snapdeal.packman.rule.dto.StoreRuleCreationDTO;
import com.snapdeal.packman.rule.dto.SubCategoryRuleCreateForm;
import com.snapdeal.packman.rule.dto.SubCategoryRuleCreationDTO;
import com.snapdeal.packman.services.external.ICatalogExternalService;
import com.snapdeal.packman.services.packaging.IPackmanExclusiveService;
import com.snapdeal.packman.services.rules.IPackmanRuleManagement;
import com.snapdeal.packman.services.rules.IPackmanRuleService;
import com.snapdeal.rule.cache.BlocksCache;
import com.snapdeal.rule.engine.entity.Block;
import com.snapdeal.rule.engine.entity.Rule;

/**
 * @version 1.0, 21-Dec-2015
 * @author brijesh
 */

@Controller("packmanRuleController")
@RequestMapping(PackmanRuleController.URL)
public class PackmanRuleController {

    private static final String LABEL_SEPARATOR = ",";

    protected static final String                                                                           URL = "/admin/packmangui/";

    private static final Logger                                                                             LOG = LoggerFactory.getLogger(PackmanRuleController.class);

    @Autowired
    IPackmanExclusiveService                                                                                packmanExclusiveService;

    @Autowired
    IPackmanRuleManagement                                                                                  packmanRuleManager;

    @Autowired
    ICatalogExternalService                                                                                 catalogExternalService;

    @Autowired
    IPackmanRuleService                                                                                     packmanRuleService;

    @Autowired
    IPackmanConverterService                                                                                packmanConverterService;

    @Autowired
    IProductAttributeService                                                                                productAttributeService;

    @Autowired
    private IPackmanExclusiveService                                                                        packmanService;

    @Autowired
    private IDataUpdater                                                                                    dataUpdater;

    @Autowired
    IUserService                                                                                            userService;

    @Autowired
    ISupcPackagingTypeMongoService                                                                          supcPackagingTypeMongoService;

    @Autowired
    @Qualifier("SupcPackagingTypeMappingDataReader")
    private IDataReader<SupcPackagingTypeUploadDTO, SupcPackagingTypeMapping, SupcPackagingTypeMappingUnit> supcPackagingTypeMappingDataReader;

    @Autowired
    @Qualifier("SupcPackagingTypeMappingDataEngine")
    private IDataEngine<SupcPackagingTypeUploadDTO, SupcPackagingTypeMapping, SupcPackagingTypeMappingUnit> supcPackagingTypeMappingDataEngine;

    @RequestMapping("{storeCode}/createrule")
    @GenerateSidebarData
    public String getRulesCreatePage(ModelMap map, @PathVariable("storeCode") String storeCode, @RequestParam(value = "page", required = false, defaultValue = "NA") String page,
            @RequestParam(value = "id", required = false) String ruleName, @RequestParam(value = "pageId", required = false) String pageId,
            @RequestParam(value = "supc", required = false) String supc, @RequestParam(value = "packagingType", required = false) String packagingType,
            @ModelAttribute("message") String message) {

        if (page.equals(PackagingRuleType.StorePackagingRuleBlock.getUrl())) {
            StoreRuleCreateForm dto = new StoreRuleCreateForm();
            enrichModelMap(map, storeCode, true);
            map.put("page", PackagingRuleType.StorePackagingRuleBlock.getUrl());
            dto.setEnabled(true);
            map.put("StoreCreateRuleDTO", dto);
            return "admin/packman/rules/storeRuleCreate";
        }

        if (page.equals(PackagingRuleType.SupcPackagingRuleBlock.getUrl())) {
            map.put("supc", supc);
            SupcPackagingTypeMappingUnit supcPackagingTypeMappingUnit = supcPackagingTypeMongoService.getSupcPackagingTypeMappingUnit(supc, storeCode);
            String epackagingType = null;
            if (supcPackagingTypeMappingUnit != null) {
                epackagingType = supcPackagingTypeMappingUnit.getPackagingType();
                map.put("packagingTypeOriginal", epackagingType);
                map.put("enabled", supcPackagingTypeMappingUnit.isEnabled());
            }
            enrichModelMap(map, storeCode, true);
            if (epackagingType == null && supc != null)
                map.addAttribute("message", "Supc Not Mapped With Any Packaging Type");
            return "admin/packman/rules/supcRuleCreate";
        }

        if (page.equals(PackagingRuleType.CategoryPackagingRuleBlock.getUrl())) {
            CategoryRuleCreateForm dto = new CategoryRuleCreateForm();
            enrichModelMap(map, storeCode, true);
            enrichModelMapWithBrandAndSuperCat(map, storeCode);
            map.put("page", PackagingRuleType.CategoryPackagingRuleBlock.getUrl());
            dto.setEnabled(true);
            map.put("CategoryCreateRuleDTO", dto);
            return "admin/packman/rules/categoryRuleCreate";
        }

        if (page.equals(PackagingRuleType.SubcategoryPackagingRuleBlock.getUrl())) {
            SubCategoryRuleCreateForm dto = new SubCategoryRuleCreateForm();
            enrichModelMap(map, storeCode, true);
            enrichModelMapWithBrandAndSuperCat(map, storeCode);
            map.put("page", PackagingRuleType.SubcategoryPackagingRuleBlock.getUrl());
            dto.setEnabled(true);
            map.put("SubCategoryCreateRuleDTO", dto);
            return "admin/packman/rules/subcategoryRuleCreate";
        }

        if (ruleName != null) {
            List<Rule> rules = packmanRuleService.getRuleFromName(ruleName, PackagingRuleParam.STORECODE.getCode(), storeCode);
            if (null != rules) {
                Rule r = rules.get(0);
                BlocksCache cache = CacheManager.getInstance().getCache(BlocksCache.class);
                if (cache != null) {
                    Integer blockId = r.getBlockId();
                    Block b = cache.getBlockById(blockId);
                    if (b != null) {
                        if (b.getName().equals(BlockName.StorePackagingRuleBlock.getCode())) {
                            map.put("page", PackagingRuleType.StorePackagingRuleBlock.getUrl());
                            StoreRuleCreateForm storeCreateRuleDTO = packmanRuleManager.getStoreCreateRuleDTO(r, new StoreRuleCreateForm());
                            map.put("StoreCreateRuleDTO", storeCreateRuleDTO);
                            enrichModelMap(map, storeCreateRuleDTO.getStoreCode(), false);
                            return "admin/packman/rules/storeRuleCreate";
                        } else if (b.getName().equals(BlockName.CategoryPackagingRuleBlock.getCode())) {
                            map.put("page", PackagingRuleType.CategoryPackagingRuleBlock.getUrl());
                            CategoryRuleCreateForm categoryCreateRuleDTO = packmanRuleManager.getCategoryCreateRuleDTO(r, new CategoryRuleCreateForm());
                            map.put("CategoryCreateRuleDTO", categoryCreateRuleDTO);
                            enrichModelMap(map, categoryCreateRuleDTO.getStoreCode(), false);
                            enrichModelMapWithBrandAndSuperCat(map, storeCode);
                            return "admin/packman/rules/categoryRuleCreate";
                        } else if (b.getName().equals(BlockName.SubcategoryPackagingRuleBlock.getCode())) {
                            map.put("page", PackagingRuleType.SubcategoryPackagingRuleBlock.getUrl());
                            SubCategoryRuleCreateForm subCategoryCreateRuleDTO = packmanRuleManager.getSubCategoryCreateRuleDTO(r, new SubCategoryRuleCreateForm());
                            map.put("SubCategoryCreateRuleDTO", subCategoryCreateRuleDTO);
                            enrichModelMap(map, subCategoryCreateRuleDTO.getStoreCode(), false);
                            enrichModelMapWithBrandAndSuperCat(map, storeCode);
                            return "admin/packman/rules/subcategoryRuleCreate";
                        }
                    } else {
                        LOG.error("Unable to find block from cache with block id : {}", blockId);
                    }
                } else {
                    LOG.error("Getting null from block cache when querying");
                }
            }
        }
        if (StringUtils.isNotEmpty(message))
            map.put("message", message);
        enrichModelMap(map, storeCode, true);
        return "admin/packman/rules/rulesCreate";
    }

    @RequestMapping("{storeCode}/createrule/createStoreRule")
    @GenerateSidebarData
    public String createStoreRule(@PathVariable("storeCode") String storeCode, @Valid StoreRuleCreateForm storeDTO, @RequestParam(value = "name", required = false) String ruleName,
            @RequestParam(value = "pageId", required = false) String pageId, RedirectAttributes rattrs, ModelMap map) {

        String error = null;
        boolean returnrequired = false;

        if (StringUtils.isNotEmpty(ruleName)) {
            storeDTO.setRuleName(ruleName);
        }

        String user = WebContextUtils.getCurrentUserEmail();
        StoreRuleCreationDTO creationDTO = (StoreRuleCreationDTO) packmanRuleManager.getRuleCreationDTO(BlockName.StorePackagingRuleBlock.getCode(), storeCode, null,
                storeDTO.getPackagingType(), null, null, user, storeDTO.getStartDate(), storeDTO.getEndDate(), storeDTO.isEnabled());

        if (StringUtils.isEmpty(ruleName) || (StringUtils.isNotEmpty(ruleName) && creationDTO.isEnabled())) {
            Object[] obj = packmanRuleService.getSameRules(PackmanRuleUnitType.STORE_LEVEL_PACKAGING.getCode(), storeCode, storeCode, creationDTO.getPriority(),
                    creationDTO.getBlock(), (String) null, (String) null, creationDTO.getStartDate(), creationDTO.getEndDate(), null, null, false, storeDTO.getRuleName());
            error = (String) obj[0];
            returnrequired = false;
        }

        if (null != error && error.length() > 0) {
            map.put("errorcause", error);
            map.put("errorfinal", "Rule creation failed ");
            returnrequired = true;
        } else {
            try {
                if (StringUtils.isNotEmpty(ruleName)) {
                    creationDTO.setRuleName(ruleName);
                    packmanRuleManager.updateRule(creationDTO);
                    LOG.info("Rule updated " + creationDTO.toString());
                    map.put("message", "Rule updated successfully with code : " + ruleName);
                } else {
                    Rule r = packmanRuleManager.createRule(creationDTO);
                    LOG.info("Rule created " + creationDTO.toString());
                    map.put("message", "Rule created successfully with code : " + r.getName());
                }
                enrichModelMap(map, storeCode, true);
                rattrs.addFlashAttribute("message", map.get("message"));
                return "redirect:/admin/packmangui/" + storeCode + "/createrule/";
            } catch (Exception e) {
                LOG.info("Rule creation failed " + creationDTO.toString(), e);
                map.put("errorfinal", "Rule creation failed for store : " + storeCode);
                map.put("errorcause", e.getMessage());
                returnrequired = true;
            }
        }
        map.put("page", PackagingRuleType.StorePackagingRuleBlock.getUrl());
        if (returnrequired)
            map.put("StoreCreateRuleDTO", storeDTO);
        else
            map.put("StoreCreateRuleDTO", new StoreRuleCreateForm());
        enrichModelMap(map, storeCode, true);
        return "admin/packman/rules/storeRuleCreate";
    }

    @RequestMapping("{storeCode}/createrule/createSupcRule")
    @GenerateSidebarData
    public String createSupcRule(@PathVariable("storeCode") String storeCode, @RequestParam(value = "supc", required = true) String supc,
            @RequestParam(value = "packagingType", required = false) String packagingType, @RequestParam(value = "enabled", required = false) boolean enabled,
            @RequestParam(value = "pageId", required = false) String pageId, RedirectAttributes rattrs, ModelMap map) {

        Set<String> storeCodeSet = getUserAccessibleStores();

        if (CollectionUtils.isEmpty(productAttributeService.getAllProductAttributeForSUPC(supc))) {
            LOG.info("Supc Not Exist && product attribute not exist for supc :" + supc);
            map.put("errorfinal", "Rule creation failed for supc : " + supc);
            map.addAttribute("message", "SUPC :" + supc + " does not exist. Pls check your input.");
        } else if (!storeCodeSet.contains(storeCode)) {
            LOG.info("User do not have permissions to create mapping at store at :" + storeCode + ".");
            map.put("errorfinal", "Rule creation failed for supc : " + supc);
            map.addAttribute("message", "User do not have permissions to create mapping at store :" + storeCode + ".");
        } else {

            UserInfo userInfo = new UserInfo();
            userInfo.setEmail(StringUtils.isNotEmpty(WebContextUtils.getCurrentUserEmail()) ? WebContextUtils.getCurrentUserEmail() : "BulkUpload");
            userInfo.setSuper(false);
            SupcPackagingTypeUploadDTO supcPackagingTypeUploadDTO = new SupcPackagingTypeUploadDTO();
            supcPackagingTypeUploadDTO.setStoreCode(storeCode);
            supcPackagingTypeUploadDTO.setEnable(enabled);
            supcPackagingTypeUploadDTO.setSupc(supc);
            supcPackagingTypeUploadDTO.setPackagingTypeCode(packagingType);

            try {
                GenericPersisterResponse<SupcPackagingTypeMapping, SupcPackagingTypeMappingUnit> resp = dataUpdater.updateDataWithDTO(supcPackagingTypeUploadDTO,
                        supcPackagingTypeMappingDataReader, supcPackagingTypeMappingDataEngine, userInfo, true, true);
                map.addAttribute("message", " Mapping Successfully Save For supc:" + supc + "-" + "storeCode:" + storeCode + ".");
            } catch (OperationNotSupportedException | GenericPersisterException e) {
                LOG.error("SupcRule creation failed ", e);
                map.put("errorfinal", "Rule creation failed for supc : " + supc);
                map.put("errorcause", e.getMessage());
                e.printStackTrace();
            }

        }

        return "admin/packman/rules/rulesCreate";
    }

    @RequestMapping("{storeCode}/createrule/createCategoryRule")
    @GenerateSidebarData
    public String createCategoryRule(@PathVariable("storeCode") String storeCode, @RequestParam(value = "name", required = false) String ruleName,
            @RequestParam(value = "pageId", required = false) String pageId, @Valid CategoryRuleCreateForm dto, RedirectAttributes rattrs, ModelMap map) {

        String error = null;
        boolean returnrequired = false;

        if (StringUtils.isNotEmpty(ruleName)) {
            dto.setRuleName(ruleName);
        }

        String user = WebContextUtils.getCurrentUserEmail();
        CategoryRuleCreationDTO creationDTO = (CategoryRuleCreationDTO) packmanRuleManager.getRuleCreationDTO(BlockName.CategoryPackagingRuleBlock.getCode(), storeCode,
                dto.getCategory(), dto.getPackagingType(), dto.getBrand(), dto.getSupercat(), user, dto.getStartDate(), dto.getEndDate(), dto.isEnabled());

        if (StringUtils.isEmpty(ruleName) || (StringUtils.isNotEmpty(ruleName) && creationDTO.isEnabled())) {
            Object[] obj = packmanRuleService.getSameRules(PackmanRuleUnitType.CATEGORY_LEVEL_PACKAGING.getCode(), storeCode, dto.getCategory(), creationDTO.getPriority(),
                    creationDTO.getBlock(), creationDTO.getCategory(), (String) null, creationDTO.getStartDate(), creationDTO.getEndDate(), creationDTO.getBrand(),
                    creationDTO.getSuperCat(), false, dto.getRuleName());
            error = (String) obj[0];
            returnrequired = true;
        }

        boolean update = false;
        if (StringUtils.isNotEmpty(ruleName)) {
            creationDTO.setRuleName(ruleName);
            update = true;
        }

        if (null != error && error.length() > 0) {
            map.put("errorcause", error);
            map.put("errorfinal", "Rule creation failed ");
        } else {
            try {
                if (update) {
                    packmanRuleManager.updateRule(creationDTO);
                    LOG.info("Rule updated " + creationDTO.toString());
                    map.put("message", "Rule updated successfully with code : " + ruleName);
                } else {
                    Rule r = packmanRuleManager.createRule(creationDTO);
                    LOG.info("Rule created " + creationDTO.toString());
                    map.put("message", "Rule created successfully with code : " + r.getName());
                }
                enrichModelMap(map, storeCode, true);
                rattrs.addFlashAttribute("message", map.get("message"));
                returnrequired = false;
                return "redirect:/admin/packmangui/" + storeCode + "/createrule/";
            } catch (Exception e) {
                LOG.info("Rule creation failed " + creationDTO.toString(), e);
                map.put("errorfinal", "Rule creation failed for category " + dto.getCategory());
                map.put("errorcause", e.getMessage());
            }
        }
        map.put("page", PackagingRuleType.CategoryPackagingRuleBlock.getUrl());

        if (returnrequired) {
            map.put("CategoryCreateRuleDTO", dto);
        } else {
            map.put("CategoryCreateRuleDTO", new CategoryRuleCreateForm());
        }
        enrichModelMap(map, storeCode, true);
        enrichModelMapWithBrandAndSuperCat(map, storeCode);
        return "admin/packman/rules/categoryRuleCreate";
    }

    @RequestMapping("{storeCode}/createrule/createSubCategoryRule")
    @GenerateSidebarData
    public String createSubCategoryRule(@PathVariable("storeCode") String storeCode, @RequestParam(value = "name", required = false) String ruleName,
            @RequestParam(value = "pageId", required = false) String pageId, @Valid SubCategoryRuleCreateForm dto, RedirectAttributes rattrs, ModelMap map) {

        String error = null;
        boolean returnrequired = false;

        if (StringUtils.isNotEmpty(ruleName)) {
            dto.setRuleName(ruleName);
        }

        String user = WebContextUtils.getCurrentUserEmail();
        SubCategoryRuleCreationDTO creationDTO = (SubCategoryRuleCreationDTO) packmanRuleManager.getRuleCreationDTO(BlockName.SubcategoryPackagingRuleBlock.getCode(), storeCode,
                dto.getSubcategory(), dto.getPackagingType(), dto.getBrand(), dto.getSupercat(), user, dto.getStartDate(), dto.getEndDate(), dto.isEnabled());

        if (StringUtils.isEmpty(ruleName) || (StringUtils.isNotEmpty(ruleName) && creationDTO.isEnabled())) {
            Object[] obj = packmanRuleService.getSameRules(PackmanRuleUnitType.SUBCATEGORY_LEVEL_PACKAGING.getCode(), storeCode, dto.getSubcategory(), creationDTO.getPriority(),
                    creationDTO.getBlock(), (String) null, creationDTO.getSubCategory(), creationDTO.getStartDate(), creationDTO.getEndDate(), creationDTO.getBrand(),
                    creationDTO.getSuperCat(), false, dto.getRuleName());
            error = (String) obj[0];
            returnrequired = false;
        }

        boolean update = false;
        if (StringUtils.isNotEmpty(ruleName)) {
            creationDTO.setRuleName(ruleName);
            update = true;
        }

        if (null != error && error.length() > 0) {
            map.put("errorcause", error);
            map.put("errorfinal", "Rule creation failed ");
            returnrequired = true;
        } else {
            try {
                if (update) {
                    packmanRuleManager.updateRule(creationDTO);
                    LOG.info("Rule updated " + creationDTO.toString());
                    map.put("message", "Rule updated successfully with code : " + ruleName);
                } else {
                    Rule r = packmanRuleManager.createRule(creationDTO);
                    LOG.info("Rule created " + creationDTO.toString());
                    map.put("message", "Rule created successfully with code : " + r.getName());
                }
                enrichModelMap(map, storeCode, true);
                rattrs.addFlashAttribute("message", map.get("message"));
                return "redirect:/admin/packmangui/" + storeCode + "/createrule/";
            } catch (Exception e) {
                LOG.info("Rule creation failed " + creationDTO.toString(), e);
                map.put("errorfinal", "Rule creation failed for subcategory " + dto.getSubcategory());
                map.put("errorcause", e.getMessage());
                returnrequired = true;
            }
        }

        if (returnrequired)
            map.put("SubCategoryCreateRuleDTO", dto);
        else
            map.put("SubCategoryCreateRuleDTO", new SubCategoryRuleCreateForm());

        map.put("page", PackagingRuleType.SubcategoryPackagingRuleBlock.getUrl());
        enrichModelMap(map, storeCode, true);
        enrichModelMapWithBrandAndSuperCat(map, storeCode);
        return "admin/packman/rules/subcategoryRuleCreate";
    }

    @RequestMapping("{storeCode}/viewrule")
    @GenerateSidebarData
    public String getViewRulesCreatePage(@PathVariable("storeCode") String storeCode, ModelMap map, @RequestParam(value = "pageId", required = false) String pageId) {
        enrichModelMap(map, storeCode, false);
        enrichModelMapWithBrandAndSuperCat(map, storeCode);
        return "admin/packman/rules/viewRules";
    }

    @RequestMapping("{storeCode}/getRuleInfo")
    public @ResponseBody JqgridResponse<PackmanRulesDTO> getRuleInfo(ModelMap modelMap, @RequestParam(value = "page", required = false, defaultValue = "1") int page,
            @RequestParam(value = "rows", required = false, defaultValue = "40") int rows, @RequestParam(required = true, value = "storeCode") String storeCode,
            @RequestParam(required = false, value = "category") String category, @RequestParam(required = false, value = "subcategory") String subcategory,
            @RequestParam(required = false, value = "brand") String brand, @RequestParam(required = false, value = "supercategory") List<String> supercategory,
            @RequestParam(required = false, value = "name") String name, @RequestParam(required = false, value = "createdby") String createdby,
            @RequestParam(required = false, value = "supc") String supc, @RequestParam(required = false, value = "enabled", defaultValue = "true") boolean enabled,
            @RequestParam(required = false, value = "packagingType") String packagingType) {
        JqgridResponse<PackmanRulesDTO> resp = new JqgridResponse<PackmanRulesDTO>();
        List<PackmanRulesDTO> dtoList = null;
        if (StringUtils.isNotEmpty(supc)) {
            dtoList = packmanConverterService.getPackagingTypeMappingForStoreCodeAndSupc(supc, storeCode);
        } else {
            List<String> brandList = new ArrayList<String>();
            if (StringUtils.isNotEmpty(brand)) {
                brandList = Arrays.asList(brand.split(LABEL_SEPARATOR));
            }
            SearchRuleDTO searchRuleDTO = packmanConverterService.createSearchRuleDto(storeCode, category, subcategory, brandList, supercategory, name, createdby, enabled,
                    packagingType);
            dtoList = packmanConverterService.getAllRules(searchRuleDTO);
            if(StringUtils.isNotEmpty(packagingType)){
                // required to fetch rules from supc packaging type mapping
                List<PackmanRulesDTO> supcRulesDto = packmanConverterService.getPackagingTypeMappingByPackagingType(packagingType,enabled);
                dtoList = packmanConverterService.addMoreRules(dtoList,supcRulesDto);
            }
        }
        int records = dtoList.size();
        resp.setRecords(String.valueOf(records));
        Integer pages = records == rows ? records / rows : records / rows + 1;
        resp.setTotal(pages);
        resp.setPage(String.valueOf(page));
        resp.setRows(dtoList);
        return resp;
    }

    private void enrichModelMap(ModelMap map, String storeCode, boolean enabled) {
        List<PackagingType> packagingTypeList = null;
        if (enabled) {
            packagingTypeList = packmanExclusiveService.getAllEnabledPackagingType(storeCode);
        } else {
            packagingTypeList = packmanExclusiveService.getAllPackagingType(storeCode);
        }
        List<String> packagingTypeCodeList = new ArrayList<String>();
        if (packagingTypeList != null) {
            for (PackagingType packagingType : packagingTypeList)
                packagingTypeCodeList.add(packagingType.getType());
            map.put("packagingType", packagingTypeCodeList);
        }
        map.put("storeCode", storeCode);
    }

    private void enrichModelMapWithBrandAndSuperCat(ModelMap map, String storeCode) {
        Map<String, String> categories = getAllCategories();
        Map<String, String> subCategories = getAllSubCategories();
        List<String> superCats = getAllLabels(storeCode);
        map.put("supercats", superCats);
        map.put("categories", categories);
        map.put("subcategories", subCategories);
    }

    private List<String> getAllLabels(String storeCode) {
        return packmanExclusiveService.getAllLabels(storeCode);
    }

    @SuppressWarnings("unchecked")
    private Map<String, String> getAllSubCategories() {
        Map<String, String> map = (Map<String, String>) Collections.EMPTY_MAP;
        ProductCategoryCache cache = CacheManager.getInstance().getCache(ProductCategoryCache.class);
        if (cache != null) {
            map = cache.getAllSubCategories();
        }
        return map;
    }

    @SuppressWarnings("unchecked")
    private Map<String, String> getAllCategories() {
        Map<String, String> map = (Map<String, String>) Collections.EMPTY_MAP;
        ProductCategoryCache cache = CacheManager.getInstance().getCache(ProductCategoryCache.class);
        if (cache != null) {
            map = cache.getAllCategories();
        }
        return map;
    }

    private Set<String> getUserAccessibleStores() {

        String email = WebContextUtils.getCurrentUserEmail();
        CocofsUser user = WebContextUtils.getCurrentUser();
        boolean superUser = false;
        if (user != null) {
            superUser = user.hasRole("admin") || user.hasRole("tech");
        }

        String storeLevelRoleNames = ConfigUtils.getStringScalar(Property.ALLOWED_STORE_ROLES_FOR_SUPC_PACKAGING_MAPPING);

        List<String> storeLevelRoles = StringUtils.split(storeLevelRoleNames);

        Set<String> storeCodeSet = packmanService.getAccessibleStoresByUserAndUserRole(email, storeLevelRoles, superUser);
        return storeCodeSet;
    }

}
