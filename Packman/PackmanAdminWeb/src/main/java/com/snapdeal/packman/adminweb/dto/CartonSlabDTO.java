package com.snapdeal.packman.adminweb.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CartonSlabDTO {

    private String id;

    private String slabSize;

    private String slabMin;

    private String slabMax;

    private String cartonBoxes;

    public CartonSlabDTO() {
        super();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSlabSize() {
        return slabSize;
    }

    public void setSlabSize(String slabSize) {
        this.slabSize = slabSize;
    }

    public String getSlabMin() {
        return slabMin;
    }

    public void setSlabMin(String slabMin) {
        this.slabMin = slabMin;
    }

    public String getSlabMax() {
        return slabMax;
    }

    public void setSlabMax(String slabMax) {
        this.slabMax = slabMax;
    }

    public String getCartonBoxes() {
        return cartonBoxes;
    }

    public void setCartonBoxes(String cartonBoxes) {
        this.cartonBoxes = cartonBoxes;
    }

    @Override
    public String toString() {
        return "CartonSlabDTO [id=" + id + ", slabSize=" + slabSize + ", slabMin=" + slabMin + ", slabMax=" + slabMax + ", cartonBoxes=" + cartonBoxes + "]";
    }

}
