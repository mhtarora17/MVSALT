/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.adminweb.dto;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *  
 *  @version     1.0, 03-Dec-2015
 *  @author brijesh
 */

@XmlRootElement
public class StoreDetailDTO {
    
    private int id;

    private String code;
    
    private String name;
    
    private String description;
    
    private boolean enabled;
    
    private String storeFrontId;
    
    private String labels;
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getStoreFrontId() {
        return storeFrontId;
    }

    public void setStoreFrontId(String storeFrontId) {
        this.storeFrontId = storeFrontId;
    }

    public String getLabels() {
        return labels;
    }

    public void setLabels(String labels) {
        this.labels = labels;
    }

    @Override
    public String toString() {
        return "StoreDetailDTO [id=" + id + ", code=" + code + ", name=" + name + ", description=" + description + ", enabled=" + enabled + ", storeFrontId=" + storeFrontId
                + ", labels=" + labels + "]";
    }
    
}
