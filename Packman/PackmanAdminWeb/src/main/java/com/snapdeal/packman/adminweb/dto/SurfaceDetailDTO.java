package com.snapdeal.packman.adminweb.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class SurfaceDetailDTO {

    private String id;

    private String type;

    private String description;


    public SurfaceDetailDTO() {
        super();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "SurfaceDetailDTO [id=" + id + ", type=" + type + ", description=" + description + "]";
    }

    

}
