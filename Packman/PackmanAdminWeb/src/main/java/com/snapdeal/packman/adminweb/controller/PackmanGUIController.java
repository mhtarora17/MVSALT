/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.packman.adminweb.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.cocofs.commonweb.utils.WebContextUtils;
import com.snapdeal.cocofs.commonweb.utils.dto.JqgridResponse;
import com.snapdeal.packman.adminweb.controller.form.PackagingTypeForm;
import com.snapdeal.packman.adminweb.controller.form.PackagingTypeItemForm;
import com.snapdeal.packman.adminweb.controller.form.StoreForm;
import com.snapdeal.packman.adminweb.converter.IPackmanConverterService;
import com.snapdeal.packman.adminweb.dto.PackagingTypeDetailDTO;
import com.snapdeal.packman.adminweb.dto.PackagingTypeItemDetailDTO;
import com.snapdeal.packman.adminweb.dto.StoreDetailDTO;
import com.snapdeal.packman.annotations.GenerateSidebarData;
import com.snapdeal.packman.configuration.PackagingProperty;
import com.snapdeal.packman.entity.PackagingType;
import com.snapdeal.packman.entity.PackagingTypeItem;
import com.snapdeal.packman.entity.PackagingTypeItemProperties;
import com.snapdeal.packman.entity.PackagingTypeProperties;
import com.snapdeal.packman.entity.Store;
import com.snapdeal.packman.entity.StoreProperties;
import com.snapdeal.packman.enums.PackagingMode;
import com.snapdeal.packman.enums.PackagingTypeCategory;
import com.snapdeal.packman.rule.dto.PackmanRulesDTO;
import com.snapdeal.packman.services.external.ICatalogExternalService;
import com.snapdeal.packman.services.packaging.IPackmanExclusiveService;
import com.snapdeal.packman.services.packaging.dto.PackagingTypeItemDTO;
import com.snapdeal.packman.services.roles.IPackmanInternalCacheReloadService;
import com.snapdeal.packman.services.roles.IPackmanRolesGeneratorService;
import com.snapdeal.packman.services.rules.IPackmanRuleManagement;
import com.snapdeal.packman.services.rules.IPackmanRuleService;
import com.snapdeal.rule.engine.entity.Rule;

/**
 * @version 1.0, 30-Nov-2015
 * @author indrajit
 */
@Controller("packmanGuiController")
@RequestMapping(PackmanGUIController.URL)
public class PackmanGUIController {

    public static final String         URL = "/admin/packmangui/";

    private static final Logger        LOG = LoggerFactory.getLogger(PackmanGUIController.class);

    @Autowired
    IPackmanExclusiveService           packmanExclusiveService;

    @Autowired
    IPackmanRolesGeneratorService      rolecreationService;

    @Autowired
    IPackmanRuleManagement             packmanRuleManager;

    @Autowired
    ICatalogExternalService            catalogExternalService;

    @Autowired
    IPackmanRuleService                packmanRuleService;

    @Autowired
    IPackmanInternalCacheReloadService cacheReloadService;

    @Autowired
    IPackmanConverterService           packmanConverterService;

    @RequestMapping("main")
    @GenerateSidebarData
    public String mainPage(ModelMap modelMap) {
        return "/admin/packman/main";
    }

    @RequestMapping("createstore")
    @GenerateSidebarData
    public String createStore(ModelMap map) {
        map.addAttribute("storeForm", new StoreForm());
        return "/admin/packman/store/createStore";
    }

    @RequestMapping("savestore")
    @GenerateSidebarData
    public String saveStore(ModelMap map, @Valid StoreForm storeForm) {
        LOG.info("Request received to save store {}", storeForm);
        boolean returnForm = true;
        try {
            Store store = packmanConverterService.getStoreFromStoreForm(storeForm);
            String labels = storeForm.getLabels();
            StoreProperties property = null;
            if (StringUtils.isNotEmpty(labels)) {
                property = packmanConverterService.getStorePropertyFromForm(labels);
            }
            String userEmail = WebContextUtils.getCurrentUserEmail();
            packmanExclusiveService.createStoreAndStoreProperties(store, property, userEmail);
            returnForm = false;
            map.put("message", "Store created successfully");
            LOG.info("Store and its roles are created successfully {}", store);
            cacheReloadService.reloadPackmanCaches();
        } catch (IllegalArgumentException e) {
            map.put("message", e.getMessage());
        } catch (Exception e) {
            map.put("message", e.getMessage());
            LOG.error("Unexpected Error occurred  {}", e);
        }
        if (returnForm)
            map.put("storeForm", storeForm);
        else
            map.put("storeForm", new StoreForm());
        return "/admin/packman/store/createStore";
    }

    @RequestMapping("viewstore")
    @GenerateSidebarData
    public String viewStore(ModelMap map) {
        return "/admin/packman/store/viewStore";
    }

    @RequestMapping("getStoreInfo")
    public @ResponseBody JqgridResponse<StoreDetailDTO> getStoreInfo(ModelMap modelMap, @RequestParam(value = "page", required = false, defaultValue = "1") int page,
            @RequestParam(value = "rows", required = false, defaultValue = "40") int rows) {
        JqgridResponse<StoreDetailDTO> resp = new JqgridResponse<StoreDetailDTO>();
        List<StoreDetailDTO> dtoList = packmanConverterService.getAllStoreDetails();
        int records = dtoList.size();
        resp.setRecords(String.valueOf(records));
        Integer pages = records == rows ? records / rows : records / rows + 1;
        resp.setTotal(pages);
        resp.setPage(String.valueOf(page));
        resp.setRows(dtoList);
        return resp;
    }

    @RequestMapping(value = "{storeCode}/createpackagingtype")
    @GenerateSidebarData
    public String createPackagingType(ModelMap map, @PathVariable("storeCode") String storeCode, @RequestParam(value = "pageId", required = false) String sideBarId,
            @RequestParam(value="packagingTypeCode",required=false) String packagingTypeCode) {
        attributesForPackagingType(map, storeCode, null, sideBarId);
        if (StringUtils.isNotEmpty(packagingTypeCode)) {
            try {
                PackagingTypeForm form = packmanConverterService.enrichPackagingTypeForm(storeCode, packagingTypeCode);
                map.put("packagingTypeForm", form);
            } catch (Exception e) {
                map.put("message", "Exception - " + e.getMessage());
                LOG.error("Exception - " + e.getMessage());
            }
        }
        return "/admin/packman/packaging/createPackagingType";
    }

    private void attributesForPackagingType(ModelMap map, String storeCode, String message, String id) {
        addShippingTypes(map);
        addPackagingTypeCategories(map);
        PackagingTypeForm packagingTypeForm = packmanConverterService.getPackagingTypeForm();
        map.addAttribute("packagingTypeForm", packagingTypeForm);
        map.put("storeCode", storeCode);
        if (null != message)
            map.put("message", message);
    }

    private void addPackagingTypeCategories(ModelMap map) {
        List<String> packagingTypeCats = new ArrayList<String>();
        for (PackagingTypeCategory s : PackagingTypeCategory.values())
            packagingTypeCats.add(s.type());
        map.put("packagingTypeCats", packagingTypeCats);
    }

    private void addShippingTypes(ModelMap map) {
        List<String> shippingTypes = new ArrayList<String>();
        for (PackagingMode s : PackagingMode.values())
            shippingTypes.add(s.mode());
        map.put("shippingTypes", shippingTypes);
    }

    @RequestMapping("{storeCode}/savepackagingtype")
    @GenerateSidebarData
    public String savePackagingType(ModelMap map, @PathVariable("storeCode") String storeCode, @Valid PackagingTypeForm packagingTypeForm,
            @RequestParam(value = "pageId", required = false) String sideBarId) {
        LOG.info("Request received for store {} to save packaging type {}", storeCode, packagingTypeForm);
        boolean returnForm = true;
        try {
           
            // Check is request for update 
            List<PackagingTypeProperties> packagingTypeProperties = null;
            PackagingType packagingType  = null;
            String user = WebContextUtils.getCurrentUserEmail();
            
            if(packagingTypeForm.getId()!=0){
                if(packagingTypeForm.isEnabled()==false){
                    String packagingTypeCode = packagingTypeForm.getPackagingTypeCode();
                    List<Rule> rules = packmanRuleService.getRuleByStoreAndPackagingType(storeCode, packagingTypeCode);
                    List<PackmanRulesDTO> supcRulesDto = packmanConverterService.getPackagingTypeMappingByPackagingType(packagingTypeCode,true);
                    boolean throwDisableRule = false;
                    for(Rule r : rules){
                        if(r.isEnabled()){
                            throwDisableRule = true;
                        }
                    }
                    if(supcRulesDto.size()>0){
                        throwDisableRule = true;
                    }
                    if(throwDisableRule){
                        throw new IllegalArgumentException("Rules for packaging type ["+packagingTypeCode+"] for store ["+storeCode+"] exists. please disable them before disabling packaging type");
                    }
                }
                packagingType = packmanConverterService.getPackagingType(packagingTypeForm);
                packagingTypeProperties = packmanConverterService.getProperties(packagingTypeForm, packagingType);
                packmanExclusiveService.updatePackagingType(packagingType,packagingTypeProperties,packagingTypeForm.getPackagingTypeCat(),user);
                map.put("message", "Packaging Type is Updated Successfully");
            } else {
                packagingType = packmanConverterService.getPackagingTypeFromForm(packagingTypeForm);
                packagingTypeProperties = packmanConverterService.getPackagingTypePropertiesFromForm(packagingTypeForm, packagingType);
                packmanExclusiveService.createPackagingType(packagingType, packagingTypeProperties,user);
                map.put("message", "Packaging Type is Created Successfully");
            }
            returnForm = false;
            
        } catch (IllegalArgumentException e){
            map.put("message", e.getMessage());
            LOG.error("Exception - {}",e.getMessage());
        }
        catch (Exception e) {
            map.put("message", "Error : " + e.getMessage());
            LOG.error("Exception occurred while saving packaging type : " + e);
        }
        attributesForPackagingType(map, storeCode, null, sideBarId);
        if (returnForm){
            map.put("packagingTypeForm", packagingTypeForm);
        }
        return "/admin/packman/packaging/createPackagingType";
    }

    @RequestMapping("{storeCode}/viewpackagingtype")
    @GenerateSidebarData
    public String viewPackagingType(ModelMap map, @PathVariable("storeCode") String storeCode, @RequestParam(value = "pageId", required = false) String sideBarId) {
        map.put("storeCode", storeCode);
        return "/admin/packman/packaging/viewPackagingType";
    }

    @RequestMapping("{storeCode}/getPackagingTypeInfo")
    public @ResponseBody JqgridResponse<PackagingTypeDetailDTO> getPackagingTypeInfo(ModelMap modelMap,
            @RequestParam(value = "page", required = false, defaultValue = "1") int page, @RequestParam(value = "rows", required = false, defaultValue = "40") int rows,
            @PathVariable("storeCode") String storeCode) {
        JqgridResponse<PackagingTypeDetailDTO> resp = new JqgridResponse<PackagingTypeDetailDTO>();
        List<PackagingTypeDetailDTO> dtoList = null;
        dtoList = packmanConverterService.getAllPackagingTypeDetails(storeCode);
        int records = dtoList.size();
        resp.setRecords(String.valueOf(records));
        Integer pages = records == rows ? records / rows : records / rows + 1;
        resp.setTotal(pages);
        resp.setPage(String.valueOf(page));
        resp.setRows(dtoList);
        return resp;
    }

    @RequestMapping("{storeCode}/viewpackagingtypeitem")
    @GenerateSidebarData
    public String viewPackagingTypeItem(ModelMap map, @PathVariable("storeCode") String storeCode,
            @RequestParam(value = "packagingTypeCode", required = true) String packagingTypeCode, @RequestParam(value = "pageId", required = false) String sideBarId) {
        PackagingType packagingType = packmanExclusiveService.getPackagingType(packagingTypeCode, storeCode);
        if (null != packagingType){
            map.put("show", packagingType.getPackagingMode().equals(PackagingMode.NORMAL.mode()));
        }
        map.put("storeCode", storeCode);
        map.put("packagingTypeCode", packagingTypeCode);
        String packagingTypeCat = packmanExclusiveService.getPackagingTypeProperties(PackagingProperty.PACKAGING_TYPE_CATEGORY.mode(), packagingType);
        map.put("packagingTypeCat", packagingTypeCat);
        return "/admin/packman/packaging/viewPackagingTypeItem";
    }

    @RequestMapping("{storeCode}/createpackagingtypeitem")
    @GenerateSidebarData
    public String createPackagingTypeItem(ModelMap map, @PathVariable("storeCode") String storeCode,
            @RequestParam(value = "packagingTypeCode", required = true) String packagingTypeCode, @RequestParam(value = "pageId", required = false) String sideBarId) {
        attributesForPackagingTypeItem(map, storeCode, packagingTypeCode, sideBarId, null);
        return "/admin/packman/packaging/createPackagingTypeItem";
    }

    private void attributesForPackagingTypeItem(ModelMap map, String storeCode, String packagingTypeCode, String id, String message) {
        map.put("storeCode", storeCode);
        map.put("packagingTypeCode", packagingTypeCode);
        PackagingType packagingType = packmanExclusiveService.getPackagingType(packagingTypeCode, storeCode);
        String shippingMode = null;
        if (null != packagingType) {
            shippingMode = packagingType.getPackagingMode();
            map.put("show", shippingMode.equals(PackagingMode.NORMAL.mode()));
            map.put("shippingMode", shippingMode);
            String packagingTypeCat = packmanExclusiveService.getPackagingTypeProperties(PackagingProperty.PACKAGING_TYPE_CATEGORY.mode(), packagingType);
            map.put("packagingTypeCat", packagingTypeCat);
        }
        map.addAttribute("packagingTypeItemForm", new PackagingTypeItemForm());
        if(message!=null)
            map.put("message", message);
    }

    @RequestMapping("{storeCode}/savepackagingtypeitem")
    @GenerateSidebarData
    public String savePackagingTypeItem(ModelMap map, @PathVariable("storeCode") String storeCode, @Valid PackagingTypeItemForm packagingTypeItemForm,
            @RequestParam(value = "pageId", required = false) String sideBarId) {
       
        String packagingTypeCode = packagingTypeItemForm.getPackagingTypeCode();
        LOG.info("Request received for store {} and packagingtype {} to save packagingtypeitem {}", storeCode, packagingTypeCode, packagingTypeItemForm);
        boolean returnRequired = true;
        
        try {
            PackagingTypeItemDTO packagingTypeItemdto = packmanConverterService.enrichingPackagingTypeItemDTO(packagingTypeItemForm, storeCode);
            PackagingTypeItem packagingTypeItem = packmanConverterService.getPackagingTypeItemFromDto(packagingTypeItemdto);
            List<PackagingTypeItemProperties> property = packmanConverterService.getPackagingTypeItemProperties(packagingTypeItemdto);
            String userEmail = WebContextUtils.getCurrentUserEmail();
            packmanExclusiveService.createPackagingTypeItem(packagingTypeItem, userEmail, packagingTypeCode, storeCode,property);
            returnRequired = false;
            map.put("message", "Packaging type is created successfully..");
        } catch (IllegalArgumentException e){
            map.put("message", e.getMessage());
            LOG.error("Exception - "+e.getMessage());
        } catch (Exception e){
            map.put("message", "Unexpected error has occcurred");
            LOG.error("Unexpected error has occcurred");
        }
        attributesForPackagingTypeItem(map, storeCode, packagingTypeCode, sideBarId, null);
        if (returnRequired) {
            map.put("packagingTypeItemForm", packagingTypeItemForm);
        }
        return "/admin/packman/packaging/createPackagingTypeItem";
    }

    @RequestMapping("{storeCode}/getPackagingTypeItemInfo")
    public @ResponseBody JqgridResponse<PackagingTypeItemDetailDTO> getPackagingTypeItemInfo(ModelMap modelMap,
            @RequestParam(value = "page", required = false, defaultValue = "1") int page, @RequestParam(value = "rows", required = false, defaultValue = "40") int rows,
            @PathVariable("storeCode") String storeCode, @RequestParam(value = "packagingTypeCode", required = true) String packagingTypeCode) {
        JqgridResponse<PackagingTypeItemDetailDTO> resp = new JqgridResponse<PackagingTypeItemDetailDTO>();
        List<PackagingTypeItemDetailDTO> dtoList = null;
        dtoList = packmanConverterService.getAllPackagingTypeItemDetails(storeCode, packagingTypeCode);
        int records = dtoList.size();
        resp.setRecords(String.valueOf(records));
        Integer pages = records == rows ? records / rows : records / rows + 1;
        resp.setTotal(pages);
        resp.setPage(String.valueOf(page));
        resp.setRows(dtoList);
        return resp;
    }

    @RequestMapping("{storeCode}/createslab")
    @GenerateSidebarData
    public String createSlab(ModelMap map, @PathVariable("storeCode") String storeCode, @RequestParam(value = "packagingTypeCode", required = true) String packagingTypeCode,
            @RequestParam(value = "pageId", required = false) String sideBarId) {
        attributesForSlab(map, storeCode, packagingTypeCode, sideBarId, null);
        return "/admin/packman/packaging/createSlab";
    }

    private void attributesForSlab(ModelMap map, String storeCode, String packagingTypeCode, String id, String message) {
        map.put("storeCode", storeCode);
        map.put("packagingTypeCode", packagingTypeCode);
        if (null != message)
            map.put("message", message);
        map.put("id", id);
    }
    
    @RequestMapping("recalculateVolWt")
    @GenerateSidebarData
    public String recalculateVolWt(ModelMap map) {
        return "/admin/packman/packaging/recalculateVolWeight";
    }

    @RequestMapping("recalculatevolumetricweight")
    @GenerateSidebarData
    public String recalculateVolumetricWeight(ModelMap map) {
        LOG.info("Request Received to recalculate vol wt..");
        try {
            packmanExclusiveService.reCalculateVolWt(WebContextUtils.getCurrentUserEmail());
            map.put("message", "Vol wt recalculated successfully..");
        } catch (IllegalArgumentException e) {
            map.put("message", e.getMessage());
            LOG.error("Exception : {}", e);
        } catch (Exception e) {
            map.put("message", "Unexpected error occurred");
            LOG.error("unexpected error - {}", e);
        }
        return "/admin/packman/packaging/recalculateVolWeight";
    }

}
