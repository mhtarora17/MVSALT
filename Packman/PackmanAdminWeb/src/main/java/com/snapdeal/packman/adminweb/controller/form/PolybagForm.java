/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.packman.adminweb.controller.form;

import javax.validation.constraints.NotNull;

/**
 * @author nitish
 */
public class PolybagForm {

    @NotNull
    private String polybagCode;

    
    private String polybagDescription;

    @NotNull
    private String length;

    @NotNull
    private String breadth;

    @NotNull
    private String height;

    public String getPolybagCode() {
        return polybagCode;
    }

    public void setPolybagCode(String polybagCode) {
        this.polybagCode = polybagCode;
    }

    public String getPolybagDescription() {
        return polybagDescription;
    }

    public void setPolybagDescription(String polybagDescription) {
        this.polybagDescription = polybagDescription;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getBreadth() {
        return breadth;
    }

    public void setBreadth(String breadth) {
        this.breadth = breadth;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    @Override
    public String toString() {
        return "PolybagForm [polybagCode=" + polybagCode + ", polybagDescription=" + polybagDescription + ", length=" + length + ", breadth=" + breadth + ", height=" + height
                + "]";
    }

}
