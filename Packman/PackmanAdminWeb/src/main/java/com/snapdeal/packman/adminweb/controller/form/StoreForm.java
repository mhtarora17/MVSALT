/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.adminweb.controller.form;

import org.hibernate.validator.constraints.NotEmpty;

/**
 *  
 *  @version     1.0, 07-Dec-2015
 *  @author brijesh
 */
public class StoreForm {

    @NotEmpty
    private String storeFrontId;

    @NotEmpty
    private String storeName;
    
    @NotEmpty
    private String storeCode;
    
    private String description;

    private String labels;
    
    public String getStoreFrontId() {
        return storeFrontId;
    }
    
    public void setStoreFrontId(String storeFrontId) {
        this.storeFrontId = storeFrontId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getStoreCode() {
        return storeCode;
    }

    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLabels() {
        return labels;
    }

    public void setLabels(String labels) {
        this.labels = labels;
    }

    @Override
    public String toString() {
        return "StoreForm [storeFrontId=" + storeFrontId + ", storeName=" + storeName + ", storeCode=" + storeCode + ", description=" + description + ", labels=" + labels + "]";
    }
}
