/**
 *  Copyright 2016 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.packman.adminweb.aspect;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.List;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.support.BindingAwareModelMap;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.gson.Gson;
import com.snapdeal.cocofs.commonweb.security.CocofsUser;
import com.snapdeal.cocofs.commonweb.utils.WebContextUtils;
import com.snapdeal.cocofs.entity.User;
import com.snapdeal.packman.services.packaging.IPackmanExclusiveService;
import com.snapdeal.packman.services.packaging.dto.SideBar;

/**
 * @version 1.0, 04-Jan-2016
 * @author brijesh
 */
@Aspect
@Service("PackmanSidebarAspectHandler")
public class PackmanSidebarAspectHandler {

    //TODO add singleton util for concurrentMap
    @Autowired
    IPackmanExclusiveService packmanExclusiveService;

    @Around("@annotation(com.snapdeal.packman.annotations.GenerateSidebarData)")
    public Object validateGenericPersistence(ProceedingJoinPoint pjp) throws Throwable {
        Object[] args = pjp.getArgs();

        for (Object arg : args) {
            if (arg != null && BindingAwareModelMap.class.getSimpleName().equals(arg.getClass().getSimpleName())) {

                BindingAwareModelMap map = (BindingAwareModelMap) arg;
                CocofsUser currentUser = WebContextUtils.getCurrentUser();
                User user = currentUser.getUser();

                MethodSignature signature = (MethodSignature) pjp.getSignature();
                Method method = signature.getMethod();

                RequestParam requestParam = null;

                Annotation[][] parameterAnnotations = method.getParameterAnnotations();
                for (int argIndex = 0; argIndex < args.length; argIndex++) {
                    for (Annotation annotation : parameterAnnotations[argIndex]) {
                        if (!(annotation instanceof RequestParam))
                            continue;
                        requestParam = (RequestParam) annotation;
                        if ("pageId".equals(requestParam.value())) {
                            map.put("pageId", args[argIndex]);
                        }
                    }
                }
                if (user != null) {
                    boolean specialUser = currentUser.hasRole("admin") || currentUser.hasRole("tech");
                    List<SideBar> sideBar = packmanExclusiveService.getSideBar(user.getEmail(),specialUser);
                    map.addAttribute("jsonvalue", new Gson().toJson(sideBar));
                }
            }
        }

        return pjp.proceed();
    }

}
