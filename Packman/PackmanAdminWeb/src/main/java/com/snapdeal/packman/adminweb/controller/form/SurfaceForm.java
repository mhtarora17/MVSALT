/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.packman.adminweb.controller.form;

import javax.validation.constraints.NotNull;

/**
 * @author nitish
 */
public class SurfaceForm {

    @NotNull
    private String surfaceType;

    private String surfaceDescription;

    public String getSurfaceType() {
        return surfaceType;
    }

    public void setSurfaceType(String surfaceType) {
        this.surfaceType = surfaceType;
    }

    public String getSurfaceDescription() {
        return surfaceDescription;
    }

    public void setSurfaceDescription(String surfaceDescription) {
        this.surfaceDescription = surfaceDescription;
    }

    @Override
    public String toString() {
        return "SurfaceForm [surfaceType=" + surfaceType + ", surfaceDescription=" + surfaceDescription + "]";
    }

}
