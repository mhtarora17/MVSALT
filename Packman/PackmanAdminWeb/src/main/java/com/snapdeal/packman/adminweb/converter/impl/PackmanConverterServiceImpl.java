/**
 *  Copyright 2016 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.packman.adminweb.converter.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.snapdeal.base.cache.CacheManager;
import com.snapdeal.base.utils.CollectionUtils;
import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.cocofs.commonweb.utils.WebContextUtils;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.mongo.model.SupcPackagingTypeMappingUnit;
import com.snapdeal.packman.adminweb.controller.form.PackagingTypeForm;
import com.snapdeal.packman.adminweb.controller.form.PackagingTypeItemForm;
import com.snapdeal.packman.adminweb.controller.form.StoreForm;
import com.snapdeal.packman.adminweb.converter.IPackmanConverterService;
import com.snapdeal.packman.adminweb.dto.PackagingTypeDetailDTO;
import com.snapdeal.packman.adminweb.dto.PackagingTypeItemDetailDTO;
import com.snapdeal.packman.adminweb.dto.StoreDetailDTO;
import com.snapdeal.packman.configuration.PackagingProperty;
import com.snapdeal.packman.configuration.PackagingTypeItemProperty;
import com.snapdeal.packman.dao.IPackmanExclusivelyDao;
import com.snapdeal.packman.dataaccess.supcpt.ISupcPackagingTypeMongoService;
import com.snapdeal.packman.entity.PackagingType;
import com.snapdeal.packman.entity.PackagingTypeItem;
import com.snapdeal.packman.entity.PackagingTypeItemProperties;
import com.snapdeal.packman.entity.PackagingTypeProperties;
import com.snapdeal.packman.entity.Slab;
import com.snapdeal.packman.entity.Store;
import com.snapdeal.packman.entity.StoreProperties;
import com.snapdeal.packman.enums.PackagingMode;
import com.snapdeal.packman.enums.PackagingRuleParam;
import com.snapdeal.packman.enums.PackagingTypeCategory;
import com.snapdeal.packman.enums.PackagingTypeRuleCriteria;
import com.snapdeal.packman.enums.StoreAttribute;
import com.snapdeal.packman.rule.dto.PackmanRulesDTO;
import com.snapdeal.packman.rule.dto.SearchRuleDTO;
import com.snapdeal.packman.services.packaging.IPackmanExclusiveService;
import com.snapdeal.packman.services.packaging.dto.PackagingTypeItemDTO;
import com.snapdeal.packman.services.rules.IPackmanRuleService;
import com.snapdeal.rule.cache.BlocksCache;
import com.snapdeal.rule.engine.entity.Rule;
import com.snapdeal.rule.engine.entity.RuleParams;

/**
 * @version 1.0, 05-Jan-2016
 * @author brijesh
 */

@Service("packmanConverterService")
public class PackmanConverterServiceImpl implements IPackmanConverterService {

    private static final Logger    LOG = LoggerFactory.getLogger(PackmanConverterServiceImpl.class);

    @Autowired
    IPackmanExclusiveService       packmanExclusiveService;

    @Autowired
    IPackmanRuleService            packmanRuleService;

    @Autowired
    IPackmanExclusivelyDao         packmanExclusivelyDao;

    @Autowired
    ISupcPackagingTypeMongoService supcPackagingTypeMongoService;

    @Override
    public Store getStoreFromStoreForm(StoreForm storeForm) {
        Store store = new Store();
        store.setStoreFrontId(storeForm.getStoreFrontId());
        store.setCode(storeForm.getStoreCode());
        store.setName(storeForm.getStoreName());
        store.setDescription(storeForm.getDescription());
        store.setEnabled(true);
        return store;
    }

    @Override
    public PackagingType getPackagingTypeFromForm(PackagingTypeForm packagingTypeForm) throws Exception {
        String storeCode = packagingTypeForm.getStoreCode();
        String packagingTypeCode = packagingTypeForm.getPackagingTypeCode();
        PackagingType packagingType = new PackagingType();
        packagingType.setType(packagingTypeCode);
        packagingType.setPackagingMode(packagingTypeForm.getPackagingMode());
        packagingType.setDescription(packagingTypeForm.getPackagingDescription());
        Store store = packmanExclusiveService.getStore(storeCode);
        if (store == null) {
            LOG.error("Unable to find store {} while saving packaging type {}", storeCode, packagingTypeCode);
            throw new Exception("Unable to find store for provided store code : " + storeCode);
        }
        packagingType.setEnabled(packagingTypeForm.isEnabled());
        packagingType.setStore(store);
        packagingType.setCreated(DateUtils.getCurrentTime());
        return packagingType;
    }

    private void getPropertyListFromForm(PackagingTypeForm packagingTypeForm, Map<String, String> map, List<PackagingTypeProperties> packagingTypeProperties) throws Exception {
        for (Map.Entry<String, String> entry : map.entrySet()) {
            PackagingTypeProperties property = new PackagingTypeProperties();
            PackagingProperty propertyByPropertyName = PackagingProperty.getPropertyByPropertyName(entry.getKey());
            if (propertyByPropertyName == null)
                throw new Exception("Packaging Type Property not found : Property Name = " + entry.getKey());
            property.setName(propertyByPropertyName.mode());
            property.setValue(entry.getValue());
            property.setEnabled(true);
            packagingTypeProperties.add(property);
        }
    }

    public PackagingTypeItemDTO enrichingPackagingTypeItemDTO(PackagingTypeItemForm packagingTypeItemForm, String storeCode) {
        PackagingTypeItemDTO packagingTypeItemdto = new PackagingTypeItemDTO();
        // TODO set packaging type while saving
        String packagingTypeCode = packagingTypeItemForm.getPackagingTypeCode();
        PackagingType packagingType = packmanExclusiveService.getPackagingType(packagingTypeCode, storeCode);
        String packagingTypeItemCode = packagingTypeItemForm.getPackagingTypeItemCode();
        if (packagingType == null) {
            LOG.error("Unable to find packaging type for provided packaging type item [{}], with packaging type [{}] and store [{}]", packagingTypeItemCode, packagingTypeCode,
                    storeCode);
            throw new IllegalArgumentException("Unable to find packaging type for packaging type item [" + packagingTypeItemCode + "]" + " with packaging type ["
                    + packagingTypeCode + "]" + " and store [" + storeCode + "].");
        }
        packagingTypeItemdto.setPackagingType(packagingType);
        packagingTypeItemdto.setCode(packagingTypeItemCode);
        packagingTypeItemdto.setDescription(packagingTypeItemForm.getDescription());
        packagingTypeItemdto.setLength(packagingTypeItemForm.getLength());
        packagingTypeItemdto.setBreadth(packagingTypeItemForm.getBreadth());
        packagingTypeItemdto.setHeight(packagingTypeItemForm.getHeight());
        packagingTypeItemdto.setPackagingTypeCategory(packagingTypeItemForm.getPackagingTypeCat());
        return packagingTypeItemdto;
    }

    @Override
    public List<StoreDetailDTO> getAllStoreDetails() {
        List<Store> storeList = packmanExclusiveService.getAllStoreDetails();
        List<StoreDetailDTO> storeDetailsListDTO = new ArrayList<StoreDetailDTO>();
        if (!CollectionUtils.isEmpty(storeList)) {
            int i = 0;
            for (Store store : storeList) {
                StoreDetailDTO storeDetailDTO = new StoreDetailDTO();
                storeDetailDTO.setId(++i);
                storeDetailDTO.setCode(store.getCode());
                storeDetailDTO.setName(store.getName());
                storeDetailDTO.setStoreFrontId(store.getStoreFrontId());
                storeDetailDTO.setEnabled(store.isEnabled());
                storeDetailDTO.setDescription(store.getDescription());
                StoreProperties fetchStoreProperty = packmanExclusiveService.fetchStoreProperty(store.getCode(), StoreAttribute.LABEL.getCode());
                if (fetchStoreProperty != null) {
                    storeDetailDTO.setLabels(fetchStoreProperty.getValue());
                }
                storeDetailsListDTO.add(storeDetailDTO);
            }
        }
        LOG.info("StoreDetails " + storeDetailsListDTO);
        return storeDetailsListDTO;
    }

    @Override
    public List<PackagingTypeDetailDTO> getAllPackagingTypeDetails(String storeCode) {
        List<PackagingType> packagingTypeList = packmanExclusiveService.getAllPackagingType(storeCode);
        List<PackagingTypeDetailDTO> packagingTypeDetailsListDTO = new ArrayList<PackagingTypeDetailDTO>();
        if (!CollectionUtils.isEmpty(packagingTypeList)) {
            int i = 0;
            for (PackagingType packagingType : packagingTypeList) {
                PackagingTypeDetailDTO packagingTypeDetailDTO = new PackagingTypeDetailDTO();
                packagingTypeDetailDTO.setId(++i);
                packagingTypeDetailDTO.setType(packagingType.getType());
                packagingTypeDetailDTO.setMode(packagingType.getPackagingMode());
                packagingTypeDetailDTO.setCode(packagingType.getStore().getCode());
                packagingTypeDetailDTO.setDescription(packagingType.getDescription());
                packagingTypeDetailDTO.setEnabled(packagingType.isEnabled());
                packagingTypeDetailsListDTO.add(packagingTypeDetailDTO);
                setPackagingTypeCategory(packagingTypeDetailDTO,packagingType);
            }
        }
        LOG.info("PackagingTypeDetails " + packagingTypeDetailsListDTO);
        return packagingTypeDetailsListDTO;
    }

    private void setPackagingTypeCategory(PackagingTypeDetailDTO packagingTypeDetailDTO, PackagingType packagingType) {
        List<PackagingTypeProperties> prop = packagingType.getProperties();
        if(!CollectionUtils.isEmpty(prop)){
            for(PackagingTypeProperties p : prop){
                if(p.getName().equals(PackagingProperty.PACKAGING_TYPE_CATEGORY.mode())){
                    packagingTypeDetailDTO.setPackagingTypeCategory(p.getValue());
                    return;
                }
            }
        }
    }

    @Override
    public List<PackagingTypeItemDetailDTO> getAllPackagingTypeItemDetails(String storeCode, String packagingTypeCode) {
        List<PackagingTypeItem> packagingTypeItemList = packmanExclusiveService.getAllPackagingTypeItemDetails(storeCode, packagingTypeCode);
        List<PackagingTypeItemDetailDTO> packagingTypeItemDetailsListDTO = new ArrayList<PackagingTypeItemDetailDTO>();
        Slab slab = null;
        if (!CollectionUtils.isEmpty(packagingTypeItemList)) {
            int i = 0;
            for (PackagingTypeItem packagingTypeItem : packagingTypeItemList) {
                PackagingTypeItemDetailDTO packagingTypeItemDetailDTO = new PackagingTypeItemDetailDTO();
                packagingTypeItemDetailDTO.setId(++i);
                String packagingTypeItemCode = packagingTypeItem.getCode();
                packagingTypeItemDetailDTO.setCode(packagingTypeItemCode);
                packagingTypeItemDetailDTO.setDescription(packagingTypeItem.getDescription());
                PackagingType packagingType = packagingTypeItem.getPackagingType();
                packagingTypeItemDetailDTO.setPackagingType(packagingType.getType());
                if (packagingType.getPackagingMode().equals(PackagingMode.NORMAL.mode())) {
                    // Fetch from property table in case of normal packaging type
                    List<PackagingTypeItemProperties> properties = packmanExclusiveService.getPackagingTypeItemProperties(packagingTypeItem);
                    enrichDtoFromPTIProperties(properties,packagingTypeItemDetailDTO);
                }
                packagingTypeItemDetailsListDTO.add(packagingTypeItemDetailDTO);
            }
        }
        LOG.info("PackagingTypeDetails : " + packagingTypeItemDetailsListDTO);
        return packagingTypeItemDetailsListDTO;
    }

    private void enrichDtoFromPTIProperties(List<PackagingTypeItemProperties> properties, PackagingTypeItemDetailDTO dto) {
        if(properties != null){
            for(PackagingTypeItemProperties prop : properties){
                if(prop.getName().equals(PackagingTypeItemProperty.LENGTH.getName())){
                    dto.setLength(Float.parseFloat(prop.getValue()));
                } else if(prop.getName().equals(PackagingTypeItemProperty.BREADTH.getName())){
                    dto.setBreadth(Float.parseFloat(prop.getValue()));
                } else if(prop.getName().equals(PackagingTypeItemProperty.HEIGHT.getName())){
                    dto.setHeight(Float.parseFloat(prop.getValue()));
                } else if(prop.getName().equals(PackagingTypeItemProperty.VOL_WEIGHT.getName())){
                    dto.setVolWeight(Double.parseDouble(prop.getValue()));
                }
            }
        }
    }

    @Override
    public List<PackmanRulesDTO> getAllRules(SearchRuleDTO dto) {
        List<Rule> ruleList = packmanRuleService.getMatchingRules(dto);
        List<PackmanRulesDTO> ruleDTOList = new ArrayList<PackmanRulesDTO>();
        int count = 1;
        BlocksCache cache = CacheManager.getInstance().getCache(BlocksCache.class);
        if (cache == null) {
            LOG.info("Getting block cache as null");
            throw new IllegalArgumentException("Error occurred while fetching data");
        }
        for (Rule r : ruleList) {
            if (r.isEnabled() == dto.isEnabled() && ((dto.getCreatedby() == null) || (r.getCreatedBy().equals(dto.getCreatedby())))) {
                PackmanRulesDTO ruleDTO = new PackmanRulesDTO();
                ruleDTO.setId(count++);
                ruleDTO.setRuleCode(r.getCode());
                ruleDTO.setRuleName(r.getName());
                String blockName = cache.getBlockById(r.getBlockId()).getName();
                if (blockName == null) {
                    LOG.info("Unable to find appropriate block for rule : " + r.getName());
                    throw new IllegalArgumentException("Error occurred while fetching data");
                }
                ruleDTO.setBlockName(blockName);
                Set<RuleParams> ruleParams = r.getRuleParams();
                if (ruleParams != null) {
                    for (RuleParams rp : ruleParams) {
                        if (rp.getParamName().equals(PackagingRuleParam.RETURN_VALUE.getCode())) {
                            ruleDTO.setPackagingType(rp.getParamValue());
                            break;
                        }
                    }
                }
                ruleDTO.setStartDate(r.getStartDate());
                ruleDTO.setEndDate(r.getEndDate());
                ruleDTO.setRemark(r.getRemark());
                ruleDTO.setSynched(r.isSynched());
                ruleDTO.setBanned(r.isBanned());
                ruleDTO.setEnabled(r.isEnabled());
                ruleDTOList.add(ruleDTO);
            }
        }
        return ruleDTOList;
    }

    @Override
    public SearchRuleDTO createSearchRuleDto(String storeCode, String category, String subcategory, List<String> brand, List<String> supercategory, String name, String createdby,
            boolean enabled, String packagingType) {
        SearchRuleDTO searchRuleDTO = new SearchRuleDTO();
        searchRuleDTO.setStoreCode(storeCode);
        searchRuleDTO.setCategory(category);
        searchRuleDTO.setSubcategory(subcategory);
        
        // FIXME Checking for empty because by default request param returns variable as empty. but we require input as null.
        if(!CollectionUtils.isEmpty(brand))
           searchRuleDTO.setBrand(brand);
        searchRuleDTO.setSupercategory(supercategory);
        searchRuleDTO.setRulename(name);
        searchRuleDTO.setCreatedby(createdby);
        searchRuleDTO.setEnabled(enabled);
        searchRuleDTO.setPackagingType(packagingType);
        return searchRuleDTO;
    }

    @Override
    public List<PackmanRulesDTO> getPackagingTypeMappingForStoreCodeAndSupc(String supc, String storeCode) {

        List<SupcPackagingTypeMappingUnit> supcPackagingTypeMappingUnits = new ArrayList<SupcPackagingTypeMappingUnit>();
        SupcPackagingTypeMappingUnit supcPackagingTypeMappingUnit = supcPackagingTypeMongoService.getSupcPackagingTypeMappingUnit(supc, storeCode);
        supcPackagingTypeMappingUnits.add(supcPackagingTypeMappingUnit);
        return getPackmanRulesDtoFromRuleUnit(supcPackagingTypeMappingUnits);

    }
    
    @Override
    public List<PackmanRulesDTO> getPackagingTypeMappingByPackagingType(String packagingType,boolean enabled) {

        List<SupcPackagingTypeMappingUnit> supcPackagingTypeMappingUnit = supcPackagingTypeMongoService.getSupcPackagingTypeMappingUnitByPackagingType(packagingType,enabled);
        return getPackmanRulesDtoFromRuleUnit(supcPackagingTypeMappingUnit);

    }

    private List<PackmanRulesDTO> getPackmanRulesDtoFromRuleUnit(List<SupcPackagingTypeMappingUnit> supcPackagingTypeMappingUnit) {
        List<PackmanRulesDTO> dtoList = new ArrayList<PackmanRulesDTO>();
        if(supcPackagingTypeMappingUnit!=null){
            for(SupcPackagingTypeMappingUnit s : supcPackagingTypeMappingUnit){
                PackmanRulesDTO packmanRulesDTO = new PackmanRulesDTO();
                if (supcPackagingTypeMappingUnit != null) {
                    packmanRulesDTO.setPackagingType(s.getPackagingType());
                    packmanRulesDTO.setEnabled(s.isEnabled());
                    packmanRulesDTO.setBlockName("Supc Block");
                    packmanRulesDTO.setRuleName(s.getStoreCode() + "-" + s.getSupc());
                    packmanRulesDTO.setId(1);
                }
                if (supcPackagingTypeMappingUnit != null){
                    dtoList.add(packmanRulesDTO);
                }
            }
        }
        return dtoList;
    }

    @Override
    public List<PackagingTypeProperties> getPackagingTypePropertiesFromForm(PackagingTypeForm packagingTypeForm, PackagingType packagingType) throws Exception {
        List<PackagingTypeProperties> packagingTypeProperties = new ArrayList<PackagingTypeProperties>();
        getPropertyListFromForm(packagingTypeForm, packagingTypeForm.getFilledPackagingPropertiesRequired(), packagingTypeProperties);
        if (packagingType.getPackagingMode().equals(PackagingMode.NORMAL.mode()))
            getPropertyListFromForm(packagingTypeForm, packagingTypeForm.getFilledPackagingPropertiesOptional(), packagingTypeProperties);
        return packagingTypeProperties;
    }

    @Override
    // Return packaging type item and its properties
    public PackagingTypeItem getPackagingTypeItemFromDto(PackagingTypeItemDTO packagingTypeItemdto) {
        // TODO move below lines to single service
        PackagingTypeItem packagingTypeItem = new PackagingTypeItem();
        packagingTypeItem.setCode(packagingTypeItemdto.getCode());
        packagingTypeItem.setDescription(packagingTypeItemdto.getDescription());
        packagingTypeItem.setUpdatedBy(WebContextUtils.getCurrentUserEmail());
        packagingTypeItem.setCreated(DateUtils.getCurrentTime());
        packagingTypeItem.setLastUpdated(DateUtils.getCurrentTime());
        return packagingTypeItem;
    }

    @Override
    public List<PackagingTypeItemProperties> getPackagingTypeItemProperties(PackagingTypeItemDTO dto) {
        
        if (dto.getPackagingType().getPackagingMode().equals(PackagingMode.PICKNPACK.mode())) {
            return null;
        }
        
        List<PackagingTypeItemProperties> property = new ArrayList<PackagingTypeItemProperties>();
        
        PackagingTypeItemProperties prop1 = new PackagingTypeItemProperties();
        prop1.setName(PackagingTypeItemProperty.LENGTH.getName());
        prop1.setValue(Float.toString(dto.getLength()));
        property.add(prop1);
        
        PackagingTypeItemProperties prop2 = new PackagingTypeItemProperties();
        prop2.setName(PackagingTypeItemProperty.BREADTH.getName());
        prop2.setValue(Float.toString(dto.getBreadth()));
        property.add(prop2);
        
        if(dto.getPackagingTypeCategory().equals(PackagingTypeCategory.THREE_DIMENSIONAL.type())){
            PackagingTypeItemProperties prop3 = new PackagingTypeItemProperties();
            prop3.setName(PackagingTypeItemProperty.HEIGHT.getName());
            prop3.setValue(Float.toString(dto.getHeight()));
            property.add(prop3);
            
            PackagingTypeItemProperties prop4 = new PackagingTypeItemProperties();
            prop4.setName(PackagingTypeItemProperty.VOL_WEIGHT.getName());
            property.add(prop4);
        }

        
        return property;
    }

    @Override
    public StoreProperties getStorePropertyFromForm(String label) {
        StoreProperties property = new StoreProperties();
        property.setName(PackagingTypeRuleCriteria.LABEL.getName());
        property.setValue(label);
        property.setEnabled(true);
        return property;
    }

    @Override
    public PackagingTypeForm enrichPackagingTypeForm(String storeCode, String packagingTypeCode) {
        PackagingType p = packmanExclusiveService.getPackagingType(packagingTypeCode, storeCode);
        if (p == null) {
            LOG.error("unable to find packaging type for packaging type code [{}] and store code {}", packagingTypeCode, storeCode);
            throw new IllegalArgumentException("unable to find packaging type for packaging type code [" + packagingTypeCode + "] and store code [" + storeCode + "]");
        }
        PackagingTypeForm form = new PackagingTypeForm();
        form.setStoreCode(storeCode);
        form.setPackagingTypeCode(packagingTypeCode);
        form.setPackagingMode(p.getPackagingMode());
        form.setPackagingDescription(p.getDescription());
        form.setEnabled(p.isEnabled());

        Map<String, String> map = packmanExclusiveService.getAllPackagingTypeProperties(p);

        form.setVALIDATION_ENABLED(map.get(PackagingProperty.VALIDATION_ENABLED.mode()));
        form.setPACKAGING_TYPE_PREFIX(map.get(PackagingProperty.PACKAGING_TYPE_PREFIX.mode()));

        if (p.getPackagingMode().equals(PackagingMode.NORMAL.mode())) {
            form.setPackagingTypeCat(map.get(PackagingProperty.PACKAGING_TYPE_CATEGORY.mode()));
        }
        form.setId(p.getId());
        return form;
    }

    @Override
    @Transactional
    public List<PackagingTypeProperties> getProperties(PackagingTypeForm packagingTypeForm, PackagingType packagingType) {
        String packagingTypeCode = packagingTypeForm.getPackagingTypeCode();
        String storeCode = packagingTypeForm.getStoreCode();
        List<PackagingTypeProperties> prop = packmanExclusiveService.getAllPackagingTypeProperty(packagingType);
        if (prop == null) {
            throw new IllegalArgumentException("Unable to find packaging type property for packaging type [" + packagingTypeCode + "] and store [" + storeCode + "]");
        }
        for (PackagingTypeProperties pro : prop) {
            if (pro.getName().equals(PackagingProperty.VALIDATION_ENABLED.mode())) {
                pro.setValue(packagingTypeForm.getVALIDATION_ENABLED());
            } else if (pro.getName().equals(PackagingProperty.PACKAGING_TYPE_PREFIX.mode())) {
                pro.setValue(packagingTypeForm.getPACKAGING_TYPE_PREFIX());
            }
        }
        return prop;
    }

    @Override
    public PackagingType getPackagingType(PackagingTypeForm packagingTypeForm) {
        String storeCode = packagingTypeForm.getStoreCode();
        String packagingTypeCode = packagingTypeForm.getPackagingTypeCode();
        PackagingType p = packmanExclusiveService.getPackagingType(packagingTypeCode, storeCode);
        if (p == null) {
            throw new IllegalArgumentException("Unable to find packaging type for packaging type [" + packagingTypeCode + "] and store [" + storeCode + "]");
        }
        p.setEnabled(packagingTypeForm.isEnabled());
        p.setDescription(packagingTypeForm.getPackagingDescription());
        return p;
    }

    @Override
    public PackagingTypeForm getPackagingTypeForm() {
        PackagingTypeForm packagingTypeForm = new PackagingTypeForm();
        packagingTypeForm.setMIN_SLAB_SIZE(ConfigUtils.getStringScalar(Property.PACKMAN_MIN_SLAB_SIZE));
        packagingTypeForm.setSLAB_SIZE(ConfigUtils.getStringScalar(Property.PACKMAN_DEFAULT_SLAB_SIZE));
        packagingTypeForm.setMAX_SLAB_SIZE(ConfigUtils.getStringScalar(Property.PACKMAN_MAX_SLAB_SIZE));
        packagingTypeForm.setMAX_VOL_WEIGHT(ConfigUtils.getStringScalar(Property.PACKMAN_MAX_VOL_WEIGHT));
        packagingTypeForm.setSLAB_PREFIX(ConfigUtils.getStringScalar(Property.PACKMAN_SLAB_PREFIX));
        packagingTypeForm.setEnabled(true);
        return packagingTypeForm;
    }

    @Override
    public List<PackagingTypeItemProperties> updatePackagingTypeItemProperties(List<PackagingTypeItemProperties> property, PackagingTypeItemDetailDTO dto) {
        if(property == null)
            return null;
        for(PackagingTypeItemProperties prop : property){
            if(prop.getName().equals(PackagingTypeItemProperty.LENGTH.getName())){
                prop.setValue(Float.toString(dto.getLength()));
            } else if(prop.getName().equals(PackagingTypeItemProperty.BREADTH.getName())){
                prop.setValue(Float.toString(dto.getBreadth()));
            } else if(prop.getName().equals(PackagingTypeItemProperty.HEIGHT.getName())){
                prop.setValue(Float.toString(dto.getHeight()));
            }
        }
        return property;
    }

    @Override
    public List<PackmanRulesDTO> addMoreRules(List<PackmanRulesDTO> dto,List<PackmanRulesDTO> supcRulesDto) {
        int length = dto.size();
        if(dto!=null && supcRulesDto!=null){
            for(PackmanRulesDTO p : supcRulesDto){
                p.setId(++length);
            }
        }
        dto.addAll(supcRulesDto);
        return dto;
    }

}
