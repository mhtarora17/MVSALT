/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.adminweb.dto;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *  
 *  @version     1.0, 09-Dec-2015
 *  @author brijesh
 */
@XmlRootElement
public class SlabDTO {

    private int id;
    
    private String name;
    
    private Integer size;
    
    private Integer lowerLimit;
    
    private Integer maxSize;
    
    private String packagingTypeItems;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Integer getLowerLimit() {
        return lowerLimit;
    }

    public void setLowerLimit(Integer lowerLimit) {
        this.lowerLimit = lowerLimit;
    }

    public Integer getMaxSize() {
        return maxSize;
    }

    public void setMaxSize(Integer maxSize) {
        this.maxSize = maxSize;
    }

    public String getPackagingTypeItems() {
        return packagingTypeItems;
    }

    public void setPackagingTypeItems(String packagingTypeItems) {
        this.packagingTypeItems = packagingTypeItems;
    }

    @Override
    public String toString() {
        return "SlabDTO [id=" + id + ", name=" + name + ", size=" + size + ", lowerLimit=" + lowerLimit + ", maxSize=" + maxSize + ", packagingTypeItems=" + packagingTypeItems
                + "]";
    }

}
