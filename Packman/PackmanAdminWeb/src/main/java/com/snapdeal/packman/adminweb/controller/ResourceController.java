/**
 *  Copyright 2016 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.adminweb.controller;

import java.util.Collections;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.snapdeal.base.cache.CacheManager;
import com.snapdeal.base.utils.CollectionUtils;
import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.cocofs.cache.ProductBrandCache;

/**
 *  
 *  @version     1.0, 14-Jan-2016
 *  @author brijesh
 */

@RequestMapping("/admin/resource/")
@Controller("ResourceController")
public class ResourceController {

    @SuppressWarnings("unchecked")
    @Produces(MediaType.APPLICATION_JSON)
    @RequestMapping("json/getBrands")
    @GET
    public 
    @ResponseBody List<String> getBrandNames(@RequestParam("query") String query) {
        List<String> brands = null;
        if (StringUtils.isNotEmpty(query)) {
            ProductBrandCache brandCache = CacheManager.getInstance().getCache(ProductBrandCache.class);
            if(brandCache != null){
                brands = brandCache.getBrandsWithStartName(query);
            }
        }
        if(CollectionUtils.isEmpty(brands)){
            brands = Collections.EMPTY_LIST;
        }
        return brands;
    }
    
}
