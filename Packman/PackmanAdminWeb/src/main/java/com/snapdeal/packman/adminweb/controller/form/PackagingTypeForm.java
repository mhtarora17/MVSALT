/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.adminweb.controller.form;

import java.util.HashMap;
import java.util.Map;

import org.hibernate.validator.constraints.NotEmpty;

/**
 *  
 *  @version     1.0, 07-Dec-2015
 *  @author brijesh
 */
public class PackagingTypeForm {

    private int id;
    
    @NotEmpty(message="Store Code is Mandatory")
    private String storeCode;
    
    @NotEmpty(message="Packaging Type Code is Mandatory")
    private String packagingTypeCode;
    
    @NotEmpty(message="Packaging Mode is Mandatory")
    private String packagingMode;
    
    // Eg - 2D Or 3D
    @NotEmpty(message="Packaging type category should not be empty")
    private String packagingTypeCat;
    
    private String packagingDescription;
    
    private boolean enabled = false;
    
    private Map<String,String> packagingPropertiesRequired = new HashMap<String,String>();
    
    private Map<String,String> packagingPropertiesOptional = new HashMap<String, String>();
    
    private String VALIDATION_ENABLED;
    
    private String PACKAGING_TYPE_PREFIX;
    
    private String SLAB_PREFIX;
    
    private String SLAB_SIZE;
    
    private String MIN_SLAB_SIZE;
    
    private String MAX_SLAB_SIZE;
    
    private String MAX_VOL_WEIGHT;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStoreCode() {
        return storeCode;
    }

    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }

    public String getPackagingTypeCode() {
        return packagingTypeCode;
    }

    public void setPackagingTypeCode(String packagingTypeCode) {
        this.packagingTypeCode = packagingTypeCode;
    }

    public String getPackagingMode() {
        return packagingMode;
    }

    public void setPackagingMode(String packagingMode) {
        this.packagingMode = packagingMode;
    }

    public String getPackagingTypeCat() {
        return packagingTypeCat;
    }

    public void setPackagingTypeCat(String packagingTypeCat) {
        this.packagingTypeCat = packagingTypeCat;
    }

    public String getPackagingDescription() {
        return packagingDescription;
    }

    public void setPackagingDescription(String packagingDescription) {
        this.packagingDescription = packagingDescription;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Map<String, String> getPackagingPropertiesRequired() {
        return packagingPropertiesRequired;
    }
    
    public Map<String, String> getFilledPackagingPropertiesRequired() {
        packagingPropertiesRequired.put("VALIDATION_ENABLED", VALIDATION_ENABLED);
        packagingPropertiesRequired.put("PACKAGING_TYPE_PREFIX", PACKAGING_TYPE_PREFIX);
        return packagingPropertiesRequired;
    }

    public void setPackagingPropertiesRequired(Map<String, String> packagingPropertiesRequired) {
        this.packagingPropertiesRequired = packagingPropertiesRequired;
    }

    public Map<String, String> getFilledPackagingPropertiesOptional() {
        packagingPropertiesOptional.put("PACKAGING_TYPE_CATEGORY", packagingTypeCat);
        return packagingPropertiesOptional;
    }
    
    public Map<String, String> getPackagingPropertiesOptional() {
        return packagingPropertiesOptional;
    }

    public void setPackagingPropertiesOptional(Map<String, String> packagingPropertiesOptional) {
        this.packagingPropertiesOptional = packagingPropertiesOptional;
    }
    
    public String getVALIDATION_ENABLED() {
        return VALIDATION_ENABLED;
    }

    public void setVALIDATION_ENABLED(String vALIDATION_ENABLED) {
        VALIDATION_ENABLED = vALIDATION_ENABLED;
    }

    public String getPACKAGING_TYPE_PREFIX() {
        return PACKAGING_TYPE_PREFIX;
    }

    public void setPACKAGING_TYPE_PREFIX(String pACKAGING_TYPE_PREFIX) {
        PACKAGING_TYPE_PREFIX = pACKAGING_TYPE_PREFIX;
    }

    public String getSLAB_PREFIX() {
        return SLAB_PREFIX;
    }

    public void setSLAB_PREFIX(String sLAB_PREFIX) {
        SLAB_PREFIX = sLAB_PREFIX;
    }

    public String getSLAB_SIZE() {
        return SLAB_SIZE;
    }

    public void setSLAB_SIZE(String sLAB_SIZE) {
        SLAB_SIZE = sLAB_SIZE;
    }

    public String getMIN_SLAB_SIZE() {
        return MIN_SLAB_SIZE;
    }

    public void setMIN_SLAB_SIZE(String mIN_SLAB_SIZE) {
        MIN_SLAB_SIZE = mIN_SLAB_SIZE;
    }

    public String getMAX_SLAB_SIZE() {
        return MAX_SLAB_SIZE;
    }

    public void setMAX_SLAB_SIZE(String mAX_SLAB_SIZE) {
        MAX_SLAB_SIZE = mAX_SLAB_SIZE;
    }

    public String getMAX_VOL_WEIGHT() {
        return MAX_VOL_WEIGHT;
    }

    public void setMAX_VOL_WEIGHT(String mAX_VOL_WEIGHT) {
        MAX_VOL_WEIGHT = mAX_VOL_WEIGHT;
    }
    
    public String printMap(Map<String,String> map){
        StringBuffer result = new StringBuffer();
        for(Map.Entry<String, String> entry : map.entrySet()){
            result.append(entry.getKey()+" = "+entry.getValue());
        }
        return result.toString();
    }

    @Override
    public String toString() {
        return "PackagingTypeForm [id=" + id + ", storeCode=" + storeCode + ", packagingTypeCode=" + packagingTypeCode + ", packagingMode=" + packagingMode + ", packagingTypeCat="
                + packagingTypeCat + ", packagingDescription=" + packagingDescription + ", enabled=" + enabled + ", packagingPropertiesRequired=" + packagingPropertiesRequired
                + ", packagingPropertiesOptional=" + packagingPropertiesOptional + ", VALIDATION_ENABLED=" + VALIDATION_ENABLED + ", PACKAGING_TYPE_PREFIX="
                + PACKAGING_TYPE_PREFIX + ", SLAB_PREFIX=" + SLAB_PREFIX + ", SLAB_SIZE=" + SLAB_SIZE + ", MIN_SLAB_SIZE=" + MIN_SLAB_SIZE + ", MAX_SLAB_SIZE=" + MAX_SLAB_SIZE
                + ", MAX_VOL_WEIGHT=" + MAX_VOL_WEIGHT + "]";
    }
    
    

}
