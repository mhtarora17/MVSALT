package com.snapdeal.packman.web.response;

import com.snapdeal.base.model.common.ServiceResponse;

public class PackManDTOResponse extends ServiceResponse {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String response;

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}
	
	

}
