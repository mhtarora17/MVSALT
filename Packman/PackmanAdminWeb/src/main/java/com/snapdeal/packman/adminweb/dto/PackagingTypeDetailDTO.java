/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.adminweb.dto;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *  
 *  @version     1.0, 03-Dec-2015
 *  @author brijesh
 */

@XmlRootElement
public class PackagingTypeDetailDTO {
    
    private int id;
    
    private String type;
    
    private String mode;
    
    private String packagingTypeCategory;
    
    private String code;
    
    private String description;
    
    private boolean enabled;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getPackagingTypeCategory() {
        return packagingTypeCategory;
    }

    public void setPackagingTypeCategory(String packagingTypeCategory) {
        this.packagingTypeCategory = packagingTypeCategory;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public String toString() {
        return "PackagingTypeDetailDTO [id=" + id + ", type=" + type + ", mode=" + mode + ", packagingTypeCategory=" + packagingTypeCategory + ", code=" + code + ", description="
                + description + ", enabled=" + enabled + "]";
    }
    
}
