/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.adminweb.controller.form;

/**
 *  
 *  @version     1.0, 09-Dec-2015
 *  @author brijesh
 */
public class SlabForm {

    private String storeCode;
    
    private String packagingTypeCode;
    
    private Integer slabSize;

    public String getStoreCode() {
        return storeCode;
    }

    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }

    public String getPackagingTypeCode() {
        return packagingTypeCode;
    }

    public void setPackagingTypeCode(String packagingTypeCode) {
        this.packagingTypeCode = packagingTypeCode;
    }

    public Integer getSlabSize() {
        return slabSize;
    }

    public void setSlabSize(Integer slabSize) {
        this.slabSize = slabSize;
    }

    @Override
    public String toString() {
        return "SlabForm [storeCode=" + storeCode + ", packagingTypeCode=" + packagingTypeCode + ", slabSize=" + slabSize + "]";
    }
    
}
