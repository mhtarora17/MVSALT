/**
 *  Copyright 2016 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.packman.adminweb.converter;

import java.util.List;

import com.snapdeal.packman.adminweb.controller.form.PackagingTypeForm;
import com.snapdeal.packman.adminweb.controller.form.PackagingTypeItemForm;
import com.snapdeal.packman.adminweb.controller.form.StoreForm;
import com.snapdeal.packman.adminweb.dto.PackagingTypeDetailDTO;
import com.snapdeal.packman.adminweb.dto.PackagingTypeItemDetailDTO;
import com.snapdeal.packman.adminweb.dto.SlabDTO;
import com.snapdeal.packman.adminweb.dto.StoreDetailDTO;
import com.snapdeal.packman.entity.PackagingType;
import com.snapdeal.packman.entity.PackagingTypeItem;
import com.snapdeal.packman.entity.PackagingTypeItemProperties;
import com.snapdeal.packman.entity.PackagingTypeProperties;
import com.snapdeal.packman.entity.Store;
import com.snapdeal.packman.entity.StoreProperties;
import com.snapdeal.packman.rule.dto.PackmanRulesDTO;
import com.snapdeal.packman.rule.dto.SearchRuleDTO;
import com.snapdeal.packman.services.packaging.dto.PackagingTypeItemDTO;

/**
 * @version 1.0, 05-Jan-2016
 * @author brijesh
 */
public interface IPackmanConverterService {

    Store getStoreFromStoreForm(StoreForm storeForm);

    PackagingType getPackagingTypeFromForm(PackagingTypeForm packagingTypeForm) throws Exception;

    PackagingTypeItemDTO enrichingPackagingTypeItemDTO(PackagingTypeItemForm packagingTypeItemForm, String storeCode);

    List<StoreDetailDTO> getAllStoreDetails();

    List<PackagingTypeDetailDTO> getAllPackagingTypeDetails(String storeCode);

    List<PackagingTypeItemDetailDTO> getAllPackagingTypeItemDetails(String storeCode, String packagingTypeCode);

    List<PackmanRulesDTO> getAllRules(SearchRuleDTO dto);

    SearchRuleDTO createSearchRuleDto(String storeCode, String category, String subcategory, List<String> brand, List<String> supercategory, String name, String createdby,
            boolean enabled, String packagingType);

    List<PackmanRulesDTO> getPackagingTypeMappingForStoreCodeAndSupc(String supc, String storeCode);

    List<PackagingTypeProperties> getPackagingTypePropertiesFromForm(PackagingTypeForm packagingTypeForm, PackagingType packagingType) throws Exception;

    PackagingTypeItem getPackagingTypeItemFromDto(PackagingTypeItemDTO packagingTypeItemdto);

    StoreProperties getStorePropertyFromForm(String label);

    PackagingTypeForm enrichPackagingTypeForm(String storeCode, String packagingTypeCode);

    List<PackagingTypeProperties> getProperties(PackagingTypeForm packagingTypeForm, PackagingType packagingType);

    PackagingType getPackagingType(PackagingTypeForm packagingTypeForm);

    PackagingTypeForm getPackagingTypeForm();

    List<PackagingTypeItemProperties> getPackagingTypeItemProperties(PackagingTypeItemDTO dto);

    List<PackagingTypeItemProperties> updatePackagingTypeItemProperties(List<PackagingTypeItemProperties> property, PackagingTypeItemDetailDTO packagingTypeItemDetailDTO);

    List<PackmanRulesDTO> addMoreRules(List<PackmanRulesDTO> supcRulesDto, List<PackmanRulesDTO> supcRulesDto2);

    List<PackmanRulesDTO> getPackagingTypeMappingByPackagingType(String packagingType, boolean enabled);

}
