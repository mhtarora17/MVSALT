/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.packman.adminweb.controller.form;

import javax.validation.constraints.NotNull;

/**
 * @author nitish
 */
public class CartonForm {

    @NotNull
    private String cartonCode;

    private String cartonDescription;

    @NotNull
    private String length;

    @NotNull
    private String breadth;

    @NotNull
    private String height;

    public String getCartonCode() {
        return cartonCode;
    }

    public void setCartonCode(String cartonCode) {
        this.cartonCode = cartonCode;
    }

    public String getCartonDescription() {
        return cartonDescription;
    }

    public void setCartonDescription(String cartonDescription) {
        this.cartonDescription = cartonDescription;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getBreadth() {
        return breadth;
    }

    public void setBreadth(String breadth) {
        this.breadth = breadth;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    @Override
    public String toString() {
        return "CartonForm [cartonCode=" + cartonCode + ", cartonDescription=" + cartonDescription + ", length=" + length + ", breadth=" + breadth + ", height=" + height + "]";
    }

}
