/**
 *  Copyright 2016 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.packman.adminweb.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.snapdeal.base.fileutils.SDFileUploadUtils;
import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.cocofs.commonweb.utils.ExportToExcelUtils;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.utils.ExcelReader;
import com.snapdeal.packman.adminweb.converter.IPackmanConverterService;
import com.snapdeal.packman.dto.RecommendationParamsDTO;
import com.snapdeal.packman.services.exception.PackagingRecomendationException;
import com.snapdeal.packman.services.packaging.IPackmanExclusiveService;
import com.snapdeal.packman.services.packaging.dto.RecommendationResponseDTO;
import com.snapdeal.packman.services.packaging.recomendation.IPackmanRecommendationService;

/**
 * @version 1.0, 10-Feb-2016
 * @author brijesh
 */

@Controller("packmanRecommendationController")
@RequestMapping(PackmanRecommendationController.URL)
public class PackmanRecommendationController {

    protected static final String URL = "/admin/lookups/packman/";

    private static final Logger   LOG = LoggerFactory.getLogger(PackmanRecommendationController.class);

    @Autowired
    IPackmanConverterService      packmanConverterService;

    @Autowired
    IPackmanExclusiveService      packmanExclusiveService;

    @Autowired
    IPackmanRecommendationService packmanRecommendationService;

    @RequestMapping("recommendation")
    public String lookupsRecoPage(ModelMap map) {
        if (ConfigUtils.getBooleanScalar(Property.PACKMAN_RECO_TEST_PAGE_SHOW)) {
            return "admin/packman/recommendation/index";
        }
        return "admin/packman/recommendation/unavailable";
    }

    @RequestMapping(method = { RequestMethod.GET }, value = "downloadRecommendationTemplate")
    public void downloadRecommendationTemplate(HttpServletRequest req, HttpServletResponse resp) {
        try {
            ExportToExcelUtils.downloadTemplate(resp, RecommendationParamsDTO.class, "RecommendationTemplate");
        } catch (IOException e) {
            LOG.info("Error while exporting template for attributes ", e);
        } catch (ClassNotFoundException e) {
            LOG.info("Error while exporting template for attributes ", e);
        }
    }

    @RequestMapping(method = { RequestMethod.POST }, value = "processRecommendationUpload")
    public void processRecommendationLookup(ModelMap map, @RequestParam(required = true, value = "inputfile") MultipartFile inputFile, HttpServletResponse response,
            ModelMap modelMap) {
        String uploadFilePath = ConfigUtils.getStringScalar(Property.TEMP_DIRECTORY) + "/" + inputFile.getOriginalFilename();

        try {
            SDFileUploadUtils.upload(inputFile.getInputStream(), inputFile.getContentType(), uploadFilePath);

            List<RecommendationParamsDTO> recoDtoList = ExcelReader.readFile(uploadFilePath, RecommendationParamsDTO.class, true);
            if (recoDtoList != null && recoDtoList.size() > 0) {
                List<RecommendationResponseDTO> dtos = new ArrayList<RecommendationResponseDTO>();
                RecommendationResponseDTO resp = null;
                RecommendationParamsDTO req = null;
                for (RecommendationParamsDTO dto : recoDtoList) {
                    try {
                        validateRecommendationDto(dto);
                        req = packmanRecommendationService.getEnrichedRecommendationDto(dto);
                        LOG.info("Checking for {}", req);
                        resp = packmanRecommendationService.getPackagingRecommendation(req);
                        LOG.info("Response : {}", resp);
                        dtos.add(resp);
                    } catch (IllegalArgumentException | PackagingRecomendationException e) {
                        LOG.error("Exception for request {} : {}", dto, e.getMessage());
                        resp = getResponseDtoFromRequestDto(dto);
                        resp.setError(e.getMessage());
                        dtos.add(resp);
                    } catch (Exception e) {
                        LOG.error("Error while getting recommendation for {} ", dto, e);
                        resp = getResponseDtoFromRequestDto(req);
                        resp.setError(e.getMessage());
                        dtos.add(resp);
                    }
                }
                if (dtos.size() > 0) {
                    ExportToExcelUtils.exportToExcel(response, RecommendationResponseDTO.class, inputFile.getOriginalFilename() + "_Result", dtos);
                }
            } else {
                modelMap.addAttribute("errorMessage", "No content found in the file uploaded");
            }

        } catch (Exception e) {
            LOG.error("Error in reading file {}", inputFile, e);
            modelMap.addAttribute("errorMessage", "Error in reading file, please check the content");
        }

    }

    private RecommendationResponseDTO getResponseDtoFromRequestDto(RecommendationParamsDTO dto) {
        RecommendationResponseDTO res = null;
        if (dto != null) {
            res = new RecommendationResponseDTO(dto.getStoreCode(), dto.getPackagingType(), dto.getSupc(), dto.getVolWeight(), dto.getLength(), dto.getBreadth(), dto.getHeight(),
                    dto.isRecommendationBasedOnLBH(), dto.isRecommendationBasedOnDeltaVolWt(), dto.getRecommendationLBHMargin(), dto.getRecommendationDeltaVolWtOffset(),
                    dto.getRecommendationDeltaMultFactor(), null, null, null);
        }
        return res;
    }

    private void validateRecommendationDto(RecommendationParamsDTO dto) {
        boolean lbhNotProvided = dto.getLength() == null || dto.getBreadth() == null || dto.getHeight() == null;
        boolean packagingTypeNotProvided = StringUtils.isEmpty(dto.getPackagingType());
        boolean storeCodeNotProvided = StringUtils.isEmpty(dto.getStoreCode());
        boolean supcNotProvided = StringUtils.isEmpty(dto.getSupc());
        boolean volWtNotProvided = dto.getVolWeight() == null;
        StringBuilder error = new StringBuilder();
        if (storeCodeNotProvided) {
            error.append("Please specify store code\n");
        }
        if (packagingTypeNotProvided) {
            error.append("Please specify packaging type\n");
        }
        if (lbhNotProvided && supcNotProvided && volWtNotProvided) {
            error.append("Please specify atleast LBH or supc or Vol Weight\n");
        }
        if (!lbhNotProvided) {
            if (dto.getLength() < 0 || dto.getBreadth() < 0 || dto.getHeight() < 0) {
                error.append("Please specify valid LBH. Negative value of them are not allowed\n");
            } else if ((dto.getBreadth() * dto.getHeight() * dto.getLength()) <= 0) {
                error.append("Please specify valid LBH.\n");
            }
        }
        if (lbhNotProvided && supcNotProvided && !volWtNotProvided) {
            if (dto.getVolWeight() <= 0) {
                error.append("Please specify valid volumetric weight");
            }
        }
        if (!StringUtils.isEmpty(error.toString())) {
            throw new IllegalArgumentException("\nUnable to recommend : Following can be possible reasons..\n" + error);
        }
    }

}
