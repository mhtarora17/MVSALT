package com.snapdeal.packman.adminweb.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.gson.Gson;
import com.snapdeal.base.utils.CollectionUtils;
import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.cocofs.commonweb.utils.WebContextUtils;
import com.snapdeal.packman.adminweb.converter.IPackmanConverterService;
import com.snapdeal.packman.adminweb.dto.PackagingTypeItemDetailDTO;
import com.snapdeal.packman.adminweb.dto.StoreDetailDTO;
import com.snapdeal.packman.entity.PackagingType;
import com.snapdeal.packman.entity.PackagingTypeItem;
import com.snapdeal.packman.entity.PackagingTypeItemProperties;
import com.snapdeal.packman.entity.Store;
import com.snapdeal.packman.entity.StoreProperties;
import com.snapdeal.packman.enums.PackagingMode;
import com.snapdeal.packman.enums.PackagingTypeRuleCriteria;
import com.snapdeal.packman.enums.StoreAttribute;
import com.snapdeal.packman.services.packaging.IPackmanExclusiveService;
import com.snapdeal.packman.services.rules.IPackmanRuleService;
import com.snapdeal.packman.services.rules.impl.PackmanRuleServiceImpl;
import com.snapdeal.packman.web.response.PackManDTOResponse;
import com.snapdeal.rule.engine.entity.Criteria;
import com.snapdeal.rule.engine.entity.Rule;

@Path("/admin/packmangui/")
@Controller("PackManAjaxController")
public class PackManAjaxController {

    private static final Logger LOG         = LoggerFactory.getLogger(PackManAjaxController.class);

    @Autowired
    IPackmanExclusiveService    packmanExclusiveService;

    @Autowired
    IPackmanConverterService    packmanConverterService;

    @Autowired
    IPackmanRuleService         packmanRuleServiceImpl;

    private final Gson          GSON_OBJECT = new Gson();

    @Path("/updatestore")
    @Produces(MediaType.APPLICATION_JSON)
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public String saveStoreDetail(@RequestParam(value = "storeDetailDTO") StoreDetailDTO storeDetailDTO) {
        PackManDTOResponse resp = new PackManDTOResponse();
        try {
            LOG.info("Request received to update Store info {}", storeDetailDTO);
            updateStore(storeDetailDTO);
            LOG.info("Success: Successfully Updated Store");
        } catch (IllegalArgumentException e ){
            LOG.error("Exception : {}",e);
            resp.setSuccessful(false);
            resp.setMessage(e.getMessage());
            resp.setResponse(e.getMessage());
        } catch (Exception e) {
            LOG.error("Exception while updating Store for " + storeDetailDTO, e);
            resp.setSuccessful(false);
            resp.setMessage("Failure :: Please check your configuration. Message:" + e.getMessage());
            resp.setResponse("Failure :: Please check your configuration. Message:" + e.getMessage());
        }
        return GSON_OBJECT.toJson(resp);
    }

    private void updateStore(StoreDetailDTO storeDetailDTO) throws Exception {
        String storeCode = storeDetailDTO.getCode();
        Store store = packmanExclusiveService.getStore(storeCode);
        if (store != null) {
            if (!store.getStoreFrontId().equals(storeDetailDTO.getStoreFrontId())) {
                Store s = packmanExclusiveService.getStoreByStoreFrontId(storeDetailDTO.getStoreFrontId());
                if (s != null)
                    throw new IllegalArgumentException("Unable to update store. another store exists with given store front id :  " + storeDetailDTO.getStoreFrontId());
            }
            store.setName(storeDetailDTO.getName());
            store.setDescription(storeDetailDTO.getDescription());
            store.setEnabled(storeDetailDTO.isEnabled());
            store.setStoreFrontId(storeDetailDTO.getStoreFrontId());
            String username = WebContextUtils.getCurrentUserEmail();
            store.setUpdatedBy(username);


            String labelsFromInput = storeDetailDTO.getLabels();

            StoreProperties property = packmanExclusiveService.fetchStoreProperty(storeCode, StoreAttribute.LABEL.getCode());


            String lablesCSVFromDB = null;
            if (property != null) {
                lablesCSVFromDB = property.getValue();
            }

            List<String> labelsListFromDB = new ArrayList<String>();
            if (lablesCSVFromDB != null) {
                labelsListFromDB = StringUtils.split(lablesCSVFromDB);
            }

            List<String> inputLabelsListFromInput = new ArrayList<String>();
            ;
            if (labelsListFromDB != null) {

                if (labelsFromInput != null) { 
                    inputLabelsListFromInput = StringUtils.split(labelsFromInput);
                }

                for (String labelsFromDb : labelsListFromDB) {
                    if (!CollectionUtils.isEmpty(inputLabelsListFromInput) && inputLabelsListFromInput.contains(labelsFromDb)) {
                        continue;
                    } else {
                        List<Rule> lRule = packmanRuleServiceImpl.getAllRulesByStore(storeDetailDTO.getCode());
                        for (Rule rule : lRule) {
                            if(rule.isEnabled()){

                                Set<Criteria> sCriteria = rule.getCriterion();

                                for (Criteria criteria : sCriteria) {
                                    if ((criteria.getName()).equals("LABEL")) {
                                        if ((criteria.getValue()).equals(labelsFromDb)) {
                                            throw new IllegalArgumentException(
                                                    "Rules for label [" + labelsFromDb + "] for store [" + storeCode + "] exists. please disable them before editing label");
                                        }
                                    }
                                }

                            
                            }
                        }
                    }
                }

            }

            if (StringUtils.isEmpty(labelsFromInput)) {
                if (property != null) {
                    packmanExclusiveService.deleteStoreProperty(property);
                    property = null;
                }
            } else {
                if (property == null) {
                    property = new StoreProperties();
                    property.setCreated(DateUtils.getCurrentTime());
                    property.setEnabled(true);
                    property.setStore(store);
                    property.setName(PackagingTypeRuleCriteria.LABEL.getName());
                }
                property.setLastUpdated(DateUtils.getCurrentTime());
                property.setUpdatedBy(WebContextUtils.getCurrentUserEmail());
                property.setValue(labelsFromInput);
            }
            packmanExclusiveService.updateStore(store, property);
        }
    }

    @Path("/{storeCode}/updatepackagingtypeitem")
    @Produces(MediaType.APPLICATION_JSON)
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public String savePackagingTypeItemDetail(@RequestParam(value = "packagingTypeDetailItemDTO") PackagingTypeItemDetailDTO packagingTypeItemDetailDTO) {
        PackManDTOResponse resp = new PackManDTOResponse();
        try {
            LOG.info("Request received to update Packaging Type Item info {}", packagingTypeItemDetailDTO);
            updatePackagingTypeItem(packagingTypeItemDetailDTO, packagingTypeItemDetailDTO.getPackagingType(), packagingTypeItemDetailDTO.getStoreCode());
            LOG.info("Success: Successfully Updated Packaging Type Item");
        } catch (Exception e) {
            LOG.error("Exception while updating Packaging Type Item for " + packagingTypeItemDetailDTO, e);
            resp.setSuccessful(false);
            resp.setMessage("Failure :: Please check your configuration. Message:" + e.getMessage());
            resp.setResponse("Failure :: Please check your configuration. Message:" + e.getMessage());
        }
        return GSON_OBJECT.toJson(resp);
    }

    private void updatePackagingTypeItem(PackagingTypeItemDetailDTO packagingTypeItemDetailDTO, String packagingTypeCode, String storeCode) {
        PackagingType packagingType = packmanExclusiveService.getPackagingType(packagingTypeCode, storeCode);
        if (packagingType != null) {
            String packagingTypeItemCode = packagingTypeItemDetailDTO.getCode();
            PackagingTypeItem packagingTypeItem = packmanExclusiveService.getPackagingTypeItem(packagingTypeItemCode, packagingType.getId());
            if (packagingTypeItem != null) {
                List<PackagingTypeItemProperties> property = null;
                if (packagingTypeItem.getPackagingType().getPackagingMode().equals(PackagingMode.NORMAL.mode())) {
                    property = packagingTypeItem.getProperties();
                    property = packmanConverterService.updatePackagingTypeItemProperties(property, packagingTypeItemDetailDTO);
                }
                packagingTypeItem.setDescription(packagingTypeItemDetailDTO.getDescription());
                String username = WebContextUtils.getCurrentUserEmail();
                packagingTypeItem.setUpdatedBy(username);
                if (property != null) {
                    for (PackagingTypeItemProperties p : property) {
                        p.setUpdatedBy(username);
                        p.setLastUpdated(DateUtils.getCurrentTime());
                    }
                }
                packmanExclusiveService.savePackagingTypeItem(packagingTypeItem, property);
            } else {
                LOG.error("Unable to find packaging type item with code : {} for packaging type {} and store code : {}", packagingTypeItemCode, packagingTypeCode, storeCode);
                throw new IllegalArgumentException(
                        "Unable to find packaging type item with code : " + packagingTypeItemCode + " for packaging type " + packagingTypeCode + " and store code : " + storeCode);
            }
        } else {
            LOG.error("No such packaging type exist {} for store {}", packagingTypeCode, storeCode);
            throw new IllegalArgumentException("Unable to find packaging type for code : " + packagingTypeCode + " and store : " + storeCode);
        }
    }

    @Path("/{storeCode}/deletepackagingtypeitem")
    @Produces(MediaType.APPLICATION_JSON)
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public String deletePackagingTypeItem(@RequestParam(value = "packagingTypeItemDetailDTO") PackagingTypeItemDetailDTO packagingTypeItemDetailDTO) {
        PackManDTOResponse resp = new PackManDTOResponse();
        String packagingTypeItemCode = packagingTypeItemDetailDTO.getCode();
        try {
            String user = WebContextUtils.getCurrentUserEmail();
            LOG.info("Request received to delete Packaging Type Item info {} by user {}", packagingTypeItemCode, user);
            String packagingTypeCode = packagingTypeItemDetailDTO.getPackagingType();
            String storeCode = packagingTypeItemDetailDTO.getStoreCode();
            PackagingType packagingType = packmanExclusiveService.getPackagingType(packagingTypeCode, storeCode);
            if (packagingType != null) {
                packmanExclusiveService.deletePackagingTypeItem(packagingTypeItemCode, packagingType.getId());
            } else {
                LOG.error("Unable to find packaging type {} for store {}", packagingTypeCode, storeCode);
                throw new IllegalArgumentException("Unable to find packaging type " + packagingTypeCode + " and store " + storeCode);
            }
            LOG.info("Success: Successfully deleted Packaging Type Item");
        } catch (Exception e) {
            LOG.error("Exception while deleting Packaging Type Item for " + packagingTypeItemCode, e);
            resp.setSuccessful(false);
            resp.setMessage("Failure :: Unable to Delete.");
            resp.setResponse("Failure :: Unable to Delete");
        }

        return GSON_OBJECT.toJson(resp);
    }

}
