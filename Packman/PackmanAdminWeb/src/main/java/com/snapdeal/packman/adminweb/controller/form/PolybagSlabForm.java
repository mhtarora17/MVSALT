/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.packman.adminweb.controller.form;

import javax.validation.constraints.NotNull;

/**
 * @author nitish
 */
public class PolybagSlabForm {

    private String slabCode;

    @NotNull
    private String slabSize;

    public String getSlabCode() {
        return slabCode;
    }

    public void setSlabCode(String slabCode) {
        this.slabCode = slabCode;
    }

    public String getSlabSize() {
        return slabSize;
    }

    public void setSlabSize(String slabSize) {
        this.slabSize = slabSize;
    }

    @Override
    public String toString() {
        return "PolybagSlabForm [slabCode=" + slabCode + ", slabSize=" + slabSize + "]";
    }

}
