package com.snapdeal.packman.client.impl;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.snapdeal.base.exception.SnapdealWSException;
import com.snapdeal.base.model.common.ServiceRequest;
import com.snapdeal.base.transport.service.ITransportService;
import com.snapdeal.base.transport.service.ITransportService.Protocol;
import com.snapdeal.packman.api.getAllSurfacePackagingTypes.v0.GetAllSurfacePackagingTypesRequest;
import com.snapdeal.packman.api.getAllSurfacePackagingTypes.v0.GetAllSurfacePackagingTypesResponse;
import com.snapdeal.packman.api.getPackagingRecommendation.v0.GetPackagingRecommendationRequest;
import com.snapdeal.packman.api.getPackagingRecommendation.v0.GetPackagingRecommendationResponse;
import com.snapdeal.packman.client.IPackmanClientService;
import com.snapdeal.packman.request.GetPackagingRecommendationsRequest;
import com.snapdeal.packman.response.GetPackagingRecommendationsResponse;


/**
 * @author nikhil
 */
@Service("packmanClientService")
public class PackmanClientServiceImpl implements IPackmanClientService {
    private static final Gson   DEBUG_JSON = new Gson();

    private static final Logger LOG        = LoggerFactory.getLogger(PackmanClientServiceImpl.class);
    @Autowired
    private ITransportService   transportService;

    private String              webServiceUrl;

    @PostConstruct
    public void init() {
        transportService.registerService("/service/packman/", "packmanserver.");
    }

    <T extends ServiceRequest> String getPackmanWebServiceURL(T request) {

        if (null == request.getRequestProtocol()) {
            request.setRequestProtocol(Protocol.PROTOCOL_PROTOSTUFF);
        }
        if (null == request.getResponseProtocol()) {
            request.setResponseProtocol(Protocol.PROTOCOL_PROTOSTUFF);
        }
        if (LOG.isDebugEnabled()) {
            LOG.debug("Packman Service Call {} ", DEBUG_JSON.toJson(request));
        }

        return webServiceUrl;
    }

    @Override
    public void setPackmanClientServiceURL(String url) {
        webServiceUrl = url + "/service/packman";
    }

    @Override
    public GetPackagingRecommendationResponse getPackagingRecommendation(GetPackagingRecommendationRequest request) throws SnapdealWSException {
        String url = getPackmanWebServiceURL(request) + "/getPackagingRecommendation";
        GetPackagingRecommendationResponse response = (GetPackagingRecommendationResponse) transportService.executeRequest(url, request, null,
                GetPackagingRecommendationResponse.class);
        return response;
    }

    public GetAllSurfacePackagingTypesResponse getAllSurfacePackagingTypes(GetAllSurfacePackagingTypesRequest request) throws SnapdealWSException {
        String url = getPackmanWebServiceURL(request) + "/getAllSurfacePackagingTypes";
        GetAllSurfacePackagingTypesResponse response = (GetAllSurfacePackagingTypesResponse) transportService.executeRequest(url, request, null,
                GetAllSurfacePackagingTypesResponse.class);
        return response;
    }

    @Override
    public GetPackagingRecommendationsResponse getPackagingRecommendations(GetPackagingRecommendationsRequest request)throws SnapdealWSException {
	 String url = getPackmanWebServiceURL(request) + "/getPackagingRecommendations";
	 GetPackagingRecommendationsResponse response = (GetPackagingRecommendationsResponse) transportService.executeRequest(url, request, null, GetPackagingRecommendationsResponse.class);
	        return response;
    }

}
