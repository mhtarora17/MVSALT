package com.snapdeal.packman.client;

import com.snapdeal.base.exception.SnapdealWSException;
import com.snapdeal.packman.api.getAllSurfacePackagingTypes.v0.GetAllSurfacePackagingTypesRequest;
import com.snapdeal.packman.api.getAllSurfacePackagingTypes.v0.GetAllSurfacePackagingTypesResponse;
import com.snapdeal.packman.api.getPackagingRecommendation.v0.GetPackagingRecommendationRequest;
import com.snapdeal.packman.api.getPackagingRecommendation.v0.GetPackagingRecommendationResponse;
import com.snapdeal.packman.request.GetPackagingRecommendationsRequest;
import com.snapdeal.packman.response.GetPackagingRecommendationsResponse;

public interface IPackmanClientService {

    /**
     * @param url
     */
    void setPackmanClientServiceURL(String url);

    /**
     * API for Packaging Recommendation for an supc,seller,courier. This returns and recommendations packaging out of
     * three categories: Carton,PolyBag,Surface.
     * 
     * @param request
     * @return
     */
    public GetPackagingRecommendationResponse getPackagingRecommendation(GetPackagingRecommendationRequest request) throws SnapdealWSException;

    /**
     * Return all Possible SurfacePackagingTypes
     * 
     * @param request
     * @return
     */
    public GetAllSurfacePackagingTypesResponse getAllSurfacePackagingTypes(GetAllSurfacePackagingTypesRequest request) throws SnapdealWSException;
    
    
    /**  This is a batch api where we can get packaging recommendation for a list of supcs
     * @param request
     * @return
     * @throws SnapdealWSException
     */
    public GetPackagingRecommendationsResponse getPackagingRecommendations(GetPackagingRecommendationsRequest request)throws SnapdealWSException;

}
