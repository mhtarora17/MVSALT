package com.snapdeal.cocofs.dataaccess.fc;

import java.util.List;

import com.snapdeal.cocofs.core.aerospike.vo.FCDetailVO;
import com.snapdeal.score.data.aerospike.AerospikeSet;


/**
 * Interface to read fulfilment data from Aerospike
 * 
 * @author gaurav
 *
 */

public interface IFCAerospikeDataReadService {
	
	public AerospikeSet getFCMapping(String key, Class<? extends AerospikeSet> klass);

    FCDetailVO getFCDetailVO(String fcCode);

    List<? extends AerospikeSet>  getAllRecordsForSet(Class<? extends AerospikeSet> klass);

}
