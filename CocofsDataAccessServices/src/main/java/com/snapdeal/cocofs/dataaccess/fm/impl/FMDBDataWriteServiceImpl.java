/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 30-Sep-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.dataaccess.fm.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.snapdeal.cocofs.dataaccess.fm.IFMDBDataWriteService;
import com.snapdeal.cocofs.db.dao.IFulfilmentModelDao;
import com.snapdeal.cocofs.entity.SellerFMMapping;
import com.snapdeal.cocofs.entity.SellerSubcatFMMapping;
import com.snapdeal.cocofs.entity.SellerSupcFMMapping;

@Service("FMDBDataWriteServiceImpl")
@Transactional
public class FMDBDataWriteServiceImpl implements IFMDBDataWriteService {

    @Autowired
    IFulfilmentModelDao fulfillmentModelDao;

    @Override
    public void updateSellerFulfilmentModelMapping(SellerFMMapping mapping) {
        fulfillmentModelDao.updateSellerFMMapping(mapping);
    }

    @Override
    public void updateSellerSubcatFulfilmentModelMapping(SellerSubcatFMMapping mapping) {
        fulfillmentModelDao.updateSellerSubcatFMMapping(mapping);
    }

    @Override
    public void updateSellerSupcFulfilmentModelMapping(SellerSupcFMMapping mapping) {
        fulfillmentModelDao.updateSellerSupcFMMapping(mapping);
    }

}
