/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.dataaccess.taxrate.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.cocofs.dataaccess.taxrate.ITaxRateDBDataReadService;
import com.snapdeal.cocofs.db.dao.ITaxRateDao;
import com.snapdeal.cocofs.entity.SellerCategoryTaxRate;
import com.snapdeal.cocofs.entity.SellerSupcTaxRate;
import com.snapdeal.cocofs.entity.StateCategoryPriceTaxRate;

/**
 *  
 *  @version     1.0, 18-Aug-2014
 *  @author ankur
 */

@Service("TaxRateDBDataReadService")
@Transactional(readOnly = true)
public class TaxRateDBDataReadService implements ITaxRateDBDataReadService{

    @Autowired
    ITaxRateDao taxRateDao;
    
    @Override
    public List<StateCategoryPriceTaxRate> getStateCategoryPriceTaxRate(String state, String categoryUrl) {
       if(StringUtils.isNotEmpty(state) && StringUtils.isNotEmpty(categoryUrl)){
           return taxRateDao.getStateCategoryPriceTaxRate(state, categoryUrl);
       }
       return new ArrayList<StateCategoryPriceTaxRate>();
    }
    
    @Override
    public List<StateCategoryPriceTaxRate> getAllEnabledStateCategoryPriceTaxRates() {
           return taxRateDao.getAllEnabledStateCategoryPriceTaxRates();       
    }
    
    @Override
    public SellerCategoryTaxRate getSellerCategoryTaxRate(String sellerCode, String categoryUrl) {
        if(StringUtils.isNotEmpty(sellerCode) && StringUtils.isNotEmpty(categoryUrl)){
            return taxRateDao.getSellerCategoryTaxRate(sellerCode, categoryUrl);
        }
        return null;
    }
    
    @Override
    public List<SellerCategoryTaxRate> getAllEnabledSellerCategoryTaxRates() {
           return taxRateDao.getAllEnabledSellerCategoryTaxRates();       
    }
    
    @Override
    public SellerSupcTaxRate getSellerSupcTaxRate(String sellerCode, String supc) {
        if(StringUtils.isNotEmpty(sellerCode) && StringUtils.isNotEmpty(supc)){
            return taxRateDao.getSellerSupcTaxRate(sellerCode, supc);
        }
        return null;
    }
    
    @Override
    public List<SellerSupcTaxRate> getAllEnabledSellerSupcTaxRates() {
           return taxRateDao.getAllEnabledSellerSupcTaxRates();       
    }
    
    @Override
    public List<SellerSupcTaxRate> getTaxRateForSellerCode(String sellerCode){
    	return taxRateDao.getTaxRateForSellerCode(sellerCode);
    }

    @Override
    public List<String> getSellersCodeswithTaxRate() {
        return taxRateDao.getSellersCodeswithTaxRate();
    }
    
}
