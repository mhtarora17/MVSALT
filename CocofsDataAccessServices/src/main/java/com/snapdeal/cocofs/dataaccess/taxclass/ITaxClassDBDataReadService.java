/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.dataaccess.taxclass;

import java.util.List;

import com.snapdeal.cocofs.entity.SubcatTaxClassMapping;
import com.snapdeal.cocofs.entity.SupcTaxClassMapping;

/**
 *  
 *  @version     1.0, 23-Dec-2015
 *  @author nitish
 */
public interface ITaxClassDBDataReadService {

    SupcTaxClassMapping getSupcTaxClassMapping(String supc);

    SubcatTaxClassMapping getSubcatTaxClassMapping(String subcat);

    List<SupcTaxClassMapping> getAllEnabledSupcTaxClassMapping();

    List<SubcatTaxClassMapping> getAllEnabledSubcatTaxClassMapping();

   

}
