/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 12-Sept-2014
 *  @author gauravg
 */
package com.snapdeal.cocofs.dataaccess.fc.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import com.aerospike.client.AerospikeException;
import com.snapdeal.cocofs.core.aerospike.vo.FCDetailVO;
import com.snapdeal.cocofs.dataaccess.fc.IFCAerospikeDataReadService;
import com.snapdeal.score.data.aerospike.AerospikeSet;
import com.snapdeal.score.data.aerospike.service.impl.ScoreAerospikeService;

/**
 * Service implementation to read fulfilment model data from aerospike
 * @author gaurav
 *
 */

@Service("FCAerospikeDataReadServiceImpl")
public class FCAerospikeDataReadServiceImpl implements IFCAerospikeDataReadService{

    private static final Logger LOG = LoggerFactory.getLogger(FCAerospikeDataReadServiceImpl.class);

	@Autowired
	@Qualifier("aerospikeService")
	private ScoreAerospikeService									aerospikeService;
	   
	@Override
	public AerospikeSet getFCMapping(String key, Class<? extends AerospikeSet> klass) {
        
        
		LOG.debug("Fetching record for key="+key+" from Aerospike");
		AerospikeSet value = null;
		try {
			value = aerospikeService.get(key, klass);
			LOG.debug("Fetched record for key="+key+" as ["+value+"].");
		} catch (com.snapdeal.score.data.aerospike.exception.GetFailedException e) {
			LOG.error("Not able to fetch data from aerospike for key:{}. Exception Message:{}", key, e.getMessage());
			e.printStackTrace();
		}
		return value;
	}
	
	@Override
    public List<? extends AerospikeSet>  getAllRecordsForSet(Class<? extends AerospikeSet> klass) {
        
        
        LOG.debug("Fetching record for set "+klass.getSimpleName());
        List<? extends AerospikeSet> value = null;
        try {
            value =  aerospikeService.getAll(klass);
            LOG.debug("Fetched record for set "+klass.getSimpleName());
        } catch (com.snapdeal.score.data.aerospike.exception.GetFailedException e) {
            LOG.error("Not able to fetch data from aerospike for set "+klass.getSimpleName(), e);
        } catch (AerospikeException e) {
            LOG.error("Not able to fetch data from aerospike for set "+klass.getSimpleName(), e);
        }
        return value;
    }
	
	@Override
	public FCDetailVO getFCDetailVO(String fcCode){
	    AerospikeSet record = getFCMapping(fcCode, FCDetailVO.class);
	    if(record != null){
	        return (FCDetailVO)record;
	    }
	    LOG.warn("could not find FCdetailVO record in aerospike for fcCode {}", fcCode);
	    return null;
	}

}
