/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 30-Sep-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.dataaccess.fm;

import java.util.Date;
import java.util.List;

import com.snapdeal.cocofs.entity.SellerFMMapping;
import com.snapdeal.cocofs.entity.SellerSubcatFMMapping;
import com.snapdeal.cocofs.entity.SellerSupcFMMapping;
import com.snapdeal.cocofs.entity.SellerSupcMapping;

public interface IFMDBDataReadService {

    SellerSupcFMMapping getEnabledSellerSupcFMMapping(String vendorCode, String supc);

    SellerSubcatFMMapping getEnabledSellerSubcatFMMapping(String vendorCode, String subcat);

    SellerFMMapping getEnabledSellerFMMapping(String vendorCode);
    
    /**
     * Returns FMMappings ir-respective of whether it is enabled or disabled.
     * @param vendorCode
     * @param supc
     * @return
     */
    SellerSupcFMMapping getSellerSupcFMMapping(String vendorCode, String supc);

    /**
     *  Returns FMMappings ir-respective of whether it is enabled or disabled.
     * @param vendorCode
     * @param subcat
     * @return
     */
    SellerSubcatFMMapping getSellerSubcatFMMapping(String vendorCode, String subcat);

    /**
     * Returns FMMappings ir-respective of whether it is enabled or disabled.
     * @param vendorCode
     * @return
     */
    SellerFMMapping getSellerFMMapping(String vendorCode);

    /**
     * Returns List of FMMappings for imput sellerCode for all categories 
     * @param sellerCode
     * @return
     */
    List<SellerSubcatFMMapping> getSellerSubcatFMMapping(String sellerCode);
    
    /**
     * /**
     * Returns List of FMMappings for imput sellerCode for all supcs 
     * @param sellerCode
     * @return
     */
    List<SellerSupcFMMapping> getAllEnabledSellerSupcFMMappingBySeller(String sellerCode);
    
    /**
     * Return the list of SUPCs having exceptions for specified seller
     * @param sellerCode
     * @return
     */
    List<String> getAllEnabledSupcExceptionListForSeller(String sellerCode);

    /**
     * Return the list of SUPCs served by specified seller
     * @param sellerCode
     * @return
     */
    List<String> getSupcListForSeller(String sellerCode);
    
    List<String> getSellersWithExceptionAtSupcLevel();

    List<SellerFMMapping> getAllSellerFMAndFCMapping();

    List<SellerFMMapping> getAllSellerFMAndFCMappingByLastUpdated(Date lastUpdated);
    
    /**
     * Return count(sellerCode, supc, enabled = 1) from seller_supc_fm_mapping table
     * @param sellerCode
     * @return
     */
    long getEnabledSellerSupcFMMappingCount(String sellerCode);

    long getSellerSupcFMMappingCountByLastUpdated(Date lastUpdated);

    long getSellerFMMappingCountByLastUpdated(Date lastUpdated);

    List<SellerFMMapping> getAllSellerFMMappingByLastUpdated(Date lastUpdated);

    List<SellerSupcFMMapping> getAllSellerSupcFMMappingByLastUpdated(Date lastUpdated);
    
    /**
     * Retrieve all SUPCs for a given seller from the database.
     * @param sellerCode
     * @return
     */
    List<SellerSupcMapping> getSellerSupcMapping(String sellerCode);
    
    /**
     * Check if a particular supc belongs to the given seller.
     * @param sellerCode
     * @param supc
     * @return
     */
    boolean isSellerSupcMappingExists(String sellerCode, String supc);

    /**
     * Return the list of all enabled SellerCode for a specified Fulfillment At Seller Level.
     * 
     * @param fulfillmentModel
     * @return
     */
	List<String> getAllEnabledSellerAtSellerLevelForFulfillmentModel(String fulfillmentModel);

	/**
     * Return the list of all enabled SellerCode for a specified Fulfillment At Supc Level.
     * 
     * @param fulfillmentModel
     * @return
     */
	List<String> getAllEnabledSellerAtSupcLevelForFulfillmentModel(String fulfillmentModel);

	/**
     * Return the list of all enabled SellerCode for a specified Fulfillment At Subcat Level.
     * 
     * @param fulfillmentModel
     * @return
     */
	List<String> getAllEnabledSellerAtSubcatLevelForFulfillmentModel(String fulfillmentModel);

    List<String> getEnabledSellerListAtSellerLevelForFCcode(String fcCode);

    List<SellerSupcFMMapping> getEnabledSellerSupcMappingListForFcCode(String fcCode);

}
