/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 30-Sep-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.dataaccess.fm.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.cocofs.dataaccess.fm.IFMDBDataReadService;
import com.snapdeal.cocofs.db.dao.IFulfilmentModelDao;
import com.snapdeal.cocofs.entity.SellerFMMapping;
import com.snapdeal.cocofs.entity.SellerSubcatFMMapping;
import com.snapdeal.cocofs.entity.SellerSupcFMMapping;
import com.snapdeal.cocofs.entity.SellerSupcMapping;

@Service("FMDBDataReadServiceImpl")
@Transactional(readOnly = true)
public class FMDBDataReadServiceImpl implements IFMDBDataReadService {

    @Autowired
    IFulfilmentModelDao         fulfillmentModelDao;

    private static final Logger LOG = LoggerFactory.getLogger(FMDBDataReadServiceImpl.class);

    @Override
    public SellerFMMapping getEnabledSellerFMMapping(String sellerCode) {
        if (StringUtils.isNotEmpty(sellerCode)) {
            SellerFMMapping mapping = fulfillmentModelDao.getEnabledSellerFMMapping(sellerCode);
            if (mapping != null) {
                Hibernate.initialize(mapping.getFcCenters());
            }
            return mapping;
        }
        return null;
    }

    @Override
    public SellerSubcatFMMapping getEnabledSellerSubcatFMMapping(String sellerCode, String subcat) {
        if (StringUtils.isNotEmpty(sellerCode) && StringUtils.isNotEmpty(subcat)) {
            return fulfillmentModelDao.getEnabledSellerSubcatFMMapping(sellerCode, subcat);
        }
        return null;
    }

    @Override
    public SellerSupcFMMapping getEnabledSellerSupcFMMapping(String sellerCode, String supc) {
        if (StringUtils.isNotEmpty(sellerCode) && StringUtils.isNotEmpty(supc)) {
            return fulfillmentModelDao.getEnabledSellerSupcFMMapping(sellerCode, supc);
        }
        return null;
    }

    @Override
    public SellerFMMapping getSellerFMMapping(String sellerCode) {
        if (StringUtils.isNotEmpty(sellerCode)) {
            SellerFMMapping mapping = fulfillmentModelDao.getSellerFMMapping(sellerCode);
            if (mapping != null) {
                Hibernate.initialize(mapping.getFcCenters());
            }
            return mapping;
        }
        return null;
    }

    @Override
    public SellerSubcatFMMapping getSellerSubcatFMMapping(String sellerCode, String subcat) {
        if (StringUtils.isNotEmpty(sellerCode) && StringUtils.isNotEmpty(subcat)) {
            return fulfillmentModelDao.getSellerSubcatFMMapping(sellerCode, subcat);
        }
        return null;
    }

    @Override
    public SellerSupcFMMapping getSellerSupcFMMapping(String sellerCode, String supc) {
        if (StringUtils.isNotEmpty(sellerCode) && StringUtils.isNotEmpty(supc)) {
            SellerSupcFMMapping mapping = fulfillmentModelDao.getSellerSupcFMMapping(sellerCode, supc);
            if (mapping != null) {
                Hibernate.initialize(mapping.getFcCenters());
            }
            return mapping;
        }
        return null;
    }

    @Override
    public List<SellerSubcatFMMapping> getSellerSubcatFMMapping(String sellerCode) {
        if (StringUtils.isNotEmpty(sellerCode)) {
            return fulfillmentModelDao.getSellerSubcatFMMapping(sellerCode);
        }
        return new ArrayList<SellerSubcatFMMapping>();
    }

    @Override
    public List<String> getSellersWithExceptionAtSupcLevel() {
        return fulfillmentModelDao.getSellersWithExceptionAtSupcLevel();
    }

    @Override
    public List<SellerFMMapping> getAllSellerFMAndFCMapping() {
        return fulfillmentModelDao.getAllSellerFMAndFCMapping();
    }

    @Override
    public List<SellerFMMapping> getAllSellerFMAndFCMappingByLastUpdated(Date lastUpdated) {
        return fulfillmentModelDao.getAllSellerFMAndFCMappingByLastUpdated(lastUpdated);

    }

    @Override
    public List<SellerSupcFMMapping> getAllEnabledSellerSupcFMMappingBySeller(String sellerCode) {
        if (StringUtils.isNotEmpty(sellerCode)) {
            List<SellerSupcFMMapping> mappings = fulfillmentModelDao.getEnabledSellerSupcFMMapping(sellerCode);
            for(SellerSupcFMMapping mapping : mappings){
                Hibernate.initialize(mapping.getFcCenters());
            }
            return mappings;
        }
        return new ArrayList<SellerSupcFMMapping>();
    }

    @Override
    public long getSellerFMMappingCountByLastUpdated(Date lastUpdated) {
        long sellerCount = 0;
        try {
            Long sellerCountObj = fulfillmentModelDao.getSellerFMMappingCountByLastUpdated(lastUpdated);
            sellerCount = sellerCountObj != null ? (Long) sellerCountObj : 0;
        } catch (Exception e) {
            LOG.error("error while fetching seller updated count ", e);
        }

        return sellerCount;

    }

    @Override
    public long getSellerSupcFMMappingCountByLastUpdated(Date lastUpdated) {
        long sellerSupcCount = 0;
        try {
            Long sellerSupcCountObj = fulfillmentModelDao.getSellerSupcFMMappingCountByLastUpdated(lastUpdated);
            sellerSupcCount = sellerSupcCountObj != null ? sellerSupcCountObj : 0;
        } catch (Exception e) {
            LOG.error("error while fetching seller updated count ", e);
        }

        return sellerSupcCount;

    }

    @Override
    public List<SellerFMMapping> getAllSellerFMMappingByLastUpdated(Date lastUpdated) {
        return fulfillmentModelDao.getAllSellerFMMappingByLastUpdated(lastUpdated);

    }

    @Override
    public List<SellerSupcFMMapping> getAllSellerSupcFMMappingByLastUpdated(Date lastUpdated) {
        return fulfillmentModelDao.getAllSellerSupcFMMappingByLastUpdated(lastUpdated);
    }

    @Override
    public long getEnabledSellerSupcFMMappingCount(String sellerCode) {
        if (StringUtils.isNotEmpty(sellerCode)) {
            return fulfillmentModelDao.getEnabledSellerSupcFMMappingCount(sellerCode);
        }
        return -1;
    }

    @Override
    public List<String> getAllEnabledSupcExceptionListForSeller(String sellerCode) {
        return fulfillmentModelDao.getEnabledSupcExceptionListForSeller(sellerCode);
    }

    @Override
    public List<String> getSupcListForSeller(String sellerCode) {
        return fulfillmentModelDao.getEnabledSupcExceptionListForSeller(sellerCode);
    }

    @Override
    public List<SellerSupcMapping> getSellerSupcMapping(String sellerCode) {
        if (StringUtils.isNotEmpty(sellerCode)) {
            return fulfillmentModelDao.getSellerSupcMappingList(sellerCode);
        }
        return null;
    }

    @Override
    public boolean isSellerSupcMappingExists(String sellerCode, String supc) {
        if (StringUtils.isNotEmpty(sellerCode) && StringUtils.isNotEmpty(supc)) {
            SellerSupcMapping sellerSupcMapping = fulfillmentModelDao.getSellerSupcMapping(sellerCode, supc);
            if (sellerSupcMapping != null) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

	@Override
	public List<String> getAllEnabledSellerAtSellerLevelForFulfillmentModel(String fulfillmentModel) {
        return fulfillmentModelDao.getEnabledSellerListAtSellerLevelForFulfillmentModel(fulfillmentModel);
	}

	@Override
	public List<String> getAllEnabledSellerAtSupcLevelForFulfillmentModel(String fulfillmentModel) {
        return fulfillmentModelDao.getEnabledSellerListAtSupcLevelForFulfillmentModel(fulfillmentModel);
	}

	@Override
	public List<String> getAllEnabledSellerAtSubcatLevelForFulfillmentModel(String fulfillmentModel) {
        return fulfillmentModelDao.getEnabledSellerListAtSubcatLevelForFulfillmentModel(fulfillmentModel);
	}
	
	@Override
    public List<String> getEnabledSellerListAtSellerLevelForFCcode(String fcCode) {
       return fulfillmentModelDao.getEnabledSellerListAtSellerLevelForFCcode(fcCode);
    }
    
    
    
    @Override
    public List<SellerSupcFMMapping> getEnabledSellerSupcMappingListForFcCode(String fcCode) {
        return fulfillmentModelDao.getEnabledSellerSupcMappingListForFcCode(fcCode);

    }

}
