/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.dataaccess.taxrate;

import java.util.List;

import com.snapdeal.cocofs.mongo.model.SellerCategoryTaxRateUnit;
import com.snapdeal.cocofs.mongo.model.SellerSupcTaxRateUnit;
import com.snapdeal.cocofs.mongo.model.StateCategoryPriceTaxRateUnit;

/**
 *  
 *  @version     1.0, 18-Aug-2014
 *  @author ankur
 */
public interface ITaxRateMongoService {

    List<StateCategoryPriceTaxRateUnit> getEnabledStateCategoryPriceTaxRateUnit(String state, String categoryUrl);

    SellerCategoryTaxRateUnit getSellerCategoryTaxRateUnit(String sellerCode, String categoryUrl);

    SellerSupcTaxRateUnit getSellerSupcTaxRateUnit(String sellerCode, String supc);

    List<StateCategoryPriceTaxRateUnit> getAllEnabledStateCategoryPriceTaxRateUnits();

    List<StateCategoryPriceTaxRateUnit> getEnabledStateCategoryTaxTypeUnitsByTaxType(String state, String categoryUrl, String taxType);

    List<StateCategoryPriceTaxRateUnit> getStateCategoryPriceTaxRateUnit(String state, String categoryUrl);


}
