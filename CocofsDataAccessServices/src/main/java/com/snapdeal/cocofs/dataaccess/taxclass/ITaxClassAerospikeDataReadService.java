package com.snapdeal.cocofs.dataaccess.taxclass;

import com.snapdeal.score.data.aerospike.AerospikeSet;

/**
 * Interface to read tax class from Aerospike
 * 
 * @author nitish
 */

public interface ITaxClassAerospikeDataReadService {

    AerospikeSet getSupcTaxClassMapping(String key, Class<? extends AerospikeSet> klass);

    AerospikeSet getSubcatTaxClassMapping(String key, Class<? extends AerospikeSet> klass);

}
