/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.dataaccess.taxrate.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.dataaccess.taxrate.ITaxRateMongoService;
import com.snapdeal.cocofs.mongo.CocofsMongoQuery;
import com.snapdeal.cocofs.mongo.Criteria;
import com.snapdeal.cocofs.mongo.mao.IGenericMao;
import com.snapdeal.cocofs.mongo.model.SellerCategoryTaxRateUnit;
import com.snapdeal.cocofs.mongo.model.SellerCategoryTaxRateUnit.SCTRFieldNames;
import com.snapdeal.cocofs.mongo.model.SellerSupcTaxRateUnit;
import com.snapdeal.cocofs.mongo.model.SellerSupcTaxRateUnit.SSTRFieldNames;
import com.snapdeal.cocofs.mongo.model.StateCategoryPriceTaxRateUnit;
import com.snapdeal.cocofs.mongo.model.StateCategoryPriceTaxRateUnit.SCPTRFieldNames;

/**
 *  
 *  @version     1.0, 18-Aug-2014
 *  @author ankur
 */

@Service("taxRateMongoServiceImpl")
public class TaxRateMongoServiceImpl implements ITaxRateMongoService{
   
    @Autowired
    private IGenericMao genericMao;
    
    @Autowired
    @Qualifier("mongoTemplateCocofs")
    private MongoOperations mongoOperations;

   @Override
    public List<StateCategoryPriceTaxRateUnit> getEnabledStateCategoryPriceTaxRateUnit(String state, String categoryUrl) {
        List<Criteria> queryCriterias = new ArrayList<Criteria>();
        queryCriterias.add(new Criteria(SCPTRFieldNames.STATE.getCode(), state, CocofsMongoQuery.Operator.EQUAL));
        queryCriterias.add(new Criteria(SCPTRFieldNames.CATEGORY_URL.getCode(), categoryUrl, CocofsMongoQuery.Operator.EQUAL));
        queryCriterias.add(new Criteria(SCPTRFieldNames.ENABLED.getCode(), Boolean.TRUE, CocofsMongoQuery.Operator.EQUAL));
        return genericMao.getDocumentList(new CocofsMongoQuery(queryCriterias), StateCategoryPriceTaxRateUnit.class);
    }
   
   @Override
   public List<StateCategoryPriceTaxRateUnit> getStateCategoryPriceTaxRateUnit(String state, String categoryUrl) {
       List<Criteria> queryCriterias = new ArrayList<Criteria>();
       queryCriterias.add(new Criteria(SCPTRFieldNames.STATE.getCode(), state, CocofsMongoQuery.Operator.EQUAL));
       queryCriterias.add(new Criteria(SCPTRFieldNames.CATEGORY_URL.getCode(), categoryUrl, CocofsMongoQuery.Operator.EQUAL));
       return genericMao.getDocumentList(new CocofsMongoQuery(queryCriterias), StateCategoryPriceTaxRateUnit.class);
   }
   
   @Override
   public List<StateCategoryPriceTaxRateUnit> getEnabledStateCategoryTaxTypeUnitsByTaxType(String state, String categoryUrl,String taxType) {
       List<Criteria> queryCriterias = new ArrayList<Criteria>();
       queryCriterias.add(new Criteria(SCPTRFieldNames.STATE.getCode(), state, CocofsMongoQuery.Operator.EQUAL));
       queryCriterias.add(new Criteria(SCPTRFieldNames.CATEGORY_URL.getCode(), categoryUrl, CocofsMongoQuery.Operator.EQUAL));
       queryCriterias.add(new Criteria(SCPTRFieldNames.TAX_TYPE.getCode(), taxType, CocofsMongoQuery.Operator.EQUAL));
       return genericMao.getDocumentList(new CocofsMongoQuery(queryCriterias), StateCategoryPriceTaxRateUnit.class);
       
   }
   
   @Override
   public List<StateCategoryPriceTaxRateUnit> getAllEnabledStateCategoryPriceTaxRateUnits() {
       List<Criteria> queryCriterias = new ArrayList<Criteria>();
       queryCriterias.add(new Criteria(SCPTRFieldNames.ENABLED.getCode(), Boolean.TRUE, CocofsMongoQuery.Operator.EQUAL));
       return genericMao.getDocumentList(new CocofsMongoQuery(queryCriterias), StateCategoryPriceTaxRateUnit.class);
   }
    
   @Override
    public SellerCategoryTaxRateUnit getSellerCategoryTaxRateUnit(String sellerCode, String categoryUrl) {
        List<Criteria> queryCriterias = new ArrayList<Criteria>();
        queryCriterias.add(new Criteria(SCTRFieldNames.SELLER_CODE.getCode(), sellerCode, CocofsMongoQuery.Operator.EQUAL));
        queryCriterias.add(new Criteria(SCTRFieldNames.CATEGORY_URL.getCode(), categoryUrl, CocofsMongoQuery.Operator.EQUAL));
        return genericMao.getDocument(new CocofsMongoQuery(queryCriterias), SellerCategoryTaxRateUnit.class);
    }
    
   @Override
    public SellerSupcTaxRateUnit getSellerSupcTaxRateUnit(String sellerCode, String supc) {
        List<Criteria> queryCriterias = new ArrayList<Criteria>();
        queryCriterias.add(new Criteria(SSTRFieldNames.SELLER_CODE.getCode(), sellerCode, CocofsMongoQuery.Operator.EQUAL));
        queryCriterias.add(new Criteria(SSTRFieldNames.SUPC.getCode(), supc, CocofsMongoQuery.Operator.EQUAL));
        return genericMao.getDocument(new CocofsMongoQuery(queryCriterias), SellerSupcTaxRateUnit.class);
    }
   
   
}
