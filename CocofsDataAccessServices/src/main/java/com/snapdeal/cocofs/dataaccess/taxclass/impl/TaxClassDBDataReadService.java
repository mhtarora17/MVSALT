/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.dataaccess.taxclass.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.cocofs.dataaccess.taxclass.ITaxClassDBDataReadService;
import com.snapdeal.cocofs.db.dao.ITaxClassDao;
import com.snapdeal.cocofs.entity.SubcatTaxClassMapping;
import com.snapdeal.cocofs.entity.SupcTaxClassMapping;

/**
 * @version 1.0, 18-Aug-2014
 * @author ankur
 */

@Service("TaxClassDBDataReadService")
@Transactional(readOnly = true)
public class TaxClassDBDataReadService implements ITaxClassDBDataReadService {

    @Autowired
    ITaxClassDao                taxClassDao;

    private static final Logger LOG = LoggerFactory.getLogger(TaxClassDBDataReadService.class);

    @Override
    public SupcTaxClassMapping getSupcTaxClassMapping(String supc) {
        if (StringUtils.isNotEmpty(supc)) {
            SupcTaxClassMapping mapping = taxClassDao.getSupcTaxClassMapping(supc);
            return mapping;
        }
        return null;
    }

    @Override
    public SubcatTaxClassMapping getSubcatTaxClassMapping(String subcat) {
        if (StringUtils.isNotEmpty(subcat)) {
            SubcatTaxClassMapping mapping = taxClassDao.getSubcatTaxClassMapping(subcat);
            return mapping;
        }
        return null;
    }

    @Override
    public List<SupcTaxClassMapping> getAllEnabledSupcTaxClassMapping() {
        return taxClassDao.getAllEnabledSupcTaxClassMapping();
    }

    @Override
    public List<SubcatTaxClassMapping> getAllEnabledSubcatTaxClassMapping() {
        return taxClassDao.getAllEnabledSubcatTaxClassMapping();

    }

}
