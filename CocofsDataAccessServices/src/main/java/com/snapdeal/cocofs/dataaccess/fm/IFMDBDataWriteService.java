/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 30-Sep-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.dataaccess.fm;

import com.snapdeal.cocofs.entity.SellerFMMapping;
import com.snapdeal.cocofs.entity.SellerSubcatFMMapping;
import com.snapdeal.cocofs.entity.SellerSupcFMMapping;

public interface IFMDBDataWriteService {

    void updateSellerSubcatFulfilmentModelMapping(SellerSubcatFMMapping mapping);

    void updateSellerFulfilmentModelMapping(SellerFMMapping mapping);

    void updateSellerSupcFulfilmentModelMapping(SellerSupcFMMapping mapping);

}
