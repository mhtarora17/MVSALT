/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 06-Oct-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.dataaccess.fm.impl;

import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.dataaccess.fm.IFMMongoService;

@Service("fulfilmentModelMongoServiceImpl")
public class FMMongoServiceImpl implements IFMMongoService {/*

    @Autowired
    private IGenericMao genericMao;

    @Override
    public SellerFMMappingUnit getSellerFMMappingUnit(String sellerCode) {
        List<Criteria> queryCriterias = new ArrayList<Criteria>();
        queryCriterias.add(new Criteria(SFMMUFieldNames.SELLER_CODE.getCode(), sellerCode, CocofsMongoQuery.Operator.EQUAL));
        queryCriterias.add(new Criteria(SSCFMMUFieldNames.ENABLED.getCode(), Boolean.TRUE, CocofsMongoQuery.Operator.EQUAL));
        return genericMao.getDocument(new CocofsMongoQuery(queryCriterias), SellerFMMappingUnit.class);
    }

    @Override
    public SellerSupcFMMappingUnit getSellerSupcFMMappingUnit(String sellerCode, String supc) {
        List<Criteria> queryCriterias = new ArrayList<Criteria>();
        queryCriterias.add(new Criteria(SFMMUFieldNames.SELLER_CODE.getCode(), sellerCode, CocofsMongoQuery.Operator.EQUAL));
        queryCriterias.add(new Criteria(SSFMMUFieldNames.SUPC.getCode(), supc, CocofsMongoQuery.Operator.EQUAL));
        queryCriterias.add(new Criteria(SSCFMMUFieldNames.ENABLED.getCode(), Boolean.TRUE, CocofsMongoQuery.Operator.EQUAL));
        return genericMao.getDocument(new CocofsMongoQuery(queryCriterias), SellerSupcFMMappingUnit.class);
    }

    @Override
    public SellerSubcatFMMappingUnit getSellerSubcatFMMappingUnit(String sellerCode, String subcat) {
        List<Criteria> queryCriterias = new ArrayList<Criteria>();
        queryCriterias.add(new Criteria(SFMMUFieldNames.SELLER_CODE.getCode(), sellerCode, CocofsMongoQuery.Operator.EQUAL));
        queryCriterias.add(new Criteria(SSCFMMUFieldNames.SUBCAT.getCode(), subcat, CocofsMongoQuery.Operator.EQUAL));
        queryCriterias.add(new Criteria(SSCFMMUFieldNames.ENABLED.getCode(), Boolean.TRUE, CocofsMongoQuery.Operator.EQUAL));
        return genericMao.getDocument(new CocofsMongoQuery(queryCriterias), SellerSubcatFMMappingUnit.class);
    }

    @Override
    public List<SellerSubcatFMMappingUnit> getSellerSubcatFMMappingUnit(String sellerCode) {
        List<Criteria> queryCriterias = new ArrayList<Criteria>();
        queryCriterias.add(new Criteria(SFMMUFieldNames.SELLER_CODE.getCode(), sellerCode, CocofsMongoQuery.Operator.EQUAL));
        queryCriterias.add(new Criteria(SSCFMMUFieldNames.ENABLED.getCode(), Boolean.TRUE, CocofsMongoQuery.Operator.EQUAL));
        return genericMao.getDocumentList(new CocofsMongoQuery(queryCriterias), SellerSubcatFMMappingUnit.class);
    }

    @Override
    public List<SellerSupcFMMappingUnit> getSellerSupcFMMappingUnits(String sellerCode) {
        List<Criteria> queryCriterias = new ArrayList<Criteria>();
        queryCriterias.add(new Criteria(SFMMUFieldNames.SELLER_CODE.getCode(), sellerCode, CocofsMongoQuery.Operator.EQUAL));
        queryCriterias.add(new Criteria(SSCFMMUFieldNames.ENABLED.getCode(), Boolean.TRUE, CocofsMongoQuery.Operator.EQUAL));
        return genericMao.getDocumentList(new CocofsMongoQuery(queryCriterias), SellerSupcFMMappingUnit.class);
    }

*/}
