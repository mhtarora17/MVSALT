package com.snapdeal.cocofs.dataaccess.fm;

import com.snapdeal.score.data.aerospike.AerospikeSet;


/**
 * Interface to read fulfilment data from Aerospike
 * 
 * @author gaurav
 *
 */

public interface IFMAerospikeDataReadService {
	
	public AerospikeSet getFMMapping(String key, Class<? extends AerospikeSet> klass);

}
