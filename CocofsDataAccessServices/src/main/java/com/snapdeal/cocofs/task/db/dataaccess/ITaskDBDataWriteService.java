/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 28-Aug-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.task.db.dataaccess;

import com.snapdeal.cocofs.entity.TaskDetail;
import com.snapdeal.cocofs.entity.TaskDetailUpdate;
import com.snapdeal.cocofs.entity.TaskParam;
import com.snapdeal.cocofs.entity.TaskStatusInfo;

public interface ITaskDBDataWriteService {

    public void updateTaskDetail(TaskDetail taskDetail);

    public void mergeTaskDetailUpdate(TaskDetailUpdate taskDetailUpdate);

    public void updateTaskStatusInfo(TaskStatusInfo taskStatusInfo);

    public void updateTaskParam(TaskParam taskParam);

}
