/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 28-Aug-2013
 *  @author A. Singhal
 */
package com.snapdeal.cocofs.task.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.snapdeal.cocofs.entity.TaskDetail;
import com.snapdeal.cocofs.entity.TaskDetailUpdate;
import com.snapdeal.cocofs.entity.TaskParam;
import com.snapdeal.cocofs.entity.TaskStatusInfo;
import com.snapdeal.cocofs.task.dao.ITaskDao;

@SuppressWarnings("unchecked")
@Repository("taskDao")
public class TaskDaoImpl implements ITaskDao {

    private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<TaskDetail> getAllTasks() {
        Query q = sessionFactory.getCurrentSession().createQuery("select distinct td from TaskDetail td  where td.quartzManaged = true ");
        return q.list();
    }

    @Override
    public List<TaskDetail> getAllEnabledTasks() {
        Query q = sessionFactory.getCurrentSession().createQuery("select distinct td from TaskDetail td where td.enabled=true");
        return q.list();
    }

    @Override
    public void mergeTaskDetail(TaskDetail taskDetail) {
        sessionFactory.getCurrentSession().merge(taskDetail);
    }

    @Override
    public void mergeTaskParam(TaskParam taskParam) {
        sessionFactory.getCurrentSession().merge(taskParam);
    }

    @Override
    public List<TaskParam> getAllTaskParam() {
        Query q = sessionFactory.getCurrentSession().createQuery("from TaskParam tp");
        return q.list();
    }

    @Override
    public List<TaskParam> getTaskParams(String taskName) {
        Query q = sessionFactory.getCurrentSession().createQuery("from TaskParam tp where taskName= :taskName");
        q.setParameter("taskName", taskName);
        return q.list();
    }

    @Override
    public TaskDetail getTaskDetail(String taskName) {
        Query q = sessionFactory.getCurrentSession().createQuery("select distinct td from TaskDetail td where td.name= :taskName");
        q.setParameter("taskName", taskName);
        return (TaskDetail) q.uniqueResult();
    }

    @Override
    public List<TaskDetail> getTaskDetailByImplClass(String implClass) {
        Query q = sessionFactory.getCurrentSession().createQuery("select distinct td from TaskDetail td  where td.implClass= :implClass");
        q.setParameter("implClass", implClass);
        return q.list();
    }

    @Override
    public void mergeTaskDetailUpdate(TaskDetailUpdate taskDetailUpdate) {
        sessionFactory.getCurrentSession().merge(taskDetailUpdate);

    }

    @Override
    public List<TaskDetailUpdate> getAllTaskDetailUpdateByHostName(String hostName) {
        Query q = sessionFactory.getCurrentSession().createQuery(
                "select distinct tdu from TaskDetailUpdate tdu  where tdu.executed = false and (tdu.hostName = :hostName or tdu.hostName is null) order by tdu.created asc");
        q.setParameter("hostName", hostName);
        return q.list();
    }

    @Override
    public TaskStatusInfo getTaskStatusInfoByTaskName(String taskName) {
        Query q = sessionFactory.getCurrentSession().createQuery("from TaskStatusInfo where taskName=:taskName");
        q.setParameter("taskName", taskName);
        return (TaskStatusInfo) q.uniqueResult();
    }

    @Override
    public void updateTaskStatusInfo(TaskStatusInfo taskStatusInfo) {
        sessionFactory.getCurrentSession().merge(taskStatusInfo);
    }

    @Override
    public List<TaskDetail> getAllTasksByHostName(String hostName) {
        Query q = sessionFactory.getCurrentSession().createQuery("select distinct td from TaskDetail td  where (td.hostName = :hostName or td.hostName is null) ");
        q.setParameter("hostName", hostName);
        return q.list();
    }

    @Override
    public TaskDetailUpdate getUnexecutedTaskDetailUpdateByTaskName(String taskName, String actionCode) {
        Query q = sessionFactory.getCurrentSession().createQuery("from TaskDetailUpdate where taskName=:taskName and command=:actionCode and executed = :executed");
        q.setParameter("taskName", taskName);
        q.setParameter("actionCode", actionCode);
        q.setParameter("executed", false);
        return (TaskDetailUpdate) q.uniqueResult();
    }

    @Override
    public TaskDetailUpdate getUnexecutedTaskDetailUpdateForAllTasks(String actionCode, String hostName) {
        Query q = sessionFactory.getCurrentSession().createQuery(
                "from TaskDetailUpdate where taskName is null and hostName=:hostName and command=:actionCode and executed = :executed");
        q.setParameter("hostName", hostName);
        q.setParameter("actionCode", actionCode);
        q.setParameter("executed", false);
        return (TaskDetailUpdate) q.uniqueResult();
    }

}
