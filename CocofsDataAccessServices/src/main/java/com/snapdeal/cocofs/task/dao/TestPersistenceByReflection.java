package com.snapdeal.cocofs.task.dao;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.snapdeal.cocofs.entity.PersistnceDemo;

public class TestPersistenceByReflection {

    /**
     * @param args
     */
    public static void main(String[] args) {

        Map<String, Object> valueToSetMap = new HashMap<String, Object>(0);
        
        List<String> a1 = new ArrayList<String>();
        a1.add("1111");
        
        valueToSetMap.put("A1", a1);
        valueToSetMap.put("A2", 121);
        RequestG<PersistnceDemo> req = new RequestG<PersistnceDemo>();
        req.setValueToSetMap(valueToSetMap);
        persistByReflection(req);
        
    }
    
    public static <T> boolean persistByReflection(RequestG<T> req){
        
//        Map<String, Object> fieldNameToValueMap = getClassMethods(fromObj);
        
        try {
            
            Object toOBJ = (T)new Object();;
            
            Method[] allMethods = toOBJ.getClass().getMethods();
            List<Method> setters = new ArrayList<Method>();
            String setMName = "";
            
            for(Method method : allMethods) {
                if(method.getName().startsWith("set")) {
                    
                    setMName = method.getName().substring(3);
//                    method.invoke(toOBJ, valueToSetMap.get(setMName));
                }
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        
        
        return true;
    }

    private static class RequestG<T> {
        
        private Map<String, Object> valueToSetMap = new HashMap<String, Object>();

        private Class<T> c;
        public Map<String, Object> getValueToSetMap() {
            return valueToSetMap;
        }

        public void setValueToSetMap(Map<String, Object> valueToSetMap) {
            this.valueToSetMap = valueToSetMap;
        }

        public Class<T> getC() {
            return c;
        }

              
        
    }
    
    private static Map<String, Object> getClassMethods(Object obj){
        Map<String, Object> map = new HashMap<String, Object>();
        Method[] methods = obj.getClass().getMethods();
        
        for(Method m : methods)
        {
            if(m.getName().startsWith("get"))
            {
                Object value;
                try {
                    value = (Object) m.invoke(obj);
                    map.put(m.getName().substring(3), value);
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } 
                
            }
        }       
        return map;
    }
}
