/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 28-Aug-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.task.db.dataaccess.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.snapdeal.cocofs.entity.TaskDetail;
import com.snapdeal.cocofs.entity.TaskDetailUpdate;
import com.snapdeal.cocofs.entity.TaskParam;
import com.snapdeal.cocofs.entity.TaskStatusInfo;
import com.snapdeal.cocofs.task.dao.ITaskDao;
import com.snapdeal.cocofs.task.db.dataaccess.ITaskDBDataWriteService;

@Transactional
@Service("taskDBDataWriteService")
public class TaskDBDataWriteServiceImpl implements ITaskDBDataWriteService {

    private static final Logger LOG = LoggerFactory.getLogger(TaskDBDataWriteServiceImpl.class);

    @Autowired
    private ITaskDao            taskDao;

    @Override
    public void updateTaskDetail(TaskDetail taskDetail) {
        taskDao.mergeTaskDetail(taskDetail);
    }
    
    @Override
    public void updateTaskParam(TaskParam taskParam) {
        taskDao.mergeTaskParam(taskParam);
    }

    @Override
    public void mergeTaskDetailUpdate(TaskDetailUpdate taskDetailUpdate) {
        taskDao.mergeTaskDetailUpdate(taskDetailUpdate);
    }

    @Override
    public void updateTaskStatusInfo(TaskStatusInfo taskStatusInfo) {
        taskDao.updateTaskStatusInfo(taskStatusInfo);
        
    }

}
