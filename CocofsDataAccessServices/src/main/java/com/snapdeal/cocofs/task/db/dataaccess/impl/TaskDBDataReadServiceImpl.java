/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 17-Aug-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.task.db.dataaccess.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.snapdeal.cocofs.entity.TaskDetail;
import com.snapdeal.cocofs.entity.TaskDetailUpdate;
import com.snapdeal.cocofs.entity.TaskParam;
import com.snapdeal.cocofs.entity.TaskStatusInfo;
import com.snapdeal.cocofs.task.dao.ITaskDao;
import com.snapdeal.cocofs.task.db.dataaccess.ITaskDBDataReadService;

@Service("TaskDBDataReadServiceImpl")
@Transactional(readOnly = true)
public class TaskDBDataReadServiceImpl implements ITaskDBDataReadService {

    @Autowired
    private ITaskDao taskDao;

    @Override
    public List<TaskDetail> getAllTasks() {
        return taskDao.getAllTasks();
    }

    @Override
    public List<TaskDetail> getAllEnabledTasks() {
        return taskDao.getAllEnabledTasks();
    }

    @Override
    public List<TaskParam> getTaskParamsByTaskName(String taskName) {
        return taskDao.getTaskParams(taskName);
    }

    @Override
    public TaskDetail getTaskDetailByName(String taskName) {
        return taskDao.getTaskDetail(taskName);
    }

    @Override
    public List<TaskDetail> getTaskDetailByImplClass(String implClass) {
        return taskDao.getTaskDetailByImplClass(implClass);
    }

    @Override
    public List<TaskDetailUpdate> getAllTaskDetailUpdateByHostName(String hostName) {
        return taskDao.getAllTaskDetailUpdateByHostName(hostName);
    }

    @Override
    public TaskStatusInfo getTaskStatusInfoByTaskName(String taskName) {
        return taskDao.getTaskStatusInfoByTaskName(taskName);
    }

    @Override
    public List<TaskDetail> getAllTasksByHostName(String hostname) {
        return taskDao.getAllTasksByHostName(hostname);
    }

    @Override
    public TaskDetailUpdate getUnexecutedTaskDetailUpdateByTaskName(String taskName, String actionCode) {
        return taskDao.getUnexecutedTaskDetailUpdateByTaskName(taskName, actionCode);
    }

    @Override
    public TaskDetailUpdate getUnexecutedTaskDetailUpdateForAllTasks(String actionCode, String hostName) {
        return taskDao.getUnexecutedTaskDetailUpdateForAllTasks( actionCode, hostName);
    }
}
