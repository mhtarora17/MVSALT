/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 22-May-2013
 *  @author amd
 */
package com.snapdeal.cocofs.task.dao;

import java.util.List;

import com.snapdeal.cocofs.entity.TaskDetail;
import com.snapdeal.cocofs.entity.TaskDetailUpdate;
import com.snapdeal.cocofs.entity.TaskParam;
import com.snapdeal.cocofs.entity.TaskStatusInfo;

public interface ITaskDao {

    public List<TaskDetail> getAllTasks();

    public void mergeTaskDetail(TaskDetail taskDetail);

    public List<TaskParam> getTaskParams(String taskName);

    public TaskDetail getTaskDetail(String taskName);

    public List<TaskDetail> getTaskDetailByImplClass(String implClass);

    public void mergeTaskDetailUpdate(TaskDetailUpdate taskDetailUpdate);

    public List<TaskDetailUpdate> getAllTaskDetailUpdateByHostName(String hostName);

    public List<TaskDetail> getAllEnabledTasks();

    public TaskStatusInfo getTaskStatusInfoByTaskName(String taskName);

    public void updateTaskStatusInfo(TaskStatusInfo taskStatusInfo);

    public List<TaskParam> getAllTaskParam();

    public void mergeTaskParam(TaskParam taskParam);

    public List<TaskDetail> getAllTasksByHostName(String hostname);

    public TaskDetailUpdate getUnexecutedTaskDetailUpdateForAllTasks(String actionCode, String hostName);

    public TaskDetailUpdate getUnexecutedTaskDetailUpdateByTaskName(String taskName, String actionCode);
}
