/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 17-Aug-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.task.db.dataaccess;

import java.util.List;

import com.snapdeal.cocofs.entity.TaskDetail;
import com.snapdeal.cocofs.entity.TaskDetailUpdate;
import com.snapdeal.cocofs.entity.TaskParam;
import com.snapdeal.cocofs.entity.TaskStatusInfo;

public interface ITaskDBDataReadService {

    public List<TaskDetail> getAllTasks();

    public List<TaskParam> getTaskParamsByTaskName(String taskName);

    public TaskDetail getTaskDetailByName(String taskName);

    public List<TaskDetail> getAllEnabledTasks();

    public List<TaskDetail> getTaskDetailByImplClass(String implClass);

    public List<TaskDetailUpdate> getAllTaskDetailUpdateByHostName(String hostname);

    public TaskStatusInfo getTaskStatusInfoByTaskName(String taskName);

    public List<TaskDetail> getAllTasksByHostName(String hostname);

    public TaskDetailUpdate getUnexecutedTaskDetailUpdateForAllTasks(String actionCode, String hostName);

    public TaskDetailUpdate getUnexecutedTaskDetailUpdateByTaskName(String taskName, String actionCode);

}
