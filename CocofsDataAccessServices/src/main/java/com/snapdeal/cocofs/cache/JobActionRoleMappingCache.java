package com.snapdeal.cocofs.cache;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.snapdeal.base.annotations.Cache;
import com.snapdeal.cocofs.entity.JobActionRoleMapping;

@Cache(name = "JobActionRoleMappingCache")
public class JobActionRoleMappingCache {

    private Map<String, Set<String>> actionCodeToRoleListMap     = new HashMap<String, Set<String>>();

    private Map<String, Set<String>> roleCodeToActionCodeListMap = new HashMap<String, Set<String>>();

    public void addJobActionRoleMapping(JobActionRoleMapping jobActionRoleMapping) {
        if (!actionCodeToRoleListMap.containsKey(jobActionRoleMapping.getActionCode())) {
            actionCodeToRoleListMap.put(jobActionRoleMapping.getActionCode(), new HashSet<String>());
        }
        actionCodeToRoleListMap.get(jobActionRoleMapping.getActionCode()).add(jobActionRoleMapping.getRoleCode());

        if (!roleCodeToActionCodeListMap.containsKey(jobActionRoleMapping.getRoleCode())) {
            roleCodeToActionCodeListMap.put(jobActionRoleMapping.getRoleCode(), new HashSet<String>());
        }
        roleCodeToActionCodeListMap.get(jobActionRoleMapping.getRoleCode()).add(jobActionRoleMapping.getActionCode());

    }

    public Set<String> getAllRoleCodesFromActionCode(String actionCode) {
        Set<String> roleCodeList=actionCodeToRoleListMap.get(actionCode);
        if(roleCodeList==null){
            roleCodeList= new HashSet<String>();
        }
        return roleCodeList;
    }

    public Set<String> getAllActionCodesFromRoleCode(String roleCode) {
        Set<String> actionCodeList= roleCodeToActionCodeListMap.get(roleCode);
        if(actionCodeList==null){
            actionCodeList= new HashSet<String>();
        }
        return actionCodeList;
    }
}
