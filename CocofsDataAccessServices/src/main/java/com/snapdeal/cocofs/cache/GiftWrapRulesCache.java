/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.cache;

import java.util.List;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.snapdeal.base.annotations.Cache;
import com.snapdeal.base.utils.CollectionUtils;
import com.snapdeal.cocofs.mongo.model.CocofsRuleUnit;

/**
 *  
 *  @version     1.0, 08-Dec-2014
 *  @author ankur
 */

@Cache(name = "GiftWrapRulesCache")
public class GiftWrapRulesCache {

    private ListMultimap<String, CocofsRuleUnit> rulesMap = ArrayListMultimap.create();

    
    public void addRules(List<CocofsRuleUnit> ruleUnits){
        if(CollectionUtils.isEmpty(ruleUnits)){
            return;
        }
        
        for(CocofsRuleUnit ru : ruleUnits){
            String key = prepareKeyFromRuleUnit(ru);
            rulesMap.put(key, ru);
        }
    }
    
    private String prepareKeyFromRuleUnit(CocofsRuleUnit ru){
        StringBuffer buffer=new StringBuffer();
        buffer.append(ru.getCategoryUrl());
        return buffer.toString();
    }
    
    @SuppressWarnings("unchecked")
    public List<CocofsRuleUnit> getRules(String key){

        if (rulesMap == null || rulesMap.isEmpty()) {
            return null;
        } else {
            return rulesMap.get(key);
        }

    }
}
