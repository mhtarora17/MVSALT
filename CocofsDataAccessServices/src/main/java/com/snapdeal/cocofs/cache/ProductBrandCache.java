package com.snapdeal.cocofs.cache;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import com.snapdeal.base.annotations.Cache;
import com.snapdeal.catalog.base.sro.MinProductBrandSRO;

@Cache(name = "productBrandCache")
public class ProductBrandCache {

    private List<String> brands    = new ArrayList<String>();
    private Set<String>  brandsSet = new LinkedHashSet<String>();

    public void addMappings(List<MinProductBrandSRO> sros) {
        List<String> brands = new ArrayList<String>();
        for (MinProductBrandSRO brand : sros) {
            brands.add(brand.getName());
        }
        Collections.sort(brands);
        this.setBrands(brands);
        this.brandsSet = new LinkedHashSet<String>(brands);
    }

    public boolean isValidBrand(String brandName) {
        return brandsSet.contains(brandName);
    }

    public List<String> getBrands() {
        return brands;
    }

    public void setBrands(List<String> brands) {
        this.brands = brands;
    }
    
    public List<String> getBrandsWithStartName(String query){
        List<String> matchedBrands = new ArrayList<String>();
        for(String brand : brands){
            if(brand.toLowerCase().startsWith(query.toLowerCase())){
                matchedBrands.add(brand);
            }
        }
        return matchedBrands;
    }

}
