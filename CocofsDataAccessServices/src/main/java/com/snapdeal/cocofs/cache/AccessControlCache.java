/*
 *  Copyright 2011 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Apr 21, 2011
 *  @author Rajpal
 */
package com.snapdeal.cocofs.cache;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.snapdeal.base.annotations.Cache;
import com.snapdeal.cocofs.entity.AccessControl;

@Cache(name = "accessControlCache")
public class AccessControlCache {
    private Map<String, AccessControl> urlToAccessControl = new HashMap<String, AccessControl>();
    private List<AccessControl>        urlList            = new ArrayList<AccessControl>();

    public void addAccessControls(AccessControl accessControl) {
        urlToAccessControl.put(accessControl.getUrlPrefix(), accessControl);
        urlList.add(accessControl);
    }

    public AccessControl getAccessControl(String url){
        return urlToAccessControl.get(url);
    }
    
    public boolean anyAccessExists(String url){
        return urlToAccessControl.containsKey(url);
    }
}
