package com.snapdeal.cocofs.cache;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import com.snapdeal.base.annotations.Cache;
import com.snapdeal.score.sro.CityStateTuple;

@Cache(name = "stateCache")
public class StateCache {

    private List<String> statesList = new ArrayList<String>();
    private Set<String>  statesSet  = new LinkedHashSet<String>();

    public List<String> getStatesList() {
        return statesList;
    }

    public boolean isValidState(String stateName) {
        return statesSet.contains(stateName);
    }

    public void addMappings(List<CityStateTuple> tuples) {

        int size = 16; //java.util.HashMap.DEFAULT_INITIAL_CAPACITY
        if (tuples != null) {
            size = tuples.size();
        }

        Set<String> statesSet = new LinkedHashSet<String>(size);
        List<String> statesList = new ArrayList<String>(size);
        for (CityStateTuple o : tuples) {
            String state = o.getState();
            if (!statesSet.contains(state)) {
                statesSet.add(state);
                statesList.add(state);
            }
        }

        Collections.sort(statesList);
        statesSet = new LinkedHashSet<String>(statesList);
        this.statesList = statesList;
        this.statesSet = statesSet;
    }

}
