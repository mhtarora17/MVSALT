package com.snapdeal.cocofs.cache;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.snapdeal.base.annotations.Cache;
import com.snapdeal.cocofs.entity.JobAction;
import com.snapdeal.cocofs.entity.JobStatus;

@Cache(name = "JobCache")
public class JobCache {
    private Map<String, JobStatus> jobStatusMap = new HashMap<String, JobStatus>();
    private Map<String, JobAction> jobActionMap = new HashMap<String, JobAction>();

    private static final Comparator<JobStatus> JSC = new Comparator<JobStatus>() {
        
        @Override
        public int compare(JobStatus o1, JobStatus o2) {
            return o1.getDescription().compareTo(o2.getDescription());
        }
        
    };
    public JobStatus getJobStatusByCode(String jobStatusCode) {
        return jobStatusMap.get(jobStatusCode);
    }

    public void addJobStatusByCode(JobStatus jobStatus) {
        jobStatusMap.put(jobStatus.getCode(), jobStatus);
    }

    public JobAction getJobActionByCode(String jobActionCode) {
        return jobActionMap.get(jobActionCode);
    }

    public void addJobActionByCode(JobAction jobAction) {
        jobActionMap.put(jobAction.getCode(), jobAction);
    }
    
    public List<JobStatus> getAllJobStatus() {
        List<JobStatus> list = new ArrayList<JobStatus>(jobStatusMap.values());
        Collections.sort(list, JSC);
        return list;
    }
}
