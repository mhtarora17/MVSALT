package com.snapdeal.cocofs.cache;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.snapdeal.base.annotations.Cache;
import com.snapdeal.cocofs.entity.Attribute;

@Cache(name = "attributeCache")
public class AttributeCache {

    private static final Comparator<? super Attribute> ATTR_COMPARATOR = new Comparator<Attribute>() {

        @Override
        public int compare(Attribute o1, Attribute o2) {
            return o1.getAttribute().compareTo(o2.getAttribute());
        }
    };
    private Map<String, Attribute> codeToAttribute    = new HashMap<String, Attribute>();
    private List<Attribute>        enabledAttributes  = new ArrayList<Attribute>();
    private List<Attribute>        disabledAttributes = new ArrayList<Attribute>();

    /**
     * Adds given attributes to cache, does not remove attributes already in cache
     * 
     * @param attrList
     */
    public void addAttributes(List<Attribute> attrList) {
        for (Attribute a : attrList) {
            codeToAttribute.put(a.getAttribute(), a);
            if (a.getEnabled()) {
                enabledAttributes.add(a);
            } else {
                disabledAttributes.add(a);
            }
        }
    }

    /**
     * Clears the cache and populates it with given attributes
     * 
     * @param attrList
     */
    public void setAttributes(List<Attribute> attrList) {
        codeToAttribute = new HashMap<String, Attribute>();
        enabledAttributes = new ArrayList<Attribute>();
        disabledAttributes = new ArrayList<Attribute>();
        addAttributes(attrList);
    }
    
    public Attribute getAttributeForCode(String code) {
        return codeToAttribute.get(code);
    }
    
    /**
     * Return all enabled attributes sorted by Attribute field(code)
     * @return
     */
    public List<Attribute> getEnabledAttributes() {
        List<Attribute> l = new ArrayList<Attribute>(enabledAttributes);
        Collections.sort(l, ATTR_COMPARATOR);
        return l;
    }
    
    /**
     * Return all disabled attributes sorted by Attribute field(code)
     * @return
     */
    public List<Attribute> getDisabledAttributes() {
        List<Attribute> l = new ArrayList<Attribute>(disabledAttributes);
        Collections.sort(l, ATTR_COMPARATOR);
        return l;
    }

}
