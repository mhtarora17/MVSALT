package com.snapdeal.cocofs.cache;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import com.snapdeal.base.annotations.Cache;
import com.snapdeal.score.sro.CityStateTuple;

@Cache(name = "taxClassCache")
public class TaxClassCache {

    private List<String> taxClassList = new ArrayList<String>();
    private Set<String>  taxClassSet  = new LinkedHashSet<String>();

    public List<String> getTaxClassList() {
        return taxClassList;
    }

    public boolean isValidTaxClass(String taxClass) {
        return taxClassSet.contains(taxClass);
    }

    public void addMappings(List<String> taxClasses) {

        int size = 16; //java.util.HashMap.DEFAULT_INITIAL_CAPACITY
        if (taxClasses != null) {
            size = taxClasses.size();
        }

        Set<String> taxClassSet = new LinkedHashSet<String>(size);
        List<String> taxClassList = new ArrayList<String>(size);
        for (String o : taxClasses) {
            if (!taxClassSet.contains(o)) {
                taxClassSet.add(o);
                taxClassList.add(o);
            }
        }

        Collections.sort(taxClassList);
        taxClassSet = new LinkedHashSet<String>(taxClassList);
        this.taxClassList = taxClassList;
        this.taxClassSet = taxClassSet;
    }

}
