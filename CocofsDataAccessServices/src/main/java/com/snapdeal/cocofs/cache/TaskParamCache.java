/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 12-Sep-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.cache;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.snapdeal.base.annotations.Cache;
import com.snapdeal.cocofs.entity.TaskParam;

@Cache(name = "taskParamCache")
public class TaskParamCache {

    private Map<String, List<TaskParam>>     taskNameToParamsMap          = new HashMap<String, List<TaskParam>>();

    private Map<String, Map<String, String>> taskNameToParamKeyToValueMap = new HashMap<String, Map<String, String>>();

    public void addToTaskParamCache(TaskParam taskParam) {
        if (null != taskParam) {
            String taskName = taskParam.getTaskName();
            List<TaskParam> paramList = taskNameToParamsMap.get(taskName);
            Map<String, String> paramkeyToValueMap = taskNameToParamKeyToValueMap.get(taskName);
            if (null == paramList) {
                paramList = new ArrayList<TaskParam>();
                paramkeyToValueMap = new HashMap<String, String>();
            }
            paramkeyToValueMap.put(taskParam.getName(), taskParam.getValue());
            paramList.add(taskParam);
            taskNameToParamsMap.put(taskName, paramList);
        }
    }

    public Map<String, List<TaskParam>> getTaskNameToParamsMap() {
        return taskNameToParamsMap;
    }

    public void setTaskNameToParamsMap(Map<String, List<TaskParam>> taskNameToParamsMap) {
        this.taskNameToParamsMap = taskNameToParamsMap;
    }

    public Map<String, Map<String, String>> getTaskNameToParamKeyToValueMap() {
        return taskNameToParamKeyToValueMap;
    }

    public void setTaskNameToParamKeyToValueMap(Map<String, Map<String, String>> taskNameToParamKeyToValueMap) {
        this.taskNameToParamKeyToValueMap = taskNameToParamKeyToValueMap;
    }

    public Map<String, String> getTaskParamKeyToValueMap(String taskName) {
        return taskNameToParamKeyToValueMap.get(taskName);
    }

}
