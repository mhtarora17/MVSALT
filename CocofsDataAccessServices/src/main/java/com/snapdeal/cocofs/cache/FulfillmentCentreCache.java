/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.cache;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.snapdeal.base.annotations.Cache;
import com.snapdeal.cocofs.entity.FulfillmentCentre;

/**
 *  
 *  @version     1.0, 30-Mar-2015
 *  @author ankur
 */
@Cache(name = "fcCache")
public class FulfillmentCentreCache {

    private List<FulfillmentCentre>              allFulfillmentCentres = new ArrayList<FulfillmentCentre>();
    private Map<String, FulfillmentCentre>       codeToFC              = new HashMap<String, FulfillmentCentre>();
    private Map<String, List<FulfillmentCentre>> typeToFC              = new HashMap<String, List<FulfillmentCentre>>();

    public void addFulfillmentCenter(FulfillmentCentre centre) {
        codeToFC.put(centre.getCode(), centre);
        allFulfillmentCentres.add(centre);
        if (typeToFC.get(centre.getType()) == null) {
            typeToFC.put(centre.getType(), new ArrayList<FulfillmentCentre>());
        }
        typeToFC.get(centre.getType()).add(centre);
    }

    public List<FulfillmentCentre> getAllFulfillmentCentres() {
        return allFulfillmentCentres;
    }

    public FulfillmentCentre getfulFulfillmentCentreByCode(String code) {
        return codeToFC.get(code);
    }

    public Set<FulfillmentCentre> getFulfillmentCentresByType(String type) {
        if (typeToFC.get(type) != null) {
            return new HashSet<FulfillmentCentre>(typeToFC.get(type));
        } else {
            return new HashSet<FulfillmentCentre>();
        }

    }
}



