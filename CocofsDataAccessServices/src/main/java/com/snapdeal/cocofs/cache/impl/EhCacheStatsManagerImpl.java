/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Nov 6, 2013
 *  @author himanshu
 */
package com.snapdeal.cocofs.cache.impl;


import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Statistics;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.cache.EhCacheStatsManager;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;

@Service("ehCacheStatsManager")
public class EhCacheStatsManagerImpl implements EhCacheStatsManager {

    /**
     */
    private static final Logger                   LOGGER = LoggerFactory.getLogger(EhCacheStatsManager.class);
    /**
     * Runs the logging task every <b>5 minutes</b> with starting delay of <b>5 minutes</b>.
     */
    private static final ScheduledExecutorService POOL   = new ScheduledThreadPoolExecutor(1);

    /**
     * {@link EhCacheCacheManager} instance retrieved from spring context.
     */
    @Autowired
    private EhCacheCacheManager                   ehCacheCacheManager;
    
    /**
     * Cache manager instance of {@link CacheManager} that is backing spring cache.
     */
    private CacheManager                          cacheManager;
    
    @Autowired
    public void setCacheManager(CacheManager cacheManager) {
        this.cacheManager = ehCacheCacheManager.getCacheManager();
    }

    @Override
    public void log() {
        CacheManager cacheManager = ehCacheCacheManager.getCacheManager();
        if (ConfigUtils.getBooleanScalar(Property.CACHE_STATS_ENABLED)) {
            String[] caches = cacheManager.getCacheNames();
            for (String cacheName : caches) {
                CacheStats stats = getCacheStats(cacheName);
                LOGGER.info(stats != null ? stats.toString() : cacheName + ": No stats available right now");
            }
        }
    }

    @Override
    public CacheStats getCacheStats(String cacheName) {
        Cache cache = cacheManager.getCache(cacheName);
        if (cache != null) {
            Statistics stats = cache.getStatistics();
            CacheStats cacheStats = new CacheStats();
            cacheStats.setCacheName(cacheName);
            cacheStats.setHits(stats.getCacheHits());
            cacheStats.setMisses(stats.getCacheMisses());
            cacheStats.setObjectCount(stats.getObjectCount());
            if((stats.getCacheHits() + stats.getCacheMisses()) > 0) {
                cacheStats.setHitRatio(((double)stats.getCacheHits()* 100)/(stats.getCacheHits() + stats.getCacheMisses()));
            }
            return cacheStats;
        }
        return null;
    }

    /**
     * 
     */
    public EhCacheStatsManagerImpl() {
        POOL.scheduleWithFixedDelay(new Runnable() {
            @Override
            public void run() {
                log();
            }
        }, 5 * 60, 5 * 60, TimeUnit.SECONDS);
    }

}
