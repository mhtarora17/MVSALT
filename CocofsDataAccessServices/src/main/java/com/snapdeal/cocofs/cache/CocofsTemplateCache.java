/*
 *  Copyright 2012 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Nov 6, 2012
 *  @author Abhinav Singhal
 */
package com.snapdeal.cocofs.cache;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.snapdeal.base.annotations.Cache;
import com.snapdeal.base.velocity.Template;
import com.snapdeal.cocofs.entity.CocofsTemplate;

@Cache(name = "cocofsTemplateCache")
public class CocofsTemplateCache {
    private final Map<String, Template> cocofsTemplates    = new HashMap<String, Template>();
    private final List<Template>        cocofsTemplateList = new ArrayList<Template>();

    public void addCocofsTemplate(CocofsTemplate cocofsTemplate) {
        Template template = Template.compile(cocofsTemplate.getTemplate());
        cocofsTemplates.put(cocofsTemplate.getName(), template);
        cocofsTemplateList.add(template);
    }

    public Template getCocofsTemplateByName(String templateName) {
        return cocofsTemplates.get(templateName);
    }

    public List<Template> getCocofsTemplateList() {
        return cocofsTemplateList;
    }

    public Map<String, Template> getCocofsTemplates() {
        return cocofsTemplates;
    }
}
