/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Nov 6, 2013
 *  @author himanshu
 */
package com.snapdeal.cocofs.cache;

import java.io.Serializable;

public interface EhCacheStatsManager {

    public static class CacheStats implements Serializable {
        /**
         * 
         */
        private static final long serialVersionUID = -7794466224954198664L;
        /**
         * Cache name to which this statistics belongs.
         */
        private String      cacheName;
        /**
         * Number of cache hits.
         */
        private long         hits;
        /**
         * Number of cache misses.
         */
        private long         misses;
        /**
         * Total objects in cache.
         */
        private long         objectCount;
        /**
         * Hit v/s miss ratio
         */
        private double      hitRatio;
        
        @Override
        public String toString() {
            StringBuilder builder = new StringBuilder();
            builder.append("CacheStats [cacheName=").append(cacheName)
                    .append(", hits=").append(hits).append(", misses=")
                    .append(misses).append(", objectCount=")
                    .append(objectCount).append(", hitRatio=").append(hitRatio)
                    .append("]");
            return builder.toString();
        }

        public String getCacheName() {
            return cacheName;
        }

        public void setCacheName(String cacheName) {
            this.cacheName = cacheName;
        }

        public long getHits() {
            return hits;
        }

        public void setHits(long hits) {
            this.hits = hits;
        }

        public long getMisses() {
            return misses;
        }

        public void setMisses(long misses) {
            this.misses = misses;
        }

        public long getObjectCount() {
            return objectCount;
        }

        public void setObjectCount(long objectCount) {
            this.objectCount = objectCount;
        }

        public double getHitRatio() {
            return hitRatio;
        }

        public void setHitRatio(double hitRatio) {
            this.hitRatio = hitRatio;
        }
        
        
    }
    
    /**
     * Cache statistics logging method.
     */
    public void log();
    /**
     * Returns {@link CacheStats} for the given cache.
     * @param cache
     * @return
     */
    public CacheStats getCacheStats(String cache);
}
