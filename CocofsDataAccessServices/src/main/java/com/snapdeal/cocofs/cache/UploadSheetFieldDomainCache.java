/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.cache;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.snapdeal.base.annotations.Cache;
import com.snapdeal.cocofs.entity.UploadSheetFieldDomain;

/**
 *  
 *  @version     1.0, 17-Feb-2015
 *  @author ankur
 */

@Cache(name = "uploadSheetFieldDomainCache")
public class UploadSheetFieldDomainCache {
    private Map<String, Map<String, List<String>>> classNameToFieldNameToValueMap = new HashMap<String, Map<String, List<String>>>();

    public void addToUploadSheetFieldDomainCache(UploadSheetFieldDomain fieldDomain) {
        if (null != fieldDomain) {
            String className = fieldDomain.getClassName().toLowerCase();
            Map<String, List<String>> fieldNameToValueMap = classNameToFieldNameToValueMap.get(className);
            if (null == fieldNameToValueMap) {
                fieldNameToValueMap = new HashMap<String, List<String>>();
                classNameToFieldNameToValueMap.put(className, fieldNameToValueMap);
            }
            
            String fieldName = fieldDomain.getFieldName().toLowerCase();
            List<String> values = fieldNameToValueMap.get(fieldName);
            
            if( null == values){
                values =  new ArrayList<String>();
                fieldNameToValueMap.put(fieldName, values);
            }
            
            values.add(fieldDomain.getValue());
            
        }
    }

  
    public List<String> getValuesForClassField(String className, String fieldName){
        if(classNameToFieldNameToValueMap != null && classNameToFieldNameToValueMap.get(className.toLowerCase()) != null){
            return classNameToFieldNameToValueMap.get(className.toLowerCase()).get(fieldName.toLowerCase());
        }
        
        return new ArrayList<String>();
    }

}
