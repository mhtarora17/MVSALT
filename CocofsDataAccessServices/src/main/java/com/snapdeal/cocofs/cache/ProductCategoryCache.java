package com.snapdeal.cocofs.cache;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.snapdeal.base.annotations.Cache;
import com.snapdeal.catalog.base.sro.ProductCategorySRO;
import com.snapdeal.cocofs.dto.ProductCategoryDTO;

@Cache(name = "productCategoryCache")
public class ProductCategoryCache {

    private static final Logger             LOG                     = LoggerFactory.getLogger(ProductCategoryCache.class);

    private Map<String, ProductCategoryDTO> productCategoryMappings = new HashMap<String, ProductCategoryDTO>();

    public Map<String, ProductCategoryDTO> getAllMappings() {
        return productCategoryMappings;
    }

    // key is pageURL or sub-cat in CoCoFS terminology
    public ProductCategoryDTO getMapping(String pageURL) {
        return productCategoryMappings.get(pageURL);
    }

    public void addMappings(List<ProductCategorySRO> productCategorySROs) {
        int size = 16; //java.util.HashMap.DEFAULT_INITIAL_CAPACITY
        if (productCategorySROs != null) {
            size = productCategorySROs.size();
        }
        Map<String, ProductCategoryDTO> productCategoryMapping = new HashMap<String, ProductCategoryDTO>(size);
        for (ProductCategorySRO o : productCategorySROs) {
            productCategoryMapping.put(o.getPageUrl(), convert(o));
        }
        this.productCategoryMappings = productCategoryMapping;
    }

    private ProductCategoryDTO convert(ProductCategorySRO in) {
        return new ProductCategoryDTO(in);
    }

    public boolean isValidSubCat(String subCategoryUrl) {
        ProductCategoryDTO item = getMapping(subCategoryUrl);
        if (item != null && item.isSubCat()) {
            if (subCategoryUrl.equals(item.getPageUrl())) {
                return true;
            }
        }

        return false;
    }

    public Map<String, String> getAllSubCategories() {
        return filterAllCategoriesInfo(true);
    }

    public Map<String, String> getAllCategories() {
        return filterAllCategoriesInfo(false);
    }

    /**
     * @param bSubcat if true return all subcats, if false return all categories
     * @return
     */
    private Map<String, String> filterAllCategoriesInfo(boolean bSubcat) {
        Map<String, String> filteredCategories = new HashMap<String, String>();
        try {
            Map<String, ProductCategoryDTO> map = getAllMappings();
            for (Entry<String, ProductCategoryDTO> o : map.entrySet()) {

                ProductCategoryDTO item = o.getValue();

                if (bSubcat) {
                    if (item != null && item.isSubCat()) {
                        // add only subcats 
                        LOG.debug("Child Category " + item);
                        String name = item.getName();
                        String parentCategoryName = item.getParentCategoryName();
                        if (null != parentCategoryName) {
                            name = parentCategoryName + " - " + name;
                        }
                        filteredCategories.put(item.getPageUrl(), name);
                    }
                } else {
                    if (item != null && !item.isSubCat()) {
                        // add only cats 
                        String name = item.getName();
                        LOG.debug("Parent Category " + item);
                        filteredCategories.put(item.getPageUrl(), name);
                    }
                }
            }

        } catch (Exception e) {
            LOG.error("Could not determine categories ", e);
        }

        Map<String, String> m = new LinkedHashMap<String, String>(filteredCategories.size());
        Set<Entry<String, String>> s = filteredCategories.entrySet();
        List<Entry<String, String>> l = new ArrayList<Entry<String, String>>(s);
        Comparator<? super Entry<String, String>> c = new Comparator<Entry<String, String>>() {

            @Override
            public int compare(Entry<String, String> o1, Entry<String, String> o2) {
                return o1.getValue().compareTo(o2.getValue());
            }
        };
        Collections.sort(l, c);
        for (Entry<String, String> e : l) {
            m.put(e.getKey(), e.getValue());
        }
        return m;
    }

}
