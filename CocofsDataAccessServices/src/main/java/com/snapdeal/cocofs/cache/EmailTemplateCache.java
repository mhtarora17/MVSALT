/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 05-Sep-2013
 *  @author himanshum
 */
package com.snapdeal.cocofs.cache;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.snapdeal.base.annotations.Cache;
import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.base.velocity.Template;
import com.snapdeal.base.vo.EmailTemplateVO;
import com.snapdeal.cocofs.entity.EmailTemplate;


@Cache(name = "emailTemplateCache")
public class EmailTemplateCache {
    private final Map<String, EmailTemplateVO> emailTemplates    = new HashMap<String, EmailTemplateVO>();
    private final List<EmailTemplateVO>        emailTemplateList = new ArrayList<EmailTemplateVO>();

    public void addEmailTemplate(EmailTemplate template) {
        EmailTemplateVO templateVO = new EmailTemplateVO();
        templateVO.setName(template.getName());
        if (StringUtils.isNotEmpty(template.getTo())) {
            templateVO.setTo(StringUtils.split(template.getTo()));
        }
        if (StringUtils.isNotEmpty(template.getCc())) {
            templateVO.setCc(StringUtils.split(template.getCc()));
        }
        if (StringUtils.isNotEmpty(template.getBcc())) {
            templateVO.setBcc(StringUtils.split(template.getBcc()));
        }
        if (StringUtils.isNotEmpty(template.getFrom())) {
            templateVO.setFrom(template.getFrom());
        }
        if (StringUtils.isNotEmpty(template.getReplyTo())) {
            templateVO.setReplyTo(template.getReplyTo());
        }
        templateVO.setBodyTemplate(Template.compile(template.getBodyTemplate()));
        templateVO.setSubjectTemplate(Template.compile(template.getSubjectTemplate()));
        templateVO.setEmailChannelId(template.getEmailChannelId());
        emailTemplates.put(template.getName(), templateVO);
        emailTemplateList.add(templateVO);
    }

    public void freeze() {
        Collections.sort(emailTemplateList, new Comparator<EmailTemplateVO>() {
            @Override
            public int compare(EmailTemplateVO arg0, EmailTemplateVO arg1) {
                return arg0.getName().compareTo(arg1.getName());
            }

        });
    }
    public EmailTemplateVO getTemplateByName(String name) {
        return emailTemplates.get(name);
    }

    public List<EmailTemplateVO> getEmailTemplates() {
        return emailTemplateList;
    }



}
