/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 03-Oct-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.mongo.mao;

import java.util.List;

import com.snapdeal.cocofs.mongo.CocofsMongoQuery;

public interface IGenericMao {

    /**
     * Return List of documents corresponding to the search criteria
     * 
     * @param <T> Document class name
     * @param queryCriterias list having fieldNames, object( which may corresponds to a single value or a list of
     *            values) and oeprator enum.
     * @param documentClass
     * @return a list of documents of class T
     */
    public <T> List<T> getDocumentList(CocofsMongoQuery cocoQuery, Class<T> documentClass);

    /**
     * Return a single document corresponding to search criteria
     * 
     * @param <T> Document class name
     * @param queryCriterias list having fieldNames, object( which may corresponds to a single value or a list of
     *            values) and oeprator enum.
     * @param documentClass
     * @return a single document of class T
     */
    public <T> T getDocument(CocofsMongoQuery cocoQuery, Class<T> documentClass);

    /**
     * Save the given document
     * 
     * @param document document to be saved
     */
    public <T> void saveDocument(T document);

    /**
     * Removes the given document
     * 
     * @param document
     */
    public <T> void deleteDocument(T document);

    /**
     * @param <T>
     * @param cocoQuery
     * @param documentClass
     * @return
     */
    public <T> Long getDocumentCount(CocofsMongoQuery cocoQuery, Class<T> documentClass);

}
