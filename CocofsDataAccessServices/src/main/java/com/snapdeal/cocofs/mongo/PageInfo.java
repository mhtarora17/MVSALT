package com.snapdeal.cocofs.mongo;

/**
 * @author nikhil
 */
public class PageInfo {

    private int          pageNumber;

    private int          size;

    private SortCriteria sortCriteria;

    public PageInfo(int pageNumber, int size, SortCriteria sortCriteria) {
        this.pageNumber = pageNumber;
        this.size = size;
        this.sortCriteria = sortCriteria;
    }

    public PageInfo(int pageNumber, int size) {
        this.pageNumber = pageNumber;
        this.size = size;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public SortCriteria getSortCriteria() {
        return sortCriteria;
    }

    public void setSortCriteria(SortCriteria sortCriteria) {
        this.sortCriteria = sortCriteria;
    }

}