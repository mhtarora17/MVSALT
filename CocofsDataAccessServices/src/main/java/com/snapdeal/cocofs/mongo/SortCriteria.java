package com.snapdeal.cocofs.mongo;

public class SortCriteria {

    public enum Sort {
        ASC, DESC;
    }

    private Sort   sortType;

    private String fieldName;
    
    public SortCriteria(Sort sortType, String fieldName){
        this.sortType = sortType;
        this.fieldName = fieldName;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public Sort getSortType() {
        return sortType;
    }

    public void setSortType(Sort sortType) {
        this.sortType = sortType;
    }

}
