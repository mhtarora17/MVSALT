/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 25-Aug-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.mongo.mao.impl;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.snapdeal.cocofs.mongo.mao.IProductAttributeUnitMao;
import com.snapdeal.cocofs.mongo.model.ProductAttributeUnit;
import com.snapdeal.cocofs.mongo.model.ProductAttributeUnit.PAUFieldNames;

@Repository("productAttributeUnitMao")
public class ProductAttributeUnitMaoImpl implements IProductAttributeUnitMao {

    @Autowired
    @Qualifier("mongoTemplateCocofs")
    private MongoOperations mongoOperations;

    @Override
    public void saveProductAttributeUnit(ProductAttributeUnit pau) {
        mongoOperations.save(pau);
    }

    @Override
    public void deleteProductAttributeUnit(ProductAttributeUnit pau) {
        mongoOperations.remove(pau);

    }

    @Override
    public ProductAttributeUnit getUniqueProductAttributeUnit(Map<PAUFieldNames, Object> fieldListToValueMap) {
        Query query = new Query();
        for (Entry<PAUFieldNames, Object> fieldListToValueEntry : fieldListToValueMap.entrySet()) {
            if (fieldListToValueEntry.getValue() instanceof List) {
                query.addCriteria(Criteria.where(fieldListToValueEntry.getKey().getCode()).is(fieldListToValueEntry.getValue()));
            } else {
                query.addCriteria(Criteria.where(fieldListToValueEntry.getKey().getCode()).in(fieldListToValueEntry.getValue()));
            }
        }
        //TODO add for enabled 
        return mongoOperations.findOne(query, ProductAttributeUnit.class);
    }
  
    @Override
    public List<ProductAttributeUnit> getProductAttributeUnit(Map<PAUFieldNames, Object> fieldListToValueMap) {
        Query query = new Query();
        for (Entry<PAUFieldNames, Object> fieldListToValueEntry : fieldListToValueMap.entrySet()) {
            if (fieldListToValueEntry.getValue() instanceof List) {
                query.addCriteria(Criteria.where(fieldListToValueEntry.getKey().getCode()).is(fieldListToValueEntry.getValue()));
            } else {
                query.addCriteria(Criteria.where(fieldListToValueEntry.getKey().getCode()).in(fieldListToValueEntry.getValue()));
            }
        }
        //TODO add for enabled 
        return mongoOperations.find(query, ProductAttributeUnit.class);
    }

}
