/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 03-Oct-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.mongo.mao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.snapdeal.cocofs.mongo.CocofsMongoQuery;
import com.snapdeal.cocofs.mongo.PageInfo;
import com.snapdeal.cocofs.mongo.SortCriteria;
import com.snapdeal.cocofs.mongo.mao.IGenericMao;

@Repository("GenericMaoImpl")
public class GenericMaoImpl implements IGenericMao {

    @Autowired
    @Qualifier("mongoTemplateCocofs")
    private MongoOperations mongoOperations;

    @Override
    public <T> void saveDocument(T document) {
        mongoOperations.save(document);
    }

    @Override
    public <T> void deleteDocument(T document) {
        mongoOperations.remove(document);
    }

    @Override
    public <T> List<T> getDocumentList(CocofsMongoQuery cocoQuery, Class<T> documentClass) {
        Query query = getQueryObject(cocoQuery);
        return mongoOperations.find(query, documentClass);
    }

    @Override
    public <T> T getDocument(CocofsMongoQuery cocoQuery, Class<T> documentClass) {
        Query query = getQueryObject(cocoQuery);
        return mongoOperations.findOne(query, documentClass);
    }

    public <T> Long getDocumentCount(CocofsMongoQuery cocoQuery, Class<T> documentClass) {
        Query query = getQueryObject(cocoQuery);
        return mongoOperations.count(query, documentClass);
    }

    private Query getQueryObject(CocofsMongoQuery cocoQuery) {
        Query query = new Query();
        for (com.snapdeal.cocofs.mongo.Criteria criteria : cocoQuery.getCriteria()) {
            switch (criteria.getOperator()) {
                case EQUAL:
                    query.addCriteria(Criteria.where(criteria.getKey()).is(criteria.getObj()));
                    break;
                case IN:
                    query.addCriteria(Criteria.where(criteria.getKey()).in(criteria.getObj()));
                    break;
                case GT:
                    query.addCriteria(Criteria.where(criteria.getKey()).gt(criteria.getObj()));
                    break;
                case LT:
                    query.addCriteria(Criteria.where(criteria.getKey()).lt(criteria.getObj()));
                    break;
                case GTE:
                    query.addCriteria(Criteria.where(criteria.getKey()).gte(criteria.getObj()));
                    break;
                case LTE:
                    query.addCriteria(Criteria.where(criteria.getKey()).lte(criteria.getObj()));
                    break;

            }
        }

        PageInfo pageInfo = cocoQuery.getPageInfo();

        if (pageInfo != null) {
            // requested pageNumber starts from 1, while in mongo it starts from 0
            query.skip((pageInfo.getPageNumber() - 1) * pageInfo.getSize()).limit(pageInfo.getSize());
            SortCriteria sortCriteria = pageInfo.getSortCriteria();
            if (sortCriteria != null) {
                query.with(new Sort(sortCriteria.getSortType().equals(SortCriteria.Sort.ASC) ? Sort.Direction.ASC : Sort.Direction.DESC, sortCriteria.getFieldName()));
            }
        }

        return query;
    }
}
