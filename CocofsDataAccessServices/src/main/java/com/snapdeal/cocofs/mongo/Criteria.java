package com.snapdeal.cocofs.mongo;

import com.snapdeal.cocofs.mongo.CocofsMongoQuery.Operator;

/**
 * @author nikhil
 */
public class Criteria {

    private String   key;

    private Object   obj;

    private Operator operator;

    public Criteria() {

    }

    public Criteria(String key, Object obj, Operator op) {
        this.key = key;
        this.obj = obj;
        this.operator = op;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Object getObj() {
        return obj;
    }

    public void setObj(Object obj) {
        this.obj = obj;
    }

    public Operator getOperator() {
        return operator;
    }

    public void setOperator(Operator operator) {
        this.operator = operator;
    }

}