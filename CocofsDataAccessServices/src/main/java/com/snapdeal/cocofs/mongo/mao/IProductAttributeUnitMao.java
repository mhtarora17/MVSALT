/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 25-Aug-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.mongo.mao;

import java.util.List;
import java.util.Map;

import com.snapdeal.cocofs.mongo.model.ProductAttributeUnit;
import com.snapdeal.cocofs.mongo.model.ProductAttributeUnit.PAUFieldNames;

public interface IProductAttributeUnitMao {

    public void saveProductAttributeUnit(ProductAttributeUnit pau);

    public void deleteProductAttributeUnit(ProductAttributeUnit pau);


    /**
     * Return the list ProductAttributeUnit that satisfies given criteria, Returns empty list if there is no
     * ProductAttributeUnit that can satisfy the criteria
     * @param fieldListToValueMap
     * @return
     */
    List<ProductAttributeUnit> getProductAttributeUnit(Map<PAUFieldNames, Object> fieldListToValueMap);

    /**
     * Return the ProductAttributeUnit that satisfies given criteria, expectation is that there
     * is at most one such ProductAttributeUnit.  Returns null if there is no
     * ProductAttributeUnit that can satisfy the criteria
     * @param fieldListToValueMap
     * @return
     */
    ProductAttributeUnit getUniqueProductAttributeUnit(Map<PAUFieldNames, Object> fieldListToValueMap);

}
