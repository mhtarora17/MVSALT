package com.snapdeal.cocofs.mongo;

import java.util.List;

/**
 * @author nikhil
 */
public class CocofsMongoQuery {

    public static enum Operator {
        EQUAL, IN, GT, LT, GTE, LTE;
    }

    public enum DataType {
        STRING, DOUBLE, DATE, INTEGER;
    }

    private PageInfo       pageInfo;

    private List<Criteria> criteria;

    public CocofsMongoQuery(List<Criteria> criterias, PageInfo pageInfo) {
        super();
        this.criteria = criterias;
        this.pageInfo = pageInfo;
    }

    public CocofsMongoQuery(List<Criteria> criterias) {
        super();
        this.criteria = criterias;
    }

    public PageInfo getPageInfo() {
        return pageInfo;
    }

    public void setPageInfo(PageInfo pageInfo) {
        this.pageInfo = pageInfo;
    }

    public List<Criteria> getCriteria() {
        return criteria;
    }

    public void setCriteria(List<Criteria> criteria) {
        this.criteria = criteria;
    }
}
