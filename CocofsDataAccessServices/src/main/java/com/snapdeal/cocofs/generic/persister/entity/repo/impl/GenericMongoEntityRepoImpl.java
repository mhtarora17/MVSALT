/**
 * 
 */
package com.snapdeal.cocofs.generic.persister.entity.repo.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import com.snapdeal.cocofs.generic.persister.entity.repo.IGenericEntityRepo;

/**
 * @author abhinav
 *
 */

@Repository("genericMongoEntityRepo")
public class GenericMongoEntityRepoImpl<T, ID extends Serializable> implements IGenericEntityRepo<T, ID> {

        
    @Autowired
    @Qualifier("mongoTemplateCocofs")
    private MongoTemplate mongoTemplate;
    
      
    
    @Override
    public <T> T save(T entity) {

        
        Assert.notNull(entity, "Entity must not be null!");
//        mongoTemplate.setWriteConcern(WriteConcern.ACKNOWLEDGED);
        mongoTemplate.save(entity);
        
        
        return (T)entity;
    }

    @Override
    public <T> List<T> save(Iterable<T> entities) {
        
        Assert.notNull(entities, "The given Iterable of entities not be null!");

        List<T> result = new ArrayList<T>();

        for (T entity : entities) {
            save(entity);
            result.add(entity);
        }

        return result;
    }

}
