/**
 * 
 */
package com.snapdeal.cocofs.generic.persister.exception;

/**
 * @author abhinav
 *
 */
public enum GenericPersisterExceptionCode {

    ENTITY_FAILED_TO_PERSIST("ENTITY_FAILED_TO_PERSIST"),
    DOCUMENT_FAILED_TO_PERSIST("DOCUMENT_FAILED_TO_PERSIST"),
    RECORD_FAILED_TO_PERSIST("RECORD_FAILED_TO_PERSIST"),
    UPLOAD_DATA_MODIFICATION_FAILED("UPLOAD_DATA_MODIFICATION_FAILED");
    
    private String code;

    private GenericPersisterExceptionCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
