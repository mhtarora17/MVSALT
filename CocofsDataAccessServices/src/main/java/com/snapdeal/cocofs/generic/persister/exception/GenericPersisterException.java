/**
 * 
 */
package com.snapdeal.cocofs.generic.persister.exception;

/**
 * @author abhinav
 *
 */

public class GenericPersisterException extends Exception{

    private static final long serialVersionUID = 8626388129014078476L;
    
    private GenericPersisterExceptionCode exceptionCode;

    public GenericPersisterException(){
        
    }
    
    
    public GenericPersisterException(Throwable t, String message, GenericPersisterExceptionCode exceptionCode) {
        super(message, t);
        this.exceptionCode = exceptionCode;
    }
    
    
    public GenericPersisterExceptionCode getExceptionCode() {
        return exceptionCode;
    }

    public void setExceptionCode(GenericPersisterExceptionCode exceptionCode) {
        this.exceptionCode = exceptionCode;
    }

    
    
    

}
