/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.generic.persister.entity.repo;

import java.util.List;

import com.snapdeal.score.data.aerospike.AerospikeSet;
import com.snapdeal.score.data.aerospike.exception.PutFailedException;

/**
 *  
 *  @version     1.0, 15-Sep-2014
 *  @author ankur
 */
public interface IGenericAerospikeEntityRepo <T extends AerospikeSet>  {

    

    T save(T entity) throws PutFailedException;

    List<T> save(Iterable<T> entities) throws PutFailedException;

}
