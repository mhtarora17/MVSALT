/**
 * 
 */
package com.snapdeal.cocofs.generic.persister.entity.repo;

import java.io.Serializable;
import java.util.List;

/**
 * @author abhinav
 *
 */
public interface IGenericEntityRepo<T, ID extends Serializable>  {

    <T> T save(T entity);
    
    <T> List<T> save(Iterable<T> entities);

}
