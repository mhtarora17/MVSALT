/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.generic.persister.entity.repo.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import com.snapdeal.cocofs.generic.persister.entity.repo.IGenericAerospikeEntityRepo;
import com.snapdeal.score.data.aerospike.AerospikeSet;
import com.snapdeal.score.data.aerospike.exception.PutFailedException;
import com.snapdeal.score.data.aerospike.service.impl.ScoreAerospikeService;

/**
 *  
 *  @version     1.0, 15-Sep-2014
 *  @author ankur
 */

@Repository("genericAerospikeEntityRepo")
public class GenericAerospikeEntityRepoImpl <T extends AerospikeSet> implements IGenericAerospikeEntityRepo<T> {

        
    @Autowired
    @Qualifier("aerospikeService")
    private ScoreAerospikeService   aerospikeService;
   
    
    @Override
    public  T save(T entity) throws PutFailedException {

        
        Assert.notNull(entity, "Entity must not be null!");
        aerospikeService.put(entity);
        
        
        return (T)entity;
    }

    @Override
    public  List<T> save(Iterable<T> entities) throws PutFailedException {
        
        Assert.notNull(entities, "The given Iterable of entities not be null!");

        List<T> result = new ArrayList<T>();

        for (T entity : entities) {
            save(entity);
            result.add(entity);
        }

        return result;
    }
}
