/**
 * 
 */
package com.snapdeal.cocofs.generic.persister.entity.repo.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import com.snapdeal.cocofs.generic.persister.entity.repo.IGenericEntityRepo;

/**
 * @author abhinav
 *
 */

@Repository("genericEntityRepo")
public class GenericEntityRepoImpl<T, ID extends Serializable> implements IGenericEntityRepo<T, ID> {

    private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        
        Assert.notNull(sessionFactory);
        this.sessionFactory = sessionFactory;
    }
    
    @Override
    public <T> T save(T entity) {

        Assert.notNull(entity, "Entity must not be null!");

        return (T)sessionFactory.getCurrentSession().merge(entity);
        
    }

    @Override
    public <T> List<T> save(Iterable<T> entities) {
        
        Assert.notNull(entities, "The given Iterable of entities not be null!");

        List<T> result = new ArrayList<T>();

        for (T entity : entities) {
            
            result.add(save(entity));
        }

        return result;
    }
    
}
