/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 10-Nov-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.memcache;

import net.spy.memcached.CASResponse;
import net.spy.memcached.CASValue;

import com.snapdeal.base.cache.Cachable;

public interface ICocofsMemcachedService {

    void shutdownMemcacheClient();

    /**
     * @param memcacheServerList
     * @throws Exception
     */
    public void initialize(String memcacheServerList) throws Exception;

    /**
     * @param key
     * @return Integer stored in memcache for the corresponding key.
     */
    Integer getInteger(String key);

    /**
     * @param key
     * @param value
     * @param secondToLive
     */
    void putInteger(String key, Integer value, int secondToLive);

    /**
     * @param key
     * @return if the key exists in memcache.
     */
    public boolean keyExists(String key);

    /**
     * @param key
     * @return
     */
    public Object get(String key);

    /**
     * @param key
     * @param value
     * @param secondToLive
     */
    void put(String key, Object value, int secondToLive);

    /**
     * Removes the element which matches the key.
     * <p/>
     * If no element matches, nothing is removed and no Exception is thrown.
     * 
     * @param key the key of the element to remove
     * @throws Exception
     */
    void remove(String key);

    /**
     * @param key
     * @return
     */
    CASValue<Boolean> getS(String key);

    /**
     * @param key
     * @param value
     * @param casId
     * @return
     */
    CASResponse compareAndSet(String key, Boolean value, long casId);

    /**
     * @param key
     * @param value
     * @return
     */
    Boolean add(String key, Boolean value);

    /**
     * @param key
     * @return
     */
    Boolean delete(String key);

    void put(String key, Cachable cachable, int secondToLive);

    public <T extends Cachable> T get(String key, Class<T> cachable);

}
