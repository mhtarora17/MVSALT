package com.snapdeal.cocofs.dto;

import java.io.Serializable;

import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.catalog.base.sro.ProductCategorySRO;

/**
 * @author shiv
 */
public class ProductCategoryDTO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1051203183173296213L;

    private Integer           id;

    private Integer           parentCategoryId;

    private String            parentCategoryPageUrl;

    private String            parentCategoryName;

    private String            name;

    private String            pageUrl;

    private boolean           visible;

    private boolean           subCat;

    public ProductCategoryDTO() {
    }

    public ProductCategoryDTO(ProductCategorySRO in) {
        super();
        this.id = in.getId();
        this.parentCategoryId = in.getParentCategoryId();
        this.parentCategoryPageUrl = in.getParentCategoryPageUrl();
        this.parentCategoryName = in.getParentCategoryName();
        this.name = in.getName();
        this.pageUrl = in.getPageUrl();
        this.visible = in.isVisible();

        //LOGIC -- if parentCategoryPageUrl is not null -- this is subcat URL, otherwise this is category URL. 
        if (StringUtils.isNotEmpty(parentCategoryPageUrl)) {
            this.subCat = true;
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getParentCategoryId() {
        return parentCategoryId;
    }

    public void setParentCategoryId(Integer parentCategoryId) {
        this.parentCategoryId = parentCategoryId;
    }

    public String getParentCategoryPageUrl() {
        return parentCategoryPageUrl;
    }

    public void setParentCategoryPageUrl(String parentCategoryPageUrl) {
        this.parentCategoryPageUrl = parentCategoryPageUrl;
    }

    public String getParentCategoryName() {
        return parentCategoryName;
    }

    public void setParentCategoryName(String parentCategoryName) {
        this.parentCategoryName = parentCategoryName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPageUrl() {
        return pageUrl;
    }

    public void setPageUrl(String pageUrl) {
        this.pageUrl = pageUrl;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public boolean isSubCat() {
        return subCat;
    }

    public void setSubCat(boolean subCat) {
        this.subCat = subCat;
    }

    @Override
    public String toString() {
        return "ProductCategoryDTO [id=" + id + ", parentCategoryId=" + parentCategoryId + ", parentCategoryPageUrl=" + parentCategoryPageUrl + ", parentCategoryName="
                + parentCategoryName + ", name=" + name + ", pageUrl=" + pageUrl + ", visible=" + visible + ", subCat=" + subCat + "]";
    }

}
