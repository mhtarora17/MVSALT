package com.snapdeal.cocofs.dto;

public class ProductAttributeDTO {

    private boolean serialized;

    private boolean fragile;

    private boolean liquid;

    private Integer productParts;

    private boolean hazMat;

    private Double  weight;

    private Double  length;

    private Double  breadth;

    private Double  height;
    
    private Double              primaryLength;

    private Double              primaryBreadth;

    private Double              primaryHeight;

    public boolean isSerialized() {
        return serialized;
    }

    public void setSerialized(boolean serialized) {
        this.serialized = serialized;
    }

    public boolean isFragile() {
        return fragile;
    }

    public void setFragile(boolean fragile) {
        this.fragile = fragile;
    }

    public boolean isLiquid() {
        return liquid;
    }

    public void setLiquid(boolean liquid) {
        this.liquid = liquid;
    }

    public Integer getProductParts() {
        return productParts;
    }

    public void setProductParts(Integer productParts) {
        this.productParts = productParts;
    }

    public boolean isHazMat() {
        return hazMat;
    }

    public void setHazMat(boolean hazMat) {
        this.hazMat = hazMat;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Double getLength() {
        return length;
    }

    public void setLength(Double length) {
        this.length = length;
    }

    public Double getBreadth() {
        return breadth;
    }

    public void setBreadth(Double breadth) {
        this.breadth = breadth;
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }
    
    public Double getPrimaryLength() {
        return primaryLength;
    }

    public void setPrimaryLength(Double primaryLength) {
        this.primaryLength = primaryLength;
    }

    public Double getPrimaryBreadth() {
        return primaryBreadth;
    }

    public void setPrimaryBreadth(Double primaryBreadth) {
        this.primaryBreadth = primaryBreadth;
    }

    public Double getPrimaryHeight() {
        return primaryHeight;
    }

    public void setPrimaryHeight(Double primaryHeight) {
        this.primaryHeight = primaryHeight;
    }

}

    