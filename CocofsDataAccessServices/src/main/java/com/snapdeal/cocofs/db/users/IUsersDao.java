/*
 *  Copyright 2012 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 21-Nov-2012
 *  @author prateek
 */
package com.snapdeal.cocofs.db.users;

import java.util.List;

import com.snapdeal.cocofs.entity.PasswordVerification;
import com.snapdeal.cocofs.entity.User;
import com.snapdeal.cocofs.entity.UserRole;

public interface IUsersDao {

    public User getUserById(int userId);

    public User getUserByEmail(String email);

    public void persistUser(User user);

    public User updateUser(User user);

    public boolean isUserExists(String email);

    public UserRole persistUserRole(UserRole userRole);

    void deleteUserRole(UserRole userRole);

    UserRole updateUserRole(UserRole userRole);

    List<UserRole> getAllUserRoles(User u);

    User getUserByEmailForEdit(String email);
    
    public PasswordVerification persistPasswordVerification(PasswordVerification passwordVerification);
    
    public PasswordVerification getPasswordVerification(String email);
}
