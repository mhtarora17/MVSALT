package com.snapdeal.cocofs.db.dao;

import java.util.List;

import com.snapdeal.cocofs.entity.ProductAttributeNotification;

/**
 * 
 * @author nikhil
 *
 */
public interface IProductAttributeNotificationDAO {

    List<ProductAttributeNotification> getVisibleProductAttributeNotification(String supc);
}
