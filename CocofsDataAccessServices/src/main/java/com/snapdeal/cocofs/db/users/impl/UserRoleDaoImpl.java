/*
 *  Copyright 2012 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 21-Nov-2012
 *  @author prateek
 */
package com.snapdeal.cocofs.db.users.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.snapdeal.cocofs.db.users.IUserRoleDao;
import com.snapdeal.cocofs.entity.Role;
import com.snapdeal.cocofs.entity.User;
import com.snapdeal.cocofs.entity.UserRole;

@Repository("roleDao")
public class UserRoleDaoImpl implements IUserRoleDao {

    private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Role getRoleByCode(String code) {
        Query q = sessionFactory.getCurrentSession().createQuery("from Role where code = :code");
        q.setParameter("code", code);
        return (Role) q.uniqueResult();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Role> getImmediateChildRoles(Role role) {
        Query q = sessionFactory.getCurrentSession().createQuery("from Role r where r.parentRole=:role");
        q.setParameter("role", role);
        return q.list();
    }

    @Override
    public List<UserRole> getUserRoleMappingForUser(User user) {
        Query q = sessionFactory.getCurrentSession().createQuery("from UserRole ur where ur.user =:user");
        q.setParameter("user", user);
        return q.list();
    }

    @Override
    public void deleteUserRole(List<String> roleCodes) {
        for(String roleCode : roleCodes){
            Role role = getRoleByCode(roleCode);
            Query q = sessionFactory.getCurrentSession().createQuery("delete from UserRole where role =:role");
            q.setParameter("role", role);
            q.executeUpdate();
        }
    }

    @Override
    public void deleteRole(List<String> roleCodes) {
        Query q = sessionFactory.getCurrentSession().createQuery("delete from Role where code in (:roleCodes)");
        q.setParameterList("roleCodes", roleCodes);
        q.executeUpdate();
    }
}
