/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.db.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.snapdeal.cocofs.db.dao.ITaxClassDao;
import com.snapdeal.cocofs.entity.SubcatTaxClassMapping;
import com.snapdeal.cocofs.entity.SupcTaxClassMapping;

/**
 * @version 1.0, 23-Dec-2015
 * @author nitish
 */

@Repository("TaxClassDaoImpl")
@SuppressWarnings("unchecked")
public class TaxClassDaoImpl implements ITaxClassDao {

    private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public SupcTaxClassMapping getSupcTaxClassMapping(String supc) {
        Query q = sessionFactory.getCurrentSession().createQuery("from SupcTaxClassMapping where supc = :supc ");
        q.setParameter("supc", supc);
        return (SupcTaxClassMapping) q.uniqueResult();
    }

    @Override
    public SubcatTaxClassMapping getSubcatTaxClassMapping(String subcat) {
        Query q = sessionFactory.getCurrentSession().createQuery("from SubcatTaxClassMapping where subcat = :subcat ");
        q.setParameter("subcat", subcat);
        return (SubcatTaxClassMapping) q.uniqueResult();
    }

    @Override
    public List<SupcTaxClassMapping> getAllEnabledSupcTaxClassMapping() {
        Query q = sessionFactory.getCurrentSession().createQuery("from SupcTaxClassMapping where enabled = true ");
        return q.list();
    }

    @Override
    public List<SubcatTaxClassMapping> getAllEnabledSubcatTaxClassMapping() {
        Query q = sessionFactory.getCurrentSession().createQuery("from SubcatTaxClassMapping where enabled = true ");
        return q.list();
    }

}
