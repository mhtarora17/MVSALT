/*
 *  Copyright 2012 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 21-Nov-2012
 *  @author prateek
 */
package com.snapdeal.cocofs.db.users;

import java.util.List;

import com.snapdeal.cocofs.entity.PasswordVerification;
import com.snapdeal.cocofs.entity.User;
import com.snapdeal.cocofs.entity.UserRole;

public interface IUserService {

    public boolean isUserExists(String email);

    public User addUser(User user);

    public User getUserByEmail(String email);

    public User updateUser(User user);

    public UserRole createUserRole(UserRole userRole);

    void persistUser(User user);

    boolean verifyPassword(User user, String password);

    UserRole updateUserRole(UserRole ur);

    List<UserRole> getAllUserRoleMappings(User u);

    public User getUserByEmailForEdit(String email);

    void clearEmailVerificationCode(String email);

    boolean verifyUser(User user, String code);

    public boolean isPasswordChangeAllowed(String email);

	public PasswordVerification getPasswordVerification(String email);

	public PasswordVerification createPasswordVerification(String email,
			String source, String targetUrl);

}
