/*
 *  Copyright 2012 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 21-Nov-2012
 *  @author prateek
 */
package com.snapdeal.cocofs.db.users.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.snapdeal.cocofs.db.users.IUsersDao;
import com.snapdeal.cocofs.entity.PasswordVerification;
import com.snapdeal.cocofs.entity.User;
import com.snapdeal.cocofs.entity.UserRole;

@Repository("usersDao")
@SuppressWarnings("unchecked")
public class UsersDaoImpl implements IUsersDao {

    private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public User getUserById(int userId) {
        return (User) sessionFactory.getCurrentSession().get(User.class, userId);
    }

    @Override
    public User getUserByEmail(String email) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select distinct u from User u left join fetch u.userRoleMapping urm where u.email=:email and urm.enabled = true");
        query.setParameter("email", email);
        return (User) query.uniqueResult();
    }

    @Override
    public boolean isUserExists(String email) {
        Query query = sessionFactory.getCurrentSession().createQuery("select id from User where email = :email");
        query.setParameter("email", email);
        List<Integer> users = query.list();
        if (users.size() == 1) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void persistUser(User user) {
        user.setLastUpdated(new Date());
        sessionFactory.getCurrentSession().persist(user);
    }

    @Override
    public User updateUser(User user) {
        user.setLastUpdated(new Date());
        return (User) sessionFactory.getCurrentSession().merge(user);
    }

    @Override
    public UserRole persistUserRole(UserRole userRole) {
        userRole.setLastUpdated(new Date());
        sessionFactory.getCurrentSession().persist(userRole);
        return userRole;
    }

    @Override
    public void deleteUserRole(UserRole userRole) {
        sessionFactory.getCurrentSession().delete(userRole);
    }

    @Override
    public UserRole updateUserRole(UserRole userRole) {
        return (UserRole) sessionFactory.getCurrentSession().merge(userRole);
    }

    @Override
    public List<UserRole> getAllUserRoles(User u) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from UserRole ur left join fetch ur.role pr left join fetch ur.user urs where ur.user =:user  ");
        query.setParameter("user", u);
        return query.list();

    }

    @Override
    public User getUserByEmailForEdit(String email) {
        Query query = sessionFactory.getCurrentSession().createQuery("select distinct u from User u left join fetch u.userRoleMapping urm where u.email=:email ");
        query.setParameter("email", email);
        return (User) query.uniqueResult();
    }

	@Override
	public PasswordVerification persistPasswordVerification(
			PasswordVerification passwordVerification) {
		//insert new of update already existing entry
        return (PasswordVerification)sessionFactory.getCurrentSession().merge(passwordVerification);
	}

	@Override
	public PasswordVerification getPasswordVerification(String email) {
		Query query = sessionFactory.getCurrentSession().createQuery("from PasswordVerification where email=:email");
        query.setParameter("email", email);
        return (PasswordVerification) query.uniqueResult();
	}
}
