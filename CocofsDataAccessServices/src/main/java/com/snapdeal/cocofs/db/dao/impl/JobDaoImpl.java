/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 04-Sep-2013
 *  @author himanshum
 */
package com.snapdeal.cocofs.db.dao.impl;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.cocofs.db.dao.IJobDao;
import com.snapdeal.cocofs.entity.JobDetail;
import com.snapdeal.cocofs.entity.JobDetailHistory;
import com.snapdeal.cocofs.entity.JobStatus;

@Repository("JobDaoImpl")
@SuppressWarnings("unchecked")
public class JobDaoImpl implements IJobDao {

    @Autowired
    private SessionFactory sessionFactory;

    @SuppressWarnings("unchecked")
    @Override
    public List<JobDetail> getAllJobDetails() {
        Query query = sessionFactory.getCurrentSession().createQuery("from JobDetail");
        return query.list();

    }

    @Override
    public JobDetail saveJobDetail(JobDetail jobDetail) {
        return (JobDetail) sessionFactory.getCurrentSession().merge(jobDetail);
    }

    @Override
    public JobDetailHistory saveJobDetailHistory(JobDetailHistory jobDetailHistory) {
        return (JobDetailHistory) sessionFactory.getCurrentSession().merge(jobDetailHistory);
    }

    @Override
    public void flushJobDetail(JobDetail jobDetail) {
        sessionFactory.getCurrentSession().persist(jobDetail);
        sessionFactory.getCurrentSession().flush();
    }

    @Override
    public List<JobDetail> getAllJobDetails(JobStatus status) {
        //Query query = sessionFactory.getCurrentSession().createQuery("from JobDetail where status.code=:statusCode and enabled= 1");
        /**
         * SNAPDEALTECH-34283 Order jobs by creation time so that older jobs run before relatively newer jobs
         */
        Query query = sessionFactory.getCurrentSession().createQuery("from JobDetail where status.code=:statusCode and enabled= 1 order by created asc");
        query.setParameter("statusCode", status.getCode());
        return query.list();
    }

    @Override
    public JobDetail getJobDetailByCode(String fileCode) {
        Query query = sessionFactory.getCurrentSession().createQuery("from JobDetail where fileCode=:fileCode ");
        query.setParameter("fileCode", fileCode);
        return (JobDetail) query.uniqueResult();
    }

    @Override
    public JobDetail getJobDetailByStatusCode(JobStatus status) {
        Query query = sessionFactory.getCurrentSession().createQuery("from JobDetail where status=:status");
        query.setParameter("status", status);
        return (JobDetail) query.uniqueResult();
    }

    @Override
    public List<JobDetail> getAllJobDetailsWithPagination(JobStatus status, int startRow, int count, Date startDate, Date endDate, String sortByField, String sortOrder) {
        if (StringUtils.isEmpty(sortByField)) {
            sortByField = "created";

        }
        if (StringUtils.isEmpty(sortOrder)) {
            sortOrder = " desc ";
        }
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from JobDetail where status.code=:statusCode " + " and enabled= 1 " + "and created >= :startDate and created <= :endDate order by " + sortByField + " "
                        + sortOrder);
        query.setParameter("statusCode", status.getCode());
        query.setParameter("startDate", startDate);
        query.setParameter("endDate", endDate);
        query.setMaxResults(count);
        query.setFirstResult(startRow);
        return query.list();
    }

    @Override
    public Integer getAllJobDetailsRowCountPagination(JobStatus status, Date startDate, Date endDate) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select count(id) from JobDetail where status.code=:statusCode " + " and enabled= 1 and created >= :startDate and created <= :endDate ");
        query.setParameter("statusCode", status.getCode());
        query.setParameter("startDate", startDate);
        query.setParameter("endDate", endDate);

        Long x = (Long) query.uniqueResult();
        return x.intValue();
    }

    @Override
    public Integer getMyJobDetailsRowCountPagination(JobStatus status, Date startDate, Date endDate, String user) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select count(id) from JobDetail where status.code=:statusCode " + " and enabled= 1 and created >= :startDate and created <= :endDate and uploadedBy=:user");
        query.setParameter("statusCode", status.getCode());
        query.setParameter("startDate", startDate);
        query.setParameter("endDate", endDate);
        query.setParameter("user", user);

        Long x = (Long) query.uniqueResult();
        return x.intValue();
    }

    @Override
    public List<JobDetail> getMyJobDetailsWithPagination(JobStatus status, int startRow, int count, Date startDate, Date endDate, String sortByField, String sortOrder, String user) {
        if (StringUtils.isEmpty(sortByField)) {
            sortByField = "created";

        }
        if (StringUtils.isEmpty(sortOrder)) {
            sortOrder = " desc ";
        }
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from JobDetail where status.code=:statusCode " + " and enabled= 1  and uploadedBy='" + user + "'" + "and created >= :startDate and created <= :endDate order by "
                        + sortByField + " " + sortOrder);
        query.setParameter("statusCode", status.getCode());
        query.setParameter("startDate", startDate);
        query.setParameter("endDate", endDate);
        query.setMaxResults(count);
        query.setFirstResult(startRow);
        return query.list();
    }

    @Override
    public JobStatus getJobStatusByCode(String code) {
        Query query = sessionFactory.getCurrentSession().createQuery("from JobStatus where code=:code");
        query.setParameter("code", code);
        return (JobStatus) query.uniqueResult();
    }

    @Override
    public boolean updateStatus(JobDetail jd, JobStatus js) {
        Timestamp ts = new Timestamp(new Date().getTime());
        String s = ts.toString();
        s = s.substring(0, s.length() - 2);
        Query qry = sessionFactory.getCurrentSession().createQuery("update JobDetail set status=:status,lastUpdated=:lu where id=:id");
        qry.setParameter("status", js);
        qry.setParameter("lu", ts);
        qry.setParameter("id", jd.getId());
        int res = qry.executeUpdate();
        if (res == 1)
            return true;
        return false;

    }

}
