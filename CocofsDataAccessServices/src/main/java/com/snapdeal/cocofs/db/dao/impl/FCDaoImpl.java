package com.snapdeal.cocofs.db.dao.impl;

import java.util.Date;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.snapdeal.cocofs.db.dao.IFCDao;
import com.snapdeal.cocofs.entity.FCAddress;


/**
 * DAO to add and update on fc_address table
 * 
 * @author indrajit
 *
 */


@Repository("FCDaoImpl")
@SuppressWarnings("unchecked")
public class FCDaoImpl implements IFCDao {

    private static final Logger LOG = LoggerFactory.getLogger(FCDaoImpl.class);

    @Autowired
    private SessionFactory      sessionFactory;

   

	@Override
	@Transactional
	public void saveFCDetails(FCAddress centerLocation) {
		
		sessionFactory.getCurrentSession().save(centerLocation);
		
	}

	@Override
	@Transactional
	public FCAddress persistFCDetails(FCAddress centerLocation) {
		  centerLocation.setCreated(getCreatedDate(centerLocation.getId()));
		 return (FCAddress)sessionFactory.getCurrentSession().merge(centerLocation);
	}
	
	@SuppressWarnings("unchecked")
    @Transactional(readOnly = true)
    public Date getCreatedDate(Integer id) {
		FCAddress fcAdress = null;
        Query q = sessionFactory.getCurrentSession().createQuery("FROM FCAddress where id = :id");
        q.setParameter("id", id);
        fcAdress = (FCAddress) q.uniqueResult();
        return fcAdress.getCreated();
    }

    
}
