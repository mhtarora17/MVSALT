/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.db.rules.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.snapdeal.cocofs.db.dao.ICocofsRuleDao;
import com.snapdeal.cocofs.db.rules.IRuleDBReadService;
import com.snapdeal.rule.engine.entity.Rule;

/**
 *  
 *  @version     1.0, 08-Dec-2014
 *  @author ankur
 */

@Service("ruleDBReadServiceImpl")
@Transactional(readOnly = true)
public class RuleDBReadServiceImpl implements IRuleDBReadService{

    @Autowired
    ICocofsRuleDao ruleDao;
    
    @Override
    public List<Rule> getEnabledAndSynchedRulesByBlockId(Integer blockId){
        return ruleDao.getEnabledAndSynchedRulesByBlockId(blockId);
    }
}
