/*
 *  Copyright 2012 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 22-Nov-2012
 *  @author prateek
 */
package com.snapdeal.cocofs.db.accesscontrol.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.snapdeal.cocofs.db.accesscontrol.IAccessControlDao;
import com.snapdeal.cocofs.entity.AccessControl;

@Repository("access_control")
@SuppressWarnings("unchecked")
public class AccessControlDAOImpl implements  IAccessControlDao{    
    private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       

    @Override
    public List<AccessControl> getAllAccessControls() {
        Query query = sessionFactory.getCurrentSession().createQuery("from AccessControl");
        return query.list();
    }

    @Override
    public AccessControl getAccessControlByPattern(String uri) {
        Query query = sessionFactory.getCurrentSession().createQuery("from AccessControl where urlPrefix=:uri");
        query.setParameter("uri", uri);
        return (AccessControl) query.uniqueResult();
    }

}
