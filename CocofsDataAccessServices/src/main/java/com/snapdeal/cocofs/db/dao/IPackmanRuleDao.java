/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.db.dao;

import java.util.List;

import com.snapdeal.rule.engine.entity.Rule;

/**
 *  
 *  @version     1.0, 27-Dec-2015
 *  @author brijesh
 */
public interface IPackmanRuleDao {

    List<Rule> getAllRuleForStore(String name,String value);
    
    List<Rule> getAllRuleByRuleParams(String paramName1,String paramValue1,String paramName2,String paramValue2);
  
    List<Rule> getRuleByName(String name,String param,String value);
    
    List<Rule> getRuleByRuleParamsAndBlockId(String name,String value,int blockId);
}
