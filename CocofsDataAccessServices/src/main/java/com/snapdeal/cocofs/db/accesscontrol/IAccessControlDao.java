/*
 *  Copyright 2012 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 22-Nov-2012
 *  @author prateek
 */
package com.snapdeal.cocofs.db.accesscontrol;

import java.util.List;

import com.snapdeal.cocofs.entity.AccessControl;

public interface IAccessControlDao {

    public List<AccessControl> getAllAccessControls();

    public AccessControl getAccessControlByPattern(String uRI);

}
