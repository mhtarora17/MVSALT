/*
 *  Copyright 2012 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 22-Nov-2012
 *  @author prateek
 */
package com.snapdeal.cocofs.db.dao;

import java.util.Date;
import java.util.List;

import com.snapdeal.cocofs.entity.JobDetail;
import com.snapdeal.cocofs.entity.JobDetailHistory;
import com.snapdeal.cocofs.entity.JobStatus;


public interface IJobDao {

    /**
     * Get all entries in JobDetails irrespective of status
     * @return
     */
    List<JobDetail> getAllJobDetails();

    /**
     * Save given jobDetail
     * @param jobDetail
     * @return
     */
    JobDetail saveJobDetail(JobDetail jobDetail);

    /**
     * record job detail update history
     * @param jobDetailHistory
     * @return
     */
    JobDetailHistory saveJobDetailHistory(JobDetailHistory jobDetailHistory);

    /**
     * Flush the job detail to db, to make sure we have an id
     * @param jobDetail
     */
    void flushJobDetail(JobDetail jobDetail);

    /**
     * Get all Job details in given status
     * @param status
     * @return
     */
    List<JobDetail> getAllJobDetails(JobStatus status);

    /**
     * Get JobDetail with given file/job code
     * @param fileCode
     * @return
     */
    JobDetail getJobDetailByCode(String fileCode);

    Integer getAllJobDetailsRowCountPagination(JobStatus status, Date startDate, Date endDate);

    /**
     * Returns Job Detail created in date range (end date inclusiv) with given date, sorted by given JobDetail field in given 
     * order ( asc or desc )
     * If sortByField is not specified (it is null or empty string) 'created' is used for sorting
     * If sortOrder not specified (it is null or empty string) 'desc' is used for sorting
     * @param status
     * @param startRow
     * @param count
     * @param startDate
     * @param endDate
     * @param sortByField
     * @param sortOrder
     * @return
     */
    List<JobDetail> getAllJobDetailsWithPagination(JobStatus status, int startRow, int count, Date startDate, Date endDate, String sortByField, String sortOrder );

    
    //Suryansh
    /*
     * Returns count for jobs of a particular user
     */
	Integer getMyJobDetailsRowCountPagination(JobStatus status, Date startDate,Date endDate,String user);

	   /**
     * Returns Job Detail for a particular user created in date range (end date inclusive) with given date, sorted by given JobDetail field in given 
     * order ( asc or desc )
     * If sortByField is not specified (it is null or empty string) 'created' is used for sorting
     * If sortOrder not specified (it is null or empty string) 'desc' is used for sorting
     * @param status
     * @param startRow
     * @param count
     * @param startDate
     * @param endDate
     * @param sortByField
     * @param sortOrder
     * @return
     */
	List<JobDetail> getMyJobDetailsWithPagination(JobStatus status,	int startRow, int count, Date startDate, Date endDate,String sortByField, String sortOrder, String user);

	/*
	 * Update status of job from VALD to CANC or HOLD
	 */
		boolean updateStatus(JobDetail jd,JobStatus js);

		JobStatus getJobStatusByCode(String code);

		JobDetail getJobDetailByStatusCode(JobStatus status);

}
