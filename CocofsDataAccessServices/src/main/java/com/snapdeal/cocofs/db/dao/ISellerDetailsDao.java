/*
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 15-Jun-2015
 *  @author Gaurav
 */
package com.snapdeal.cocofs.db.dao;

import com.snapdeal.cocofs.entity.SellerContactDetails;

public interface ISellerDetailsDao {

	public SellerContactDetails createUpdateSellerContactDetails(SellerContactDetails scd);
	
	public SellerContactDetails getSellerContactDetailsBySeller(String sellerCode);
	
}

