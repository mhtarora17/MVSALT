package com.snapdeal.cocofs.db.dao;

import java.util.List;

import com.snapdeal.cocofs.entity.EventInstance;

public interface IEventHandlerDao {

    List<EventInstance> getAllEventInstances(boolean overrideEnable);

    EventInstance save(EventInstance eventInstance);

    EventInstance getEventInstanceById(Integer id);

    List<EventInstance> getAllEventsToExecute(boolean overrideEnable);
}
