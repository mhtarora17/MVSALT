/*
 *  Copyright 2010 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Aug 22, 2010
 *  @author singla
 */
package com.snapdeal.cocofs.db.users.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.base.utils.ValidatorUtils;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.db.users.IUserRoleService;
import com.snapdeal.cocofs.db.users.IUserService;
import com.snapdeal.cocofs.db.users.IUsersDao;
import com.snapdeal.cocofs.entity.PasswordVerification;
import com.snapdeal.cocofs.entity.User;
import com.snapdeal.cocofs.entity.UserRole;
import com.snapdeal.cocofs.utils.EncryptionUtils;

@Transactional
@Service("userService")
public class UserServiceImpl implements IUserService {

    @Autowired
    private IUsersDao               usersDao;

    @Autowired
    private IUserRoleService        userRoleService;

    private static final Logger     LOG = LoggerFactory.getLogger(UserServiceImpl.class);

    @Override
    public boolean isUserExists(String email) {
        return usersDao.isUserExists(email);
    }

    @Override
    public User addUser(User user) {
        user.setLastUpdated(DateUtils.getCurrentTime());
        user.setCreated(DateUtils.getCurrentTime());
        return usersDao.updateUser(user);
    }

    @Override
    public User getUserByEmail(String email) {
        return usersDao.getUserByEmail(email);
    }

    @Override
    public User updateUser(User user) {
        user.setLastUpdated(DateUtils.getCurrentTime());
        return usersDao.updateUser(user);
    }

    @Override
    public UserRole createUserRole(UserRole userRole) {
        return usersDao.persistUserRole(userRole);
    }

    @Override
    public void persistUser(User user) {
        user.setLastUpdated(DateUtils.getCurrentTime());
        user.setCreated(DateUtils.getCurrentTime());
        usersDao.persistUser(user);
    }

    @Override
    public UserRole updateUserRole(UserRole ur) {
        if (ur != null) {
            return usersDao.updateUserRole(ur);
        }
        return null;
    }

    @Override
    public List<UserRole> getAllUserRoleMappings(User u) {
        return usersDao.getAllUserRoles(u);
    }

    @Override
    public User getUserByEmailForEdit(String email) {
        return usersDao.getUserByEmailForEdit(email);
    }

    @Override
    public boolean verifyPassword(User user, String password) {
        if (user != null) {
            String userPassword = user.getPassword();
            String encryptedInputPwd = EncryptionUtils.getMD5EncodedPassword(password);
            if (StringUtils.isNotEmpty(userPassword) && StringUtils.isNotEmpty(encryptedInputPwd)) {
                return encryptedInputPwd.equalsIgnoreCase(userPassword);
            }
        } else {
            LOG.warn("User is found to be null");
            return false;
        }
        return false;
    }

    @Override
    public void clearEmailVerificationCode(String email) {
        try {
            //update ttl to 0 --> expire the entry in db
        	PasswordVerification passwordVerification = getPasswordVerification(email);
            passwordVerification.setTtl(0);
            usersDao.persistPasswordVerification(passwordVerification);
        } catch (Exception e) {
            LOG.error("Exception ", e);
        }
    }

    @Override
    public boolean verifyUser(User user, String code) {
        if (user != null) {
        	//SNAPDEALTECH-34283 - Verify from mysql instead of memcache
        	PasswordVerification passwordVerification = getPasswordVerification(user.getEmail());
            if (passwordVerification != null && passwordVerification.getVerificationCode().equals(code)) {
            	//validate if the the code is within the TTL..
            	long createdSeconds = passwordVerification.getCreated().getTime()/1000;
            	long currentSeconds = System.currentTimeMillis()/1000;
            	if(createdSeconds + passwordVerification.getTtl() < currentSeconds){
            		//the code has expired
            		LOG.info("The password verification has expired. It was created at "+passwordVerification.getCreated());
            		return false;
            	}
                return true;
            }
        } else {
            LOG.warn("User is found to be null");
            return false;
        }
        return false;
    }

    @Override
    public boolean isPasswordChangeAllowed(String email) {
        if (StringUtils.isEmpty(email)) {
            return false;
        } else if (!ValidatorUtils.isEmailPatternValid(email)) {
            return false;
        } else {
            return true;
        }

    }

    @Override
    public PasswordVerification createPasswordVerification(String email, String source, String targetUrl) {
    	/**
         * SNAPDEALTECH-34338 Use Mysql instead of memcache for forgot password
         */
    	PasswordVerification passwordVerification = getPasswordVerification(email);
    	if(passwordVerification == null){
    		passwordVerification = new PasswordVerification();
    	}
        
        passwordVerification.setEmail(email);
        passwordVerification.setTtl(ConfigUtils.getIntegerScalar(Property.FORGOT_PASSWORD_TTL));
	
	    passwordVerification.setVerificationCode(StringUtils.getRandom());
        passwordVerification.setCreated(DateUtils.getCurrentTime());
        return usersDao.persistPasswordVerification(passwordVerification);
    }

	@Override
	public PasswordVerification getPasswordVerification(String email) {
		return usersDao.getPasswordVerification(email);
	}
	
	
}
