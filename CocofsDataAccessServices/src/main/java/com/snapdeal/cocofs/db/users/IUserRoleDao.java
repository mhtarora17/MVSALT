/*
 *  Copyright 2012 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 21-Nov-2012
 *  @author prateek
 */
package com.snapdeal.cocofs.db.users;

import java.util.List;

import com.snapdeal.cocofs.entity.Role;
import com.snapdeal.cocofs.entity.User;
import com.snapdeal.cocofs.entity.UserRole;

public interface IUserRoleDao {

    Role getRoleByCode(String code);
	
    List<Role> getImmediateChildRoles(Role role);

    List<UserRole> getUserRoleMappingForUser(User user);

    void deleteUserRole(List<String> roleCodes);

    void deleteRole(List<String> roleCodes);
}
