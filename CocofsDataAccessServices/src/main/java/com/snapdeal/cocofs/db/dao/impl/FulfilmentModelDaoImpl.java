/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 30-Sep-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.db.dao.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.snapdeal.cocofs.db.dao.IFulfilmentModelDao;
import com.snapdeal.cocofs.entity.SellerFMMapping;
import com.snapdeal.cocofs.entity.SellerSubcatFMMapping;
import com.snapdeal.cocofs.entity.SellerSupcFMMapping;
import com.snapdeal.cocofs.entity.SellerSupcMapping;

@Repository("FulfilmentModelDaoImpl")
@SuppressWarnings("unchecked")
public class FulfilmentModelDaoImpl implements IFulfilmentModelDao {

    private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public SellerFMMapping getEnabledSellerFMMapping(String sellerCode) {
        Query q = sessionFactory.getCurrentSession().createQuery("from SellerFMMapping where sellerCode = :sellerCode and enabled = true");
        q.setParameter("sellerCode", sellerCode);
        return (SellerFMMapping) q.uniqueResult();
    }

    @Override
    public SellerSubcatFMMapping getEnabledSellerSubcatFMMapping(String sellerCode, String subcat) {
        Query q = sessionFactory.getCurrentSession().createQuery("from SellerSubcatFMMapping where sellerCode = :sellerCode and subcat = :subcat and enabled = true");
        q.setParameter("sellerCode", sellerCode);
        q.setParameter("subcat", subcat);
        return (SellerSubcatFMMapping) q.uniqueResult();
    }

    @Override
    public SellerSupcFMMapping getEnabledSellerSupcFMMapping(String sellerCode, String supc) {
        Query q = sessionFactory.getCurrentSession().createQuery("from SellerSupcFMMapping where sellerCode = :sellerCode and supc = :supc and enabled = true");
        q.setParameter("sellerCode", sellerCode);
        q.setParameter("supc", supc);
        return (SellerSupcFMMapping) q.uniqueResult();
    }

    @Override
    public SellerFMMapping updateSellerFMMapping(SellerFMMapping mapping) {
        return (SellerFMMapping) sessionFactory.getCurrentSession().merge(mapping);
    }

    @Override
    public SellerFMMapping getSellerFMMapping(String sellerCode) {
        Query q = sessionFactory.getCurrentSession().createQuery("from SellerFMMapping where sellerCode = :sellerCode ");
        q.setParameter("sellerCode", sellerCode);
        return (SellerFMMapping) q.uniqueResult();
    }

    @Override
    public SellerSubcatFMMapping getSellerSubcatFMMapping(String sellerCode, String subcat) {
        Query q = sessionFactory.getCurrentSession().createQuery("from SellerSubcatFMMapping where sellerCode = :sellerCode and subcat = :subcat ");
        q.setParameter("sellerCode", sellerCode);
        q.setParameter("subcat", subcat);
        return (SellerSubcatFMMapping) q.uniqueResult();
    }

    @Override
    public SellerSupcFMMapping getSellerSupcFMMapping(String sellerCode, String supc) {
        Query q = sessionFactory.getCurrentSession().createQuery("from SellerSupcFMMapping where sellerCode = :sellerCode and supc = :supc ");
        q.setParameter("sellerCode", sellerCode);
        q.setParameter("supc", supc);
        return (SellerSupcFMMapping) q.uniqueResult();
    }

    @Override
    public SellerSubcatFMMapping updateSellerSubcatFMMapping(SellerSubcatFMMapping mapping) {
        return (SellerSubcatFMMapping) sessionFactory.getCurrentSession().merge(mapping);
    }

    @Override
    public SellerSupcFMMapping updateSellerSupcFMMapping(SellerSupcFMMapping mapping) {
        return (SellerSupcFMMapping) sessionFactory.getCurrentSession().merge(mapping);
    }

    @Override
    public List<SellerSubcatFMMapping> getSellerSubcatFMMapping(String sellerCode) {
        Query q = sessionFactory.getCurrentSession().createQuery("from SellerSubcatFMMapping where sellerCode = :sellerCode and enabled = true");
        q.setParameter("sellerCode", sellerCode);
        return q.list();
    }

    @Override
    public List<String> getSellersWithExceptionAtSupcLevel() {
        Query q = sessionFactory.getCurrentSession().createQuery("select distinct sellerCode from SellerSupcFMMapping where enabled = true");
        return q.list();
    }

    @Override
    public List<SellerFMMapping> getAllSellerFMAndFCMapping() {
        
        Query q = sessionFactory.getCurrentSession().createQuery("select distinct sfm from SellerFMMapping sfm left join fetch sfm.fcCenters sfc  where  sfm.enabled = true  ");
        return q.list();
    }

    @Override
    public List<SellerFMMapping> getAllSellerFMAndFCMappingByLastUpdated(Date lastUpdated) {
        Query q = sessionFactory.getCurrentSession().createQuery("select distinct sfm from SellerFMMapping sfm left join fetch sfm.fcCenters sfc where   (sfm.lastUpdated >= :lastUpdated or sfc.lastUpdated >= :lastUpdated)");
        q.setParameter("lastUpdated", lastUpdated);
        return q.list();
    }

    @Override
    public List<SellerSupcFMMapping> getEnabledSellerSupcFMMapping(String sellerCode) {
        Query q = sessionFactory.getCurrentSession().createQuery("from SellerSupcFMMapping where sellerCode = :sellerCode and enabled = true");
        q.setParameter("sellerCode", sellerCode);
        return q.list();
    }

    @Override
    public List<String> getEnabledSupcExceptionListForSeller(String sellerCode) {
        Query q = sessionFactory.getCurrentSession().createQuery("select supc from SellerSupcFMMapping where sellerCode = :sellerCode and enabled = true");
        q.setParameter("sellerCode", sellerCode);
        return q.list();
    }

    @Override
    public List<String> getSupcListForSeller(String sellerCode) {
        Query q = sessionFactory.getCurrentSession().createQuery("select supc from SellerSupcMapping where sellerCode = :sellerCode");
        q.setParameter("sellerCode", sellerCode);
        return q.list();
    }

    @Override
    public long getEnabledSellerSupcFMMappingCount(String sellerCode) {
        Query q = sessionFactory.getCurrentSession().createQuery("select count(*) from SellerSupcFMMapping where sellerCode = :sellerCode and enabled = true");
        q.setParameter("sellerCode", sellerCode);
        return (Long) q.uniqueResult();
    }

    @Override
    public Long getSellerFMMappingCountByLastUpdated(Date lastUpdated) {
        Query q = sessionFactory.getCurrentSession().createSQLQuery("select count(*) as seller_count from seller_fm_mapping where   last_updated > :lastUpdated").addScalar(
                "seller_count", Hibernate.LONG);
        q.setParameter("lastUpdated", lastUpdated);
        return (Long) q.uniqueResult();

    }

    @Override
    public Long getSellerSupcFMMappingCountByLastUpdated(Date lastUpdated) {
        Query q = sessionFactory.getCurrentSession().createSQLQuery("select count(*) as seller_supc_count from seller_supc_fm_mapping where   last_updated > :lastUpdated").addScalar(
                "seller_supc_count", Hibernate.LONG);
        q.setParameter("lastUpdated", lastUpdated);
        return (Long) q.uniqueResult();

    }

    @Override
    public List<SellerFMMapping> getAllSellerFMMappingByLastUpdated(Date lastUpdated) {
        Query q = sessionFactory.getCurrentSession().createQuery("from SellerFMMapping where   lastUpdated > :lastUpdated");
        q.setParameter("lastUpdated", lastUpdated);
        return q.list();

    }

    @Override
    public List<SellerSupcFMMapping> getAllSellerSupcFMMappingByLastUpdated(Date lastUpdated) {
        Query q = sessionFactory.getCurrentSession().createQuery("from SellerSupcFMMapping where  lastUpdated > :lastUpdated");
        q.setParameter("lastUpdated", lastUpdated);
        return q.list();
    }

	@Override
	public List<SellerSupcMapping> getSellerSupcMappingList(String sellerCode) {
		Query q = sessionFactory.getCurrentSession().createQuery("from SellerSupcMapping where  sellerCode = :sellerCode");
        q.setParameter("sellerCode", sellerCode);
        return q.list();
	}

	@Override
	public SellerSupcMapping getSellerSupcMapping(String sellerCode, String supc) {
		Query q = sessionFactory.getCurrentSession().createQuery("from SellerSupcMapping where  sellerCode = :sellerCode and supc = :supc");
        q.setParameter("sellerCode", sellerCode);
        q.setParameter("supc", supc);
        return (SellerSupcMapping)q.uniqueResult();
	}

	@Override
	public List<String> getEnabledSellerListAtSellerLevelForFulfillmentModel(String fulfillmentModel) {
		Query q = sessionFactory.getCurrentSession().createQuery("select sellerCode from SellerFMMapping where fulfilmentModel = :fulfillmentModel and enabled = true");
		q.setParameter("fulfillmentModel", fulfillmentModel);
        return q.list();
	}

	@Override
	public List<String> getEnabledSellerListAtSupcLevelForFulfillmentModel(String fulfillmentModel) {
		Query q = sessionFactory.getCurrentSession().createQuery("select distinct sellerCode from SellerSupcFMMapping where fulfilmentModel = :fulfillmentModel and enabled = true");
		q.setParameter("fulfillmentModel", fulfillmentModel);
		return q.list();
	}

	@Override
	public List<String> getEnabledSellerListAtSubcatLevelForFulfillmentModel(String fulfillmentModel) {
		Query q = sessionFactory.getCurrentSession().createQuery("select distinct sellerCode from SellerSubcatFMMapping where fulfilmentModel = :fulfillmentModel and enabled = true");
		q.setParameter("fulfillmentModel", fulfillmentModel);
		return q.list();
	}
	
	@Override
    public List<String> getEnabledSellerListAtSellerLevelForFCcode(String fcCode) {
        Query q = sessionFactory.getCurrentSession().createQuery("select distinct sellerFMMapping.sellerCode from SellerFCCodeMapping where fcCode = :fcCode and enabled = true");
        q.setParameter("fcCode", fcCode);
        return q.list();
    }
	
	
	
	@Override
    public List<SellerSupcFMMapping> getEnabledSellerSupcMappingListForFcCode(String fcCode) {
        Query q = sessionFactory.getCurrentSession().createQuery("select  ssf from SellerSupcFCCodeMapping scc inner join scc.sellerSupcFMMapping ssf where scc.fcCode = :fcCode and scc.enabled = true");
        q.setParameter("fcCode", fcCode);
        return q.list();
    }
	
	
}
