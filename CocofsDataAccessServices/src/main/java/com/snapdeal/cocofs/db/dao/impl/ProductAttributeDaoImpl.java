/*
 *  Copyright 2012 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 28-May-2012
 *  @author amd
 */
package com.snapdeal.cocofs.db.dao.impl;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.snapdeal.cocofs.db.dao.IProductAttributeDao;
import com.snapdeal.cocofs.entity.ProductAttribute;
import com.snapdeal.cocofs.entity.ProductAttributeHistory;

@Repository("productDao")
@SuppressWarnings("unchecked")
public class ProductAttributeDaoImpl implements IProductAttributeDao {

    private static final Comparator<? super ProductAttributeHistory> historyComparator = new Comparator<ProductAttributeHistory>() {

        @Override
        public int compare(ProductAttributeHistory o1, ProductAttributeHistory o2) {
            return o2.getCreated().compareTo(o1.getCreated());
        }
        
    };
    
    @Autowired
    private SessionFactory sessionFactory;
    
    

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    @Override
    public ProductAttribute getProductAttributeForSUPC(String supc, String attribute ) {
        Query query = sessionFactory.getCurrentSession().createQuery("from ProductAttribute WHERE " +
        		" supc = :supc and attribute= :attribute and enabled = true ");
        query.setParameter("attribute", attribute);
        query.setParameter("supc", supc);
        return (ProductAttribute) query.uniqueResult();
        
    }
    
    @Override
    public List<ProductAttribute> getAllProductAttributeForSUPC(String supc) {
        Query query = sessionFactory.getCurrentSession().createQuery("from ProductAttribute WHERE " +
                " supc = :supc  and enabled = true ");
        query.setParameter("supc", supc);
        return query.list();
        
    }
    
    @Override
    public List<ProductAttributeHistory> getProductAttributeHistoryForSUPC(String supc, String attribute ) {
        Query query = sessionFactory.getCurrentSession().createQuery("from ProductAttributeHistory WHERE " +
                " supc = :supc and attribute= :attribute ");
        query.setParameter("attribute", attribute);
        query.setParameter("supc", supc);
        List<ProductAttributeHistory> outlist = (List<ProductAttributeHistory>) query.list();
        Collections.sort(outlist, historyComparator);
        return outlist;
        
    }
    
    @Override
    public List<ProductAttributeHistory> getAllProductAttributeHistoryForSUPC(String supc ) {
        Query query = sessionFactory.getCurrentSession().createQuery("from ProductAttributeHistory WHERE " +
                " supc = :supc  ");
        query.setParameter("supc", supc);
        List<ProductAttributeHistory> outlist = (List<ProductAttributeHistory>) query.list();
        Collections.sort(outlist, historyComparator);
        return outlist;
        
    }
    
    @Override
    public List<ProductAttribute> getAllProductAttributeFor(int firstResult, int maxResults) {
        Query query = sessionFactory.getCurrentSession().createQuery("from ProductAttribute WHERE " +
                " enabled = true ORDER BY supc ");
        query.setFirstResult(firstResult);
        query.setMaxResults(maxResults);
        return query.list();
        
    }
    
    @Override
    public ProductAttribute merge(ProductAttribute pa, ProductAttributeHistory pah) {
        
        
        sessionFactory.getCurrentSession().merge(pah);
        return (ProductAttribute) sessionFactory.getCurrentSession().merge(pa);
    }



   
}
