package com.snapdeal.cocofs.db.dao;

import java.util.Date;
import java.util.List;

import com.snapdeal.cocofs.entity.PendingProductAttributeUpdate;

public interface IPendingProductAttributeDAO {

    List<PendingProductAttributeUpdate> getPendingProductAttribute(String supc);

    List<PendingProductAttributeUpdate> getPlayablePendingProductAttribute(String supc, Date playDate);

    List<PendingProductAttributeUpdate> getPlayablePendingProductAttributeByDateRange(Date startDate, Date endDate);

    void merge(PendingProductAttributeUpdate pendingProductAttributeUpdate);
    
    public List<PendingProductAttributeUpdate> getProductSystemWeightCapturedFlag(String supc);

}
