package com.snapdeal.cocofs.db.dao;


import com.snapdeal.cocofs.entity.FCAddress;


public interface IFCDao {

    void saveFCDetails(FCAddress centerLocation);
    FCAddress persistFCDetails(FCAddress centerLocation);

    

}
