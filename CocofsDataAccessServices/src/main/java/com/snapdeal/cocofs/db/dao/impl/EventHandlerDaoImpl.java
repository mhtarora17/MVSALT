package com.snapdeal.cocofs.db.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.db.dao.IEventHandlerDao;
import com.snapdeal.cocofs.entity.EventInstance;

@Repository("EventHandlerDao")
@Transactional
public class EventHandlerDaoImpl implements IEventHandlerDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<EventInstance> getAllEventInstances(boolean overrideEnable) {
        String hql = "from EventInstance where enabled=true";
        if (overrideEnable) {//No need to check enablement
            hql = "from EventInstance";
        }
        Query query = sessionFactory.getCurrentSession().createQuery(hql);
        int maxResults = ConfigUtils.getIntegerScalar(Property.EVENT_INSTANCE_DB_RECORD_FETCH_LIMIT);
        query.setMaxResults(maxResults);
        return query.list();
    }

    @Override
    public EventInstance save(EventInstance eventInstance) {
        eventInstance.setUpdated(DateUtils.getCurrentTime());
        return (EventInstance) sessionFactory.getCurrentSession().merge(eventInstance);
    }

    @Override
    public EventInstance getEventInstanceById(Integer id) {
        Query query = sessionFactory.getCurrentSession().createQuery("from EventInstance where id=:id");
        query.setParameter("id", id);
        return (EventInstance) query.uniqueResult();
    }

    @Override
    public List<EventInstance> getAllEventsToExecute(boolean overrideEnable) {
        String hql = "from EventInstance where enabled=true and executed=false and retry_count<:maxRetry";
        if (overrideEnable) {//No need to check enablement
            hql = "from EventInstance where executed=false";
        }
        Query query = sessionFactory.getCurrentSession().createQuery(hql);
        if (!overrideEnable) {
            int maxRetry = ConfigUtils.getIntegerScalar(Property.MAX_EVENT_RETRY_COUNT);
            query.setParameter("maxRetry", maxRetry);
        }
        return query.list();
    }

}