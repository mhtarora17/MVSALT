/*
 *  Copyright 2012 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 22-Nov-2012
 *  @author prateek
 */
package com.snapdeal.cocofs.db.accesscontrol.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.snapdeal.base.cache.CacheManager;
import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.cocofs.cache.AccessControlCache;
import com.snapdeal.cocofs.db.accesscontrol.IAccessControlDao;
import com.snapdeal.cocofs.db.accesscontrol.IAccessControlService;
import com.snapdeal.cocofs.entity.AccessControl;

@Service("accessControlService")
public class AccessControlServiceImpl implements IAccessControlService {

    @Autowired
    private IAccessControlDao accessControlDao;

    @Override
    @Transactional
    public List<String> getRoleCodesByPattern(String URI) {
        AccessControl accessControls = CacheManager.getInstance().getCache(AccessControlCache.class).getAccessControl(URI);
        if (accessControls == null) {
            accessControls = accessControlDao.getAccessControlByPattern(URI);
        }
        if(accessControls != null){
            return StringUtils.split(accessControls.getCsvRoleCodes(), ",");
        }
        return new ArrayList<String>();
    }
    
    @Override
    public List<AccessControl> getAllAccessControlsFromDB() {
        return accessControlDao.getAllAccessControls();
    }

}
