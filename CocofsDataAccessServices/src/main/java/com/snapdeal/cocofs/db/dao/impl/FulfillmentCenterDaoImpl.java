/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 11-Jul-2013
 *  @author prateek
 */
package com.snapdeal.cocofs.db.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.cocofs.db.dao.IFulfillmentCenterDao;
import com.snapdeal.cocofs.entity.FulfillmentCentre;

/**
 * add and search from fc_details table
 * 
 * @author indrajit
 */
@Repository("fulfillmentCenterDaoImpl")
public class FulfillmentCenterDaoImpl implements IFulfillmentCenterDao {

    @Autowired
    private SessionFactory sessionFactory;

    @SuppressWarnings("unchecked")
    @Override
    @Transactional
    public List<FulfillmentCentre> getAllFulfillmentCenter() {
        Query q = sessionFactory.getCurrentSession().createQuery("FROM FulfillmentCentre where enabled=1");
        return q.list();
    }

    @SuppressWarnings("unchecked")
    @Override
    @Transactional(readOnly = true)
    public List<FulfillmentCentre> getAllFulfillmentCenterByType(String centerType) {
        Query q = sessionFactory.getCurrentSession().createQuery("FROM FulfillmentCentre where type =:centerType and enabled=1");
        q.setParameter("centerType", centerType);
        return q.list();
    }

    @Override
    @Transactional(readOnly = true)
    public FulfillmentCentre getFulfillmentCenterForFCCode(String fcCode) {
        Query q = sessionFactory.getCurrentSession().createQuery("FROM FulfillmentCentre where enabled=1 and " + " code = :fcCode");
        q.setParameter("fcCode", fcCode);
        return (FulfillmentCentre) q.uniqueResult();
    }

    
    @Override
    @Transactional
    public FulfillmentCentre updateCenter(FulfillmentCentre center) {
        center.setLastUpdated(DateUtils.getCurrentTime());
        return (FulfillmentCentre) sessionFactory.getCurrentSession().merge(center);
    }

    @Override
    @Transactional
    @SuppressWarnings("unchecked")
    public Integer updateFCName(String fcCode, String centerName, String username) {
        Query q = sessionFactory.getCurrentSession().createQuery("update FulfillmentCentre set name = :centerName, lastUpdated = :lastupdated, updatedBy =:username where code = :fcCode");
        q.setParameter("fcCode", fcCode);
        q.setParameter("centerName", centerName);
        q.setParameter("lastupdated", DateUtils.getCurrentTime());
        q.setParameter("username",username);
        return q.executeUpdate();
    }

}
