/*
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 15-Jun-2015
 *  @author Gaurav
 */
package com.snapdeal.cocofs.db.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.cocofs.db.dao.ISellerZoneDao;
import com.snapdeal.cocofs.entity.SellerZoneMapping;

@Repository("sellerZoneDaoImpl")
public class SellerZoneDaoImpl implements ISellerZoneDao {

    @Autowired
    private SessionFactory sessionFactory;

	@Override
	@Transactional(readOnly = true)
	public List<SellerZoneMapping> getAllSellerZoneMappings() {
		Query q = sessionFactory.getCurrentSession().createQuery("FROM SellerZoneMapping");
        return q.list();
	}
	
	@SuppressWarnings("unchecked")
    @Override
    @Transactional(readOnly = true)
    public List<SellerZoneMapping> getAllSellerZoneMappingsByPinCode(String pincode) {
	    SQLQuery q = sessionFactory.getCurrentSession().createSQLQuery("select distinct {sz.*} from seller_zone_mapping sz inner join seller_contact_details sc on sc.seller_code=sz.seller_code where sc.pin_code = :pincode");
        q.addEntity("sz", SellerZoneMapping.class);
        q.setString("pincode", pincode);
        
        return q.list();
    }

	@Override
	@Transactional(readOnly = true)
	public SellerZoneMapping getSellerZoneMappingBySeller(String sellerCode) {
		Query q = sessionFactory.getCurrentSession().createQuery("FROM SellerZoneMapping where sellerCode = :sellerCode");
        q.setParameter("sellerCode", sellerCode);
        return (SellerZoneMapping) q.uniqueResult();
	}

	@Override
	public SellerZoneMapping persistSellerZoneMapping(
			SellerZoneMapping sellerZoneMapping) {
		sellerZoneMapping.setLastUpdated(DateUtils.getCurrentTime());
		return (SellerZoneMapping) sessionFactory.getCurrentSession().merge(sellerZoneMapping);
	}
}

