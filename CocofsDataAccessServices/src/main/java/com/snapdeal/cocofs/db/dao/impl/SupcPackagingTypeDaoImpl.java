/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.db.dao.impl;


import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.snapdeal.cocofs.db.dao.ISupcPackagingTypeDao;
import com.snapdeal.cocofs.entity.SupcPackagingTypeMapping;



/**
 *  
 *  @version     1.0, 15-Dec-2015
 *  @author vikassharma03
 */

@Repository("SupcPackagingTypeDaoImpl")
@SuppressWarnings("unchecked")
public class SupcPackagingTypeDaoImpl implements ISupcPackagingTypeDao{
    
    private static final Logger LOG = LoggerFactory.getLogger(SupcPackagingTypeDaoImpl.class);

    @Autowired
    private SessionFactory      sessionFactory;
    
   
    
   

    @Override
    public SupcPackagingTypeMapping persistSupcPackagingTypeMappingDetails(SupcPackagingTypeMapping supcPackagingTypeMapping) {
        return (SupcPackagingTypeMapping)sessionFactory.getCurrentSession().merge(supcPackagingTypeMapping);
    }


   
    @Override
    @Transactional(readOnly = true)
    public SupcPackagingTypeMapping getSupcPackagingTypeMappingBySupcAndStoreCode(String supc,String storeCode) {
        // TODO Auto-generated method stub
        Query q = sessionFactory.getCurrentSession().createQuery("FROM SupcPackagingTypeMapping where supc = :supc and storeCode = :storeCode");
        q.setParameter("supc", supc);
        q.setParameter("storeCode", storeCode);
        return (SupcPackagingTypeMapping) q.uniqueResult();
        
     
    }



    @Override
    @Transactional(readOnly = true)
    public List<SupcPackagingTypeMapping> getSupcPackagignTypeMappingListBySUPC(String supc) {
        // TODO Auto-generated method stub
        Query query = sessionFactory.getCurrentSession().createQuery("FROM SupcPackagingTypeMapping WHERE " + " supc = :supc  ");
        query.setParameter("supc", supc);
        return query.list();
    }



    @Override
    @Transactional(readOnly = true)
    public SupcPackagingTypeMapping getEnabledandActiveSPTMappingBySupcAndStoreCode(String supc, String storeCode ,boolean enabled) {
        // TODO Auto-generated method stub
        SupcPackagingTypeMapping supcPackagingTypeMapping = null;
        enabled = true;
        Query q = sessionFactory.getCurrentSession().createQuery("FROM SupcPackagingTypeMapping where supc = :supc and storeCode = :storeCode and enabled = :true");
        q.setParameter("supc", supc);
        q.setParameter("storeCode", storeCode);
        q.setParameter("enabled", enabled);
        supcPackagingTypeMapping = (SupcPackagingTypeMapping) q.uniqueResult();       
        return supcPackagingTypeMapping;
            
        
        
    } 
    

    
    }
    
    
    

    

