/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.db.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.snapdeal.cocofs.db.dao.ITaxRateDao;
import com.snapdeal.cocofs.entity.SellerCategoryTaxRate;
import com.snapdeal.cocofs.entity.SellerSupcFMMapping;
import com.snapdeal.cocofs.entity.SellerSupcTaxRate;
import com.snapdeal.cocofs.entity.StateCategoryPriceTaxRate;

/**
 * @version 1.0, 18-Aug-2014
 * @author ankur
 */

@Repository("TaxRateDaoImpl")
@SuppressWarnings("unchecked")
public class TaxRateDaoImpl implements ITaxRateDao {

    private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<StateCategoryPriceTaxRate> getStateCategoryPriceTaxRate(String state, String categoryUrl) {
        Query q = sessionFactory.getCurrentSession().createQuery("from StateCategoryPriceTaxRate where state = :state and categoryUrl = :categoryUrl");
        q.setParameter("state", state);
        q.setParameter("categoryUrl", categoryUrl);

        return q.list();
    }

    @Override
    public List<StateCategoryPriceTaxRate> getAllEnabledStateCategoryPriceTaxRates() {
        Query q = sessionFactory.getCurrentSession().createQuery("from StateCategoryPriceTaxRate where enabled = true");
        return q.list();
    }

    @Override
    public SellerCategoryTaxRate getSellerCategoryTaxRate(String sellerCode, String categoryUrl) {
        Query q = sessionFactory.getCurrentSession().createQuery("from SellerCategoryTaxRate where sellerCode = :sellerCode and categoryUrl = :categoryUrl");
        q.setParameter("sellerCode", sellerCode);
        q.setParameter("categoryUrl", categoryUrl);

        return (SellerCategoryTaxRate) q.uniqueResult();
    }

    @Override
    public List<SellerCategoryTaxRate> getAllEnabledSellerCategoryTaxRates() {
        Query q = sessionFactory.getCurrentSession().createQuery("from SellerCategoryTaxRate where enabled = true");
        return q.list();
    }

    @Override
    public SellerSupcTaxRate getSellerSupcTaxRate(String sellerCode, String supc) {
        Query q = sessionFactory.getCurrentSession().createQuery("from SellerSupcTaxRate where sellerCode = :sellerCode and supc = :supc ");
        q.setParameter("sellerCode", sellerCode);
        q.setParameter("supc", supc);

        return (SellerSupcTaxRate) q.uniqueResult();
    }

    @Override
    public List<SellerSupcTaxRate> getAllEnabledSellerSupcTaxRates() {
        Query q = sessionFactory.getCurrentSession().createQuery("from SellerSupcTaxRate where enabled = true");

        return q.list();
    }

    @Override
    public List<SellerSupcTaxRate> getTaxRateForSellerCode(String sellerCode) {

        Query q = sessionFactory.getCurrentSession().createQuery("from SellerSupcTaxRate where  sellerCode = :sellerCode and enabled = true");
        q.setParameter("sellerCode", sellerCode);
        return q.list();

    }

    @Override
    public StateCategoryPriceTaxRate updateStateCategoryPriceTaxRate(StateCategoryPriceTaxRate taxRate) {
        return (StateCategoryPriceTaxRate) sessionFactory.getCurrentSession().merge(taxRate);
    }

    @Override
    public SellerCategoryTaxRate updateSellerCategoryTaxRate(SellerCategoryTaxRate taxRate) {
        return (SellerCategoryTaxRate) sessionFactory.getCurrentSession().merge(taxRate);
    }

    @Override
    public SellerSupcTaxRate updateSellerSupcTaxRate(SellerSupcFMMapping taxRate) {
        return (SellerSupcTaxRate) sessionFactory.getCurrentSession().merge(taxRate);
    }

    @Override
    public List<String> getSellersCodeswithTaxRate() {
        Query q = sessionFactory.getCurrentSession().createQuery("select distinct sellerCode from SellerSupcTaxRate where enabled = true");
        return q.list();
    }
}
