/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 30-Sep-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.db.dao;

import java.util.Date;
import java.util.List;

import com.snapdeal.cocofs.entity.SellerFMMapping;
import com.snapdeal.cocofs.entity.SellerSubcatFMMapping;
import com.snapdeal.cocofs.entity.SellerSupcFMMapping;
import com.snapdeal.cocofs.entity.SellerSupcMapping;

public interface IFulfilmentModelDao {

    /**
     * @param vendorCode
     * @return
     */
    public SellerFMMapping getEnabledSellerFMMapping(String vendorCode);

    /**
     * @param mapping
     * @return
     */
    public SellerFMMapping updateSellerFMMapping(SellerFMMapping mapping);

    /**
     * @param mapping
     * @returnseller_fm_mapping
     */
    public SellerSubcatFMMapping updateSellerSubcatFMMapping(SellerSubcatFMMapping mapping);

    /**
     * @param mapping
     * @return
     */
    public SellerSupcFMMapping updateSellerSupcFMMapping(SellerSupcFMMapping mapping);

    /**
     * @param vendorCode
     * @param subcat
     * @return
     */
    public SellerSubcatFMMapping getEnabledSellerSubcatFMMapping(String vendorCode, String subcat);

    /**
     * @param vendorCode
     * @param supc
     * @return
     */
    public SellerSupcFMMapping getEnabledSellerSupcFMMapping(String vendorCode, String supc);

    /**
     * @param vendorCode
     * @return
     */
    public SellerFMMapping getSellerFMMapping(String vendorCode);

    /**
     * @param vendorCode
     * @param subcat
     * @return
     */
    public SellerSubcatFMMapping getSellerSubcatFMMapping(String vendorCode, String subcat);

    /**
     * @param vendorCode
     * @param supc
     * @return
     */
    public SellerSupcFMMapping getSellerSupcFMMapping(String vendorCode, String supc);

    /**
     * @param sellerCode
     * @return
     */
    public List<SellerSubcatFMMapping> getSellerSubcatFMMapping(String sellerCode);

    /**
     * @param sellerCode
     * @return
     */
    public List<SellerSupcFMMapping> getEnabledSellerSupcFMMapping(String sellerCode);

    /**
     * Get Enabled SUPC Exception List for seller
     * 
     * @param sellerCode
     * @return
     */
    public List<String> getEnabledSupcExceptionListForSeller(String sellerCode);

    /**
     * Get List SUPC served by the seller
     * 
     * @param sellerCode
     * @return
     */
    public List<String> getSupcListForSeller(String sellerCode);
    
    /**
     * @return all sellers having seller supc fm mapping
     */
    public List<String> getSellersWithExceptionAtSupcLevel();

    List<SellerFMMapping> getAllSellerFMAndFCMapping();

    List<SellerFMMapping> getAllSellerFMAndFCMappingByLastUpdated(Date lastUpdated);

    /**
     * @param sellerCode
     * @return
     */
    public long getEnabledSellerSupcFMMappingCount(String sellerCode);

    Long getSellerFMMappingCountByLastUpdated(Date lastUpdated);

    Long getSellerSupcFMMappingCountByLastUpdated(Date lastUpdated);

    List<SellerFMMapping> getAllSellerFMMappingByLastUpdated(Date lastUpdated);

    List<SellerSupcFMMapping> getAllSellerSupcFMMappingByLastUpdated(Date lastUpdated);
    
    /**
     * Retrieve all SUPCs for a given seller from the database.
     * @param sellerCode
     * @return
     */
    List<SellerSupcMapping> getSellerSupcMappingList(String sellerCode);
    
    /**
     * Retrieve mapping for particular supc and seller.
     * @param sellerCode
     * @param supc
     * @return
     */
    SellerSupcMapping getSellerSupcMapping(String sellerCode, String supc);

    /**
     * Return the list of all enabled SellerCode for a specified Fulfillment At Seller Level.
     * 
     * @param fulfillmentModel
     * @return
     */
	public List<String> getEnabledSellerListAtSellerLevelForFulfillmentModel(String fulfillmentModel);
	
	/**
     * Return the list of all enabled SellerCode for a specified Fulfillment At Supc Level.
     * 
     * @param fulfillmentModel
     * @return
     */
	public List<String> getEnabledSellerListAtSupcLevelForFulfillmentModel(String fulfillmentModel);
	
	/**
     * Return the list of all enabled SellerCode for a specified Fulfillment At Subcat Level.
     * 
     * @param fulfillmentModel
     * @return
     */
	public List<String> getEnabledSellerListAtSubcatLevelForFulfillmentModel(String fulfillmentModel);

    List<String> getEnabledSellerListAtSellerLevelForFCcode(String fcCode);

    List<SellerSupcFMMapping> getEnabledSellerSupcMappingListForFcCode(String fcCode);

}
