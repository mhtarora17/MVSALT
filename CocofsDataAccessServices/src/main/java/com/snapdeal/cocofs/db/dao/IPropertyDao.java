/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.db.dao;

import com.snapdeal.cocofs.entity.CocofsProperty;

public interface IPropertyDao {
	
	public void save(CocofsProperty property);
    
	public void update(CocofsProperty property);
    
    public CocofsProperty merge(CocofsProperty cpl);
    
    public CocofsProperty getPropertyFromName(String name);
    
    public void remove(CocofsProperty property);
}
