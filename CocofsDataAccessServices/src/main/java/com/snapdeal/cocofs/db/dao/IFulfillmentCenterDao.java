/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 11-Jul-2013
 *  @author prateek
 */
package com.snapdeal.cocofs.db.dao;

import java.util.List;

import com.snapdeal.cocofs.entity.FulfillmentCentre;

public interface IFulfillmentCenterDao {

    List<FulfillmentCentre> getAllFulfillmentCenter();

	FulfillmentCentre getFulfillmentCenterForFCCode(String fcCode);
		
	Integer updateFCName(String fcCode, String centerName, String username);

    List<FulfillmentCentre> getAllFulfillmentCenterByType(String centerType);

    FulfillmentCentre updateCenter(FulfillmentCentre center);
	
	

}

