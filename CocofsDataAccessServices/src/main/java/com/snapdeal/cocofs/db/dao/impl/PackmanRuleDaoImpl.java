/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.db.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.snapdeal.cocofs.db.dao.IPackmanRuleDao;
import com.snapdeal.rule.engine.entity.Rule;

/**
 *  
 *  @version     1.0, 27-Dec-2015
 *  @author brijesh
 */
@Repository("packmanRuleDao")
public class PackmanRuleDaoImpl implements IPackmanRuleDao {

    @Autowired
    private SessionFactory sessionFactory;
    
    @Override
    @Transactional(readOnly=true)
    @SuppressWarnings("unchecked")
    public List<Rule> getAllRuleForStore(String name,String value) {
        Criteria c = sessionFactory.getCurrentSession().createCriteria(Rule.class);
        c.createAlias("ruleParams", "rp");
        c.add(Restrictions.eq("rp.paramName", name)).add(Restrictions.eq("rp.paramValue", value));
        return (List<Rule>) c.list();
    }

    @Override
    @Transactional(readOnly=true)
    @SuppressWarnings("unchecked")
    public List<Rule> getAllRuleByRuleParams(String paramName1, String paramValue1, String paramName2, String paramValue2) {
        Query q = sessionFactory.getCurrentSession().createQuery("select rp.rule From RuleParams rp where rp.paramName =:paramName and rp.paramValue=:paramValue and  rp.rule.id in (select r1.rule.id From RuleParams r1 where paramName =:paramName1 and paramValue=:paramValue1)");
        q.setParameter("paramName", paramName1);
        q.setParameter("paramValue", paramValue1);
        q.setParameter("paramName1", paramName2);
        q.setParameter("paramValue1", paramValue2);
        return (List<Rule>) q.list();
    }

    @Override
    @Transactional(readOnly=true)
    @SuppressWarnings("unchecked")
    public List<Rule> getRuleByName(String name,String param,String value) {
        Criteria c = sessionFactory.getCurrentSession().createCriteria(Rule.class);
        c.add(Restrictions.eq("name", name));
        c.createAlias("ruleParams", "rp");
        c.add(Restrictions.eq("rp.paramName", param)).add(Restrictions.eq("rp.paramValue", value));
        return (List<Rule>) c.list();
    }

    @Override
    @Transactional(readOnly=true)
    @SuppressWarnings("unchecked")
    public List<Rule> getRuleByRuleParamsAndBlockId(String name, String value, int blockId) {
        Criteria c = sessionFactory.getCurrentSession().createCriteria(Rule.class);
        c.createAlias("ruleParams", "rp");
        c.add(Restrictions.eq("rp.paramName", name)).add(Restrictions.eq("rp.paramValue", value));
        c.add(Restrictions.eq("blockId", blockId));
        return (List<Rule>) c.list();
    }

}
