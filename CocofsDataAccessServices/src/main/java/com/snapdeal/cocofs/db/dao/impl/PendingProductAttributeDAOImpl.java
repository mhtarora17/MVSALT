package com.snapdeal.cocofs.db.dao.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.snapdeal.cocofs.db.dao.IPendingProductAttributeDAO;
import com.snapdeal.cocofs.entity.PendingProductAttributeUpdate;

/**
 * @author nikhil
 */
@Repository("pendingProductAttributeDAO")
public class PendingProductAttributeDAOImpl implements IPendingProductAttributeDAO {

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public List<PendingProductAttributeUpdate> getPendingProductAttribute(String supc) {
        Query query = sessionFactory.getCurrentSession().createQuery("from PendingProductAttributeUpdate ppau where ppau.supc=:supc and ppau.pending =:pending");
        query.setParameter("supc", supc);
        query.setParameter("pending", true);
        return query.list();
    }

    @Override
    public List<PendingProductAttributeUpdate> getPlayablePendingProductAttribute(String supc, Date playDate) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from PendingProductAttributeUpdate ppau where ppau.supc=:supc and ppau.pending =:pending and ppau.playDate=:playDate");
        query.setParameter("supc", supc);
        query.setParameter("pending", true);
        query.setParameter("playDate", playDate);
        return query.list();
    }

    @Override
    public List<PendingProductAttributeUpdate> getPlayablePendingProductAttributeByDateRange(Date startDate, Date endDate) {
        Query query = sessionFactory.getCurrentSession().createQuery("from PendingProductAttributeUpdate ppau where ppau.pending =:pending and ppau.playDate >= :startDate and ppau.playDate <= :endDate" );
        query.setParameter("pending", true);
        query.setParameter("startDate", startDate);
        query.setParameter("endDate", endDate);
        return query.list();
    }

    @Override
    public void merge(PendingProductAttributeUpdate pendingProductAttributeUpdate) {
        sessionFactory.getCurrentSession().merge(pendingProductAttributeUpdate);
    }
    
    public List<PendingProductAttributeUpdate> getProductSystemWeightCapturedFlag(String supc) {
        Query query = sessionFactory.getCurrentSession().createQuery("from PendingProductAttributeUpdate ppau where ppau.supc=:supc and ppau.pending =:pending and ppau.attributeName = :attributeName order by ppau.created desc");
        query.setParameter("supc", supc);
        query.setParameter("pending", true);
        query.setParameter("attributeName", "SYSTEMWEIGHTCAPTURED");
        return query.list();
    }
}
