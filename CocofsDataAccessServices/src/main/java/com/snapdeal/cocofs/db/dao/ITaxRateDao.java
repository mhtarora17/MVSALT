/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.db.dao;

import java.util.List;

import com.snapdeal.cocofs.entity.SellerCategoryTaxRate;
import com.snapdeal.cocofs.entity.SellerSupcFMMapping;
import com.snapdeal.cocofs.entity.SellerSupcTaxRate;
import com.snapdeal.cocofs.entity.StateCategoryPriceTaxRate;

/**
 *  
 *  @version     1.0, 18-Aug-2014
 *  @author ankur
 */
public interface ITaxRateDao {

    List<StateCategoryPriceTaxRate> getStateCategoryPriceTaxRate(String state, String categoryUrl);

    SellerCategoryTaxRate getSellerCategoryTaxRate(String sellerCode, String categoryUrl);

    SellerSupcTaxRate getSellerSupcTaxRate(String sellerCode, String supc);

    StateCategoryPriceTaxRate updateStateCategoryPriceTaxRate(StateCategoryPriceTaxRate scpTR);

    SellerCategoryTaxRate updateSellerCategoryTaxRate(SellerCategoryTaxRate taxRate);

    SellerSupcTaxRate updateSellerSupcTaxRate(SellerSupcFMMapping taxRate);

    List<StateCategoryPriceTaxRate> getAllEnabledStateCategoryPriceTaxRates();

    List<SellerCategoryTaxRate> getAllEnabledSellerCategoryTaxRates();

    List<SellerSupcTaxRate> getAllEnabledSellerSupcTaxRates();
    
    List<SellerSupcTaxRate> getTaxRateForSellerCode(String sellerCode);

    List<String> getSellersCodeswithTaxRate();


}
