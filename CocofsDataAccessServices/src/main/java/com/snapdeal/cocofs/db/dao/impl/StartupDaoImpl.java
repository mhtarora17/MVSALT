/*
 *  Copyright 2012 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 28-May-2012
 *  @author amd
 */
package com.snapdeal.cocofs.db.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.snapdeal.cocofs.db.dao.IStartupDao;
import com.snapdeal.cocofs.entity.Attribute;
import com.snapdeal.cocofs.entity.CocofsMapProperty;
import com.snapdeal.cocofs.entity.CocofsProperty;
import com.snapdeal.cocofs.entity.EmailTemplate;
import com.snapdeal.cocofs.entity.JobAction;
import com.snapdeal.cocofs.entity.JobActionRoleMapping;
import com.snapdeal.cocofs.entity.JobStatus;
import com.snapdeal.cocofs.entity.Role;
import com.snapdeal.cocofs.entity.TaskParam;
import com.snapdeal.cocofs.entity.UploadSheetFieldDomain;

@Repository("startupDao")
@SuppressWarnings("unchecked")
public class StartupDaoImpl implements IStartupDao {

    private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<CocofsProperty> getProperties() {
        Query query = sessionFactory.getCurrentSession().createQuery("from CocofsProperty");
        return query.list();
    }

    @Override
    public List<CocofsMapProperty> getMapProperties() {
        Query query = sessionFactory.getCurrentSession().createQuery("from CocofsMapProperty");
        return query.list();
    }

    @Override
    public List<Attribute> getAllEnabledAttributes() {
        Query query = sessionFactory.getCurrentSession().createQuery("from Attribute WHERE enabled = 1 ");
        return query.list();

    }

    @Override
    public List<Attribute> getAllAttributes() {
        Query query = sessionFactory.getCurrentSession().createQuery("from Attribute  ");
        return query.list();

    }

    @Override
    public List<JobAction> getAllJobActions() {
        Query query = sessionFactory.getCurrentSession().createQuery("from JobAction");
        return (List<JobAction>) query.list();
    }

    @Override
    public List<JobStatus> getAllJobStatus() {
        Query query = sessionFactory.getCurrentSession().createQuery("from JobStatus");
        return (List<JobStatus>) query.list();
    }

    @Override
    public List<JobActionRoleMapping> getJobActionRoleMapping() {
        Query query = sessionFactory.getCurrentSession().createQuery("from JobActionRoleMapping where enabled=true");
        return (List<JobActionRoleMapping>) query.list();
    }

    @Override
    public List<EmailTemplate> getEmailTemplates() {
        Query query = sessionFactory.getCurrentSession().createQuery("from EmailTemplate");
        return (List<EmailTemplate>) query.list();
    }

    @Override
    public List<TaskParam> getAllTaskParams() {
        Query query = sessionFactory.getCurrentSession().createQuery("from TaskParam");
        return (List<TaskParam>) query.list();
    }

    @Override
    public List<Role> getAllRoles() {
        Query query = sessionFactory.getCurrentSession().createQuery("from Role");
        return query.list();
    }

    @Override
    public List<UploadSheetFieldDomain> getAllUploadSheetFieldDomains() {
        Query query = sessionFactory.getCurrentSession().createQuery("from UploadSheetFieldDomain");
        return query.list();
    }
}
