/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.db.dao;

import java.util.List;

import com.snapdeal.cocofs.entity.SupcPackagingTypeMapping;

/**
 *  
 *  @version     1.0, 15-Dec-2015
 *  @author vikassharma03
 */
public interface ISupcPackagingTypeDao {
    
    
    /**
     * @param supc
     * @return
     */
    
    
    
    SupcPackagingTypeMapping persistSupcPackagingTypeMappingDetails(SupcPackagingTypeMapping supcPackagingTypeMapping);
    
    SupcPackagingTypeMapping getSupcPackagingTypeMappingBySupcAndStoreCode(String supc,String storeCode);
    
    List<SupcPackagingTypeMapping> getSupcPackagignTypeMappingListBySUPC(String supc);

    SupcPackagingTypeMapping getEnabledandActiveSPTMappingBySupcAndStoreCode(String supc, String storeCode, boolean enabled);    
   

}
