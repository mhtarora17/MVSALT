/*
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 15-Jun-2015
 *  @author Gaurav
 */
package com.snapdeal.cocofs.db.dao;

import java.util.List;

import com.snapdeal.cocofs.entity.SellerZoneMapping;

public interface ISellerZoneDao {

    public List<SellerZoneMapping> getAllSellerZoneMappings();
    
    public SellerZoneMapping getSellerZoneMappingBySeller(String sellerCode);

    public SellerZoneMapping persistSellerZoneMapping(SellerZoneMapping sellerZoneMapping);

    public List<SellerZoneMapping> getAllSellerZoneMappingsByPinCode(String pincode);

}

