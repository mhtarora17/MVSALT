package com.snapdeal.cocofs.db.dao.impl;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.cocofs.db.dao.IPropertyDao;
import com.snapdeal.cocofs.entity.CocofsProperty;

@Repository("cocofsPropertyDaoImpl")
public class PropertyDaoImpl implements IPropertyDao {

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public void save(CocofsProperty property) {
        sessionFactory.getCurrentSession().save(property);
    }

    @Override
    public CocofsProperty merge(CocofsProperty property) {
        return (CocofsProperty) sessionFactory.getCurrentSession().merge(property);
    }

    @Override
    public CocofsProperty getPropertyFromName(String name) {
        Query query = sessionFactory.getCurrentSession().createQuery("from CocofsProperty where name=:name");
        query.setParameter("name", name);
        return (CocofsProperty) query.uniqueResult();
    }

    @Override
    public void remove(CocofsProperty property) {
        sessionFactory.getCurrentSession().delete(property);
    }

    @Override
    public void update(CocofsProperty property) {
        Query query = sessionFactory.getCurrentSession().createQuery("update CocofsProperty set value=:value, lastUpdated = :lastupdated, updatedBy =:username where name=:name");
        query.setParameter("name", property.getName());
        query.setParameter("value", property.getValue());
        query.setParameter("lastupdated", DateUtils.getCurrentTime());
        query.setParameter("username", property.getUpdated_by());
        query.executeUpdate();
    }

}
