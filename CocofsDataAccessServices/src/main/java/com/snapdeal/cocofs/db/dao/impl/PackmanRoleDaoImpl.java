/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.db.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.cocofs.db.dao.IPackmanRoleDao;
import com.snapdeal.cocofs.entity.Role;

/**
 *  
 *  @version     1.0, 09-Dec-2015
 *  @author indrajit
 */
@Repository("packmanRoleDao")
public class PackmanRoleDaoImpl implements IPackmanRoleDao{
    
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @Transactional
    public Role persistRole(Role role) {
	role.setCreated(DateUtils.getCurrentTime());
	role.setLastUpdated(DateUtils.getCurrentTime());
	return (Role) sessionFactory.getCurrentSession().merge(role);
    }

}
