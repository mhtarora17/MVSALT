/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.db.dao;

import java.util.List;

import com.snapdeal.rule.engine.entity.Rule;

/**
 *  
 *  @version     1.0, 08-Dec-2014
 *  @author ankur
 */
public interface ICocofsRuleDao {

    List<Rule> getEnabledAndSynchedRulesByBlockId(Integer blockId);

}
