/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.db.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.cocofs.db.dao.ICocofsRuleDao;
import com.snapdeal.rule.engine.entity.Rule;

/**
 *  
 *  @version     1.0, 08-Dec-2014
 *  @author ankur
 */

@Repository("cocofsRuleDao")
public class CocofsRuleDao implements ICocofsRuleDao {
  
    @Autowired
    SessionFactory sessionFactory;

    
    @Override
    public List<Rule> getEnabledAndSynchedRulesByBlockId(Integer blockId){
        Criteria crit = sessionFactory.getCurrentSession().createCriteria(Rule.class);
        crit.add(Restrictions.eq("blockId", blockId));
        crit.add(Restrictions.eq("enabled", true));
        crit.add(Restrictions.eq("synched", true));
        crit.add(Restrictions.le("startDate", DateUtils.getCurrentTime()));
        crit.add(Restrictions.or(Restrictions.ge("endDate", DateUtils.getCurrentTime()), Restrictions.isNull("endDate")));
        
        return crit.list();
    }
}
