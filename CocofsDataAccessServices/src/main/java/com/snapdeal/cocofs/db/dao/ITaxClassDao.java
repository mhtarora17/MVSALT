/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.db.dao;

import java.util.List;

import com.snapdeal.cocofs.entity.SubcatTaxClassMapping;
import com.snapdeal.cocofs.entity.SupcTaxClassMapping;

/**
 *  
 *  @version     1.0, 18-Aug-2014
 *  @author ankur
 */
public interface ITaxClassDao {

    SupcTaxClassMapping getSupcTaxClassMapping(String supc);

    SubcatTaxClassMapping getSubcatTaxClassMapping(String subcat);

    List<SupcTaxClassMapping> getAllEnabledSupcTaxClassMapping();

    List<SubcatTaxClassMapping> getAllEnabledSubcatTaxClassMapping();


}
