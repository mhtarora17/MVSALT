package com.snapdeal.cocofs.db.dao;

import java.util.List;

import com.snapdeal.cocofs.entity.EventInstance;

public interface IEventHandlerDAS {

    EventInstance save(EventInstance eventInstance);

    EventInstance getEventInstanceById(Integer id);

    List<EventInstance> getAllEventsToExecute(boolean overrideEnable);

    List<EventInstance> getAllEventsInstances(boolean overrideEnable);
}
