/*
 *  Copyright 2012 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 28-May-2012
 *  @author amd
 */
package com.snapdeal.cocofs.db.dao;

import java.util.List;

import com.snapdeal.cocofs.entity.ProductAttribute;
import com.snapdeal.cocofs.entity.ProductAttributeHistory;


public interface IProductAttributeDao {

    /**
     * Returns the enabled ProductAttribute for given supc and attribute if there is one
     * else null is returned.  We expect exactly one enabled row to be present for given supc and attribute
     * though there can be man disabled rows present for given supc and attribute hence 
     * DB level constraints are not applied, but we expect unique Result for enabled row for supc and attribute
     * @param supc
     * @param attribute
     * @return
     */
    ProductAttribute getProductAttributeForSUPC(String supc, String attribute);

    /**
     * Returns the enabled ProductAttribute for given supc if there are any
     * else empty list is returned.
     * @param supc
     * @param attribute
     * @return
     */
    List<ProductAttribute> getAllProductAttributeForSUPC(String supc);

    /**
     * Return all history line item for edits of given supc, attribute combination, if any are present
     * else empty list is returned.
     * The returned list is sorted from latest to oldest entry
     * @param supc
     * @param attribute
     * @return
     */
    List<ProductAttributeHistory> getProductAttributeHistoryForSUPC(String supc, String attribute);

    /**
     * Return all history line item for edits of given supc (its all attributes) if any are present
     * else empty list is returned.
     * The returned list is sorted from latest to oldest entry
     * @param supc
     * @return
     */
    List<ProductAttributeHistory> getAllProductAttributeHistoryForSUPC(String supc);

    /**
     * Method specifically added to load all enabled ProductAttributes in a paginated fashion, ordering the results by
     * supc, so as to group all attributes of single SUPC next to each other
     * 
     * @param firstResult represents the first row number ( startin from 0 )
     * @param maxResults represents the max number of rows to fetch 
     * 
     * e.g if we were fetching 25 rows at time and wanted 3 page we would set firstResult = 25 * 2 (rows already fetched) = 50 (index of 51st row)
     * and maxResults will be set to 25
     * <code>getAllProductAttributeFor(50,25)</code>
     * 
     * In case number of rows returned is less than maxResults , that indicates we are out of rows and no more rows need to be fetched
     * 
     * Ideally Nikhil should go over this and dictate if he wants any changes.
     * @return
     */
    List<ProductAttribute> getAllProductAttributeFor(int firstResult, int maxResults);

    /**
     * Add or update product attribute with given history
     * @param pa
     * @param pah
     * @return
     */
    ProductAttribute merge(ProductAttribute pa, ProductAttributeHistory pah);


}
