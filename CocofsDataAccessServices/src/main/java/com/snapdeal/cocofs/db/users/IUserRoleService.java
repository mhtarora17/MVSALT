/*
 *  Copyright 2012 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 21-Nov-2012
 *  @author prateek
 */
package com.snapdeal.cocofs.db.users;

import java.util.List;

import com.snapdeal.cocofs.entity.Role;

public interface IUserRoleService {

    public Role getRoleByCode(String code);

    public List<Role> getRoleByUserEmail(String email);

    public List<Role> getAllChilds(Role r);

    public List<Role> getRoleTree(Role r);
    
    void deleteUserRoleByRoleCode(List<String> roleCodes);
    
    void deleteRoleByRoleCode(List<String> roleCodes);

}
