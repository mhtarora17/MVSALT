/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.db.dao;

import com.snapdeal.cocofs.entity.Role;

/**
 *  
 *  @version     1.0, 09-Dec-2015
 *  @author indrajit
 */
public interface IPackmanRoleDao {
    
    Role persistRole(Role role);

}
