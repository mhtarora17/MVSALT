package com.snapdeal.cocofs.db.dao.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.snapdeal.cocofs.db.dao.IEventHandlerDAS;
import com.snapdeal.cocofs.db.dao.IEventHandlerDao;
import com.snapdeal.cocofs.entity.EventInstance;

@Service("EventHandlerDAS")
public class EventHandlerDASImpl implements IEventHandlerDAS {

    @Autowired
    private IEventHandlerDao    eventHandlerDao;

    private static final Logger LOG = LoggerFactory.getLogger(EventHandlerDASImpl.class);

    @Override
    @Transactional
    public EventInstance save(EventInstance eventInstance) {
        LOG.info("saving event instance with event type:" + eventInstance.getEventType() + " and arguments:" + eventInstance.getJsonArguments());
        return eventHandlerDao.save(eventInstance);
    }

    @Override
    @Transactional(readOnly = true)
    public EventInstance getEventInstanceById(Integer id) {
        LOG.info("get event instance with id:" + id);
        return eventHandlerDao.getEventInstanceById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<EventInstance> getAllEventsToExecute(boolean overrideEnable) {
        return eventHandlerDao.getAllEventsToExecute(overrideEnable);
    }

    /**
     * Added to get All Event Instances. Will be used to display all events in Admin console
     * 
     * @return
     */
    @Override
    @Transactional(readOnly = true)
    public List<EventInstance> getAllEventsInstances(boolean overrideEnable) {
        return eventHandlerDao.getAllEventInstances(overrideEnable);
    }

}
