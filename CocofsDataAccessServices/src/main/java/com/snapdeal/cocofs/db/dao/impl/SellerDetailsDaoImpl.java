/*
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 15-Jun-2015
 *  @author Gaurav
 */
package com.snapdeal.cocofs.db.dao.impl;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.cocofs.db.dao.ISellerDetailsDao;
import com.snapdeal.cocofs.entity.SellerContactDetails;

@Repository("sellerDetailsDaoImpl")
public class SellerDetailsDaoImpl implements ISellerDetailsDao {

    @Autowired
    private SessionFactory sessionFactory;

	@Override
	public SellerContactDetails createUpdateSellerContactDetails(
			SellerContactDetails scd) {
		scd.setLastUpdated(DateUtils.getCurrentTime());
		return (SellerContactDetails) sessionFactory.getCurrentSession().merge(scd);
	}

	@Override
	public SellerContactDetails getSellerContactDetailsBySeller(
			String sellerCode) {
		Query q = sessionFactory.getCurrentSession().createQuery("FROM SellerContactDetails where sellerCode = :sellerCode");
        q.setParameter("sellerCode", sellerCode);
        return (SellerContactDetails) q.uniqueResult();
	}
}

