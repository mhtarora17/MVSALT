package com.snapdeal.cocofs.db.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.snapdeal.cocofs.db.dao.IProductAttributeNotificationDAO;
import com.snapdeal.cocofs.entity.ProductAttributeNotification;

/**
 * @author nikhil
 */
@Repository("productAttributeNotificationDAO")
public class ProductAttributeNotificationDAOImpl implements IProductAttributeNotificationDAO {

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public List<ProductAttributeNotification> getVisibleProductAttributeNotification(String supc) {
        Query query = sessionFactory.getCurrentSession().createQuery("from ProductAttributeNotification where supc=:supc and visible=:visible");
        query.setParameter("supc", supc);
        query.setParameter("visible", true);
        return query.list();
    }

}
