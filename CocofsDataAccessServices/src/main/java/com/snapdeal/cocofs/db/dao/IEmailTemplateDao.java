/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 05-Sep-2013
 *  @author himanshum
 */
package com.snapdeal.cocofs.db.dao;

import com.snapdeal.cocofs.entity.EmailTemplate;

public interface IEmailTemplateDao {

    /**
     * Get Email Template by name, returns null if no such template
     * @param templateName
     * @return
     */
    EmailTemplate getEmailTemplateByName(String templateName);

    /**
     * Update the email template back to db
     * @param emailTemplate
     * @return
     */
    EmailTemplate update(EmailTemplate emailTemplate);

}
