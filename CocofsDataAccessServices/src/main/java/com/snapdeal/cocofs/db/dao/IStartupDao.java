/*
 *  Copyright 2012 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 28-May-2012
 *  @author amd
 */
package com.snapdeal.cocofs.db.dao;

import java.util.List;

import com.snapdeal.cocofs.entity.Attribute;
import com.snapdeal.cocofs.entity.CocofsMapProperty;
import com.snapdeal.cocofs.entity.CocofsProperty;
import com.snapdeal.cocofs.entity.EmailTemplate;
import com.snapdeal.cocofs.entity.JobAction;
import com.snapdeal.cocofs.entity.JobActionRoleMapping;
import com.snapdeal.cocofs.entity.JobStatus;
import com.snapdeal.cocofs.entity.Role;
import com.snapdeal.cocofs.entity.TaskParam;
import com.snapdeal.cocofs.entity.UploadSheetFieldDomain;

public interface IStartupDao {

    public List<CocofsProperty> getProperties();

    /**
     * Get all enabled Attributes from DB
     * @return
     */
    public List<Attribute> getAllEnabledAttributes();

    /**
     * Get all (enabled and disabled both) Attributes from DB
     * @return
     */
    List<Attribute> getAllAttributes();

    /**
     * Get all Job Actions
     * @return
     */
    List<JobAction> getAllJobActions();

    /**
     * Get all Job Status
     * @return
     */
    List<JobStatus> getAllJobStatus();

    /**
     * Load all enabled job action role mappings
     * @return
     */
    List<JobActionRoleMapping> getJobActionRoleMapping();

    /**
     * Load all email templates
     * @return
     */
    List<EmailTemplate> getEmailTemplates();

    /**
     * Load all task params
     * @return
     */
    List<TaskParam> getAllTaskParams();

    public List<Role> getAllRoles();

    List<UploadSheetFieldDomain> getAllUploadSheetFieldDomains();

	public List<CocofsMapProperty> getMapProperties();

}
