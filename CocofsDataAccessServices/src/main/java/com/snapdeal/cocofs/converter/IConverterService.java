package com.snapdeal.cocofs.converter;

import java.util.List;

import com.snapdeal.base.entity.EmailChannel;
import com.snapdeal.shipping.sro.EmailChannelSRO;


public interface IConverterService {

    List<EmailChannel> getEmailChannelList(List<EmailChannelSRO> emailChannelSROList);
	

}
