package com.snapdeal.cocofs.converter.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.snapdeal.base.entity.EmailChannel;
import com.snapdeal.base.entity.EmailChannelParameter;
import com.snapdeal.cocofs.converter.IConverterService;
import com.snapdeal.shipping.sro.EmailChannelParameterSRO;
import com.snapdeal.shipping.sro.EmailChannelSRO;

@Service("ConverterServiceImpl")
public class ConverterServiceImpl implements IConverterService {

    @Override
    public List<EmailChannel> getEmailChannelList(List<EmailChannelSRO> emailChannelSROList) {
        List<EmailChannel> ecList = new ArrayList<EmailChannel>();
        for (EmailChannelSRO ecSro : emailChannelSROList) {
            EmailChannel ec = new EmailChannel();
            ec.setId(ecSro.getId());
            ec.setName(ecSro.getName());
            ec.setEnabled(ecSro.isEnabled());
            ec.setClassName(ecSro.getClassName());
            for (EmailChannelParameterSRO ecpSro : ecSro.getEmailChannelSROs()) {
                EmailChannelParameter ecp = new EmailChannelParameter();
                ecp.setId(ecpSro.getId());
                ecp.setName(ecpSro.getName());
                ecp.setValue(ecpSro.getValue());
                ecp.setEmailChannel(ec);
                ec.getEmailChannelParameters().add(ecp);
            }
            ecList.add(ec);
        }
        return ecList;
    }

   

 }
