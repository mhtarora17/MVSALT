/*
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 02-Apr-2014
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.external.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.base.entity.EmailChannel;
import com.snapdeal.cocofs.converter.IConverterService;
import com.snapdeal.cocofs.external.service.IShippingExternalService;
import com.snapdeal.shipping.core.model.GetEmailSmsMetadataRequest;
import com.snapdeal.shipping.core.model.GetEmailSmsMetadataResponse;
import com.snapdeal.shipping.service.IShippingClientService;

@Service("ShippingExternalServiceImpl")
public class ShippingExternalServiceImpl implements IShippingExternalService {

    private static final Logger    LOG = LoggerFactory.getLogger(ShippingExternalServiceImpl.class);

    @Autowired
    private IConverterService      converterService;

    @Autowired
    private IShippingClientService shippingClient;

    @Override
    public List<EmailChannel> getEmailChannels() {
        GetEmailSmsMetadataResponse resp = shippingClient.getEmailSMSMetadata(new GetEmailSmsMetadataRequest());
        if (!resp.isMetadataFetched()) {
            LOG.error("Error in Fetching email and sms metadata from shipping");
            return new ArrayList<EmailChannel>();
        }
        return converterService.getEmailChannelList(resp.getEmailChannelSROList());
    }

}
