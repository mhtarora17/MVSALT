package com.snapdeal.cocofs.external.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.snapdeal.base.exception.TransportException;
import com.snapdeal.cocofs.external.service.IOPSExternalService;
import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.external.service.exception.ExternalDataUpdateFailedException;
import com.snapdeal.ops.base.api.getRateCard.GetRateCardRequest;
import com.snapdeal.ops.base.api.getRateCard.GetRateCardResponse;
import com.snapdeal.ops.base.api.getRateCardByCategory.GetRateCardByCategoryRequest;
import com.snapdeal.ops.base.api.getRateCardByCategory.GetRateCardByCategoryResponse;
import com.snapdeal.ops.base.api.isFulfillmentFeeChanged.IsFulfillmentFeeChangedRequest;
import com.snapdeal.ops.base.api.isFulfillmentFeeChanged.IsFulfillmentFeeChangedResponse;
import com.snapdeal.ops.base.api.isFulfillmentFeeChangedForSellers.IsFulfillmentFeeChangedForSellersRequest;
import com.snapdeal.ops.base.api.isFulfillmentFeeChangedForSellers.IsFulfillmentFeeChangedForSellersResponse;
import com.snapdeal.ops.base.api.updateRateCard.UpdateRateCardRequest;
import com.snapdeal.ops.base.api.updateRateCard.UpdateRateCardResponse;
import com.snapdeal.ops.base.api.updateRateCardByCategory.UpdateRateCardByCategoryRequest;
import com.snapdeal.ops.base.api.updateRateCardByCategory.UpdateRateCardByCategoryResponse;
import com.snapdeal.ops.base.exception.OPSException;
import com.snapdeal.ops.client.service.IOPSClientService;

public abstract class AbstractOPSExternalService implements IOPSExternalService {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractOPSExternalService.class);
    @Autowired
    private IOPSClientService   opsClient;

    public GetRateCardResponse getRateCardInternal(GetRateCardRequest request) throws ExternalDataNotFoundException {

        try {

            return opsClient.getRateCard(request);
        } catch (TransportException e) {

            LOG.error("Error in getting rate card from OPS , ex message {}, ex cause {}", e.getMessage(), e.getCause());
            throw new ExternalDataNotFoundException(ExternalDataNotFoundException.Type.DEFAULT, "Error in getting rate card from OPS");
        } catch (OPSException e) {
            LOG.error("Error in getting rate card from OPS , ex message {}, ex cause {}", e.getMessage(), e.getCause());
            throw new ExternalDataNotFoundException(ExternalDataNotFoundException.Type.DEFAULT, "Error in getting rate card from OPS");
        }
        

    }

    public UpdateRateCardResponse updateRateCardInternal(UpdateRateCardRequest request) throws ExternalDataUpdateFailedException {
        try {

            return opsClient.updateRateCard(request);
        } catch (TransportException e) {

            LOG.error("Error in updating rate card to OPS , ex message {}, ex cause {}", e.getMessage(), e.getCause());
            throw new ExternalDataUpdateFailedException(ExternalDataUpdateFailedException.Type.DEFAULT, "Error in updating rate card to OPS");
        } catch (OPSException e) {
            LOG.error("Error in updating rate card to OPS , ex message {}, ex cause {}", e.getMessage(), e.getCause());
            throw new ExternalDataUpdateFailedException(ExternalDataUpdateFailedException.Type.DEFAULT, "Error in updating rate card to OPS");
        
        }
    }
    
    public UpdateRateCardByCategoryResponse updateRateCardByCategoryInternal(UpdateRateCardByCategoryRequest request) throws ExternalDataUpdateFailedException {
        try {

            return opsClient.UpdateRateCardByCategory(request);
        } catch (TransportException e) {

            LOG.error("Error in updating rate card to OPS , ex message {}, ex cause {}", e.getMessage(), e.getCause());
            throw new ExternalDataUpdateFailedException(ExternalDataUpdateFailedException.Type.DEFAULT, "Error in updating rate card to OPS");
        } catch (OPSException e) {
            LOG.error("Error in updating rate card to OPS , ex message {}, ex cause {}", e.getMessage(), e.getCause());
            throw new ExternalDataUpdateFailedException(ExternalDataUpdateFailedException.Type.DEFAULT, "Error in updating rate card to OPS");
        
        }
    }
    
    public GetRateCardByCategoryResponse getRateCardByCategoryInternal(GetRateCardByCategoryRequest request) throws ExternalDataNotFoundException {

        try {

            return opsClient.getRateCardByCategory(request);
        } catch (TransportException e) {

            LOG.error("Error in getting rate card by category from OPS , ex message {}, ex cause {}", e.getMessage(), e.getCause());
            throw new ExternalDataNotFoundException(ExternalDataNotFoundException.Type.DEFAULT, "Error in getting rate card from OPS");
        } catch (OPSException e) {
            LOG.error("Error in getting rate card by category from OPS , ex message {}, ex cause {}", e.getMessage(), e.getCause());
            throw new ExternalDataNotFoundException(ExternalDataNotFoundException.Type.DEFAULT, "Error in getting rate card from OPS");
        }
        

    }
    
    public IsFulfillmentFeeChangedResponse isFulfillmentFeeChangedInternal(IsFulfillmentFeeChangedRequest request) throws TransportException, OPSException {
        return opsClient.isFulfillmentFeeChanged(request);
    }
    
    public IsFulfillmentFeeChangedForSellersResponse isFulfillmentFeeChangedForSellersInternal(IsFulfillmentFeeChangedForSellersRequest request) throws TransportException, OPSException {
        return opsClient.isFulfillmentFeeChangedForSellers(request);
    }
    
}
