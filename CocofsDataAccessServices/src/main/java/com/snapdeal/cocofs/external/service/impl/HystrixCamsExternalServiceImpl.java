package com.snapdeal.cocofs.external.service.impl;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.snapdeal.catalog.base.model.GetAllCategoryListRequest;
import com.snapdeal.catalog.base.model.GetCategoryListByIdListRequest;
import com.snapdeal.catalog.base.model.GetCategoryListResponse;
import com.snapdeal.catalog.base.model.GetMinProductInfoBySupcRequest;
import com.snapdeal.catalog.base.model.GetMinProductInfoResponse;
import com.snapdeal.catalog.base.model.GetPOGDetailListBySUPCListRequest;
import com.snapdeal.catalog.base.model.GetPOGDetailListResponse;
import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.external.service.hystrix.command.CMDGetCategories;
import com.snapdeal.cocofs.external.service.hystrix.command.CMDGetCategoriesByIdList;
import com.snapdeal.cocofs.external.service.hystrix.command.CMDGetMinProductInfo;
import com.snapdeal.cocofs.external.service.hystrix.command.CMDGetPOGList;

/**
 * @author nikhil
 */
@Service("hystrixCamsExternalService")
public class HystrixCamsExternalServiceImpl extends AbstractCAMSExternalService {

    @Override
    @Cacheable(value="productInfoCache", key="#request.supc")
    public GetMinProductInfoResponse getMinProductInfo(GetMinProductInfoBySupcRequest request) throws ExternalDataNotFoundException {
        CMDGetMinProductInfo command = new CMDGetMinProductInfo(request, this);
        return command.execute();
    }

    @Override
    public GetCategoryListResponse getAllCategories(GetAllCategoryListRequest request) throws ExternalDataNotFoundException {
        CMDGetCategories command = new CMDGetCategories(this);
        return command.execute();
    }
    
    @Override
    public GetCategoryListResponse getCategoryListByIdList(GetCategoryListByIdListRequest request) throws ExternalDataNotFoundException{
    	CMDGetCategoriesByIdList command = new CMDGetCategoriesByIdList(this);
         return command.execute();
     }
    
    @Override
    @Cacheable(value="supcdetails", key="#request.supc")
    public GetPOGDetailListResponse getPOGDetailsBbySupc(GetPOGDetailListBySUPCListRequest request)throws ExternalDataNotFoundException {
	CMDGetPOGList command = new CMDGetPOGList(this);
	return command.execute();
    }

}
