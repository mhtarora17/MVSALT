package com.snapdeal.cocofs.external.service.hystrix.command;

import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;
import com.snapdeal.catalog.base.model.GetMinProductInfoBySupcRequest;
import com.snapdeal.catalog.base.model.GetMinProductInfoResponse;
import com.snapdeal.cocofs.external.service.impl.AbstractCAMSExternalService;

/**
 * @author nikhil
 */
public class CMDGetMinProductInfo extends HystrixCommand<GetMinProductInfoResponse> {

    private GetMinProductInfoBySupcRequest request;

    private AbstractCAMSExternalService    externalService;

    public CMDGetMinProductInfo(GetMinProductInfoBySupcRequest request, AbstractCAMSExternalService camsExternalService) {
        super(HystrixCommandGroupKey.Factory.asKey("getMinProductInfo"));
        externalService = camsExternalService;
        this.request = request;
    }

    @Override
    protected GetMinProductInfoResponse run() throws Exception {
        return externalService.getMinProductInfoInternal(request);
    }

}
