package com.snapdeal.cocofs.external.service.hystrix.command;

import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;
import com.snapdeal.cocofs.external.service.impl.AbstractOPSExternalService;
import com.snapdeal.ops.base.api.updateRateCard.UpdateRateCardRequest;
import com.snapdeal.ops.base.api.updateRateCard.UpdateRateCardResponse;

/**
 * @author nikhil
 */
public class CMDUpdateRateCard extends HystrixCommand<UpdateRateCardResponse> {

    private AbstractOPSExternalService opsExternalService;
    private UpdateRateCardRequest request;

    public CMDUpdateRateCard(AbstractOPSExternalService opsExternalService, UpdateRateCardRequest request) {
        super(HystrixCommandGroupKey.Factory.asKey("updateRateCard"));
        this.opsExternalService = opsExternalService;
        this.request   = request;
    }

    @Override
    protected UpdateRateCardResponse run() throws Exception {
        return opsExternalService.updateRateCardInternal(request);
    }

}
