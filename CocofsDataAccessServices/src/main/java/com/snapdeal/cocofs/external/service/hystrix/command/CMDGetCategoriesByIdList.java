package com.snapdeal.cocofs.external.service.hystrix.command;

import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;
import com.snapdeal.catalog.base.model.GetCategoryListByIdListRequest;
import com.snapdeal.catalog.base.model.GetCategoryListResponse;
import com.snapdeal.cocofs.external.service.impl.AbstractCAMSExternalService;

public class CMDGetCategoriesByIdList extends HystrixCommand<GetCategoryListResponse> {

    AbstractCAMSExternalService camsExternalService;
    private GetCategoryListByIdListRequest request;


    public CMDGetCategoriesByIdList(AbstractCAMSExternalService camsExternalService) {
        super(HystrixCommandGroupKey.Factory.asKey("categoriesByIdList"));
        this.camsExternalService = camsExternalService;
    }

    @Override
    protected GetCategoryListResponse run() throws Exception {
        return camsExternalService.getCategoryListByIdListInternal(request);
    }

}
