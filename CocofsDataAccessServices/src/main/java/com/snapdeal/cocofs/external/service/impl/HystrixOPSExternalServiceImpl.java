package com.snapdeal.cocofs.external.service.impl;

import org.springframework.stereotype.Service;

import com.snapdeal.base.exception.TransportException;
import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.external.service.exception.ExternalDataUpdateFailedException;
import com.snapdeal.cocofs.external.service.hystrix.command.CMDGetRateCard;
import com.snapdeal.cocofs.external.service.hystrix.command.CMDGetRateCardByCategory;
import com.snapdeal.cocofs.external.service.hystrix.command.CMDIsFulfillmentFeeChanged;
import com.snapdeal.cocofs.external.service.hystrix.command.CMDIsFulfillmentFeeChangedForSellers;
import com.snapdeal.cocofs.external.service.hystrix.command.CMDUpdateRateCard;
import com.snapdeal.cocofs.external.service.hystrix.command.CMDUpdateRateCardByCategory;
import com.snapdeal.ops.base.api.getRateCard.GetRateCardRequest;
import com.snapdeal.ops.base.api.getRateCard.GetRateCardResponse;
import com.snapdeal.ops.base.api.getRateCardByCategory.GetRateCardByCategoryRequest;
import com.snapdeal.ops.base.api.getRateCardByCategory.GetRateCardByCategoryResponse;
import com.snapdeal.ops.base.api.isFulfillmentFeeChanged.IsFulfillmentFeeChangedRequest;
import com.snapdeal.ops.base.api.isFulfillmentFeeChanged.IsFulfillmentFeeChangedResponse;
import com.snapdeal.ops.base.api.isFulfillmentFeeChangedForSellers.IsFulfillmentFeeChangedForSellersRequest;
import com.snapdeal.ops.base.api.isFulfillmentFeeChangedForSellers.IsFulfillmentFeeChangedForSellersResponse;
import com.snapdeal.ops.base.api.updateRateCard.UpdateRateCardRequest;
import com.snapdeal.ops.base.api.updateRateCard.UpdateRateCardResponse;
import com.snapdeal.ops.base.api.updateRateCardByCategory.UpdateRateCardByCategoryRequest;
import com.snapdeal.ops.base.api.updateRateCardByCategory.UpdateRateCardByCategoryResponse;
import com.snapdeal.ops.base.exception.OPSException;

@Service("hystrixOPSExternalService")
public class HystrixOPSExternalServiceImpl extends AbstractOPSExternalService{

    @Override
    public GetRateCardResponse getRateCard(GetRateCardRequest request) throws ExternalDataNotFoundException {
        CMDGetRateCard command = new CMDGetRateCard(this, request);
        return command.execute();
    }

    @Override
    public UpdateRateCardResponse updateRateCard(UpdateRateCardRequest request) throws ExternalDataUpdateFailedException {
        CMDUpdateRateCard command = new CMDUpdateRateCard(this, request);
        return command.execute();
    }
    
    @Override
    public UpdateRateCardByCategoryResponse updateRateCardByCategory(UpdateRateCardByCategoryRequest request) throws ExternalDataUpdateFailedException{
    	CMDUpdateRateCardByCategory command = new CMDUpdateRateCardByCategory(this, request);
    	return command.execute();
    }

    
    @Override
    public GetRateCardByCategoryResponse getRateCardByCategory(GetRateCardByCategoryRequest request) throws ExternalDataNotFoundException {
        CMDGetRateCardByCategory command = new CMDGetRateCardByCategory(this, request);
        return command.execute();
    }

    @Override
    public IsFulfillmentFeeChangedResponse isFulfillmentFeeChanged(IsFulfillmentFeeChangedRequest request) throws TransportException, OPSException {
        CMDIsFulfillmentFeeChanged command = new CMDIsFulfillmentFeeChanged(this, request);
        return command .execute();
    }

    @Override
    public IsFulfillmentFeeChangedForSellersResponse isFulfillmentFeeChangedForSellers(IsFulfillmentFeeChangedForSellersRequest request) throws TransportException, OPSException {
        CMDIsFulfillmentFeeChangedForSellers command = new CMDIsFulfillmentFeeChangedForSellers(this, request);
        
        return command.execute();
    }

    
}
