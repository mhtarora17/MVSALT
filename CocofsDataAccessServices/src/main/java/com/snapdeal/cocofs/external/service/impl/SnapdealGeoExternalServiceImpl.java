package com.snapdeal.cocofs.external.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.base.exception.SnapdealWSException;
import com.snapdeal.cocofs.external.service.ISnapdealGeoExternalService;
import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.geo.base.sro.ZoneRequest;
import com.snapdeal.geo.base.sro.ZoneResponse;
import com.snapdeal.geo.client.IGeoClientService;

@Service("snapdealGeoExternalService")
public class SnapdealGeoExternalServiceImpl implements ISnapdealGeoExternalService{

    @Autowired
    private IGeoClientService geoClientService;
    
    
    private static final Logger   LOG = LoggerFactory.getLogger(AbstractCAMSExternalService.class);

   
    @Override
    public ZoneResponse getZoneFromPincode(ZoneRequest request) throws ExternalDataNotFoundException {
        try {
            return geoClientService.getZoneFromPincode(request);
        } catch (SnapdealWSException e) {

            LOG.error("Error in getting zone from SnapdealGeo, ex message "+e.getMessage() +" ex cause {}",  e.getCause());
            throw new ExternalDataNotFoundException(ExternalDataNotFoundException.Type.DEFAULT, "Error in getting zone info from SnapdealGeo for pincode " + request.getPincode());

        }
    }

    
}
