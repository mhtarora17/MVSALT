package com.snapdeal.cocofs.external.service;

import com.snapdeal.catalog.base.model.GetAllCategoryListRequest;
import com.snapdeal.catalog.base.model.GetCategoryListByIdListRequest;
import com.snapdeal.catalog.base.model.GetCategoryListResponse;
import com.snapdeal.catalog.base.model.GetMinProductInfoBySupcRequest;
import com.snapdeal.catalog.base.model.GetMinProductInfoResponse;
import com.snapdeal.catalog.base.model.GetPOGDetailListBySUPCListRequest;
import com.snapdeal.catalog.base.model.GetPOGDetailListResponse;
import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;

/**
 * @author nikhil
 */
public interface ICAMSExternalService {

    public GetMinProductInfoResponse getMinProductInfo(GetMinProductInfoBySupcRequest request) throws ExternalDataNotFoundException;

    public GetCategoryListResponse getAllCategories(GetAllCategoryListRequest request) throws ExternalDataNotFoundException;
    
    
    public GetCategoryListResponse getCategoryListByIdList(GetCategoryListByIdListRequest request) throws ExternalDataNotFoundException;
    
    public GetPOGDetailListResponse getPOGDetailsBbySupc(GetPOGDetailListBySUPCListRequest request)throws ExternalDataNotFoundException;
    
}
