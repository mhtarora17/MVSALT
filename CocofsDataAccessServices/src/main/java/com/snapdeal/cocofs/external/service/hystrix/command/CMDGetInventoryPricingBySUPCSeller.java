/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.external.service.hystrix.command;

import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;
import com.snapdeal.cocofs.external.service.impl.AbstractVIPMSExternalService;
import com.snapdeal.ipms.base.api.getInventoryPricingBySUPCSeller.v1.SUPCSellerInventoryPricingSRO;

/**
 *  
 *  @version     1.0, 24-Jul-2014
 *  @author ankur
 */
public class CMDGetInventoryPricingBySUPCSeller  extends HystrixCommand<SUPCSellerInventoryPricingSRO>{

    private AbstractVIPMSExternalService externalService;
    private String supc ;
    private String sellerCode;
    
    
    public CMDGetInventoryPricingBySUPCSeller( AbstractVIPMSExternalService externalService, String supc, String sellerCode) {
        super(HystrixCommandGroupKey.Factory.asKey("getInventoryPricingBySUPCSeller"));
        this.externalService = externalService;
        this.supc = supc;
        this.sellerCode = sellerCode;
    }


    @Override
    protected SUPCSellerInventoryPricingSRO run() throws Exception {
        return externalService.getInventoryPricingBySUPCSeller(supc, sellerCode);
    }


}
