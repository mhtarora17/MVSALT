/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.external.service.exception;


/**
 *  
 *  @version     1.0, 24-Jul-2014
 *  @author ankur
 */
public class FulfillmentModelNotFoundException extends Exception {

   
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public enum Type {
        DEFAULT("default");

        private String code;

        private Type(String code) {
            this.code = code;
        }

        public String code() {
            return this.code;
        }
    }

    Type errorType;

    public FulfillmentModelNotFoundException(Type type, String message) {
        super(message);
        errorType = type;
    }

    public FulfillmentModelNotFoundException(String message) {
        super(message);
    }

    public Type getErrorType() {
        return errorType;
    }

    public void setErrorType(Type errorType) {
        this.errorType = errorType;
    }
}
