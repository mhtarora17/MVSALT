/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.external.service.hystrix.command;

import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;
import com.snapdeal.catalog.base.model.GetPOGDetailListBySUPCListRequest;
import com.snapdeal.catalog.base.model.GetPOGDetailListResponse;
import com.snapdeal.cocofs.external.service.impl.AbstractCAMSExternalService;

/**
 *  
 *  @version     1.0, 25-Dec-2015
 *  @author indrajit
 */
public class CMDGetPOGList extends HystrixCommand<GetPOGDetailListResponse> {
    AbstractCAMSExternalService camsExternalService;
    GetPOGDetailListBySUPCListRequest request;
    
    public CMDGetPOGList(AbstractCAMSExternalService camsExternalService) {
        super(HystrixCommandGroupKey.Factory.asKey("pogList"));
        this.camsExternalService = camsExternalService;
    }
    
    @Override
    protected GetPOGDetailListResponse run() throws Exception {
        return camsExternalService.getSimplePOGDetailListBySUPCList(request);
    }

}
