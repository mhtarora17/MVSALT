package com.snapdeal.cocofs.external.service;

import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.geo.base.sro.ZoneRequest;
import com.snapdeal.geo.base.sro.ZoneResponse;

public interface ISnapdealGeoExternalService {
    
    public ZoneResponse getZoneFromPincode (ZoneRequest request) throws ExternalDataNotFoundException;;

}
