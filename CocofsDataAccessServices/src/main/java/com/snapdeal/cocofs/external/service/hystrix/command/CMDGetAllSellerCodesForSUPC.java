/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Nov 22, 2013
 *  @author himanshu
 */
package com.snapdeal.cocofs.external.service.hystrix.command;

import java.util.List;

import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;
import com.snapdeal.cocofs.external.service.impl.AbstractVIPMSExternalService;

public class CMDGetAllSellerCodesForSUPC extends HystrixCommand<List<String>>{

    private AbstractVIPMSExternalService externalService;
    private String supc ;
    
    
    public CMDGetAllSellerCodesForSUPC( AbstractVIPMSExternalService externalService, String supc) {
        super(HystrixCommandGroupKey.Factory.asKey("getAllSellerCodesForSUPC"));
        this.externalService = externalService;
        this.supc = supc;
    }


    @Override
    protected List<String> run() throws Exception {
        return externalService.getAllSellerCodesForSUPCInternal(supc);
    }

}
