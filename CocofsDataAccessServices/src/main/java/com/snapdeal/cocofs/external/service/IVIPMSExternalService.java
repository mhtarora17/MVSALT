/**
 * 
 */
package com.snapdeal.cocofs.external.service;

import java.util.List;

import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.ipms.base.api.getInventoryPricingBySUPCSeller.v1.SUPCSellerInventoryPricingSRO;

/**
 * @author abhinav
 *
 */
public interface IVIPMSExternalService {

    List<String> getAllSellerCodesForSUPC(String supc);
    
    public SUPCSellerInventoryPricingSRO getInventoryPricingBySUPCSeller(String supc, String sellerCode) throws ExternalDataNotFoundException ;

}
