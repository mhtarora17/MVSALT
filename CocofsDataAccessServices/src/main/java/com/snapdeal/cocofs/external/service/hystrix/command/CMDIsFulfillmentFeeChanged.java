/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Nov 22, 2013
 *  @author himanshu
 */
package com.snapdeal.cocofs.external.service.hystrix.command;

import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;
import com.snapdeal.cocofs.external.service.impl.AbstractOPSExternalService;
import com.snapdeal.ops.base.api.isFulfillmentFeeChanged.IsFulfillmentFeeChangedRequest;
import com.snapdeal.ops.base.api.isFulfillmentFeeChanged.IsFulfillmentFeeChangedResponse;

public class CMDIsFulfillmentFeeChanged extends HystrixCommand<IsFulfillmentFeeChangedResponse> {

    private AbstractOPSExternalService opsExternalService;
    private IsFulfillmentFeeChangedRequest request;
    
    
    public CMDIsFulfillmentFeeChanged(AbstractOPSExternalService opsExternalService, IsFulfillmentFeeChangedRequest request) {
        super(HystrixCommandGroupKey.Factory.asKey("isFulfillmentFeeChanged"));
        
        this.opsExternalService = opsExternalService;
        this.request = request;
    }


    @Override
    protected IsFulfillmentFeeChangedResponse run() throws Exception {
        return opsExternalService.isFulfillmentFeeChangedInternal(request);
    }

}
