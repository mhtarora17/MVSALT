/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Nov 22, 2013
 *  @author himanshu
 */
package com.snapdeal.cocofs.external.service.hystrix.command;

import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;
import com.snapdeal.cocofs.external.service.impl.AbstractOPSExternalService;
import com.snapdeal.ops.base.api.isFulfillmentFeeChangedForSellers.IsFulfillmentFeeChangedForSellersRequest;
import com.snapdeal.ops.base.api.isFulfillmentFeeChangedForSellers.IsFulfillmentFeeChangedForSellersResponse;

public class CMDIsFulfillmentFeeChangedForSellers extends HystrixCommand<IsFulfillmentFeeChangedForSellersResponse> {

    private AbstractOPSExternalService opsExternalService;
    private IsFulfillmentFeeChangedForSellersRequest request;
    
    
    public CMDIsFulfillmentFeeChangedForSellers(AbstractOPSExternalService opsExternalService, IsFulfillmentFeeChangedForSellersRequest request) {
        super(HystrixCommandGroupKey.Factory.asKey("isFulfillmentFeeChangedForSellers"));
        
        this.opsExternalService = opsExternalService;
        this.request = request;
    }


    @Override
    protected IsFulfillmentFeeChangedForSellersResponse run() throws Exception {
        return opsExternalService.isFulfillmentFeeChangedForSellersInternal(request);
    }

}
