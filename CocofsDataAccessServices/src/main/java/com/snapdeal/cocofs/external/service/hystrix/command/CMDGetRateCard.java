package com.snapdeal.cocofs.external.service.hystrix.command;

import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;
import com.snapdeal.cocofs.external.service.impl.AbstractOPSExternalService;
import com.snapdeal.ops.base.api.getRateCard.GetRateCardRequest;
import com.snapdeal.ops.base.api.getRateCard.GetRateCardResponse;

/**
 * @author nikhil
 */
public class CMDGetRateCard extends HystrixCommand<GetRateCardResponse> {

    private AbstractOPSExternalService opsExternalService;
    private GetRateCardRequest request;

    public CMDGetRateCard(AbstractOPSExternalService opsExternalService, GetRateCardRequest request) {
        super(HystrixCommandGroupKey.Factory.asKey("getRateCard"));
        this.opsExternalService = opsExternalService;
        this.request   = request;
    }

    @Override
    protected GetRateCardResponse run() throws Exception {
        return opsExternalService.getRateCardInternal(request);
    }

}
