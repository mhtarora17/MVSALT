/*
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 02-Apr-2014
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.external.service;

import java.util.List;

import com.snapdeal.base.entity.EmailChannel;

public interface IShippingExternalService {

    List<EmailChannel> getEmailChannels();

}
