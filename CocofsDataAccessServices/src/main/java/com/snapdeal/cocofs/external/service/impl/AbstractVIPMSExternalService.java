/**
 * 
 */
package com.snapdeal.cocofs.external.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.snapdeal.base.exception.TransportException;
import com.snapdeal.base.model.common.CatalogType;
import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.cocofs.external.service.IVIPMSExternalService;
import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.ipms.base.api.getInventoryPricingBySUPCSeller.v1.GetInventoryPricingBySUPCSellerRequest;
import com.snapdeal.ipms.base.api.getInventoryPricingBySUPCSeller.v1.GetInventoryPricingBySUPCSellerResponse;
import com.snapdeal.ipms.base.api.getInventoryPricingBySUPCSeller.v1.SUPCSellerInventoryPricingSRO;
import com.snapdeal.ipms.base.api.getInventoryPricingBySUPCSeller.v1.SUPCSellerMappingSRO;
import com.snapdeal.ipms.base.request.GetVendorCodeListBySupcRequest;
import com.snapdeal.ipms.base.response.GetVendorCodeListResponse;
import com.snapdeal.ipms.client.service.IIPMSClientService;

/**
 * @author abhinav
 */

public abstract class AbstractVIPMSExternalService implements IVIPMSExternalService {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractVIPMSExternalService.class);

    @Autowired
    IIPMSClientService          ipmsClientService;

    public List<String> getAllSellerCodesForSUPCInternal(String supc) {

        List<String> sellerCodes = new ArrayList<String>(0);

        if (StringUtils.isEmpty(supc)) {

            LOG.info("SUPC IS BLANK/NULL");
            return sellerCodes;
        }

        GetVendorCodeListBySupcRequest req = new GetVendorCodeListBySupcRequest();
        GetVendorCodeListResponse resp = null;
        req.setSUPC(supc);
        try {

            resp = ipmsClientService.getProductVendorBySUPC(req);

            if (!resp.isSuccessful()) {

                LOG.info("VIPMS call getProductVendorBySUPC was not successful[validation reason - {}; message - {}] for supc {}", resp.getIpmsResponseCode(), resp.getMessage(),
                        supc);
                return sellerCodes;
            }

            if (resp.getVendorCodes() == null || (resp.getVendorCodes() != null && resp.getVendorCodes().isEmpty())) {

                LOG.info("VIPMS call getProductVendorBySUPC returned no vendors for supc {}", supc);
                return sellerCodes;
            }

            LOG.info("For supc {}, returning seller codes {}", supc, resp.getVendorCodes());
            return resp.getVendorCodes();

        } catch (TransportException e) {

            LOG.info("VIPMS call getProductVendorBySUPC threw TransportException for supc {}", supc);
            LOG.error("VIPMS call getProductVendorBySUPC threw TransportException for supc", e);
            return sellerCodes;
        }

    }
    
    
    public SUPCSellerInventoryPricingSRO getInventoryPricingBySUPCSellerInternal(String supc, String sellerCode) throws ExternalDataNotFoundException {


        if (StringUtils.isEmpty(supc) || StringUtils.isEmpty(sellerCode)) {

            LOG.info("SUPC/sellerCode IS BLANK/NULL");
            return null;
        }

        GetInventoryPricingBySUPCSellerResponse resp = null;

        GetInventoryPricingBySUPCSellerRequest req = new GetInventoryPricingBySUPCSellerRequest();
        List<SUPCSellerMappingSRO> supcSellerMappingSROList = new ArrayList<SUPCSellerMappingSRO>();
        SUPCSellerMappingSRO supcSellerMappingSRO = new SUPCSellerMappingSRO(supc, sellerCode, CatalogType.PRODUCT);
        supcSellerMappingSROList.add(supcSellerMappingSRO);
        req.setSupcSellerMappings(supcSellerMappingSROList);
       
        try {

            resp = ipmsClientService.getInventoryPricingBySUPCSeller(req);

            if (!resp.isSuccessful() || resp.getValidationErrors().size() > 0) {

                LOG.info("VIPMS call getInventoryPricingBySUPCSeller was not successful[validation reason - {}; message - {}] for supc {}", resp.getIpmsResponseCode(),
                        resp.getMessage(), supc);
                throw new ExternalDataNotFoundException(ExternalDataNotFoundException.Type.DEFAULT, "unsuccessful response recieved from VIPMS");
            }
            
            
            Map<SUPCSellerMappingSRO, SUPCSellerInventoryPricingSRO> supcTopriceMap = resp.getSupcToInventoryPricingMap();
            if (supcTopriceMap == null || supcTopriceMap.isEmpty()) {
                LOG.info("VIPMS call getInventoryPricingBySUPCSeller returned  null map for supc {} seller {}", supc, sellerCode);
                throw new ExternalDataNotFoundException(ExternalDataNotFoundException.Type.DEFAULT, "null pricing details recieved from VIPMS");
            }
           
            SUPCSellerInventoryPricingSRO pricingSRO = supcTopriceMap.get(supcSellerMappingSRO);           
            if (pricingSRO == null) {
                LOG.info("VIPMS call getInventoryPricingBySUPCSeller returned  null prcing details for supc {} seller {}", supc, sellerCode);
                throw new ExternalDataNotFoundException(ExternalDataNotFoundException.Type.DEFAULT, "null pricing details recieved from VIPMS");

            }
            
            
            LOG.info("For supc {}, returning pricing details  {}", supc, pricingSRO);
            return pricingSRO;

        } catch (TransportException e) {

            LOG.info("VIPMS call getInventoryPricingBySUPCSeller threw TransportException for supc {} seller {}", supc, sellerCode);
            LOG.error("VIPMS call getInventoryPricingBySUPCSeller threw TransportException for supc", e);
            throw new ExternalDataNotFoundException(ExternalDataNotFoundException.Type.DEFAULT, "Error in getting pricing details from VIPMS");
        }

    }

}
