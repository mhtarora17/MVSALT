/*
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 27-Jan-2015
 *  @author gaurav
 */
package com.snapdeal.cocofs.external.service;

import com.snapdeal.score.model.request.UpdateProductFulfilmentAttributeRequest;

/**
 * Service for communication(data update/fetch/set) with Score
 * @author gaurav
 *
 */
public interface IScoreExternalService {
	
	public boolean updateProductFulfilmentAttributesInScore(UpdateProductFulfilmentAttributeRequest scoreUploadDtoRequest);

}
