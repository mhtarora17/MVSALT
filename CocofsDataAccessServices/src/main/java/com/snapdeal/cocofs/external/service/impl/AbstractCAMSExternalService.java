package com.snapdeal.cocofs.external.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.snapdeal.base.exception.TransportException;
import com.snapdeal.catalog.base.model.GetAllCategoryListRequest;
import com.snapdeal.catalog.base.model.GetCategoryListByIdListRequest;
import com.snapdeal.catalog.base.model.GetCategoryListResponse;
import com.snapdeal.catalog.base.model.GetMinProductInfoBySupcRequest;
import com.snapdeal.catalog.base.model.GetMinProductInfoResponse;
import com.snapdeal.catalog.base.model.GetPOGDetailListBySUPCListRequest;
import com.snapdeal.catalog.base.model.GetPOGDetailListResponse;
import com.snapdeal.cocofs.external.service.ICAMSExternalService;
import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.product.client.service.IProductClientService;

public abstract class AbstractCAMSExternalService implements ICAMSExternalService {

    @Autowired
    private IProductClientService productClientService;

    private static final Logger   LOG = LoggerFactory.getLogger(AbstractCAMSExternalService.class);

    public GetMinProductInfoResponse getMinProductInfoInternal(GetMinProductInfoBySupcRequest request) throws ExternalDataNotFoundException {
        try {
            return productClientService.getMinProductInfoBySupc(request);
        } catch (TransportException e) {

            LOG.error("Error in getting min product info from CAMS, ex message {}, ex cause {}", e.getMessage(), e.getCause());
            throw new ExternalDataNotFoundException(ExternalDataNotFoundException.Type.DEFAULT, "Error in getting min product info from CAMS for supc " + request.getSupc());

        }
    }

    public GetCategoryListResponse getAllCategoriesInternal(GetAllCategoryListRequest request) throws ExternalDataNotFoundException {
        try {

            return productClientService.getAllCategories(request);
        } catch (TransportException e) {

            LOG.error("Error in getting categories from CAMS, ex message {}, ex cause {}", e.getMessage(), e.getCause());
            throw new ExternalDataNotFoundException(ExternalDataNotFoundException.Type.DEFAULT, "Error in getting categories from CAMS");
        }
    }
    
    public GetCategoryListResponse getCategoryListByIdListInternal(GetCategoryListByIdListRequest request) throws ExternalDataNotFoundException{
    	try {

            return productClientService.getCategoryListByIdList(request);
        } catch (TransportException e) {

            LOG.error("Error in getting categories from CAMS, ex message {}, ex cause {}", e.getMessage(), e.getCause());
            throw new ExternalDataNotFoundException(ExternalDataNotFoundException.Type.DEFAULT, "Error in getting categories from CAMS");
        }
    }

    public GetPOGDetailListResponse getSimplePOGDetailListBySUPCList(GetPOGDetailListBySUPCListRequest request)throws ExternalDataNotFoundException{
	
	try {

            return productClientService.getSimplePOGDetailListBySUPCList(request);
        } catch (TransportException e) {

            LOG.error("Error in getting categories from CAMS, ex message {}, ex cause {}", e.getMessage(), e.getCause());
            throw new ExternalDataNotFoundException(ExternalDataNotFoundException.Type.DEFAULT, "Error in getting categories from CAMS");
        }
    }
    
    

}
