package com.snapdeal.cocofs.external.service;

import com.snapdeal.base.exception.TransportException;
import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.external.service.exception.ExternalDataUpdateFailedException;
import com.snapdeal.ops.base.api.getRateCard.GetRateCardRequest;
import com.snapdeal.ops.base.api.getRateCard.GetRateCardResponse;
import com.snapdeal.ops.base.api.getRateCardByCategory.GetRateCardByCategoryRequest;
import com.snapdeal.ops.base.api.getRateCardByCategory.GetRateCardByCategoryResponse;
import com.snapdeal.ops.base.api.isFulfillmentFeeChanged.IsFulfillmentFeeChangedRequest;
import com.snapdeal.ops.base.api.isFulfillmentFeeChanged.IsFulfillmentFeeChangedResponse;
import com.snapdeal.ops.base.api.isFulfillmentFeeChangedForSellers.IsFulfillmentFeeChangedForSellersRequest;
import com.snapdeal.ops.base.api.isFulfillmentFeeChangedForSellers.IsFulfillmentFeeChangedForSellersResponse;
import com.snapdeal.ops.base.api.updateRateCard.UpdateRateCardRequest;
import com.snapdeal.ops.base.api.updateRateCard.UpdateRateCardResponse;
import com.snapdeal.ops.base.api.updateRateCardByCategory.UpdateRateCardByCategoryRequest;
import com.snapdeal.ops.base.api.updateRateCardByCategory.UpdateRateCardByCategoryResponse;
import com.snapdeal.ops.base.exception.OPSException;

public interface IOPSExternalService {

    GetRateCardResponse getRateCard(GetRateCardRequest request) throws ExternalDataNotFoundException;

    UpdateRateCardResponse updateRateCard(UpdateRateCardRequest request) throws ExternalDataUpdateFailedException;
    
    public GetRateCardByCategoryResponse getRateCardByCategory(GetRateCardByCategoryRequest request) throws ExternalDataNotFoundException ;

    UpdateRateCardByCategoryResponse updateRateCardByCategory(UpdateRateCardByCategoryRequest request) throws ExternalDataUpdateFailedException;
    

    IsFulfillmentFeeChangedResponse isFulfillmentFeeChanged(IsFulfillmentFeeChangedRequest request) throws TransportException, OPSException;

    IsFulfillmentFeeChangedForSellersResponse isFulfillmentFeeChangedForSellers(IsFulfillmentFeeChangedForSellersRequest request) throws TransportException, OPSException;

	
}
