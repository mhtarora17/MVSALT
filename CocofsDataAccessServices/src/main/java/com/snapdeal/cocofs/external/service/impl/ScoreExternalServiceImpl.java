/*
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 27-Jan-2015
 *  @author gaurav
 */
package com.snapdeal.cocofs.external.service.impl;

import java.util.Iterator;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.base.validation.ValidationError;
import com.snapdeal.cocofs.converter.IConverterService;
import com.snapdeal.cocofs.external.service.IScoreExternalService;
import com.snapdeal.score.client.service.IScoreClientService;
import com.snapdeal.score.model.request.UpdateProductFulfilmentAttributeRequest;
import com.snapdeal.score.model.response.ApiErrorCodes;
import com.snapdeal.score.model.response.UpdateProductAttributeFulfilmentResponse;

@Service("ScoreExternalServiceImpl")
public class ScoreExternalServiceImpl implements IScoreExternalService {

    private static final Logger LOG = LoggerFactory.getLogger(ScoreExternalServiceImpl.class);

    @Autowired
    private IScoreClientService scoreClient;

    @Autowired
    private IConverterService   converterService;

    /*
     * Updates the product attributes in score as well in silent mode(no exceptions to be thrown back in case of failure)
     * In case of failure, adds an event to retry.
     * (non-Javadoc)
     * @see com.snapdeal.cocofs.external.service.IScoreExternalService#createProductFulfilmentAttributesInScore(com.snapdeal.cocofs.excelupload.ProductAttributeUploadDTO)
     */
    @Override
    public boolean updateProductFulfilmentAttributesInScore(UpdateProductFulfilmentAttributeRequest request) {

        boolean success = true;
        LOG.info("Updating product attributes in SCORE for request {}", request);
        try {
            UpdateProductAttributeFulfilmentResponse response = scoreClient.updateProductFulfilmentAttributes(request);
            LOG.info("Response {}", response);
            success = isRequestSuccessfull(request, response);
        } catch (Exception ex) {
            LOG.error("Unable to update product attributes in SCORE for request {}. Exception {}", request, ex);
            success = false;
        }
        LOG.info("returning {}", success);
        return success;
    }

    private boolean isRequestSuccessfull(UpdateProductFulfilmentAttributeRequest request, UpdateProductAttributeFulfilmentResponse response) {
        boolean success = true;
        if (!response.isSuccessful()) {
            LOG.error("Unable to update product attributes in SCORE for request {}. Response message {}", request, response.getMessage());
            success = false;
        } else if (response.getFailures() != null && !response.getFailures().isEmpty()) {
            LOG.error("Failures were encountered during update of product attributes in SCORE");
            Iterator<Entry<String, ValidationError>> itr = response.getFailures().entrySet().iterator();
            while (success && itr.hasNext()) {
                Entry<String, ValidationError> entry = itr.next();
                if (entry.getValue() == null) {
                    LOG.warn("Validation error in falure map is null for key/supc:{}", entry.getKey());
                    continue;
                }
                if (isRetryNeeded(entry.getValue().getCode())) {
                    LOG.warn("Retry needed for failure for supc:{}, code:{}, msg:{}", entry.getKey(), entry.getValue().getCode(), entry.getValue().getMessage());
                    success = false;
                } else {
                    LOG.warn("Retry not needed for failure for supc:{}, code:{}, msg:{}", entry.getKey(), entry.getValue().getCode(), entry.getValue().getMessage());
                }
            }
        }
        return success;
    }

    private boolean isRetryNeeded(int code) {
        return (code == ApiErrorCodes.INTERNAL_ERROR.code() || code == ApiErrorCodes.DATA_NOT_PERSISTED.code() || code == ApiErrorCodes.UNKNOWN_ERROR.code());
    }
}
