package com.snapdeal.cocofs.external.service.impl;

import org.springframework.stereotype.Service;

import com.snapdeal.base.exception.TransportException;
import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.external.service.exception.ExternalDataUpdateFailedException;
import com.snapdeal.ops.base.api.getRateCard.GetRateCardRequest;
import com.snapdeal.ops.base.api.getRateCard.GetRateCardResponse;
import com.snapdeal.ops.base.api.getRateCardByCategory.GetRateCardByCategoryRequest;
import com.snapdeal.ops.base.api.getRateCardByCategory.GetRateCardByCategoryResponse;
import com.snapdeal.ops.base.api.isFulfillmentFeeChanged.IsFulfillmentFeeChangedRequest;
import com.snapdeal.ops.base.api.isFulfillmentFeeChanged.IsFulfillmentFeeChangedResponse;
import com.snapdeal.ops.base.api.isFulfillmentFeeChangedForSellers.IsFulfillmentFeeChangedForSellersRequest;
import com.snapdeal.ops.base.api.isFulfillmentFeeChangedForSellers.IsFulfillmentFeeChangedForSellersResponse;
import com.snapdeal.ops.base.api.updateRateCard.UpdateRateCardRequest;
import com.snapdeal.ops.base.api.updateRateCard.UpdateRateCardResponse;
import com.snapdeal.ops.base.api.updateRateCardByCategory.UpdateRateCardByCategoryRequest;
import com.snapdeal.ops.base.api.updateRateCardByCategory.UpdateRateCardByCategoryResponse;
import com.snapdeal.ops.base.exception.OPSException;

@Service("opsExternalService")
public class OPSExternalServiceImpl extends AbstractOPSExternalService {

    @Override
    public GetRateCardResponse getRateCard(GetRateCardRequest request) throws ExternalDataNotFoundException {
        return getRateCardInternal(request);
    }

    @Override
    public UpdateRateCardResponse updateRateCard(UpdateRateCardRequest request) throws ExternalDataUpdateFailedException {
        return updateRateCardInternal(request);
    }
    
    @Override
    public UpdateRateCardByCategoryResponse updateRateCardByCategory(UpdateRateCardByCategoryRequest request) throws ExternalDataUpdateFailedException {
        return updateRateCardByCategoryInternal(request);
    }
    
    @Override
    public GetRateCardByCategoryResponse getRateCardByCategory(GetRateCardByCategoryRequest request) throws ExternalDataNotFoundException {
        return getRateCardByCategoryInternal(request);
    }

    @Override
    public IsFulfillmentFeeChangedResponse isFulfillmentFeeChanged(IsFulfillmentFeeChangedRequest request) throws TransportException, OPSException {
        return isFulfillmentFeeChangedInternal(request);
    }

    @Override
    public IsFulfillmentFeeChangedForSellersResponse isFulfillmentFeeChangedForSellers(IsFulfillmentFeeChangedForSellersRequest request) throws TransportException, OPSException {
        return isFulfillmentFeeChangedForSellersInternal(request);
    }
    
    
}
