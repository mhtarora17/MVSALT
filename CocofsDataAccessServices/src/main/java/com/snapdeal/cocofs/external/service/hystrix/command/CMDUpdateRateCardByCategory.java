package com.snapdeal.cocofs.external.service.hystrix.command;

import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;
import com.snapdeal.cocofs.external.service.impl.AbstractOPSExternalService;
import com.snapdeal.ops.base.api.updateRateCardByCategory.UpdateRateCardByCategoryRequest;
import com.snapdeal.ops.base.api.updateRateCardByCategory.UpdateRateCardByCategoryResponse;

public class CMDUpdateRateCardByCategory extends HystrixCommand<UpdateRateCardByCategoryResponse> {

    private AbstractOPSExternalService opsExternalService;
    private UpdateRateCardByCategoryRequest request;

    public CMDUpdateRateCardByCategory(AbstractOPSExternalService opsExternalService, UpdateRateCardByCategoryRequest request) {
        super(HystrixCommandGroupKey.Factory.asKey("updateRateCardByCategory"));
        this.opsExternalService = opsExternalService;
        this.request   = request;
    }

    @Override
    protected UpdateRateCardByCategoryResponse run() throws Exception {
        return opsExternalService.updateRateCardByCategoryInternal(request);
    }
}
