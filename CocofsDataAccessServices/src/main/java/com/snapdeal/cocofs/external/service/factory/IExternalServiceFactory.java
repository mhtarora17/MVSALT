package com.snapdeal.cocofs.external.service.factory;

import com.snapdeal.cocofs.external.service.ICAMSExternalService;
import com.snapdeal.cocofs.external.service.IOPSExternalService;
import com.snapdeal.cocofs.external.service.IVIPMSExternalService;

/**
 * 
 * @author nikhil
 *
 */
public interface IExternalServiceFactory {

    public ICAMSExternalService getCAMSExternalService();

    public IOPSExternalService getOPSExternalService();

    public IVIPMSExternalService getVIPMSPSExternalService();

}
