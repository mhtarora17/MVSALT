package com.snapdeal.cocofs.external.service.impl;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.snapdeal.catalog.base.model.GetAllCategoryListRequest;
import com.snapdeal.catalog.base.model.GetCategoryListByIdListRequest;
import com.snapdeal.catalog.base.model.GetCategoryListResponse;
import com.snapdeal.catalog.base.model.GetMinProductInfoBySupcRequest;
import com.snapdeal.catalog.base.model.GetMinProductInfoResponse;
import com.snapdeal.catalog.base.model.GetPOGDetailListBySUPCListRequest;
import com.snapdeal.catalog.base.model.GetPOGDetailListResponse;
import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;

/**
 * @author nikhil
 */
@Service("camsExternalService")
public class CAMSExternalServiceImpl extends AbstractCAMSExternalService {

    @Override
    @Cacheable(value="productInfoCache", key="#request.supc")
    public GetMinProductInfoResponse getMinProductInfo(GetMinProductInfoBySupcRequest request) throws ExternalDataNotFoundException {
        return getMinProductInfoInternal(request);
    }

    @Override
    public GetCategoryListResponse getAllCategories(GetAllCategoryListRequest request) throws ExternalDataNotFoundException {
        return getAllCategoriesInternal(request);
    }
    
    @Override
    public GetCategoryListResponse getCategoryListByIdList(GetCategoryListByIdListRequest request) throws ExternalDataNotFoundException{
    	return getCategoryListByIdListInternal(request);
    }

    @Override
    public GetPOGDetailListResponse getPOGDetailsBbySupc(GetPOGDetailListBySUPCListRequest request)throws ExternalDataNotFoundException {
	// TODO Auto-generated method stub
	return getSimplePOGDetailListBySUPCList(request);
    }


}
