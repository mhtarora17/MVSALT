/**
 * 
 */
package com.snapdeal.cocofs.external.service.impl;

import java.util.List;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.external.service.hystrix.command.CMDGetAllSellerCodesForSUPC;
import com.snapdeal.cocofs.external.service.hystrix.command.CMDGetInventoryPricingBySUPCSeller;
import com.snapdeal.ipms.base.api.getInventoryPricingBySUPCSeller.v1.SUPCSellerInventoryPricingSRO;

/**
 * @author abhinav
 *
 */

@Service("hystrixVIPMSExternalService")
public class HystrixVIPMSExternalServiceImpl extends AbstractVIPMSExternalService {

    
    @Override
    @Cacheable(value="supcSellerCache", key="#supc")
    public List<String> getAllSellerCodesForSUPC(String supc) {

        CMDGetAllSellerCodesForSUPC command = new CMDGetAllSellerCodesForSUPC(this, supc);
        return command.execute();
    }
    
    @Override
    @Cacheable(value="supcSellerPricingCache", key="#supc('-').concat(#sellerCode)")
    public SUPCSellerInventoryPricingSRO getInventoryPricingBySUPCSeller(String supc, String sellerCode) throws ExternalDataNotFoundException{
        CMDGetInventoryPricingBySUPCSeller command = new CMDGetInventoryPricingBySUPCSeller(this, supc, sellerCode);
        return command.execute();
    }
    

}
