package com.snapdeal.cocofs.external.service.exception;

public class ExternalDataUpdateFailedException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public enum Type {
        DEFAULT("default");

        private String code;

        private Type(String code) {
            this.code = code;
        }

        public String code() {
            return this.code;
        }
    }

    Type errorType;

    public ExternalDataUpdateFailedException(Type type, String message) {
        super(message);
        errorType = type;
    }

    public ExternalDataUpdateFailedException(String message) {
        super(message);
    }

    public Type getErrorType() {
        return errorType;
    }

    public void setErrorType(Type errorType) {
        this.errorType = errorType;
    }

}
