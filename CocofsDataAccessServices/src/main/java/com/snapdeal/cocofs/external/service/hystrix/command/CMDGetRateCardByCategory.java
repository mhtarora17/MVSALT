package com.snapdeal.cocofs.external.service.hystrix.command;

import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;
import com.snapdeal.cocofs.external.service.impl.AbstractOPSExternalService;
import com.snapdeal.ops.base.api.getRateCardByCategory.GetRateCardByCategoryRequest;
import com.snapdeal.ops.base.api.getRateCardByCategory.GetRateCardByCategoryResponse;

public class CMDGetRateCardByCategory extends HystrixCommand<GetRateCardByCategoryResponse> {

    private AbstractOPSExternalService opsExternalService;
    private GetRateCardByCategoryRequest request;

    public CMDGetRateCardByCategory(AbstractOPSExternalService opsExternalService, GetRateCardByCategoryRequest request) {
        super(HystrixCommandGroupKey.Factory.asKey("getRateCardByCategory"));
        this.opsExternalService = opsExternalService;
        this.request   = request;
    }

    @Override
    protected GetRateCardByCategoryResponse run() throws Exception {
        return opsExternalService.getRateCardByCategoryInternal(request);
    }

}
