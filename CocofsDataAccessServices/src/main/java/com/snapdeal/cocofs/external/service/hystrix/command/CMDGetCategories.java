package com.snapdeal.cocofs.external.service.hystrix.command;

import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;
import com.snapdeal.catalog.base.model.GetAllCategoryListRequest;
import com.snapdeal.catalog.base.model.GetCategoryListResponse;
import com.snapdeal.cocofs.external.service.impl.AbstractCAMSExternalService;

/**
 * @author nikhil
 */
public class CMDGetCategories extends HystrixCommand<GetCategoryListResponse> {

    AbstractCAMSExternalService camsExternalService;

    public CMDGetCategories(AbstractCAMSExternalService camsExternalService) {
        super(HystrixCommandGroupKey.Factory.asKey("subCatbySupc"));
        this.camsExternalService = camsExternalService;
    }

    @Override
    protected GetCategoryListResponse run() throws Exception {
        return camsExternalService.getAllCategoriesInternal(new GetAllCategoryListRequest());
    }

}
