/**
 * 
 */
package com.snapdeal.cocofs.external.service.impl;

import java.util.List;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.ipms.base.api.getInventoryPricingBySUPCSeller.v1.SUPCSellerInventoryPricingSRO;

/**
 * @author abhinav
 *
 */

@Service("vipmsExternalService")
public class VIPMSExternalServiceImpl extends AbstractVIPMSExternalService {

    
    @Override
    @Cacheable(value="supcSellerCache", key="#supc")
    public List<String> getAllSellerCodesForSUPC(String supc) {

        return getAllSellerCodesForSUPCInternal(supc);
    }
    
    @Override
    @Cacheable(value="supcSellerPricingCache", key="#supc.concat('-').concat(#sellerCode)")
    public SUPCSellerInventoryPricingSRO getInventoryPricingBySUPCSeller(String supc, String sellerCode) throws ExternalDataNotFoundException {

        return getInventoryPricingBySUPCSellerInternal(supc, sellerCode);
    }


}
