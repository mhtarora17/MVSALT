package com.snapdeal.cocofs.external.service.factory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.external.service.ICAMSExternalService;
import com.snapdeal.cocofs.external.service.IOPSExternalService;
import com.snapdeal.cocofs.external.service.IVIPMSExternalService;

/**
 * @author nikhil
 */
@Service("externalServiceFactory")
public class ExternalServiceFactory implements IExternalServiceFactory {

    @Autowired
    @Qualifier("hystrixCamsExternalService")
    private ICAMSExternalService hystrixCamsExternalService;

    @Autowired
    @Qualifier("camsExternalService")
    private ICAMSExternalService camsExternalService;

    @Autowired
    @Qualifier("hystrixOPSExternalService")
    private IOPSExternalService  hystrixOpsExternalService;

    @Autowired
    @Qualifier("opsExternalService")
    private IOPSExternalService  opsExternalService;
    
    @Autowired
    @Qualifier("vipmsExternalService")
    private IVIPMSExternalService       vipmsService;
    
    @Autowired
    @Qualifier("hystrixVIPMSExternalService")
    private IVIPMSExternalService       hystrixVipmsService;

    @Override
    public ICAMSExternalService getCAMSExternalService() {
        return ConfigUtils.getBooleanScalar(Property.HYSTRIX_SWITCH) ? hystrixCamsExternalService : camsExternalService;
    }

    @Override
    public IOPSExternalService getOPSExternalService() {
        return ConfigUtils.getBooleanScalar(Property.HYSTRIX_SWITCH) ? hystrixOpsExternalService : opsExternalService;
    }
    
    
    @Override
    public IVIPMSExternalService getVIPMSPSExternalService() {
        return ConfigUtils.getBooleanScalar(Property.HYSTRIX_SWITCH) ? hystrixVipmsService : vipmsService;
    }
    

}
