package com.snapdeal.cocofs.access;

import java.util.List;

import com.snapdeal.cocofs.mongo.model.ProductAttributeNotificationUnit;

/**
 * @author nikhil
 */
public interface IProductAttributeNotificationMongoDataReadService {

    /**
     * @param seller
     * @return
     */
    public List<ProductAttributeNotificationUnit> getProductAttributeNotification(String seller);

    /**
     * @param seller
     * @param supc
     * @return
     */
    public ProductAttributeNotificationUnit getProductAttributeNotification(String seller, String supc);

    /**
     * @param seller
     * @return
     */
    public List<ProductAttributeNotificationUnit> getLiveProductAttributeNotification(String seller);

    /**
     * @param seller
     * @param supc
     * @param firstRow
     * @param maxSize
     * @return
     */
    List<ProductAttributeNotificationUnit> getLivePaginatedProductAttributeNotificationUnit(String seller, String supc, Integer firstRow, Integer maxSize);

    /**
     * @param seller
     * @param firstRow
     * @param maxSize
     * @return
     */
    List<ProductAttributeNotificationUnit> getLivePaginatedAndSortedProductAttributeNotificationUnit(String seller, Integer firstRow, Integer maxSize);

    /**
     * @param sellerCode
     * @return
     */
    public Long getTotalLiveProductAttributeNotificationCount(String sellerCode);

    ProductAttributeNotificationUnit getVisibleProductAttributeNotificationBySUPC(String supc);

    public void savePAN(ProductAttributeNotificationUnit productAttributeNotificationUnit);
}
