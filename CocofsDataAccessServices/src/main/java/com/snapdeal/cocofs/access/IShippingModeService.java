/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 16, 2013
 *  @author himanshu
 */
package com.snapdeal.cocofs.access;

import java.util.List;
import java.util.Map;

import com.snapdeal.cocofs.sro.CategoryShippingModeSRO;
import com.snapdeal.cocofs.sro.SupcShippingModeSRO;

public interface IShippingModeService {

    List<CategoryShippingModeSRO> getAllCategoryModeMapping();

    List<SupcShippingModeSRO> getAllSupcModeMapping();

    Map<String, CategoryShippingModeSRO> getCategoryModeMapping(List<String> categories);

    Map<String, SupcShippingModeSRO> getSupcModeMapping(List<String> supcs);

    boolean addOrUpdateCategoryModeMapping(CategoryShippingModeSRO x);

    boolean addOrUpdateSupcModeMapping(SupcShippingModeSRO x);

    boolean removeCategoryModeMapping(CategoryShippingModeSRO x);

    boolean removeSupcModeMapping(SupcShippingModeSRO x);

}
