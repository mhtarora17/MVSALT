package com.snapdeal.cocofs.access.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.cocofs.access.IProductAttributeNotificationMongoDataReadService;
import com.snapdeal.cocofs.mongo.CocofsMongoQuery;
import com.snapdeal.cocofs.mongo.Criteria;
import com.snapdeal.cocofs.mongo.PageInfo;
import com.snapdeal.cocofs.mongo.SortCriteria;
import com.snapdeal.cocofs.mongo.mao.IGenericMao;
import com.snapdeal.cocofs.mongo.model.ProductAttributeNotificationUnit;
import com.snapdeal.cocofs.mongo.model.ProductAttributeNotificationUnit.PANUFieldNames;

/**
 * @author nikhil
 */
@Service("productAttributeNotificationMongoReadService")
public class ProductAttributeNotificationMongoReadService implements IProductAttributeNotificationMongoDataReadService {

    @Autowired
    IGenericMao genericMao;

    @Override
    public List<ProductAttributeNotificationUnit> getProductAttributeNotification(String seller) {
        List<Criteria> queryCriterias = new ArrayList<Criteria>();
        queryCriterias.add(new Criteria(ProductAttributeNotificationUnit.PANUFieldNames.SELLERS.getCode(), seller, CocofsMongoQuery.Operator.IN));
        return genericMao.getDocumentList(new CocofsMongoQuery(queryCriterias), ProductAttributeNotificationUnit.class);
    }

    @Override
    public List<ProductAttributeNotificationUnit> getLiveProductAttributeNotification(String seller) {
        List<Criteria> queryCriterias = new ArrayList<Criteria>();
        //TODO: ADD VISIBLE TRUE
        queryCriterias.add(new Criteria(ProductAttributeNotificationUnit.PANUFieldNames.SELLERS.getCode(), seller, CocofsMongoQuery.Operator.IN));
        queryCriterias.add(new Criteria(ProductAttributeNotificationUnit.PANUFieldNames.NOTIFICATION_END_DATE.getCode(), DateUtils.getCurrentTime(), CocofsMongoQuery.Operator.LTE));
        return genericMao.getDocumentList(new CocofsMongoQuery(queryCriterias), ProductAttributeNotificationUnit.class);
    }

    @Override
    public ProductAttributeNotificationUnit getProductAttributeNotification(String seller, String supc) {
        List<Criteria> queryCriterias = new ArrayList<Criteria>();
        queryCriterias.add(new Criteria(ProductAttributeNotificationUnit.PANUFieldNames.SELLERS.getCode(), seller, CocofsMongoQuery.Operator.IN));
        queryCriterias.add(new Criteria(ProductAttributeNotificationUnit.PANUFieldNames.SUPC.getCode(), supc, CocofsMongoQuery.Operator.EQUAL));
        return genericMao.getDocument(new CocofsMongoQuery(queryCriterias), ProductAttributeNotificationUnit.class);
    }

    @Override
    public List<ProductAttributeNotificationUnit> getLivePaginatedProductAttributeNotificationUnit(String seller, String supc, Integer firstRow, Integer maxSize) {
        List<Criteria> queryCriterias = new ArrayList<Criteria>();
        queryCriterias.add(new Criteria(ProductAttributeNotificationUnit.PANUFieldNames.SELLERS.getCode(), seller, CocofsMongoQuery.Operator.IN));
        queryCriterias.add(new Criteria(ProductAttributeNotificationUnit.PANUFieldNames.SUPC.getCode(), supc, CocofsMongoQuery.Operator.EQUAL));
        return genericMao.getDocumentList(new CocofsMongoQuery(queryCriterias, new PageInfo(firstRow, maxSize)), ProductAttributeNotificationUnit.class);
    }

    @Override
    public List<ProductAttributeNotificationUnit> getLivePaginatedAndSortedProductAttributeNotificationUnit(String seller, Integer pageNum, Integer pageSize) {
        List<Criteria> queryCriterias = new ArrayList<Criteria>();
        queryCriterias.add(new Criteria(ProductAttributeNotificationUnit.PANUFieldNames.SELLERS.getCode(), seller, CocofsMongoQuery.Operator.IN));
        queryCriterias.add(new Criteria(ProductAttributeNotificationUnit.PANUFieldNames.NOTIFICATION_END_DATE.getCode(), DateUtils.getCurrentTime(), CocofsMongoQuery.Operator.GTE));
        queryCriterias.add(new Criteria(ProductAttributeNotificationUnit.PANUFieldNames.VISIBLE.getCode(), true, CocofsMongoQuery.Operator.EQUAL));
        SortCriteria sc = new SortCriteria(SortCriteria.Sort.ASC, PANUFieldNames.WITH_EFFECT_FROM.getCode());
        return genericMao.getDocumentList(new CocofsMongoQuery(queryCriterias, new PageInfo(pageNum, pageSize, sc)), ProductAttributeNotificationUnit.class);
    }

    @Override
    public Long getTotalLiveProductAttributeNotificationCount(String sellerCode) {
        List<Criteria> queryCriterias = new ArrayList<Criteria>();
        queryCriterias.add(new Criteria(ProductAttributeNotificationUnit.PANUFieldNames.SELLERS.getCode(), sellerCode, CocofsMongoQuery.Operator.IN));
        queryCriterias.add(new Criteria(ProductAttributeNotificationUnit.PANUFieldNames.NOTIFICATION_END_DATE.getCode(), DateUtils.getCurrentTime(), CocofsMongoQuery.Operator.GTE));
        queryCriterias.add(new Criteria(ProductAttributeNotificationUnit.PANUFieldNames.VISIBLE.getCode(), true, CocofsMongoQuery.Operator.EQUAL));
        return genericMao.getDocumentCount(new CocofsMongoQuery(queryCriterias), ProductAttributeNotificationUnit.class);
    }

    @Override
    public ProductAttributeNotificationUnit getVisibleProductAttributeNotificationBySUPC(String supc) {

        List<Criteria> queryCriterias = new ArrayList<Criteria>();

        queryCriterias.add(new Criteria(ProductAttributeNotificationUnit.PANUFieldNames.VISIBLE.getCode(), true, CocofsMongoQuery.Operator.EQUAL));
        queryCriterias.add(new Criteria(ProductAttributeNotificationUnit.PANUFieldNames.SUPC.getCode(), supc, CocofsMongoQuery.Operator.EQUAL));

        return genericMao.getDocument(new CocofsMongoQuery(queryCriterias), ProductAttributeNotificationUnit.class);
    }

    @Override
    public void savePAN(ProductAttributeNotificationUnit productAttributeNotificationUnit) {
        genericMao.saveDocument(productAttributeNotificationUnit);
    }

}
