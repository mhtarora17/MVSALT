package com.snapdeal.cocofs.access.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.cocofs.access.IProductAttributeNotificationDBDataReadService;
import com.snapdeal.cocofs.db.dao.IProductAttributeNotificationDAO;
import com.snapdeal.cocofs.entity.ProductAttributeNotification;

/**
 * @author nikhil
 */
@Service("productAttributeNotificationService")
@Transactional(readOnly = true)
public class ProductAttributeNotificationDBDataReadService implements IProductAttributeNotificationDBDataReadService {

    @Autowired
    IProductAttributeNotificationDAO productAttributeNotificationDAO;

    @Override
    public List<ProductAttributeNotification> getVisibleProductAttributeNotifications(String supc) {
        if (StringUtils.isEmpty(supc)) {
            throw new IllegalArgumentException("Input parameter supc is found to be null");
        }

        return productAttributeNotificationDAO.getVisibleProductAttributeNotification(supc);
    }
}
