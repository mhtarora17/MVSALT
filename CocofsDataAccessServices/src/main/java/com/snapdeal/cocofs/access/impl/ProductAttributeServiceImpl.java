/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 09-Sep-2013
 *  @author himanshum
 */
package com.snapdeal.cocofs.access.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.snapdeal.cocofs.access.IProductAttributeService;
import com.snapdeal.cocofs.db.dao.IProductAttributeDao;
import com.snapdeal.cocofs.entity.ProductAttribute;
import com.snapdeal.cocofs.mongo.mao.IProductAttributeUnitMao;
import com.snapdeal.cocofs.mongo.model.ProductAttributeUnit;
import com.snapdeal.cocofs.mongo.model.ProductAttributeUnit.PAUFieldNames;

@Service("productAttributeSercvice")
public class ProductAttributeServiceImpl implements IProductAttributeService {

    @Autowired
    private IProductAttributeDao     productDao;

    @Autowired
    private IProductAttributeUnitMao productMao;

    @Override
    @Transactional(readOnly = true)
    public List<ProductAttribute> getAllProductAttributeForSUPC(String supc) {
        return productDao.getAllProductAttributeForSUPC(supc);
    }

    @Override
    public ProductAttributeUnit getProductAttributeUnitBySUPC(String supc) {
        Map<PAUFieldNames, Object> fieldListToValueMap = new HashMap<ProductAttributeUnit.PAUFieldNames, Object>();
        fieldListToValueMap.put(PAUFieldNames.SUPC, supc);
        return productMao.getUniqueProductAttributeUnit(fieldListToValueMap);
    }

}
