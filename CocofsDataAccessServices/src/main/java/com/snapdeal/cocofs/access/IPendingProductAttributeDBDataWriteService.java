package com.snapdeal.cocofs.access;

import java.util.List;

import com.snapdeal.cocofs.entity.PendingProductAttributeUpdate;

/**
 * 
 * @author nikhil
 *
 */
public interface IPendingProductAttributeDBDataWriteService {

    void updatePPAUs(List<PendingProductAttributeUpdate> updates);

    void updatePPAU(PendingProductAttributeUpdate update);

}
