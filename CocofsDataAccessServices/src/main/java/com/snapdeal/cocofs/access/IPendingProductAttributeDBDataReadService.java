package com.snapdeal.cocofs.access;

import java.util.Date;
import java.util.List;

import com.snapdeal.cocofs.entity.PendingProductAttributeUpdate;

/**
 * @author nikhil
 */
public interface IPendingProductAttributeDBDataReadService {

    List<PendingProductAttributeUpdate> getPendingProductAtributeUpdate(String supc);

    List<PendingProductAttributeUpdate> getAllPendingProductAttributes(Date startDate, Date endDate);
    
    List<PendingProductAttributeUpdate> getProductSystemWeightCapturedFlag(String supc);

}
