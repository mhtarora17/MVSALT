/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 16, 2013
 *  @author himanshu
 */
package com.snapdeal.cocofs.access.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.access.IShippingModeService;
import com.snapdeal.cocofs.mongo.CocofsMongoQuery;
import com.snapdeal.cocofs.mongo.Criteria;
import com.snapdeal.cocofs.mongo.mao.IGenericMao;
import com.snapdeal.cocofs.mongo.model.CategoryShippingMode;
import com.snapdeal.cocofs.mongo.model.CategoryShippingMode.CSMFieldNames;
import com.snapdeal.cocofs.mongo.model.SupcShippingMode;
import com.snapdeal.cocofs.mongo.model.SupcShippingMode.SSMFieldNames;
import com.snapdeal.cocofs.sro.CategoryShippingModeSRO;
import com.snapdeal.cocofs.sro.SupcShippingModeSRO;

@Service("shippingModeServiceImpl")
public class ShippingModeServiceImpl implements IShippingModeService {
    
    @Autowired
    private IGenericMao genericMao;

    private CategoryShippingModeSRO getEmptyCategoryShippingModeSRO(String categoryUrl) {
        CategoryShippingModeSRO sro = new CategoryShippingModeSRO();
        sro.setCategoryUrl(categoryUrl);
        sro.setShippingMode(new ArrayList<String>());
        return sro;
    }
    
    private SupcShippingModeSRO getEmptySupcShippingModeSRO(String supc) {
        SupcShippingModeSRO sro = new SupcShippingModeSRO();
        sro.setSupc(supc);
        sro.setShippingMode(new ArrayList<String>());
        return sro;
    }
    
    @Override
    public List<CategoryShippingModeSRO> getAllCategoryModeMapping() {
        List<CategoryShippingModeSRO> outList = new ArrayList<CategoryShippingModeSRO>();
        List<Criteria> queryCriterias = new ArrayList<Criteria>();
        Map<String, CategoryShippingModeSRO> map = new HashMap<String, CategoryShippingModeSRO>();
        List<CategoryShippingMode> list = genericMao.getDocumentList(new CocofsMongoQuery(queryCriterias), CategoryShippingMode.class);
        if ( null == list ) {
            return outList;
        }
        for ( CategoryShippingMode m : list ) {
            if ( ! map.containsKey(m.getCategoryUrl())) {
                CategoryShippingModeSRO l = getEmptyCategoryShippingModeSRO(m.getCategoryUrl());
                outList.add(l );
                map.put(m.getCategoryUrl(), l);
            }
            CategoryShippingModeSRO sro = map.get(m.getCategoryUrl());
            sro.getShippingMode().add(m.getShippingMode());
        }
        return outList;
    }

    

    @Override
    public List<SupcShippingModeSRO> getAllSupcModeMapping() {
        List<SupcShippingModeSRO> outList = new ArrayList<SupcShippingModeSRO>();
        List<Criteria> queryCriterias = new ArrayList<Criteria>();
        Map<String, SupcShippingModeSRO> map = new HashMap<String, SupcShippingModeSRO>();
        List<SupcShippingMode> list = genericMao.getDocumentList(new CocofsMongoQuery(queryCriterias), SupcShippingMode.class);
        if ( null == list ) {
            return outList;
        }
        for ( SupcShippingMode m : list ) {
            if ( ! map.containsKey(m.getSupc())) {
                SupcShippingModeSRO l = getEmptySupcShippingModeSRO(m.getSupc());
                outList.add(l );
                map.put(m.getSupc(), l);
            }
            SupcShippingModeSRO sro = map.get(m.getSupc());
            sro.getShippingMode().add(m.getShippingMode());
        }
        return outList;
    }

    

    @Override
    public Map<String, CategoryShippingModeSRO> getCategoryModeMapping(List<String> categories) {
        Map<String, CategoryShippingModeSRO> map = new HashMap<String, CategoryShippingModeSRO>();
        
        for (String categoryUrl : categories) {
            List<Criteria> queryCriterias = new ArrayList<Criteria>();
            queryCriterias.add(new Criteria(CSMFieldNames.SUBCAT.getCode(), categoryUrl, CocofsMongoQuery.Operator.EQUAL));
            
            List<CategoryShippingMode> list = genericMao.getDocumentList(new CocofsMongoQuery(queryCriterias), CategoryShippingMode.class);
            if ( null == list ) {
                continue;
            }
            for ( CategoryShippingMode m : list ) {
                if ( ! map.containsKey(m.getCategoryUrl())) {
                    CategoryShippingModeSRO l = getEmptyCategoryShippingModeSRO(m.getCategoryUrl());
                    map.put(m.getCategoryUrl(), l);
                }
                CategoryShippingModeSRO sro = map.get(m.getCategoryUrl());
                sro.getShippingMode().add(m.getShippingMode());
            }
        }
        
        
        return map;
    }

    @Override
    public Map<String, SupcShippingModeSRO> getSupcModeMapping(List<String> supcs) {
        Map<String, SupcShippingModeSRO> map = new HashMap<String, SupcShippingModeSRO>();
        
        for (String supc : supcs ) {
            List<Criteria> queryCriterias = new ArrayList<Criteria>();

            queryCriterias.add(new Criteria(SSMFieldNames.SUPC.getCode(), supc, CocofsMongoQuery.Operator.EQUAL));
            List<SupcShippingMode> list = genericMao.getDocumentList(new CocofsMongoQuery(queryCriterias), SupcShippingMode.class);
            if ( null == list ) {
                continue;
            }
            for ( SupcShippingMode m : list ) {
                if ( ! map.containsKey(m.getSupc())) {
                    SupcShippingModeSRO l = getEmptySupcShippingModeSRO(m.getSupc());
                    map.put(m.getSupc(), l);
                }
                SupcShippingModeSRO sro = map.get(m.getSupc());
                sro.getShippingMode().add(m.getShippingMode());
            }
        }

        return map;
    }

    @Override
    public boolean addOrUpdateCategoryModeMapping(CategoryShippingModeSRO x) {
        for ( String mode : x.getShippingMode() ) {
            List<Criteria> queryCriterias = new ArrayList<Criteria>();
            queryCriterias.add(new Criteria(CSMFieldNames.SUBCAT.getCode(), x.getCategoryUrl(), CocofsMongoQuery.Operator.EQUAL));
            queryCriterias.add(new Criteria(CSMFieldNames.MODE.getCode(), mode, CocofsMongoQuery.Operator.EQUAL));
            List<CategoryShippingMode> list = genericMao.getDocumentList(new CocofsMongoQuery(queryCriterias), CategoryShippingMode.class);
            if ( (null == list) || list.isEmpty() ) {
                CategoryShippingMode newEntry = new CategoryShippingMode();
                newEntry.setCategoryUrl(x.getCategoryUrl());
                newEntry.setShippingMode(mode);
                genericMao.saveDocument(newEntry);
            }
        }
        return true;
    }

    @Override
    public boolean addOrUpdateSupcModeMapping(SupcShippingModeSRO x) {
        for ( String mode : x.getShippingMode() ) {
            List<Criteria> queryCriterias = new ArrayList<Criteria>();
            queryCriterias.add(new Criteria(SSMFieldNames.SUPC.getCode(), x.getSupc(), CocofsMongoQuery.Operator.EQUAL));
            queryCriterias.add(new Criteria(CSMFieldNames.MODE.getCode(), mode, CocofsMongoQuery.Operator.EQUAL));
            List<SupcShippingMode> list = genericMao.getDocumentList(new CocofsMongoQuery(queryCriterias), SupcShippingMode.class);
            if ( (null == list) || list.isEmpty() ) {
                SupcShippingMode newEntry = new SupcShippingMode();
                newEntry.setSupc(x.getSupc());
                newEntry.setShippingMode(mode);
                genericMao.saveDocument(newEntry);
            }
        }
        return true;
    }

    @Override
    public boolean removeCategoryModeMapping(CategoryShippingModeSRO x) {
        for ( String mode : x.getShippingMode() ) {
            List<Criteria> queryCriterias = new ArrayList<Criteria>();
            queryCriterias.add(new Criteria(CSMFieldNames.SUBCAT.getCode(), x.getCategoryUrl(), CocofsMongoQuery.Operator.EQUAL));
            queryCriterias.add(new Criteria(CSMFieldNames.MODE.getCode(), mode, CocofsMongoQuery.Operator.EQUAL));
            List<CategoryShippingMode> list = genericMao.getDocumentList(new CocofsMongoQuery(queryCriterias), CategoryShippingMode.class);
            if ( null != list ) {
                for ( CategoryShippingMode l : list ) {
                    genericMao.deleteDocument(l);
                }
            }
        }
        return true;
    }

    @Override
    public boolean removeSupcModeMapping(SupcShippingModeSRO x) {
        for ( String mode : x.getShippingMode() ) {
            List<Criteria> queryCriterias = new ArrayList<Criteria>();
            queryCriterias.add(new Criteria(SSMFieldNames.SUPC.getCode(), x.getSupc(), CocofsMongoQuery.Operator.EQUAL));
            queryCriterias.add(new Criteria(CSMFieldNames.MODE.getCode(), mode, CocofsMongoQuery.Operator.EQUAL));
            List<SupcShippingMode> list = genericMao.getDocumentList(new CocofsMongoQuery(queryCriterias), SupcShippingMode.class);
            if ( null != list ) {
                for ( SupcShippingMode l : list ) {
                    genericMao.deleteDocument(l);
                }
                
            }
        }
        return true;
    }

}
