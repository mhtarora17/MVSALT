package com.snapdeal.cocofs.access;

import java.util.List;

import com.snapdeal.cocofs.entity.ProductAttributeNotification;

public interface IProductAttributeNotificationDBDataReadService {

    public List<ProductAttributeNotification> getVisibleProductAttributeNotifications(String supc);

}
