package com.snapdeal.cocofs.access.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.snapdeal.cocofs.access.IPendingProductAttributeDBDataWriteService;
import com.snapdeal.cocofs.db.dao.IPendingProductAttributeDAO;
import com.snapdeal.cocofs.entity.PendingProductAttributeUpdate;

/**
 * @author nikhil
 */
@Service("pendingProductAttributeDBDataWriteService")
public class PendingProductAttributeDBDataWriteService implements IPendingProductAttributeDBDataWriteService {

    @Autowired
    IPendingProductAttributeDAO pendingProductAttributeDAO;

    @Override
    @Transactional
    public void updatePPAUs(List<PendingProductAttributeUpdate> updates) {

        if (updates != null && updates.size() > 0) {
            for (PendingProductAttributeUpdate update : updates) {
                updatePPAU(update);
            }
        }
    }

    @Override
    @Transactional
    public void updatePPAU(PendingProductAttributeUpdate update) {
        if (update == null) {
            throw new IllegalArgumentException("Input parameter PendingProductAttributeDBDataWriteService is found to be null");
        }
        pendingProductAttributeDAO.merge(update);
    }

}
