package com.snapdeal.cocofs.access.impl;

import java.util.Date;
import java.util.List;

import org.drools.core.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.snapdeal.cocofs.access.IPendingProductAttributeDBDataReadService;
import com.snapdeal.cocofs.db.dao.IPendingProductAttributeDAO;
import com.snapdeal.cocofs.entity.PendingProductAttributeUpdate;

/**
 * @author nikhil
 */
@Service("pendingProductAttirbuteDBDataReadService")
@Transactional(readOnly=true)
public class PendingProductAttirbuteDBDataReadServiceImpl implements IPendingProductAttributeDBDataReadService {

    @Autowired
    IPendingProductAttributeDAO pendingProductAttributeDAO;

    @Override
    public List<PendingProductAttributeUpdate> getPendingProductAtributeUpdate(String supc) {

        if (StringUtils.isEmpty(supc)) {
            throw new IllegalArgumentException("Input parameter supc is found to be null");
        }

        return pendingProductAttributeDAO.getPendingProductAttribute(supc);
    }

    @Override
    public List<PendingProductAttributeUpdate> getAllPendingProductAttributes(Date startDate, Date endDate) {
        if (startDate == null || endDate == null) {
            throw new IllegalArgumentException("Input parameter supc is found to be null");
        }
        return pendingProductAttributeDAO.getPlayablePendingProductAttributeByDateRange(startDate, endDate);
    }
    
    public List<PendingProductAttributeUpdate> getProductSystemWeightCapturedFlag(String supc) {
    	if (StringUtils.isEmpty(supc)) {
            throw new IllegalArgumentException("Input parameter supc is found to be null");
        }

        return pendingProductAttributeDAO.getProductSystemWeightCapturedFlag(supc);
    }
}
