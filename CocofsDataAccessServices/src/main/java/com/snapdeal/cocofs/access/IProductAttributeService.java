/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 09-Sep-2013
 *  @author himanshum
 */
package com.snapdeal.cocofs.access;

import java.util.List;

import com.snapdeal.cocofs.entity.ProductAttribute;
import com.snapdeal.cocofs.mongo.model.ProductAttributeUnit;

public interface IProductAttributeService {
    /**
     * Returns the enabled ProductAttribute for given supc if there are any
     * else empty list is returned.
     * @param supc
     * @param attribute
     * @return
     */
    List<ProductAttribute> getAllProductAttributeForSUPC(String supc);

    /**
     * Gets mongo doc represing all the product units of given supc
     * @param supc
     * @return
     */
    ProductAttributeUnit getProductAttributeUnitBySUPC(String supc);
    
}
