/**
 *  Copyright 2016 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.dataaccess.supcpt.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.mongo.CocofsMongoQuery;
import com.snapdeal.cocofs.mongo.Criteria;
import com.snapdeal.cocofs.mongo.mao.IGenericMao;
import com.snapdeal.cocofs.mongo.model.SupcPackagingTypeMappingUnit;
import com.snapdeal.cocofs.mongo.model.SupcPackagingTypeMappingUnit.SupcPtFieldNames;
import com.snapdeal.packman.dataaccess.supcpt.ISupcPackagingTypeMongoService;

/**
 *  
 *  @version     1.0, 05-Jan-2016
 *  @author vikassharma03
 */

@Service("supcPackagingTypeMongoServiceImpl")
public class SupcPackagingTypeMongoServiceImpl implements ISupcPackagingTypeMongoService{
    
    @Autowired
    private IGenericMao genericMao;
    
    private static final Logger  LOG = LoggerFactory.getLogger(SupcPackagingTypeMongoServiceImpl.class);

    @Override
    public SupcPackagingTypeMappingUnit getSupcPackagingTypeMappingUnit(String supc, String storeCode) {
        // TODO Auto-generated method stub
        List<Criteria> queryCriterias = new ArrayList<Criteria>();
        queryCriterias.add(new Criteria(SupcPtFieldNames.SUPC.getCode(), supc, CocofsMongoQuery.Operator.EQUAL));
        queryCriterias.add(new Criteria(SupcPtFieldNames.STORE_CODE.getCode(), storeCode, CocofsMongoQuery.Operator.EQUAL));
        return genericMao.getDocument(new CocofsMongoQuery(queryCriterias), SupcPackagingTypeMappingUnit.class);
    }
    
    @Override
    public List<SupcPackagingTypeMappingUnit> getSupcPackagingTypeMappingUnitByPackagingType(String packagingType,boolean enabled) {
        // TODO Auto-generated method stub
        List<Criteria> queryCriterias = new ArrayList<Criteria>();
        queryCriterias.add(new Criteria(SupcPtFieldNames.PACKAGING_TYPE.getCode(), packagingType, CocofsMongoQuery.Operator.EQUAL));
        queryCriterias.add(new Criteria(SupcPtFieldNames.ENABLED.getCode(), enabled, CocofsMongoQuery.Operator.EQUAL));
        return genericMao.getDocumentList(new CocofsMongoQuery(queryCriterias), SupcPackagingTypeMappingUnit.class);
    }
    
    @Override
    public String getEnabledAndActivePackagingTypeBySupcAndStoreCode(String storeCode,String supc)
    {
        boolean enabled = true;
        List<Criteria> queryCriterias = new ArrayList<Criteria>();
        queryCriterias.add(new Criteria(SupcPtFieldNames.SUPC.getCode(), supc, CocofsMongoQuery.Operator.EQUAL));
        queryCriterias.add(new Criteria(SupcPtFieldNames.STORE_CODE.getCode(),storeCode , CocofsMongoQuery.Operator.EQUAL));
        queryCriterias.add(new Criteria(SupcPtFieldNames.ENABLED.getCode(), enabled, CocofsMongoQuery.Operator.EQUAL));
        SupcPackagingTypeMappingUnit supcPackagingTypeMappingUnit = genericMao.getDocument(new CocofsMongoQuery(queryCriterias), SupcPackagingTypeMappingUnit.class);        
        if(supcPackagingTypeMappingUnit!=null)
        {

            return supcPackagingTypeMappingUnit.getPackagingType();
            
        }
        else
            return null;
    }
}
