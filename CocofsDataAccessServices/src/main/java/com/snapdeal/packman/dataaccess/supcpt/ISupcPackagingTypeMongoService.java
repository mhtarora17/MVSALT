/**
 *  Copyright 2016 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.dataaccess.supcpt;

import java.util.List;

import com.snapdeal.cocofs.mongo.model.SupcPackagingTypeMappingUnit;

/**
 *  
 *  @version     1.0, 05-Jan-2016
 *  @author vikassharma03
 */
public interface ISupcPackagingTypeMongoService {
    
    public SupcPackagingTypeMappingUnit getSupcPackagingTypeMappingUnit(String supc, String storeCode);

    public String getEnabledAndActivePackagingTypeBySupcAndStoreCode(String storeCode, String supc);

    List<SupcPackagingTypeMappingUnit> getSupcPackagingTypeMappingUnitByPackagingType(String packagingType, boolean enabled);


}
