package com.snapdeal.score.core.response.reason.impl;

import java.util.ArrayList;
import java.util.List;

import com.snapdeal.score.core.response.reason.IGenericResponseReason;

/**
 * @author abhinav
 *
 */
public class PABuildValidationDataFailureReasons<T> implements IGenericResponseReason<T> {

    public static enum PABuildValidationDataFailReasons {
        NO_OLD_ATTRIBUTES("NO_OLD_ATTRIBUTES"),
        NO_VENDORS_FOUND("NO_VENDORS_FOUND"),
        BAD_REQUEST_NO_ENTITIES("BAD_REQUEST_NO_ENTITIES"),
        EMPTY_SUPC("EMPTY_SUPC"),
        NO_DELIVERY_TYPE_FOUND("NO_DELIVERY_TYPE_FOUND"),
        NO_PACKAGING_TYPE_FOUND("NO_PACKAGING_TYPE_FOUND"),
        EXCEPTION_POPULATING_DATA("EXCEPTION_POPULATING_DATA"),
        EXCEPTION_POPULATING_WEIGHTS("EXCEPTION_POPULATING_WEIGHTS");

        private String code;

        PABuildValidationDataFailReasons(String code) {
            this.code = code;
        }

        public String getCode() {
            return code;
        }
    }
    
    private List<T> responseReasonList = new ArrayList<T>();
    
    public PABuildValidationDataFailureReasons(){
        
    }
    
    public PABuildValidationDataFailureReasons(T t){
        
        if(null != t){
            
            responseReasonList.add(t);
        }
        
    }
    
    
    
    @Override
    public List<T> getResponseReasonList() {
        return responseReasonList;
    }

    @Override
    public void setResponseReasonList(List<T> responseReasonList) {

        this.responseReasonList = responseReasonList;
    }
}
