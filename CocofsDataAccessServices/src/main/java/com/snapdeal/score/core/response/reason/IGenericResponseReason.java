package com.snapdeal.score.core.response.reason;

import java.util.List;

/**
 * @author abhinav
 *
 */
public interface IGenericResponseReason<T> {

    List<T> getResponseReasonList();
    
    void setResponseReasonList(List<T> responseReasonList);
}
