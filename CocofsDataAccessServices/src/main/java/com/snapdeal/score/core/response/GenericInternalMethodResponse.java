/**
 * 
 */
package com.snapdeal.score.core.response;

import com.snapdeal.score.core.response.reason.IGenericResponseReason;

/**
 * It is a generic response that could be used for internal methods.
 * Will help in conveying appropriate failure reasons.
 * <T> - Object to be returned.
 * <S> - Implementation of IGenericResponseReason which encapsulates failure reason Enum List.
 * 
 * @author abhinav
 *
 */
public class GenericInternalMethodResponse<T, S extends IGenericResponseReason<?>> {

    private String message;
    
    private T t;
    
    private S s;
    
    private boolean successful;
    
    public GenericInternalMethodResponse(){
        
    }

    public GenericInternalMethodResponse(String message, T t, S s, boolean successful) {
        super();
        this.message = message;
        this.t = t;
        this.s = s;
        this.successful = successful;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getT() {
        return t;
    }

    public void setT(T t) {
        this.t = t;
    }

    public S getS() {
        return s;
    }

    public void setS(S s) {
        this.s = s;
    }

    public boolean isSuccessful() {
        return successful;
    }

    public void setSuccessful(boolean successful) {
        this.successful = successful;
    }
    
    
}