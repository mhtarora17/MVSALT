/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 08-Oct-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.creator;

import java.io.IOException;

import com.mongodb.Mongo;

import de.flapdoodle.embed.mongo.MongodExecutable;
import de.flapdoodle.embed.mongo.MongodProcess;
import de.flapdoodle.embed.mongo.MongodStarter;
import de.flapdoodle.embed.mongo.config.MongodConfig;
import de.flapdoodle.embed.mongo.config.RuntimeConfig;
import de.flapdoodle.embed.mongo.distribution.Version;
import de.flapdoodle.embed.process.extract.UserTempNaming;

public class MongoUtils {

    private static final String  LOCALHOST       = "127.0.0.1";
    private static final String  DB_NAME         = "cocofs";
    private static final int     MONGO_TEST_PORT = 27028;

    private static MongodProcess mongoProcess;

    private static Mongo         mongo;

    public static Mongo initMongo() throws IOException {
        if (mongoProcess == null) {
            RuntimeConfig config = new RuntimeConfig();
            config.setExecutableNaming(new UserTempNaming());

            MongodStarter starter = MongodStarter.getInstance(config);

            MongodExecutable mongoExecutable = starter.prepare(new MongodConfig(Version.V2_2_0, 27028, false));
            mongoProcess = mongoExecutable.start();
        }
        if (mongo == null) {
            mongo = new Mongo(LOCALHOST, MONGO_TEST_PORT);
            mongo.getDB(DB_NAME);
        }
        return mongo;
    }

    public static void shutdownMongo() {
        if (null != mongoProcess) {
            mongoProcess.stop();
        }
        mongo.close();
    }

    public static Mongo getMongo() {
        return mongo;
    }

    public static void setMongo(Mongo mongo) {
        MongoUtils.mongo = mongo;
    }

}
