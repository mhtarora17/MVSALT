/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 08-Oct-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.creator;

import java.io.IOException;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.DisposableBean;

public class MongoSchemaCreator implements DisposableBean {

    @PostConstruct
    public void init() throws IOException {
        try {
            MongoUtils.initMongo();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void destroy() throws Exception {
        MongoUtils.shutdownMongo();

    }

}
