/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 08-Oct-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.test;

import java.util.List;


import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.cocofs.entity.SellerFMMapping;

@ContextConfiguration(locations = { "/hsql-applicationContext-test.xml" })
@Service("BaseEntityTest")
public class BaseEntityTest extends AbstractTransactionalTestNGSpringContextTests {

    private static final Logger LOG = LoggerFactory.getLogger(BaseEntityTest.class);

    @Autowired
    SessionFactory              sessionFactory;

    @BeforeMethod
    public void setup() {
        SellerFMMapping mapping = new SellerFMMapping();
        mapping.setCreated(DateUtils.getCurrentTime());
        mapping.setFulfilmentModel("sdsds");
        mapping.setSellerCode("sdsas");
        sessionFactory.getCurrentSession().persist(mapping);
        SellerFMMapping mapping1 = new SellerFMMapping();
        mapping1.setCreated(DateUtils.getCurrentTime());
        mapping1.setFulfilmentModel("sdsds1");
        mapping1.setSellerCode("sdsas1");
        sessionFactory.getCurrentSession().persist(mapping);

    }

    @Test
    public void test() {
        Query q = sessionFactory.getCurrentSession().createQuery("from SellerFMMapping where sellerCode = 'sdsas' ");
        List<SellerFMMapping> test22 = q.list();
        LOG.info(test22.toString());

    }

}
