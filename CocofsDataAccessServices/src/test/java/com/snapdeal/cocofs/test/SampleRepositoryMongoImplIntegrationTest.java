/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 08-Oct-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

import com.mongodb.Mongo;
import com.snapdeal.cocofs.mongo.mao.IGenericMao;
import com.snapdeal.cocofs.mongo.model.SellerFMMappingUnit;

@ContextConfiguration(locations = { "/hsql-applicationContext-test.xml" })
@Service("SampleRepositoryMongoImplIntegrationTest")
public class SampleRepositoryMongoImplIntegrationTest extends AbstractTestNGSpringContextTests {

    @Autowired
    @Qualifier("mongoTemplateCocofs")
    private MongoOperations template;

    @Autowired
    @Qualifier("mongoCocofs")
    private Mongo           mongo;

    @Autowired
    private IGenericMao     repoImpl;

    public void testSave() {
        SellerFMMappingUnit unit = new SellerFMMappingUnit();
        unit.setFulfilmentModel("12121");
        unit.setSellerCode("aaaaaa");
        template.save(unit);
    }

    @Test
    public void testFindByKey() {
        testSave();
        Query query = new Query();
        Criteria.where("sellerCode").is("aaaaaa");
        SellerFMMappingUnit unit = template.findOne(query, SellerFMMappingUnit.class);
        System.out.println(unit.toString());
    }

}