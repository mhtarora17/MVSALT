package com.snapdeal.cocofs.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Use this annotation on a field in a DTO using which we download excel template.
 * 
 * @author kirti
 */
@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface IgnoreInDownloadTemplate {
    /**
     * Name of the property to check against.
     * 
     * @return
     */
    public String propertyName() default "--NotAssigned--";

    public boolean onValue() default false;

}
