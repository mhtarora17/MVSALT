/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 29-Aug-2013
 *  @author himanshum
 */
package com.snapdeal.cocofs.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation to indicate that we wish to limit the rate at 
 * which given method is invoked.
 * Rate is specified as number of calls we want to handle
 * in given number of seconds
 * 
 * This implies the gap b/w two invocations of a method has to be 
 * greater than seconds/calls seconds.
 * 
 * This does not allow for handling of burst traffic. e.g.
 * if we specified call rate of 2 calls per second , it would imply
 * that any to successive calls to given method need to be apart by
 * 0.5 second.
 * 
 * Hence say if didn't get any invocation for 0.8 seconds, and then we get
 * an invocation of the method.  The next call to method will be allowed after 
 * 0.5 seconds more (making time interval of 1.3 seconds)
 * 
 * @author himanshum
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface RateLimited {
    

    /**
     * Number of calls allowed
     * @return
     */
    int calls() default 1000;
    /**
     * Time duration to be taken to handle given calls
     * @return
     */
    int seconds() default 1;
    
    

}
