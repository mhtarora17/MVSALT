package com.snapdeal.cocofs.sro;

import java.io.Serializable;
import java.util.Date;

public class SellerSupcAssociation implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = -2192504012703004512L;

    private String            sellerCode;

    private String            supc;

    private Date              associationDate;

    public SellerSupcAssociation(String sellerCode, String supc, Date associationDate) {
        this.sellerCode = sellerCode;
        this.supc = supc;
        this.associationDate = associationDate;
    }

    public String getSellerCode() {
        return sellerCode;
    }

    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }

    public String getSupc() {
        return supc;
    }

    public void setSupc(String supc) {
        this.supc = supc;
    }

    public Date getAssociationDate() {
        return associationDate;
    }

    public void setAssociationDate(Date associationDate) {
        this.associationDate = associationDate;
    }

    @Override
    public String toString() {
        return "SellerSupcAssociation [sellerCode=" + sellerCode + ", supc=" + supc + ", associationDate=" + associationDate + "]";
    }

}
