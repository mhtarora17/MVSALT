/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 17-Nov-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.sro;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.validation.ValidationError;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SellerFMMappingUpdateFailedSro implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 2368118788020591329L;

    @Tag(1)
    private String            sellerCode;

    @Tag(2)
    private ValidationError   validationError;

    public SellerFMMappingUpdateFailedSro() {
    }

    public SellerFMMappingUpdateFailedSro(String sellerCode, ValidationError validationError) {
        this.sellerCode = sellerCode;
        this.validationError = validationError;
    }

    public String getSellerCode() {
        return sellerCode;
    }

    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }

    public ValidationError getValidationError() {
        return validationError;
    }

    public void setValidationError(ValidationError validationError) {
        this.validationError = validationError;
    }

    @Override
    public String toString() {
        return "SellerFMMappingUpdateFailedSro [sellerCode=" + sellerCode + ", validationError=" + validationError + "]";
    }

}
