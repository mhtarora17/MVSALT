/**

 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.sro;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.annotations.NotNull;

/**
 * @version 1.0, 24-Dec-2015
 * @author nitish
 */

@JsonIgnoreProperties
public class SupcSubcatPair implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 7472335647770525717L;

    @Tag(14)
    @NotNull
    private String            supc;

    @Tag(15)
    private String            subcat;

    public String getSupc() {
        return supc;
    }

    public void setSupc(String supc) {
        this.supc = supc;
    }

    public String getSubcat() {
        return subcat;
    }

    public void setSubcat(String subcat) {
        this.subcat = subcat;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((supc == null) ? 0 : supc.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SupcSubcatPair other = (SupcSubcatPair) obj;
        if (supc == null) {
            if (other.supc != null)
                return false;
        } else if (!supc.equals(other.supc))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "SupcSubcatPair [supc=" + supc + ", subcat=" + subcat + "]";
    }

}
