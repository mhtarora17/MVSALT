/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.sro;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.annotations.NotNull;

/**
 * @version 1.0, 24-Sep-2014
 * @author ankur
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SupcGroupSRO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 6863864153780465900L;

    @Tag(1)
    @NotNull
    private List<String>      sellerCodes      = new ArrayList<String>();

    @Tag(2)
    @NotNull
    private String            supc;

    @Tag(3)
    @NotNull
    private String            categoryUrl;

    public List<String> getSellerCodes() {
        return sellerCodes;
    }

    public void setSellerCodes(List<String> sellerCodes) {
        this.sellerCodes = sellerCodes;
    }

    public String getSupc() {
        return supc;
    }

    public void setSupc(String supc) {
        this.supc = supc;
    }

    public String getCategoryUrl() {
        return categoryUrl;
    }

    public void setCategoryUrl(String categoryUrl) {
        this.categoryUrl = categoryUrl;
    }

    @Override
    public String toString() {
        return "SupcGroupSRO [sellerCodes=" + sellerCodes + ", supc=" + supc + ", categoryUrl=" + categoryUrl + "]";
    }

}
