/*
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 12-Mar-2014
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.sro;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.annotations.NotNull;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SellerSupcSubcatSro implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 805088836300709634L;

    @Tag(1)
    @NotNull
    private String sellerCode;

    @Tag(2)
    @NotNull
    private String supc;

    /**
     * subcat (Category Url) is non mandatory
     */
    @Tag(3)
    private String categoryUrl;

    public SellerSupcSubcatSro() {
    }

    public SellerSupcSubcatSro(String sellerCode, String supc) {
        this.sellerCode = sellerCode;
        this.supc = supc;
    }

    public SellerSupcSubcatSro(String sellerCode, String supc, String categoryUrl) {
        this.sellerCode = sellerCode;
        this.supc = supc;
        this.categoryUrl = categoryUrl;
    }

    public String getSellerCode() {
        return sellerCode;
    }

    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }

    public String getSupc() {
        return supc;
    }

    public void setSupc(String supc) {
        this.supc = supc;
    }

    public String getCategoryUrl() {
        return categoryUrl;
    }

    public void setCategoryUrl(String categoryUrl) {
        this.categoryUrl = categoryUrl;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((sellerCode == null) ? 0 : sellerCode.hashCode());
        result = prime * result + ((supc == null) ? 0 : supc.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SellerSupcSubcatSro other = (SellerSupcSubcatSro) obj;
        if (sellerCode == null) {
            if (other.sellerCode != null)
                return false;
        } else if (!sellerCode.equals(other.sellerCode))
            return false;
        if (supc == null) {
            if (other.supc != null)
                return false;
        } else if (!supc.equals(other.supc))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "SellerSupcSubcatSro [sellerCode=" + sellerCode + ", supc=" + supc + ", categoryUrl=" + categoryUrl + "]";
    }

}
