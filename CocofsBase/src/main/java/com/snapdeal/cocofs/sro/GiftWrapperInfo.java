/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.sro;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;

/**
 *  
 *  @version     1.0, 08-Jul-2014
 *  @author ankur
 */
@JsonIgnoreProperties(ignoreUnknown= true)
public class GiftWrapperInfo implements Serializable{

    private static final long serialVersionUID = 2997097771116004500L;

    @Tag(1)
    private boolean giftWrapApplicable;
    
    @Tag(2)
    private String supc;
    
    @Tag(3)
    private String vendorCode;

    
    public GiftWrapperInfo(){
        
    }
    
    
    
    public GiftWrapperInfo(String supc, String vendorCode, boolean giftWrapApplicable) {
        super();
        this.supc = supc;
        this.vendorCode = vendorCode;
        this.giftWrapApplicable = giftWrapApplicable;
    }



    public boolean isGiftWrapApplicable() {
        return giftWrapApplicable;
    }



    public void setGiftWrapApplicable(boolean giftWrapApplicable) {
        this.giftWrapApplicable = giftWrapApplicable;
    }



    public String getSupc() {
        return supc;
    }



    public void setSupc(String supc) {
        this.supc = supc;
    }



    public String getVendorCode() {
        return vendorCode;
    }



    public void setVendorCode(String vendorCode) {
        this.vendorCode = vendorCode;
    }



    @Override
    public String toString() {
        return "GiftWrapperInfo [giftWrapApplicable=" + giftWrapApplicable + ", supc=" + supc + ", vendorCode=" + vendorCode + "]";
    }


   
    
    
    
}
