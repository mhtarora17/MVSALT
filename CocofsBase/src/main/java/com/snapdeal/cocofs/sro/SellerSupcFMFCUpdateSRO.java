/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.sro;

import java.io.Serializable;
import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.annotations.NotNull;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SellerSupcFMFCUpdateSRO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 6324433568736785309L;

    @Tag(1)
    @NotNull
    private String            sellerCode;

    @Tag(2)
    @NotNull
    private String            supc;

    @Tag(3)
    private String        fulfillmentModel;

    @Tag(4)
    private List<String>      fcCenters;
    
    @Tag(5)
    private boolean           sdFulfilled;

    public SellerSupcFMFCUpdateSRO() {

    }

    public String getSellerCode() {
        return sellerCode;
    }

    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }

    public String getSupc() {
        return supc;
    }

    public void setSupc(String supc) {
        this.supc = supc;
    }

    public String getFulfillmentModel() {
        return fulfillmentModel;
    }

    public void setFulfillmentModel(String fulfillmentModel) {
        this.fulfillmentModel = fulfillmentModel;
    }

    public List<String> getFcCenters() {
        return fcCenters;
    }

    public void setFcCenters(List<String> fcCenters) {
        this.fcCenters = fcCenters;
    }

    public boolean isSdFulfilled() {
        return sdFulfilled;
    }

    public void setSdFulfilled(boolean sdFulfilled) {
        this.sdFulfilled = sdFulfilled;
    }

    
    @Override
    public String toString() {
        return "SellerSupcFMFCUpdateSRO [sellerCode=" + sellerCode + ", supc=" + supc + ", fulfillmentModel=" + fulfillmentModel + ", fcCenters=" + fcCenters + "]";
    }

}
