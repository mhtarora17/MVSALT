package com.snapdeal.cocofs.sro;

import java.io.Serializable;
import java.util.Date;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WeightUpdateSRO implements Serializable{

    /**
     * 
     */
    private static final long serialVersionUID = 3609701011191618667L;

    @Tag(1)
    private String supc;

    @Tag(2)
    private Double oldVolumetricWeight;

    @Tag(3)
    private Double newVolumetricWeight;

    @Tag(4)
    private Double oldDeadWeight;

    @Tag(5)
    private Double newDeadWeight;

    @Tag(6)
    private Date   withEffectFrom;

    @Tag(7)
    private Date   created;

    public String getSupc() {
        return supc;
    }

    public void setSupc(String supc) {
        this.supc = supc;
    }

    public Double getOldVolumetricWeight() {
        return oldVolumetricWeight;
    }

    public void setOldVolumetricWeight(Double oldVolumetricWeight) {
        this.oldVolumetricWeight = oldVolumetricWeight;
    }

    public Double getNewVolumetricWeight() {
        return newVolumetricWeight;
    }

    public void setNewVolumetricWeight(Double newVolumetricWeight) {
        this.newVolumetricWeight = newVolumetricWeight;
    }

    public Double getOldDeadWeight() {
        return oldDeadWeight;
    }

    public void setOldDeadWeight(Double oldDeadWeight) {
        this.oldDeadWeight = oldDeadWeight;
    }

    public Double getNewDeadWeight() {
        return newDeadWeight;
    }

    public void setNewDeadWeight(Double newDeadWeight) {
        this.newDeadWeight = newDeadWeight;
    }

    public Date getWithEffectFrom() {
        return withEffectFrom;
    }

    public void setWithEffectFrom(Date withEffectFrom) {
        this.withEffectFrom = withEffectFrom;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

}
