package com.snapdeal.cocofs.sro;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.annotations.NotNull;
import com.snapdeal.cocofs.annotation.NonZero;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductFulfilmentAttributeSRO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -4439683290541510287L;

    @Tag(6)
    @NotNull
    private String            supc;

    @Tag(7)
    private boolean           serialized;

    @Tag(8)
    private boolean           fragile;

    @Tag(9)
    @Deprecated
    private boolean           liquid;

    @Tag(10)
    private Integer           productParts;

    @Tag(11)
    @Deprecated
    private boolean           hazMat;

    @Tag(12)
    @NotNull
    @NonZero
    private Double            weight;

    @Tag(13)
    @NotNull
    @NonZero
    private Double            length;

    @Tag(14)
    @NotNull
    @NonZero
    private Double            breadth;

    @Tag(15)
    @NotNull
    @NonZero
    private Double            height;

    @Tag(16)
    private boolean           systemWeightCaptured;

    @Tag(17)
    private boolean           woodenPackaging;

    /**
     * 
     */
    @Tag(18)
    private String            dangerousGoodsType;

    @Tag(19)
    private String            serializedType;

    @Tag(20)
    private String            packagingType;

    @Tag(21)
    private Double            primaryLength;

    @Tag(22)
    private Double            primaryBreadth;

    @Tag(23)
    private Double            primaryHeight;

    public ProductFulfilmentAttributeSRO() {
        super();
        // TODO Auto-generated constructor stub
    }

    public ProductFulfilmentAttributeSRO(String supc, boolean serialized, boolean fragile, boolean liquid, Integer productParts, boolean hazMat, Double weight, Double length,
            Double breadth, Double height, Double primaryLength, Double primaryBreadth, Double primaryHeight, boolean woodenPackaging) {
        super();
        this.supc = supc;
        this.serialized = serialized;
        this.fragile = fragile;
        this.liquid = liquid;
        this.productParts = productParts;
        this.hazMat = hazMat;
        this.weight = weight;
        this.length = length;
        this.breadth = breadth;
        this.height = height;
        this.primaryLength = primaryLength;
        this.primaryBreadth = primaryBreadth;
        this.primaryHeight = primaryHeight;
        this.setSystemWeightCaptured(false); //initialize to false for existing code
        this.woodenPackaging = woodenPackaging;
    }

    public String getSupc() {
        return supc;
    }

    public void setSupc(String supc) {
        this.supc = supc;
    }

    public boolean isSerialized() {
        return serialized;
    }

    public void setSerialized(boolean serialized) {
        this.serialized = serialized;
    }

    public boolean isFragile() {
        return fragile;
    }

    public void setFragile(boolean fragile) {
        this.fragile = fragile;
    }

    public boolean isLiquid() {
        return liquid;
    }

    public void setLiquid(boolean liquid) {
        this.liquid = liquid;
    }

    public Integer getProductParts() {
        return productParts;
    }

    public void setProductParts(Integer productParts) {
        this.productParts = productParts;
    }

    public boolean isHazMat() {
        return hazMat;
    }

    public void setHazMat(boolean hazMat) {
        this.hazMat = hazMat;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Double getPrimaryLength() {
        return primaryLength;
    }

    public void setPrimaryLength(Double primaryLength) {
        this.primaryLength = primaryLength;
    }

    public Double getPrimaryBreadth() {
        return primaryBreadth;
    }

    public void setPrimaryBreadth(Double primaryBreadth) {
        this.primaryBreadth = primaryBreadth;
    }

    public Double getPrimaryHeight() {
        return primaryHeight;
    }

    public void setPrimaryHeight(Double primaryHeight) {
        this.primaryHeight = primaryHeight;
    }

    public Double getLength() {
        return length;
    }

    public void setLength(Double length) {
        this.length = length;
    }

    public Double getBreadth() {
        return breadth;
    }

    public void setBreadth(Double breadth) {
        this.breadth = breadth;
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public boolean isSystemWeightCaptured() {
        return systemWeightCaptured;
    }

    public void setSystemWeightCaptured(boolean systemWeightCaptured) {
        this.systemWeightCaptured = systemWeightCaptured;
    }

    public boolean isWoodenPackaging() {
        return woodenPackaging;
    }

    public void setWoodenPackaging(boolean woodenPackaging) {
        this.woodenPackaging = woodenPackaging;
    }

    public String getDangerousGoodsType() {
        return dangerousGoodsType;
    }

    public void setDangerousGoodsType(String dangerousGoodsType) {
        this.dangerousGoodsType = dangerousGoodsType;
    }

    public String getSerializedType() {
        return serializedType;
    }

    public void setSerializedType(String serializedType) {
        this.serializedType = serializedType;
    }

    public String getPackagingType() {
        return packagingType;
    }

    public void setPackagingType(String packagingType) {
        this.packagingType = packagingType;
    }

    @Override
    public String toString() {
        return "ProductFulfilmentAttributeSRO [supc=" + supc + ", serialized=" + serialized + ", fragile=" + fragile + ", liquid=" + liquid + ", productParts=" + productParts
                + ", hazMat=" + hazMat + ", weight=" + weight + ", length=" + length + ", breadth=" + breadth + ", height=" + height + ", systemWeightCaptured="
                + systemWeightCaptured + ", woodenPackaging=" + woodenPackaging + ", dangerousGoodsType=" + dangerousGoodsType + ", serializedType=" + serializedType
                + ", packagingType=" + packagingType + ", primaryLength=" + primaryLength + ", primaryBreadth=" + primaryBreadth + ", primaryHeight=" + primaryHeight + "]";
    }


}
