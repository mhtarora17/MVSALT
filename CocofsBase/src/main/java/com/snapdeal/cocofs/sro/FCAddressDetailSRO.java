/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.sro;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;

/**
 * @version 1.0, 27-Mar-2015
 * @author ankur
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class FCAddressDetailSRO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3000813671563053776L;

    @Tag(1)
    private String            name;

    @Tag(2)
    private String            addressLine1;

    @Tag(3)
    private String            addressLine2;

    @Tag(4)
    private String            city;

    @Tag(5)
    private String            state;

    @Tag(6)
    private String            pincode;

    @Tag(7)
    private String            mobile;

    @Tag(8)
    private String            landline;

    @Tag(9)
    private String            email;

    public FCAddressDetailSRO() {

    }

    public FCAddressDetailSRO(String name, String addressLine1, String addressLine2, String city, String state, String pincode, String mobile, String landline, String email) {
        super();
        this.name = name;
        this.addressLine1 = addressLine1;
        this.addressLine2 = addressLine2;
        this.city = city;
        this.state = state;
        this.pincode = pincode;
        this.mobile = mobile;
        this.landline = landline;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getLandline() {
        return landline;
    }

    public void setLandline(String landline) {
        this.landline = landline;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((addressLine1 == null) ? 0 : addressLine1.hashCode());
        result = prime * result + ((addressLine2 == null) ? 0 : addressLine2.hashCode());
        result = prime * result + ((city == null) ? 0 : city.hashCode());
        result = prime * result + ((landline == null) ? 0 : landline.hashCode());
        result = prime * result + ((mobile == null) ? 0 : mobile.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((pincode == null) ? 0 : pincode.hashCode());
        result = prime * result + ((state == null) ? 0 : state.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        FCAddressDetailSRO other = (FCAddressDetailSRO) obj;
        if (addressLine1 == null) {
            if (other.addressLine1 != null) {
                return false;
            }
        } else if (!addressLine1.equals(other.addressLine1)) {
            return false;
        }
        if (addressLine2 == null) {
            if (other.addressLine2 != null) {
                return false;
            }
        } else if (!addressLine2.equals(other.addressLine2)) {
            return false;
        }
        if (city == null) {
            if (other.city != null) {
                return false;
            }
        } else if (!city.equals(other.city)) {
            return false;
        }
        if (landline == null) {
            if (other.landline != null) {
                return false;
            }
        } else if (!landline.equals(other.landline)) {
            return false;
        }
        if (mobile == null) {
            if (other.mobile != null) {
                return false;
            }
        } else if (!mobile.equals(other.mobile)) {
            return false;
        }
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        if (pincode == null) {
            if (other.pincode != null) {
                return false;
            }
        } else if (!pincode.equals(other.pincode)) {
            return false;
        }
        if (state == null) {
            if (other.state != null) {
                return false;
            }
        } else if (!state.equals(other.state)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "FCAddressDetailSRO [name=" + name + ", addressLine1=" + addressLine1 + ", addressLine2=" + addressLine2 + ", city=" + city + ", state=" + state + ", pincode="
                + pincode + ", mobile=" + mobile + ", landline=" + landline + ", email=" + email + "]";
    }

}
