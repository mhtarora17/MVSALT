/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 17-Nov-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.sro;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SellerSubcatPair implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3356616360799100403L;

    @Tag(1)
    private String            sellerCode;

    @Tag(2)
    private String            subcat;

    public SellerSubcatPair() {
    }

    public SellerSubcatPair(String sellerCode, String subcat) {
        this.sellerCode = sellerCode;
        this.subcat = subcat;
    }

    public String getSellerCode() {
        return sellerCode;
    }

    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }

    public String getSubcat() {
        return subcat;
    }

    public void setSubcat(String subcat) {
        this.subcat = subcat;
    }

    @Override
    public String toString() {
        return "SellerSubcatPair [sellerCode=" + sellerCode + ", subcat=" + subcat + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((sellerCode == null) ? 0 : sellerCode.hashCode());
        result = prime * result + ((subcat == null) ? 0 : subcat.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SellerSubcatPair other = (SellerSubcatPair) obj;
        if (sellerCode == null) {
            if (other.sellerCode != null)
                return false;
        } else if (!sellerCode.equals(other.sellerCode))
            return false;
        if (subcat == null) {
            if (other.subcat != null)
                return false;
        } else if (!subcat.equals(other.subcat))
            return false;
        return true;
    }

}
