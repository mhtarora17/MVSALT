/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 03-Oct-2013
 *  @author himanshum
 */
package com.snapdeal.cocofs.sro;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.annotations.NotNull;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SellerSUPCPair implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = -378936958916909612L;
    @Tag(1)
    @NotNull
    private String            sellerCode;
    @Tag(2)
    @NotNull
    private String            supc;

    public String getSellerCode() {
        return sellerCode;
    }

    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }

    public String getSupc() {
        return supc;
    }

    public void setSupc(String supc) {
        this.supc = supc;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((sellerCode == null) ? 0 : sellerCode.hashCode());
        result = prime * result + ((supc == null) ? 0 : supc.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SellerSUPCPair other = (SellerSUPCPair) obj;
        if (sellerCode == null) {
            if (other.sellerCode != null)
                return false;
        } else if (!sellerCode.equals(other.sellerCode))
            return false;
        if (supc == null) {
            if (other.supc != null)
                return false;
        } else if (!supc.equals(other.supc))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "SellerSUPCPair [sellerCode=" + sellerCode + ", supc=" + supc + "]";
    }

}
