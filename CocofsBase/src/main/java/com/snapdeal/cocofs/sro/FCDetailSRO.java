/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.sro;

/**
 *  
 *  @version     1.0, 27-Mar-2015
 *  @author ankur
 */
import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FCDetailSRO implements Serializable {

    /**
     * 
     */
    private static final long  serialVersionUID = -8786971756864499549L;

    @Tag(1)
    private String             code;

    @Tag(2)
    private String             name;

    @Tag(3)
    private FCAddressDetailSRO fcAddress;

    @Tag(5)
    private String             type;
    
    @Tag(6)
    private boolean             sdInstant;

    public FCDetailSRO() {
        super();
    }

    public FCDetailSRO(String code, String name, FCAddressDetailSRO fcAddress) {
        super();
        this.code = code;
        this.name = name;
        this.fcAddress = fcAddress;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public FCAddressDetailSRO getFcAddress() {
        return fcAddress;
    }

    public void setFcAddress(FCAddressDetailSRO fcAddress) {
        this.fcAddress = fcAddress;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    

    public boolean isSdInstant() {
        return sdInstant;
    }

    public void setSdInstant(boolean sdInstant) {
        this.sdInstant = sdInstant;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((code == null) ? 0 : code.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        FCDetailSRO other = (FCDetailSRO) obj;
        if (code == null) {
            if (other.code != null)
                return false;
        } else if (!code.equals(other.code))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "FCDetailSRO [code=" + code + ", name=" + name + ", fcAddress=" + fcAddress + ", type=" + type + ", sdInstant=" + sdInstant + "]";
    }

   
    
}
