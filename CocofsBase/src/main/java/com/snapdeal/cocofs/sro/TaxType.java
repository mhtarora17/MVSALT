/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.sro;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;

/**
 * @version 1.0, 29-Oct-2015
 * @author ankur
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public enum TaxType {

    CST("CST"), VAT("VAT"), SERVICE_TAX("SERVICE_TAX");

    @Tag(1)
    private String type;

    private TaxType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
