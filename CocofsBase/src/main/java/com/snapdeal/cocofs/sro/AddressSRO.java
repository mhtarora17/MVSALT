package com.snapdeal.cocofs.sro;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.annotations.NotNull;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AddressSRO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 5969150851381466875L;

    @Tag(11)
    @NotNull
    private String            state;

    @Tag(12)
    private String            city;

    @Tag(13)
    private String            country;

    @Tag(14)
    @NotNull
    private String            pincode;
    
    @Tag(15)
    private String            addressLine1;
    
    @Tag(16)
    private String            addressLine2;


    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    
    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    @Override
    public String toString() {
        return "AddressSRO [state=" + state + ", city=" + city + ", country=" + country + ", pincode=" + pincode + "]";
    }

}
