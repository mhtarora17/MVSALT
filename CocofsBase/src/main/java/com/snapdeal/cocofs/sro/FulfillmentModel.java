/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 03-Oct-2013
 *  @author himanshum
 */
package com.snapdeal.cocofs.sro;

import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;

@JsonIgnoreProperties(ignoreUnknown = true)
public enum FulfillmentModel {
    DROPSHIP("DROPSHIP", false), //
    VENDOR_SELF("VENDOR_SELF", false), // 
    ONESHIP("ONESHIP", true), // 
    FC("FC", true), //
    FC_VOI("FC_VOI", true), //
    FC_BOOKS("FC_BOOKS", true), //
    O2O("O2O", false),
    OCPLUS("OCPLUS", true);

    @Tag(1)
    private String                                     code;
    @Tag(2)
    private boolean                                    locationRequired;

    private static final Map<String, FulfillmentModel> codeToFMMap = new HashMap<String, FulfillmentModel>();

    private FulfillmentModel(String code, boolean locatonRequired) {
        this.code = code;
        this.locationRequired = locatonRequired;

    }

    public String getCode() {
        return code;
    }

    /**
     * This will be true for all fulfimntModels which are on sd-wharehouse model
     * @return
     */
    public boolean isLocationRequired() {
        return locationRequired;
    }

    public static FulfillmentModel getFulfilmentModelByCode(String code) {

        if (codeToFMMap.isEmpty()) {
            for (FulfillmentModel model : FulfillmentModel.values()) {
                codeToFMMap.put(model.getCode(), model);
            }
        }
        return codeToFMMap.get(code);
    }

}
