/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.sro;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.annotations.NotNull;

/**
 *  
 *  @version     1.0, 16-Dec-2014
 *  @author ankur
 */
@JsonIgnoreProperties(ignoreUnknown= true)
public class GiftWrapProductSRO implements Serializable {

    
    /**
     * 
     */
    private static final long serialVersionUID = 8127563599828765687L;

    @Tag(1)
    @NotNull
    private String sellerCode;
    
    @Tag(2)
    @NotNull
    private String supc;
    
    @Tag(3)
    @NotNull
    private Integer sellingPrice;
    
    @Tag(5)
    @NotNull
    private String   subcat;
    

    public GiftWrapProductSRO(){
        
    }
    
    public String getSellerCode() {
        return sellerCode;
    }

    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }

    public Integer getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(Integer sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public String getSupc() {
        return supc;
    }

    public void setSupc(String supc) {
        this.supc = supc;
    }

    public String getSubcat() {
        return subcat;
    }

    public void setSubcat(String subcat) {
        this.subcat = subcat;
    }
    
    

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((sellerCode == null) ? 0 : sellerCode.hashCode());
        result = prime * result + ((supc == null) ? 0 : supc.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        GiftWrapProductSRO other = (GiftWrapProductSRO) obj;
        if (sellerCode == null) {
            if (other.sellerCode != null)
                return false;
        } else if (!sellerCode.equals(other.sellerCode))
            return false;
        if (supc == null) {
            if (other.supc != null)
                return false;
        } else if (!supc.equals(other.supc))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "GiftWrapProductSRO [sellerCode=" + sellerCode + ", supc=" + supc + ", sellingPrice=" + sellingPrice + ", subcat=" + subcat + "]";
    }

   
    
    
    
}
