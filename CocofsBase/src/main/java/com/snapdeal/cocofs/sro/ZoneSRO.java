package com.snapdeal.cocofs.sro;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;


@JsonIgnoreProperties(ignoreUnknown = true)
public class ZoneSRO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8326509558556246480L;

	@Tag(1)
	private String name;

	public ZoneSRO(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "ZoneSRO [name=" + name + "]";
	}

}
