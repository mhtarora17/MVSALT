/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.sro;

import java.io.Serializable;
import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.annotations.NotNull;

/**
 *  
 *  @version     1.0, 27-Mar-2015
 *  @author ankur
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class FMFCPair implements Serializable {

    
    /**
     * 
     */
    private static final long serialVersionUID = -6596322008041128576L;
    
    @Tag(1)
    @NotNull
    private String            fulfilmentModel;
    @Tag(2)
    @NotNull
    private List<String>            fcCodes;
    public String getFulfilmentModel() {
        return fulfilmentModel;
    }
    public void setFulfilmentModel(String fulfilmentModel) {
        this.fulfilmentModel = fulfilmentModel;
    }
    public List<String> getFcCodes() {
        return fcCodes;
    }
    public void setFcCodes(List<String> fcCodes) {
        this.fcCodes = fcCodes;
    }
    
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((fcCodes == null) ? 0 : fcCodes.hashCode());
        result = prime * result + ((fulfilmentModel == null) ? 0 : fulfilmentModel.hashCode());
        return result;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        FMFCPair other = (FMFCPair) obj;
        if (fcCodes == null) {
            if (other.fcCodes != null)
                return false;
        } else if (!fcCodes.equals(other.fcCodes))
            return false;
        if (fulfilmentModel == null) {
            if (other.fulfilmentModel != null)
                return false;
        } else if (!fulfilmentModel.equals(other.fulfilmentModel))
            return false;
        return true;
    }
    
    @Override
    public String toString() {
        return "FMFCPair [fulfilmentModel=" + fulfilmentModel + ", fcCodes=" + fcCodes + "]";
    }

    
    
}
