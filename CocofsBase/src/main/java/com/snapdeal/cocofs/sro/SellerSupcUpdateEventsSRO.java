/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.sro;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.annotations.NotNull;

/**
 *  
 *  @version     1.0, 25-Sep-2014
 *  @author ankur
 */


/**
 * This object is pushed to search while cocofs  updates the fulfillment model  for any supc-seller combination.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SellerSupcUpdateEventsSRO implements Serializable {

   private static final long serialVersionUID = 1503938069054713440L;
  
    @Tag(1)
    @NotNull
    private List<SellerSupcUpdateSRO>           sellerSupcUpdateList = new ArrayList<SellerSupcUpdateSRO>();

    public SellerSupcUpdateEventsSRO(){
        
    }
    
    public List<SellerSupcUpdateSRO> getSellerSupcUpdateList() {
        return sellerSupcUpdateList;
    }

    public void setSellerSupcUpdateList(List<SellerSupcUpdateSRO> sellerSupcUpdateList) {
        this.sellerSupcUpdateList = sellerSupcUpdateList;
    }

    @Override
    public String toString() {
        return "SellerSupcUpdateEventsSRO [sellerSupcUpdateList=" + sellerSupcUpdateList + "]";
    }
   
    
    
}
