package com.snapdeal.cocofs.sro;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.annotations.NotNull;


@JsonIgnoreProperties(ignoreUnknown = true)
public class FMZonePair implements Serializable {

    
    /**
     * 
     */
    private static final long serialVersionUID = -3708312378606069405L;
    
    @Tag(1)
    @NotNull
    private String            fulfilmentModel;
    @Tag(2)
    @NotNull
    private ZoneSRO           zoneDetails;
    
    
    public FMZonePair(){       
    }
    
    
    public String getFulfilmentModel() {
        return fulfilmentModel;
    }
    public void setFulfilmentModel(String fulfilmentModel) {
        this.fulfilmentModel = fulfilmentModel;
    }
    public ZoneSRO getZoneDetails() {
        return zoneDetails;
    }
    public void setZoneDetails(ZoneSRO zoneDetails) {
        this.zoneDetails = zoneDetails;
    }


    @Override
    public String toString() {
        return "FMZonePair [fulfilmentModel=" + fulfilmentModel + ", zoneDetails=" + zoneDetails + "]";
    }
    
    
    
}
