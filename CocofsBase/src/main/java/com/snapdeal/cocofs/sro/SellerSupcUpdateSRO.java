/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.sro;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.annotations.NotNull;

/**
 *  
 *  @version     1.0, 25-Sep-2014
 *  @author ankur
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class SellerSupcUpdateSRO  implements Serializable {
    
    
    /**
     * 
     */
    private static final long serialVersionUID = 6324433568736785309L;
    

    @Tag(1)
    @NotNull
    private String            sellerCode;
   
    @Tag(2)
    @NotNull
    private String            supc;
    
    @Tag(3)
    private boolean            sdFulfilled;

    public SellerSupcUpdateSRO(){
        
    }
    
    public String getSellerCode() {
        return sellerCode;
    }

    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }

    public String getSupc() {
        return supc;
    }

    public void setSupc(String supc) {
        this.supc = supc;
    }

    public boolean isSdFulfilled() {
        return sdFulfilled;
    }

    public void setSdFulfilled(boolean sdFulfilled) {
        this.sdFulfilled = sdFulfilled;
    }

    @Override
    public String toString() {
        return "SellerSupcUpdateSRO [sellerCode=" + sellerCode + ", supc=" + supc + ", sdFulfilled=" + sdFulfilled + "]";
    }
    
    

}
