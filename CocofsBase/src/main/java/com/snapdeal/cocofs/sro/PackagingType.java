/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 03-Oct-2013
 *  @author himanshum
 */
package com.snapdeal.cocofs.sro;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;

@JsonIgnoreProperties(ignoreUnknown = true)
public enum PackagingType {
    NORMAL("Normal"), WOODEN("Wooden");
    @Tag(1)
    private String code;
    private PackagingType(String code) {
        this.code = code;
    }
    public String getCode() {
        return code;
    }
    

}
