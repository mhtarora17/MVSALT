/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.sro;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;

/**
 *  
 *  @version     1.0, 20-Oct-2015
 *  @author ankur
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class TaxSRO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 5738650916797357116L;

    @Tag(11)
    private String taxType;
    
    @Tag(12)
    private Double taxPercentage;

    public String getTaxType() {
        return taxType;
    }

    public void setTaxType(String taxType) {
        this.taxType = taxType;
    }

    public Double getTaxPercentage() {
        return taxPercentage;
    }

    public void setTaxPercentage(Double taxPercentage) {
        this.taxPercentage = taxPercentage;
    }

    @Override
    public String toString() {
        return "TaxSRO [taxType=" + taxType + ", taxPercentage=" + taxPercentage + "]";
    }
    
    
}
