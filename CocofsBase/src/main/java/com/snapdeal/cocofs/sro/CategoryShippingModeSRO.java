/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 16, 2013
 *  @author himanshu
 */
package com.snapdeal.cocofs.sro;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CategoryShippingModeSRO implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 7741702757133004619L;
    @Tag(1)
    private String categoryUrl;
    @Tag(2)
    private List<String> shippingMode = new ArrayList<String>();
    

    public List<String> getShippingMode() {
        return shippingMode;
    }
    public void setShippingMode(List<String> shippingMode) {
        this.shippingMode = shippingMode;
    }
    public String getCategoryUrl() {
        return categoryUrl;
    }
    public void setCategoryUrl(String categoryUrl) {
        this.categoryUrl = categoryUrl;
    }
    @Override
    public String toString() {
        return "CategoryShippingModeSRO [categoryUrl=" + categoryUrl + ", shippingMode=" + shippingMode + "]";
    }
    
}
