package com.snapdeal.cocofs.sro;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ShipFromInfoSRO implements Serializable  {

    /**
     * 
     */
    private static final long serialVersionUID = 8948135651066326778L;

    @Tag(1)
    private boolean            warehouseSdInstant;
    
    @Tag(2)
    private AddressSRO        address;

    public boolean isWarehouseSdInstant() {
        return warehouseSdInstant;
    }

    public void setWarehouseSdInstant(boolean warehouseSdInstant) {
        this.warehouseSdInstant = warehouseSdInstant;
    }

    public AddressSRO getAddress() {
        return address;
    }

    public void setAddress(AddressSRO address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "ShipFromInfoSRO [warehouseSdInstant=" + warehouseSdInstant + ", address=" + address + "]";
    }

    
    
    

}
