/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 06-Oct-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.sro;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.annotations.NotNull;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SellerSubcatFulfilmentModelMappingSRO implements Serializable{

    /**
     * 
     */
    private static final long serialVersionUID = 4848219786597634150L;

    @Tag(1)
    @NotNull
    private String           sellerCode;

    /**
     * Category Url 
     */
    @Tag(2)
    @NotNull
    private String           subcat;

    @Tag(3)
    @NotNull
    private FulfillmentModel fulfilmentModel;

    public SellerSubcatFulfilmentModelMappingSRO() {
    }

    public SellerSubcatFulfilmentModelMappingSRO(String sellerCode, String subcat, FulfillmentModel fulfilmentModel) {
        this.sellerCode = sellerCode;
        this.subcat = subcat;
        this.fulfilmentModel = fulfilmentModel;
    }

    public String getSellerCode() {
        return sellerCode;
    }

    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }

    public String getSubcat() {
        return subcat;
    }

    public void setSubcat(String subcat) {
        this.subcat = subcat;
    }

    public FulfillmentModel getFulfilmentModel() {
        return fulfilmentModel;
    }

    public void setFulfilmentModel(FulfillmentModel fulfilmentModel) {
        this.fulfilmentModel = fulfilmentModel;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("SellerSubcatFulfilmentModelMappingSRO [sellerCode=").append(sellerCode).append(", subcat=").append(subcat).append(", fulfilmentModel=").append(
                fulfilmentModel).append("]");
        return builder.toString();
    }

}
