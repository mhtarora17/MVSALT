/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 03-Oct-2013
 *  @author himanshum
 */
package com.snapdeal.cocofs.sro;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FulfilmentFeeParamSRO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 8788169254139286727L;
    @Tag(1)
    private FulfillmentModel  fulfillmentModel;
    @Tag(2)
    private DeliveryType      deliveryType;
    @Tag(3)
    private PackagingType     packagingType;
    @Tag(4)
    private Double            deadWeight;
    @Tag(5)
    private Double            volumetricWeight;
    
    @Tag(6)
    private Double            perimeter;
    

    public FulfillmentModel getFulfillmentModel() {
        return fulfillmentModel;
    }

    public void setFulfillmentModel(FulfillmentModel fulfillmentModel) {
        this.fulfillmentModel = fulfillmentModel;
    }

    public DeliveryType getDeliveryType() {
        return deliveryType;
    }

    public void setDeliveryType(DeliveryType deliveryType) {
        this.deliveryType = deliveryType;
    }

    public PackagingType getPackagingType() {
        return packagingType;
    }

    public void setPackagingType(PackagingType packagingType) {
        this.packagingType = packagingType;
    }

    public Double getDeadWeight() {
        return deadWeight;
    }

    public void setDeadWeight(Double deadWeight) {
        this.deadWeight = deadWeight;
    }

    public Double getVolumetricWeight() {
        return volumetricWeight;
    }

    public void setVolumetricWeight(Double volumetricWeight) {
        this.volumetricWeight = volumetricWeight;
    }

    public Double getPerimeter() {
		return perimeter;
	}

	public void setPerimeter(Double perimeter) {
		this.perimeter = perimeter;
	}

	public FulfilmentFeeParamSRO(FulfillmentModel fulfillmentModel, DeliveryType deliveryType, PackagingType packagingType, Double weight, Double volumetricWeight, Double perimeter) {
        super();
        this.fulfillmentModel = fulfillmentModel;
        this.deliveryType = deliveryType;
        this.packagingType = packagingType;
        this.deadWeight = weight;
        this.volumetricWeight = volumetricWeight;
        this.perimeter = perimeter;
    }

	@Override
	public String toString() {
		return "FulfilmentFeeParamSRO [fulfillmentModel=" + fulfillmentModel
				+ ", deliveryType=" + deliveryType + ", packagingType="
				+ packagingType + ", deadWeight=" + deadWeight
				+ ", volumetricWeight=" + volumetricWeight + ", perimeter="
				+ perimeter + "]";
	}

    
}
