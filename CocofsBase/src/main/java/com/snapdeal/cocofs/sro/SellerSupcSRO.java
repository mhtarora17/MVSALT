/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 22-Oct-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.sro;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.annotations.NotNull;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SellerSupcSRO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -2647529981504438335L;

    @Tag(1)
    @NotNull
    private String            sellerCode;

    @Tag(2)
    @NotNull
    private String            supc;

    public SellerSupcSRO() {
    }

    public SellerSupcSRO(String sellerCode, String supc) {
        this.sellerCode = sellerCode;
        this.supc = supc;
    }

    public String getSellerCode() {
        return sellerCode;
    }

    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }

    public String getSupc() {
        return supc;
    }

    public void setSupc(String supc) {
        this.supc = supc;
    }

    @Override
    public String toString() {
        return "SellerSupcSro [sellerCode=" + sellerCode + ", supc=" + supc + "]";
    }
}
