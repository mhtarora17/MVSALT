/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 16-Nov-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.sro;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.annotations.NotNull;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SellerFulfilmentModelMappingSRO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -3902088761104364754L;

    @Tag(1)
    @NotNull
    private String            sellerCode;

    @Tag(2)
    @NotNull
    private FulfillmentModel  fulfilmentModel;

    public SellerFulfilmentModelMappingSRO() {
    }

    public SellerFulfilmentModelMappingSRO(String sellerCode, FulfillmentModel fulfilmentModel) {
        this.sellerCode = sellerCode;
        this.fulfilmentModel = fulfilmentModel;
    }

    public String getSellerCode() {
        return sellerCode;
    }

    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }

    public FulfillmentModel getFulfilmentModel() {
        return fulfilmentModel;
    }

    public void setFulfilmentModel(FulfillmentModel fulfilmentModel) {
        this.fulfilmentModel = fulfilmentModel;
    }

    @Override
    public String toString() {
        return "SellerFulfilmentMappingSRO [sellerCode=" + sellerCode + ", fulfilmentModel=" + fulfilmentModel + "]";
    }

}
