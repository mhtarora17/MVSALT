/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 29-Aug-2013
 *  @author himanshum
 */
package com.snapdeal.cocofs.util;

import java.util.concurrent.ConcurrentHashMap;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.snapdeal.cocofs.annotation.RateLimited;

@Component
@Aspect
public class RateLimiter {
    
    private static final Logger LOG = LoggerFactory.getLogger(RateLimited.class);
    private final ConcurrentHashMap<String, MethodHistory> invocationMap = new ConcurrentHashMap<String, RateLimiter.MethodHistory>();
    private static class MethodHistory {
        public int calls;
        public int seconds;
        public long lastInvocation;
        public double allowance;
        @Override
        public String toString() {
            return "MethodHistory [calls=" + calls + ", seconds=" + seconds + ", lastInvocation=" + lastInvocation + ", allowance=" + allowance + "]";
        }
        
        
    }
    
    //within(com.snapdeal..*) && 
    @Before("@annotation(rateLimited)")
    public void rateLimit(JoinPoint jp, RateLimited rateLimited) {
        String methodName = jp.getSignature().toLongString();
        int c = rateLimited.calls();
        int s = rateLimited.seconds();
        
        MethodHistory h = invocationMap.get(methodName);
        if ( null == h ) {
            h = new MethodHistory();
            h.calls = c;
            h.seconds = s;
            h.allowance = 1;
            h.lastInvocation  = System.nanoTime() ;
            // Handle concurrent calls when we do not have method invocation history
            MethodHistory oh = invocationMap.putIfAbsent(methodName, h);
            if ( null != oh) {
                h = oh;
            }
            
            
        }
        
        synchronized (h) {
            long currentInvocation = System.nanoTime();
            long timeElapsed = currentInvocation - h.lastInvocation;
            double allowance = h.allowance;
            // Time elapsed in nano seconds * 1.0e-9d = time elapsed in seconds
            allowance += (timeElapsed * 1.0e-9d ) * ( h.calls * 1.0 /h.seconds) ;
            if ( allowance >= 1 ) {
                allowance = 1;
            }
            h.lastInvocation = currentInvocation;
            if ( allowance < 1) {
                long li = h.lastInvocation ;
                
                String msg = "Too many calls : " + methodName + " history " + h.toString() + " Prior invocation " + li;
                LOG.info(msg);
                throw new RuntimeException( msg );
            } else {
                // Call is allowed to proceed
                LOG.debug("RateLimited method invocation allowed " + methodName + " history " + h.toString());
                h.allowance = 0;
            }
        }
        
    }

}
