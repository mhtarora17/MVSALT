/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.model.request;

import java.util.Map;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.model.common.ServiceResponse;
import com.snapdeal.cocofs.sro.GiftWrapProductSRO;

/**
 *  
 *  @version     1.0, 16-Dec-2014
 *  @author ankur
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetGiftWrapperInfoBySUPCSellerListResponse extends ServiceResponse {

   
    /**
     * 
     */
    private static final long serialVersionUID = 6287952334558917801L;
   
    @Tag(10)
    private Map<GiftWrapProductSRO, Boolean> productGiftWrapMap;

    public Map<GiftWrapProductSRO, Boolean> getProductGiftWrapMap() {
        return productGiftWrapMap;
    }

    public void setProductGiftWrapMap(Map<GiftWrapProductSRO, Boolean> productGiftWrapMap) {
        this.productGiftWrapMap = productGiftWrapMap;
    }

    @Override
    public String toString() {
        return "GetGiftWrapperInfoBySUPCSellerListResponse [productGiftWrapMap=" + productGiftWrapMap + "]";
    }

    

    
   
}
