/*
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 26-Mar-2014
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.model.response;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.model.common.ServiceResponse;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetAllDangerousGoodsTypeResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID    = -1806247481114371482L;

    @Tag(5)
    private List<String>      dangerousGoodsTypes = new ArrayList<String>();

    public GetAllDangerousGoodsTypeResponse() {
    }

    public GetAllDangerousGoodsTypeResponse(List<String> dangerousGoodsTypes) {
        this.dangerousGoodsTypes = dangerousGoodsTypes;
    }

    public List<String> getDangerousGoodsTypes() {
        return dangerousGoodsTypes;
    }

    public void setDangerousGoodsTypes(List<String> dangerousGoodsTypes) {
        this.dangerousGoodsTypes = dangerousGoodsTypes;
    }

    @Override
    public String toString() {
        return "GetAllDangerousGoodsTypeResponse [dangerousGoodsTypes=" + dangerousGoodsTypes + "]";
    }

}
