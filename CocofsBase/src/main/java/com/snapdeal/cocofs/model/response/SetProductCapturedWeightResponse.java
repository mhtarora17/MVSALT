package com.snapdeal.cocofs.model.response;

import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.model.common.ServiceResponse;
import com.snapdeal.base.validation.ValidationError;
/**
 * 
 * @author kirti
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SetProductCapturedWeightResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long            serialVersionUID = -6326576626488932159L;

    @Tag(5)
    private Map<String, ValidationError> failures         = new HashMap<String, ValidationError>();

    @Tag(6)
    private Integer                      successCount;

    public Map<String, ValidationError> getFailures() {
        return failures;
    }

    public void setFailures(Map<String, ValidationError> failures) {
        this.failures = failures;
    }

    public Integer getSuccessCount() {
        return successCount;
    }

    public void setSuccessCount(Integer successCount) {
        this.successCount = successCount;
    }

    public void addFailures(ValidationError error, String supc) {
        failures.put(supc, error);
    }

}
