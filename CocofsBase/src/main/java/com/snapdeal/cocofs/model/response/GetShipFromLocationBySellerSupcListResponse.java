package com.snapdeal.cocofs.model.response;

import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.model.common.ServiceResponse;
import com.snapdeal.base.validation.ValidationError;
import com.snapdeal.cocofs.sro.AddressSRO;
import com.snapdeal.cocofs.sro.SellerSUPCPair;
import com.snapdeal.cocofs.sro.ShipFromInfoSRO;
import com.snapdeal.cocofs.sro.ZoneSRO;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetShipFromLocationBySellerSupcListResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 7159567586221523308L;

    @Tag(10)
    private Map<SellerSUPCPair, ShipFromInfoSRO> sellerSupcToShipFromInfo = new HashMap<SellerSUPCPair, ShipFromInfoSRO>();

    @Tag(11)
    private Map<SellerSUPCPair, ValidationError> failures = new HashMap<SellerSUPCPair, ValidationError>();

    @Tag(12)
    private Integer successCount;

    public GetShipFromLocationBySellerSupcListResponse() {
    }

    public GetShipFromLocationBySellerSupcListResponse(Map<SellerSUPCPair, ShipFromInfoSRO> sellerSupcToShipFromInfo) {
        this.sellerSupcToShipFromInfo = sellerSupcToShipFromInfo;
    }

    public Map<SellerSUPCPair, ShipFromInfoSRO> getSellerSupcToShipFromInfo() {
        return sellerSupcToShipFromInfo;
    }

    public void setSellerSupcToShipFromInfo(Map<SellerSUPCPair, ShipFromInfoSRO> sellerSupcToShiFromInfo) {
        this.sellerSupcToShipFromInfo = sellerSupcToShiFromInfo;
    }

    public Map<SellerSUPCPair, ValidationError> getFailures() {
        return failures;
    }

    public void setFailures(Map<SellerSUPCPair, ValidationError> failures) {
        this.failures = failures;
    }

    public void addFailures(ValidationError error, SellerSUPCPair sellerSupcPair) {
        failures.put(sellerSupcPair, error);
    }

    public Integer getSuccessCount() {
        return successCount;
    }

    public void setSuccessCount(Integer successCount) {
        this.successCount = successCount;
    }

    @Override
    public String toString() {
        return "GetShipFromLocationBySellerSupcListResponse [sellerSupcToShipFromInfo=" + sellerSupcToShipFromInfo + ", failures=" + failures + ", successCount=" + successCount + "]";
    }

}
