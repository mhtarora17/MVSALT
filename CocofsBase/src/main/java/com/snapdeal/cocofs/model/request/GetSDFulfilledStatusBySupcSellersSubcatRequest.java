/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.model.request;

import java.util.ArrayList;
import java.util.List;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.annotations.NotNull;
import com.snapdeal.base.model.common.ServiceRequest;
import com.snapdeal.cocofs.sro.SupcGroupSRO;

/**
 *  
 *  @version     1.0, 24-Sep-2014
 *  @author ankur
 */
public class GetSDFulfilledStatusBySupcSellersSubcatRequest extends ServiceRequest{
    
    
    /**
     * 
     */
    private static final long serialVersionUID = -8150610402566925412L;
    
    @Tag(10)
    @NotNull
    private List<SupcGroupSRO>      supcGroups = new ArrayList<SupcGroupSRO>();

    public GetSDFulfilledStatusBySupcSellersSubcatRequest() {
        
    }
    
    public GetSDFulfilledStatusBySupcSellersSubcatRequest(List<SupcGroupSRO> supcGroups) {
        this.supcGroups = supcGroups;
    }

   
    public List<SupcGroupSRO> getSupcGroups() {
        return supcGroups;
    }

    public void setSupcGroups(List<SupcGroupSRO> supcGroups) {
        this.supcGroups = supcGroups;
    }

    @Override
    public String toString() {
        return "GetSDFulfilledStatusBySupcSellersSubcatRequest [supcGroups=" + supcGroups + "]";
    }
    
    
    
}
