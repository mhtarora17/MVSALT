/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.model.request;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.annotations.NotNull;
import com.snapdeal.base.model.common.ServiceRequest;
import com.snapdeal.cocofs.sro.GiftWrapProductSRO;

/**
 *  
 *  @version     1.0, 16-Dec-2014
 *  @author ankur
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetGiftWrapperInfoBySUPCSellerListRequest  extends ServiceRequest {

    
    private static final long serialVersionUID = 1989841649021661114L;

    @Tag(3)
    @NotNull
    private List<GiftWrapProductSRO>      giftWrapProducts;

   

    public GetGiftWrapperInfoBySUPCSellerListRequest() {
    }



    public List<GiftWrapProductSRO> getGiftWrapProducts() {
        return giftWrapProducts;
    }



    public void setGiftWrapProducts(List<GiftWrapProductSRO> giftWrapProducts) {
        this.giftWrapProducts = giftWrapProducts;
    }



    @Override
    public String toString() {
        return "GetGiftWrapperInfoBySUPCSellerListRequest [giftWrapProducts=" + giftWrapProducts + "]";
    }



   

}
