package com.snapdeal.cocofs.model.response;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.model.common.ServiceResponse;
import com.snapdeal.cocofs.sro.ProductFulfilmentAttributeSRO;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetProductFulfilmentAttributeResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long             serialVersionUID = 2496457072216247113L;

    @Tag(5)
    private ProductFulfilmentAttributeSRO productFulfilmentAttribute;

    public ProductFulfilmentAttributeSRO getProductFulfilmentAttribute() {
        return productFulfilmentAttribute;
    }

    public void setProductFulfilmentAttribute(ProductFulfilmentAttributeSRO productFulfilmentAttribute) {
        this.productFulfilmentAttribute = productFulfilmentAttribute;
    }

    public GetProductFulfilmentAttributeResponse(ProductFulfilmentAttributeSRO productFulfilmentAttribute) {
        super();
        this.productFulfilmentAttribute = productFulfilmentAttribute;
    }

    public GetProductFulfilmentAttributeResponse(boolean successful, String message) {
        super(successful, message);
    }

    public GetProductFulfilmentAttributeResponse() {
        super();
    }

    @Override
    public String toString() {
        return "GetProductFulfilmentAttributeResponse [productFulfilmentAttribute=" + productFulfilmentAttribute + "]";
    }

    
}
