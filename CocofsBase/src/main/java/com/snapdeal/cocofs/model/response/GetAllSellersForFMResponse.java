/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.model.response;

import java.util.HashSet;
import java.util.Set;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.model.common.ServiceResponse;

/**
 * @version 1.0, 07-May-2015
 * @author shiv
 */
public class GetAllSellersForFMResponse extends ServiceResponse {

	/**
     * 
     */
	private static final long serialVersionUID = 7372557035599733873L;
	@Tag(11)
	private Set<String> sellerCodes = new HashSet<String>();

	public Set<String> getSellerCodes() {
		return sellerCodes;
	}

	public void setSellerCodes(Set<String> sellerCodes) {
		this.sellerCodes = sellerCodes;
	}

	@Override
	public String toString() {
		return "GetAllSellerForFMResponse [sellerCodes=" + sellerCodes + "]";
	}

}
