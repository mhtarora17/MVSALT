/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.model.response;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.model.common.ServiceResponse;
import com.snapdeal.cocofs.sro.FCDetailSRO;

/**
 *  
 *  @version     1.0, 27-Mar-2015
 *  @author ankur
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class AllFCDetailResponse extends ServiceResponse {
    
    
    /**
     * 
     */
    private static final long serialVersionUID = -186196912972604820L;
    
    @Tag(5)
    List<FCDetailSRO>         fcDetailList     = new ArrayList<FCDetailSRO>();

    public AllFCDetailResponse() {
        super();
    }

    public AllFCDetailResponse(List<FCDetailSRO> fcDetailList) {
        super();
        this.fcDetailList = fcDetailList;
    }

    public List<FCDetailSRO> getFcDetailList() {
        return fcDetailList;
    }

    public void setFcDetailList(List<FCDetailSRO> fcDetailList) {
        this.fcDetailList = fcDetailList;
    }

    public void addFCDetail(FCDetailSRO fcDetailSRO) {
        if (!this.fcDetailList.contains(fcDetailSRO)) {
            this.fcDetailList.add(fcDetailSRO);
        }
    }

    @Override
    public String toString() {
        return "AllFCDetailResponse [fcDetailList=" + fcDetailList + "]";
    }
    
}



