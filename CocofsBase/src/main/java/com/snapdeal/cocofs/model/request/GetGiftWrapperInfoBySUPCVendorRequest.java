/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.model.request;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.annotations.NotNull;
import com.snapdeal.base.model.common.ServiceRequest;
/**
 *  
 *  @version     1.0, 08-Jul-2014
 *  @author ankur
 */

@JsonIgnoreProperties(ignoreUnknown= true)
public class GetGiftWrapperInfoBySUPCVendorRequest extends ServiceRequest {

  
    private static final long serialVersionUID = 7215491141587884331L;

    @Tag(10)
    @NotNull
    private String supc;
    
    @Tag(11)
    @NotNull
    private String vendorCode;
    
    @Tag(12)
    private Integer sellingPrice;
    
    @Tag(13)
    private String subcat;

    
    public GetGiftWrapperInfoBySUPCVendorRequest() {
        super();
    }

    public String getSupc() {
        return supc;
    }

    public void setSupc(String supc) {
        this.supc = supc;
    }

    public String getVendorCode() {
        return vendorCode;
    }

    public void setVendorCode(String vendorCode) {
        this.vendorCode = vendorCode;
    }

    
    public Integer getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(Integer sellingPrice) {
        this.sellingPrice = sellingPrice;
    }
    
    

    public String getSubcat() {
        return subcat;
    }

    public void setSubcat(String subcat) {
        this.subcat = subcat;
    }

    @Override
    public String toString() {
        return "GetGiftWrapperInfoBySUPCVendorRequest [supc=" + supc + ", vendorCode=" + vendorCode + ", sellingPrice=" + sellingPrice + ", subcat=" + subcat + "]";
    }

    
    
}
