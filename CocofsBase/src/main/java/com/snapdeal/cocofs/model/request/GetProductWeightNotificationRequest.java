package com.snapdeal.cocofs.model.request;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.annotations.NotNull;
import com.snapdeal.base.model.common.ServiceRequest;
import com.snapdeal.cocofs.annotation.NonZero;

/**
 * @author nikhil
 */
@JsonIgnoreProperties(ignoreUnknown =  true)
public class GetProductWeightNotificationRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 4533373422402859336L;

    @Tag(3)
    @NotNull
    private String            sellerCode;

    @Tag(4)
    @NonZero
    @NotNull
    private Integer           pageNum;

    @Tag(5)
    @NonZero
    @NotNull
    private Integer           pageSize;

    public String getSellerCode() {
        return sellerCode;
    }

    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    @Override
    public String toString() {
        return "GetProductWeightNotificationRequest [sellerCode=" + sellerCode + ", pageNum=" + pageNum + ", pageSize=" + pageSize + "]";
    }

}
