/*
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 16-Apr-2014
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.model.response;

import java.util.Map;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.model.common.ServiceResponse;
import com.snapdeal.base.validation.ValidationError;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SetSellerSubcatFMMappingResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 1021410938097350270L;
    /**
     * If seller subcat FM Mapping cannot be updated, then this map contains subcat to error for the same
     */
    @Tag(5)
    private Map<String, ValidationError> subcatToErrorMap;

    public SetSellerSubcatFMMappingResponse() {
    }

    public SetSellerSubcatFMMappingResponse(Map<String, ValidationError> subcatToErrorMap) {
        this.subcatToErrorMap = subcatToErrorMap;
    }

    public Map<String, ValidationError> getSubcatToErrorMap() {
        return subcatToErrorMap;
    }

    public void setSubcatToErrorMap(Map<String, ValidationError> subcatToErrorMap) {
        this.subcatToErrorMap = subcatToErrorMap;
    }

    @Override
    public String toString() {
        return "SetSellerSubcatFMMappingResponse [subcatToErrorMap=" + subcatToErrorMap + "]";
    }

}
