/*
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 15-Jun-2015
 *  @author Gaurav
 */
package com.snapdeal.cocofs.model.response;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.model.common.ServiceResponse;
import com.snapdeal.base.validation.ValidationError;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SetSellerDetailsResponse extends ServiceResponse {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7931626133984049473L;

	@Tag(5)
	private ValidationError error;

	public SetSellerDetailsResponse() {
	}

	@Override
	public String toString() {
		return "SetSellerDetailsResponse [error=" + error + "]";
	}

	public ValidationError getError() {
		return error;
	}

	public void setError(ValidationError error) {
		this.error = error;
	}
}
