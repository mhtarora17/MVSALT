/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 30-Sep-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.model.response;

import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.model.common.ServiceResponse;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AddOrUpdateSupcModeMappingResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -2886186530693329246L;

    @Tag(5)
    private Map<String, Boolean> supcResult = new HashMap<String, Boolean>();

    public Map<String, Boolean> getSupcResult() {
        return supcResult;
    }

    public void setSupcResult(Map<String, Boolean> supcResult) {
        this.supcResult = supcResult;
    }

    @Override
    public String toString() {
        return "AddOrUpdateSupcModeMappingResponse [supcResult=" + supcResult + "]";
    }
    
}
