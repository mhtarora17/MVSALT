package com.snapdeal.cocofs.model.request;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.annotations.NotNull;
import com.snapdeal.base.model.common.ServiceRequest;

/**
 * 
 * @author kirti
 *
 */
		
@JsonIgnoreProperties(ignoreUnknown =  true)
public class GetProductSystemWeightCapturedFlagRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 4533373422402859336L;

    @Tag(3)
    @NotNull
    private String            supc;

    
    public String getSupc() {
		return supc;
	}


	public void setSupc(String supc) {
		this.supc = supc;
	}


	@Override
    public String toString() {
        return "GetProductSystemWeightCapturedFlagRequest [supc=" + supc + "]";
    }

}
