/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.model.response;

import java.util.ArrayList;
import java.util.List;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.model.common.ServiceResponse;

/**
 *  
 *  @version     1.0, 06-Apr-2015
 *  @author ankur
 */
public class GetAllFCForSellerResponse extends ServiceResponse{

    /**
     * 
     */
    private static final long serialVersionUID = 2900695534542543980L;
    
    @Tag(10)
    private List<String>            fcCodeList = new ArrayList<String>();

    public List<String> getFcCodeList() {
        return fcCodeList;
    }

    public void setFcCodeList(List<String> fcCodeList) {
        this.fcCodeList = fcCodeList;
    }

    @Override
    public String toString() {
        return "GetAllFCForSellerResponse [fcCodeList=" + fcCodeList + "]";
    }
    
    
}
