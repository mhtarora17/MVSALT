/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 30-Sep-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.model.request;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.annotations.NotNull;
import com.snapdeal.base.model.common.ServiceRequest;
import com.snapdeal.cocofs.sro.CategoryShippingModeSRO;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AddOrUpdateCategoryModeMappingRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 4643072448377911751L;

    @Tag(3)
    @NotNull
    private List<CategoryShippingModeSRO> categoryShippingModes = new ArrayList<CategoryShippingModeSRO>();

    public List<CategoryShippingModeSRO> getCategoryShippingModes() {
        return categoryShippingModes;
    }

    public void setCategoryShippingModes(List<CategoryShippingModeSRO> categoryShippingModes) {
        this.categoryShippingModes = categoryShippingModes;
    }

    @Override
    public String toString() {
        return "AddOrUpdateCategoryModeMappingRequest [categoryShippingModes=" + categoryShippingModes + "]";
    }

}
