/*
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 26-Mar-2014
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.model.request;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.snapdeal.base.model.common.ServiceRequest;

@JsonIgnoreProperties(ignoreUnknown= true)
public class GetAllDangerousGoodsTypeRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -5096473178837792140L;

}
