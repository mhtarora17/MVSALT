package com.snapdeal.cocofs.model.request;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.snapdeal.base.model.common.ServiceRequest;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetAllCategoryModeMappingRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 6313473227326676294L;

    
}
