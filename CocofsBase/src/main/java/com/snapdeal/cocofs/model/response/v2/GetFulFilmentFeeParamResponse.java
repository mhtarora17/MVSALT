/*
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 12-Mar-2014
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.model.response.v2;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.model.common.ServiceResponse;
import com.snapdeal.base.validation.ValidationError;
import com.snapdeal.cocofs.sro.FulfilmentFeeParamSRO;
import com.snapdeal.cocofs.sro.SellerSupcSubcatSro;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetFulFilmentFeeParamResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long                               serialVersionUID              = -3098516725305900565L;

    @Tag(5)
    private Map<SellerSupcSubcatSro, FulfilmentFeeParamSRO> sellerSupcFulfilmentFeeParams = new HashMap<SellerSupcSubcatSro, FulfilmentFeeParamSRO>(0);

    @Tag(6)
    private Map<SellerSupcSubcatSro, ValidationError>       failures                      = new HashMap<SellerSupcSubcatSro, ValidationError>(0);

    @Tag(7)
    private Integer                                         successCount;

    public GetFulFilmentFeeParamResponse() {
    }

    public GetFulFilmentFeeParamResponse(Map<SellerSupcSubcatSro, FulfilmentFeeParamSRO> sellerSupcFulfilmentFeeParams, Map<SellerSupcSubcatSro, ValidationError> failures,
            Integer successCount) {
        this.sellerSupcFulfilmentFeeParams = sellerSupcFulfilmentFeeParams;
        this.failures = failures;
        this.successCount = successCount;
    }

    public Map<SellerSupcSubcatSro, FulfilmentFeeParamSRO> getSellerSupcFulfilmentFeeParams() {
        return sellerSupcFulfilmentFeeParams;
    }

    public void setSellerSupcFulfilmentFeeParams(Map<SellerSupcSubcatSro, FulfilmentFeeParamSRO> sellerSupcFulfilmentFeeParams) {
        this.sellerSupcFulfilmentFeeParams = sellerSupcFulfilmentFeeParams;
    }

    public Map<SellerSupcSubcatSro, ValidationError> getFailures() {
        return failures;
    }

    public void setFailures(Map<SellerSupcSubcatSro, ValidationError> failures) {
        this.failures = failures;
    }

    public void addFailures(ValidationError error, SellerSupcSubcatSro sro) {
        failures.put(sro, error);
    }

    public Integer getSuccessCount() {
        return successCount;
    }

    public void setSuccessCount(Integer successCount) {
        this.successCount = successCount;
    }

    @Override
    public String toString() {
        return "GetFulfilmentFeeParamResponse [sellerSupcFulfilmentFeeParams=" + sellerSupcFulfilmentFeeParams + ", failures=" + failures + ", successCount=" + successCount + "]";
    }

    public void addFailures(ValidationError validationError, Collection<SellerSupcSubcatSro> values) {
        for (SellerSupcSubcatSro sro : values) {
            addFailures(validationError, sro);
        }

    }

}