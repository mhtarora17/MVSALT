/*
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-Apr-2014
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.model.response;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.model.common.ServiceResponse;
import com.snapdeal.cocofs.sro.SellerSubcatFulfilmentModelMappingSRO;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetSellerSubcatFMMappingResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long                           serialVersionUID             = 3455517326055218718L;
    @Tag(5)
    private List<SellerSubcatFulfilmentModelMappingSRO> sellerSubcatFMMappingSroList = new ArrayList<SellerSubcatFulfilmentModelMappingSRO>(0);

    public GetSellerSubcatFMMappingResponse() {
    }

    public GetSellerSubcatFMMappingResponse(List<SellerSubcatFulfilmentModelMappingSRO> sellerSubcatFMMappingSroList) {
        this.sellerSubcatFMMappingSroList = sellerSubcatFMMappingSroList;
    }

    public void addSellerSubcatFMMappingSro(SellerSubcatFulfilmentModelMappingSRO sro) {
        sellerSubcatFMMappingSroList.add(sro);
    }

    public List<SellerSubcatFulfilmentModelMappingSRO> getSellerSubcatFMMappingSroList() {
        return sellerSubcatFMMappingSroList;
    }

    public void setSellerSubcatFMMappingSroList(List<SellerSubcatFulfilmentModelMappingSRO> sellerSubcatFMMappingSroList) {
        this.sellerSubcatFMMappingSroList = sellerSubcatFMMappingSroList;
    }

    @Override
    public String toString() {
        return "GetSellerSubcatFMMappingResponse [sellerSubcatFMMappingSroList=" + sellerSubcatFMMappingSroList + "]";
    }

}
