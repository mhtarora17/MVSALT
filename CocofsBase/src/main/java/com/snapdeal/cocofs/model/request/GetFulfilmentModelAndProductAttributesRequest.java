/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 26-Nov-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.model.request;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.annotations.NotNull;
import com.snapdeal.base.model.common.ServiceRequest;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetFulfilmentModelAndProductAttributesRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -5339001446756845438L;

    @Tag(3)
    @NotNull
    private String            sellerCode;

    @Tag(4)
    @NotNull
    private String            supc;

    public GetFulfilmentModelAndProductAttributesRequest() {
    }

    public GetFulfilmentModelAndProductAttributesRequest(String sellerCode, String supc) {
        this.sellerCode = sellerCode;
        this.supc = supc;
    }

    public String getSellerCode() {
        return sellerCode;
    }

    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }

    public String getSupc() {
        return supc;
    }

    public void setSupc(String supc) {
        this.supc = supc;
    }

    @Override
    public String toString() {
        return "GetFulfilmentModelAndProductAttributesRequest [sellerCode=" + sellerCode + ", supc=" + supc + "]";
    }

}
