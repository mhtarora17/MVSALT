/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 30-Sep-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.model.response;

import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.model.common.ServiceResponse;
import com.snapdeal.base.validation.ValidationError;
import com.snapdeal.cocofs.sro.SellerSubcatPair;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AddOrUpdateSellerSubcatFMMappingResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long                      serialVersionUID               = 3753014562191216783L;

    @Tag(5)
    private Map<SellerSubcatPair, ValidationError> sellerSubcatToFailureReasonMap = new HashMap<SellerSubcatPair, ValidationError>();

    @Tag(6)
    private int                                    successCount;

    public AddOrUpdateSellerSubcatFMMappingResponse() {
    }

    public AddOrUpdateSellerSubcatFMMappingResponse(Map<SellerSubcatPair, ValidationError> sellerSubcatToFailureReasonMap, int successCount) {
        this.sellerSubcatToFailureReasonMap = sellerSubcatToFailureReasonMap;
        this.successCount = successCount;
    }

    public int getSuccessCount() {
        return successCount;
    }

    public void setSuccessCount(int successCount) {
        this.successCount = successCount;
    }

    public Map<SellerSubcatPair, ValidationError> getSellerSubcatToFailureReasonMap() {
        return sellerSubcatToFailureReasonMap;
    }

    public void setSellerSubcatToFailureReasonMap(Map<SellerSubcatPair, ValidationError> sellerSubcatToFailureReasonMap) {
        this.sellerSubcatToFailureReasonMap = sellerSubcatToFailureReasonMap;
    }

    @Override
    public String toString() {
        return "AddOrUpdateSellerSubcatFMMappingResponse [sellerSubcatToFailureReasonMap=" + sellerSubcatToFailureReasonMap + ", successCount=" + successCount + "]";
    }

}
