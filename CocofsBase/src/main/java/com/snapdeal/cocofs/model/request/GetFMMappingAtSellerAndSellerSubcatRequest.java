/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 16-Nov-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.model.request;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.annotations.NotNull;
import com.snapdeal.base.model.common.ServiceRequest;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetFMMappingAtSellerAndSellerSubcatRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -7179298706454872517L;
    @Tag(3)
    @NotNull
    private String            sellerCode;

    public GetFMMappingAtSellerAndSellerSubcatRequest() {
    }

    public GetFMMappingAtSellerAndSellerSubcatRequest(String sellerCode) {
        this.sellerCode = sellerCode;
    }

    public String getSellerCode() {
        return sellerCode;
    }

    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }

}
