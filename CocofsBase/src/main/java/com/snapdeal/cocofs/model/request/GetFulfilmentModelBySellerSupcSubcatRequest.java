/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 13-Nov-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.model.request;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.annotations.NotNull;
import com.snapdeal.base.model.common.ServiceRequest;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetFulfilmentModelBySellerSupcSubcatRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 7439716365071440524L;

    @Tag(3)
    @NotNull
    private List<String>      sellerCodes;

    @Tag(4)
    @NotNull
    private String            supc;

    @Tag(5)
    private String            subcat;

    public GetFulfilmentModelBySellerSupcSubcatRequest() {
    }

    public GetFulfilmentModelBySellerSupcSubcatRequest(List<String> sellerCodes, String supc, String subcat) {
        this.sellerCodes = sellerCodes;
        this.supc = supc;
        this.subcat = subcat;
    }

    public List<String> getSellerCodes() {
        return sellerCodes;
    }

    public void setSellerCodes(List<String> sellerCodes) {
        this.sellerCodes = sellerCodes;
    }

    public String getSupc() {
        return supc;
    }

    public void setSupc(String supc) {
        this.supc = supc;
    }

    public String getSubcat() {
        return subcat;
    }

    public void setSubcat(String subcat) {
        this.subcat = subcat;
    }

    @Override
    public String toString() {
        return "GetFulfilmentModelBySellerSupcSubcatRequest [sellerCodes=" + sellerCodes + ", supc=" + supc + ", subcat=" + subcat + "]";
    }

}
