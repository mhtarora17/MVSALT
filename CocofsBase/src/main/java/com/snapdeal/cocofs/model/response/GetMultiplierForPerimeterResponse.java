package com.snapdeal.cocofs.model.response;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.model.common.ServiceResponse;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetMultiplierForPerimeterResponse extends ServiceResponse{

	/**
	 * 
	 */
	private static final long serialVersionUID = 9110845437439029559L;
	
	@Tag(11)
	private double multiplier ;
	
	public GetMultiplierForPerimeterResponse(double multiplier) {
		super();
		this.multiplier = multiplier;
	}
	
	public GetMultiplierForPerimeterResponse(){
		super();
	}

	public double getMultiplier() {
		return multiplier;
	}

	public void setMultiplier(double multiplier) {
		this.multiplier = multiplier;
	}

	@Override
	public String toString() {
		return "GetMultiplierForPerimeterResponse [multiplier=" + multiplier
				+ "]";
	}
	
	

}
