package com.snapdeal.cocofs.model.response;

import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.model.common.ServiceResponse;
import com.snapdeal.base.validation.ValidationError;
import com.snapdeal.cocofs.sro.FMZonePair;
import com.snapdeal.cocofs.sro.SellerSUPCPair;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetFMAndZonesBySellerSupcListResponse extends ServiceResponse {

   
    /**
     * 
     */
    private static final long serialVersionUID = -7027051757725971969L;

    @Tag(10)
    private Map<SellerSUPCPair, FMZonePair>         sellerSupcToFMAndZone = new HashMap<SellerSUPCPair, FMZonePair>();

    @Tag(11)
    private Map<SellerSUPCPair, ValidationError> failures         = new HashMap<SellerSUPCPair, ValidationError>();

    @Tag(12)
    private Integer                              successCount;

    public GetFMAndZonesBySellerSupcListResponse() {
    }

    public GetFMAndZonesBySellerSupcListResponse(Map<SellerSUPCPair, FMZonePair> sellerSupcToFMAndZone) {
        super();
        this.sellerSupcToFMAndZone = sellerSupcToFMAndZone;
    }
   

    public Map<SellerSUPCPair, FMZonePair> getSellerSupcToFMAndZone() {
        return sellerSupcToFMAndZone;
    }


    public void setSellerSupcToFMAndZone(Map<SellerSUPCPair, FMZonePair> sellerSupcToFMAndZone) {
        this.sellerSupcToFMAndZone = sellerSupcToFMAndZone;
    }

    public Map<SellerSUPCPair, ValidationError> getFailures() {
        return failures;
    }

    public void setFailures(Map<SellerSUPCPair, ValidationError> failures) {
        this.failures = failures;
    }

    public void addFailures(ValidationError error, SellerSUPCPair sellerSupcPair) {
        failures.put(sellerSupcPair, error);
    }

    public Integer getSuccessCount() {
        return successCount;
    }

    public void setSuccessCount(Integer successCount) {
        this.successCount = successCount;
    }

    @Override
    public String toString() {
        return "GetFMAndZonesBySellerSupcListResponse [sellerSupcToFMAndZone=" + sellerSupcToFMAndZone + ", failures=" + failures + ", successCount=" + successCount + "]";
    }

   

}
