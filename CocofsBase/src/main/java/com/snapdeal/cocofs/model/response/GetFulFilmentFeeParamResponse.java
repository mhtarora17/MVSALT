package com.snapdeal.cocofs.model.response;

import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.model.common.ServiceResponse;
import com.snapdeal.base.validation.ValidationError;
import com.snapdeal.cocofs.sro.FulfilmentFeeParamSRO;
import com.snapdeal.cocofs.sro.SellerSUPCPair;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetFulFilmentFeeParamResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long                          serialVersionUID              = -6326276626488932159L;

    @Tag(5)
    private Map<SellerSUPCPair, FulfilmentFeeParamSRO> sellerSupcFulfilmentFeeParams = new HashMap<SellerSUPCPair, FulfilmentFeeParamSRO>();

    @Tag(6)
    private Map<SellerSUPCPair, ValidationError>       failures                      = new HashMap<SellerSUPCPair, ValidationError>();

    @Tag(7)
    private Integer                                    successCount;

    public Map<SellerSUPCPair, ValidationError> getFailures() {
        return failures;
    }

    public void setFailures(Map<SellerSUPCPair, ValidationError> failures) {
        this.failures = failures;
    }

    public Integer getSuccessCount() {
        return successCount;
    }

    public void setSuccessCount(Integer successCount) {
        this.successCount = successCount;
    }

    public void addFailures(ValidationError error, SellerSUPCPair sellerSupcPair) {
        failures.put(sellerSupcPair, error);
    }

    public Map<SellerSUPCPair, FulfilmentFeeParamSRO> getSellerSupcFulfilmentFeeParams() {
        return sellerSupcFulfilmentFeeParams;
    }

    public void setSellerSupcFulfilmentFeeParams(Map<SellerSUPCPair, FulfilmentFeeParamSRO> sellerSupcFulfilmentFeeParams) {
        this.sellerSupcFulfilmentFeeParams = sellerSupcFulfilmentFeeParams;
    }

    @Override
    public String toString() {
        return "GetFulFilmentFeeParamResponse [sellerSupcFulfilmentFeeParams=" + sellerSupcFulfilmentFeeParams + ", failures=" + failures + ", successCount=" + successCount + "]";
    }

}
