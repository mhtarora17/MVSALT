package com.snapdeal.cocofs.model.request;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.snapdeal.base.model.common.ServiceRequest;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetMultiplierForPerimeterRequest extends ServiceRequest{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3337275689206638198L;

}
