package com.snapdeal.cocofs.model.response;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.model.common.ServiceResponse;

public class GetVolumetricWeightFormulaParamsResponse extends ServiceResponse{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7403288693064085267L;

	@Tag(10)
	double denominator;

	public GetVolumetricWeightFormulaParamsResponse() {
		super();
	}
	
	public GetVolumetricWeightFormulaParamsResponse(double denominator) {
		super();
		this.denominator = denominator;
	}

	public double getDenominator() {
		return denominator;
	}

	public void setDenominator(double denominator) {
		this.denominator = denominator;
	}

	@Override
	public String toString() {
		return "GetVolumetricWeightFormulaParamsResponse [denominator=" + denominator + "]";
	}
	
	
	
}
