/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 16-Nov-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.model.response;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.model.common.ServiceResponse;
import com.snapdeal.cocofs.sro.SellerFulfilmentModelMappingSRO;
import com.snapdeal.cocofs.sro.SellerSubcatFulfilmentModelMappingSRO;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetFMMappingAtSellerAndSellerSubcatResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long                           serialVersionUID = 127274667702635130L;

    @Tag(5)
    private SellerFulfilmentModelMappingSRO             sellerFMMappingSRO;

    @Tag(6)
    private List<SellerSubcatFulfilmentModelMappingSRO> sellerSubcatFMMappingSroList=new ArrayList<SellerSubcatFulfilmentModelMappingSRO>();

    public GetFMMappingAtSellerAndSellerSubcatResponse() {
    }

    public GetFMMappingAtSellerAndSellerSubcatResponse(SellerFulfilmentModelMappingSRO sellerFMMappingSRO, List<SellerSubcatFulfilmentModelMappingSRO> sellerSubcatFMMappingSroList) {
        this.sellerFMMappingSRO = sellerFMMappingSRO;
        this.sellerSubcatFMMappingSroList = sellerSubcatFMMappingSroList;
    }

    public SellerFulfilmentModelMappingSRO getSellerFMMappingSRO() {
        return sellerFMMappingSRO;
    }

    public void setSellerFMMappingSRO(SellerFulfilmentModelMappingSRO sellerFMMappingSRO) {
        this.sellerFMMappingSRO = sellerFMMappingSRO;
    }

    public List<SellerSubcatFulfilmentModelMappingSRO> getSellerSubcatFMMappingSroList() {
        return sellerSubcatFMMappingSroList;
    }

    public void setSellerSubcatFMMappingSroList(List<SellerSubcatFulfilmentModelMappingSRO> sellerSubcatFMMappingSroList) {
        this.sellerSubcatFMMappingSroList = sellerSubcatFMMappingSroList;
    }

    public void addSellerSubcatFMMappingSro(SellerSubcatFulfilmentModelMappingSRO sro) {
        sellerSubcatFMMappingSroList.add(sro);
    }

    @Override
    public String toString() {
        return "GetFMAtSellerAndSellerSubcatResponse [sellerFMMappingSRO=" + sellerFMMappingSRO + ", sellerSubcatFMMappingSroList=" + sellerSubcatFMMappingSroList + "]";
    }

}
