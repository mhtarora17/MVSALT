/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.model.request;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.annotations.NotNull;
import com.snapdeal.base.model.common.ServiceRequest;
import com.snapdeal.cocofs.sro.SellerSUPCPair;

/**
 *  
 *  @version     1.0, 27-Mar-2015
 *  @author ankur
 */

@JsonIgnoreProperties(ignoreUnknown = true)

public class GetFMAndFCBySellerSupcListRequest  extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -4137894577095401439L;
    
    
    @Tag(3)
    @NotNull
    private List<SellerSUPCPair> sellerSupcPairs;

    public GetFMAndFCBySellerSupcListRequest() {
    }

    public List<SellerSUPCPair> getSellerSupcPairs() {
        return sellerSupcPairs;
    }

    public void setSellerSupcPairs(List<SellerSUPCPair> sellerSupcPairs) {
        this.sellerSupcPairs = sellerSupcPairs;
    }

    public GetFMAndFCBySellerSupcListRequest(List<SellerSUPCPair> sellerSupcPairs) {
        this.sellerSupcPairs = sellerSupcPairs;
    }

    @Override
    public String toString() {
        return "GetFMAndFCBySellerSupcListRequest [sellerSupcPairs=" + sellerSupcPairs + "]";
    }

}
