package com.snapdeal.cocofs.model.request;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.annotations.NotNull;
import com.snapdeal.base.model.common.ServiceRequest;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetProductFulfilmentAttributeRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 6310473017326676294L;

    @Tag(3)
    @NotNull
    private String            supc;

    public GetProductFulfilmentAttributeRequest() {
        super();
    }

    public GetProductFulfilmentAttributeRequest(String supc) {
        super();
        this.supc = supc;
    }

    public String getSupc() {
        return supc;
    }

    public void setSupc(String supc) {
        this.supc = supc;
    }

    @Override
    public String toString() {
        return "GetProductFulfilmentAttributeRequest [supc=" + supc + "]";
    }

}
