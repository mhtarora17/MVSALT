/*
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 15-Jun-2015
 *  @author gaurav
 */
package com.snapdeal.cocofs.model.request;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.annotations.NotNull;
import com.snapdeal.base.model.common.ServiceRequest;
import com.snapdeal.cocofs.sro.SellerContactDetailSRO;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SetSellerDetailsRequest extends ServiceRequest {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1553532348700993357L;

	@Tag(3)
	@NotNull
	private String sellerCode;

	/**
	 * Default Fulfilment Model at Seller Level
	 */
	@Tag(4)
	@NotNull
	private String defaultSellerFMMapping;

	/**
	 * Address details of the seller
	 */
	@Tag(5)
	@NotNull
	private SellerContactDetailSRO sellerContactDetails;
	
	@Tag(6)
	@NotNull
	private String userEmail;

	public SetSellerDetailsRequest() {
	}

	public SetSellerDetailsRequest(String sellerCode,
			String defaultSellerFMMapping,
			SellerContactDetailSRO sellerContactDetails) {
		super();
		this.sellerCode = sellerCode;
		this.defaultSellerFMMapping = defaultSellerFMMapping;
		this.sellerContactDetails = sellerContactDetails;
	}

	public String getSellerCode() {
		return sellerCode;
	}

	public void setSellerCode(String sellerCode) {
		this.sellerCode = sellerCode;
	}

	public String getDefaultSellerFMMapping() {
		return defaultSellerFMMapping;
	}

	public void setDefaultSellerFMMapping(
			String defaultSellerFMMapping) {
		this.defaultSellerFMMapping = defaultSellerFMMapping;
	}

	public SellerContactDetailSRO getSellerContactDetails() {
		return sellerContactDetails;
	}

	public void setSellerContactDetails(
			SellerContactDetailSRO sellerContactDetails) {
		this.sellerContactDetails = sellerContactDetails;
	}
	
	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	@Override
	public String toString() {
		return "SetSellerDetailsRequest [sellerCode=" + sellerCode
				+ ", defaultSellerFMMapping=" + defaultSellerFMMapping
				+ ", sellerContactDetails=" + sellerContactDetails
				+ ", userEmail=" + userEmail + "]";
	}
}
