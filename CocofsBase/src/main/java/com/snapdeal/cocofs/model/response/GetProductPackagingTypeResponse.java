package com.snapdeal.cocofs.model.response;

import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.model.common.ServiceResponse;
import com.snapdeal.base.validation.ValidationError;
import com.snapdeal.cocofs.sro.PackagingType;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetProductPackagingTypeResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long            serialVersionUID = -6326976626488932159L;

    @Tag(5)
    private Map<String, PackagingType>   supcToPackagingTypeMap = new HashMap<String, PackagingType>();
    @Tag(6)
    private Map<String, ValidationError> failures         = new HashMap<String, ValidationError>();

    @Tag(7)
    private Integer                      successCount;

    public Map<String, ValidationError> getFailures() {
        return failures;
    }

    public void setFailures(Map<String, ValidationError> failures) {
        this.failures = failures;
    }

    public Integer getSuccessCount() {
        return successCount;
    }

    public void setSuccessCount(Integer successCount) {
        this.successCount = successCount;
    }

    public void addFailures(ValidationError error, String supc) {
        failures.put(supc, error);
    }

    public Map<String, PackagingType> getSupcToPackagingTypeMap() {
        return supcToPackagingTypeMap;
    }

    public void setSupcToPackagingTypeMap(Map<String, PackagingType> supcToPackagingTypeMap) {
        this.supcToPackagingTypeMap = supcToPackagingTypeMap;
    }
    
    

}
