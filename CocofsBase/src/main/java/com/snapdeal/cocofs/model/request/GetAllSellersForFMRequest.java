/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.model.request;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.annotations.NotNull;
import com.snapdeal.base.model.common.ServiceRequest;
import com.snapdeal.cocofs.sro.FulfillmentModel;

/**
 * @version 1.0, 07-May-2015
 * @author shiv
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class GetAllSellersForFMRequest extends ServiceRequest {

    /**
     * 
     */

    private static final long serialVersionUID = -6414833293277494701L;
    @Tag(11)
    @NotNull
    private FulfillmentModel  fulfillmentModel;

    public GetAllSellersForFMRequest() {
        super();
    }

    public GetAllSellersForFMRequest(FulfillmentModel fulfillmentModel) {
        super();
        this.fulfillmentModel = fulfillmentModel;
    }

    public FulfillmentModel getFulfillmentModel() {
        return fulfillmentModel;
    }

    public void setFulfillmentModel(FulfillmentModel fulfillmentModel) {
        this.fulfillmentModel = fulfillmentModel;
    }

    @Override
    public String toString() {
        return "GetAllSellerForFMRequest [fulfillmentModel=" + fulfillmentModel + "]";
    }

}
