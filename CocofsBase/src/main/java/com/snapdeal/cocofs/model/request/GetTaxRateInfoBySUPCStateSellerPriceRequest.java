package com.snapdeal.cocofs.model.request;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.annotations.NotNull;
import com.snapdeal.base.model.common.ServiceRequest;

/**
 * Request class to retrieve tax rates for 
 * a SUPC/State/both
 * @version 1.0 19th August 2014
 * @author gauravg
 *
 */
@JsonIgnoreProperties(ignoreUnknown= true)
public class GetTaxRateInfoBySUPCStateSellerPriceRequest extends ServiceRequest{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8851003165806962643L;
	
	@NotNull
	@Tag(5)
	private String supc;	//SUPC of the product -- mandatory
	
	@NotNull
	@Tag(6)
	private String state;	//State of origin -- mandatory
	
	@Tag(7)
	private String category;	//category
	
	@NotNull
	@Tag(8)
	private String sellerCode;	//SellerCode -- mandatory
	
	@NotNull
	@Tag(9)
	private Integer price;	//Price -- mandatory
	
	public String getSupc() {
		return supc;
	}
	public void setSupc(String supc) {
		this.supc = supc;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getSellerCode() {
		return sellerCode;
	}
	public void setSellerCode(String sellerCode) {
		this.sellerCode = sellerCode;
	}
	public Integer getPrice() {
		return price;
	}
	public void setPrice(Integer price) {
		this.price = price;
	}
	@Override
	public String toString() {
		return "GetTaxRateInfoBySUPCStateSellerPriceRequest [supc=" + supc + ", state="
				+ state + ", category=" + category + ", sellerCode="
				+ sellerCode + ", price=" + price + "]";
	}	

}
