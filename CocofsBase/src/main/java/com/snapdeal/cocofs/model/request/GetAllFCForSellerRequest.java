/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.model.request;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.annotations.NotNull;
import com.snapdeal.base.model.common.ServiceRequest;

/**
 *  
 *  @version     1.0, 06-Apr-2015
 *  @author ankur
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class GetAllFCForSellerRequest extends ServiceRequest{

    /**
     * 
     */
    private static final long serialVersionUID = -1869974203209632943L;
    
    @Tag(5)
    @NotNull
    private String            sellerCode;

    public String getSellerCode() {
        return sellerCode;
    }

    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }

    @Override
    public String toString() {
        return "GetAllFCForSellerRequest [sellerCode=" + sellerCode + "]";
    }
    
    
    
    
}
