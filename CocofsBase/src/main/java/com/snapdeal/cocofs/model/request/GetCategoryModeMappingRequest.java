package com.snapdeal.cocofs.model.request;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.annotations.NotNull;
import com.snapdeal.base.model.common.ServiceRequest;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetCategoryModeMappingRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 6313473227326676294L;

    @Tag(3)
    @NotNull
    private List<String>            categoryList = new ArrayList<String>();

    public List<String> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<String> categoryList) {
        this.categoryList = categoryList;
    }

    @Override
    public String toString() {
        return "GetCategoryModeMappingRequest [categoryList=" + categoryList + "]";
    }
    
    
}
