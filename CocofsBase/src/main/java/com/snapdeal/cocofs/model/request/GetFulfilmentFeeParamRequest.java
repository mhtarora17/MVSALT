package com.snapdeal.cocofs.model.request;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.annotations.NotNull;
import com.snapdeal.base.model.common.ServiceRequest;
import com.snapdeal.cocofs.sro.SellerSUPCPair;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetFulfilmentFeeParamRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 6313473227111176294L;

    @Tag(3)
    @NotNull
    private List<SellerSUPCPair>      sellerProductPairs;

    public List<SellerSUPCPair> getSellerProductPairs() {
        return sellerProductPairs;
    }

    public void setSellerProductPairs(List<SellerSUPCPair> sellerProductPairs) {
        this.sellerProductPairs = sellerProductPairs;
    }

    @Override
    public String toString() {
        return "GetFulfilmentFeeParamRequest [sellerProductPairs=" + sellerProductPairs + "]";
    }
    
    
    



    




}
