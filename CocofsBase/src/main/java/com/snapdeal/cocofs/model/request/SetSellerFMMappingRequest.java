/*
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 16-Apr-2014
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.model.request;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.annotations.NotNull;
import com.snapdeal.base.model.common.ServiceRequest;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SetSellerFMMappingRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 4184606832860521498L;

    /**
     * Seller code for which Fulfilment Model mapping is to be done, both at seller and seller-subcat level
     */
    @NotNull
    @Tag(3)
    private String            sellerCode;

    /**
     * Default Fulfilment Model at Seller Level
     */
    @Tag(4)
    @NotNull
    private String            fulfilmentModel;

    /**
     * Email of the user who is behind all this.
     */
    @Tag(5)
    @NotNull
    private String            userEmail;

    public SetSellerFMMappingRequest() {
    }

    public SetSellerFMMappingRequest(String sellerCode, String fulfilmentModel, String userEmail) {
        this.sellerCode = sellerCode;
        this.fulfilmentModel = fulfilmentModel;
        this.userEmail = userEmail;
    }

    public String getSellerCode() {
        return sellerCode;
    }

    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getFulfilmentModel() {
        return fulfilmentModel;
    }

    public void setFulfilmentModel(String fulfilmentModel) {
        this.fulfilmentModel = fulfilmentModel;
    }

    @Override
    public String toString() {
        return "SetSellerFMMappingRequest [sellerCode=" + sellerCode + ", fulfilmentModel=" + fulfilmentModel + ", userEmail=" + userEmail + "]";
    }

}
