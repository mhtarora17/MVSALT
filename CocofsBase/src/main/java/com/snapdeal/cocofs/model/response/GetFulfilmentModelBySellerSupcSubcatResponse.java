/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 13-Nov-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.model.response;

import java.util.Map;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.model.common.ServiceResponse;
import com.snapdeal.cocofs.sro.FulfillmentModel;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetFulfilmentModelBySellerSupcSubcatResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 7803860258239180601L;
    
    @Tag(5)
    private Map<String, FulfillmentModel> sellerToFulfilmentModel;

    public Map<String, FulfillmentModel> getSellerToFulfilmentModel() {
        return sellerToFulfilmentModel;
    }

    public void setSellerToFulfilmentModel(Map<String, FulfillmentModel> sellerToFulfilmentModel) {
        this.sellerToFulfilmentModel = sellerToFulfilmentModel;
    }

    @Override
    public String toString() {
        return "GetFulfilmentModelBySellerSupcSubcatResponse [sellerToFulfilmentModel=" + sellerToFulfilmentModel + "]";
    }

}
