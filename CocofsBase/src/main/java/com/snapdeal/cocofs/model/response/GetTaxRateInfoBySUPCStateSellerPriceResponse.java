package com.snapdeal.cocofs.model.response;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.model.common.ServiceResponse;

/**
 * Response class containing tax 
 * for SUPC/State/Both
 * @version 1.0 19 Aug 2014
 * @author gauravg
 *
 */

@JsonIgnoreProperties(ignoreUnknown= true)
public class GetTaxRateInfoBySUPCStateSellerPriceResponse extends ServiceResponse{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5586583389615331989L;
	
	@Tag(5)
	private Double taxPercentage;

	public Double getTax() {
		return taxPercentage;
	}

	public void setTax(Double taxPercentage) {
		this.taxPercentage = taxPercentage;
	}

	@Override
	public String toString() {
		return "GetTaxRateInfoBySUPCStateSellerPriceResponse [taxPercentage=" + taxPercentage + "]";
	}
	
}
