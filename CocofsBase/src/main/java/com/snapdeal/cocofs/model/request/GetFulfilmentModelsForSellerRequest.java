/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 26-Nov-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.model.request;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.annotations.NotNull;
import com.snapdeal.base.model.common.ServiceRequest;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetFulfilmentModelsForSellerRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -5799100876024240291L;
    @Tag(3)
    @NotNull
    private String            sellerCode;

    public GetFulfilmentModelsForSellerRequest() {
    }

    public GetFulfilmentModelsForSellerRequest(String sellerCode) {
        this.sellerCode = sellerCode;
    }

    public String getSellerCode() {
        return sellerCode;
    }

    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }

    @Override
    public String toString() {
        return "GetFulfilmentModelsForSellerRequest [sellerCode=" + sellerCode + "]";
    }

}
