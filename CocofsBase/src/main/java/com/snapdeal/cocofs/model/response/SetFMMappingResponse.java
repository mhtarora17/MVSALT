/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 16-Nov-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.model.response;

import java.util.Map;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.model.common.ServiceResponse;
import com.snapdeal.base.validation.ValidationError;
import com.snapdeal.cocofs.sro.SellerFMMappingUpdateFailedSro;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SetFMMappingResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long              serialVersionUID = -5527670791528658848L;

    /**
     * If seller FM Mapping cannot be updated, then this sro contains validation error for the same
     */
    @Tag(5)
    private SellerFMMappingUpdateFailedSro sellerFMMappingUpdateFailedSro;

    /**
     * If seller subcat FM Mapping cannot be updated, then this map contains subcat to error for the same
     */
    @Tag(6)
    private Map<String, ValidationError>   subcatToErrorMap;

    public SetFMMappingResponse() {
    }

    public SellerFMMappingUpdateFailedSro getSellerFMMappingUpdateFailedSro() {
        return sellerFMMappingUpdateFailedSro;
    }

    public void setSellerFMMappingUpdateFailedSro(SellerFMMappingUpdateFailedSro sellerFMMappingUpdateFailedSro) {
        this.sellerFMMappingUpdateFailedSro = sellerFMMappingUpdateFailedSro;
    }

    public Map<String, ValidationError> getSubcatToErrorMap() {
        return subcatToErrorMap;
    }

    public void setSubcatToErrorMap(Map<String, ValidationError> subcatToErrorMap) {
        this.subcatToErrorMap = subcatToErrorMap;
    }

}
