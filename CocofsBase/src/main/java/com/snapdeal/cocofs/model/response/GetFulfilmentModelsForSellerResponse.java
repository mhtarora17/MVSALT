/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 26-Nov-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.model.response;

import java.util.List;

import com.snapdeal.base.model.common.ServiceResponse;

public class GetFulfilmentModelsForSellerResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -5734113385045829624L;
    private List<String>      fulfilmentModels;

    public GetFulfilmentModelsForSellerResponse() {
    }

    public GetFulfilmentModelsForSellerResponse(List<String> fulfilmentModels) {
        this.fulfilmentModels = fulfilmentModels;
    }

    public List<String> getFulfilmentModels() {
        return fulfilmentModels;
    }

    public void setFulfilmentModels(List<String> fulfilmentModels) {
        this.fulfilmentModels = fulfilmentModels;
    }

    @Override
    public String toString() {
        return "GetFulfilmentModelsForSellerResponse [fulfilmentModels=" + fulfilmentModels + "]";
    }

}
