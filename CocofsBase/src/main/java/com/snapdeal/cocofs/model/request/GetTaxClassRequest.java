/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.model.request;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.annotations.NotNull;
import com.snapdeal.base.model.common.ServiceRequest;
import com.snapdeal.cocofs.sro.SupcSubcatPair;

/**
 * @version 1.0, 23-Dec-2015
 * @author nitish
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class GetTaxClassRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long    serialVersionUID = -8147396143332227376L;

    @Tag(10)
    @NotNull
    private List<SupcSubcatPair> supcSubcatPairList;

    public List<SupcSubcatPair> getSupcSubcatPairList() {
        return supcSubcatPairList;
    }

    public void setSupcSubcatPairList(List<SupcSubcatPair> supcSubcatPairList) {
        this.supcSubcatPairList = supcSubcatPairList;
    }

    @Override
    public String toString() {
        return "GetTaxClassRequest [supcSubcatPairList=" + supcSubcatPairList + "]";
    }

}
