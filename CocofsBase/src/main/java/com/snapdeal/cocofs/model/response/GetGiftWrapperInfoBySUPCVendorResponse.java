/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.model.response;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.model.common.ServiceResponse;
import com.snapdeal.cocofs.sro.GiftWrapperInfo;

/**
 *  
 *  @version     1.0, 08-Jul-2014
 *  @author ankur
 */
@JsonIgnoreProperties(ignoreUnknown= true)
public class GetGiftWrapperInfoBySUPCVendorResponse extends ServiceResponse{
   
    private static final long serialVersionUID = 4508694475163854110L;
   
    @Tag(10)
    private GiftWrapperInfo  giftWrapperInfo;

    public GetGiftWrapperInfoBySUPCVendorResponse(){
        
    }
    
    public GiftWrapperInfo getGiftWrapperInfo() {
        return giftWrapperInfo;
    }

    public void setGiftWrapperInfo(GiftWrapperInfo giftWrapperInfo) {
        this.giftWrapperInfo = giftWrapperInfo;
    }

    @Override
    public String toString() {
        return "GetGiftWrapperInfoBySUPCVendorRespone [giftWrapperInfo=" + giftWrapperInfo + "]";
    }
    
    
    

}
