/*
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 16-Apr-2014
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.model.request;

import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.annotations.NotNull;
import com.snapdeal.base.model.common.ServiceRequest;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SetSellerSubcatFMMappingRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long   serialVersionUID  = 4715517413056698543L;

    /**
     * Seller code for which Fulfilment Model mapping is to be done, both at seller and seller-subcat level
     */
    @NotNull
    @Tag(3)
    private String              sellerCode;

    /**
     * Email of the user who is behind all this.
     */
    @Tag(5)
    @NotNull
    private String              userEmail;

    /**
     * Subcat to Fulfilment Model Mapping Map for raising exceptions at seller-subcat level. It gives the current
     * snapshot of the exceptions. Previous exceptions, if any, will be disabled
     */
    @Tag(6)
    private Map<String, String> subcatToFMMapping = new HashMap<String, String>(0);

    public SetSellerSubcatFMMappingRequest() {
    }

    public SetSellerSubcatFMMappingRequest(String sellerCode, String userEmail, Map<String, String> subcatToFMMapping) {
        this.sellerCode = sellerCode;
        this.userEmail = userEmail;
        this.subcatToFMMapping = subcatToFMMapping;
    }

    public String getSellerCode() {
        return sellerCode;
    }

    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public Map<String, String> getSubcatToFMMapping() {
        return subcatToFMMapping;
    }

    public void setSubcatToFMMapping(Map<String, String> subcatToFMMapping) {
        this.subcatToFMMapping = subcatToFMMapping;
    }

    @Override
    public String toString() {
        return "SetSellerSubcatFMMappingRequest [sellerCode=" + sellerCode + ", userEmail=" + userEmail + ", subcatToFMMapping=" + subcatToFMMapping + "]";
    }

}
