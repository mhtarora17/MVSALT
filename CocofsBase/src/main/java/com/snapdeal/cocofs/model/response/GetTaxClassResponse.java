/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.model.response;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.model.common.ServiceResponse;
import com.snapdeal.base.validation.ValidationError;
import com.snapdeal.cocofs.sro.FulfillmentModel;
import com.snapdeal.cocofs.sro.SellerSUPCPair;
import com.snapdeal.cocofs.sro.SupcSubcatPair;

/**
 * @version 1.0, 23-Dec-2015
 * @author nitish
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetTaxClassResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long                    serialVersionUID     = -1221462872562632399L;

    @Tag(10)
    private Map<SupcSubcatPair, String>          supcSubcatToTaxClass = new HashMap<SupcSubcatPair, String>();

    @Tag(11)
    private Map<SupcSubcatPair, ValidationError> failures             = new HashMap<SupcSubcatPair, ValidationError>();

    @Tag(12)
    private Integer                              successCount;

    public Map<SupcSubcatPair, String> getSupcSubcatToTaxClass() {
        return supcSubcatToTaxClass;
    }

    public void setSupcSubcatToTaxClass(Map<SupcSubcatPair, String> supcSubcatToTaxClass) {
        this.supcSubcatToTaxClass = supcSubcatToTaxClass;
    }

    public Map<SupcSubcatPair, ValidationError> getFailures() {
        return failures;
    }

    public void setFailures(Map<SupcSubcatPair, ValidationError> failures) {
        this.failures = failures;
    }

    public void addFailures(ValidationError error, SupcSubcatPair supcSubcatPair) {
        failures.put(supcSubcatPair, error);
    }

    public Integer getSuccessCount() {
        return successCount;
    }

    public void setSuccessCount(Integer successCount) {
        this.successCount = successCount;
    }

    @Override
    public String toString() {
        return "GetTaxClassResponse [supcSubcatToTaxClass=" + supcSubcatToTaxClass + ", failures=" + failures + ", successCount=" + successCount + "]";
    }

}
