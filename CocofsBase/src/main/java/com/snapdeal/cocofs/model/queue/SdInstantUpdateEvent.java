/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.model.queue;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;

/**
 *  
 *  @version     1.0, 25-Sep-2014
 *  @author ankur
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class SdInstantUpdateEvent  implements Serializable {
  
    /**
     * 
     */
    private static final long serialVersionUID = 1884513726966706608L;
    
    private List<SdInstantUpdateMessage>  sellersupcSdInstantUpdates = new ArrayList<SdInstantUpdateMessage>();

    public SdInstantUpdateEvent(){
        
    }

    public List<SdInstantUpdateMessage> getSellersupcSdInstantUpdates() {
        return sellersupcSdInstantUpdates;
    }

    public void setSellersupcSdInstantUpdates(List<SdInstantUpdateMessage> sellersupcSdInstantUpdates) {
        this.sellersupcSdInstantUpdates = sellersupcSdInstantUpdates;
    }

    @Override
    public String toString() {
        return "SdInstantUpdateEvent [sellersupcSdInstantUpdates=" + sellersupcSdInstantUpdates + "]";
    }

    
   

}
