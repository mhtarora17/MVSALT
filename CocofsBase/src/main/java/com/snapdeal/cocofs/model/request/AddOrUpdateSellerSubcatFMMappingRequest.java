/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 30-Sep-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.model.request;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.annotations.NotNull;
import com.snapdeal.base.model.common.ServiceRequest;
import com.snapdeal.cocofs.sro.SellerSubcatFulfilmentModelMappingSRO;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AddOrUpdateSellerSubcatFMMappingRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long                           serialVersionUID = 3690190195429361645L;

    @Tag(3)
    @NotNull
    private List<SellerSubcatFulfilmentModelMappingSRO> sellerSubcatFulfilmentModelMappingList;

    public AddOrUpdateSellerSubcatFMMappingRequest() {
    }

    public AddOrUpdateSellerSubcatFMMappingRequest(List<SellerSubcatFulfilmentModelMappingSRO> sellerSubcatFulfilmentModelMappingList) {
        this.sellerSubcatFulfilmentModelMappingList = sellerSubcatFulfilmentModelMappingList;
    }

    public List<SellerSubcatFulfilmentModelMappingSRO> getSellerSubcatFulfilmentModelMappingList() {
        return sellerSubcatFulfilmentModelMappingList;
    }

    public void setSellerSubcatFulfilmentModelMappingList(List<SellerSubcatFulfilmentModelMappingSRO> sellerSubcatFulfilmentModelMappingList) {
        this.sellerSubcatFulfilmentModelMappingList = sellerSubcatFulfilmentModelMappingList;
    }

    @Override
    public String toString() {
        return "AddOrUpdateSellerSubcatFulfilmentModelMappingRequest [sellerSubcatFulfilmentModelMappingList=" + sellerSubcatFulfilmentModelMappingList + "]";
    }

}
