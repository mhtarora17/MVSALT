/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 26-Nov-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.model.response;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.model.common.ServiceResponse;
import com.snapdeal.cocofs.sro.SupcShippingModeSRO;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetAllSupcModeMappingResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -5734113385045829624L;
    
    @Tag(5)
    private List<SupcShippingModeSRO>      supcModes = new ArrayList<SupcShippingModeSRO>();
    
    public List<SupcShippingModeSRO> getSupcModes() {
        return supcModes;
    }
    public void setSupcModes(List<SupcShippingModeSRO> supcModes) {
        this.supcModes = supcModes;
    }
    @Override
    public String toString() {
        return "GetSupcModeMappingResponse [supcModes=" + supcModes + "]";
    }


    
 
}
