package com.snapdeal.cocofs.model.request;

import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.annotations.NotNull;
import com.snapdeal.base.model.common.ServiceRequest;
import com.snapdeal.cocofs.annotation.Limited;
import com.snapdeal.cocofs.sro.ProductFulfilmentAttributeSRO;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SetProductFulfilmentAttributeRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long                          serialVersionUID = -1548898007012007914L;

    @Tag(3)
    @Limited
    @NotNull
    private Map<String, ProductFulfilmentAttributeSRO> requestMap       = new HashMap<String, ProductFulfilmentAttributeSRO>();

    @Tag(4)
    private String                                     userEmail;

    public Map<String, ProductFulfilmentAttributeSRO> getRequestMap() {
        return requestMap;
    }

    public void setRequestMap(Map<String, ProductFulfilmentAttributeSRO> requestMap) {
        this.requestMap = requestMap;
    }

    public void addSupcInRequest(String supc, ProductFulfilmentAttributeSRO attributes) {
        requestMap.put(supc, attributes);
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    @Override
    public String toString() {
        return "SetProductFulfilmentAttributeRequest [requestMap=" + requestMap + ", userEmail=" + userEmail + "]";
    }
}
