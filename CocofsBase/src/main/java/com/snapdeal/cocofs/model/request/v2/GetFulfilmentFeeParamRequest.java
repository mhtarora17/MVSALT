/*
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 12-Mar-2014
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.model.request.v2;

import java.util.Set;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.annotations.NotNull;
import com.snapdeal.base.model.common.ServiceRequest;
import com.snapdeal.cocofs.sro.SellerSupcSubcatSro;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetFulfilmentFeeParamRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long        serialVersionUID = 8368929715382632715L;
    @Tag(3)
    @NotNull
    private Set<SellerSupcSubcatSro> sellerProductPairs;

    public GetFulfilmentFeeParamRequest() {
    }

    public GetFulfilmentFeeParamRequest(Set<SellerSupcSubcatSro> sellerProductPairs) {
        this.sellerProductPairs = sellerProductPairs;
    }

    public Set<SellerSupcSubcatSro> getSellerProductPairs() {
        return sellerProductPairs;
    }

    public void setSellerProductPairs(Set<SellerSupcSubcatSro> sellerProductPairs) {
        this.sellerProductPairs = sellerProductPairs;
    }

    @Override
    public String toString() {
        return "GetFulfilmentFeeParamRequest [sellerProductPairs=" + sellerProductPairs + "]";
    }

}
