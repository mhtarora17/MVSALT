package com.snapdeal.cocofs.model.response;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.model.common.ServiceResponse;
import com.snapdeal.cocofs.sro.TaxSRO;

/**
 * Response class containing tax for SUPC/State/Both
 * 
 * @version 1.0 19 Oct 2015
 * @author ankurg
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetTaxInfoResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -7541767754828284271L;

    @Tag(10)
    private List<TaxSRO>      taxes;

    public List<TaxSRO> getTaxes() {
        return taxes;
    }

    public void setTaxes(List<TaxSRO> taxes) {
        this.taxes = taxes;
    }

    @Override
    public String toString() {
        return "GetTaxInfoResponse [taxes=" + taxes + ", isSuccessful()=" + isSuccessful() + ", getCode()=" + getCode() + ", getMessage()=" + getMessage() + ", getProtocol()="
                + getProtocol() + "]";
    }

}
