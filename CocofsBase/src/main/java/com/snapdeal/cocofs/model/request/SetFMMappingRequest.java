/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 16-Nov-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.model.request;

import java.util.Map;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.annotations.NotNull;
import com.snapdeal.base.model.common.ServiceRequest;
import com.snapdeal.cocofs.sro.FulfillmentModel;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SetFMMappingRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long             serialVersionUID = -2168396890501644597L;

    /**
     * Seller code for which Fulfilment Model mapping is to be done, both at seller and seller-subcat level
     */
    @NotNull
    @Tag(3)
    private String                        sellerCode;

    /**
     * Default Fulfilment Model at Seller Level
     */
    @Tag(4)
    @NotNull
    private FulfillmentModel              defaultSellerFMMapping;

    /**
     * Email of the user who is behind all this.
     */
    @Tag(5)
    @NotNull
    private String                        userEmail;

    /**
     * Subcat to Fulfilment Model Mapping Map for raising exceptions at seller-subcat level. It gives the current
     * snapshot of the exceptions. Previous exceptions, if any, will be disabled
     */
    @Tag(6)
    private Map<String, FulfillmentModel> subcatToFMMapping;

    public SetFMMappingRequest() {
    }

    public SetFMMappingRequest(String sellerCode, FulfillmentModel defaultSellerFMMapping, String userEmail, Map<String, FulfillmentModel> subcatToFMMapping) {
        this.sellerCode = sellerCode;
        this.defaultSellerFMMapping = defaultSellerFMMapping;
        this.userEmail = userEmail;
        this.subcatToFMMapping = subcatToFMMapping;
    }

    public String getSellerCode() {
        return sellerCode;
    }

    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }

    public FulfillmentModel getDefaultSellerFMMapping() {
        return defaultSellerFMMapping;
    }

    public void setDefaultSellerFMMapping(FulfillmentModel defaultSellerFMMapping) {
        this.defaultSellerFMMapping = defaultSellerFMMapping;
    }

    public Map<String, FulfillmentModel> getSubcatToFMMapping() {
        return subcatToFMMapping;
    }

    public void setSubcatToFMMapping(Map<String, FulfillmentModel> subcatToFMMapping) {
        this.subcatToFMMapping = subcatToFMMapping;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    @Override
    public String toString() {
        return "SetFMMappingRequest [sellerCode=" + sellerCode + ", defaultSellerFMMapping=" + defaultSellerFMMapping + ", userEmail=" + userEmail + ", subcatToFMMapping="
                + subcatToFMMapping + "]";
    }

}
