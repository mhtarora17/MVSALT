/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.model.request;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.model.common.ServiceRequest;

/**
 * @version 1.0, 27-Mar-2015
 * @author ankur
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetAllFCDetailRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -2386633003565397244L;

    @Tag(11)
    private String            type;

    public GetAllFCDetailRequest() {
        super();
    }

    public GetAllFCDetailRequest(String type) {
        super();
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "GetAllFCDetailRequest [type=" + type + "]";
    }
}
