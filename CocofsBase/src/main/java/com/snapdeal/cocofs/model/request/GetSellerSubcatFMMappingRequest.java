/*
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-Apr-2014
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.model.request;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.annotations.NotNull;
import com.snapdeal.base.model.common.ServiceRequest;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetSellerSubcatFMMappingRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 9183422611351758423L;
    @Tag(3)
    @NotNull
    private String            sellerCode;

    public GetSellerSubcatFMMappingRequest(String sellerCode) {
        this.sellerCode = sellerCode;
    }

    public GetSellerSubcatFMMappingRequest() {
    }

    public String getSellerCode() {
        return sellerCode;
    }

    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }

    @Override
    public String toString() {
        return "GetSellerSubcatFMMappingRequest [sellerCode=" + sellerCode + "]";
    }

}
