package com.snapdeal.cocofs.model.response;

import java.util.Map;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.model.common.ServiceResponse;

/**
 *  24-Jul-2015
 * @author indrajit
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ValidateSellerCodeResponse extends ServiceResponse{

	/**
	 * 
	 */
	private static final long serialVersionUID = 162478126L;
	 
	/*
	 *  @Map(Key is seller code, value is Exist(@True) Not Exist(@False))
	 */
	@Tag(11)
	Map<String, Boolean>      sellerCodeValidationMap;

	public ValidateSellerCodeResponse(Map<String, Boolean> sellerCodeValidationMap) {
		super();
		this.sellerCodeValidationMap = sellerCodeValidationMap;
	}
	
	public ValidateSellerCodeResponse(){
		super();
	}

	public Map<String, Boolean> getSellerCodeValidationMap() {
		return sellerCodeValidationMap;
	}

	public void setSellerCodeValidationMap(
			Map<String, Boolean> sellerCodeValidationMap) {
		this.sellerCodeValidationMap = sellerCodeValidationMap;
	}

	@Override
	public String toString() {
		return "ValidateSellerCodeResponse [sellerCodeValidationMap="
				+ sellerCodeValidationMap + "]";
	}
	
	

}
