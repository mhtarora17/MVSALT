package com.snapdeal.cocofs.model.response;

import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.model.common.ServiceResponse;
import com.snapdeal.base.validation.ValidationError;
import com.snapdeal.cocofs.sro.SellerSUPCPair;
import com.snapdeal.cocofs.sro.ZoneSRO;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetZonesBySellerSupcListResponse extends ServiceResponse {

    /**
	 * 
	 */
    private static final long                    serialVersionUID = -3048249392984061228L;

    @Tag(10)
    private Map<SellerSUPCPair, ZoneSRO>         sellerSupcToZone = new HashMap<SellerSUPCPair, ZoneSRO>();

    @Tag(11)
    private Map<SellerSUPCPair, ValidationError> failures         = new HashMap<SellerSUPCPair, ValidationError>();

    @Tag(12)
    private Integer                              successCount;

    public GetZonesBySellerSupcListResponse() {
    }

    public GetZonesBySellerSupcListResponse(Map<SellerSUPCPair, ZoneSRO> sellerSupcToZone) {
        this.sellerSupcToZone = sellerSupcToZone;
    }

    public Map<SellerSUPCPair, ZoneSRO> getSellerSupcToZone() {
        return sellerSupcToZone;
    }

    public void setSellerSupcToZone(Map<SellerSUPCPair, ZoneSRO> sellerSupcToZone) {
        this.sellerSupcToZone = sellerSupcToZone;
    }

    public Map<SellerSUPCPair, ValidationError> getFailures() {
        return failures;
    }

    public void setFailures(Map<SellerSUPCPair, ValidationError> failures) {
        this.failures = failures;
    }

    public void addFailures(ValidationError error, SellerSUPCPair sellerSupcPair) {
        failures.put(sellerSupcPair, error);
    }

    public Integer getSuccessCount() {
        return successCount;
    }

    public void setSuccessCount(Integer successCount) {
        this.successCount = successCount;
    }

    @Override
    public String toString() {
        return "GetZonesBySellerSupcListResponse [sellerSupcToZone=" + sellerSupcToZone + ", failures=" + failures + ", successCount=" + successCount + "]";
    }

}
