/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-Oct-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.model.response;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.annotations.NotNull;
import com.snapdeal.base.model.common.ServiceResponse;
import com.snapdeal.cocofs.sro.FulfillmentModel;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetFulfilmentModelAndWeightResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 6446702780066640588L;

    @Tag(5)
    @NotNull
    private FulfillmentModel  fulfilmentModel;

    @Tag(6)
    @NotNull
    private Double            weight;

    public FulfillmentModel getFulfilmentModel() {
        return fulfilmentModel;
    }

    public void setFulfilmentModel(FulfillmentModel fulfilmentModel) {
        this.fulfilmentModel = fulfilmentModel;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }
}
