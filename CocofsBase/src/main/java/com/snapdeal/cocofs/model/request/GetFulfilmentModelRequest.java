/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 30-Sep-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.model.request;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.annotations.NotNull;
import com.snapdeal.base.model.common.ServiceRequest;
import com.snapdeal.cocofs.sro.SellerSUPCPair;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetFulfilmentModelRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long    serialVersionUID = -1600510323178996120L;

    @Tag(3)
    @NotNull
    private List<SellerSUPCPair> sellerSupcPairs;

    public GetFulfilmentModelRequest() {
    }

    public List<SellerSUPCPair> getSellerSupcPairs() {
        return sellerSupcPairs;
    }

    public void setSellerSupcPairs(List<SellerSUPCPair> sellerSupcPairs) {
        this.sellerSupcPairs = sellerSupcPairs;
    }

    public GetFulfilmentModelRequest(List<SellerSUPCPair> sellerSupcPairs) {
        this.sellerSupcPairs = sellerSupcPairs;
    }

    @Override
    public String toString() {
        return "GetFulfilmentModelRequest [sellerSupcPairs=" + sellerSupcPairs + "]";
    }

}
