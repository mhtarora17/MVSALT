package com.snapdeal.cocofs.model.response;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.model.common.ServiceResponse;
import com.snapdeal.cocofs.sro.WeightUpdateSRO;

/**
 * @author nikhil
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class GetProductWeightNotificationResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long                  serialVersionUID       = -4190091907222342876L;

    @Tag(5)
    private Integer                            total;

    @Tag(6)
    private String                             sellerCode;

    @Tag(7)
    private Map<String, WeightUpdateSRO>       supcToLatestUpdates    = new HashMap<String, WeightUpdateSRO>();

    @Tag(8)
    private Map<String, List<WeightUpdateSRO>> supctoAggregateUpdates = new HashMap<String, List<WeightUpdateSRO>>();

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public String getSellerCode() {
        return sellerCode;
    }

    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }

    public Map<String, WeightUpdateSRO> getSupcToLatestUpdates() {
        return supcToLatestUpdates;
    }

    public void setSupcToLatestUpdates(Map<String, WeightUpdateSRO> supcToLatestUpdates) {
        this.supcToLatestUpdates = supcToLatestUpdates;
    }

    public Map<String, List<WeightUpdateSRO>> getSupctoAggregateUpdates() {
        return supctoAggregateUpdates;
    }

    public void setSupctoAggregateUpdates(Map<String, List<WeightUpdateSRO>> supctoAggregateUpdates) {
        this.supctoAggregateUpdates = supctoAggregateUpdates;
    }

    public void addSupcToLatestUpdates(String supc, WeightUpdateSRO weightUpdateSRO) {
        supcToLatestUpdates.put(supc, weightUpdateSRO);
    }
}
