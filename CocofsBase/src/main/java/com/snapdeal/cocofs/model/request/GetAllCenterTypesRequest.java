/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.model.request;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.snapdeal.base.model.common.ServiceRequest;

/**
 * @version 1.0, 30-Jul-2015
 * @author nitish
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetAllCenterTypesRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = -9078875890418178026L;

    public GetAllCenterTypesRequest() {
    }

    @Override
    public String toString() {
        return "GetAllCenterTypesRequest [toString()=" + super.toString() + "]";
    }

}
