package com.snapdeal.cocofs.model.request;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.annotations.NotNull;
import com.snapdeal.base.model.common.ServiceRequest;
import com.snapdeal.cocofs.annotation.NonZero;

/**
 * 
 * @author kirti
 *
 */
		
@JsonIgnoreProperties(ignoreUnknown =  true)
public class SetProductCapturedWeightRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 4533373422402859336L;

    @Tag(3)
    @NotNull
    private String            supc;

    @Tag(4)
    @NonZero
    @NotNull
    private Double           weight;
    
    @Tag(5)
    @NotNull
    private String           email;

    @Tag(6)
    private Boolean          superUser;
    
    public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getSupc() {
		return supc;
	}


	public void setSupc(String supc) {
		this.supc = supc;
	}


	public Double getWeight() {
		return weight;
	}


	public void setWeight(Double weight) {
		this.weight = weight;
	}


    public Boolean isSuperUser() {
		return superUser;
	}


	public void setSuperUser(Boolean superUser) {
		this.superUser = superUser;
	}
	
	@Override
    public String toString() {
        return "setProductCapturedWeightRequest [supc=" + supc + ", weight=" + weight + ", email=" + email + ", superUser=" + superUser + "]";
    }

}
