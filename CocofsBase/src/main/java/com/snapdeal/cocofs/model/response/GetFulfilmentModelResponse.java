/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 30-Sep-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.model.response;

import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.model.common.ServiceResponse;
import com.snapdeal.base.validation.ValidationError;
import com.snapdeal.cocofs.sro.FulfillmentModel;
import com.snapdeal.cocofs.sro.SellerSUPCPair;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetFulfilmentModelResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long                     serialVersionUID            = 6423414961984946920L;

    @Tag(5)
    private Map<SellerSUPCPair, FulfillmentModel> sellerSupcToFulfilmentModel = new HashMap<SellerSUPCPair, FulfillmentModel>();

    @Tag(6)
    private Map<SellerSUPCPair, ValidationError>  failures                    = new HashMap<SellerSUPCPair, ValidationError>();

    @Tag(7)
    private Integer                               successCount;

    public GetFulfilmentModelResponse() {
    }

    public GetFulfilmentModelResponse(Map<SellerSUPCPair, FulfillmentModel> sellerSupcToFulfilmentModel) {
        this.sellerSupcToFulfilmentModel = sellerSupcToFulfilmentModel;
    }

    public Map<SellerSUPCPair, FulfillmentModel> getSellerSupcToFulfilmentModel() {
        return sellerSupcToFulfilmentModel;
    }

    public void setSellerSupcToFulfilmentModel(Map<SellerSUPCPair, FulfillmentModel> sellerSupcToFulfilmentModel) {
        this.sellerSupcToFulfilmentModel = sellerSupcToFulfilmentModel;
    }

    public Map<SellerSUPCPair, ValidationError> getFailures() {
        return failures;
    }

    public void setFailures(Map<SellerSUPCPair, ValidationError> failures) {
        this.failures = failures;
    }

    public void addFailures(ValidationError error, SellerSUPCPair sellerSupcPair) {
        failures.put(sellerSupcPair, error);
    }
    
    public Integer getSuccessCount() {
        return successCount;
    }

    public void setSuccessCount(Integer successCount) {
        this.successCount = successCount;
    }

    @Override
    public String toString() {
        return "GetFulfilmentModelResponse [sellerSupcToFulfilmentModel=" + sellerSupcToFulfilmentModel + ", failures=" + failures + ", successCount=" + successCount + "]";
    }

}
