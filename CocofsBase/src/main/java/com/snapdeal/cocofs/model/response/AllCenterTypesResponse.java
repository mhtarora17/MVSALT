/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.model.response;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.model.common.ServiceResponse;

/**
 * @version 1.0, 30-Jul-2015
 * @author nitish
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class AllCenterTypesResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 884859161777119985L;

    @Tag(21)
    List<String>              centerTypes      = new ArrayList<String>();

    public AllCenterTypesResponse() {
        super();
    }

    public AllCenterTypesResponse(List<String> centerTypes) {
        super();
        this.centerTypes = centerTypes;
    }

    public List<String> getCenterTypes() {
        return centerTypes;
    }

    public void setCenterTypes(List<String> centerTypes) {
        this.centerTypes = centerTypes;
    }

    @Override
    public String toString() {
        return "AllCenterTypesResponse [centerTypes=" + centerTypes + ", toString()=" + super.toString() + "]";
    }

}
