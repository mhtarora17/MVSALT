/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.model.response;

import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.model.common.ServiceResponse;
import com.snapdeal.base.validation.ValidationError;
import com.snapdeal.cocofs.sro.FMFCPair;
import com.snapdeal.cocofs.sro.SellerSUPCPair;

/**
 * @version 1.0, 27-Mar-2015
 * @author ankur
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetFMAndFCBySellerSupcListResponse extends ServiceResponse {
    /**
     * 
     */
    private static final long                    serialVersionUID    = 7401029321145009743L;
    @Tag(5)
    private Map<SellerSUPCPair, FMFCPair>        sellerSupcToFMAndFC = new HashMap<SellerSUPCPair, FMFCPair>();

    @Tag(6)
    private Map<SellerSUPCPair, ValidationError> failures            = new HashMap<SellerSUPCPair, ValidationError>();

    @Tag(7)
    private Integer                              successCount;

    public GetFMAndFCBySellerSupcListResponse() {
    }

    public GetFMAndFCBySellerSupcListResponse(Map<SellerSUPCPair, FMFCPair> sellerSupcToFMAndFC) {
        this.sellerSupcToFMAndFC = sellerSupcToFMAndFC;
    }

    public Map<SellerSUPCPair, FMFCPair> getSellerSupcToFMAndFC() {
        return sellerSupcToFMAndFC;
    }

    public void setSellerSupcToFMAndFC(Map<SellerSUPCPair, FMFCPair> sellerSupcToFMAndFC) {
        this.sellerSupcToFMAndFC = sellerSupcToFMAndFC;
    }

    public Map<SellerSUPCPair, ValidationError> getFailures() {
        return failures;
    }

    public void setFailures(Map<SellerSUPCPair, ValidationError> failures) {
        this.failures = failures;
    }

    public void addFailures(ValidationError error, SellerSUPCPair sellerSupcPair) {
        failures.put(sellerSupcPair, error);
    }

    public Integer getSuccessCount() {
        return successCount;
    }

    public void setSuccessCount(Integer successCount) {
        this.successCount = successCount;
    }

    @Override
    public String toString() {
        return "GetFMAndFCBySellerSupcListResponse [sellerSupcToFMAndFC=" + sellerSupcToFMAndFC + ", failures=" + failures + ", successCount=" + successCount + "]";
    }

}
