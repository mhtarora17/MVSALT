package com.snapdeal.cocofs.model.response;

import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.model.common.ServiceResponse;
import com.snapdeal.base.validation.ValidationError;
/**
 * 
 * @author kirti
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class GetProductSystemWeightCapturedFlagResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long            serialVersionUID = -6326576626488932159L;
    
    @Tag(5)
    private String supc;

    @Tag(6)
    private String systemWeightCaptured;
    
	@Tag(7)
    private Map<String, ValidationError> failures         = new HashMap<String, ValidationError>();
	
    public String getSupc() {
		return supc;
	}

	public void setSupc(String supc) {
		this.supc = supc;
	}



	public String getSystemWeightCaptured() {
		return systemWeightCaptured;
	}

	public void setSystemWeightCaptured(String systemWeightCaptured) {
		this.systemWeightCaptured = systemWeightCaptured;
	}
    
    public Map<String, ValidationError> getFailures() {
        return failures;
    }

    public void setFailures(Map<String, ValidationError> failures) {
        this.failures = failures;
    }

    public void addFailures(ValidationError error, String supc) {
        failures.put(supc, error);
    }
    
    @Override
    public String toString() {
        return "GetProductSystemWeightCapturedFlagResponse [supc=" + supc + ", systemWeightCaptured=" + systemWeightCaptured + "]";
    }

}
