package com.snapdeal.cocofs.model.request;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.annotations.NotNull;
import com.snapdeal.base.model.common.ServiceRequest;
import com.snapdeal.cocofs.annotation.NonNegative;
import com.snapdeal.cocofs.sro.AddressSRO;

/**
 * Request class to retrieve tax rates for a SUPC/State/both
 * 
 * @version 1.0 19th August 2014
 * @author gauravg
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class GetTaxInfoRequest extends ServiceRequest {

   
    /**
     * 
     */
    private static final long serialVersionUID = 9083184482257431252L;

    @NotNull
    @Tag(5)
    private String supc; //SUPC of the product -- mandatory

    @NotNull
    @Tag(6)
    private String sellerCode; //State of origin -- mandatory

    @Tag(7)
    private String category; //category

    @NotNull
    @Tag(8)
    private AddressSRO sellerAddress;

    @NotNull
    @Tag(9)
    private AddressSRO customerAddress;

    @NotNull
    @NonNegative
    @Tag(10)
    private Integer price; //Price -- mandatory

    public String getSupc() {
        return supc;
    }

    public void setSupc(String supc) {
        this.supc = supc;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSellerCode() {
        return sellerCode;
    }

    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public AddressSRO getSellerAddress() {
        return sellerAddress;
    }

    public void setSellerAddress(AddressSRO sellerAddress) {
        this.sellerAddress = sellerAddress;
    }

    public AddressSRO getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(AddressSRO customerAddress) {
        this.customerAddress = customerAddress;
    }

    @Override
    public String toString() {
        return "GetTaxInfoRequest [supc=" + supc + ", sellerCode=" + sellerCode + ", category=" + category + ", sellerAddress=" + sellerAddress + ", customerAddress="
                + customerAddress + ", price=" + price + "]";
    }

}
