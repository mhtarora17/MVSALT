/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 26-Nov-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.model.response;

import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.model.common.ServiceResponse;
import com.snapdeal.cocofs.sro.CategoryShippingModeSRO;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetCategoryModeMappingResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -5734113385045829624L;
    
    @Tag(5)
    private Map<String, CategoryShippingModeSRO>   categoryModes = new HashMap<String, CategoryShippingModeSRO>();
    
    public Map<String, CategoryShippingModeSRO> getCategoryModes() {
        return categoryModes;
    }
    public void setCategoryModes(Map<String, CategoryShippingModeSRO> categoryModes) {
        this.categoryModes = categoryModes;
    }
    
    
    
    
    
}
