/*
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 16-Apr-2014
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.model.response;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.model.common.ServiceResponse;
import com.snapdeal.cocofs.sro.SellerFMMappingUpdateFailedSro;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SetSellerFMMappingResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long              serialVersionUID = -6785955900030629017L;
    /**
     * If seller FM Mapping cannot be updated, then this sro contains validation error for the same
     */
    @Tag(5)
    private SellerFMMappingUpdateFailedSro sellerFMMappingUpdateFailedSro;

    public SetSellerFMMappingResponse() {
    }

    public SetSellerFMMappingResponse(SellerFMMappingUpdateFailedSro sellerFMMappingUpdateFailedSro) {
        this.sellerFMMappingUpdateFailedSro = sellerFMMappingUpdateFailedSro;
    }

    public SellerFMMappingUpdateFailedSro getSellerFMMappingUpdateFailedSro() {
        return sellerFMMappingUpdateFailedSro;
    }

    public void setSellerFMMappingUpdateFailedSro(SellerFMMappingUpdateFailedSro sellerFMMappingUpdateFailedSro) {
        this.sellerFMMappingUpdateFailedSro = sellerFMMappingUpdateFailedSro;
    }

    @Override
    public String toString() {
        return "SetSellerFMMappingResponse [sellerFMMappingUpdateFailedSro=" + sellerFMMappingUpdateFailedSro + "]";
    }

}
