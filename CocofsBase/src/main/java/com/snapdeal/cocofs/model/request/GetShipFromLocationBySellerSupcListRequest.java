package com.snapdeal.cocofs.model.request;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.annotations.NotNull;
import com.snapdeal.base.model.common.ServiceRequest;
import com.snapdeal.cocofs.sro.SellerSUPCPair;


@JsonIgnoreProperties(ignoreUnknown = true)
public class GetShipFromLocationBySellerSupcListRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 484786225477717054L;

    @Tag(10)
    @NotNull
    private List<SellerSUPCPair> sellerSupcPairs = new ArrayList<SellerSUPCPair>();

    public GetShipFromLocationBySellerSupcListRequest() {
    }

    public GetShipFromLocationBySellerSupcListRequest(List<SellerSUPCPair> sellerSupcPairs) {
        this.sellerSupcPairs = sellerSupcPairs;
    }

    public List<SellerSUPCPair> getSellerSupcPairs() {
        return sellerSupcPairs;
    }

    public void setSellerSupcPairs(List<SellerSUPCPair> sellerSupcPairs) {
        this.sellerSupcPairs = sellerSupcPairs;
    }

    @Override
    public String toString() {
        return "GetShipFromLocationBySellerSupcListRequest [sellerSupcPairs=" + sellerSupcPairs + "]";
    }

}
