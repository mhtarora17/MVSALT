/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.model.queue;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.annotations.NotNull;

/**
 *  
 *  @version     1.0, 25-Sep-2014
 *  @author ankur
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class ShipFromUpdateMessage  implements Serializable {
    
    
     

    /**
     * 
     */
    private static final long serialVersionUID = 4112570304687119758L;

    @Tag(1)
    @NotNull
    private String            sellerCode;
   
    @Tag(2)
    @NotNull
    private String            supc;
    
    @Tag(3)
    private String            pincode;

    public ShipFromUpdateMessage(){
        
    }
    
    public String getSellerCode() {
        return sellerCode;
    }

    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }

    public String getSupc() {
        return supc;
    }

    public void setSupc(String supc) {
        this.supc = supc;
    }

   

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    @Override
    public String toString() {
        return "ShipFromUpdateMessgae [sellerCode=" + sellerCode + ", supc=" + supc + ", pincode=" + pincode + "]";
    }
    
    

}
