package com.snapdeal.cocofs.model.queue;

import com.dyuproject.protostuff.Tag;

public enum SellerSupcUpdateParameters {

    FULFILMENT_MODEL("fm"), //
    PINCODE("pincode"), // 
    SDINSTANT("sdinstant"), // 
    FULFILMENTCENTER("FC"), //
    SDPLUS("sd+");

    @Tag(1)
    private String                                     code;


    private SellerSupcUpdateParameters(String code) {
        this.code = code;

    }

    public String getCode() {
        return code;
    }

}
