/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 26-Nov-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.model.response;

import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.model.common.ServiceResponse;
import com.snapdeal.cocofs.sro.SupcShippingModeSRO;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetSupcModeMappingResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = -5734113385045829624L;
    
    @Tag(5)
    private Map<String, SupcShippingModeSRO> supcModes = new HashMap<String, SupcShippingModeSRO>();

    public Map<String, SupcShippingModeSRO> getSupcModes() {
        return supcModes;
    }

    public void setSupcModes(Map<String, SupcShippingModeSRO> supcModes) {
        this.supcModes = supcModes;
    }

    @Override
    public String toString() {
        return "GetSupcModeMappingResponse [supcModes=" + supcModes + "]";
    }
    
    


    
 
}
