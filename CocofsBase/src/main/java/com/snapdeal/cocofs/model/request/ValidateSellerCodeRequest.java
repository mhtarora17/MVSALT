package com.snapdeal.cocofs.model.request;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.annotations.NotNull;
import com.snapdeal.base.model.common.ServiceRequest;

/**
 * 24-Jul-2015
 * @author indrajit
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ValidateSellerCodeRequest extends ServiceRequest {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1524871254781L;
	
	/**
	 *  List of seller which needs to be checked
	 */
	@Tag(11)
    @NotNull
    private List<String>            sellerList;
	
	public ValidateSellerCodeRequest(){
		super();
	}

	public ValidateSellerCodeRequest(List<String> sellerList) {
		super();
		this.sellerList = sellerList;
	}

	public List<String> getSellerList() {
		return sellerList;
	}

	public void setSellerList(List<String> sellerList) {
		this.sellerList = sellerList;
	}

	@Override
	public String toString() {
		return "ValidateSellerCodeRequest [sellerList=" + sellerList + "]";
	}
	
	

}
