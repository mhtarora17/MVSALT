package com.snapdeal.cocofs.model.request;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.annotations.NotNull;
import com.snapdeal.base.model.common.ServiceRequest;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetProductDeliveryTypeRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 6313473227326676294L;

    @Tag(3)
    @NotNull
    private List<String>            supcList;

    public List<String> getSupcList() {
        return supcList;
    }

    public void setSupcList(List<String> supcList) {
        this.supcList = supcList;
    }

    @Override
    public String toString() {
        return "GetProductDeliveryTypeRequest [supcList=" + supcList + "]";
    }
    

}
