/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.model.request;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.model.common.ServiceResponse;

/**
 *  
 *  @version     1.0, 24-Sep-2014
 *  @author ankur
 */
public class GetSDFulfilledStatusBySupcSellersSubcatResponse extends ServiceResponse{



	@Tag(10)
    private boolean            sdFulfilled;

    
    public boolean isSdFulfilled() {
        return sdFulfilled;
    }

    public void setSdFulfilled(boolean sdFulfilled) {
        this.sdFulfilled = sdFulfilled;
    }
    

    @Override
	public String toString() {
		return "GetSDFulfilledStatusBySupcSellersSubcatResponse [sdFulfilled="
				+ sdFulfilled + "]";
	}
    
    
    
}
