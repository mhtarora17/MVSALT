/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.model.queue;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;

/**
 *  
 *  @version     1.0, 25-Sep-2014
 *  @author ankur
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class ShipFromUpdateEvent  implements Serializable {
      

    /**
     * 
     */
    private static final long serialVersionUID = -5654461406147421852L;

    private List<ShipFromUpdateMessage>  sellersupcPincodeUpdates = new ArrayList<ShipFromUpdateMessage>();

    public ShipFromUpdateEvent(){
        
    }

    public List<ShipFromUpdateMessage> getSellersupcPincodeUpdates() {
        return sellersupcPincodeUpdates;
    }

    public void setSellersupcPincodeUpdates(List<ShipFromUpdateMessage> sellersupcPincodeUpdates) {
        this.sellersupcPincodeUpdates = sellersupcPincodeUpdates;
    }

    @Override
    public String toString() {
        return "SellerSupcPincodeUpdateEvent [sellersupcPincodeUpdates=" + sellersupcPincodeUpdates + "]";
    }
    
   

}
