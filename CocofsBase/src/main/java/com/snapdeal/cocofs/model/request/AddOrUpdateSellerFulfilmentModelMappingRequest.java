/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 30-Sep-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.model.request;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.annotations.NotNull;
import com.snapdeal.base.model.common.ServiceRequest;
import com.snapdeal.cocofs.sro.FulfillmentModel;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AddOrUpdateSellerFulfilmentModelMappingRequest extends ServiceRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 4643077448377916751L;

    @Tag(3)
    @NotNull
    private String            sellerCode;

    @Tag(4)
    @NotNull
    private FulfillmentModel  fulfilmentModel;

    public AddOrUpdateSellerFulfilmentModelMappingRequest() {
    }

    public AddOrUpdateSellerFulfilmentModelMappingRequest(String sellerCode, FulfillmentModel fulfilmentModel) {
        this.sellerCode = sellerCode;
        this.fulfilmentModel = fulfilmentModel;
    }

    public String getSellerCode() {
        return sellerCode;
    }

    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }

    public FulfillmentModel getFulfilmentModel() {
        return fulfilmentModel;
    }

    public void setFulfilmentModel(FulfillmentModel fulfilmentModel) {
        this.fulfilmentModel = fulfilmentModel;
    }

    @Override
    public String toString() {
        return "AddOrUpdateSellerFulfilmentModelMappingRequest [sellerCode=" + sellerCode + ", fulfilmentModel=" + fulfilmentModel + "]";
    }

}
