/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 26-Nov-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.model.response;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dyuproject.protostuff.Tag;
import com.snapdeal.base.model.common.ServiceResponse;
import com.snapdeal.cocofs.sro.FulfillmentModel;
import com.snapdeal.cocofs.sro.ProductFulfilmentAttributeSRO;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetFulfilmentModelAndProductAttributesResponse extends ServiceResponse {

    /**
     * 
     */
    private static final long             serialVersionUID = 9103456191314234865L;

    @Tag(5)
    private FulfillmentModel              fulfilmentModel;

    @Tag(6)
    private ProductFulfilmentAttributeSRO productAttributeSro;

    public GetFulfilmentModelAndProductAttributesResponse() {
    }

    public GetFulfilmentModelAndProductAttributesResponse(FulfillmentModel fulfilmentModel, ProductFulfilmentAttributeSRO productAttributeSro) {
        this.fulfilmentModel = fulfilmentModel;
        this.productAttributeSro = productAttributeSro;
    }

    public FulfillmentModel getFulfilmentModel() {
        return fulfilmentModel;
    }

    public void setFulfilmentModel(FulfillmentModel fulfilmentModel) {
        this.fulfilmentModel = fulfilmentModel;
    }

    public ProductFulfilmentAttributeSRO getProductAttributeSro() {
        return productAttributeSro;
    }

    public void setProductAttributeSro(ProductFulfilmentAttributeSRO productAttributeSro) {
        this.productAttributeSro = productAttributeSro;
    }

    @Override
    public String toString() {
        return "GetFulfilmentModelAndProductAttributesResponse [fulfilmentModel=" + fulfilmentModel + ", productAttributeSro=" + productAttributeSro + "]";
    }
}
