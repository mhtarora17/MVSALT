//use root permission to do all these steps

1)Get setup (wget -O aerospike-3.tgz http://www.aerospike.com/download/server/3.3.17/artifact/el6)

2)Extract tgz file (tar xvzf aerospike-3.tgz)

3)cd aerospike-server-community-3.3.17-el6

4)install tools (rpm -i aerospike-tools-3.3.18-1.el6.x86_64.rpm)

5)install aerospike (rpm -i aerospike-server-community-3.3.17-1.el6.x86_64.rpm)

6)Perform this on other node 

7)Copy aerospike.conf in /etc/aerospike/

8) create direcotories /data/aerospike/ on each aerospike node server.

9)Mention ip address of aerospike node in other node. Placeholder for this is present in /etc/aerospike/aerospike.conf

10)Open port 3000,3001,3002,3003 on each node, such that it can be connected from other node in cluster, IPMS web, IPMS tomcat

11)Start aerospike on each node (/etc/init.d/aerospike start)

---------------------------------------------------------------------------------------------------------

Install Aerospike Monitoring console on any of the aerospike node, or preferably on another hardware using
 
1)http://www.aerospike.com/docs/amc/install/linux/el6/ 

2) See the port in /opt/amc/config/gunicorn_config.py and open this for office network.Make sure this box is able to connect to Aerospike nodes.

3)Start AMC using /etc/init.d/amc start


