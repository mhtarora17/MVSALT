/*
 *  Copyright 2012 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 08-Oct-2012
 *  @author amd
 */
package com.snapdeal.cocofs.configuration;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.snapdeal.base.cache.CacheManager;
import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.cocofs.configuration.exception.InvalidConfigurationException;

/**
 * Retrieves value of a specified {@link Property} from either file configuration (COCOFS_CONFIG_RELATIVE_PATH) or
 * {@link com.snapdeal.Property.core.entity.CocofsProperty} table. If present, value if file overrides value in DB. All
 * methods present in this util are static.
 * 
 * @author amd
 */
public class ConfigUtils {

    private static final Logger LOG = LoggerFactory.getLogger(ConfigUtils.class);

    /**
     * Get the value of scalar {@link Property} having a String value.
     * <p>
     * Order of precedence:
     * <p>
     * 1. Name of the {@link Property} prefixed by {@link Environment} present in COCOFS_CONFIG_RELATIVE_PATH. Example:
     * prod#ipms.web.service.url.
     * <p>
     * 2. Name of the {@link Property} present in COCOFS_CONFIG_RELATIVE_PATH without any prefix. Such a property would
     * be applicable across all envirnments.
     * <p>
     * 3. Name of the {@link Property} present in {@link com.snapdeal.cocofsProperty.core.entity.CocofsProperty} table.
     * <p>
     * 4. Default value of {@link Property}
     * <p>
     * 5. Returns null if default value is not found. Should never happen. Note: {@link Property} name present in
     * {@link com.snapdeal.cocofsProperty.core.entity.CocofsProperty} will never have environment prefixed.
     * <p>
     * 
     * @param p
     * @return
     */
    public static String getStringScalar(Property p) {
        String value = null;

        // look for env.p.name
        try {
            value = CacheManager.getInstance().getCache(FilePropertiesCache.class).getScalar(overridePropertyName(p));
        } catch (InvalidConfigurationException e) {
        } catch (NullPointerException npe) {
        }

        // then look for p.name
        if (StringUtils.isEmpty(value)) {
            try {
                value = CacheManager.getInstance().getCache(FilePropertiesCache.class).getScalar(p.getName());
            } catch (InvalidConfigurationException e) {
            } catch (NullPointerException npe) {
            }
        }

        // then look in Cocofs_property
        if (StringUtils.isEmpty(value)) {
            value = CacheManager.getInstance().getCache(CocofsPropertiesCache.class).getProperty(p.getName());
        } 

        // then use default
        if (StringUtils.isEmpty(value)) {
            value = p.getValue();
        }
        return value;
    }

    /**
     * Get the value of scalar {@link Property} having a Boolean value.
     * 
     * @param p
     * @return
     */
    public static boolean getBooleanScalar(Property p) {
        if ("1".equals(getStringScalar(p)) || "true".equalsIgnoreCase(getStringScalar(p))) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get the value of scalar {@link Property} having a Integer value.
     * 
     * @param p
     * @return
     */
    public static int getIntegerScalar(Property p) {
        String s = getStringScalar(p);
        try {
            return Integer.parseInt(s);
        } catch (NumberFormatException ex) {
            LOG.warn("Incorrect property: {} found in file. Default value: {} will be used.", p.getName(), p.getValue());
            return Integer.parseInt(p.getValue());
        }
    }

    /**
     * Get the value of scalar {@link Property} having a Integer value.
     * 
     * @param p
     * @return
     */
    public static double getDoubleScalar(Property p) {
        String s = getStringScalar(p);
        try {
            return Double.parseDouble(s);
        } catch (NumberFormatException ex) {
            LOG.warn("Incorrect property: {} found in file. Default value: {} will be used.", p.getName(), p.getValue());
            return Double.parseDouble(p.getValue());
        }
    }

    /**
     * Get the value of map {@link Property} having String as key and String value. Such properties can be present only
     * in the COCOFS_CONFIG_RELATIVE_PATH.
     * 
     * @param p
     * @param key
     * @return
     */
    public static String getMapProperty(Property p, String key) {

        Map<String, String> map = getMap(p);
        String value = null;

        // look for env.p.name
        if (map != null && map.size() != 0) {
            value = map.get(key);
        }

        return value;
    }

    /**
     * Returns a list of String for a property name. Same order of precedence applies as for getStringScalar.
     * 
     * @param p
     * @param delimitor
     * @return
     */
    public static List<String> getStringList(Property p, String delimitor) {
        List<String> list = null;

        // look for env.p.name
        try {
            list = CacheManager.getInstance().getCache(FilePropertiesCache.class).getList(overridePropertyName(p));
        } catch (InvalidConfigurationException e) {
        } catch (NullPointerException npe) {
        }

        // look for p.name
        if (list == null) {
            try {
                list = CacheManager.getInstance().getCache(FilePropertiesCache.class).getList(p.getName());
            } catch (InvalidConfigurationException e) {
            } catch (NullPointerException npe) {
            }
        }

        // look for p.name in Cocofs_property
        if (list == null) {
            String propValue = CacheManager.getInstance().getCache(CocofsPropertiesCache.class).getProperty(p.getName());
            list = StringUtils.isNotEmpty(propValue) ? StringUtils.split(propValue, delimitor) : null;
        }

        // use default value
        if (list == null) {
            list = Collections.unmodifiableList(StringUtils.split(p.getValue()));
        }

        if (list != null) {
            return list;
        }
        // return empty list. should *never* happen.
        list = new ArrayList<String>(0);
        return list;
    }

    /**
     * Returns a list of String for a property name. Same order of precedence applies as for getStringScalar. Uses ","
     * as default delimitor.
     * 
     * @param p
     * @return
     */
    public static List<String> getStringList(Property p) {
        return getStringList(p, ",");
    }

    /**
     * Returns a list of integer for a property name.
     * 
     * @param p
     * @param delimitor
     * @return
     */
    public static List<Integer> getIntegerList(Property p, String delimitor) {
        List<String> list = getStringList(p, delimitor);
        List<Integer> returnList = new ArrayList<Integer>();
        for (String item : list) {
            try {
                returnList.add(Integer.parseInt(item));
            } catch (NumberFormatException e) {
                LOG.error("NumberFormatException while parsing Property: {}. Returning BLANK integer list.", p.getName());
                return new ArrayList<Integer>();
            }
        }
        return returnList;
    }

    /**
     * Returns a list of integer for a property name. Uses "," as default delimitor.
     * 
     * @param p
     * @return
     */
    public static List<Integer> getIntegerList(Property p) {
        return getIntegerList(p, ",");
    }

    /**
     * Returns a K,V map for a given {@link Property}. Such a property can be present only in
     * COCOFS_CONFIG_RELATIVE_PATH.
     * 
     * @param p
     * @return
     */
    public static Map<String, String> getMap(Property p) {
        Map<String, String> map = null;
        map = getMapFromFilePropertiesCache(p);
        return map == null ? getMapFromCocofsMapPropertiesCache(p) : map;

    }

    private static Map<String, String> getMapFromFilePropertiesCache(Property p) {
        Map<String, String> map = null;
        try {
            map = CacheManager.getInstance().getCache(FilePropertiesCache.class).getMap(overridePropertyName(p));
        } catch (InvalidConfigurationException ex) {
            LOG.warn("Property: {} not found in file. Returning null.", p.getName());
        }

        if (map == null) {
            try {
                map = CacheManager.getInstance().getCache(FilePropertiesCache.class).getMap(p.getName());
            } catch (InvalidConfigurationException e) {
                LOG.error("Exception {}", e);
            } catch (NullPointerException npe) {
                LOG.error("Exception {}", npe);
            }
        }
        return map;
    }

    private static Map<String, String> getMapFromCocofsMapPropertiesCache(Property p) {
        Map<String, String> map = null;
        if (map == null) {
            try {
                map = CacheManager.getInstance().getCache(CocofsMapPropertiesCache.class).getMap(overridePropertyName(p));
            } catch (InvalidConfigurationException ex) {
                LOG.warn("Property: {} not found in CocofsMapProperties. Returning null.", p.getName());
            }
        }
        if (map == null) {

            try {
                map = CacheManager.getInstance().getCache(CocofsMapPropertiesCache.class).getMap(p.getName());
            } catch (InvalidConfigurationException e) {
                LOG.error("Exception {}", e);
            } catch (NullPointerException npe) {
                LOG.error("Exception {}", npe);
            }

        }

        return map;
    }

    /**
     * @param p
     * @param value
     * @return
     */
    public static boolean isPresentInList(Property p, String value) {
        List<String> list = getStringList(p);
        return list != null ? list.contains(value) : false;
    }

    /**
     * @param p
     * @param value
     * @return
     */
    public static boolean isPresentInList(Property p, int value) {
        List<Integer> list = getIntegerList(p);
        return list != null ? list.contains(value) : false;
    }

    private static String overridePropertyName(Property p) {
        return StringUtils.join(Environment.DELIMITOR.getName().charAt(0), System.getProperty(Environment.KEY.getName()), p.getName());
    }
}
