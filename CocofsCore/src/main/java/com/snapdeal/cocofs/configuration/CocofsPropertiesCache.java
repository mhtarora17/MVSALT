/*
 *  Copyright 2012 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 21-Nov-2012
 *  @author prateek
 */
package com.snapdeal.cocofs.configuration;

import java.util.HashMap;
import java.util.Map;

import com.snapdeal.base.annotations.Cache;

@Cache(name = "properties")
public class CocofsPropertiesCache {
    

    private final Map<String, String> properties = new HashMap<String, String>();

    public void addProperty(String name, String value) {
        properties.put(name, value);
    }

    public String getProperty(String name) {
        return properties.get(name);
    }

    public String getProperty(String name, String defaultValue) {
        String value = properties.get(name);
        return value != null ? value : defaultValue;
    }



}
