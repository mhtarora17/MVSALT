/*
 *  Copyright 2012 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 29-Jun-2012
 *  @author sankalp
 */
package com.snapdeal.cocofs.configuration.exception;

public class InvalidConfigurationException extends Exception {
    private static final long serialVersionUID = 1027150492317778428L;
    private String            message          = "";

    public InvalidConfigurationException() {
    }

    public InvalidConfigurationException(String message) {
        this.message = message;
    }

    public void printStackTrace() {
        System.out.println(message);
        super.printStackTrace();
    }
}
