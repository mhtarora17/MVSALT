/*
 *  Copyright 2012 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 21-Nov-2012
 *  @author prateek
 */
package com.snapdeal.cocofs.configuration;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.snapdeal.base.annotations.Cache;
import com.snapdeal.cocofs.configuration.exception.InvalidConfigurationException;

@Cache(name = "FileProperties")
public class FilePropertiesCache {
    private Map<String, Object> cocofsFilePropertyMap = new ConcurrentHashMap<String, Object>();

    public FilePropertiesCache() {
    }

    public FilePropertiesCache(Map<String, Object> maps) {
        cocofsFilePropertyMap = maps;
    }

    protected String getScalar(String property) throws InvalidConfigurationException {
        property = property.toLowerCase();
        if (cocofsFilePropertyMap.containsKey(property)) {
            if (!(cocofsFilePropertyMap.get(property) instanceof Collection<?>)) {
                return (String) cocofsFilePropertyMap.get(property);
            } else {
                throw new InvalidConfigurationException("The property passed is a collection");
            }
        }
        return null;
    }

    protected String getMapValue(String property, String key) throws InvalidConfigurationException {
        property = property.toLowerCase();
        if (cocofsFilePropertyMap.containsKey(property)) {
            if (cocofsFilePropertyMap.get(property) instanceof Map<?, ?>) {
                @SuppressWarnings("unchecked")
                Map<String, String> tempMap = (Map<String, String>) cocofsFilePropertyMap.get(property);
                return tempMap.get(key);
            } else {
                throw new InvalidConfigurationException("The property passed is not a Map");
            }
        }
        return null;
    }

    protected List<String> getList(String property) throws InvalidConfigurationException {
        property = property.toLowerCase();
        if (cocofsFilePropertyMap.containsKey(property) && cocofsFilePropertyMap.get(property) != null) {
            if (cocofsFilePropertyMap.get(property) instanceof List<?>) {
                List<String> list = (List<String>) cocofsFilePropertyMap.get(property);
                return new ArrayList<String>(list);
            } else {
                throw new InvalidConfigurationException("The property Called is not a List");
            }
        }
        return null;
    }

    protected Map<String, String> getMap(String property) throws InvalidConfigurationException {
        property = property.toLowerCase();
        if (cocofsFilePropertyMap.containsKey(property)) {
            if (cocofsFilePropertyMap.get(property) instanceof Map<?, ?>) {
                Map<String, String> tempMap = (Map<String, String>) cocofsFilePropertyMap.get(property);
                return new ConcurrentHashMap<String, String>(tempMap);
            } else {
                throw new InvalidConfigurationException("The property Called is not a Map");
            }
        }
        return null;
    }

    protected boolean isPresentInList(String property, String value) {
        property = property.toLowerCase();
        if (cocofsFilePropertyMap.containsKey(property) && cocofsFilePropertyMap.get(property) instanceof List<?>) {
            List<String> list = (List<String>) cocofsFilePropertyMap.get(property);
            return list.contains(value);
        }
        return false;
    }


}
