/*
 *  Copyright 2012 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 27-Jun-2012
 *  @author sankalp
 */
package com.snapdeal.cocofs.configuration.exception;

public class InvalidFormatException extends Exception {

    private static final long serialVersionUID = -9076109639122796180L;

    private String            fileName;

    public InvalidFormatException(String fileName) {
        this.fileName = fileName;
    }

    public InvalidFormatException() {
        this.fileName = "";
    }

    public void printStackTrace() {
        System.out.println("fileName:" + fileName);
        super.printStackTrace();
    }
}
