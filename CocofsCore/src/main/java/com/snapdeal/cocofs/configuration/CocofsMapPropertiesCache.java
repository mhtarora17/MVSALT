package com.snapdeal.cocofs.configuration;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.snapdeal.base.annotations.Cache;
import com.snapdeal.cocofs.configuration.exception.InvalidConfigurationException;

@Cache(name = "cocofsmapproperties")
public class CocofsMapPropertiesCache {

    private Map<String, Map<String, String>> cocofsMapProperties = new ConcurrentHashMap<String, Map<String, String>>();

    public CocofsMapPropertiesCache() {
    }

    public CocofsMapPropertiesCache(Map<String, Map<String, String>> maps) {
        cocofsMapProperties = maps;
    }

    protected String getMapValue(String property, String key) throws InvalidConfigurationException {
        property = property.toLowerCase();
        if (cocofsMapProperties.containsKey(property)) {
            if (cocofsMapProperties.get(property) instanceof Map<?, ?>) {
                Map<String, String> tempMap = (Map<String, String>) cocofsMapProperties.get(property);
                return tempMap.get(key);
            } else {
                throw new InvalidConfigurationException("The property passed is not a Map");
            }
        }
        return null;
    }

    protected Map<String, String> getMap(String property) throws InvalidConfigurationException {
        property = property.toLowerCase();
        if (cocofsMapProperties.containsKey(property)) {
            if (cocofsMapProperties.get(property) instanceof Map<?, ?>) {
                Map<String, String> tempMap = (Map<String, String>) cocofsMapProperties.get(property);
                return new ConcurrentHashMap<String, String>(tempMap);
            } else {
                throw new InvalidConfigurationException("The property Called is not a Map");
            }
        }
        return null;
    }

}
