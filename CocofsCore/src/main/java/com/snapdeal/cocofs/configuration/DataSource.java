package com.snapdeal.cocofs.configuration;

/**
 * Enum describing various 
 * data source variants
 * available to the application 
 * to read from or write to.
 * Example - MongoDB, Aerospike, MySql, 
 * Memcache, Redis, etc.
 * 
 * @author gaurav
 *
 */
public enum DataSource {
	MYSQL, MONGODB, AEROSPIKE
	//More data sources can e added in future..
}
