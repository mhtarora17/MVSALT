/*
 *  Copyright 2012 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 08-Oct-2012
 *  @author amd
 */
package com.snapdeal.cocofs.configuration;


/**
 * Name and default value of each property used in shipping system. Each of these Property's name can be present at:  
 * <p>
 * 1. CONFIG_RELATIVE_PATH: prefixed by {@link Environment} name. Property name would be applicable globally 
 *    if no environment name is prefixed.
 * <p>
 * 2. In {@link com.snapdeal.cocofs.entity.CocofsProperty} table.
 * <p> 
 * Value present at CONFIG_RELATIVE_PATH will always override value present in cocofs_property table.
 * 
 * @author amd
 */

public enum Property {
    
    STATIC_RESOURCES_PATH                                   ("static.resources.path",null),
    CONFIG_RELATIVE_PATH                                    ("cocofs.config.file.path", "/configuration/cocofsconfig.xml"),
    BARCODE_TYPE                                            ("barcode.type","BDBarcode.ttf"),
    CONTENT_PATH                                            ("content.path", "http://i.sdlcdn.com/"),
    CONFIGURATION_DIR_PATH                                  ("cocofs.configuration.dir.path", "/opt/cocofs"),
    CACHE_RELOAD_OVERRIDE_MAP                               ("cocofs.cache.reload.override.map",null),
    CACHE_RELOAD_PASSWD                                     ("cocofs.cache.reload.passwd", "snapdeal"), //
    FILE_UPLOAD_LOCATION                                    ("cocofs.file.upload.location", "/opt/cocofs"),
    COCOFS_FTP_SERVER_PATH                                  ("cocofs.ftp.server.path", null),
    COCOFS_FTP_USERNAME                                     ("cocofs.ftp.usename", null),
    COCOFS_FTP_PASSWORD                                     ("cocofs.ftp.password", null),
    PRODUCT_ATTRIBUTE_UPLOAD_DTO_FIELD_MAP                  ("productAttributeUploadDTOAttributeMap", null),
    PRODUCT_WEIGHT_UPLOAD_DTO_FIELD_MAP                     ("productWeightUploadDTOAttributeMap", null),
    JOB_WEB_DAEMON_ENABLED                                  ("job.web.daemon.enabled","false"),
    TASK_IMPL_CLASSES                                       ("taskImplClasses",null),
    HOST_NAMES                                              ("hostNames","test"), 
    HYSTRIX_SWITCH                                          ("hystrix.switch","false"),
    USE_HYSTRIX_FOR_GENERIC_COMMAND                         ("use.hystrix.for.generic.command","true"),
    BATCH_SIZE_FOR_API                                      ("batch.size.for.apis",null),
    CATALOG_WEB_SERVICE_URL                                 ("product.catalog.web.service.url",null),
    PENDING_CHECK_ATTRIBUTES_LIST                           ("pending.check.attributes.list", null),
    VALIDATION_ELIGIBILE_ENTITIES_LIST                      ("validation.eligible.entities.list", null),
    OPS_WEB_SERVICE_URL                                     ("ops.web.service.url",null),
    IPMS_WEB_SERVICE_URL                                    ("ipms.web.service.url", null),
    COSTHEAD_WITH_SPLIT_DELIVERY                            ("costhead.with.split.delivery", null),
    THROTTLE_NIGHT_START_TIME                               ("throttleNightStartTime","21:30"),
    THROTTLE_NIGHT_END_TIME                                 ("throttleNightEndTime","09:30"),
    THROTTLED_CALLS_DURING_DAY                              ("throttledCallsDuringDay","30"),
    THROTTLED_CALLS_DURING_NIGHT                            ("throttledCallsDuringNight","300"),
    THROTTLED_CALL_DURATION_WINDOW                          ("throttledCallDurationWindow","3"),
    RESET_THROTTLING_TIME_DELAY_IN_MIN                      ("resetThrottlingTimeDelayInMin","10"),
    SELLER_SUPC_UPLOAD_THROTTLE_PROPERTY                    ("sellerSupcUploadThrottleProperty",null), 
    JOB_PROCESSING_ENABLED                                  ("jobProcessingEnabled","true"),
    SELLER_SUPC_ASSOCIATION_QUEUE                           ("sellerSupcAssociationQueue", null),
    PENDING_PAU_REPLAY_SHARED_KEY                           ("pending.pau.replay.shared.key", "snapdeal1234"),
    ACTIVE_MQ_URL                                           ("activemqURL", null),
    ACTIVE_MQ_USERNAME                                      ("activemqUsername", null),
    ACTIVE_MQ_PASSWORD                                      ("activemqPassword", null), 
    LOAD_ACTIVE_MQ                                          ("loadActiceMQ","true"),
    CACHE_ACCESS_KEY                                        ("cache.access.key", "mycache"),
    CACHE_STATS_ENABLED                                     ("cache.stats.enabled", "true"),
    MEMCACHE_KEY_PREFIX_EMAIL_VERIFICATION_CODE             ("emailVerificationCodeMemCacheKeyPrefix","emailVerificationCodeCocofs/"), 
    PREFIX_EMAIL_VERIFICATION_CODE                          ("emailVerificationCodeMemCacheKeyPrefix","emailVerificationCodeCocofs/"), 
    LOAD_MEMCACHED_SERVICE                                  ("loadMemcachedService","true"), 
    MEMCACHED_SERVER_LIST                                   ("memcached.server.list", null),
    SHIPPING_WEB_SERVICE_URL                                ("shipping.web.service.url", null),
    PPAU_WAITING_PERIOD                                     ("ppau.waiting.period", "3"),
    PANU_NOTIFICATION_EXPIRY                                ("panu.notificaiton.expiry", "3"), 
    GET_START_ALL_ACTION_SLEEP_TIME                         ("startAllTaskSleepTime","200000"), 
    THROTTLING_ALLOWED                                      ("throttlingAllowed", "true"),
    DECIMAL_PLACES_PRECISION                                ("decimalPlacesPrecision","4"),
    TEMP_DIRECTORY                                          ("temp.dir","/tmp/"),
    MAX_SIZE_ALLOWED_LOOKUP                                 ("max.allowed.size.lookup", "1000") ,
    SHIPPING_MODE_DELIVERY_TYPE_MAP                         ("scoreShippingModeToDeliveryTypeMap", null),
    PREFERRED_SHIPPING_MODE                                 ("preferredShippingMode" , "Air" ),
    SYSTEM_WEIGHT_CAPTURE_ENABLED                           ("system.weight.capture.enabled","true"), 
    DANGEROUS_GOODS_TYPE                                    ("dangerousGoodsType",null),
    DANGEROUS_GOODS_TYPE_DEFAULT_VALUE                      ("dangerousGoodsTypeDefaultValue","NONE"), 
    DANGEROUS_GOODS_TYPE_LIQUID_HAZMAT                      ("dangerousGoodsTypeLiquidHazmatValue","FLAMMABLE LIQUIDS"), 
    DANGEROUS_GOODS_TYPE_HAZMAT                             ("dangerousGoodsTypeHazmatValue","MISCELLANEOUS"), 
    DANGEROUS_GOODS_TYPE_LIQUID                             ("dangerousGoodsTypeLiquidValue","FLAMMABLE LIQUIDS"),
    HAZMAT_DANGEROUS_GOODS_TYPES                            ("hazmatDangerousGoodsTypes", null),
    LIQUID_DANGEROUS_GOODS_TYPES                            ("liquidDangerousGoodsTypes", null),
    WOODEN_PACKAGING_WITH_SUPC_LEVEL_EXCEPTION              ("woodenPackagingWithSupcLevelException","true"),
    WOODEN_PACKAGING_AT_ONLY_SUPC_LEVEL                     ("woodenPackagingAtOnlySupcLevel","false"), 
    GET_FULFILMENT_MODEL_CONCURRENTLY                       ("getFulfilmentModelConcurrently","false"),
    GET_GIFTWRAP_CONCURRENTLY                               ("getGiftwrapConcurrently","false"),
    DEFAULT_TAX_RATE_FOR_ALL_STATES                         ("defaultTaxRateForAllStates",null),
    GRAPHITE_URL                                            ("graphite.url","54.255.186.239"),
    GRAPHITE_PORT                                           ("graphite.port", "2003"),
    GRAPHITE_REPORTING_INTERVAL                             ("graphite.reporting.interval", "60"),
    MAX_PRODUCT_PARTS                                       ("max.product.parts","7"),
    FORGOT_PASSWORD_TTL                                     ("forgot.password.ttl","864000"),
    SCORE_WEB_SERVICE_URL									("score.web.service.url",null),
    SDGEO_WEB_SERVICE_URL                                   ("sdgeo.web.service.url","TBD"),

    // main aerospike properties
    AEROSPIKE_URL                                           ("aerospike.url","127.0.0.1:3000"),
    AEROSPIKE_VO_BASE_PACKAGE                               ("aerospike.vo.base.package","com.snapdeal.cocofs.core.aerospike.vo"),
    AEROSPIKE_FAIL_IF_NOT_CONNECTED                         ("aerospike.fail.if.not.connected","true"),
    AEROSPIKE_MAX_SOCKET_IDLE                               ("aerospike.max.socket.idle","20"),
    AEROSPIKE_MAX_THREADS                                   ("aerospike.max.threads","100"),
    AEROSPIKE_TIMEOUT                                       ("aerospike.timeout","30000"),
    
    // large datastructure aerospike properties
    AEROSPIKE_LDT_URL                                       ("aerospike.ldt.url","127.0.0.1:3000"),
    AEROSPIKE_LDT_VO_BASE_PACKAGE                           ("aerospike.ldt.vo.base.package","com.snapdeal.cocofs.core.aerospike.vo"),
    AEROSPIKE_LDT_FAIL_IF_NOT_CONNECTED                     ("aerospike.ldt.fail.if.not.connected","true"),
    AEROSPIKE_LDT_MAX_SOCKET_IDLE                           ("aerospike.ldt.max.socket.idle","20"),
    AEROSPIKE_LDT_MAX_THREADS                               ("aerospike.ldt.max.threads","100"),
    AEROSPIKE_LDT_TIMEOUT                                   ("aerospike.ldt.timeout","30000"),
    
    //Fulfiment model api properties
    DATA_SOURCE_FOR_FM_READ									("data.source.for.fm.read",DataSource.AEROSPIKE.toString()),
    GET_SD_FULFILLED_CONCURRENTLY                           ("get.sd.fulfilled.concurrently","false"),
    UPDATE_FM_FROM_API                                      ("update.fm.from.api","true"),
    VOLUMETRIC_WEIGHT_FORMULA_DENOMINATOR                   ("volumetric.weight.formula.denominator","5"),
    USE_GIFTWRAP_RULES_CACHE                                ("use.giftwrap.rules.cache","true"),
    APPLY_GIFTWARP_LOGIC_FOR_API                            ("apply.giftwarp.logic.for.api","true"),
    APPLY_FM_LOGIC_FOR_WEB_API                              ("apply.fm.logic.for.web.api","true"),
    DEFAULT_FM_FOR_WEB_API                                  ("default.fm.for.web.api","DROPSHIP"),
    PERIMETER_MULTIPLIER_VALUE                              ("perimeter.multiplier.value", "4"),
   
   
    // search queue properties
    ACTIVE_MQ_SEARCH_PUSH_ITEM_COUNT                        ("activemq.search.push.item.count", "100"),
    ACTIVE_MQ_SEARCH_PUSH_QUEUE_NAME                        ("activemq.search.queue.name", null),
    ACTIVE_MQ_SEARCH_PUSH_QUEUE_URL                         ("activemq.search.queue.url", null),
    ACTIVE_MQ_SEARCH_PUSH_USERNAME                          ("activemq.search.queue.username", null),
    ACTIVE_MQ_SEARCH_PUSH_PASSWORD                          ("activemq.search.queue.password", null),
    ACTIVE_MQ_SEARCH_PUSH_ENABLED                           ("activemq.search.push.enabled","false"),
    MANUAL_SELLER_UPDATES_PUSH_LIMIT                        ("manual.seller.updates.push.limit","50"),
    MANUAL_SELLER_SUPC_UPDATES_PUSH_LIMIT                   ("manual.seller.supc.updates.push.limit","10000"),
    
 // pincode queue properties
    ACTIVE_MQ_SHIPFROM_PUSH_ITEM_COUNT                        ("activemq.shipfrom.push.item.count", "100"),
    ACTIVE_MQ_SHIPFROM_PUSH_QUEUE_NAME                        ("activemq.shipfrom.queue.name", "COCOFS.SHIPFROM.PUSH.QUEUE"),
    ACTIVE_MQ_SHIPFROM_PUSH_QUEUE_URL                         ("activemq.shipfrom.queue.url", "failover:(tcp://127.0.0.1:61616)"),
    ACTIVE_MQ_SHIPFROM_PUSH_USERNAME                          ("activemq.shipfrom.queue.username", "system"),
    ACTIVE_MQ_SHIPFROM_PUSH_PASSWORD                          ("activemq.shipfrom.queue.password", "manager"),
    ACTIVE_MQ_SHIPFROM_PUSH_ENABLED                           ("activemq.shipfrom.push.enabled","true"),
    SHIPFROM_UPDATE_PUSH_ENABLED                              ("shipfrom.update.push.enabled", "true"),
    LOAD_ACTIVE_MQ_SHIPFROM_PUSH                              ("load.activemq.shipfrom.push","false"),
    
    // sdinstant queue properties
    ACTIVE_MQ_SDINSTANT_PUSH_ITEM_COUNT                        ("activemq.sdinstant.push.item.count", "100"),
    ACTIVE_MQ_SDINSTANT_PUSH_QUEUE_NAME                        ("activemq.sdinstant.queue.name", "COCOFS.SDINSTANT.PUSH.QUEUE"),
    ACTIVE_MQ_SDINSTANT_PUSH_QUEUE_URL                         ("activemq.sdinstant.queue.url", "failover:(tcp://127.0.0.1:61616)"),
    ACTIVE_MQ_SDINSTANT_PUSH_USERNAME                          ("activemq.sdinstant.queue.username", "system"),
    ACTIVE_MQ_SDINSTANT_PUSH_PASSWORD                          ("activemq.sdinstant.queue.password", "manager"),
    ACTIVE_MQ_SDINSTANT_PUSH_ENABLED                           ("activemq.sdinstant.push.enabled","true"),
    SDINSTANT_UPDATE_PUSH_ENABLED                              ("sdinstant.update.push.enabled", "true"),
    LOAD_ACTIVE_MQ_SDINSTANT_PUSH                              ("load.activemq.sdinstant.push","false"),
    
   
    //seller fm mapping cache properties
    SELLER_FM_MAPPING_CACHE_SIZE                            ("seller.fm.mapping.cache.size","200000"),
    ENABLE_SELLER_FM_MAPPING_CACHE                          ("enable.seller.fm.mapping.cache","true"),
    SELLER_FM_MAPPING_CACHE_TTL                             ("seller.fm.mapping.cache.ttl","3600"),
   
    ENABLE_SELLER_SUPC_FM_MAPPING_CACHE                     ("enable.seller.supc.fm.mapping.cache","false"),
    SELLER_SUPC_FM_MAPPING_CACHE_SIZE                       ("seller.supc.fm.mapping.cache.size","200000"),
    SELLER_SUPC_FM_MAPPING_CACHE_TTL                        ("seller.supc.fm.mapping.cache.ttl","3600"),

    //seller zone mapping cache
    ENABLE_SELLER_ZONE_MAPPING_CACHE                        ("enable.seller.zone.mapping.cache","true"),
    SELLER_ZONE_MAPPING_CACHE_SIZE                          ("seller.zone.mapping.cache.size","200000"),
    SELLER_ZONE_MAPPING_CACHE_TTL                           ("seller.zone.mapping.cache.ttl","3600"),
    DATA_SOURCE_FOR_ZONE_READ                               ("data.source.for.zone.read",DataSource.AEROSPIKE.toString()),
    SHIP_NEAR												("ship.near","false"),
    
    //seller detail cache
    ENABLE_SELLER_DETAIL_CACHE                              ("enable.seller.detail.cache","false"),
    SELLER_DETAIL_CACHE_SIZE                                ("seller.detail.cache.size","200000"),
    SELLER_DETAIL_CACHE_TTL                                 ("seller.detail.cache.ttl","3600"),
    DATA_SOURCE_FOR_SELLER_DETAIL_READ                      ("data.source.for.seller.detail.read",DataSource.AEROSPIKE.toString()),

    // SOI flags
    SOI_FILTER_FEATURE_ENABLED                              ("soi.filter.feature.enabled", "false"),
    LOAD_ACTIVE_MQ_SEARCH_PUSH                              ("load.activemq.search.push","false"),
    SUBCAT_LEVEL_FM_ENABLED                                 ("subcat.level.fm.enabled","true"),
    IPMS_SUBAT_FM_API_ENABLED                               ("ipms.subat.fm.api.enabled","true"),
    FM_API_SELLER_BATCH_SIZE                                ("fm.api.seller.batch.size","1500"),
    GIFTWRAP_API_SELLER_BATCH_SIZE                          ("giftwrap.api.seller.batch.size","15"),
    ZONE_FM_API_BATCH_SIZE                                  ("zone.fm.api.batch.size","30"),
    ZONE_API_BATCH_SIZE                                     ("zone.api.batch.size","30"),


     
    //Event Instance
    MAX_EVENT_RETRY_COUNT                                   ("max.event.retry.count","3"), 
    EVENT_INSTANCE_STATUS                                   ("event.instance.status","PENDING"),
    EVENT_INSTANCE_SEARCH_CRITERIA                          ("event.instance.search.criteria","Param1, Param2"),
   
    UPDATE_PA_FROM_API                                      ("update.pa.from.api","false"),
    
    // flag to validate seller-supc mapping on upload or process, possible values - 
    // DISABLED,ON_UPLOAD,ON_PROCESS,ALWAYS. 
    // - refer SELLER_SUPC_VALIDATION_MAPPING
    VALIDATE_SELLER_SUPC_MAPPING                            ("validate.seller.supc.mapping","ON_UPLOAD"),
    
    EVENT_INSTANCE_DB_RECORD_FETCH_LIMIT                    ("event.instance.db.record.fetch.limit","1000"),
    OVERRIDE_FC_UPDATE_PUSH                                 ("overrider.fc.update.push.to.external.queue", "false"),
    OVERRIDE_FM_UPDATE_PUSH                                 ("overrider.fm.update.push.to.external.queue", "false"),

    SELLER_SUPC_FM_FC_UPDATE_PUSH_ENABLED                   (PropertyNames.SELLER_SUPC_FM_FC_UPDATE_PUSH_ENABLED_KEY, "false"),
    LOAD_ACTIVE_MQ_SELLER_SUPC_FM_FC_PUSH                   ("load.activemq.seller.supc.fm.fc.push","false"),
    // search queue properties
    ACTIVE_MQ_SELLER_SUPC_FM_FC_PUSH_ITEM_COUNT             ("activemq.seller.supc.fm.fc.push.item.count", "100"),
    ACTIVE_MQ_SELLER_SUPC_FM_FC_PUSH_QUEUE_NAME             ("activemq.seller.supc.fm.fc.push.queue.name", null),
    ACTIVE_MQ_SELLER_SUPC_FM_FC_PUSH_QUEUE_URL              ("activemq.seller.supc.fm.fc.push.queue.url", null),
    ACTIVE_MQ_SELLER_SUPC_FM_FC_PUSH_USERNAME               ("activemq.seller.supc.fm.fc.push.queue.username", null),
    ACTIVE_MQ_SELLER_SUPC_FM_FC_PUSH_PASSWORD               ("activemq.seller.supc.fm.fc.push.queue.password", null),
    ACTIVE_MQ_SELLER_SUPC_FM_FC_PUSH_ENABLED                ("activemq.seller.supc.fm.fc.push.enabled","false"),
    VALID_LAND_LINE_NUMBER_LENGTH                           ("valid.land.line.number.length", "11"),
    
    COCOFS_CENTER_TYPES									    ("cocofs.center.types", "FC_VOI,ONESHIP,RMS,OCPLUS"),
    FM_CODE_TO_FC_TYPE_MAP                                  ("fm.code.to.fc.type.map", null),
    //seller pincode zone mapping
    PINCODE_UPLOAD_LIMIT                                    ("pincode.upload.limit","10000"),
    ENABLE_CHECK_HEALTH					                    ("enable.health.check","true"),
    ENABLE_SUPC_TAX_RATE_TEMPLATE_DOWNLOAD                  ("enable.supc.tax.rate.template.download","true"),
    COCOFS_JOB_DEFAULT_PRIORITY                             ("cocofs.job.default.priority", "100"),
    
    ALL_FC_CACHE_ENABLED                                    ("all.fc.cache.enabled","true"),
    
    //########################  PACKMAN Properties  ####################################################
    
    PACKMAN_DEFAULT_RECO_AIR                                ("packman.default.reco.air","AIR_DEF"),
    PACKMAN_DEFAULT_RECO_SURFACE_TYPE                       ("packman.default.reco.surface.type","SURFACE_DEF"),    
    PACKMAN_DEFAULT_RECO_SURFACE_INST                       ("packman.default.reco.surface.inst","SURFACE DEFAULT"),    
    PACKMAN_ROLE_PREFIX					                    ("packman.role.prefix", "packman_"),
    PACKMAN_DEFAULT_ROLE_LIST		                        ("packman.default.role.list", null),
    PACKMAN_DEFAULT_ROLE_URLS_MAP				            ("packman.default.role.url.map", null),
    PACKMAN_DEFAULT_ROLE_MAP                                ("packman.default.role.map", null),
    PACKMAN_DEFAULT_URL_MAIN                                ("packman.default.url.main", null),
    SNAPDEAL_STORE_CODE                                     ("snapdeal.store.code","SD"),
    PACKMAN_API_BATCH_SIZE                                  ("packman.api.batch.size","10"),
    PACKMAN_PACKAGING_RULE_EVALUATION_REQ                   ("packman.packaging.rule.evaluation.required","false"),
    PACKAGING_RULE_EXECUTION_ORDER                          ("packaging.rule.execution.order", null),
    PACKAGING_RECOMMENDATION_API_KEY                        ("packaging.recommendation.api.key","p@(km@n"),
    USE_PRIMARY_LBH					                        ("use.primary.lbh", "false"),
   
    VALIDATION_ENABLE_TAX_RATE_AND_SERVICE_TAX              ("validation.enable.taxrate.and.servicetax","true"),
    CAMS_GET_BRANDS_BATCH_SIZE                              ("cams.get.brands.batch.size","500"),
    ALLOWED_STORE_ROLES_FOR_SUPC_PACKAGING_MAPPING          ("allowed.store.roles.for.supc.packaging.mapping","createRule,admin"),
    
    EXCEL_SHEET_MAX_VALUE                                   ("excel.sheet.max.value","65000"),
    TAX_RATE_MAX_VALUE                                      ("tax.rate.max.value","100"),
    ROUND_OFF_SCALE                                         ("round.off.scale","2"),

    PACKMAN_SLAB_PREFIX                                     ("packman.slab.prefix","SLAB"),
    PACKMAN_DEFAULT_SLAB_SIZE                               ("packman.default.slab.size","500"),
    PACKMAN_MIN_SLAB_SIZE                                   ("packman.min.slab.size","250"),
    PACKMAN_MAX_SLAB_SIZE                                   ("packman.max.slab.size","5000"),
    PACKMAN_MAX_VOL_WEIGHT                                  ("packman.max.vol.weight","25000"),
    PACKMAN_RECO_BASED_ON_LBH                               ("packman.reco.based.on.lbh","false"),
    PACKMAN_RECO_BASED_ON_DELTA_VOL_WT                      ("packman.reco.based.on.delta.vol.wt","false"),
    PACKMAN_RECO_LBH_MARGIN                                 ("packman.reco.lbh.margin","0"),
    PACKMAN_RECO_DELTA_VOL_WT_MULT_FACTOR                   ("packman.reco.delta.vol.wt.mult.factor","2"),
    PACKMAN_RECO_DELTA_VOL_WT_OFFSET                        ("packman.reco.delta.vol.wt.offset","1000"),
    PACKMAN_UNIQUE_PACKAGING_TYPE_ITEM                      ("packman.unique.packaging.type.item","true"),
    PACKMAN_RECO_TEST_PAGE_SHOW                             ("packman.reco.test.page.show","false"),
    PACKMAN_STORE_FRONT_TO_FILTER_VALUE_MAP                 ("packman.store.front.to.filter.value.map", null),
    PACKMAN_SHIP_TOGETHER_BRUTE_FORCE_SUPPORT_COUNT         ("packman.ship.together.brute.force.support.count","5"),
    PACKMAN_BRUTE_FORCE_APPROACH_TIME_OUT                   ("packman.brute.force.approach.time.out","100"),
    
    //########################  TaxClass Properties  ####################################################

    
    ENABLE_TAX_CLASS                                        ("enable.tax.class","true"),
    ENABLE_SUPC_TAX_CLASS_MAPPING_CACHE                     ("enable.supc.tax.class.mapping.cache","true"),
    DATA_SOURCE_FOR_TAX_CLASS_READ                          ("data.source.for.tax.class.read",DataSource.AEROSPIKE.toString()),
    ENABLE_SUBCAT_TAX_CLASS_MAPPING_CACHE                   ("enable.subcat.tax.class.mapping.cache","true"),
    VALIDATE_SUPC_TAX_MAPPING                               ("validate.supc.tax.mapping","ON_UPLOAD"),
    VALIDATE_SUBCAT_TAX_MAPPING                             ("validate.subcat.tax.mapping","ON_UPLOAD"),
    DEFAULT_SUBCAT_TAX_MAPPING                              ("default.subcat.tax.mapping","default"),
    TAX_ENGINE_WEB_SERVICE_URL                              ("tax.engine.web.service.url",null),
    MAX_BATCH_SIZE_FOR_TAX_CLASS                            ("max.batch.size.for.tax.class","10"),
    SUPC_TAX_CLASS_CACHE_TTL                                ("supc.tax.class.cache.ttl","3600"),
    SUPC_TAX_CLASS_CACHE_SIZE                               ("supc.tax.class.cache.size","20000"),
    SUBCAT_TAX_CLASS_CACHE_TTL                              ("subcat.tax.class.cache.ttl","3600"),
    SUBCAT_TAX_CLASS_CACHE_SIZE                             ("subcat.tax.class.cache.size","20000")
    ;
    
    private String name;
    private String value;

    private Property(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }
    
    public String getValue(){
        return value;
    }
    
    public static Property getPropertyByKey(String key) {
        for (Property p : values()) {
            if (p.getName().equals(key)) {
                return p;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return "Property[name:" + name() + ",key:" + getName() + "]";
    }
    
    public static class PropertyNames  {
        public static final String SELLER_SUPC_FM_FC_UPDATE_PUSH_ENABLED_KEY = "seller.supc.fm.fc.update.push.enabled";
    }
}
