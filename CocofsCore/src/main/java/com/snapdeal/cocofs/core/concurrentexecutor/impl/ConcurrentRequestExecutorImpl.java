/**
 * 
 */
package com.snapdeal.cocofs.core.concurrentexecutor.impl;

import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.Semaphore;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.springframework.stereotype.Service;

import com.snapdeal.base.cache.CacheManager;
import com.snapdeal.cocofs.configuration.CocofsPropertiesCache;
import com.snapdeal.cocofs.core.concurrent.dto.ArgumentDTO;
import com.snapdeal.cocofs.core.concurrent.dto.ConcurrentRequest;
import com.snapdeal.cocofs.core.concurrent.dto.DelayedAction;
import com.snapdeal.cocofs.core.concurrent.dto.DelayedMap;
import com.snapdeal.cocofs.core.concurrent.dto.DelayedThreadInterrupter;
import com.snapdeal.cocofs.core.concurrentexecutor.ConcurrentRequestExecutor;

/**
 * This class is an implementation of {@link ConcurrentRequestExecutor} which takes collection of {@link ConcurrentRequest}
 * and executes those parallely. 
 */
@Service("concurrentRequestExecutor")
public class ConcurrentRequestExecutorImpl implements ConcurrentRequestExecutor {

    private static final Logger            LOGGER               = org.slf4j.LoggerFactory.getLogger(ConcurrentRequestExecutorImpl.class);
    /**
     * Minimum request queue size. 
     */
    private static final int 				 MIN_POOL_SIZE        = 500;
    /**
     * Maximum request queue size.
     */
    private static final int              MAX_POOL_SIZE        = 10000;
    /**
     * Maximum number of times task would be retried to be queued in the POOL.
     */
    private static final int				 MAX_TASK_RETRIES	  = 3;
    /**
     * Minimum threads that should be alive all the time in the thread pool.
     */
    private static int                     MIN_THREAD_SIZE      = 10;
    /**
     * Maximum threads that can be alive at a time in the thread pool.
     */
    private static int                     MAX_THREAD_SIZE      = 101;
    /**
     * Defines the max time for which thread will wait before gets timeout on semaphore permit acquire.
     */
    private static final long			 MAX_SEMAPHORE_ACQUIRE_TIME = 1000l;
    /**
     * Holds the maximum time task assigner thread will wait after assigning the task after that the thread will be
     * interrupted to proceed further if result is yet awaited.<br>
     * This value will make sure that user thread will not wait more than {@link #MAX_THREAD_WAIT_TIME}.
     */
    private static AtomicLong              MAX_THREAD_WAIT_TIME = new AtomicLong(6000000l);
    /**
     * Maximum request Queue size.<br>
     * Please note that keeping this size very high could lead to delay as the queue processing is sequential. Keeping
     * this very low would lead to inefficient use of this module.
     */
    private static AtomicInteger           QUEUE_SIZE           = new AtomicInteger(1000);
    /**
     * Request queue that will hold {@link ConcurrentRequest} for processing. This will work as a 
     * temporary queue which will take the requests from clients and then assign to executor in 
     * executor's queue.
     */
    private static BlockingQueue<Runnable> REQUEST_QUEUE        = new LinkedBlockingQueue<Runnable>();
    /**
     * Thread pool executor queue. It'll hold the requests which will be currently executed by 
     * the pool threads. The type of the pool {@link SynchronousQueue} causes the pool to spawn 
     * no of threads equal to the tasks count. Since the pool thread size is limited to {@value #MAX_THREAD_SIZE}
     * this queue will hold at most {@link #MAX_THREAD_SIZE}.<p>
     * If the pool is full of requests and client tries to add new requests it'll be dropped. Either client
     * should retry assigning the task or define a {@link RejectedExecutionHandler}. 
     */
    private static BlockingQueue<Runnable> REQUEST_EXECUTOR_QUEUE = new SynchronousQueue<Runnable>(true);
    /**
     * This is used to keep a check on number of tasks added to pool's request queue. Since we're
     * using {@link SynchronousQueue} we have to make sure that pool's request queue should be kept 
     * below {@link #MAX_THREAD_SIZE} so that task shouldn't get rejected. <P>
     * The purpose of this is to restrict the request size within the thread pool acceptable limit
     * in order to avoid task rejection.
     */
    private static final Semaphore 		 semaphore = new Semaphore(MAX_THREAD_SIZE - 5);
    /**
     * Thread pool to process the tasks parallely.
     */
    private static ThreadPoolExecutor      POOL                 = new ThreadPoolExecutor(MIN_THREAD_SIZE, MAX_THREAD_SIZE, 10 * 60 * 1000, TimeUnit.MILLISECONDS, REQUEST_EXECUTOR_QUEUE,
                                                                        new ParallelExecutorFactory(), new RejectedTaskExecutor());
    /**
     * Keeps track of {@link DelayedAction} tasks. In this implementation we are interrupting the threads that 
     * are working even after the {@link #MAX_THREAD_WAIT_TIME} is expired.<p>This implementation is 
     * given by {@link DelayedThreadInterrupter}.
     */
    private DelayedMap<ConcurrentRequest, DelayedAction> delayedTask = new DelayedMap<ConcurrentRequest, DelayedAction>(2000);
    
    @Override
    public <T> Map<ConcurrentRequest, T> executeConcurrently(List<ConcurrentRequest> requests, long timeout) {
    	return execute(requests, new AtomicLong(timeout));
    }
    
    @Override
    public <T> Map<ConcurrentRequest, T> executeConcurrently(List<ConcurrentRequest> requests) {
    	return execute(requests, MAX_THREAD_WAIT_TIME);
    }
    
    /**
     * Executes given tasks with given timeout. If timeout elapses and all threads aren't completed yet, 
     * it would interrupt threads and return immediately. 
     * @param <T>
     * @param requests
     * @param threadWaitTime
     * @return
     */
    public <T> Map<ConcurrentRequest, T> execute(List<ConcurrentRequest> requests, AtomicLong threadWaitTime) {
        ConcurrentHashMap<ConcurrentRequest, T> result = new ConcurrentHashMap<ConcurrentRequest, T>();
        long currentQueueSize = POOL.getQueue().size() + REQUEST_QUEUE.size();
        if ((currentQueueSize + requests.size()) < QUEUE_SIZE.get()) {
        	long startTime = System.currentTimeMillis();
        	CountDownLatch countGaurd = new CountDownLatch(requests.size());
            for (ConcurrentRequest concurrentRequest : requests) {
                REQUEST_QUEUE.add(new Request<T>(concurrentRequest, countGaurd, result, threadWaitTime.get()));
            }
            try {
            	if(!countGaurd.await(threadWaitTime.get(), TimeUnit.MILLISECONDS)) {
                	handleDelayedTasks(requests);
                }
            } catch (InterruptedException ex) {
                LOGGER.error("Thread interrupted while waiting for other threads to complete");
            }
            long endTime = System.currentTimeMillis();
            LOGGER.info("Total parallel execution time for {} tasks is {}ms", requests.size(), (endTime - startTime));
        } else {
            LOGGER.info("Parallel executor's Queue is full. Size currently {}. Executing tasks synchronously...", currentQueueSize);
            for (ConcurrentRequest concurrentRequest : requests) {
                T response = (T) invokeMethod(concurrentRequest);
                if(response == null) {
                	response = (T)new Object();
                }
                result.put(concurrentRequest, response);
            }
        }
        return result;
    }
    
    /**
     * Defines the task that will be executed by the Thread pool defined in this class.
     *
     * @param <T>
     */
    private class Request<T> implements Runnable {
        private final ConcurrentHashMap<ConcurrentRequest, T>    result;
        private final ConcurrentRequest                          request;
        private final CountDownLatch                             countGuard;
        /**
         * Defines the maximum time for which this thread can run. After this delay gets expired this thread will 
         * be interrupted. <p>The implementation for this is given in {@link DelayedThreadInterrupter}.
         */
        private final long									   maxThreadWaitTime;
        
        public Request(ConcurrentRequest request, CountDownLatch countGuard, ConcurrentHashMap<ConcurrentRequest, T> result, long maxThreadWaitTime) {
            this.request = request;
            this.countGuard = countGuard;
            this.result = result;
            this.maxThreadWaitTime = (long)(maxThreadWaitTime * 0.8);
        }

        @Override
        public void run() {
        	try {
	        	delayedTask.addTask(request, new DelayedThreadInterrupter(Thread.currentThread()), maxThreadWaitTime, TimeUnit.MILLISECONDS);
	            T response = invokeMethod(request);
	            if(response == null) {
	            	response = (T)new Object();
	            }
	            result.put(request, response);
        	} catch(Exception e) {
        		LOGGER.error("Error while executing concurrent task.", e);
        	} finally {
        		delayedTask.removeTask(request);
                countGuard.countDown();
                semaphore.release();
        	}
        }
    }

    /**
     * Invokes the method with given parameter in {@link ConcurrentRequest} object. <p>If method
     * execution is successful {@link ConcurrentRequest#isSuccessful()} returns a <code>true</code>
     * value other wise the value is set as <code>false</code> and the exception which occurred
     * during execution is set into the request.
     * 
     * @param <T>
     * @param request
     * @return result of the method invocation.
     */
    @SuppressWarnings("unchecked")
    public <T> T invokeMethod(ConcurrentRequest request) {
        if (request.getObject() != null) {
            T result = null;
            try {
                Method method = getAppropriateMethod(request.getMethodName(), request.getObject(), request.getArguments());
                method.setAccessible(true);
                
                int numberOfArguments =  (request.getArguments() == null ) ? 0 : request.getArguments().length;
                Object[] argumentValues = new Object[numberOfArguments];
                for(int i = 0; i < numberOfArguments; i++ ){
                    argumentValues[i] = request.getArguments()[i].getArgumentValue();
                }
                
                result = (T) method.invoke(request.getObject(), argumentValues);
                request.setSuccessful(true);
            } catch (Exception ex) {
                request.setSuccessful(false);
                request.setException(ex);
            	LOGGER.error(ex.getMessage(), ex);
            }
            return result;
        } else {
            return null;
        }
    }

    /**
     * This method is responsible for identifying the exact method that has to be invoked to perform the operation.
     * <p>
     * Methods can be overloaded and thus the name can be same but the methods would be different from each other.
     * <p>
     * The method arguments of the invoked method will identify the exact method and return the method object for this
     * class.
     * 
     * @param name name of the method that needs to be invoked.
     * @param object object on which the method will be invoked.
     * @param param arguments of the method to be invoked.
     * @return {@link Method} object representing given method name and parameter.
     * @throws NoSuchMethodException if method with given name and parameter doesn't exists.
     */
    private Method getAppropriateMethod(String name, Object object, ArgumentDTO[] arguments) throws NoSuchMethodException {
        
        Method mte = null;
        Class[] parameterTypes = new Class[arguments.length];
        for(int i = 0; i < arguments.length; i++ ){
            parameterTypes[i] = arguments[i].getArgumentType();
        }
        mte = object.getClass().getMethod(name, parameterTypes);
      
        if (mte != null) {
            return mte;
        }
        throw new NoSuchMethodException(name + " and given arguments in : " + object.getClass());
    }

    /**
     * Performs delayed action for tasks that are delayed more than the specified {@link #MAX_THREAD_WAIT_TIME}. {@link DelayedThreadInterrupter} 
     * interrupts threads that are still working event after the delay has been expired.
     * @param requests
     */
    private void handleDelayedTasks(List<ConcurrentRequest> requests) {
    	LOGGER.info("Starting delayed tasks handler for delayed tasks..");
    	for (ConcurrentRequest concurrentRequest : requests) {
			DelayedAction delayedTask = this.delayedTask.removeTask(concurrentRequest);
			if(delayedTask != null) {
				delayedTask.performDelayedAction();
			}
		}
    }
    
    /**
     * Creates a {@link ThreadFactory} which will create threads with specified name.
     * 
     * @author fanendra
     */
    private static class ParallelExecutorFactory implements ThreadFactory {
        private final AtomicInteger threadCount = new AtomicInteger(1);

        @Override
        public Thread newThread(Runnable r) {
            return new Thread(r, "ParallelExecutorFactory_" + threadCount.getAndIncrement());
        }
    }

    /**
     * Checks the system properties file regularly and sets the new queue size if changed in file. It also the logs the
     * current queue size that is being used.
     */
    private static final ScheduledExecutorService QUEUE_VIGIL_SERVICE = new ScheduledThreadPoolExecutor(1);
    /**
     * This scheduled task will keep track of no of threads available in the pool. If reqest queue size changes 
     * no of threads in the pool will change between {@link #MIN_THREAD_SIZE} and {@link #MAX_THREAD_SIZE} depending 
     * on the queue size. For further information please check {@link #adjustCorePoolSize()}.
     */
    private static final ScheduledExecutorService POOL_VIGIL_SERVICE = new ScheduledThreadPoolExecutor(1);
    /**
     * Assigns task to thread pool, waiting if necessary for a maximum of {@link #MAX_SEMAPHORE_ACQUIRE_TIME} to acquire semaphore 
     * permit. If permit is not acquired within the time this task will be retried for a maximum of .
     * 
     * @param task
     */
    private static void addTasksToPool(Runnable task) {
    	boolean assigned = false;
    	for(int i = 0; !assigned && i++ < MAX_TASK_RETRIES;) {
	    	try {
		    	if(semaphore.tryAcquire(MAX_SEMAPHORE_ACQUIRE_TIME, TimeUnit.MILLISECONDS)) {
					try {
						POOL.submit(task);
						assigned = true;
					} catch (Exception e) {
						semaphore.release();
						throw e;
					}
				}
	    	} catch (Exception e) {
	    		LOGGER.error("Failed to add task to ConcurrentRequestExecutor for execution...");
	    	}
    	}
    }
    
    static {
    	POOL.submit(new Runnable() {
			@Override
			public void run() {
				while(true) {
					try {
						Runnable task = REQUEST_QUEUE.take();
						addTasksToPool(task);
					} catch (Exception e) {
					}
				}
			}
		});
    	
        QUEUE_VIGIL_SERVICE.scheduleWithFixedDelay(new Runnable() {
            @Override
            public void run() {
                CocofsPropertiesCache cache = CacheManager.getInstance().getCache(CocofsPropertiesCache.class);
                if (cache != null) {
                    try {
                        if(Boolean.valueOf(cache.getProperty("executor.stats.enabled", "true"))) {
                        	logThreadPoolStats();
                        }
                    	int externalQueueSize = Integer.parseInt(cache.getProperty("queue.size", "-1"));
                        if (externalQueueSize != -1 && externalQueueSize != QUEUE_SIZE.get()) {
                            if(externalQueueSize >= MIN_POOL_SIZE && externalQueueSize <= MAX_POOL_SIZE) {
                            	LOGGER.info("Changing parallel executor's queue size from: {} to : {}", QUEUE_SIZE.get(), externalQueueSize);
                            	QUEUE_SIZE.compareAndSet(QUEUE_SIZE.get(), externalQueueSize);
                            } else {
                            	LOGGER.info("Invalid Queue size specified. Default size would be kept. " +
                            			"Please specify between {} and {}", MIN_POOL_SIZE, MAX_POOL_SIZE);
                            }
                        }
                        long externalMaxWaitTime = Long.parseLong(cache.getProperty("max.wait.time", "-1"));
                        if (externalMaxWaitTime != -1 && externalMaxWaitTime != MAX_THREAD_WAIT_TIME.get()) {
                            LOGGER.info("Changing user thread's max wait time from : {} to : {}", MAX_THREAD_WAIT_TIME.get(), externalMaxWaitTime);
                            MAX_THREAD_WAIT_TIME.compareAndSet(MAX_THREAD_WAIT_TIME.get(), externalMaxWaitTime);
                        }
                    } catch (Exception e) {
                    }
                }
            }
        }, 0l, 30 * 1000, TimeUnit.MILLISECONDS);
        
        POOL_VIGIL_SERVICE.scheduleWithFixedDelay(new Runnable() {
			@Override
			public void run() {
				adjustCorePoolSize();
			}
		}, 0, 5, TimeUnit.SECONDS);
    }
    
    /**
     * This method would dynamically adjust the core pool size for the thread pool.
     */
    private static void adjustCorePoolSize() {
    	int coreThread = (int)(MAX_THREAD_SIZE * ((double)(POOL.getQueue().size() + REQUEST_QUEUE.size())/QUEUE_SIZE.get()));
//    	POOL.setCorePoolSize(coreThread);
    }
    /**
     * Logs the thread pool statistics like the max number of threads, total tasks executed etc.
     */
    private static void logThreadPoolStats() {
        LOGGER.info("Current parallel executor's queue size is: {}", (POOL.getQueue().size() + REQUEST_QUEUE.size()));
        LOGGER.info("Current POOL active tasks count: {}", POOL.getQueue().size());
        LOGGER.info("Total currently active threads {}", POOL.getActiveCount());
        LOGGER.info("Tasks completed count till now {}", POOL.getCompletedTaskCount());
        LOGGER.info("Maximum pool size that has grown ever {}", POOL.getLargestPoolSize());
        LOGGER.info("Current count of threads in the POOL {}", POOL.getPoolSize());
    }
    
    /**
     * Adding a shut down hook so that Thread pool should be destroyed after recieving a CTRL+C or kill command.
     */
    public void attachShutDownHook(){
    	Runtime.getRuntime().addShutdownHook(new Thread() {
    		@Override
    		public void run() {
    			POOL.shutdownNow();
    			QUEUE_VIGIL_SERVICE.shutdownNow();
    			POOL_VIGIL_SERVICE.shutdownNow();
    		}
    	});
    	LOGGER.info("Shut down hook for ConcurrentRequestExecutor attached...");
    }
    
    public ConcurrentRequestExecutorImpl() {
    	attachShutDownHook();
    }
    
    /**
     * Handler for rejected task. When a task is added to the pool and all threads are busy the task is dropped 
     * from the pool stating exception {@link RejectedExecutionException}. This handler is specified in pool
     * definition and would be used when the task is dropped.<p>
     * The dropped task is sent to {@link #rejectedExecution(Runnable, ThreadPoolExecutor)} with reference to the 
     * dropped task and the pool from which the task has been rejected.
     * 
     * @author fanendra
     */
    private static class RejectedTaskExecutor implements RejectedExecutionHandler {
    	@Override
    	public void rejectedExecution(Runnable task, ThreadPoolExecutor executor) {
    		addTasksToPool(task);
    	}
    }
}
