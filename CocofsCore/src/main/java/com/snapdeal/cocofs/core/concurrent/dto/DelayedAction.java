/**
 * 
 */
package com.snapdeal.cocofs.core.concurrent.dto;

/**
 * Defines the action that has to be executed after required delay.
 * @author fanendra
 *
 */
public interface DelayedAction {
	/**
	 * This method is called after specified delay is expired for the given entry.
	 */
	public void performDelayedAction();
}
