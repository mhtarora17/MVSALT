/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.core.aerospike.vo;

import java.util.ArrayList;
import java.util.List;

import com.snapdeal.score.data.aerospike.AerospikeSet;
import com.snapdeal.score.data.aerospike.annotations.Bin;
import com.snapdeal.score.data.aerospike.annotations.Key;
import com.snapdeal.score.data.aerospike.annotations.Set;

/**
 * @version 1.0, 19-Mar-2015
 * @author ankur
 */

@Set(namespace = "cocofs", name = "seller_supc_fc_mapping_vo")
public class SellerSupcFCMappingVO implements AerospikeSet {

    /**
     * 
     */
    private static final long serialVersionUID = -2817662587335492704L;

    @Key
    @Bin(name = "ssc")
    private String            supcSellerCode;

    @Bin(name = "su")
    private String            supc;

    @Bin(name = "sc")
    private String            sellerCode;

    @Bin(name = "fm")
    private String            fulfillmentModel;

    @Bin(name = "fc")
    private List<String>      fcCodes          = new ArrayList<String>();

    @Bin(name = "e")
    private int               enabled;

    public String getSupcSellerCode() {
        return supcSellerCode;
    }

    public void setSupcSellerCode(String supcSellerCode) {
        this.supcSellerCode = supcSellerCode;
    }

    public String getSellerCode() {
        return sellerCode;
    }

    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }

    public String getFulfillmentModel() {
        return fulfillmentModel;
    }

    public void setFulfillmentModel(String fulfillmentModel) {
        this.fulfillmentModel = fulfillmentModel;
    }

    public List<String> getFcCodes() {
        return fcCodes;
    }

    public void setFcCodes(List<String> fcCode) {
        this.fcCodes = fcCode;
    }

    public int getEnabled() {
        return enabled;
    }

    public void setEnabled(int enabled) {
        this.enabled = enabled;
    }

    public String getSupc() {
        return supc;
    }

    public void setSupc(String supc) {
        this.supc = supc;
    }

    @Override
    public String toString() {
        return "SellerFCMappingVO [sellerCode=" + sellerCode + "supc=" + supc + ", fulfillmentModel=" + fulfillmentModel + ", fcCodes=" + fcCodes + ", enabled=" + enabled + "]";
    }

}
