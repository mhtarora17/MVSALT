/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.core.metrics;

import com.codahale.metrics.MetricRegistry;

/**
 *  
 *  @version     1.0, 22-Aug-2014
 *  @author ankur
 */
public class Metrics {


    public static final String          REGISTRY_ATTRIBUTE = "com.codahale.metrics.servlet.InstrumentedFilter.registry";
    private final static MetricRegistry registry           = new MetricRegistry();

    public static MetricRegistry getRegistry() {
        return registry;
    }

    private Metrics() {

    }

}
