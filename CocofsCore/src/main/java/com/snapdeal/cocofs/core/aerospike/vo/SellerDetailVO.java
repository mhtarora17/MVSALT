/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.core.aerospike.vo;

import com.snapdeal.score.data.aerospike.AerospikeSet;
import com.snapdeal.score.data.aerospike.annotations.Bin;
import com.snapdeal.score.data.aerospike.annotations.Key;
import com.snapdeal.score.data.aerospike.annotations.Set;

/**
 * @version 1.0, 15-Jun-2015
 * @author Gaurav
 */

@Set(namespace = "cocofs", name = "seller_detail_vo")
public class SellerDetailVO implements AerospikeSet {

    /**
     * 
     */
    private static final long serialVersionUID = -4129367989434628440L;
    @Key
    @Bin(name = "sc")
    private String            sellerCode;

    @Bin(name = "pc")
    private String pincode;

    @Bin(name = "al1")
    private String addressLine1;

    @Bin(name = "al2")
    private String addressLine2;

    @Bin(name = "ci")
    private String city;

    @Bin(name = "s")
    private String state;
    
    @Bin(name = "co")
    private String country;

    public String getSellerCode() {
        return sellerCode;
    }

    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
    
    
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "SellerDetailVO [sellerCode=" + sellerCode + ", pincode=" + pincode + ", addressLine1=" + addressLine1 + ", addressLine2=" + addressLine2 + ", city=" + city
                + ", state=" + state + "]";
    }

}
