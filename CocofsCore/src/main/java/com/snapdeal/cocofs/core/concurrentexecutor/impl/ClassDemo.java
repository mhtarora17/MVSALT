/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.core.concurrentexecutor.impl;

import java.lang.reflect.Method;

/**
 *  
 *  @version     1.0, 23-Sep-2014
 *  @author ankur
 */
public class ClassDemo {

    public static void main(String[] args) {
     
      ClassDemo cls = new ClassDemo();
      Class c = cls.getClass();

      try {                
         // parameter type is null
         Method m = c.getMethod("show", null);
         System.out.println("method = " + m.toString());        
      }
     
      catch(NoSuchMethodException e) {
         System.out.println(e.toString());
      }
         
      try {
         // method Long
         Class[] cArg = new Class[1];
         cArg[0] = int.class;
         Method lMethod = c.getMethod("showLong", cArg);
         System.out.println("method = " + lMethod.toString());
      }
      catch(NoSuchMethodException e) {
         System.out.println(e.toString());
      }
    }

    public Integer show() {
       return 1;
    }
     
    public void showLong(int l) {
       this.l = l;
    }
    int l = 78655;
 } 