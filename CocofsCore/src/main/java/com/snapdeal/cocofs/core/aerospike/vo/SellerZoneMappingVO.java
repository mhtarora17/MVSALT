/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.core.aerospike.vo;

import com.snapdeal.score.data.aerospike.AerospikeSet;
import com.snapdeal.score.data.aerospike.annotations.Bin;
import com.snapdeal.score.data.aerospike.annotations.Key;
import com.snapdeal.score.data.aerospike.annotations.Set;

/**
 *  
 *  @version     1.0, 15-Jun-2015
 *  @author Gaurav
 */

@Set(namespace = "cocofs", name = "seller_zone_mapping_vo")
public class SellerZoneMappingVO implements AerospikeSet{


    /**
	 * 
	 */
	private static final long serialVersionUID = -4129367989434628440L;
	@Key
    @Bin(name = "sc")
    private String            sellerCode;

    @Bin(name = "z")
    private String   zone;

	public String getSellerCode() {
        return sellerCode;
    }

    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }

    public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

	@Override
	public String toString() {
		return "SellerZoneMappingVO [sellerCode=" + sellerCode + ", zone="
				+ zone + "]";
	}
   
}
