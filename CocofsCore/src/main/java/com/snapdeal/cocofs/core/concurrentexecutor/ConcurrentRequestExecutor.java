package com.snapdeal.cocofs.core.concurrentexecutor;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

import com.snapdeal.cocofs.core.concurrent.dto.ConcurrentRequest;

public interface ConcurrentRequestExecutor {
       
    /**
       * This method takes a list of {@link ConcurrentRequest} and process those asynchronously. The result of processing
         is returned once all the requests has been processed or the thread gets <code>TIMED OUT</code><br>
       * <p>The bunch execution is taken care by using {@link CountDownLatch} and then waiting for the specified time.<br>
       * <p>The request queue for the thread pool is bounded and if the request queue size reaches the max boundary
       * the execution follows a synchronous pattern which is a failover mechanism designed in order to make sure that all
       * the request should be executed and should not be left due to any reason.
       * @param <T>
       * @param requests
       * @return
       */
     public <T> Map<ConcurrentRequest, T> executeConcurrently(List<ConcurrentRequest> requests);
    
      /**
       * Overloaded version for providing external timeout for tasks.
       * @param <T>
       * @param requests
       * @param timeout
       * @return
       */
      public <T> Map<ConcurrentRequest, T> executeConcurrently(List<ConcurrentRequest> requests, long timeout);
}
