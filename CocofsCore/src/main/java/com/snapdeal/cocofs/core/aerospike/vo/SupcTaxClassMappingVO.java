/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.core.aerospike.vo;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.snapdeal.score.data.aerospike.AerospikeSet;
import com.snapdeal.score.data.aerospike.annotations.Bin;
import com.snapdeal.score.data.aerospike.annotations.Key;
import com.snapdeal.score.data.aerospike.annotations.Set;

@Set(namespace = "cocofs", name = "supc_tax_class_mapping_vo")
public class SupcTaxClassMappingVO implements AerospikeSet {

    /**
     * 
     */
    private static final long   serialVersionUID = 9060120656508141583L;

    private static final Logger LOG              = LoggerFactory.getLogger(SupcTaxClassMappingVO.class);

    @Key
    @Bin(name = "supc")
    private String              supc;

    @Bin(name = "tc")
    private String              taxClass;

    @Bin(name = "e")
    private int                 enabled;

    @Bin(name = "c")
    private String              createdStr;

    @Bin(name = "u")
    private String              updatedStr;

    public String getSupc() {
        return supc;
    }

    public void setSupc(String supc) {
        this.supc = supc;
    }

    public String getTaxClass() {
        return taxClass;
    }

    public void setTaxClass(String taxClass) {
        this.taxClass = taxClass;
    }

    public int getEnabled() {
        return enabled;
    }

    public void setEnabled(int enabled) {
        this.enabled = enabled;
    }

    public Date getCreated() {
        Date created = null;
        try {
            created = new Date(Long.parseLong(createdStr));
        } catch (Exception e) {
            LOG.error("Cannot create Date object from createdStr = " + createdStr);
        }
        return created;
    }

    public Date getUpdated() {
        Date updated = null;
        try {
            updated = new Date(Long.parseLong(updatedStr));
        } catch (Exception e) {
            LOG.error("Cannot create Date object from updatedStr = " + updatedStr);
        }
        return updated;
    }

    public void setCreatedStr(String createdStr) {
        this.createdStr = createdStr;
    }

    public void setUpdatedStr(String updatedStr) {
        this.updatedStr = updatedStr;
    }

    public String getCreatedStr() {
        return createdStr;
    }

    public String getUpdatedStr() {
        return updatedStr;
    }

}
