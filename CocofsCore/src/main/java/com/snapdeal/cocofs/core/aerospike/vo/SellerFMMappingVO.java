/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.core.aerospike.vo;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.snapdeal.score.data.aerospike.AerospikeSet;
import com.snapdeal.score.data.aerospike.annotations.Bin;
import com.snapdeal.score.data.aerospike.annotations.Key;
import com.snapdeal.score.data.aerospike.annotations.Set;

/**
 * @version 1.0, 11-Sep-2014
 * @author ankur
 */
@Set(namespace = "cocofs", name = "seller_fm_mapping_vo")
public class SellerFMMappingVO implements AerospikeSet {

    /**
     * 
     */
    private static final long   serialVersionUID = 2905978857244433876L;

    private static final Logger LOG              = LoggerFactory.getLogger(SellerFMMappingVO.class);

    @Key
    @Bin(name = "sc")
    private String              sellerCode;

    @Bin(name = "fm")
    private String              fulfillmentModel;

    @Bin(name = "e")
    private int                 enabled;

    @Bin(name = "c")
    private String              createdStr;

    @Bin(name = "u")
    private String              updatedStr;

    @Bin(name = "se")
    private int                 supcExist;

    @Bin(name = "sce")
    private int                 subcatExist;

    @Bin(name = "fc")
    private List<String>        fcCenters;

    
    public String getSellerCode() {
        return sellerCode;
    }

    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }

    public String getFulfillmentModel() {
        return fulfillmentModel;
    }

    public void setFulfillmentModel(String fulfillmentModel) {
        this.fulfillmentModel = fulfillmentModel;
    }


    public boolean isEnabled() {
        return enabled == 1 ? true : false;
    }

    public void setEnabled(int enabled) {
        this.enabled = enabled;
    }
    
    

    public List<String> getFcCenters() {
        return fcCenters;
    }

    public void setFcCenters(List<String> fcCenters) {
        this.fcCenters = fcCenters;
    }

    public Date getCreated() {
        Date created = null;
        try {
            created = new Date(Long.parseLong(createdStr));
        } catch (Exception e) {
            LOG.error("Cannot create Date object from createdStr = " + createdStr);
        }
        return created;
    }

    public Date getUpdated() {
        Date updated = null;
        try {
            updated = new Date(Long.parseLong(updatedStr));
        } catch (Exception e) {
            LOG.error("Cannot create Date object from updatedStr = " + updatedStr);
        }
        return updated;
    }

    public boolean isSupcExist() {
        return supcExist == 1 ? true : false;
    }

    public void setSupcExist(int supcExist) {
        this.supcExist = supcExist;
    }

    public boolean isSubcatExist() {
        return subcatExist == 1 ? true : false;
    }

    public void setSubcatExist(int subcatExist) {
        this.subcatExist = subcatExist;
    }

    @Deprecated
    public int getEnabled() {
        return enabled;
    }

    @Deprecated
    public int getSupcExist() {
        return supcExist;
    }

    @Deprecated
    public int getSubcatExist() {
        return subcatExist;
    }

    @Deprecated
    public String getCreatedStr() {
        return createdStr;
    }

    public void setCreatedStr(String createdStr) {
        this.createdStr = createdStr;
    }

    @Deprecated
    public String getUpdatedStr() {
        return updatedStr;
    }

    public void setUpdatedStr(String updatedStr) {
        this.updatedStr = updatedStr;
    }

    @Override
    public String toString() {
        return "SellerFMMappingVO [sellerCode=" + sellerCode + ", fulfillmentModel=" + fulfillmentModel + ", fcCenters=" + fcCenters + ", enabled=" + enabled + ", createdStr="
                + createdStr + ", updatedStr=" + updatedStr + ", supcExist=" + supcExist + ", subcatExist=" + subcatExist + "]";
    }

}
