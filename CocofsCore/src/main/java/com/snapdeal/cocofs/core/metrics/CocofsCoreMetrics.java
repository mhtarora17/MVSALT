/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.core.metrics;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Service;

import com.codahale.metrics.jvm.FileDescriptorRatioGauge;
import com.codahale.metrics.jvm.GarbageCollectorMetricSet;
import com.codahale.metrics.jvm.MemoryUsageGaugeSet;
import com.codahale.metrics.jvm.ThreadStatesGaugeSet;



/**
 *  
 *  @version     1.0, 22-Aug-2014
 *  @author ankur
 */

@Service("cocofsCoreMetrics")
public class CocofsCoreMetrics {


    private CocofsCoreMetrics(){
        
    }
    
    @PostConstruct
    private void initMetrics() {
        
        Metrics.getRegistry().register("jvm.gc", new GarbageCollectorMetricSet());
        Metrics.getRegistry().register("jvm.memory", new MemoryUsageGaugeSet());
        Metrics.getRegistry().register("jvm.thread-states", new ThreadStatesGaugeSet());
        Metrics.getRegistry().register("jvm.fd.usage", new FileDescriptorRatioGauge());
        
    }
    

}
