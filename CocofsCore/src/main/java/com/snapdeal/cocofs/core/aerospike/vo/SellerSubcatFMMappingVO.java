/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.core.aerospike.vo;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.snapdeal.score.data.aerospike.AerospikeSet;
import com.snapdeal.score.data.aerospike.annotations.Bin;
import com.snapdeal.score.data.aerospike.annotations.Key;
import com.snapdeal.score.data.aerospike.annotations.Set;

/**
 *  
 *  @version     1.0, 12-Sep-2014
 *  @author ankur
 */
@Set(namespace = "cocofs", name = "seller_subcat_fm_mapping_vo")
public class SellerSubcatFMMappingVO implements AerospikeSet{

    /**
     * 
     */
    private static final long serialVersionUID = 3697953779925597169L;

    private static final Logger  LOG = LoggerFactory.getLogger(SellerSubcatFMMappingVO.class);
    
    @Key
    @Bin(name = "ssc")
    private String            	subcatSellerCode;

	@Bin(name = "sc")
    private String            	sellerCode;
    
    @Bin(name = "sub")
    private String            	subcat;
       
    @Bin(name = "fm")
    private String   			fulfillmentModel;
   
    @Bin(name = "e")
    private int  			enabled;

	@Bin(name = "c")
    private String     			createdStr;

    @Bin(name = "u")
    private String     			updatedStr;

    public String getSubcatSellerCode() {
        return subcatSellerCode;
    }

    public void setSubcatSellerCode(String subcatSellerCode) {
        this.subcatSellerCode = subcatSellerCode;
    }

    public String getFulfillmentModel() {
        return fulfillmentModel;
    }

    public void setFulfillmentModel(String fulfillmentModel) {
        this.fulfillmentModel = fulfillmentModel;
    }

    public boolean isEnabled() {
        return enabled == 1 ? true : false;
    }

    public void setEnabled(int enabled) {
        this.enabled = enabled;
    }
    
    public String getSellerCode() {
		return sellerCode;
	}

	public void setSellerCode(String sellerCode) {
		this.sellerCode = sellerCode;
	}

	public String getSubcat() {
		return subcat;
	}

	public void setSubcat(String subcat) {
		this.subcat = subcat;
	}
 
    public Date getCreated() {
    	Date created = null;
    	try{
    		created = new Date(Long.parseLong(createdStr));
    	}
    	catch (Exception e) {
    		LOG.error("Cannot create Date object from createdStr = "+createdStr);
		}
    	return created;
    }

    public Date getUpdated() {
    	Date updated = null;
    	try{
    		updated = new Date(Long.parseLong(updatedStr));
    	}
    	catch(Exception e){
    		LOG.error("Cannot create Date object from updatedStr = "+updatedStr);
    	}
    	return updated;
    }

    @Deprecated
	public int getEnabled() {
		return enabled;
	}
	 
    @Deprecated
    public String getCreatedStr() {
		return createdStr;
	}

	public void setCreatedStr(String createdStr) {
		this.createdStr = createdStr;
	}
	
	@Deprecated
	public String getUpdatedStr() {
		return updatedStr;
	}

	public void setUpdatedStr(String updatedStr) {
		this.updatedStr = updatedStr;
	}

	
    @Override
	public String toString() {
    	
		return "SellerSubcatFMMappingVO [subcatSellerCode=" + subcatSellerCode
				+ ", sellerCode=" + sellerCode + ", subcat=" + subcat
				+ ", fulfillmentModel=" + fulfillmentModel 
				+ ", enabled=" + enabled + ", createdStr=" + createdStr
				+ ", updatedStr=" + updatedStr + "]";
	}

    

}
