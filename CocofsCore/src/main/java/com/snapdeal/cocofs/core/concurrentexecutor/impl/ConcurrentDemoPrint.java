package com.snapdeal.cocofs.core.concurrentexecutor.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConcurrentDemoPrint {

    private static final Logger             LOG = LoggerFactory.getLogger(ConcurrentDemoPrint.class);
    
    public int printOne(String one){
        
        LOG.info("one -" + one);
        try {
            Thread.sleep(60000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        LOG.info("delayed one -" + one);
        return 1;
    }
    
    public int printTwo(String two){
        
        LOG.info("two -" + two);
        try {
            Thread.sleep(60000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        LOG.info("delayed two -" + two);
        return 2;
    }
    
public int printThree(){
        
        LOG.info("three " );
        try {
            Thread.sleep(60000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        LOG.info("delayed three");
        return 2;
    }
    
}
