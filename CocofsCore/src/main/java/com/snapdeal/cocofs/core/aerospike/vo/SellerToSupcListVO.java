/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.core.aerospike.vo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aerospike.client.large.LargeSet;
import com.snapdeal.score.data.aerospike.AerospikeSet;
import com.snapdeal.score.data.aerospike.annotations.Bin;
import com.snapdeal.score.data.aerospike.annotations.Key;
import com.snapdeal.score.data.aerospike.annotations.LDTSet;
import com.snapdeal.score.data.aerospike.annotations.Set;

/**
 *  
 *  @version     1.0, 25-Nov-2014
 *  @author ankur
 */

@Set(namespace = "cocofs_ldt", name = "seller_to_supc_list_vo")
public class SellerToSupcListVO implements AerospikeSet {

    /**
     * 
     */
    private static final long   serialVersionUID                      = 2315972857243333211L;

    private static final Logger LOG                                   = LoggerFactory.getLogger(SellerToSupcListVO.class);

    public static final String  SELLER_SUPC_LIST_BIN_NAME             = "ssls";

    @Key
    @Bin(name = "sc")
    private String              sellerCode;

    @Bin(name = SELLER_SUPC_LIST_BIN_NAME)
    @LDTSet
    private LargeSet            sellerToSupcList;

    /**
     * Default constructor should not be used explicity . The reason is we want to initialize LDTHolder
     */
    /*  @Deprecated
      SellerToSupcListVO(){    
      }*/

    /**
     * constructor used to initialize the LDTHolder with additional List of supc is to be added in sellerToSupcList.
     * 
     * @param sellerCode
     * @param supcList
     */
    /*public SellerToSupcListVO(String sellerCode, List<String> supcList) {
        this.sellerCode =sellerCode;
        this.sellerToSupcList = new LDTHolderList<String>(supcList);
        
    }*/

    public String getSellerCode() {
        return sellerCode;
    }

    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }

    public LargeSet getSellerToSupcList() {
        return sellerToSupcList;
    }

    public void setSellerToSupcList(LargeSet sellerToSupcList) {
        this.sellerToSupcList = sellerToSupcList;
    }

    @Override
    public String toString() {
        return "SellerToSupcListVO [sellerCode=" + sellerCode + ", sellerToSupcList=" + sellerToSupcList + "]";
    }

}
