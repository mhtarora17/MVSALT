/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.core.metrics.reporter;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.Meter;
import com.codahale.metrics.MetricFilter;
import com.codahale.metrics.graphite.Graphite;
import com.codahale.metrics.graphite.GraphiteReporter;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.core.metrics.Metrics;



/**
 *  
 *  @version     1.0, 22-Aug-2014
 *  @author ankur
 */
public class MetricsReporter {


    private static final Logger LOG = LoggerFactory.getLogger(MetricsReporter.class);

    private static GraphiteReporter    reporter;

    public void reportToGraphite() {
        LOG.info("Reporter Called");
        String appName = null;
        try {
            appName = InetAddress.getLocalHost().getHostName();
            LOG.info("AppName:{}", appName);
        } catch (UnknownHostException e) {
            LOG.info("Cannot Determine hostname, Reporting will not be up", e);
            return;
        }

        String serverName = null;
        try {
            serverName = System.getProperty("hostName");
            LOG.info("servername:{}", serverName);
        } catch (Exception e) {
            LOG.info("Cannot read system property", e);
        }

        String reportingPrefix = appName;
        if (serverName != null) {
            reportingPrefix += "-" + serverName;
        }

        LOG.info("Reporting Prefix : {}", reportingPrefix);

        final Graphite graphite = new Graphite(new InetSocketAddress(ConfigUtils.getStringScalar(Property.GRAPHITE_URL), ConfigUtils.getIntegerScalar(Property.GRAPHITE_PORT)));

        reporter = GraphiteReporter.forRegistry(Metrics.getRegistry()).prefixedWith(reportingPrefix).convertRatesTo(TimeUnit.SECONDS).convertDurationsTo(TimeUnit.MILLISECONDS).filter(
                MetricFilter.ALL).build(graphite);

        reporter.start(ConfigUtils.getIntegerScalar(Property.GRAPHITE_REPORTING_INTERVAL), TimeUnit.SECONDS);
    }

    public static void stopReporting() {
        if(reporter!=null){
            reporter.stop();
            Map <String,Meter> meters=Metrics.getRegistry().getMeters();
            Collection<Meter> existingMeters=meters.values();
            // First Need to unregister the meters from MetricRegistry, otherwise it wont let them GCed
            LOG.info("Removing Meters from MetricRegistry");
           
            Metrics.getRegistry().remove("jvm.gc");
            Metrics.getRegistry().remove("jvm.memory");
            Metrics.getRegistry().remove("jvm.thread-states");
            Metrics.getRegistry().remove("jvm.fd.usage");
            LOG.info("Meters Removed from MetricRegistry");
            // Setting the Meter references to Null, so that they can be  GCed
            LOG.info("Setting Meter Objects to Null");
            for(Meter meter:existingMeters){
                if(meter!=null){
                    meter=null;
                }
            }
            LOG.info("Meter Objects set to Null");
            LOG.info("Graphite Reporting stopped Successfully");
        }
    }
    
    public static GraphiteReporter getGraphiteReporter(){
        return reporter;
    }


}
