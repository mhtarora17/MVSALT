/*
 *  Copyright 2012 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 29-Oct-2012
 *  @author sankalp
 */
package com.snapdeal.cocofs.core.property.configurer;

import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

public class ApplicationPropertyConfigurer extends PropertyPlaceholderConfigurer {

    private static final Logger LOG = LoggerFactory.getLogger(ApplicationPropertyConfigurer.class);

    protected String resolvePlaceholder(String placeholder, Properties props) {
        return props.getProperty(placeholder);
    }
}
