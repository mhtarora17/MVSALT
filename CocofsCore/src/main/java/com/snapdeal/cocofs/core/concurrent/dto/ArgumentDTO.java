/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.core.concurrent.dto;


/**
 *  
 *  @version     1.0, 23-Sep-2014
 *  @author ankur
 */
@SuppressWarnings("rawtypes")
public class ArgumentDTO {

    private Class argumentType;
    
    private Object argumentValue;
    
    public ArgumentDTO(){
        
    }

    public ArgumentDTO(Class argumentType, Object argumentValue) {
        super();
        this.argumentType = argumentType;
        this.argumentValue = argumentValue;
    }

    public Class getArgumentType() {
        return argumentType;
    }

    public void setArgumentType(Class argumentType) {
        this.argumentType = argumentType;
    }

    public Object getArgumentValue() {
        return argumentValue;
    }

    public void setArgumentValue(Object argumentValue) {
        this.argumentValue = argumentValue;
    }

    @Override
    public String toString() {
        return "ArgumentDTO [argumentType=" + argumentType + ", argumentValue=" + argumentValue + "]";
    }

   

   
    
}
