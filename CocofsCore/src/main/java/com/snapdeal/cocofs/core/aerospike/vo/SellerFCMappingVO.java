/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.core.aerospike.vo;

import java.util.ArrayList;
import java.util.List;

import com.snapdeal.score.data.aerospike.AerospikeSet;
import com.snapdeal.score.data.aerospike.annotations.Bin;
import com.snapdeal.score.data.aerospike.annotations.Key;
import com.snapdeal.score.data.aerospike.annotations.Set;

/**
 *  
 *  @version     1.0, 19-Mar-2015
 *  @author ankur
 */

@Set(namespace = "cocofs", name = "seller_fc_mapping_vo")
public class SellerFCMappingVO implements AerospikeSet{


    /**
     * 
     */
    private static final long serialVersionUID = -3896451660845662139L;

    @Key
    @Bin(name = "sc")
    private String            sellerCode;

    @Bin(name = "fm")
    private String   fulfillmentModel;
    
    @Bin(name = "fc")
    private List<String>   fcCodes= new ArrayList<String>();
   
    @Bin(name = "e")
    private int  enabled;

    public String getSellerCode() {
        return sellerCode;
    }

    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }

    public String getFulfillmentModel() {
        return fulfillmentModel;
    }

    public void setFulfillmentModel(String fulfillmentModel) {
        this.fulfillmentModel = fulfillmentModel;
    }

    public List<String> getFcCodes() {
        return fcCodes;
    }

    public void setFcCodes(List<String> fcCode) {
        this.fcCodes = fcCode;
    }

    public int getEnabled() {
        return enabled;
    }

    public void setEnabled(int enabled) {
        this.enabled = enabled;
    }

    @Override
    public String toString() {
        return "SellerFCMappingVO [sellerCode=" + sellerCode + ", fulfillmentModel=" + fulfillmentModel + ", fcCodes=" + fcCodes + ", enabled=" + enabled + "]";
    }
  
   
}
