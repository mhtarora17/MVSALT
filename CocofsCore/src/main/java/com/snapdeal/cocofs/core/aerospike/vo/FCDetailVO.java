package com.snapdeal.cocofs.core.aerospike.vo;

import com.snapdeal.score.data.aerospike.AerospikeSet;
import com.snapdeal.score.data.aerospike.annotations.Bin;
import com.snapdeal.score.data.aerospike.annotations.Key;
import com.snapdeal.score.data.aerospike.annotations.Set;

@Set(namespace = "cocofs", name = "fc_detail_vo")
public class FCDetailVO implements AerospikeSet {

    /**
     * 
     */
    private static final long serialVersionUID = -977726152152405590L;

    @Key
    @Bin(name = "fc")
    private String            code;

    @Bin(name = "fnm")
    private String            fcName;

    @Bin(name = "ty")
    private String            type;

    @Bin(name = "sd")
    private int               sdinstant;

    @Bin(name = "anm")
    private String            addrName;

    @Bin(name = "al1")
    private String            addressLine1;

    @Bin(name = "al2")
    private String            addressLine2;

    @Bin(name = "ci")
    private String            city;

    @Bin(name = "st")
    private String            state;

    @Bin(name = "pi")
    private String            pincode;

    @Bin(name = "mo")
    private String            mobile;

    @Bin(name = "la")
    private String            landLine;

    @Bin(name = "dnd")
    private int               dnd_active;

    @Bin(name = "em")
    private String            email;

    @Bin(name = "e")
    private int            enabled;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getFcName() {
        return fcName;
    }

    public void setFcName(String fcName) {
        this.fcName = fcName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getSdinstant() {
        return sdinstant;
    }

    public void setSdinstant(int sdinstant) {
        this.sdinstant = sdinstant;
    }

    public String getAddrName() {
        return addrName;
    }

    public void setAddrName(String addrName) {
        this.addrName = addrName;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getLandLine() {
        return landLine;
    }

    public void setLandLine(String landLine) {
        this.landLine = landLine;
    }

    public int getDnd_active() {
        return dnd_active;
    }

    public void setDnd_active(int dnd_active) {
        this.dnd_active = dnd_active;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    
    public int getEnabled() {
        return enabled;
    }

    public void setEnabled(int enabled) {
        this.enabled = enabled;
    }

    @Override
    public String toString() {
        return "FCDetailVO [code=" + code + ", fcName=" + fcName + ", type=" + type + ", sdinstant=" + sdinstant + ", addrName=" + addrName + ", addressLine1=" + addressLine1
                + ", addressLine2=" + addressLine2 + ", city=" + city + ", state=" + state + ", pincode=" + pincode + ", mobile=" + mobile + ", landLine=" + landLine
                + ", dnd_active=" + dnd_active + ", email=" + email + ", enabled=" + enabled +"]";
    }

}
