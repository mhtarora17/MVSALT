package com.snapdeal.cocofs.core.concurrent.dto;


public class ConcurrentRequest {
	private String methodName;
	private Object object;
	private ArgumentDTO[] arguments;
	private boolean successful;
	private Throwable exception;
	private String   requetKey;  
	
	public ConcurrentRequest(String methodName, Object object, String requetKey, ArgumentDTO...arguments) {
		this.methodName = methodName;
		this.object = object;
		this.arguments = arguments;
		this.requetKey = requetKey;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}


   
    public ArgumentDTO[] getArguments() {
        return arguments;
    }

    public void setArguments(ArgumentDTO[] arguments) {
        this.arguments = arguments;
    }

    public Throwable getException() {
		return exception;
	}
	
	public void setException(Throwable exception) {
		this.exception = exception;
	}
	
	public boolean isSuccessful() {
		return successful;
	}
	
	public void setSuccessful(boolean successful) {
		this.successful = successful;
	}

    public String getRequetKey() {
        return requetKey;
    }

    public void setRequetKey(String requetKey) {
        this.requetKey = requetKey;
    }
    
    @Override
    public String toString() {

        StringBuffer sb = new StringBuffer("ConcurrentRequest : { methodName : ").append(this.methodName).append(", object : ").append(this.object)
        .append(", parameter : ").append(this.arguments).append(", successful : ").append(this.successful).append(", requetKey : ")
        .append(this.requetKey).append(", exception : ").append(null != this.exception ? this.exception.getMessage() : "");
        return sb.toString();
    }
}
