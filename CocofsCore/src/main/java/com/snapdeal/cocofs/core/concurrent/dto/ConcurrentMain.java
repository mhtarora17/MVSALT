package com.snapdeal.cocofs.core.concurrent.dto;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;

import com.snapdeal.cocofs.core.concurrentexecutor.ConcurrentRequestExecutor;
import com.snapdeal.cocofs.core.concurrentexecutor.impl.ConcurrentDemoPrint;
import com.snapdeal.cocofs.core.concurrentexecutor.impl.ConcurrentRequestExecutorImpl;

public class ConcurrentMain {

    private static final Logger            LOGGER               = org.slf4j.LoggerFactory.getLogger(ConcurrentMain.class);
    /**
     * @param args
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub

        LOGGER.info("start main");
        ConcurrentDemoPrint cdp = new ConcurrentDemoPrint();
        
        
        ArgumentDTO parameter = new ArgumentDTO(String.class, "abhinav1");
     //   ConcurrentRequest cr1 = new ConcurrentRequest("printOne", cdp, "abhinav1",parameter);
      //  ConcurrentRequest cr2 = new ConcurrentRequest("printTwo", cdp, "abhinav2",parameter);
        ConcurrentRequest cr3 = new ConcurrentRequest("printThree", cdp, "abhinav2");
        
        
        List<ConcurrentRequest> crList = new ArrayList<ConcurrentRequest>();
        //crList.add(cr1);
        //crList.add(cr2);
        crList.add(cr3);
        ConcurrentRequestExecutor e = new ConcurrentRequestExecutorImpl();
        e.executeConcurrently(crList);
        
    }

}
