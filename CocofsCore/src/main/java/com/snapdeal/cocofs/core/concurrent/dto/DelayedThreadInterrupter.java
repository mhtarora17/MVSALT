/**
 * 
 */
package com.snapdeal.cocofs.core.concurrent.dto;

/**
 * @author fanendra
 *
 */
public class DelayedThreadInterrupter implements DelayedAction {
	private final Thread thread;
	
	public DelayedThreadInterrupter(Thread thread) {
		this.thread = thread;
	}

	@Override
	public void performDelayedAction() {
		this.thread.interrupt();
	}
}
