/**
 * 
 */
package com.snapdeal.cocofs.core.concurrent.dto;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.DelayQueue;
import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is a special map which keeps track of {@link DelayedAction}. We can put DelayedAction 
 * which can be executed after the specified delay expires.<p>
 * The key is used to identify the associated DelayedAction.
 * <p>There are two methods for adding and removing the task.
 * {@link #addTask(Object, DelayedAction, long, TimeUnit)} adds a new key and associated DelayedAction. If
 * specified delay expires the associated task is removed from the map and gets executed. This action is 
 * performed using the inrenal thread {@link #delayActionExecutor}.<p>
 * If Delayed action for a key needs to be executed externally and it has been added to the map, 
 * it should be removed from this map using
 * {@link #removeTask(Object)} method.
 *  
 * @author fanendra
 *
 */
public class DelayedMap<K, V extends DelayedAction> {
	
	private static final Logger 					LOGGER = LoggerFactory.getLogger(DelayedMap.class);
	/**
	 * 
	 */
	private int 									MAX_SIZE = 2000;
	/**
	 * Holds the key and associated {@link DelayedAction}.
	 */
	private ConcurrentHashMap<K, DelayedAction> 	delayedMap = new ConcurrentHashMap<K, DelayedAction>();
	/**
	 * Holds delayed keys which are queued and retrieved whenever the given delay expires for the key.
	 */
	private DelayQueue<Delayed> 					delayedQueue = new DelayQueue<Delayed>();
	
	/**
	 * Creates {@link DelayedMap} instance with given {@link #MAX_SIZE}.
	 * @param maxSize
	 */
	public DelayedMap(int maxSize) {
		MAX_SIZE = maxSize;
		delayActionExecutor.start();
	}
	
	/**
	 * Creates {@link DelayedMap} with default {@link #MAX_SIZE}.
	 */
	public DelayedMap() {
		delayActionExecutor.start();
	}

	/**
	 * Adds a new DelayedAction with given delay and time unit. The key is provided to later access the 
	 * associated DelayedAction.
	 * @param key
	 * @param action
	 * @param delay
	 * @param unit
	 */
	public void addTask(K key, DelayedAction action, long delay, TimeUnit unit) {
		if(delayedMap.size() <= MAX_SIZE){
			DelayedKey keyData = new DelayedKey(key, delay, unit);
			delayedMap.put(key, action);
			delayedQueue.add(keyData);
		} else {
			LOGGER.info("Unable to add new Delayed task. Map is full...");
		}
	}
	
	/**
	 * Removes the DelayedAction that is associated with the given key and returns it.
	 * @param key
	 */
	public DelayedAction removeTask(K key) {
		DelayedKey keyData = new DelayedKey(key);
		delayedQueue.remove(keyData);
		return delayedMap.remove(key);
	}
	
	/**
	 * Defines Delayed key for given action and delay.
	 * @author fanendra
	 *
	 */
	private class DelayedKey implements Delayed {
		private K key;
		private long delay;

		public DelayedKey(K key, long delay, TimeUnit unit) {
			this.delay = System.currentTimeMillis() + unit.convert(delay, TimeUnit.MILLISECONDS);
			this.key = key;
		}
		
		public DelayedKey(K key) {
			this.key = key;
		}
		
		@Override
		public int compareTo(Delayed o) {
			return (int)(this.delay - ((DelayedKey)o).delay);
		}
		
		@Override
		public long getDelay(TimeUnit unit) {
			return unit.convert(this.delay-System.currentTimeMillis(), TimeUnit.MILLISECONDS);
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + ((key == null) ? 0 : key.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (!(obj instanceof DelayedMap.DelayedKey)) {
				return false;
			}
			DelayedKey other = (DelayedKey) obj;
			if (!getOuterType().equals(other.getOuterType())) {
				return false;
			}
			if (key == null) {
				if (other.key != null) {
					return false;
				}
			} else if (!key.equals(other.key)) {
				return false;
			}
			return true;
		}

		private DelayedMap getOuterType() {
			return DelayedMap.this;
		}
	}
	
	/**
	 * Executes the delayed action for expired {@link DelayedKey}. If associated {@link DelayedAction} is not 
	 * found (which is a valid scenario as external system can remove this delayed task if required) in the map 
	 * no action is taken.
	 */
	private Thread delayActionExecutor = new Thread(new Runnable() {
		@Override
		public void run() {
			while(true) {
				try {
					DelayedKey key = (DelayedKey)delayedQueue.take();
					DelayedAction actionToExecute = delayedMap.remove(key.key);
					if(actionToExecute != null){
						LOGGER.info("Running delayed task...");
						actionToExecute.performDelayedAction();
					}
				} catch(Throwable t) {
				}
			}
		}
	}, "DelayActionExecutor");
}
