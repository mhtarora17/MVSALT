/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 23-Oct-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.aspect;

import java.lang.reflect.Field;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.base.utils.DateUtils.DateRange;
import com.snapdeal.cocofs.annotations.Throttled;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.dto.ThrottledParamsDTO;

@Component
@Aspect
public class Throttler {

    private static final Logger                            LOG              = LoggerFactory.getLogger(Throttled.class);
    private final ConcurrentHashMap<String, MethodHistory> invocationMap    = new ConcurrentHashMap<String, Throttler.MethodHistory>();

//    private static final ThrottledParamsDTO                THROTTLED_PARAMS = new ThrottledParamsDTO();
    
    private static Map<MethodHistory, ThrottledParamsDTO> historyToParamsDTOMap = new HashMap<Throttler.MethodHistory, ThrottledParamsDTO>();

    private static class MethodHistory {
        public int  seconds;
        public long lastInvocation;
        public long allowance;
        public long lastResetTime;

        @Override
        public String toString() {
            return "MethodHistory [seconds=" + seconds + ", lastInvocation=" + lastInvocation + ", allowance=" + allowance + "]";
        }

    }

    @Before("@annotation(throttled)")
    public void rateLimit(JoinPoint jp, Throttled throttled) throws InterruptedException {
        String methodName = jp.getSignature().toLongString();
        MethodHistory h = getMethodHistory(throttled, methodName);
        synchronized (h) {
            long timeElapsed = System.nanoTime() - h.lastInvocation;
            if ((timeElapsed * 1.0e-9d) > h.seconds) {
                h = resetMethodHistory(h, throttled, methodName);
            }
            if (h.allowance > 0l) {
                h.allowance--;
                if (LOG.isDebugEnabled()) {
                    LOG.debug("Throttled method invocation allowed " + methodName + " history " + h.toString());
                }
            } else {
                long sleepTime = (long) (h.seconds * 1e3 - (System.nanoTime() - h.lastInvocation) * 1e-6);
                if (sleepTime > 0) {
                    if (LOG.isDebugEnabled()) {
                        LOG.debug("Sleeping for some time..{} ", sleepTime);
                    }
                    Thread.sleep(sleepTime);
                }
            }
        }

    }

    private MethodHistory getMethodHistory(Throttled throttled, String methodName) {
        MethodHistory h = invocationMap.get(methodName);
        if (null == h) {
            h = new MethodHistory();
            h = resetMethodHistory(h, throttled, methodName);
            MethodHistory oh = invocationMap.putIfAbsent(methodName, h);
            if (null != oh) {
                h = oh;
            }
        }
        return h;
    }

    private MethodHistory resetMethodHistory(MethodHistory h, Throttled throttled, String methodName) {
        h.lastInvocation = System.nanoTime();
        //        if (resetMethodHistoryParams(h, throttled.overridingProperty())) {
        //            ThrottledParamsDTO dto = getThrottledParamDTO(throttled);
        //            h.seconds = dto.getDurationTime();
        //            h.allowance = getAllowance(dto);
        //            h.lastResetTime = System.currentTimeMillis();
        //        }
        ThrottledParamsDTO dto = getThrottledParamDTO(throttled, h);
        h.seconds = dto.getDurationTime();
        h.allowance = getAllowance(dto);
        return h;
    }

    private boolean resetMethodHistoryParams(MethodHistory h, Property property) {
        String resetTimeDelay = ConfigUtils.getStringScalar(Property.RESET_THROTTLING_TIME_DELAY_IN_MIN);
        long timeDelay = Long.parseLong(resetTimeDelay);
        if (((System.currentTimeMillis() - h.lastResetTime) >= (timeDelay * 60 * 1e6))) {
            return true;
        }
        DateRange dateRange = getStartAndEndDateDayRange(getHistoryTime(property, Property.THROTTLE_NIGHT_END_TIME), getHistoryTime(property, Property.THROTTLE_NIGHT_START_TIME));
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(h.lastResetTime);
        Date resetTime = calendar.getTime();
        if ((DateUtils.after(dateRange.getStart(), resetTime) && DateUtils.after(DateUtils.getCurrentTime(), dateRange.getStart()))
                || (DateUtils.after(dateRange.getEnd(), resetTime) && DateUtils.after(DateUtils.getCurrentTime(), dateRange.getEnd()))) {
            return true;
        }
        return false;

    }

    private String getHistoryTime(Property overridingProperty, Property propertyName) {
        String value = null;
        if (null != overridingProperty) {
            Map<String, String> paramToValueMap = ConfigUtils.getMap(overridingProperty);
            if (null != paramToValueMap && !paramToValueMap.isEmpty()) {
                value = paramToValueMap.get(propertyName.getName());
            }
        }
        if (null == value) {
            value = ConfigUtils.getStringScalar(propertyName);
        }
        return value;
    }

    // Return the date range with start and end date signifying the date start and end of duration considered night
    public DateRange getStartAndEndDateDayRange(String nightEndTime, String nightStartTime) {
        Date nightEndDate = updateDateWithHoursAndMin(Integer.parseInt(nightEndTime.split(":")[0]), Integer.parseInt(nightEndTime.split(":")[1]));
        Date nightStartDate = updateDateWithHoursAndMin(Integer.parseInt(nightStartTime.split(":")[0]), Integer.parseInt(nightStartTime.split(":")[1]));

        // Make sure night end is after night start :)
        if (nightStartDate.after(nightEndDate)) {
            nightEndDate = DateUtils.addToDate(nightEndDate, Calendar.DATE, 1);
        }

        // Night start time and current time always fall on same day
        Date now = DateUtils.getCurrentTime();

        if (now.before(nightStartDate)) {
            // If start time is after now say e.g now is 14:00 and start is 14:30, 
            // then make sure start time is before now if end time is like 14:15
            if (now.before(DateUtils.addToDate(nightEndDate, Calendar.DATE, -1))) {
                nightEndDate = DateUtils.addToDate(nightEndDate, Calendar.DATE, -1);
                nightStartDate = DateUtils.addToDate(nightStartDate, Calendar.DATE, -1);
            }
        }
        /*
         * end time 09:30 - 16 9:30
         * start 21:30 - 15 21:30
         * today 15 01:00
         */
        return new DateRange(nightStartDate, nightEndDate);

    }

    private Date updateDateWithHoursAndMin(int hours, int minutes) {
        Calendar now = Calendar.getInstance();
        now.set(now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_MONTH), hours, minutes, 0);
        now.set(Calendar.MILLISECOND, 0);
        return now.getTime();
    }

    private long getAllowance(ThrottledParamsDTO dto) {
        DateRange dateRange = getStartAndEndDateDayRange(dto.getNightEndTime(), dto.getNightStartTime());
        Date now = DateUtils.getCurrentTime();
        if (DateUtils.after(now, dateRange.getStart()) && DateUtils.after(dateRange.getEnd(), now)) {
            return dto.getCallsAllowedAtNight();
        } else {
            return dto.getCallsAllowedDuringDay();
        }
    }

    private ThrottledParamsDTO getThrottledParamDTO(Throttled throttled, MethodHistory h) {
        ThrottledParamsDTO dto = getThrottledParamDTOAtMethodPropertyLevel(h, throttled.overridingProperty());
        try {
            if (isAnyNullField(dto)) {
                enrichRemainingThrottledParamsDto(throttled, dto);
                if (isAnyNullField(dto)) {
                    throw new RuntimeException("*****... You are trying to make a mockery of annotation :P");
                }
            }
        } catch (IllegalArgumentException e) {
            LOG.error("Exception ", e);
        } catch (IllegalAccessException e) {
            LOG.error("Exception ", e);
        }
        return dto;
    }

    private void enrichRemainingThrottledParamsDto(Throttled throttled, ThrottledParamsDTO dto) {
        if (null == dto.getCallsAllowedAtNight()) {
            if (throttled.callsAllowedAtNight() > 0) {
                dto.setCallsAllowedAtNight(throttled.callsAllowedAtNight());
            } else {
                dto.setCallsAllowedAtNight(Long.parseLong(ConfigUtils.getStringScalar(Property.THROTTLED_CALLS_DURING_NIGHT)));
            }
        }

        if (null == dto.getCallsAllowedDuringDay()) {
            if (throttled.callsAllowedDuringDay() > 0) {
                dto.setCallsAllowedDuringDay(throttled.callsAllowedDuringDay());
            } else {
                dto.setCallsAllowedDuringDay(Long.parseLong(ConfigUtils.getStringScalar(Property.THROTTLED_CALLS_DURING_DAY)));
            }
        }
        if (null == dto.getDurationTime()) {
            if (throttled.seconds() > 0) {
                dto.setDurationTime(throttled.seconds());
            } else {
                dto.setDurationTime(Integer.parseInt(ConfigUtils.getStringScalar(Property.THROTTLED_CALL_DURATION_WINDOW)));
            }
        }
        if (dto.getNightEndTime() == null) {
            dto.setNightEndTime(ConfigUtils.getStringScalar(Property.THROTTLE_NIGHT_END_TIME));
        }
        if (dto.getNightStartTime() == null) {
            dto.setNightStartTime(ConfigUtils.getStringScalar(Property.THROTTLE_NIGHT_START_TIME));
        }

    }

    private ThrottledParamsDTO getThrottledParamDTOAtMethodPropertyLevel(MethodHistory h, Property overridingProperty) {
        ThrottledParamsDTO dto =historyToParamsDTOMap.get(h);
        if(null == dto){
            dto = new ThrottledParamsDTO();
            historyToParamsDTOMap.put(h, dto);
        }
        if (null == overridingProperty) {
            return dto;
        }
        dto.setCallsAllowedAtNight(null);
        dto.setCallsAllowedDuringDay(null);
        dto.setDurationTime(null);
        dto.setNightEndTime(null);
        dto.setNightStartTime(null);

        Map<String, String> paramToValueMap = ConfigUtils.getMap(overridingProperty);
        if (null != paramToValueMap && !paramToValueMap.isEmpty()) {
            String durationWindow = paramToValueMap.get(Property.THROTTLED_CALL_DURATION_WINDOW.getName());
            dto.setDurationTime(null != durationWindow ? Integer.parseInt(durationWindow) : null);
            String allowedCallsDuringDay = paramToValueMap.get(Property.THROTTLED_CALLS_DURING_DAY.getName());
            dto.setCallsAllowedDuringDay(null == allowedCallsDuringDay ? null : Long.parseLong(allowedCallsDuringDay));
            String allowedCallsAtNight = paramToValueMap.get(Property.THROTTLED_CALLS_DURING_NIGHT.getName());
            dto.setCallsAllowedAtNight(null == allowedCallsAtNight ? null : Long.parseLong(allowedCallsAtNight));
            dto.setNightStartTime(paramToValueMap.get(Property.THROTTLE_NIGHT_START_TIME.getName()));
            dto.setNightEndTime(paramToValueMap.get(Property.THROTTLE_NIGHT_END_TIME.getName()));
        }
        return dto;
    }

    public static <T> boolean isAnyNullField(T t) throws IllegalArgumentException, IllegalAccessException {
        boolean anyFieldNull = false;
        for (Field field : t.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            if (field.get(t) == null) {
                anyFieldNull = true;
                break;
            }
        }
        return anyFieldNull;
    }

}