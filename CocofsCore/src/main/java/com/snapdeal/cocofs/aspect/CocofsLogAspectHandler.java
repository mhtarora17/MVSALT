/**
 * 
 */
package com.snapdeal.cocofs.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * @author abhinav
 */

@Aspect
@Service("CocofsLogAspectHandler")
public class CocofsLogAspectHandler {

    private static final Logger LOG = LoggerFactory.getLogger(CocofsLogAspectHandler.class);

    @Pointcut("execution(* com.snapdeal.cocofs.mongo.mao.IGenericMao.*(..))")
    public void executeGenericMaoMethods() {
    }
    
    @Pointcut("execution(* com.snapdeal.cocofs.external.service.ICAMSExternalService.*(..))")
    public void executeCAMSExternalServiceMethods() {
    }
    
    @Pointcut("execution(* com.snapdeal.cocofs.external.service.IOPSExternalService.*(..))")
    public void executeOPSExternalServiceMethods() {
        
    }
    
    @Pointcut("execution(* com.snapdeal.cocofs.external.service.IVIPMSExternalService.*(..))")
    public void executeVIPMSExternalServiceMethods() {
        
    }
    
    @Around("executeGenericMaoMethods() || executeCAMSExternalServiceMethods() || executeOPSExternalServiceMethods() || executeVIPMSExternalServiceMethods() ")
    public Object profile(ProceedingJoinPoint pjp) throws Throwable {
        long timeStartInMillisecs = System.currentTimeMillis();
        MethodSignature signature = (MethodSignature) pjp.getSignature();
        Object output = pjp.proceed();
        LOG.debug("Time Taken to execute : " + signature + " " + (System.currentTimeMillis() - timeStartInMillisecs) + " ms");
        return output;

    }
}
