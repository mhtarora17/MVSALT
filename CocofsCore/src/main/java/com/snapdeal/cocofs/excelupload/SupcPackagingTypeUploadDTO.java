/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.excelupload;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import com.snapdeal.cocofs.annotation.IgnoreInDownloadTemplate;

/**
 *  
 *  @version     1.0, 09-Dec-2015
 *  @author vikas sharma
 */

@XmlRootElement
public class SupcPackagingTypeUploadDTO {

    private String              supc;
    
    private String              storeCode;
    
    private String              packagingType;
    
    private Boolean             enable; 
    
    @IgnoreInDownloadTemplate
    private Date     created;
    
    @IgnoreInDownloadTemplate
    private Date last_updated;
    
    @IgnoreInDownloadTemplate
    private Date     updated;
    
    @IgnoreInDownloadTemplate
    private String updated_by;
    
    
    
    public String getSupc() {
        return supc;
    }

    public void setSupc(String supc) {
        this.supc = supc;
    }
    
    public String getPackagingType() {
        return packagingType;
    }

    public void setPackagingTypeCode(String packagingType) {
        this.packagingType = packagingType;
    }

   

    public String getStoreCode() {
        return storeCode;
    }
    
    public void setStoreCode(String storeCode)
    {
        this.storeCode = storeCode;
    }

    public Boolean isEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }
    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getLast_updated() {
        return last_updated;
    }

    public void setLast_updated(Date last_updated) {
        this.last_updated = last_updated;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public String getUpdated_by() {
        return updated_by;
    }

    public void setUpdated_by(String updated_by) {
        this.updated_by = updated_by;
    }


    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((storeCode == null) ? 0 : storeCode.hashCode());
        result = prime * result + ((supc == null) ? 0 : supc.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SupcPackagingTypeUploadDTO other = (SupcPackagingTypeUploadDTO) obj;
        if (storeCode == null) {
            if (other.storeCode != null)
                return false;
        } else if (!storeCode.equals(other.storeCode))
            return false;
        if (supc == null) {
            if (other.supc != null)
                return false;
        } else if (!supc.equals(other.supc))
            return false;
        return true;
    }
    
}
