/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 03-Sep-2013
 *  @author himanshum
 */
package com.snapdeal.cocofs.excelupload;

import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

import com.snapdeal.cocofs.annotation.IgnoreInDownloadTemplate;

@XmlRootElement
public class ProductAttributeUploadDTO {

    private String              supc;

    private String              serializedType;

    private Boolean             fragile;

    private Integer             productParts;

    private Double              weight;

    private Double              length;

    private Double              breadth;

    private Double              height;

    private Double              primaryLength;

    private Double              primaryBreadth;

    private Double              primaryHeight;

    @IgnoreInDownloadTemplate
    private Boolean             systemWeightCaptured;

    @IgnoreInDownloadTemplate
    private Map<String, String> createdByEmailMap = new HashMap<String, String>();

    private Boolean             woodenPackaging;

    private String              dangerousGoodsType;

    private String              packagingType;

    
    public String getSupc() {
        return supc;
    }

    public void setSupc(String supc) {
        this.supc = supc;
    }

    public boolean isFragile() {
        return fragile;
    }

    public void setFragile(boolean fragile) {
        this.fragile = fragile;
    }

    public Integer getProductParts() {
        return productParts;
    }

    public void setProductParts(Integer productParts) {
        this.productParts = productParts;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Double getLength() {
        return length;
    }

    public void setLength(Double length) {
        this.length = length;
    }

    public Double getBreadth() {
        return breadth;
    }

    public void setBreadth(Double breadth) {
        this.breadth = breadth;
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public Double getPrimaryLength() {
        return primaryLength;
    }

    public void setPrimaryLength(Double primaryLength) {
        this.primaryLength = primaryLength;
    }

    public Double getPrimaryBreadth() {
        return primaryBreadth;
    }

    public void setPrimaryBreadth(Double primaryBreadth) {
        this.primaryBreadth = primaryBreadth;
    }

    public Double getPrimaryHeight() {
        return primaryHeight;
    }

    public void setPrimaryHeight(Double primaryHeight) {
        this.primaryHeight = primaryHeight;
    }

    public Boolean getSystemWeightCaptured() {
        return systemWeightCaptured;
    }

    public void setSystemWeightCaptured(Boolean systemWeightCaptured) {
        this.systemWeightCaptured = systemWeightCaptured;
    }

    public void addCreatedByforAttribute(String attribute, String createdBy) {
        createdByEmailMap.put(attribute, createdBy);
    }

    public String getCreatedByforAttribute(String attribute) {
        return createdByEmailMap.get(attribute);
    }

    public Boolean isWoodenPackaging() {
        return woodenPackaging;
    }

    public void setWoodenPackaging(Boolean woodenPackaging) {
        this.woodenPackaging = woodenPackaging;
    }

    public String getDangerousGoodsType() {
        return dangerousGoodsType;
    }

    public void setDangerousGoodsType(String dangerousGoodsType) {
        this.dangerousGoodsType = dangerousGoodsType;
    }

    public String getSerializedType() {
        return serializedType;
    }

    public void setSerializedType(String serializedType) {
        this.serializedType = serializedType;
    }

    //using only SUPC for hashCode and equals comparision.

    public String getPackagingType() {
        return packagingType;
    }

    public void setPackagingType(String packagingType) {
        this.packagingType = packagingType;
    }

    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((supc == null) ? 0 : supc.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ProductAttributeUploadDTO other = (ProductAttributeUploadDTO) obj;
        if (supc == null) {
            if (other.supc != null)
                return false;
        } else if (!supc.equals(other.supc))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "ProductAttributeUploadDTO [supc=" + supc + ", serializedType=" + serializedType + ", fragile=" + fragile + ", productParts=" + productParts + ", weight=" + weight
                + ", length=" + length + ", breadth=" + breadth + ", height=" + height + ", primaryLength=" + primaryLength + ", primaryBreadth=" + primaryBreadth
                + ", primaryHeight=" + primaryHeight + ",systemWeightCaptured=" + systemWeightCaptured + ", createdByEmailMap=" + createdByEmailMap + ", woodenPackaging="
                + woodenPackaging + ", dangerousGoodsType=" + dangerousGoodsType + ", packagingType=" + packagingType +  "]";
    }

}
