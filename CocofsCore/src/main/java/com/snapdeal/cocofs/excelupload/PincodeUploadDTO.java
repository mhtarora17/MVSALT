package com.snapdeal.cocofs.excelupload;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.cocofs.annotation.IgnoreInDownloadTemplate;

@XmlRootElement
public class PincodeUploadDTO {

    private String pincode;
    
    @IgnoreInDownloadTemplate
    private Date   lastUpdated = DateUtils.getCurrentTime();


    public PincodeUploadDTO(){
    }
    
    public PincodeUploadDTO(String pincode) {
        super();
        this.pincode = pincode;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }
    

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    @Override
    public String toString() {
        return "PincodeUploadDTO [pincode=" + pincode + "]";
    }
    
    
}
