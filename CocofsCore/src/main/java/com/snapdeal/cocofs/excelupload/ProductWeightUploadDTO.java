/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 03-Sep-2013
 *  @author himanshum
 */
package com.snapdeal.cocofs.excelupload;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ProductWeightUploadDTO {

    private String  supc;


    private Double  weight;


    public String getSupc() {
        return supc;
    }

    public void setSupc(String supc) {
        this.supc = supc;
    }



    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "ProductWeightUploadDTO [supc=" + supc + ", weight=" + weight + "]";
    }




}
