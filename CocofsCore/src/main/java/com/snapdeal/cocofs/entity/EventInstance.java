package com.snapdeal.cocofs.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "event_instance")
public class EventInstance extends BaseEntity {

    /**
     * 
     */
    private static final long serialVersionUID = 3189955040629875201L;
    private String            eventType;
    private String            jsonArguments;
    private String            param1;
    private String            param2;
    private boolean           executed;
    private Integer           retryCount;
    private boolean           enabled;
    private Date              updated;

    public EventInstance() {
    }

    @Column(name = "event_type")
    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    @Column(name = "param1")
    public String getParam1() {
        return param1;
    }

    public void setParam1(String param1) {
        this.param1 = param1;
    }

    @Column(name = "param2")
    public String getParam2() {
        return param2;
    }

    public void setParam2(String param2) {
        this.param2 = param2;
    }

    @Column(name = "json_arguments")
    public String getJsonArguments() {
        return jsonArguments;
    }

    public void setJsonArguments(String jsonArguments) {
        this.jsonArguments = jsonArguments;
    }

    @Column(name = "executed")
    public boolean isExecuted() {
        return executed;
    }

    public void setExecuted(boolean executed) {
        this.executed = executed;
    }

    @Column(name = "retry_count")
    public Integer getRetryCount() {
        return retryCount;
    }

    public void setRetryCount(Integer retryCount) {
        this.retryCount = retryCount;
    }

    @Column(name = "enabled")
    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Column(name = "updated")
    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    @Override
    public String toString() {
        return "EventInstance [" + "id=" + getId() + ", eventType=" + eventType + ", jsonArguments=" + jsonArguments + ", param1=" + param1 + ", param2=" + param2 + ", executed="
                + executed + ", retryCount=" + retryCount + ", enabled=" + enabled + ", created=" + getCreated() + ", updated=" + updated + "]";
    }

}
