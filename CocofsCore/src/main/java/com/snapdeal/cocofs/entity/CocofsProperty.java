package com.snapdeal.cocofs.entity;

// Generated 16 Aug, 2010 10:08:39 PM by Hibernate Tools 3.2.4.GA
//Cocofs
import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

import com.snapdeal.base.utils.DateUtils;

@Entity
@Audited
@Table(name = "cocofs_property")
public class CocofsProperty implements java.io.Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -6507176103210740800L;
    private String            name;
    private String            value;
    private Date              created;
    private Integer           id;
    private Date              lastUpdated;
    private String            updated_by;

    public CocofsProperty() {
    }

    public CocofsProperty(String name, String value) {
        this.name = name;
        this.value = value;
        this.created = DateUtils.getCurrentTime();
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "name", unique = true, nullable = false, length = 48)
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "value", nullable = false, length = 256)
    public String getValue() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created", nullable = false)
    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_updated", length = 19)
    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    @Column(name = "updated_by", nullable = false)
    public String getUpdated_by() {
        return updated_by;
    }

    public void setUpdated_by(String updated_by) {
        this.updated_by = updated_by;
    }

    @Override
    public String toString() {
        return "CocofsProperty [name=" + name + ", value=" + value + ", created=" + created + ", id=" + id + ", lastUpdated=" + lastUpdated + ", updated_by=" + updated_by + "]";
    }

}
