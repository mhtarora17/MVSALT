/**
 
*  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *  
 *  @version     1.0, 15-Dec-2015
 *  @author vikas sharma
 */
@Entity
@Table(name = "supc_packagingtype_mapping")
public class SupcPackagingTypeMapping  extends AbstractSupcPackagingTypeMapping{

    /**
     * 
     */
    private static final long serialVersionUID = 6583455831134991280L;
    
    
    private String              supc;
    
    private String              storeCode;
    
    private String              packagingType;
    
    private Boolean             enabled;
     
    
    @Column(name = "supc")
    public String getSupc() {
        return supc;
    }


    public void setSupc(String supc) {
        this.supc = supc;
    }
    
    @Column(name = "store_code")
    public String getStoreCode() {
        return storeCode;
    }


    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }

    @Column(name = "packaging_type")
    public String getPackagingType() {
        return packagingType;
    }


    public void setPackagingType(String packagingType) {
        this.packagingType = packagingType;
    }


    @Column(name = "enabled")
    public Boolean isEnabled() {
        return enabled;
    }


    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }


    @Override
    public String toString() {
        return "SupcPackagingTypeMapping [supc=" + supc + ", storeCode=" + storeCode + ", packagingType=" + packagingType + ",  enabled=" + enabled + "]";
    }


    



 
}
