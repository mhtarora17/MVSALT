/**

 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.hibernate.envers.Audited;

/**
 * @version 1.0, 18-Aug-2014
 * @author ankur
 */

@MappedSuperclass
@Audited
public abstract class AbstractTaxRate extends BaseEntity {

    private static final long serialVersionUID = 4306523124126814218L;

    public enum Type {
        VAT_CST("vat/cst"), SERVICE_TAX("service_tax");

        private String code;

        private Type(String code) {
            this.code = code;
        }

        public String code() {
            return code;
        }
    }

    private Double  taxRate;

    private Date    lastUpdated;

    private boolean enabled;

    private String  taxType;

    private String  updatedBy;

    private Long    version;

    @Column(name = "tax_rate")
    public Double getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(Double taxRate) {
        this.taxRate = taxRate;
    }

    @Column(name = "tax_type")
    public String getTaxType() {
        return taxType;
    }

    public void setTaxType(String taxType) {
        this.taxType = taxType;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_updated", length = 19)
    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    @Column(name = "enabled")
    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Column(name = "updated_by")
    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Version
    @Column(name = "version")
    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "AbstractTaxRate [taxRate=" + taxRate + ", lastUpdated=" + lastUpdated + ", enabled=" + enabled + ", taxType=" + taxType + ", updatedBy=" + updatedBy + "]";
    }

}
