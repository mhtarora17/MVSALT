/*
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 15-Jun-2015
 *  @author Gaurav
 */
package com.snapdeal.cocofs.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

/**
 * Seller-to-Zone mapping entity as deduced from the P-Z system...
 * @author gaurav
 *
 */

@Entity
@Audited
@Table(name = "seller_zone_mapping")
public class SellerZoneMapping implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3961769285582952631L;
	private Integer id;
	private String sellerCode;
	private String zone;
	private Date created;
	private String updatedBy;
	private Date lastUpdated;

	public SellerZoneMapping() {
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "seller_code", nullable = false)
	public String getSellerCode() {
		return sellerCode;
	}

	public void setSellerCode(String sellerCode) {
		this.sellerCode = sellerCode;
	}

	@Column(name = "zone", nullable = false)
	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created", length = 19)
	public Date getCreated() {
		return this.created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	
	@Column(name = "updated_by")
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_updated", length = 19)
    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }


	@Override
	public String toString() {
		return "SellerZoneMapping [id=" + id + ", sellerCode=" + sellerCode
				+ ", zone=" + zone + ", created=" + created + ",  updatedBy=" + updatedBy + ", lastUpdated="
				+ lastUpdated + "]";
	}

}
