/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

/**
 *  
 *  @version     1.0, 18-Aug-2014
 *  @author ankur
 */

@Audited
@Entity
@Table(name = "state_category_price_tax_rate")
public class StateCategoryPriceTaxRate extends AbstractTaxRate {
  
    /**
     * 
     */
    private static final long serialVersionUID = 1237523929127812227L;

    private String state;

    private String categoryUrl;

    private Double lowerPriceLimit;

    private Double upperPriceLimit;

    
    @Column(name = "state", nullable = false)
    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Column(name = "category_url", nullable = false)
    public String getCategoryUrl() {
        return categoryUrl;
    }

    public void setCategoryUrl(String categoryUrl) {
        this.categoryUrl = categoryUrl;
    }

    @Column(name = "lower_price_limit", nullable = true)
    public Double getLowerPriceLimit() {
        return lowerPriceLimit;
    }

    public void setLowerPriceLimit(Double lowerPriceLimit) {
        this.lowerPriceLimit = lowerPriceLimit;
    }

    @Column(name = "upper_price_limit", nullable = true)
    public Double getUpperPriceLimit() {
        return upperPriceLimit;
    }

    public void setUpperPriceLimit(Double upperPriceLimit) {
        this.upperPriceLimit = upperPriceLimit;
    }

    @Override
    public String toString() {
        return "StateCategoryPriceTaxRate [state=" + state + ", categoryUrl=" + categoryUrl + ", lowerPriceLimit=" + lowerPriceLimit + ", upperPriceLimit=" + upperPriceLimit
                + ", getTaxRate()=" + getTaxRate() + "]";
    }

   
}
