package com.snapdeal.cocofs.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "job_status")
public class JobStatus implements java.io.Serializable {

	private static final long serialVersionUID = 6998679651911103572L;

	private Integer id;
	private String code;
	private String value;
	private String description;
	private Date created;

	public enum Type {
		CREATED("CTD"), VALIDATION_FAILED("VALF"), VALIDATED("VALD"), PROCESSING(
				"PROC"), EXECUTED("EXEC"), FAILED("FLD"), CANCELLED("CLD"), HOLD(
				"HLD");
		private String code;

		Type(String code) {
			this.code = code;
		}

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}

	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "code", unique = true, nullable = false)
	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Column(name = "value", nullable = false)
	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Column(name = "description")
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "created")
	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	@Override
	public String toString() {
		return "JobStatus [id=" + id + ", code=" + code + ", value=" + value
				+ ", description=" + description + ", created=" + created + "]";
	}

}
