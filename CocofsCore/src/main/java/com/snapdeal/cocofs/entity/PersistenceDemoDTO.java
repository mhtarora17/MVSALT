package com.snapdeal.cocofs.entity;

public class PersistenceDemoDTO {

    private String a1;
    private Integer a2;
    private PesistentDemoSub1 sub1;
    
    
    public String getA1() {
        return a1;
    }
    public void setA1(String a1) {
        this.a1 = a1;
    }
    public Integer getA2() {
        return a2;
    }
    public void setA2(Integer a2) {
        this.a2 = a2;
    }
    public PesistentDemoSub1 getSub1() {
        return sub1;
    }
    public void setSub1(PesistentDemoSub1 sub1) {
        this.sub1 = sub1;
    }
}
