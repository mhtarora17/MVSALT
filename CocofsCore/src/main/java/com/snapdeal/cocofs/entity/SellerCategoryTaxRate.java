/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

/**
 *  
 *  @version     1.0, 18-Aug-2014
 *  @author ankur
 */
@Audited
@Entity
@Table(name = "seller_category_tax_rate")
public class SellerCategoryTaxRate extends AbstractTaxRate{

    /**
     * 
     */
    private static final long serialVersionUID = 2179013920131611227L;

    private String categoryUrl;
    
    private String sellerCode;

    @Column(name = "category_url", nullable = false)
    public String getCategoryUrl() {
        return categoryUrl;
    }

    public void setCategoryUrl(String categoryUrl) {
        this.categoryUrl = categoryUrl;
    }

    @Column(name = "seller_code", nullable = false)
    public String getSellerCode() {
        return sellerCode;
    }

    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }

    @Override
    public String toString() {
        return "SellerCategoryTaxRate [categoryUrl=" + categoryUrl + ", sellerCode=" + sellerCode + ", getTaxRate()=" + getTaxRate() + super.toString() + "]";
    }

    
    
    
    
}
