/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 09-Sep-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "task_status_info", catalog = "cocofs")
public class TaskStatusInfo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 7102234146244758209L;

    private Integer           Id;

    private String            taskName;

    private Date              previousExecutionTime;

    private Date              nextExecutionTime;

    private String            currentStatus;

    private Date              created;

    public TaskStatusInfo() {
    }

    public TaskStatusInfo(String taskName, Date previousExecutionTime, Date nextExecutionTime, String currentStatus, Date created) {
        this.taskName = taskName;
        this.previousExecutionTime = previousExecutionTime;
        this.nextExecutionTime = nextExecutionTime;
        this.currentStatus = currentStatus;
        this.created = created;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    @Column(name = "task_name")
    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "previous_execution_time")
    public Date getPreviousExecutionTime() {
        return previousExecutionTime;
    }

    public void setPreviousExecutionTime(Date previousExecutionTime) {
        this.previousExecutionTime = previousExecutionTime;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "next_execution_time")
    public Date getNextExecutionTime() {
        return nextExecutionTime;
    }

    public void setNextExecutionTime(Date nextExecutionTime) {
        this.nextExecutionTime = nextExecutionTime;
    }

    @Column(name = "current_status")
    public String getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created")
    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

}
