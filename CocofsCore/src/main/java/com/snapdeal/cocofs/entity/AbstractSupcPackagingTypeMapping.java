/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *  
 *  @version     1.0, 15-Dec-2015
 *  @author vikassharma03
 */

@MappedSuperclass
public class AbstractSupcPackagingTypeMapping extends BaseEntity  {

    /**
     * 
     */
    private static final long serialVersionUID = -5296150800048938825L;
    
    private Date              lastUpdated;

    private String            updatedBy;
    
    
    
    
   
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_updated", length=19)
    public Date getLastUpdated() {
        return lastUpdated;
    }



    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }


    @Column(name = "updated_by")
    public String getUpdatedBy() {
        return updatedBy;
    }





    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }



    @Override
    public String toString() {
        return "AbstractSupcPackagingTypeMapping [lastUpdated=" + lastUpdated + ", updatedBy=" + updatedBy + "]";
    }
    
  


    


   




 

    }
    
   
    
    
    


