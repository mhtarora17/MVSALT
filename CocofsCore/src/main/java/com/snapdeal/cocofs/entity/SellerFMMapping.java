/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 30-Sep-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.entity;

import java.util.Collection;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

@Entity
@Audited
@Table(name = "seller_fm_mapping")
public class SellerFMMapping extends AbstractFMMapping {

    /**
     * 
     */
    private static final long        serialVersionUID = 5183633788798364193L;

    private boolean                  supcExist;

    private boolean                  subcatExist;

    private Set<SellerFCCodeMapping> fcCenters;

    @Column(name = "supc_exist")
    public boolean isSupcExist() {
        return supcExist;
    }

    public void setSupcExist(boolean supcExist) {
        this.supcExist = supcExist;
    }

    @Column(name = "subcat_exist")
    public boolean isSubcatExist() {
        return subcatExist;
    }

    public void setSubcatExist(boolean subcatExist) {
        this.subcatExist = subcatExist;
    }

    @NotAudited
    @OneToMany(mappedBy = "sellerFMMapping", cascade = CascadeType.ALL)
    public Set<SellerFCCodeMapping> getFcCenters() {
        return this.fcCenters;
    }

    public void setFcCenters(Set<SellerFCCodeMapping> fcCenters) {
        this.fcCenters = fcCenters;
    }

    @Override
    public String toString() {
        return "SellerFMMapping [supcExist=" + supcExist + ", subcatExist=" + subcatExist + ", " + super.toString() + ", fcCenters=" + getFCCentersStr(fcCenters) + "]";
    }

    private String getFCCentersStr(Collection<SellerFCCodeMapping> fcCenters) {

        String str = null;
        if (fcCenters != null) {
            str = "{";
            if (!fcCenters.isEmpty()) {
                for (SellerFCCodeMapping fc : fcCenters) {
                    if (fc != null) {
                        str += fc.getFcCode() + "?" + fc.isEnabled() + ",";
                    }
                }
            }
            //TODO get rid of last comma
            str += "}";
        }
        return str;

    }

}
