package com.snapdeal.cocofs.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "pending_product_attribute_update")
public class PendingProductAttributeUpdate implements java.io.Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /** Reasons because of which a notification has pending = true **/
    private static enum PendancyReasons {
        DEFAULT;
    }

    public enum PendingAttributes {
        length("LENGTH"), breadth("BREADTH"), height("HEIGHT"), weight("WEIGHT");

        private String code;

        PendingAttributes(String code) {
            this.code = code;
        }

        public String getCode() {
            return code;
        }
    }

    private Integer id;
    private String  supc;
    private String  oldValue;
    private String  newValue;
    private String  attributeName;
    private Date    created;
    private Date    playDate;
    private boolean pending;
    private String  createdBy;

    /** String variable to hold name() of enum PendancyReasons **/
    private String  pendancyReason;

    public PendingProductAttributeUpdate() {

    }

    public PendingProductAttributeUpdate(String supc, String oldValue, String newValue, String attributeName, Date created, Date playDate, boolean pending, String pendancyReason,String createdBy) {
        this.supc = supc;
        this.oldValue = oldValue;
        this.newValue = newValue;
        this.attributeName = attributeName;
        this.created = created;
        this.playDate = playDate;
        this.pending = pending;
        this.pendancyReason = pendancyReason;
        this.createdBy = createdBy;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "supc", nullable = false)
    public String getSupc() {
        return supc;
    }

    public void setSupc(String supc) {
        this.supc = supc;
    }

    @Column(name = "old_value", nullable = false)
    public String getOldValue() {
        return oldValue;
    }

    public void setOldValue(String oldValue) {
        this.oldValue = oldValue;
    }

    @Column(name = "new_value", nullable = false)
    public String getNewValue() {
        return newValue;
    }

    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }

    @Column(name = "attribute_name", nullable = false)
    public String getAttributeName() {
        return attributeName;
    }

    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created", length = 19, nullable = false)
    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "play_date", length = 19, nullable = false)
    public Date getPlayDate() {
        return playDate;
    }

    public void setPlayDate(Date playDate) {
        this.playDate = playDate;
    }

    @Column(name = "pending")
    public boolean isPending() {
        return pending;
    }

    public void setPending(boolean pending) {
        this.pending = pending;
    }

    @Column(name = "pendancy_reason", nullable = false)
    public String getPendancyReason() {
        return pendancyReason;
    }

    public void setPendancyReason(String pendancyReason) {
        this.pendancyReason = pendancyReason;
    }
    
    @Column(name = "created_by", nullable = true)
    public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

    @Override
    public String toString() {
        return "PendingProductAttributeUpdate [id=" + id + ", supc=" + supc + ", oldValue=" + oldValue + ", newValue=" + newValue + ", attributeName=" + attributeName
                + ", created=" + created + ", playDate=" + playDate + ", pending=" + pending + ", createdBy=" + createdBy + ", pendancyReason=" + pendancyReason + "]";
    }
}
