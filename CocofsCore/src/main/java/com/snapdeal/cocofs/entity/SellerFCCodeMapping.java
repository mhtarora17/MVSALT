/*
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 30-Mar-2015
 *  @author shiv
 */
package com.snapdeal.cocofs.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

import com.snapdeal.base.utils.DateUtils;

@Audited
@Entity
@Table(name = "seller_fc_mapping")
public class SellerFCCodeMapping implements java.io.Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -8538072468892285739L;
    private Integer           id;
    private SellerFMMapping   sellerFMMapping;
    private String            fcCode;
    private boolean           enabled;
    private Date              created;
    private Date              lastUpdated;
    private String              updatedBy;


    public SellerFCCodeMapping() {
    }

    public SellerFCCodeMapping(SellerFMMapping mapping, String fcCode, String userEmail) {
        this.sellerFMMapping = mapping;
        this.fcCode = fcCode;

        setCreated(DateUtils.getCurrentTime());
        setLastUpdated(DateUtils.getCurrentTime());
        setEnabled(true);
        setUpdatedBy(userEmail);
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne
    @JoinColumn(name = "seller_fm_id", nullable = false, referencedColumnName = "id")
    public SellerFMMapping getSellerFMMapping() {
        return sellerFMMapping;
    }

    public void setSellerFMMapping(SellerFMMapping sellerFMMapping) {
        this.sellerFMMapping = sellerFMMapping;
    }

    @Column(name = "fc_code", nullable = false)
    public String getFcCode() {
        return fcCode;
    }

    public void setFcCode(String fcCode) {
        this.fcCode = fcCode;
    }

    @Column(name = "enabled", nullable = false)
    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created", length = 19, nullable = false)
    public Date getCreated() {
        return this.created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_updated", length = 19)
    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }


    @Column(name = "updated_by")
    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }
    
    @Override
    public String toString() {
        return "SellerFCCodeMapping [id=" + id + ", sellerFMMapping=" + sellerFMMapping + ", fcCode=" + fcCode + ", enabled=" + enabled + ", created=" + created + ", lastUpdated="
                + lastUpdated + "]";
    }

}
