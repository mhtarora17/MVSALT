/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 30-Sep-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "seller_subcat_fm_mapping")
public class SellerSubcatFMMapping extends AbstractFMMapping {

    /**
     * 
     */
    private static final long serialVersionUID = -4404159106234436374L;

    private String            subcat;

    @Column(name = "subcat")
    public String getSubcat() {
        return subcat;
    }

    public void setSubcat(String subcat) {
        this.subcat = subcat;
    }

}
