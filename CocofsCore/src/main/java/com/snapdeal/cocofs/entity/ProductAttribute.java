package com.snapdeal.cocofs.entity;

// default package
// Generated 22 Aug, 2013 2:58:51 PM by Hibernate Tools 3.4.0.CR1

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

/**
 * ProductAttribute generated by hbm2java
 */
@Entity
@Table(name = "product_attribute")
public class ProductAttribute implements java.io.Serializable {

    /**
	 * 
	 */
    private static final long            serialVersionUID = -8967966740246468859L;
    private Integer                      id;
    private String                       supc;

    private String                       attribute;
    private String                       value;
    private Boolean                      enabled;
    private String                       updatedBy;
    private String                       createdBy;
    private Date                         created;
    private Date                         lastUpdated;
    private Long                         version;
    private Set<ProductAttributeHistory> history;

    public ProductAttribute() {
        history = new HashSet<ProductAttributeHistory>();
        created = new Date();
        lastUpdated = created;
    }

    public ProductAttribute(String supc, String attribute, String value, String updatedBy, String createdBy) {
        this.history = new HashSet<ProductAttributeHistory>();
        this.supc = supc;
        this.attribute = attribute;
        this.value = value;
        this.updatedBy = updatedBy;
        this.createdBy = createdBy;
    }

    public ProductAttribute(String supc, String attribute, String value, Boolean enabled, String updatedBy, String createdBy, Date created, Date lastUpdated) {
        this.supc = supc;
        this.attribute = attribute;
        this.value = value;
        this.enabled = enabled;
        this.updatedBy = updatedBy;
        this.createdBy = createdBy;
        this.created = created;
        this.lastUpdated = lastUpdated;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "supc", nullable = false)
    public String getSupc() {
        return this.supc;
    }

    public void setSupc(String supc) {
        this.supc = supc;
    }

    @Column(name = "attribute", nullable = false)
    public String getAttribute() {
        return this.attribute;
    }

    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    @Column(name = "value", nullable = false)
    public String getValue() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Column(name = "enabled")
    public Boolean getEnabled() {
        return this.enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    @Column(name = "updated_by", nullable = false)
    public String getUpdatedBy() {
        return this.updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Override
    public String toString() {
        return "ProductAttribute [id=" + id + ", supc=" + supc + ", attribute=" + attribute + ", value=" + value + ", enabled=" + enabled + ", updatedBy=" + updatedBy
                + ", createdBy=" + createdBy + ", created=" + created + ", lastUpdated=" + lastUpdated + ", version=" + version + "]";
    }

    @Column(name = "created_by", nullable = false)
    public String getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created", length = 19)
    public Date getCreated() {
        return this.created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_updated", length = 19)
    public Date getLastUpdated() {
        return this.lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    @Version
    @Column(name = "version")
    public Long getVersion() {
        return version;
    }

    @Deprecated
    public void setVersion(Long version) {
        this.version = version;
    }

    @OneToMany(fetch = FetchType.EAGER, cascade = { CascadeType.ALL })
    @JoinColumns({ @JoinColumn(name = "attribute", referencedColumnName = "attribute"), @JoinColumn(name = "supc", referencedColumnName = "supc") })
    public Set<ProductAttributeHistory> getHistory() {
        return history;
    }

    public void setHistory(Set<ProductAttributeHistory> history) {
        this.history = history;
    }

}
