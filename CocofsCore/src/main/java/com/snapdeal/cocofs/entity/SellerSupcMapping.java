/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 13-Jan-2015
 *  @author gaurav
 */
package com.snapdeal.cocofs.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "seller_supc_mapping")
public class SellerSupcMapping extends BaseEntity{

    /**
	 * 
	 */
	private static final long serialVersionUID = 478004942088312634L;
	private String	sellerCode;
	private String supc;
	
    @Column(name = "seller_code")
    public String getSellerCode() {
        return sellerCode;
    }

    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }
    
    @Column(name = "supc")
    public String getSupc() {
        return supc;
    }

    public void setSupc(String supc) {
        this.supc = supc;
    }

	@Override
	public String toString() {
		return "SellerSupcMapping [sellerCode=" + sellerCode + ", supc=" + supc
				+ "]";
	}
}
