/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 30-Sep-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.entity;

import java.util.Collection;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Audited
@Entity
@Table(name = "seller_supc_fm_mapping")
public class SellerSupcFMMapping extends AbstractFMMapping {
    /**
     * 
     */
    private static final long            serialVersionUID = -3800927246330403680L;
    private String                       supc;
    private Set<SellerSupcFCCodeMapping> fcCenters;

    @Column(name = "supc")
    public String getSupc() {
        return supc;
    }

    public void setSupc(String supc) {
        this.supc = supc;
    }

    @OneToMany(mappedBy = "sellerSupcFMMapping", cascade = CascadeType.ALL)
    public Set<SellerSupcFCCodeMapping> getFcCenters() {
        return this.fcCenters;
    }

    public void setFcCenters(Set<SellerSupcFCCodeMapping> fcCenters) {
        this.fcCenters = fcCenters;
    }

    @Override
    public String toString() {
        return "SellerSupcFMMapping [sellerCode=" + getSellerCode() + ", supc=" + supc + ", fulfilmentModel=" + getFulfilmentModel() + ", fcCenters=" + getFCCentersStr(fcCenters)
                + ", enabled=" + isEnabled() + ", updatedBy=" + getUpdatedBy() + ", created=" + getCreated() + ", lastUpdated=" + getLastUpdated() + "]";
    }

    private String getFCCentersStr(Collection<SellerSupcFCCodeMapping> fcCenters) {

        String str = null;
        if (fcCenters != null) {
            str = "{";
            if (!fcCenters.isEmpty()) {
                for (SellerSupcFCCodeMapping fc : fcCenters) {
                    if (fc != null) {
                        str += fc.getFcCode() + "?" + fc.isEnabled() + ",";

                    }
                }
            }
            //TODO get rid of last comma
            str += "}";
        }
        return str;
    }
}
