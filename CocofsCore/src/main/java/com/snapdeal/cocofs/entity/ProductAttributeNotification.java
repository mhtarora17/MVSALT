package com.snapdeal.cocofs.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "product_attribute_notification")
public class ProductAttributeNotification implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private Integer           id;

    private String            supc;

    private String            notificationType;

    private String            currValue;

    private String            newValue;

    /**
     * Date from which the changes are getting effected, will be equal to currentTime() in case a super user creates the
     * notification
     **/
    private Date              withEffectFrom;

    private Date              created;

    /** Date on which the notification expires irrespective of the pending flag value **/
    private Date              notificationEndDate;

    /** String variable to hold UserType **/
    private String            userType;

    /** notification is visible or not **/
    private boolean           visible;

    /** this update resulted in change in fulfilment fee**/
    private boolean           ffChanged;
    
    public ProductAttributeNotification(){
        
    }

    public ProductAttributeNotification(String supc, String notificationType, String currValue, String newValue, Date withEffectFrom, Date created, 
            Date notificationEndDate, String userType, boolean visible, boolean ffChanged) {
        this.supc = supc;
        this.notificationType = notificationType;
        this.currValue = currValue;
        this.newValue = newValue;
        this.withEffectFrom = withEffectFrom;
        this.created = created;
        this.notificationEndDate = notificationEndDate;
        this.userType = userType;
        this.visible = visible;
        this.ffChanged = ffChanged;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "supc", nullable = false)
    public String getSupc() {
        return supc;
    }

    public void setSupc(String supc) {
        this.supc = supc;
    }

    

    @Column(name = "current_value", nullable = false)
    public String getCurrValue() {
        return currValue;
    }

    public void setCurrValue(String currValue) {
        this.currValue = currValue;
    }

    @Column(name = "new_value", nullable = false)
    public String getNewValue() {
        return newValue;
    }

    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }

    @Column(name = "with_effect_from", nullable = false)
    public Date getWithEffectFrom() {
        return withEffectFrom;
    }

    public void setWithEffectFrom(Date withEffectFrom) {
        this.withEffectFrom = withEffectFrom;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created", nullable = false, length = 19)
    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "notification_end_date", nullable = false, length = 19)
    public Date getNotificationEndDate() {
        return notificationEndDate;
    }

    public void setNotificationEndDate(Date notiificationEndDate) {
        this.notificationEndDate = notiificationEndDate;
    }

    @Column(name = "user_type", nullable = false)
    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    @Column(name = "notification_type", nullable = false)
    public String getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(String notificationType) {
        this.notificationType = notificationType;
    }

    @Column(name = "visible")
    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    @Column(name = "ff_changed")
    public boolean isFfChanged() {
        return ffChanged;
    }

    public void setFfChanged(boolean ffChanged) {
        this.ffChanged = ffChanged;
    }

    @Override
    public String toString() {
        return "ProductAttributeNotification [id=" + id + ", supc=" + supc + ", notificationType=" + notificationType + ", currValue=" + currValue + ", newValue=" + newValue
                + ", withEffectFrom=" + withEffectFrom + ", created=" + created + ", notificationEndDate=" + notificationEndDate + ", userType=" + userType + ", visible="
                + visible + ", ffChanged=" + ffChanged + "]";
    }

}
