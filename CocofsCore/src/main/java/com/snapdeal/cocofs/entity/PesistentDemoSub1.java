package com.snapdeal.cocofs.entity;

public class PesistentDemoSub1 {
    
    private String b1;
    
    private Double b2;
    
    private DemoTest2 dt;

    public String getB1() {
        return b1;
    }

    public void setB1(String b1) {
        this.b1 = b1;
    }

    public Double getB2() {
        return b2;
    }

    public void setB2(Double b2) {
        this.b2 = b2;
    }

    public DemoTest2 getDt() {
        return dt;
    }

    public void setDt(DemoTest2 dt) {
        this.dt = dt;
    }
    
    

}
