/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 13-Jan-2015
 *  @author gaurav
 */
package com.snapdeal.cocofs.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "forgot_password")
public class PasswordVerification extends BaseEntity{

    /**
	 * 
	 */
	private static final long serialVersionUID = -124845575488638780L;
	private String	email;
	private String verificationCode;
	private int ttl;

	@Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    @Column(name = "verification_code")
    public String getVerificationCode() {
        return verificationCode;
    }

    public void setVerificationCode(String verificationCode) {
        this.verificationCode = verificationCode;
    }
    	
    @Column(name = "ttl")
    public int getTtl() {
		return ttl;
	}

	public void setTtl(int ttl) {
		this.ttl = ttl;
	}

	@Override
	public String toString() {
		return "PasswordVerification [email=" + email + ", verificaionCode="
				+ verificationCode + ", ttl=" + ttl  + "]";
	}
}
