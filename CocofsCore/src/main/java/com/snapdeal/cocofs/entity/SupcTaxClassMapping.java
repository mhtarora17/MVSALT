/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.hibernate.envers.Audited;

/**
 * @version 1.0, 24-Dec-2015
 * @author nitish
 */
@Audited
@Entity
@Table(name = "supc_tax_class_mapping")
public class SupcTaxClassMapping extends AbstractTaxClass {

    /**
     * 
     */
    private static final long serialVersionUID = -383388017971266515L;

    private String            supc;

    private Date              lastUpdated;

    private boolean           enabled;

    private String            updatedBy;

    private Long              version;

    @Column(name = "supc", nullable = false)
    public String getSupc() {
        return supc;
    }

    public void setSupc(String supc) {
        this.supc = supc;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_updated", length = 19)
    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    @Column(name = "enabled")
    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Column(name = "updated_by")
    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Version
    @Column(name = "version")
    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "SupcTaxClassMapping [supc=" + supc + ", lastUpdated=" + lastUpdated + ", enabled=" + enabled + ", updatedBy=" + updatedBy + ", version=" + version + "]";
    }

}
