/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

/**
 *  
 *  @version     1.0, 18-Aug-2014
 *  @author ankur
 */

@Audited
@Entity
@Table(name = "seller_supc_tax_rate")
public class SellerSupcTaxRate   extends AbstractTaxRate{
    
    private static final long serialVersionUID = 3219013920131521229L;

    private String sellerCode;
    
    private String supc;

   
    @Column(name = "seller_code", nullable = false)
    public String getSellerCode() {
        return sellerCode;
    }

    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }

    @Column(name = "supc", nullable = false)
    public String getSupc() {
        return supc;
    }

    public void setSupc(String supc) {
        this.supc = supc;
    }

    @Override
    public String toString() {
        return "SellerSupcTaxRate [sellerCode=" + sellerCode + ", supc=" + supc + ", getTaxRate()=" + getTaxRate() + "]";
    }

   
}
