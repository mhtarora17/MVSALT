/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

/**
 * @version 1.0, 22-Dec-2015
 * @author nitish
 */

@MappedSuperclass
public abstract class AbstractTaxClass extends BaseEntity {

    private static final long serialVersionUID = 8397109710765172229L;

    private String            taxClass;

    @Column(name = "tax_class", nullable = false)
    public String getTaxClass() {
        return taxClass;
    }

    public void setTaxClass(String taxClass) {
        this.taxClass = taxClass;
    }

    @Override
    public String toString() {
        return "TaxClass [taxClass=" + taxClass + "]";
    }

}
