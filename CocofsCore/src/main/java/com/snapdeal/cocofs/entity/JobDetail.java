package com.snapdeal.cocofs.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "job_detail")
public class JobDetail implements Comparable<JobDetail>, java.io.Serializable {

	private static final long serialVersionUID = -5468010432172144436L;

	private Integer id;
	private String fileCode;
	private String fileName;
	private String filePath;
	private boolean toRetry;
	private JobAction action;
	private JobStatus status;
	private int priority; // higher the more priority
	private String remarks;
	private String uploadedBy;
	private boolean enabled;
	private Date lastUpdated;
	private Date created;
	private Long version;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "file_code")
	public String getFileCode() {
		return fileCode;
	}

	public void setFileCode(String fileCode) {
		this.fileCode = fileCode;
	}

	@Column(name = "file_name")
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	@Column(name = "filepath")
	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	@Column(name = "retry")
	public boolean isToRetry() {
		return toRetry;
	}

	public void setToRetry(boolean toRetry) {
		this.toRetry = toRetry;
	}

	@ManyToOne
	@JoinColumn(name = "action_code", referencedColumnName = "code", nullable = false)
	public JobAction getAction() {
		return action;
	}

	public void setAction(JobAction action) {
		this.action = action;
	}

	@ManyToOne
	@JoinColumn(name = "status_code", referencedColumnName = "code", nullable = false)
	public JobStatus getStatus() {
		return status;
	}

	public void setStatus(JobStatus status) {
		this.status = status;
	}

	@Column(name = "priority")
	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	@Column(name = "remarks")
	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Column(name = "uploaded_by")
	public String getUploadedBy() {
		return uploadedBy;
	}

	public void setUploadedBy(String uploadedBy) {
		this.uploadedBy = uploadedBy;
	}

	@Column(name = "enabled")
	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	@Column(name = "last_updated")
	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	@Column(name = "created")
	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	@Version
	@Column(name = "version")
	public Long getVersion() {
		return version;
	}

	@Deprecated
	public void setVersion(Long version) {
		this.version = version;
	}

	@Override
	public int compareTo(JobDetail jd) {
		if (this.getPriority() > jd.getPriority()) {
			return 1;
		} else if (this.getPriority() == jd.getPriority()) {
			return this.created.compareTo(jd.created);
		} else {
			return -1;
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((fileCode == null) ? 0 : fileCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JobDetail other = (JobDetail) obj;
		if (fileCode == null) {
			if (other.fileCode != null)
				return false;
		} else if (!fileCode.equals(other.fileCode))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "JobDetail [id=" + id + ", fileCode=" + fileCode + ", fileName="
				+ fileName + ", filePath=" + filePath + ", toRetry=" + toRetry
				+ ", action=" + action + ", status=" + status + ", priority="
				+ priority + ", remarks=" + remarks + ", uploadedBy="
				+ uploadedBy + ", enabled=" + enabled + ", lastUpdated="
				+ lastUpdated + ", created=" + created + ", version=" + version
				+ "]";
	}

}
