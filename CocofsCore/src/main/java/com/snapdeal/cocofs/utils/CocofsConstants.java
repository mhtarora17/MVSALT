/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.utils;

/**
 * @version 1.0, 09-Sep-2015
 * @author ankur
 */
public interface CocofsConstants {
    public static final String COCOFS_PROFILE_NAME  = "COCOFS";
}
