package com.snapdeal.cocofs.utils;

import java.math.BigDecimal;

import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;



public class UtilHelper {

    private static IDependency       dep = new Dependency();
    
    public static Double getEffectiveWeight(Double deadWeight, Double volumetricWeight) {
        return (deadWeight > volumetricWeight) ? deadWeight : volumetricWeight;
    }

    public static double getVolumetricWeightFromLBH(double l, double b, double h) {
        double denominator = dep.getDoubleScalar(Property.VOLUMETRIC_WEIGHT_FORMULA_DENOMINATOR);
        return (l *  b * h) / denominator;
    }

    public static double getPerimterFromLBH(double l, double b, double h) {
        double multiplier = ConfigUtils.getDoubleScalar(Property.PERIMETER_MULTIPLIER_VALUE);
        BigDecimal length = BigDecimal.valueOf(l);
        BigDecimal breadth = BigDecimal.valueOf(b);
        BigDecimal height = BigDecimal.valueOf(h);
        BigDecimal perimeter = (length.add(breadth).add(height)).multiply(BigDecimal.valueOf(multiplier));
        return perimeter.doubleValue();
    }
    
    public interface IDependency {
        double getDoubleScalar(Property property);
    }

    static class Dependency implements IDependency {
        @Override
        public double getDoubleScalar(Property property) {
            return ConfigUtils.getDoubleScalar(property);
        }
    }

}
