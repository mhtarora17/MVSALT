/*
 *  Copyright 2012 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 09-Oct-2012
 *  @author prateek
 */
package com.snapdeal.cocofs.utils;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import com.aerospike.client.Log;
import com.snapdeal.base.fileutils.ExcelSheetParser;
import com.snapdeal.base.fileutils.FileParser;
import com.snapdeal.base.fileutils.Row;
import com.snapdeal.base.fileutils.SDFileUploadUtils;
import com.sun.media.sound.InvalidDataException;

public class ExcelReader {

    public enum SuccessMessage {
        SUCCESS
    }

    private static final Logger LOG = LoggerFactory.getLogger(ExcelReader.class);
    
    private static final String[] allowedDateFormats = new String []{"yyyy/MM/dd","yyyy/MM/dd HH:mm:ss"};

    public static <T> List<T> readFile(MultipartFile file, String uploadPath, Class<T> dtoClass) throws IOException, ClassNotFoundException, InvalidDataException {
        return readFile(file, uploadPath, dtoClass, false);
    }

    public static <T> List<T> readFile(MultipartFile file, String uploadPath, Class<T> dtoClass, boolean emptyFieldAllowed) throws IOException, ClassNotFoundException,
            InvalidDataException {
        if (file != null) {
            try {
                LOG.info("uploading file {}", file.getOriginalFilename());
                SDFileUploadUtils.upload(file.getInputStream(), file.getContentType(), uploadPath);
            } catch (IOException e) {
                LOG.error("FAILED to upload file {} exception {} " + file.getOriginalFilename(), e);
            }
            return readFile(uploadPath, dtoClass, emptyFieldAllowed);
        } else {
            return new ArrayList<T>();
        }
    }

    public static <T> List<T> readFile(String uploadPath, Class<T> dtoClass) throws IOException, ClassNotFoundException, InvalidDataException {
        return readFile(uploadPath, dtoClass, false);
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static <T> List<T> readFile(String uploadPath, Class dtoClass, boolean emptyFieldAllowed) throws IOException, ClassNotFoundException, InvalidDataException {
        return readFile(uploadPath, dtoClass, emptyFieldAllowed, 0, false, false);

    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static <T> List<T> readFile(String uploadPath, Class dtoClass, boolean emptyFieldAllowed, int maxSizeAllowed, boolean checkMaxSize, boolean checkValidDataFormat) throws IOException,
            ClassNotFoundException, InvalidDataException {
    	
    	boolean inValidDataFormat=true;
        StringBuilder sb = new StringBuilder();

        List<T> dataList = new ArrayList<T>();
        if (StringUtils.isNotEmpty(uploadPath)) {
            FileParser sheetParser = new ExcelSheetParser(uploadPath, true);
            int noOfColoums = 0;
            Field[] fields = null;

            Constructor[] ctors = dtoClass.getDeclaredConstructors();
            Constructor ctor = null;
            for (int i = 0; i < ctors.length; i++) {
                ctor = ctors[i];
                ctor.setAccessible(true);
                if (ctor.getGenericParameterTypes().length == 0) {
                    break;
                }
            }

            T c = null;
            Iterator<Row> iterator = sheetParser.parse();
            // validate the incoming xl sheet first
            int rowCountWithEmptyFieldValues = 0;
            int rowCount = 0;
            while (iterator.hasNext()) {
                rowCount++;
                if (checkMaxSize && rowCount > maxSizeAllowed) {
                    throw new InvalidDataException("The file uploaded has greater size than allowed value ");
                }

                Row row = iterator.next();
                try {

                    c = (T) ctor.newInstance();
                    fields = (Field[]) (dtoClass.isAnnotationPresent(SubClass.class) ? ArrayUtils.addAll(dtoClass.getDeclaredFields(), dtoClass.getSuperclass().getDeclaredFields())
                            : dtoClass.getDeclaredFields());
                    noOfColoums = fields.length;
                    if (null != row.getColumnValue(noOfColoums)) {
                        throw new InvalidDataException("Column count incorrect, extra columns may have been added");
                    }
                    int nullCellCount = 0;
                    for (int i = 0; i < noOfColoums; i++) {
                        fields[i].setAccessible(true);
                        String coloumnValue = null;
                        Object value = null;
                        boolean validRow = true;

                        if (nullCellCount < noOfColoums) {
                            coloumnValue = row.getColumnValue(fields[i].getName().toUpperCase());

                            if (StringUtils.isEmpty(coloumnValue)) {
                                Log.info("field value is empty");
                                nullCellCount++;
                            } else {
                                if (fields[i].getType().isAssignableFrom(Integer.class)) {
                                    value = (coloumnValue == null ? null : Integer.parseInt(coloumnValue));
                                } else if (fields[i].getType().isAssignableFrom(Double.class)) {
                                    value = (coloumnValue == null ? null : Double.parseDouble(coloumnValue));
                                } else if (fields[i].getType().isAssignableFrom(Date.class)) {
                                    value = (coloumnValue == null ? null : org.apache.commons.lang.time.DateUtils.parseDate(coloumnValue, allowedDateFormats));
                                } else if (fields[i].getType().isAssignableFrom(String.class)) {
                                    value = coloumnValue;
                                } else if (fields[i].getType().isAssignableFrom(Boolean.class)) {
                                    value = Boolean.valueOf(coloumnValue);
                                } else {
                                    validRow = false;
                                    try {
                                        c.getClass().getField("validRow").setBoolean(c, validRow);
                                    } catch (SecurityException e) {
                                    	inValidDataFormat=false;
                                    	sb.append("security exception from row : " + rowCount + "<br>");
                                        LOG.error("security exception {} ", e);
                                        e.printStackTrace();
                                    } catch (NoSuchFieldException e) {
                                    	inValidDataFormat=false;
                                    	sb.append("invalid field type " + fields[i].getType().getName() + " field name  " + fields[i].getName() +"from row : " + rowCount + "<br>");
                                    	LOG.error("exception field type " + fields[i].getType().getName() + " field name  " + fields[i].getName(), e);
                                    }
                                }
                                fields[i].set(c, value);
                            }
                        } else {
                            throw new InvalidDataException();
                        }
                    }
                    if (nullCellCount != noOfColoums) {
                        dataList.add(c);
                    }
                    if (!emptyFieldAllowed && nullCellCount > 0) {
                        if (nullCellCount == noOfColoums) {
                            rowCountWithEmptyFieldValues++;
                        } else {
                            throw new InvalidDataException("Some required fields have empty values");
                        }
                    } else if (emptyFieldAllowed && nullCellCount == noOfColoums) {
                        rowCountWithEmptyFieldValues++;
                    }
                } catch (IllegalArgumentException e) {
                	inValidDataFormat=false;
                    sb.append("invalid data from row : " + rowCount + "<br>");
                    LOG.error("exception {} ", e);
                } catch (InstantiationException e) {
                	inValidDataFormat=false;
                    sb.append("invalid data from row : " + rowCount + "<br>");
                    LOG.error("exception {} ", e);
                } catch (IllegalAccessException e) {
                	inValidDataFormat=false;
                    sb.append("invalid data from row : " + rowCount + "<br>");
                    LOG.error("exception {} ", e);
                } catch (InvocationTargetException e) {
                	inValidDataFormat=false;
                    sb.append("invalid data from row : " + rowCount + "<br>");
                    LOG.error("exception {} ", e);
                }catch(ParseException e){
                    inValidDataFormat=false;
                    LOG.error("exception {} ", e);
                }
            }
            if (rowCountWithEmptyFieldValues == rowCount && rowCount != 0) {
                throw new InvalidDataException("All rows have empty field values ");
            }
        }
        if(checkValidDataFormat == false){
        	return dataList;
        }
        else{
        	if(inValidDataFormat ==true){
        		return dataList;
        	}
        	else{
        	    throw new InvalidDataException(sb.toString());
        	}
        }
    }

    /**
     * @param file
     * @return
     */
    public static String validateInputFile(MultipartFile file) {
        String response = SuccessMessage.SUCCESS.name();
        if (file == null || file.isEmpty()) {
            response = "Empty or No file found.";
            return response;
        }
        if ("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet".equals(file.getContentType())) {
            response = "Please upload a file generated in Excel 2003 or lower version. Excel 2007 and above files are not supported.";
            return response;
        }
        if (!"application/vnd.ms-excel".equals(file.getContentType()) && !"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet".equals(file.getContentType())) {
            response = "Please upload a valid xls (2003 Excel) file only.";
            return response;
        }
        return response;
    }

    public static boolean checkSizeLimitation(MultipartFile file, Integer maxSize) {
        FileParser sheetParser = new ExcelSheetParser(file.getOriginalFilename(), true);
        int sizeCount = 0;
        Iterator<Row> iterator = sheetParser.parse();

        while (iterator.hasNext()) {
            sizeCount++;
            if (sizeCount > maxSize) {
                return false;
            }
        }

        return true;
    }
}
