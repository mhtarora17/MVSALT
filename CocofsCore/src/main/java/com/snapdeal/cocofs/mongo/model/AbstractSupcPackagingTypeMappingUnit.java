/**
 *  Copyright 2016 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.mongo.model;


import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

/**
 *  
 *  @version     1.0, 04-Jan-2016
 *  @author vikassharma03AbstractSupcPackagingTypeMappingUnit
 */
public class AbstractSupcPackagingTypeMappingUnit {
    
    @Id
    private ObjectId objectId;

    private boolean  enabled;
    
    private Date     created;

    private Date     updated;

    public ObjectId getObjectId() {
        return objectId;
    }

    public void setObjectId(ObjectId objectId) {
        this.objectId = objectId;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    @Override
    public String toString() {
        return "AbstractSupcPackagingTypeMappingUnit [objectId=" + objectId + ", enabled=" + enabled + ", created=" + created + ", updated=" + updated + "]";
    }

}
