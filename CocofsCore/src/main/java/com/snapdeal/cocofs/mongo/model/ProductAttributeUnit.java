/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 23-Aug-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.mongo.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class ProductAttributeUnit {

    public static enum PAUFieldNames {
        ID("id"), SUPC("supc"), SERIALIZED("serialized"), FRAGILE("fragile"), LIQUID("liquid"), PRODUCT_PARTS("productParts"), HAZMAT("hazMat"), WEIGHT(
                "weight"), SYSTEMWEIGHTCAPTURED("systemWeightCaptured"), WOODEN_PACKAGING("woodenPackaging"), DANGEROUS_GOODS_TYPE("dangerousGoodsType"), SERIALIZED_TYPE(
                        "serializedType"), PACKAGING_TYPE("packagingType"), PRIMARY_LENGTH("primaryLength"), PRIMARY_BREADTH("primaryBreadth"), PRIMARY_HEIGHT("primaryHeight");

        private String code;

        PAUFieldNames(String code) {
            this.code = code;
        }

        public String getCode() {
            return code;
        }
    }

    @Id
    private ObjectId objectId;

    private String   supc;

    private boolean  serialized;

    private boolean  fragile;

    private boolean  liquid;

    private Integer  productParts;

    private boolean  hazMat;

    private Double   weight;

    private Double   length;

    private Double   breadth;

    private Double   height;

    private Double   primaryLength;

    private Double   primaryBreadth;

    private Double   primaryHeight;

    private boolean  systemWeightCaptured;

    private boolean  woodenPackaging;

    private String   dangerousGoodsType;

    private String   serializedType;

    private String   packagingType;

    public boolean isSerialized() {
        return serialized;
    }

    public void setSerialized(boolean serialized) {
        this.serialized = serialized;
    }

    public boolean isFragile() {
        return fragile;
    }

    public void setFragile(boolean fragile) {
        this.fragile = fragile;
    }

    public boolean isLiquid() {
        return liquid;
    }

    public void setLiquid(boolean liquid) {
        this.liquid = liquid;
    }

    public Integer getProductParts() {
        return productParts;
    }

    public void setProductParts(Integer productParts) {
        this.productParts = productParts;
    }

    public boolean isHazMat() {
        return hazMat;
    }

    public void setHazMat(boolean hazMat) {
        this.hazMat = hazMat;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Double getLength() {
        return length;
    }

    public void setLength(Double length) {
        this.length = length;
    }

    public Double getBreadth() {
        return breadth;
    }

    public void setBreadth(Double breadth) {
        this.breadth = breadth;
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public Double getPrimaryLength() {
        return primaryLength;
    }

    public void setPrimaryLength(Double primaryLength) {
        this.primaryLength = primaryLength;
    }

    public Double getPrimaryBreadth() {
        return primaryBreadth;
    }

    public void setPrimaryBreadth(Double primaryBreadth) {
        this.primaryBreadth = primaryBreadth;
    }

    public Double getPrimaryHeight() {
        return primaryHeight;
    }

    public void setPrimaryHeight(Double primaryHeight) {
        this.primaryHeight = primaryHeight;
    }

    public String getSupc() {
        return supc;
    }

    public void setSupc(String supc) {
        this.supc = supc;
    }

    public ObjectId getObjectId() {
        return objectId;
    }

    public void setObjectId(ObjectId objectId) {
        this.objectId = objectId;
    }

    public void setSystemWeightCaptured(boolean systemWeightCaptured) {
        this.systemWeightCaptured = systemWeightCaptured;
    }

    public boolean isSystemWeightCaptured() {
        return systemWeightCaptured;
    }

    public boolean isWoodenPackaging() {
        return woodenPackaging;
    }

    public void setWoodenPackaging(boolean woodenPackaging) {
        this.woodenPackaging = woodenPackaging;
    }

    public String getDangerousGoodsType() {
        return dangerousGoodsType;
    }

    public void setDangerousGoodsType(String dangerousGoodsType) {
        this.dangerousGoodsType = dangerousGoodsType;
    }

    public String getSerializedType() {
        return serializedType;
    }

    public void setSerializedType(String serializedType) {
        this.serializedType = serializedType;
    }

    public String getPackagingType() {
        return packagingType;
    }

    public void setPackagingType(String packagingType) {
        this.packagingType = packagingType;
    }

    @Override
    public String toString() {
        return "ProductAttributeUnit [objectId=" + objectId + ", supc=" + supc + ", serialized=" + serialized + ", fragile=" + fragile + ", liquid=" + liquid + ", productParts="
                + productParts + ", hazMat=" + hazMat + ", weight=" + weight + ", length=" + length + ", breadth=" + breadth + ", height=" + height + ",primaryLength="
                + primaryLength + ", primaryBreadth=" + primaryBreadth + ", primaryHeight=" + primaryHeight + ", systemWeightCaptured=" + systemWeightCaptured
                + ", woodenPackaging=" + woodenPackaging + ", dangerousGoodsType=" + dangerousGoodsType + ", serializedType=" + serializedType + ", packagingType=" + packagingType
                + "]";
    }

}
