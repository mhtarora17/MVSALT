/**
 *  Copyright 2016 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.mongo.model;


import org.springframework.data.mongodb.core.mapping.Document;

/**
 *  
 *  @version     1.0, 04-Jan-2016
 *  @author vikassharma03
 */

@Document
public class SupcPackagingTypeMappingUnit extends AbstractSupcPackagingTypeMappingUnit {
    
    public static enum SupcPtFieldNames {
        STORE_CODE("storeCode"), SUPC("supc") , CREATED("created"), UPDATED("updated"), ENABLED("enabled"),PACKAGING_TYPE("packagingType");

        private String code;
        
        SupcPtFieldNames(String code) {
            this.code = code;
        }

        public String getCode() {
            return code;
        }

    }
    
    
    private String              supc;
    
    private String              storeCode;
    
    private String              packagingType;


    public String getSupc() {
        return supc;
    }

    public void setSupc(String supc) {
        this.supc = supc;
    }

    public String getStoreCode() {
        return storeCode;
    }

    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }

    public String getPackagingType() {
        return packagingType;
    }

    public void setPackagingType(String packagingType) {
        this.packagingType = packagingType;
    }



    @Override
    public String toString() {
        return "SupcPackagingTypeMappingUnit [supc=" + supc + ", storeCode=" + storeCode + ", packagingType=" + packagingType + ",   enabled=" + super.isEnabled() + ", created=" + super.getCreated() + ", updated=" + super.getUpdated() + " ]";
    }
    

}
