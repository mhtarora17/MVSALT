/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 09-Oct-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.mongo.model;

import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

public abstract class AbstractFMMappingUnit {

    @Id
    private ObjectId objectId;

    private String   sellerCode;

    private String   fulfilmentModel;

    private boolean  enabled;

    private Date     created;

    private Date     updated;

    public ObjectId getObjectId() {
        return objectId;
    }

    public void setObjectId(ObjectId objectId) {
        this.objectId = objectId;
    }

    public String getSellerCode() {
        return sellerCode;
    }

    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }

    public String getFulfilmentModel() {
        return fulfilmentModel;
    }

    public void setFulfilmentModel(String fulfilmentModel) {
        this.fulfilmentModel = fulfilmentModel;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public String toString() {
        return "AbstractFMMappingUnit [objectId=" + objectId + ", sellerCode=" + sellerCode + ", fulfilmentModel=" + fulfilmentModel + ", enabled=" + enabled + ", created="
                + created + ", updated=" + updated + "]";
    }

}
