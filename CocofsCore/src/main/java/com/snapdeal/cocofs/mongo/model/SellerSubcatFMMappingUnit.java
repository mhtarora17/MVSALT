/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 30-Sep-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.mongo.model;

import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class SellerSubcatFMMappingUnit extends AbstractFMMappingUnit {

    public static enum SSCFMMUFieldNames {
        SUBCAT("subcat"), FULFILMENT_MODEL("fulfilmentModel"), SELLER_CODE("sellerCode"), CREATED("created"), UPDATED("updated"), ENABLED("enabled");

        private String code;

        SSCFMMUFieldNames(String code) {
            this.code = code;
        }

        public String getCode() {
            return code;
        }
    }

    private String subcat;

    public String getSubcat() {
        return subcat;
    }

    public void setSubcat(String subcat) {
        this.subcat = subcat;
    }

    @Override
    public String toString() {
        return "SellerSubcatFMMappingUnit [subcat=" + subcat + ", " + super.toString() + "]";
    }

}
