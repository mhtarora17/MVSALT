package com.snapdeal.cocofs.mongo.model;

import java.util.Date;

import org.springframework.data.annotation.Id;

public class NotificationSubUnit {

    @Id
    private Object id;

    private String currValue;

    private String newValue;

    private Date   withEffectFrom;

    private String notificationType;
    
    public NotificationSubUnit(){
        
    }

    public NotificationSubUnit(String currValue, String newValue, Date withEffectFrom, String notificationType) {

        this.currValue = currValue;
        this.newValue = newValue;
        this.withEffectFrom = withEffectFrom;
        this.notificationType = notificationType;
    }

    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public String getCurrValue() {
        return currValue;
    }

    public void setCurrValue(String currValue) {
        this.currValue = currValue;
    }

    public String getNewValue() {
        return newValue;
    }

    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }

    public Date getWithEffectFrom() {
        return withEffectFrom;
    }

    public void setWithEffectFrom(Date withEffectFrom) {
        this.withEffectFrom = withEffectFrom;
    }

    public String getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(String notificationType) {
        this.notificationType = notificationType;
    }

    @Override
    public String toString() {
        return "NotificationSubUnit [id=" + id + ", currValue=" + currValue + ", newValue=" + newValue + ", withEffectFrom=" + withEffectFrom + ", notificationType="
                + notificationType + "]";
    }

}
