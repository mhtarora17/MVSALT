/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 16, 2013
 *  @author himanshu
 */
package com.snapdeal.cocofs.mongo.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class CategoryShippingMode {
    
    public static enum CSMFieldNames {
        SUBCAT("categoryUrl"), MODE("shippingMode");
        private String code;

        private CSMFieldNames(String code) {
            this.code = code;
        }
        public String getCode() {
            return code;
        }

        
        
    }
    
    @Id
    private ObjectId objectId;
    
    private String categoryUrl;
    private String shippingMode;
    
    
    public ObjectId getObjectId() {
        return objectId;
    }
    public void setObjectId(ObjectId objectId) {
        this.objectId = objectId;
    }
    
    
    public String getCategoryUrl() {
        return categoryUrl;
    }
    public void setCategoryUrl(String categoryUrl) {
        this.categoryUrl = categoryUrl;
    }
    public String getShippingMode() {
        return shippingMode;
    }
    public void setShippingMode(String shippingMode) {
        this.shippingMode = shippingMode;
    }
    @Override
    public String toString() {
        return "CategoryShippingMode [objectId=" + objectId + ", categoryUrl=" + categoryUrl + ", shippingMode=" + shippingMode + "]";
    }
    

    
    
    

}
