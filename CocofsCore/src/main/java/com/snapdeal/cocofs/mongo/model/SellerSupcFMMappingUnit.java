/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 30-Sep-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.mongo.model;

import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class SellerSupcFMMappingUnit extends AbstractFMMappingUnit {
    public static enum SSFMMUFieldNames {
        SUPC("supc"), SELLER_CODE("sellerCode"), FULFILMENT_MODEL("fulfilmentModel"), CREATED("created"), UPDATED("updated"), ENABLED("enabled");

        private String code;

        SSFMMUFieldNames(String code) {
            this.code = code;
        }

        public String getCode() {
            return code;
        }
    }

    private String supc;

    public String getSupc() {
        return supc;
    }

    public void setSupc(String supc) {
        this.supc = supc;
    }

    @Override
    public String toString() {
        return "SellerSupcFMMappingUnit [supc=" + supc + "," + super.toString() + "]";
    }

}
