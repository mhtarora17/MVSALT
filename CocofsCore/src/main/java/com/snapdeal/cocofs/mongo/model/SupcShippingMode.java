/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 16, 2013
 *  @author himanshu
 */
package com.snapdeal.cocofs.mongo.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class SupcShippingMode {
    
    public static enum SSMFieldNames {
        SUPC("supc"), MODE("shippingMode");
        private String code;

        private SSMFieldNames(String code) {
            this.code = code;
        }
        public String getCode() {
            return code;
        }
    }
    
    @Id
    private ObjectId objectId;
    
    private String supc;
    private String shippingMode;
    public ObjectId getObjectId() {
        return objectId;
    }
    public void setObjectId(ObjectId objectId) {
        this.objectId = objectId;
    }
    public String getSupc() {
        return supc;
    }
    public void setSupc(String supc) {
        this.supc = supc;
    }
    public String getShippingMode() {
        return shippingMode;
    }
    public void setShippingMode(String shippingMode) {
        this.shippingMode = shippingMode;
    }
    @Override
    public String toString() {
        return "SupcShippingMode [objectId=" + objectId + ", supc=" + supc + ", shippingMode=" + shippingMode + "]";
    }
    
    

}
