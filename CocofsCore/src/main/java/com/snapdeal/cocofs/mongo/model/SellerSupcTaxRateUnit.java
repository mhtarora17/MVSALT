/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.mongo.model;

/**
 *  
 *  @version     1.0, 18-Aug-2014
 *  @author ankur
 */
public class SellerSupcTaxRateUnit  extends AbstractTaxRateUnit{
   
    public static enum SSTRFieldNames {
        SELLER_CODE("sellerCode"), SUPC("supc") , CREATED("created"), UPDATED("updated"), ENABLED("enabled"), TAX_TYPE("taxType");

        private String code;

        SSTRFieldNames(String code) {
            this.code = code;
        }

        public String getCode() {
            return code;
        }
    }
    private String sellerCode;

    private String supc;

    public String getSellerCode() {
        return sellerCode;
    }

    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }

    public String getSupc() {
        return supc;
    }

    public void setSupc(String supc) {
        this.supc = supc;
    }

    @Override
    public String toString() {
        return "SellerSupcTaxRateUnit [sellerCode=" + sellerCode + ", supc=" + supc + ", getTaxRate()=" + getTaxRate() +  super.toString() +"]";
    }

    
}
