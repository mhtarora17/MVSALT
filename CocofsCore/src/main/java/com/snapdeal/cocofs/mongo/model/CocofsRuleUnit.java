/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 23-Aug-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.mongo.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class CocofsRuleUnit {

    public enum CocofsRuleUnitFieldName {
        CATEGORY_URL("categoryUrl"), TYPE("type"),  PARAM_VALUE("paramValue"), RULE_CODE("ruleCode");
        private String name;
        private CocofsRuleUnitFieldName(String name ) {
            this.name = name;
        }
        public String getName() {
            return name;
        }
        
    }
    
    public static enum CocofsRuleUnitType {
        GIFTWRAP_CATEGORY("giftwrap-categorywise");

        private String code;

        CocofsRuleUnitType(String code) {
            this.code = code;
        }

        public String getCode() {
            return code;
        }

    };
    private Integer            id;

    private String             type;

    private String             name;
    
    private String             paramValue; 

    private Date               startDate;

    private Date               endDate;

    private boolean            enabled;

    private Integer            priority;

    private List<CriteriaUnit> criteria = new ArrayList<CriteriaUnit>(0);

    private Date               created;

    private Date               updated;

    private String             categoryUrl;
    
    private String             ruleCode;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    
    public String getParamValue() {
        return paramValue;
    }

    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public List<CriteriaUnit> getCriteria() {
        return criteria;
    }

    public void setCriteria(List<CriteriaUnit> criteria) {
        this.criteria = criteria;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public String getCategoryUrl() {
        return categoryUrl;
    }

    public void setCategoryUrl(String categoryUrl) {
        this.categoryUrl = categoryUrl;
    }

    
    public String getRuleCode() {
        return ruleCode;
    }

    public void setRuleCode(String ruleCode) {
        this.ruleCode = ruleCode;
    }

    @Override
    public String toString() {
        return "CocofsRuleUnit [id=" + id + ", type=" + type + ", name=" + name + ", startDate=" + startDate + ", endDate=" + endDate + ", enabled=" + enabled + ", priority="
                + priority + ", criteria=" + criteria + ", created=" + created + ", updated=" + updated + ", categoryUrl=" + categoryUrl + ", ruleCode=" + ruleCode + "]";
    }

   
    

}
