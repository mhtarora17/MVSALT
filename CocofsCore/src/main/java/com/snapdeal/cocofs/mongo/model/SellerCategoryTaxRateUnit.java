/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.mongo.model;

import org.springframework.data.mongodb.core.mapping.Document;

/**
 *  
 *  @version     1.0, 18-Aug-2014
 *  @author ankur
 */

@Document
public class SellerCategoryTaxRateUnit extends AbstractTaxRateUnit{
    public static enum SCTRFieldNames {
        SELLER_CODE("sellerCode"), CATEGORY_URL("categoryUrl"), CREATED("created"), UPDATED("updated"), ENABLED("enabled"), TAX_TYPE("taxType");

        private String code;

        SCTRFieldNames(String code) {
            this.code = code;
        }

        public String getCode() {
            return code;
        }
    }
    private String sellerCode;

    private String categoryUrl;

    public String getSellerCode() {
        return sellerCode;
    }

    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }

    public String getCategoryUrl() {
        return categoryUrl;
    }

    public void setCategoryUrl(String categoryUrl) {
        this.categoryUrl = categoryUrl;
    }

    @Override
    public String toString() {
        return "SellerCategoryTaxRateUnit [sellerCode=" + sellerCode + ", categoryUrl=" + categoryUrl + ", getTaxRate()=" + getTaxRate() + "]";
    }

   
    
}
