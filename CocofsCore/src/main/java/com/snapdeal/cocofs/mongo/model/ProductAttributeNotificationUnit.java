package com.snapdeal.cocofs.mongo.model;

import java.util.Date;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class ProductAttributeNotificationUnit {

    public static enum PANUFieldNames {
        SUPC("supc"), ATTRIBUTE_NAME("attributeName"), CREATED("created"), NOTIFICATION_END_DATE("notificationEndDate"), SELLERS("sellers"), 
        WITH_EFFECT_FROM("withEffectFrom"), VISIBLE("visible");

        private final String code;

        PANUFieldNames(String code) {
            this.code = code;
        }

        public String getCode() {
            return code;
        }
    }

    @Id
    private ObjectId                    id;

    private String                    supc;

    private Date                      created;

    private Date                      notificationEndDate;

    private List<String>              sellers;

    private Date                      withEffectFrom;

    private List<NotificationSubUnit> subUnits;
    
    private boolean                   visible;

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getSupc() {
        return supc;
    }

    public void setSupc(String supc) {
        this.supc = supc;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getNotificationEndDate() {
        return notificationEndDate;
    }

    public void setNotificationEndDate(Date notificationEndDate) {
        this.notificationEndDate = notificationEndDate;
    }

    public List<String> getSellers() {
        return sellers;
    }

    public void setSellers(List<String> sellers) {
        this.sellers = sellers;
    }

    public List<NotificationSubUnit> getSubUnits() {
        return subUnits;
    }

    public void setSubUnits(List<NotificationSubUnit> subUnits) {
        this.subUnits = subUnits;
    }

    public Date getWithEffectFrom() {
        return withEffectFrom;
    }

    public void setWithEffectFrom(Date withEffectFrom) {
        this.withEffectFrom = withEffectFrom;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    @Override
    public String toString() {
        return "ProductAttributeNotificationUnit [id=" + id + ", supc=" + supc + ", created=" + created + ", notificationEndDate=" + notificationEndDate + ", sellers=" + sellers
                + ", withEffectFrom=" + withEffectFrom + ", subUnits=" + subUnits + ", visible=" + visible + "]";
    }

}
