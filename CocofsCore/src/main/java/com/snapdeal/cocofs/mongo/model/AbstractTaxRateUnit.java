/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.mongo.model;


import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

/**
 *  
 *  @version     1.0, 18-Aug-2014
 *  @author ankur
 */
public class AbstractTaxRateUnit {
    
    @Id
    private ObjectId objectId;

    private Double   taxRate;

    private boolean  enabled;
    
    private String   taxType;

    private Date     created;

    private Date     updated;

    public ObjectId getObjectId() {
        return objectId;
    }

    public void setObjectId(ObjectId objectId) {
        this.objectId = objectId;
    }

    public Double getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(Double taxRate) {
        this.taxRate = taxRate;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    

    public String getTaxType() {
        return taxType;
    }

    public void setTaxType(String taxType) {
        this.taxType = taxType;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    @Override
    public String toString() {
        return "AbstractTaxRateUnit [objectId=" + objectId + ", taxRate=" + taxRate + ", enabled=" + enabled + ", taxType=" + taxType + ", created=" + created + ", updated="
                + updated + "]";
    }

   
    
}
