/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.mongo.model;

import org.springframework.data.mongodb.core.mapping.Document;

/**
 *  
 *  @version     1.0, 18-Aug-2014
 *  @author ankur
 */

@Document
public class StateCategoryPriceTaxRateUnit extends AbstractTaxRateUnit{
    
    public static enum SCPTRFieldNames {
        STATE("state"), CATEGORY_URL("categoryUrl"), CREATED("created"), UPDATED("updated"), ENABLED("enabled"), TAX_TYPE("taxType");

        private String code;

        SCPTRFieldNames(String code) {
            this.code = code;
        }

        public String getCode() {
            return code;
        }
    }

    private String state;

    private String categoryUrl;

    private Double lowerPriceLimit;

    private Double upperPriceLimit;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCategoryUrl() {
        return categoryUrl;
    }

    public void setCategoryUrl(String categoryUrl) {
        this.categoryUrl = categoryUrl;
    }

    public Double getLowerPriceLimit() {
        return lowerPriceLimit;
    }

    public void setLowerPriceLimit(Double lowerPriceLimit) {
        this.lowerPriceLimit = lowerPriceLimit;
    }

    public Double getUpperPriceLimit() {
        return upperPriceLimit;
    }

    public void setUpperPriceLimit(Double upperPriceLimit) {
        this.upperPriceLimit = upperPriceLimit;
    }

    @Override
    public String toString() {
        return "StateCategoryPriceTaxRateUnit [state=" + state + ", categoryUrl=" + categoryUrl + ", lowerPriceLimit=" + lowerPriceLimit + ", upperPriceLimit=" + upperPriceLimit
                + ", getTaxRate()=" + getTaxRate() + super.toString() +"]";
    }

    
}
