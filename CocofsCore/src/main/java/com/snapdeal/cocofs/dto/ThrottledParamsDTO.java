/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 23-Oct-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.dto;

public class ThrottledParamsDTO {

    private Integer durationTime;

    private Long    callsAllowedDuringDay;

    private Long    callsAllowedAtNight;

    private String  nightStartTime;

    private String  nightEndTime;

    public Integer getDurationTime() {
        return durationTime;
    }

    public void setDurationTime(Integer durationTime) {
        this.durationTime = durationTime;
    }

    public Long getCallsAllowedDuringDay() {
        return callsAllowedDuringDay;
    }

    public void setCallsAllowedDuringDay(Long callsAllowedDuringDay) {
        this.callsAllowedDuringDay = callsAllowedDuringDay;
    }

    public Long getCallsAllowedAtNight() {
        return callsAllowedAtNight;
    }

    public void setCallsAllowedAtNight(Long callsAllowedAtNight) {
        this.callsAllowedAtNight = callsAllowedAtNight;
    }

    public String getNightStartTime() {
        return nightStartTime;
    }

    public void setNightStartTime(String nightStartTime) {
        this.nightStartTime = nightStartTime;
    }

    public String getNightEndTime() {
        return nightEndTime;
    }

    public void setNightEndTime(String nightEndTime) {
        this.nightEndTime = nightEndTime;
    }

}
