/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.dto;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *  
 *  @version     1.0, 19-Dec-2014
 *  @author ankur
 */

@XmlRootElement

public class SearchEventDTO {

    private long sellerUpdateCount;
    
    private long sellerSupcUpdateCount;
    
    private String message;

    public SearchEventDTO(){
        
    }
    public long getSellerUpdateCount() {
        return sellerUpdateCount;
    }

    public void setSellerUpdateCount(long sellerUpdateCount) {
        this.sellerUpdateCount = sellerUpdateCount;
    }

    public long getSellerSupcUpdateCount() {
        return sellerSupcUpdateCount;
    }

    public void setSellerSupcUpdateCount(long sellerSupcUpdateCount) {
        this.sellerSupcUpdateCount = sellerSupcUpdateCount;
    }
    
    

    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
    @Override
    public String toString() {
        return "SearchEventDTO [sellerUpdateCount=" + sellerUpdateCount + ", sellerSupcUpdateCount=" + sellerSupcUpdateCount + "]";
    }
    
    
    
}
