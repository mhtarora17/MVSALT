/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 23-Oct-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.snapdeal.cocofs.configuration.Property;

/**
 * Annotation to limit the number of calls within a window The window is defined by seconds. The number of calls that
 * can be made in that window is defined by two variables callsAllowedDuringDay and callsAllowedAtNight which gives us
 * the allowed calls during day and night time which is defined by {@link Property#THROTTLE_NIGHT_START_TIME} and
 * {@link Property#THROTTLE_NIGHT_END_TIME} These properties can be overridden through the configuration file map of
 * overridingProperty. For more information on this see {@code ConfigUtils}. Example: The moving window start at the
 * moment first call is made to the specified method e.g. 10h 20m 10s with moving window of 20s and calls allowed = 100.
 * If the number of calls exceed 100 at 10h 20m 20s then thread will sleep for the remaining interval 10 s and after
 * that allowed counter is reset and above process continues
 * 
 * @author abhinav
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.METHOD })
public @interface Throttled {

    /**
     * Number of calls allowed during day
     * 
     * @return
     */
    long callsAllowedDuringDay() default 0;

    /**
     * Time duration to be taken to handle given calls
     * 
     * @return
     */
    int seconds() default 0;

    /**
     * Calls allowed at night
     * 
     * @return
     */
    long callsAllowedAtNight() default 0;

    /**
     * @return
     */
    Property overridingProperty();

}
