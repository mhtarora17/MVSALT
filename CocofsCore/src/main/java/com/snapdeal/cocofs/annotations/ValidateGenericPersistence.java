package com.snapdeal.cocofs.annotations;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.METHOD;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Bounded with aspect GenericPersistenceValidator to validate entities in request.
 * 
 * 
 * @author abhinav
 *
 */

@Retention(RetentionPolicy.RUNTIME)
@Target({ ANNOTATION_TYPE, METHOD })
public @interface ValidateGenericPersistence {

}
