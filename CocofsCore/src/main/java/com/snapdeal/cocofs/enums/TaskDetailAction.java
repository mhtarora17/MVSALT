/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 09-Sep-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.enums;

public enum TaskDetailAction {

    START_ALL("StartAll"), RESUME_ALL("ResumeAll"), STOP_All("StopAll"),PAUSE_ALL("PauseAll"), START_TASK("StartTask"), STOP_TASK("StopTask"), UPDATE_TASK_DETAIL("UpdateTaskDetail");

    private String code;

    private TaskDetailAction(String code) {
        this.code = code;
    }

    public String getCode() {
        return this.code;
    }

}
