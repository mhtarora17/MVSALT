/**
 * 
 */
package com.snapdeal.cocofs.enums;

/**
 * @author abhinav
 */
public enum BlockName {

    PacakingType("PacakingType"), GiftWrapCategoryRuleblock("GiftWrapCategoryRuleblock");

    private String code;

    private BlockName(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
