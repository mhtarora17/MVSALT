/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 09-Sep-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.enums;

public enum TaskStatus {

    RUNNING("Running"), WAITING("Waiting"), INACTIVE("Inactive"), PAUSED("Paused");

    private String code;

    private TaskStatus(String code) {
        this.code = code;
    }

    public String getCode() {
        return this.code;
    }
}
