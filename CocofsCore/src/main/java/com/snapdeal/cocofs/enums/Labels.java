/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.enums;

import java.util.ArrayList;
import java.util.List;

/**
 *  
 *  @version     1.0, 28-Dec-2015
 *  @author indrajit
 */
public enum Labels {

    Premium("premium"), Designer("designer"), Luxury("luxury");
    
    String name;

    private Labels(String name) {
	this.name = name;
    }

    public String getCode() {
	return name;
    }

    public static List<String> getAllTypes() {
	List<String> list = new ArrayList<String>();
	Labels[] saList = Labels.values();
	for (Labels sa : saList) {
	    list.add(sa.name);
	}
	return list;
    }

}
