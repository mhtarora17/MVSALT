/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 29-Apr-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.enums;

public enum BulkUploadValidationError {

    INVALID_FUFILMENT_MODEL("Invalid Fulfillment Model"), //
    INVALID_PINCODE("Invalid Pincode"),//
    SUPC_MISSING("Supc not specified"),
    STORE_CODE_MISSING("Store not specified"),//
    SUBCAT_MISSING("Subcat not specified"), //
    SELLER_NON_EXIST("Non existent seller specified"), //
    SELLER_SUPC_MAPPING_NON_EXIST("Non existent seller-supc mapping specified"), //
    INVALID_SELLER_SUPC_FM_MAPPING_EXIST("invalid seller-supc fm mapping"), //

    FM_FC_MAPPING_NOT_VALID("Invalid fulfillment center mapping specified"), //
    FC_NON_EXIST("Non existent fulfillment center specified"), //
    FC_INCORRECT_CASE("Fulfillment center code specified in incorrect case"), //
    FILE_UNREADABLE("File not readable/format error "), //
    FILE_TYPE_NOT_CORRECT("Invalid File Type"), //
    FILE_SIZE_LIMIT_EXCEEDED("File Size Too Large"), //
    UPLOAD_FILE_READ_ERROR("File could not be read"), //
    INVALID_FILE_FORMAT("Invalid file format"), //
    EMPTY_FILE("Empty file"), //
    REQUIRED_FIELDS_MISSING("Required Fields Missing"), //
    INVALID_DANGEROUS_GOODS_TYPE("Invalid Dangerous Goods Type"), //
    INVALID_SERIALIZED_TYPE("Invalid Serialized Type"), //
    INVALID_PACKAGING_TYPE("Invalid Packaging Type"), //
    INVALID_DATA_VALUE("invalid value for data"),
    INVALID_PACKAGINGTYPE("invalid packagingtype"),
    SUPC_NON_EXIST("non existant supc specified"), //
    SUBCAT_NON_EXIST("non existant subcat specified"), //
    INVALID_TAX_CLASS("Invalid tax class"); //

    private String description;

    private BulkUploadValidationError(String desc) {
        this.description = desc;
    }

    public String getDescription() {
        return description;
    }

}
