package com.snapdeal.cocofs.enums;

public enum ProductAttributeNotificationUserType {

    DEFAULT, SUPER_USER;

}
