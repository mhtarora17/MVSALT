package com.snapdeal.cocofs.enums;

public enum NotificationType {
    DEAD_WEIGHT("DEAD-WEIGHT"), VOLUMETRIC_WEIGHT("VOL-WEIGHT");

    private final String code;

    NotificationType(String notificationCode) {
        code = notificationCode;
    }

    public String getCode() {
        return code;
    }
}
