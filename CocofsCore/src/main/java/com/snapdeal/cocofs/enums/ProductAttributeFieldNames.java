/**
 * 
 */
package com.snapdeal.cocofs.enums;

/**
 * @author abhinav
 *
 */
public enum ProductAttributeFieldNames {

    SUPC("supc"), ATTRIBUTE("attribute"), VALUE("value"), ENABLED("enabled"), UPDATED_BY("updatedBy"), CREATED_BY("createdBy"),
    CREATED("created"), LAST_UPDATED("lastUpdated"), UPDATED("updated");
    
    private String code;

    private ProductAttributeFieldNames(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
