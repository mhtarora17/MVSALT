package com.snapdeal.cocofs.enums;

public enum GiftWrapCategoryServiceabilityRuleCriteria {

     PRICE("PRICE"), WEIGHT("WEIGHT"), VOLUME("VOLUME"), DANGEROUSGOODSTYPE("DANGEROUSGOODSTYPE"),FRAGILE("FRAGILE");

    private String code;

    private GiftWrapCategoryServiceabilityRuleCriteria(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

}
