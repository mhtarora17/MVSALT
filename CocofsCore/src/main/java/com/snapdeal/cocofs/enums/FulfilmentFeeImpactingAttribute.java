package com.snapdeal.cocofs.enums;

/**
 * 
 * @author nikhil
 *
 */
public enum FulfilmentFeeImpactingAttribute {
    WEIGHT("weight"), LENGTH("length"), HEIGHT("height"), BREADTH("breadth"), SYSTEMWEIGHTCAPTURED("systemWeightCaptured"); 
    //systemWeightCaptured is actually a  flag associated with weight and not a real attribute. 
    //but as it is associated with weight it must go along with it.
    private final String attrName;

    FulfilmentFeeImpactingAttribute(String code) {
        attrName = code;
    }

    public String getCode() {
        return attrName;
    }

}
