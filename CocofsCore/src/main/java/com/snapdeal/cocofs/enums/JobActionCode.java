/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 21-Oct-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.enums;

public enum JobActionCode {

    SELLER_SUPC_FM_MAPPING("SellerSupcFMMapping"),
    STATE_CATEGORY_PRICE_TAX_RATE("StateCategoryPriceTaxRate"),
    SELLER_CATEGORY_TAX_RATE("SellerCategoryTaxRate"),
    SELLER_SUPC_TAX_RATE("SellerSUPCTaxRate"),
    SUPC_PACKAGINGTYPE_MAPPING("SupcPackagingTypeMapping"),
    SUPC_TAX_CLASS("SupcTaxClass"),
    SUBCAT_TAX_CLASS("SubcatTaxClass"),
    P_Z_UPDATE("PZUpdate");


    private String code;

    private JobActionCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return this.code;
    }
}
