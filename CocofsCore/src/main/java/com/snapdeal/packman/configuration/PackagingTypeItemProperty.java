/**
 *  Copyright 2016 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.configuration;

/**
 *  
 *  @version     1.0, 19-Jan-2016
 *  @author brijesh
 */
public enum PackagingTypeItemProperty {

    LENGTH                                    ("length"),
    BREADTH                                   ("breadth"),
    HEIGHT                                    ("height"),
    VOL_WEIGHT                                ("vol.weight");
    
    private String name;

    private PackagingTypeItemProperty(String name){
        this.name = name;
    }
    
    public String getName(){
        return this.name;
    }
    
}
