/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.configuration;

/**
 *  
 *  @version     1.0, 06-Dec-2015
 *  @author brijesh
 */
public enum PackagingProperty {
        
        VALIDATION_ENABLED                                      ("validation.enabled","Boolean"),
        PACKAGING_TYPE_PREFIX                                   ("packaging.type.prefix","String"),
        PACKAGING_TYPE_CATEGORY                                 ("packaging.type.category","String");
        
        private String name;
        private String type;

        private PackagingProperty(String name,String type){
            this.name = name;
            this.type = type;
        }
        
        public String mode(){
            return this.name;
        }
        
        public String value(){
            return this.type;
        }
        
        public static PackagingProperty getPropertyByPropertyName(String property) {
            for (PackagingProperty p : values()) {
                if (p.toString().equals(property)) {
                    return p;
                }
            }
            return null;
        }
        
}

    
    

