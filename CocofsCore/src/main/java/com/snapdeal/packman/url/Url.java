/**
 *  Copyright 2016 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packman.url;

/**
 *  
 *  @version     1.0, 03-Jan-2016
 *  @author brijesh
 */
public enum Url {

    CreatePackagingType("createpackagingtype"),
    ViewPackagingType("viewpackagingtype"),
    CreatePackagingTypeItem("createpackagingtypeitem"),
    ViewPackagingTypeItem("viewpackagingtypeitem"),
    CreateSlab("createslab"),
    ViewSlab("viewslab"),
    CreateRule("createrule"),
    ViewRule("viewrule");
    
    private String code;
    
    Url(String code){
        this.code = code;
    }
    
    public String getCode(){
        return this.code;
    }
}
