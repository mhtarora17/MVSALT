import aerospike
import uuid
import random
import datetime

#RANDOM COCOFS FM MODEL DATA GENERATOR

LIMIT = 999
FM = ["FC_VOI", "DROPSHIP"]
SUB = ["Mobiles", "Sports", "Home Decor", "Televisions", "AC", "Watches", "Fashion"]
SELLER_HAS_SUPC = set()
SELLER_HAS_SUBCAT = set()
SELLER_HAS_SUPC_SUBCAT = set()
config = {
    'hosts' : [('127.0.0.1', 3000)]
}

client = aerospike.client(config).connect()
namespace = 'cocofspy'

print 'Inserting data in seller_supc_fm_mapping_vo ...'

##SELLER SUPC FM MAPPINGS
set = 'seller_supc_fm_mapping_vo'
#id, ssc, sc, su, fm, e, c, u
for i in range(0, LIMIT+1):
    seller_code = str(uuid.uuid4())[0:5]
    supc = random.randrange(0, 100000000)
    key = (namespace, set, seller_code+'-'+str(supc))
    rand = int(random.randrange(0, 2))

    if rand == 1 :
        SELLER_HAS_SUPC.add(seller_code)

    value = {
        'id' : i,
        'sc' : seller_code,
        'su' : supc,
        'fm' : FM[rand],
        'e' : rand,
        'c' : str(int((datetime.datetime.now()-datetime.datetime(1970, 1, 1)).total_seconds())+1),
        'u' : str(int((datetime.datetime.now()-datetime.datetime(1970, 1, 1)).total_seconds())+2)
    }

    client.put(key, value)


print 'Inserting data in seller_subcat_fm_mapping_vo ...'

##SELLER SUBCAT FM MAPPINGS
set = 'seller_subcat_fm_mapping_vo'
#id, ssc, sc, sub, fm, e, c, u
for i in range(0, LIMIT+1):
    seller_code = str(uuid.uuid4())[0:5]
    sub = SUB[int(random.randrange(0, len(SUB)))]
    key = (namespace, set, seller_code+'-'+sub)
    rand = int(random.randrange(0, 2))

    selector = int(random.randrange(0,3))
    if selector == 1 :
        SELLER_HAS_SUBCAT.add(seller_code)
    if selector == 2 :
        seller_code = SELLER_HAS_SUPC.pop()
        SELLER_HAS_SUPC_SUBCAT.add(seller_code)


    value = {
        'id' : i,
        'sc' : seller_code,
        'sub' : sub,
        'fm' : FM[rand],
        'e' : rand,
        'c' : str(int((datetime.datetime.now()-datetime.datetime(1970, 1, 1)).total_seconds())+1),
        'u' : str(int((datetime.datetime.now()-datetime.datetime(1970, 1, 1)).total_seconds())+2)
    }
    client.put(key, value)

print 'Inserting data in seller_fm_mapping_vo ...'

##SELLER FM MAPPINGS
set = 'seller_fm_mapping_vo'

#id, sc, fm, e, c, u, se, sce
for i in range(0, LIMIT+1):
    seller_code = str(uuid.uuid4())[0:5]
    key = (namespace, set, seller_code)
    rand = int(random.randrange(0, 2))

    selector = int(random.randrange(0,4))
    se = 0
    sce = 0

    if rand == 0 and len(SELLER_HAS_SUPC) > 0:
        seller_code = SELLER_HAS_SUPC.pop()
        se = 1
        sce = 0

    if rand == 1 and len(SELLER_HAS_SUBCAT) > 0:
        seller_code = SELLER_HAS_SUBCAT.pop()
        se = 0
        sce = 1

    if rand == 2 and len(SELLER_HAS_SUPC_SUBCAT)>0:
        seller_code = SELLER_HAS_SUPC_SUBCAT.pop()
        se = 1
        sce = 1

    value = {
        'id' : i,
        'sc' : seller_code,
        'fm': FM[rand],
        'e': rand,
        'c': str(int((datetime.datetime.now()-datetime.datetime(1970, 1, 1)).total_seconds())+1),
        'u': str(int((datetime.datetime.now()-datetime.datetime(1970, 1, 1)).total_seconds())+2),
        'se': se,
        'sce': sce
    }

    client.put(key, value)

client.close()