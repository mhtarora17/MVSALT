package com.snapdeal.cocofs.fcmapping;

/**
 * Utility to form/generate the key for the record to be searched/retreived from Aerospike.
 * 
 * @author gaurav
 *
 */
public class AerospikeKeyHelper {
	
	/**
	 * Key = concatenate(subkey1, subkey2, ..... , subkeyN)
	 * @param subkey
	 * @return
	 */
	public static String getKey(String...subkeys){
		if(subkeys == null)
			return null;
		
		StringBuilder key = new StringBuilder("");
		
		key.append(subkeys[0]);
		for(int i = 1 ; i < subkeys.length; i++){
			key.append("-");
			key.append(subkeys[i]);
		}
		
		return key.toString();
	}
}
