package com.snapdeal.cocofs.fcmapping;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.aerospike.client.AerospikeClient;
import com.aerospike.client.AerospikeException;
import com.aerospike.client.Host;
import com.aerospike.client.policy.ClientPolicy;

/**
 * Migration for cocofs Fulfilment Model data from mysql to aerospike
 * 
 * @author gaurav
 *
 */
public class Migration {
    
    //Parameters for Mysql data connection
	public static String MYSQL_HOSTNAME = "";
	public static int MYSQL_PORT = 0;
	public static String MYSQL_USERNAME = "";
	public static String MYSQL_PASSWORD = "";
	

	//Parameters for Aerospike data connection
	public static String AEROSPIKE_HOSTNAME = "";
	public static int AEROSPIKE_PORT = 0;
	public static String AEROSPIKE_USERNAME = "";
	public static String AEROSPIKE_PASSWORD = "";
	
	//Program parameters
	public static boolean DEBUG = true;
	
	//Data connections
	public static AerospikeClient AEROSPIKE_CONNECTION;
	public static Connection MYSQL_CONNECTION;
	
	public static void main(String[] args) {
		if(args == null || args.length < 8){
			printUsage();
			return;
		}
		processParameters(args);
		makeConnections();
		DataMigrator migrator = new DataMigrator(AEROSPIKE_CONNECTION, MYSQL_CONNECTION);
		migrator.harmonizeSellerFmMapping();
		migrator.harmonizeSellerSupcFmMapping();
		
	}
	
	private static void makeConnections(){
		if(Migration.DEBUG)
			System.out.println("Establishing data connections...");
		
        try {

            Host[] hosts = new Host[1];

            hosts[0] = new Host(AEROSPIKE_HOSTNAME, AEROSPIKE_PORT);

            ClientPolicy cp = new ClientPolicy();
            cp.failIfNotConnected = false;

            AEROSPIKE_CONNECTION = new AerospikeClient(cp, hosts);

            //AEROSPIKE_CONNECTION = new AerospikeClient(clientPolicy, AEROSPIKE_HOSTNAME, AEROSPIKE_PORT);
        } catch (AerospikeException e) {
            System.out.println("An exception occured while making connnection to aerospike cluster.");
            try {
                AEROSPIKE_CONNECTION = new AerospikeClient(AEROSPIKE_HOSTNAME, AEROSPIKE_PORT);
            } catch (AerospikeException ex) {
                System.out.println("Still not able to make a connection with Aerospike..");
                System.out.println("Exiting now..");
                System.exit(0);
            }
        }
		 System.out.println("Retrying without username and password...");

         
		
		String url = "jdbc:mysql://"+MYSQL_HOSTNAME+":"+MYSQL_PORT+"/cocofs";
	    try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			MYSQL_CONNECTION = DriverManager.getConnection(url, MYSQL_USERNAME, MYSQL_PASSWORD);
		} catch (InstantiationException e) {
			e.printStackTrace();
			System.exit(0);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			System.exit(0);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			System.exit(0);
		} catch (SQLException e) {
			e.printStackTrace();
			System.exit(0);
		}
       
		System.out.println("Mysql and Aerospike connections successful..");
	}
	
	private static void printUsage(){
		System.out.println("<USAGE>:");
		System.out.println("java -jar <JARNAME> <MYSQL_HOSTNAME> <MYSQL_PORT> <MYSQL_USERNAME> <MYSQL_PASSWORD><AEROSPIKE_HOSTNAME> <AEROSPIKE_PORT> <AEROSPIKE_USERNAME> <AEROSPIKE_PASSWORD>");
		System.out.println("For any param not available add ? in its place.");
	}
	
	private static void processParameters(String[] args){
		if(Migration.DEBUG)
			System.out.println("Processing input parameters now..");
		
		MYSQL_HOSTNAME = normalize(args[0]);
		MYSQL_PORT = Integer.parseInt(normalize(args[1]));
		MYSQL_USERNAME = normalize(args[2]);
		MYSQL_PASSWORD = normalize(args[3]);
		
		AEROSPIKE_HOSTNAME = normalize(args[4]);
		AEROSPIKE_PORT = Integer.parseInt(normalize(args[5]));
		AEROSPIKE_USERNAME = normalize(args[6]);
		AEROSPIKE_PASSWORD = normalize(args[7]);
		
		if(Migration.DEBUG){
			
			System.out.println("MYSQL_HOSTNAME = "+MYSQL_HOSTNAME);
			System.out.println("MYSQL_PORT = "+MYSQL_PORT);
			System.out.println("MYSQL_USERNAME = "+MYSQL_USERNAME);
			System.out.println("MYSQL_PASSWORD = "+MYSQL_PASSWORD);
			
			System.out.println("AEROSPIKE_HOSTNAME = "+AEROSPIKE_HOSTNAME);
			System.out.println("AEROSPIKE_PORT = "+AEROSPIKE_PORT);
			System.out.println("AEROSPIKE_USERNAME = "+AEROSPIKE_USERNAME);
			System.out.println("AEROSPIKE_PASSWORD = "+AEROSPIKE_PASSWORD);
				
		}
		
	}
	
	private static String normalize(String param){
		if(param.trim().equalsIgnoreCase("?")){
			return "0";
		}
		return param.trim();
	}

}