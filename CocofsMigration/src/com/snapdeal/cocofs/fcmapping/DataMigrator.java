package com.snapdeal.cocofs.fcmapping;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.aerospike.client.AerospikeClient;
import com.aerospike.client.AerospikeException;
import com.aerospike.client.Bin;
import com.aerospike.client.Key;
import com.aerospike.client.policy.Priority;
import com.aerospike.client.policy.WritePolicy;

public class DataMigrator {

    private AerospikeClient aerospikeClient;
    private Connection      mysqlConnection;
    private final String    NAMESPACE = "cocofs";
    private int             timeout   = 10000;   // 10 sec timeout

    //private final String NAMESPACE = "cocofs_test2";
    public DataMigrator(AerospikeClient aerospikeClient, Connection mysqlConnection) {
        this.aerospikeClient = aerospikeClient;
        this.mysqlConnection = mysqlConnection;
    }

    public void harmonizeSellerFmMapping() {

        if(Migration.DEBUG){
            System.out.println("Starting migration of Seller_FC_Mapping now...");
        }
        int rowAffected = 0;
        try {
            Statement statement = mysqlConnection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT fc.fc_code,fm.seller_code,fm.fulfilment_model,fm.enabled,fm.created,fm.updated,fm.supc_exist,fm.subcat_exist"+
                                                        " FROM seller_fm_mapping fm, seller_fc_mapping fc where fm.id=fc.seller_fm_id and fc.enabled=1");
            String setname = "seller_fm_mapping_vo";
            WritePolicy wpolicy = new WritePolicy();
            wpolicy.timeout = timeout;
            wpolicy.priority = Priority.MEDIUM;
            while (resultSet.next()) {

                //read one row from mysql table
                String sellerCode = resultSet.getString("fm.seller_code");
                //System.out.println("processing seller:" + sellerCode);

                String fulfilmentModel = resultSet.getString("fm.fulfilment_model");
                int enabled = resultSet.getInt("fm.enabled");
                Timestamp created = resultSet.getTimestamp("fm.created");
                Timestamp updated = resultSet.getTimestamp("fm.updated");
                boolean supc_exist = resultSet.getBoolean("fm.supc_exist");
                boolean subcat_exist = resultSet.getBoolean("fm.subcat_exist");
                String fcCode = resultSet.getString("fc.fc_code");
                List<String> fcList = null;
                if (fcCode != null && fcCode != "") {
                    fcList = new ArrayList<String>();
                    fcList.add(fcCode);

                    //write on record into aerospike set
                    String keystr = AerospikeKeyHelper.getKey(sellerCode);
                    Key key = new Key(NAMESPACE, setname, keystr);
                    Bin[] bins = new Bin[SELLER_BINS.values().length];

                    //sc, fm, e, c, u, se, sce, fc

                    bins[0] = new Bin(SELLER_BINS.sc.toString(), sellerCode);
                    bins[1] = new Bin(SELLER_BINS.fm.toString(), fulfilmentModel);
                    bins[2] = new Bin(SELLER_BINS.e.toString(), enabled);
                    bins[3] = new Bin(SELLER_BINS.c.toString(), "" + created.getTime());
                    bins[4] = new Bin(SELLER_BINS.u.toString(), "" + updated.getTime());
                    bins[5] = new Bin(SELLER_BINS.se.toString(), supc_exist ? 1 : 0);
                    bins[6] = new Bin(SELLER_BINS.sce.toString(), subcat_exist ? 1 : 0);
                    bins[7] = new Bin(SELLER_BINS.fc.toString(), fcList);

                    try {
                        System.out.println("inserting seller:" + sellerCode + " fcList: " + fcList);
                        aerospikeClient.put(wpolicy, key, bins);                        
                        rowAffected++;
                    } catch (AerospikeException e) {
                        if (Migration.DEBUG) {
                            System.out.println("Not able to write record to aerospike for seller:"+sellerCode);
                            e.printStackTrace();
                        }
                    }
                }else{
                    System.out.println("fc code is empty or null , hence aerospike transaction is skipped for seller:"+sellerCode);
                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
            System.exit(0);
        } catch (AerospikeException e) {
            e.printStackTrace();
            System.exit(0);
        }

        if (Migration.DEBUG) {
            System.out.println("Migration of Seller_FM_Mapping completed.");
            System.out.println("Number of records inserted = "+rowAffected);
        }
    
    }
    
    public void harmonizeSellerSupcFmMapping() {

        if(Migration.DEBUG){
            System.out.println("Starting migration of Seller_SUPC_FC_Mapping now...");
        }
        int rowAffected = 0;
        try {
            Statement statement = mysqlConnection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT fc.fc_code,fm.seller_code,fm.supc,fm.fulfilment_model,fm.enabled,fm.created,fm.updated"+
                                                        " FROM seller_supc_fm_mapping fm, seller_supc_fc_mapping fc where fm.id=fc.seller_supc_fm_id and fc.enabled=1");
            String setname = "seller_supc_fm_mapping_vo";
            WritePolicy wpolicy = new WritePolicy();
            wpolicy.timeout = timeout;
            wpolicy.priority = Priority.MEDIUM;
            while (resultSet.next()) {

                //read one row from mysql table
                String sellerCode = resultSet.getString("fm.seller_code");
                String supc = resultSet.getString("fm.supc");

                //System.out.println("processing seller:" + sellerCode + " supc:" +  supc);

                String fulfilmentModel = resultSet.getString("fm.fulfilment_model");
                int enabled = resultSet.getInt("fm.enabled");
                Timestamp created = resultSet.getTimestamp("fm.created");
                Timestamp updated = resultSet.getTimestamp("fm.updated");
                 String fcCode = resultSet.getString("fc.fc_code");
                List<String> fcList = null;
                if (fcCode != null && fcCode != "") {
                    fcList = new ArrayList<String>();
                    fcList.add(fcCode);

                    //write on record into aerospike set
                    String keystr = AerospikeKeyHelper.getKey(sellerCode,supc);
                    Key key = new Key(NAMESPACE, setname, keystr);
                    Bin[] bins = new Bin[SELLER_BINS.values().length];

                    //sc, fm, e, c, u, se, sce, fc

                    bins[0] = new Bin(SELLER_SUPC_BINS.ssc.toString(), keystr);
                    bins[1] = new Bin(SELLER_SUPC_BINS.sc.toString(), sellerCode);
                    bins[2] = new Bin(SELLER_SUPC_BINS.su.toString(), supc);

                    bins[3] = new Bin(SELLER_SUPC_BINS.fm.toString(), fulfilmentModel);
                    bins[4] = new Bin(SELLER_SUPC_BINS.e.toString(), enabled);
                    bins[5] = new Bin(SELLER_SUPC_BINS.c.toString(), "" + created.getTime());
                    bins[6] = new Bin(SELLER_SUPC_BINS.u.toString(), "" + updated.getTime());
                    bins[7] = new Bin(SELLER_SUPC_BINS.fc.toString(), fcList);

                    try {
                        System.out.println("inserting:" + sellerCode + " supc:" +  supc +" fcList: " + fcList);
                        aerospikeClient.put(wpolicy, key, bins);                        
                        rowAffected++;
                    } catch (AerospikeException e) {
                        if (Migration.DEBUG) {
                            System.out.println("Not able to write record to aerospike for seller:"+ sellerCode + " and supc:"+supc);
                            e.printStackTrace();
                        }
                    }
                }else{
                    System.out.println("fc code is empty or null , hence aerospike transaction is skipped for seller:"+sellerCode);
                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
            System.exit(0);
        } catch (AerospikeException e) {
            e.printStackTrace();
            System.exit(0);
        }

        if (Migration.DEBUG) {
            System.out.println("Migration of Seller_SUPC_FM_Mapping completed.");
            System.out.println("Number of records inserted = "+rowAffected);
        }
    
    }
}
