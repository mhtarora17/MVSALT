package com.snapdeal.cocofs.update.sellercodefix;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.aerospike.client.AerospikeClient;
import com.aerospike.client.AerospikeException;
import com.aerospike.client.Bin;
import com.aerospike.client.Key;
import com.aerospike.client.policy.WritePolicy;

public class DataMigrator {

    private AerospikeClient aerospikeClient;
    private Connection      mysqlConnection;
    private final String    NAMESPACE = "cocofs";
    private int             timeout   = 10000;   // 10 sec timeout

    //private final String NAMESPACE = "cocofs_test2";
    public DataMigrator(AerospikeClient aerospikeClient, Connection mysqlConnection) {
        this.aerospikeClient = aerospikeClient;
        this.mysqlConnection = mysqlConnection;
    }

    public void harmonizeSellerSupcFmMapping() {
        if (Migration.DEBUG) {
            System.out.println("Starting updation of Seller_SUPC_FM_Mapping now...");
        }
        long tStart = System.nanoTime();
        int rowAffected = 0;

        Set<String> failedSellerSUPCSet = new LinkedHashSet<String>();
        try {
            long tQStart = System.nanoTime();
            Statement statement = mysqlConnection.createStatement();
            ResultSet resultSet = statement.executeQuery("select "
                    + "sfm.seller_code, ssfm.seller_code, ssfm.supc, ssfm.fulfilment_model, ssfm.enabled, ssfm.created, "
                    //+ "ssfm.last_updated, ssfm.updated_by, "
                    + "ssfm.updated " //
                    + "from seller_fm_mapping sfm " + "join seller_supc_fm_mapping ssfm " + "where ssfm.seller_code = sfm.seller_code "
                    + "AND BINARY ssfm.seller_code != sfm.seller_code");
            //            ResultSet resultSet = statement.executeQuery("select "
            //                    + "sfm.seller_code, ssfm.seller_code, ssfm.supc, ssfm.fulfilment_model, ssfm.enabled, ssfm.created, ssfm.last_updated, ssfm.updated_by, ssfm.updated "
            //                    + "from seller_fm_mapping sfm " + "join seller_supc_fm_mapping ssfm " + "where ssfm.seller_code = sfm.seller_code "
            //                    + "AND BINARY ssfm.seller_code != sfm.seller_code limit 10");
            long tQEnd = System.nanoTime();

            String setname = "seller_supc_fm_mapping_vo";
            WritePolicy wpolicy = new WritePolicy();
            wpolicy.timeout = timeout;

            System.out.println("aero-client:" + aerospikeClient);
            System.out.println("aero-wpolicy-timeout:" + timeout);
            System.out.println("mysql-client:" + mysqlConnection);
            // Set auto-commit to false
            mysqlConnection.setAutoCommit(false);

            String updateSql = "UPDATE seller_supc_fm_mapping set seller_code=? where seller_code=?";
            PreparedStatement pstmt = mysqlConnection.prepareStatement(updateSql);

            Set<String> sellerSet = new LinkedHashSet<String>();
            Set<String> failedSellerSet = new LinkedHashSet<String>();

            Map<String, String> mapOld = new LinkedHashMap<String, String>();
            Map<String, String> mapNew = new LinkedHashMap<String, String>();

            while (resultSet.next()) {

                //read one row from mysql table
                String sellerCodeNew = resultSet.getString("sfm.seller_code");
                String sellerCodeOld = resultSet.getString("ssfm.seller_code");
                String supc = resultSet.getString("ssfm.supc");
                String fulfilmentModel = resultSet.getString("ssfm.fulfilment_model");
                int enabled = resultSet.getInt("ssfm.enabled");
                Timestamp created = resultSet.getTimestamp("ssfm.created");
                //                Timestamp last_updated = resultSet.getTimestamp("ssfm.last_updated");
                //                String updated_by = resultSet.getString("ssfm.updated_by");
                Timestamp updated = resultSet.getTimestamp("ssfm.updated");

                //1) write to AEROSPIKE ( add new entry, delete old)

                //write on record into aerospike set

                //TODO - remove the below - only for testing ... 

                //                String keyStrOld = AerospikeKeyHelper.getKey(sellerCodeOld, supc);
                //                String keyStrNew = AerospikeKeyHelper.getKey(sellerCodeNew, supc);
                //
                //                Key keyOld = new Key(NAMESPACE, setname, keyStrNew);
                //                Key keyNew = new Key(NAMESPACE, setname, keyStrOld);

                //TODO - remove the above- only for testing ... 

                String keyStrOld = AerospikeKeyHelper.getKey(sellerCodeOld, supc);
                String keyStrNew = AerospikeKeyHelper.getKey(sellerCodeNew, supc);
                Key keyOld = new Key(NAMESPACE, setname, keyStrOld);
                Key keyNew = new Key(NAMESPACE, setname, keyStrNew);

                Bin[] bins = new Bin[SELLER_SUPC_BINS.values().length];

                //ssc, sc, su, fm, e, c, u

                bins[0] = new Bin(SELLER_SUPC_BINS.sc.toString(), sellerCodeNew);
                bins[1] = new Bin(SELLER_SUPC_BINS.fm.toString(), fulfilmentModel);
                bins[2] = new Bin(SELLER_SUPC_BINS.e.toString(), enabled);
                bins[3] = new Bin(SELLER_SUPC_BINS.c.toString(), String.valueOf(created.getTime()));
                bins[4] = new Bin(SELLER_SUPC_BINS.u.toString(), String.valueOf(updated.getTime()));
                bins[5] = new Bin(SELLER_SUPC_BINS.su.toString(), supc);
                bins[6] = new Bin(SELLER_SUPC_BINS.ssc.toString(), keyStrNew);

                try {
                    System.out.println("count=" + (rowAffected + 1));
                    System.out.println("putting  keyNew=" + keyNew);
                    aerospikeClient.put(wpolicy, keyNew, bins);
                    System.out.println("deleting keyOld=" + keyOld);
                    boolean delete = aerospikeClient.delete(wpolicy, keyOld);
                    rowAffected++;
                    System.out.println("deleted=" + delete);
                    System.out.println("done");
                } catch (AerospikeException e) {
                    if (Migration.DEBUG) {
                        System.out.println(" === Not able to write record to aerospike. - keyStrNew:" + keyStrNew + "- keyStrOld:" + keyStrOld);
                        e.printStackTrace(System.out);
                        e.printStackTrace();
                    }
                    failedSellerSet.add(sellerCodeOld);
                    failedSellerSUPCSet.add("keyStrNew:" + keyStrNew + "- keyStrOld:" + keyStrOld);
                    //remove old seller code - failed has to be re-attempted
                    sellerSet.remove(sellerCodeOld);
                    mapOld.remove(sellerCodeOld);
                    mapNew.remove(sellerCodeOld);
                }

                //2) write to MySQL (update) in batch

                if (!sellerSet.contains(sellerCodeOld) && !failedSellerSet.contains(sellerCodeOld)) {
                    sellerSet.add(sellerCodeOld);
                    mapOld.put(sellerCodeOld, sellerCodeOld);
                    mapNew.put(sellerCodeOld, sellerCodeNew);
                } else {
                    System.out.print("-");
                }
                //

            }//end of while

            System.out.println("starting with mysql related activity.");
            for (Entry<String, String> entry : mapOld.entrySet()) {

                String sellerCodeOld = entry.getKey();//or value
                String sellerCodeNew = mapNew.get(sellerCodeOld);//or value
                pstmt.setString(1, sellerCodeNew);
                pstmt.setString(2, sellerCodeOld);
                pstmt.addBatch();
                System.out.println("sql - " + pstmt);
            }

            System.out.println("Start with mysql batch execute .. ");
            long tCommitStart = System.nanoTime();
            //Create an int[] to hold returned values
            int[] count = pstmt.executeBatch();
            long tBatchUpdateEnd = System.nanoTime();

            System.out.println("Start with mysql commit .. ");
            mysqlConnection.commit();
            long tCommitEnd = System.nanoTime();

            System.out.println("no of sellers updated in mysql :" + sellerSet.size());
            System.out.println("sellers updated in mysql :" + sellerSet);
            System.out.println("commit result :" + Arrays.toString(count));

            int index = 0;
            StringBuilder sb = new StringBuilder();
            for (String seller : sellerSet) {
                sb.append(seller + "=" + count[index++] + ", ");
            }
            System.out.println("mapped result :" + sb.toString());
            System.out.println(" === no of failed sellers-supc pair(s) :" + failedSellerSUPCSet.size());
            System.out.println(" === failed sellers-supc pair(s) :" + failedSellerSUPCSet);

            String msg = "Time taken for MySQL query execution: " + (tQEnd - tQStart) + " ns, [" + (tQEnd - tQStart) / 1000000 + " ms]";
            String msg0 = "Time taken for MySQL batch execute: " + (tBatchUpdateEnd - tCommitStart) + " ns, [" + (tBatchUpdateEnd - tCommitStart) / 1000000 + " ms]";
            String msg1 = "Time taken for MySQL commit: " + (tCommitEnd - tBatchUpdateEnd) + " ns, [" + (tCommitEnd - tBatchUpdateEnd) / 1000000 + " ms]";
            String msg2 = "Time taken for MySQL update (batch+commit): " + (tCommitEnd - tCommitStart) + " ns, [" + (tCommitEnd - tCommitStart) / 1000000 + " ms]";
            System.out.println(msg);
            System.out.println(msg0);
            System.out.println(msg1);
            System.out.println(msg2);

        } catch (SQLException e) {
            e.printStackTrace(System.out);
            e.printStackTrace();
            System.exit(0);
        } catch (AerospikeException e) {
            e.printStackTrace(System.out);
            e.printStackTrace();
            System.exit(0);
        }
        long tEnd = System.nanoTime();
        if (Migration.DEBUG) {
            System.out.println("Updation of Seller_Supc_FM_Mapping completed.");
            System.out.println("Number of records updated (each - added and deleted) = " + rowAffected);
            String msg = "Time taken: " + (tEnd - tStart) + " ns, [" + (tEnd - tStart) / 1000000 + " ms]";
            System.out.println(msg);
        }
        if (!failedSellerSUPCSet.isEmpty()) {
            System.out.println("Note: Please re-run the script as some seller-supc update has failed !!! ");
        }
    }
}
