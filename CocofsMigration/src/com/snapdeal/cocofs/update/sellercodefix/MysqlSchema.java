package com.snapdeal.cocofs.update.sellercodefix;

/*
 * SCHEMA DEFINITION FOR MYSQL TABLES 
 */

public enum MysqlSchema {

}

enum SELLER_SUPC_SCHEMA {
	seller_code,supc,fulfilment_model,enabled,created, last_updated,updated_by,updated
}

enum SELLER_SCHEMA {	
	seller_code,fulfilment_model,enabled,created, last_updated,updated_by,updated, supc_exist, subcat_exist
}

enum SELLER_SUBCAT_SCHEMA{
	seller_code,subcat,fulfilment_model,enabled,created, last_updated,updated_by,updated
}

