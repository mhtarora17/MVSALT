package com.snapdeal.cocofs.update.sellersupccodefix.fm;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.aerospike.client.AerospikeClient;
import com.aerospike.client.AerospikeException;
import com.aerospike.client.policy.ClientPolicy;

/**
 * Migration for cocofs Fulfilment Model data from mysql to aerospike
 * 
 * @author gaurav
 */
public class DeleteRecordScript {

    //Parameters for Mysql data connection
    private static String          MYSQL_HOSTNAME     = "";
    private static int             MYSQL_PORT         = 0;
    private static String          MYSQL_USERNAME     = "";
    private static String          MYSQL_PASSWORD     = "";

    //Parameters for Aerospike data connection
    private static String          AEROSPIKE_HOSTNAME = "";
    private static int             AEROSPIKE_PORT     = 0;
    private static String          AEROSPIKE_USERNAME = "";
    private static String          AEROSPIKE_PASSWORD = "";

    // start, end, batch and limit
    private static int             START_INDEX        = 0;
    private static int             END_INDEX          = 0;
    private static int             BATCH_SIZE         = 10;
    private static int             LIMIT              = 0;

    //Program parameters
    public static boolean         DEBUG              = true;

    //Data connections
    private static AerospikeClient AEROSPIKE_CONNECTION;
    private static Connection      MYSQL_CONNECTION;

    public static void main(String[] args) {
        if (args == null || args.length < 11) {
            printUsage();
            return;
        }
        processParameters(args);
        makeConnections();
        DataMigrator migrator = new DataMigrator(AEROSPIKE_CONNECTION, MYSQL_CONNECTION);
        migrator.harmonizeSellerSupcFmMapping(START_INDEX, END_INDEX, BATCH_SIZE, LIMIT);

    }

    private static void makeConnections() {
        if (DeleteRecordScript.DEBUG)
            System.out.println("Establishing data connections...");
        ClientPolicy clientPolicy = new ClientPolicy();
        clientPolicy.user = AEROSPIKE_USERNAME;
        clientPolicy.password = AEROSPIKE_PASSWORD;

        try {
            AEROSPIKE_CONNECTION = new AerospikeClient(clientPolicy, AEROSPIKE_HOSTNAME, AEROSPIKE_PORT);
        } catch (AerospikeException e) {
            System.out.println("An exception occured while making connnection to aerospike cluster.");
        }
        System.out.println("Retrying without username and password...");

        try {
            AEROSPIKE_CONNECTION = new AerospikeClient(AEROSPIKE_HOSTNAME, AEROSPIKE_PORT);
        } catch (AerospikeException e) {
            System.out.println("Still not able to make a connection with Aerospike..");
            System.out.println("Exiting now..");
            System.exit(0);
        }

        String url = "jdbc:mysql://" + MYSQL_HOSTNAME + ":" + MYSQL_PORT + "/cocofs";
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            MYSQL_CONNECTION = DriverManager.getConnection(url, MYSQL_USERNAME, MYSQL_PASSWORD);
        } catch (InstantiationException e) {
            e.printStackTrace();
            System.exit(0);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            System.exit(0);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            System.exit(0);
        } catch (SQLException e) {
            e.printStackTrace();
            System.exit(0);
        }

        System.out.println("Mysql and Aerospike connections successful..");
    }

    private static void printUsage() {
        System.out.println("<USAGE>:");
        System.out.println("java -jar <JARNAME> <MYSQL_HOSTNAME> <MYSQL_PORT> <MYSQL_USERNAME> <MYSQL_PASSWORD><AEROSPIKE_HOSTNAME> <AEROSPIKE_PORT> <AEROSPIKE_USERNAME> <AEROSPIKE_PASSWORD> <START_INDEX> <END_INDEX> <BATCH_SIZE> <LIMIT> ");
        System.out.println("For any param not available add ? in its place.");
    }

    private static void processParameters(String[] args) {
        if (DeleteRecordScript.DEBUG)
            System.out.println("Processing input parameters now..");

        MYSQL_HOSTNAME = normalize(args[0]);
        MYSQL_PORT = Integer.parseInt(normalize(args[1]));
        MYSQL_USERNAME = normalize(args[2]);
        MYSQL_PASSWORD = normalize(args[3]);

        AEROSPIKE_HOSTNAME = normalize(args[4]);
        AEROSPIKE_PORT = Integer.parseInt(normalize(args[5]));
        AEROSPIKE_USERNAME = normalize(args[6]);
        AEROSPIKE_PASSWORD = normalize(args[7]);

        START_INDEX = Integer.parseInt(normalize(args[8]));
        END_INDEX = Integer.parseInt(normalize(args[9]));
        BATCH_SIZE = Integer.parseInt(normalize(args[10]));
        LIMIT = Integer.parseInt(normalize(args[11]));

        System.out.println("MYSQL_HOSTNAME     = " + MYSQL_HOSTNAME);
        System.out.println("MYSQL_PORT         = " + MYSQL_PORT);
        System.out.println("MYSQL_USERNAME     = " + MYSQL_USERNAME);
        System.out.println("MYSQL_PASSWORD     = " + MYSQL_PASSWORD);

        System.out.println("AEROSPIKE_HOSTNAME = " + AEROSPIKE_HOSTNAME);
        System.out.println("AEROSPIKE_PORT     = " + AEROSPIKE_PORT);
        System.out.println("AEROSPIKE_USERNAME = " + AEROSPIKE_USERNAME);
        System.out.println("AEROSPIKE_PASSWORD = " + AEROSPIKE_PASSWORD);

        System.out.println("START_INDEX        = " + START_INDEX);
        System.out.println("END_INDEX          = " + END_INDEX);
        System.out.println("BATCH_SIZE         = " + BATCH_SIZE);
        System.out.println("LIMIT              = " + LIMIT);

    }

    private static String normalize(String param) {
        if (param.trim().equalsIgnoreCase("?")) {
            return "0";
        }
        return param.trim();
    }

}