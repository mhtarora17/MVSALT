package com.snapdeal.cocofs.update.sellersupccodefix.fm;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;

import com.aerospike.client.AerospikeClient;
import com.aerospike.client.AerospikeException;
import com.aerospike.client.Key;
import com.aerospike.client.policy.WritePolicy;

public class DataMigrator {

    private AerospikeClient aerospikeClient;
    private Connection      mysqlConnection;
    private final String    NAMESPACE = "cocofs";
    private int             timeout   = 10000;   // 10 sec timeout

    //private final String NAMESPACE = "cocofs_test2";
    public DataMigrator(AerospikeClient aerospikeClient, Connection mysqlConnection) {
        this.aerospikeClient = aerospikeClient;
        this.mysqlConnection = mysqlConnection;
    }

    public void harmonizeSellerSupcFmMapping(int start, int end, int batchSize, int limit) {
        if (DeleteRecordScript.DEBUG) {
            System.out.println("Started at " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
            System.out.println("Starting deletion of Seller_SUPC_FM_Mapping now...");
        }

        System.out.println("start :" + start);
        System.out.println("end   :" + end);
        System.out.println("batch :" + batchSize);
        System.out.println("limit :" + limit);
        if (end < start || end == start || end < 1 || start < 0) {
            System.out.println("invalid input ... returning");
            return;
        }
        long tStart = System.nanoTime();
        int rowAffected = 0;

        Set<String> failedSellerSUPCSet = new LinkedHashSet<String>();
        final String setname = "seller_supc_fm_mapping_vo";
        WritePolicy wpolicy = new WritePolicy();
        wpolicy.timeout = timeout;

        System.out.println("aero-client:" + aerospikeClient);
        System.out.println("aero-wpolicy-timeout:" + timeout);
        System.out.println("mysql-client:" + mysqlConnection);
        // Set auto-commit to false

        try {
            mysqlConnection.setAutoCommit(false);

            int startIndex = start;

            while (startIndex < end) {
                int projectedEndIndex = startIndex + batchSize;
                int endIndex = Math.min(projectedEndIndex, end);
                rowAffected = executeBatchDeleteV2(startIndex, endIndex, limit, rowAffected, failedSellerSUPCSet, setname, wpolicy);
                startIndex = endIndex;
            }
            System.out.println("Done -- :");

        } catch (SQLException e) {
            e.printStackTrace(System.out);
            e.printStackTrace();
            System.exit(0);
        } catch (AerospikeException e) {
            e.printStackTrace(System.out);
            e.printStackTrace();
            System.exit(0);
        }
        long tEnd = System.nanoTime();
        if (DeleteRecordScript.DEBUG) {

            System.out.println(" === no of failed sellers-supc pair(s) :" + failedSellerSUPCSet.size());
            System.out.println(" === failed sellers-supc pair(s) :" + failedSellerSUPCSet);

            System.out.println("Deletion of Junk of Seller_Supc_FM_Mapping completed.");
            System.out.println("Number of records deleted = " + rowAffected);
            String msg = "Time taken: " + (tEnd - tStart) + " ns, [" + (tEnd - tStart) / 1000000 + " ms]";
            System.out.println(msg);
            System.out.println("Finished at " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        }
        if (!failedSellerSUPCSet.isEmpty()) {
            System.out.println("Note: Please re-run the script as some seller-supc deletion has failed !!! ");
        }
    }

    private int executeBatchDelete(int start, int end, int limit, int rowAffected, Set<String> failedSellerSUPCSet, final String setname, WritePolicy wpolicy) throws SQLException,
            AerospikeException {

        System.out.println(" *********** start:" + start);
        System.out.println(" ***********   end:" + end);

        boolean bUseLimit = true;
        if (limit < 1) {
            limit = end - start;
            bUseLimit = false;
        }
        long tQStart = System.nanoTime();

        //        if (true)
        //            return rowAffected;

        Statement statement = mysqlConnection.createStatement();
        String selectSQL = ""//
                + "select "//
                + " t1.id, t1.seller_code, t1.supc, t1.fulfilment_model, t1.enabled, t1.created, t1.last_updated, t1.updated_by, t1.updated" //
                + " from seller_supc_fm_mapping t1 "//
                + " left join seller_supc_mapping t2 "//
                + " on t1.seller_code = t2.seller_code and t2.supc=t1.supc "//
                + " where t2.seller_code is null "//
                + " and t1.id <=" + String.valueOf(end)//
                + " and t1.id >" + String.valueOf(start)//
                + (bUseLimit ? " limit " + limit : "");
        System.out.println("SQL:" + selectSQL);
        ResultSet resultSet = statement.executeQuery(selectSQL);
        //            ResultSet resultSet = statement.executeQuery("select "
        //                    + "sfm.seller_code, ssfm.seller_code, ssfm.supc, ssfm.fulfilment_model, ssfm.enabled, ssfm.created, ssfm.last_updated, ssfm.updated_by, ssfm.updated "
        //                    + "from seller_fm_mapping sfm " + "join seller_supc_fm_mapping ssfm " + "where ssfm.seller_code = sfm.seller_code "
        //                    + "AND BINARY ssfm.seller_code != sfm.seller_code limit 10");
        long tQEnd = System.nanoTime();

        String deleteRecordSql = "delete from seller_supc_fm_mapping where id=?";
        PreparedStatement pstmt = mysqlConnection.prepareStatement(deleteRecordSql);

        Set<String> sellerSUPCSet = new LinkedHashSet<String>();

        boolean bFoundRecords = false;
        while (resultSet.next()) {

            bFoundRecords = true;

            //read one row from mysql table
            String id = resultSet.getString("t1.id");
            String sellerCodeOld = resultSet.getString("t1.seller_code");
            String supc = resultSet.getString("t1.supc");

            sellerSUPCSet.add(id + "," + sellerCodeOld + "," + supc);
            //1) delete from AEROSPIKE (delete old)

            String keyStrOld = AerospikeKeyHelper.getKey(sellerCodeOld, supc);
            Key keyOld = new Key(NAMESPACE, setname, keyStrOld);

            try {
                System.out.println("count=" + (rowAffected + 1));
                System.out.println("deleting keyOld=" + keyOld);
                boolean delete = aerospikeClient.delete(wpolicy, keyOld);
                rowAffected++;
                System.out.println("deleted=" + delete);
                System.out.println("done");

                //2) write to MySQL (update) in batch
                pstmt.setString(1, id);
                pstmt.addBatch();
                System.out.println("sql - " + pstmt);

            } catch (AerospikeException e) {
                if (DeleteRecordScript.DEBUG) {
                    System.out.println(" === Not able to write record to aerospike. - keyStrOld:" + keyStrOld);
                    e.printStackTrace(System.out);
                    e.printStackTrace();
                }
                failedSellerSUPCSet.add(id + "," + sellerCodeOld + "," + supc);
            }

        }//end of while

        System.out.println("starting with mysql related activity.");

        if (bFoundRecords == false) {
            System.out.println("No record to delete this time -- returning.");
            return rowAffected;
        }
        System.out.println("Start with mysql batch execute .. ");
        long tCommitStart = System.nanoTime();
        //Create an int[] to hold returned values
        int[] count = pstmt.executeBatch();
        long tBatchUpdateEnd = System.nanoTime();

        System.out.println("Start with mysql commit .. ");
        mysqlConnection.commit();
        long tCommitEnd = System.nanoTime();

        System.out.println("no of sellers deleted in mysql :" + count.length);
        System.out.println("seller-supc-fm deleted in mysql :" + sellerSUPCSet);
        System.out.println("commit result :" + Arrays.toString(count));

        int index = 0;
        StringBuilder sb = new StringBuilder();
        for (String seller : sellerSUPCSet) {
            sb.append(seller + "=" + count[index++] + ", ");
        }
        System.out.println("mapped result :" + sb.toString());
        System.out.println(" === no of failed sellers-supc pair(s) :" + failedSellerSUPCSet.size());
        System.out.println(" === failed sellers-supc pair(s) :" + failedSellerSUPCSet);

        String msg = "Time taken for MySQL query execution: " + (tQEnd - tQStart) + " ns, [" + (tQEnd - tQStart) / 1000000 + " ms]";
        String msg0 = "Time taken for MySQL batch execute: " + (tBatchUpdateEnd - tCommitStart) + " ns, [" + (tBatchUpdateEnd - tCommitStart) / 1000000 + " ms]";
        String msg1 = "Time taken for MySQL commit: " + (tCommitEnd - tBatchUpdateEnd) + " ns, [" + (tCommitEnd - tBatchUpdateEnd) / 1000000 + " ms]";
        String msg2 = "Time taken for MySQL update (batch+commit): " + (tCommitEnd - tCommitStart) + " ns, [" + (tCommitEnd - tCommitStart) / 1000000 + " ms]";
        System.out.println(msg);
        System.out.println(msg0);
        System.out.println(msg1);
        System.out.println(msg2);
        return rowAffected;
    }

    private int executeBatchDeleteV2(int start, int end, int limit, int rowAffected, Set<String> failedSellerSUPCSet, final String setname, WritePolicy wpolicy)
            throws SQLException, AerospikeException {

        System.out.println(" *********** start:" + start);
        System.out.println(" ***********   end:" + end);

        boolean bUseLimit = true;
        if (limit < 1) {
            limit = end - start;
            bUseLimit = false;
        }
        long tQStart = System.nanoTime();
        int cnt = 0;

        //        if (true)
        //            return rowAffected;

        Statement statement = mysqlConnection.createStatement();
        String selectSQL = ""//
                + "select "//
                + " t1.id, t1.seller_code, t1.supc, t1.fulfilment_model, t1.enabled, t1.created, t1.last_updated, t1.updated_by, t1.updated" //
                + " from seller_supc_fm_mapping t1 "//
                + " left join seller_supc_mapping t2 "//
                + " on t1.seller_code = t2.seller_code and t2.supc=t1.supc "//
                + " where t2.seller_code is null "//
                + " and t1.id <=" + String.valueOf(end)//
                + " and t1.id >" + String.valueOf(start)//
                + (bUseLimit ? " limit " + limit : "");
        System.out.println("SQL:" + selectSQL);
        ResultSet resultSet = statement.executeQuery(selectSQL);
        //            ResultSet resultSet = statement.executeQuery("select "
        //                    + "sfm.seller_code, ssfm.seller_code, ssfm.supc, ssfm.fulfilment_model, ssfm.enabled, ssfm.created, ssfm.last_updated, ssfm.updated_by, ssfm.updated "
        //                    + "from seller_fm_mapping sfm " + "join seller_supc_fm_mapping ssfm " + "where ssfm.seller_code = sfm.seller_code "
        //                    + "AND BINARY ssfm.seller_code != sfm.seller_code limit 10");
        long tQEnd = System.nanoTime();

        Set<String> sellerSUPCSet = new LinkedHashSet<String>();

        boolean bFoundRecords = false;
        while (resultSet.next()) {

            bFoundRecords = true;

            //read one row from mysql table
            String id = resultSet.getString("t1.id");
            String sellerCodeOld = resultSet.getString("t1.seller_code");
            String supc = resultSet.getString("t1.supc");

            sellerSUPCSet.add(id + "," + sellerCodeOld + "," + supc);
            //1) delete from AEROSPIKE (delete old)

            String keyStrOld = AerospikeKeyHelper.getKey(sellerCodeOld, supc);
            Key keyOld = new Key(NAMESPACE, setname, keyStrOld);

            try {
                System.out.println("count=" + (cnt + 1) + ",count2 =" + (rowAffected + 1));
                System.out.println("deleting keyOld=" + keyOld);
                boolean delete = aerospikeClient.delete(wpolicy, keyOld);
                cnt++;
                rowAffected++;
                System.out.println("deleted=" + delete);
                System.out.println("done");

            } catch (AerospikeException e) {
                if (DeleteRecordScript.DEBUG) {
                    System.out.println(" === Not able to write record to aerospike. - keyStrOld:" + keyStrOld);
                    e.printStackTrace(System.out);
                    e.printStackTrace();
                }
                failedSellerSUPCSet.add(id + "," + sellerCodeOld + "," + supc);
            }

        }//end of while

        if (bFoundRecords == false) {
            System.out.println("No record to delete this time -- returning.");
            return rowAffected;
        }
        String msg = "Time taken for MySQL query execution: " + (tQEnd - tQStart) + " ns, [" + (tQEnd - tQStart) / 1000000 + " ms]";
        System.out.println(msg);
        
        System.out.println(" === no of failed sellers-supc pair(s) :" + failedSellerSUPCSet.size());
        System.out.println(" === failed sellers-supc pair(s) :" + failedSellerSUPCSet);

        return rowAffected;
    }
}
