package com.snapdeal.cocofs.migration.sdinstant;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.aerospike.client.AerospikeClient;
import com.aerospike.client.AerospikeException;
import com.aerospike.client.Bin;
import com.aerospike.client.Key;
import com.aerospike.client.policy.WritePolicy;
public class AerospikeMigration {


    // Parameters for Mysql data connection
    public static String          MYSQL_HOSTNAME     = "";
    public static int             MYSQL_PORT         = 0;
    public static String          MYSQL_USERNAME     = "";
    public static String          MYSQL_PASSWORD     = "";

    // Parameters for Aerospike data connection
    public static String          AEROSPIKE_HOSTNAME = "";
    public static int             AEROSPIKE_PORT     = 0;
    private static int            timeout            = 600000;

    // Program parameters
    public static boolean         DEBUG              = true;

    // Data connections
    public static AerospikeClient AEROSPIKE_CONNECTION;
    public static Connection      MYSQL_CONNECTION;

    public static void main(String[] args) throws IOException, SQLException {
        printUsage();
        processParameters();
        makeConnections();
        migrateSellerDetails();
        migrateFCDetails();
    }

    private static void makeConnections() {
        if (AerospikeMigration.DEBUG)
            System.out.println("Establishing data connections...");

        try {
            AEROSPIKE_CONNECTION = new AerospikeClient(AEROSPIKE_HOSTNAME, AEROSPIKE_PORT);
        } catch (AerospikeException ae) {
            System.out.println("Not able to make a connection with Aerospike..");
            System.out.println("Exiting now..");
            System.exit(0);
        }

        String url = "jdbc:mysql://" + MYSQL_HOSTNAME + ":" + MYSQL_PORT + "/cocofs";
        try {
            System.out.println(url);
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            MYSQL_CONNECTION = DriverManager.getConnection(url, MYSQL_USERNAME, MYSQL_PASSWORD);
        } catch (InstantiationException e) {
            e.printStackTrace();
            System.exit(0);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            System.exit(0);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            System.exit(0);
        } catch (SQLException e) {
            e.printStackTrace();
            System.exit(0);
        }

        System.out.println("Mysql and Aerospike connections successful..");
    }

    private static void migrateSellerDetails() throws SQLException {
        System.out.println("Starting migration of Seller_Details now...");
        long t1 = System.currentTimeMillis();
        int rowAffected = 0;
        try {
            Statement statement = MYSQL_CONNECTION.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM seller_contact_details");
            String setname = "seller_detail_vo";
            WritePolicy wpolicy = new WritePolicy();
            wpolicy.timeout = timeout;
            while (resultSet.next()) {
                try {
                    System.out.println("Writing seller:" + resultSet.getString("seller_code"));
                    String keystr = resultSet.getString("seller_code");
                    Key key = new Key("cocofs", setname, keystr);
                    Bin[] bins = new Bin[6];

                    bins[0] = new Bin("sc", resultSet.getString("seller_code"));
                    bins[1] = new Bin("pc", resultSet.getString("pin_code"));
                    bins[2] = new Bin("al1", resultSet.getString("address_line1"));
                    bins[3] = new Bin("al2", resultSet.getString("address_line2"));
                    bins[4] = new Bin("ci", resultSet.getString("city"));
                    bins[5] = new Bin("s", resultSet.getString("state"));
                    AEROSPIKE_CONNECTION.put(wpolicy, key, bins);
                    rowAffected++;
                } catch (Exception ex) {
                    ex.printStackTrace();
                    System.out.println("Error while writing detaills for seller:" + resultSet.getString("seller_code") + ", exception:" + ex.getMessage());
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        }
        System.out.println("Time taken for migration of seller details(in milliseconds) is: " + (System.currentTimeMillis() - t1));
        System.out.println("Migration of Seller Details completed.");
        System.out.println("Number of records inserted = " + rowAffected);

    }

    private static void migrateFCDetails() {
        System.out.println("Starting migration of FC Details now...");
        long t1 = System.currentTimeMillis();
        int rowAffected = 0;
        try {
            Statement statement = MYSQL_CONNECTION.createStatement();
            ResultSet resultSet = statement.executeQuery("select code,fc_details.name as fc_name, type, enabled,sd_instant,fc_address.name as addr_name,address_line1,address_line2, city, state, pincode, mobile, landline, dnd_active, email  from fc_details inner join fc_address on primary_address_id where fc_details.primary_address_id = fc_address.id");
            String setname = "fc_detail_vo";
            WritePolicy wpolicy = new WritePolicy();
            wpolicy.timeout = timeout;
            while (resultSet.next()) {
                try {
                    System.out.println("Writing fc detail:" + resultSet.getString("code"));
                    String keystr = resultSet.getString("code");
                    Key key = new Key("cocofs", setname, keystr);
                    Bin[] bins = new Bin[15];

                    bins[0] = new Bin("fc", resultSet.getString("code"));
                    bins[1] = new Bin("fnm", resultSet.getString("fc_name"));
                    bins[2] = new Bin("ty", resultSet.getString("type"));
                    bins[3] = new Bin("sd", resultSet.getInt("sd_instant"));
                    bins[4] = new Bin("anm", resultSet.getString("addr_name"));
                    bins[5] = new Bin("al1", resultSet.getString("address_line1"));
                    bins[6] = new Bin("al2", resultSet.getString("address_line2"));
                    bins[7] = new Bin("ci", resultSet.getString("city"));
                    bins[8] = new Bin("st", resultSet.getString("state"));
                    bins[9] = new Bin("pi", resultSet.getString("pincode"));
                    bins[10] = new Bin("mo", resultSet.getString("mobile"));
                    bins[11] = new Bin("la", resultSet.getString("landline"));
                    bins[12] = new Bin("dnd", resultSet.getInt("dnd_active"));
                    bins[13] = new Bin("em", resultSet.getString("email"));
                    bins[14] = new Bin("e", resultSet.getInt("enabled"));


                    AEROSPIKE_CONNECTION.put(wpolicy, key, bins);
                    rowAffected++;
                } catch (Exception ex) {
                    ex.printStackTrace();
                    System.out.println("Error while writing detaills for fc:" + resultSet.getString("code") + ", exception:" + ex.getMessage());
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        }
        System.out.println("Time taken for migration of fc details(in milliseconds) is: " + (System.currentTimeMillis() - t1));
        System.out.println("Migration of fc code completed.");
        System.out.println("Number of records inserted = " + rowAffected);

    }

    private static void printUsage() {
        System.out.println("<USAGE>: Fill out in params.txt placed next to jar for various configuration per line in order of \n<MYSQL_HOSTNAME>\n" + "<MYSQL_PORT>\n"
                + "<MYSQL_USERNAME>\n" + "<MYSQL_PASSWORD>\n" + "<AEROSPIKE_HOSTNAME>\n" + "<AEROSPIKE_PORT>\n");
        System.out.println("java -jar <JARNAME>");
    }

    private static void processParameters() throws IOException {
        if (AerospikeMigration.DEBUG)
            System.out.println("Processing input parameters now..");
        BufferedReader reader = new BufferedReader(new FileReader("params.txt"));
        MYSQL_HOSTNAME = reader.readLine();
        MYSQL_PORT = Integer.parseInt(reader.readLine());
        MYSQL_USERNAME = reader.readLine();
        MYSQL_PASSWORD = reader.readLine();

        AEROSPIKE_HOSTNAME = reader.readLine();
        AEROSPIKE_PORT = Integer.parseInt(reader.readLine());

        if (AerospikeMigration.DEBUG) {

            System.out.println("MYSQL_HOSTNAME = " + MYSQL_HOSTNAME);
            System.out.println("MYSQL_PORT = " + MYSQL_PORT);
            System.out.println("MYSQL_USERNAME = " + MYSQL_USERNAME);

            System.out.println("AEROSPIKE_HOSTNAME = " + AEROSPIKE_HOSTNAME);
            System.out.println("AEROSPIKE_PORT = " + AEROSPIKE_PORT);

        }

    }


}
