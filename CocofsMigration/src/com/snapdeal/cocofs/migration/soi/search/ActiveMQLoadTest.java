package com.snapdeal.cocofs.migration.soi.search;

import java.util.ArrayList;
import java.util.List;

import com.snapdeal.cocofs.sro.SellerSupcUpdateSRO;

public class ActiveMQLoadTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		PublishExternalNotificationsServiceImpl searchPublisher;
		searchPublisher = new PublishExternalNotificationsServiceImpl();
		searchPublisher.setActiveMQManager(new ActiveMQManager());
		searchPublisher.registerProducer("test_queue", "tcp://127.0.0.1:61616", "admin", "admin");
		int itemCount = 20000000;
		
		for (int i = 0; i < itemCount/10000; i++) {
			List<SellerSupcUpdateSRO> supcMessages = new ArrayList<SellerSupcUpdateSRO>();

			for(int j = 0;j<10000;j++){
				String supc = "Oe122e"+i+j;
				supcMessages.add(createUpdateMessage("sellerABC",supc,"FC_VOI"));
			}
			searchPublisher.publishToSearch(supcMessages);
		}
	}
	
	private static SellerSupcUpdateSRO createUpdateMessage(String sellerCode,
			String supc, String fm) {
		SellerSupcUpdateSRO updateMessage = new SellerSupcUpdateSRO();
		updateMessage.setSdFulfilled(fm.equalsIgnoreCase("FC_VOI") ? true : false);
		updateMessage.setSellerCode(sellerCode);
		updateMessage.setSupc(supc);
		return updateMessage;
	}
	
	


}
