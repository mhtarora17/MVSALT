package com.snapdeal.cocofs.migration.soi.search;


import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.commons.lang.math.RandomUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ActiveMQManager

{
	private static final Logger LOG = LoggerFactory
			.getLogger(ActiveMQManager.class);

	public ActiveMQManager() {
		this.tokenToPublisherQueue = new HashMap();
		this.tokenToSubscriberQueue = new HashMap();
	}

	private class ActiveMQProducer {
		private final Session session;
		private final MessageProducer messageProducer;
		private final Connection connection;

		public ActiveMQProducer(Session session, MessageProducer producer,
				Connection connection) {
			this.session = session;
			this.messageProducer = producer;
			this.connection = connection;
		}

		public Session getSession() {
			return this.session;
		}

		public MessageProducer getMessageProducer() {
			return this.messageProducer;
		}

		public Connection getConnection() {
			return this.connection;
		}
	}

	private class ActiveMQConsumer {
		private final MessageConsumer consumer;
		private final Session session;
		private final Connection connection;

		public ActiveMQConsumer(MessageConsumer consumer, Session session,
				Connection connection) {
			this.consumer = consumer;
			this.session = session;
			this.connection = connection;

		}

		public MessageConsumer getConsumer() {
			return this.consumer;
		}

		public Session getSession() {
			return this.session;
		}

		public Connection getConnection() {
			return this.connection;
		}
	}

	public Long registerPublisher(String queueName, String url,
			String userName, String password) throws JMSException {
		return registerPublisher(queueName, url, userName, password, false, 1,
				2);
	}

	Map<Long, ActiveMQProducer> tokenToPublisherQueue;

	Map<Long, ActiveMQConsumer> tokenToSubscriberQueue;

	public Long registerPublisher(String queueName, String url,
			String userName, String password, boolean transacted,
			int acknowledgeMode, int deliveryMode) throws JMSException {
		Connection connection = null;
		Session session = null;
		MessageProducer producer = null;
		try {
			connection = getConnection(userName, password, url);
			connection.start();
			session = connection.createSession(transacted, acknowledgeMode);
			Destination destination = session.createQueue(queueName);
			producer = session.createProducer(destination);
			producer.setDeliveryMode(deliveryMode);
			Long token = Long.valueOf(RandomUtils.nextLong());
			ActiveMQProducer activeMQProducer = new ActiveMQProducer(session,
					producer, connection);
			this.tokenToPublisherQueue.put(token, activeMQProducer);
			return token;
		} catch (JMSException e) {
			close(producer, session, connection);
			throw e;
		} catch (RuntimeException e) {
			close(producer, session, connection);
			throw e;
		}
	}

	public void unregisterPublisher(Long token) throws JMSException {
		ActiveMQProducer activeMQProducer = (ActiveMQProducer) this.tokenToPublisherQueue
				.get(token);
		if (activeMQProducer != null) {
			close(activeMQProducer.getMessageProducer(),
					activeMQProducer.getSession(),
					activeMQProducer.getConnection());
		}
		this.tokenToPublisherQueue.remove(token);
	}

	public <T extends Serializable> void publish(Long token, List<T> messages)
			throws  JMSException {
		ActiveMQProducer activeMQProducer = (ActiveMQProducer) this.tokenToPublisherQueue
				.get(token);
		if (activeMQProducer == null) {
			throw new JMSException("Producer not registered");
		}

		for (Serializable message : messages) {
			ObjectMessage obj = activeMQProducer.getSession()
					.createObjectMessage(message);
			activeMQProducer.getMessageProducer().send(obj);
		}
	}

	public <T extends Serializable> void publish(Long token, T message)
			throws JMSException {
		ActiveMQProducer activeMQProducer = (ActiveMQProducer) this.tokenToPublisherQueue
				.get(token);
		if (activeMQProducer == null) {
			throw new JMSException("Producer not registered");
		}

		ObjectMessage obj = activeMQProducer.getSession().createObjectMessage(
				message);
		activeMQProducer.getMessageProducer().send(obj);
	}

	public Long registerSubscriber(String queueName, String url,
			String userName, String password, MessageListener listener)
			throws JMSException {
		return registerSubscriber(queueName, url, userName, password, listener,
				false, 1);
	}

	public Long registerSubscriber(String queueName, String url,
			String userName, String password, MessageListener listener,
			boolean transacted, int acknowledgeMode) throws JMSException {
		Connection connection = null;
		Session session = null;
		MessageConsumer consumer = null;
		try {
			connection = getConnection(userName, password, url);
			connection.start();
			session = connection.createSession(transacted, acknowledgeMode);
			Destination destination = createQueue(session, queueName);

			consumer = session.createConsumer(destination);
			consumer.setMessageListener(listener);
			Long token = Long.valueOf(RandomUtils.nextLong());
			ActiveMQConsumer activeMQConsumer = new ActiveMQConsumer(consumer,
					session, connection);
			this.tokenToSubscriberQueue.put(token, activeMQConsumer);
			return token;
		} catch (JMSException e) {
			close(consumer, session, connection);
			throw e;
		} catch (RuntimeException e) {
			close(consumer, session, connection);
			throw e;
		}
	}

	public void unregisterSubscriber(Long token) throws JMSException {
		ActiveMQConsumer activeMQConsumer = (ActiveMQConsumer) this.tokenToSubscriberQueue
				.get(token);
		if (activeMQConsumer != null) {
			close(activeMQConsumer.getConsumer(),
					activeMQConsumer.getSession(),
					activeMQConsumer.getConnection());
		}
		this.tokenToSubscriberQueue.remove(token);
	}

	private Connection getConnection(String userName, String password,
			String url) throws JMSException {
		ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(
				userName, password, url);
		return connectionFactory.createConnection();
	}

	private Destination createQueue(Session session, String queueName)
			throws JMSException {
		return session.createQueue(queueName);
	}

	private void close(MessageConsumer consumer, Session session,
			Connection connection) {
		if (consumer != null) {
			try {
				consumer.setMessageListener(null);
				consumer.close();
			} catch (Exception e) {
				//System.out.println("Error in closing consumer"+ e);
			}
		}
		close(session, connection);
	}

	private void close(MessageProducer producer, Session session,
			Connection connection) {
		if (producer != null) {
			try {
				producer.close();
			} catch (Exception e) {
				//System.out.println("Error in closing producer"+ e);
			}
		}
		close(session, connection);
	}

	private void close(Session session, Connection connection) {
		if (session != null) {
			try {
				session.close();
			} catch (Exception e) {
				//System.out.println("Error in closing session"+ e);
			}
		}
		if (connection != null) {
			try {
				connection.close();
			} catch (Exception e) {
				//System.out.println("Error in closing connection"+ e);
			}
		}
	}
}
