package com.snapdeal.cocofs.migration.soi.search;

import java.util.ArrayList;
import java.util.List;

import javax.jms.JMSException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.snapdeal.cocofs.sro.SellerSupcUpdateEventsSRO;
import com.snapdeal.cocofs.sro.SellerSupcUpdateSRO;

public class PublishExternalNotificationsServiceImpl {

	private static Long activeMqToken = null;
	private String queueName = null;
	private String queueURL = null;
	private ActiveMQManager activeMQManager;
	private static final Logger LOG = LoggerFactory
			.getLogger(PublishExternalNotificationsServiceImpl.class);

	public void registerProducer(String queueName, String networkPath,
			String username, String password) {

		this.queueName = queueName;
		this.queueURL = networkPath;

		try {
			if (activeMqToken == null) {
				activeMqToken = activeMQManager.registerPublisher(queueName,
						networkPath, username, password);
				System.out.println("Active mq producer registered successfully  ...");
			} else {
				System.out.println("Active mq producer was already registered ... not attempting registration");
			}
		} catch (Exception e) {
			LOG.error(
					"unable to register external-notification-service producer to activemq. "
							+ "attempted with queueName:" + queueName
							+ ", queueURL:" + queueURL + ", username"
							+ username, e);
		}
	}

	public void unregisterProducer() {
		try {
			if (activeMqToken != null) {
				activeMQManager.unregisterPublisher(activeMqToken);
				System.out.println("Active mq producer unregistered successfully  ...");
			}
			activeMqToken = null;
		} catch (Exception e) {
			LOG.error(
					"unable to unregister external-notification-service producer from activemq",
					e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.snapdeal.cocofs.services.producer.IPublishExternalNotificationsService
	 * #publishToSearch(com.snapdeal.cocofs.sro.SellerSupcUpdateEventsSRO)
	 */
	public void publishToSearch(List<SellerSupcUpdateSRO> msg) {

		//System.out.println("publishing to search started ... items:" + msg.size());

		if (!msg.isEmpty()) {

			// get no of records to send in 1 go , accordingly packetize and
			// send

			int maxPublishSize = 100;// ConfigUtils.getIntegerScalar(Property.ACTIVE_MQ_SEARCH_PUSH_ITEM_COUNT);
			int startIndex = 0;
			while (startIndex < msg.size()) {
				int projectedEndIndex = startIndex + maxPublishSize;
				int endIndex = Math.min(projectedEndIndex, msg.size());
				List<SellerSupcUpdateSRO> publishList = new ArrayList<SellerSupcUpdateSRO>(msg.subList(startIndex,
						endIndex));
				SellerSupcUpdateEventsSRO event = new SellerSupcUpdateEventsSRO();
				event.setSellerSupcUpdateList(publishList);

				try {
					activeMQManager.publish(activeMqToken, event);
					//System.out.println("Published updates on url: " + queueURL
						//	+ " with update event: " + event + " on queue: "
							//+ queueName);
				} catch (JMSException e) {
					System.out.println("Error publishing on url: " + queueURL
							+ " with update event: " + event + " on queue: "
							+ queueName);
					System.out.println("Exception "+ e.getMessage());
				} catch (Exception e) {
					System.out.println("Unable to publish on url: " + queueURL
							+ " with update event: " + event + " on queue: "
							+ queueName);
					System.out.println("Exception "+ e.getMessage());
				}
				startIndex = endIndex;
			}
		}
		//System.out.println("publishing to search done ...");

	}

	public void setActiveMQManager(ActiveMQManager activeMQManager) {
		this.activeMQManager = activeMQManager;
	}

}
