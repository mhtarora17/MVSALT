package com.snapdeal.cocofs.migration.soi.search;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.snapdeal.cocofs.sro.SellerSupcUpdateSRO;

public class Migrator {

	private PublishExternalNotificationsServiceImpl searchPublisher;
	private Connection cocofsMysqlConnection;
	private final String SD_FM = "FC_VOI";

	public Migrator(PublishExternalNotificationsServiceImpl searchPublisher,
			Connection cocofsMysqlConnection) {
		this.searchPublisher = searchPublisher;
		this.cocofsMysqlConnection = cocofsMysqlConnection;
	}

	public void migrateData() {
		// Fetch all the distinct sellercodes from IPMS database
		Statement statement;
		ResultSet resultSet = null;
		String query = "SELECT seller_code FROM seller_fm_mapping where fulfilment_model='"+SD_FM+"'";
		int sellerCount = 0;
		
		try {
			
			long start = System.currentTimeMillis();
			int exceptionRows = 0;
			int a2Rows = 0;
			// First fetch exceptional supc and fulfilment models and
			// push
			// to search queue
			String exceptionSupcQuery = "SELECT seller_code, supc from seller_supc_fm_mapping where enabled = 1 and fulfilment_model='"+SD_FM+"'";

			Statement exceptionStatement = cocofsMysqlConnection
					.createStatement();
			ResultSet exceptionResultSet = exceptionStatement
					.executeQuery(exceptionSupcQuery);

			Map<String, Boolean> supcToMsgSentMap = new HashMap<String, Boolean>();
			List<SellerSupcUpdateSRO> messages = new ArrayList<SellerSupcUpdateSRO>();
			System.out.println("Starting migration of exceptions...");
			// Push messages to search
			while (exceptionResultSet.next()) {
				exceptionRows++;
				String supc = exceptionResultSet.getString("supc");
				String exSellerCode = exceptionResultSet.getString("seller_code");
					messages.add(createUpdateMessage(exSellerCode, supc,
							SD_FM));
					supcToMsgSentMap.put(supc, true);
			}
			sendMessages(messages);

			System.out.println("Number of exception rows migrated:"+exceptionRows);
			System.out.println("Starting migration now...");
			statement = cocofsMysqlConnection.createStatement();

			resultSet = statement.executeQuery(query);				
			
			while (resultSet.next()) {
				sellerCount++;
				String sellerCode = resultSet.getString("seller_code");
				try {
					Statement sellerSupcsStatement = cocofsMysqlConnection.createStatement();
					// Fetch all supcs for this seller from aerospike largeset
					String sellerSupcQuery = "SELECT distinct(supc) from seller_supc_mapping where seller_code='"+sellerCode+"' and supc not in (select supc from seller_supc_fm_mapping where seller_code='"+sellerCode+"')";
					//System.out.println(sellerSupcQuery);
					ResultSet sellerSupcsSet = sellerSupcsStatement.executeQuery(sellerSupcQuery);
					List<SellerSupcUpdateSRO> supcMessages = new ArrayList<SellerSupcUpdateSRO>();
					boolean send = false;
					while(sellerSupcsSet.next()){
						send=true;
						String supc = sellerSupcsSet.getString("supc");
//						// check if an exception message for this supc
//						// has
//						// already been sent before
//						if (!supcToMsgSentMap.containsKey(supc)) {
								supcMessages.add(createUpdateMessage(
										sellerCode, supc, SD_FM));

//						}
					}
					if(send){
						sendMessages(supcMessages);
						a2Rows+=supcMessages.size();
						System.out.println("Sending messages for seller:"+sellerCode+" size:"+supcMessages.size());
					}
					
				} catch (Exception e) {
					System.out.println("Unable to process seller code:"
							+ sellerCode + ". Ignoring and moving further..");
					e.printStackTrace();
				}
			}
			start = System.currentTimeMillis() - start;
			System.out.println("Number of sellers catered:"+sellerCount+" Number of rows tranferred: "+a2Rows+" Total time taken:"+(double)start/1000.0+"s");
			shutdown();
		} catch (SQLException e) {
			System.out
					.println("Not able to fetch seller codes from db. Exiting now..");
			e.printStackTrace();
			System.exit(0);
		}

	}

	private void shutdown(){
		try {
			cocofsMysqlConnection.close();
			searchPublisher.unregisterProducer();
			System.exit(0);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	private void sendMessages(List<SellerSupcUpdateSRO> messages) {
		if (messages.size() > 0) {
			searchPublisher.publishToSearch(messages);
		}
	}

	private SellerSupcUpdateSRO createUpdateMessage(String sellerCode,
			String supc, String fm) {
		SellerSupcUpdateSRO updateMessage = new SellerSupcUpdateSRO();
		updateMessage.setSdFulfilled(true);
		updateMessage.setSellerCode(sellerCode);
		updateMessage.setSupc(supc);
		return updateMessage;
	}
}
