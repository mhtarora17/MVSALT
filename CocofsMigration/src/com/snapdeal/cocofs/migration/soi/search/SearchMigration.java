package com.snapdeal.cocofs.migration.soi.search;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.aerospike.client.AerospikeClient;
import com.aerospike.client.AerospikeException;
import com.aerospike.client.policy.ClientPolicy;


public class SearchMigration {

	 
    //Parameters for Search ActiveMQ queue
	public static String ACTIVEMQ_NETWORK_PATH = "";
	public static String ACTIVEMQ_QUEUE = "";
	public static String ACTIVEMQ_USERNAME = "";
	public static String ACTIVEMQ_PASSWORD = "";

    //Parameters for COCOFS Mysql data connection
	public static String COCOFS_MYSQL_HOSTNAME = "";
	public static int COCOFS_MYSQL_PORT = 0;
	public static String COCOFS_MYSQL_USERNAME = "";
	public static String COCOFS_MYSQL_PASSWORD = "";
	
	//Program parameters
	public static boolean DEBUG = true;
	
	//Data connections
	public static PublishExternalNotificationsServiceImpl searchPublisher;
	public static Connection COCOFS_MYSQL_CONNECTION;

	public static void main(String[] args) {
		if(args == null || args.length < 8){
			printUsage();
			return;
		}
		processParameters(args);
		makeConnections();
		Migrator migrator = new Migrator(searchPublisher, COCOFS_MYSQL_CONNECTION);
		migrator.migrateData();
		
	}
	
	private static void makeConnections(){
		System.out.println("Establishing data connections...");		
		String cocofsUrl = "jdbc:mysql://"+COCOFS_MYSQL_HOSTNAME+":"+COCOFS_MYSQL_PORT+"/cocofs";
	    
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			COCOFS_MYSQL_CONNECTION = DriverManager.getConnection(cocofsUrl, COCOFS_MYSQL_USERNAME, COCOFS_MYSQL_PASSWORD);

		} catch (InstantiationException e) {
			e.printStackTrace();
			System.exit(0);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			System.exit(0);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			System.exit(0);
		} catch (SQLException e) {
			e.printStackTrace();
			System.exit(0);
		}
		
		searchPublisher = new PublishExternalNotificationsServiceImpl();
		searchPublisher.setActiveMQManager(new ActiveMQManager());
		searchPublisher.registerProducer(ACTIVEMQ_QUEUE, ACTIVEMQ_NETWORK_PATH, ACTIVEMQ_USERNAME, ACTIVEMQ_PASSWORD);
       
		System.out.println("Mysql and ActiveMQ connections successful..");
	}
	
	private static void printUsage(){
		System.out.println("<USAGE>:");
		System.out.println("java -jar <JARNAME> <ACTIVEMQ_NETWORK_PATH> <ACTIVEMQ_QUEUE> <ACTIVEMQ_USERNAME> <ACTIVEMQ_PASSWORD>" +
				" <COCOFS_MYSQL_HOSTNAME> <COCOFS_MYSQL_PORT> <COCOFS_MYSQL_USERNAME> <COCOFS_MYSQL_PASSWORD> ");
		System.out.println("For any param not available add ? in its place.");
	}
	
	private static void processParameters(String[] args){
		if(DEBUG)
			System.out.println("Processing input parameters now..");
		ACTIVEMQ_NETWORK_PATH = normalize(args[0]);
		ACTIVEMQ_QUEUE = normalize(args[1]);
		ACTIVEMQ_USERNAME = normalize(args[2]);
		ACTIVEMQ_PASSWORD = normalize(args[3]);
		
		COCOFS_MYSQL_HOSTNAME = normalize(args[4]);
		COCOFS_MYSQL_PORT = Integer.parseInt(normalize(args[5]));
		COCOFS_MYSQL_USERNAME = normalize(args[6]);
		COCOFS_MYSQL_PASSWORD = normalize(args[7]);
		
		if(DEBUG){
			System.out.println("ACTIVEMQ_NETWORK_PATH = "+ACTIVEMQ_NETWORK_PATH);
			System.out.println("ACTIVEMQ_QUEUE = "+ACTIVEMQ_QUEUE);
			System.out.println("ACTIVEMQ_USERNAME = "+ACTIVEMQ_USERNAME);
			
			System.out.println("COCOFS_MYSQL_HOSTNAME = "+COCOFS_MYSQL_HOSTNAME);
			System.out.println("COCOFS_MYSQL_PORT = "+COCOFS_MYSQL_PORT);
			System.out.println("COCOFS_MYSQL_USERNAME = "+COCOFS_MYSQL_USERNAME);
				
		}
		
	}
	
	private static String normalize(String param){
		if(param.trim().equalsIgnoreCase("?")){
			return "0";
		}
		return param.trim();
	}

}
