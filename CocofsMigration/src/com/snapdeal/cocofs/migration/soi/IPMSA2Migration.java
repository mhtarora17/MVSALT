package com.snapdeal.cocofs.migration.soi;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.aerospike.client.AerospikeClient;
import com.aerospike.client.AerospikeException;
import com.aerospike.client.policy.ClientPolicy;

public class IPMSA2Migration {

    
    //Parameters for IPMS Mysql data connection
	public static String IPMS_MYSQL_HOSTNAME = "";
	public static int IPMS_MYSQL_PORT = 0;
	public static String IPMS_MYSQL_USERNAME = "";
	public static String IPMS_MYSQL_PASSWORD = "";

    //Parameters for COCOFS Mysql data connection
	public static String COCOFS_MYSQL_HOSTNAME = "";
	public static int COCOFS_MYSQL_PORT = 0;
	public static String COCOFS_MYSQL_USERNAME = "";
	public static String COCOFS_MYSQL_PASSWORD = "";


	//Parameters for Aerospike data connection
	public static String AEROSPIKE_HOSTNAME = "";
	public static int AEROSPIKE_PORT = 0;
	public static String AEROSPIKE_USERNAME = "";
	public static String AEROSPIKE_PASSWORD = "";

	//Optional Parameters
	public static String NAMESPACE = "cocofs_ldt";
	public static String SETNAME = "seller_to_supc_list_vo";
	public static int TIMEOUT = Integer.MAX_VALUE;
	public static int STARTID = 0;
	public static int ENDID = Integer.MAX_VALUE;
	
	//Program parameters
	public static boolean DEBUG = true;
	
	//Data connections
	public static AerospikeClient AEROSPIKE_CONNECTION;
	public static Connection IPMS_MYSQL_CONNECTION;
	public static Connection COCOFS_MYSQL_CONNECTION;
	
	public static void main(String[] args) {
		if(args == null || args.length < 14){
			printUsage();
			return;
		}
		processParameters(args);
		makeConnections();
		RowsToLargeSetMigrator migrator = new RowsToLargeSetMigrator(AEROSPIKE_CONNECTION, IPMS_MYSQL_CONNECTION, COCOFS_MYSQL_CONNECTION);
		migrator.migrateData(NAMESPACE, SETNAME, TIMEOUT,STARTID, ENDID);
		
	}
	
	private static void makeConnections(){
		if(DEBUG)
			System.out.println("Establishing data connections...");
		ClientPolicy clientPolicy = new ClientPolicy();
		clientPolicy.user = AEROSPIKE_USERNAME;
		clientPolicy.password = AEROSPIKE_PASSWORD;
		
		try {
			AEROSPIKE_CONNECTION = new AerospikeClient(clientPolicy, AEROSPIKE_HOSTNAME, AEROSPIKE_PORT);
		} catch (AerospikeException e) {
			System.out.println("An exception occured while making connnection to aerospike cluster.");
		}
		System.out.println("Retrying without username and password...");
		
		try {
			AEROSPIKE_CONNECTION = new AerospikeClient( AEROSPIKE_HOSTNAME, AEROSPIKE_PORT);
		} catch (AerospikeException e) {
			System.out.println("Still not able to make a connection with Aerospike..");
			System.out.println("Exiting now..");
			System.exit(0);
		}
		
		String ipmsUrl = "jdbc:mysql://"+IPMS_MYSQL_HOSTNAME+":"+IPMS_MYSQL_PORT+"/snapdeal_ipms";
		String cocofsUrl = "jdbc:mysql://"+COCOFS_MYSQL_HOSTNAME+":"+COCOFS_MYSQL_PORT+"/cocofs";
	    
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			IPMS_MYSQL_CONNECTION = DriverManager.getConnection(ipmsUrl, IPMS_MYSQL_USERNAME, IPMS_MYSQL_PASSWORD);
			COCOFS_MYSQL_CONNECTION = DriverManager.getConnection(cocofsUrl, COCOFS_MYSQL_USERNAME, COCOFS_MYSQL_PASSWORD);

		} catch (InstantiationException e) {
			e.printStackTrace();
			System.exit(0);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			System.exit(0);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			System.exit(0);
		} catch (SQLException e) {
			e.printStackTrace();
			System.exit(0);
		}
       
		System.out.println("Mysql and Aerospike connections successful..");
	}
	
	private static void printUsage(){
		System.out.println("<USAGE>:");
		System.out.println("java -jar <JARNAME> <IPMS_MYSQL_HOSTNAME> <IPMS_MYSQL_PORT> <IPMS_MYSQL_USERNAME> <IPMS_MYSQL_PASSWORD> <COCOFS_MYSQL_HOSTNAME> <COCOFS_MYSQL_PORT> <COCOFS_MYSQL_USERNAME> <COCOFS_MYSQL_PASSWORD> " +
				"<AEROSPIKE_HOSTNAME> <AEROSPIKE_PORT> <AEROSPIKE_USERNAME> <STARTID> <ENDID>" +
				"<AEROSPIKE_PASSWORD>");
		System.out.println("For any param not available add ? in its place.");
	}
	
	private static void processParameters(String[] args){
		if(DEBUG)
			System.out.println("Processing input parameters now..");
		
		IPMS_MYSQL_HOSTNAME = normalize(args[0]);
		IPMS_MYSQL_PORT = Integer.parseInt(normalize(args[1]));
		IPMS_MYSQL_USERNAME = normalize(args[2]);
		IPMS_MYSQL_PASSWORD = normalize(args[3]);
		
		COCOFS_MYSQL_HOSTNAME = normalize(args[4]);
		COCOFS_MYSQL_PORT = Integer.parseInt(normalize(args[5]));
		COCOFS_MYSQL_USERNAME = normalize(args[6]);
		COCOFS_MYSQL_PASSWORD = normalize(args[7]);
		
		AEROSPIKE_HOSTNAME = normalize(args[8]);
		AEROSPIKE_PORT = Integer.parseInt(normalize(args[9]));
		AEROSPIKE_USERNAME = normalize(args[10]);
		AEROSPIKE_PASSWORD = normalize(args[11]);
		try{
			STARTID = args[12].trim().equalsIgnoreCase("?")?0:Integer.parseInt(args[12].trim());
			ENDID = args[13].trim().equalsIgnoreCase("?")?Integer.MAX_VALUE:Integer.parseInt(args[13].trim());
		}catch(Exception ex){
			System.out.println("Unable to read index values. Using defaults...");
			STARTID = 0;
			ENDID = Integer.MAX_VALUE;
		}
		
		if(DEBUG){
			
			System.out.println("IPMS_MYSQL_HOSTNAME = "+IPMS_MYSQL_HOSTNAME);
			System.out.println("IPMS_MYSQL_PORT = "+IPMS_MYSQL_PORT);
			System.out.println("IPMS_MYSQL_USERNAME = "+IPMS_MYSQL_USERNAME);
			System.out.println("IPMS_MYSQL_PASSWORD = "+IPMS_MYSQL_PASSWORD);
			
			System.out.println("COCOFS_MYSQL_HOSTNAME = "+COCOFS_MYSQL_HOSTNAME);
			System.out.println("COCOFS_MYSQL_PORT = "+COCOFS_MYSQL_PORT);
			System.out.println("COCOFS_MYSQL_USERNAME = "+COCOFS_MYSQL_USERNAME);
			System.out.println("COCOFS_MYSQL_PASSWORD = "+COCOFS_MYSQL_PASSWORD);
			
			System.out.println("AEROSPIKE_HOSTNAME = "+AEROSPIKE_HOSTNAME);
			System.out.println("AEROSPIKE_PORT = "+AEROSPIKE_PORT);
			System.out.println("AEROSPIKE_USERNAME = "+AEROSPIKE_USERNAME);
			System.out.println("AEROSPIKE_PASSWORD = "+AEROSPIKE_PASSWORD);
			System.out.println("STARTID = "+STARTID);
			System.out.println("ENDID = "+ENDID);
			
				
		}
		
	}
	
	private static String normalize(String param){
		if(param.trim().equalsIgnoreCase("?")){
			return "0";
		}
		return param.trim();
	}
}
