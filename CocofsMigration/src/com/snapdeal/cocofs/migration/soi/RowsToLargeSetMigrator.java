package com.snapdeal.cocofs.migration.soi;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.aerospike.client.AerospikeClient;
import com.aerospike.client.AerospikeException;
import com.aerospike.client.Key;
import com.aerospike.client.Value;
import com.aerospike.client.large.LargeSet;
import com.aerospike.client.policy.Policy;
import com.aerospike.client.policy.WritePolicy;

public class RowsToLargeSetMigrator {
	private AerospikeClient aerospikeClient;
	private Connection ipmsMysqlConnection;
	private Connection cocofsMysqlConnection;
	public final String SELLER_SUPC_LIST_BIN_NAME = "ssls";
	public final String SELLER_SUPC_EXECPTIONAL_LIST_BIN_NAME = "ssels";
	private int subRecSize = 100;

	public RowsToLargeSetMigrator(AerospikeClient aerospikeClient,
			Connection ipmsMysqlConnection, Connection cocofsMysqlConnection) {
		this.aerospikeClient = aerospikeClient;
		this.ipmsMysqlConnection = ipmsMysqlConnection;
		this.cocofsMysqlConnection = cocofsMysqlConnection;
	}

	public void migrateData(String a2NameSpace, String a2SetName, int timeout,
			int startId, int endId) {

		// Fetch all the distinct sellercodes from COCOFS database
		Statement statement;
		ResultSet resultSet = null;
		String query = "SELECT distinct "
				+ COCOFS_SELLER_SUPC_SCHEMA.seller_code.toString()
				+ " FROM seller_fm_mapping where id >= " + startId
				+ " and id <= " + endId +" order by id asc";

		System.out
				.println("Fetching distinct sellercodes from COCOFS using query: "
						+ query);
		try {
			statement = cocofsMysqlConnection.createStatement();
			resultSet = statement.executeQuery(query);

			Policy wpolicy = new WritePolicy();
			wpolicy.timeout = timeout;
			wpolicy.maxRetries = 2;
			wpolicy.sleepBetweenRetries = 1;
			int seller_count = 0;

			System.out.println("Seller codes fetched from COCOFS..");
			System.out.println("Inserting data into A2..");
			long startTime = System.currentTimeMillis();
			while (resultSet.next()) {
				String sellerCode = resultSet
						.getString(COCOFS_SELLER_SUPC_SCHEMA.seller_code
								.toString());
				try {
					int rowCount = 0;
					int rowCount2 = 0;

					Key sellerKey = new Key(a2NameSpace, a2SetName, sellerCode);
					long sellerStartTime = System.currentTimeMillis();
					// for each seller code fetch all supcs from IPMS db
					Statement statementIpms = ipmsMysqlConnection
							.createStatement();
					String supcQuery = "SELECT "
							+ IPMS_SELLER_SUPC_SCHEMA.supc.toString()
							+ " FROM vendor_inventory_pricing where "
							+ IPMS_SELLER_SUPC_SCHEMA.vendor_code.toString()
							+ "='" + sellerCode + "'";
					ResultSet supcSet = statementIpms.executeQuery(supcQuery);

					LargeSet largeSupcSet = aerospikeClient
							.getLargeSet(wpolicy, sellerKey,
									SELLER_SUPC_LIST_BIN_NAME, null);

					// insert list of supcs
					Set<Value> supcA2Set = new HashSet<Value>();
					while (supcSet.next()) {
						rowCount++;
						String supc = supcSet
								.getString(IPMS_SELLER_SUPC_SCHEMA.supc
										.toString());
						supcA2Set.add(Value.get(supc));

					}
					sendToAerospike(supcA2Set, largeSupcSet, sellerCode);

					sellerStartTime = System.currentTimeMillis()
							- sellerStartTime;
					seller_count ++;
					System.out.println(seller_count+". Inserted " + (rowCount + rowCount2)
							+ "(IPMS:" + rowCount + ") SUPCs for seller:" + sellerCode
							+ " Time taken:" + sellerStartTime + "ms.");
				} catch (SQLException e) {
					System.out
							.println("Not able to create statement to fetch seller codes from db..Exiting now.");
					System.exit(0);
					e.printStackTrace();
				} catch (AerospikeException e) {
					System.out.println("Not able to insert SUPCs for seller:"
							+ sellerCode + ". Hence ignoring this sellercode.");
					e.printStackTrace();
				}

			}
			startTime = System.currentTimeMillis() - startTime;
			System.out.println("Data insertion completed. Total time taken:"
					+ ((double) startTime / 1000.0) + "s");

		} catch (SQLException e) {
			System.out
					.println("Not able to create statement to fetch seller codes from db..Exiting now.");
			System.exit(0);
			e.printStackTrace();
		}
	}

	private void sendToAerospike(Set<Value> supcSet, LargeSet largeSupcSet,
			String sellerCode) {
		try {
			if (supcSet.size() > 0) {
				largeSupcSet.add(new ArrayList<Value>(supcSet));
				// List<Value> supcsList = new ArrayList<Value>(supcSet);
				// int next = 0;
				// int batchSize = subRecSize;
				// int size = supcSet.size();
				// while (size > 0) {
				// batchSize = subRecSize > size ? size : subRecSize;
				// size -= subRecSize;
				// largeSupcSet.add(new ArrayList<Value>(supcsList.subList(
				// next, next + batchSize)));
				// next += batchSize;
				// }
			}

		} catch (AerospikeException aex) {
			System.out.println("Trying sequential inserts (" + sellerCode
					+ ") exception :" + aex.getMessage());
			for (Value supc : supcSet) {
				try {

					largeSupcSet.add(supc);

				} catch (AerospikeException aaex) {

				}
			}

		}
	}
}
