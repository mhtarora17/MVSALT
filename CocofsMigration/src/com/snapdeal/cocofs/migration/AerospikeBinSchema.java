package com.snapdeal.cocofs.migration;

/*
 * SCHEMA DEFINITION FOR AEROSPIKE SETS 
 */

public enum AerospikeBinSchema {

}

enum SELLER_SUPC_BINS {
	ssc, sc, su, fm, e, c, u
}

enum SELLER_BINS {	
	sc, fm, e, c, u, se, sce
}

enum SELLER_SUBCAT_BINS{
	ssc, sc, sub, fm, e, c, u
}
