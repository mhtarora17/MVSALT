package com.snapdeal.cocofs.migration;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Date;

import com.aerospike.client.AerospikeClient;
import com.aerospike.client.AerospikeException;
import com.aerospike.client.Bin;
import com.aerospike.client.Key;
import com.aerospike.client.policy.WritePolicy;

public class DataMigrator {

    private AerospikeClient aerospikeClient;
    private Connection      mysqlConnection;
    private final String    NAMESPACE = "cocofs";
    private int timeout = 600000;
    
    //private final String NAMESPACE = "cocofs_test2";
    public DataMigrator(AerospikeClient aerospikeClient, Connection mysqlConnection) {
        this.aerospikeClient = aerospikeClient;
        this.mysqlConnection = mysqlConnection;
    }

    public void migrateSellerFmMapping() {
        if(Migration.DEBUG){
            System.out.println("Starting migration of Seller_FM_Mapping now...");
        }
        int rowAffected = 0;
        try {
            Statement statement = mysqlConnection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM seller_fm_mapping");
            String setname = "seller_fm_mapping_vo";
            WritePolicy wpolicy = new WritePolicy();
            wpolicy.timeout = timeout;
            while (resultSet.next()) {

                //read one row from mysql table
                String sellerCode = resultSet.getString(SELLER_SCHEMA.seller_code.toString());
                String fulfilmentModel = resultSet.getString(SELLER_SCHEMA.fulfilment_model.toString());
                int enabled = resultSet.getInt(SELLER_SCHEMA.enabled.toString());
                Timestamp created = resultSet.getTimestamp(SELLER_SCHEMA.created.toString());
                Timestamp last_updated = resultSet.getTimestamp(SELLER_SCHEMA.last_updated.toString());
                String updated_by = resultSet.getString(SELLER_SCHEMA.updated_by.toString());
                Timestamp updated = resultSet.getTimestamp(SELLER_SCHEMA.updated.toString());
                boolean supc_exist = resultSet.getBoolean(SELLER_SCHEMA.supc_exist.toString());
                boolean subcat_exist = resultSet.getBoolean(SELLER_SCHEMA.subcat_exist.toString());

                //write on record into aerospike set
                String keystr = AerospikeKeyHelper.getKey(sellerCode);
                Key key = new Key(NAMESPACE, setname, keystr);
                Bin[] bins = new Bin[SELLER_BINS.values().length];

                //id, sc, fm, e, c, u, se, sce

                bins[0] = new Bin(SELLER_BINS.sc.toString(), sellerCode);
                bins[1] = new Bin(SELLER_BINS.fm.toString(), fulfilmentModel);
                bins[2] = new Bin(SELLER_BINS.e.toString(), enabled);
                bins[3] = new Bin(SELLER_BINS.c.toString(), "" + created.getTime());
                bins[4] = new Bin(SELLER_BINS.u.toString(), "" + updated.getTime());
                bins[5] = new Bin(SELLER_BINS.se.toString(), supc_exist ? 1 : 0);
                bins[6] = new Bin(SELLER_BINS.sce.toString(), subcat_exist ? 1 : 0);

                try {
                    aerospikeClient.put(wpolicy, key, bins);
                    rowAffected ++;
                } catch (AerospikeException e) {
                    if (Migration.DEBUG) {
                        System.out.println("Not able to write record to aerospike.");
                        e.printStackTrace();
                    }
                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
            System.exit(0);
        } catch (AerospikeException e) {
            e.printStackTrace();
            System.exit(0);
        }

        if (Migration.DEBUG) {
            System.out.println("Migration of Seller_FM_Mapping completed.");
            System.out.println("Number of records inserted = "+rowAffected);
        }
    }

    public void migrateSellerSupcFmMapping() {
        if (Migration.DEBUG) {
            System.out.println("Starting migration of Seller_SUPC_FM_Mapping now...");
        }
        int rowAffected = 0;
        try {
            Statement statement = mysqlConnection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM seller_supc_fm_mapping");
            String setname = "seller_supc_fm_mapping_vo";
            WritePolicy wpolicy = new WritePolicy();
            wpolicy.timeout = timeout;
            
            while (resultSet.next()) {

                //read one row from mysql table
                String sellerCode = resultSet.getString(SELLER_SUPC_SCHEMA.seller_code.toString());
                String supc = resultSet.getString(SELLER_SUPC_SCHEMA.supc.toString());
                String fulfilmentModel = resultSet.getString(SELLER_SUPC_SCHEMA.fulfilment_model.toString());
                int enabled = resultSet.getInt(SELLER_SUPC_SCHEMA.enabled.toString());
                Timestamp created = resultSet.getTimestamp(SELLER_SUPC_SCHEMA.created.toString());
                Timestamp last_updated = resultSet.getTimestamp(SELLER_SUPC_SCHEMA.last_updated.toString());
                String updated_by = resultSet.getString(SELLER_SUPC_SCHEMA.updated_by.toString());
                Timestamp updated = resultSet.getTimestamp(SELLER_SUPC_SCHEMA.updated.toString());

                //write on record into aerospike set

                //write on record into aerospike set
                String keystr = AerospikeKeyHelper.getKey(sellerCode, supc);
                Key key = new Key(NAMESPACE, setname, keystr);
                Bin[] bins = new Bin[SELLER_SUPC_BINS.values().length];

                //ssc, sc, su, fm, e, c, u

                bins[0] = new Bin(SELLER_SUPC_BINS.sc.toString(), sellerCode);
                bins[1] = new Bin(SELLER_SUPC_BINS.fm.toString(), fulfilmentModel);
                bins[2] = new Bin(SELLER_SUPC_BINS.e.toString(), enabled);
                bins[3] = new Bin(SELLER_SUPC_BINS.c.toString(), "" + created.getTime());
                bins[4] = new Bin(SELLER_SUPC_BINS.u.toString(), "" + updated.getTime());
                bins[5] = new Bin(SELLER_SUPC_BINS.su.toString(), supc);
                bins[6] = new Bin(SELLER_SUPC_BINS.ssc.toString(), keystr);
                
                
                

                try {
                    aerospikeClient.put(wpolicy, key, bins);
                    rowAffected ++;
                } catch (AerospikeException e) {
                    if (Migration.DEBUG) {
                        System.out.println("Not able to write record to aerospike.");
                        e.printStackTrace();
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            System.exit(0);
        } catch (AerospikeException e) {
            e.printStackTrace();
            System.exit(0);
        }
        if (Migration.DEBUG) {
            System.out.println("Migration of Seller_Supc_FM_Mapping completed.");
            System.out.println("Number of records inserted = "+rowAffected);
        }

    }

    public void migrateSellerSubcatFmMapping() {
        if (Migration.DEBUG) {
            System.out.println("Starting migration of Seller_Subcat_FM_Mapping now...");
        }
        int rowAffected = 0;
        try {
            Statement statement = mysqlConnection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM seller_subcat_fm_mapping");
            String setname = "seller_subcat_fm_mapping_vo";
            WritePolicy wpolicy = new WritePolicy();
            wpolicy.timeout = timeout;
            while (resultSet.next()) {

                //read one row from mysql table
                String sellerCode = resultSet.getString(SELLER_SUBCAT_SCHEMA.seller_code.toString());
                String subcat = resultSet.getString(SELLER_SUBCAT_SCHEMA.subcat.toString());
                String fulfilmentModel = resultSet.getString(SELLER_SUBCAT_SCHEMA.fulfilment_model.toString());
                int enabled = resultSet.getInt(SELLER_SUBCAT_SCHEMA.enabled.toString());
                Timestamp created = resultSet.getTimestamp(SELLER_SUBCAT_SCHEMA.created.toString());
                Timestamp last_updated = resultSet.getTimestamp(SELLER_SUBCAT_SCHEMA.last_updated.toString());
                String updated_by = resultSet.getString(SELLER_SUBCAT_SCHEMA.updated_by.toString());
                Timestamp updated = resultSet.getTimestamp(SELLER_SUBCAT_SCHEMA.updated.toString());

                //write on record into aerospike set
                String keystr = AerospikeKeyHelper.getKey(sellerCode, subcat);
                Key key = new Key(NAMESPACE, setname, keystr);
                Bin[] bins = new Bin[SELLER_SUBCAT_BINS.values().length];

                //id, ssc, sc, su, fm, e, c, u

                bins[0] = new Bin(SELLER_SUBCAT_BINS.sc.toString(), sellerCode);
                bins[1] = new Bin(SELLER_SUBCAT_BINS.fm.toString(), fulfilmentModel);
                bins[2] = new Bin(SELLER_SUBCAT_BINS.e.toString(), enabled);
                bins[3] = new Bin(SELLER_SUBCAT_BINS.c.toString(), "" + created.getTime());
                bins[4] = new Bin(SELLER_SUBCAT_BINS.u.toString(), "" + updated.getTime());
                bins[5] = new Bin(SELLER_SUBCAT_BINS.sub.toString(), subcat);
                bins[6] = new Bin(SELLER_SUBCAT_BINS.ssc.toString(), keystr);

                try {
                    aerospikeClient.put(wpolicy, key, bins);
                    rowAffected ++;
                } catch (AerospikeException e) {
                    if (Migration.DEBUG) {
                        System.out.println("Not able to write record to aerospike.");
                        e.printStackTrace();
                    }
                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
            System.exit(0);
        } catch (AerospikeException e) {
            e.printStackTrace();
            System.exit(0);
        }
        if (Migration.DEBUG) {
            System.out.println("Migration of Seller_Subcat_FM_Mapping completed.");
            System.out.println("Number of rows affected = "+rowAffected);
        }
    }

}
