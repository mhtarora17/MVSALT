package com.snapdeal.cocofs.migration.shipnear;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;


/**
 * Migration for Shipnear project of
 * 1. Seller details from shipping to cocofs
 * 2. Zone details from P-Z system to cocofs
 * 
 * @author gaurav
 *
 */
public class MysqlMigration {
    
    //Parameters for Mysql data connection
	public static String COCOFS_MYSQL_HOSTNAME = "";
	public static int COCOFS_MYSQL_PORT = 0;
	public static String COCOFS_MYSQL_USERNAME = "";
	public static String COCOFS_MYSQL_PASSWORD = "";

	public static String SHIPPING_MYSQL_HOSTNAME = "";
	public static int SHIPPING_MYSQL_PORT = 0;
	public static String SHIPPING_MYSQL_USERNAME = "";
	public static String SHIPPING_MYSQL_PASSWORD = "";
	public static int SHIPPING_MYSQL_LOWER_ID = 0;
	
	public static String PZ_MYSQL_HOSTNAME = "";
	public static int PZ_MYSQL_PORT = 0;
	public static String PZ_MYSQL_USERNAME = "";
	public static String PZ_MYSQL_PASSWORD = "";
	
	//Program parameters
	public static boolean DEBUG = true;
	
	public static Connection COCOFS_MYSQL_CONNECTION;
	public static Connection SHIPPING_MYSQL_CONNECTION;
	public static Connection PZ_MYSQL_CONNECTION;
	
	private static Map<String,String> pincodeZoneCacheMap;
	public static void main(String[] args) {
		if(args == null || args.length < 13){
			printUsage();
			return;
		}
		processParameters(args);
		makeConnections();
		migrate();
	}
	
	private static void makeConnections(){
		if(MysqlMigration.DEBUG)
			System.out.println("Establishing data connections...");
		
		String cocofsUrl = "jdbc:mysql://"+COCOFS_MYSQL_HOSTNAME+":"+COCOFS_MYSQL_PORT+"/cocofs";
		String shippingUrl = "jdbc:mysql://"+SHIPPING_MYSQL_HOSTNAME+":"+SHIPPING_MYSQL_PORT+"/shipping";
		String pzUrl = "jdbc:mysql://"+PZ_MYSQL_HOSTNAME+":"+PZ_MYSQL_PORT+"/sdgeoapi";
	    try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			COCOFS_MYSQL_CONNECTION = DriverManager.getConnection(cocofsUrl, COCOFS_MYSQL_USERNAME, COCOFS_MYSQL_PASSWORD);
		}catch(Exception ex){
			ex.printStackTrace();
			System.exit(0);
		}
	    
	    try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			SHIPPING_MYSQL_CONNECTION = DriverManager.getConnection(shippingUrl, SHIPPING_MYSQL_USERNAME, SHIPPING_MYSQL_PASSWORD);
		}catch(Exception ex){
			System.out.println("Not able to connect to shipping db...Exiting...");
			System.exit(0);
		}
	    
	    try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			PZ_MYSQL_CONNECTION = DriverManager.getConnection(pzUrl, PZ_MYSQL_USERNAME, PZ_MYSQL_PASSWORD);
		}catch(Exception ex){
			System.out.println("Not able to connect to pz db...Exiting...");
			System.exit(0);
		}
       
		System.out.println("Mysql connections were successful..");
	}
	
	private static void migrate(){
		System.out.println("Starting migration now...");
		populatePincodeZoneMappings();
		
		System.out.println("Shipping -----> Cocofs");
		int rowsMigrated = 0;
		try{
			Statement statement = SHIPPING_MYSQL_CONNECTION.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM vendor_contact where contact_type=4 and id>"+SHIPPING_MYSQL_LOWER_ID);
            String scdquery = " insert into seller_contact_details (seller_code, pin_code, address_line1, address_line2, city, state,created,updated,updated_by,last_updated)"
                    + " values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            String szquery = " insert into seller_zone_mapping (seller_code, zone, created, updated, updated_by,last_updated)"
                    + " values (?, ?, ?, ?, ?, ?)";
            
            while (resultSet.next()) {
            	try{
            		String zone = pincodeZoneCacheMap.get(resultSet.getString("pin_code"));
            		if(zone == null){
            			System.out.println("Zone could not be found for id="+resultSet.getInt("id")+" , sellercode="+resultSet.getString("vendor_code")+".Ignoring this insertion.");
            			continue;
            		}
            		
            		PreparedStatement scdps = COCOFS_MYSQL_CONNECTION.prepareStatement(scdquery);
            		scdps.setString(1, resultSet.getString("vendor_code"));
            		scdps.setString(2, resultSet.getString("pin_code"));
            		scdps.setString(3, resultSet.getString("address_line1"));
            		scdps.setString(4, resultSet.getString("address_line2"));
            		scdps.setString(5, resultSet.getString("city"));
            		scdps.setString(6, resultSet.getString("address_state"));
            		scdps.setTimestamp(7, resultSet.getTimestamp("created"));
            		scdps.setTimestamp(8, resultSet.getTimestamp("updated"));
            		scdps.setString(9, "Migration");
            		scdps.setTimestamp(10, new Timestamp(System.currentTimeMillis()));
            		
            		PreparedStatement szps = COCOFS_MYSQL_CONNECTION.prepareStatement(szquery);
            		szps.setString(1, resultSet.getString("vendor_code"));
            		szps.setString(2, zone);
            		szps.setTimestamp(3, new Timestamp(System.currentTimeMillis()));
            		szps.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
            		szps.setString(5, "Migration");
            		szps.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
            		
            		scdps.execute();
            		szps.execute();
            		
            		rowsMigrated++;
            	} catch(Exception ex){
            		System.out.println("Error encountered, id="+resultSet.getInt("id")+" , sellerCode="+resultSet.getString("vendor_code")+":"+ex.getMessage());
            	}
            }
            System.out.println("Number of rows migrated from shipping->cocofs = "+rowsMigrated);
		}catch(Exception ex){
			System.out.println("Exception encountered... Exiting..."+ex.getMessage());
			System.out.println("Number of rows migrated from shipping->cocofs = "+rowsMigrated);
			System.exit(0);
		}
		
	}
	
	private static void populatePincodeZoneMappings(){
		pincodeZoneCacheMap = new HashMap<String,String>();
		try{
			Statement statement = PZ_MYSQL_CONNECTION.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM pincode_zone_mapping");
            
            int rowsMigrated = 0;
            while (resultSet.next()) {
            	try{
            		pincodeZoneCacheMap.put(resultSet.getString("pincode"), resultSet.getString("zone"));
            		rowsMigrated++;
            	} catch(Exception ex){
            		System.out.println("Error encountered with rowid="+resultSet.getInt("id"));
            	}
            }
            System.out.println("Number of rows fetched from pz->cocofs = "+rowsMigrated);
		}catch(Exception ex){
			System.out.println("Could not fetch data from pz db...Completing now...");
		}
	}
	
	private static void printUsage(){
		System.out.println("<USAGE>:");
		System.out.println("java -jar <JARNAME> "
				+ "<COCOFS_MYSQL_HOSTNAME> <COCOFS_MYSQL_PORT> <COCOFS_MYSQL_USERNAME> <COCOFS_MYSQL_PASSWORD>"
				+ "<SHIPPING_MYSQL_HOSTNAME> <SHIPPING_MYSQL_PORT> <SHIPPING_MYSQL_USERNAME> <SHIPPING_MYSQL_PASSWORD> <SHIPPING_MYSQL_LOWER_ID>"
				+ "<PZ_MYSQL_HOSTNAME> <PZ_MYSQL_PORT> <PZ_MYSQL_USERNAME> <PZ_MYSQL_PASSWORD>");
		System.out.println("For any param not available add ? in its place.");
	}
	
	private static void processParameters(String[] args){
		if(MysqlMigration.DEBUG)
			System.out.println("Processing input parameters now..");
		
		COCOFS_MYSQL_HOSTNAME = normalize(args[0]);
		COCOFS_MYSQL_PORT = Integer.parseInt(normalize(args[1]));
		COCOFS_MYSQL_USERNAME = normalize(args[2]);
		COCOFS_MYSQL_PASSWORD = normalize(args[3]);

		SHIPPING_MYSQL_HOSTNAME = normalize(args[4]);
		SHIPPING_MYSQL_PORT = Integer.parseInt(normalize(args[5]));
		SHIPPING_MYSQL_USERNAME = normalize(args[6]);
		SHIPPING_MYSQL_PASSWORD = normalize(args[7]);
		SHIPPING_MYSQL_LOWER_ID = Integer.parseInt(normalize(args[8]));
		
		PZ_MYSQL_HOSTNAME = normalize(args[9]);
		PZ_MYSQL_PORT = Integer.parseInt(normalize(args[10]));
		PZ_MYSQL_USERNAME = normalize(args[11]);
		PZ_MYSQL_PASSWORD = normalize(args[12]);
		
		if(MysqlMigration.DEBUG){
			
			System.out.println("COCOFS_MYSQL_HOSTNAME="+COCOFS_MYSQL_HOSTNAME);
			System.out.println("COCOFS_MYSQL_PORT="+COCOFS_MYSQL_PORT);
			System.out.println("COCOFS_MYSQL_USERNAME="+COCOFS_MYSQL_USERNAME);
			
			System.out.println("SHIPPING_MYSQL_HOSTNAME="+SHIPPING_MYSQL_HOSTNAME);
			System.out.println("SHIPPING_MYSQL_PORT="+SHIPPING_MYSQL_PORT);
			System.out.println("SHIPPING_MYSQL_USERNAME="+SHIPPING_MYSQL_USERNAME);
			System.out.println("SHIPPING_MYSQL_LOWER_ID="+SHIPPING_MYSQL_LOWER_ID);
			
			System.out.println("PZ_MYSQL_HOSTNAME"+PZ_MYSQL_HOSTNAME);
			System.out.println("PZ_MYSQL_PORT="+PZ_MYSQL_PORT);
			System.out.println("PZ_MYSQL_USERNAME="+PZ_MYSQL_USERNAME);
		}
		
	}
	
	private static String normalize(String param){
		if(param.trim().equalsIgnoreCase("?")){
			return "0";
		}
		return param.trim();
	}

}