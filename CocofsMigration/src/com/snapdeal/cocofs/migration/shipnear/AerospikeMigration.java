package com.snapdeal.cocofs.migration.shipnear;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.aerospike.client.AerospikeClient;
import com.aerospike.client.AerospikeException;
import com.aerospike.client.Bin;
import com.aerospike.client.Key;
import com.aerospike.client.policy.WritePolicy;

/**
 * Migration for cocofs Seller-Zone data from mysql to aerospike
 * 
 * @author gaurav
 *
 */
public class AerospikeMigration {

	// Parameters for Mysql data connection
	public static String MYSQL_HOSTNAME = "";
	public static int MYSQL_PORT = 0;
	public static String MYSQL_USERNAME = "";
	public static String MYSQL_PASSWORD = "";

	// Parameters for Aerospike data connection
	public static String AEROSPIKE_HOSTNAME = "";
	public static int AEROSPIKE_PORT = 0;
	private static int timeout = 600000;

	// Program parameters
	public static boolean DEBUG = true;

	// Data connections
	public static AerospikeClient AEROSPIKE_CONNECTION;
	public static Connection MYSQL_CONNECTION;

	public static void main(String[] args) {
		if (args == null || args.length < 6) {
			printUsage();
			return;
		}
		processParameters(args);
		makeConnections();
		migrate();
	}

	private static void makeConnections() {
		if (AerospikeMigration.DEBUG)
			System.out.println("Establishing data connections...");

		try {
			AEROSPIKE_CONNECTION = new AerospikeClient(AEROSPIKE_HOSTNAME,
					AEROSPIKE_PORT);
		} catch (AerospikeException ae) {
			System.out
					.println("Not able to make a connection with Aerospike..");
			System.out.println("Exiting now..");
			System.exit(0);
		}

		String url = "jdbc:mysql://" + MYSQL_HOSTNAME + ":" + MYSQL_PORT
				+ "/cocofs";
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			MYSQL_CONNECTION = DriverManager.getConnection(url, MYSQL_USERNAME,
					MYSQL_PASSWORD);
		} catch (InstantiationException e) {
			e.printStackTrace();
			System.exit(0);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			System.exit(0);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			System.exit(0);
		} catch (SQLException e) {
			e.printStackTrace();
			System.exit(0);
		}

		System.out.println("Mysql and Aerospike connections successful..");
	}

	private static void migrate() {
		System.out.println("Starting migration of Seller_Zone_Mapping now...");

		int rowAffected = 0;
		try {
			Statement statement = MYSQL_CONNECTION.createStatement();
			ResultSet resultSet = statement
					.executeQuery("SELECT * FROM seller_zone_mapping");
			String setname = "seller_zone_mapping_vo";
			WritePolicy wpolicy = new WritePolicy();
			wpolicy.timeout = timeout;
			while (resultSet.next()) {
				try {
					System.out.println("Writing seller:"
							+ resultSet.getString("seller_code"));
					String keystr = resultSet.getString("seller_code");
					Key key = new Key("cocofs", setname, keystr);
					Bin[] bins = new Bin[2];

					bins[0] = new Bin("sc", resultSet.getString("seller_code"));
					bins[1] = new Bin("z", resultSet.getString("zone"));
					AEROSPIKE_CONNECTION.put(wpolicy, key, bins);
					rowAffected++;
				} catch (Exception ex) {
					System.out.println("Error while writing zone for seller:"
							+ resultSet.getString("seller_code")
							+ ", exception:" + ex.getMessage());
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(0);
		}

		System.out.println("Migration of Seller_ZOne_Mapping completed.");
		System.out.println("Number of records inserted = " + rowAffected);

	}

	private static void printUsage() {
		System.out.println("<USAGE>:");
		System.out
				.println("java -jar <JARNAME> <MYSQL_HOSTNAME> <MYSQL_PORT> <MYSQL_USERNAME> <MYSQL_PASSWORD><AEROSPIKE_HOSTNAME> <AEROSPIKE_PORT>");
		System.out.println("For any param not available add ? in its place.");
	}

	private static void processParameters(String[] args) {
		if (AerospikeMigration.DEBUG)
			System.out.println("Processing input parameters now..");

		MYSQL_HOSTNAME = normalize(args[0]);
		MYSQL_PORT = Integer.parseInt(normalize(args[1]));
		MYSQL_USERNAME = normalize(args[2]);
		MYSQL_PASSWORD = normalize(args[3]);

		AEROSPIKE_HOSTNAME = normalize(args[4]);
		AEROSPIKE_PORT = Integer.parseInt(normalize(args[5]));

		if (AerospikeMigration.DEBUG) {

			System.out.println("MYSQL_HOSTNAME = " + MYSQL_HOSTNAME);
			System.out.println("MYSQL_PORT = " + MYSQL_PORT);
			System.out.println("MYSQL_USERNAME = " + MYSQL_USERNAME);

			System.out.println("AEROSPIKE_HOSTNAME = " + AEROSPIKE_HOSTNAME);
			System.out.println("AEROSPIKE_PORT = " + AEROSPIKE_PORT);

		}

	}

	private static String normalize(String param) {
		if (param.trim().equalsIgnoreCase("?")) {
			return "0";
		}
		return param.trim();
	}

}