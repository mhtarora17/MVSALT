/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 04-Sep-2013
 *  @author himanshum
 */
package com.snapdeal.cocofs.common;

import com.snapdeal.base.services.exception.ServiceException;

public class JobValidationException extends ServiceException {

    private static final long            serialVersionUID = 111221L;
    private BulkUploadValidationResponse response;

    public JobValidationException(String message) {
        super(message);
    }

    public JobValidationException(BulkUploadValidationResponse response) {
        super(response.getErrorMessage());
        this.response = response;
    }

    public BulkUploadValidationResponse getResponse() {
        return response;
    }

    public void setResponse(BulkUploadValidationResponse response) {
        this.response = response;
    }

}
