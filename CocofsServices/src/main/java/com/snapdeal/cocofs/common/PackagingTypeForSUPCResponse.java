/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 9, 2013
 *  @author himanshu
 */
package com.snapdeal.cocofs.common;

import com.snapdeal.base.validation.ValidationError;
import com.snapdeal.cocofs.sro.PackagingType;

public class PackagingTypeForSUPCResponse {
    
    private String supc;
    private PackagingType packageType;
    private ValidationError validationError;
    private boolean error;
    
    
    public String getSupc() {
        return supc;
    }
    public void setSupc(String supc) {
        this.supc = supc;
    }

    public PackagingType getPackageType() {
        return packageType;
    }
    public void setPackageType(PackagingType packageType) {
        this.packageType = packageType;
    }
    public ValidationError getValidationError() {
        return validationError;
    }
    public void setValidationError(ValidationError validationError) {
        this.validationError = validationError;
    }
    public boolean isError() {
        return error;
    }
    public void setError(boolean error) {
        this.error = error;
    }
    @Override
    public String toString() {
        return "PackagingTypeForSUPCResponse [supc=" + supc + ", packageType=" + packageType + ", validationError=" + validationError + ", error=" + error + "]";
    }

    
    

}
