/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 23, 2013
 *  @author himanshu
 */
package com.snapdeal.cocofs.common;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RateCardUpdateRequest {
    
    private List<RateCardUpdateDTO> rows ;
    
    private Integer categoryId;

    public List<RateCardUpdateDTO> getRows() {
        return rows;
    }

    public void setRows(List<RateCardUpdateDTO> rows) {
        this.rows = rows;
    }
    
    
    public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	@Override
	public String toString() {
		return "RateCardUpdateRequest [rows=" + rows + ", categoryId="
				+ categoryId + "]";
	}

	

    
}
