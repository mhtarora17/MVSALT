/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 9, 2013
 *  @author himanshu
 */
package com.snapdeal.cocofs.common;

import com.snapdeal.base.validation.ValidationError;
import com.snapdeal.cocofs.sro.FulfillmentModel;

public class FulfillmentModelForSupcAndSellerRepsonse {
    
    private String supc;
    private String sellerCode;
    private FulfillmentModel fulfillmentModel;
    private ValidationError validationError;
    private boolean error;
    public String getSupc() {
        return supc;
    }
    public void setSupc(String supc) {
        this.supc = supc;
    }

    
    public String getSellerCode() {
        return sellerCode;
    }
    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }
    public FulfillmentModel getFulfillmentModel() {
        return fulfillmentModel;
    }
    public void setFulfillmentModel(FulfillmentModel fulfillmentModel) {
        this.fulfillmentModel = fulfillmentModel;
    }
    public ValidationError getValidationError() {
        return validationError;
    }
    public void setValidationError(ValidationError validationError) {
        this.validationError = validationError;
    }
    public boolean isError() {
        return error;
    }
    public void setError(boolean error) {
        this.error = error;
    }
    
    
    

}
