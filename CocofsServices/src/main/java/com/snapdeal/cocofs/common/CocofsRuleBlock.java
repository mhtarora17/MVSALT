/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 10, 2013
 *  @author himanshu
 */
package com.snapdeal.cocofs.common;

public enum CocofsRuleBlock {
    DELIVERY_TYPE("DeliveryType"), PACKAGING_TYPE("PackagingType");
    private String name;

    private CocofsRuleBlock(String name ) {
        this.name = name;
    }

    public String getName() {
        return name;
    }


}
