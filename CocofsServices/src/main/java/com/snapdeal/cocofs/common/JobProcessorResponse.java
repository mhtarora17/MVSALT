/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 04-Sep-2013
 *  @author himanshum
 */
package com.snapdeal.cocofs.common;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JobProcessorResponse {
    private Boolean                   processingSuccessful;
    private Map<String, List<String>> error = new HashMap<String, List<String>>();

    public enum ResponseType {
        SAVE_FAILED;
    }

    private String responseType;

    public Boolean getProcessingSuccessful() {
        return processingSuccessful;
    }

    public void setProcessingSuccessful(Boolean processingSuccessful) {
        this.processingSuccessful = processingSuccessful;
    }

    public Map<String, List<String>> getError() {
        return error;
    }

    public void setError(Map<String, List<String>> error) {
        this.error = error;
    }

    public String getResponseType() {
        return responseType;
    }

    public void setResponseType(String responseType) {
        this.responseType = responseType;
    }

    @Override
    public String toString() {
        return "JobProcessorResponse [processingSuccessful=" + processingSuccessful + ", error=" + error + ", responseType=" + responseType + "]";
    }

    

}
