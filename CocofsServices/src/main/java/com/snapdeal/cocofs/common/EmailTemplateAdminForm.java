/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 05-Sep-2013
 *  @author himanshum
 */
package com.snapdeal.cocofs.common;

import java.util.List;

import javax.validation.Valid;

import com.snapdeal.cocofs.entity.EmailTemplate;

public class EmailTemplateAdminForm {
    @Valid
    EmailTemplate                 emailTemplate;

    private List<EmailChannelDTO> channels;

    public EmailTemplateAdminForm() {
    }

    public EmailTemplateAdminForm(EmailTemplate emailTemplate) {
        this.emailTemplate = emailTemplate;
    }

    public EmailTemplate getEmailTemplate() {
        return emailTemplate;
    }

    public void setEmailTemplate(EmailTemplate emailTemplate) {
        this.emailTemplate = emailTemplate;
    }

    public List<EmailChannelDTO> getChannels() {
        return channels;
    }

    public void setChannels(List<EmailChannelDTO> channels) {
        this.channels = channels;
    }
    
    


}
