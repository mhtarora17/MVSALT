/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 22, 2013
 *  @author himanshu
 */
package com.snapdeal.cocofs.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author himanshu
 *
 */
public class RateCardRowDisplayDTO {
    
    private String name;
    private List<String> deliveryType = new ArrayList<String>();
    private String costHead;
    private Map<String, Boolean> fmEditable = new HashMap<String, Boolean>();
    private Map<String, Double>  fmCost = new HashMap<String, Double>();
    private int slabIndex;
    private double slabWeight;
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public List<String> getDeliveryType() {
        return deliveryType;
    }
    public void setDeliveryType(List<String> deliveryType) {
        this.deliveryType = deliveryType;
    }
    public String getCostHead() {
        return costHead;
    }
    public void setCostHead(String costHead) {
        this.costHead = costHead;
    }
    public Map<String, Boolean> getFmEditable() {
        return fmEditable;
    }
    public void setFmEditable(Map<String, Boolean> fmEditable) {
        this.fmEditable = fmEditable;
    }
    public Map<String, Double> getFmCost() {
        return fmCost;
    }
    public void setFmCost(Map<String, Double> fmCost) {
        this.fmCost = fmCost;
    }
    public int getSlabIndex() {
        return slabIndex;
    }
    public void setSlabIndex(int slabIndex) {
        this.slabIndex = slabIndex;
    }
    public double getSlabWeight() {
        return slabWeight;
    }
    public void setSlabWeight(double slabWeight) {
        this.slabWeight = slabWeight;
    }
    @Override
    public String toString() {
        return "RateCardRowDisplayDTO [name=" + name + ", deliveryType=" + deliveryType + ", costHead=" + costHead + ", fmEditable=" + fmEditable + ", fmCost=" + fmCost
                + ", slabIndex=" + slabIndex + ", slabWeight=" + slabWeight + "]";
    }
    
    
    
    

}
