/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 23, 2013
 *  @author himanshu
 */
package com.snapdeal.cocofs.common;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.google.gson.Gson;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RateCardUpdateDTO {
    
    private String costHead;
    private double weight;
    private List<String> deliveryTypes ;
    private int slabIndex;
    private List<FulfillmentModelCost> fmCosts;
    public String getCostHead() {
        return costHead;
    }
    public void setCostHead(String costHead) {
        this.costHead = costHead;
    }
    public double getWeight() {
        return weight;
    }
    public void setWeight(double weight) {
        this.weight = weight;
    }
    public List<String> getDeliveryTypes() {
        return deliveryTypes;
    }
    public void setDeliveryTypes(List<String> deliveryTypes) {
        this.deliveryTypes = deliveryTypes;
    }
    public int getSlabIndex() {
        return slabIndex;
    }
    public void setSlabIndex(int slabIndex) {
        this.slabIndex = slabIndex;
    }
    public List<FulfillmentModelCost> getFmCosts() {
        return fmCosts;
    }
    public void setFmCosts(List<FulfillmentModelCost> fmCosts) {
        this.fmCosts = fmCosts;
    }
    
    
    @Override
    public String toString() {
        return "RateCardUpdateDTO [costHead=" + costHead + ", weight=" + weight + ", deliveryTypes=" + deliveryTypes + ", slabIndex=" + slabIndex + ", fmCosts=" + fmCosts + "]";
    }
    
    public static void main(String args[]) {
        Gson g = new Gson();
        RateCardUpdateDTO dto = new RateCardUpdateDTO();
        dto.setCostHead("TestCost");
        List<String> deliveryType = new ArrayList<String>();
        deliveryType.add("NORMAL");
        deliveryType.add("Express");
        
        dto.setDeliveryTypes(deliveryType);
        List<FulfillmentModelCost> costs = new ArrayList<FulfillmentModelCost>();
        FulfillmentModelCost e = new FulfillmentModelCost();
        e.setCost(3435.55);
        e.setFulfillmentModel("FC");
        costs.add(e );
        dto.setFmCosts(costs );
        dto.setSlabIndex(0);
        dto.setWeight(34.78);
        
        System.out.println(g.toJson(dto));
    }

}
