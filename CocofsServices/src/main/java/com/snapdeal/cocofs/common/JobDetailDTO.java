/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 04-Sep-2013
 *  @author himanshum
 */
package com.snapdeal.cocofs.common;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class JobDetailDTO implements Serializable {

	private static final long serialVersionUID = -608565453070982942L;

	private String jobCode;
	private String fileName;
	private String status;
	private String action;
	private String created;
	private String updated;
	private String uploadedBy;
	private int priority; // higher the more priority

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public String getJobCode() {
		return jobCode;
	}

	public void setJobCode(String jobCode) {
		this.jobCode = jobCode;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public String getUpdated() {
		return updated;
	}

	public void setUpdated(String updated) {
		this.updated = updated;
	}

	public String getUploadedBy() {
		return uploadedBy;
	}

	public void setUploadedBy(String uploadedBy) {
		this.uploadedBy = uploadedBy;
	}

	@Override
	public String toString() {
		return "JobDetailDTO [jobCode=" + jobCode + ", fileName=" + fileName
				+ ", status=" + status + ", action=" + action + ", created="
				+ created + ", updated=" + updated + ", uploadedBy="
				+ uploadedBy + ", priority=" + priority + "]";
	}

}
