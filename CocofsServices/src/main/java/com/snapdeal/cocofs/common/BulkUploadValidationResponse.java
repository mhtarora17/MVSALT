/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 04-Sep-2013
 *  @author himanshum
 */
package com.snapdeal.cocofs.common;

import java.util.List;

public class BulkUploadValidationResponse {

    private Boolean                       valid;

    private String                        errorMessage;

    private List<BulkUploadValidationDTO> list;

    public Boolean getValid() {
        return valid;
    }

    public void setValid(Boolean valid) {
        this.valid = valid;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public List<BulkUploadValidationDTO> getList() {
        return list;
    }

    public void setList(List<BulkUploadValidationDTO> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (list != null) {
            String BR = "<br/>";
            sb.append("<pre>");
            sb.append("Total errors: " + list.size());
            sb.append(BR);
            int count = 1;
            for (BulkUploadValidationDTO o : list) {
                sb.append((count++) + " [rowNo=" + o.getRowNo() + ", errorType=" + o.getErrorType() + ", invalidParameterValue=" + o.getInvalidParameterValue() + "]");
                sb.append(BR);
            }
            sb.append("</pre>");
        }
        return sb.toString();
    }

}
