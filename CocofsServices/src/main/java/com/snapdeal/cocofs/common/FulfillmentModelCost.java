/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 23, 2013
 *  @author himanshu
 */
package com.snapdeal.cocofs.common;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FulfillmentModelCost {
    private String fulfillmentModel;
    private double cost;
    public String getFulfillmentModel() {
        return fulfillmentModel;
    }
    public void setFulfillmentModel(String fulfillmentModel) {
        this.fulfillmentModel = fulfillmentModel;
    }
    public double getCost() {
        return cost;
    }
    public void setCost(double cost) {
        this.cost = cost;
    }
    @Override
    public String toString() {
        return "FulfillmentModelCost [fulfillmentModel=" + fulfillmentModel + ", cost=" + cost + "]";
    }
    
    

}
