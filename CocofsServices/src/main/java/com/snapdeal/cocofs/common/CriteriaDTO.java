/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 17-Apr-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.common;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.snapdeal.rule.engine.entity.Operators;
import com.snapdeal.rule.engine.entity.ValueTypes;

public class CriteriaDTO {

    private String       name;

    private Operators    operator;

    private Object       value;

    private Integer      id;

    private ValueTypes   valueType;

    private List<Object> values  = new ArrayList<Object>(0);

    private Boolean      enabled = true;

    private Date         created;

    public CriteriaDTO() {
    }

    public CriteriaDTO(CriteriaDTO dto) {
        this.name = dto.getName();
        this.created = dto.getCreated();
        this.enabled = dto.getEnabled();
        this.id = dto.getId();
        this.operator = dto.getOperator();
        this.value = dto.getValue();
        this.values = dto.getValues();
        this.valueType = dto.getValueType();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Operators getOperator() {
        return operator;
    }

    public void setOperator(Operators operator) {
        this.operator = operator;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ValueTypes getValueType() {
        return valueType;
    }

    public void setValueType(ValueTypes valueType) {
        this.valueType = valueType;
    }

    public List<Object> getValues() {
        return values;
    }

    public void setValues(List<Object> values) {
        this.values = values;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        CriteriaDTO other = (CriteriaDTO) obj;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Override
    public String toString() {
        return "CriteriaDTO [name=" + name + ", operator=" + operator + ", value=" + value + ", id=" + id + ", values=" + values + ", enabled=" + enabled + "]";
    }

}
