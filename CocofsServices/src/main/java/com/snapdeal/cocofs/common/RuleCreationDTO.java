/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 17, 2013
 *  @author himanshu
 */
package com.snapdeal.cocofs.common;

import java.util.Date;

public class RuleCreationDTO {
    private String updatedBy;
    private String categoryUrl;
    private String ruleType;
    private Date updateTime;
    private Boolean fragile;
    private Boolean liquid;
    private Boolean hazMat;
    private Double weight;
    private Double volumetricWeight;
    private Date startDate;
    private Date endDate;
    public String getUpdatedBy() {
        return updatedBy;
    }
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }
    public String getCategoryUrl() {
        return categoryUrl;
    }
    public void setCategoryUrl(String categoryUrl) {
        this.categoryUrl = categoryUrl;
    }
    public String getRuleType() {
        return ruleType;
    }
    public void setRuleType(String ruleType) {
        this.ruleType = ruleType;
    }
    public Date getUpdateTime() {
        return updateTime;
    }
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
    public Boolean getFragile() {
        return fragile;
    }
    public void setFragile(Boolean fragile) {
        this.fragile = fragile;
    }
    public Boolean getLiquid() {
        return liquid;
    }
    public void setLiquid(Boolean liquid) {
        this.liquid = liquid;
    }
    public Boolean getHazMat() {
        return hazMat;
    }
    public void setHazMat(Boolean hazMat) {
        this.hazMat = hazMat;
    }
    public Double getWeight() {
        return weight;
    }
    public void setWeight(Double weight) {
        this.weight = weight;
    }
    public Double getVolumetricWeight() {
        return volumetricWeight;
    }
    public void setVolumetricWeight(Double volumetricWeight) {
        this.volumetricWeight = volumetricWeight;
    }
    public Date getStartDate() {
        return startDate;
    }
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
    public Date getEndDate() {
        return endDate;
    }
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
    @Override
    public String toString() {
        return "RuleCreationDTO [updatedBy=" + updatedBy + ", categoryUrl=" + categoryUrl + ", ruleType=" + ruleType + ", updateTime=" + updateTime + ", fragile=" + fragile
                + ", liquid=" + liquid + ", hazMat=" + hazMat + ", weight=" + weight + ", volumetricWeight=" + volumetricWeight + ", startDate=" + startDate + ", endDate="
                + endDate + "]";
    }

    
    

}
