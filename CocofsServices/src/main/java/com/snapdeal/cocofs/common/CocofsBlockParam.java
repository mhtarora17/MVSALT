/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 10, 2013
 *  @author himanshu
 */
package com.snapdeal.cocofs.common;

import com.snapdeal.rule.engine.entity.Operators;
import com.snapdeal.rule.engine.entity.ValueTypes;

public enum CocofsBlockParam {
    WEIGHT("WEIGHT", ValueTypes.DOUBLE, Operators.GREATER_THAN), 
    VOLUMETRIC_WEIGHT("VOLWEIGHT", ValueTypes.DOUBLE, Operators.GREATER_THAN), 
    LIQUID("LIQUID", ValueTypes.BOOLEAN, Operators.EQUAL_TO), 
    HAZARDOUS("HAZMAT", ValueTypes.BOOLEAN, Operators.EQUAL_TO), 
    FRAGILE("FRAGILE", ValueTypes.BOOLEAN, Operators.EQUAL_TO);
    private String     name;
    private ValueTypes type;
    private Operators  operator;

    private CocofsBlockParam(String name, ValueTypes type, Operators operator) {
        this.name = name;
        this.type = type;
        this.operator = operator;
    }

    public String getName() {
        return name;
    }

    public ValueTypes getType() {
        return type;
    }

    public Operators getOperator() {
        return operator;
    }

}
