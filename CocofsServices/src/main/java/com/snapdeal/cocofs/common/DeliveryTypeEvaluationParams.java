/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 10, 2013
 *  @author himanshu
 */
package com.snapdeal.cocofs.common;

public class DeliveryTypeEvaluationParams {

    private boolean hazMat;
    private boolean liquid;
    private boolean fragile;
    private double weight;
    private double volumetricWeight;
    public boolean isHazMat() {
        return hazMat;
    }
    public void setHazMat(boolean hazMat) {
        this.hazMat = hazMat;
    }
    public boolean isLiquid() {
        return liquid;
    }
    public void setLiquid(boolean liquid) {
        this.liquid = liquid;
    }
    public boolean isFragile() {
        return fragile;
    }
    public void setFragile(boolean fragile) {
        this.fragile = fragile;
    }
    public double getWeight() {
        return weight;
    }
    public void setWeight(double weight) {
        this.weight = weight;
    }
    public double getVolumetricWeight() {
        return volumetricWeight;
    }
    public void setVolumetricWeight(double volumetricWeight) {
        this.volumetricWeight = volumetricWeight;
    }
    
    /* Getters for rule framework to work */
    public Boolean getFragile() {
        return fragile;
    }
    
    public Boolean getLiquid() {
        return liquid;
    }
    
    public Boolean getHazMat() {
        return hazMat;
    }
    
    
    @Override
    public String toString() {
        return "DeliveryTypeEvaluationParams [hazMat=" + hazMat + ", liquid=" + liquid + ", fragile=" + fragile + ", weight=" + weight + ", volumetricWeight=" + volumetricWeight
                + "]";
    }
    
    
    
}
