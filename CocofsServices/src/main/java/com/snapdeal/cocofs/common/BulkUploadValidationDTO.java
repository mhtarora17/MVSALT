/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 04-Sep-2013
 *  @author himanshum
 */
package com.snapdeal.cocofs.common;

public class BulkUploadValidationDTO {
    private Integer rowNo;

    private String  errorType;

    private String  invalidParameterValue;

    public BulkUploadValidationDTO(Integer rowNo, String errorType, String invalidParamterValue) {
        this.rowNo = rowNo;
        this.errorType = errorType;
        this.invalidParameterValue = invalidParamterValue;
    }

    public BulkUploadValidationDTO() {
    }

    public Integer getRowNo() {
        return rowNo;
    }

    public void setRowNo(Integer rowNo) {
        this.rowNo = rowNo;
    }

    public String getErrorType() {
        return errorType;
    }

    public void setErrorType(String errorType) {
        this.errorType = errorType;
    }

    public String getInvalidParameterValue() {
        return invalidParameterValue;
    }

    public void setInvalidParameterValue(String invalidParameterValue) {
        this.invalidParameterValue = invalidParameterValue;
    }

    @Override
    public String toString() {
        return "BulkUploadValidationDTO [rowNo=" + rowNo + ", errorType=" + errorType + ", invalidParameterValue=" + invalidParameterValue + "]";
    }

}
