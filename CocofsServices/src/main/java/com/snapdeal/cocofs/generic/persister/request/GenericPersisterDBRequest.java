/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @author gaurav
 */
package com.snapdeal.cocofs.generic.persister.request;

import java.util.ArrayList;
import java.util.List;

import com.snapdeal.cocofs.services.common.dto.UserInfo;

/**
 * Generic Mysql Entity Only Persister Request
 * 
 * @author gaurav
 *
 * @param <E>
 */
public class GenericPersisterDBRequest<E> {
	
	private List<E>  entitiesToPersist  = new ArrayList<E>();

	private UserInfo userInfo;

    private boolean  skipValidation;

    public List<E> getEntitiesToPersist() {
		return entitiesToPersist;
	}

	public void setEntitiesToPersist(List<E> entitiesToPersist) {
		this.entitiesToPersist = entitiesToPersist;
	}

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public boolean isSkipValidation() {
        return skipValidation;
    }

    public void setSkipValidation(boolean skipValidation) {
        this.skipValidation = skipValidation;
    }
}
