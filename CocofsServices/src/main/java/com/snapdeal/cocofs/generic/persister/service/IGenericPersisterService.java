/**
 * 
 */
package com.snapdeal.cocofs.generic.persister.service;

import java.util.List;

import com.snapdeal.cocofs.generic.persister.exception.GenericPersisterException;
import com.snapdeal.cocofs.generic.persister.request.GenericPersisterByReflectionRequest;
import com.snapdeal.cocofs.generic.persister.request.GenericPersisterDBRequest;
import com.snapdeal.cocofs.generic.persister.request.GenericPersisterRequest;
import com.snapdeal.cocofs.generic.persister.request.GenericPersisterWithAerospikeRequest;
import com.snapdeal.cocofs.generic.persister.response.GenericPersisterDBResponse;
import com.snapdeal.cocofs.generic.persister.response.GenericPersisterResponse;
import com.snapdeal.cocofs.generic.persister.response.GenericPersisterWithAerospikeResponse;
import com.snapdeal.score.data.aerospike.AerospikeSet;

/**
 * @author abhinav
 *
 */
public interface IGenericPersisterService<T, D, R extends AerospikeSet> {

    /**
     * Persists entities and documents in the request in a single transaction. 
     * Doesn't try to persist in Mongo if request param asynchToMongo is set true.
     * @param <T>
     * @param <D>
     * @param req
     * @return GenericPersisterResponse
     */
    GenericPersisterResponse<T, D> persistOne(GenericPersisterRequest<T, D> req) throws GenericPersisterException;
    
    /**
     * Persists each request in the list in separate transaction. 
     * If any single request transaction fails, rest of the requests in the list will be attempted to persist.
     * @param <T>
     * @param <D>
     * @param req
     * @return List<GenericPersisterResponse>
     */
    List<GenericPersisterResponse<T, D>> persistAll(List<GenericPersisterRequest<T, D>> req) throws GenericPersisterException;
    
    /**
     * Still in Beta :)
     * @param req
     * @return
     */
    GenericPersisterResponse persistOneByReflection(GenericPersisterByReflectionRequest req) throws GenericPersisterException;
    
    /**
     * Still in Beta :)
     * @param req
     * @return
     */
    List<GenericPersisterResponse> persistAllByReflection(List<GenericPersisterByReflectionRequest> req) throws GenericPersisterException;


    /**
     * Persists entities , Aerospike records and documents in the request in a single transaction. 
     * Doesn't try to persist  in Aerospike if request param asynchToAerospike is set true and in Mongo if request param asynchToMongo is set true.
     * @param <T>
     * @param <D>
     * @param req
     * @return GenericPersisterResponse
     */
    GenericPersisterWithAerospikeResponse<T, R> persistOneWithAerospike(GenericPersisterWithAerospikeRequest<T, R> req) throws GenericPersisterException;

    /**
     * Persists db entities in the request in a single transaction. 
     * @param <T>
     * @param req
     * @return GenericPersisterResponse
     */
    GenericPersisterDBResponse<T> persistOneDBEntity(GenericPersisterDBRequest<T> req) throws GenericPersisterException;

}
