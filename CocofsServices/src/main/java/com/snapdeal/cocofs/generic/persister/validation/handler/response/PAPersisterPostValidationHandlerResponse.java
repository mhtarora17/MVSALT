/**
 * 
 */
package com.snapdeal.cocofs.generic.persister.validation.handler.response;

import java.util.ArrayList;
import java.util.List;

import com.snapdeal.cocofs.entity.PendingProductAttributeUpdate;
import com.snapdeal.cocofs.entity.ProductAttribute;
import com.snapdeal.cocofs.entity.ProductAttributeNotification;
import com.snapdeal.cocofs.generic.persister.response.GenericPersisterResponse;
import com.snapdeal.cocofs.mongo.model.ProductAttributeNotificationUnit;
import com.snapdeal.cocofs.mongo.model.ProductAttributeUnit;

/**
 * @author abhinav
 *
 */
public class PAPersisterPostValidationHandlerResponse extends GenericPersisterResponse<ProductAttribute, ProductAttributeUnit>{
    
    private List<PendingProductAttributeUpdate>         pendingPAList = new ArrayList<PendingProductAttributeUpdate>(0);
    
    private List<ProductAttributeNotification>          paNotificationList = new ArrayList<ProductAttributeNotification>(0);
    
    private List<ProductAttributeNotificationUnit>      paNotificationUnitList = new ArrayList<ProductAttributeNotificationUnit>(0);
    
    private boolean entitiesPersisted;
    
    private boolean pendingPersisted;

    public List<PendingProductAttributeUpdate> getPendingPAList() {
        return pendingPAList;
    }

    public void setPendingPAList(List<PendingProductAttributeUpdate> pendingPAList) {
        this.pendingPAList = pendingPAList;
    }

    public List<ProductAttributeNotification> getPaNotificationList() {
        return paNotificationList;
    }

    public void setPaNotificationList(List<ProductAttributeNotification> paNotificationList) {
        this.paNotificationList = paNotificationList;
    }

    public List<ProductAttributeNotificationUnit> getPaNotificationUnitList() {
        return paNotificationUnitList;
    }

    public void setPaNotificationUnitList(List<ProductAttributeNotificationUnit> paNotificationUnitList) {
        this.paNotificationUnitList = paNotificationUnitList;
    }

    public boolean isEntitiesPersisted() {
        return entitiesPersisted;
    }

    public void setEntitiesPersisted(boolean entitiesPersisted) {
        this.entitiesPersisted = entitiesPersisted;
    }

    public boolean isPendingPersisted() {
        return pendingPersisted;
    }

    public void setPendingPersisted(boolean pendingPersisted) {
        this.pendingPersisted = pendingPersisted;
    }

    
}
