package com.snapdeal.cocofs.generic.persister.validation.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.base.exception.TransportException;
import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.cocofs.access.IProductAttributeService;
import com.snapdeal.cocofs.common.DeliveryTypeForSUPCResponse;
import com.snapdeal.cocofs.common.PackagingTypeForSUPCResponse;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.entity.ProductAttribute;
import com.snapdeal.cocofs.external.service.IOPSExternalService;
import com.snapdeal.cocofs.external.service.IVIPMSExternalService;
import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.external.service.factory.IExternalServiceFactory;
import com.snapdeal.cocofs.generic.persister.request.GenericPersisterRequest;
import com.snapdeal.cocofs.generic.persister.validation.IGenericValidationDataBuilder;
import com.snapdeal.cocofs.generic.persister.validation.dto.PAValidationData;
import com.snapdeal.cocofs.mongo.model.ProductAttributeUnit;
import com.snapdeal.cocofs.services.IDeliveryTypeService;
import com.snapdeal.cocofs.services.IPackagingTypeService;
import com.snapdeal.cocofs.services.fm.IFulfilmentModelService;
import com.snapdeal.cocofs.sro.FulfillmentModel;
import com.snapdeal.ops.base.api.isFulfillmentFeeChanged.IsFulfillmentFeeChangedRequest;
import com.snapdeal.ops.base.api.isFulfillmentFeeChanged.IsFulfillmentFeeChangedResponse;
import com.snapdeal.ops.base.api.isFulfillmentFeeChanged.SellerFulfillmentModelSRO;
import com.snapdeal.ops.base.exception.OPSException;
import com.snapdeal.score.core.response.GenericInternalMethodResponse;
import com.snapdeal.score.core.response.reason.impl.PABuildValidationDataFailureReasons;
import com.snapdeal.score.core.response.reason.impl.PABuildValidationDataFailureReasons.PABuildValidationDataFailReasons;

/**
 * @author abhinav
 */

@Service("productAttributeValidationDataBuilder")
public class ProductAttributeValidationDataBuilder implements IGenericValidationDataBuilder<ProductAttribute, ProductAttributeUnit> {

    private static final Logger      LOG     = LoggerFactory.getLogger(ProductAttributeValidationDataBuilder.class);

    @Autowired
    private IProductAttributeService productAttrService;

    @Autowired
    private IDeliveryTypeService     deliveryTypeService;

    @Autowired
    private IPackagingTypeService    packagingTypeService;

    @Autowired
    private IFulfilmentModelService  fulfilmentModelService;

    @Autowired
    private IExternalServiceFactory  externalServiceFactory;

    private static final String      WEIGHT  = "WEIGHT";

    private static final String      LENGTH  = "LENGTH";

    private static final String      BREADTH = "BREADTH";

    private static final String      HEIGHT  = "HEIGHT";

    @Override
    public GenericInternalMethodResponse<PAValidationData, PABuildValidationDataFailureReasons<PABuildValidationDataFailReasons>> buildRequestValidationData(
            GenericPersisterRequest<ProductAttribute, ProductAttributeUnit> req) {

        GenericInternalMethodResponse<PAValidationData, PABuildValidationDataFailureReasons<PABuildValidationDataFailReasons>> resp = new GenericInternalMethodResponse<PAValidationData, PABuildValidationDataFailureReasons<PABuildValidationDataFailReasons>>();

        resp.setSuccessful(true);

        PAValidationData data = new PAValidationData();
        data.setValidationRequired(false);
        data.setValidationDataBuildSuccessful(true);
        String supc = null;
        List<ProductAttribute> newPAClone = null;

        if (req != null && req.getEntitiesToPersist() != null && !req.getEntitiesToPersist().isEmpty()) {

            newPAClone = new ArrayList<ProductAttribute>(req.getEntitiesToPersist());

            for (ProductAttribute pa : req.getEntitiesToPersist()) {

                if (ConfigUtils.isPresentInList(Property.PENDING_CHECK_ATTRIBUTES_LIST, pa.getAttribute())) {

                    LOG.info("New PA has atribute {} to be checked for pending", pa.getAttribute());
                    if (supc == null) {

                        supc = req.getEntitiesToPersist().get(0).getSupc();

                        data.setValidationRequired(true);
                        resp = getSUPCDataForValidation(newPAClone, data);

                        return resp;
                    }
                }

                LOG.info("Skipping validation data build for New PA {} ", pa.getAttribute());
            }
        } else {

            LOG.info("Entities in the request are empty");
            data.setValidationRequired(true);
            return getPAValidationDataBuildResp(data, false, "Entities in the request are empty", PABuildValidationDataFailReasons.BAD_REQUEST_NO_ENTITIES);
        }

        return getPAValidationDataBuildResp(data, true, "Successfully populated validation data.", null);
    }

    private GenericInternalMethodResponse<PAValidationData, PABuildValidationDataFailureReasons<PABuildValidationDataFailReasons>> getSUPCDataForValidation(
            List<ProductAttribute> newAttributes, PAValidationData data) {

        String supc = newAttributes.get(0).getSupc();

        if (StringUtils.isEmpty(supc)) {

            LOG.info("SUPC IS NULL, returning false.");
            return getPAValidationDataBuildResp(data, false, "SUPC IS NULL.", PABuildValidationDataFailReasons.EMPTY_SUPC);
        }

        List<ProductAttribute> oldAttributes = productAttrService.getAllProductAttributeForSUPC(supc);

        if (oldAttributes == null || (oldAttributes != null && oldAttributes.isEmpty())) {

            LOG.info("No old attributes found for supc {}, returning false.", supc);
            data.setValidationRequired(false);
            return getPAValidationDataBuildResp(data, true, "No old attributes found for supc " + supc, PABuildValidationDataFailReasons.NO_OLD_ATTRIBUTES);
        }

        try {

            if (!populateWeights(oldAttributes, newAttributes, data)) {

                LOG.info("Exception occured while populating weights in PAValidationData for newAttributes {}", newAttributes);
                return getPAValidationDataBuildResp(data, false, "Exception occured while populating weights in PAValidationData for newAttributes",
                        PABuildValidationDataFailReasons.EXCEPTION_POPULATING_WEIGHTS);
            }
        } catch (Exception e) {

            LOG.info("Exception occured while populating weights in PAValidationData for newAttributes {}", newAttributes);
            LOG.error("Exception occured while populating weights in PAValidationData for newAttributes", e);
            return getPAValidationDataBuildResp(data, false, "Exception occured while populating weights in PAValidationData for newAttributes",
                    PABuildValidationDataFailReasons.EXCEPTION_POPULATING_WEIGHTS);
        }

        DeliveryTypeForSUPCResponse deliveryTypeResp = deliveryTypeService.getDeliveryTypeForSUPC(supc);
        if (deliveryTypeResp == null || (deliveryTypeResp != null && deliveryTypeResp.isError())) {

            LOG.info("No delivery type found for supc {}, returning false.", supc);
            return getPAValidationDataBuildResp(data, false, "No delivery type found for supc " + supc, PABuildValidationDataFailReasons.NO_DELIVERY_TYPE_FOUND);
        }

        data.setDeliveryType(deliveryTypeResp.getDeliveryType());

        PackagingTypeForSUPCResponse packagingTypeResp = packagingTypeService.getPackagingTypeForSUPC(supc);
        if (packagingTypeResp == null || (packagingTypeResp != null && packagingTypeResp.isError())) {

            LOG.info("No packaging type found for supc {}, returning false.", supc);
            return getPAValidationDataBuildResp(data, false, "No packaging type found for supc " + supc, PABuildValidationDataFailReasons.NO_PACKAGING_TYPE_FOUND);
        }

        data.setPackagingType(packagingTypeResp.getPackageType());

        IVIPMSExternalService vipmsService = externalServiceFactory.getVIPMSPSExternalService();

        List<String> sellerCodes = vipmsService.getAllSellerCodesForSUPC(supc);
        if (sellerCodes == null || (sellerCodes != null && sellerCodes.isEmpty())) {

            LOG.info("No sellers found for supc {}, returning false.", supc);
            return getPAValidationDataBuildResp(data, false, "No sellers found for supc " + supc, PABuildValidationDataFailReasons.NO_VENDORS_FOUND);
        }

        ResultHolder result = populateFeeChanges(sellerCodes, data, supc);
        
        if (!result.isSuccess()) {

            String msg = "Validation failed for populating fee changes. Message: " + result.getMessage();
            LOG.info(msg);
            return getPAValidationDataBuildResp(data, false, msg, PABuildValidationDataFailReasons.EXCEPTION_POPULATING_WEIGHTS);
        }

        LOG.info("Validation data populated for supc {} - currentUsedWeight - {}, newDeadWeight - {}, newVolWeight - {} , isFeeChangedByNewWeight - {} "
                + ", isFeeChangedByNewVolWeight - {}, deliveryType - {} , packagingType - {}", supc, data.getCurrentUsedWeight(), data.getNewDeadWeight(), data.getNewVolWeight(),
                data.getIsFeeChangedByNewWeight(), data.getIsFeeChangedByNewVolWeight(), data.getDeliveryType(), data.getPackagingType());

        return getPAValidationDataBuildResp(data, true, "Validation data populated for supc " + supc, null);
    }

    private GenericInternalMethodResponse<PAValidationData, PABuildValidationDataFailureReasons<PABuildValidationDataFailReasons>> getPAValidationDataBuildResp(
            PAValidationData data, boolean successful, String message, PABuildValidationDataFailReasons reason) {

        GenericInternalMethodResponse<PAValidationData, PABuildValidationDataFailureReasons<PABuildValidationDataFailReasons>> resp = new GenericInternalMethodResponse<PAValidationData, PABuildValidationDataFailureReasons<PABuildValidationDataFailReasons>>();

        resp.setMessage(message);
        resp.setSuccessful(successful);
        data.setValidationDataBuildSuccessful(successful);
        resp.setT(data);
        resp.setS(new PABuildValidationDataFailureReasons<PABuildValidationDataFailReasons>(reason));
        return resp;
    }

    private ResultHolder populateFeeChanges(List<String> sellerCodes, PAValidationData data, String supc) {

        List<SellerFulfillmentModelSRO> sros = new ArrayList<SellerFulfillmentModelSRO>(0);
        FulfillmentModel fulfillmentModel = null;

        for (String sellerCode : sellerCodes) {

            try {
                fulfillmentModel = fulfilmentModelService.getFulfilmentModel(sellerCode, supc);
            } catch (ExternalDataNotFoundException e) {
                LOG.error("FM not found {}", e.getMessage());
            }

            if (fulfillmentModel == null) {

                LOG.info("fulfillmentModel found null for supc {}, seller {}", supc, sellerCode);
                continue;
            }

            sros.add(new SellerFulfillmentModelSRO(sellerCode, fulfillmentModel.getCode()));
        }

        if (sros.isEmpty()) {

            LOG.info("No SellerFulfillmentModelSRO populated for supc {}, sellers {}", supc, sellerCodes);
            return new ResultHolder(false, "FulfillmentModel found null for supc " + supc + ", sellers " + sellerCodes);
        }

        IsFulfillmentFeeChangedRequest isFeeChangedReq = new IsFulfillmentFeeChangedRequest(supc, data.getCurrentUsedWeight(), data.getNewUsedWeight(),
                data.getDeliveryType().name(), data.getPackagingType().getCode().equalsIgnoreCase("Wooden"), sros);
        try {
            IOPSExternalService opsService = externalServiceFactory.getOPSExternalService();

            IsFulfillmentFeeChangedResponse isFeeChangedResp = opsService.isFulfillmentFeeChanged(isFeeChangedReq);

            if (!isFeeChangedResp.isSuccessful()) {

                String msg = "OPS call isFulfillmentFeeChanged for type " + (data.isNewUsedWeightOfTypeDead() ? "Dead-Weight" : "Vol-Weight")
                        + " returned successfull false for supc " + supc + ", CurrentUsedWeight " + data.getCurrentUsedWeight() + ", " + "NewUsedWeight " + data.getNewUsedWeight()
                        + ", DeliveryType " + data.getDeliveryType() + ", PackagingType " + data.getPackagingType() + "";
                LOG.info(msg);
                return createFailedResult(msg);
            }

            String msg = "OPS call isFulfillmentFeeChanged for type " + (data.isNewUsedWeightOfTypeDead() ? "Dead-Weight" : "Vol-Weight") + " returned isFeeChanged "
                    + isFeeChangedResp.isFulfillmentFeeChanged() + " for supc " + supc + ", CurrentUsedWeight " + data.getCurrentUsedWeight() + ", " + "NewUsedWeight "
                    + data.getNewUsedWeight() + ", DeliveryType " + data.getDeliveryType() + ", PackagingType " + data.getPackagingType() + "";
            LOG.info(msg);

            if (!isFeeChangedResp.isFulfillmentFeeChanged()) {

                data.setIsFeeChangedByNewWeight(false);
                data.setIsFeeChangedByNewVolWeight(false);
                return createSuccessResult();
            }

            if (data.isNewUsedWeightOfTypeDead()) {

                data.setIsFeeChangedByNewWeight(isFeeChangedResp.isFulfillmentFeeChanged());
            } else {

                data.setIsFeeChangedByNewVolWeight(isFeeChangedResp.isFulfillmentFeeChanged());
            }
        } catch (OPSException e) {

            String msg = "OPS call isFulfillmentFeeChanged for type " + (data.isNewUsedWeightOfTypeDead() ? "Dead-Weight" : "Vol-Weight") + " threw OPSException for supc " + supc
                    + ", CurrentUsedWeight " + data.getCurrentUsedWeight() + ", " + "NewUsedWeight " + data.getNewUsedWeight() + ", DeliveryType " + data.getDeliveryType()
                    + ", PackagingType " + data.getPackagingType() + ". Retry for this SUPC.";
            LOG.info(msg);
            LOG.error("OPS call isFulfillmentFeeChanged threw OPSException", e);
            return createFailedResult(msg);

        } catch (TransportException e) {

            String msg = "OPS call isFulfillmentFeeChanged for type " + (data.isNewUsedWeightOfTypeDead() ? "Dead-Weight" : "Vol-Weight") + " threw TransportException for supc "
                    + supc + ", CurrentUsedWeight " + data.getCurrentUsedWeight() + ", " + "NewUsedWeight " + data.getNewUsedWeight() + ", DeliveryType " + data.getDeliveryType()
                    + ", PackagingType " + data.getPackagingType() + ". Retry for this SUPC.";
            LOG.info(msg);
            LOG.error("OPS call isFulfillmentFeeChanged threw TransportException", e);
            return createFailedResult(msg);
        }

        return populateNonUsedWeightFeeChange(data, supc, sros);
    }

    private ResultHolder populateNonUsedWeightFeeChange(PAValidationData data, String supc, List<SellerFulfillmentModelSRO> sros) {

        if ((data.isNewUsedWeightOfTypeDead() && data.getNewVolWeight() != null) || (!data.isNewUsedWeightOfTypeDead() && data.getNewDeadWeight() != null)) {

            LOG.info("NonUsedWeightFeeChange check is required since isNewUsedWeightOfTypeDead is {} && new non used wt is not null {}.", data.isNewUsedWeightOfTypeDead(),
                    data.isNewUsedWeightOfTypeDead() ? data.getNewVolWeight() : data.getNewDeadWeight());

            IsFulfillmentFeeChangedRequest isFeeChangeByNonUsedWeight = new IsFulfillmentFeeChangedRequest(supc, data.getCurrentUsedWeight(),
                    data.isNewUsedWeightOfTypeDead() ? data.getNewVolWeight() : data.getNewDeadWeight(), data.getDeliveryType().name(),
                    data.getPackagingType().getCode().equalsIgnoreCase("Wooden"), sros);

            try {
                IOPSExternalService opsService = externalServiceFactory.getOPSExternalService();

                IsFulfillmentFeeChangedResponse isFeeChangedByNonUsedWtResp = opsService.isFulfillmentFeeChanged(isFeeChangeByNonUsedWeight);

                if (!isFeeChangedByNonUsedWtResp.isSuccessful()) {

                    String msg = "OPS call isFulfillmentFeeChanged for new non used wt of type " + (data.isNewUsedWeightOfTypeDead() ? "Vol-Weight" : "Dead-Weight")
                            + " returned successfull false for supc " + supc + ", CurrentUsedWeight " + data.getCurrentUsedWeight() + ", " + "NewNonUsedWeight " + (data.isNewUsedWeightOfTypeDead() ? data.getNewVolWeight() : data.getNewDeadWeight())
                            + ", DeliveryType " + data.getDeliveryType() + ", PackagingType " + data.getPackagingType() + "";
                    LOG.info(msg);
                    return createFailedResult(msg);
                }
                LOG.info("OPS call isFulfillmentFeeChanged for new non used wt of type {} returned isFeeChanged {} for supc {}, CurrentUsedWeight {}, "
                        + "NewNonUsedWeight {}, DeliveryType {}, PackagingType {}", data.isNewUsedWeightOfTypeDead() ? "Vol-Weight" : "Dead-Weight",
                        isFeeChangedByNonUsedWtResp.isFulfillmentFeeChanged(), supc, data.getCurrentUsedWeight(),
                        data.isNewUsedWeightOfTypeDead() ? data.getNewVolWeight() : data.getNewDeadWeight(), data.getDeliveryType().getCode(),
                        data.getPackagingType().getCode().equalsIgnoreCase("Wooden"));

                if (data.isNewUsedWeightOfTypeDead()) {

                    data.setIsFeeChangedByNewVolWeight(isFeeChangedByNonUsedWtResp.isFulfillmentFeeChanged());
                } else {

                    data.setIsFeeChangedByNewWeight(isFeeChangedByNonUsedWtResp.isFulfillmentFeeChanged());
                }

            } catch (OPSException e) {

                String msg = "OPS call isFulfillmentFeeChanged for new non used wt of type " + (data.isNewUsedWeightOfTypeDead() ? "Dead-Weight" : "Vol-Weight")
                        + " threw OPSException for supc " + supc + ", CurrentUsedWeight " + data.getCurrentUsedWeight() + ", " + "NewNonUsedWeight " + (data.isNewUsedWeightOfTypeDead() ? data.getNewVolWeight() : data.getNewDeadWeight())
                        + ", DeliveryType " + data.getDeliveryType() + ", PackagingType " + data.getPackagingType() + ". Retry for this SUPC.";
                LOG.info(msg);
                LOG.error("OPS call isFulfillmentFeeChanged threw OPSException", e);
                return createFailedResult(msg);
                
            } catch (TransportException e) {

                String msg = "OPS call isFulfillmentFeeChanged for new non used wt of type " + (data.isNewUsedWeightOfTypeDead() ? "Dead-Weight" : "Vol-Weight")
                        + " threw TransportException for supc " + supc + ", CurrentUsedWeight " + data.getCurrentUsedWeight() + ", " + "NewNonUsedWeight " + (data.isNewUsedWeightOfTypeDead() ? data.getNewVolWeight() : data.getNewDeadWeight())
                        + ", DeliveryType " + data.getDeliveryType() + ", PackagingType " + data.getPackagingType() + ". Retry for this SUPC.";
                LOG.info(msg);
                LOG.error("OPS call isFulfillmentFeeChanged threw TransportException", e);
                return createFailedResult(msg);
            }
        } else {

            LOG.info("NonUsedWeightFeeChange check is not required since isNewUsedWeightOfTypeDead is {} && new non used wt is null {}.", data.isNewUsedWeightOfTypeDead());

            if (data.isNewUsedWeightOfTypeDead()) {

                data.setIsFeeChangedByNewVolWeight(false);
            } else {

                data.setIsFeeChangedByNewWeight(false);
            }
        }

        return createSuccessResult();
    }

    private boolean populateWeights(List<ProductAttribute> oldAttributes, List<ProductAttribute> newAttributes, PAValidationData data) {

        double volumeWeightFormulaDenominator = ConfigUtils.getIntegerScalar(Property.VOLUMETRIC_WEIGHT_FORMULA_DENOMINATOR);

        Double oldUsedWeight = null;
        Double newUsedWeight = null;
        Double weight = null;
        Double vWeight = null;
        Map<String, Double> oldAttrToValMap = new HashMap<String, Double>(0);

        for (ProductAttribute pa : oldAttributes) {

            if (pa.getAttribute().equals(WEIGHT)) {

                weight = Double.valueOf(pa.getValue());
                oldAttrToValMap.put(pa.getAttribute(), weight);

            } else if (pa.getAttribute().equals(LENGTH) || pa.getAttribute().equals(BREADTH) || pa.getAttribute().equals(HEIGHT)) {

                oldAttrToValMap.put(pa.getAttribute(), Double.valueOf(pa.getValue()));
                if (vWeight == null) {

                    vWeight = Double.valueOf(pa.getValue());
                } else {

                    vWeight *= Double.valueOf(pa.getValue());
                }
            }
        }

        if (weight == null || vWeight == null) {

            LOG.info("Weight or Vol Weight is null returning false.");
            return false;
        }

        vWeight = vWeight / volumeWeightFormulaDenominator;

        oldUsedWeight = weight >= vWeight ? weight : vWeight;
        LOG.info("Using old Weight value {}", oldUsedWeight);

        data.setCurrentUsedWeight(oldUsedWeight);

        Map<String, Double> newAttrToValMap = new HashMap<String, Double>(0);

        for (ProductAttribute pa : newAttributes) {
            if (ConfigUtils.isPresentInList(Property.PENDING_CHECK_ATTRIBUTES_LIST, pa.getAttribute())) {

                newAttrToValMap.put(pa.getAttribute(), Double.valueOf(pa.getValue()));
            }

        }

        LOG.info("newAttrToValMap - " + newAttrToValMap);

        Double newVWeight = null;
        if (newAttrToValMap.get(LENGTH) != null || newAttrToValMap.get(BREADTH) != null || newAttrToValMap.get(HEIGHT) != null) {

            LOG.info("Atlaest one new Vol weight attribute has been changed.");
            newVWeight = (newAttrToValMap.get(LENGTH) != null ? newAttrToValMap.get(LENGTH) : oldAttrToValMap.get(LENGTH))
                    * (newAttrToValMap.get(BREADTH) != null ? newAttrToValMap.get(BREADTH) : oldAttrToValMap.get(BREADTH))
                    * (newAttrToValMap.get(HEIGHT) != null ? newAttrToValMap.get(HEIGHT) : oldAttrToValMap.get(HEIGHT));

            newVWeight = newVWeight / volumeWeightFormulaDenominator;
        }

        Double newWeight = newAttrToValMap.get(WEIGHT);

        LOG.info("New weight {} & vol. weight {}", newWeight != null ? newWeight : "Null", newVWeight != null ? newVWeight : "Null");

        data.setNewDeadWeight(newWeight);
        data.setNewVolWeight(newVWeight);

        newUsedWeight = (newWeight != null ? newWeight : weight) > (newVWeight != null ? newVWeight : vWeight) ? (newWeight != null ? newWeight : weight)
                : (newVWeight != null ? newVWeight : vWeight);

        boolean isNewUsedWeightOfTypeDead = (newWeight != null ? newWeight : weight) > (newVWeight != null ? newVWeight : vWeight);

        LOG.info("New used weight would be {}, isNewUsedWeightOfTypeDead - {}", newUsedWeight, isNewUsedWeightOfTypeDead);

        data.setNewUsedWeight(newUsedWeight);
        data.setNewUsedWeightOfTypeDead(isNewUsedWeightOfTypeDead);
        return true;
    }

    private ResultHolder createFailedResult(String msg) {
        return new ResultHolder(false, msg);
    }

    private ResultHolder createSuccessResult() {
        return new ResultHolder();
    }

    static class ResultHolder {
        boolean bResult;
        String  message;

        public ResultHolder() {
            this.bResult = true;
            this.message = "";
        }

        public ResultHolder(boolean bResult, String message) {
            this.bResult = bResult;
            this.message = message;
        }

        boolean isSuccess() {
            return bResult;
        }

        String getMessage() {
            return message;
        }
    }
}
