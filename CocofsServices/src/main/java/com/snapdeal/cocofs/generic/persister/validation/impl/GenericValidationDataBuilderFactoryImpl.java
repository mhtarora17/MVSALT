package com.snapdeal.cocofs.generic.persister.validation.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.entity.ProductAttribute;
import com.snapdeal.cocofs.generic.persister.validation.IGenericValidationDataBuilder;
import com.snapdeal.cocofs.generic.persister.validation.IGenericValidationDataBuilderFactory;
import com.snapdeal.cocofs.mongo.model.ProductAttributeUnit;

/**
 * @author abhinav
 *
 */

@Service("genericValidationDataBuilderFactory")
public class GenericValidationDataBuilderFactoryImpl implements IGenericValidationDataBuilderFactory {

    @Autowired
    @Qualifier("productAttributeValidationDataBuilder")
    IGenericValidationDataBuilder<ProductAttribute, ProductAttributeUnit> productAttributeValidationDataBuilder;
    
    
    /* (non-Javadoc)
     * @see com.snapdeal.cocofs.aspects.generic.persister.validation.IGenericValidationDataBuilderFactory#getGenericValidationDataBuilder(java.lang.Class)
     */
    @Override
    public <S> IGenericValidationDataBuilder<?, ?> getGenericValidationDataBuilder(Class<S> clazz) {
        
        if(null != clazz){
            
            if(clazz.equals(ProductAttribute.class)){
                
                return productAttributeValidationDataBuilder;
            }
        }
        // TODO Auto-generated method stub
        return null;
    }

}
