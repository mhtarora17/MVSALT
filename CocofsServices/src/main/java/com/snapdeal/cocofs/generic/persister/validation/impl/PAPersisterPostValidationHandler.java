package com.snapdeal.cocofs.generic.persister.validation.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.snapdeal.base.exception.TransportException;
import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.cocofs.access.IPendingProductAttributeDBDataReadService;
import com.snapdeal.cocofs.access.IProductAttributeNotificationDBDataReadService;
import com.snapdeal.cocofs.access.IProductAttributeNotificationMongoDataReadService;
import com.snapdeal.cocofs.access.IProductAttributeService;
import com.snapdeal.cocofs.annotations.TouchyTransaction;
import com.snapdeal.cocofs.common.DeliveryTypeForSUPCResponse;
import com.snapdeal.cocofs.common.PackagingTypeForSUPCResponse;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.entity.PendingProductAttributeUpdate;
import com.snapdeal.cocofs.entity.ProductAttribute;
import com.snapdeal.cocofs.entity.ProductAttributeNotification;
import com.snapdeal.cocofs.enums.NotificationType;
import com.snapdeal.cocofs.external.service.IOPSExternalService;
import com.snapdeal.cocofs.external.service.IVIPMSExternalService;
import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.external.service.factory.IExternalServiceFactory;
import com.snapdeal.cocofs.generic.persister.entity.repo.IGenericEntityRepo;
import com.snapdeal.cocofs.generic.persister.response.GenericPersisterResponse;
import com.snapdeal.cocofs.generic.persister.validation.IPersisterPostValidationHandler;
import com.snapdeal.cocofs.generic.persister.validation.handler.response.PAPersisterPostValidationHandlerResponse;
import com.snapdeal.cocofs.mongo.model.NotificationSubUnit;
import com.snapdeal.cocofs.mongo.model.ProductAttributeNotificationUnit;
import com.snapdeal.cocofs.mongo.model.ProductAttributeUnit;
import com.snapdeal.cocofs.services.IDeliveryTypeService;
import com.snapdeal.cocofs.services.IPackagingTypeService;
import com.snapdeal.cocofs.services.IProductAttributeUnitResetService;
import com.snapdeal.cocofs.services.common.dto.UserInfo;
import com.snapdeal.cocofs.services.fm.IFulfilmentModelService;
import com.snapdeal.cocofs.sro.FulfillmentModel;
import com.snapdeal.ops.base.api.isFulfillmentFeeChangedForSellers.IsFulfillmentFeeChangedForSellersRequest;
import com.snapdeal.ops.base.api.isFulfillmentFeeChangedForSellers.IsFulfillmentFeeChangedForSellersResponse;
import com.snapdeal.ops.base.api.isFulfillmentFeeChangedForSellers.SellerFulfillmentFeeChangeSRO;
import com.snapdeal.ops.base.api.isFulfillmentFeeChangedForSellers.SellerFulfillmentModelSRO;
import com.snapdeal.ops.base.exception.OPSException;
import com.snapdeal.ops.base.exception.OPSException.OPSErrorCode;

/**
 * TO BE REFACTORED.
 * 
 * @author abhinav
 */

@Service("paPersisterPostValidationHandler")
public class PAPersisterPostValidationHandler implements IPersisterPostValidationHandler<ProductAttribute, ProductAttributeUnit, PAPersisterPostValidationHandlerResponse> {

    private static final Logger                       LOG     = LoggerFactory.getLogger(PAPersisterPostValidationHandler.class);

    @Autowired
    private IProductAttributeService                  productAttrService;

    

    @Autowired
    IProductAttributeUnitResetService                 pauResetService;

    @Autowired
    IProductAttributeNotificationMongoDataReadService panuReadService;

    @Autowired
    IPendingProductAttributeDBDataReadService         pendingPADBDataReadService;

    @Autowired
    IProductAttributeNotificationDBDataReadService    paNotificationDBDataReadService;

    @Autowired
    @Qualifier("genericEntityRepo")
    private IGenericEntityRepo<?, Serializable>       entityRepo;

    @Autowired
    @Qualifier("genericMongoEntityRepo")
    private IGenericEntityRepo<?, Serializable>       mongoRepo;
    
    @Autowired
    private IExternalServiceFactory                   externalServiceFactory;
    
    @Autowired
    private IFulfilmentModelService                   fulfilmentModelService;
    
    @Autowired
    private IDeliveryTypeService        deliveryTypeService;
    
    @Autowired
    private IPackagingTypeService       packagingTypeService;

    private static final String                       WEIGHT  = "WEIGHT";

    private static final String                       LENGTH  = "LENGTH";

    private static final String                       BREADTH = "BREADTH";

    private static final String                       HEIGHT  = "HEIGHT";

    @Override
    @SuppressWarnings("unused")
    @Transactional
    public PAPersisterPostValidationHandlerResponse handleValidationResponse(GenericPersisterResponse<ProductAttribute, ProductAttributeUnit> resp, UserInfo userInfo) {

        PAPersisterPostValidationHandlerResponse handlerResp = new PAPersisterPostValidationHandlerResponse();

        String supc = "";
        if (resp.getValidationFailedEntityList() != null & !resp.getValidationFailedEntityList().isEmpty()) {
            supc = resp.getValidationFailedEntityList().get(0).getSupc();
        } else if (resp.getValidationPassedEntityList() != null && !resp.getValidationPassedEntityList().isEmpty()) {
            supc = resp.getValidationPassedEntityList().get(0).getSupc();
        }

        LOG.info("PAPersisterPostValidationHandler handleValidationResponse called for supc {}", supc);

        HandlerData data = new HandlerData();

        handlerResp = populateHandlerData(data, resp, userInfo, supc);

        if (!handlerResp.isSuccessful()) {

            LOG.info("Error occured while populating handler data. Returning resp false for supc {}", supc);
            return handlerResp;
        }

        try {

            handlerResp = persistHandlerData(data);

            LOG.info("Persistence of hander data for supc {} was successful.", supc);
            return handlerResp;
        } catch (Exception e) {

            LOG.info("Exception {} occured while persisting handler data for supc {}", e.getMessage(), supc);
            LOG.error("Exception occured while persisting handler data for supc", e);
            handlerResp.setSuccessful(false);
        }

        return handlerResp;
    }

    private PAPersisterPostValidationHandlerResponse populateHandlerData(HandlerData data, GenericPersisterResponse<ProductAttribute, ProductAttributeUnit> resp,
            UserInfo userInfo, String supc) {

        PAPersisterPostValidationHandlerResponse handlerResp = new PAPersisterPostValidationHandlerResponse();
        handlerResp.setSuccessful(true);

        List<ProductAttribute> oldAttributes = productAttrService.getAllProductAttributeForSUPC(supc);

        Map<String, ProductAttribute> oldWeightAttributesMap = getOldWeightAttributesMap(oldAttributes);

        Date playDate = getPendingPlayDate(userInfo);
        Date notificationEndDate = DateUtils.addToDate(DateUtils.getCurrentTime(), Calendar.DATE, ConfigUtils.getIntegerScalar(Property.PANU_NOTIFICATION_EXPIRY));

        populatePendingAttributes(resp.getValidationPassedEntityList(), resp.getValidationFailedEntityList(), data, userInfo, oldWeightAttributesMap, playDate);

        if (data.getPendingPAList().isEmpty() && data.getNonPendingPAList().isEmpty()) {

            LOG.info("Returning resp successful false, since both pendingPAList and nonNendingPAList is empty");
            handlerResp.setSuccessful(false);
            return handlerResp;
        }

        getPAUsToPersist(data, resp, userInfo);

        if (data.getPauToPersist() == null || (data.getPauToPersist() != null && data.getPauToPersist().isEmpty())) {

            LOG.info("No PAU to persist. Will set resp successful false for supc {}", supc);
            handlerResp.setSuccessful(false);
            return handlerResp;
        }

        try {
            
            populateNotifications(data, userInfo, playDate, notificationEndDate, oldWeightAttributesMap, resp.getValidationFailedEntityList(), supc);
        } catch (OPSException e) {

            LOG.error("OPSException occured while populating notifications", e);
            handlerResp.setSuccessful(false);
            return handlerResp;
        } catch (TransportException e) {
            
            LOG.error("TransportException occured while populating notifications", e);
            handlerResp.setSuccessful(false);
            return handlerResp;
        }

        //Things to disable/delete will now be populated
        data.setPaNotificationUnitToDisable(panuReadService.getVisibleProductAttributeNotificationBySUPC(supc));

        LOG.info("PanuReadService returned paNotificationUnitToDisable {} for supc {}", data.getPaNotificationUnitToDisable() != null ? data.getPaNotificationUnitToDisable()
                : "NULL", supc);

        data.setPendingPAListToDisable(pendingPADBDataReadService.getPendingProductAtributeUpdate(supc));

        LOG.info("PendingPADBDataReadService returned PendingPAListToDisable {} for supc {}", data.getPendingPAList() != null ? data.getPendingPAList() : "NULL", supc);

        data.setPaNotificationListToDisable(paNotificationDBDataReadService.getVisibleProductAttributeNotifications(supc));

        LOG.info("paNotificationDBDataReadService returned PaNotificationListToDisable {} for supc {}",
                data.getPaNotificationListToDisable() != null ? data.getPaNotificationListToDisable() : "NULL", supc);

        return handlerResp;
    }

    @TouchyTransaction
    private PAPersisterPostValidationHandlerResponse persistHandlerData(HandlerData data) {

        PAPersisterPostValidationHandlerResponse handlerResp = new PAPersisterPostValidationHandlerResponse();
        handlerResp.setSuccessful(true);

        if (!data.getNonPendingPAList().isEmpty()) {

            LOG.info("Going to persist nonPendingPAList {}", data.getNonPendingPAList());
            for (ProductAttribute pa : data.getNonPendingPAList()) {

                LOG.info("Going to persist nonPendingPAList - pa {}", pa);
                handlerResp.getPersistedEntity().add(entityRepo.save(pa));
            }
        }

        if (!data.getPendingPAList().isEmpty()) {

            LOG.info("Going to persist pendingPAList {}", data.getPendingPAList());
            for (PendingProductAttributeUpdate ppa : data.getPendingPAList()) {

                LOG.info("Going to persist PendingPAList - ppa {}", ppa);
                handlerResp.getPendingPAList().add(entityRepo.save(ppa));
            }
        }

        if (!data.getPendingPAListToDisable().isEmpty()) {

            LOG.info("Going to persist PendingPAListToDisable {}", data.getPendingPAListToDisable());
            for (PendingProductAttributeUpdate ppa : data.getPendingPAListToDisable()) {

                ppa.setPending(false);
                LOG.info("Going to persist PendingPAListToDisable - ppa {}", ppa);
                entityRepo.save(ppa);
            }
        }

        if (!data.getPaNotificationList().isEmpty()) {

            LOG.info("Going to persist PANotificationList {}", data.getPaNotificationList());
            for (ProductAttributeNotification pan : data.getPaNotificationList()) {

                LOG.info("Going to persist PaNotificationList - pan {}", pan);
                entityRepo.save(pan);
            }
        }

        if (!data.getPaNotificationListToDisable().isEmpty()) {

            LOG.info("Going to persist PaNotificationListToDisable {}", data.getPaNotificationListToDisable());
            for (ProductAttributeNotification pan : data.getPaNotificationListToDisable()) {

                LOG.info("Going to persist PaNotificationListToDisable - pan {}", pan);
                pan.setVisible(false);
                entityRepo.save(pan);
            }
        }

        LOG.info("Going to persist PauToPersist {}", data.getPauToPersist());
        for (ProductAttributeUnit pau : data.getPauToPersist()) {

            LOG.info("Going to persist PauToPersist - pan {}", pau);
            handlerResp.getPersistedDocument().add(mongoRepo.save(pau));
        }

        if (data.getPaNotificationUnitToDisable() != null) {

            LOG.info("Going to persist PaNotificationUnitToDisable {}", data.getPaNotificationUnitToDisable());
            data.getPaNotificationUnitToDisable().setVisible(false);
            mongoRepo.save(data.getPaNotificationUnitToDisable());
        }

        if (data.getPaNotificationUnit() != null) {

            LOG.info("Going to persist PaNotificationUnit {}", data.getPaNotificationUnit());
            mongoRepo.save(data.getPaNotificationUnit());
        }

        return handlerResp;
    }

    private void getPAUsToPersist(HandlerData data, GenericPersisterResponse<ProductAttribute, ProductAttributeUnit> resp, UserInfo userInfo) {

        if (!resp.getNonPersistedDocument().isEmpty()) {

            if (resp.getValidationFailedEntityList().isEmpty()) {

                LOG.info("Since ValidationFailedEntityList is empty, returning NonPersistedDocument.");
                data.setPauToPersist(resp.getNonPersistedDocument());
            } else {

                data.setPauToPersist(new ArrayList<ProductAttributeUnit>(0));
                if (!userInfo.isSuper()) {
                    ProductAttributeUnit pau = pauResetService.resetAttributes(resp.getNonPersistedDocument().get(0), resp.getValidationFailedEntityList());

                    LOG.info("PauResetService returning pau {} for non persisted pau {} and validation failed entities {}", pau != null ? pau : "NULL",
                            resp.getNonPersistedDocument().get(0), resp.getValidationFailedEntityList());

                    data.getPauToPersist().add(pau);
                } else {
                    LOG.info("Super user is doing the update... persisting new documents {}", resp.getNonPersistedDocument());
                    data.setPauToPersist(resp.getNonPersistedDocument());
                }
            }
        } else {

            LOG.info("Couldn't load pauToPersist list since resp has empty NonPersistedDocuments");
        }

    }

    private void populatePendingAttributes(List<ProductAttribute> validationPassedPAs, List<ProductAttribute> validationfailedPAs, HandlerData data, UserInfo userInfo,
            Map<String, ProductAttribute> oldWeightAttributesMap, Date playDate) {

        LOG.info("Received for populating pending PAs - validationPassedPAs {}, validationfailedPAs {}", validationPassedPAs, validationfailedPAs);
        data.setNonPendingPAList(validationPassedPAs);

        if (!validationfailedPAs.isEmpty()) {

            if (userInfo.isSuper()) {
                LOG.info("User {} is super, adding validationfailedPAs to non pending list.", userInfo.getEmail());
                if (null == data.getNonPendingPAList()) {
                    data.setNonPendingPAList(new ArrayList<ProductAttribute>());
                }
                data.getNonPendingPAList().addAll(validationfailedPAs);
            } else {

                LOG.info("User {} is not super, adding validationfailedPAs to pending list.", userInfo.getEmail());

                for (ProductAttribute pa : validationfailedPAs) {

                    data.getPendingPAList().add(
                            new PendingProductAttributeUpdate(pa.getSupc(), oldWeightAttributesMap.get(pa.getAttribute()).getValue(), pa.getValue(), pa.getAttribute(), new Date(),
                                    playDate, true, "Validation Failed",userInfo.getEmail()));
                    //Following condition is added for weight capture--Kirti
                    processCapturedWeighforPendingAttributes(pa,validationPassedPAs,validationfailedPAs,data,userInfo,oldWeightAttributesMap,playDate);
                }
            }
        }

        LOG.info("Returning nonNendingPAList {}, pendingPAList {}", data.getNonPendingPAList(), data.getPendingPAList());
    }

	private void processCapturedWeighforPendingAttributes(ProductAttribute pa,
			List<ProductAttribute> validationPassedPAs,
			List<ProductAttribute> validationfailedPAs, HandlerData data,
			UserInfo userInfo,
			Map<String, ProductAttribute> oldWeightAttributesMap, Date playDate) {
		if (ConfigUtils
				.getStringScalar(Property.SYSTEM_WEIGHT_CAPTURE_ENABLED) != null
				&& ConfigUtils.getStringScalar(
						Property.SYSTEM_WEIGHT_CAPTURE_ENABLED).equals("true")
				&& pa.getAttribute().equals("WEIGHT")) {
			ProductAttribute swc = null;
			for (ProductAttribute vpa : validationPassedPAs) {
				if (vpa.getAttribute().equals(
						"SYSTEMWEIGHTCAPTURED")) {
					swc = vpa;
					break;
				}
			}

			if (swc != null) {
				data.getPendingPAList()
						.add(new PendingProductAttributeUpdate(
								swc.getSupc(),
								oldWeightAttributesMap.get(swc.getAttribute()) == null ? "false"
										: oldWeightAttributesMap.get(
												swc.getAttribute()).getValue(),
								swc.getValue(), swc.getAttribute(), new Date(),
								playDate, true, "Validation Failed",userInfo.getEmail()));
				validationPassedPAs.remove(swc);
			}
		}
    }
    private Map<String, ProductAttribute> getOldWeightAttributesMap(List<ProductAttribute> oldAttributes) {

        Map<String, ProductAttribute> oldWeightAttributesMap = new HashMap<String, ProductAttribute>(0);

        for (ProductAttribute pa : oldAttributes) {
            if (ConfigUtils.isPresentInList(Property.PENDING_CHECK_ATTRIBUTES_LIST, pa.getAttribute())) {

                oldWeightAttributesMap.put(pa.getAttribute(), pa);
            }
        }

        LOG.info("Returning oldWeightAttributesMap {}", oldWeightAttributesMap);
        return oldWeightAttributesMap;
    }

    @SuppressWarnings("unused")
    private void populateNotifications(HandlerData data, UserInfo userInfo, Date playDate, Date notificationEndDate, Map<String, ProductAttribute> oldWeightAttributesMap,
            List<ProductAttribute> validationfailedPAs, String supc) throws OPSException, TransportException {

        if (!validationfailedPAs.isEmpty()) {

            IVIPMSExternalService vipmsService = externalServiceFactory.getVIPMSPSExternalService();
            List<String> sellerCodes = vipmsService .getAllSellerCodesForSUPC(supc);

            if (sellerCodes != null && !sellerCodes.isEmpty()) {

                Weights weights = populateWeights(oldWeightAttributesMap, validationfailedPAs);

                ProductAttributeNotificationUnit paNotificationUnit = new ProductAttributeNotificationUnit();
                paNotificationUnit.setCreated(new Date());
                paNotificationUnit.setNotificationEndDate(notificationEndDate);
                paNotificationUnit.setSupc(supc);
                paNotificationUnit.setWithEffectFrom(playDate);
                paNotificationUnit.setVisible(true);
                paNotificationUnit.setSellers(getFeeChangedSellers(sellerCodes, supc, weights));
                paNotificationUnit.setSubUnits(new ArrayList<NotificationSubUnit>(0));

                if (weights.getNewWeight() != null) {

                    LOG.info("New weight is not null, will populate notification entity of type DEAD-WEIGHT");
                    data.getPaNotificationList().add(
                            new ProductAttributeNotification(supc, "DEAD-WEIGHT", weights.getOldWeight().toString(), weights.getNewWeight().toString(), playDate, new Date(),
                                    notificationEndDate, userInfo.isSuper() ? "SUPER" : "NON-SUPER", true, true));

                    paNotificationUnit.getSubUnits().add(
                            new NotificationSubUnit(weights.getOldWeight().toString(), weights.getNewWeight().toString(), playDate, NotificationType.DEAD_WEIGHT.getCode()));
                }

                if (weights.getNewVolWeight() != null) {

                    LOG.info("New vol-weight is not null, will populate notification entity of type VOL-WEIGHT");
                    data.getPaNotificationList().add(
                            new ProductAttributeNotification(supc, "VOL-WEIGHT", weights.getOldVolWeight().toString(), weights.getNewVolWeight().toString(), playDate, new Date(),
                                    notificationEndDate, userInfo.isSuper() ? "SUPER" : "NON-SUPER", true, true));

                    paNotificationUnit.getSubUnits().add(
                            new NotificationSubUnit(weights.getOldVolWeight().toString(), weights.getNewVolWeight().toString(), playDate,
                                    NotificationType.VOLUMETRIC_WEIGHT.getCode()));
                }

                data.setPaNotificationUnit(paNotificationUnit);
            } else {

                LOG.info("Seller code returned for supc {} is null/empty", supc);
            }
        }

    }

    private List<String> getFeeChangedSellers(List<String> sellerCodes, String supc, Weights weights) throws OPSException, TransportException{
        
        List<String> feeChangedSellerCodes = new ArrayList<String>(0);
        
        List<SellerFulfillmentModelSRO> sros = new ArrayList<SellerFulfillmentModelSRO>(0);
        FulfillmentModel fulfillmentModel = null;
        
        for(String sellerCode : sellerCodes){
            
            try {
                fulfillmentModel = fulfilmentModelService.getFulfilmentModel(sellerCode, supc);
            } catch (ExternalDataNotFoundException e) {
                LOG.error("FM not foundd {}", e.getMessage());
            }
            
            if(fulfillmentModel == null){
                
                LOG.info("fulfillmentModel found null for supc {}, seller {}", supc, sellerCode);
                continue;
            }
            
            sros.add(new SellerFulfillmentModelSRO(sellerCode, fulfillmentModel.getCode()));
        }
        
        DeliveryTypeForSUPCResponse deliveryTypeResp  = deliveryTypeService.getDeliveryTypeForSUPC(supc);
        
        PackagingTypeForSUPCResponse packagingTypeResp = packagingTypeService.getPackagingTypeForSUPC(supc);
        
        
        IsFulfillmentFeeChangedForSellersRequest req = new IsFulfillmentFeeChangedForSellersRequest(supc, weights.getOldUsedWeight(), 
                weights.getNewUsedWeight(), deliveryTypeResp.getDeliveryType().name(), 
                packagingTypeResp.getPackageType().getCode().equalsIgnoreCase("Wooden"), sros);
        
        IOPSExternalService opsService = externalServiceFactory.getOPSExternalService();
        IsFulfillmentFeeChangedForSellersResponse resp = opsService.isFulfillmentFeeChangedForSellers(req);
        
        
        if(resp.isSuccessful()){
            
            List<SellerFulfillmentFeeChangeSRO> feeChangedSros = resp.getSellerFulfillmentFeeChangeSROs();
            for(SellerFulfillmentFeeChangeSRO feeChangedSro : feeChangedSros){
                
                if(feeChangedSro.isFulfillmentFeeChanged()){
                    
                    LOG.info("Fee has changed for seller {}", feeChangedSro.getSellerCode());
                    feeChangedSellerCodes.add(feeChangedSro.getSellerCode());
                }else{
                    
                    LOG.info("Fee has not changed for seller {}", feeChangedSro.getSellerCode());
                }
                
            }
            
            LOG.info("OPS call isFulfillmentFeeChangedForSellers returned successful false for sellers {} - supc {}, CurrentUsedWeight {}, " +
                    "NewUsedWeight {}, DeliveryType {}, PackagingType {}", feeChangedSellerCodes, supc, weights.getOldUsedWeight(), 
                    weights.getNewUsedWeight(), deliveryTypeResp.getDeliveryType().name(), packagingTypeResp.getPackageType().getCode());
        }else{
            
            LOG.info("OPS call isFulfillmentFeeChangedForSellers returned FeeChanged for sellers {} - supc {}, CurrentUsedWeight {}, " +
                    "NewUsedWeight {}, DeliveryType {}, PackagingType {}", sellerCodes, supc, weights.getOldUsedWeight(), 
                    weights.getNewUsedWeight(), deliveryTypeResp.getDeliveryType().name(), packagingTypeResp.getPackageType().getCode());
            
            throw new OPSException(OPSErrorCode.IMPROPER_DATA, "successful false");
        }
        
        return feeChangedSellerCodes;
    }
    
    
    private Weights populateWeights(Map<String, ProductAttribute> oldWeightAttributesMap, List<ProductAttribute> validationfailedPAs) {

        Double oldWeight = null;
        Double oldVolWeight = null;
        Double newVolWeight = null;
        Double newWeight = null;
        Double oldUsedWeight = null;
        Double newUsedWeight = null;
        
        double VolWeightFormulaDenominator = ConfigUtils.getIntegerScalar(Property.VOLUMETRIC_WEIGHT_FORMULA_DENOMINATOR);

        oldWeight = Double.valueOf(oldWeightAttributesMap.get(WEIGHT).getValue());

        oldVolWeight = (Double.valueOf(oldWeightAttributesMap.get(BREADTH).getValue()) * Double.valueOf(oldWeightAttributesMap.get(LENGTH).getValue()) * Double.valueOf(oldWeightAttributesMap.get(
                HEIGHT).getValue())) / VolWeightFormulaDenominator;

        if (oldWeight == null || oldVolWeight == null) {

            LOG.info("Either old weight/vol-weight is null, will not be able to populate notifications");
            return null;
        }

        oldUsedWeight = oldWeight > oldVolWeight ? oldWeight : oldVolWeight;
        
        Map<String, Double> validationfailedAttrToValMap = new HashMap<String, Double>(0);

        for (ProductAttribute pa : validationfailedPAs) {

            validationfailedAttrToValMap.put(pa.getAttribute(), Double.valueOf(pa.getValue()));
        }

        LOG.info("validationfailedAttrToValMap - " + validationfailedAttrToValMap);

        if (validationfailedAttrToValMap.get(LENGTH) != null || validationfailedAttrToValMap.get(BREADTH) != null || validationfailedAttrToValMap.get(HEIGHT) != null) {

            LOG.info("Atlaest one new Vol weight attribute has been changed.");
            Double length = validationfailedAttrToValMap.get(LENGTH) != null ? validationfailedAttrToValMap.get(LENGTH)
                    : Double.valueOf(oldWeightAttributesMap.get(LENGTH).getValue());

            Double breadth = validationfailedAttrToValMap.get(BREADTH) != null ? validationfailedAttrToValMap.get(BREADTH)
                    : Double.valueOf(oldWeightAttributesMap.get(BREADTH).getValue());

            Double height = validationfailedAttrToValMap.get(HEIGHT) != null ? validationfailedAttrToValMap.get(HEIGHT)
                    : Double.valueOf(oldWeightAttributesMap.get(HEIGHT).getValue());

            newVolWeight = (length * breadth * height) / VolWeightFormulaDenominator;
            LOG.info("For calculated new vol-weight {} in notification, used L {}, B {}, H {}", newVolWeight, length, breadth, height);
        }

        newWeight = validationfailedAttrToValMap.get(WEIGHT);

        newUsedWeight = (newWeight != null ? newWeight : oldWeight) > (newVolWeight != null ? newVolWeight : oldVolWeight) ? 
                (newWeight != null ? newWeight : oldWeight) : (newVolWeight != null ? newVolWeight : oldVolWeight);
        
        LOG.info("New weight {} & vol. weight {}", newWeight != null ? newWeight : "Null", newVolWeight != null ? newVolWeight : "Null");
        LOG.info("oldUsedWeight {}, newUsedWeight {}", oldUsedWeight, newUsedWeight);

        return new Weights(oldWeight, oldVolWeight, newVolWeight, newWeight, oldUsedWeight, newUsedWeight);
    }

    private Date getPendingPlayDate(UserInfo userInfo) {

        Calendar c = Calendar.getInstance();
        if (!userInfo.isSuper()) {
            c.add(Calendar.DAY_OF_MONTH, ConfigUtils.getIntegerScalar(Property.PPAU_WAITING_PERIOD));
        }

        return c.getTime();
    }

    /**
     * Data used by handler to populate and persist.
     * 
     * @author abhinav
     */
    private class HandlerData {

        private List<ProductAttribute>              nonPendingPAList            = new ArrayList<ProductAttribute>(0);
        private List<ProductAttributeUnit>          pauToPersist                = null;

        private List<PendingProductAttributeUpdate> pendingPAList               = new ArrayList<PendingProductAttributeUpdate>(0);
        private List<PendingProductAttributeUpdate> pendingPAListToDisable      = new ArrayList<PendingProductAttributeUpdate>(0);

        private List<ProductAttributeNotification>  paNotificationList          = new ArrayList<ProductAttributeNotification>(0);
        private List<ProductAttributeNotification>  paNotificationListToDisable = new ArrayList<ProductAttributeNotification>(0);

        private ProductAttributeNotificationUnit    paNotificationUnit          = null;
        private ProductAttributeNotificationUnit    paNotificationUnitToDisable = null;

        public List<ProductAttribute> getNonPendingPAList() {
            return nonPendingPAList;
        }

        public void setNonPendingPAList(List<ProductAttribute> nonPendingPAList) {
            this.nonPendingPAList = nonPendingPAList;
        }

        public List<ProductAttributeUnit> getPauToPersist() {
            return pauToPersist;
        }

        public void setPauToPersist(List<ProductAttributeUnit> pauToPersist) {
            this.pauToPersist = pauToPersist;
        }

        public List<PendingProductAttributeUpdate> getPendingPAList() {
            return pendingPAList;
        }

        public void setPendingPAList(List<PendingProductAttributeUpdate> pendingPAList) {
            this.pendingPAList = pendingPAList;
        }

        public List<PendingProductAttributeUpdate> getPendingPAListToDisable() {
            return pendingPAListToDisable;
        }

        public void setPendingPAListToDisable(List<PendingProductAttributeUpdate> pendingPAListToDisable) {
            this.pendingPAListToDisable = pendingPAListToDisable;
        }

        public List<ProductAttributeNotification> getPaNotificationList() {
            return paNotificationList;
        }

        public void setPaNotificationList(List<ProductAttributeNotification> paNotificationList) {
            this.paNotificationList = paNotificationList;
        }

        public List<ProductAttributeNotification> getPaNotificationListToDisable() {
            return paNotificationListToDisable;
        }

        public void setPaNotificationListToDisable(List<ProductAttributeNotification> paNotificationListToDisable) {
            this.paNotificationListToDisable = paNotificationListToDisable;
        }

        public ProductAttributeNotificationUnit getPaNotificationUnit() {
            return paNotificationUnit;
        }

        public void setPaNotificationUnit(ProductAttributeNotificationUnit paNotificationUnit) {
            this.paNotificationUnit = paNotificationUnit;
        }

        public ProductAttributeNotificationUnit getPaNotificationUnitToDisable() {
            return paNotificationUnitToDisable;
        }

        public void setPaNotificationUnitToDisable(ProductAttributeNotificationUnit paNotificationUnitToDisable) {
            this.paNotificationUnitToDisable = paNotificationUnitToDisable;
        }

    }

    private class Weights {

        private Double oldWeight    = null;
        private Double oldVolWeight = null;
        private Double newVolWeight = null;
        private Double newWeight    = null;
        private Double oldUsedWeight = null;
        private Double newUsedWeight = null;

        public Weights() {

        }

        public Weights(Double oldWeight, Double oldVolWeight, Double newVolWeight, Double newWeight, Double oldUsedWeight, Double newUsedWeight) {

            this.oldWeight = oldWeight;
            this.oldVolWeight = oldVolWeight;
            this.newVolWeight = newVolWeight;
            this.newWeight = newWeight;
            this.oldUsedWeight = oldUsedWeight;
            this.newUsedWeight = newUsedWeight;
        }

        public Double getOldWeight() {
            return oldWeight;
        }

        public void setOldWeight(Double oldWeight) {
            this.oldWeight = oldWeight;
        }

        public Double getOldVolWeight() {
            return oldVolWeight;
        }

        public void setOldVolWeight(Double oldVolWeight) {
            this.oldVolWeight = oldVolWeight;
        }

        public Double getNewVolWeight() {
            return newVolWeight;
        }

        public void setNewVolWeight(Double newVolWeight) {
            this.newVolWeight = newVolWeight;
        }

        public Double getNewWeight() {
            return newWeight;
        }

        public void setNewWeight(Double newWeight) {
            this.newWeight = newWeight;
        }

        public Double getOldUsedWeight() {
            return oldUsedWeight;
        }

        public void setOldUsedWeight(Double oldUsedWeight) {
            this.oldUsedWeight = oldUsedWeight;
        }

        public Double getNewUsedWeight() {
            return newUsedWeight;
        }

        public void setNewUsedWeight(Double newUsedWeight) {
            this.newUsedWeight = newUsedWeight;
        }

    }
}
