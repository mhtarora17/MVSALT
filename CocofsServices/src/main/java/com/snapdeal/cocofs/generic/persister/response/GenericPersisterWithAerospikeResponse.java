/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.generic.persister.response;

import java.util.ArrayList;
import java.util.List;

/**
 *  
 *  @version     1.0, 15-Sep-2014
 *  @author ankur
 */
public class GenericPersisterWithAerospikeResponse<E,R> {

    private List<E> persistedEntity = new ArrayList<E>(0);
    
    private List<E> nonPersistedEntity = new ArrayList<E>(0);
    
    private List<R> persistedRecords = new ArrayList<R>(0);
    
    private List<R> nonPersistedRecords = new ArrayList<R>(0);
    
    private List<E> validationFailedEntityList = new ArrayList<E>(0);
    
    private List<E> validationPassedEntityList = new ArrayList<E>(0);
    
    private String message;
    
    private boolean successful;
    
    public List<E> getPersistedEntity() {
        return persistedEntity;
    }

    public void setPersistedEntity(List<E> persistedEntity) {
        this.persistedEntity = persistedEntity;
    }

    public List<E> getNonPersistedEntity() {
        return nonPersistedEntity;
    }

    public void setNonPersistedEntity(List<E> nonPersistedEntity) {
        this.nonPersistedEntity = nonPersistedEntity;
    }

    public List<R> getPersistedRecords() {
        return persistedRecords;
    }

    public void setPersistedRecords(List<R> persistedRecords) {
        this.persistedRecords = persistedRecords;
    }

    public List<R> getNonPersistedRecords() {
        return nonPersistedRecords;
    }

    public void setNonPersistedRecords(List<R> nonPersistedRecords) {
        this.nonPersistedRecords = nonPersistedRecords;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSuccessful() {
        return successful;
    }

    public void setSuccessful(boolean successful) {
        this.successful = successful;
    }

    public List<E> getValidationFailedEntityList() {
        return validationFailedEntityList;
    }

    public void setValidationFailedEntityList(List<E> validationFailedEntityList) {
        this.validationFailedEntityList = validationFailedEntityList;
    }

    public List<E> getValidationPassedEntityList() {
        return validationPassedEntityList;
    }

    public void setValidationPassedEntityList(List<E> validationPassedEntityList) {
        this.validationPassedEntityList = validationPassedEntityList;
    }
}
