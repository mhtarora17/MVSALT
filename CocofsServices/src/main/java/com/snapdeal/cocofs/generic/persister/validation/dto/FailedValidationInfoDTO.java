/**
 * 
 */
package com.snapdeal.cocofs.generic.persister.validation.dto;

import com.snapdeal.cocofs.generic.persister.validation.failReason.IValidationFailReason;

/**
 * @author abhinav
 *
 */
public class FailedValidationInfoDTO<T, S extends IValidationFailReason> {

    private T t;
    
    private S s;

    public FailedValidationInfoDTO(){
        
    }
    
    public FailedValidationInfoDTO(T t, S s) {
        this.t = t;
        this.s = s;
    }
    
    /**
     * T - the entity for which validation has failed.
     * @return t
     */
    public T getT() {
        return t;
    }

    public void setT(T t) {
        this.t = t;
    }

    /**
     * S - Entity specific implementation of IValidationFailReason.
     * @return s
     */
    public S getS() {
        return s;
    }

    public void setS(S s) {
        this.s = s;
    }

    
    
    
}
