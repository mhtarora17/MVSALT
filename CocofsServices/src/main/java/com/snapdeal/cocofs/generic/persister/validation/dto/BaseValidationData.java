/**
 * 
 */
package com.snapdeal.cocofs.generic.persister.validation.dto;

/**
 * @author abhinav
 *
 */
public class BaseValidationData {

    private boolean validationDataBuildSuccessful;

    private boolean validationRequired;
    
    
    public boolean isValidationDataBuildSuccessful() {
        return validationDataBuildSuccessful;
    }

    public void setValidationDataBuildSuccessful(boolean validationDataBuildSuccessful) {
        this.validationDataBuildSuccessful = validationDataBuildSuccessful;
    }

    public boolean isValidationRequired() {
        return validationRequired;
    }

    public void setValidationRequired(boolean validationRequired) {
        this.validationRequired = validationRequired;
    }
    
    
    
    
}
