/**
 * 
 */
package com.snapdeal.cocofs.generic.persister.request;

import java.util.ArrayList;
import java.util.List;

import com.snapdeal.cocofs.services.common.dto.UserInfo;

/**
 * @author abhinav
 */
public class GenericPersisterRequest<T, D> {

    private List<T>  entitiesToPersist  = new ArrayList<T>();

    private List<D>  documentsToPersist = new ArrayList<D>();

    private boolean  asynchToMongo;

    private UserInfo userInfo;

    private boolean  skipValidation;

    public List<T> getEntitiesToPersist() {
        return entitiesToPersist;
    }

    public void setEntitiesToPersist(List<T> entitiesToPersist) {
        this.entitiesToPersist = entitiesToPersist;
    }

    public List<D> getDocumentsToPersist() {
        return documentsToPersist;
    }

    public void setDocumentsToPersist(List<D> documentsToPersist) {
        this.documentsToPersist = documentsToPersist;
    }

    public boolean isAsynchToMongo() {
        return asynchToMongo;
    }

    public void setAsynchToMongo(boolean asynchToMongo) {
        this.asynchToMongo = asynchToMongo;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public boolean isSkipValidation() {
        return skipValidation;
    }

    public void setSkipValidation(boolean skipValidation) {
        this.skipValidation = skipValidation;
    }

}
