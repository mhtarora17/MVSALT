package com.snapdeal.cocofs.generic.persister.validation.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.entity.ProductAttribute;
import com.snapdeal.cocofs.generic.persister.validation.IEntityValidationService;
import com.snapdeal.cocofs.generic.persister.validation.dto.PAValidationData;


/**
 * @author abhinav
 *
 */

@Service("productAttributeValidationService")
public class ProductAttributeValidationServiceImpl implements IEntityValidationService<ProductAttribute, PAValidationData> {

    private static final Logger            LOG = LoggerFactory.getLogger(ProductAttributeValidationServiceImpl.class);
    
    
    @Override
    public boolean isValid(ProductAttribute pa, PAValidationData validationData) {

        if(ConfigUtils.isPresentInList(Property.PENDING_CHECK_ATTRIBUTES_LIST, pa.getAttribute())){
            
            if(validationData == null){
                
                LOG.info("Product Attribute {} is new, returning pending eligible false.", pa.getAttribute());
                return true;
            }
            
                       
            if(pa.getAttribute().equalsIgnoreCase("weight")){
                
                return checkPendingEligibilityForWeightChange(pa, validationData);
                
            }else{
                
                return checkPendingEligibilityForVolWeightChange(pa, validationData);
            }
            
            
        }else{
            
            LOG.info("Not checking pending eligibility for Product attribute {}", pa.getAttribute());
            return true;
        }
    }
    
    
    private boolean checkPendingEligibilityForWeightChange(ProductAttribute pa, PAValidationData validationDataObj){
        
     
        
        return validationDataObj.getIsFeeChangedByNewWeight() == null ? true : validationDataObj.getIsFeeChangedByNewWeight() ? false : true;
    }
    
    private boolean checkPendingEligibilityForVolWeightChange(ProductAttribute pa, PAValidationData validationDataObj){
        
     
        
        return validationDataObj.getIsFeeChangedByNewVolWeight() == null ? true : validationDataObj.getIsFeeChangedByNewVolWeight() ? false : true;
    }

    

}
