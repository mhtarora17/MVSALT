package com.snapdeal.cocofs.generic.persister.validation;

import com.snapdeal.cocofs.generic.persister.validation.dto.BaseValidationData;

/**
 * @author abhinav
 *
 */
public interface IEntityValidationService<T, S extends BaseValidationData> {

    boolean isValid(T t, S s);
}
