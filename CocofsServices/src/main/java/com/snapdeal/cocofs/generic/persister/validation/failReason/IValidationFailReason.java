/**
 * 
 */
package com.snapdeal.cocofs.generic.persister.validation.failReason;

/**
 * @author abhinav
 *
 */
public interface IValidationFailReason {

    <T> T getValidationFailReason();
}
