/**
 * 
 */
package com.snapdeal.cocofs.generic.persister.validation.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.entity.ProductAttribute;
import com.snapdeal.cocofs.generic.persister.validation.IEntityValidationService;
import com.snapdeal.cocofs.generic.persister.validation.IEntityValidationServiceFactory;
import com.snapdeal.cocofs.generic.persister.validation.dto.BaseValidationData;

/**
 * @author abhinav
 *
 */

@Service("entityValidationServiceFactory")
public class EntityValidationServiceFactoryImpl<T, S extends BaseValidationData> implements IEntityValidationServiceFactory<T, S> {

    private static final Logger            LOG = LoggerFactory.getLogger(EntityValidationServiceFactoryImpl.class);
    
    @Autowired
    @Qualifier("productAttributeValidationService")
    IEntityValidationService<T, S> productAttributeValidationService;

    @Override
    public IEntityValidationService<T, S> getService(Class<T> clazz) {
        if(clazz == null){
          
          LOG.info("Clazz is null, returning validation service null.");
          return null;
      }
      
      if(clazz.getName().equalsIgnoreCase(ProductAttribute.class.getName())){
          
          LOG.info("For clazz {}, returning productAttributeValidationService", clazz);
          return productAttributeValidationService;
      }else{
          
          LOG.info("For clazz {}, returning no validation service", clazz);
          return null;
      }
    }
    

}
