package com.snapdeal.cocofs.generic.persister.response;

import java.util.ArrayList;
import java.util.List;

/**
 * @author abhinav
 *
 */
public class GenericPersisterResponse<T, D>{

    private List<T> persistedEntity = new ArrayList<T>(0);
    
    private List<T> nonPersistedEntity = new ArrayList<T>(0);
    
    private List<D> persistedDocument = new ArrayList<D>(0);
    
    private List<D> nonPersistedDocument = new ArrayList<D>(0);
    
    private List<T> validationFailedEntityList = new ArrayList<T>(0);
    
    private List<T> validationPassedEntityList = new ArrayList<T>(0);
    
    private String message;
    
    private boolean successful;
    

    public List<T> getPersistedEntity() {
        return persistedEntity;
    }

    public void setPersistedEntity(List<T> persistedEntity) {
        this.persistedEntity = persistedEntity;
    }

    public List<T> getNonPersistedEntity() {
        return nonPersistedEntity;
    }

    public void setNonPersistedEntity(List<T> nonPersistedEntity) {
        this.nonPersistedEntity = nonPersistedEntity;
    }

    public List<D> getPersistedDocument() {
        return persistedDocument;
    }

    public void setPersistedDocument(List<D> persistedDocument) {
        this.persistedDocument = persistedDocument;
    }

    public List<D> getNonPersistedDocument() {
        return nonPersistedDocument;
    }

    public void setNonPersistedDocument(List<D> nonPersistedDocument) {
        this.nonPersistedDocument = nonPersistedDocument;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSuccessful() {
        return successful;
    }

    public void setSuccessful(boolean successful) {
        this.successful = successful;
    }

    public List<T> getValidationFailedEntityList() {
        return validationFailedEntityList;
    }

    public void setValidationFailedEntityList(List<T> validationFailedEntityList) {
        this.validationFailedEntityList = validationFailedEntityList;
    }

    public List<T> getValidationPassedEntityList() {
        return validationPassedEntityList;
    }

    public void setValidationPassedEntityList(List<T> validationPassedEntityList) {
        this.validationPassedEntityList = validationPassedEntityList;
    }
    

}
