package com.snapdeal.cocofs.generic.persister.validation;


/**
 * Factory to return GenericValidationDataBuilder implementation.
 * @author abhinav
 *
 */
public interface IGenericValidationDataBuilderFactory {

    <S> IGenericValidationDataBuilder<?, ?> getGenericValidationDataBuilder(Class<S> clazz);
}
