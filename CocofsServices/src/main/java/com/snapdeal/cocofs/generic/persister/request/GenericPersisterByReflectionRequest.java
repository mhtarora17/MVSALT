/**
 * 
 */
package com.snapdeal.cocofs.generic.persister.request;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author abhinav
 *
 */
public class GenericPersisterByReflectionRequest implements Serializable{

    private static final long           serialVersionUID = 5723022193048880743L;
    
    private String                      persistentEntityName ;
    
    private String                      persistentDocumentName;
    
    private List<Map<String, Object>>   entityFieldsToValueMapList      = new ArrayList<Map<String,Object>>(0);
    
    private List<Map<String, Object>>   documentFieldsToValueMapList    = new ArrayList<Map<String,Object>>(0);
    
    private Boolean                     asyncMongo                      = false;

    public String getPersistentEntityName() {
        return persistentEntityName;
    }

    public void setPersistentEntityName(String persistentEntityName) {
        this.persistentEntityName = persistentEntityName;
    }

    public String getPersistentDocumentName() {
        return persistentDocumentName;
    }

    public void setPersistentDocumentName(String persistentDocumentName) {
        this.persistentDocumentName = persistentDocumentName;
    }

    public List<Map<String, Object>> getEntityFieldsToValueMapList() {
        return entityFieldsToValueMapList;
    }

    public void setEntityFieldsToValueMapList(List<Map<String, Object>> entityFieldsToValueMapList) {
        this.entityFieldsToValueMapList = entityFieldsToValueMapList;
    }

    public List<Map<String, Object>> getDocumentFieldsToValueMapList() {
        return documentFieldsToValueMapList;
    }

    public void setDocumentFieldsToValueMapList(List<Map<String, Object>> documentFieldsToValueMapList) {
        this.documentFieldsToValueMapList = documentFieldsToValueMapList;
    }

    public Boolean getAsyncMongo() {
        return asyncMongo;
    }

    public void setAsyncMongo(Boolean asyncMongo) {
        this.asyncMongo = asyncMongo;
    } 
    
    

}
