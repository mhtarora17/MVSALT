package com.snapdeal.cocofs.generic.persister.validation.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.entity.ProductAttribute;
import com.snapdeal.cocofs.generic.persister.response.GenericPersisterResponse;
import com.snapdeal.cocofs.generic.persister.validation.IPersisterPostValidationHandler;
import com.snapdeal.cocofs.generic.persister.validation.IPostValidationHandlerFactory;

/**
 * @author abhinav
 *
 */

@Service("postValidationHandlerFactory")
public class PostValidationHandlerFactoryImpl<T, D, S extends GenericPersisterResponse<T, D>> implements IPostValidationHandlerFactory<T, D, S> {

    private static final Logger            LOG = LoggerFactory.getLogger(PostValidationHandlerFactoryImpl.class);
    
    @Autowired
    @Qualifier("paPersisterPostValidationHandler")
    IPersisterPostValidationHandler<T, D, S>        paPostValidationHandler;
    
    
    
    @Override
    public IPersisterPostValidationHandler<T, D, S> getService(Class<T> clazz) {

        if(clazz == null){
            
            LOG.info("Clazz is null, returning post validation handler null.");
            return null;
        }
        
        if(clazz.getName().equalsIgnoreCase(ProductAttribute.class.getName())){
            
            LOG.info("For clazz {}, returning paPostValidationHandler", clazz);
            return paPostValidationHandler;
        }else{
            
            LOG.info("For clazz {}, returning no post validation handler", clazz);
            return null;
        }
    }

}
