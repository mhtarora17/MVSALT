package com.snapdeal.cocofs.generic.persister.validation.dto;

import com.snapdeal.cocofs.sro.DeliveryType;
import com.snapdeal.cocofs.sro.PackagingType;

/**
 * @author abhinav
 *
 */
public class PAValidationData extends BaseValidationData{
    

    private Double currentUsedWeight;
    
    private Double newUsedWeight;
    
    private Double newDeadWeight;
    
    private Double newVolWeight;
    
    private Boolean isFeeChangedByNewWeight;
    
    private Boolean isFeeChangedByNewVolWeight;
    
    private DeliveryType deliveryType;
    
    private PackagingType packagingType;
    
    private boolean isNewUsedWeightOfTypeDead;


    public PAValidationData(){
        
    }
    
    

    public PAValidationData(Double currentUsedWeight, Double newDeadWeight, Double newVolWeight,
            DeliveryType deliveryType, PackagingType packagingType, Boolean isFeeChangedByNewWeight, Boolean isFeeChangedByNewVolWeight) {
        super();
        this.currentUsedWeight = currentUsedWeight;
        this.newDeadWeight = newDeadWeight;
        this.newVolWeight = newVolWeight;
        this.deliveryType = deliveryType;
        this.packagingType = packagingType;
        this.isFeeChangedByNewWeight = isFeeChangedByNewWeight;
        this.isFeeChangedByNewVolWeight = isFeeChangedByNewVolWeight;
    }



    public boolean isNewUsedWeightOfTypeDead() {
        return isNewUsedWeightOfTypeDead;
    }



    public void setNewUsedWeightOfTypeDead(boolean isNewUsedWeightOfTypeDead) {
        this.isNewUsedWeightOfTypeDead = isNewUsedWeightOfTypeDead;
    }
    
    public DeliveryType getDeliveryType() {
        return deliveryType;
    }

    public void setDeliveryType(DeliveryType deliveryType) {
        this.deliveryType = deliveryType;
    }

    public PackagingType getPackagingType() {
        return packagingType;
    }

    public void setPackagingType(PackagingType packagingType) {
        this.packagingType = packagingType;
    }



    public Double getCurrentUsedWeight() {
        return currentUsedWeight;
    }



    public void setCurrentUsedWeight(Double currentUsedWeight) {
        this.currentUsedWeight = currentUsedWeight;
    }



    public Double getNewDeadWeight() {
        return newDeadWeight;
    }



    public void setNewDeadWeight(Double newDeadWeight) {
        this.newDeadWeight = newDeadWeight;
    }



    public Double getNewVolWeight() {
        return newVolWeight;
    }



    public void setNewVolWeight(Double newVolWeight) {
        this.newVolWeight = newVolWeight;
    }



    public Boolean getIsFeeChangedByNewWeight() {
        return isFeeChangedByNewWeight;
    }



    public void setIsFeeChangedByNewWeight(Boolean isFeeChangedByNewWeight) {
        this.isFeeChangedByNewWeight = isFeeChangedByNewWeight;
    }



    public Boolean getIsFeeChangedByNewVolWeight() {
        return isFeeChangedByNewVolWeight;
    }



    public void setIsFeeChangedByNewVolWeight(Boolean isFeeChangedByNewVolWeight) {
        this.isFeeChangedByNewVolWeight = isFeeChangedByNewVolWeight;
    }



    public Double getNewUsedWeight() {
        return newUsedWeight;
    }



    public void setNewUsedWeight(Double newUsedWeight) {
        this.newUsedWeight = newUsedWeight;
    }
    
    

}
