package com.snapdeal.cocofs.generic.persister.service.impl;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.cocofs.annotations.TouchyTransaction;
import com.snapdeal.cocofs.annotations.ValidateGenericPersistence;
import com.snapdeal.cocofs.generic.persister.entity.repo.IGenericAerospikeEntityRepo;
import com.snapdeal.cocofs.generic.persister.entity.repo.IGenericEntityRepo;
import com.snapdeal.cocofs.generic.persister.exception.GenericPersisterException;
import com.snapdeal.cocofs.generic.persister.exception.GenericPersisterExceptionCode;
import com.snapdeal.cocofs.generic.persister.request.GenericPersisterByReflectionRequest;
import com.snapdeal.cocofs.generic.persister.request.GenericPersisterDBRequest;
import com.snapdeal.cocofs.generic.persister.request.GenericPersisterRequest;
import com.snapdeal.cocofs.generic.persister.request.GenericPersisterWithAerospikeRequest;
import com.snapdeal.cocofs.generic.persister.response.GenericPersisterDBResponse;
import com.snapdeal.cocofs.generic.persister.response.GenericPersisterResponse;
import com.snapdeal.cocofs.generic.persister.response.GenericPersisterWithAerospikeResponse;
import com.snapdeal.cocofs.generic.persister.service.IGenericPersisterService;
import com.snapdeal.score.data.aerospike.AerospikeSet;

/**
 * @author abhinav
 *
 */

@Service("genericPersisterService")
public class GenericPersisterServiceImpl<T, D, R extends AerospikeSet> implements IGenericPersisterService<T, D, R> {

    private static final Logger            LOG = LoggerFactory.getLogger(GenericPersisterServiceImpl.class);
    
    private static final String FIELD_DELIMETER = ".";
    
    @Autowired
    @Qualifier("genericEntityRepo")
    private IGenericEntityRepo<T, Serializable> entityRepo;
    
    @Autowired
    @Qualifier("genericMongoEntityRepo")
    private IGenericEntityRepo<T, Serializable> mongoRepo;
    
    @Autowired
    private IGenericAerospikeEntityRepo<R> aerospikeRepo;
    
    
    private <T> T persistByReflection(Map<String, Object> valueToSetMap, String persistClassName){
        
        Object toOBJ = null;
        
      try {
          Class<T> c = (Class<T>) Class.forName(persistClassName);
          toOBJ = c.newInstance();
          
          Method[] allMethods = c.getMethods();
          String setMName = "";
          
          for(Method method : allMethods) {
              if(method.getName().startsWith("set")) {
//                  method.
//                  setMName = method.getName().substring(3);
//                  method.invoke(toOBJ, valueToSetMap.get(setMName));
              }
          }
      } catch (Exception e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
      }
      
      
      
      return (T)toOBJ;
  }

    private Map<String, Map<String, Object>> getAssociatedFieldValues(Map<String, Object> valueToSetMap, String persistClassName){
        
        Map<String, Map<String, Object>> finalMap = new HashMap<String, Map<String,Object>>(0);
        finalMap.put(persistClassName, new HashMap<String, Object>(0));
        
        for(Entry<String, Object> entry : valueToSetMap.entrySet()){
            
            if(StringUtils.isNotEmpty(entry.getKey()) && entry.getKey().indexOf(FIELD_DELIMETER) > 0){
                
                List<String> s = Arrays.asList(entry.getKey().split(FIELD_DELIMETER));
                Collections.sort(s, Collections.reverseOrder());
                
                if(!finalMap.containsKey(s.get(1))){
                    
                    finalMap.put(s.get(1), new HashMap<String, Object>(0));
                }
                
                finalMap.get(s.get(1)).put(s.get(0), entry.getValue());
            }else{
                
                finalMap.get(persistClassName).put(entry.getKey(), entry.getValue());
            }
        }
        
        
        
        return finalMap;
        
    }

    @Override
    @TouchyTransaction
    @ValidateGenericPersistence
    public GenericPersisterResponse<T, D> persistOne(GenericPersisterRequest<T, D> req) throws GenericPersisterException{

        GenericPersisterResponse<T, D> resp = new GenericPersisterResponse<T, D>();
        
        if(req != null && req.getEntitiesToPersist() != null && !req.getEntitiesToPersist().isEmpty()){
            
            for(T t : req.getEntitiesToPersist()){
                
                try{
                    
                    resp.getPersistedEntity().add(entityRepo.save(t));
                }catch(Exception e){
                    
                    LOG.info("Exception occured while trying to persist entity {}", t);
                    throw new GenericPersisterException(e, e.getMessage(), GenericPersisterExceptionCode.ENTITY_FAILED_TO_PERSIST);
                }
            }
        }else{
            
            resp.setSuccessful(false);
            resp.setMessage("No Entities to persist.");
            return resp;
        }
        
        LOG.info("All entity persisted {}", req.getEntitiesToPersist());
        if(!req.isAsynchToMongo()){
            LOG.info("Going to persist in mongo {}", req.getDocumentsToPersist());
            
            if(req.getDocumentsToPersist() != null && !req.getDocumentsToPersist().isEmpty()){
                
                for(D d : req.getDocumentsToPersist()){
                    try{
                        
                        resp.getPersistedDocument().add(mongoRepo.save(d));
                    }catch(Exception e){
                        
                        LOG.info("Exception occured while trying to persist document {}", d);
                        throw new GenericPersisterException(e, e.getMessage(), GenericPersisterExceptionCode.DOCUMENT_FAILED_TO_PERSIST);
                    }
                }
            }
            
        }
        
        resp.setSuccessful(true);
        return resp;
    }
    
    @Override
    @TouchyTransaction
    public GenericPersisterWithAerospikeResponse<T, R> persistOneWithAerospike(GenericPersisterWithAerospikeRequest<T, R> req) throws GenericPersisterException{

        GenericPersisterWithAerospikeResponse<T,  R> resp = new GenericPersisterWithAerospikeResponse<T,  R>();
        
        if(req != null && req.getEntitiesToPersist() != null && !req.getEntitiesToPersist().isEmpty()){
            
            for(T t : req.getEntitiesToPersist()){
                
                try{
                    
                    resp.getPersistedEntity().add(entityRepo.save(t));
                }catch(Exception e){
                    
                    LOG.info("Exception occured while trying to persist entity {}", t);
                    throw new GenericPersisterException(e, e.getMessage(), GenericPersisterExceptionCode.ENTITY_FAILED_TO_PERSIST);
                }
            }
        }else{
            
            resp.setSuccessful(false);
            resp.setMessage("No Entities to persist.");
            return resp;
        }
        
        LOG.info("All entity persisted {}", req.getEntitiesToPersist());
     


        if(!req.isAsynchToAerospike() ){
            LOG.info("Going to persist in aerospike {}", req.getRecordsToPersist());
            
            if(req.getRecordsToPersist() != null && !req.getRecordsToPersist().isEmpty()){
                
                for(R r : req.getRecordsToPersist()){
                    try{
                        
                        resp.getPersistedRecords().add(aerospikeRepo.save(r));
                    }catch(Exception e){
                        
                        LOG.info("Exception occured while trying to persist aerospike record {}", r);
                        throw new GenericPersisterException(e, e.getMessage(), GenericPersisterExceptionCode.RECORD_FAILED_TO_PERSIST);
                    }
                }
            }
            
        }
        
        
        resp.setSuccessful(true);
        return resp;
    }


    @Override
    public List<GenericPersisterResponse<T, D>> persistAll(List<GenericPersisterRequest<T, D>> req){

        if(req != null && !req.isEmpty()){
            List<GenericPersisterResponse<T, D>> respList = new ArrayList<GenericPersisterResponse<T, D>>(0);
            
            GenericPersisterResponse<T, D> resp = null;
            for(GenericPersisterRequest<T, D> request : req){
                
                resp = new GenericPersisterResponse<T, D>();
                try {
                    
                    resp = persistOne(request);
                    
                } catch (GenericPersisterException e) {
                    
                    LOG.info("Exception occured while persisting req {}", request);
                    resp.getNonPersistedEntity().addAll(request.getEntitiesToPersist());
                    resp.getNonPersistedDocument().addAll(request.getDocumentsToPersist());
                }
                
                respList.add(resp);
            }
            
            return respList;
        }
        
        return null;
    }

    @Override
    public GenericPersisterResponse persistOneByReflection(GenericPersisterByReflectionRequest req) throws GenericPersisterException{
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<GenericPersisterResponse> persistAllByReflection(List<GenericPersisterByReflectionRequest> req) throws GenericPersisterException{
        // TODO Auto-generated method stub
        return null;
    }

	@Override
	@TouchyTransaction
	public GenericPersisterDBResponse<T> persistOneDBEntity(GenericPersisterDBRequest<T> req) throws GenericPersisterException {
		GenericPersisterDBResponse<T> resp = new GenericPersisterDBResponse<T>();
        
        if(req != null && req.getEntitiesToPersist() != null && !req.getEntitiesToPersist().isEmpty()){
            
            for(T t : req.getEntitiesToPersist()){
                
                try{
                    
                    resp.getPersistedEntity().add(entityRepo.save(t));
                }catch(Exception e){
                    
                    LOG.info("Exception occured while trying to persist entity {}", t);
                    throw new GenericPersisterException(e, e.getMessage(), GenericPersisterExceptionCode.ENTITY_FAILED_TO_PERSIST);
                }
            }
        }else{
            
            resp.setSuccessful(false);
            resp.setMessage("No Entities to persist.");
            return resp;
        }
        
        LOG.info("All entity persisted {}", req.getEntitiesToPersist());
        resp.setSuccessful(true);
        return resp;
	}
}
