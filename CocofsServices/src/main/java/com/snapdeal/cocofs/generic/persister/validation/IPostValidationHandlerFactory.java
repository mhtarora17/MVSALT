/**
 * 
 */
package com.snapdeal.cocofs.generic.persister.validation;

import com.snapdeal.cocofs.generic.persister.response.GenericPersisterResponse;

/**
 * @author abhinav
 *
 */
public interface IPostValidationHandlerFactory<T, D, S extends GenericPersisterResponse<T, D>> {

    IPersisterPostValidationHandler<T, D, S> getService(Class<T> clazz);
}
