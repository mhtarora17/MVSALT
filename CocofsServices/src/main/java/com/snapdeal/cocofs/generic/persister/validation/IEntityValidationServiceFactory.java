/**
 * 
 */
package com.snapdeal.cocofs.generic.persister.validation;

import com.snapdeal.cocofs.generic.persister.validation.dto.BaseValidationData;


/**
 * @author abhinav
 *
 */
public interface IEntityValidationServiceFactory<T, S extends BaseValidationData> {

    IEntityValidationService<T, S> getService(Class<T> clazz);
}
