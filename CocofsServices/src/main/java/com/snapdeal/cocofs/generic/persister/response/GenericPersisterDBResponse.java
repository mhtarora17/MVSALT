/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @author gaurav
 */
package com.snapdeal.cocofs.generic.persister.response;

import java.util.ArrayList;
import java.util.List;

/**
 * Generic Mysql Entity Only Persister Response
 * 
 * @author gaurav
 *
 * @param <T>
 */
public class GenericPersisterDBResponse<T> {
	private List<T> persistedEntity = new ArrayList<T>(0);
    
    private List<T> nonPersistedEntity = new ArrayList<T>(0);
    
    private List<T> validationFailedEntityList = new ArrayList<T>(0);
    
    private List<T> validationPassedEntityList = new ArrayList<T>(0);
    
    private String message;
    
    private boolean successful;
    

    public List<T> getPersistedEntity() {
        return persistedEntity;
    }

    public void setPersistedEntity(List<T> persistedEntity) {
        this.persistedEntity = persistedEntity;
    }

    public List<T> getNonPersistedEntity() {
        return nonPersistedEntity;
    }

    public void setNonPersistedEntity(List<T> nonPersistedEntity) {
        this.nonPersistedEntity = nonPersistedEntity;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSuccessful() {
        return successful;
    }

    public void setSuccessful(boolean successful) {
        this.successful = successful;
    }

    public List<T> getValidationFailedEntityList() {
        return validationFailedEntityList;
    }

    public void setValidationFailedEntityList(List<T> validationFailedEntityList) {
        this.validationFailedEntityList = validationFailedEntityList;
    }

    public List<T> getValidationPassedEntityList() {
        return validationPassedEntityList;
    }

    public void setValidationPassedEntityList(List<T> validationPassedEntityList) {
        this.validationPassedEntityList = validationPassedEntityList;
    }
}
