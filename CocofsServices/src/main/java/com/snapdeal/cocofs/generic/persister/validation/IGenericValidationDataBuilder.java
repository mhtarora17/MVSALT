package com.snapdeal.cocofs.generic.persister.validation;

import com.snapdeal.cocofs.generic.persister.request.GenericPersisterRequest;
import com.snapdeal.cocofs.generic.persister.validation.dto.BaseValidationData;
import com.snapdeal.score.core.response.GenericInternalMethodResponse;
import com.snapdeal.score.core.response.reason.IGenericResponseReason;

/**
 * 
 * This service is called before validation is performed on generic persister request.
 * It builds, on generic persister request, data required for validation.
 * @author abhinav
 *
 */
public interface IGenericValidationDataBuilder<T, D> {

    GenericInternalMethodResponse<? extends BaseValidationData, ? extends IGenericResponseReason<?>> 
        buildRequestValidationData(GenericPersisterRequest<T, D> req);
    
}
