/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.generic.persister.request;

import java.util.ArrayList;
import java.util.List;

import com.snapdeal.cocofs.services.common.dto.UserInfo;

/**
 *  
 *  @version     1.0, 15-Sep-2014
 *  @author ankur
 */
public class GenericPersisterWithAerospikeRequest <T , R> {

    private List<T>  entitiesToPersist  = new ArrayList<T>();
    
    private List<R>  recordsToPersist = new ArrayList<R>();
    
    private boolean  asynchToAerospike;

    private UserInfo userInfo;

    private boolean  skipValidation;

    public List<T> getEntitiesToPersist() {
        return entitiesToPersist;
    }

    public void setEntitiesToPersist(List<T> entitiesToPersist) {
        this.entitiesToPersist = entitiesToPersist;
    }

    public List<R> getRecordsToPersist() {
        return recordsToPersist;
    }

    public void setRecordsToPersist(List<R> recordsToPersist) {
        this.recordsToPersist = recordsToPersist;
    }

    public boolean isAsynchToAerospike() {
        return asynchToAerospike;
    }

    public void setAsynchToAerospike(boolean asynchToAerospike) {
        this.asynchToAerospike = asynchToAerospike;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public boolean isSkipValidation() {
        return skipValidation;
    }

    public void setSkipValidation(boolean skipValidation) {
        this.skipValidation = skipValidation;
    }


}
