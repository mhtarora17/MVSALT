/**
 * 
 */
package com.snapdeal.cocofs.generic.persister.validation;

import com.snapdeal.cocofs.generic.persister.response.GenericPersisterResponse;
import com.snapdeal.cocofs.services.common.dto.UserInfo;

/**
 * @author abhinav
 *
 */
public interface IPersisterPostValidationHandler<T, D, S extends GenericPersisterResponse<T, D>> {

    S handleValidationResponse(GenericPersisterResponse<T, D> resp, UserInfo userInfo);
    
}
