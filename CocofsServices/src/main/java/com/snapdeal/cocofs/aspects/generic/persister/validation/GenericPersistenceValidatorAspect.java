package com.snapdeal.cocofs.aspects.generic.persister.validation;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.generic.persister.request.GenericPersisterRequest;
import com.snapdeal.cocofs.generic.persister.response.GenericPersisterResponse;
import com.snapdeal.cocofs.generic.persister.validation.IEntityValidationService;
import com.snapdeal.cocofs.generic.persister.validation.IEntityValidationServiceFactory;
import com.snapdeal.cocofs.generic.persister.validation.IGenericValidationDataBuilder;
import com.snapdeal.cocofs.generic.persister.validation.IGenericValidationDataBuilderFactory;
import com.snapdeal.cocofs.generic.persister.validation.dto.BaseValidationData;
import com.snapdeal.score.core.response.GenericInternalMethodResponse;
import com.snapdeal.score.core.response.reason.IGenericResponseReason;

/**
 * @author abhinav
 */

@Aspect
@Service("genericPersistenceValidatorAspect")
public class GenericPersistenceValidatorAspect {

    @Autowired
    private IGenericValidationDataBuilderFactory validationDataBuildFactory;

    @Autowired
    private IEntityValidationServiceFactory      entityValidationServiceFactory;

    private static final Logger                  LOG = LoggerFactory.getLogger(GenericPersistenceValidatorAspect.class);

    @Around("@annotation(com.snapdeal.cocofs.annotations.ValidateGenericPersistence)")
    public <T, D> GenericPersisterResponse<T, D> validateGenericPersistence(ProceedingJoinPoint pjp) throws Throwable {

        GenericPersisterResponse<T, D> resp = new GenericPersisterResponse<T, D>();
        resp.setSuccessful(true);

        GenericPersisterRequest<T, D> req = (GenericPersisterRequest<T, D>) pjp.getArgs()[0];

        if (req != null && req.isSkipValidation()) {
            //we need to skip validation, if the generic persistence request has skipValidation flag to true
            LOG.info("SkipValidation is true for request {}", req);
            return (GenericPersisterResponse<T, D>)pjp.proceed();
        }

        String entityClassName = null;

        if (req != null && req.getEntitiesToPersist() != null && !req.getEntitiesToPersist().isEmpty()) {
            entityClassName = req.getEntitiesToPersist().get(0).getClass().getSimpleName();

            if (ConfigUtils.isPresentInList(Property.VALIDATION_ELIGIBILE_ENTITIES_LIST, entityClassName)) {

                LOG.info("Entity {} eligible for validation check.", entityClassName);
                
                GenericInternalMethodResponse<? extends BaseValidationData, ? extends IGenericResponseReason<?>> validationResponse = buildRequestWithValidationData(req);
                BaseValidationData validationData = validationResponse.getT();

                if (validationData == null || (validationData != null && !validationData.isValidationDataBuildSuccessful())) {
                    LOG.info("Validation has failed. Returning false. Message: " + validationResponse.getMessage());
                    resp.setValidationFailedEntityList(req.getEntitiesToPersist());
                    resp.setMessage("Validation has failed. Returning false. Message: " + validationResponse.getMessage());
                    resp.setSuccessful(false);
                    return resp;
                }

                if (!validationData.isValidationRequired()) {

                    LOG.info("ValidationData says validation is not required for request {}.", req);
                    return (GenericPersisterResponse<T, D>)pjp.proceed();
                }

                IEntityValidationService<T, BaseValidationData> validationService = entityValidationServiceFactory.getService(req.getEntitiesToPersist().get(0).getClass());

                if (validationService == null) {

                    LOG.info("EntityValidationServiceFactory returned null service for entity class {}. Returning validation false",
                            req.getEntitiesToPersist().get(0).getClass().getName());
                    resp.setValidationFailedEntityList(req.getEntitiesToPersist());
                    resp.setMessage("EntityValidationServiceFactory returned null service for entity class. Returning validation false");
                    resp.setSuccessful(false);
                    return resp;
                }

                for (T t : req.getEntitiesToPersist()) {

                    try {

                        if (!validationService.isValid(t, validationData)) {

                            LOG.info("Entity FAILED validation - {}", t);
                            resp.getValidationFailedEntityList().add(t);
                        } else {

                            LOG.info("Entity Passed validation - {}", t);
                            resp.getValidationPassedEntityList().add(t);
                        }

                    } catch (Exception e) {

                        LOG.info("Exception occured while trying to validate entity {}", t);
                        LOG.error("Exception occured while trying to validate entity", e);
                        resp.getValidationFailedEntityList().add(t);
                        resp.setMessage("Exception occured while trying to validate entity - " + t);
                    }
                }

                resp.setNonPersistedDocument(req.getDocumentsToPersist());
            } else {
                
                LOG.info("Skipping Validation as entity {} is not registered for validation", entityClassName);
                return (GenericPersisterResponse<T, D>)pjp.proceed();
            }

        } else {

            resp.setSuccessful(false);
            resp.setMessage("No Entities to persist.");
            return resp;
        }

        return resp;
    }

    @SuppressWarnings("unchecked")
    private <T, D> GenericInternalMethodResponse<? extends BaseValidationData, ? extends IGenericResponseReason<?>> buildRequestWithValidationData(GenericPersisterRequest<T, D> req) {

        IGenericValidationDataBuilder<T, D> builder = (IGenericValidationDataBuilder<T, D>) validationDataBuildFactory.getGenericValidationDataBuilder(req.getEntitiesToPersist().get(
                0).getClass());

        if (builder != null) {

            GenericInternalMethodResponse<? extends BaseValidationData, ? extends IGenericResponseReason<?>> resp = builder.buildRequestValidationData(req);

            if (!resp.isSuccessful()) {

                LOG.info("Validation data build failed for req {}", req);
            }

            return resp;

        }

        LOG.info("GenericValidationDataBuilder is null, returning BaseValidationData null");
        return null;

    }
}
