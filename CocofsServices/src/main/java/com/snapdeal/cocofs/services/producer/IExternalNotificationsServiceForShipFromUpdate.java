package com.snapdeal.cocofs.services.producer;

import java.util.List;

import com.snapdeal.cocofs.model.queue.ShipFromUpdateMessage;

public interface IExternalNotificationsServiceForShipFromUpdate extends IQueueListener {

    boolean publishToQueue(List<ShipFromUpdateMessage> msg);
}
