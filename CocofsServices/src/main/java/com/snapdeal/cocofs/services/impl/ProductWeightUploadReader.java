/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 09-Sep-2013
 *  @author himanshum
 */
package com.snapdeal.cocofs.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.snapdeal.cocofs.access.IProductAttributeService;
import com.snapdeal.cocofs.entity.ProductAttribute;
import com.snapdeal.cocofs.excelupload.ProductWeightUploadDTO;
import com.snapdeal.cocofs.mongo.model.ProductAttributeUnit;
import com.snapdeal.cocofs.services.IDataReader;

@Service("productWeightUploadReader")
public class ProductWeightUploadReader implements IDataReader<ProductWeightUploadDTO , ProductAttribute, ProductAttributeUnit> {

    @Autowired
    private IProductAttributeService productAttributeService;
    
    @Override
    public List<ProductAttributeUnit> getDocumentsForDTO(ProductWeightUploadDTO dto) {
        ProductAttributeUnit pau = productAttributeService.getProductAttributeUnitBySUPC(dto.getSupc());
        ArrayList<ProductAttributeUnit> list = new ArrayList<ProductAttributeUnit>();
        if ( null != pau ) {
            list.add(pau);
        }
        return list;
    }

    @Override
    @Transactional
    public List<ProductAttribute> getEntitiesForDTO(ProductWeightUploadDTO dto) {
        List<ProductAttribute> aList = productAttributeService.getAllProductAttributeForSUPC(dto.getSupc());
        return aList;
    }

}
