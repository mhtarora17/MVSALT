package com.snapdeal.cocofs.services.events.score.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.entity.EventInstance;
import com.snapdeal.cocofs.services.events.dto.ProductAttributeScoreEvent;
import com.snapdeal.cocofs.services.events.impl.BaseEventProducer;
import com.snapdeal.cocofs.services.events.impl.EventType;
import com.snapdeal.score.sro.UpdateProductFulfilmentAttributeSRO;

@Service("ProductAttributeUpdateScoreEventProducer")
public class ProductAttributeUpdateScoreEventProducer extends BaseEventProducer<ProductAttributeScoreEvent> {

    @Override
    protected String getEventCode() {
        return EventType.PRODUCT_ATTRIBUTE_UPDATE_SCORE_EVENT.getCode();
    }

    @Override
    public void setParams(EventInstance eventInstance, ProductAttributeScoreEvent event) {
        List<UpdateProductFulfilmentAttributeSRO> requestList = event.getUpdateProductFulfilmentAttributeRequest().getRequestList();
        if (requestList != null && !requestList.isEmpty()) {
            eventInstance.setParam1(requestList.get(0).getSupc());
        }
    }

}
