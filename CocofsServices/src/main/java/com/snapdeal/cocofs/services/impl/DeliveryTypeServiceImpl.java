/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 9, 2013
 *  @author himanshu
 */
package com.snapdeal.cocofs.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.base.validation.ValidationError;
import com.snapdeal.catalog.base.model.GetMinProductInfoBySupcRequest;
import com.snapdeal.catalog.base.model.GetMinProductInfoResponse;
import com.snapdeal.cocofs.access.IShippingModeService;
import com.snapdeal.cocofs.common.DeliveryTypeForSUPCResponse;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.external.service.factory.IExternalServiceFactory;
import com.snapdeal.cocofs.model.response.ApiErrorCode;
import com.snapdeal.cocofs.services.IDeliveryTypeService;
import com.snapdeal.cocofs.sro.CategoryShippingModeSRO;
import com.snapdeal.cocofs.sro.DeliveryType;
import com.snapdeal.cocofs.sro.SupcShippingModeSRO;
import com.snapdeal.rule.engine.evaluator.impl.RuleEvaluator;

@Service("deliveryTypeService")
public class DeliveryTypeServiceImpl extends RuleEvaluator implements IDeliveryTypeService {

    private static final Logger     LOG = LoggerFactory.getLogger(DeliveryTypeServiceImpl.class);
    @Autowired
    private IExternalServiceFactory factory;

    @Autowired
    private IShippingModeService    shippingModeService;

    @Override
    public DeliveryTypeForSUPCResponse getDeliveryTypeForSUPC(String supc) {
        return getDeliveryTypeForSUPC(supc, null);

    }

    @Override
    public DeliveryTypeForSUPCResponse getDeliveryTypeForSUPC(String supc, String categoryUrl) {
        DeliveryTypeForSUPCResponse response = new DeliveryTypeForSUPCResponse();
        response.setError(true);
        response.setSupc(supc);

        if (StringUtils.isEmpty(supc)) {
            ValidationError validationError = new ValidationError();
            validationError.setCode(ApiErrorCode.REQUIRED_FIELD_NULL.code());
            validationError.setMessage("SUPC passed is empty/null");
            response.setValidationError(validationError);
            return response;
        }

        List<String> shippingModes = getShippingModesForSUPC(supc, categoryUrl);
        if (shippingModes.isEmpty()) {
            response.setError(false);
            response.setDeliveryType(DeliveryType.EXPRESS);
        } else {
            response.setError(false);
            response.setDeliveryType(getDeliverTypeForShippingModes(shippingModes));
        }
        return response;

    }

    @Override
    public DeliveryType getDeliverTypeForShippingModes(List<String> shippingModes) {
        Map<String, String> map = ConfigUtils.getMap(Property.SHIPPING_MODE_DELIVERY_TYPE_MAP);
        LOG.debug("Shipping modes {} ", shippingModes);
        if (shippingModes.isEmpty()) {
            return DeliveryType.EXPRESS;
        } else if (shippingModes.size() == 1) {
            if (map.containsKey(shippingModes.get(0))) {
                String deliveryTypeCode = map.get(shippingModes.get(0));
                DeliveryType dt = getDeliveryTypeByCode(deliveryTypeCode);
                if (null == dt) {
                    LOG.debug("No Delivery type with  code {} ", deliveryTypeCode);
                    return DeliveryType.EXPRESS;
                }
                return dt;
            } else {
                LOG.debug("No mapping for shipping mode {} ", shippingModes.get(0));
                return DeliveryType.EXPRESS;
            }
        } else {
            String preferredMode = ConfigUtils.getStringScalar(Property.PREFERRED_SHIPPING_MODE);
            LOG.debug("Preferred Shipping Mode for in case of multiple modes ", preferredMode);
            for (String sm : shippingModes) {
                if (sm.equals(preferredMode)) {
                    DeliveryType dt = getDeliveryTypeByCode(sm);
                    if (null == dt) {
                        LOG.debug("No Delivery type with  code {} ", sm);
                        return DeliveryType.EXPRESS;
                    }
                    return dt;
                }
            }
            LOG.debug("Multiple shipping modes given but none of them preferred " + preferredMode + " Return Express delivery");
            return DeliveryType.EXPRESS;
        }

    }

    private DeliveryType getDeliveryTypeByCode(String code) {
        for (DeliveryType dt : DeliveryType.values()) {
            if (dt.getCode().equalsIgnoreCase(code)) {
                return dt;
            }
        }
        return null;
    }

    private List<String> getShippingModesForSUPC(String supc, String categoryUrl) {

        List<String> supcs = new ArrayList<String>();
        supcs.add(supc);
        Map<String, SupcShippingModeSRO> m = shippingModeService.getSupcModeMapping(supcs);
        if (m.containsKey(supc) && (!m.get(supc).getShippingMode().isEmpty())) {
            return m.get(supc).getShippingMode();
        }
        LOG.debug("No SUPC level mode mapping found for supc {} ", supc);
        if (StringUtils.isEmpty(categoryUrl)) {
            GetMinProductInfoBySupcRequest request = new GetMinProductInfoBySupcRequest(supc);
            GetMinProductInfoResponse response;
            try {
                response = factory.getCAMSExternalService().getMinProductInfo(request);
            } catch (ExternalDataNotFoundException e) {
                LOG.info("Could not obtain category for supc " + supc, e);
                return new ArrayList<String>();
            }
            if ((null == response) || !response.isSuccessful()) {
                LOG.info("Could not obtain category for supc response null or unsuccessfull " + supc);
                return new ArrayList<String>();
            }
            categoryUrl = response.getProductInfo().getCategoryUrl();
        }
        List<String> categories = new ArrayList<String>();
        categories.add(categoryUrl);
        Map<String, CategoryShippingModeSRO> catMap = shippingModeService.getCategoryModeMapping(categories);

        if (catMap.containsKey(categoryUrl) && (!catMap.get(categoryUrl).getShippingMode().isEmpty())) {
            return catMap.get(categoryUrl).getShippingMode();
        }
        LOG.debug("No SUBCAT level mode mapping found for supc {} subcat {}", supc, categoryUrl);
        return new ArrayList<String>();
    }

}
