/*
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 07-July-2014
 *  @author Ankur Choudhary
 *  @Desc	Will be used in Event Controller and displaying Event Instance entites 
 */
package com.snapdeal.cocofs.services.events.dto;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class EventDetailDTO {

    private Integer id;
    private String  eventType;
    private String  param1;
    private String  param2;
    private String  jsonArguments;
    private boolean executed;
    private Integer retryCount;
    private boolean enabled;
    private Date    created;
    private Date    updated;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getParam1() {
        return param1;
    }

    public void setParam1(String Param1) {
        this.param1 = Param1;
    }

    public String getParam2() {
        return param2;
    }

    public void setParam2(String param2) {
        this.param2 = param2;
    }

    public String getJsonArguments() {
        return jsonArguments;
    }

    public void setJsonArguments(String jsonArguments) {
        this.jsonArguments = jsonArguments;
    }

    public boolean isExecuted() {
        return executed;
    }

    public void setExecuted(boolean executed) {
        this.executed = executed;
    }

    public Integer getRetryCount() {
        return retryCount;
    }

    public void setRetryCount(Integer retryCount) {
        this.retryCount = retryCount;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("EventDetailDTO [id=").append(id).append(", eventType=").append(eventType).append(", param1=").append(param1).append(", param2=").append(param2).append(
                ", jsonArguments=").append(jsonArguments).append(", executed=").append(executed).append(", retryCount=").append(retryCount).append(", enabled=").append(enabled).append(
                ", created=").append(created).append(", updated=").append(updated).append("]");
        return builder.toString();
    }

}
