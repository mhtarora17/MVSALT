/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 07-Oct-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.services.fm.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.naming.OperationNotSupportedException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.base.utils.CollectionUtils;
import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.cocofs.core.aerospike.vo.SellerFMMappingVO;
import com.snapdeal.cocofs.dataaccess.fm.IFMDBDataReadService;
import com.snapdeal.cocofs.entity.SellerFCCodeMapping;
import com.snapdeal.cocofs.entity.SellerFMMapping;
import com.snapdeal.cocofs.entity.SellerSubcatFMMapping;
import com.snapdeal.cocofs.entity.SellerSupcFMMapping;
import com.snapdeal.cocofs.services.fm.dto.SellerFMMappingUpdateDto;

@Service("SellerFMMappingDataEngine")
public class SellerFMMappingDataEngine extends AbstractFMMappingDataEngine<SellerFMMappingUpdateDto, SellerFMMapping, SellerFMMappingVO> {

    @Autowired
    private IFMDBDataReadService                                     fmDBDataReadService;

    @Autowired
    private com.snapdeal.cocofs.services.converter.IConverterService convertorService;

    @Override
    public List<SellerFMMapping> enrichEntities(SellerFMMappingUpdateDto dto, List<SellerFMMapping> entities, String userEmail) throws OperationNotSupportedException {
        if (null != dto) {
            boolean isCreateRequest = false;
            SellerFMMapping mapping = null;
            List<SellerFMMapping> mappingList = new ArrayList<SellerFMMapping>();

            if (null != entities && !entities.isEmpty()) {
                mapping = entities.get(0);
            } else {
                mapping = new SellerFMMapping();
                isCreateRequest = true;
            }

            enrichFMMappingEntity(dto, mapping, userEmail);
            enrichFCMappingDetails(dto, mapping, userEmail);

            if (isCreateRequest) {
                setIfSupcAndSubCatMappingExists(mapping, dto);
            }

            mappingList.add(mapping);
            return mappingList;
        }
        return null;
    }

    private void setIfSupcAndSubCatMappingExists(SellerFMMapping mapping, SellerFMMappingUpdateDto dto) {
        List<SellerSubcatFMMapping> subcatMappingList = fmDBDataReadService.getSellerSubcatFMMapping(dto.getSellerCode());
        if (subcatMappingList != null && subcatMappingList.size() > 0) {
            mapping.setSubcatExist(true);
        }

        List<SellerSupcFMMapping> supcMappingList = fmDBDataReadService.getAllEnabledSellerSupcFMMappingBySeller(dto.getSellerCode());
        if (supcMappingList != null && supcMappingList.size() > 0) {
            mapping.setSupcExist(true);
        }
    }

    @Override
    public List<SellerFMMappingVO> enrichRecords(SellerFMMappingUpdateDto dto, List<SellerFMMappingVO> records, String userEmail) throws OperationNotSupportedException {
        if (dto != null) {
            boolean isCreateRequest = false;
            SellerFMMappingVO record = null;
            List<SellerFMMappingVO> recordList = new ArrayList<SellerFMMappingVO>();
            if (null != records && !records.isEmpty()) {
                record = records.get(0);
            } else {
                record = new SellerFMMappingVO();
                isCreateRequest = true;
            }
            //enrichFMMappingRecord
            updateSellerFMMappingVO(dto, record, isCreateRequest);

            recordList.add(record);
            return recordList;

        }
        return null;
    }

    private void updateSellerFMMappingVO(SellerFMMappingUpdateDto dto, SellerFMMappingVO record, boolean isCreateRequest) {
        if (isCreateRequest) {
            List<SellerSubcatFMMapping> subcatMappingList = fmDBDataReadService.getSellerSubcatFMMapping(dto.getSellerCode());
            if (subcatMappingList != null && subcatMappingList.size() > 0) {
                record.setSubcatExist(1);
            }

            List<SellerSupcFMMapping> supcMappingList = fmDBDataReadService.getAllEnabledSellerSupcFMMappingBySeller(dto.getSellerCode());
            if (supcMappingList != null && supcMappingList.size() > 0) {
                record.setSupcExist(1);
            }
        }

        record.setCreatedStr(record.getCreatedStr() != null ? record.getCreatedStr() : dto.getLastUpdated().getTime() + "");
        record.setUpdatedStr(dto.getLastUpdated().getTime() + "");
        record.setFulfillmentModel(dto.getFulfilmentModel());
        record.setSellerCode(dto.getSellerCode());
        record.setEnabled(1);

        //update FC centers in record
        List<String> newFCCenters = convertorService.convertToFCCentreList(dto.getFcCenters());
        record.setFcCenters(newFCCenters);

    }

    private void enrichFCMappingDetails(SellerFMMappingUpdateDto dto, SellerFMMapping mapping, String userEmail) {

        Set<SellerFCCodeMapping> existingFCCenters = mapping.getFcCenters();
        List<String> newFCCenters = convertorService.convertToFCCentreList(dto.getFcCenters());

        //case 1) no new data - disable all existing if present
        if (CollectionUtils.isEmpty(newFCCenters)) {
            if (CollectionUtils.isEmpty(existingFCCenters)) {
                // no new data, no existing data - nothing to be done
            } else {
                // disable all existing
                for (SellerFCCodeMapping o : existingFCCenters) {
                    if (o != null) {
                        o.setEnabled(false);
                        o.setLastUpdated(DateUtils.getCurrentTime());
                        o.setUpdatedBy(userEmail);
                    }
                }
            }
        }
        //case 2) no existing data - add all new data
        else if (CollectionUtils.isEmpty(existingFCCenters)) {
            existingFCCenters = new HashSet<SellerFCCodeMapping>();
            mapping.setFcCenters(existingFCCenters);
            for (String o : newFCCenters) {
                SellerFCCodeMapping item = new SellerFCCodeMapping(mapping, o, userEmail);
                existingFCCenters.add(item);
            }
        }
        //case 3) change in FC data - disable existing, add/update new
        else {
            // set enabled status based on new fc center data
            for (SellerFCCodeMapping o : existingFCCenters) {
                if (o.isEnabled() == false && !newFCCenters.contains(o.getFcCode())) {
                    // do not do anything.
                } else {
                    o.setEnabled(newFCCenters.contains(o.getFcCode()));
                    o.setLastUpdated(DateUtils.getCurrentTime());
                    o.setUpdatedBy(userEmail);
                }
            }
            // add remaining fc centers 
            for (String o : newFCCenters) {
                if (!containsFCCode(existingFCCenters, o)) {
                    SellerFCCodeMapping item = new SellerFCCodeMapping(mapping, o, userEmail);
                    existingFCCenters.add(item);
                }
            }
        }
    }

    private boolean containsFCCode(Set<SellerFCCodeMapping> existingFCCenters, String item) {
        boolean bResult = false;
        for (SellerFCCodeMapping o : existingFCCenters) {
            if (o != null && item.equals(o.getFcCode())) {
                bResult = true;
                break;
            }
        }
        return bResult;
    }

}
