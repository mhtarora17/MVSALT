/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 11-Sep-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.services.task.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.snapdeal.cocofs.annotations.TouchyTransaction;
import com.snapdeal.cocofs.entity.TaskDetail;
import com.snapdeal.cocofs.enums.TaskStatus;
import com.snapdeal.cocofs.services.task.update.ITaskUpdateConverterService;
import com.snapdeal.cocofs.services.task.update.ITaskUpdateService;
import com.snapdeal.task.core.AbstractTask;
import com.snapdeal.task.core.TaskRequest;
import com.snapdeal.task.exception.TaskException;
import com.snapdeal.task.service.ITaskService;

public abstract class QuartzTask extends AbstractTask {

    private static final Logger LOG = LoggerFactory.getLogger(QuartzTask.class);

    @Autowired
    ITaskUpdateService          taskUpdateService;

    @Autowired
    ITaskService                taskService;

    @Autowired
    ITaskUpdateConverterService converterService;

    private Thread              executionThread;

    @Override
    public final void execute() throws TaskException {
        executionThread = Thread.currentThread();
        String taskName = getTaskName();
        TaskDetail taskDetail = taskUpdateService.getTaskDetailByName(taskName);
        TaskRequest taskRequest = converterService.getTaskRequest(taskDetail);
        taskUpdateService.updateTaskStatusInfo(taskName, taskService.getNextScheduledDate(taskRequest), taskService.getPreviousFireTime(taskRequest), TaskStatus.RUNNING.getCode());
        try {
            execute(taskName);
        } catch (Exception e) {
            LOG.error("Exception while trying to execute task with name {} ", e, taskName);
        }
        taskUpdateService.updateTaskStatusInfo(taskName, taskService.getNextScheduledDate(taskRequest), taskService.getPreviousFireTime(taskRequest), TaskStatus.WAITING.getCode());
    }

    public abstract void execute(String taskName);

    @Override
    @TouchyTransaction
    public void interruptTask() throws TaskException {
        taskUpdateService.updateTaskStatusInfo(getTaskName(), TaskStatus.WAITING.getCode());
        this.executionThread.interrupt();
    }
}
