/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.services;

import java.util.List;

import com.snapdeal.cocofs.services.common.dto.SupcPackagingTypeMappingDownloadData;

/**
 *  
 *  @version     1.0, 23-Dec-2015
 *  @author vikassharma03
 */
public interface ISupcPackagingTypeMappingLookupService {
    
    public List<SupcPackagingTypeMappingDownloadData> getSupcPackagingTypeDownloadData(String supc);

}
