package com.snapdeal.cocofs.services.taxclass.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.catalog.base.model.GetMinProductInfoBySupcRequest;
import com.snapdeal.catalog.base.model.GetMinProductInfoResponse;
import com.snapdeal.cocofs.core.aerospike.vo.SellerFMMappingVO;
import com.snapdeal.cocofs.core.aerospike.vo.SellerSubcatFMMappingVO;
import com.snapdeal.cocofs.core.aerospike.vo.SellerSupcFMMappingVO;
import com.snapdeal.cocofs.core.aerospike.vo.SubcatTaxClassMappingVO;
import com.snapdeal.cocofs.core.aerospike.vo.SupcTaxClassMappingVO;
import com.snapdeal.cocofs.dataaccess.fm.IFMAerospikeDataReadService;
import com.snapdeal.cocofs.dataaccess.taxclass.ITaxClassAerospikeDataReadService;
import com.snapdeal.cocofs.entity.SubcatTaxClassMapping;
import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.external.service.factory.IExternalServiceFactory;
import com.snapdeal.cocofs.services.fm.IFMDataSourceService;
import com.snapdeal.cocofs.services.fm.dto.SellerFmFcDTO;
import com.snapdeal.cocofs.services.fm.dto.SellerSupcFmFcDTO;
import com.snapdeal.cocofs.services.taxclass.ITaxClassDataSourceService;
import com.snapdeal.cocofs.services.taxclass.dto.SubcatTaxClassMappingDTO;
import com.snapdeal.cocofs.services.taxclass.dto.SupcTaxClassMappingDTO;
import com.snapdeal.cocofs.utils.AerospikeKeyHelper;

@Service("TaxClassDataSourceServiceForAerospike")
public class TaxClassDataSourceServiceForAerospike implements ITaxClassDataSourceService {

    @Autowired
    private ITaxClassAerospikeDataReadService taxClassAerospikeService;

    @Autowired
    private IExternalServiceFactory           externalServiceFactory;

    private static final Logger               LOG = LoggerFactory.getLogger(TaxClassDataSourceServiceForAerospike.class);

    @Override
    public SupcTaxClassMappingDTO getTaxClassBySupc(String supc) throws ExternalDataNotFoundException {
        SupcTaxClassMappingDTO taxClassMappingDTO = null;
        SupcTaxClassMappingVO supcTaxClassMappingVO = getSupcTaxClass(supc);
        if (supcTaxClassMappingVO != null) {
            taxClassMappingDTO = new SupcTaxClassMappingDTO(supcTaxClassMappingVO);
        }
        LOG.info("Returning taxClass {} for supc {} from Aerospike ", taxClassMappingDTO, supc);
        return taxClassMappingDTO;
    }

    @Override
    public SubcatTaxClassMappingDTO getTaxClassBySubcat(String subcat) throws ExternalDataNotFoundException {
        SubcatTaxClassMappingDTO taxClass = null;
        SubcatTaxClassMappingVO subcatTaxClassMappingVO = getSubcatTaxClass(subcat);
        if (subcatTaxClassMappingVO != null) {
            taxClass = new SubcatTaxClassMappingDTO(subcatTaxClassMappingVO);
        }
        LOG.info("Returning taxClass {} for subcat {} from Aerospike", taxClass, subcat);
        return taxClass;
    }

    /////////////////////////////////
    //////// Private Methods ////////
    /////////////////////////////////

    private SupcTaxClassMappingVO getSupcTaxClass(String supc) throws ExternalDataNotFoundException {
        if (StringUtils.isEmpty(supc)) {
            LOG.warn("Illegal paramters passed while trying to find tax class at supc level");
            throw new ExternalDataNotFoundException("Required Parameters not there while getting Tax class At supc Level");
        }

        LOG.debug("Going to read tax class from Aerospike for supc {}", supc);
        SupcTaxClassMappingVO supcTaxClassMappingVO = (SupcTaxClassMappingVO) taxClassAerospikeService.getSupcTaxClassMapping(AerospikeKeyHelper.getKey(supc),
                SupcTaxClassMappingVO.class);
        LOG.debug("Tax Class obtained from Aerospike: " + supcTaxClassMappingVO);
        return supcTaxClassMappingVO;
    }

    private SubcatTaxClassMappingVO getSubcatTaxClass(String subcat) throws ExternalDataNotFoundException {
        if (StringUtils.isEmpty(subcat)) {
            LOG.warn("Illegal paramters passed while trying to find tax class at subcat level");
            throw new ExternalDataNotFoundException("Required Parameters not there while getting Tax class At subcat Level");
        }

        LOG.debug("Going to read tax class from Aerospike for subcat {}", subcat);
        SubcatTaxClassMappingVO subcatTaxClassMappingVO = (SubcatTaxClassMappingVO) taxClassAerospikeService.getSubcatTaxClassMapping(AerospikeKeyHelper.getKey(subcat),
                SubcatTaxClassMappingVO.class);
        LOG.debug("Tax Class obtained from Aerospike: " + subcatTaxClassMappingVO);
        return subcatTaxClassMappingVO;
    }

}
