/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.services.taxrate.impl;

import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.cocofs.entity.AbstractTaxRate;
import com.snapdeal.cocofs.mongo.model.AbstractTaxRateUnit;
import com.snapdeal.cocofs.services.data.engine.IDataEngine;
import com.snapdeal.cocofs.services.taxrate.dto.TaxRateUpdateDTO;

/**
 * @version 1.0, 18-Aug-2014
 * @author ankur
 */
public abstract class AbstractTaxRateDataEngine<T, E, D> implements IDataEngine<T, E, D> {

    public void enrichTaxRateUnit(TaxRateUpdateDTO dto, AbstractTaxRateUnit document) {
        document.setCreated(document.getCreated() != null ? document.getCreated() : DateUtils.getCurrentTime());
        document.setUpdated(DateUtils.getCurrentTime());
        document.setTaxRate(dto.getTaxRate());
        document.setTaxType(dto.getTaxType());
        document.setEnabled(dto.getEnabled()); 

    }

    public void enrichTaxRateEntity(TaxRateUpdateDTO dto, AbstractTaxRate entity, String userEmail) {
        entity.setCreated(entity.getCreated() != null ? entity.getCreated() : DateUtils.getCurrentTime());
        entity.setTaxRate(dto.getTaxRate());
        entity.setTaxType(dto.getTaxType());
        entity.setLastUpdated(DateUtils.getCurrentTime());
        entity.setUpdatedBy(userEmail);
        entity.setEnabled(dto.getEnabled());

    }

}
