/**
 *  Copyright 2016 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.services.supcptmapping.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.entity.SupcPackagingTypeMapping;
import com.snapdeal.cocofs.excelupload.SupcPackagingTypeUploadDTO;
import com.snapdeal.cocofs.mongo.model.SupcPackagingTypeMappingUnit;
import com.snapdeal.cocofs.services.IDataReader;
import com.snapdeal.cocofs.services.ISupcToPackagingTypeMappingService;
import com.snapdeal.packman.dataaccess.supcpt.ISupcPackagingTypeMongoService;

/**
 *  
 *  @version     1.0, 04-Jan-2016
 *  @author vikassharma03
 */
@Service("SupcPackagingTypeMappingDataReader")
public class SupcPackagingTypeMappingDataReader implements IDataReader<SupcPackagingTypeUploadDTO, SupcPackagingTypeMapping, SupcPackagingTypeMappingUnit> {

    @Autowired
    private ISupcPackagingTypeMongoService     supcPackagingTypeMongoService;
    
    @Autowired
    private ISupcToPackagingTypeMappingService  supcToPackagingTypeMappingService;

    
    @Override
    public List<SupcPackagingTypeMappingUnit> getDocumentsForDTO(SupcPackagingTypeUploadDTO dto) {
        SupcPackagingTypeMappingUnit document = supcPackagingTypeMongoService.getSupcPackagingTypeMappingUnit(dto.getSupc(), dto.getStoreCode());
        List<SupcPackagingTypeMappingUnit> documentList = new ArrayList<SupcPackagingTypeMappingUnit>();
        if (null != document) {
            documentList.add(document);
        }
        return documentList;    
    }

    @Override
    public List<SupcPackagingTypeMapping> getEntitiesForDTO(SupcPackagingTypeUploadDTO dto) {
        // TODO Auto-generated method stub
        SupcPackagingTypeMapping entity = supcToPackagingTypeMappingService.getSupcPackagingTypeMappingBySupcAndStoreCode(dto.getSupc(), dto.getStoreCode());
        List<SupcPackagingTypeMapping> entityList = new ArrayList<SupcPackagingTypeMapping>();
        if (null != entity) {
            entityList.add(entity);
        }
        return entityList;
    }
      

}
