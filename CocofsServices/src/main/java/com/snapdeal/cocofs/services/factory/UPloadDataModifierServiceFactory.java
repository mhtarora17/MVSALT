package com.snapdeal.cocofs.services.factory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.snapdeal.cocofs.excelupload.ProductAttributeUploadDTO;
import com.snapdeal.cocofs.services.IUploadDataModifier;
import com.snapdeal.cocofs.services.impl.PAUDataModifier;

/**
 * @author nikhil
 */
@Component
public class UPloadDataModifierServiceFactory {

    @Autowired
    @Qualifier("pauDataModifier")
    private IUploadDataModifier<PAUDataModifier> pauDataModifier;

    @Autowired
    @Qualifier("defaultDataModifier")
    private IUploadDataModifier<Object>          defaultDataModifier;

    public IUploadDataModifier getDataModifier(Object obj) {

        if (obj instanceof ProductAttributeUploadDTO) {
            return pauDataModifier;
        } else {
            return defaultDataModifier;
        }

    }
}
