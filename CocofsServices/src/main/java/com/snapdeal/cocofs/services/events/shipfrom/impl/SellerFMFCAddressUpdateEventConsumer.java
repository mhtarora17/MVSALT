/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.services.events.shipfrom.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.services.events.dto.SellerFMFCUpdateEventObj;
import com.snapdeal.cocofs.services.events.impl.BaseEventConsumer;
import com.snapdeal.cocofs.services.events.impl.EventType;
import com.snapdeal.cocofs.services.fm.ISellerSupcShipFromMappingProcessor;

/**
 * @version 1.0, 25-Mar-2015
 * @author shiv
 */
@Service("SellerFMFCAddressUpdateEventConsumer")
public class SellerFMFCAddressUpdateEventConsumer extends BaseEventConsumer<SellerFMFCUpdateEventObj> {

    @Autowired
    @Qualifier("SellerSUPCShipFromMappingProcessorImpl")
    private ISellerSupcShipFromMappingProcessor sellerSupcShipFromMappingProcessor;

    private static final Logger             LOG = LoggerFactory.getLogger(SellerFMFCAddressUpdateEventConsumer.class);

    @Override
    protected String getEventCode() {
        return EventType.SHIPFROM_UPDATE_EVENT_ON_SELLER_UPDATE.getCode();
    }

    @Override
    protected boolean processEventObj(SellerFMFCUpdateEventObj eventObject) {

        try {
            return sellerSupcShipFromMappingProcessor.processChangeOfSellerFMMappingForExternalNotification(eventObject);
        } catch (Exception e) {
            LOG.error("Exception while processing the request for event: {}. Exception {}", eventObject, e);
        }

        return false;
    }
}
