/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.services.taxrate.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.dataaccess.taxrate.ITaxRateDBDataReadService;
import com.snapdeal.cocofs.dataaccess.taxrate.ITaxRateMongoService;
import com.snapdeal.cocofs.entity.SellerSupcTaxRate;
import com.snapdeal.cocofs.mongo.model.SellerSupcTaxRateUnit;
import com.snapdeal.cocofs.services.IDataReader;
import com.snapdeal.cocofs.services.taxrate.dto.SellerSUPCTaxRateUploadDTO;

/**
 *  
 *  @version     1.0, 18-Aug-2014
 *  @author ankur
 */

@Service("SellerSupcTaxRateDataReader")
public class SellerSupcTaxRateDataReader implements IDataReader<SellerSUPCTaxRateUploadDTO, SellerSupcTaxRate, SellerSupcTaxRateUnit> {
   
    @Autowired
    private ITaxRateMongoService     taxRateMongoService;

    @Autowired

    private ITaxRateDBDataReadService taxRateDBDataReadService;
    @Override
    public List<SellerSupcTaxRateUnit> getDocumentsForDTO(SellerSUPCTaxRateUploadDTO dto) {
        SellerSupcTaxRateUnit document = taxRateMongoService.getSellerSupcTaxRateUnit(dto.getSellerCode(), dto.getSupc());
        List<SellerSupcTaxRateUnit> documentList = new ArrayList<SellerSupcTaxRateUnit>();
        if (null != document) {
            documentList.add(document);
        }
        return documentList;
    }

    @Override
    public List<SellerSupcTaxRate> getEntitiesForDTO(SellerSUPCTaxRateUploadDTO dto) {
        SellerSupcTaxRate entity = taxRateDBDataReadService.getSellerSupcTaxRate(dto.getSellerCode(), dto.getSupc());
        List<SellerSupcTaxRate> entityList = new ArrayList<SellerSupcTaxRate>();
        if (null != entity) {
            entityList.add(entity);
        }
        return entityList;
    }
}
