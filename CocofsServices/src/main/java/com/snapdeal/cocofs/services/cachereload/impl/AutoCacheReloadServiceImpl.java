/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 07-May-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.services.cachereload.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.configurations.IStartupService;
import com.snapdeal.cocofs.services.cachereload.IAutoCacheReloadService;

@Service("autoReloadService")
public class AutoCacheReloadServiceImpl implements IAutoCacheReloadService {

    @Autowired
    private IStartupService     startupService;

    private static final Logger LOG = LoggerFactory.getLogger(AutoCacheReloadServiceImpl.class);

    /**
     * this method will be called every configured time when the server is up here it will reload cocofs cache
     */

    @Override
    public void reloadCache() {
        try {
            LOG.info("Starting Cocofs Cache Reload");
            startupService.loadAll(false);
            LOG.info("Cache loaded Successfully");
        } catch (Exception e) {
            e.printStackTrace();
            LOG.error(e.toString());
        }
    }

}
