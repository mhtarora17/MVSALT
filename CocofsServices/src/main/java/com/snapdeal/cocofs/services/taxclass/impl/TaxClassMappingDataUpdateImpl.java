/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-Oct-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.services.taxclass.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.cocofs.annotations.Throttled;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.core.aerospike.vo.SellerFMMappingVO;
import com.snapdeal.cocofs.core.aerospike.vo.SellerSubcatFMMappingVO;
import com.snapdeal.cocofs.core.aerospike.vo.SellerSupcFMMappingVO;
import com.snapdeal.cocofs.core.aerospike.vo.SubcatTaxClassMappingVO;
import com.snapdeal.cocofs.core.aerospike.vo.SupcTaxClassMappingVO;
import com.snapdeal.cocofs.dataaccess.fm.IFMAerospikeDataReadService;
import com.snapdeal.cocofs.dataaccess.fm.IFMDBDataReadService;
import com.snapdeal.cocofs.dataaccess.taxclass.ITaxClassAerospikeDataReadService;
import com.snapdeal.cocofs.dataaccess.taxclass.ITaxClassDBDataReadService;
import com.snapdeal.cocofs.entity.SellerFMMapping;
import com.snapdeal.cocofs.entity.SellerSubcatFMMapping;
import com.snapdeal.cocofs.entity.SellerSupcFMMapping;
import com.snapdeal.cocofs.entity.SellerSupcMapping;
import com.snapdeal.cocofs.entity.SubcatTaxClassMapping;
import com.snapdeal.cocofs.entity.SupcTaxClassMapping;
import com.snapdeal.cocofs.generic.persister.exception.GenericPersisterException;
import com.snapdeal.cocofs.generic.persister.request.GenericPersisterDBRequest;
import com.snapdeal.cocofs.generic.persister.request.GenericPersisterWithAerospikeRequest;
import com.snapdeal.cocofs.generic.persister.response.GenericPersisterDBResponse;
import com.snapdeal.cocofs.generic.persister.response.GenericPersisterResponse;
import com.snapdeal.cocofs.generic.persister.response.GenericPersisterWithAerospikeResponse;
import com.snapdeal.cocofs.generic.persister.service.IGenericPersisterService;
import com.snapdeal.cocofs.generic.persister.validation.IPersisterPostValidationHandler;
import com.snapdeal.cocofs.services.fm.IFMMappingDataUpdater;
import com.snapdeal.cocofs.services.taxclass.ITaxClassMappingDataUpdater;

@Service("TaxClassMappingDataUpdateImpl")
public class TaxClassMappingDataUpdateImpl implements ITaxClassMappingDataUpdater {

    private static final Logger               LOG = LoggerFactory.getLogger(TaxClassMappingDataUpdateImpl.class);

    @Autowired
    private IGenericPersisterService          persistenceService;

    @Autowired
    private ITaxClassDBDataReadService        taxClassDBDataReadService;

    @Autowired
    private ITaxClassAerospikeDataReadService taxClassAerospikeReadService;

    @Override
    public GenericPersisterWithAerospikeResponse<SupcTaxClassMapping, SupcTaxClassMappingVO> updateSupcTaxClassMapping(List<SupcTaxClassMapping> entityListToSave,
            List<SupcTaxClassMappingVO> recordListToSave) throws GenericPersisterException {
        GenericPersisterWithAerospikeRequest<SupcTaxClassMapping, SupcTaxClassMappingVO> req = new GenericPersisterWithAerospikeRequest<SupcTaxClassMapping, SupcTaxClassMappingVO>();
        req.setEntitiesToPersist(entityListToSave);
        req.setRecordsToPersist(recordListToSave);
        req.setAsynchToAerospike(false);
        req.setSkipValidation(false);
        GenericPersisterWithAerospikeResponse<SupcTaxClassMapping, SupcTaxClassMappingVO> resp = persistenceService.persistOneWithAerospike(req);
        return resp;
    }

    @Override
    public GenericPersisterWithAerospikeResponse<SubcatTaxClassMapping, SubcatTaxClassMappingVO> updateSubcatTaxClassMapping(List<SubcatTaxClassMapping> entityListToSave,
            List<SubcatTaxClassMappingVO> recordListToSave) throws GenericPersisterException {
        GenericPersisterWithAerospikeRequest<SubcatTaxClassMapping, SubcatTaxClassMappingVO> req = new GenericPersisterWithAerospikeRequest<SubcatTaxClassMapping, SubcatTaxClassMappingVO>();
        req.setEntitiesToPersist(entityListToSave);
        req.setRecordsToPersist(recordListToSave);
        req.setAsynchToAerospike(false);
        req.setSkipValidation(false);
        GenericPersisterWithAerospikeResponse<SubcatTaxClassMapping, SubcatTaxClassMappingVO> resp = persistenceService.persistOneWithAerospike(req);
        return resp;
    }

}
