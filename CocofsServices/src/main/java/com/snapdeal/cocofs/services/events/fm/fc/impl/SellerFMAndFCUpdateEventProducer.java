/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.services.events.fm.fc.impl;

import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.entity.EventInstance;
import com.snapdeal.cocofs.services.events.dto.SellerFMFCUpdateEventObj;
import com.snapdeal.cocofs.services.events.impl.BaseEventProducer;
import com.snapdeal.cocofs.services.events.impl.EventType;

/**
 * @version 1.0, 25-Mar-2015
 * @author shiv
 */

@Service("SellerFMAndFCUpdateEventProducer")
public class SellerFMAndFCUpdateEventProducer extends BaseEventProducer<SellerFMFCUpdateEventObj> {

    @Override
    protected String getEventCode() {
        return EventType.FC_UPDATE_EVENT_ON_SELLER_FM_UPDATE.getCode();
    }

    @Override
    public void setParams(EventInstance eventInstance, SellerFMFCUpdateEventObj eventObj) {
        eventInstance.setParam1(eventObj.getSellerCode());
        eventInstance.setParam2(eventObj.getNewFM());
        eventInstance.setCreated(eventObj.getCreated());
    }

}
