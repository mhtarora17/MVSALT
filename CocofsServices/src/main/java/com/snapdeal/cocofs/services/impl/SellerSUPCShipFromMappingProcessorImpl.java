package com.snapdeal.cocofs.services.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.core.aerospike.vo.FCDetailVO;
import com.snapdeal.cocofs.core.aerospike.vo.SellerFMMappingVO;
import com.snapdeal.cocofs.dataaccess.fc.IFCAerospikeDataReadService;
import com.snapdeal.cocofs.dataaccess.fm.IFMDBDataReadService;
import com.snapdeal.cocofs.entity.SellerSupcFMMapping;
import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.generic.persister.exception.GenericPersisterException;
import com.snapdeal.cocofs.model.queue.ShipFromUpdateMessage;
import com.snapdeal.cocofs.services.ISellerDetailsService;
import com.snapdeal.cocofs.services.events.dto.FCAddressUpdateEventObj;
import com.snapdeal.cocofs.services.events.dto.SellerFMFCUpdateEventObj;
import com.snapdeal.cocofs.services.events.dto.SellerSupcAddEventObj;
import com.snapdeal.cocofs.services.events.dto.SellerSupcFMFCExceptionAddEventObj;
import com.snapdeal.cocofs.services.events.dto.SellerSupcFMFCExceptionDeleteEventObj;
import com.snapdeal.cocofs.services.fm.IFulfilmentModelService;
import com.snapdeal.cocofs.services.fm.ISellerSupcShipFromMappingProcessor;
import com.snapdeal.cocofs.services.fm.ISellerToSUPCListDBLookupService;
import com.snapdeal.cocofs.services.fm.dto.AddressDTO;
import com.snapdeal.cocofs.services.producer.IExternalNotificationsServiceForShipFromUpdate;
import com.snapdeal.cocofs.sro.FulfillmentModel;
import com.snapdeal.score.data.aerospike.exception.GetFailedException;
import com.snapdeal.score.data.aerospike.exception.PutFailedException;

/**
 * Class to process Seller SUPC ShipFrom Location  data update. Collect the notifications and push to external service to
 * publish these notification to activemq <br>
 * 1) SALESFORCE send SELLER-FM/FC mapping, Address Update add/updates via API hit <br>
 * 2) IPSM send SELLER-SUPC updates (new SUPC addition for SELLER) <br>
 * 3) CoCoFS Admin send SELLER-SUPC-FM/FC mappings to update/disable exceptions.<br>
 * 4) 
 * 
 * @author ssuthar
 */

@Service("SellerSUPCShipFromMappingProcessorImpl")
public class SellerSUPCShipFromMappingProcessorImpl implements ISellerSupcShipFromMappingProcessor {

    private static final Logger                                  LOG = LoggerFactory.getLogger(SellerSUPCShipFromMappingProcessorImpl.class);

    @Autowired
    private ISellerToSUPCListDBLookupService                     sellerToSUPCListLookupService;
    @Autowired
    private IFulfilmentModelService                              fulfilmentModelService;
    
    @Autowired
    private ISellerDetailsService                                sellerDetailService;

    @Autowired
    private IFMDBDataReadService                                 fmDBService;
    
    @Autowired
    IFCAerospikeDataReadService        fcAerospikeService;
   
    @Autowired
    @Qualifier("externalNotificationsServiceForShipFromUpdateImpl")
    private IExternalNotificationsServiceForShipFromUpdate shipFromUpdateProducer;

    private IDependency                                          dep = new Dependency();

    // case when FC adress is updated
    @Override
    public boolean processChangeOfFCAddressForExternalNotification(FCAddressUpdateEventObj event) throws GetFailedException, ExternalDataNotFoundException {
        if (!isShipFromUpdatePushEnabled()) {
            // returning true as this is logically correct termination of the process flow
            return true;
        }
        LOG.info("processing started for change of FC Address, event:{}", event);

        List<ShipFromUpdateMessage> shipFromMessageForQueuePush = new ArrayList<ShipFromUpdateMessage>();

        boolean bResult = true;
        if (event.getNewPincode() != null) {
            try {

                if (event.getNewPincode().equals(event.getOldPincode())) {
                    bResult = true;
                } else {
                    prepareSupcSellerListForFCToBePushed(event.getFcCode(), event.getNewPincode(), shipFromMessageForQueuePush);
                    bResult = pushToExternalNotificationService(shipFromMessageForQueuePush);
                }

            } catch (Exception e) {
                LOG.error("Could not fetch FM Model for pincode:{} fc:{} ", event.getNewPincode(), event.getFcCode());
                throw e;
            }
        }

        LOG.info("processing done for change of change of FC Address, returning {}", bResult);
        return bResult;
    
    }
    // case when SELLER-FM mapping is added/changed from salesforce
    @Override
    public boolean processChangeOfSellerFMMappingForExternalNotification(SellerFMFCUpdateEventObj e) {

        if (!isShipFromUpdatePushEnabled()) {
            // returning true as this is logically correct termination of the process flow
            return true;
        }

        LOG.info("processing started for change of seller fm mapping, event:{}", e);

        // find the corresponding FM type
        FulfillmentModel newFMType = FulfillmentModel.getFulfilmentModelByCode(e.getNewFM());
        FulfillmentModel oldFMType = FulfillmentModel.getFulfilmentModelByCode(e.getOldFM());

        List<ShipFromUpdateMessage> shipFromMessageForQueuePush = new ArrayList<ShipFromUpdateMessage>();
        // case 1) SELLER-FM is added first time, as this is supposed to be handled at SUPC push from IPMS
        if (oldFMType == null) {
        
            LOG.info("new SELLER-FM mapping is added for seller:{} FM:{}, no information is to be published for external service", e.getSellerCode(), newFMType);
        
        }else if (!e.getNewSellerPincode().equals(e.getOldSellerPincode())) {
          
            //CASE : pincode changes at seller level
            if (!isFMWareHouseModel(newFMType)) {
                // push all supc of seller minus exception list
                prepareSupcSellerListToBePushed(e.getSellerCode(), e.getNewSellerPincode(), shipFromMessageForQueuePush);
                
                // PLUS ++ get seller-supcs list from seller-supc-exception list which are not on ware house model
                prepareOnlySellerSupcExceptionListToBePushed(e.getSellerCode(), e.getNewSellerPincode(), shipFromMessageForQueuePush, false);

            } else if (isFMWareHouseModel(newFMType)) {
                //get seller-supcs list from seller-supc-exception list which are not on ware house model
                prepareOnlySellerSupcExceptionListToBePushed(e.getSellerCode(), e.getNewSellerPincode(), shipFromMessageForQueuePush, false);
            }
        
        }
        else {
            // find all SUPC for the seller and create queue push for all SUPC
            // except in exception list
            String newPincode = null;
            String oldPincode = null;
            
            // 
            //calculate new pincode for any non exception supc for this seller
            if(!isFMWareHouseModel(newFMType) || CollectionUtils.isEmpty(e.getNewFCCenters())){
                AddressDTO addressDTO = sellerDetailService.getSellerDetailsFromBaseSource(e.getSellerCode());
                newPincode = addressDTO.getPincode();
            }else {
                newPincode = getPincodeForFC(e.getNewFCCenters());
            }
            
            //calculate old pincode  for any non exception supc for this seller
            if(!isFMWareHouseModel(oldFMType) || CollectionUtils.isEmpty(e.getExistingFCCenters())){
                oldPincode = e.getOldSellerPincode();
            }else {
                oldPincode = getPincodeForFC(e.getExistingFCCenters());
            }
            
            
            
            //Calculate seller-supc list   
            if (!oldPincode.equals(newPincode)) {
                LOG.info("new shipFrom pincode "+newPincode + " is not equal to old shipfrom pincode "+ oldPincode+" hence publishing data for event {}",e);
                prepareSupcSellerListToBePushed(e.getSellerCode(), newPincode, shipFromMessageForQueuePush);

                /*
            
                //CASE 1: FC change at seller level
                if (hasFCChanged(e.getNewFCCenters(), e.getExistingFCCenters())) {
                    prepareSupcSellerListToBePushed(e.getSellerCode(), newPincode, shipFromMessageForQueuePush);
                }
                //CASE 2: FM change at seller level
                else if (!newFMType.equals(oldFMType)) {
                    if (!isFMWareHouseModel(newFMType)) {
                        //Case 2.1 when fm changes to non-warehouse model
                        prepareSupcSellerListToBePushed(e.getSellerCode(), newPincode, shipFromMessageForQueuePush);

                    } else if (isFMWareHouseModel(newFMType) && !e.getNewSellerPincode().equals(e.getOldSellerPincode())) {
                        //Case 2.2 when fm changes to warehouse model and sellercontactdetail pincode also changes
                        prepareSupcSellerListToBePushed(e.getSellerCode(), newPincode, shipFromMessageForQueuePush);

                    }
                }
            */}
           
            
        }

        boolean bResult = pushToExternalNotificationService(shipFromMessageForQueuePush);
        LOG.info("processing done for change of seller-fm-fc-mapping, returning {}", bResult);
        return bResult;
    }

    private boolean hasFCChanged(List<String> newCenters, List<String> existingCenters){
        if(newCenters == null){
            newCenters = new ArrayList<String>();
        }
        if(existingCenters == null){
            existingCenters = new ArrayList<String>();
        }
        return !newCenters.equals(existingCenters);
    }
    
    @Override
    public boolean processAdditionOfNewSellerSUPCMappingForExternalNotification(SellerSupcAddEventObj event) throws GetFailedException, ExternalDataNotFoundException,
            PutFailedException, GenericPersisterException {

        String sellerCode = event.getSellerCode();
        String supc = event.getSupc();

        if (!isShipFromUpdatePushEnabled()) {
            // returning true as this is logically correct termination of the process flow
            return true;
        }
        boolean bResult = true;
        LOG.info("processing started for addition of new seller SUPC mapping, seller:{}, supc:{}", sellerCode, supc);
        try {

            String pincode = null;
            FulfillmentModel fm = fulfilmentModelService.getFulfilmentModel(sellerCode, supc);
            
            List<String> fcCenters = getListOfFCsForSeller(sellerCode);
            
            if(!isFMWareHouseModel(fm) || CollectionUtils.isEmpty(fcCenters)){
                AddressDTO addressDTO = sellerDetailService.getSellerDetailsFromBaseSource(sellerCode);
                pincode = addressDTO.getPincode();
            }else {
                pincode = getPincodeForFC(fcCenters);
            }
            
            
            if(pincode != null){
                bResult = pushNotification(sellerCode, supc, pincode);
            }

        } catch (ExternalDataNotFoundException e) {
            LOG.error("Could not fetch FM Model for seller:{} supc:{} ", sellerCode, supc);
            throw e;
        }
        LOG.info("processing done for addition of seller-SUPC pair, returning {}", bResult);
        return bResult;
    }

    
    // case when seller-SUPC-FM-FC mapping is changed in exception list from CoCoFS admin
    @Override
    public boolean processChangeOfSellerSUPCFMMappingExceptionForExternalNotification(SellerSupcFMFCExceptionAddEventObj event) throws  Exception{

        if (!isShipFromUpdatePushEnabled()) {
            // returning true as this is logically correct termination of the process flow
            return true;
        }

        LOG.info("processing started for change of seller-SUPC-FM-FC exception mapping, event:{}", event);

        // find the corresponding FM type
        FulfillmentModel newFMType = FulfillmentModel.getFulfilmentModelByCode(event.getNewFM());
        FulfillmentModel oldFMType = FulfillmentModel.getFulfilmentModelByCode(event.getOldFM());

        boolean bResult = true;
        try {
            List<String> existinCenters = event.getExistingFCCenters();
            //fetch fm from seller level if not defined at seller supc level earlier
            if (oldFMType == null) {
                LOG.info("fetching fm-fc from seller level for event {}", event);

                String defaultFMStr = fulfilmentModelService.getFulfilmentModelBySeller(event.getSellerCode(), false);
                oldFMType = FulfillmentModel.getFulfilmentModelByCode(defaultFMStr);

                existinCenters = fulfilmentModelService.getEnabledFCsBySeller(event.getSellerCode(), false);

            }
           
            String oldPincode = getPincodeForFCandFM(event.getSellerCode(), existinCenters, oldFMType);
            String newPincode = getPincodeForFCandFM(event.getSellerCode(), event.getNewFCCenters(), newFMType);


           
            if (newPincode != null && !newPincode.equals(oldPincode)) {
                bResult = pushNotification(event.getSellerCode(), event.getSupc(), newPincode);
            }

        } catch (Exception e) {
            LOG.error("Could not fetch FM Model for seller:{} supc:{} ", event.getSellerCode(), event.getSupc());
            throw e;
        }
        
        LOG.info("processing done for change of seller-SUPC-FM-FC exception mapping, returning {}", bResult);
        return bResult;
    }

    private String getPincodeForFCandFM(String sellerCode, List<String> centers,FulfillmentModel fm){
        String pincode = null;
        if(!isFMWareHouseModel(fm) || CollectionUtils.isEmpty(centers)){
            LOG.info("fetching pincode from sellerContactDetails for seller {}, fm {}",sellerCode,fm.getCode());
            AddressDTO addressDTO = sellerDetailService.getSellerDetailsFromBaseSource(sellerCode);
            pincode = addressDTO.getPincode();
        }else {
            LOG.info("fetching pincode from fcCenter for seller {}, fc {}",sellerCode,centers);
            pincode = getPincodeForFC(centers);          
        }
        
        return pincode;
    }
    

    // case when seller-SUPC-FM-FC mapping is disabled in exception list from admin
    @Override
    public boolean processDisablingOfSellerSUPCFMMappingExceptionForExternalNotification(SellerSupcFMFCExceptionDeleteEventObj event) {

        if (!isShipFromUpdatePushEnabled()) {
            // returning true as this is logically correct termination of the process flow
            return true;
        }
        LOG.info("processing started for disabling of seller-SUPC-FM-FC exception mapping, event:{}", event);

        boolean bResult = true;
        try {
            FulfillmentModel oldFMType = FulfillmentModel.getFulfilmentModelByCode(event.getFM());
   
            
            String defaultFMStr = fulfilmentModelService.getFulfilmentModelBySeller(event.getSellerCode(), false);          
            FulfillmentModel newFMType = FulfillmentModel.getFulfilmentModelByCode(defaultFMStr);
            List<String> newCenters  = null;
            if(isFMWareHouseModel(newFMType)){
                newCenters = fulfilmentModelService.getEnabledFCsBySeller(event.getSellerCode(), false);
            }

            
            String oldPincode = getPincodeForFCandFM(event.getSellerCode(), event.getDisabledFCCenters(), oldFMType);
            String newPincode = getPincodeForFCandFM(event.getSellerCode(), newCenters, newFMType);


           
            if (newPincode != null && !newPincode.equals(oldPincode)) {
                bResult = pushNotification(event.getSellerCode(), event.getSupc(), newPincode);
            }

          

        } catch (ExternalDataNotFoundException e) {
            LOG.error("Could not fetch FM Model for seller:{} supc:{} ", event.getSellerCode(),event.getSupc());
            bResult = false;
        }
       

        LOG.info("processing done for disabling of seller-SUPC-FM-FC exception mapping, returning {}", bResult);
        return bResult;
    }

   
    //find all seller supc combinations which for fulfilmentCenter code is fcCode(passed in method).
    private void prepareSupcSellerListForFCToBePushed(String fcCode, String pincode,
            List<ShipFromUpdateMessage> shipFromMessageForQueuePush ) {
        // find all SUPC for the seller and create queue push for all SUPC
        // except in exception list
            List<String> sellerCodes = fmDBService.getEnabledSellerListAtSellerLevelForFCcode(fcCode);
            for (String sellerCode : sellerCodes) {
                // ask from aerospike
            try {
                List<String> listOfSUPCsFromDataSource = sellerToSUPCListLookupService.getSupcListForSeller(sellerCode);
                if (listOfSUPCsFromDataSource == null) {
                    listOfSUPCsFromDataSource = new ArrayList<String>();
                }
                LOG.info("supc-list-size {} for seller:{} ", listOfSUPCsFromDataSource.size(), sellerCode);


                // for each item in the SELLER-List<SUPC> mapping, update to
                // external push if no exception exists for SELLER-SUPC
                // combination

                List<String> listOfSUPCExceptionsFromDataSource = sellerToSUPCListLookupService.getSupcExceptionListForSeller(sellerCode);
                if (listOfSUPCExceptionsFromDataSource == null) {
                    listOfSUPCExceptionsFromDataSource = new ArrayList<String>();
                }

                LOG.info("supc-exception-list-size {} for seller:{} ", listOfSUPCExceptionsFromDataSource.size(), sellerCode);

                // create a set for faster lookup
                Set<String> supcExceptionSet = new HashSet<String>(listOfSUPCExceptionsFromDataSource);
                LOG.info("unique supc-exception-set-size {} for seller:{} ", supcExceptionSet.size(), sellerCode);

                for (String supc : listOfSUPCsFromDataSource) {
                    //if SUPC does not exist in Exception list, push to queue
                    if (!supcExceptionSet.contains(supc)) {
                        //collect all such events
                        addToNotificationList(sellerCode, supc, pincode, shipFromMessageForQueuePush);
                    }
                }
            } catch (ExternalDataNotFoundException e) {
                    LOG.error("Could not fetch seller-supc-mapping for seller:{} Exception:{} ", sellerCode, e);
                }
            }

            
            //Get list for seller supc excpetion which has fc code as this.
            List<SellerSupcFMMapping> listOfSellerSUPCsForFcCode = fmDBService.getEnabledSellerSupcMappingListForFcCode(fcCode);
            if (listOfSellerSUPCsForFcCode == null) {
                listOfSellerSUPCsForFcCode = new ArrayList<SellerSupcFMMapping>();
            }
            LOG.info("supc-list-size {} for fcCode:{} ", listOfSellerSUPCsForFcCode.size(), fcCode);

        for (SellerSupcFMMapping sellersupcMapping : listOfSellerSUPCsForFcCode) {

            //collect all such events
            addToNotificationList(sellersupcMapping.getSellerCode(), sellersupcMapping.getSupc(), pincode, shipFromMessageForQueuePush);
        }
            

        
        
    }
    
    private void prepareOnlySellerSupcExceptionListToBePushed(String sellerCode, String pincode,
            List<ShipFromUpdateMessage> shipFromMessageForQueuePush, boolean wareHouseModelFM ) {
        // find all SUPC for the seller in seller supc fm mapping exception list and create queue push for all SUPC
        // also check if fm is on ware-house or non-ware-house model depending on input argument wareHouseModelFM.
        try {
            // ask from aerospike
           
            List<SellerSupcFMMapping> listOfSUPCExceptionsFromDataSource = fmDBService.getAllEnabledSellerSupcFMMappingBySeller(sellerCode);
            if (listOfSUPCExceptionsFromDataSource == null) {
                listOfSUPCExceptionsFromDataSource = new ArrayList<SellerSupcFMMapping>();
            }

            LOG.info("supc-exception-list-size {} for seller:{} ", listOfSUPCExceptionsFromDataSource.size(), sellerCode);

           
            for (SellerSupcFMMapping mapping : listOfSUPCExceptionsFromDataSource) {
                FulfillmentModel fmType = FulfillmentModel.getFulfilmentModelByCode(mapping.getFulfilmentModel());
                // if wareHouseModelFM flag is true then fmType should also be on warehouse model and vice versa
                if (!(isFMWareHouseModel(fmType)^ wareHouseModelFM)) {
                    //collect all such events
                    addToNotificationList(sellerCode, mapping.getSupc(), pincode, shipFromMessageForQueuePush);
                }
            }

        } catch (Exception e) {
            LOG.error("Could not fetch seller-supc-fm-mapping for seller:" +  sellerCode+" Exception:{} ", e);
        }
    }
    
    
    private void prepareSupcSellerListToBePushed(String sellerCode, String pincode,
            List<ShipFromUpdateMessage> shipFromMessageForQueuePush ) {
        // find all SUPC for the seller and create queue push for all SUPC
        // except in exception list
        try {
            // ask from aerospike
            List<String> listOfSUPCsFromDataSource = sellerToSUPCListLookupService.getSupcListForSeller(sellerCode);
            if (listOfSUPCsFromDataSource == null) {
                listOfSUPCsFromDataSource = new ArrayList<String>();
            }
            LOG.info("supc-list-size {} for seller:{} ", listOfSUPCsFromDataSource.size(), sellerCode);

            // for each item in the SELLER-List<SUPC> mapping, update to
            // external push if no exception exists for SELLER-SUPC
            // combination

            List<String> listOfSUPCExceptionsFromDataSource = sellerToSUPCListLookupService.getSupcExceptionListForSeller(sellerCode);
            if (listOfSUPCExceptionsFromDataSource == null) {
                listOfSUPCExceptionsFromDataSource = new ArrayList<String>();
            }

            LOG.info("supc-exception-list-size {} for seller:{} ", listOfSUPCExceptionsFromDataSource.size(), sellerCode);

            // create a set for faster lookup
            Set<String> supcExceptionSet = new HashSet<String>(listOfSUPCExceptionsFromDataSource);
            LOG.info("unique supc-exception-set-size {} for seller:{} ", supcExceptionSet.size(), sellerCode);

            //compare the items in list with exceptions
            for (String supc : listOfSUPCsFromDataSource) {
                //if SUPC does not exist in Exception list, push to queue
                if (!supcExceptionSet.contains(supc)) {
                    //collect all such events
                    addToNotificationList(sellerCode, supc, pincode, shipFromMessageForQueuePush);
                }
            }

        } catch (ExternalDataNotFoundException e) {
            LOG.error("Could not fetch seller-supc-mapping for seller:{} Exception:{} ", sellerCode, e);
        }
    }

    private List<String> getListOfFCsForSeller(String sellerCode) throws ExternalDataNotFoundException {
        SellerFMMappingVO vo = fulfilmentModelService.getFulfilmentModelBySellerFromAerospike(sellerCode);
        List<String> sellerFCPairs = null;
        if (vo != null && vo.isEnabled()) {
            sellerFCPairs = vo.getFcCenters();
        }
        return sellerFCPairs;
    }
    
    private String getPincodeForFC(List<String> fcCenters){
    String pincode = null;
        if (!CollectionUtils.isEmpty(fcCenters)) {
            FCDetailVO fcVO = fcAerospikeService.getFCDetailVO(fcCenters.get(0));
            if(fcVO != null){    
                pincode = fcVO.getPincode();
            }
        }
        return pincode;
   }

    private boolean pushNotification(String sellerCode, String supc, String pincode) {
        List<ShipFromUpdateMessage> shipFromUpdateMessageQueuePush = new ArrayList<ShipFromUpdateMessage>();
        addToNotificationList(sellerCode, supc, pincode, shipFromUpdateMessageQueuePush);
        return pushToExternalNotificationService(shipFromUpdateMessageQueuePush);
    }

    private void addToNotificationList(String sellerCode, String supc, String pincode, List<ShipFromUpdateMessage> shipFromMessageForQueuePush) {
        // if any update requires push-to-queue, collect it for sending to push. add this record to later push-to-queue.
        ShipFromUpdateMessage obj = createSellerSupcFMFCUpdateSRO(sellerCode, supc,pincode);
        LOG.info("adding SRO:{} to external notification list", obj);
        shipFromMessageForQueuePush.add(obj);
    }

    private ShipFromUpdateMessage createSellerSupcFMFCUpdateSRO(String sellerCode, String supc, String pincode) {
        ShipFromUpdateMessage obj = new ShipFromUpdateMessage();
        obj.setSellerCode(sellerCode);
        obj.setSupc(supc);
        obj.setPincode(pincode);
        return obj;
    }

    /**
     * push the data to external notification service currently backed by activemq, data pushed is in form:
     * seller-supc-fm-fcList. The current intended recipient is Search module, which may change in future.
     * 
     * @param sellerSUPCFMMappingForQueuePush
     */
    private boolean pushToExternalNotificationService(List<ShipFromUpdateMessage> shipFromUpdateMessageListForQueuePush) {
        // all seller/supc processed, push to score if anything
        if (!shipFromUpdateMessageListForQueuePush.isEmpty()) {
            // forward the data for push-to-queue
            return shipFromUpdateProducer.publishToQueue(shipFromUpdateMessageListForQueuePush);
        }
        // return true in case of no events
        return true;
    }

    
     
    private boolean isFMWareHouseModel(FulfillmentModel fMType){
        return fMType.isLocationRequired();
    }
    
    private boolean isFMMovedtoNonWareHouseModel(FulfillmentModel oldFMType ,FulfillmentModel newFMType){
        return oldFMType != newFMType && (isFMWareHouseModel(oldFMType)) && !isFMWareHouseModel(newFMType);
    }
    
    private boolean isShipFromUpdatePushEnabled() {
        if (!dep.getBooleanPropertyValue(Property.SHIPFROM_UPDATE_PUSH_ENABLED)) {
            LOG.warn("push of shipfrom is disabled. Returning without any further processing ... ");
            return false;
        }
        return true;
    }

    public interface IDependency {
        boolean getBooleanPropertyValue(Property property);
    }

    static class Dependency implements IDependency {
        @Override
        public boolean getBooleanPropertyValue(Property property) {
            return ConfigUtils.getBooleanScalar(property);
        }
    }
}
