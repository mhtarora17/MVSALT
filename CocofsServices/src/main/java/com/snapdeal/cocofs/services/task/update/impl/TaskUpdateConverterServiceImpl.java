/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 28-Aug-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.services.task.update.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.cocofs.entity.TaskDetail;
import com.snapdeal.cocofs.entity.TaskParam;
import com.snapdeal.cocofs.services.task.dto.TaskDetailDTO;
import com.snapdeal.cocofs.services.task.dto.TaskParamDTO;
import com.snapdeal.cocofs.services.task.update.ITaskUpdateConverterService;
import com.snapdeal.task.core.TaskRequest;

@Service("TaskUpdateConverterServiceImpl")
public class TaskUpdateConverterServiceImpl implements ITaskUpdateConverterService {

    private static final Logger LOG = LoggerFactory.getLogger(TaskUpdateConverterServiceImpl.class);

    @Override
    public TaskDetail getTaskDetail(TaskDetailDTO taskDetailDTO, String updatedBy) {
        TaskDetail taskDetail = null;
        if (null != taskDetailDTO) {
            taskDetail = new TaskDetail();
            taskDetail.setName(taskDetailDTO.getTaskName());
            taskDetail.setId(taskDetailDTO.getTaskDetailId());
            taskDetail.setClustered(taskDetailDTO.getClustered());
            taskDetail.setCronExpression(taskDetailDTO.getCronExpression());
            taskDetail.setImplClass(taskDetailDTO.getImplClass());
            taskDetail.setConcurrent(taskDetailDTO.getConcurrent());
            if (StringUtils.isNotEmpty(taskDetailDTO.getCreated())) {
                taskDetail.setCreated(DateUtils.stringToDate(taskDetailDTO.getCreated(), "yyyy-MM-dd HH:mm:ss"));
            } else {
                taskDetail.setCreated(DateUtils.getCurrentTime());
            }
            taskDetail.setEnabled(taskDetailDTO.getEnabled());
            taskDetail.setHostName(taskDetailDTO.getHostName());
            taskDetail.setLastUpdated(DateUtils.getCurrentTime());
            taskDetail.setUpdatedBy(updatedBy);
            taskDetail.setQuartzManaged(true);
        }
        return taskDetail;
    }

    @Override
    public List<TaskParam> getTaskParams(List<TaskParamDTO> taskParamsDTO, String taskName, String updatedBy) {
        List<TaskParam> taskParams = new ArrayList<TaskParam>(0);
        if (null != taskParamsDTO && !taskParamsDTO.isEmpty()) {
            for (TaskParamDTO param : taskParamsDTO) {
                TaskParam taskParam = new TaskParam();
                if (StringUtils.isNotEmpty(param.getCreated())) {
                    taskParam.setCreated(DateUtils.stringToDate(param.getCreated(), "yyyy-MM-dd HH:mm:ss"));
                } else {
                    taskParam.setCreated(DateUtils.getCurrentTime());
                }
                taskParam.setId(param.getTaskParamId());
                taskParam.setLastUpdated(DateUtils.getCurrentTime());
                taskParam.setName(param.getParamName());
                taskParam.setValue(param.getParamValue());
                taskParam.setUpdatedBy(updatedBy);
                taskParam.setTaskName(taskName);
                taskParams.add(taskParam);
            }
        }
        return taskParams;
    }

    @Override
    public TaskDetailDTO getTaskDetailDTO(TaskDetail taskDetail) {
        TaskDetailDTO dto = null;
        if (null != taskDetail) {
            dto = new TaskDetailDTO();
            dto.setClustered(taskDetail.isClustered());
            dto.setConcurrent(taskDetail.isConcurrent());
            dto.setCreated(DateUtils.dateToString(taskDetail.getCreated(), "yyyy-MM-dd HH:mm:ss"));
            dto.setCronExpression(taskDetail.getCronExpression());
            dto.setEnabled(taskDetail.isEnabled());
            dto.setTaskDetailId(taskDetail.getId());
            dto.setImplClass(taskDetail.getImplClass());
            dto.setTaskName(taskDetail.getName());
            dto.setHostName(taskDetail.getHostName());
        }
        return dto;
    }

    @Override
    public List<TaskParamDTO> getTaskParamDTOList(List<TaskParam> taskParams) {
        List<TaskParamDTO> taskParamDTOs = new ArrayList<TaskParamDTO>();
        if (null != taskParams && !taskParams.isEmpty()) {
            for (TaskParam param : taskParams) {
                taskParamDTOs.add(getTaskParamDTO(param));
            }
        }
        return taskParamDTOs;
    }

    @Override
    public TaskParamDTO getTaskParamDTO(TaskParam taskParam) {
        TaskParamDTO dto = null;
        if (null != taskParam) {
            dto = new TaskParamDTO();
            dto.setTaskParamId(taskParam.getId());
            dto.setParamName(taskParam.getName());
            dto.setParamValue(taskParam.getValue());
            dto.setCreated(DateUtils.dateToString(taskParam.getCreated(), "yyyy-MM-dd HH:mm:ss"));
        }
        return dto;

    }

    @Override
    public TaskRequest getTaskRequest(TaskDetail taskDetail) {
        return new TaskRequest(taskDetail.getName(), taskDetail.getImplClass(), taskDetail.getCronExpression(), taskDetail.isClustered(), taskDetail.isConcurrent());
    }

}
