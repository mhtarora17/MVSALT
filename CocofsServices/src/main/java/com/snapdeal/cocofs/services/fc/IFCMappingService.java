/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 09-Oct-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.services.fc;

import java.util.List;
import java.util.Map;

import com.snapdeal.cocofs.core.aerospike.vo.SellerFMMappingVO;
import com.snapdeal.cocofs.core.aerospike.vo.SellerSupcFMMappingVO;
import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.sro.FMFCPair;
import com.snapdeal.cocofs.sro.SellerSUPCPair;

public interface IFCMappingService {

    Map<SellerSUPCPair, FMFCPair> getFulfilmentCentres(List<SellerSUPCPair> sroList) throws ExternalDataNotFoundException;

    SellerFMMappingVO getFulfilmentCentreBySellerFromAerospike(String sellerCode) throws ExternalDataNotFoundException;

    SellerSupcFMMappingVO getFulfilmentCentreBySellerAndSupcFromAerospike(String sellerCode, String supc) throws ExternalDataNotFoundException;

    FMFCPair getFulfilmentCentreCodes(String sellerCode, String supc) throws ExternalDataNotFoundException;

}
