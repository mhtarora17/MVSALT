/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.services.events.fm.fc.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.services.events.dto.SellerSupcFMFCExceptionAddEventObj;
import com.snapdeal.cocofs.services.events.impl.BaseEventConsumer;
import com.snapdeal.cocofs.services.events.impl.EventType;
import com.snapdeal.cocofs.services.fm.ISellerSUPCFMMappingProcessor;

/**
 * @version 1.0, 25-Mar-2015
 * @author shiv
 */
@Service("SellerSupcFMExceptionAddFMUpdateEventConsumer")
public class SellerSupcFMExceptionAddFMUpdateEventConsumer extends BaseEventConsumer<SellerSupcFMFCExceptionAddEventObj> {

    @Autowired
    @Qualifier("sellerSUPCFMMappingProcessor")
    private ISellerSUPCFMMappingProcessor sellerSUPCFMMappingProcessor;

    private static final Logger           LOG = LoggerFactory.getLogger(SellerSupcFMExceptionAddFMUpdateEventConsumer.class);

    @Override
    protected String getEventCode() {
        return EventType.FM_UPDATE_EVENT_ON_SELLER_SUPC_FM_EXC_UPDATE.getCode();
    }

    @Override
    protected boolean processEventObj(SellerSupcFMFCExceptionAddEventObj eventObject) {

        try {
            return sellerSUPCFMMappingProcessor.processChangeOfSellerSUPCFMMappingExceptionForExternalNotification(eventObject);
        } catch (Exception e) {
            LOG.error("Exception while processing the request for event: {}. Exception {}", eventObject, e);
        }

        return false;
    }
}
