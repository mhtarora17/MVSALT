package com.snapdeal.cocofs.services.fm;

import java.util.List;

import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.generic.persister.exception.GenericPersisterException;
import com.snapdeal.score.data.aerospike.exception.GetFailedException;

public interface ISellerToSUPCListDBLookupService {

	boolean isSUPCExistsInSellerSUPCListMapping(String sellerCode, String supc) throws ExternalDataNotFoundException, GetFailedException;

    List<String> getSupcListForSeller(String sellerCode) throws ExternalDataNotFoundException;

    List<String> getSupcExceptionListForSeller(String sellerCode) throws ExternalDataNotFoundException;

    void putNewSellerSupcMapping(String sellerCode, String supc) throws GenericPersisterException;
    
    void removeSellerSupcMapping(String sellerCode, String supc) throws GenericPersisterException;
}
