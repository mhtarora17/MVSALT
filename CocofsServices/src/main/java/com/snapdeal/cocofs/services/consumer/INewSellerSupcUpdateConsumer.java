package com.snapdeal.cocofs.services.consumer;

import javax.jms.MessageListener;

public interface INewSellerSupcUpdateConsumer extends MessageListener {

    public void unregisterConsumer();

    public void registerConsumer(String queueName, String networkPath, String username, String password);

}
