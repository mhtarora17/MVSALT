/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.services.events.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @version 1.0, 25-Mar-2015
 * @author shiv
 */
public class SellerSupcFMFCExceptionDeleteEventObj implements Serializable  {

    /**
     * 
     */
    private static final long serialVersionUID = -334826955431117480L;
    private String            sellerCode;
    private String            supc;
    private String            fm;
    private List<String>      disabledFCCenters;
    private Date              created;

    public SellerSupcFMFCExceptionDeleteEventObj() {
    }

    public SellerSupcFMFCExceptionDeleteEventObj(String sellerCode, String supc, String fm, List<String> disabledFCCenters) {
        this.sellerCode = sellerCode;
        this.supc = supc;
        this.fm = fm;
        this.disabledFCCenters = disabledFCCenters;
        this.created = new Date();
    }

    public String getSellerCode() {
        return sellerCode;
    }

    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }

    public String getSupc() {
        return supc;
    }

    public void setSupc(String supc) {
        this.supc = supc;
    }

    public String getFM() {
        return fm;
    }

    public void setFM(String fm) {
        this.fm = fm;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public List<String> getDisabledFCCenters() {
        return disabledFCCenters;
    }

    public void setDisabledFCCenters(List<String> disabledFCCenters) {
        this.disabledFCCenters = disabledFCCenters;
    }

    @Override
    public String toString() {
        return "SellerSupcFMFCExceptionDeleteEventObj [sellerCode=" + sellerCode + ", supc=" + supc + ", fm=" + fm + ", disabledFCCenters=" + disabledFCCenters + ", created="
                + created + "]";
    }


}
