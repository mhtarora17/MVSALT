/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 04-Sep-2013
 *  @author himanshum
 */
package com.snapdeal.cocofs.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.cocofs.common.JobDetailDTO;
import com.snapdeal.cocofs.common.MyJobDetailDTO;
import com.snapdeal.cocofs.entity.JobDetail;
import com.snapdeal.cocofs.services.IJobsConvertorService;

@Service("JobsConvertorServiceImpl")
public class JobsConvertorServiceImpl implements IJobsConvertorService {

    @Override
    public List<JobDetailDTO> getJobDetailDTOList(List<JobDetail> allJobDetails) {
            List<JobDetailDTO> jdList=new ArrayList<JobDetailDTO>();
            for(JobDetail jobDetail:allJobDetails){
                    JobDetailDTO jdDTO=new JobDetailDTO();
                    jdDTO.setJobCode(jobDetail.getFileCode());
                    jdDTO.setFileName(jobDetail.getFileName());
                    jdDTO.setStatus(jobDetail.getStatus().getDescription());
                    jdDTO.setAction(jobDetail.getAction().getDescription());
                    jdDTO.setCreated(DateUtils.dateToString(jobDetail.getCreated(), "yyyy-MM-dd HH:mm:ss"));
                    jdDTO.setUpdated(DateUtils.dateToString(jobDetail.getLastUpdated(), "yyyy-MM-dd HH:mm:ss"));
                    jdDTO.setUploadedBy(jobDetail.getUploadedBy());
                    jdDTO.setPriority(jobDetail.getPriority());
                    jdList.add(jdDTO);
            }
            return jdList;
    }
    @Override
    public List<MyJobDetailDTO> getMyJobDetailDTOList(List<JobDetail> allJobDetails) {
            List<MyJobDetailDTO> jdList=new ArrayList<MyJobDetailDTO>();
            for(JobDetail jobDetail:allJobDetails){
                    MyJobDetailDTO jdDTO=new MyJobDetailDTO();
                    jdDTO.setJobCode(jobDetail.getFileCode());
                    jdDTO.setFileName(jobDetail.getFileName());
                    jdDTO.setStatus(jobDetail.getStatus().getDescription());
                    jdDTO.setAction(jobDetail.getAction().getDescription());
                    jdDTO.setCreated(DateUtils.dateToString(jobDetail.getCreated(), "yyyy-MM-dd HH:mm:ss"));
                    jdDTO.setUpdated(DateUtils.dateToString(jobDetail.getLastUpdated(), "yyyy-MM-dd HH:mm:ss"));
                    jdList.add(jdDTO);
            }
            return jdList;
    }

}
