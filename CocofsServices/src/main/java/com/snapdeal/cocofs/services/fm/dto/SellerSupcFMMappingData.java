/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 26-Nov-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.services.fm.dto;

import com.snapdeal.cocofs.annotation.IgnoreInDownloadTemplate;

public class SellerSupcFMMappingData {

    private String supc;

    private String sellerCode;

    private String fulfilmentModel;

    //comma separated list
    @IgnoreInDownloadTemplate(propertyName = com.snapdeal.cocofs.configuration.Property.PropertyNames.SELLER_SUPC_FM_FC_UPDATE_PUSH_ENABLED_KEY, onValue = false)
    private String fcCenters;

    private String updated;

    private String updatedBy;

    public SellerSupcFMMappingData() {
    }

    public SellerSupcFMMappingData(String supc, String sellerCode, String fulfilmentModel, String fcCentreList, String updated, String updatedBy) {
        this.supc = supc;
        this.sellerCode = sellerCode;
        this.fulfilmentModel = fulfilmentModel;
        this.fcCenters = fcCentreList;
        this.updated = updated;
        this.updatedBy = updatedBy;
    }

    public String getSupc() {
        return supc;
    }

    public void setSupc(String supc) {
        this.supc = supc;
    }

    public String getSellerCode() {
        return sellerCode;
    }

    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }

    public String getFulfilmentModel() {
        return fulfilmentModel;
    }

    public void setFulfilmentModel(String fulfilmentModel) {
        this.fulfilmentModel = fulfilmentModel;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getFcCenters() {
        return fcCenters;
    }

    public void setFcCenters(String fcCentres) {
        this.fcCenters = fcCentres;
    }

    @Override
    public String toString() {
        return "SellerSupcFMMappingData [supc=" + supc + ", sellerCode=" + sellerCode + ", fulfilmentModel=" + fulfilmentModel + ", fcCentres=" + fcCenters + ", updated="
                + updated + ", updatedBy=" + updatedBy + "]";
    }

}
