/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.services.events.impl;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.services.events.IEventConsumer;
import com.snapdeal.cocofs.services.events.IEventConsumerRegistry;

/**
 * @version 1.0, 26-Jan-2015
 * @author shiv
 */
@Service
public class EventConsumerRegistryService implements IEventConsumerRegistry {

    private static final Logger    LOG = LoggerFactory.getLogger(EventConsumerRegistryService.class);

    Map<String, IEventConsumer<?>> map = new HashMap<String, IEventConsumer<?>>();

    @Override
    public boolean registerConsumer(String eventCode, IEventConsumer<?> consumer) {

        boolean success = false;
        if (!map.containsKey(eventCode)) {
            map.put(eventCode, consumer);
            success = true;
        }

        LOG.info("registerConsumer - returning {} for event: {}, consumer: {}", success, eventCode, consumer);
        return success;
    }

    @Override
    public boolean unregisterConsumer(String eventCode) {
        boolean success = false;

        IEventConsumer<?> consumer = null;
        if (map.containsKey(eventCode)) {
            consumer = map.remove(eventCode);
            success = true;
        }
        LOG.info("unregisterConsumer - removed? {} for event: {}, removed consumer: {}", success, eventCode, consumer);
        return success;
    }

    @Override
    public IEventConsumer<?> getConsumer(String eventCode) {
        return map.get(eventCode);
    }

}
