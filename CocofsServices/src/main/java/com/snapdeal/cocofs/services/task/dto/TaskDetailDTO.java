/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 28-Aug-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.services.task.dto;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TaskDetailDTO {

    private Integer            taskDetailId;

    private String             taskName;

    private String             implClass;

    private String             cronExpression;

    private Boolean            enabled;

    private Boolean            concurrent;

    private Boolean            clustered;

    private String             created;

    private String             nextScheduled;

    private String             hostName;

    private List<TaskParamDTO> taskParams = new ArrayList<TaskParamDTO>(0);

    public Integer getTaskDetailId() {
        return taskDetailId;
    }

    public void setTaskDetailId(Integer id) {
        this.taskDetailId = id;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getImplClass() {
        return implClass;
    }

    public void setImplClass(String implClass) {
        this.implClass = implClass;
    }

    public String getCronExpression() {
        return cronExpression;
    }

    public void setCronExpression(String cronExpression) {
        this.cronExpression = cronExpression;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Boolean getConcurrent() {
        return concurrent;
    }

    public void setConcurrent(Boolean concurrent) {
        this.concurrent = concurrent;
    }

    public Boolean getClustered() {
        return clustered;
    }

    public void setClustered(Boolean clustered) {
        this.clustered = clustered;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getNextScheduled() {
        return nextScheduled;
    }

    public void setNextScheduled(String nextScheduled) {
        this.nextScheduled = nextScheduled;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public List<TaskParamDTO> getTaskParams() {
        return taskParams;
    }

    public void setTaskParams(List<TaskParamDTO> taskParams) {
        this.taskParams = taskParams;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("TaskDetailDTO [taskDetailId=").append(taskDetailId).append(", taskName=").append(taskName).append(", implClass=").append(implClass).append(
                ", cronExpression=").append(cronExpression).append(", enabled=").append(enabled).append(", concurrent=").append(concurrent).append(", clustered=").append(clustered).append(
                ", created=").append(created).append(", nextScheduled=").append(nextScheduled).append(", hostName=").append(hostName).append(", taskParams=").append(taskParams).append(
                "]");
        return builder.toString();
    }

}
