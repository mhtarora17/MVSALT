package com.snapdeal.cocofs.services.impl;

import java.lang.reflect.Field;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.cocofs.access.IPendingProductAttributeDBDataReadService;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.entity.PendingProductAttributeUpdate;
import com.snapdeal.cocofs.excelupload.ProductAttributeUploadDTO;
import com.snapdeal.cocofs.services.IUploadDataModifier;

/**
 * @author nikhil
 */

@Service("pauDataModifier")
public class PAUDataModifier implements IUploadDataModifier<ProductAttributeUploadDTO> {

    @Autowired
    IPendingProductAttributeDBDataReadService pendingProductAttributeDBDataReadService;

    private static final Logger               LOG = LoggerFactory.getLogger(PAUDataModifier.class);

    @Override
    public void modifyData(ProductAttributeUploadDTO dto) throws IllegalArgumentException, IllegalAccessException {
        if (checkIfPendingDataRequiredToBePopulated(dto)) {
            LOG.info("Pending data check required for dto {}", dto.toString());
            Field[] fields = dto.getClass().getDeclaredFields();

            for (Field field : fields) {
                field.setAccessible(true);
                if (isFieldInPendingConfriguration(field.getName()) && field.get(dto) == null) {
                    Double value = getPendingData(dto.getSupc(), field.getName());
                    field.set(dto, value);
                }
            }
        } else {
            LOG.info("No pendancy check required for dto {} as all the required attributes are present", dto);
            return;
        }
    }

    private boolean checkIfPendingDataRequiredToBePopulated(ProductAttributeUploadDTO dto) throws IllegalArgumentException, IllegalAccessException {

        Field[] fields = dto.getClass().getDeclaredFields();
        int count = 0;
        for (Field field : fields) {
            field.setAccessible(true);
            if (isFieldInPendingConfriguration(field.getName()) && field.get(dto) == null) {
                count++;
            }
        }
        //if no field which is in pending configuration is uploaded, we skip pending data check.
        // if the count  == size, then also we  should not check pending configuration.
        return (count > 0);
    }

    private Double getPendingData(String supc, String field) {
        List<PendingProductAttributeUpdate> productAttributes = pendingProductAttributeDBDataReadService.getPendingProductAtributeUpdate(supc);

        for (PendingProductAttributeUpdate pendingUpdate : productAttributes) {
            if (StringUtils.isEmpty(pendingUpdate.getAttributeName()))
                continue;

            if (isFieldInPendingConfriguration(pendingUpdate.getAttributeName())) {
                if (field.equalsIgnoreCase(pendingUpdate.getAttributeName())) {
                    LOG.info("Fetched pending value {} - field {} for supc {}", pendingUpdate.getNewValue(), field, supc);
                    return Double.valueOf(pendingUpdate.getNewValue());
                }
            }
        }

        return null;
    }

    private boolean isFieldInPendingConfriguration(String fieldName) {
        return ConfigUtils.isPresentInList(Property.PENDING_CHECK_ATTRIBUTES_LIST, fieldName.toUpperCase());
    }
}
