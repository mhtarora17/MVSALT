package com.snapdeal.cocofs.services.common.dto;

import java.util.Date;

public class ProductAttributeDownloadData {

    private String  supc;

    private String  serializedType;

    private String  packagingType;

    private Boolean fragile;

    private Integer productParts;

    private Double  weight;

    private Double  pendingWeight;

    private Date    wefPendingWeight;

    private Double  length;

    private Double  pendingLength;

    private Date    wefPendingLength;

    private Double  primaryLength;

    private Double  breadth;

    private Double  pendingBreadth;

    private Date    wefPendingBreadth;

    private Double  primaryBreadth;

    private Double  height;

    private Double  pendingHeight;

    private Date    wefPendingHeight;

    private Double  primaryHeight;

    private String  dangerousGoodsType;

    private Boolean woodenPackaging;

    private Boolean serialized;

    public ProductAttributeDownloadData() {
        super();
    }

    public String getSupc() {
        return supc;
    }

    public void setSupc(String supc) {
        this.supc = supc;
    }

    public Boolean getSerialized() {
        return serialized;
    }

    public void setSerialized(Boolean serialized) {
        this.serialized = serialized;
    }

    public Boolean getFragile() {
        return fragile;
    }

    public void setFragile(Boolean fragile) {
        this.fragile = fragile;
    }

    public Integer getProductParts() {
        return productParts;
    }

    public void setProductParts(Integer productParts) {
        this.productParts = productParts;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Double getPendingWeight() {
        return pendingWeight;
    }

    public void setPendingWeight(Double pendingWeight) {
        this.pendingWeight = pendingWeight;
    }

    public Date getWefPendingWeight() {
        return wefPendingWeight;
    }

    public void setWefPendingWeight(Date wefPendingWeight) {
        this.wefPendingWeight = wefPendingWeight;
    }

    public Double getLength() {
        return length;
    }

    public void setLength(Double length) {
        this.length = length;
    }

    public Double getPrimaryLength() {
        return primaryLength;
    }

    public void setPrimaryLength(Double primaryLength) {
        this.primaryLength = primaryLength;
    }

    public Double getPendingLength() {
        return pendingLength;
    }

    public void setPendingLength(Double pendingLength) {
        this.pendingLength = pendingLength;
    }

    public Date getWefPendingLength() {
        return wefPendingLength;
    }

    public void setWefPendingLength(Date wefPendingLength) {
        this.wefPendingLength = wefPendingLength;
    }

    public Double getBreadth() {
        return breadth;
    }

    public void setBreadth(Double breadth) {
        this.breadth = breadth;
    }

    public Double getPrimaryBreadth() {
        return primaryBreadth;
    }

    public void setPrimaryBreadth(Double primaryBreadth) {
        this.primaryBreadth = primaryBreadth;
    }

    public Double getPendingBreadth() {
        return pendingBreadth;
    }

    public void setPendingBreadth(Double pendingBreadth) {
        this.pendingBreadth = pendingBreadth;
    }

    public Date getWefPendingBreadth() {
        return wefPendingBreadth;
    }

    public void setWefPendingBreadth(Date wefPendingBreadth) {
        this.wefPendingBreadth = wefPendingBreadth;
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public Double getPrimaryHeight() {
        return primaryHeight;
    }

    public void setPrimaryHeight(Double primaryHeight) {
        this.primaryHeight = primaryHeight;
    }

    public Double getPendingHeight() {
        return pendingHeight;
    }

    public void setPendingHeight(Double pendingHeight) {
        this.pendingHeight = pendingHeight;
    }

    public Date getWefPendingHeight() {
        return wefPendingHeight;
    }

    public void setWefPendingHeight(Date wefPendingHeight) {
        this.wefPendingHeight = wefPendingHeight;
    }

    public String getDangerousGoodsType() {
        return dangerousGoodsType;
    }

    public void setDangerousGoodsType(String dangerousGoodsType) {
        this.dangerousGoodsType = dangerousGoodsType;
    }

    public Boolean getWoodenPackaging() {
        return woodenPackaging;
    }

    public void setWoodenPackaging(Boolean woodenPackaging) {
        this.woodenPackaging = woodenPackaging;
    }

    public String getSerializedType() {
        return serializedType;
    }

    public void setSerializedType(String serializedType) {
        this.serializedType = serializedType;
    }

    public String getPackagingType() {
        return packagingType;
    }

    public void setPackagingType(String packagingType) {
        this.packagingType = packagingType;
    }

}
