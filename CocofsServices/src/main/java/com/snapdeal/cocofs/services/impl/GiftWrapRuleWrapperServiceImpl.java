/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.services.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.math.RandomUtils;
import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.snapdeal.base.cache.CacheManager;
import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.cocofs.common.CocofsRuleParam;
import com.snapdeal.cocofs.common.CriteriaDTO;
import com.snapdeal.cocofs.common.RuleUpdateResponse;
import com.snapdeal.cocofs.enums.BlockName;
import com.snapdeal.cocofs.enums.GiftWrapCategoryServiceabilityRuleCriteria;
import com.snapdeal.cocofs.giftwrap.dto.GiftWrapCategoryRuleEditForm;
import com.snapdeal.cocofs.giftwrap.dto.GiftWrapRuleUpdateDTO;
import com.snapdeal.cocofs.mongo.CocofsMongoQuery;
import com.snapdeal.cocofs.mongo.CocofsMongoQuery.Operator;
import com.snapdeal.cocofs.mongo.mao.IGenericMao;
import com.snapdeal.cocofs.mongo.model.CocofsRuleUnit;
import com.snapdeal.cocofs.mongo.model.CocofsRuleUnit.CocofsRuleUnitFieldName;
import com.snapdeal.cocofs.mongo.model.CocofsRuleUnit.CocofsRuleUnitType;
import com.snapdeal.cocofs.mongo.model.CriteriaUnit;
import com.snapdeal.cocofs.services.IGiftWrapRuleWrapperService;
import com.snapdeal.rule.cache.BlocksCache;
import com.snapdeal.rule.engine.entity.Criteria;
import com.snapdeal.rule.engine.entity.Operators;
import com.snapdeal.rule.engine.entity.Rule;
import com.snapdeal.rule.engine.entity.RuleParams;
import com.snapdeal.rule.engine.entity.ValueTypes;
import com.snapdeal.rule.engine.services.IRulesService;

/**
 *  
 *  @version     1.0, 21-Jul-2014
 *  @author ankur
 */


@Service("giftWrapRuleWrapperService")
public class GiftWrapRuleWrapperServiceImpl implements IGiftWrapRuleWrapperService  {
    
    private static final Logger LOG = LoggerFactory.getLogger(GiftWrapRuleWrapperServiceImpl.class);

    
    @Autowired
    private IRulesService       ruleService;
    
    @Autowired
    private IRulesService       ruleDao;


    @Autowired
    private IGenericMao         genericMao;
    
    
    @Transactional
    @Override
    public Rule saveServiceabilityRules(GiftWrapRuleUpdateDTO dto){
      
        CocofsRuleUnit unit = getRuleUnitFromGiftWrapRuleUpdateDTO(dto);
        Rule entity = getRuleFromRuleCreationDTO(dto, dto.getName() );
        
        ruleService.createRule(entity);
        genericMao.saveDocument(unit);
        return entity;
    }
    
    @Transactional
    @Override
    public RuleUpdateResponse upadteGiftWrapRules(GiftWrapRuleUpdateDTO dto) {
        List<CocofsRuleUnit> ruleUnits = getRuleUnitsForCategoryAndType(dto.getRuleCode());
        List<Rule> rList = getGiftWrapRulesByCode(dto.getRuleCode());
        Rule ruleToEdit = null;
        CocofsRuleUnit ruleUnitToEdit = null;
        if (null != rList) {
            for ( Rule r : rList) {
                if (dto.getName().equals(r.getName())) {
                    ruleToEdit = r;
                }
            }
        }
        if (null != ruleUnits) {
            for (CocofsRuleUnit ruleUnit : ruleUnits) {
                if (dto.getName().equals(ruleUnit.getName())) {
                    ruleUnitToEdit = ruleUnit;
                }
            }
        }
        
        RuleUpdateResponse resp = new RuleUpdateResponse();
        resp.setSuccessful(false);
        if ((null == ruleUnitToEdit) && (null == ruleToEdit)) {
            String msg = "No enabled rule found with category " + dto.getParamValue() + " and rulename " + dto.getName();
            LOG.info(msg);
            resp.setFailureReason(msg);
        } else if (((null != ruleUnitToEdit) && (null == ruleToEdit)) || ((null == ruleUnitToEdit) && (null != ruleToEdit))) {
            String msg = "Mongo and DB out of sync for category " + dto.getParamValue() + " and rulename " + dto.getName();
            LOG.info(msg);
            resp.setFailureReason(msg);
        } else {
            updateRuleUnitWithRuleEditDTO(dto, ruleUnitToEdit);
            updateRuleWithRuleEditDTO(ruleToEdit, dto);

            ruleService.updateRule(ruleToEdit);
            genericMao.saveDocument(ruleUnitToEdit);
            resp.setSuccessful(true);
        }

        return resp;
    }
    
    private List<CocofsRuleUnit> getRuleUnitsForCategoryAndType(String ruleCode) {
        List<com.snapdeal.cocofs.mongo.Criteria> queryCriterias = new ArrayList<com.snapdeal.cocofs.mongo.Criteria>();
        com.snapdeal.cocofs.mongo.Criteria mqcForRuleCode = new com.snapdeal.cocofs.mongo.Criteria(CocofsRuleUnitFieldName.RULE_CODE.getName(), ruleCode, Operator.EQUAL);
        queryCriterias.add(mqcForRuleCode);
        List<CocofsRuleUnit> ruleUnit = genericMao.getDocumentList(new CocofsMongoQuery(queryCriterias), CocofsRuleUnit.class);
        if (null != ruleUnit ) {
            return ruleUnit;
        }
        
        return null;
    }
    
    @Transactional(readOnly = true)
    @Override
    public List<Rule> getGiftWrapRulesByCode(String ruleCode) {
        List<Rule> rules = ruleService.getRuleByCode(ruleCode);
        for (Rule r : rules) {
            if (!Hibernate.isInitialized(r.getCriterion())) {
                Hibernate.initialize(r.getCriterion());
            }
            if (!Hibernate.isInitialized(r.getRuleParams())) {
                Hibernate.initialize(r.getRuleParams());
            }

        }
        return rules;
    }
    
  
    

    private CocofsRuleUnit getRuleUnitFromGiftWrapRuleUpdateDTO(GiftWrapRuleUpdateDTO dto) {
        CocofsRuleUnit unit = new CocofsRuleUnit();
        unit.setCreated(dto.getCreated());
        List<CriteriaUnit> criteria = getRuleCriteriaUnitFromRuleCreationDTO(dto);
        unit.setCriteria(criteria);
        unit.setEnabled(true);
        unit.setEndDate(dto.getEndDate());
        unit.setRuleCode(dto.getRuleCode());
        unit.setParamValue(dto.getParamValue());
        unit.setName(dto.getName());
        unit.setPriority(0);
        unit.setStartDate(dto.getStartDate());
        unit.setType(getAppropriateRuleType(dto.getBlockName()));
        unit.setUpdated(DateUtils.getCurrentTime());
        unit.setId(RandomUtils.nextInt());
        return unit;
    }
    
    private List<CriteriaUnit> getRuleCriteriaUnitFromRuleCreationDTO(GiftWrapRuleUpdateDTO dto) {
        List<CriteriaUnit> list = new ArrayList<CriteriaUnit>();

        for(CriteriaDTO criteriaDTO : dto.getCriterion()){
            CriteriaUnit unit = new CriteriaUnit();
            unit.setCreated(criteriaDTO.getCreated() != null ? criteriaDTO.getCreated() : new Date());
            unit.setEnabled(true);
            unit.setName(criteriaDTO.getName());
            unit.setOperator(criteriaDTO.getOperator().getCode());
            unit.setUpdated(DateUtils.getCurrentTime());
            unit.setValue(getAppropriateCriteriaValue(criteriaDTO));
            unit.setValueType(criteriaDTO.getValueType().name());
            unit.setId(RandomUtils.nextInt());
            list.add(unit);

        }
        return list;
    }
    
    private static String getAppropriateCriteriaValue(CriteriaDTO criteriaDTO) {
        StringBuilder sb = new StringBuilder();

        if (criteriaDTO.getValues() != null && !criteriaDTO.getValues().isEmpty()) {

            for (Object o : criteriaDTO.getValues()) {
                sb.append(o).append(",");
            }

            return sb.toString().substring(0, sb.toString().lastIndexOf(","));

        } else if (criteriaDTO.getValue() != null) {

            return String.valueOf(criteriaDTO.getValue());
        }

        return null;
    }
    
    
    
    private static String getAppropriateRuleType(BlockName blockName) {
        switch (blockName) {

            case GiftWrapCategoryRuleblock:
                return CocofsRuleUnit.CocofsRuleUnitType.GIFTWRAP_CATEGORY.getCode();

            
        }
        return null;
    }
    
    
    private Rule getRuleFromRuleCreationDTO(GiftWrapRuleUpdateDTO dto, String ruleName) {
        Rule r = new Rule();
        r.setBlockId(CacheManager.getInstance().getCache(BlocksCache.class).getBlockIDByName(dto.getBlockName().name()));

        r.setCreated(dto.getCreated() != null ? dto.getCreated() : new Date());
        r.setCreatedBy(dto.getUpdatedBy());
        Set<Criteria> criterion = getRuleCriteriaFromRuleCreationDTO(dto);
        r.setCriterion(criterion);
        r.setEnabled(true);
        r.setEndDate(dto.getEndDate());
        r.setName(dto.getName());
        r.setCode(dto.getRuleCode());
        r.setPriority(0);
        r.setRemark("Creating rule for " + dto.getBlockName() + " and " + dto.getParamValue() + " by user " + dto.getUpdatedBy());
        Set<RuleParams> ruleParams = getRuleParamFromRuleCreationDTO(dto, r);
        r.setRuleParams(ruleParams);
        r.setStartDate(dto.getStartDate());
        r.setUpdated(DateUtils.getCurrentTime());
        r.setUpdatedBy(dto.getUpdatedBy());
        r.setSynched(true);
        return r;
    }
    
    private Set<Criteria> getRuleCriteriaFromRuleCreationDTO(GiftWrapRuleUpdateDTO dto) {
        Set<Criteria> criteria = new HashSet<Criteria>();

        for(CriteriaDTO criteriaDTO : dto.getCriterion()){
            Criteria crit = new Criteria();
            crit.setCreated(criteriaDTO.getCreated() != null ? criteriaDTO.getCreated() : new Date());
            crit.setEnabled(true);
            crit.setName(criteriaDTO.getName());
            crit.setOperator(criteriaDTO.getOperator());
            crit.setValue(getAppropriateCriteriaValue(criteriaDTO));
            crit.setValueType(criteriaDTO.getValueType());
            //crit.setId(RandomUtils.nextInt());
            criteria.add(crit);

        }
        return criteria;
    }
    
    private Set<RuleParams> getRuleParamFromRuleCreationDTO(GiftWrapRuleUpdateDTO dto, Rule r) {
        Set<RuleParams> params = new HashSet<RuleParams>();
        RuleParams p = new RuleParams();
        p.setParamValue(dto.getParamValue());
        p.setParamName(CocofsRuleParam.CATEGORY_URL.getName());
        p.setRule(r);
        params.add(p);
        return params;
    }
    
    @Override
    public GiftWrapCategoryRuleEditForm getGiftWrapRuleEditDTOFromRule(Rule rule) {
        GiftWrapCategoryRuleEditForm dto = new GiftWrapCategoryRuleEditForm();
        for ( RuleParams p : rule.getRuleParams()) {
            if ( p.getParamName().equals(CocofsRuleParam.CATEGORY_URL.getName())) {
                dto.setCategoryUrl(p.getParamValue());
            }
        }
        dto.setName(rule.getName());
        dto.setEnabled(rule.isEnabled());
        dto.setRuleCode(rule.getCode());
        Date ed = rule.getEndDate();
        if (null != ed) {
            dto.setEndDate(DateUtils.dateToString(ed, "yyyy-MM-dd"));

        }
        Date sd = rule.getStartDate();
        if (null != sd) {
            dto.setStartDate(DateUtils.dateToString(sd, "yyyy-MM-dd"));
        }
        Criteria crit = findCriteria(rule.getCriterion(), GiftWrapCategoryServiceabilityRuleCriteria.FRAGILE.getCode());
        if ( null != crit ) {
            dto.setFragile(crit.getValue());
        }
        crit = findCriteria(rule.getCriterion(), GiftWrapCategoryServiceabilityRuleCriteria.PRICE.getCode());
    
        if ( null != crit ) {
            String[] values = crit.getValue().split(",");
            dto.setPrice(Double.valueOf(values[0]));
            if (Operators.BETWEEN == crit.getOperator() || Operators.NOT_BETWEEN == crit.getOperator()) {
                dto.setPriceTo(Double.valueOf(values[1]));
            }
            dto.setPriceOperator(crit.getOperator().getCode());
        }
       
        crit = findCriteria(rule.getCriterion(), GiftWrapCategoryServiceabilityRuleCriteria.WEIGHT.getCode());
        if ( null != crit ) {
            String[] values = crit.getValue().split(",");
            dto.setWeight(Double.valueOf(values[0]));
            if (Operators.BETWEEN == crit.getOperator() || Operators.NOT_BETWEEN == crit.getOperator()) {
                dto.setWeightTo(Double.valueOf(values[1]));
            }
            dto.setWeightOperator(crit.getOperator().getCode());
        }
      
        crit = findCriteria(rule.getCriterion(), GiftWrapCategoryServiceabilityRuleCriteria.VOLUME.getCode());
        if ( null != crit ) {
            String[] values = crit.getValue().split(",");
            dto.setVolume(Double.valueOf(values[0]));
            if (Operators.BETWEEN == crit.getOperator() || Operators.NOT_BETWEEN == crit.getOperator()) {
                dto.setVolumeTo(Double.valueOf(values[1]));
            }
            dto.setVolumeOperator(crit.getOperator().getCode());
        }
        
        crit = findCriteria(rule.getCriterion(), GiftWrapCategoryServiceabilityRuleCriteria.DANGEROUSGOODSTYPE.getCode());
        if (null != crit) {
            String[] values = crit.getValue().split(",");
            if (values.length > 0) {
                dto.setDangerousGoodsType(Arrays.asList(values));
            }
        }
        
        dto.setRuleType(CocofsRuleUnitType.GIFTWRAP_CATEGORY.getCode());
        dto.setUpdatedBy(rule.getUpdatedBy());
        dto.setUpdateTime(rule.getUpdated());
        
        return dto;
    }
    
    private Criteria findCriteria(Set<Criteria> criterion, String name) {
        for ( Criteria c : criterion) {
            if ( name.equals(c.getName())) {
                return c;
            }
        }
        return null;
    }
    
    private CriteriaUnit findCriteria(List<CriteriaUnit> criteria, String name) {
        for ( CriteriaUnit c : criteria) {
            if ( name.equals(c.getName())) {
                return c;
            }
        }
        return null;
    }
    
    private CriteriaDTO findCriteriainDTO(Set<CriteriaDTO> criterion, String name) {
        for ( CriteriaDTO c : criterion) {
            if ( name.equals(c.getName())) {
                return c;
            }
        }
        return null;
    }
    
    private void updateRuleUnitWithRuleEditDTO(GiftWrapRuleUpdateDTO dto, CocofsRuleUnit unit) {
        if ( dto.getEnabled()) {
            updateRuleCriteriaUnitWithRuleEditDTO(dto, unit.getCriteria());
        }
        
        
        unit.setEnabled(dto.getEnabled());
        unit.setEndDate(dto.getEndDate());
        unit.setStartDate(dto.getStartDate());
        unit.setUpdated(DateUtils.getCurrentTime());
        
    }
    
    private void updateRuleCriteriaUnitWithRuleEditDTO(GiftWrapRuleUpdateDTO dto, List<CriteriaUnit> criteria) {
        CriteriaUnit origCrit = findCriteria(criteria, GiftWrapCategoryServiceabilityRuleCriteria.FRAGILE.getCode());
        CriteriaDTO  newCrit = findCriteriainDTO(dto.getCriterion(), GiftWrapCategoryServiceabilityRuleCriteria.FRAGILE.getCode());
        if ((null != newCrit) && (null == origCrit)) {
            CriteriaUnit unit= getFragileCriteriaUnit(newCrit);
            criteria.add(unit);
        } else if ( (null == newCrit ) && ( null != origCrit))  {
            criteria.remove(origCrit);
        } else if ( (null != newCrit ) && ( null != origCrit)) {
            origCrit.setValue(newCrit.getValue().toString());
            
        }
        
        origCrit = findCriteria(criteria, GiftWrapCategoryServiceabilityRuleCriteria.PRICE.getCode());
        newCrit = findCriteriainDTO(dto.getCriterion(), GiftWrapCategoryServiceabilityRuleCriteria.PRICE.getCode());
        if ((null != newCrit) && (null == origCrit)) {
            CriteriaUnit unit= getPriceCriteriaUnit(newCrit);
            criteria.add(unit);
           
        } else if ( (null == newCrit ) && ( null != origCrit))  {
            criteria.remove(origCrit);
        } else if ( (null != newCrit ) && ( null != origCrit)) {
            origCrit.setValue(getAppropriateCriteriaValue(newCrit));
            origCrit.setOperator(newCrit.getOperator().getCode());
            origCrit.setEnabled(newCrit.getEnabled());
            origCrit.setUpdated(DateUtils.getCurrentTime());
            
        }
        
        origCrit = findCriteria(criteria, GiftWrapCategoryServiceabilityRuleCriteria.WEIGHT.getCode());
        newCrit = findCriteriainDTO(dto.getCriterion(), GiftWrapCategoryServiceabilityRuleCriteria.WEIGHT.getCode());
        if ((null != newCrit) && (null == origCrit)) {
            CriteriaUnit unit= getWeightCriteriaUnit(newCrit);
            criteria.add(unit);
           
        } else if ( (null == newCrit ) && ( null != origCrit))  {
            criteria.remove(origCrit);
        } else if ( (null != newCrit ) && ( null != origCrit)) {
            origCrit.setValue(getAppropriateCriteriaValue(newCrit));
            origCrit.setOperator(newCrit.getOperator().getCode());
            origCrit.setEnabled(newCrit.getEnabled());
            origCrit.setUpdated(DateUtils.getCurrentTime());

        }
        
        origCrit = findCriteria(criteria, GiftWrapCategoryServiceabilityRuleCriteria.VOLUME.getCode());
        newCrit = findCriteriainDTO(dto.getCriterion(), GiftWrapCategoryServiceabilityRuleCriteria.VOLUME.getCode());
        if ((null != newCrit) && (null == origCrit)) {
            CriteriaUnit unit= getVolumeCriteriaUnit(newCrit);
            criteria.add(unit);
           
        } else if ( (null == newCrit ) && ( null != origCrit))  {
            criteria.remove(origCrit);
        } else if ( (null != newCrit ) && ( null != origCrit)) {
            origCrit.setValue(getAppropriateCriteriaValue(newCrit));
            origCrit.setOperator(newCrit.getOperator().getCode());
            origCrit.setEnabled(newCrit.getEnabled());
            origCrit.setUpdated(DateUtils.getCurrentTime());

        }
        
        origCrit = findCriteria(criteria, GiftWrapCategoryServiceabilityRuleCriteria.DANGEROUSGOODSTYPE.getCode());
        newCrit = findCriteriainDTO(dto.getCriterion(), GiftWrapCategoryServiceabilityRuleCriteria.DANGEROUSGOODSTYPE.getCode());
        if ((null != newCrit) && (null == origCrit)) {
            CriteriaUnit unit= getDangerousGoodsTypeCriteriaUnit(newCrit);
            criteria.add(unit);
           
        } else if ( (null == newCrit ) && ( null != origCrit))  {
            criteria.remove(origCrit);
        } else if ( (null != newCrit ) && ( null != origCrit)) {
            origCrit.setValue(getAppropriateCriteriaValue(newCrit));
            origCrit.setEnabled(newCrit.getEnabled());
            origCrit.setUpdated(DateUtils.getCurrentTime());

        }
               
        
    }
    
    private static CriteriaUnit getFragileCriteriaUnit(CriteriaDTO critDTO) {
        CriteriaUnit fragileCriteriaUnit = new CriteriaUnit();
        fragileCriteriaUnit.setName(GiftWrapCategoryServiceabilityRuleCriteria.FRAGILE.getCode());
        fragileCriteriaUnit.setOperator(Operators.EQUAL_TO.getCode());
        fragileCriteriaUnit.setValue(getAppropriateCriteriaValue(critDTO));
        fragileCriteriaUnit.setValueType(ValueTypes.BOOLEAN.name());
        fragileCriteriaUnit.setId(RandomUtils.nextInt());
        fragileCriteriaUnit.setCreated(DateUtils.getCurrentTime());
        fragileCriteriaUnit.setUpdated(DateUtils.getCurrentTime());

        fragileCriteriaUnit.setEnabled(true);
        return fragileCriteriaUnit;
    }
    
    private static CriteriaUnit getPriceCriteriaUnit(CriteriaDTO  critDTO) {
        CriteriaUnit unit = new CriteriaUnit();
        unit.setName(GiftWrapCategoryServiceabilityRuleCriteria.PRICE.getCode());
        unit.setOperator(critDTO.getOperator().getCode());
        unit.setValueType(ValueTypes.DOUBLE.name());
        unit.setValue(getAppropriateCriteriaValue(critDTO));
        unit.setId(RandomUtils.nextInt());
        unit.setCreated(DateUtils.getCurrentTime());
        unit.setUpdated(DateUtils.getCurrentTime());
        unit.setEnabled(true);
        
        return unit;
    }

    private static CriteriaUnit getVolumeCriteriaUnit(CriteriaDTO  critDTO) {
        CriteriaUnit unit = new CriteriaUnit();
        unit.setName(GiftWrapCategoryServiceabilityRuleCriteria.VOLUME.getCode());
        unit.setOperator(critDTO.getOperator().getCode());
        unit.setValueType(ValueTypes.DOUBLE.name());
        unit.setValue(getAppropriateCriteriaValue(critDTO));
        unit.setId(RandomUtils.nextInt());
        unit.setCreated(DateUtils.getCurrentTime());
        unit.setUpdated(DateUtils.getCurrentTime());
        unit.setEnabled(true);
        
        return unit;
    }

    private static CriteriaUnit getWeightCriteriaUnit(CriteriaDTO  critDTO) {
        CriteriaUnit unit = new CriteriaUnit();
        unit.setName(GiftWrapCategoryServiceabilityRuleCriteria.WEIGHT.getCode());
        unit.setOperator(critDTO.getOperator().getCode());
        unit.setValueType(ValueTypes.DOUBLE.name());
        unit.setValue(getAppropriateCriteriaValue(critDTO));
        unit.setId(RandomUtils.nextInt());
        unit.setCreated(DateUtils.getCurrentTime());
        unit.setUpdated(DateUtils.getCurrentTime());
        unit.setEnabled(true);
        return unit;
    }
    
    private static CriteriaUnit getDangerousGoodsTypeCriteriaUnit(CriteriaDTO  critDTO) {
        CriteriaUnit unit = new CriteriaUnit();
        unit.setName(GiftWrapCategoryServiceabilityRuleCriteria.DANGEROUSGOODSTYPE.getCode());
        unit.setOperator(critDTO.getOperator().getCode());
        unit.setValueType(ValueTypes.STRING.name());
        unit.setValue(getAppropriateCriteriaValue(critDTO));
        unit.setId(RandomUtils.nextInt());
        unit.setCreated(DateUtils.getCurrentTime());
        unit.setUpdated(DateUtils.getCurrentTime());
        unit.setEnabled(true);
        
        return unit;
    }
    
   
    
    private void updateRuleWithRuleEditDTO(Rule r, GiftWrapRuleUpdateDTO dto ) {

        if (dto.getEnabled()) {
            // Only update criteria only if rule is not being disabled
            updateRuleCriteriaFromRuleEditDTO(dto, r.getCriterion());
            for (Criteria rc : r.getCriterion()) {
                if (null == rc.getRule()) {
                    rc.setRule(r);
                }
            }
        }
        r.setEnabled(dto.getEnabled());
        r.setEndDate(dto.getEndDate());
        r.setPriority(0);
        r.setRemark("Updating rule for " + dto.getName() + " and " + dto.getParamValue() + " by user " + dto.getUpdatedBy());
        r.setStartDate(dto.getStartDate());
        r.setUpdated(DateUtils.getCurrentTime());
        r.setUpdatedBy(dto.getUpdatedBy());
        
    }
    
    private void updateRuleCriteriaFromRuleEditDTO(GiftWrapRuleUpdateDTO dto, Set<Criteria> criterion) {
        List<Criteria> criteriaToDelete = new ArrayList<Criteria>();
        List<Criteria> criteriaToAdd = new ArrayList<Criteria>();
       
        
        Criteria origCrit = findCriteria(criterion, GiftWrapCategoryServiceabilityRuleCriteria.FRAGILE.getCode());
        CriteriaDTO  newCrit = findCriteriainDTO(dto.getCriterion(), GiftWrapCategoryServiceabilityRuleCriteria.FRAGILE.getCode());     
        if ((null != newCrit ) && ( null == origCrit) ) {
            Criteria crit= getFragileCriteria(newCrit);
            criteriaToAdd.add(crit);
        } else if ( (null == newCrit ) && ( null != origCrit))  {
            criterion.remove(origCrit);
            criteriaToDelete.add(origCrit);
        } else if ( (null != newCrit ) && ( null != origCrit)) {
            origCrit.setValue(newCrit.getValue().toString());
            origCrit.setEnabled(newCrit.getEnabled());
            
        }
        
        
        origCrit = findCriteria(criterion, GiftWrapCategoryServiceabilityRuleCriteria.PRICE.getCode());
        newCrit = findCriteriainDTO(dto.getCriterion(), GiftWrapCategoryServiceabilityRuleCriteria.PRICE.getCode());     
        if ((null != newCrit ) && ( null == origCrit) ) {
            Criteria crit= getPriceCriteria(newCrit);
            criteriaToAdd.add(crit);
        } else if ( (null == newCrit ) && ( null != origCrit))  {
            criterion.remove(origCrit);
            criteriaToDelete.add(origCrit);
        } else if ( (null != newCrit ) && ( null != origCrit)) {
            origCrit.setValue(getAppropriateCriteriaValue(newCrit));
            origCrit.setOperator(newCrit.getOperator());
            origCrit.setEnabled(newCrit.getEnabled());
            
        }
        
        
        origCrit = findCriteria(criterion, GiftWrapCategoryServiceabilityRuleCriteria.WEIGHT.getCode());
        newCrit = findCriteriainDTO(dto.getCriterion(), GiftWrapCategoryServiceabilityRuleCriteria.WEIGHT.getCode());     
        if ((null != newCrit ) && ( null == origCrit) ) {
            Criteria crit= getWeightCriteria(newCrit);
            criteriaToAdd.add(crit);
        } else if ( (null == newCrit ) && ( null != origCrit))  {
            criterion.remove(origCrit);
            criteriaToDelete.add(origCrit);
        } else if ( (null != newCrit ) && ( null != origCrit)) {
            origCrit.setValue(getAppropriateCriteriaValue(newCrit));
            origCrit.setOperator(newCrit.getOperator());
            origCrit.setEnabled(newCrit.getEnabled());

        }
        
        origCrit = findCriteria(criterion, GiftWrapCategoryServiceabilityRuleCriteria.VOLUME.getCode());
        newCrit = findCriteriainDTO(dto.getCriterion(), GiftWrapCategoryServiceabilityRuleCriteria.VOLUME.getCode());     
        if ((null != newCrit ) && ( null == origCrit) ) {
            Criteria crit= getVolumeCriteria(newCrit);
            criteriaToAdd.add(crit);
        } else if ( (null == newCrit ) && ( null != origCrit))  {
            criterion.remove(origCrit);
            criteriaToDelete.add(origCrit);
        } else if ( (null != newCrit ) && ( null != origCrit)) {
            origCrit.setValue(getAppropriateCriteriaValue(newCrit));
            origCrit.setOperator(newCrit.getOperator());            
            origCrit.setEnabled(newCrit.getEnabled());

        }
        
        origCrit = findCriteria(criterion, GiftWrapCategoryServiceabilityRuleCriteria.DANGEROUSGOODSTYPE.getCode());
        newCrit = findCriteriainDTO(dto.getCriterion(), GiftWrapCategoryServiceabilityRuleCriteria.DANGEROUSGOODSTYPE.getCode());     
        if ((null != newCrit ) && ( null == origCrit) ) {
            Criteria crit= getDangerousGoodsTypeCriteria(newCrit);
            criteriaToAdd.add(crit);
        } else if ( (null == newCrit ) && ( null != origCrit))  {
            criterion.remove(origCrit);
            criteriaToDelete.add(origCrit);
        } else if ( (null != newCrit ) && ( null != origCrit)) {
            origCrit.setValue(getAppropriateCriteriaValue(newCrit));
            origCrit.setEnabled(newCrit.getEnabled());

        }
        
               
        if ( !criteriaToDelete.isEmpty()) {
            //ruleService.updateRule(criteriaToDelete.get(0).getRule());
              for ( Criteria cr : criteriaToDelete ) {
                  ruleService.deleteCriteriaById(cr.getId());
              }
          }
          
          criterion.addAll(criteriaToAdd);
          
          
    }
    
    private static Criteria getFragileCriteria(CriteriaDTO critDTO) {
        Criteria fragileCriteriaUnit = new Criteria();
        fragileCriteriaUnit.setName(GiftWrapCategoryServiceabilityRuleCriteria.FRAGILE.getCode());
        fragileCriteriaUnit.setOperator(Operators.EQUAL_TO);
        fragileCriteriaUnit.setValue(getAppropriateCriteriaValue(critDTO));
        fragileCriteriaUnit.setValueType(ValueTypes.BOOLEAN);
        fragileCriteriaUnit.setCreated(DateUtils.getCurrentTime());
        fragileCriteriaUnit.setEnabled(true);
        return fragileCriteriaUnit;
    }
    
    private static Criteria getPriceCriteria(CriteriaDTO  critDTO) {
        Criteria unit = new Criteria();
        unit.setName(GiftWrapCategoryServiceabilityRuleCriteria.PRICE.getCode());
        unit.setOperator(critDTO.getOperator());
        unit.setValueType(ValueTypes.DOUBLE);
        unit.setValue(getAppropriateCriteriaValue(critDTO));
        unit.setCreated(DateUtils.getCurrentTime());
        unit.setEnabled(true);
        
        return unit;
    }

    private static Criteria getVolumeCriteria(CriteriaDTO  critDTO) {
        Criteria unit = new Criteria();
        unit.setName(GiftWrapCategoryServiceabilityRuleCriteria.VOLUME.getCode());
        unit.setOperator(critDTO.getOperator());
        unit.setValueType(ValueTypes.DOUBLE);
        unit.setValue(getAppropriateCriteriaValue(critDTO));
        unit.setCreated(DateUtils.getCurrentTime());
        unit.setEnabled(true);
        
        return unit;
    }

    private static Criteria getWeightCriteria(CriteriaDTO  critDTO) {
        Criteria unit = new Criteria();
        unit.setName(GiftWrapCategoryServiceabilityRuleCriteria.WEIGHT.getCode());
        unit.setOperator(critDTO.getOperator());
        unit.setValueType(ValueTypes.DOUBLE);
        unit.setValue(getAppropriateCriteriaValue(critDTO));
        unit.setCreated(DateUtils.getCurrentTime());
        unit.setEnabled(true);
        return unit;
    }
    
    private static Criteria getDangerousGoodsTypeCriteria(CriteriaDTO  critDTO) {
        Criteria unit = new Criteria();
        unit.setName(GiftWrapCategoryServiceabilityRuleCriteria.DANGEROUSGOODSTYPE.getCode());
        unit.setOperator(critDTO.getOperator());
        unit.setValueType(ValueTypes.STRING);
        unit.setValue(getAppropriateCriteriaValue(critDTO));
        unit.setCreated(DateUtils.getCurrentTime());
        unit.setEnabled(true);
        
        return unit;
    }
    
    public static  CocofsRuleUnit getGiftWrapRuleUnitFromRule(Rule rule) {
        CocofsRuleUnit unit = new CocofsRuleUnit();
        for ( RuleParams p : rule.getRuleParams()) {
            if ( p.getParamName().equals(CocofsRuleParam.CATEGORY_URL.getName())) {
                unit.setCategoryUrl(p.getParamValue());
                unit.setParamValue(p.getParamValue());
            }
        }
        unit.setId(rule.getId());
        unit.setCreated(rule.getCreated());
        List<CriteriaUnit> criteria = getRuleCriteriaUnitFromRuleEntity(rule);
        unit.setCriteria(criteria);
        unit.setEnabled(rule.isEnabled());
        unit.setEndDate(rule.getEndDate());
        unit.setRuleCode(rule.getCode());
        unit.setName(rule.getName());
        unit.setPriority(rule.getPriority());
        unit.setStartDate(rule.getStartDate());
        unit.setType(getAppropriateRuleType(BlockName.GiftWrapCategoryRuleblock));
        unit.setUpdated(DateUtils.getCurrentTime());
        return unit;
    }
    
    private static List<CriteriaUnit> getRuleCriteriaUnitFromRuleEntity(Rule rule) {
        List<CriteriaUnit> list = new ArrayList<CriteriaUnit>();

        for(Criteria criteria : rule.getCriterion()){
            CriteriaUnit unit = new CriteriaUnit();
            unit.setId(criteria.getId());
            unit.setCreated(criteria.getCreated());
            unit.setEnabled(true);
            unit.setName(criteria.getName());
            unit.setOperator(criteria.getOperator().getCode());
            unit.setUpdated(DateUtils.getCurrentTime());
            unit.setValue(criteria.getValue());
            unit.setValueType(criteria.getValueType().name());
            list.add(unit);

        }
        return list;
    }
    
    
    
}
