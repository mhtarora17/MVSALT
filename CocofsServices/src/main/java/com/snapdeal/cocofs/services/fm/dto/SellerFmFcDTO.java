package com.snapdeal.cocofs.services.fm.dto;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.collections.CollectionUtils;

import com.snapdeal.cocofs.core.aerospike.vo.SellerFMMappingVO;
import com.snapdeal.cocofs.entity.SellerFCCodeMapping;
import com.snapdeal.cocofs.entity.SellerFMMapping;

@XmlRootElement
public class SellerFmFcDTO {

	private String sellerCode;

	private String fulfilmentModel;

	private boolean supcExist;

	private boolean subcatExist;

	private boolean enabled;
	
	private List<String> enabledFCCenters = new ArrayList<>();

	public SellerFmFcDTO(){
		
	}
	
	public SellerFmFcDTO(SellerFMMapping entity) {
		this.sellerCode = entity.getSellerCode();
		this.fulfilmentModel = entity.getFulfilmentModel();
		this.supcExist = entity.isSupcExist();
		this.subcatExist = entity.isSubcatExist();
		this.enabled = entity.isEnabled();
		
		if (CollectionUtils.isNotEmpty(entity.getFcCenters())) {
			for (SellerFCCodeMapping fcMapping : entity.getFcCenters()) {
				if (fcMapping.isEnabled()) {
					this.getEnabledFCCenters().add(fcMapping.getFcCode());
				}
			}
		}
	}
	
	
	public SellerFmFcDTO(SellerFMMappingVO sellerFMMappingVO) {
		this.sellerCode = sellerFMMappingVO.getSellerCode();
		this.fulfilmentModel = sellerFMMappingVO.getFulfillmentModel();
		this.supcExist = sellerFMMappingVO.isSupcExist();
		this.subcatExist = sellerFMMappingVO.isSubcatExist();
		this.enabled = sellerFMMappingVO.isEnabled();
		
		if (CollectionUtils.isNotEmpty(sellerFMMappingVO.getFcCenters())) {
			for (String fcCode : sellerFMMappingVO.getFcCenters()) {
				this.getEnabledFCCenters().add(fcCode);
			}
		}
	}

	public String getSellerCode() {
		return sellerCode;
	}

	public void setSellerCode(String sellerCode) {
		this.sellerCode = sellerCode;
	}

	public String getFulfilmentModel() {
		return fulfilmentModel;
	}

	public void setFulfilmentModel(String fulfilmentModel) {
		this.fulfilmentModel = fulfilmentModel;
	}

	public boolean isSupcExist() {
		return supcExist;
	}

	public void setSupcExist(boolean supcExist) {
		this.supcExist = supcExist;
	}

	public boolean isSubcatExist() {
		return subcatExist;
	}

	public void setSubcatExist(boolean subcatExist) {
		this.subcatExist = subcatExist;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public List<String> getEnabledFCCenters() {
		return enabledFCCenters;
	}

	public void setEnabledFCCenters(List<String> enabledFCCenters) {
		this.enabledFCCenters = enabledFCCenters;
	}

	@Override
	public String toString() {
		return "SellerFmFcDTO [sellerCode=" + sellerCode + ", fulfilmentModel=" + fulfilmentModel + ", supcExist="
				+ supcExist + ", subcatExist=" + subcatExist + ", enabled=" + enabled + ", enabledFCCenters="
				+ enabledFCCenters + "]";
	}
	
	

}
