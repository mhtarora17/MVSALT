/*
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 15-Jun-2015
 *  @author gaurav
 */
package com.snapdeal.cocofs.services;

import com.snapdeal.cocofs.entity.SellerContactDetails;
import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.generic.persister.exception.GenericPersisterException;
import com.snapdeal.cocofs.services.fm.dto.AddressDTO;
import com.snapdeal.cocofs.sro.AddressSRO;

public interface ISellerDetailsService {

    public SellerContactDetails createUpdateSellerContactDetails(SellerContactDetails scd) throws GenericPersisterException, ExternalDataNotFoundException;
	
	public SellerContactDetails getSellerContactDetailsBySeller(String sellerCode);

    SellerContactDetails persistSellerContactDetails(SellerContactDetails scd) throws GenericPersisterException;

    AddressDTO getSellerDetailsFromBaseSource(String sellerCode);

    AddressSRO getSellerDetails(String sellerCode);

}

