/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 26-Nov-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.services.fm.dto;

public class SellerDto {

    private String sellerCode;

    public SellerDto() {
    }

    public SellerDto(String sellerCode) {
        this.sellerCode = sellerCode;
    }

    public String getSellerCode() {
        return sellerCode;
    }

    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }

    @Override
    public String toString() {
        return "SellerDto [sellerCode=" + sellerCode + "]";
    }

}
