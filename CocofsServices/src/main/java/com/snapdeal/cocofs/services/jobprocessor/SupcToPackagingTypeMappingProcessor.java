/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.services.jobprocessor;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.naming.OperationNotSupportedException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.snapdeal.base.utils.CollectionUtils;
import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.cocofs.access.IProductAttributeService;
import com.snapdeal.cocofs.common.BulkUploadValidationDTO;
import com.snapdeal.cocofs.common.BulkUploadValidationResponse;
import com.snapdeal.cocofs.common.JobProcessorResponse;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.entity.JobDetail;
import com.snapdeal.cocofs.entity.SupcPackagingTypeMapping;
import com.snapdeal.cocofs.enums.BulkUploadValidationError;
import com.snapdeal.cocofs.enums.JobActionCode;
import com.snapdeal.cocofs.excelupload.SupcPackagingTypeUploadDTO;
import com.snapdeal.cocofs.generic.persister.response.GenericPersisterResponse;
import com.snapdeal.cocofs.mongo.model.SupcPackagingTypeMappingUnit;
import com.snapdeal.cocofs.services.IDataReader;
import com.snapdeal.cocofs.services.IDataUpdater;
import com.snapdeal.cocofs.services.IJobProcessor;
import com.snapdeal.cocofs.services.IJobSchedulerService;
import com.snapdeal.cocofs.services.ISupcToPackagingTypeMappingService;
import com.snapdeal.cocofs.services.common.dto.UserInfo;
import com.snapdeal.cocofs.services.data.engine.IDataEngine;
import com.snapdeal.cocofs.utils.ExcelReader;
import com.snapdeal.cocofs.utils.FTPUtils;
import com.snapdeal.packman.dao.IPackmanExclusivelyDao;
import com.snapdeal.packman.entity.PackagingType;
import com.sun.media.sound.InvalidDataException;

/**
 * @version 1.0, 10-Dec-2015
 * @author vikassharma03
 */
@Service("supcPackagingTypeMappingProcess")
public class SupcToPackagingTypeMappingProcessor implements IJobProcessor {

    private static final Logger                                                                             LOG = LoggerFactory.getLogger(
            SupcToPackagingTypeMappingProcessor.class);

    @Autowired
    private IJobSchedulerService                                                                            jobService;

    @Autowired
    private IPackmanExclusivelyDao                                                                          packmanDao;

    @Autowired
    private IDataUpdater                                                                                    dataUpdater;

    @Autowired
    @Qualifier("SupcPackagingTypeMappingDataReader")
    private IDataReader<SupcPackagingTypeUploadDTO, SupcPackagingTypeMapping, SupcPackagingTypeMappingUnit> supcPackagingTypeMappingDataReader;

    @Autowired
    @Qualifier("SupcPackagingTypeMappingDataEngine")
    private IDataEngine<SupcPackagingTypeUploadDTO, SupcPackagingTypeMapping, SupcPackagingTypeMappingUnit> supcPackagingTypeMappingDataEngine;

    @Autowired
    private ISupcToPackagingTypeMappingService                                                              supcToPackagingTypeMappingService;

    @Autowired
    private IProductAttributeService                                                                        productAttributeService;

    private IDependency                                                                                     dep = new Dependency();

    @PostConstruct
    public void init() {
        // jobService.addJobProcessor(JobActionCode.P_T_ROLE_UPDATE.getCode(), this);
        jobService.addJobProcessor(JobActionCode.SUPC_PACKAGINGTYPE_MAPPING.getCode(), this);
    }

    @Override
    public JobProcessorResponse process(JobDetail jobDetail) {
        JobProcessorResponse response = new JobProcessorResponse();
        LOG.info("processing file:" + jobDetail.getFileName() + " from path:" + jobDetail.getFilePath() + " status:" + jobDetail.getStatus().getCode());

        String ftpServerPath = dep.getStringPropertyValue(Property.COCOFS_FTP_SERVER_PATH);
        String ftpUsername = dep.getStringPropertyValue(Property.COCOFS_FTP_USERNAME);
        String ftpPassword = dep.getStringPropertyValue(Property.COCOFS_FTP_PASSWORD);

        String filepath = jobDetail.getFilePath() + File.separator + jobDetail.getFileName();
        UserInfo userInfo = new UserInfo();
        userInfo.setEmail(StringUtils.isNotEmpty(jobDetail.getUploadedBy()) ? jobDetail.getUploadedBy() + jobDetail.getFileCode() : "BulkUpload");
        userInfo.setSuper(false);
        boolean enabled;

        // We save file on identical paths on local FS and FTP
        Map<String, List<String>> errorMap = new HashMap<String, List<String>>(0);
        boolean success = false;

        try {
            dep.downloadFile(ftpServerPath, ftpUsername, ftpPassword, filepath, filepath);
            List<SupcPackagingTypeUploadDTO> dtoList = dep.readFile(filepath, SupcPackagingTypeUploadDTO.class, true);
            int list_size = dtoList.size();
            LOG.info("Reading of file complete and size of list is :" + list_size);
            errorMap = processData(dtoList, errorMap, userInfo);
            success = true;

        } catch (InvalidDataException e) {
            updateErrorMapForFileError(jobDetail, errorMap, e);
            LOG.error("Exception ", e);
        } catch (IOException e) {
            updateErrorMapForFileError(jobDetail, errorMap, e);
            LOG.error("Exception ", e);
        } catch (ClassNotFoundException e) {
            updateErrorMapForFileError(jobDetail, errorMap, e);
            LOG.error("Exception ", e);
        } catch (Exception e) {
            updateErrorMapForFileError(jobDetail, errorMap, e);
            LOG.error("Exception ", e);
        }

        errorMap = null;
        success = true;

        response.setError(errorMap);
        if ((errorMap != null && errorMap.size() > 0) || !success) {
            response.setProcessingSuccessful(false);
        } else {
            response.setProcessingSuccessful(true);
        }
        return response;
    }

    private Map<String, List<String>> processData(List<SupcPackagingTypeUploadDTO> dtoList, Map<String, List<String>> errorMap, UserInfo userInfo)
            throws ClassNotFoundException, InvalidDataException, IOException {
        // TODO Auto-generated method stub

        for (SupcPackagingTypeUploadDTO dto : dtoList) {
            processDataForSupcPackagingTypeMapping(dto, errorMap, userInfo);
        }
        return errorMap;
    }

    private void updateErrorMapForFileError(JobDetail jobDetail, Map<String, List<String>> errorMap, Exception e) {
        if (!errorMap.containsKey("FileError")) {
            errorMap.put("FileError", new ArrayList<String>());

        }
        errorMap.get("FileError").add("Error in reading file for job " + jobDetail.getFileCode() + " Error Message " + e.getMessage());
    }

    private void updateErrorMapForSupcPackagingTypeMappingRowError(SupcPackagingTypeUploadDTO dto, Map<String, List<String>> errorMap, String message) {
        if (!errorMap.containsKey("RowError")) {
            errorMap.put("RowError", new ArrayList<String>());

        }
        errorMap.get("RowError").add("Error in row with Supc : " + dto.getSupc() + " and storeCode : " + dto.getStoreCode() + " Error : " + message);
    }

    @Override
    public BulkUploadValidationResponse validateJob(File file, String actionCode, String param) {
        BulkUploadValidationResponse response = new BulkUploadValidationResponse();
        List<BulkUploadValidationDTO> validationDTOList = new ArrayList<BulkUploadValidationDTO>();
        try {
            List<SupcPackagingTypeUploadDTO> uploadList = ExcelReader.readFile(file.getAbsolutePath(), SupcPackagingTypeUploadDTO.class, true);

            Set<SupcPackagingTypeUploadDTO> dtoSet = new HashSet<SupcPackagingTypeUploadDTO>(uploadList);

            LOG.info("Uploaded records count - {}", uploadList.size());

            if (uploadList.size() != dtoSet.size()) {
                LOG.info("List size uploadList.size() {} unique supc set size {}", uploadList, dtoSet.size());
                response.setErrorMessage("Duplicate supc combination exists in the file. Total records: " + uploadList.size() + ", unique records: " + dtoSet.size());
                response.setValid(false);
                return response;
            }
            int row = 0;
            StringBuilder sb = new StringBuilder();
            for (SupcPackagingTypeUploadDTO o : uploadList) {
                row++;
                if (StringUtils.isEmpty(o.getSupc())) {
                    validationDTOList.add(new BulkUploadValidationDTO(row, BulkUploadValidationError.SUPC_MISSING.getDescription(), o.getSupc()));
                    sb.append("supc missing for rows : " + row);
                } else if (CollectionUtils.isEmpty(productAttributeService.getAllProductAttributeForSUPC(o.getSupc()))) {
                    validationDTOList.add(new BulkUploadValidationDTO(row, BulkUploadValidationError.SUPC_NON_EXIST.getDescription(), o.getSupc()));
                    sb.append("Invalid Supc : product attribute does not exist for supc at row : " + row);
                }

                if (StringUtils.isEmpty(o.getStoreCode())) {
                    validationDTOList.add(new BulkUploadValidationDTO(row, BulkUploadValidationError.STORE_CODE_MISSING.getDescription(), o.getStoreCode()));
                    sb.append("store code  missing from rows : " + row);
                }

                if (isValidEnabledPackagingTypeForStoreCode(o.getStoreCode(), o.getPackagingType()) == false) {
                    validationDTOList.add(new BulkUploadValidationDTO(row, BulkUploadValidationError.INVALID_PACKAGINGTYPE.getDescription(), o.getPackagingType()));
                    sb.append("invalid PackagingType for specified store Code at row: " + row);
                }

            }
            if (!validationDTOList.isEmpty()) {
                response.setValid(false);
                response.setErrorMessage(sb.toString());
                response.setList(validationDTOList);
                return response;
            }

            response.setValid(true);
            return response;
        } catch (InvalidDataException e) {
            response.setValid(false);
            response.setErrorMessage(BulkUploadValidationError.REQUIRED_FIELDS_MISSING.getDescription() + " " + e.getMessage());
            LOG.error("Error while uploading the data for file {} for action type {} ", file.getAbsoluteFile(), actionCode, e);
        } catch (IOException e) {
            response.setValid(false);
            response.setErrorMessage(BulkUploadValidationError.UPLOAD_FILE_READ_ERROR.getDescription());
            LOG.error("Error while uploading the data for file {} for action type {} ", file.getAbsoluteFile(), actionCode, e);
        } catch (ClassNotFoundException e) {
            response.setValid(false);
            response.setErrorMessage(BulkUploadValidationError.FILE_TYPE_NOT_CORRECT.getDescription());
            LOG.error("Error while uploading the data for file {} for action type {} ", file.getAbsoluteFile(), actionCode, e);
        }

        return response;
    }

    private boolean isValidEnabledPackagingTypeForStoreCode(String storeCode, String PackagingType) {
        List<PackagingType> packagingTypeListByStoreCode = packmanDao.getAllPackagingType(storeCode);
        if (packagingTypeListByStoreCode == null)
            return false;
        for (PackagingType packagingType : packagingTypeListByStoreCode) {

            if ((packagingType.isEnabled()) && packagingType.getType().equals(PackagingType)) {

                return true;
            }

        }
        return false;
    }

    public interface IDependency {
        boolean getBooleanPropertyValue(Property property);

        String getStringPropertyValue(Property property);

        public <T> List<T> readFile(String uploadPath, Class<T> dtoClass) throws IOException, ClassNotFoundException, InvalidDataException;

        public <T> List<T> readFile(String uploadPath, Class<T> dtoClass, boolean emptyFieldAllowed) throws IOException, ClassNotFoundException, InvalidDataException;

        public void downloadFile(String serverName, String userName, String password, String ftpFullPath, String localFullPath);
    }

    static class Dependency implements IDependency {
        @Override
        public boolean getBooleanPropertyValue(Property property) {
            return ConfigUtils.getBooleanScalar(property);
        }

        @Override
        public String getStringPropertyValue(Property property) {
            return ConfigUtils.getStringScalar(property);
        }

        @Override
        public <T> List<T> readFile(String uploadPath, Class<T> dtoClass, boolean emptyFieldAllowed) throws IOException, ClassNotFoundException, InvalidDataException {
            return ExcelReader.readFile(uploadPath, dtoClass, emptyFieldAllowed);
        }

        @Override
        public <T> List<T> readFile(String uploadPath, Class<T> dtoClass) throws IOException, ClassNotFoundException, InvalidDataException {
            return ExcelReader.readFile(uploadPath, dtoClass);
        }

        @Override
        public void downloadFile(String serverName, String userName, String password, String ftpFullPath, String localFullPath) {
            FTPUtils.downloadFile(serverName, userName, password, ftpFullPath, localFullPath);
        }
    }

    private Map<String, List<String>> processDataForSupcPackagingTypeMapping(SupcPackagingTypeUploadDTO uploadDto, Map<String, List<String>> errorMap, UserInfo userInfo)
            throws ClassNotFoundException, InvalidDataException, IOException {

        try {
            GenericPersisterResponse<SupcPackagingTypeMapping, SupcPackagingTypeMappingUnit> resp = dataUpdater.updateDataWithDTO(uploadDto, supcPackagingTypeMappingDataReader,
                    supcPackagingTypeMappingDataEngine, userInfo, true, true);
            if (!resp.isSuccessful()) {
                updateErrorMapForSupcPackagingTypeMappingRowError(uploadDto, errorMap, resp.getMessage());
            } else {
                LOG.info("Successfully persisted for supc {} ", uploadDto.getSupc());
            }
        } catch (OperationNotSupportedException e) {
            LOG.error("Looks like we already have data for supc {} ", uploadDto.getSupc());
            updateErrorMapForSupcPackagingTypeMappingRowError(uploadDto, errorMap, e.getMessage());
        } catch (Exception e) {
            LOG.error("Error while invoking generic persistence wrapper for supc {} ", uploadDto.getSupc(), e);
            updateErrorMapForSupcPackagingTypeMappingRowError(uploadDto, errorMap, e.getMessage());
        }
        return errorMap;
    }

}
