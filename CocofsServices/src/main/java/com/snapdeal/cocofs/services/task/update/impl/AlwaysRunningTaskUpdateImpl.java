/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 03-Sep-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.services.task.update.impl;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.entity.TaskDetail;
import com.snapdeal.cocofs.enums.TaskStatus;
import com.snapdeal.cocofs.services.task.alwaysrunning.IAlwaysRunningTask;
import com.snapdeal.cocofs.services.task.update.IAlwaysRunningTaskUpdate;
import com.snapdeal.cocofs.services.task.update.ITaskUpdateService;
import com.snapdeal.cocofs.task.db.dataaccess.ITaskDBDataReadService;

@Service("AlwaysRunningTask")
public class AlwaysRunningTaskUpdateImpl implements IAlwaysRunningTaskUpdate, ApplicationContextAware {

    private static final Logger LOG = LoggerFactory.getLogger(AlwaysRunningTaskUpdateImpl.class);

    private ApplicationContext  context;

    @Autowired
    ITaskUpdateService          taskUpdateService;

    @Autowired
    ITaskDBDataReadService      taskDBDataReadService;

    @Override
    public void setApplicationContext(ApplicationContext context) throws BeansException {
        this.context = context;
    }

    @Override
    public void startTask(String taskName) {
        if (StringUtils.isNotEmpty(taskName)) {
            TaskDetail taskDetail = taskDBDataReadService.getTaskDetailByName(taskName);
            IAlwaysRunningTask task;
            try {
                task = (IAlwaysRunningTask) context.getBean(Class.forName(taskDetail.getImplClass()));
                if (null != task && !task.isTaskRunning()) {
                    taskUpdateService.updateTaskStatusInfo(taskName, null, null, TaskStatus.RUNNING.getCode());
                    task.execute();
                }
            } catch (BeansException e) {
                LOG.error("Exception while starting non quartz task", e);
            } catch (ClassNotFoundException e) {
                LOG.error("Exception while starting non quartz task", e);
            }
        }
    }

    @Override
    public void stopTask(String taskName) {
        if (StringUtils.isNotEmpty(taskName)) {
            TaskDetail taskDetail = taskDBDataReadService.getTaskDetailByName(taskName);
            IAlwaysRunningTask task;
            try {
                task = (IAlwaysRunningTask) context.getBean(Class.forName(taskDetail.getImplClass()));
                if (null != task) {
                    taskUpdateService.updateTaskStatusInfo(taskName, null, null, TaskStatus.INACTIVE.getCode());
                    task.stopProcess();
                }
            } catch (BeansException e) {
                LOG.error("Exception while starting non quartz task", e);
            } catch (ClassNotFoundException e) {
                LOG.error("Exception while starting non quartz task", e);
            }
        }
    }
}
