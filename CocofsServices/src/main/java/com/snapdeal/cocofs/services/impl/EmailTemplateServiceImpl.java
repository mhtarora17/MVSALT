/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 05-Sep-2013
 *  @author himanshum
 */
package com.snapdeal.cocofs.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.snapdeal.cocofs.db.dao.IEmailTemplateDao;
import com.snapdeal.cocofs.entity.EmailTemplate;
import com.snapdeal.cocofs.services.IEmailTemplateService;

@Service("emailTemplateService")
public class EmailTemplateServiceImpl implements IEmailTemplateService {
    @Autowired
    private IEmailTemplateDao emailTemplateDao;

    
    public void setEmailTemplateDao(IEmailTemplateDao emailTemplateDao) {
        this.emailTemplateDao = emailTemplateDao;
    }

    @Override
    @Transactional
    public EmailTemplate update(EmailTemplate emailTemplate) {
        return emailTemplateDao.update(emailTemplate);
    }

    @Override
    @Transactional
    public EmailTemplate getEmailTemplateByName(String templateName) {
        return emailTemplateDao.getEmailTemplateByName(templateName);
    }


}
