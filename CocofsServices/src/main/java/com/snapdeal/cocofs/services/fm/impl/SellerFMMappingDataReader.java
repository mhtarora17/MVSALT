/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 07-Oct-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.services.fm.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.core.aerospike.vo.SellerFMMappingVO;
import com.snapdeal.cocofs.dataaccess.fm.IFMAerospikeDataReadService;
import com.snapdeal.cocofs.dataaccess.fm.IFMDBDataReadService;
import com.snapdeal.cocofs.entity.SellerFMMapping;
import com.snapdeal.cocofs.services.IDataReaderWithAerospike;
import com.snapdeal.cocofs.services.fm.dto.SellerFMMappingUpdateDto;

@Service("SellerFMMappingDataReader")
public class SellerFMMappingDataReader implements IDataReaderWithAerospike<SellerFMMappingUpdateDto, SellerFMMapping,  SellerFMMappingVO> {

  
    @Autowired
    private IFMDBDataReadService fmDBDataReadService;
    
    @Autowired
    private IFMAerospikeDataReadService  fmAerospikeReadService;

 

    @Override
    public List<SellerFMMapping> getEntitiesForDTO(SellerFMMappingUpdateDto dto) {
        SellerFMMapping mapping = fmDBDataReadService.getEnabledSellerFMMapping(dto.getSellerCode());
        List<SellerFMMapping> mappingList = new ArrayList<SellerFMMapping>();
        if (null != mapping) {
            mappingList.add(mapping);
        }
        return mappingList;
    }

    @Override
    public SellerFMMappingVO getRecordForDTO(SellerFMMappingUpdateDto dto) {
        SellerFMMappingVO record = (SellerFMMappingVO)fmAerospikeReadService.getFMMapping(dto.getSellerCode(), SellerFMMappingVO.class);
        
        return record;
    }
}
