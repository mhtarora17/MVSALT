package com.snapdeal.cocofs.services.fm.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.cocofs.dataaccess.fm.IFMDBDataReadService;
import com.snapdeal.cocofs.entity.SellerSupcMapping;
import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.generic.persister.exception.GenericPersisterException;
import com.snapdeal.cocofs.generic.persister.exception.GenericPersisterExceptionCode;
import com.snapdeal.cocofs.generic.persister.request.GenericPersisterDBRequest;
import com.snapdeal.cocofs.generic.persister.response.GenericPersisterDBResponse;
import com.snapdeal.cocofs.generic.persister.service.IGenericPersisterService;
import com.snapdeal.cocofs.services.fm.ISellerToSUPCListDBLookupService;
import com.snapdeal.score.data.aerospike.exception.GetFailedException;

/**
 * 
 * Service to fetch supcs for a seller from mysql db
 * @author gaurav
 *
 */
@Service("sellerToSUPCListDBLookupServiceImpl")
public class SellerToSUPCListDBLookupServiceImpl implements ISellerToSUPCListDBLookupService{

    private static final Logger      LOG = LoggerFactory.getLogger(SellerToSUPCListDBLookupServiceImpl.class);

    @Autowired
    private IFMDBDataReadService     fmDBDataReadService;
    
    @Autowired
    private IGenericPersisterService     persistenceService;
    
    /**
     * Check if supc exist for the seller.
     */
	@Override
	public boolean isSUPCExistsInSellerSUPCListMapping(String sellerCode,
			String supc) throws ExternalDataNotFoundException,
			GetFailedException {
		return fmDBDataReadService.isSellerSupcMappingExists(sellerCode, supc);
	}

	/**
	 * Fetch all supcs for the seller.
	 */
	@Override
	public List<String> getSupcListForSeller(String sellerCode)
			throws ExternalDataNotFoundException {
	
		List<SellerSupcMapping> sellerSupcMapping = fmDBDataReadService.getSellerSupcMapping(sellerCode);
		return extractSupcs(sellerSupcMapping);
	
	}

	/**
	 * Fetch supcs at exception level for the seller.
	 */
	@Override
	public List<String> getSupcExceptionListForSeller(String sellerCode)
			throws ExternalDataNotFoundException {
    
		List<String> supcExceptionListForSeller = fmDBDataReadService.getAllEnabledSupcExceptionListForSeller(sellerCode);
        return supcExceptionListForSeller;

	}

	private List<String> extractSupcs(List<SellerSupcMapping> mappings) {

        List<String> supcList = new ArrayList<String>();

        if (mappings != null && !mappings.isEmpty()) {
            for (SellerSupcMapping mapping : mappings) {
                supcList.add(mapping.getSupc());
            }
        }

        return supcList;
    }

	@Override
	public void putNewSellerSupcMapping(String sellerCode, String supc) throws GenericPersisterException {
		
		SellerSupcMapping mapping = new SellerSupcMapping();
		mapping.setSellerCode(sellerCode);
		mapping.setSupc(supc);
		mapping.setCreated(DateUtils.getCurrentTime());
		GenericPersisterDBRequest<SellerSupcMapping> request  = new GenericPersisterDBRequest<SellerSupcMapping>();
		request.setEntitiesToPersist(Arrays.asList(mapping));
		request.setSkipValidation(true);
		GenericPersisterDBResponse<SellerSupcMapping> response = persistenceService.persistOneDBEntity(request);

		if (!response.isSuccessful()) {
            LOG.info("GenericPersisterResponse messgae - {}", response.getMessage());
            throw new GenericPersisterException(null, response.getMessage(), GenericPersisterExceptionCode.ENTITY_FAILED_TO_PERSIST);
        }
		
       LOG.info("Successfully persisted the new seller-supc mapping{}.",mapping);
	}

	@Override
	public void removeSellerSupcMapping(String sellerCode, String supc) {
		// TODO Auto-generated method stub
		
	}

}
