/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.services.events.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * @version 1.0, 25-Mar-2015
 * @author shiv
 */
public class FCAddressUpdateEventObj implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = -6829240080593625319L;
    private String            fcCode;
    private String            oldPincode;
    private String            newPincode;  
    private Date              created;

    public FCAddressUpdateEventObj() {
    }

    
    public FCAddressUpdateEventObj(String fcCode, String oldPincode, String newPincode) {
        super();
        this.fcCode = fcCode;
        this.oldPincode = oldPincode;
        this.newPincode = newPincode;
        this.created = new Date();
    }


    public String getFcCode() {
        return fcCode;
    }

    public void setFcCode(String fcCode) {
        this.fcCode = fcCode;
    }

    public String getOldPincode() {
        return oldPincode;
    }

    public void setOldPincode(String oldPincode) {
        this.oldPincode = oldPincode;
    }

    public String getNewPincode() {
        return newPincode;
    }

    public void setNewPincode(String newPincode) {
        this.newPincode = newPincode;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }


    @Override
    public String toString() {
        return "FCAddressUpdateEventObj [fcCode=" + fcCode + ", oldPincode=" + oldPincode + ", newPincode=" + newPincode + ", created=" + created
                + "]";
    }

    
}
