/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 21-Oct-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.services.jobprocessor;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.base.utils.CollectionUtils;
import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.base.utils.ValidatorUtils;
import com.snapdeal.cocofs.common.BulkUploadValidationDTO;
import com.snapdeal.cocofs.common.BulkUploadValidationResponse;
import com.snapdeal.cocofs.common.JobProcessorResponse;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.entity.JobDetail;
import com.snapdeal.cocofs.entity.SellerZoneMapping;
import com.snapdeal.cocofs.enums.BulkUploadValidationError;
import com.snapdeal.cocofs.enums.JobActionCode;
import com.snapdeal.cocofs.excelupload.PincodeUploadDTO;
import com.snapdeal.cocofs.external.service.ISnapdealGeoExternalService;
import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.generic.persister.exception.GenericPersisterException;
import com.snapdeal.cocofs.services.IJobProcessor;
import com.snapdeal.cocofs.services.IJobSchedulerService;
import com.snapdeal.cocofs.services.ISellerZoneService;
import com.snapdeal.cocofs.utils.ExcelReader;
import com.snapdeal.cocofs.utils.FTPUtils;
import com.snapdeal.geo.base.sro.ZoneRequest;
import com.snapdeal.geo.base.sro.ZoneResponse;
import com.sun.media.sound.InvalidDataException;

@Service("pincodeZoneMappingUploadProcessor")
public class PincodeZoneMappingUploadProcessor implements IJobProcessor {

    private static final Logger                                                                                LOG = LoggerFactory.getLogger(PincodeZoneMappingUploadProcessor.class);
    @Autowired
    private IJobSchedulerService                                                                               jobService;

    @Autowired
    private ISellerZoneService                                                                                sellerZoneService;
    
    @Autowired
    private ISnapdealGeoExternalService geoExternalService;

    
    private IDependency                                                                                        dep = new Dependency();

   

    @PostConstruct
    public void init() {
        jobService.addJobProcessor(JobActionCode.P_Z_UPDATE.getCode(), this);
    }

    @Override
    public JobProcessorResponse process(JobDetail jobDetail) {
        JobProcessorResponse response = new JobProcessorResponse();
        LOG.info("processing file:" + jobDetail.getFileName() + " from path:" + jobDetail.getFilePath() + " status:" + jobDetail.getStatus().getCode());
        String ftpServerPath = dep.getStringPropertyValue(Property.COCOFS_FTP_SERVER_PATH);
        String ftpUsername = dep.getStringPropertyValue(Property.COCOFS_FTP_USERNAME);
        String ftpPassword = dep.getStringPropertyValue(Property.COCOFS_FTP_PASSWORD);
        String filepath = jobDetail.getFilePath() + File.separator + jobDetail.getFileName();
        
        // We save file on identical paths on local FS and FTP
        Map<String, List<String>> errorMap = new HashMap<String, List<String>>(0);
        boolean success = false;
        try {
            dep.downloadFile(ftpServerPath, ftpUsername, ftpPassword, filepath, filepath);
            List<PincodeUploadDTO> dtoList = dep.readFile(filepath, PincodeUploadDTO.class, true);

            errorMap = processData(dtoList, errorMap, jobDetail.getUploadedBy() + ";" + jobDetail.getFileCode());
            success = true;
        } catch (InvalidDataException e) {
            updateErrorMapForFileError(jobDetail, errorMap, e);
            LOG.error("Exception ", e);
        } catch (IOException e) {
            updateErrorMapForFileError(jobDetail, errorMap, e);
            LOG.error("Exception ", e);
        } catch (ClassNotFoundException e) {
            updateErrorMapForFileError(jobDetail, errorMap, e);
            LOG.error("Exception ", e);
        } catch (Exception e) {
            updateErrorMapForFileError(jobDetail, errorMap, e);
            LOG.error("Exception ", e);
        }
        response.setError(errorMap);
        if ((errorMap != null && errorMap.size() > 0) || !success) {
            response.setProcessingSuccessful(false);
        } else {
            response.setProcessingSuccessful(true);
        }
        return response;

    }

    private Map<String, List<String>> processData(List<PincodeUploadDTO> dtoList, Map<String, List<String>> errorMap, String updatedBy) {

        for (PincodeUploadDTO dto : dtoList) {
            processDataForPincode(dto, errorMap, updatedBy);
        }

        return errorMap;
    }

       
    
    private void processDataForPincode(PincodeUploadDTO uploadDto, Map<String, List<String>> errorMap, String updatedBy) {
        String pincode = uploadDto.getPincode().trim();
        LOG.info("processing pincode upload data for pincode {}",pincode);
       
        try {
            List<SellerZoneMapping> sellerZoneMappingList = sellerZoneService.getSellerZoneMappingsByPincode(pincode);

            if (CollectionUtils.isEmpty(sellerZoneMappingList)) {

                LOG.info("no seller zone mapping found for pincode {} hence nothing to be updated", pincode);

                return;
            }
            LOG.info("Number of SellerZoneMapping found for pincode {} are  {}",pincode, sellerZoneMappingList.size());

            String zoneName = getZoneForPincode(pincode);
            
            for (SellerZoneMapping sellerZoneMapping : sellerZoneMappingList) {
            
                sellerZoneMapping.setZone(zoneName);
                sellerZoneMapping.setUpdatedBy(updatedBy);
                
                try {
                    sellerZoneService.persistSellerZoneMapping(sellerZoneMapping);
                } catch (GenericPersisterException gpe) {
                    updateErrorMapForRowError(uploadDto, errorMap, "error while updating zone of pincode " + pincode + "  for seller " + sellerZoneMapping.getSellerCode());
                    LOG.error("error while persisiting sellerZoneMapping", gpe);
                }
                
            }

        } catch (ExternalDataNotFoundException e) {
            LOG.error("Error while fetching zone for pincode "+ pincode , e);
            updateErrorMapForRowError(uploadDto, errorMap, e.getMessage());
        } catch (Exception e) {
            LOG.error("UnExpected Error while fetching zone for pincode " + pincode, e);
            updateErrorMapForRowError(uploadDto, errorMap, e.getMessage());
        }

        uploadDto.setLastUpdated(DateUtils.getCurrentTime());

       

    }


    private String getZoneForPincode(String pincode) throws ExternalDataNotFoundException {
        ZoneRequest zrequest = new ZoneRequest();
        zrequest.setPincode(pincode);
        ZoneResponse resp = geoExternalService.getZoneFromPincode(zrequest);
        if(resp == null || StringUtils.isEmpty(resp.getZone())){
            throw new ExternalDataNotFoundException("null zone response received from SnapdealGeo  for pincode " +pincode);
        }
           
        return resp.getZone();
    }

    private void updateErrorMapForRowError(PincodeUploadDTO dto, Map<String, List<String>> errorMap, String message) {
        if (!errorMap.containsKey("RowError")) {
            errorMap.put("RowError", new ArrayList<String>());
        }
        errorMap.get("RowError").add("Error in row with pincode : " + dto.getPincode() + " Error : " + message);
    }

    
    private void updateErrorMapForFileError(JobDetail jobDetail, Map<String, List<String>> errorMap, Exception e) {
        if (!errorMap.containsKey("FileError")) {
            errorMap.put("FileError", new ArrayList<String>());
        }
        errorMap.get("FileError").add("Error in reading file for job " + jobDetail.getFileCode() + " Error Message " + e.getMessage());
    }


    @Override
    public BulkUploadValidationResponse validateJob(File file, String actionCode, String param) {
        BulkUploadValidationResponse response = new BulkUploadValidationResponse();
        List<BulkUploadValidationDTO> validationDTOList = new ArrayList<BulkUploadValidationDTO>();
        boolean success = false;
        List<PincodeUploadDTO> dtoList = null;
        StringBuilder sb = new StringBuilder();

        try {
            dtoList = dep.readFile(file.getAbsolutePath(), PincodeUploadDTO.class, true);
            int rowNo = 1;
            Set<PincodeUploadDTO> dtoSet = new HashSet<PincodeUploadDTO>(dtoList);

            LOG.info("Uploaded records count - {}", dtoList.size());

            if (dtoList.size() != dtoSet.size()) {
                LOG.info("List size dtoList.size() {} set size {}", dtoList.size(), dtoSet.size());
                sb.append("Duplicate pincode  exists in the file <br>");
                response.setValid(false);
                return response;
            }
  
            for (PincodeUploadDTO dto : dtoList) {
                rowNo++;
                String pincode = dto.getPincode();
                if (StringUtils.isEmpty(pincode)) {
                    LOG.info("row {} pincode is empty: {}", rowNo, pincode);
                    validationDTOList.add(new BulkUploadValidationDTO(rowNo, BulkUploadValidationError.INVALID_PINCODE.getDescription(), pincode));
                    sb.append("Empty pincode value from rows : " + rowNo + "<br>");
                    continue;
                }

               

                if (!ValidatorUtils.isPincodeValid(pincode.trim())) {
                    LOG.info("row {} pincode is not valid: {}", rowNo, pincode);
                    validationDTOList.add(new BulkUploadValidationDTO(rowNo, BulkUploadValidationError.INVALID_PINCODE.getDescription(), pincode));
                    sb.append("invalid pincode value from rows : " + rowNo + "<br>");

                } 
            }
            success = true;
        } catch (InvalidDataException e) {
            response.setErrorMessage(BulkUploadValidationError.REQUIRED_FIELDS_MISSING.getDescription() + ". " + e.getMessage());
            LOG.error("Error while uploading the data for file {} for action type {} ", file.getAbsoluteFile(), actionCode, e);
        } catch (IOException e) {
            response.setErrorMessage(BulkUploadValidationError.UPLOAD_FILE_READ_ERROR.getDescription());
            LOG.error("Error while uploading the data for file {} for action type {} ", file.getAbsoluteFile(), actionCode, e);
        } catch (ClassNotFoundException e) {
            response.setErrorMessage(BulkUploadValidationError.FILE_TYPE_NOT_CORRECT.getDescription());
            LOG.error("Error while uploading the data for file {} for action type {} ", file.getAbsoluteFile(), actionCode, e);
        }
        
        response.setValid(success && validationDTOList.isEmpty());

        if (!validationDTOList.isEmpty()) {
            response.setList(validationDTOList);
            response.setErrorMessage(sb.toString());

        }

        return response;
    }

   

   

   

    public interface IDependency {
        boolean getBooleanPropertyValue(Property property);

        String getStringPropertyValue(Property property);

        public <T> List<T> readFile(String uploadPath, Class<T> dtoClass) throws IOException, ClassNotFoundException, InvalidDataException;

        public <T> List<T> readFile(String uploadPath, Class<T> dtoClass, boolean emptyFieldAllowed) throws IOException, ClassNotFoundException, InvalidDataException;

        public void downloadFile(String serverName, String userName, String password, String ftpFullPath, String localFullPath);
    }

    static class Dependency implements IDependency {
        @Override
        public boolean getBooleanPropertyValue(Property property) {
            return ConfigUtils.getBooleanScalar(property);
        }

        @Override
        public String getStringPropertyValue(Property property) {
            return ConfigUtils.getStringScalar(property);
        }

        @Override
        public <T> List<T> readFile(String uploadPath, Class<T> dtoClass, boolean emptyFieldAllowed) throws IOException, ClassNotFoundException, InvalidDataException {
            return ExcelReader.readFile(uploadPath, dtoClass, emptyFieldAllowed);
        }

        @Override
        public <T> List<T> readFile(String uploadPath, Class<T> dtoClass) throws IOException, ClassNotFoundException, InvalidDataException {
            return ExcelReader.readFile(uploadPath, dtoClass);
        }

        @Override
        public void downloadFile(String serverName, String userName, String password, String ftpFullPath, String localFullPath) {
            FTPUtils.downloadFile(serverName, userName, password, ftpFullPath, localFullPath);
        }
    }
}
