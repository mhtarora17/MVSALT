/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.services.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.snapdeal.base.cache.CacheManager;
import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.cocofs.annotations.TouchyTransaction;
import com.snapdeal.cocofs.cache.loader.impl.SellerZoneMappingCache;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.core.aerospike.vo.SellerZoneMappingVO;
import com.snapdeal.cocofs.db.dao.ISellerZoneDao;
import com.snapdeal.cocofs.entity.SellerZoneMapping;
import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.generic.persister.exception.GenericPersisterException;
import com.snapdeal.cocofs.generic.persister.exception.GenericPersisterExceptionCode;
import com.snapdeal.cocofs.services.ISellerZoneService;
import com.snapdeal.cocofs.services.converter.IConverterService;
import com.snapdeal.cocofs.sro.ZoneSRO;
import com.snapdeal.score.data.aerospike.exception.GetFailedException;
import com.snapdeal.score.data.aerospike.service.impl.ScoreAerospikeService;

/**
 * @version 1.0, 15-Jun-2015
 * @author gaurav
 */

@Service("sellerZoneService")
public class SellerZoneServiceImpl implements ISellerZoneService {

    private static final Logger      LOG = LoggerFactory.getLogger(SellerZoneServiceImpl.class);

    @Autowired
    private ISellerZoneDao  sellerZoneDao;
    
    @Autowired
	@Qualifier("aerospikeService")
	private ScoreAerospikeService	aerospikeService;
    
    @Autowired
    private IConverterService converterService;

    @Override
    public List<SellerZoneMapping> getAllSellerZoneMappings() {
        LOG.info("Processing request to fetch all seller-zone mapping ");
        return sellerZoneDao.getAllSellerZoneMappings();
    }
    
    @Override
    public List<SellerZoneMapping> getSellerZoneMappingsByPincode(String pincode) {
        LOG.info("Processing request to fetch seller-zone mapping for pincode {}",pincode);
        return sellerZoneDao.getAllSellerZoneMappingsByPinCode(pincode);
    }

	@Override
	public String getSellerZoneMappingBySeller(String sellerCode) {
		LOG.debug("Processing request to fetch seller-zone mapping by sellerCode:{}",sellerCode);
		String zone = null;
		try {
			if(ConfigUtils.getBooleanScalar(Property.ENABLE_SELLER_ZONE_MAPPING_CACHE)){
				//I level - hit lru cache(if flag is enabled).. + II level - aerospike hit..
				zone = CacheManager.getInstance().getCache(SellerZoneMappingCache.class).getSellerZoneMapping(sellerCode);
			} else{
				//Fetch directly from the defined data source...
				zone = getSellerZoneMappingBySellerFromBaseSource(sellerCode);
			}
		} catch (ExternalDataNotFoundException e) {
			LOG.warn("Seller zone info not found in cache & aerospike.");
			e.printStackTrace();
		} catch(Exception ex){
			LOG.error("Unknown exception occurred while fetching data from sellerCode:{}",sellerCode);
			ex.printStackTrace();
		}
		return zone;
	}
    

    @Override
    public ZoneSRO getZoneForSellerSupc(String sellerCode, String supc) throws ExternalDataNotFoundException{
        ZoneSRO zoneSRO = null;
        String zoneName = getSellerZoneMappingBySeller(sellerCode);
        if (StringUtils.isNotEmpty(zoneName)) {
            zoneSRO = new ZoneSRO(zoneName);
        } else {
            LOG.debug("No zone found for seller {} supc {} ", sellerCode, supc);
        }

        return zoneSRO;
    }

    

	@Override
	public String getSellerZoneMappingBySellerFromBaseSource(
			String sellerCode) {
		String zone = null;
		LOG.debug("Processing request to fetch seller-zone mapping by sellerCode={} from base data source",sellerCode);
		if(ConfigUtils.getStringScalar(Property.DATA_SOURCE_FOR_ZONE_READ).equalsIgnoreCase("AEROSPIKE")){
			SellerZoneMappingVO szmv = null;
			try {
				szmv = (SellerZoneMappingVO)aerospikeService.get(sellerCode, SellerZoneMappingVO.class);
				zone = szmv==null ? null:szmv.getZone();
			} catch (GetFailedException e) {
				e.printStackTrace();
			}
		}
		else if(ConfigUtils.getStringScalar(Property.DATA_SOURCE_FOR_ZONE_READ).equalsIgnoreCase("MYSQL")){
			SellerZoneMapping szm = sellerZoneDao.getSellerZoneMappingBySeller(sellerCode);
			
			zone = szm==null ? null:szm.getZone();
            	        
			if(szm != null && !szm.getSellerCode().equals(sellerCode)){
			    zone = null;
			    LOG.debug("returning null zone as alphapbet cases of request seller {} does not match the seller {} in db  ", sellerCode,szm.getSellerCode());
			}
			
		} else{
			LOG.error("Unknown data source encountered while fetching data for seller-mapping for seller:{}.",sellerCode);
		}
		return zone;
	}


	@Override
	@TouchyTransaction
	public SellerZoneMapping persistSellerZoneMapping(SellerZoneMapping szm) throws GenericPersisterException {
		SellerZoneMapping updatedSzm = null;
		//First persist in db...
		try{
			SellerZoneMapping existingSzm = sellerZoneDao.getSellerZoneMappingBySeller(szm.getSellerCode());
			if(existingSzm!=null){
				szm.setId(existingSzm.getId());
				szm.setCreated(existingSzm.getCreated());
			} else{
				szm.setCreated(DateUtils.getCurrentTime());
			}
			updatedSzm = sellerZoneDao.persistSellerZoneMapping(szm);
		} catch(Exception ex){
			LOG.error("Failed to persist seller zone in db:{}",szm);
			throw new GenericPersisterException(ex,"Not able to persist seller zone details. Reverting tx."
					,GenericPersisterExceptionCode.RECORD_FAILED_TO_PERSIST);
		}
		//Now try to persist in aerospike
		try{
			SellerZoneMappingVO szmv = converterService.getSellerZoneMappingVoFromEntity(updatedSzm);
			aerospikeService.put(szmv);
		} catch(Exception ex){
			LOG.error("Failed to persist seller zone in aerospike:{} with error:{}",szm,ex.getMessage());
			throw new GenericPersisterException(ex,"Not able to persist seller zone details in aerospike. Reverting tx."
					,GenericPersisterExceptionCode.RECORD_FAILED_TO_PERSIST);
		}
		return updatedSzm;
	}
	
}
