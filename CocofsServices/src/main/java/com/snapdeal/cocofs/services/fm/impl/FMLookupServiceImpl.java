/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 26-Nov-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.services.fm.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.cocofs.dataaccess.fm.IFMDBDataReadService;
import com.snapdeal.cocofs.entity.SellerSupcFCCodeMapping;
import com.snapdeal.cocofs.entity.SellerSupcFMMapping;
import com.snapdeal.cocofs.services.converter.IConverterService;
import com.snapdeal.cocofs.services.fm.IFMLookupService;
import com.snapdeal.cocofs.services.fm.dto.SellerDto;
import com.snapdeal.cocofs.services.fm.dto.SellerSupcFMMappingData;

@Service("FMLookupServiceImpl")
public class FMLookupServiceImpl implements IFMLookupService {

    @Autowired
    IFMDBDataReadService fmDBDataReadService;
    
    @Autowired
    IConverterService   converterService;

    @Override
    public List<SellerDto> getSellersWithExceptionAtSupcLevel() {
        List<String> sellers = fmDBDataReadService.getSellersWithExceptionAtSupcLevel();
        List<SellerDto> dtos = new ArrayList<SellerDto>();
        for (String seller : sellers) {
            dtos.add(new SellerDto(seller));
        }
        return dtos;
    }

    @Override
    public List<SellerSupcFMMappingData> getSellerSupcFMMappingData(String sellerCode) {
        List<SellerSupcFMMapping> mappingList = fmDBDataReadService.getAllEnabledSellerSupcFMMappingBySeller(sellerCode);
        List<SellerSupcFMMappingData> mappingData = new ArrayList<SellerSupcFMMappingData>();
        for (SellerSupcFMMapping mapping : mappingList) {
           
            String fcCodes = getCommaSepratedFCListStr(mapping);
            mappingData.add(new SellerSupcFMMappingData(mapping.getSupc(), mapping.getSellerCode(), mapping.getFulfilmentModel(), fcCodes, DateUtils.dateToString(mapping.getLastUpdated(), "dd/MMM/yyyy HH:mm:ss"), mapping.getUpdatedBy()));
        }
        return mappingData;
    }
    
    private String getCommaSepratedFCListStr(SellerSupcFMMapping mapping){
        List<String> fcCodeList = new ArrayList<String>();
        for(SellerSupcFCCodeMapping fcCodeMapping : mapping.getFcCenters()){
            if(fcCodeMapping.isEnabled()){
                fcCodeList.add(fcCodeMapping.getFcCode());
            }
        }
        
       return converterService.convertFCCentreListToString(fcCodeList);       
        
    }
}
