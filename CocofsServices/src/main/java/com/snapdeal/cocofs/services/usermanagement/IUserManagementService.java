/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 10-Nov-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.services.usermanagement;

import java.util.List;

import com.snapdeal.cocofs.entity.User;
import com.snapdeal.cocofs.services.usermanagement.impl.UserDTO;

public interface IUserManagementService {

    UserDTO getUserInfo(String email);

    void sendUserCreationEmail(String userEmail, String password);

    void enrichUserWithUserRole(User user, List<String> enabledRoleCodes);

    void updateUser(User u, String password);

}
