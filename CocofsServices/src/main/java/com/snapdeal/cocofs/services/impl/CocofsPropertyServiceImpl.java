/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.snapdeal.cocofs.db.dao.IPropertyDao;
import com.snapdeal.cocofs.entity.CocofsProperty;
import com.snapdeal.cocofs.services.ICocofsPropertyService;

@Service("cocofsPropertyServiceImpl")
@Transactional
public class CocofsPropertyServiceImpl implements ICocofsPropertyService{

	@Autowired
	private IPropertyDao propertyDao;
	
	@Override
    public void save(CocofsProperty property) {
    	propertyDao.save(property);
    }

    @Override
    public CocofsProperty merge(CocofsProperty property) {
        return propertyDao.merge(property);
    }

    @Override
    public CocofsProperty getPropertyFromName(String name){
    	return propertyDao.getPropertyFromName(name);
    }
    
    @Override
    public void remove(CocofsProperty property) {
        propertyDao.remove(property);
    }

	@Override
	public void update(CocofsProperty property) {
		propertyDao.update(property);
	}

}
