/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.services.events.shipfrom.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.services.events.dto.FCAddressUpdateEventObj;
import com.snapdeal.cocofs.services.events.impl.BaseEventConsumer;
import com.snapdeal.cocofs.services.events.impl.EventType;
import com.snapdeal.cocofs.services.fm.ISellerSupcShipFromMappingProcessor;

/**
 * @version 1.0, 25-Mar-2015
 * @author shiv
 */
@Service("FCAddressUpdateEventConsumer")
public class FCAddressUpdateEventConsumer extends BaseEventConsumer<FCAddressUpdateEventObj> {

    @Autowired
    @Qualifier("SellerSUPCShipFromMappingProcessorImpl")
    private ISellerSupcShipFromMappingProcessor sellerSupcShipFromMappingProcessor;

    private static final Logger             LOG = LoggerFactory.getLogger(FCAddressUpdateEventConsumer.class);

    @Override
    protected String getEventCode() {
        return EventType.SHIPFROM_UPDATE_EVENT_ON_FC_ADDRESS_UPDATE.getCode();
    }

    @Override
    protected boolean processEventObj(FCAddressUpdateEventObj eventObject) {

        try {
            return sellerSupcShipFromMappingProcessor.processChangeOfFCAddressForExternalNotification(eventObject);
        } catch (Exception e) {
            LOG.error("Exception while processing the request for event: {}. Exception {}", eventObject, e);
        }

        return false;
    }
}
