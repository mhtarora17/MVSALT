/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 06-Sep-2013
 *  @author himanshum
 */
package com.snapdeal.cocofs.services.jobprocessor;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.snapdeal.cocofs.common.BulkUploadValidationResponse;
import com.snapdeal.cocofs.common.JobProcessorResponse;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.entity.JobDetail;
import com.snapdeal.cocofs.entity.ProductAttribute;
import com.snapdeal.cocofs.excelupload.ProductWeightUploadDTO;
import com.snapdeal.cocofs.mongo.model.ProductAttributeUnit;
import com.snapdeal.cocofs.services.IDataReader;
import com.snapdeal.cocofs.services.IDataUpdater;
import com.snapdeal.cocofs.services.IJobProcessor;
import com.snapdeal.cocofs.services.IJobSchedulerService;
import com.snapdeal.cocofs.utils.ExcelReader;
import com.snapdeal.cocofs.utils.FTPUtils;
import com.sun.media.sound.InvalidDataException;

@Component
public class WeightUploadProcessor implements IJobProcessor {

    private static final Logger  LOG = LoggerFactory.getLogger(WeightUploadProcessor.class);

    @Autowired
    @Qualifier("productWeightUploadReader")
    private IDataReader<ProductWeightUploadDTO, ProductAttribute, ProductAttributeUnit> dataReader;

//    @Autowired
//    @Qualifier("productWeightUploadMerger")
//    private IDataEngine<ProductWeightUploadDTO, ProductAttribute, ProductAttributeUnit> dataMerger;
//    
    
    @Autowired
    private IDataUpdater dataUpdaterService;
    @Autowired
    private IJobSchedulerService jobService;

    @PostConstruct
    public void init() {
        jobService.addJobProcessor("WeightUpdate", this);
    }

    @Override
    public JobProcessorResponse process(JobDetail jobDetail) {
        JobProcessorResponse response = new JobProcessorResponse();
        LOG.info("processing file:" + jobDetail.getFileName() + " from path:" + jobDetail.getFilePath() + " status:" + jobDetail.getStatus().getCode());
        String ftpServerPath = ConfigUtils.getStringScalar(Property.COCOFS_FTP_SERVER_PATH);
        String ftpUsername = ConfigUtils.getStringScalar(Property.COCOFS_FTP_USERNAME);
        String ftpPassword = ConfigUtils.getStringScalar(Property.COCOFS_FTP_PASSWORD);
        
        String filepath = jobDetail.getFilePath() + File.separator + jobDetail.getFileName();
        
        // We save file on identical paths on local FS and FTP
        FTPUtils.downloadFile(ftpServerPath, ftpUsername, ftpPassword, filepath, filepath);
        boolean atleastOneRecordProcessed = false;
        Map<String, List<String>> errorMap =  new HashMap<String, List<String>>();
        try {
            List<ProductWeightUploadDTO> uploadList = ExcelReader.readFile(filepath, ProductWeightUploadDTO.class, false);
            for ( ProductWeightUploadDTO dto : uploadList) {
//                try {
////                    dataUpdaterService.updateDataWithDTO(dto, dataReader, dataMerger, jobDetail.getUploadedBy());
//                    atleastOneRecordProcessed = true;
//                } catch (GenericPersisterException e) {
//                    LOG.info("Could not update dto " + dto , e);
//                    if ( ! errorMap.containsKey("RowError")) {
//                        errorMap.put("RowError", new ArrayList<String>());
//                    }
//                    errorMap.get("RowError").add("Error in row with SUPC : " + dto.getSupc() + " Error : " + e.getMessage());
//                } catch (OperationNotSupportedException e) {
//                    e.printStackTrace();
//                }
            }
        } catch (InvalidDataException e) {
            updateErrorMapForFileError(jobDetail, errorMap, e);
            LOG.info("File Upload " + filepath + " corresponding to job detail " + jobDetail.getFileCode() + " could not be processed ", e);
        } catch (IOException e) {
            updateErrorMapForFileError(jobDetail, errorMap, e);
            LOG.info("File Upload " + filepath + " corresponding to job detail " + jobDetail.getFileCode() + " could not be processed ", e);
        } catch (ClassNotFoundException e) {
            updateErrorMapForFileError(jobDetail, errorMap, e);
            LOG.info("File Upload " + filepath + " corresponding to job detail " + jobDetail.getFileCode() + " could not be processed ", e);
        }
        response.setProcessingSuccessful(atleastOneRecordProcessed);
        return response;
    }
    
    private void updateErrorMapForFileError(JobDetail jobDetail, Map<String, List<String>> errorMap, Exception e) {
        if ( ! errorMap.containsKey("FileError")) {
            errorMap.put("FileError", new ArrayList<String>());
            
        }
        errorMap.get("FileError").add("Error in reading file for job " + jobDetail.getFileCode() + " Error Message " + e.getMessage());
    }

    @Override
    public BulkUploadValidationResponse validateJob(File file, String actionCode, String param) {
        BulkUploadValidationResponse response=new BulkUploadValidationResponse();
        response.setValid(true);
        return response;
    }


}
