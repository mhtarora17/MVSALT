/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.services.taxclass.dto;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @version 1.0, 23-Dec-2015
 * @author nitish
 */

@XmlRootElement
public class TaxClassDTO {

    private String taxClass;

    public TaxClassDTO(String taxClass) {
        super();
        this.taxClass = taxClass;
    }

    public String getTaxClass() {
        return taxClass;
    }

    public void setTaxClass(String taxClass) {
        this.taxClass = taxClass;
    }

    @Override
    public String toString() {
        return "TaxClassDTO [taxClass=" + taxClass + "]";
    }

}
