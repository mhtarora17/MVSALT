package com.snapdeal.cocofs.services.fm;

import java.util.List;

import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.services.fm.dto.SellerFmFcDTO;
import com.snapdeal.cocofs.services.fm.dto.SellerSupcFmFcDTO;

public interface IFMDataSourceService {

	/**
	 * Returns the fulfilment model by sellercode and subcat 
	 * 
	 * @param sellerCode
	 * @param supc
	 * @param subcat
	 * @return fulfilmentModel
	 * @throws ExternalDataNotFoundException
	 */
	public String getFulfilmentModelBySellerAndSubcat(String sellerCode, String supc, String subcat) throws ExternalDataNotFoundException;

	/**
	 * Returns the fulfilment model by sellercode and supc 
	 * 
	 * @param sellerCode
	 * @param supc
	 * @return fulfilmentModel
	 * @throws ExternalDataNotFoundException
	 */
	public String getFulfilmentModelBySellerAndSupc(String sellerCode, String supc) throws ExternalDataNotFoundException;

	/**
	 * Returns the fulfilment model by sellercode and subcat 
	 * 
	 * @param sellerCode
	 * @return fulfilmentModel
	 * @throws ExternalDataNotFoundException
	 */
	public String getFulfilmentModelBySeller(String sellerCode) throws ExternalDataNotFoundException;

	/**
	 * Returns True if seller exists in Data Source otherwise return False.
	 * 
	 * @param sellerCode
	 * @return isSellerExist
	 * @throws ExternalDataNotFoundException
	 */
	public boolean isSellerExists(String sellerCode) throws ExternalDataNotFoundException;
	
	/**
	 * Returns the SellerFmFc Mapping
	 * 
	 * @param sellerCode
	 * @return SellerFmFcDTO
	 * @throws ExternalDataNotFoundException
	 */
	public SellerFmFcDTO getSellerFmFcDTO(String sellerCode) throws ExternalDataNotFoundException;

	/**
	 * Returns the SellerSupcFmFc Mapping
	 * 
	 * @param sellerCode
	 * @return SellerFmFcDTO
	 * @throws ExternalDataNotFoundException
	 */
	public SellerSupcFmFcDTO getSellerSupcFmFcDTO(String sellerCode, String supc) throws ExternalDataNotFoundException;

	
	/**
     * Returns the centers by sellercode 
     * 
     * @param sellerCode
     * @return centers
     * @throws ExternalDataNotFoundException
     */
    List<String> getFCBySeller(String sellerCode) throws ExternalDataNotFoundException;

}
