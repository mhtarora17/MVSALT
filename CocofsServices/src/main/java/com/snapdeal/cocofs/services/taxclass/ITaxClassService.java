package com.snapdeal.cocofs.services.taxclass;

import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.services.taxclass.dto.SubcatTaxClassMappingDTO;
import com.snapdeal.cocofs.services.taxclass.dto.SupcTaxClassMappingDTO;

public interface ITaxClassService {

    String getTaxClassBySupcSubcatPair(String supc, String subcat) throws ExternalDataNotFoundException;

    SubcatTaxClassMappingDTO getTaxClassBySubcat(String subcat) throws ExternalDataNotFoundException;

    SupcTaxClassMappingDTO getTaxClassBySupc(String supc) throws ExternalDataNotFoundException;

    SupcTaxClassMappingDTO getTaxClassBySupcFromDataSource(String key) throws ExternalDataNotFoundException;

    SubcatTaxClassMappingDTO getTaxClassBySubcatFromDataSource(String key) throws ExternalDataNotFoundException;

}
