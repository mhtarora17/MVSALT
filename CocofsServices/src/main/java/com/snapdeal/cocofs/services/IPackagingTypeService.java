/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 9, 2013
 *  @author himanshu
 */
package com.snapdeal.cocofs.services;

import com.snapdeal.cocofs.common.PackagingTypeForSUPCResponse;
import com.snapdeal.cocofs.mongo.model.ProductAttributeUnit;

public interface IPackagingTypeService {
    
    public PackagingTypeForSUPCResponse getPackagingTypeForSUPC(String supc);

    PackagingTypeForSUPCResponse getPackagingType(ProductAttributeUnit paUnit, String supc);

    PackagingTypeForSUPCResponse getPackagingType(ProductAttributeUnit paUnit, String supc, String categoryUrl);

    PackagingTypeForSUPCResponse getPackagingTypeForSUPC(String supc, String categoryUrl);

}
