/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 28-Aug-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.services.task.dto;

import java.util.ArrayList;
import java.util.List;

import com.snapdeal.base.model.common.ServiceResponse;

public class TaskDetailUpdateResponseDTO extends ServiceResponse {

    /**
     * 
     */
    private static final long serialVersionUID = 5807879885857334027L;

    private List<String>      failedList       = new ArrayList<String>(0);

    public List<String> getFailedList() {
        return failedList;
    }

    public void setFailedList(List<String> failedList) {
        this.failedList = failedList;
    }

}
