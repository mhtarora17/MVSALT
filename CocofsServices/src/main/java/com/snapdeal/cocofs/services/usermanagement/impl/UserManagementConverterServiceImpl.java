/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 10-Nov-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.services.usermanagement.impl;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.entity.User;
import com.snapdeal.cocofs.entity.UserRole;
import com.snapdeal.cocofs.services.usermanagement.IUserManagementConverterService;

@Service("UserManagementConverterServiceImpl")
public class UserManagementConverterServiceImpl implements IUserManagementConverterService {

    private static final Logger LOG = LoggerFactory.getLogger(UserManagementConverterServiceImpl.class);

    @Override
    public UserDTO getUserDTO(User user) {
        if (user != null) {
            UserDTO dto = new UserDTO();
            dto.setId(user.getId());
            dto.setCreated(user.getCreated());
            dto.setDisplayName(user.getDisplayName());
            dto.setEmail(user.getEmail());
            dto.setEmailVerificationCode(user.getEmailVerificationCode());
            dto.setEnabled(user.isEnabled());
            dto.setFirstName(user.getFirstName());
            dto.setLastName(user.getLastName());
            dto.setMiddleName(user.getMiddleName());
            dto.setUserRoles(getUserRoles(user.getUserRoleMapping()));
            return dto;
        }
        return null;
    }

    private Set<String> getUserRoles(Set<UserRole> userRoleMapping) {
        Set<String> userRoleCodes = new HashSet<String>();
        for (UserRole userRole : userRoleMapping) {
            if (userRole.isEnabled()) {
                userRoleCodes.add(userRole.getRole().getCode());
            }
        }
        return userRoleCodes;
    }

}
