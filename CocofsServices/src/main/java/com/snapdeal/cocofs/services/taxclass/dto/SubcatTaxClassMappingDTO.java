/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.services.taxclass.dto;

import javax.xml.bind.annotation.XmlRootElement;

import com.snapdeal.cocofs.annotation.IgnoreInDownloadTemplate;
import com.snapdeal.cocofs.core.aerospike.vo.SubcatTaxClassMappingVO;
import com.snapdeal.cocofs.entity.SubcatTaxClassMapping;

/**
 * @version 1.0, 23-Dec-2015
 * @author nitish
 */

@XmlRootElement
public class SubcatTaxClassMappingDTO {

    private String  subcat;

    private String  taxClass;

    @IgnoreInDownloadTemplate
    private Boolean enabled;

    public SubcatTaxClassMappingDTO() {
    }

    public SubcatTaxClassMappingDTO(SubcatTaxClassMapping entity) {
        super();
        this.subcat = entity.getSubcat();
        this.taxClass = entity.getTaxClass();
        this.enabled = entity.isEnabled();
    }

    public SubcatTaxClassMappingDTO(SubcatTaxClassMappingVO subcatTaxClassMappingVO) {
        super();
        this.subcat = subcatTaxClassMappingVO.getSubcat();
        this.taxClass = subcatTaxClassMappingVO.getTaxClass();
        this.enabled = subcatTaxClassMappingVO.getEnabled() == 1 ? true : false;
    }

    public String getSubcat() {
        return subcat;
    }

    public void setSubcat(String subcat) {
        this.subcat = subcat;
    }

    public String getTaxClass() {
        return taxClass;
    }

    public void setTaxClass(String taxClass) {
        this.taxClass = taxClass;
    }

    public Boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public String toString() {
        return "SubcatTaxClassMappingDTO [subcat=" + subcat + ", taxClass=" + taxClass + ", enabled=" + enabled + "]";
    }

}
