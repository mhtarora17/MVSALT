package com.snapdeal.cocofs.services;

import java.util.Date;

/**
 * @author shiv
 */
public interface IPendingPAUPlayService {
    public void promotePendingPAUs(Date startDate, Date endDate);
}
