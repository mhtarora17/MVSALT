/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.services.taxrate.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.annotations.Throttled;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.entity.StateCategoryPriceTaxRate;
import com.snapdeal.cocofs.generic.persister.exception.GenericPersisterException;
import com.snapdeal.cocofs.generic.persister.request.GenericPersisterRequest;
import com.snapdeal.cocofs.generic.persister.response.GenericPersisterResponse;
import com.snapdeal.cocofs.generic.persister.service.IGenericPersisterService;
import com.snapdeal.cocofs.mongo.model.StateCategoryPriceTaxRateUnit;
import com.snapdeal.cocofs.services.taxrate.ITaxRateUpdater;

/**
 *  
 *  @version     1.0, 19-Aug-2014
 *  @author ankur
 */

@Service("TaxRateDataUpdateImpl")
public class TaxRateDataUpdateImpl implements ITaxRateUpdater {
    private static final Logger      LOG = LoggerFactory.getLogger(TaxRateDataUpdateImpl.class);

    @Autowired
    private IGenericPersisterService persistenceService;

    @Override
    @Throttled(overridingProperty = Property.SELLER_SUPC_UPLOAD_THROTTLE_PROPERTY)
    public GenericPersisterResponse<StateCategoryPriceTaxRate, StateCategoryPriceTaxRateUnit> updateStateCategoryPriceTaxRate(List<StateCategoryPriceTaxRate> entityListToSave,
            List<StateCategoryPriceTaxRateUnit> docListToSave) throws GenericPersisterException {
        GenericPersisterRequest<StateCategoryPriceTaxRate, StateCategoryPriceTaxRateUnit> req = new GenericPersisterRequest<StateCategoryPriceTaxRate, StateCategoryPriceTaxRateUnit>();
        req.setDocumentsToPersist(docListToSave);
        req.setEntitiesToPersist(entityListToSave);
        req.setAsynchToMongo(false);
        req.setSkipValidation(true);
        GenericPersisterResponse<StateCategoryPriceTaxRate, StateCategoryPriceTaxRateUnit> resp = persistenceService.persistOne(req);
        return resp;
    }

}
