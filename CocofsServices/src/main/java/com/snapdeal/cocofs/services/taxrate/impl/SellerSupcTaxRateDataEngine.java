/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.services.taxrate.impl;

import java.util.ArrayList;
import java.util.List;

import javax.naming.OperationNotSupportedException;

import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.entity.SellerSupcTaxRate;
import com.snapdeal.cocofs.mongo.model.SellerSupcTaxRateUnit;
import com.snapdeal.cocofs.services.taxrate.dto.SellerSUPCTaxRateUploadDTO;

/**
 * @version 1.0, 18-Aug-2014
 * @author ankur
 */

@Service("SellerSupcTaxRateDataEngine")
public class SellerSupcTaxRateDataEngine extends AbstractTaxRateDataEngine<SellerSUPCTaxRateUploadDTO, SellerSupcTaxRate, SellerSupcTaxRateUnit> {

    @Override
    public List<SellerSupcTaxRateUnit> enrichDocuments(SellerSUPCTaxRateUploadDTO dto, List<SellerSupcTaxRateUnit> documents) throws OperationNotSupportedException {
        if (dto != null) {
            SellerSupcTaxRateUnit document = null;
            List<SellerSupcTaxRateUnit> documentList = new ArrayList<SellerSupcTaxRateUnit>();
            if (null != documents && !documents.isEmpty()) {
                document = documents.get(0);
            } else {
                document = new SellerSupcTaxRateUnit();
            }
            updateSellerCategoryTaxRateUnit(dto, document);
            documentList.add(document);
            return documentList;
        }
        return null;
    }

    private void updateSellerCategoryTaxRateUnit(SellerSUPCTaxRateUploadDTO dto, SellerSupcTaxRateUnit document) {
        enrichTaxRateUnit(dto, document);
        document.setSupc(dto.getSupc());
        document.setSellerCode(dto.getSellerCode());

    }

    @Override
    public List<SellerSupcTaxRate> enrichEntities(SellerSUPCTaxRateUploadDTO dto, List<SellerSupcTaxRate> entities, String userEmail) throws OperationNotSupportedException {
        if (null != dto) {
            SellerSupcTaxRate entity = null;
            List<SellerSupcTaxRate> entityList = new ArrayList<SellerSupcTaxRate>();
            if (null != entities && !entities.isEmpty()) {
                entity = entities.get(0);
            } else {
                entity = new SellerSupcTaxRate();
            }
            updateSellerCategoryTaxRate(dto, entity, userEmail);
            entityList.add(entity);
            return entityList;
        }
        return null;
    }

    private void updateSellerCategoryTaxRate(SellerSUPCTaxRateUploadDTO dto, SellerSupcTaxRate entity, String userEmail) {
        enrichTaxRateEntity(dto, entity, userEmail);
        entity.setSupc(dto.getSupc());
        entity.setSellerCode(dto.getSellerCode());
    }

}
