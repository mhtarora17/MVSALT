/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 04-Sep-2013
 *  @author himanshum
 */
package com.snapdeal.cocofs.services;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.snapdeal.cocofs.entity.JobDetail;
import com.snapdeal.cocofs.entity.JobStatus;

public interface IJobService {

    /**
     * Get all job details
     * @return
     */
    List<JobDetail> getAllJobDetails();

    /**
     * Get all job details for given status list
     * @param jobStatusList
     * @return
     */
    List<JobDetail> getAllJobDetails(List<JobStatus> jobStatusList);

    /**
     * Update JobDetail to given new status and record history for this act
     * @param jobDetail
     * @param jobStatusCode
     * @return
     */
    JobDetail updateJobStatus(JobDetail jobDetail, String jobStatusCode);

    /**
     * Get JobDetail by given file/job code
     * @param fileCode
     * @return
     */
    JobDetail getJobDetailByCode(String fileCode);

    /**
     * Given File that has same path name as already saved in FTP
     * create a new Job with given action type, recording the user
     * asked
     * @param file
     * @param actionCode
     * @param username
     * @return
     */
    JobDetail saveJob(File file, String actionCode, String username);

    /**
     * Extra version of the method above with an added parameter
     * @param file
     * @param actionCode
     * @param username
     * @return
     */
    JobDetail saveJob(File file, String actionCode, String username, String param);

    
    /**
     * Saves multipart file to the disk and returns the file object associated with it 
     * @param inputFile
     * @return
     * @throws IOException
     */
    File saveToDisk(MultipartFile inputFile) throws IOException;
    
    
    Integer getAllJobDetailsRowCountPagination(JobStatus status, Date startDate, Date endDate);

    /**
     * Returns Job Detail created in date range (end date inclusiv) with given date, sorted by given JobDetail field in given 
     * order ( asc or desc )
     * If sortByField is not specified (it is null or empty string) 'created' is used for sorting
     * If sortOrder not specified (it is null or empty string) 'desc' is used for sorting
     * @param status
     * @param startRow
     * @param count
     * @param startDate
     * @param endDate
     * @param sortByField
     * @param sortOrder
     * @return
     */
    List<JobDetail> getAllJobDetailsWithPagination(JobStatus status, int startRow, int count, Date startDate, Date endDate,  String sortByField, String sortOrder);
/*
 * Returns count for my jobs
 */
    Integer getMyJobDetailsRowCountPagination(JobStatus status, Date startDate, Date endDate,String user);

    /**
     * Returns Job Detail of the current user created in date range (end date inclusiv) with given date, sorted by given JobDetail field in given 
     * order ( asc or desc )
     * If sortByField is not specified (it is null or empty string) 'created' is used for sorting
     * If sortOrder not specified (it is null or empty string) 'desc' is used for sorting
     * @param status
     * @param startRow
     * @param count
     * @param startDate
     * @param endDate
     * @param sortByField
     * @param sortOrder
     * @param user
     * @return
     */
	List<JobDetail> getMyJobDetailsWithPagination(JobStatus status,int startRow, int count, Date startDate, Date endDate,String sortByField, String sortOrder, String user);
/*
 * Update status of job from VALD to CANC or HOLD
 */
	String updateStatus(String id,String button,String username);

	Boolean updateJob(JobDetail job, String username);


	
	
}
