/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.services.events.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * @version 1.0, 25-Mar-2015
 * @author shiv
 */
public class SellerSupcAddEventObj implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3920851214964657230L;
    private String            sellerCode;
    private String            supc;
    private Date              created;

    public SellerSupcAddEventObj() {
    }

    public SellerSupcAddEventObj(String sellerCode, String supc) {
        this.sellerCode = sellerCode;
        this.setSupc(supc);
        this.created = new Date();
    }

    public String getSellerCode() {
        return sellerCode;
    }

    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }

  
    public String getSupc() {
        return supc;
    }

    public void setSupc(String supc) {
        this.supc = supc;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Override
    public String toString() {
        return "SellerSupcAddEventObj [sellerCode=" + sellerCode + ", supc=" + supc + ", created=" + created + "]";
    }
    
}
