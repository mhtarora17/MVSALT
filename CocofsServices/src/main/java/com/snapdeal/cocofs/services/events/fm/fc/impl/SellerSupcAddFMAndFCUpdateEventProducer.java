/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.services.events.fm.fc.impl;

import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.entity.EventInstance;
import com.snapdeal.cocofs.services.events.dto.SellerSupcAddEventObj;
import com.snapdeal.cocofs.services.events.impl.BaseEventProducer;
import com.snapdeal.cocofs.services.events.impl.EventType;

/**
 * @version 1.0, 25-Mar-2015
 * @author shiv
 */

@Service("SellerSupcAddFMAndFCUpdateEventProducer")
public class SellerSupcAddFMAndFCUpdateEventProducer extends BaseEventProducer<SellerSupcAddEventObj> {

    @Override
    protected String getEventCode() {
        return EventType.FC_UPDATE_EVENT_ON_SELLER_SUPC_ADD.getCode();
    }

    @Override
    public void setParams(EventInstance eventInstance, SellerSupcAddEventObj eventObj) {
        eventInstance.setParam1(eventObj.getSellerCode());
        eventInstance.setParam2(eventObj.getSupc());
        eventInstance.setCreated(eventObj.getCreated());
    }

}
