/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.services.lookup.impl;


import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.entity.SupcPackagingTypeMapping;
import com.snapdeal.cocofs.services.ISupcPackagingTypeMappingLookupService;
import com.snapdeal.cocofs.services.ISupcToPackagingTypeMappingService;
import com.snapdeal.cocofs.services.common.dto.SupcPackagingTypeMappingDownloadData;
import com.snapdeal.packman.dataaccess.supcpt.ISupcPackagingTypeMongoService;

/**
 *  
 *  @version     1.0, 23-Dec-2015
 *  @author vikassharma03
 */

@Service("supcPackagingTypeMappingLookupService")
public class SupcPackagingTypeMappingLookupServiceImpl implements ISupcPackagingTypeMappingLookupService {
    
    private static final Logger                LOG = LoggerFactory.getLogger(SupcPackagingTypeMappingLookupServiceImpl.class);

    
    @Autowired
    ISupcToPackagingTypeMappingService  supcToPackagingTypeMappingService;


    
    @Autowired
    ISupcPackagingTypeMongoService   supcPackagingTypeMongoService;
    
    
    
    
    @Override
    public List<SupcPackagingTypeMappingDownloadData> getSupcPackagingTypeDownloadData(String supc) {    
        List<SupcPackagingTypeMapping> mappingList = supcToPackagingTypeMappingService.getSupcPackagignTypeMappingListBySUPC(supc);
        List<SupcPackagingTypeMappingDownloadData> mappingData = new ArrayList<SupcPackagingTypeMappingDownloadData>();
        for (SupcPackagingTypeMapping mapping : mappingList) {
            mappingData.add(new SupcPackagingTypeMappingDownloadData(mapping.getSupc(), mapping.getStoreCode(), mapping.getPackagingType(), mapping.isEnabled(),mapping.getCreated(),mapping.getLastUpdated(),mapping.getUpdatedBy()));
        }
        return mappingData;
    }
}