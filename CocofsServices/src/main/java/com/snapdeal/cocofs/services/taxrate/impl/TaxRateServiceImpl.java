package com.snapdeal.cocofs.services.taxrate.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.axis.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.base.utils.CollectionUtils;
import com.snapdeal.catalog.base.model.GetMinProductInfoBySupcRequest;
import com.snapdeal.catalog.base.model.GetMinProductInfoResponse;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.dataaccess.taxrate.ITaxRateMongoService;
import com.snapdeal.cocofs.entity.AbstractTaxRate;
import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.external.service.factory.IExternalServiceFactory;
import com.snapdeal.cocofs.model.request.GetTaxClassRequest;
import com.snapdeal.cocofs.model.request.GetTaxInfoRequest;
import com.snapdeal.cocofs.model.response.GetTaxClassResponse;
import com.snapdeal.cocofs.model.response.GetTaxInfoResponse;
import com.snapdeal.cocofs.mongo.model.SellerCategoryTaxRateUnit;
import com.snapdeal.cocofs.mongo.model.SellerSupcTaxRateUnit;
import com.snapdeal.cocofs.mongo.model.StateCategoryPriceTaxRateUnit;
import com.snapdeal.cocofs.services.taxrate.ITaxRateservice;
import com.snapdeal.cocofs.sro.TaxSRO;
import com.snapdeal.cocofs.sro.TaxType;

/**
 * Cocofs tax rate landing internal service implementation
 * 
 * @author gauravg
 */
@Service("TaxRateServiceImpl")
public class TaxRateServiceImpl implements ITaxRateservice {

    @Autowired
    private ITaxRateMongoService    taxRateMongoService;

    @Autowired
    private IExternalServiceFactory externalServiceFactory;

    private static final Logger     LOG = LoggerFactory.getLogger(TaxRateServiceImpl.class);

    @Override
    public GetTaxInfoResponse getTaxInfoBySellerSUPCState(GetTaxInfoRequest request) {

        //Business logic/Rules implementation

        //RULE 1
        GetTaxInfoResponse response = getTaxRateBySellerCodeAndSUPC(request);

        if (response == null) {
            //Enrich category from CAMS if category is not present in the request
            if (enrichRequest(request)) {
                //RULE 2
                response = getTaxRateBySellerCodeAndCat(request);

                if (response == null) {
                    //RULE 3
                    response = getTaxRateByCategoryStateAndPrice(request);

                }
            } else {
                //Enrichment was unsuccessful - category cannot be retrieved from CAMS
                LOG.info("Enrichment of request parameters was unsuccessful. Sending back an error response now.");
                response = generateFailureResponse("Could not get category from CAMS for this request.");
            }

            if (response == null) {
                //RULE 4
                response = getTaxRateByStateDefault(request);
            }
        }

        /*
         * If response is still null
         * this means that the system was not able to find the tax rate
         * using any of the predefined retrieval rules
         * => send failure response
         */
        if (response == null) {
            LOG.info("Unable to fetch tax rate information for this request:" + request);
            return generateFailureResponse("Unable to fetch tax rate information for this request.");
        }

        LOG.info("Sending response {}", response);
        return response;
    }

    /**
     * This method tell us whether tax type for given requests is of type CST or VAT
     * 
     * @param request
     * @return
     */
    private TaxType getTaxType(GetTaxInfoRequest request) {
        String customerState = request.getCustomerAddress().getState();
        String sellerState = request.getSellerAddress().getState();
        if (sellerState.equalsIgnoreCase(customerState)) {
            return TaxType.VAT;
        }
        return TaxType.CST;

    }

    /**
     * RULE 1: Try to fetch the tax rate based upon the seller/SUPC combination at the finest level
     * 
     * @return
     */
    private GetTaxInfoResponse getTaxRateBySellerCodeAndSUPC(GetTaxInfoRequest request) {

        LOG.debug("Applying RULE 1: Tax rate by Seller+SUPC...");
        SellerSupcTaxRateUnit taxRateUnitBySupc = taxRateMongoService.getSellerSupcTaxRateUnit(request.getSellerCode(), request.getSupc());
        if (taxRateUnitBySupc != null && taxRateUnitBySupc.isEnabled()) {
            if (!AbstractTaxRate.Type.SERVICE_TAX.code().equals(taxRateUnitBySupc.getTaxType())) {
                return generateSuccessResponse(taxRateUnitBySupc.getTaxRate(), getTaxType(request));
            } else {
                return generateSuccessResponse(taxRateUnitBySupc.getTaxRate(), TaxType.SERVICE_TAX);
            }
        }
        LOG.debug("Unable to fetch tax rate using RULE 1.");
        return null;

    }

    /**
     * If no tax rate information available through rule 1 RULE 2: Try to fetch the tax rate based upon the
     * seller/category combination at the next level
     * 
     * @return
     */
    private GetTaxInfoResponse getTaxRateBySellerCodeAndCat(GetTaxInfoRequest request) {
        LOG.debug("Applying RULE 2: Tax rate by Seller+Category...");
        SellerCategoryTaxRateUnit taxRateUnitByCat = taxRateMongoService.getSellerCategoryTaxRateUnit(request.getSellerCode(), request.getCategory());
        if (taxRateUnitByCat != null && taxRateUnitByCat.isEnabled()) {
            if (!AbstractTaxRate.Type.SERVICE_TAX.code().equals(taxRateUnitByCat.getTaxType())) {
                return generateSuccessResponse(taxRateUnitByCat.getTaxRate(), getTaxType(request));
            } else {
                return generateSuccessResponse(taxRateUnitByCat.getTaxRate(), TaxType.SERVICE_TAX);
            }
        }

        LOG.debug("Unable to fetch tax rate using RULE 2.");
        return null;
    }

    /**
     * If no tax rate information available through rules 1 and 2 RULE 3: Try to fetch the tax rate based upon the
     * category/state/Price combination at the coarse level
     * 
     * @return
     */
    private GetTaxInfoResponse getTaxRateByCategoryStateAndPrice(GetTaxInfoRequest request) {
        LOG.debug("Applying RULE 3: Tax rate by Category+State+Price...");

        List<StateCategoryPriceTaxRateUnit> taxRateUnitsByPrice = taxRateMongoService.getEnabledStateCategoryPriceTaxRateUnit(request.getSellerAddress().getState(),
                request.getCategory());
        List<StateCategoryPriceTaxRateUnit> filteredList = filterStateCategoryPriceTaxRateUnitByTaxType(taxRateUnitsByPrice, AbstractTaxRate.Type.VAT_CST.code());

        if (CollectionUtils.isEmpty(filteredList)) {
            filteredList = filterStateCategoryPriceTaxRateUnitByTaxType(taxRateUnitsByPrice, AbstractTaxRate.Type.SERVICE_TAX.code());
        }

        if (filteredList != null) {
            StateCategoryPriceTaxRateUnit filteredTaxRateUnit = findTaxRateUnitByRange(request.getPrice(), filteredList);
            if (filteredTaxRateUnit != null && filteredTaxRateUnit.isEnabled()) {
                if (!AbstractTaxRate.Type.SERVICE_TAX.code().equals(filteredTaxRateUnit.getTaxType())) {
                    return generateSuccessResponse(filteredTaxRateUnit.getTaxRate(), getTaxType(request));
                } else {
                    return generateSuccessResponse(filteredTaxRateUnit.getTaxRate(), TaxType.SERVICE_TAX);
                }
            }
        }

        LOG.debug("Unable to fetch tax rate using RULE 3.");
        return null;
    }

    /**
     * If no tax rate information available through rules 1,2 and 3 RULE 4: Use default tax rates defined for states in
     * configuration
     */
    private GetTaxInfoResponse getTaxRateByStateDefault(GetTaxInfoRequest request) {
        LOG.debug("Applying RULE 4: Tax rate by State Default...");
        Map<String, String> defaultTaxRateMap = ConfigUtils.getMap(Property.DEFAULT_TAX_RATE_FOR_ALL_STATES);

        if (defaultTaxRateMap != null && defaultTaxRateMap.containsKey(request.getSellerAddress().getState())) {
            LOG.debug("RULE 4 successfully executed..");
            return generateSuccessResponse(Double.valueOf(defaultTaxRateMap.get(request.getSellerAddress().getState())), getTaxType(request));
        }
        LOG.info("Unable to fetch tax rate using any of the RULES.");
        return null;
    }

    private StateCategoryPriceTaxRateUnit findTaxRateUnitByRange(Integer price, List<StateCategoryPriceTaxRateUnit> taxRateUnitsByPrice) {

        for (StateCategoryPriceTaxRateUnit taxRateUnit : taxRateUnitsByPrice) {
            double lowerLimit = taxRateUnit.getLowerPriceLimit() == null ? -1 : taxRateUnit.getLowerPriceLimit();
            double upperLimit = taxRateUnit.getUpperPriceLimit() == null ? Double.MAX_VALUE : taxRateUnit.getUpperPriceLimit();
            //Need to cast here for comparison
            double dprice = (double) price;

            LOG.debug("Checking for LowerLimit=" + lowerLimit + " UpperLimit=" + upperLimit + " Price = " + dprice);
            if (dprice >= lowerLimit && dprice <= upperLimit) {
                return taxRateUnit;
            }

        }
        return null;
    }

    private List<StateCategoryPriceTaxRateUnit> filterStateCategoryPriceTaxRateUnitByTaxType(List<StateCategoryPriceTaxRateUnit> taxRateUnitsByPrice, String taxType) {
        List<StateCategoryPriceTaxRateUnit> filteredList = new ArrayList<StateCategoryPriceTaxRateUnit>();
        for (StateCategoryPriceTaxRateUnit taxRateUnit : taxRateUnitsByPrice) {
            if (StringUtils.isEmpty(taxRateUnit.getTaxType()) && AbstractTaxRate.Type.VAT_CST.code().equals(taxType)) {
                filteredList.add(taxRateUnit);
            } else if (taxType.equals(taxRateUnit.getTaxType())) {
                filteredList.add(taxRateUnit);
            }
        }

        return filteredList;
    }

    /**
     * Enrich request with the category if category is empty
     * 
     * @param request
     */
    private boolean enrichRequest(GetTaxInfoRequest request) {
        //If category is not provided in the request
        //fetch the information from CAMS
        if (StringUtils.isEmpty(request.getCategory())) {
            LOG.info("Category is not present in he request. Enriching the category from CAMS...");
            try {
                GetMinProductInfoResponse resp = externalServiceFactory.getCAMSExternalService().getMinProductInfo(new GetMinProductInfoBySupcRequest(request.getSupc()));
                LOG.debug("Response received from CAMS:" + resp);
                if (null != resp && resp.isSuccessful() && resp.getProductInfo() != null) {
                    LOG.debug("Setting category in the request as :" + resp.getProductInfo().getCategoryUrl());
                    request.setCategory(resp.getProductInfo().getCategoryUrl());
                } else {
                    LOG.info("Null/Unsuccessful response received from CAMS. Could not fetch category from CAMS.");
                    return false;
                }

            } catch (ExternalDataNotFoundException e) {
                LOG.error("Could not fetch category from CAMS :" + e.getMessage());
                return false;
            }

        } else {
            LOG.info("Enrichment not required. Category already present in the request.");
        }
        return true;
    }

    private GetTaxInfoResponse generateFailureResponse(String errorMessage) {
        GetTaxInfoResponse response = new GetTaxInfoResponse();
        response.setCode("FAILED");
        response.setSuccessful(false);
        response.setMessage(errorMessage);
        return response;
    }

    private GetTaxInfoResponse generateSuccessResponse(Double taxRate, TaxType taxType) {
        GetTaxInfoResponse response = new GetTaxInfoResponse();
        List<TaxSRO> taxes = new ArrayList<TaxSRO>();
        response.setCode("SUCCESS");
        response.setSuccessful(true);

        TaxSRO tax = new TaxSRO();
        tax.setTaxPercentage(taxRate);
        tax.setTaxType(taxType.getType());
        taxes.add(tax);

        response.setTaxes(taxes);
        return response;
    }

    //#############################################################################################################

}
