/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.services.events.sdinstant.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.services.events.dto.SellerSupcAddEventObj;
import com.snapdeal.cocofs.services.events.impl.BaseEventConsumer;
import com.snapdeal.cocofs.services.events.impl.EventType;
import com.snapdeal.cocofs.services.fm.ISellerSupcSdInstantMappingProcessor;

/**
 * @version 1.0, 25-Mar-2015
 * @author ankur
 */
@Service("SellerSupcAddSdInstantUpdateEventConsumer")
public class SellerSupcAddSdInstantUpdateEventConsumer extends BaseEventConsumer<SellerSupcAddEventObj> {

    @Autowired
    @Qualifier("SellerSUPCSdInstantMappingProcessorImpl")
    private ISellerSupcSdInstantMappingProcessor sellerSupcMappingProcessor;

    private static final Logger           LOG = LoggerFactory.getLogger(SellerSupcAddSdInstantUpdateEventConsumer.class);

    @Override
    protected String getEventCode() {
        return EventType.SDINSTANT_UPDATE_EVENT_ON_SELLER_SUPC_ADD.getCode();
    }

    @Override
    protected boolean processEventObj(SellerSupcAddEventObj eventObject) {

        try {
            return sellerSupcMappingProcessor.processAdditionOfNewSellerSUPCMappingForExternalNotification(eventObject);
        } catch (Exception e) {
            LOG.error("Exception while processing the request for event: {}. Exception {}", eventObject, e);
        }

        return false;
    }

}
