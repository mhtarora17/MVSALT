package com.snapdeal.cocofs.services.data.engine.impl;

import java.util.List;

import javax.naming.OperationNotSupportedException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.entity.ProductAttribute;
import com.snapdeal.cocofs.excelupload.ProductAttributeUploadDTO;
import com.snapdeal.cocofs.mongo.model.ProductAttributeUnit;
import com.snapdeal.cocofs.services.data.engine.IDataEngine;

/**
 * @author nikhil
 */

@Service("productAttributeEngine")
public class ProductAttributeEngine extends AbstractProductAttributeEngine {

    @Autowired
    @Qualifier("productAttributeCreateOnlyEngine")
    IDataEngine<ProductAttributeUploadDTO, ProductAttribute, ProductAttributeUnit> productAttributeCreateOnlyEngine;

    @Autowired
    @Qualifier("productAttributeUpdateOnlyEngine")
    IDataEngine<ProductAttributeUploadDTO, ProductAttribute, ProductAttributeUnit> productAttributeUpdateOnlyEngine;

    private static final Logger                                                    LOG = LoggerFactory.getLogger(AbstractProductAttributeEngine.class);

    @Override
    public List<ProductAttributeUnit> enrichDocuments(ProductAttributeUploadDTO dto, List<ProductAttributeUnit> documents) {

        try {
            if (documents == null || (documents != null && documents.isEmpty())) {
                return productAttributeCreateOnlyEngine.enrichDocuments(dto, documents);
            } else {
                return productAttributeUpdateOnlyEngine.enrichDocuments(dto, documents);
            }
        } catch (OperationNotSupportedException e) {
            LOG.error("Error in enriching the data for persistence : ", e);
        }

        return null;
    }

    @Override
    public List<ProductAttribute> enrichEntities(ProductAttributeUploadDTO dto, List<ProductAttribute> entities, String userEmail) {
        try {
            if (entities == null || (entities != null && entities.isEmpty())) {
                return productAttributeCreateOnlyEngine.enrichEntities(dto, entities, userEmail);
            } else {
                return productAttributeUpdateOnlyEngine.enrichEntities(dto, entities, userEmail);
            }
        } catch (OperationNotSupportedException e) {
            LOG.error("Error in enriching the data for persistence : ", e);
        }

        return null;
    }

}
