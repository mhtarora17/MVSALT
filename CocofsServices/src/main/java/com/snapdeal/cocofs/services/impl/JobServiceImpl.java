/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 04-Sep-2013
 *  @author himanshum
 */
package com.snapdeal.cocofs.services.impl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.io.Files;
import com.snapdeal.base.cache.CacheManager;
import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.cocofs.cache.JobCache;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.db.dao.IJobDao;
import com.snapdeal.cocofs.entity.JobDetail;
import com.snapdeal.cocofs.entity.JobDetailHistory;
import com.snapdeal.cocofs.entity.JobStatus;
import com.snapdeal.cocofs.services.IJobService;
import com.snapdeal.cocofs.utils.FTPUtils;

@Service("JobService")
public class JobServiceImpl implements IJobService {

    @Autowired
    private IJobDao             jobDao;

    private static final Logger LOG = LoggerFactory.getLogger(JobServiceImpl.class);

    @Override
    @Transactional(readOnly = true)
    public List<JobDetail> getAllJobDetails() {
        return jobDao.getAllJobDetails();
    }

    @Override
    @Transactional(readOnly = true)
    public List<JobDetail> getAllJobDetails(List<JobStatus> jobStatusList) {
        List<JobDetail> jobDetailList = new ArrayList<JobDetail>();
        for (JobStatus jobStatus : jobStatusList) {
            jobDetailList.addAll(jobDao.getAllJobDetails(jobStatus));
        }
        return jobDetailList;
    }

    @Transactional
    @Override
    public JobDetail saveJob(File file, String actionCode, String username) {
        JobDetail jobDetail = new JobDetail();
        enrichJobDetail(jobDetail, file, actionCode, username, "job uploaded through file upload by " + username);
        jobDetail = jobDao.saveJobDetail(jobDetail);
        JobDetailHistory jdh = enrichJobDetailHistory(jobDetail, JobStatus.Type.VALIDATED.getCode(), username);
        jdh = jobDao.saveJobDetailHistory(jdh);

        return jobDetail;
    }

    @Transactional
    @Override
    public JobDetail saveJob(File file, String actionCode, String username, String param) {
        JobDetail jobDetail = new JobDetail();
        enrichJobDetail(jobDetail, file, actionCode, username, param);
        jobDetail = jobDao.saveJobDetail(jobDetail);
        JobDetailHistory jdh = enrichJobDetailHistory(jobDetail, JobStatus.Type.VALIDATED.getCode(), username);
        jdh = jobDao.saveJobDetailHistory(jdh);

        return jobDetail;
    }

    @Override
    public File saveToDisk(MultipartFile inputFile) throws IOException {
        String dir = ConfigUtils.getStringScalar(Property.FILE_UPLOAD_LOCATION);
        String datedDir = getDatedDirForDate(new Date());
        String parentDirLocation = dir + File.separator + datedDir;
        String filePath = parentDirLocation + File.separator + StringUtils.getRandomAlphaNumeric(4) + "_" + System.currentTimeMillis() + "_" + inputFile.getOriginalFilename();
        File fileToSave = new File(filePath);
        Files.createParentDirs(fileToSave);
        if (fileToSave.createNewFile()) {
            inputFile.transferTo(fileToSave);
        }

        String ftpServerPath = ConfigUtils.getStringScalar(Property.COCOFS_FTP_SERVER_PATH);
        String ftpUsername = ConfigUtils.getStringScalar(Property.COCOFS_FTP_USERNAME);
        String ftpPassword = ConfigUtils.getStringScalar(Property.COCOFS_FTP_PASSWORD);
        String workingDir = parentDirLocation;
        FTPUtils.uploadFileToFtpLocation(fileToSave, ftpServerPath, ftpUsername, ftpPassword, workingDir, null);
        LOG.info("File uploaded to " + ftpServerPath + " folder " + workingDir + " file name " + fileToSave.getName());
        return fileToSave;

    }

    private String getDatedDirForDate(Date date) {
        Calendar c = new GregorianCalendar();
        c.setTime(date);
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH) + 1;
        int day = c.get(Calendar.DAY_OF_MONTH);
        String dateDir = String.format("%04d%s%02d%s%02d", year, File.separator, month, File.separator, day);
        return dateDir;
    }

    private JobDetailHistory enrichJobDetailHistory(JobDetail jobDetail, String nextJobStatusCode, String username) {
        JobDetailHistory jdh = new JobDetailHistory();
        jdh.setJobId(jobDetail.getId());
        jdh.setCreated(DateUtils.getCurrentTime());
        jdh.setPreviousStatus(jobDetail.getStatus() != null ? jobDetail.getStatus().getCode() : null);
        jdh.setNextStatus(nextJobStatusCode);
        jdh.setRemark("job with action code:" + jobDetail.getAction().getCode() + " requested by:" + username);
        return jdh;
    }

    private JobDetailHistory enrichJobDetailPriorityHistory(JobDetail jobDetail, String previousJobStatusCode, String newJobStatusCode, int previousPriority, int newpriority,
            int id, String username) {
        JobDetailHistory jdh = new JobDetailHistory();
        jdh.setJobId(id);
        jdh.setCreated(DateUtils.getCurrentTime());
        jdh.setPreviousStatus(previousJobStatusCode);
        jdh.setNextStatus(newJobStatusCode);
        jdh.setRemark("updated job priority from:" + previousPriority + " to:" + newpriority + " by:" + username);
        return jdh;
    }

    private void enrichJobDetail(JobDetail jobDetail, File file, String actionCode, String username, String paramRemarks) {
        jobDetail.setAction(CacheManager.getInstance().getCache(JobCache.class).getJobActionByCode(actionCode));
        jobDetail.setStatus(CacheManager.getInstance().getCache(JobCache.class).getJobStatusByCode(JobStatus.Type.VALIDATED.getCode()));
        jobDetail.setFileName(file.getName());
        jobDetail.setFileCode("dummyFile" + StringUtils.getRandomAlphaNumeric(4));
        jobDetail.setFilePath(file.getParent());
        jobDetail.setCreated(DateUtils.getCurrentTime());
        jobDetail.setLastUpdated(DateUtils.getCurrentTime());
        jobDetail.setUploadedBy(username);
        jobDetail.setEnabled(true);
        jobDetail.setRemarks(paramRemarks);
        // adding default job priority as COCOFS_JOB_DEFAULT_PRIORITY
        jobDetail.setPriority(getJobPriority());
        jobDao.flushJobDetail(jobDetail);
        jobDetail.setFileCode("F" + jobDetail.getId());
    }

    private int getJobPriority() {
        Integer priority = ConfigUtils.getIntegerScalar(Property.COCOFS_JOB_DEFAULT_PRIORITY);
        return priority;
    }

    @Override
    @Transactional(readOnly = true)
    public JobDetail getJobDetailByCode(String fileCode) {
        return jobDao.getJobDetailByCode(fileCode);
    }

    @Override
    @Transactional
    public JobDetail updateJobStatus(JobDetail jobDetail, String jobStatusCode) {
        JobDetailHistory jdh = enrichJobDetailHistory(jobDetail, jobStatusCode, "by task");
        jobDetail.setStatus(CacheManager.getInstance().getCache(JobCache.class).getJobStatusByCode(jobStatusCode));
        jobDao.saveJobDetailHistory(jdh);
        jobDetail.setLastUpdated(DateUtils.getCurrentTime());
        return jobDao.saveJobDetail(jobDetail);
    }

    @Override
    @Transactional(readOnly = true)
    public Integer getAllJobDetailsRowCountPagination(JobStatus status, Date startDate, Date endDate) {
        return jobDao.getAllJobDetailsRowCountPagination(status, startDate, endDate);
    }

    @Override
    @Transactional(readOnly = true)
    public List<JobDetail> getAllJobDetailsWithPagination(JobStatus status, int startRow, int count, Date startDate, Date endDate, String sortByField, String sortOrder) {
        return jobDao.getAllJobDetailsWithPagination(status, startRow, count, startDate, endDate, sortByField, sortOrder);
    }

    @Override
    @Transactional(readOnly = true)
    public Integer getMyJobDetailsRowCountPagination(JobStatus status, Date startDate, Date endDate, String user) {
        return jobDao.getMyJobDetailsRowCountPagination(status, startDate, endDate, user);
    }

    @Override
    @Transactional(readOnly = true)
    public List<JobDetail> getMyJobDetailsWithPagination(JobStatus status, int startRow, int count, Date startDate, Date endDate, String sortByField, String sortOrder, String user) {
        return jobDao.getMyJobDetailsWithPagination(status, startRow, count, startDate, endDate, sortByField, sortOrder, user);
    }

    @Override
    @Transactional
    public String updateStatus(String fileCode, String button, String username) {
        JobStatus js = null;
        if (button.equalsIgnoreCase("cancel")) {
            js = jobDao.getJobStatusByCode(JobStatus.Type.CANCELLED.getCode());
        } else if (button.equalsIgnoreCase("hold")) {
            js = jobDao.getJobStatusByCode(JobStatus.Type.HOLD.getCode());
        } else if (button.equalsIgnoreCase("unhold")) {
            js = jobDao.getJobStatusByCode(JobStatus.Type.VALIDATED.getCode());
        }
        JobStatus ValdJS = jobDao.getJobStatusByCode(JobStatus.Type.VALIDATED.getCode());
        JobStatus ExecJS = jobDao.getJobStatusByCode(JobStatus.Type.EXECUTED.getCode());
        JobStatus HoldJS = jobDao.getJobStatusByCode(JobStatus.Type.HOLD.getCode());
        JobDetail jd = jobDao.getJobDetailByCode(fileCode);
        if (jd.getStatus().getCode().equals(ValdJS.getCode())) {
            JobDetailHistory jdh = enrichJobDetailHistory(jd, js.getCode(), username);
            jdh = jobDao.saveJobDetailHistory(jdh);
            if (jobDao.updateStatus(jd, js))
                return "Status update successful:" + js.getCode();
            else
                return "Status update failed(query execution): " + js.getCode();
        } else if (jd.getStatus().getCode().equals(HoldJS.getCode())) {
            JobDetailHistory jdh = enrichJobDetailHistory(jd, js.getCode(), username);
            jdh = jobDao.saveJobDetailHistory(jdh);
            if (jobDao.updateStatus(jd, js))
                return "Status update successful: " + js.getCode();
            else
                return "Status update failed(query execution): "+js.getCode();
        } else if (jd.getStatus().getCode().equals(ExecJS.getCode()))
            return "Status update failed:Job scheduled by Job Poller while status update.";
        return "Status update failed";
    }

    @Override
    @Transactional
    public Boolean updateJob(JobDetail job, String username) {
        JobDetail savedJob = jobDao.getJobDetailByCode(job.getFileCode());
        LOG.info("Job Status fetched - " + savedJob);
        if (JobStatus.Type.VALIDATED.getCode().equals(savedJob.getStatus().getCode())) {
            int previousPriority = savedJob.getPriority();
            String actionCode = savedJob.getAction().getCode();
            savedJob.setPriority(job.getPriority());
            job.setLastUpdated(DateUtils.getCurrentTime());
            JobDetailHistory jdh = enrichJobDetailPriorityHistory(job, savedJob.getStatus().getCode(), JobStatus.Type.VALIDATED.getCode(), previousPriority, job.getPriority(),
                    savedJob.getId(), username);
            jdh = jobDao.saveJobDetailHistory(jdh);
            jobDao.saveJobDetail(savedJob);

            return true;
        }
        return false;
    }

}
