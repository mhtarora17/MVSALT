/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 07-Oct-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.services.taxclass.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.core.aerospike.vo.SellerFMMappingVO;
import com.snapdeal.cocofs.core.aerospike.vo.SupcTaxClassMappingVO;
import com.snapdeal.cocofs.dataaccess.fm.IFMAerospikeDataReadService;
import com.snapdeal.cocofs.dataaccess.fm.IFMDBDataReadService;
import com.snapdeal.cocofs.dataaccess.taxclass.ITaxClassAerospikeDataReadService;
import com.snapdeal.cocofs.dataaccess.taxclass.ITaxClassDBDataReadService;
import com.snapdeal.cocofs.entity.SellerFMMapping;
import com.snapdeal.cocofs.entity.SupcTaxClassMapping;
import com.snapdeal.cocofs.services.IDataReaderWithAerospike;
import com.snapdeal.cocofs.services.fm.dto.SellerFMMappingUpdateDto;
import com.snapdeal.cocofs.services.taxclass.dto.SupcTaxClassMappingDTO;

@Service("SupcTaxClassMappingDataReader")
public class SupcTaxClassMappingDataReader implements IDataReaderWithAerospike<SupcTaxClassMappingDTO, SupcTaxClassMapping, SupcTaxClassMappingVO> {

    @Autowired
    private ITaxClassDBDataReadService        taxClassDBDataReadService;

    @Autowired
    private ITaxClassAerospikeDataReadService taxClassAerospikeReadService;

    @Override
    public List<SupcTaxClassMapping> getEntitiesForDTO(SupcTaxClassMappingDTO dto) {
        SupcTaxClassMapping mapping = taxClassDBDataReadService.getSupcTaxClassMapping(dto.getSupc());
        List<SupcTaxClassMapping> mappingList = new ArrayList<SupcTaxClassMapping>();
        if (null != mapping) {
            mappingList.add(mapping);
        }
        return mappingList;
    }

    @Override
    public SupcTaxClassMappingVO getRecordForDTO(SupcTaxClassMappingDTO dto) {
        SupcTaxClassMappingVO record = (SupcTaxClassMappingVO) taxClassAerospikeReadService.getSupcTaxClassMapping(dto.getSupc(), SupcTaxClassMappingVO.class);

        return record;
    }
}
