/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.services;

import java.util.List;

import com.snapdeal.cocofs.entity.SupcPackagingTypeMapping;
import com.snapdeal.packaman.storecodeandpt.dto.ScAndPtDownloadDto;

/**
 * @version 1.0, 21-Dec-2015
 * @author vikassharma03
 */
public interface ISupcToPackagingTypeMappingService {

    public SupcPackagingTypeMapping persistSupcPackagingTypeMappingDetails(SupcPackagingTypeMapping supcPackagingTypeMappingDetail);

    public SupcPackagingTypeMapping getSupcPackagingTypeMappingBySupcAndStoreCode(String supc, String StoreCode);

    public List<SupcPackagingTypeMapping> getSupcPackagignTypeMappingListBySUPC(String supc);
    
    public List<ScAndPtDownloadDto> getAllSCs();
    
   // public String getStoreCodeForRoleCode(String roleCode);

 }
