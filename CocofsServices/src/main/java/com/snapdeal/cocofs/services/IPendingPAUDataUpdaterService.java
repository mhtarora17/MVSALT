package com.snapdeal.cocofs.services;

import java.util.List;

import com.snapdeal.cocofs.entity.PendingProductAttributeUpdate;

/**
 * @author nikhil
 */
public interface IPendingPAUDataUpdaterService {

    /**
     * 
     * @param updates
     * @param supc
     */
    public void promotePendingPAU(List<PendingProductAttributeUpdate> updates, String supc);

}
