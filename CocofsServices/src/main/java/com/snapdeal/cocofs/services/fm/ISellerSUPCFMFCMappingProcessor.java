package com.snapdeal.cocofs.services.fm;

import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.generic.persister.exception.GenericPersisterException;
import com.snapdeal.cocofs.services.events.dto.FCAddressUpdateEventObj;
import com.snapdeal.cocofs.services.events.dto.SellerSupcAddEventObj;
import com.snapdeal.score.data.aerospike.exception.GetFailedException;
import com.snapdeal.score.data.aerospike.exception.PutFailedException;

/**
 * 
 * Interface to handle seller-supc-fm-fc change notification updates
 * 
 * @author ssuthar
 */
public interface ISellerSUPCFMFCMappingProcessor extends ISellerSUPCFMMappingProcessor{

}