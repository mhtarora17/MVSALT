package com.snapdeal.cocofs.services.events.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SnapDealJsonData implements Serializable   
{  
	
	private static final long serialVersionUID = -1526503153618805194L;
	
	private String page;  
	private Double total;  
	private String records; 
	private List rows = new ArrayList();
	
	
	public String getPage() {
		return page;
	}
	
	public void setPage(String page) {
		this.page = page;
	}
	
	public Double getTotal() {
		return total;
	}
	
	public void setTotal(Double total) {
		this.total = total;
	}
	
	public String getRecords() {
		return records;
	}
	
	public void setRecords(String records) {
		this.records = records;
	}
	
	
	public List getRows() {
		return rows;
	}

	public void setRows(List rows) {
		this.rows = rows;
	}
	

}
