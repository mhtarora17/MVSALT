package com.snapdeal.cocofs.services.producer;

import java.util.List;

import com.snapdeal.cocofs.sro.SellerSupcFMFCUpdateSRO;

public interface IExternalNotificationsServiceForSellerSupcFMFCUpdate extends IQueueListener {

    boolean publishToQueue(List<SellerSupcFMFCUpdateSRO> msg);
}
