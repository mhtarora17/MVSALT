/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 25-Oct-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.services.task.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.configurations.IStartupService;
import com.snapdeal.cocofs.services.IJobSchedulerService;

public class JobPoller extends QuartzTask {

    private static final Logger LOG = LoggerFactory.getLogger(JobPoller.class);

    @Autowired
    IJobSchedulerService        jobSchedulerService;

    @Autowired
    IStartupService             startupService;

    @Override
    public void execute(String taskParam) {
        if (ConfigUtils.getBooleanScalar(Property.JOB_PROCESSING_ENABLED)) {
            LOG.info("Starting Job polling...");
            jobSchedulerService.startDaemonProcess();
        }
        LOG.info("Job Poller task ending...");
    }

}
