/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 10-Oct-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.services.fm.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.naming.OperationNotSupportedException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.aerospike.client.Log;
import com.snapdeal.base.utils.CollectionUtils;
import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.base.validation.ValidationError;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.core.aerospike.vo.SellerFMMappingVO;
import com.snapdeal.cocofs.core.aerospike.vo.SellerSubcatFMMappingVO;
import com.snapdeal.cocofs.core.concurrent.dto.ArgumentDTO;
import com.snapdeal.cocofs.core.concurrent.dto.ConcurrentRequest;
import com.snapdeal.cocofs.core.concurrentexecutor.ConcurrentRequestExecutor;
import com.snapdeal.cocofs.dataaccess.fm.IFMAerospikeDataReadService;
import com.snapdeal.cocofs.dataaccess.fm.IFMDBDataReadService;
import com.snapdeal.cocofs.entity.SellerFMMapping;
import com.snapdeal.cocofs.entity.SellerSubcatFMMapping;
import com.snapdeal.cocofs.entity.SellerSupcFMMapping;
import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.external.service.factory.IExternalServiceFactory;
import com.snapdeal.cocofs.generic.persister.exception.GenericPersisterException;
import com.snapdeal.cocofs.generic.persister.response.GenericPersisterWithAerospikeResponse;
import com.snapdeal.cocofs.model.response.ApiErrorCode;
import com.snapdeal.cocofs.services.common.dto.UserInfo;
import com.snapdeal.cocofs.services.data.engine.IDataEngineWithAerospike;
import com.snapdeal.cocofs.services.fm.IFMDataSourceService;
import com.snapdeal.cocofs.services.fm.IFMMappingDataUpdater;
import com.snapdeal.cocofs.services.fm.IFulfilmentModelService;
import com.snapdeal.cocofs.services.fm.dto.SellerFmFcDTO;
import com.snapdeal.cocofs.services.fm.dto.SellerSubcatFMMappingDataDto;
import com.snapdeal.cocofs.services.fm.dto.SellerSubcatFMMappingUpdateDto;
import com.snapdeal.cocofs.services.fm.dto.SellerSupcFmFcDTO;
import com.snapdeal.cocofs.sro.FulfillmentModel;
import com.snapdeal.cocofs.sro.SellerSUPCPair;
import com.snapdeal.cocofs.sro.SellerSubcatFulfilmentModelMappingSRO;
import com.snapdeal.cocofs.sro.SupcGroupSRO;
import com.snapdeal.cocofs.utils.AerospikeKeyHelper;

@Service("FulfilmentModelServiceImpl")
public class FulfilmentModelServiceImpl implements IFulfilmentModelService {

    private static final Logger                                                                                      LOG = LoggerFactory.getLogger(FulfilmentModelServiceImpl.class);

    @Autowired
    private IFMDBDataReadService                                                                                     fmDBDataReadService;

    @Autowired
    private IFMAerospikeDataReadService                                                                              fmAerospikeService;

    @Autowired
    private IFMMappingDataUpdater                                                                                    dataUpdater;

    @Autowired
    @Qualifier("SellerSubcatFMMappingDataEngine")
    private IDataEngineWithAerospike<SellerSubcatFMMappingUpdateDto, SellerSubcatFMMapping, SellerSubcatFMMappingVO> dataEngine;

    @Autowired
    private IExternalServiceFactory                                                                                  externalServiceFactory;

    @Autowired
    private ConcurrentRequestExecutor                                                                                concurrentRequestExecutor;

    @Autowired
    private FMDataSourceServiceFactory                                                                               dataSourceService;

    @Override
    public Map<SellerSUPCPair, FulfillmentModel> getFulfilmentModel(List<SellerSUPCPair> sroList) throws ExternalDataNotFoundException {
        Map<SellerSUPCPair, FulfillmentModel> sellerSupcSROToFMMap = new HashMap<SellerSUPCPair, FulfillmentModel>();
        for (SellerSUPCPair sro : sroList) {
            FulfillmentModel model = getFulfilmentModel(sro.getSellerCode(), sro.getSupc(), null);
            sellerSupcSROToFMMap.put(sro, model);
        }
        return sellerSupcSROToFMMap;
    }

    @Override
    public FulfillmentModel getFulfilmentModel(String sellerCode, String supc, String subcat) throws ExternalDataNotFoundException {
        return FulfillmentModel.getFulfilmentModelByCode(getFulfilmentModelCode(sellerCode, supc, subcat));

    }

    @Override
    public String getFulfilmentModelCode(String sellerCode, String supc, String subcat) throws ExternalDataNotFoundException {

        boolean supcExist = false;
        boolean subcatExist = false;
        String fulfilmentModel = null;

        //Get SellerFM mapping for sellerCode
        SellerFmFcDTO fmDTO = dataSourceService.getFMServiceForSellerFmLookup().getSellerFmFcDTO(sellerCode);

        if (fmDTO != null && fmDTO.isEnabled()) {
            supcExist = fmDTO.isSupcExist();
            fulfilmentModel = fmDTO.getFulfilmentModel();
            subcatExist = fmDTO.isSubcatExist();
        }

        if (StringUtils.isNotEmpty(fulfilmentModel)) {
            //now check if supc exist for this seller
            //If yes then read the fulfilment model for supc and seller combination
            String fulfilmentModelCode = null;
            if (supcExist) {
                LOG.debug("Looking up fulfilment model by sellercode & supc combination now..");
                fulfilmentModelCode = dataSourceService.getFMServiceForSellerSupcFmLookup().getFulfilmentModelBySellerAndSupc(sellerCode, supc);
                if (StringUtils.isNotEmpty(fulfilmentModelCode)) {
                    return fulfilmentModelCode;
                }
            }

            //if supc does not exist then check if subcat exists for the seller
            //If yes the read the fulfilment model for subcat and seller combination
            boolean subactLevelFMEnabled = ConfigUtils.getBooleanScalar(Property.SUBCAT_LEVEL_FM_ENABLED);
            if (subactLevelFMEnabled && subcatExist) {
                LOG.debug("Looking up fulfilment model by sellercode & subcat combination now..");
                fulfilmentModelCode = getFulfilmentModelBySellerAndSubcat(sellerCode, supc, subcat);
                if (StringUtils.isNotEmpty(fulfilmentModelCode)) {
                    return fulfilmentModelCode;
                }
            }
        }

        LOG.debug("Not able to find fulfilment model by supc and subcat. Returning fulfilment mapping by seller only.");
        return fulfilmentModel;
    }

    @Override
    public SellerSupcFmFcDTO getSellerSupcFmFcDTOFromDataSource(String sellerCode, String supc) throws ExternalDataNotFoundException {
        return dataSourceService.getFMDataSourceService().getSellerSupcFmFcDTO(sellerCode, supc);
    }

    @Deprecated
    public String getFulfilmentModelBySellerAndSubcat(String sellerCode, String supc, String subcat) throws ExternalDataNotFoundException {
        String fulfilmentModel = null;
        IFMDataSourceService fmDataSourceService = dataSourceService.getFMDataSourceService();
        if (fmDataSourceService != null) {
            fulfilmentModel = fmDataSourceService.getFulfilmentModelBySellerAndSubcat(sellerCode, supc, subcat);
        }

        return fulfilmentModel;
    }

    /**
     * It returns fulfilmentModel from datasource directly, It does not uses in-memory cache which can cause conflicts
     * in case of updates to be pushed to search queue.
     */
    @Override
    public String getFulfilmentModelBySeller(String sellerCode, boolean useCache) throws ExternalDataNotFoundException {
        String fulfilmentModel = null;
        IFMDataSourceService fmDataSourceService = dataSourceService.getFMDataSourceService();
        if (useCache && ConfigUtils.getBooleanScalar(Property.ENABLE_SELLER_FM_MAPPING_CACHE)) {
            fulfilmentModel = dataSourceService.getFMDataSourceServiceForCache().getFulfilmentModelBySeller(sellerCode);
        } else if (fmDataSourceService != null) {
            fulfilmentModel = fmDataSourceService.getFulfilmentModelBySeller(sellerCode);
        }

        return fulfilmentModel;
    }
    
    /**
     * It returns fulfilmentModel from datasource directly, It does not uses in-memory cache which can cause conflicts
     * in case of updates to be pushed to search queue.
     */
    @Override
    public List<String> getEnabledFCsBySeller(String sellerCode, boolean useCache) throws ExternalDataNotFoundException {
        List<String> centers = null;
        IFMDataSourceService fmDataSourceService = dataSourceService.getFMDataSourceService();
        if (useCache && ConfigUtils.getBooleanScalar(Property.ENABLE_SELLER_FM_MAPPING_CACHE)) {
            centers = dataSourceService.getFMDataSourceServiceForCache().getFCBySeller(sellerCode);
        } else if (fmDataSourceService != null) {
            centers = fmDataSourceService.getFCBySeller(sellerCode);
        }

        return centers;
    }


    @Override
    public Map<String, FulfillmentModel> getFulfilmentModel(List<String> sellerCodes, String supc, String subcat) {
        Map<String, FulfillmentModel> sellerToFMMap = null;

        if (ConfigUtils.getBooleanScalar(Property.GET_FULFILMENT_MODEL_CONCURRENTLY) && sellerCodes.size() > 1) {
            sellerToFMMap = getSellerToFMMapConcurrently(sellerCodes, supc, subcat);
        } else {
            sellerToFMMap = new HashMap<String, FulfillmentModel>();
            for (String sellerCode : sellerCodes) {
                FulfillmentModel model;
                //If fulfilment model is not found for some seller codes , don't add it to the map, simply ignore and move on..
                try {
                    model = getFulfilmentModel(sellerCode, supc, subcat);
                    sellerToFMMap.put(sellerCode, model);
                } catch (ExternalDataNotFoundException e) {
                    LOG.error("Fulfilment model not found for seller{}. Exception: {}", sellerCode, e.getMessage());
                } catch (Exception e) {
                    LOG.error("Not able to find the fulfilment model for seller{}. Exception: {}", sellerCode, e.getMessage());
                }
            }
        }
        return sellerToFMMap;
    }

    private Map<String, FulfillmentModel> getSellerToFMMapConcurrently(List<String> sellerCodes, String supc, String subcat) {
        Map<String, FulfillmentModel> sellerToFMMap = new HashMap<String, FulfillmentModel>();
        List<ConcurrentRequest> conReqList = new ArrayList<ConcurrentRequest>(0);
        for (String sellerCode : sellerCodes) {
            ArgumentDTO sellerCodeArg = new ArgumentDTO(String.class, sellerCode);
            ArgumentDTO supcArg = new ArgumentDTO(String.class, supc);
            ArgumentDTO subcatArg = new ArgumentDTO(String.class, subcat);

            conReqList.add(new ConcurrentRequest("getFulfilmentModel", this, sellerCode, sellerCodeArg, supcArg, subcatArg));
        }
        Map<ConcurrentRequest, FulfillmentModel> reqToFMMap = concurrentRequestExecutor.executeConcurrently(conReqList);
        for (Entry<ConcurrentRequest, FulfillmentModel> entry : reqToFMMap.entrySet()) {

            // ConcurrentRequestExecutor returns an Object Class instance  as returnValue when the returnValue of called function is evaluated to null.
            // And in our case the returnType {FulfillmentModel} is of type Enum which can-not be casted from Object Class while serializing 
            //the response in API call. This check is to avoid such failures.
            if (entry.getValue() instanceof Enum<?>) {
                sellerToFMMap.put(entry.getKey().getRequetKey(), entry.getValue());
            } else {
                sellerToFMMap.put(entry.getKey().getRequetKey(), null);
            }
        }
        return sellerToFMMap;
    }

    @Override
    public FulfillmentModel getFulfilmentModel(String sellerCode, String supc) throws ExternalDataNotFoundException {
        return getFulfilmentModel(sellerCode, supc, null);
    }

    @Override
    public List<SellerSubcatFulfilmentModelMappingSRO> getSellerSubcatFMMappingSRO(String sellerCode) {
        // Read from sql db directly instead of mongodb as mongodb will be removed soon...
        //List<SellerSubcatFMMappingUnit> sellerSubcatFMMappingUnitList = fmDBDataReadService.getSellerSubcatFMMapping(sellerCode);
        List<SellerSubcatFulfilmentModelMappingSRO> sroList = new ArrayList<SellerSubcatFulfilmentModelMappingSRO>();

        List<SellerSubcatFMMapping> sellerSubcatFMMappingList = fmDBDataReadService.getSellerSubcatFMMapping(sellerCode);

        if (!CollectionUtils.isEmpty(sellerSubcatFMMappingList)) {
            for (SellerSubcatFMMapping mapping : sellerSubcatFMMappingList) {
                sroList.add(new SellerSubcatFulfilmentModelMappingSRO(mapping.getSellerCode(), mapping.getSubcat(),
                        FulfillmentModel.getFulfilmentModelByCode(mapping.getFulfilmentModel())));
            }
        }
        return sroList;
    }

    private List<SellerSubcatFMMappingVO> getSellerSubcatFMMappingRecordList(List<SellerSubcatFMMapping> entityList) {

        List<SellerSubcatFMMappingVO> recordList = new ArrayList<SellerSubcatFMMappingVO>();
        if (CollectionUtils.isEmpty(entityList)) {
            return recordList;
        }

        for (SellerSubcatFMMapping entity : entityList) {
            SellerSubcatFMMappingVO sellerSubcatFMMappingVO = (SellerSubcatFMMappingVO) fmAerospikeService.getFMMapping(
                    AerospikeKeyHelper.getKey(entity.getSellerCode(), entity.getSubcat()), SellerSubcatFMMappingVO.class);
            if (sellerSubcatFMMappingVO != null) {
                recordList.add(sellerSubcatFMMappingVO);
            } else {
                LOG.error("no aerosplike record found for seller: " + entity.getSellerCode() + " and subcat: " + entity.getSubcat());
            }
        }

        return recordList;

    }

    @Override
    @Deprecated
    public Map<String, ValidationError> addOrUpdateSellerSubcatFMMapping(String sellerCode, Map<String, String> subcatToFMMapping, UserInfo info) {

        Map<String, ValidationError> errorMap = new HashMap<String, ValidationError>();

        boolean ipmsAPIEnabled = ConfigUtils.getBooleanScalar(Property.IPMS_SUBAT_FM_API_ENABLED);
        if (!ipmsAPIEnabled) {
            LOG.info("subcat level fm mapping is disabled, hence not updaing seller-subcat fm mapping for seller ", sellerCode);
            errorMap.put(sellerCode, new ValidationError(ApiErrorCode.DATA_NOT_PERSISTED.code(), "subcat level fm mapping is disabled"));
            return errorMap;
        }

        List<SellerSubcatFMMapping> entityList = fmDBDataReadService.getSellerSubcatFMMapping(sellerCode);
        List<SellerSubcatFMMappingVO> recordList = getSellerSubcatFMMappingRecordList(entityList);

        List<SellerSubcatFMMapping> toBeUpdatedEntityList = new ArrayList<SellerSubcatFMMapping>();
        List<SellerSubcatFMMappingVO> toBeUpdatedRecordList = new ArrayList<SellerSubcatFMMappingVO>();

        SellerSubcatFMMappingUpdateDto dto = new SellerSubcatFMMappingUpdateDto();
        if (null != subcatToFMMapping) {
            for (Entry<String, String> entry : subcatToFMMapping.entrySet()) {
                dto = enrichUpdateDto(dto, sellerCode, entry.getKey(), entry.getValue());
                toBeUpdatedEntityList.clear();
                toBeUpdatedRecordList.clear();

                SellerSubcatFMMapping matchedEntity = getMatchedEntity(entityList, dto);
                SellerSubcatFMMappingVO mathedRecord = getMatchedRecord(recordList, dto);

                if (null != matchedEntity) {
                    toBeUpdatedEntityList.add(matchedEntity);
                    entityList.remove(matchedEntity);
                }
                if (null != mathedRecord) {
                    toBeUpdatedRecordList.add(mathedRecord);
                    recordList.remove(mathedRecord);
                }

                try {
                    toBeUpdatedEntityList = dataEngine.enrichEntities(dto, toBeUpdatedEntityList, info.getEmail());
                    toBeUpdatedRecordList = dataEngine.enrichRecords(dto, toBeUpdatedRecordList, info.getEmail());

                    GenericPersisterWithAerospikeResponse<SellerSubcatFMMapping, SellerSubcatFMMappingVO> resp = dataUpdater.updateSellerSubcatFMMapping(toBeUpdatedEntityList,
                            toBeUpdatedRecordList);
                    if (!resp.isSuccessful()) {
                        updateErrorMapForRowError(dto, errorMap, resp.getMessage());
                    }
                } catch (OperationNotSupportedException e) {
                    LOG.error("Exception ", e);
                    updateErrorMapForRowError(dto, errorMap, e.getMessage());
                } catch (GenericPersisterException e) {
                    LOG.error("Exception ", e);
                    updateErrorMapForRowError(dto, errorMap, e.getMessage());
                } catch (Exception e) {
                    LOG.error("Error", e);
                    updateErrorMapForRowError(dto, errorMap, e.getMessage());
                }
            }
        }

        // disabling all remaining docs and entities
        disableRemainingEntitiesAndDocs(getSubcatToFMMappingDataDto(entityList, recordList, info.getEmail()), errorMap);
        return errorMap;
    }

    @Override
    @Deprecated
    public Map<String, ValidationError> updateSellerSubcatFMMapping(String sellerCode, Map<String, FulfillmentModel> subcatToFMMapping, UserInfo info) {
        Map<String, String> subcatToFulfilmentModel = new HashMap<String, String>(0);
        if (null != subcatToFMMapping) {
            for (Entry<String, FulfillmentModel> entry : subcatToFMMapping.entrySet()) {
                subcatToFulfilmentModel.put(entry.getKey(), entry.getValue().getCode());
            }
        }
        return addOrUpdateSellerSubcatFMMapping(sellerCode, subcatToFulfilmentModel, info);

    }

    private void updateErrorMapForRowError(SellerSubcatFMMappingUpdateDto dto, Map<String, ValidationError> errorMap, String message) {
        errorMap.put(dto.getSubcat(), new ValidationError(ApiErrorCode.DATA_NOT_PERSISTED.code(), message));

    }

    private SellerSubcatFMMappingUpdateDto enrichUpdateDto(SellerSubcatFMMappingUpdateDto dto, String sellerCode, String subcat, String model) {
        if (dto == null) {
            dto = new SellerSubcatFMMappingUpdateDto();
        }
        dto.setSellerCode(sellerCode);
        dto.setSubcat(subcat);
        dto.setFulfilmentModel(model);
        return dto;
    }

    private Map<String, SellerSubcatFMMappingDataDto> getSubcatToFMMappingDataDto(List<SellerSubcatFMMapping> entityList, List<SellerSubcatFMMappingVO> recordList, String userEmail) {
        Map<String, SellerSubcatFMMappingDataDto> subcatToFMMappingData = new HashMap<String, SellerSubcatFMMappingDataDto>();

        if (!CollectionUtils.isEmpty(entityList)) {
            for (SellerSubcatFMMapping entity : entityList) {
                entity.setLastUpdated(DateUtils.getCurrentTime());
                entity.setEnabled(false);
                entity.setUpdatedBy(userEmail);
                if (!subcatToFMMappingData.containsKey(entity.getSubcat())) {
                    subcatToFMMappingData.put(entity.getSubcat(), new SellerSubcatFMMappingDataDto());
                }
                subcatToFMMappingData.get(entity.getSubcat()).setEntity(entity);
            }
        }

        if (!CollectionUtils.isEmpty(recordList)) {
            for (SellerSubcatFMMappingVO record : recordList) {
                record.setUpdatedStr(DateUtils.getCurrentTime().getTime() + "");
                record.setEnabled(0);
                if (!subcatToFMMappingData.containsKey(record.getSubcat())) {
                    subcatToFMMappingData.put(record.getSubcat(), new SellerSubcatFMMappingDataDto());
                }
                subcatToFMMappingData.get(record.getSubcat()).setRecord(record);
            }
        }

        return subcatToFMMappingData;
    }

    private void disableRemainingEntitiesAndDocs(Map<String, SellerSubcatFMMappingDataDto> subcatToFMMappingData, Map<String, ValidationError> errorMap) {

        List<SellerSubcatFMMapping> toBeUpdatedEntityList = new ArrayList<SellerSubcatFMMapping>();
        List<SellerSubcatFMMappingVO> toBeUpdatedRecordList = new ArrayList<SellerSubcatFMMappingVO>();

        for (Entry<String, SellerSubcatFMMappingDataDto> entry : subcatToFMMappingData.entrySet()) {
            SellerSubcatFMMappingDataDto dto = entry.getValue();
            toBeUpdatedEntityList.clear();
            toBeUpdatedRecordList.clear();

            if (null == dto.getRecord() || null == dto.getEntity()) {
                updateErrorMapForDisableError(entry.getKey(), errorMap, "Either record, entity doesn't exists for this key");
                LOG.warn("Error while disabling data, either record or entity doesn't exists for {} and {}", entry.getKey(), entry.getValue());
                continue;
            }
            toBeUpdatedEntityList.add(dto.getEntity());

            if (null != dto.getRecord()) {
                toBeUpdatedRecordList.add(dto.getRecord());
            }

            GenericPersisterWithAerospikeResponse<SellerSubcatFMMapping, SellerSubcatFMMappingVO> resp;

            try {
                resp = dataUpdater.updateSellerSubcatFMMapping(toBeUpdatedEntityList, toBeUpdatedRecordList);

                if (!resp.isSuccessful()) {
                    updateErrorMapForDisableError(entry.getKey(), errorMap, resp.getMessage());
                }
            } catch (GenericPersisterException e) {
                updateErrorMapForDisableError(entry.getKey(), errorMap, e.getMessage());
                LOG.error("Error", e);
            } catch (Exception e) {
                updateErrorMapForDisableError(entry.getKey(), errorMap, e.getMessage());
                LOG.error("Error", e);
            }

        }

    }

    private void updateErrorMapForDisableError(String subcat, Map<String, ValidationError> errorMap, String message) {
        errorMap.put(subcat, new ValidationError(ApiErrorCode.DATA_NOT_DISABLED.code(), message));
    }

    private SellerSubcatFMMapping getMatchedEntity(List<SellerSubcatFMMapping> entityList, SellerSubcatFMMappingUpdateDto updateDto) {
        if (!CollectionUtils.isEmpty(entityList)) {
            for (SellerSubcatFMMapping entity : entityList) {
                if (entity.getSubcat().equals(updateDto.getSubcat())) {
                    return entity;
                }
            }
        }
        return null;
    }

    private SellerSubcatFMMappingVO getMatchedRecord(List<SellerSubcatFMMappingVO> recordList, SellerSubcatFMMappingUpdateDto updateDto) {
        if (!CollectionUtils.isEmpty(recordList)) {
            for (SellerSubcatFMMappingVO record : recordList) {
                if (record.getSubcat().equals(updateDto.getSubcat())) {
                    return record;
                }
            }
        }
        return null;
    }

    @Override
    public List<String> getFulfilmentModelsForSeller(String sellerCode) {
        Set<String> fulfilmentModels = new HashSet<String>();

        //Read directly from sql db instead of mongodb as mongo will soon be removed 
        //List<SellerSupcFMMappingUnit> sellerSupcFMMappingUnitList = fmMongoService.getSellerSupcFMMappingUnits(sellerCode);
        List<SellerSupcFMMapping> sellerSupcFMMappingList = fmDBDataReadService.getAllEnabledSellerSupcFMMappingBySeller(sellerCode);

        if (!CollectionUtils.isEmpty(sellerSupcFMMappingList)) {
            for (SellerSupcFMMapping mapping : sellerSupcFMMappingList) {
                fulfilmentModels.add(mapping.getFulfilmentModel());
            }
        }
        //Read directly from sql db instead of mongodb as mongo will soon be removed
        //List<SellerSubcatFMMappingUnit> sellerSubcatFMMappingUnitList = fmMongoService.getSellerSubcatFMMappingUnit(sellerCode);
        boolean subactLevelFMEnabled = ConfigUtils.getBooleanScalar(Property.SUBCAT_LEVEL_FM_ENABLED);
        List<SellerSubcatFMMapping> sellerSubcatFMMappingList = null;
        if (subactLevelFMEnabled) {
            sellerSubcatFMMappingList = fmDBDataReadService.getSellerSubcatFMMapping(sellerCode);
        }

        if (!CollectionUtils.isEmpty(sellerSubcatFMMappingList)) {
            for (SellerSubcatFMMapping mapping : sellerSubcatFMMappingList) {
                fulfilmentModels.add(mapping.getFulfilmentModel());
            }
        }
        //SellerFMMappingUnit sellerFMMapping = fmMongoService.getSellerFMMappingUnit(sellerCode);
        SellerFMMapping sellerFMMapping = fmDBDataReadService.getEnabledSellerFMMapping(sellerCode);

        if (null != sellerFMMapping) {
            fulfilmentModels.add(sellerFMMapping.getFulfilmentModel());
        }
        return new ArrayList<String>(fulfilmentModels);
    }

    /**
     * Get the fulfilment model by sellercode from aerospike
     * 
     * @param sellerCode
     * @return
     * @throws ExternalDataNotFoundException
     */
    @Override
    public SellerFMMappingVO getFulfilmentModelBySellerFromAerospike(String sellerCode) throws ExternalDataNotFoundException {
        SellerFMMappingVO fulfilmentModel = null;
        if (StringUtils.isNotEmpty(sellerCode)) {
            LOG.debug("Going to read fulfilment model from Aerospike for sellercode {}", sellerCode);
            SellerFMMappingVO sellerFMMappingUnit = (SellerFMMappingVO) fmAerospikeService.getFMMapping(AerospikeKeyHelper.getKey(sellerCode), SellerFMMappingVO.class);
            if (null != sellerFMMappingUnit && sellerFMMappingUnit.isEnabled()) {
                fulfilmentModel = sellerFMMappingUnit;
            }
        } else {
            LOG.warn("Illegal paramters passed while trying to find fm at seler level");
            throw new ExternalDataNotFoundException("Required Parameters not there while getting FM At Seller Level");
        }
        return fulfilmentModel;
    }

    @Override
    public boolean getSDFUlfilledBySellerSupcSubcat(List<SupcGroupSRO> supcSellerSubcatList) throws ExternalDataNotFoundException {
        boolean sdFulfilled = false;
        if (ConfigUtils.getBooleanScalar(Property.GET_SD_FULFILLED_CONCURRENTLY) && supcSellerSubcatList.size() > 1) {
            sdFulfilled = getSDFUlfilledBySellerSupcSubcatConcurrently(supcSellerSubcatList);
        } else {
            sdFulfilled = getSDFUlfilledBySellerSupcSubcatSequentially(supcSellerSubcatList);
        }
        return sdFulfilled;
    }

    /**
     * Process the request parallely to determine if sdfulfilled
     * 
     * @param supcSellerSubcatList
     * @return
     */
    private boolean getSDFUlfilledBySellerSupcSubcatConcurrently(List<SupcGroupSRO> supcSellerSubcatList) {

        LOG.debug("Executing the request to determine SD fulfilability concurrently.");
        List<ConcurrentRequest> conReqList = new ArrayList<ConcurrentRequest>();
        for (SupcGroupSRO sro : supcSellerSubcatList) {
            ArgumentDTO sroArg = new ArgumentDTO(SupcGroupSRO.class, sro);
            conReqList.add(new ConcurrentRequest("getSDFUlfilledBySellerSupcSubcat", this, sro.getSupc(), sroArg));
        }

        Map<ConcurrentRequest, Boolean> reqToFMMap = concurrentRequestExecutor.executeConcurrently(conReqList);
        if (reqToFMMap != null && reqToFMMap.values() != null) {

            for (Object sdFulfilledObj : reqToFMMap.values()) {
                if (sdFulfilledObj != null && sdFulfilledObj instanceof Boolean) {
                    Boolean sdFulfilled = (Boolean) sdFulfilledObj;
                    if (sdFulfilled) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Process the request sequentially to determine if sdfulfilled
     * 
     * @param supcSellerSubcatList
     * @return
     */
    private boolean getSDFUlfilledBySellerSupcSubcatSequentially(List<SupcGroupSRO> supcSellerSubcatList) {

        LOG.debug("Executing the request to determine SD fulfilability sequentially.");
        for (SupcGroupSRO sro : supcSellerSubcatList) {
            if (getSDFUlfilledBySellerSupcSubcat(sro)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get SD Fulfilled status - Method at finest level
     * 
     * @param sro
     * @return
     */
    public boolean getSDFUlfilledBySellerSupcSubcat(SupcGroupSRO sro) {
        List<String> sellerCodes = sro.getSellerCodes();
        if (sellerCodes != null) {
            for (int j = 0; j < sellerCodes.size(); j++) {
                try {
                    String fmModel = getFulfilmentModelCode(sellerCodes.get(j), sro.getSupc(), sro.getCategoryUrl());
                    if (fmModel != null && fmModel.equals(FulfillmentModel.FC_VOI.getCode())) {
                        LOG.debug("The fulfilment model for seller:{} supc:{} subcategory:{} found to be FC_VOI. Hence, returning SD fulfilled flag as TRUE.", sellerCodes.get(j),
                                sro.getSupc(), sro.getCategoryUrl());
                        return true;
                    }
                } catch (ExternalDataNotFoundException e) {
                    LOG.error("Could not fetch FM Model for seller:{} supc:{} subcategory:{}", sellerCodes.get(j), sro.getSupc(), sro.getCategoryUrl());
                    LOG.info("Ignoring the exception {} and moving on..", e.getMessage());
                }
            }
        }
        return false;
    }

    /* 
     * Check if the seller exists, seller is assumed to be existing if seller-fm
     * mapping is present.
     * 
     * (non-Javadoc)
     * @see com.snapdeal.cocofs.services.fm.IFulfilmentModelService#isSellerExists(java.lang.String)
     */
    @Override
    public boolean isSellerExists(String sellerCode) throws ExternalDataNotFoundException {
        boolean bResult = false;
        IFMDataSourceService fmDataSourceService = dataSourceService.getFMDataSourceService();
        if (fmDataSourceService != null) {
            bResult = fmDataSourceService.isSellerExists(sellerCode);
        }

        LOG.info("Returning flag if seller {} is found {}", sellerCode, bResult);
        return bResult;

    }

    /**
     * returns all enabled seller for a given Fulfillment at Seller, Supc and Subcat Level.
     * 
     * @param fulfillmentModel
     * @return
     */
    @Override
    public Set<String> getAllSellerForFM(String fulfillmentModel) {
        Set<String> sellers = new HashSet<String>();

        List<String> sellerListAtSupcLevel = fmDBDataReadService.getAllEnabledSellerAtSupcLevelForFulfillmentModel(fulfillmentModel);
        if (!CollectionUtils.isEmpty(sellerListAtSupcLevel)) {
            sellers.addAll(sellerListAtSupcLevel);
        }

        boolean subcatLevelFMEnabled = ConfigUtils.getBooleanScalar(Property.SUBCAT_LEVEL_FM_ENABLED);
        if (subcatLevelFMEnabled) {
            List<String> sellerListAtSubcatLevel = fmDBDataReadService.getAllEnabledSellerAtSubcatLevelForFulfillmentModel(fulfillmentModel);
            if (!CollectionUtils.isEmpty(sellerListAtSubcatLevel)) {
                sellers.addAll(sellerListAtSubcatLevel);
            }
        }

        List<String> sellerListAtSellerLevel = fmDBDataReadService.getAllEnabledSellerAtSellerLevelForFulfillmentModel(fulfillmentModel);
        if (!CollectionUtils.isEmpty(sellerListAtSellerLevel)) {
            sellers.addAll(sellerListAtSellerLevel);
        }
        return sellers;
    }

	@Override
	public Map<String, Boolean> validateSellers(List<String> sellerList)throws ExternalDataNotFoundException {
		if(null == sellerList || sellerList.isEmpty()){
			Log.info("Seller list is null/empty in the request");
			throw new ExternalDataNotFoundException("Seller List is Null or Empty");
		}
		Map<String, Boolean> resultMap = new HashMap<String, Boolean>();
		for(String sellerCode : sellerList){
			try{
				resultMap.put(sellerCode, isSellerExists(sellerCode));
			}catch(ExternalDataNotFoundException ex){
				Log.error("Exception has come while processing " + ex);
				resultMap.put(sellerCode, Boolean.FALSE);
			}
		}
		
		return resultMap;
		
	}

}
