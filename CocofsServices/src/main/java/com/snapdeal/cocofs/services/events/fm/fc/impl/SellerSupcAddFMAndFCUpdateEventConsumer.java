/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.services.events.fm.fc.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.services.events.dto.SellerSupcAddEventObj;
import com.snapdeal.cocofs.services.events.impl.BaseEventConsumer;
import com.snapdeal.cocofs.services.events.impl.EventType;
import com.snapdeal.cocofs.services.fm.ISellerSUPCFMFCMappingProcessor;

/**
 * @version 1.0, 25-Mar-2015
 * @author shiv
 */
@Service("SellerSupcAddFMAndFCUpdateEventConsumer")
public class SellerSupcAddFMAndFCUpdateEventConsumer extends BaseEventConsumer<SellerSupcAddEventObj> {

    @Autowired
    @Qualifier("sellerSUPCFMFCMappingProcessor")
    private ISellerSUPCFMFCMappingProcessor sellerSUPCFMFCMappingProcessor;

    private static final Logger             LOG = LoggerFactory.getLogger(SellerSupcAddFMAndFCUpdateEventConsumer.class);

    @Override
    protected String getEventCode() {
        return EventType.FC_UPDATE_EVENT_ON_SELLER_SUPC_ADD.getCode();
    }

    @Override
    protected boolean processEventObj(SellerSupcAddEventObj eventObject) {

        try {
            return sellerSUPCFMFCMappingProcessor.processAdditionOfNewSellerSUPCMappingForExternalNotification(eventObject);
        } catch (Exception e) {
            LOG.error("Exception while processing the request for event: {}. Exception {}", eventObject, e);
        }

        return false;
    }
}
