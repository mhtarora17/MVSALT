package com.snapdeal.cocofs.services.data.engine.impl;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.naming.OperationNotSupportedException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.entity.ProductAttribute;
import com.snapdeal.cocofs.excelupload.ProductAttributeUploadDTO;
import com.snapdeal.cocofs.mongo.model.ProductAttributeUnit;
import com.snapdeal.cocofs.mongo.model.ProductAttributeUnit.PAUFieldNames;
import com.snapdeal.cocofs.services.converter.IConverterService;
import com.snapdeal.cocofs.utils.DangerousGoodsTypeConversionUtil;

/**
 * @author nikhil
 */

@Service("productAttributeUploadEngine")
public class ProductAttributeUploadEngine extends AbstractProductAttributeEngine {

    protected static final Logger LOG = LoggerFactory.getLogger(ProductAttributeUploadEngine.class);

    @Autowired
    IConverterService             converterService;

    @Override
    public List<ProductAttributeUnit> enrichDocuments(ProductAttributeUploadDTO dto, List<ProductAttributeUnit> documents) throws OperationNotSupportedException {

        Field[] fields = dto.getClass().getDeclaredFields();

        if (documents == null || (documents != null && documents.isEmpty())) {
            throw new IllegalArgumentException("No mongo document exists for supc " + dto.getSupc());
        }

        ProductAttributeUnit unit = documents.get(0);

        for (Field field : fields) {
            try {
                field.setAccessible(true);
                Object val = field.get(dto);
                if (val == null) {
                    if (field.getName().equals(PAUFieldNames.DANGEROUS_GOODS_TYPE.getCode())) {
                        unit.setDangerousGoodsType(DangerousGoodsTypeConversionUtil.getDangerousGoodsType(unit));
                    }

                    continue;
                } else {

                    if (field.getName().equals("supc")) {
                        continue;
                    }
                    for (Field innerField : unit.getClass().getDeclaredFields()) {
                        innerField.setAccessible(true);
                        if (innerField.getName().equals(field.getName())) {
                            innerField.set(unit, val);
                        }
                    }
                }
            } catch (Exception e) {
                LOG.error("Error in reading field : {}", field.getName());
                throw new IllegalArgumentException("Error in reading field : " + field.getName());
            }
        }

        return documents;
    }

    @Override
    public List<ProductAttribute> enrichEntities(ProductAttributeUploadDTO dto, List<ProductAttribute> entities, String userEmail) throws OperationNotSupportedException {
        List<ProductAttribute> toReturn = new ArrayList<ProductAttribute>();

        if (entities == null || (entities != null && entities.isEmpty())) {
            throw new IllegalArgumentException("No entity exists for supc " + dto.getSupc());
        }

        Map<String, String> m = ConfigUtils.getMap(Property.PRODUCT_ATTRIBUTE_UPLOAD_DTO_FIELD_MAP);
        Field[] flds = dto.getClass().getDeclaredFields();
        Field dangerousGoodsTypeField = null;
        Field serializedTypeField = null;
        Field packagingTypeField = null;
        Field primaryLengthTypeField = null;
        Field primaryBreadthTypeField = null;
        Field primaryHeightTypeField = null;

        for (Field fld : flds) {
            int mods = fld.getModifiers();
            if (Modifier.isStatic(mods)) {
                continue;
            }

            String attributeName = m.get(fld.getName());

            if (fld.getName().equals("supc") || fld.getName().equals("createdByEmailMap")) {
                continue;
            }

            ProductAttribute attr = findProductAttributeForName(attributeName, entities);
            String email = userEmail;

            if (null == attr) {

                String fieldValueAsString = null;
                if (fld.getName().equals("woodenPackaging")) {
                    fieldValueAsString = dto.isWoodenPackaging() != null ? dto.isWoodenPackaging().toString() : Boolean.FALSE.toString();
                } else if (fld.getName().equals("dangerousGoodsType")) {
                    dangerousGoodsTypeField = fld;
                    continue;
                } else if (fld.getName().equals("primaryLength")) {
                    if (dto.getPrimaryLength() != null) {
                        primaryLengthTypeField = fld;
                    }
                    continue;
                } else if (fld.getName().equals("primaryBreadth")) {
                    if (dto.getPrimaryBreadth() != null) {
                        primaryBreadthTypeField = fld;
                    }
                    continue;
                } else if (fld.getName().equals("primaryHeight")) {
                    if (dto.getPrimaryHeight() != null) {
                        primaryHeightTypeField = fld;
                    }
                    continue;
                } else if (fld.getName().equals("serializedType")) {
                    if (StringUtils.isNotEmpty(dto.getSerializedType())) {
                        serializedTypeField = fld;
                    }
                    continue;
                } else if (fld.getName().equals("packagingType")) {
                    if (StringUtils.isNotEmpty(dto.getPackagingType())) {
                        packagingTypeField = fld;
                    }
                    continue;
                } else if ("systemWeightCaptured".equals(fld.getName())) {
                    fieldValueAsString = Boolean.FALSE.toString();
                }

                else {
                    fieldValueAsString = getNotNullFieldValueAsString(attributeName, fld, dto);
                }
                if (dto.getCreatedByforAttribute(attributeName) != null)
                    email = dto.getCreatedByforAttribute(attributeName);
                LOG.info("Creating new product attribute for supc " + dto.getSupc() + " attribute " + attributeName + " with value " + fieldValueAsString + " per bulk update by "
                        + email);

                ProductAttribute newAttr = createNewProductAttribute(dto, email, attributeName, fieldValueAsString);
                toReturn.add(newAttr);
            } else {
                String fieldValueAsString = getFieldAsString(attributeName, fld, dto);
                if (StringUtils.isEmpty(fieldValueAsString)) {
                    // if this value is found to be null, we don't have to include this in entity list to be persisted. 
                    continue;
                }

                if (dto.getCreatedByforAttribute(attributeName) != null)
                    email = dto.getCreatedByforAttribute(attributeName);
                LOG.info("Updating product attribute for supc " + dto.getSupc() + " attribute " + attributeName + " with value " + fieldValueAsString + " per bulk update by "
                        + email);
                updateProductAttribute(email, attr, fieldValueAsString);
                toReturn.add(attr);
            }
        }

        if (null != dangerousGoodsTypeField) {
            ProductAttribute newAttr = createNewProductAttribute(dto, userEmail, m.get(dangerousGoodsTypeField.getName()),
                    DangerousGoodsTypeConversionUtil.getDangerousGoodsType(dto.getDangerousGoodsType(), getBooleanValue(entities, "HAZMAT"), getBooleanValue(entities, "LIQUID")));
            toReturn.add(newAttr);
        }
        if (null != serializedTypeField) {
            ProductAttribute newAttr = createNewProductAttribute(dto, userEmail, m.get(serializedTypeField.getName()), dto.getSerializedType());
            toReturn.add(newAttr);
        }
        if (null != packagingTypeField) {
            ProductAttribute newAttr = createNewProductAttribute(dto, userEmail, m.get(packagingTypeField.getName()), dto.getPackagingType());
            toReturn.add(newAttr);
        }

        if (null != primaryLengthTypeField) {
            ProductAttribute newAttr = createNewProductAttribute(dto, userEmail, m.get(primaryLengthTypeField.getName()),
                    dto.getPrimaryLength() != null ? dto.getPrimaryLength().toString() : null);
            toReturn.add(newAttr);
        }

        if (null != primaryBreadthTypeField) {
            ProductAttribute newAttr = createNewProductAttribute(dto, userEmail, m.get(primaryBreadthTypeField.getName()),
                    dto.getPrimaryBreadth() != null ? dto.getPrimaryBreadth().toString() : null);
            toReturn.add(newAttr);
        }

        if (null != primaryHeightTypeField) {
            ProductAttribute newAttr = createNewProductAttribute(dto, userEmail, m.get(primaryHeightTypeField.getName()),
                    dto.getPrimaryHeight() != null ? dto.getPrimaryHeight().toString() : null);
            toReturn.add(newAttr);
        }

        return toReturn;
    }

    private boolean getBooleanValue(List<ProductAttribute> entities, String attrName) {
        ProductAttribute attr = findProductAttributeForName(attrName, entities);
        if (null != attr) {
            return Boolean.parseBoolean(attr.getValue());
        }
        return false;
    }

}
