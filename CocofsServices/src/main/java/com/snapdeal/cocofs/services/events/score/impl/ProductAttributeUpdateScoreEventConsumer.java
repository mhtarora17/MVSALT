package com.snapdeal.cocofs.services.events.score.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.external.service.IScoreExternalService;
import com.snapdeal.cocofs.services.events.dto.ProductAttributeScoreEvent;
import com.snapdeal.cocofs.services.events.impl.BaseEventConsumer;
import com.snapdeal.cocofs.services.events.impl.EventType;

@Service("ProductAttributeUpdateScoreEventConsumer")
public class ProductAttributeUpdateScoreEventConsumer extends BaseEventConsumer<ProductAttributeScoreEvent>{

	@Autowired
	private IScoreExternalService 							scoreService;
	
	@Override
	protected boolean processEventObj(ProductAttributeScoreEvent eventObject) {
		return scoreService.updateProductFulfilmentAttributesInScore(eventObject.getUpdateProductFulfilmentAttributeRequest());
	}

	@Override
	protected String getEventCode() {
		return EventType.PRODUCT_ATTRIBUTE_UPDATE_SCORE_EVENT.getCode();
	}

}
