/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 09-Oct-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.services.fm;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.snapdeal.base.validation.ValidationError;
import com.snapdeal.cocofs.core.aerospike.vo.SellerFMMappingVO;
import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.services.common.dto.UserInfo;
import com.snapdeal.cocofs.services.fm.dto.SellerSupcFmFcDTO;
import com.snapdeal.cocofs.sro.FulfillmentModel;
import com.snapdeal.cocofs.sro.SellerSUPCPair;
import com.snapdeal.cocofs.sro.SellerSubcatFulfilmentModelMappingSRO;
import com.snapdeal.cocofs.sro.SupcGroupSRO;

public interface IFulfilmentModelService {

    /**
     * @param sroList
     * @return
     * @throws ExternalDataNotFoundException
     */
    Map<SellerSUPCPair, FulfillmentModel> getFulfilmentModel(List<SellerSUPCPair> sroList) throws ExternalDataNotFoundException;

    /**
     * Returns Fulfilment model at seller supc level. If subcat is null in this API, then subcat if fetched from CAMS
     * through supc
     * 
     * @param sellerCode
     * @param supc    @Override

     * @param subcat
     * @return
     * @throws ExternalDataNotFoundException
     */
    FulfillmentModel getFulfilmentModel(String sellerCode, String supc, String subcat) throws ExternalDataNotFoundException;

    /**
     * Returns map of seller to fulfilment model for a particular supc and subcat. If subcat is null in this API, then
     * subcat if fetched from CAMS
     * 
     * @param sellerCode
     * @param supc
     * @param subcat
     * @return
     */
    Map<String, FulfillmentModel> getFulfilmentModel(List<String> sellerCode, String supc, String subcat);

    /**
     * Returns Fulfilment model at seller supc level.
     * 
     * @param sellerCode
     * @param supc
     * @return
     * @throws ExternalDataNotFoundException
     */
    FulfillmentModel getFulfilmentModel(String sellerCode, String supc) throws ExternalDataNotFoundException;

    /**
     * Returns default fulfilment model at seller level
     * 
     * @param sellerCode
     * @return
     * @throws ExternalDataNotFoundException
     */
    String getFulfilmentModelBySeller(String sellerCode, boolean useCache) throws ExternalDataNotFoundException;

    /**
     * @param sellerCode
     * @return
     */
    List<SellerSubcatFulfilmentModelMappingSRO> getSellerSubcatFMMappingSRO(String sellerCode);

    /**
     * @param sellerCode
     * @param subcatToFMMapping
     * @param info
     * @return
     */
    Map<String, ValidationError> updateSellerSubcatFMMapping(String sellerCode, Map<String, FulfillmentModel> subcatToFMMapping, UserInfo info);

    /**
     * @param sellerCode
     * @return
     */
    List<String> getFulfilmentModelsForSeller(String sellerCode);

    Map<String, ValidationError> addOrUpdateSellerSubcatFMMapping(String sellerCode, Map<String, String> subcatToFMMapping, UserInfo info);

    String getFulfilmentModelCode(String sellerCode, String supc, String subcat) throws ExternalDataNotFoundException;
    
    /**
     * Get SD fulfilled status by seller, supc, subcat
     * 
     * @param supcSellerSubcatList
     * @return
     * @throws ExternalDataNotFoundException
     */
    boolean getSDFUlfilledBySellerSupcSubcat(List<SupcGroupSRO> supcSellerSubcatList) throws ExternalDataNotFoundException;

    /**
	 * Check if the seller exists, seller is assumed to be existing if seller-fm
	 * mapping is present.
	 * 
	 * @param sellerCode
	 * @return
	 * @throws ExternalDataNotFoundException
	 */
    public boolean isSellerExists(String sellerCode) throws ExternalDataNotFoundException;

    SellerFMMappingVO getFulfilmentModelBySellerFromAerospike(String sellerCode) throws ExternalDataNotFoundException;
    /**
     * Returns all enabled seller for a given fulfillment Model at seller, supc  and subcat level.
     * 
     * @param fulfillmentModel
     * @return
     */
    Set<String> getAllSellerForFM(String fulfillmentModel);

    SellerSupcFmFcDTO getSellerSupcFmFcDTOFromDataSource(String sellerCode, String supc) throws ExternalDataNotFoundException;
    
    Map<String, Boolean> validateSellers(List<String> sellerList)throws ExternalDataNotFoundException;

    List<String> getEnabledFCsBySeller(String sellerCode, boolean useCache) throws ExternalDataNotFoundException;

	
}
