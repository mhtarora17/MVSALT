/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 02-Sep-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.services.task.alwaysrunning;

import org.springframework.beans.factory.DisposableBean;

public interface IAlwaysRunningTask extends DisposableBean {

    public void execute();

    public boolean isTaskRunning();

    public void stopProcess();

}
