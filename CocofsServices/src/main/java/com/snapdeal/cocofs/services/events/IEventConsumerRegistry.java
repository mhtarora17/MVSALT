/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.services.events;

/**
 * @version 1.0, 26-Jan-2015
 * @author shiv
 */
public interface IEventConsumerRegistry {
    boolean registerConsumer(String eventCode, IEventConsumer<?> consumer);

    boolean unregisterConsumer(String eventCode);

    IEventConsumer<?> getConsumer(String eventCode);
}
