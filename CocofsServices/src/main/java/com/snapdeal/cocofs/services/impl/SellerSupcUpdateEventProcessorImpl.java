package com.snapdeal.cocofs.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.model.queue.ShipFromUpdateMessage;
import com.snapdeal.cocofs.services.ISellerSupcUpdateEventProcessor;
import com.snapdeal.cocofs.services.producer.IExternalNotificationsServiceForShipFromUpdate;

@Service("sellerSupcUpdateEventProcessorImpl")
public class SellerSupcUpdateEventProcessorImpl implements ISellerSupcUpdateEventProcessor{

    @Autowired
    IExternalNotificationsServiceForShipFromUpdate   pincodeNotificationService;
    
    
    public void processPincodeUpdate(List<ShipFromUpdateMessage> sellersupcPincodeUpdates){
        
        pincodeNotificationService.publishToQueue(sellersupcPincodeUpdates);
    }
    
}
