package com.snapdeal.cocofs.services.fm.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.base.cache.CacheManager;
import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.cocofs.cache.loader.impl.SellerFMMappingCache;
import com.snapdeal.cocofs.cache.loader.impl.SellerSupcFMMappingCache;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.entity.SellerFCCodeMapping;
import com.snapdeal.cocofs.entity.SellerFMMapping;
import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.services.fm.IFMDataSourceService;
import com.snapdeal.cocofs.services.fm.dto.SellerFmFcDTO;
import com.snapdeal.cocofs.services.fm.dto.SellerSupcFmFcDTO;

@Service("FMDataSourceServiceForCache")
public class FMDataSourceServiceForCache implements IFMDataSourceService{

	@Autowired
	private FMDataSourceServiceFactory					fmDataSource;

	private static final Logger		LOG = LoggerFactory.getLogger(FMDataSourceServiceForCache.class);

	/**
	 * Returns the fulfilment model by sellercode from cache
	 * 
	 * @param sellerCode
	 * @return fulfilmentModel
	 * @throws ExternalDataNotFoundException
	 */
	@Override
	public String getFulfilmentModelBySeller(String sellerCode)	throws ExternalDataNotFoundException {
		String fulfilmentModel = null;
		//Get SellerFM mapping for sellerCode
		SellerFmFcDTO sellerFmFcDTO = getSellerFmFcDTO(sellerCode);
		if (sellerFmFcDTO != null && sellerFmFcDTO.isEnabled()) {
			fulfilmentModel = sellerFmFcDTO.getFulfilmentModel();
		}
		LOG.debug("Returning fm from seller mapping {} for seller {}  ", fulfilmentModel, sellerCode);
		return fulfilmentModel;
	}
	
	/**
     * Returns the fc centers by sellercode from aerospike
     * 
     * @param sellerCode
     * @return fulfilmentModel
     * @throws ExternalDataNotFoundException
     */
    @Override
    public List<String> getFCBySeller(String sellerCode) throws ExternalDataNotFoundException {
        List<String> centers = null;
        
        SellerFmFcDTO sellerFmFcDTO = getSellerFmFcDTO(sellerCode);
        if (sellerFmFcDTO != null && sellerFmFcDTO.isEnabled()) {
            centers = sellerFmFcDTO.getEnabledFCCenters();
        }
        LOG.debug("Returning centers from seller mapping {} for seller {} ", centers, sellerCode);
        return centers;
    }

	/**
	 * Returns the fulfilment model by sellercode and supc from cache
	 * 
	 * @param sellerCode
	 * @param supc
	 * @return fulfilmentModel
	 * @throws ExternalDataNotFoundException
	 */
	@Override
	public String getFulfilmentModelBySellerAndSupc(String sellerCode, String supc) throws ExternalDataNotFoundException {
        String fulfilmentModel = null;
        SellerSupcFmFcDTO dto = getSellerSupcFmFcDTO(sellerCode, supc);
        if (dto != null && dto.isEnabled()) {
			fulfilmentModel = dto.getFulfilmentModel();
		}        LOG.debug("Fulfilment Model obtained from sellerSupcFMMapping Cache:" + fulfilmentModel);
        LOG.debug("Returning fm from seller-supc mapping {} for seller {}  supc {} ", fulfilmentModel, sellerCode, supc);
		return fulfilmentModel;
	}

	/**
	 * Returns the fulfilment model by sellercode and subcat from cache.
	 * This method always throw @ExternalDataNotFoundException as we don't
	 * cache Fulfilment Model at seller and subcat level.
	 * 
	 * @param sellerCode
	 * @param supc
	 * @param subcat
	 * @return fulfilmentModel
	 * @throws ExternalDataNotFoundException
	 */
	@Override
	public String getFulfilmentModelBySellerAndSubcat(String sellerCode, String supc, String subcat) throws ExternalDataNotFoundException {
		//get data source
		IFMDataSourceService getFulfilmentModel =  fmDataSource.getFMDataSourceService();
		return getFulfilmentModel.getFulfilmentModelBySellerAndSubcat(sellerCode, supc, subcat);
	}

	@Override
	public boolean isSellerExists(String sellerCode) throws ExternalDataNotFoundException {
		//get data source
		IFMDataSourceService getFulfilmentModel =  fmDataSource.getFMDataSourceService();
		return getFulfilmentModel.isSellerExists(sellerCode);
	}

	/**
	 * Returns the SellerFM Mapping from cache.
	 * If object not found in cache, try to get it from data source 
	 * and if found add to cache for future reference.
	 * 
	 * @param sellerCode
	 * @return sellerFMMappingDTO
	 * @throws ExternalDataNotFoundException
	 */
	@SuppressWarnings("unused")
	@Override
	public SellerFmFcDTO getSellerFmFcDTO(String sellerCode) throws ExternalDataNotFoundException{
		if(StringUtils.isEmpty(sellerCode)){
			LOG.warn("Illegal paramters passed while trying to find fm at seller level");
			throw new ExternalDataNotFoundException("Required Parameters not there while getting FM At Seller Level");
		}

        LOG.debug("Going to read fulfilment model from cache for sellerCode {}", sellerCode);
        SellerFmFcDTO sellerFmFcDTO = null;
        sellerFmFcDTO = CacheManager.getInstance().getCache(SellerFMMappingCache.class).getSellerFmFcMapping(sellerCode);
        LOG.debug("Fulfilment model obtained from sellerFMMapping cache:" + sellerFmFcDTO);

        //if object not in cache , try to get it from data source and if found add to cache.
        if (sellerFmFcDTO == null) {
            //get data source
            IFMDataSourceService fmService = fmDataSource.getFMDataSourceService();
            sellerFmFcDTO = fmService.getSellerFmFcDTO(sellerCode);
            if (sellerFmFcDTO != null) {
                LOG.debug("fulfilment model for sellerCode{} not found in cache, fetching the value from Datasource {} and adding it to cache {}", sellerCode,
                        ConfigUtils.getStringScalar(Property.DATA_SOURCE_FOR_FM_READ).toUpperCase(), sellerFmFcDTO);
                CacheManager.getInstance().getCache(SellerFMMappingCache.class).addSellerFmFc(sellerFmFcDTO);
            }
        }
		return sellerFmFcDTO;
	}

	@Override
	public SellerSupcFmFcDTO getSellerSupcFmFcDTO(String sellerCode,String supc) throws ExternalDataNotFoundException {
		if(StringUtils.isEmpty(sellerCode)){
			LOG.warn("Illegal paramters passed while trying to find fm at seller-supc level");
			throw new ExternalDataNotFoundException("Required Parameters [sellerCode] not there while getting FM At Seller-Supc Level");
		}else if(StringUtils.isEmpty(supc)){
			LOG.warn("Illegal paramters passed while trying to find fm at seller-supc level");
			throw new ExternalDataNotFoundException("Required Parameters [supc] not there while getting FM At Seller-Supc Level");
		}

        LOG.debug("Going to read fulfilment model from cache for sellerCode {} supc {}", sellerCode, supc);
        SellerSupcFmFcDTO sellerSupcFmFcMappingDTO = null;
        sellerSupcFmFcMappingDTO = CacheManager.getInstance().getCache(SellerSupcFMMappingCache.class).getSellerSupcFMMapping(sellerCode, supc);
        LOG.debug("Fulfilment model obtained from SellerSupcFMMappingCache cache:" + sellerSupcFmFcMappingDTO);

		return sellerSupcFmFcMappingDTO;
	}
}
