package com.snapdeal.cocofs.services.producer;

import java.util.List;

import com.snapdeal.cocofs.model.queue.SdInstantUpdateMessage;

public interface IExternalNotificationsServiceForSdInstantUpdate extends IQueueListener {

    boolean publishToQueue(List<SdInstantUpdateMessage> msg);
}
