/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 07-May-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.services.cachereload;

public interface IAutoCacheReloadService {
    
    public void reloadCache();

}
