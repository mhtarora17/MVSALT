package com.snapdeal.cocofs.services.events.dto;

import java.util.List;

public class ParameterList {

    private List<ParameterInfo> parameters;

    public List<ParameterInfo> getParameters() {
        return parameters;
    }

    public void setParameters(List<ParameterInfo> parameters) {
        this.parameters = parameters;
    }

}
