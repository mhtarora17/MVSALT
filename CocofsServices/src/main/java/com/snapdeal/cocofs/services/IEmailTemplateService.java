/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 05-Sep-2013
 *  @author himanshum
 */
package com.snapdeal.cocofs.services;

import com.snapdeal.cocofs.entity.EmailTemplate;

public interface IEmailTemplateService {

    EmailTemplate update(EmailTemplate emailTemplate);

    EmailTemplate getEmailTemplateByName(String templateName);

}
