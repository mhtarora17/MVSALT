/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.services.impl;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.snapdeal.base.cache.CacheManager;
import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.cocofs.annotations.TouchyTransaction;
import com.snapdeal.cocofs.cache.loader.impl.SellerContactDetailCache;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.core.aerospike.vo.SellerDetailVO;
import com.snapdeal.cocofs.db.dao.ISellerDetailsDao;
import com.snapdeal.cocofs.entity.SellerContactDetails;
import com.snapdeal.cocofs.entity.SellerZoneMapping;
import com.snapdeal.cocofs.external.service.ISnapdealGeoExternalService;
import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.generic.persister.exception.GenericPersisterException;
import com.snapdeal.cocofs.generic.persister.exception.GenericPersisterExceptionCode;
import com.snapdeal.cocofs.services.ISellerDetailsService;
import com.snapdeal.cocofs.services.ISellerZoneService;
import com.snapdeal.cocofs.services.converter.IConverterService;
import com.snapdeal.cocofs.services.fm.dto.AddressDTO;
import com.snapdeal.cocofs.sro.AddressSRO;
import com.snapdeal.geo.base.sro.ZoneRequest;
import com.snapdeal.score.data.aerospike.exception.GetFailedException;
import com.snapdeal.score.data.aerospike.service.impl.ScoreAerospikeService;

/**
 * @version 1.0, 15-Jun-2015
 * @author gaurav
 */

@Service("sellerDetailsService")
public class SellerDetailsServiceImpl implements ISellerDetailsService {

    private static final Logger      LOG = LoggerFactory.getLogger(SellerDetailsServiceImpl.class);

    @Autowired
    private ISellerDetailsDao  sellerDetailsDao;
    
    @Autowired
    private ISellerZoneService sellerZoneService;
    
    @Autowired
    private IConverterService converterService;
    
    @Autowired
    @Qualifier("aerospikeService")
    private ScoreAerospikeService   aerospikeService;
    
    @Autowired
    private ISnapdealGeoExternalService geoExternalService;

    @Override
    @TouchyTransaction
    public SellerContactDetails createUpdateSellerContactDetails(
            SellerContactDetails scd) throws GenericPersisterException, ExternalDataNotFoundException {
        LOG.info("Processing request to create/update seller contact details : {}",scd);
        SellerContactDetails updatedScd = null;
        String zone = null;
        SellerContactDetails existingScd = sellerDetailsDao.getSellerContactDetailsBySeller(scd.getSellerCode());
        //Check if there is a need to fetch zone from P-Z system...i.e check if pincode is same or has changed...
        boolean sellerZonePersistenceNotRequired = (existingScd !=null && existingScd.getPinCode().equalsIgnoreCase(scd.getPinCode()));
        
        if(sellerZonePersistenceNotRequired){
            LOG.info("The pincode for this seller has not changed. Therefore, not hitting P-Z system...");
        } else{
            //First fetch zone for pincode...if this fails, propagate the exception upwards...
            ZoneRequest zrequest = new ZoneRequest();
            zrequest.setPincode(scd.getPinCode());
            zone = geoExternalService.getZoneFromPincode(zrequest).getZone();
            if(StringUtils.isEmpty(zone)){
                throw new ExternalDataNotFoundException("Zone found out to be null from external P-Z system for this pincode.");
            }
        }
        
        
        //Now persist seller details only in db...
        try{
            updatedScd = persistSellerContactDetails(scd);
        } catch(Exception ex){
            LOG.error("Failed to persist seller contact details in DB:{} with exception:{}",scd,ex.getMessage());
            throw new GenericPersisterException(ex,"Not able to persist seller contact details. Reverting tx.",GenericPersisterExceptionCode.RECORD_FAILED_TO_PERSIST);
        }
        
        //Now persist seller zone in db and aerospike only if it is required...
        if(!sellerZonePersistenceNotRequired){
            SellerZoneMapping szm = converterService.getSellerZoneMappingFromDetails(updatedScd, zone);
            SellerZoneMapping updatedSzm = sellerZoneService.persistSellerZoneMapping(szm);
        }
        return updatedScd;
    }

    @Override
    @Transactional
    public SellerContactDetails getSellerContactDetailsBySeller(
            String sellerCode) {
        LOG.info("Processing request to get seller contact details by code: {}",sellerCode);
        return sellerDetailsDao.getSellerContactDetailsBySeller(sellerCode);
    }
    
    
    
    @Override
    @TouchyTransaction
    public SellerContactDetails persistSellerContactDetails(SellerContactDetails scd) throws GenericPersisterException {
        SellerContactDetails updatedScd = null;
        //First persist in db...
        try {
            SellerContactDetails existingScd = sellerDetailsDao.getSellerContactDetailsBySeller(scd.getSellerCode());
            if (existingScd != null) {
                //Need to update if it already exists...
                scd.setId(existingScd.getId());
                scd.setCreated(existingScd.getCreated());
            } else {
                scd.setCreated(DateUtils.getCurrentTime());
            }
            updatedScd = sellerDetailsDao.createUpdateSellerContactDetails(scd);
        } catch (Exception ex) {
            LOG.error("Failed to persist seller zone in db:{}", scd);
            throw new GenericPersisterException(ex, "Not able to persist seller contact details. Reverting tx.", GenericPersisterExceptionCode.RECORD_FAILED_TO_PERSIST);
        }
        //Now try to persist in aerospike
        try {
            SellerDetailVO sdv = converterService.getSellerDetailVoFromEntity(updatedScd);
            aerospikeService.put(sdv);
        } catch (Exception ex) {
            LOG.error("Failed to persist seller zone in aerospike:{} with error:{}", scd, ex.getMessage());
            throw new GenericPersisterException(ex, "Not able to persist seller zone details in aerospike. Reverting tx.", GenericPersisterExceptionCode.RECORD_FAILED_TO_PERSIST);
        }
        return updatedScd;
    }
    
    @TouchyTransaction
    @Override
    public AddressSRO getSellerDetails(String sellerCode) {
        LOG.info("Processing request to fetch seller-Details for sellerCode:{}",sellerCode);
        AddressDTO dto = null;
        AddressSRO sro = null;
        try {
            if(ConfigUtils.getBooleanScalar(Property.ENABLE_SELLER_DETAIL_CACHE)){
                //I level - hit lru cache(if flag is enabled).. + II level - aerospike hit..
                 dto  = CacheManager.getInstance().getCache(SellerContactDetailCache.class).getSellerDetails(sellerCode);
            } else{
                //Fetch directly from the defined data source...
                dto = getSellerDetailsFromBaseSource(sellerCode);
            }
        } catch (ExternalDataNotFoundException e) {
            LOG.warn("Seller detail info not found in cache & aerospike.",e);
        } catch(Exception ex){
            LOG.error("Unknown exception occurred while fetching data from sellerCode: "+ sellerCode,ex);
        }
        if(dto != null){
            sro = converterService.getAddressSROfromDTO(dto);
        }
        return sro;
    }
    

    

    @TouchyTransaction
    @Override
    public AddressDTO getSellerDetailsFromBaseSource(String sellerCode) {
        AddressDTO address = null;
        LOG.info("Processing request to fetch seller details for sellerCode={} from base data source",sellerCode);
        if(ConfigUtils.getStringScalar(Property.DATA_SOURCE_FOR_SELLER_DETAIL_READ).equalsIgnoreCase("AEROSPIKE")){
            SellerDetailVO sdv = null;
            try {
                sdv = (SellerDetailVO)aerospikeService.get(sellerCode, SellerDetailVO.class);
                address = sdv==null ? null: converterService.getAddressDTO(sdv);
            } catch (GetFailedException e) {
                e.printStackTrace();
            }
        }
        else if(ConfigUtils.getStringScalar(Property.DATA_SOURCE_FOR_SELLER_DETAIL_READ).equalsIgnoreCase("MYSQL")){
            SellerContactDetails scd = sellerDetailsDao.getSellerContactDetailsBySeller(sellerCode);
            
            address = scd==null ? null:converterService.getAddressDTO(scd);
                        
            
        } else{
            LOG.error("Unknown data source encountered while fetching data for seller-mapping for seller:{}.",sellerCode);
        }
        return address;
    }
    

}
