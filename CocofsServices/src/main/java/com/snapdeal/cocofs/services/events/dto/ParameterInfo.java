package com.snapdeal.cocofs.services.events.dto;

public class ParameterInfo {

    private String type;
    private String jsonString;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getJsonString() {
        return jsonString;
    }

    public void setJsonString(String jsonString) {
        this.jsonString = jsonString;
    }

    @Override
    public String toString() {
        return "ParameterInfo [type=" + type + ", jsonString=" + jsonString + "]";
    }

}
