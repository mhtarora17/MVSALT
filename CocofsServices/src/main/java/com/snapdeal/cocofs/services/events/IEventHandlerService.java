/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.services.events;

import java.util.List;

import com.snapdeal.cocofs.entity.EventInstance;
import com.snapdeal.cocofs.services.events.dto.EventDetailDTO;

/**
 * @version 1.0, 23-Jan-2015
 * @author shiv
 */
public interface IEventHandlerService {

    void callEventAsynchronously(Integer id);

    void callEventSynchronously(Integer id);

    void callEventForcefully(Integer id);

    List<EventDetailDTO> getEvents(boolean allEvents, boolean overrideEnable);

    List<EventInstance> getEventsToExecute(boolean overrideEnable);

    void callEventSynchronouslyWithEvent(EventInstance event);

}
