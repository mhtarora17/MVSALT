/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.services.events.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.Gson;
import com.snapdeal.cocofs.db.dao.IEventHandlerDAS;
import com.snapdeal.cocofs.entity.EventInstance;
import com.snapdeal.cocofs.services.events.IEventHandlerService;
import com.snapdeal.cocofs.services.events.IEventProducer;
import com.snapdeal.cocofs.services.events.dto.ParameterInfo;
import com.snapdeal.cocofs.services.events.dto.ParameterList;

/**
 * @version 1.0, 26-Jan-2015
 * @author shiv
 */
public abstract class BaseEventProducer<T> implements IEventProducer<T> {

    private static final Logger  LOG = LoggerFactory.getLogger(BaseEventProducer.class);

    @Autowired
    private IEventHandlerDAS     eventHandlerDAS;

    @Autowired
    private IEventHandlerService eventHandlerService;

    protected abstract String getEventCode();

    protected abstract void setParams(EventInstance eventInstance, T event);

    @Override
    public void createEventAndCallAsynchronously(T event) {
        //TODO check getEventArgs method is needed
        Object[] args = getEventArgs(event);
        String jsonConvertedArguments = null;
        if (args.length > 0) {
            jsonConvertedArguments = getJsonFromArguments(args);
        }
        EventInstance eventInstance = enrichEventInstance(jsonConvertedArguments, event);
        eventInstance = eventHandlerDAS.save(eventInstance);
        eventHandlerService.callEventAsynchronously(eventInstance.getId());
    }

    //TODO - make it protected
    private Object[] getEventArgs(T event) {
        Object[] args = { event };
        return args;
    }

    private EventInstance enrichEventInstance(String jsonConvertedArguments, T event) {
        EventInstance eventInstance = new EventInstance();
        eventInstance.setCreated(new Date());
        eventInstance.setEnabled(true);
        eventInstance.setExecuted(false);
        eventInstance.setJsonArguments(jsonConvertedArguments);
        eventInstance.setRetryCount(0);
        eventInstance.setEventType(getEventCode());
        setParams(eventInstance, event);
        return eventInstance;
    }

    private String getJsonFromArguments(Object[] args) {
        Gson gson = new Gson();
        List<ParameterInfo> paramList = new ArrayList<ParameterInfo>();
        for (Object argument : args) {
            ParameterInfo param = new ParameterInfo();
            param.setType(argument.getClass().getName());
            param.setJsonString(gson.toJson(argument));
            paramList.add(param);
        }
        ParameterList params = new ParameterList();
        params.setParameters(paramList);
        String jsonParam = gson.toJson(params);
        LOG.info("json arguments structure created:" + jsonParam);
        return jsonParam;
    }
}
