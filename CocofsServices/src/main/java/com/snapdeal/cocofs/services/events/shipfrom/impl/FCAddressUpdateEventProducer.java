/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.services.events.shipfrom.impl;

import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.entity.EventInstance;
import com.snapdeal.cocofs.services.events.dto.FCAddressUpdateEventObj;
import com.snapdeal.cocofs.services.events.impl.BaseEventProducer;
import com.snapdeal.cocofs.services.events.impl.EventType;

/**
 * @version 1.0, 25-Mar-2015
 * @author shiv
 */

@Service("FCAddressUpdateEventProducer")
public class FCAddressUpdateEventProducer extends BaseEventProducer<FCAddressUpdateEventObj> {

    @Override
    protected String getEventCode() {
        return EventType.SHIPFROM_UPDATE_EVENT_ON_FC_ADDRESS_UPDATE.getCode();
    }

    @Override
    public void setParams(EventInstance eventInstance, FCAddressUpdateEventObj eventObj) {
        eventInstance.setParam1(eventObj.getNewPincode());
        eventInstance.setParam2(eventObj.getFcCode());
        eventInstance.setCreated(eventObj.getCreated());
    }

}
