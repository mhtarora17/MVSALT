/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.services.taxrate.impl;

import java.util.ArrayList;
import java.util.List;

import javax.naming.OperationNotSupportedException;

import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.entity.SellerCategoryTaxRate;
import com.snapdeal.cocofs.mongo.model.SellerCategoryTaxRateUnit;
import com.snapdeal.cocofs.services.taxrate.dto.SellerCategoryTaxRateUploadDTO;

/**
 * @version 1.0, 18-Aug-2014
 * @author ankur
 */

@Service("SellerCategoryTaxRateDataEngine")
public class SellerCategoryTaxRateDataEngine extends AbstractTaxRateDataEngine<SellerCategoryTaxRateUploadDTO, SellerCategoryTaxRate, SellerCategoryTaxRateUnit> {

    @Override
    public List<SellerCategoryTaxRateUnit> enrichDocuments(SellerCategoryTaxRateUploadDTO dto, List<SellerCategoryTaxRateUnit> documents) throws OperationNotSupportedException {
        if (dto != null) {
            SellerCategoryTaxRateUnit document = null;
            List<SellerCategoryTaxRateUnit> documentList = new ArrayList<SellerCategoryTaxRateUnit>();
            if (null != documents && !documents.isEmpty()) {
                document = documents.get(0);
            } else {
                document = new SellerCategoryTaxRateUnit();
            }
            updateSellerCategoryTaxRateUnit(dto, document);
            documentList.add(document);
            return documentList;
        }
        return null;
    }

    private void updateSellerCategoryTaxRateUnit(SellerCategoryTaxRateUploadDTO dto, SellerCategoryTaxRateUnit document) {
        enrichTaxRateUnit(dto, document);
        document.setCategoryUrl(dto.getCategoryUrl());
        document.setSellerCode(dto.getSellerCode());

    }

    @Override
    public List<SellerCategoryTaxRate> enrichEntities(SellerCategoryTaxRateUploadDTO dto, List<SellerCategoryTaxRate> entities, String userEmail)
            throws OperationNotSupportedException {
        if (null != dto) {
            SellerCategoryTaxRate entity = null;
            List<SellerCategoryTaxRate> entityList = new ArrayList<SellerCategoryTaxRate>();
            if (null != entities && !entities.isEmpty()) {
                entity = entities.get(0);
            } else {
                entity = new SellerCategoryTaxRate();
            }
            updateSellerCategoryTaxRate(dto, entity, userEmail);
            entityList.add(entity);
            return entityList;
        }
        return null;
    }

    private void updateSellerCategoryTaxRate(SellerCategoryTaxRateUploadDTO dto, SellerCategoryTaxRate entity, String userEmail) {
        enrichTaxRateEntity(dto, entity, userEmail);
        entity.setCategoryUrl(dto.getCategoryUrl());
        entity.setSellerCode(dto.getSellerCode());

    }

}
