/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 17, 2013
 *  @author himanshu
 */
package com.snapdeal.cocofs.services;

import java.util.List;

import com.snapdeal.cocofs.common.RuleCreationDTO;
import com.snapdeal.cocofs.common.RuleEditDTO;
import com.snapdeal.cocofs.common.RuleUpdateResponse;
import com.snapdeal.rule.engine.entity.Rule;

public interface IRuleManagement {

    RuleCreationDTO getRuleCreationDTO(String ruleType, String categoryUrl, String fragile, String hazmat, String liquid, String weight, String volweight, String enddate,
            String startdate, String user);

    /**
     * Returns the list of enabled rules that are present in db for this category and url
     * @param categoryUrl
     * @param type
     * @return
     */
    List<Rule> getEnabledRulesForCategoryAndType(String categoryUrl, String type);

    /**
     * Create a rule for given dto, assumption is that you have already checked that rule for category and type doesn't exist
     * @param creationDTO
     * @return
     */
    Rule createRule(RuleCreationDTO creationDTO);

    /**
     * Try and update rules in mongo and DB corresponding to the dto
     * in case of failure, failure reason is provided in response, else its null
     * check for successful flag in response to see if update went well or not
     * @param dto
     * @return
     */
    RuleUpdateResponse updateRule(RuleEditDTO dto, String ruleName);

    /**
     * Create and dto to be used for updating the rules given the fields
     * @param ruleType
     * @param categoryUrl
     * @param fragile
     * @param hazmat
     * @param liquid
     * @param weight
     * @param volweight
     * @param enddate
     * @param startdate
     * @param user
     * @param enabled
     * @return
     */
    RuleEditDTO getRuleEditDTO(String ruleType, String categoryUrl, String fragile, String hazmat, String liquid, String weight, String volweight, String enddate,
            String startdate, String user, String enabled);

    /**
     * Get rule edit dto from rule object
     * @param rule
     * @return
     */
    RuleEditDTO getRuleEditDTOFromRule(Rule rule);

}
