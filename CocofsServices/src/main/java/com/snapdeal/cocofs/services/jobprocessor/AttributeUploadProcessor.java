/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 06-Sep-2013
 *  @author himanshum
 */
package com.snapdeal.cocofs.services.jobprocessor;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.naming.OperationNotSupportedException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.cocofs.common.BulkUploadValidationDTO;
import com.snapdeal.cocofs.common.BulkUploadValidationResponse;
import com.snapdeal.cocofs.common.JobProcessorResponse;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.entity.JobDetail;
import com.snapdeal.cocofs.entity.PendingProductAttributeUpdate;
import com.snapdeal.cocofs.entity.ProductAttribute;
import com.snapdeal.cocofs.enums.BulkUploadValidationError;
import com.snapdeal.cocofs.excelupload.ProductAttributeUploadDTO;
import com.snapdeal.cocofs.external.service.IScoreExternalService;
import com.snapdeal.cocofs.generic.persister.exception.GenericPersisterException;
import com.snapdeal.cocofs.generic.persister.response.GenericPersisterResponse;
import com.snapdeal.cocofs.generic.persister.validation.handler.response.PAPersisterPostValidationHandlerResponse;
import com.snapdeal.cocofs.mongo.model.ProductAttributeUnit;
import com.snapdeal.cocofs.services.IDataReader;
import com.snapdeal.cocofs.services.IDataUpdater;
import com.snapdeal.cocofs.services.IJobProcessor;
import com.snapdeal.cocofs.services.IJobSchedulerService;
import com.snapdeal.cocofs.services.common.dto.UserInfo;
import com.snapdeal.cocofs.services.converter.IConverterService;
import com.snapdeal.cocofs.services.data.engine.IDataEngine;
import com.snapdeal.cocofs.services.events.IEventProducer;
import com.snapdeal.cocofs.services.events.dto.ProductAttributeScoreEvent;
import com.snapdeal.cocofs.utils.DangerousGoodsTypeConversionUtil;
import com.snapdeal.cocofs.utils.ExcelReader;
import com.snapdeal.cocofs.utils.FTPUtils;
import com.snapdeal.cocofs.utils.ProductAttributeUtil;
import com.snapdeal.score.model.request.UpdateProductFulfilmentAttributeRequest;
import com.sun.media.sound.InvalidDataException;

@Component
public class AttributeUploadProcessor implements IJobProcessor {

    private static final Logger                                                            LOG = LoggerFactory.getLogger(AttributeUploadProcessor.class);

    @Autowired
    @Qualifier("productAttributeUploadReader")
    private IDataReader<ProductAttributeUploadDTO, ProductAttribute, ProductAttributeUnit> dataReader;

    @Autowired
    @Qualifier("productAttributeUploadEngine")
    private IDataEngine<ProductAttributeUploadDTO, ProductAttribute, ProductAttributeUnit> dataMerger;

    @Autowired
    private IDataUpdater                                                                   dataUpdaterService;
    @Autowired
    private IJobSchedulerService                                                           jobService;

    @Autowired
    private IScoreExternalService                                                          scoreService;

    @Autowired
    private IConverterService                                                              converterService;

    @Autowired
    @Qualifier("ProductAttributeUpdateScoreEventProducer")
    private IEventProducer<ProductAttributeScoreEvent>                                     productAttributeUpdateScoreEventProducer;

    @PostConstruct
    public void init() {
        jobService.addJobProcessor("AttributeUpdate", this);
        jobService.addJobProcessor("SuperAttributeUpdate", this);
    }

    @Override
    public JobProcessorResponse process(JobDetail jobDetail) {
        JobProcessorResponse response = new JobProcessorResponse();
        LOG.info("processing file:" + jobDetail.getFileName() + " from path:" + jobDetail.getFilePath() + " status:" + jobDetail.getStatus().getCode());

        String ftpServerPath = ConfigUtils.getStringScalar(Property.COCOFS_FTP_SERVER_PATH);
        String ftpUsername = ConfigUtils.getStringScalar(Property.COCOFS_FTP_USERNAME);
        String ftpPassword = ConfigUtils.getStringScalar(Property.COCOFS_FTP_PASSWORD);

        String filepath = jobDetail.getFilePath() + File.separator + jobDetail.getFileName();
        // We save file on identical paths on local FS and FTP
        FTPUtils.downloadFile(ftpServerPath, ftpUsername, ftpPassword, filepath, filepath);
        boolean atleastOneRecordProcessed = false;
        boolean isSuperUpload = false;
        if (jobDetail.getAction().getCode().equals("SuperAttributeUpdate")) {
            isSuperUpload = true;
        }

        UserInfo i = new UserInfo();
        i.setEmail(jobDetail.getUploadedBy() + ";" + jobDetail.getFileCode());
        i.setSuper(isSuperUpload);
        Map<String, List<String>> errorMap = new HashMap<String, List<String>>();
        try {
            List<ProductAttributeUploadDTO> uploadList = ExcelReader.readFile(filepath, ProductAttributeUploadDTO.class, true);
            unsetSystemWeightCapturedFlag(uploadList);

            int row = 0;
            int total = uploadList.size();
            for (ProductAttributeUploadDTO dto : uploadList) {
                row++;
                long tStart = System.currentTimeMillis();
                try {
                    LOG.info("processing started for supc {} at row: {} of total: {};", dto.getSupc(), row, total);
                    // PAPersisterPostValidationHandlerResponse
                    //TODO get response from following call and send even that in email
                    GenericPersisterResponse<ProductAttribute, ProductAttributeUnit> resp = dataUpdaterService.updateDataWithDTO(dto, dataReader, dataMerger, i);

                    if (!resp.isSuccessful()) {
                        if (!errorMap.containsKey("RowError")) {
                            errorMap.put("RowError", new ArrayList<String>());
                        }
                        errorMap.get("RowError").add("Error in row with SUPC : " + dto.getSupc() + " Error : " + resp.getMessage());
                    } else {
                        atleastOneRecordProcessed = true;
                        if (resp instanceof PAPersisterPostValidationHandlerResponse) {
                            PAPersisterPostValidationHandlerResponse r = (PAPersisterPostValidationHandlerResponse) resp;

                            if (!r.getPendingPAList().isEmpty() && r.isPendingPersisted()) {
                                if (!errorMap.containsKey("PendingList")) {
                                    errorMap.put("PendingList", new ArrayList<String>());
                                }
                                StringBuffer sb = new StringBuffer("Partial Update SUPC : " + dto.getSupc() + " Attributes pending persistence : ");
                                for (PendingProductAttributeUpdate u : r.getPendingPAList()) {
                                    sb.append(" " + u.getAttributeName() + " ");
                                }

                                errorMap.get("PendingList").add(sb.toString());
                            } //TODO add one more else case for pending persistence failure
                        }
                        /*
                         * At the end, update the attributes in Score in silent mode 
                         */
                        try {
                            ProductAttributeUnit pau = resp.getPersistedDocument().get(0);
                            if (pau != null) {
                                UpdateProductFulfilmentAttributeRequest scoreUploadDtoRequest = converterService.getUpdateProductAttributesSRORequest(pau);
                                boolean result = scoreService.updateProductFulfilmentAttributesInScore(scoreUploadDtoRequest);
                                if (!result) {
                                    //add event for retrial
                                    ProductAttributeScoreEvent event = new ProductAttributeScoreEvent(scoreUploadDtoRequest);
                                    productAttributeUpdateScoreEventProducer.createEventAndCallAsynchronously(event);
                                    LOG.info("Added event for updating PA in score...");
                                }
                            }
                        } catch (Exception ex) {
                            LOG.error("Unable to add event to push PAs to score.{}", ex);
                        }
                    }

                } catch (GenericPersisterException e) {
                    LOG.info("Could not update dto " + dto, e);
                    if (!errorMap.containsKey("RowError")) {
                        errorMap.put("RowError", new ArrayList<String>());
                    }
                    errorMap.get("RowError").add("Error in row with SUPC : " + dto.getSupc() + " Error : " + e.getMessage());
                } catch (OperationNotSupportedException e) {
                    if (!errorMap.containsKey("RowError")) {
                        errorMap.put("RowError", new ArrayList<String>());
                    }
                    LOG.info("Processing error for row with supc " + dto.getSupc(), e);
                    errorMap.get("RowError").add("Error in row with SUPC : " + dto.getSupc() + " Error : " + e.getMessage());
                } catch (Exception e) {
                    if (!errorMap.containsKey("RowError")) {
                        errorMap.put("RowError", new ArrayList<String>());
                    }
                    LOG.info("Processing error for row with supc " + dto.getSupc(), e);
                    errorMap.get("RowError").add("Error in row with SUPC : " + dto.getSupc() + " Error : " + e.getMessage());
                }
                LOG.info("processing done for supc {} at row: {} of total: {}; took {} ms", dto.getSupc(), row, total, (System.currentTimeMillis() - tStart));
            }
        } catch (InvalidDataException e) {
            updateErrorMapForFileError(jobDetail, errorMap, e);
            LOG.info("File Upload " + filepath + " corresponding to job detail " + jobDetail.getFileCode() + " could not be processed ", e);
        } catch (IOException e) {
            updateErrorMapForFileError(jobDetail, errorMap, e);
            LOG.info("File Upload " + filepath + " corresponding to job detail " + jobDetail.getFileCode() + " could not be processed ", e);
        } catch (ClassNotFoundException e) {
            updateErrorMapForFileError(jobDetail, errorMap, e);
            LOG.info("File Upload " + filepath + " corresponding to job detail " + jobDetail.getFileCode() + " could not be processed ", e);
        }
        if (!errorMap.isEmpty()) {
            response.setError(errorMap);
        }
        response.setProcessingSuccessful(atleastOneRecordProcessed);
        return response;
    }

    private void updateErrorMapForFileError(JobDetail jobDetail, Map<String, List<String>> errorMap, Exception e) {
        if (!errorMap.containsKey("FileError")) {
            errorMap.put("FileError", new ArrayList<String>());

        }
        errorMap.get("FileError").add("Error in reading file for job " + jobDetail.getFileCode() + " Error Message " + e.getMessage());
    }

    @Override
    public BulkUploadValidationResponse validateJob(File file, String actionCode, String param) {
        BulkUploadValidationResponse response = new BulkUploadValidationResponse();
        List<BulkUploadValidationDTO> validationDTOList = new ArrayList<BulkUploadValidationDTO>();
        try {
            List<ProductAttributeUploadDTO> uploadList = ExcelReader.readFile(file.getAbsolutePath(), ProductAttributeUploadDTO.class, true);

            Set<ProductAttributeUploadDTO> dtoSet = new HashSet<ProductAttributeUploadDTO>(uploadList);

            LOG.info("Uploaded records count - {}", uploadList.size());

            if (uploadList.size() != dtoSet.size()) {
                LOG.info("List size uploadList.size() {} unique supc set size {}", uploadList, dtoSet.size());
                response.setErrorMessage("Duplicate supc combination exists in the file. Total records: " + uploadList.size() + ", unique records: " + dtoSet.size());
                response.setValid(false);
                return response;
            }
            int row = 0;
            StringBuilder sb = new StringBuilder();
            for (ProductAttributeUploadDTO productAttributeUploadDTO : uploadList) {
                row++;
                if (StringUtils.isEmpty(productAttributeUploadDTO.getSupc())) {
                    validationDTOList.add(new BulkUploadValidationDTO(row, BulkUploadValidationError.SUPC_MISSING.getDescription(), productAttributeUploadDTO.getSupc()));
                    sb.append("SUPC missing from rows : " + row);

                }

                if (StringUtils.isNotEmpty(productAttributeUploadDTO.getDangerousGoodsType())) {
                    if (!DangerousGoodsTypeConversionUtil.isValidDangerousGoodsType(productAttributeUploadDTO.getDangerousGoodsType())) {
                        validationDTOList.add(new BulkUploadValidationDTO(row, BulkUploadValidationError.INVALID_DANGEROUS_GOODS_TYPE.getDescription(),
                                productAttributeUploadDTO.getDangerousGoodsType()));
                        sb.append("Invalid Dangerous Goods Type in row : " + row);

                    }
                }

                if (StringUtils.isNotEmpty(productAttributeUploadDTO.getSerializedType())) {
                    if (!ProductAttributeUtil.isValidSerializedType(productAttributeUploadDTO.getSerializedType())) {
                        validationDTOList.add(new BulkUploadValidationDTO(row, BulkUploadValidationError.INVALID_SERIALIZED_TYPE.getDescription(),
                                productAttributeUploadDTO.getSerializedType()));
                        sb.append("Invalid Serialized Type in row : " + row);
                    }
                }

                if (StringUtils.isNotEmpty(productAttributeUploadDTO.getPackagingType())) {
                    if (!ProductAttributeUtil.isValidPackagingType(productAttributeUploadDTO.getPackagingType())) {
                        validationDTOList.add(
                                new BulkUploadValidationDTO(row, BulkUploadValidationError.INVALID_PACKAGING_TYPE.getDescription(), productAttributeUploadDTO.getPackagingType()));
                        sb.append("Invalid Packaging Type in row : " + row);
                    }
                }

            }
            if (!validationDTOList.isEmpty()) {
                response.setValid(false);
                response.setErrorMessage(sb.toString());
                response.setList(validationDTOList);
                return response;
            }

            response.setValid(true);
            return response;
        } catch (InvalidDataException e) {
            response.setValid(false);
            response.setErrorMessage(BulkUploadValidationError.REQUIRED_FIELDS_MISSING.getDescription() + " " + e.getMessage());
            LOG.error("Error while uploading the data for file {} for action type {} ", file.getAbsoluteFile(), actionCode, e);
        } catch (IOException e) {
            response.setValid(false);
            response.setErrorMessage(BulkUploadValidationError.UPLOAD_FILE_READ_ERROR.getDescription());
            LOG.error("Error while uploading the data for file {} for action type {} ", file.getAbsoluteFile(), actionCode, e);
        } catch (ClassNotFoundException e) {
            response.setValid(false);
            response.setErrorMessage(BulkUploadValidationError.FILE_TYPE_NOT_CORRECT.getDescription());
            LOG.error("Error while uploading the data for file {} for action type {} ", file.getAbsoluteFile(), actionCode, e);
        }

        return response;
    }

    /**
     * Explicitly set the systemWeightCaptured to false if the weight is captured through admin excel upload.
     */
    public void unsetSystemWeightCapturedFlag(List<ProductAttributeUploadDTO> uploadList) {
        if (ConfigUtils.getStringScalar(Property.SYSTEM_WEIGHT_CAPTURE_ENABLED) != null && ConfigUtils.getStringScalar(Property.SYSTEM_WEIGHT_CAPTURE_ENABLED).equals("true")) {
            for (ProductAttributeUploadDTO dto : uploadList) {
                if (dto.getWeight() != null) {
                    dto.setSystemWeightCaptured(false);
                }
            }
        }
    }

}
