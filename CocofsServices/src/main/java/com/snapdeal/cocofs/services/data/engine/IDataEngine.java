/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 09-Sep-2013
 *  @author himanshum
 */
package com.snapdeal.cocofs.services.data.engine;

import java.util.List;

import javax.naming.OperationNotSupportedException;

/**
 * @author abhinav
 * @param <T> DTO on which data engine will work
 * @param <E> Entity which will be enriched
 * @param <D> Document which will be enriched
 */
public interface IDataEngine<T, E, D> {

    /**    public void enrichTaxRateUnit(TaxRateUpdateDTO dto, AbstractTaxRateUnit document) {
        document.setCreated(document.getCreated() != null ? document.getCreated() : DateUtils.getCurrentTime());
        document.setUpdated(DateUtils.getCurrentTime());
        document.setTaxRate(dto.getTaxRate());
        document.setEnabled(true);        


    }

    public void enrichTaxRateEntity(TaxRateUpdateDTO dto, AbstractTaxRate entity, String userEmail) {
        entity.setCreated(entity.getCreated() != null ? entity.getCreated() : DateUtils.getCurrentTime());
        entity.setTaxRate(dto.getTaxRate());
        entity.setLastUpdated(DateUtils.getCurrentTime());
        entity.setUpdatedBy(userEmail);
        entity.setEnabled(true);        

    }
     * Update given mongo document list to correspond to dto T, and create new documents if required. The resulting
     * update documents list is returned.
     * 
     * @param dto
     * @param documents
     * @return
     * @throws OperationNotSupportedException
     */
    List<D> enrichDocuments(T dto, List<D> documents) throws OperationNotSupportedException;

    /**
     * Update given entity list to correspond to dto T, and create new entities if required. The resulting update
     * documents list is returned.
     * 
     * @param dto
     * @param documents
     * @return
     * @throws OperationNotSupportedException
     */
    List<E> enrichEntities(T dto, List<E> entities, String userEmail) throws OperationNotSupportedException;

}
