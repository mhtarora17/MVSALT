/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 28, 2013
 *  @author himanshu
 */
package com.snapdeal.cocofs.services;

import java.util.List;

import com.snapdeal.cocofs.entity.ProductAttribute;
import com.snapdeal.cocofs.mongo.model.ProductAttributeUnit;

public interface IProductAttributeUnitResetService {

    ProductAttributeUnit resetAttributes(ProductAttributeUnit unit, List<ProductAttribute> list);

}
