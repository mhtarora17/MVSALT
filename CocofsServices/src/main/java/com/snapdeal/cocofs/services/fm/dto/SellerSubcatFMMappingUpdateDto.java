/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 07-Oct-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.services.fm.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class SellerSubcatFMMappingUpdateDto extends SellerFMMappingUpdateDto {

    private String subcat;

    public SellerSubcatFMMappingUpdateDto() {
    }

    public SellerSubcatFMMappingUpdateDto(String sellerCode, String subcat, String fulfilmentModel, String fcCentres) {
        super(sellerCode, fulfilmentModel, fcCentres);
        this.subcat = subcat;
    }

    public String getSubcat() {
        return subcat;
    }

    public void setSubcat(String subcat) {
        this.subcat = subcat;
    }

}
