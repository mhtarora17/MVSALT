package com.snapdeal.cocofs.services.events.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.db.dao.IEventHandlerDAS;
import com.snapdeal.cocofs.entity.EventInstance;
import com.snapdeal.cocofs.services.converter.IConverterService;
import com.snapdeal.cocofs.services.events.IEventConsumer;
import com.snapdeal.cocofs.services.events.IEventConsumerRegistry;
import com.snapdeal.cocofs.services.events.IEventHandlerService;
import com.snapdeal.cocofs.services.events.dto.EventDetailDTO;

@Service("EventHandlerService")
public class EventHandlerService implements IEventHandlerService {

    private static final Logger    LOG = LoggerFactory.getLogger(EventHandlerService.class);

    @Autowired
    private IEventHandlerDAS       eventDBReadService;

    @Autowired
    @Qualifier("converterServiceImpl")
    private IConverterService      convertorService;

    @Autowired
    private IEventConsumerRegistry eventRegistry;

    private IEventConsumer<?> getServiceForEvent(String eventType) {
        return eventRegistry.getConsumer(eventType);
    }

    @Override
    @Async
    public void callEventAsynchronously(Integer id) {
        LOG.info("Asynchronously calling event for id:" + id);
        callEvent(id);
    }

    @Override
    public void callEventSynchronously(Integer id) {
        LOG.info("Synchronously calling event for id:" + id);
        callEvent(id);
    }

    private void callEvent(Integer id) {
        EventInstance eventInstance = eventDBReadService.getEventInstanceById(id);
        callEvent(eventInstance, isRetryable(eventInstance));
    }

    @Override
    public List<EventDetailDTO> getEvents(boolean allEvents, boolean overrideEnable) {
        List<EventInstance> eventInstances = null;
        if (allEvents) {
            eventInstances = eventDBReadService.getAllEventsInstances(overrideEnable);
        } else {
            eventInstances = eventDBReadService.getAllEventsToExecute(overrideEnable);
        }
        List<EventDetailDTO> dtos = new ArrayList<EventDetailDTO>();

        for (EventInstance instance : eventInstances) {
            EventDetailDTO dto = convertorService.getEventDetailDTO(instance);
            dtos.add(dto);
        }

        return dtos;
    }

    @Override
    public List<EventInstance> getEventsToExecute(boolean overrideEnable) {
        List<EventInstance> eventInstances = null;
        eventInstances = eventDBReadService.getAllEventsToExecute(overrideEnable);
        return eventInstances;
    }

    @Override
    public void callEventSynchronouslyWithEvent(EventInstance event) {
        LOG.info("Synchronously calling event for id:" + event.getId());
        callEvent(event, isRetryable(event));
    }

    private boolean isRetryable(EventInstance event) {
        int maxRetries = ConfigUtils.getIntegerScalar(Property.MAX_EVENT_RETRY_COUNT);
        boolean bResult = event.getRetryCount() < maxRetries;
        if (!bResult) {
            LOG.info("Event id:" + event.getId() + " reached/exceeded maximum retry count- current={} vs max={}", event.getRetryCount(), maxRetries);
        }
        return bResult;
    }

    private void callEvent(EventInstance eventInstance, boolean runFlag) {
        if (!runFlag) {
            LOG.info("Event id:" + eventInstance.getId() + " skipped run. - runFlag:{}", runFlag);
            return;
        }

        try {
            IEventConsumer<?> obj = getServiceForEvent(eventInstance.getEventType());
            if (obj != null) {
                Boolean boolRes = obj.processEvent(eventInstance);
                boolean retryEnabled = true;
                if (boolRes) {
                    eventInstance.setExecuted(true);
                    retryEnabled = false;
                } else {
                    eventInstance.setRetryCount(eventInstance.getRetryCount() + 1);
                    retryEnabled = isRetryable(eventInstance);
                }
                eventInstance.setEnabled(retryEnabled);
            } else {
                LOG.error("no consumer found for event --  {}", eventInstance);
            }
        } catch (RuntimeException e) {
            LOG.error("Catched:" + e.getMessage() + " with Runtime exception: " + e);
            LOG.info("exception {} ", e);
            eventInstance.setRetryCount(eventInstance.getRetryCount() + 1);
        } catch (Exception e) {
            LOG.error("Catch exception:" + e);
            LOG.info("exception {} ", e);
            eventInstance.setRetryCount(eventInstance.getRetryCount() + 1);
        }
        eventDBReadService.save(eventInstance);
    }

    @Override
    public void callEventForcefully(Integer id) {
        EventInstance eventInstance = eventDBReadService.getEventInstanceById(id);
        //call event forcefully, bypass the run
        callEvent(eventInstance, true);
    }
}