/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 07-Oct-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.services.fm.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.core.aerospike.vo.SellerSubcatFMMappingVO;
import com.snapdeal.cocofs.core.aerospike.vo.SellerSupcFMMappingVO;
import com.snapdeal.cocofs.dataaccess.fm.IFMAerospikeDataReadService;
import com.snapdeal.cocofs.dataaccess.fm.IFMDBDataReadService;
import com.snapdeal.cocofs.entity.SellerSupcFMMapping;
import com.snapdeal.cocofs.services.IDataReaderWithAerospike;
import com.snapdeal.cocofs.services.fm.dto.SellerSupcFMMappingUploadDto;
import com.snapdeal.cocofs.utils.AerospikeKeyHelper;

@Service("SellerSupcFMMappingDataReader")
public class SellerSupcFMMappingDataReader implements IDataReaderWithAerospike<SellerSupcFMMappingUploadDto, SellerSupcFMMapping, SellerSupcFMMappingVO> {

    @Autowired
    private IFMAerospikeDataReadService  fmAerospikeReadService;


    @Autowired
    private IFMDBDataReadService fmDBDataReadService;

   /* @Override
    @Deprecated
    public List<SellerSupcFMMappingUnit> getDocumentsForDTO(SellerSupcFMMappingUploadDto dto) {
        SellerSupcFMMappingUnit document = fmMongoService.getSellerSupcFMMappingUnit(dto.getSellerCode(), dto.getSupc());
        List<SellerSupcFMMappingUnit> documentList = new ArrayList<SellerSupcFMMappingUnit>();
        if (null != document) {
            documentList.add(document);
        }
        return documentList;
        return null;
    }*/

    @Override
    public List<SellerSupcFMMapping> getEntitiesForDTO(SellerSupcFMMappingUploadDto dto) {
        SellerSupcFMMapping mapping = fmDBDataReadService.getEnabledSellerSupcFMMapping(dto.getSellerCode(), dto.getSupc());
        List<SellerSupcFMMapping> mappingList = new ArrayList<SellerSupcFMMapping>();
        if (null != mapping) {
            mappingList.add(mapping);
        }
        return mappingList;
    }
    
    @Override
    public SellerSupcFMMappingVO getRecordForDTO(SellerSupcFMMappingUploadDto dto) {
        SellerSupcFMMappingVO record = (SellerSupcFMMappingVO)fmAerospikeReadService.getFMMapping(AerospikeKeyHelper.getKey(dto.getSellerCode(),dto.getSupc()), SellerSubcatFMMappingVO.class);
        
        return record;
    }

}
