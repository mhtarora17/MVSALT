package com.snapdeal.cocofs.services.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.core.aerospike.vo.FCDetailVO;
import com.snapdeal.cocofs.core.aerospike.vo.SellerFMMappingVO;
import com.snapdeal.cocofs.dataaccess.fc.IFCAerospikeDataReadService;
import com.snapdeal.cocofs.dataaccess.fm.IFMDBDataReadService;
import com.snapdeal.cocofs.entity.SellerSupcFMMapping;
import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.generic.persister.exception.GenericPersisterException;
import com.snapdeal.cocofs.model.queue.SdInstantUpdateMessage;
import com.snapdeal.cocofs.services.events.dto.FCAttributeUpdateEventObj;
import com.snapdeal.cocofs.services.events.dto.SellerFMFCUpdateEventObj;
import com.snapdeal.cocofs.services.events.dto.SellerSupcAddEventObj;
import com.snapdeal.cocofs.services.events.dto.SellerSupcFMFCExceptionAddEventObj;
import com.snapdeal.cocofs.services.events.dto.SellerSupcFMFCExceptionDeleteEventObj;
import com.snapdeal.cocofs.services.fc.IFCMappingService;
import com.snapdeal.cocofs.services.fm.IFulfilmentModelService;
import com.snapdeal.cocofs.services.fm.ISellerSupcSdInstantMappingProcessor;
import com.snapdeal.cocofs.services.fm.ISellerToSUPCListDBLookupService;
import com.snapdeal.cocofs.services.producer.IExternalNotificationsServiceForSdInstantUpdate;
import com.snapdeal.cocofs.sro.FulfillmentModel;
import com.snapdeal.score.data.aerospike.exception.GetFailedException;
import com.snapdeal.score.data.aerospike.exception.PutFailedException;

/**
 * Class to process Seller SUPC SdInstant Location data update. Collect the notifications and push to external service
 * to publish these notification to activemq <br>
 * 1) SALESFORCE send SELLER-FM/FC mappingUpdate add/updates via API hit <br>
 * 2) IPSM send SELLER-SUPC updates (new SUPC addition for SELLER) <br>
 * 3) CoCoFS Admin send SELLER-SUPC-FM/FC mappings to update/disable exceptions.<br>
 * 4) Change of sdInstant for a fulfillment Center
 * 
 * @author ankur
 */

@Service("SellerSUPCSdInstantMappingProcessorImpl")
public class SellerSUPCSdInstantMappingProcessorImpl implements ISellerSupcSdInstantMappingProcessor {

    private static final Logger LOG = LoggerFactory.getLogger(SellerSUPCSdInstantMappingProcessorImpl.class);

    @Autowired
    private ISellerToSUPCListDBLookupService sellerToSUPCListLookupService;
   
    @Autowired
    private IFMDBDataReadService fmDBService;

    @Autowired
    IFCMappingService fcMappingService;

    @Autowired
    IFCAerospikeDataReadService        fcAerospikeService;
    
    @Autowired
    private IFulfilmentModelService                              fulfilmentModelService;
    
   
  
    @Autowired
    @Qualifier("externalNotificationsServiceForSdInstantUpdateImpl")
    private IExternalNotificationsServiceForSdInstantUpdate sdInstantUpdateProducer;

    private IDependency dep = new Dependency();

    // case when FC adress is updated
    @Override
    public boolean processChangeOfFCAttributesForExternalNotification(FCAttributeUpdateEventObj event) throws GetFailedException, ExternalDataNotFoundException {
        if (!isSdInstantUpdatePushEnabled()) {
            // returning true as this is logically correct termination of the process flow
            return true;
        }
        LOG.info("processing started for change of Sd instant for FC , event:{}", event);

        List<SdInstantUpdateMessage> sdInstantMessageForQueuePush = new ArrayList<SdInstantUpdateMessage>();

        boolean bResult = true;
        try {
            int sdInstant = event.isSdInstant() ? 1 : 0;
            prepareSupcSellerListForFCToBePushed(event.getFcCode(), sdInstant, sdInstantMessageForQueuePush);
            bResult = pushToExternalNotificationService(sdInstantMessageForQueuePush);

        } catch (Exception e) {
            LOG.error("Could not fetch prepare seller supc list for sdInstant:{} fc:{} ", event.isSdInstant(), event.getFcCode());
            throw e;
        }

        LOG.info("processing done for change of change of sdInstant, returning {}", bResult);
        return bResult;

    }

    // case when SELLER-FM mapping is added/changed from salesforce/seller-fc-panel
    @Override
    public boolean processChangeOfSellerFMMappingForExternalNotification(SellerFMFCUpdateEventObj e) {

        if (!isSdInstantUpdatePushEnabled()) {
            // returning true as this is logically correct termination of the process flow
            return true;
        }

        LOG.info("processing started for change of seller fm mapping, event:{}", e);

        // find the corresponding FM type
        FulfillmentModel newFMType = FulfillmentModel.getFulfilmentModelByCode(e.getNewFM());
        FulfillmentModel oldFMType = FulfillmentModel.getFulfilmentModelByCode(e.getOldFM());

        List<String> existingCenters = e.getExistingFCCenters();
        String existingCenter = CollectionUtils.isEmpty(existingCenters) ? null : existingCenters.get(0);

        List<String> newCenters = e.getNewFCCenters();
        String newCenter = CollectionUtils.isEmpty(newCenters) ? null : newCenters.get(0);

        List<SdInstantUpdateMessage> sdInstantMessageForQueuePush = new ArrayList<SdInstantUpdateMessage>();
        // case 1) SELLER-FM is added first time, as this is supposed to be handled at SUPC push from IPMS
        if (oldFMType == null) {
            LOG.info("new SELLER-FM mapping is added for seller:{} FM:{}, no information is to be published for external service", e.getSellerCode(), newFMType);
        }else{ 
            // find all SUPC for the seller and create queue push for all SUPC
            // except in exception list

            FCDetailVO existingFc = null;
            FCDetailVO newFc = null;
            if(existingCenter != null){
                existingFc = getFCCenterForFCCode(existingCenter);
            }
            
            if(newCenter != null){
                newFc = getFCCenterForFCCode(newCenter);
            }
            
            int oldSdInstant =  existingFc != null ? existingFc.getSdinstant(): 0;
            int newSdInstant = newFc != null ? newFc.getSdinstant(): 0;

            LOG.info("new FC is {}  and old FC is {} for seller "+ e.getSellerCode() , newFc, existingFc);
            
            if (oldSdInstant != newSdInstant) {
                LOG.info("newFCCenter's {} sdInstant  is different than existingFcCenter's {} sdInstant, hence push required", newFc, existingFc);
                prepareSupcSellerListToBePushed(e.getSellerCode(), newSdInstant, sdInstantMessageForQueuePush);
            }
          

        }

        boolean bResult = pushToExternalNotificationService(sdInstantMessageForQueuePush);
        LOG.info("processing done for change of seller-fm-fc-mapping, returning {}", bResult);
        return bResult;
    }

    @Override
    public boolean processAdditionOfNewSellerSUPCMappingForExternalNotification(SellerSupcAddEventObj event)
            throws GetFailedException, ExternalDataNotFoundException, PutFailedException, GenericPersisterException {

        String sellerCode = event.getSellerCode();
        String supc = event.getSupc();

        if (!isSdInstantUpdatePushEnabled()) {
            // returning true as this is logically correct termination of the process flow
            return true;
        }
        boolean bResult = true;
        LOG.info("processing started for addition of new seller SUPC mapping, seller:{}, supc:{}", sellerCode, supc);
        try {

           List<String> centers = fulfilmentModelService.getEnabledFCsBySeller(event.getSellerCode(), false);

           if (!CollectionUtils.isEmpty(centers)) {
                FCDetailVO fc = getFCCenterForFCCode(centers.get(0));
                if (fc != null) {
                    bResult = pushNotification(sellerCode, supc, fc.getSdinstant());
                }
            }

        } catch (ExternalDataNotFoundException e) {
            LOG.error("Could not fetch FMFC  for seller:{} supc:{} ", sellerCode, supc);
            throw e;
        }
        LOG.info("processing done for addition of seller-SUPC pair, returning {}", bResult);
        return bResult;
    }

    // case when seller-SUPC-FM-FC mapping is changed in exception list from CoCoFS admin
    @Override
    public boolean processChangeOfSellerSUPCFMMappingExceptionForExternalNotification(SellerSupcFMFCExceptionAddEventObj e) throws Exception {

        if (!isSdInstantUpdatePushEnabled()) {
            // returning true as this is logically correct termination of the process flow
            return true;
        }

        LOG.info("processing started for change of seller-SUPC-FM-FC exception mapping, event:{}", e);

        
        boolean bResult = true;
        FulfillmentModel oldFMType = FulfillmentModel.getFulfilmentModelByCode(e.getOldFM());      
        List<String> existingCenters = e.getExistingFCCenters();

        if(oldFMType == null){
            LOG.info("getting fc from seller level for event {}" ,e);
            existingCenters = fulfilmentModelService.getEnabledFCsBySeller(e.getSellerCode(), false);
        }
        
        
        String existingCenter = CollectionUtils.isEmpty(existingCenters) ? null : existingCenters.get(0);

        List<String> newCenters = e.getNewFCCenters();
        String newCenter = CollectionUtils.isEmpty(newCenters) ? null : newCenters.get(0);

        // find all SUPC for the seller and create queue push for all SUPC
        // except in exception list
        
        
        FCDetailVO existingFc = null;
        FCDetailVO newFc = null;
        if(existingCenter != null){
            existingFc = getFCCenterForFCCode(existingCenter);
        }
        
        if(newCenter != null){
            newFc = getFCCenterForFCCode(newCenter);
        }
        
        int oldSdInstant =  existingFc != null ? existingFc.getSdinstant(): 0;
        int newSdInstant = newFc != null ? newFc.getSdinstant(): 0;

        LOG.info("new FC is {}  and old FC is {} for seller "+ e.getSellerCode() + "and supc "+ e.getSupc(), newFc, existingFc);
        
        if (oldSdInstant != newSdInstant) {
            LOG.info("newFCCenter's {} sdInstant  is different than existingFcCenter's {} sdInstant, hence push required", newFc, existingFc);
            bResult = pushNotification( e.getSellerCode(), e.getSupc(), newSdInstant);
        }
       

        LOG.info("processing done for change of seller-SUPC-FM-FC exception mapping, returning {}", bResult);
        return bResult;
    }

    // case when seller-SUPC-FM-FC mapping is disabled in exception list from admin
    @Override
    public boolean processDisablingOfSellerSUPCFMMappingExceptionForExternalNotification(SellerSupcFMFCExceptionDeleteEventObj event) {

        if (!isSdInstantUpdatePushEnabled()) {
            // returning true as this is logically correct termination of the process flow
            return true;
        }
        LOG.info("processing started for disabling of seller-SUPC-FM-FC exception mapping, event:{}", event);

        boolean bResult = true;
        try {
            
            //get fc on seller supc which has been disabled now
            FCDetailVO disabledFc = null;
            if(!CollectionUtils.isEmpty(event.getDisabledFCCenters())){
                 disabledFc = getFCCenterForFCCode(event.getDisabledFCCenters().get(0));
            }
            
            
            //get fc at seller level
            FCDetailVO fcAtSeller = null;
            SellerFMMappingVO fmVo = fcMappingService.getFulfilmentCentreBySellerFromAerospike(event.getSellerCode());
            List<String> centers = fmVo.getFcCenters();          
            if (!CollectionUtils.isEmpty(centers) ) {
                 fcAtSeller = getFCCenterForFCCode(centers.get(0));
            }
            
            int oldSdInstant = disabledFc != null ? disabledFc.getSdinstant() :0;
            int newSdInstant = fcAtSeller != null ? fcAtSeller.getSdinstant():0;
            
            if(oldSdInstant != newSdInstant){
                bResult = pushNotification(event.getSellerCode(), event.getSupc(), newSdInstant);
            }
          

        } catch (ExternalDataNotFoundException e) {
            LOG.error("Could not fetch FM Model for seller:{} supc:{} ", event.getSellerCode(), event.getSupc());
            bResult = false;
        }

        LOG.info("processing done for disabling of seller-SUPC-FM-FC exception mapping, returning {}", bResult);
        return bResult;
    }

    
    //find all seller supc combinations which for fulfilmentCenter code is fcCode(passed in method).
    private void prepareSupcSellerListForFCToBePushed(String fcCode, int sdInstant, List<SdInstantUpdateMessage> sdInstantMessageForQueuePush) {
        // find all SUPC for the seller and create queue push for all SUPC
        // except in exception list
        List<String> sellerCodes = fmDBService.getEnabledSellerListAtSellerLevelForFCcode(fcCode);
        for (String sellerCode : sellerCodes) {
            // ask from aerospike
            try {
                List<String> listOfSUPCsFromDataSource = sellerToSUPCListLookupService.getSupcListForSeller(sellerCode);
                if (listOfSUPCsFromDataSource == null) {
                    listOfSUPCsFromDataSource = new ArrayList<String>();
                }
                LOG.info("supc-list-size {} for seller:{} ", listOfSUPCsFromDataSource.size(), sellerCode);

                // for each item in the SELLER-List<SUPC> mapping, update to
                // external push if no exception exists for SELLER-SUPC
                // combination

                List<String> listOfSUPCExceptionsFromDataSource = sellerToSUPCListLookupService.getSupcExceptionListForSeller(sellerCode);
                if (listOfSUPCExceptionsFromDataSource == null) {
                    listOfSUPCExceptionsFromDataSource = new ArrayList<String>();
                }

                LOG.info("supc-exception-list-size {} for seller:{} ", listOfSUPCExceptionsFromDataSource.size(), sellerCode);

                // create a set for faster lookup
                Set<String> supcExceptionSet = new HashSet<String>(listOfSUPCExceptionsFromDataSource);
                LOG.info("unique supc-exception-set-size {} for seller:{} ", supcExceptionSet.size(), sellerCode);

                for (String supc : listOfSUPCsFromDataSource) {
                    //if SUPC does not exist in Exception list, push to queue
                    if (!supcExceptionSet.contains(supc)) {
                        //collect all such events
                        addToNotificationList(sellerCode, supc, sdInstant, sdInstantMessageForQueuePush);
                    }
                }
            } catch (ExternalDataNotFoundException e) {
                LOG.error("Could not fetch seller-supc-mapping for seller:{} Exception:{} ", sellerCode, e);
            }
        }

        //Get list for seller supc excpetion which has fc code as this.
        List<SellerSupcFMMapping> listOfSellerSUPCsForFcCode = fmDBService.getEnabledSellerSupcMappingListForFcCode(fcCode);
        if (listOfSellerSUPCsForFcCode == null) {
            listOfSellerSUPCsForFcCode = new ArrayList<SellerSupcFMMapping>();
        }
        LOG.info("seller-supc-list-size {} for fcCode:{} ", listOfSellerSUPCsForFcCode.size(), fcCode);

        for (SellerSupcFMMapping sellersupcMapping : listOfSellerSUPCsForFcCode) {

            //collect all such events
            addToNotificationList(sellersupcMapping.getSellerCode(), sellersupcMapping.getSupc(), sdInstant, sdInstantMessageForQueuePush);
        }

    }

    private void prepareSupcSellerListToBePushed(String sellerCode, int sdInstant, List<SdInstantUpdateMessage> sdInstantMessageForQueuePush) {
        // find all SUPC for the seller and create queue push for all SUPC
        // except in exception list
        try {
            // ask from aerospike
            List<String> listOfSUPCsFromDataSource = sellerToSUPCListLookupService.getSupcListForSeller(sellerCode);
            if (listOfSUPCsFromDataSource == null) {
                listOfSUPCsFromDataSource = new ArrayList<String>();
            }
            LOG.info("supc-list-size {} for seller:{} ", listOfSUPCsFromDataSource.size(), sellerCode);

            // for each item in the SELLER-List<SUPC> mapping, update to
            // external push if no exception exists for SELLER-SUPC
            // combination

            List<String> listOfSUPCExceptionsFromDataSource = sellerToSUPCListLookupService.getSupcExceptionListForSeller(sellerCode);
            if (listOfSUPCExceptionsFromDataSource == null) {
                listOfSUPCExceptionsFromDataSource = new ArrayList<String>();
            }

            LOG.info("supc-exception-list-size {} for seller:{} ", listOfSUPCExceptionsFromDataSource.size(), sellerCode);

            // create a set for faster lookup
            Set<String> supcExceptionSet = new HashSet<String>(listOfSUPCExceptionsFromDataSource);
            LOG.info("unique supc-exception-set-size {} for seller:{} ", supcExceptionSet.size(), sellerCode);

            //compare the items in list with exceptions
            for (String supc : listOfSUPCsFromDataSource) {
                //if SUPC does not exist in Exception list, push to queue
                if (!supcExceptionSet.contains(supc)) {
                    //collect all such events
                    addToNotificationList(sellerCode, supc, sdInstant, sdInstantMessageForQueuePush);
                }
            }

        } catch (ExternalDataNotFoundException e) {
            LOG.error("Could not fetch seller-supc-mapping for seller:{} Exception:{} ", sellerCode, e);
        }
    }

  
    private FCDetailVO getFCCenterForFCCode(String fcCode) {
        return fcAerospikeService.getFCDetailVO(fcCode);
                   
    }

    
    private boolean pushNotification(String sellerCode, String supc, int sdInstant) {
        List<SdInstantUpdateMessage> sdInstantUpdateMessageQueuePush = new ArrayList<SdInstantUpdateMessage>();
        addToNotificationList(sellerCode, supc, sdInstant, sdInstantUpdateMessageQueuePush);
        return pushToExternalNotificationService(sdInstantUpdateMessageQueuePush);
    }

    private void addToNotificationList(String sellerCode, String supc, int sdInstant, List<SdInstantUpdateMessage> sdInstantUpdateMessageQueuePush) {
        // if any update requires push-to-queue, collect it for sending to push. add this record to later push-to-queue.
        SdInstantUpdateMessage obj = createSellerSupcSdInstantUpdateSRO(sellerCode, supc, sdInstant);
        LOG.info("adding SRO:{} to external notification list", obj);
        sdInstantUpdateMessageQueuePush.add(obj);
    }

    private SdInstantUpdateMessage createSellerSupcSdInstantUpdateSRO(String sellerCode, String supc, int sdInstant) {
        SdInstantUpdateMessage obj = new SdInstantUpdateMessage();
        obj.setSellerCode(sellerCode);
        obj.setSupc(supc);
        obj.setSdInstant(sdInstant ==1 ? true: false);
        return obj;
    }

    /**
     * push the data to external notification service currently backed by activemq, data pushed is in form:
     * seller-supc-fm-fcList. The current intended recipient is Search module, which may change in future.
     * 
     * @param sellerSUPCFMMappingForQueuePush
     */
    private boolean pushToExternalNotificationService(List<SdInstantUpdateMessage> sdInstantUpdateMessageListForQueuePush) {
        // all seller/supc processed, push to score if anything
        if (!sdInstantUpdateMessageListForQueuePush.isEmpty()) {
            // forward the data for push-to-queue
            return sdInstantUpdateProducer.publishToQueue(sdInstantUpdateMessageListForQueuePush);
        }
        // return true in case of no events
        return true;
    }

    
    private boolean isFMWareHouseModel(FulfillmentModel fMType){
        return fMType.isLocationRequired();
    }
    
    private boolean isFMMovedtoNonWareHouseModel(FulfillmentModel oldFMType ,FulfillmentModel newFMType){
        return oldFMType != newFMType && (isFMWareHouseModel(oldFMType)) && (!isFMWareHouseModel(newFMType));
    }

    private boolean isSdInstantUpdatePushEnabled() {
        if (!dep.getBooleanPropertyValue(Property.SDINSTANT_UPDATE_PUSH_ENABLED)) {
            LOG.warn("push of sdinstant is disabled. Returning without any further processing ... ");
            return false;
        }
        return true;
    }

    public interface IDependency {
        boolean getBooleanPropertyValue(Property property);
    }

    static class Dependency implements IDependency {
        @Override
        public boolean getBooleanPropertyValue(Property property) {
            return ConfigUtils.getBooleanScalar(property);
        }
    }
}
