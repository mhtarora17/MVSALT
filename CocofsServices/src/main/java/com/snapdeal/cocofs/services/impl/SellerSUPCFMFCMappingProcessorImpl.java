package com.snapdeal.cocofs.services.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.google.common.base.Objects;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.core.aerospike.vo.SellerFMMappingVO;
import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.generic.persister.exception.GenericPersisterException;
import com.snapdeal.cocofs.services.events.dto.SellerFMFCUpdateEventObj;
import com.snapdeal.cocofs.services.events.dto.SellerSupcAddEventObj;
import com.snapdeal.cocofs.services.events.dto.SellerSupcFMFCExceptionAddEventObj;
import com.snapdeal.cocofs.services.events.dto.SellerSupcFMFCExceptionDeleteEventObj;
import com.snapdeal.cocofs.services.fm.IFulfilmentModelService;
import com.snapdeal.cocofs.services.fm.IManualPushOfSellerFMUpdateForExternalNotification;
import com.snapdeal.cocofs.services.fm.ISellerSUPCFMFCMappingProcessor;
import com.snapdeal.cocofs.services.fm.ISellerToSUPCListDBLookupService;
import com.snapdeal.cocofs.services.producer.IExternalNotificationsServiceForSellerSupcFMFCUpdate;
import com.snapdeal.cocofs.sro.FulfillmentModel;
import com.snapdeal.cocofs.sro.SellerSupcFMFCUpdateSRO;
import com.snapdeal.score.data.aerospike.exception.GetFailedException;
import com.snapdeal.score.data.aerospike.exception.PutFailedException;

/**
 * Class to process Seller SUPC FM mapping data update. Collect the notifications and push to external service to
 * publish these notification to activemq <br>
 * 1) SALESFORCE send SELLER-FM/FC mapping add/updates via API hit <br>
 * 2) IPSM send SELLER-SUPC updates (new SUPC addition for SELLER) <br>
 * 3) CoCoFS Admin send SELLER-SUPC-FM/FC mappings to update/disable exceptions.<br>
 * 
 * @author ssuthar
 */

@Service("sellerSUPCFMFCMappingProcessor")
public class SellerSUPCFMFCMappingProcessorImpl implements ISellerSUPCFMFCMappingProcessor, IManualPushOfSellerFMUpdateForExternalNotification {

    private static final Logger                                  LOG = LoggerFactory.getLogger(SellerSUPCFMFCMappingProcessorImpl.class);

    @Autowired
    private ISellerToSUPCListDBLookupService                     sellerToSUPCListLookupService;
    @Autowired
    private IFulfilmentModelService                              fulfilmentModelService;

    @Autowired
    @Qualifier("externalNotificationsServiceForSellerSupcFMFCUpdate")
    private IExternalNotificationsServiceForSellerSupcFMFCUpdate sellerSupcFMFCListUpdateProducer;

    private IDependency                                          dep = new Dependency();

    // case when SELLER-FM mapping is added/changed from salesforce
    @Override
    public boolean processChangeOfSellerFMMappingForExternalNotification(SellerFMFCUpdateEventObj e) {

        if (!isSellerSupcFMFCUpdatePushEnabled()) {
            // returning true as this is logically correct termination of the process flow
            return true;
        }

        LOG.info("processing started for change of seller fm mapping, event:{}", e);
        boolean bResult = true;
        // find the corresponding FM type
        FulfillmentModel newFMType = FulfillmentModel.getFulfilmentModelByCode(e.getNewFM());
        FulfillmentModel oldFMType = FulfillmentModel.getFulfilmentModelByCode(e.getOldFM());

        List<SellerSupcFMFCUpdateSRO> sellerSUPCFMMappingForQueuePush = new ArrayList<SellerSupcFMFCUpdateSRO>();
        // case 1) SELLER-FM is added first time, as this is supposed to be handled at SUPC push from IPMS
        if (oldFMType == null) {
            LOG.info("new SELLER-FM mapping is added for seller:{} FM:{}, no information is to be published for external service", e.getSellerCode(), newFMType);
        } else if( !oldFMType.equals(newFMType) || hasFCChanged(e.getNewFCCenters(), e.getExistingFCCenters()) ){
            // find all SUPC for the seller and create queue push for all SUPC
            // except in exception list
            prepareSupcSellerListToBePushed(e.getSellerCode(), newFMType, e.getNewFCCenters(), sellerSUPCFMMappingForQueuePush);
        }

        if(sellerSUPCFMMappingForQueuePush.size() >0){
            bResult = pushToExternalNotificationService(sellerSUPCFMMappingForQueuePush);
        }else{
            LOG.info("No eligible data for FMFC push for event {}",e);
        }
        LOG.info("processing done for change of seller-fm-fc-mapping, returning {}", bResult);
        return bResult;
    }
    
    private boolean hasFCChanged(List<String> newCenters, List<String> existingCenters){
        if(newCenters == null){
            newCenters = new ArrayList<String>();
        }
        if(existingCenters == null){
            existingCenters = new ArrayList<String>();
        }
        return !newCenters.equals(existingCenters);
    }

    // case when we forcefully push the sellerFM to serach from RetriggerSerachEvent Cocofs admin Panel
    @Override
    public boolean processManualPushOfExistingSellerFMMappingForExternalNotification(String sellerCode, String existingFulfilmentModel) {
        if (!isSellerSupcFMFCUpdatePushEnabled()) {
            // returning true as this is logically correct termination of the process flow
            return true;
        }
        LOG.info("processing started for pushing existing seller fm mapping from retriggerQueuePushEvent Panel");

        // find the corresponding FM type
        FulfillmentModel currentFMType = FulfillmentModel.getFulfilmentModelByCode(existingFulfilmentModel);

        List<SellerSupcFMFCUpdateSRO> sellerSUPCFMMappingForQueuePush = new ArrayList<SellerSupcFMFCUpdateSRO>();
        // find all SUPC for the seller and create search push for all SUPC
        // except in exception list
        List<String> fcList = null;
        try {
            fcList = getListOfFCsForSeller(sellerCode);
        } catch (ExternalDataNotFoundException e) {
            LOG.error("error while fetching fc for seller "+ sellerCode,e);
            e.printStackTrace();
        }

        prepareSupcSellerListToBePushed(sellerCode, currentFMType, fcList, sellerSUPCFMMappingForQueuePush);

        boolean bResult = pushToExternalNotificationService(sellerSUPCFMMappingForQueuePush);
        LOG.info("processing done for pushing existing seller fm mapping from retriggerQueueEvent Panel, returning {}", bResult);
        return bResult;
    }

    @Override
    public boolean processAdditionOfNewSellerSUPCMappingForExternalNotification(SellerSupcAddEventObj event) throws GetFailedException, ExternalDataNotFoundException,
            PutFailedException, GenericPersisterException {

        String sellerCode = event.getSellerCode();
        String supc = event.getSupc();

        if (!isSellerSupcFMFCUpdatePushEnabled()) {
            // returning true as this is logically correct termination of the process flow
            return true;
        }
        boolean bResult = true;
        LOG.info("processing started for addition of new seller SUPC mapping, seller:{}, supc:{}", sellerCode, supc);
        try {

            FulfillmentModel fm = fulfilmentModelService.getFulfilmentModel(sellerCode, supc);
            List<String> fcCenters = getListOfFCsForSeller(sellerCode);
            bResult = pushNotification(sellerCode, supc, fm, fcCenters);

        } catch (ExternalDataNotFoundException e) {
            LOG.error("Could not fetch FM Model for seller:{} supc:{} ", sellerCode, supc);
            throw e;
        }
        LOG.info("processing done for addition of seller-SUPC pair, returning {}", bResult);
        return bResult;
    }

    // case when seller-SUPC-FM-FC mapping is changed in exception list from CoCoFS admin
    @Override
    public boolean processChangeOfSellerSUPCFMMappingExceptionForExternalNotification(SellerSupcFMFCExceptionAddEventObj event) {

        if (!isSellerSupcFMFCUpdatePushEnabled()) {
            // returning true as this is logically correct termination of the process flow
            return true;
        }
        
        boolean bResult = true;

        try{
            String oldFM = event.getOldFM();
            List<String> existingCenters = event.getExistingFCCenters();

            if(StringUtils.isEmpty(oldFM)){
                 LOG.info("fetching fm  fc from seller level for event {}",event);
                 oldFM = fulfilmentModelService.getFulfilmentModelBySeller(event.getSellerCode(), false);
                 existingCenters = getListOfFCsForSeller(event.getSellerCode());
            }
            FulfillmentModel oldFMType = FulfillmentModel.getFulfilmentModelByCode(oldFM);

           
            LOG.info("processing started for change of seller-SUPC-FM-FC exception mapping, event:{}", event);

            // find the corresponding FM type
            FulfillmentModel newFMType = FulfillmentModel.getFulfilmentModelByCode(event.getNewFM());

            // add this record to later push-to-queue.
            if(!newFMType.equals(oldFMType) || hasFCChanged(event.getNewFCCenters(), existingCenters)){
                bResult = pushNotification(event.getSellerCode(), event.getSupc(), newFMType, event.getNewFCCenters());
            }
            
        } catch (ExternalDataNotFoundException e) {
            bResult = false;
            LOG.error("Could not fetch FM Model from seller-fm-fc-mapping for event:{} Exception:{} ", event, e);
        }
        LOG.info("processing done for change of seller-SUPC-FM-FC exception mapping, returning {}", bResult);
        return bResult;
    }

    // case when we forcefully push the sellerSupcFM to queue from RetriggerQueuePushEvent Cocofs admin Panel
    @Override
    public boolean processExistingSellerSUPCFMMappingExceptionForExternalNotification(String sellerCode, String supc, String existingFM) {

        if (!isSellerSupcFMFCUpdatePushEnabled()) {
            // returning true as this is logically correct termination of the process flow
            return true;
        }

        LOG.info("processing started for pushing existing seller-SUPC-FM-FC exception mapping, seller:{}, supc:{}", sellerCode, supc);

        // find the corresponding FM type
        FulfillmentModel currentFMType = FulfillmentModel.getFulfilmentModelByCode(existingFM);
        // sending to push.
        List<String> fcCenters = null;
        try {
            fcCenters = getListOfFCsForSeller(sellerCode);
        } catch (ExternalDataNotFoundException e) {
            e.printStackTrace();
        }

        // sending to push.
        boolean bResult = pushNotification(sellerCode, supc, currentFMType, fcCenters);
        LOG.info("processing done for pushing existing seller-SUPC-FM-FC exception mapping, returning {}", bResult);
        return bResult;
    }

    // case when seller-SUPC-FM-FC mapping is disabled in exception list from admin
    @Override
    public boolean processDisablingOfSellerSUPCFMMappingExceptionForExternalNotification(SellerSupcFMFCExceptionDeleteEventObj event) {

        if (!isSellerSupcFMFCUpdatePushEnabled()) {
            // returning true as this is logically correct termination of the process flow
            return true;
        }
        LOG.info("processing started for disabling of seller-SUPC-FM-FC exception mapping, event:{}", event);

        boolean bResult = true;
        try {
            FulfillmentModel existingFM = FulfillmentModel.getFulfilmentModelByCode(event.getFM());
            
            String defaultFMStr = fulfilmentModelService.getFulfilmentModelBySeller(event.getSellerCode(), false);
            FulfillmentModel defaultFMType = FulfillmentModel.getFulfilmentModelByCode(defaultFMStr);
            
            List<String> newFCListAtSellerLevel = getListOfFCsForSeller(event.getSellerCode());
            
            if(!existingFM.equals(defaultFMType) || hasFCChanged(newFCListAtSellerLevel, event.getDisabledFCCenters())){
                bResult = pushNotification(event.getSellerCode(), event.getSupc(), defaultFMType, newFCListAtSellerLevel);
            }

        } catch (ExternalDataNotFoundException e) {
            bResult = false;
            LOG.error("Could not fetch FM Model from seller-fm-fc-mapping for event:{} Exception:{} ", event, e);
        }

        LOG.info("processing done for disabling of seller-SUPC-FM-FC exception mapping, returning {}", bResult);
        return bResult;
    }

    private boolean isFCCentersChanged(List<String> newFCList, List<String> existingFCCenters) {

        // if {existing has data. new one does not} 
        //else if {vice-versa} 
        //else {both has data}
        if (existingFCCenters == null || existingFCCenters.isEmpty()) {
            if (newFCList != null && !newFCList.isEmpty()) {
                return true;
            } else if (newFCList == null || newFCList.isEmpty()) {
                return false;
            }

        } else if (newFCList == null || newFCList.isEmpty()) {
            if (existingFCCenters != null && !existingFCCenters.isEmpty()) {
                return true;
            } else if (existingFCCenters == null || existingFCCenters.isEmpty()) {
                return false;
            }
        } else {
            Collections.sort(newFCList);
            Collections.sort(existingFCCenters);
        }
        return !Objects.equal(newFCList, existingFCCenters);
    }

    private void prepareSupcSellerListToBePushed(String sellerCode, FulfillmentModel newFMType, List<String> fcCenters,
            List<SellerSupcFMFCUpdateSRO> sellerSUPCFMMappingForQueuePush) {
        // find all SUPC for the seller and create queue push for all SUPC
        // except in exception list
        try {
            // ask from aerospike
            List<String> listOfSUPCsFromDataSource = sellerToSUPCListLookupService.getSupcListForSeller(sellerCode);
            if (listOfSUPCsFromDataSource == null) {
                listOfSUPCsFromDataSource = new ArrayList<String>();
            }
            LOG.info("supc-list-size {} for seller:{} ", listOfSUPCsFromDataSource.size(), sellerCode);

            // for each item in the SELLER-List<SUPC> mapping, update to
            // external push if no exception exists for SELLER-SUPC
            // combination

            List<String> listOfSUPCExceptionsFromDataSource = sellerToSUPCListLookupService.getSupcExceptionListForSeller(sellerCode);
            if (listOfSUPCExceptionsFromDataSource == null) {
                listOfSUPCExceptionsFromDataSource = new ArrayList<String>();
            }

            LOG.info("supc-exception-list-size {} for seller:{} ", listOfSUPCExceptionsFromDataSource.size(), sellerCode);

            // create a set for faster lookup
            Set<String> supcExceptionSet = new HashSet<String>(listOfSUPCExceptionsFromDataSource);
            LOG.info("unique supc-exception-set-size {} for seller:{} ", supcExceptionSet.size(), sellerCode);

            //compare the items in list with exceptions
            for (String supc : listOfSUPCsFromDataSource) {
                //if SUPC does not exist in Exception list, push to queue
                if (!supcExceptionSet.contains(supc)) {
                    //collect all such events
                    addToNotificationList(sellerCode, supc, newFMType, fcCenters, sellerSUPCFMMappingForQueuePush);
                }
            }

        } catch (ExternalDataNotFoundException e) {
            LOG.error("Could not fetch seller-supc-mapping for seller:{} Exception:{} ", sellerCode, e);
        }
    }

    private List<String> getListOfFCsForSeller(String sellerCode) throws ExternalDataNotFoundException {
        SellerFMMappingVO vo = fulfilmentModelService.getFulfilmentModelBySellerFromAerospike(sellerCode);
        List<String> sellerFCPairs = null;
        if (vo != null && vo.isEnabled()) {
            sellerFCPairs = vo.getFcCenters();
        }
        return sellerFCPairs;
    }

    private boolean pushNotification(String sellerCode, String supc, FulfillmentModel fm, List<String> fcCenters) {
        List<SellerSupcFMFCUpdateSRO> sellerSUPCFMMappingForQueuePush = new ArrayList<SellerSupcFMFCUpdateSRO>();
        addToNotificationList(sellerCode, supc, fm, fcCenters, sellerSUPCFMMappingForQueuePush);
        return pushToExternalNotificationService(sellerSUPCFMMappingForQueuePush);
    }

    private void addToNotificationList(String sellerCode, String supc, FulfillmentModel fm, List<String> fcCenters, List<SellerSupcFMFCUpdateSRO> sellerSUPCFMMappingForQueuePush) {
        // if any update requires push-to-queue, collect it for sending to push. add this record to later push-to-queue.
        SellerSupcFMFCUpdateSRO obj = createSellerSupcFMFCUpdateSRO(sellerCode, supc, fm, fcCenters);
        LOG.info("adding SRO:{} to external notification list", obj);
        sellerSUPCFMMappingForQueuePush.add(obj);
    }

    private SellerSupcFMFCUpdateSRO createSellerSupcFMFCUpdateSRO(String sellerCode, String supc, FulfillmentModel fm, List<String> fcCenters) {
        SellerSupcFMFCUpdateSRO obj = new SellerSupcFMFCUpdateSRO();
        obj.setSellerCode(sellerCode);
        obj.setSupc(supc);
        obj.setFulfillmentModel(fm.getCode());
        obj.setFcCenters(fcCenters);
        obj.setSdFulfilled(isFMFC_VOI(fm));
        return obj;
    }

    /**
     * push the data to external notification service currently backed by activemq, data pushed is in form:
     * seller-supc-fm-fcList. The current intended recipient is Search module, which may change in future.
     * 
     * @param sellerSUPCFMMappingForQueuePush
     */
    private boolean pushToExternalNotificationService(List<SellerSupcFMFCUpdateSRO> sellerSUPCFMMappingForQueuePush) {
        // all seller/supc processed, push to score if anything
        if (!sellerSUPCFMMappingForQueuePush.isEmpty()) {
            // forward the data for push-to-queue
            return sellerSupcFMFCListUpdateProducer.publishToQueue(sellerSUPCFMMappingForQueuePush);
        }
        // return true in case of no events
        return true;
    }

    private boolean isFCMappingPushNeeded(FulfillmentModel newFMType, FulfillmentModel oldFMType) {
        return isFC_VOIFMToggled(newFMType, oldFMType) || isOneshipFMToggled(newFMType, oldFMType) || isOCPLUSFMToggled(newFMType, oldFMType) ;
    }

    private boolean isFC_VOIFMToggled(FulfillmentModel newFMType, FulfillmentModel oldFMType) {
        LOG.debug("oldFMType:{},newFMType:{}", oldFMType, newFMType);
        return oldFMType != newFMType && (isFMFC_VOI(newFMType) || isFMFC_VOI(oldFMType));
    }

    private boolean isOneshipFMToggled(FulfillmentModel newFMType, FulfillmentModel oldFMType) {
        LOG.debug("oldFMType:{},newFMType:{}", oldFMType, newFMType);
        return oldFMType != newFMType && (isFMOneShip(newFMType) || isFMOneShip(oldFMType));
    }
    
    private boolean isOCPLUSFMToggled(FulfillmentModel newFMType, FulfillmentModel oldFMType) {
        LOG.debug("oldFMType:{},newFMType:{}", oldFMType, newFMType);
        return oldFMType != newFMType && (isFMOCPLUS(newFMType) || isFMOCPLUS(oldFMType));
    }

    private boolean isFMFC_VOI(FulfillmentModel newFMType) {
        return newFMType == FulfillmentModel.FC_VOI;
    }

    private boolean isFMOneShip(FulfillmentModel newFMType) {
        return newFMType == FulfillmentModel.ONESHIP;
    }

    private boolean isFMOCPLUS(FulfillmentModel newFMType) {
        return newFMType == FulfillmentModel.OCPLUS;
    }
    private boolean isSellerSupcFMFCUpdatePushEnabled() {
        if (!dep.getBooleanPropertyValue(Property.SELLER_SUPC_FM_FC_UPDATE_PUSH_ENABLED)) {
            LOG.warn("push of seller-supc-fm-fcList is disabled. Returning without any further processing ... ");
            return false;
        }
        return true;
    }

    public interface IDependency {
        boolean getBooleanPropertyValue(Property property);
    }

    static class Dependency implements IDependency {
        @Override
        public boolean getBooleanPropertyValue(Property property) {
            return ConfigUtils.getBooleanScalar(property);
        }
    }
}
