/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 04-Sep-2013
 *  @author himanshum
 */
package com.snapdeal.cocofs.services.impl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.framework.AopContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.snapdeal.base.cache.CacheManager;
import com.snapdeal.base.notification.INotificationService;
import com.snapdeal.base.notification.email.EmailMessage;
import com.snapdeal.base.vo.EmailTemplateVO;
import com.snapdeal.cocofs.cache.EmailTemplateCache;
import com.snapdeal.cocofs.cache.JobCache;
import com.snapdeal.cocofs.common.BulkUploadValidationResponse;
import com.snapdeal.cocofs.common.JobProcessorResponse;
import com.snapdeal.cocofs.common.JobThreadStatus;
import com.snapdeal.cocofs.common.JobValidationException;
import com.snapdeal.cocofs.db.dao.IJobDao;
import com.snapdeal.cocofs.entity.JobAction;
import com.snapdeal.cocofs.entity.JobDetail;
import com.snapdeal.cocofs.entity.JobStatus;
import com.snapdeal.cocofs.services.IJobProcessor;
import com.snapdeal.cocofs.services.IJobSchedulerService;
import com.snapdeal.cocofs.services.IJobService;
import com.snapdeal.cocofs.utils.ExcelReader;
import com.snapdeal.cocofs.utils.ExcelReader.SuccessMessage;

@Service("JobSchedulerServiceImpl")
public class JobSchedulerServiceImpl implements IJobSchedulerService {

    private boolean                    daemonRunning            = false;

    private static final Logger        LOG                      = LoggerFactory.getLogger(JobSchedulerServiceImpl.class);

    private Map<String, IJobProcessor> jobProcessorMap          = new HashMap<String, IJobProcessor>();

    @Autowired
    private IJobService                jobService;

    @Autowired
    private IJobDao                    jobDao;

    @Autowired
    private INotificationService       notificationService;

    /*
     * jobtype,jobthreadStatus
     */
    private Map<String, String>        threadStatusByJobTypeMap = new HashMap<String, String>();

    @Override
    public void searchAndStartJobs(String requestedBy) {
        if (daemonRunning) {
            LOG.info("Searching for any Job to start (requested by:{})", requestedBy);
            List<JobDetail> jobDetailList = ((IJobSchedulerService) AopContext.currentProxy()).getAllJobDetailsToRun();
            LOG.info("No of jobs to run:" + jobDetailList.size());
            Map<String, PriorityQueue<JobDetail>> jdListByJobTypeMap = new HashMap<String, PriorityQueue<JobDetail>>();
            for (JobDetail jd : jobDetailList) {
                if (!jdListByJobTypeMap.containsKey(jd.getAction().getJobType())) {
                    jdListByJobTypeMap.put(jd.getAction().getJobType(), new PriorityQueue<JobDetail>());
                }
                jdListByJobTypeMap.get(jd.getAction().getJobType()).add(jd);
            }
            LOG.info("No of threads:" + jdListByJobTypeMap.keySet().size());
            for (String jobType : jdListByJobTypeMap.keySet()) {
                boolean toRun = false;
                synchronized (this) {
                    if (!threadStatusByJobTypeMap.containsKey(jobType) || threadStatusByJobTypeMap.get(jobType).equals(JobThreadStatus.Type.NOT_RUNNING.getCode())) {
                        threadStatusByJobTypeMap.put(jobType, JobThreadStatus.Type.RUNNING.getCode());
                        toRun = true;
                    }
                }
                if (toRun) {
                    ((IJobSchedulerService) AopContext.currentProxy()).startJob(jdListByJobTypeMap.get(jobType).poll());
                    LOG.info("Job:" + jobType + " has been called asynchroously");
                }
            }
        } else {
            LOG.info("Daemon is not running. Please enable it to start jobs. (requested by:{})", requestedBy);
        }
    }

    @Override
    @Transactional
    public List<JobDetail> getAllJobDetailsToRun() {
        List<JobDetail> jobDetailList = new ArrayList<JobDetail>();
        JobStatus validatedStatus = CacheManager.getInstance().getCache(JobCache.class).getJobStatusByCode(JobStatus.Type.VALIDATED.getCode());

        for (JobDetail jd : jobDao.getAllJobDetails(validatedStatus)) {
            jobDetailList.add(jd);
        }

        JobStatus failedStatus = CacheManager.getInstance().getCache(JobCache.class).getJobStatusByCode(JobStatus.Type.FAILED.getCode());
        for (JobDetail jd : jobDao.getAllJobDetails(failedStatus)) {
            if (jd.isToRetry()) {
                jobDetailList.add(jd);
            }
        }
        return jobDetailList;
    }

    @Override
    @Async
    public void startJob(JobDetail jobDetail) {
        if (jobDetail != null) {
            LOG.info("To Start Job code:{} with job status:{} and action: " + jobDetail.getAction().getCode(), jobDetail.getFileCode(), jobDetail.getStatus().getCode());
            try {
                processJob(jobDetail);
                endJob(jobDetail);
            } catch (RuntimeException e) {
                LOG.info("Job " + jobDetail.getFileCode() + " ended with exception ", e);
                endJob(jobDetail);
                throw e;
            }

        }
    }

    @Override
    public boolean addJobProcessor(String actionCode, IJobProcessor jobProcessor) {
        LOG.info("Registering Job Processor:" + jobProcessor.getClass().getSimpleName() + "with action:" + actionCode);
        if (jobProcessorMap.get(actionCode) != null) {
            return false;
        }
        jobProcessorMap.put(actionCode, jobProcessor);
        return true;
    }

    @Override
    public boolean removeJobProcessor(String actionCode) {
        LOG.info("Removing job Processor with action code:{}", actionCode);
        jobProcessorMap.remove(actionCode);
        return true;
    }

    @Override
    public void processJob(JobDetail jobDetail) {
        try {
            jobDetail = jobService.getJobDetailByCode(jobDetail.getFileCode());
            JobAction action = jobDetail.getAction();
            if (action == null || jobProcessorMap.get(action.getCode()) == null) {
                LOG.error("didnt find the action specified for the job detail:{} for action-code:{}", jobDetail.getFileCode(), (action == null ? "null" : action.getCode()));
                throw new RuntimeException();
            }
            if (jobDetail.getStatus().getCode().equalsIgnoreCase(JobStatus.Type.FAILED.getCode())
                    || jobDetail.getStatus().getCode().equalsIgnoreCase(JobStatus.Type.VALIDATED.getCode())) {
                LOG.info("processing the processendJobSuccessfullNotifications code:{}", jobDetail.getFileCode());
                jobDetail = jobService.updateJobStatus(jobDetail, JobStatus.Type.PROCESSING.getCode());
                JobProcessorResponse response = jobProcessorMap.get(action.getCode()).process(jobDetail);

                if (response.getProcessingSuccessful()) {
                    jobDetail = jobService.getJobDetailByCode(jobDetail.getFileCode());
                    jobDetail = jobService.updateJobStatus(jobDetail, JobStatus.Type.EXECUTED.getCode());
                    sendJobSuccessfullNotification(jobDetail, response);
                } else {
                    jobDetail = jobService.getJobDetailByCode(jobDetail.getFileCode());
                    jobDetail = jobService.updateJobStatus(jobDetail, JobStatus.Type.FAILED.getCode());
                    sendJobFailureNotification(jobDetail, null, response);
                }

            } else {
                LOG.info("process:{} is not in valid state to get processed", jobDetail.getFileCode());
                throw new RuntimeException();
            }
        } catch (Exception e) {
            LOG.error("failed with exception while processing the job ", e);
            jobDetail = jobService.getJobDetailByCode(jobDetail.getFileCode());
            jobDetail = jobService.updateJobStatus(jobDetail, JobStatus.Type.FAILED.getCode());
            sendJobFailureNotification(jobDetail, e, null);
        }

    }

    private void endJob(JobDetail jobDetail) {
        LOG.info("Job:{} ended", jobDetail.getFileCode());
        synchronized (this) {
            threadStatusByJobTypeMap.put(jobDetail.getAction().getJobType(), JobThreadStatus.Type.NOT_RUNNING.getCode());
        }
        //commented as auto proxy creating some problem so was not executing properly
        //searchAndStartJobs("jobType thread:" + jobDetail.getAction().getJobType());
    }

    @Override
    public JobDetail saveJob(MultipartFile multipartFile, String actionCode, String username) throws JobValidationException, IOException {
        String fileValidationMessage = ExcelReader.validateInputFile(multipartFile);
        if (SuccessMessage.SUCCESS.name().equals(fileValidationMessage)) {
            LOG.info("saving job with action code:" + actionCode + " by user:" + username);
            File file = jobService.saveToDisk(multipartFile);
            validate(file, actionCode, null);
            JobDetail jobDetail = jobService.saveJob(file, actionCode, username);
            if (jobDetail == null) {
                throw new RuntimeException("Job Detail could not be saved");
            }
            LOG.info("Job saved with jobcode:" + jobDetail.getFileCode() + " Sending notification email for this");
            sendJobSavedNotification(jobDetail);
            return jobDetail;
        } else {
            LOG.warn("NOT saving job message {} with action code:" + actionCode + " by user:" + username, fileValidationMessage);
            throw new JobValidationException(fileValidationMessage);
        }

    }

    /*
     * Additional method version in which an extra parameter can be passed and processed
     * (non-Javadoc)
     * @see com.snapdeal.cocofs.services.IJobSchedulerService#saveJob(org.springframework.web.multipart.MultipartFile, java.lang.String, java.lang.String, java.util.Map)
     */
    @Override
    public JobDetail saveJob(MultipartFile multipartFile, String actionCode, String username, String param) throws JobValidationException, IOException {

        String fileValidationMessage = ExcelReader.validateInputFile(multipartFile);
        if (SuccessMessage.SUCCESS.name().equals(fileValidationMessage)) {
            LOG.info("saving job with action code:" + actionCode + " by user:" + username);
            File file = jobService.saveToDisk(multipartFile);
            validate(file, actionCode, param);
            JobDetail jobDetail = jobService.saveJob(file, actionCode, username, param);
            if (jobDetail == null) {
                throw new RuntimeException("Job Detail could not be saved");
            }
            LOG.info("Job saved with jobcode:" + jobDetail.getFileCode() + " Sending notification email for this");
            sendJobSavedNotification(jobDetail);
            return jobDetail;
        } else {
            LOG.warn("NOT saving job message {} with action code:" + actionCode + " by user:" + username, fileValidationMessage);
            throw new JobValidationException(fileValidationMessage);
        }
    }

  
    
    private void validate(File file, String actionCode,String param) throws JobValidationException {
        IJobProcessor processor = jobProcessorMap.get(actionCode);
        if (processor == null) {
            LOG.error("Processor not present for this action code");
            throw new RuntimeException();
        }
        BulkUploadValidationResponse response = processor.validateJob(file, actionCode, param);
        if (response == null || !response.getValid()) {
            throw new JobValidationException(response);
        }
    }


    @Override
    public boolean startDaemonProcess() {
        LOG.info("Job processing daemon is started.");
        daemonRunning = true;
        searchAndStartJobs("deamon start");
        return true;
    }

    @Override
    public boolean stopDaemonProcess() {
        LOG.info("Job processing daemon is stopped.");
        daemonRunning = false;
        return true;
    }

    private void sendJobSuccessfullNotification(JobDetail jobDetail, JobProcessorResponse resp) {
        LOG.debug("Going to send job success mail for " + jobDetail.getFileCode());
        EmailTemplateVO template = CacheManager.getInstance().getCache(EmailTemplateCache.class).getTemplateByName("JobExecutionSuccessfullEmail");

        List<String> to = getToList(jobDetail, template);

        EmailMessage message = new EmailMessage(to, template.getName());
        message.addTemplateParam("jobDetail", jobDetail);
        message.addTemplateParam("errorMap", resp.getError());
        notificationService.sendEmail(message, template);
        LOG.debug("Sent job success mail for " + jobDetail.getFileCode());
    }

    private void sendJobFailureNotification(JobDetail jobDetail, Exception e, JobProcessorResponse resp) {
        LOG.debug("Going to send job failure mail for " + jobDetail.getFileCode());
        EmailTemplateVO template = CacheManager.getInstance().getCache(EmailTemplateCache.class).getTemplateByName("JobExecutionFailureEmail");

        List<String> to = getToList(jobDetail, template);

        EmailMessage message = new EmailMessage(to, template.getName());
        message.addTemplateParam("jobDetail", jobDetail);
        message.addTemplateParam("exception", e);
        Map<String, List<String>> error = new HashMap<String, List<String>>();

        if (resp != null) {
            error = resp.getError();
        }
        message.addTemplateParam("errorMap", error);

        notificationService.sendEmail(message, template);
        LOG.debug("Sent job failure mail for " + jobDetail.getFileCode());
    }

    private void sendJobSavedNotification(JobDetail jobDetail) {
        LOG.debug("Going to send job saved mail for " + jobDetail.getFileCode());
        EmailTemplateVO template = CacheManager.getInstance().getCache(EmailTemplateCache.class).getTemplateByName("JobSavedEmail");

        List<String> to = getToList(jobDetail, template);

        EmailMessage message = new EmailMessage(to, template.getName());
        message.addTemplateParam("jobDetail", jobDetail);

        notificationService.sendEmail(message, template);
        LOG.debug("Sent job saved mail for " + jobDetail.getFileCode());

    }

    private List<String> getToList(JobDetail jobDetail, EmailTemplateVO template) {
        Set<String> toUsers = new HashSet<String>();
        toUsers.add(jobDetail.getUploadedBy());
        if (null != template.getTo()) {
            toUsers.addAll(template.getTo());
        }

        List<String> to = new ArrayList<String>(toUsers);
        return to;
    }

}
