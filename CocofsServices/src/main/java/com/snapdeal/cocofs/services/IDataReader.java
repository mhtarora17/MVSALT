/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 09-Sep-2013
 *  @author himanshum
 */
package com.snapdeal.cocofs.services;

import java.util.List;

public interface IDataReader<T,E,D> {

    /**
     * Returns list of mongo Documents of Type D that correspond to the 
     * object dto of Type T
     * @param dto
     * @return
     */
    List<D> getDocumentsForDTO(T dto);
    


    /**
     * Returns list of db entities of Type E that correspond to the object 
     * dto of type T
     * @param dto
     * @return
     */
    List<E> getEntitiesForDTO(T dto);
    
}
