package com.snapdeal.cocofs.services.producer;

import java.util.List;

import com.snapdeal.cocofs.sro.SellerSupcUpdateSRO;

public interface IExternalNotificationsServiceForSearchPush extends IQueueListener {

    boolean publishToSearch(List<SellerSupcUpdateSRO> msg);
}
