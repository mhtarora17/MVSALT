/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 28-Aug-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.services.task.update.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.quartz.CronExpression;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.base.utils.CollectionUtils;
import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.cocofs.annotations.TouchyTransaction;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.entity.TaskDetail;
import com.snapdeal.cocofs.entity.TaskDetailUpdate;
import com.snapdeal.cocofs.entity.TaskParam;
import com.snapdeal.cocofs.entity.TaskStatusInfo;
import com.snapdeal.cocofs.enums.SystemProperty;
import com.snapdeal.cocofs.enums.TaskDetailAction;
import com.snapdeal.cocofs.enums.TaskStatus;
import com.snapdeal.cocofs.services.task.dto.TaskDetailDTO;
import com.snapdeal.cocofs.services.task.dto.TaskDetailUpdateResponseDTO;
import com.snapdeal.cocofs.services.task.dto.TaskInfoDTO;
import com.snapdeal.cocofs.services.task.dto.TaskParamDTO;
import com.snapdeal.cocofs.services.task.update.IAlwaysRunningTaskUpdate;
import com.snapdeal.cocofs.services.task.update.ITaskUpdateConverterService;
import com.snapdeal.cocofs.services.task.update.ITaskUpdateSchedulerService;
import com.snapdeal.cocofs.services.task.update.ITaskUpdateService;
import com.snapdeal.cocofs.task.db.dataaccess.ITaskDBDataReadService;
import com.snapdeal.cocofs.task.db.dataaccess.ITaskDBDataWriteService;
import com.snapdeal.task.core.TaskRequest;
import com.snapdeal.task.exception.TaskException;
import com.snapdeal.task.service.ITaskService;

@Service("taskUpdateService")
public class TaskUpdateServiceImpl implements ITaskUpdateService {

    private static final Logger LOG      = LoggerFactory.getLogger(TaskUpdateServiceImpl.class);

    private static final String hostName = System.getProperty(SystemProperty.HOST_NAME.getCode());

    @Autowired
    ITaskService                taskService;

    @Autowired
    ITaskDBDataReadService      taskDBDataReadService;

    @Autowired
    ITaskDBDataWriteService     taskDBDataWriteService;

    @Autowired
    ITaskUpdateConverterService converterService;

    @Autowired
    IAlwaysRunningTaskUpdate    alwaysRunningTaskUpdate;

    @Autowired
    ITaskUpdateSchedulerService tuss;

    @Override
    @TouchyTransaction
    public void updateTaskDetailAndParams(TaskDetailDTO taskDetailDTO, String updatedBy) throws TaskException {
        TaskDetail taskDetail = converterService.getTaskDetail(taskDetailDTO, updatedBy);
        createTaskDetailUpdate(TaskDetailAction.UPDATE_TASK_DETAIL.getCode(), taskDetail.getName(), taskDetail.getHostName(), updatedBy);

        taskDBDataWriteService.updateTaskDetail(taskDetail);
        List<TaskParam> taskParamList = converterService.getTaskParams(taskDetailDTO.getTaskParams(), taskDetailDTO.getTaskName(), updatedBy);
        for (TaskParam taskParam : taskParamList) {
            taskDBDataWriteService.updateTaskParam(taskParam);
        }
    }

    @Override
    public void createTaskDetailUpdateForAllHosts(String actionCode, String taskName, String updatedBy) {
        List<String> hostNames = ConfigUtils.getStringList(Property.HOST_NAMES);
        if (null != hostNames && !hostNames.isEmpty()) {
            for (String hostName : hostNames) {
                createTaskDetailUpdate(actionCode, taskName, hostName, updatedBy);
            }
        }
    }

    @Override
    public void createTaskDetailUpdate(String actionCode, String taskName, String hostName, String requestedBy) {
        TaskDetailUpdate existingUpdate = null;
        if (StringUtils.isEmpty(taskName)) {
            existingUpdate = taskDBDataReadService.getUnexecutedTaskDetailUpdateForAllTasks(actionCode, hostName);
        } else {
            existingUpdate = taskDBDataReadService.getUnexecutedTaskDetailUpdateByTaskName(taskName, actionCode);
        }

        if (null == existingUpdate) {
            TaskDetailUpdate taskDetailUpdate = new TaskDetailUpdate(taskName, actionCode, Boolean.FALSE, hostName, DateUtils.getCurrentTime(), requestedBy);
            taskDBDataWriteService.mergeTaskDetailUpdate(taskDetailUpdate);
        } else {
            LOG.warn("Not saving this request since taskDetail update exists {}", existingUpdate);
        }
    }

    @Override
    public void createTaskDetailUpdate(String actionCode, String taskName, String updatedBy) {
        TaskDetail taskDetail = taskDBDataReadService.getTaskDetailByName(taskName);
        if (StringUtils.isNotEmpty(taskDetail.getHostName())) {
            createTaskDetailUpdate(actionCode, taskName, taskDetail.getHostName(), updatedBy);
        } else {
            createTaskDetailUpdateForAllHosts(actionCode, taskName, updatedBy);
        }

    }

    @Override
    public void updateTask(TaskDetail taskDetail) throws TaskException {
        if (taskDetail.isQuartzManaged()) {
            TaskRequest request = converterService.getTaskRequest(taskDetail);
            if (taskDetail.isEnabled()) {
                taskService.addOrUpdateTask(request);
                updateTaskStatusInfo(taskDetail.getName(), taskService.getNextScheduledDate(request), null, TaskStatus.WAITING.getCode());
            } else {
                taskService.deleteTask(request);
                updateTaskStatusInfo(taskDetail.getName(), null, taskService.getPreviousFireTime(request), TaskStatus.INACTIVE.getCode());
            }
        } else {
            if (taskDetail.isEnabled()) {
                alwaysRunningTaskUpdate.startTask(taskDetail.getName());
            } else {
                alwaysRunningTaskUpdate.stopTask(taskDetail.getName());
            }
        }
    }

    @Override
    public void startAllTasks() throws TaskException {
        updateAllTasks();
        taskService.startTasks();

    }

    @Override
    public void updateAllTasks() throws TaskException {
        List<TaskDetail> taskDetails = taskDBDataReadService.getAllTasksByHostName(hostName);
        if (null != taskDetails && !taskDetails.isEmpty()) {
            for (TaskDetail taskDetail : taskDetails) {
                updateTask(taskDetail);
            }
        }
    }

    @Override
    public void resumeAllTasks() throws TaskException {
        updateAllTasks();
        taskService.resumeScheduler();
    }

    @Override
    public void stopAllTasks() throws TaskException {
        List<TaskDetail> taskDetails = taskDBDataReadService.getAllEnabledTasks();
        if (!CollectionUtils.isEmpty(taskDetails)) {
            TaskRequest request = null;
            for (TaskDetail taskDetail : taskDetails) {
                if (taskDetail.isQuartzManaged()) {
                    request = converterService.getTaskRequest(taskDetail);
                    taskService.deleteTask(request);
                    updateTaskStatusInfo(taskDetail.getName(), null, taskService.getPreviousFireTime(request), TaskStatus.INACTIVE.getCode());
                } else {
                    alwaysRunningTaskUpdate.stopTask(taskDetail.getName());
                }

            }
        }
        taskService.pauseScheduler();
    }

    @Override
    public void pauseAllTasks() throws TaskException {
        List<TaskDetail> taskDetails = taskDBDataReadService.getAllEnabledTasks();
        if (!CollectionUtils.isEmpty(taskDetails)) {
            TaskRequest request = null;
            for (TaskDetail taskDetail : taskDetails) {
                if (taskDetail.isQuartzManaged()) {
                    request = converterService.getTaskRequest(taskDetail);
                    taskService.deleteTask(request);
                    updateTaskStatusInfo(taskDetail.getName(), null, taskService.getPreviousFireTime(request), TaskStatus.PAUSED.getCode());
                }
            }
        }
        taskService.pauseScheduler();

    }

    @Override
    public List<TaskDetailDTO> getAllTasks() {
        List<TaskDetail> taskDetails = taskDBDataReadService.getAllTasks();
        return getTaskDetailDTOs(taskDetails);

    }

    private List<TaskDetailDTO> getTaskDetailDTOs(List<TaskDetail> taskDetails) {
        List<TaskDetailDTO> dtoList = new ArrayList<TaskDetailDTO>();
        TaskStatusInfo statusInfo = null;
        if (null != taskDetails && !taskDetails.isEmpty()) {
            for (TaskDetail taskDetail : taskDetails) {
                TaskDetailDTO dto = converterService.getTaskDetailDTO(taskDetail);
                Date nextScheduled = null;
                statusInfo = taskDBDataReadService.getTaskStatusInfoByTaskName(taskDetail.getName());
                nextScheduled = statusInfo != null ? statusInfo.getNextExecutionTime() : null;
                dto.setNextScheduled(DateUtils.dateToString(nextScheduled, "yyyy-MM-dd HH:mm:ss"));
                dtoList.add(dto);
            }
        }
        return dtoList;
    }

    @Override
    public List<TaskDetailDTO> getAllTaskByClass(String implClass) {
        List<TaskDetail> taskDetails = taskDBDataReadService.getTaskDetailByImplClass(implClass);
        return getTaskDetailDTOs(taskDetails);
    }

    @Override
    public TaskDetail getTaskDetailByName(String taskName) {
        TaskDetail taskDetail = taskDBDataReadService.getTaskDetailByName(taskName);
        return taskDetail;
    }

    @Override
    public List<TaskParamDTO> getTaskParamsByTaskName(String taskName) {
        return converterService.getTaskParamDTOList(taskDBDataReadService.getTaskParamsByTaskName(taskName));
    }

    @Override()
    public List<TaskInfoDTO> getAllEnabledTaskInfo() {
        List<TaskDetail> taskDetailList = taskDBDataReadService.getAllEnabledTasks();
        List<TaskInfoDTO> dtoList = new ArrayList<TaskInfoDTO>();
        for (TaskDetail taskDetail : taskDetailList) {
            Date previousExecutionTime = null;
            Date nextExecutionTime = null;
            TaskStatusInfo statusInfo = taskDBDataReadService.getTaskStatusInfoByTaskName(taskDetail.getName());
            String currentStatus = null;
            if (null != statusInfo) {
                previousExecutionTime = statusInfo.getPreviousExecutionTime();
                nextExecutionTime = statusInfo.getNextExecutionTime();
                currentStatus = statusInfo.getCurrentStatus();
            }
            TaskInfoDTO dto = new TaskInfoDTO(taskDetail.getName(), taskDetail.getImplClass(), previousExecutionTime, nextExecutionTime, currentStatus, taskDetail.getHostName());
            dto.setQuartzTask(taskDetail.isQuartzManaged());
            dtoList.add(dto);
        }
        return dtoList;

    }

    @Override
    public void updateTask(String taskName) throws TaskException {
        updateTask(taskDBDataReadService.getTaskDetailByName(taskName));

    }

    @Override
    public void startTask(String taskName) throws TaskException {
        TaskDetail taskDetail = taskDBDataReadService.getTaskDetailByName(taskName);
        if (taskDetail.isEnabled()) {
            if (taskDetail.isQuartzManaged()) {
                taskService.executeOnDemand(converterService.getTaskRequest(taskDetail));
            } else {
                alwaysRunningTaskUpdate.startTask(taskName);
            }
        } else {
            LOG.info("Not running this task since it is disabled");
        }

    }

    @Override
    public void stopTask(String taskName) throws TaskException {
        TaskDetail taskDetail = taskDBDataReadService.getTaskDetailByName(taskName);
        if (taskDetail.isQuartzManaged()) {
            taskService.stopTask(converterService.getTaskRequest(taskDetail));
        } else {
            alwaysRunningTaskUpdate.stopTask(taskName);
        }
    }

    @Override
    public void updateTaskStatusInfo(String taskName, Date nextExecutionTime, Date previousExecutionTime, String code) {
        TaskStatusInfo taskStatusInfo = taskDBDataReadService.getTaskStatusInfoByTaskName(taskName);
        if (null == taskStatusInfo) {
            taskStatusInfo = new TaskStatusInfo(taskName, previousExecutionTime, nextExecutionTime, code, DateUtils.getCurrentTime());
        } else {
            taskStatusInfo.setCurrentStatus(code);
            taskStatusInfo.setPreviousExecutionTime(previousExecutionTime);
            taskStatusInfo.setNextExecutionTime(nextExecutionTime);
        }
        taskDBDataWriteService.updateTaskStatusInfo(taskStatusInfo);

    }

    @Override
    public void updateTaskStatusInfo(String taskName, String code) {
        TaskStatusInfo taskStatusInfo = taskDBDataReadService.getTaskStatusInfoByTaskName(taskName);
        if (null == taskStatusInfo) {
            taskStatusInfo = new TaskStatusInfo(taskName, null, null, code, DateUtils.getCurrentTime());
        } else {
            taskStatusInfo.setCurrentStatus(code);
        }
        taskDBDataWriteService.updateTaskStatusInfo(taskStatusInfo);

    }

    @Override
    public TaskDetailUpdateResponseDTO validateRequest(TaskDetailDTO taskDetailDTO) {
        TaskDetailUpdateResponseDTO resp = new TaskDetailUpdateResponseDTO();
        if (!CronExpression.isValidExpression(taskDetailDTO.getCronExpression())) {
            resp.setSuccessful(false);
            resp.getFailedList().add("CronExpression is invalid");
        }
        if (taskDetailDTO.getTaskDetailId() == 0 && taskDBDataReadService.getTaskDetailByName(taskDetailDTO.getTaskName()) != null) {
            resp.setSuccessful(false);
            resp.getFailedList().add("Given task Name exists");
        }
        return resp;
    }

}
