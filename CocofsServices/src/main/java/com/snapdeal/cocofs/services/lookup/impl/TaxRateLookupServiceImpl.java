package com.snapdeal.cocofs.services.lookup.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.dataaccess.taxrate.ITaxRateDBDataReadService;
import com.snapdeal.cocofs.entity.SellerSupcTaxRate;
import com.snapdeal.cocofs.services.ITaxRateLookupService;
import com.snapdeal.cocofs.services.common.dto.TaxRateDownloadData;
import com.snapdeal.cocofs.services.fm.dto.SellerDto;

/**
 * @version 1.0 4 Nov 2015
 * @author indrajit
 */
@Service("taxRateLookupService")
public class TaxRateLookupServiceImpl implements ITaxRateLookupService {

    @Autowired
    ITaxRateDBDataReadService taxRateDBReadService;

    @Override
    public List<SellerDto> getSellersCodeswithTaxRate(){
        List<String> sellers = taxRateDBReadService.getSellersCodeswithTaxRate();
        List<SellerDto> dtos = new ArrayList<SellerDto>();
        for (String seller : sellers) {
            dtos.add(new SellerDto(seller));
        }
        return dtos;
    }
    
    @Override
    public List<TaxRateDownloadData> getTaxRateForSellerForAllSupcs(String sellerCode) {
        // TODO Auto-generated method stub
        List<TaxRateDownloadData> result = new ArrayList<TaxRateDownloadData>();
        List<SellerSupcTaxRate> resultFromDB = taxRateDBReadService.getTaxRateForSellerCode(sellerCode);
        convertData(result, resultFromDB);
        return result;

    }

    private void convertData(List<TaxRateDownloadData> out, List<SellerSupcTaxRate> in) {
        if (null != out || null != in) {
            for (SellerSupcTaxRate i : in) {
                TaxRateDownloadData o = new TaxRateDownloadData();
                o.setSellerCode(i.getSellerCode());
                o.setSupc(i.getSupc());
                o.setTaxRate(i.getTaxRate());
                o.setTaxType(i.getTaxType());
                o.setEnabled(i.isEnabled());
                out.add(o);

            }
        }
    }

}
