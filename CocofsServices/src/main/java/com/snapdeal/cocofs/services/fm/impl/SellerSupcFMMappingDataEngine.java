/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 01-Oct-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.services.fm.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.naming.OperationNotSupportedException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.base.utils.CollectionUtils;
import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.cocofs.core.aerospike.vo.SellerSupcFMMappingVO;
import com.snapdeal.cocofs.entity.SellerSupcFCCodeMapping;
import com.snapdeal.cocofs.entity.SellerSupcFMMapping;
import com.snapdeal.cocofs.services.fm.dto.SellerSupcFMMappingUploadDto;
import com.snapdeal.cocofs.utils.AerospikeKeyHelper;

@Service("SellerSupcFMMappingDataEngine")
public class SellerSupcFMMappingDataEngine extends AbstractFMMappingDataEngine<SellerSupcFMMappingUploadDto, SellerSupcFMMapping, SellerSupcFMMappingVO> {

    @Autowired
    private com.snapdeal.cocofs.services.converter.IConverterService convertorService;

    @Override
    public List<SellerSupcFMMapping> enrichEntities(SellerSupcFMMappingUploadDto dto, List<SellerSupcFMMapping> entities, String userEmail) throws OperationNotSupportedException {
        if (null != dto) {
            SellerSupcFMMapping mapping = null;
            List<SellerSupcFMMapping> mappingList = new ArrayList<SellerSupcFMMapping>();
            if (null != entities && !entities.isEmpty()) {
                mapping = entities.get(0);
            } else {
                mapping = new SellerSupcFMMapping();
            }
            updateSellerSupcFMMapping(dto, mapping, userEmail);
            mappingList.add(mapping);
            return mappingList;
        }
        return null;
    }

    private void updateSellerSupcFMMapping(SellerSupcFMMappingUploadDto dto, SellerSupcFMMapping mapping, String userEmail) {
        mapping.setSupc(dto.getSupc());
        mapping.setEnabled(dto.isEnabled());
        enrichFMMappingEntity(dto, mapping, userEmail);
        enrichFCMappingDetails(dto, mapping, userEmail);
    }

    private void enrichFCMappingDetails(SellerSupcFMMappingUploadDto dto, SellerSupcFMMapping mapping, String userEmail) {

        Set<SellerSupcFCCodeMapping> existingFCCenters = mapping.getFcCenters();
        List<String> newFCCenters = convertorService.convertToFCCentreList(dto.getFcCenters());

        //case 1) no new data - disable all existing if present
        if (CollectionUtils.isEmpty(newFCCenters)) {
            if (CollectionUtils.isEmpty(existingFCCenters)) {
                // no new data, no existing data - nothing to be done
            } else {
                // disable all existing
                for (SellerSupcFCCodeMapping o : existingFCCenters) {
                    if (o != null && o.isEnabled() != false) {
                        o.setEnabled(false);
                        o.setLastUpdated(DateUtils.getCurrentTime());
                        o.setUpdatedBy(userEmail);
                    }
                }
            }
        }
        //case 2) no existing data - add all new data
        else if (CollectionUtils.isEmpty(existingFCCenters)) {
            existingFCCenters = new HashSet<SellerSupcFCCodeMapping>();
            mapping.setFcCenters(existingFCCenters);
            for (String o : newFCCenters) {
                SellerSupcFCCodeMapping item = new SellerSupcFCCodeMapping(mapping, o, userEmail);
                item.setEnabled(dto.isEnabled());
                existingFCCenters.add(item);
            }
        }
        //case 3) change in FC data - disable existing, add/update new
        else {
            // set enabled status based on new fc center data
            for (SellerSupcFCCodeMapping o : existingFCCenters) {
                if (o != null) {
                    // if center is already disabled and does not exist in newList.
                    if (o.isEnabled() == false && !newFCCenters.contains(o.getFcCode())) {
                        // do not do anything.
                    } else {
                        o.setEnabled(newFCCenters.contains(o.getFcCode()) && dto.isEnabled());
                        o.setLastUpdated(DateUtils.getCurrentTime());
                        o.setUpdatedBy(userEmail);
                    }
                }
            }
            // add remaining fc centers 
            for (String o : newFCCenters) {
                if (!containsFCCode(existingFCCenters, o)) {
                    SellerSupcFCCodeMapping item = new SellerSupcFCCodeMapping(mapping, o, userEmail);
                    item.setEnabled(dto.isEnabled());
                    existingFCCenters.add(item);
                }
            }
        }
    }

    private boolean containsFCCode(Set<SellerSupcFCCodeMapping> existingFCCenters, String item) {

        boolean bResult = false;
        for (SellerSupcFCCodeMapping o : existingFCCenters) {
            if (o != null && item.equals(o.getFcCode())) {
                bResult = true;
                break;
            }
        }

        return bResult;
    }

    @Override
    public List<SellerSupcFMMappingVO> enrichRecords(SellerSupcFMMappingUploadDto dto, List<SellerSupcFMMappingVO> records, String userEmail) throws OperationNotSupportedException {
        if (dto != null) {
            SellerSupcFMMappingVO record = null;
            List<SellerSupcFMMappingVO> documentList = new ArrayList<SellerSupcFMMappingVO>();
            if (null != records && !records.isEmpty()) {
                record = records.get(0);
            } else {
                record = new SellerSupcFMMappingVO();
            }
            updateSellerSupcFMMappingVO(dto, record);
            documentList.add(record);
            return documentList;
        }
        return null;
    }

    private void updateSellerSupcFMMappingVO(SellerSupcFMMappingUploadDto dto, SellerSupcFMMappingVO record) {
        record.setSupcSellerCode(AerospikeKeyHelper.getKey(dto.getSellerCode(), dto.getSupc()));
        record.setSellerCode(dto.getSellerCode());
        record.setSupc(dto.getSupc());
        record.setCreatedStr(record.getCreatedStr() != null ? record.getCreatedStr() : DateUtils.getCurrentTime().getTime() + "");
        record.setUpdatedStr(DateUtils.getCurrentTime().getTime() + "");
        record.setFulfillmentModel(dto.getFulfilmentModel());
        record.setEnabled(1);

        //update FC centers in record
        List<String> newFCCenters = null;
        if (dto.isEnabled()) {
            newFCCenters = convertorService.convertToFCCentreList(dto.getFcCenters());
        }
        record.setFcCenters(newFCCenters);
    }

}
