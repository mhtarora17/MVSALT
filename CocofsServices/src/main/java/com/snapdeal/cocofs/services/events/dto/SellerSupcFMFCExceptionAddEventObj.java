/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.services.events.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @version 1.0, 25-Mar-2015
 * @author shiv
 */
public class SellerSupcFMFCExceptionAddEventObj implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1843468712814577143L;

    private String            sellerCode;
    private String            supc;
    private String            newFM;
    private String            oldFM;
    private Date              created;
    private List<String>      existingFCCenters;
    private List<String>      newFCCenters;

    public SellerSupcFMFCExceptionAddEventObj() {
    }

    public SellerSupcFMFCExceptionAddEventObj(String sellerCode, String supc, String newFM, String oldFM, List<String> newFCCenters, List<String> existingFCCenters) {
        this.sellerCode = sellerCode;
        this.supc = supc;
        this.newFM = newFM;
        this.oldFM = oldFM;
        this.setExistingFCCenters(existingFCCenters);
        this.setNewFCCenters(newFCCenters);
        this.created = new Date();
    }

    public String getSellerCode() {
        return sellerCode;
    }

    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }

    public String getSupc() {
        return supc;
    }

    public void setSupc(String supc) {
        this.supc = supc;
    }

    public String getNewFM() {
        return newFM;
    }

    public void setNewFM(String newFM) {
        this.newFM = newFM;
    }

    public String getOldFM() {
        return oldFM;
    }

    public void setOldFM(String oldFM) {
        this.oldFM = oldFM;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public List<String> getExistingFCCenters() {
        return existingFCCenters;
    }

    public void setExistingFCCenters(List<String> existingFCCenters) {
        this.existingFCCenters = existingFCCenters;
    }

    public List<String> getNewFCCenters() {
        return newFCCenters;
    }

    public void setNewFCCenters(List<String> newFCCenters) {
        this.newFCCenters = newFCCenters;
    }

    @Override
    public String toString() {
        return "SellerSupcFMFCExceptionAddEventObj [sellerCode=" + sellerCode + ", supc=" + supc + ", newFM=" + newFM + ", oldFM=" + oldFM + ", created=" + created
                + ", existingFCCenters=" + existingFCCenters + ", newFCCenters=" + newFCCenters + "]";
    }


}
