/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.services.events.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.Gson;
import com.snapdeal.cocofs.entity.EventInstance;
import com.snapdeal.cocofs.services.events.IEventConsumer;
import com.snapdeal.cocofs.services.events.IEventConsumerRegistry;
import com.snapdeal.cocofs.services.events.dto.ParameterInfo;
import com.snapdeal.cocofs.services.events.dto.ParameterList;

/**
 * @version 1.0, 26-Jan-2015
 * @author shiv
 */
public abstract class BaseEventConsumer<T> implements IEventConsumer<T> {

    private static final Logger    LOG = LoggerFactory.getLogger(BaseEventConsumer.class);

    @Autowired
    private IEventConsumerRegistry eventConsumerRegistry;

    protected abstract boolean processEventObj(T eventObject);

    protected abstract String getEventCode();

    @PostConstruct
    @Override
    public void registerAsEventListener() {
        eventConsumerRegistry.registerConsumer(getEventCode(), this);
    }

    @Override
    public boolean processEvent(EventInstance event) {
        try {
            LOG.debug("Started to process event: {}", event);
            T eventObject = getArgumentObject(event);
            return processEventObj(eventObject);
        } catch (ClassNotFoundException e) {
            LOG.error("Error while executing event: {}. Exception:", e);
        }
        return false;
    }

    /**
     * method to process the json argument list. Assuming only single argument for current implementation.
     * 
     * @param eventInstance
     * @return
     * @throws ClassNotFoundException
     */
    @SuppressWarnings("unchecked")
    private T getArgumentObject(EventInstance eventInstance) throws ClassNotFoundException {
        Gson gson = new Gson();
        ParameterList params = gson.fromJson(eventInstance.getJsonArguments(), ParameterList.class);
        List<Object> argumentsList = new ArrayList<Object>();
        for (ParameterInfo param : params.getParameters()) {
            Object object = gson.fromJson(param.getJsonString(), Class.forName(param.getType()));
            argumentsList.add(object);
        }
        if (argumentsList.isEmpty()) {
            return null;
        }
        return (T) argumentsList.get(0);
    }

}
