package com.snapdeal.cocofs.services.data.engine.impl;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.naming.OperationNotSupportedException;

import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.entity.ProductAttribute;
import com.snapdeal.cocofs.excelupload.ProductAttributeUploadDTO;
import com.snapdeal.cocofs.mongo.model.ProductAttributeUnit;

/**
 * @author nikhil
 */
@Service("productAttributeUpdateOnlyEngine")
public class ProductAttributeUpdateOnlyEngine extends AbstractProductAttributeEngine {

    @Override
    public List<ProductAttributeUnit> enrichDocuments(ProductAttributeUploadDTO dto, List<ProductAttributeUnit> documents) throws OperationNotSupportedException {

        List<ProductAttributeUnit> docList = new ArrayList<ProductAttributeUnit>();

        if (documents == null || (documents != null && documents.isEmpty())) {
            throw new IllegalArgumentException("No mongo document exists for supc " + dto.getSupc());
        }

        ProductAttributeUnit pau = documents.get(0);
        LOG.info("Existing mongo doc for supc " + dto.getSupc() + " being updated with " + dto + " Mongo object id " + pau.getObjectId());
        pau.setBreadth(dto.getBreadth());
        pau.setPrimaryBreadth(dto.getPrimaryBreadth());
        pau.setFragile(dto.isFragile());
        pau.setHeight(dto.getHeight());
        pau.setPrimaryHeight(dto.getPrimaryHeight());
        pau.setLength(dto.getLength());
        pau.setPrimaryLength(dto.getPrimaryLength());
        pau.setProductParts(dto.getProductParts());
        pau.setSupc(dto.getSupc());
        pau.setWeight(dto.getWeight());
        pau.setSystemWeightCaptured(dto.getSystemWeightCaptured());
        pau.setDangerousGoodsType(dto.getDangerousGoodsType());
        pau.setSerializedType(dto.getSerializedType());
        pau.setPackagingType(dto.getPackagingType());
        docList.add(pau);
        return docList;
    }

    @Override
    public List<ProductAttribute> enrichEntities(ProductAttributeUploadDTO dto, List<ProductAttribute> entities, String userEmail) throws OperationNotSupportedException {
        List<ProductAttribute> toReturn = new ArrayList<ProductAttribute>();

        if (entities == null || (entities != null && entities.isEmpty())) {
            throw new IllegalArgumentException("No entity exists for supc " + dto.getSupc());
        }

        Map<String, String> m = ConfigUtils.getMap(Property.PRODUCT_ATTRIBUTE_UPLOAD_DTO_FIELD_MAP);
        Field[] flds = dto.getClass().getDeclaredFields();
        for (Field fld : flds) {
            int mods = fld.getModifiers();
            if (Modifier.isStatic(mods)) {
                continue;
            }

            String attributeName = m.get(fld.getName());

            if (fld.getName().equals("supc")) {
                continue;
            }

            String fieldValueAsString = getNotNullFieldValueAsString(attributeName, fld, dto);

            ProductAttribute attr = findProductAttributeForName(attributeName, entities);

            if (null == attr) {
                LOG.info("Creating new product attribute for supc " + dto.getSupc() + " attribute " + attributeName + " with value " + fieldValueAsString + " per bulk update by "
                        + userEmail);

                ProductAttribute newAttr = createNewProductAttribute(dto, userEmail, attributeName, fieldValueAsString);
                toReturn.add(newAttr);
            } else {
                LOG.info("Updating product attribute for supc " + dto.getSupc() + " attribute " + attributeName + " with value " + fieldValueAsString + " per bulk update by "
                        + userEmail);
                updateProductAttribute(userEmail, attr, fieldValueAsString);
                toReturn.add(attr);
            }
        }
        return toReturn;
    }
}
