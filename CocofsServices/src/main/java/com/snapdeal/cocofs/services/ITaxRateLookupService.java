package com.snapdeal.cocofs.services;

import java.util.List;

import com.snapdeal.cocofs.services.common.dto.TaxRateDownloadData;
import com.snapdeal.cocofs.services.fm.dto.SellerDto;

/**
 *  @version 1.0 4 Nov 2015
 *  @author indrajit
 *
 */
public interface ITaxRateLookupService {
	
	List<TaxRateDownloadData> getTaxRateForSellerForAllSupcs(String sellerCode);

    List<SellerDto> getSellersCodeswithTaxRate();

}
