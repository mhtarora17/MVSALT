///*
// *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
// *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
// *  
// *  @version     1.0, 09-Sep-2013
// *  @author himanshum
// */
//package com.snapdeal.cocofs.services.data.engine.impl;
//
//import java.lang.reflect.Field;
//import java.lang.reflect.Modifier;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
//import java.util.Map;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.stereotype.Service;
//
//import com.snapdeal.base.utils.DateUtils;
//import com.snapdeal.base.utils.StringUtils;
//import com.snapdeal.cocofs.configuration.ConfigUtils;
//import com.snapdeal.cocofs.configuration.Property;
//import com.snapdeal.cocofs.entity.ProductAttribute;
//import com.snapdeal.cocofs.entity.ProductAttributeHistory;
//import com.snapdeal.cocofs.excelupload.ProductWeightUploadDTO;
//import com.snapdeal.cocofs.mongo.model.ProductAttributeUnit;
//import com.snapdeal.cocofs.services.data.engine.IDataEngine;
//
//@Service("productWeightUploadMerger")
//public class ProductWeightUploadMerger implements IDataEngine<ProductWeightUploadDTO , ProductAttribute, ProductAttributeUnit> {
//
//    
//    private static final Logger LOG = LoggerFactory.getLogger(ProductWeightUploadMerger.class);
//    
//    @Override
//    public List<ProductAttributeUnit> mergeDTOWithDocuments(ProductWeightUploadDTO dto, List<ProductAttributeUnit> documents) {
//        List<ProductAttributeUnit> docList = new ArrayList<ProductAttributeUnit>();
//        if ( documents.isEmpty()) {
//            LOG.info("No Existing mongo doc for supc " + dto.getSupc() + " creatinng one now " + dto);
//            ProductAttributeUnit pau = createNewProductAttributeUnitDocument(dto);
//            docList.add(pau);
//        } else {
//            
//            ProductAttributeUnit pau = documents.get(0);
//            LOG.info("Existing mongo doc for supc " + dto.getSupc() + " being updated with " + dto + " Mongo object id " + pau.getObjectId());
//            
//            pau.setSupc(dto.getSupc());
//            pau.setWeight(dto.getWeight());
//            docList.add(pau);
//        }
//        return docList;
//    }
//
//    private ProductAttributeUnit createNewProductAttributeUnitDocument(ProductWeightUploadDTO dto) {
//        ProductAttributeUnit pau = new ProductAttributeUnit();
//        pau.setSupc(dto.getSupc());
//        pau.setWeight(dto.getWeight());
//        return pau;
//    }
//
//    @Override
//    public List<ProductAttribute> mergeDTOWithEntities(ProductWeightUploadDTO dto, List<ProductAttribute> entities, String userEmail) {
//        List<ProductAttribute> outList = new ArrayList<ProductAttribute>();
//        Map<String, String> m = ConfigUtils.getMap(Property.PRODUCT_WEIGHT_UPLOAD_DTO_FIELD_MAP);
//        Field[] flds = dto.getClass().getDeclaredFields();
//        for ( Field fld : flds) {
//            int mods = fld.getModifiers();
//            if ( Modifier.isStatic(mods)) {
//                continue;
//            }
//            String attributeName = m.get(fld.getName());
//            if ( StringUtils.isEmpty(attributeName)) {
//                continue;
//            }
//            ProductAttribute attr = findProductAttributeForName(attributeName, entities);
//            String fieldValueAsString = null;
//            try {
//                fieldValueAsString = getFieldValueAsString(fld, dto);
//            } catch (IllegalArgumentException e) {
//                LOG.info("Could not obtain attribute value as string from dto field " + fld.getName(), e);
//            } catch (IllegalAccessException e) {
//                LOG.info("Could not obtain attribute value as string from dto field " + fld.getName(), e);
//            }
//            
//            if ( null == fieldValueAsString) {
//                LOG.info("attribute value is null for dto field " + fld.getName());
//                continue;
//            }
//            if ( null == attr ) {
//                LOG.info("Creating new product attribute for supc " + dto.getSupc() + " attribute " + attributeName + " with value " + fieldValueAsString + " per bulk update by "
//                        + userEmail);
//                ProductAttribute newAttr = createNewProductAttribute(dto, userEmail, attributeName, fieldValueAsString);
//                outList.add(newAttr);
//            } else {
//                LOG.info("Updating product attribute for supc " + dto.getSupc() + " attribute " + attributeName + " with value " + fieldValueAsString + " per bulk update by "
//                        + userEmail);
//                updateProductAttribute(userEmail, attr, fieldValueAsString);
//                outList.add(attr);
//            }
//        }
//        return outList;
//        
//    }
//    
//    private void updateProductAttribute(String userEmail, ProductAttribute attr, String fieldValueAsString) {
//        attr.getHistory().add(createPAHistory(attr.getSupc(), userEmail, attr.getAttribute(), fieldValueAsString, attr));
//        attr.setValue(fieldValueAsString);
//        attr.setLastUpdated(new Date());
//        attr.setUpdatedBy(userEmail);
//    }
//
//    private ProductAttribute createNewProductAttribute(ProductWeightUploadDTO dto, String userEmail, String attributeName, String fieldValueAsString) {
//        ProductAttribute newAttr = new ProductAttribute();
//        newAttr.setAttribute(attributeName);
//        newAttr.setCreated(new Date());
//        newAttr.setCreatedBy(userEmail);
//        newAttr.setEnabled(true);
//        newAttr.setLastUpdated(newAttr.getCreated());
//        newAttr.setSupc(dto.getSupc());
//        newAttr.setUpdatedBy(userEmail);
//        newAttr.setValue(fieldValueAsString);
//        ProductAttributeHistory attrHistory = createNewPAHistory(dto, userEmail, attributeName, fieldValueAsString);
//        newAttr.getHistory().add(attrHistory );
//        return newAttr;
//    }
//
//    private ProductAttributeHistory createNewPAHistory(ProductWeightUploadDTO dto, String userEmail, String attributeName, String fieldValueAsString) {
//        ProductAttributeHistory attrHistory = new ProductAttributeHistory();
//        attrHistory.setAttribute(attributeName);
//        attrHistory.setCreated(new Date());
//        attrHistory.setCreatedBy(userEmail);
//        attrHistory.setLastUpdated(attrHistory.getCreated());
//        attrHistory.setNewValue(fieldValueAsString);
//        attrHistory.setOldValue(null);
//        attrHistory.setRemark("Creating attribute " + attributeName + " on upload request by " + userEmail);
//        attrHistory.setSupc(dto.getSupc());
//        attrHistory.setUpdatedBy(userEmail);
//        return attrHistory;
//    }
//    
//    private ProductAttributeHistory createPAHistory(String supc, String userEmail, String attributeName, String fieldValueAsString, ProductAttribute attr) {
//        ProductAttributeHistory attrHistory = new ProductAttributeHistory();
//        attrHistory.setAttribute(attributeName);
//        attrHistory.setCreated(new Date());
//        attrHistory.setCreatedBy(userEmail);
//        attrHistory.setLastUpdated(attrHistory.getCreated());
//        attrHistory.setNewValue(fieldValueAsString);
//        attrHistory.setOldValue(attr.getValue());
//        attrHistory.setRemark("Updating attribute " + attributeName + " on upload request by " + userEmail);
//        attrHistory.setSupc(supc);
//        attrHistory.setUpdatedBy(userEmail);
//        return attrHistory;
//    }
//    
//
//    private String getFieldValueAsString(Field fld, ProductWeightUploadDTO dto) throws IllegalArgumentException, IllegalAccessException {
//        fld.setAccessible(true);
//        Object o = fld.get(dto);
//        if ( null == o ) {
//            LOG.info("Field value for field " + fld.getName() + " is null , supc " + dto.getSupc());
//            return null;
//        }
//        if ( o instanceof Date ) {
//            // Except for Dates we are okay with default toString for most objects
//            return DateUtils.dateToString( (Date) o, "YYYY-MM-dd HH:mm:ss");
//        
//        } else {
//            return o.toString();
//        }
//        
//
//    }
//
//    private ProductAttribute findProductAttributeForName(String attributeName, List<ProductAttribute> entities) {
//        for ( ProductAttribute attr : entities) {
//            if (attr.getAttribute().equals(attributeName)) {
//                return attr;
//            }
//        }
//        return null;
//    }
//
//
//
//}
