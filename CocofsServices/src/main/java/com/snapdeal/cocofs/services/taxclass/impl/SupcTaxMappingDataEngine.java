/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 07-Oct-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.services.taxclass.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.naming.OperationNotSupportedException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.base.utils.CollectionUtils;
import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.cocofs.core.aerospike.vo.SellerFMMappingVO;
import com.snapdeal.cocofs.core.aerospike.vo.SupcTaxClassMappingVO;
import com.snapdeal.cocofs.dataaccess.fm.IFMDBDataReadService;
import com.snapdeal.cocofs.dataaccess.taxclass.ITaxClassDBDataReadService;
import com.snapdeal.cocofs.entity.SellerFCCodeMapping;
import com.snapdeal.cocofs.entity.SellerFMMapping;
import com.snapdeal.cocofs.entity.SellerSubcatFMMapping;
import com.snapdeal.cocofs.entity.SellerSupcFMMapping;
import com.snapdeal.cocofs.entity.SupcTaxClassMapping;
import com.snapdeal.cocofs.services.fm.dto.SellerFMMappingUpdateDto;
import com.snapdeal.cocofs.services.taxclass.dto.SupcTaxClassMappingDTO;

@Service("SupcTaxMappingDataEngine")
public class SupcTaxMappingDataEngine extends AbstractTaxMappingDataEngine<SupcTaxClassMappingDTO, SupcTaxClassMapping, SupcTaxClassMappingVO> {

    @Autowired
    private ITaxClassDBDataReadService                               taxClassDBDataReadService;

    @Autowired
    private com.snapdeal.cocofs.services.converter.IConverterService convertorService;

    @Override
    public List<SupcTaxClassMapping> enrichEntities(SupcTaxClassMappingDTO dto, List<SupcTaxClassMapping> entities, String userEmail) throws OperationNotSupportedException {
        if (null != dto) {
            SupcTaxClassMapping mapping = null;
            List<SupcTaxClassMapping> mappingList = new ArrayList<SupcTaxClassMapping>();

            if (null != entities && !entities.isEmpty()) {
                mapping = entities.get(0);
            } else {
                mapping = new SupcTaxClassMapping();
            }

            enrichTaxClassMappingEntity(dto, mapping, userEmail);
            mappingList.add(mapping);
            return mappingList;
        }
        return null;
    }

    @Override
    public List<SupcTaxClassMappingVO> enrichRecords(SupcTaxClassMappingDTO dto, List<SupcTaxClassMappingVO> records, String userEmail) throws OperationNotSupportedException {
        if (dto != null) {
            SupcTaxClassMappingVO record = null;
            List<SupcTaxClassMappingVO> recordList = new ArrayList<SupcTaxClassMappingVO>();
            if (null != records && !records.isEmpty()) {
                record = records.get(0);
            } else {
                record = new SupcTaxClassMappingVO();
            }
            updateSupcTaxClassMappingVO(dto, record);
            recordList.add(record);
            return recordList;

        }
        return null;
    }

    private void updateSupcTaxClassMappingVO(SupcTaxClassMappingDTO dto, SupcTaxClassMappingVO record) {
        record.setCreatedStr(record.getCreated() != null ? String.valueOf(record.getCreated().getTime()) : String.valueOf(DateUtils.getCurrentTime().getTime()));
        record.setUpdatedStr(DateUtils.getCurrentTime().getTime() + "");
        record.setTaxClass(dto.getTaxClass());
        record.setSupc(dto.getSupc());
        record.setEnabled(dto.isEnabled() == true ? 1 : 0);
    }

}
