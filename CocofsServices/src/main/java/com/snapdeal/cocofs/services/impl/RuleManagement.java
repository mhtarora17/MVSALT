/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 17, 2013
 *  @author himanshu
 */
package com.snapdeal.cocofs.services.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.math.RandomUtils;
import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.snapdeal.base.cache.CacheManager;
import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.cocofs.common.CocofsBlockParam;
import com.snapdeal.cocofs.common.CocofsRuleParam;
import com.snapdeal.cocofs.common.RuleCreationDTO;
import com.snapdeal.cocofs.common.RuleEditDTO;
import com.snapdeal.cocofs.common.RuleUpdateResponse;
import com.snapdeal.cocofs.mongo.CocofsMongoQuery;
import com.snapdeal.cocofs.mongo.CocofsMongoQuery.Operator;
import com.snapdeal.cocofs.mongo.mao.IGenericMao;
import com.snapdeal.cocofs.mongo.model.CocofsRuleUnit;
import com.snapdeal.cocofs.mongo.model.CocofsRuleUnit.CocofsRuleUnitFieldName;
import com.snapdeal.cocofs.mongo.model.CriteriaUnit;
import com.snapdeal.cocofs.services.IRuleManagement;
import com.snapdeal.rule.cache.BlocksCache;
import com.snapdeal.rule.engine.entity.Criteria;
import com.snapdeal.rule.engine.entity.Rule;
import com.snapdeal.rule.engine.entity.RuleParams;
import com.snapdeal.rule.engine.services.IRulesService;

@Service("ruleManager")
public class RuleManagement implements IRuleManagement {

    private static final Logger LOG = LoggerFactory.getLogger(RuleManagement.class);

    @Autowired
    private IRulesService       ruleService;

    @Autowired
    private IGenericMao         genericMao;

    @Transactional
    @Override
    public Rule createRule(RuleCreationDTO dto) {
        String ruleName = "R"+DateUtils.dateToString(DateUtils.getCurrentTime(), "yyyyMMddHHmmssSSS")+"-"+StringUtils.getRandomAlphaNumeric(4);
        
        CocofsRuleUnit unit = getRuleUnitFromRuleCreationDTO(dto, ruleName);
        Rule entity = getRuleFromRuleCreationDTO(dto, ruleName );
        
        ruleService.createRule(entity);
        genericMao.saveDocument(unit);
        return entity;
    }

    private CriteriaUnit findCriteria(List<CriteriaUnit> criteria, String name) {
        for ( CriteriaUnit c : criteria) {
            if ( name.equals(c.getName())) {
                return c;
            }
        }
        return null;
    }

    private Criteria findCriteria(Set<Criteria> criterion, String name) {
        for ( Criteria c : criterion) {
            if ( name.equals(c.getName())) {
                return c;
            }
        }
        return null;
    }

    private Boolean getBooleanFromString(String fragile) {
        if (StringUtils.isEmpty(fragile)) {
            return null;
        }
        return !fragile.startsWith("not");
    }

    private Date getDateFromString(String enddate) {
        if (StringUtils.isEmpty(enddate)) {
            return null;
        }
        return DateUtils.stringToDate(enddate, "yyyy-MM-dd");
    }

    private Double getDoubleFromString(String volweight) {
        if (StringUtils.isEmpty(volweight)) {
            return null;
        }
        try {
            Double d = Double.valueOf(volweight);
            return d;
        } catch (NumberFormatException e) {
            LOG.info("Could not convert " + volweight + " to double number ");
            return null;
        }
    }

    @Transactional(readOnly = true)
    @Override
    public List<Rule> getEnabledRulesForCategoryAndType(String categoryUrl, String type) {
        List<Rule> rules = ruleService.getRuleByCode(getRuleCodeForCategoryAndType(categoryUrl, type));
        List<Rule> enabledRules = new ArrayList<Rule>();
        for (Rule r : rules) {
            if (r.isEnabled()) {
                if (!Hibernate.isInitialized(r.getCriterion())) {
                    Hibernate.initialize(r.getCriterion());
                }
                if (!Hibernate.isInitialized(r.getRuleParams())) {
                    Hibernate.initialize(r.getRuleParams());
                }
                enabledRules.add(r);
            }
        }
        return enabledRules;
    }

    private List<CocofsRuleUnit> getEnabledRuleUnitsForCategoryAndType(String categoryUrl, String ruleType) {
        List<com.snapdeal.cocofs.mongo.Criteria> queryCriterias = new ArrayList<com.snapdeal.cocofs.mongo.Criteria>();
        com.snapdeal.cocofs.mongo.Criteria mqcForCategory = new com.snapdeal.cocofs.mongo.Criteria(CocofsRuleUnitFieldName.CATEGORY_URL.getName(), categoryUrl, Operator.EQUAL);
        com.snapdeal.cocofs.mongo.Criteria mqcForType = new com.snapdeal.cocofs.mongo.Criteria(CocofsRuleUnitFieldName.TYPE.getName(), ruleType, Operator.EQUAL);
        queryCriterias.add(mqcForCategory);
        queryCriterias.add(mqcForType);
        List<CocofsRuleUnit> ruleList = genericMao.getDocumentList(new CocofsMongoQuery(queryCriterias), CocofsRuleUnit.class);
        List<CocofsRuleUnit> outList = new ArrayList<CocofsRuleUnit>();
        for ( CocofsRuleUnit r : ruleList) {
            if ( r.isEnabled() ) {
                outList.add(r);
            }
        }
        return outList;
    }

    private String getRuleCodeForCategoryAndType(String categoryUrl, String type) {
        return type + "|" + categoryUrl;
    }

    @Override
    public RuleCreationDTO getRuleCreationDTO(String ruleType, String categoryUrl, String fragile, String hazmat, String liquid, String weight, String volweight, String enddate,
            String startdate, String user) {
        RuleCreationDTO dto = new RuleCreationDTO();
        dto.setCategoryUrl(categoryUrl);
        Date d = getDateFromString(enddate);
        if (null != d) {
            Calendar cal = new GregorianCalendar();
            cal.setTime(d);
            cal.add(Calendar.DATE, 1);
            dto.setEndDate(cal.getTime());
        } else {
            dto.setEndDate(null);
        }

        dto.setFragile(getBooleanFromString(fragile));
        dto.setHazMat(getBooleanFromString(hazmat));
        dto.setLiquid(getBooleanFromString(liquid));
        dto.setRuleType(ruleType);
        dto.setStartDate(getDateFromString(startdate));
        dto.setUpdatedBy(user);
        dto.setUpdateTime(new Date());
        dto.setVolumetricWeight(getDoubleFromString(volweight));
        dto.setWeight(getDoubleFromString(weight));
        LOG.info(dto.toString());
        return dto;
    }
    
    @Override
    public RuleEditDTO getRuleEditDTO(String ruleType, String categoryUrl, String fragile, String hazmat, String liquid, String weight, String volweight, String enddate,
            String startdate, String user, String enabled) {
        RuleEditDTO dto = new RuleEditDTO();
        dto.setCategoryUrl(categoryUrl);
        Date d = getDateFromString(enddate);
        if (null != d) {
            Calendar cal = new GregorianCalendar();
            cal.setTime(d);
            cal.add(Calendar.DATE, 1);
            dto.setEndDate(cal.getTime());
        } else {
            dto.setEndDate(null);
        }

        dto.setFragile(getBooleanFromString(fragile));
        dto.setHazMat(getBooleanFromString(hazmat));
        dto.setLiquid(getBooleanFromString(liquid));
        dto.setRuleType(ruleType);
        dto.setStartDate(getDateFromString(startdate));
        dto.setUpdatedBy(user);
        dto.setUpdateTime(new Date());
        dto.setVolumetricWeight(getDoubleFromString(volweight));
        dto.setWeight(getDoubleFromString(weight));
        dto.setEnabled(getBooleanFromString(enabled));
        LOG.info(dto.toString());
        return dto;
    }

    private Set<Criteria> getRuleCriteriaFromRuleCreationDTO(RuleCreationDTO dto) {
        Set<Criteria> criteria = new HashSet<Criteria>();

        if (null != dto.getFragile()) {
            Criteria crit = new Criteria();
            crit.setCreated(new Date());
            crit.setEnabled(true);
            crit.setName(CocofsBlockParam.FRAGILE.getName());
            crit.setOperator(CocofsBlockParam.FRAGILE.getOperator());
            crit.setValue(dto.getFragile().toString());
            crit.setValueType(CocofsBlockParam.FRAGILE.getType());
            criteria.add(crit);
        }
        if (null != dto.getLiquid()) {
            Criteria crit = new Criteria();
            crit.setCreated(new Date());
            crit.setEnabled(true);
            crit.setName(CocofsBlockParam.LIQUID.getName());
            crit.setOperator(CocofsBlockParam.LIQUID.getOperator());
            crit.setValue(dto.getLiquid().toString());
            crit.setValueType(CocofsBlockParam.LIQUID.getType());
            criteria.add(crit);
        }
        if (null != dto.getHazMat()) {
            Criteria crit = new Criteria();
            crit.setCreated(new Date());
            crit.setEnabled(true);
            crit.setName(CocofsBlockParam.HAZARDOUS.getName());
            crit.setOperator(CocofsBlockParam.HAZARDOUS.getOperator());
            crit.setValue(dto.getHazMat().toString());
            crit.setValueType(CocofsBlockParam.HAZARDOUS.getType());
            criteria.add(crit);
        }
        if (null != dto.getWeight()) {
            Criteria crit = new Criteria();
            crit.setCreated(new Date());
            crit.setEnabled(true);
            crit.setName(CocofsBlockParam.WEIGHT.getName());
            crit.setOperator(CocofsBlockParam.WEIGHT.getOperator());
            crit.setValue(dto.getWeight().toString());
            crit.setValueType(CocofsBlockParam.WEIGHT.getType());
            criteria.add(crit);

        }
        if (null != dto.getVolumetricWeight()) {
            Criteria crit = new Criteria();
            crit.setCreated(new Date());
            crit.setEnabled(true);
            crit.setName(CocofsBlockParam.VOLUMETRIC_WEIGHT.getName());
            crit.setOperator(CocofsBlockParam.VOLUMETRIC_WEIGHT.getOperator());
            crit.setValue(dto.getVolumetricWeight().toString());
            crit.setValueType(CocofsBlockParam.VOLUMETRIC_WEIGHT.getType());
            criteria.add(crit);

        }
        return criteria;
    }

    private List<CriteriaUnit> getRuleCriteriaUnitFromRuleCreationDTO(RuleCreationDTO dto) {
        List<CriteriaUnit> list = new ArrayList<CriteriaUnit>();
        if (null != dto.getFragile()) {

            CriteriaUnit unit = new CriteriaUnit();
            unit.setCreated(dto.getUpdateTime());
            unit.setEnabled(true);
            unit.setName(CocofsBlockParam.FRAGILE.getName());
            unit.setOperator(CocofsBlockParam.FRAGILE.getOperator().getCode());
            unit.setUpdated(dto.getUpdateTime());
            unit.setValue(dto.getFragile().toString());
            unit.setValueType(CocofsBlockParam.FRAGILE.getType().name());
            unit.setId(RandomUtils.nextInt());
            list.add(unit);
        }
        if (null != dto.getLiquid()) {
            CriteriaUnit unit = new CriteriaUnit();
            unit.setCreated(dto.getUpdateTime());
            unit.setEnabled(true);
            unit.setName(CocofsBlockParam.LIQUID.getName());
            unit.setOperator(CocofsBlockParam.LIQUID.getOperator().getCode());
            unit.setUpdated(dto.getUpdateTime());
            unit.setValue(dto.getLiquid().toString());
            unit.setValueType(CocofsBlockParam.LIQUID.getType().name());
            unit.setId(RandomUtils.nextInt());
            list.add(unit);
        }
        if (null != dto.getHazMat()) {
            CriteriaUnit unit = new CriteriaUnit();
            unit.setCreated(dto.getUpdateTime());
            unit.setEnabled(true);
            unit.setName(CocofsBlockParam.HAZARDOUS.getName());
            unit.setOperator(CocofsBlockParam.HAZARDOUS.getOperator().getCode());
            unit.setUpdated(dto.getUpdateTime());
            unit.setValue(dto.getHazMat().toString());
            unit.setValueType(CocofsBlockParam.HAZARDOUS.getType().name());
            unit.setId(RandomUtils.nextInt());
            list.add(unit);
        }
        if (null != dto.getWeight()) {
            CriteriaUnit unit = new CriteriaUnit();
            unit.setCreated(dto.getUpdateTime());
            unit.setEnabled(true);
            unit.setName(CocofsBlockParam.WEIGHT.getName());
            unit.setOperator(CocofsBlockParam.WEIGHT.getOperator().getCode());
            unit.setUpdated(dto.getUpdateTime());
            unit.setValue(dto.getWeight().toString());
            unit.setValueType(CocofsBlockParam.WEIGHT.getType().name());
            unit.setId(RandomUtils.nextInt());
            list.add(unit);

        }
        if (null != dto.getVolumetricWeight()) {
            CriteriaUnit unit = new CriteriaUnit();
            unit.setCreated(dto.getUpdateTime());
            unit.setEnabled(true);
            unit.setName(CocofsBlockParam.VOLUMETRIC_WEIGHT.getName());
            unit.setOperator(CocofsBlockParam.VOLUMETRIC_WEIGHT.getOperator().getCode());
            unit.setUpdated(dto.getUpdateTime());
            unit.setValue(dto.getVolumetricWeight().toString());
            unit.setValueType(CocofsBlockParam.VOLUMETRIC_WEIGHT.getType().name());
            unit.setId(RandomUtils.nextInt());
            list.add(unit);

        }
        return list;
    }
    
    private Rule getRuleFromRuleCreationDTO(RuleCreationDTO dto, String ruleName) {
        Rule r = new Rule();
        r.setBlockId(CacheManager.getInstance().getCache(BlocksCache.class).getBlockIDByName(dto.getRuleType()));

        r.setCreated(dto.getUpdateTime());
        r.setCreatedBy(dto.getUpdatedBy());
        Set<Criteria> criterion = getRuleCriteriaFromRuleCreationDTO(dto);
        r.setCriterion(criterion);
        r.setEnabled(true);
        r.setEndDate(dto.getEndDate());
        r.setName(ruleName);
        r.setCode(getRuleCodeForCategoryAndType(dto.getCategoryUrl(), dto.getRuleType()));
        r.setPriority(0);
        r.setRemark("Creating rule for " + dto.getRuleType() + " and " + dto.getCategoryUrl() + " by user " + dto.getUpdatedBy());
        Set<RuleParams> ruleParams = getRuleParamFromRuleCreationDTO(dto, r);
        r.setRuleParams(ruleParams);
        r.setStartDate(dto.getStartDate());
        r.setUpdated(dto.getUpdateTime());
        r.setUpdatedBy(dto.getUpdatedBy());
        r.setSynched(true);
        return r;
    }
    
    
    private Set<RuleParams> getRuleParamFromRuleCreationDTO(RuleCreationDTO dto, Rule r) {
        Set<RuleParams> params = new HashSet<RuleParams>();
        RuleParams p = new RuleParams();
        p.setParamValue(dto.getCategoryUrl());
        p.setParamName(CocofsRuleParam.CATEGORY_URL.getName());
        p.setRule(r);
        params.add(p);
        return params;
    }

    private CocofsRuleUnit getRuleUnitFromRuleCreationDTO(RuleCreationDTO dto, String ruleName) {
        CocofsRuleUnit unit = new CocofsRuleUnit();
        unit.setCategoryUrl(dto.getCategoryUrl());
        unit.setCreated(dto.getUpdateTime());
        List<CriteriaUnit> criteria = getRuleCriteriaUnitFromRuleCreationDTO(dto);
        unit.setCriteria(criteria);
        unit.setEnabled(true);
        unit.setEndDate(dto.getEndDate());
        unit.setRuleCode(getRuleCodeForCategoryAndType(dto.getCategoryUrl(), dto.getRuleType()));
        
        unit.setName(ruleName);
        unit.setPriority(0);
        unit.setStartDate(dto.getStartDate());
        unit.setType(dto.getRuleType());
        unit.setUpdated(dto.getUpdateTime());
        unit.setId(RandomUtils.nextInt());
        return unit;
    }
    
    @Transactional
    @Override
    public RuleUpdateResponse updateRule(RuleEditDTO dto, String ruleName) {
        List<Rule> rList = getEnabledRulesForCategoryAndType(dto.getCategoryUrl(), dto.getRuleType());
        List<CocofsRuleUnit> ruleList  =  getEnabledRuleUnitsForCategoryAndType(dto.getCategoryUrl(), dto.getRuleType());
        Rule ruleToEdit = null;
        CocofsRuleUnit ruleUnitToEdit = null;
        if (null != rList) {
            for ( Rule r : rList) {
                if (ruleName.equals(r.getName())) {
                    ruleToEdit = r;
                }
            }
        }
        if (null != ruleList) {
            for (CocofsRuleUnit r : ruleList) {
                if (ruleName.equals(r.getName())) {
                    ruleUnitToEdit = r;
                }
            }
        }
        RuleUpdateResponse resp = new RuleUpdateResponse();
        resp.setSuccessful(false);
        
        if ( (null == ruleUnitToEdit) && (null == ruleToEdit)) {
            String msg = "No enabled rule found with category " + dto.getCategoryUrl() + " and type " + dto.getRuleType() + " and name " + ruleName;
            LOG.info(msg);
            resp.setFailureReason(msg);
        } else if (((null != ruleUnitToEdit) && (null == ruleToEdit)) || ((null == ruleUnitToEdit) && (null != ruleToEdit))) {
            String msg = "Mongo and DB out of sync for category " + dto.getCategoryUrl() + " and type " + dto.getRuleType() + " and name " + ruleName;
            LOG.info(msg);
            resp.setFailureReason(msg);
        } else {
            
            updateRuleUnitWithRuleEditDTO(dto, ruleUnitToEdit);
            updateRuleWithRuleEditDTO(ruleToEdit, dto);
            
            ruleService.updateRule(ruleToEdit);
            genericMao.saveDocument(ruleUnitToEdit);
            resp.setSuccessful(true);
        }
        return resp;
    }

    

    private void updateRuleCriteriaFromRuleEditDTO(RuleEditDTO dto, Set<Criteria> criterion) {
        List<Criteria> criteriaToDelete = new ArrayList<Criteria>();
        List<Criteria> criteriaToAdd = new ArrayList<Criteria>();
        Criteria origCrit = findCriteria(criterion, CocofsBlockParam.FRAGILE.getName());
        if ((null != dto.getFragile() ) && ( null == origCrit) ) {
            Criteria crit = new Criteria();
            crit.setCreated(new Date());
            crit.setEnabled(true);
            crit.setName(CocofsBlockParam.FRAGILE.getName());
            crit.setOperator(CocofsBlockParam.FRAGILE.getOperator());
            crit.setValue(dto.getFragile().toString());
            crit.setValueType(CocofsBlockParam.FRAGILE.getType());
            criteriaToAdd.add(crit);
        } else if ( (null == dto.getFragile() ) && ( null != origCrit))  {
            criterion.remove(origCrit);
            criteriaToDelete.add(origCrit);
        } else if ( (null != dto.getFragile() ) && ( null != origCrit)) {
            origCrit.setValue(dto.getFragile().toString());
            
        }
        
        origCrit = findCriteria(criterion, CocofsBlockParam.LIQUID.getName());
        
        if ((null != dto.getLiquid())&& ( null == origCrit)) {
            Criteria crit = new Criteria();
            crit.setCreated(new Date());
            crit.setEnabled(true);
            crit.setName(CocofsBlockParam.LIQUID.getName());
            crit.setOperator(CocofsBlockParam.LIQUID.getOperator());
            crit.setValue(dto.getLiquid().toString());
            crit.setValueType(CocofsBlockParam.LIQUID.getType());
            criteriaToAdd.add(crit);
        } else if ( (null == dto.getLiquid() ) && ( null != origCrit))  {
            criterion.remove(origCrit);
            criteriaToDelete.add(origCrit);
        } else if ( (null != dto.getLiquid() ) && ( null != origCrit)) {
            origCrit.setValue(dto.getLiquid().toString());
            
        }
        
        origCrit = findCriteria(criterion, CocofsBlockParam.HAZARDOUS.getName());
        if ((null != dto.getHazMat()) && ( null == origCrit)) {
            Criteria crit = new Criteria();
            crit.setCreated(new Date());
            crit.setEnabled(true);
            crit.setName(CocofsBlockParam.HAZARDOUS.getName());
            crit.setOperator(CocofsBlockParam.HAZARDOUS.getOperator());
            crit.setValue(dto.getHazMat().toString());
            crit.setValueType(CocofsBlockParam.HAZARDOUS.getType());
            criteriaToAdd.add(crit);
            
        } else if ( (null == dto.getHazMat() ) && ( null != origCrit))  {
            criterion.remove(origCrit);
            criteriaToDelete.add(origCrit);
        } else if ( (null != dto.getHazMat() ) && ( null != origCrit)) {
            origCrit.setValue(dto.getHazMat().toString());
            
        }
        
        
        origCrit = findCriteria(criterion, CocofsBlockParam.WEIGHT.getName());
        if ((null != dto.getWeight()) && ( null == origCrit)) {
            Criteria crit = new Criteria();
            crit.setCreated(new Date());
            crit.setEnabled(true);
            crit.setName(CocofsBlockParam.WEIGHT.getName());
            crit.setOperator(CocofsBlockParam.WEIGHT.getOperator());
            crit.setValue(dto.getWeight().toString());
            crit.setValueType(CocofsBlockParam.WEIGHT.getType());
            criteriaToAdd.add(crit);

        } else if ( (null == dto.getWeight() ) && ( null != origCrit))  {
            criterion.remove(origCrit);
            criteriaToDelete.add(origCrit);
        } else if ( (null != dto.getWeight() ) && ( null != origCrit)) {
            origCrit.setValue(dto.getWeight().toString());
            
        }
        
        origCrit = findCriteria(criterion, CocofsBlockParam.VOLUMETRIC_WEIGHT.getName());
        if ((null != dto.getVolumetricWeight()) && ( null == origCrit)) {
            Criteria crit = new Criteria();
            crit.setCreated(new Date());
            crit.setEnabled(true);
            crit.setName(CocofsBlockParam.VOLUMETRIC_WEIGHT.getName());
            crit.setOperator(CocofsBlockParam.VOLUMETRIC_WEIGHT.getOperator());
            crit.setValue(dto.getVolumetricWeight().toString());
            crit.setValueType(CocofsBlockParam.VOLUMETRIC_WEIGHT.getType());
            criteriaToAdd.add(crit);

        } else if ( (null == dto.getVolumetricWeight() ) && ( null != origCrit))  {
            criterion.remove(origCrit);
            criteriaToDelete.add(origCrit);
        } else if ( (null != dto.getVolumetricWeight() ) && ( null != origCrit)) {
            origCrit.setValue(dto.getVolumetricWeight().toString());
            
        }
        
        
        
        if ( !criteriaToDelete.isEmpty()) {
          //ruleService.updateRule(criteriaToDelete.get(0).getRule());
            for ( Criteria cr : criteriaToDelete ) {
                ruleService.deleteCriteriaById(cr.getId());
            }
        }
        
        criterion.addAll(criteriaToAdd);
        
        
        
    }

    private void updateRuleCriteriaUnitWithRuleEditDTO(RuleEditDTO dto, List<CriteriaUnit> criteria) {
        CriteriaUnit origCrit = findCriteria(criteria, CocofsBlockParam.FRAGILE.getName());
        
        if ((null != dto.getFragile()) && (null == origCrit)) {

            CriteriaUnit unit = new CriteriaUnit();
            unit.setCreated(dto.getUpdateTime());
            unit.setEnabled(true);
            unit.setName(CocofsBlockParam.FRAGILE.getName());
            unit.setOperator(CocofsBlockParam.FRAGILE.getOperator().getCode());
            unit.setUpdated(dto.getUpdateTime());
            unit.setValue(dto.getFragile().toString());
            unit.setValueType(CocofsBlockParam.FRAGILE.getType().name());
            unit.setId(RandomUtils.nextInt());
            criteria.add(unit);
        } else if ( (null == dto.getFragile() ) && ( null != origCrit))  {
            criteria.remove(origCrit);
        } else if ( (null != dto.getFragile() ) && ( null != origCrit)) {
            origCrit.setValue(dto.getFragile().toString());
            
        }
        
        origCrit = findCriteria(criteria, CocofsBlockParam.LIQUID.getName());
        
        if ((null != dto.getLiquid())&& (null == origCrit)) {
            CriteriaUnit unit = new CriteriaUnit();
            unit.setCreated(dto.getUpdateTime());
            unit.setEnabled(true);
            unit.setName(CocofsBlockParam.LIQUID.getName());
            unit.setOperator(CocofsBlockParam.LIQUID.getOperator().getCode());
            unit.setUpdated(dto.getUpdateTime());
            unit.setValue(dto.getLiquid().toString());
            unit.setValueType(CocofsBlockParam.LIQUID.getType().name());
            unit.setId(RandomUtils.nextInt());
            criteria.add(unit);
        } else if ( (null == dto.getLiquid() ) && ( null != origCrit))  {
            criteria.remove(origCrit);
        } else if ( (null != dto.getLiquid() ) && ( null != origCrit)) {
            origCrit.setValue(dto.getLiquid().toString());
            
        }
        
        origCrit = findCriteria(criteria, CocofsBlockParam.HAZARDOUS.getName());
        if ((null != dto.getHazMat())&& (null == origCrit)) {
            CriteriaUnit unit = new CriteriaUnit();
            unit.setCreated(dto.getUpdateTime());
            unit.setEnabled(true);
            unit.setName(CocofsBlockParam.HAZARDOUS.getName());
            unit.setOperator(CocofsBlockParam.HAZARDOUS.getOperator().getCode());
            unit.setUpdated(dto.getUpdateTime());
            unit.setValue(dto.getHazMat().toString());
            unit.setValueType(CocofsBlockParam.HAZARDOUS.getType().name());
            unit.setId(RandomUtils.nextInt());
            criteria.add(unit);
        } else if ( (null == dto.getHazMat() ) && ( null != origCrit))  {
            criteria.remove(origCrit);
        } else if ( (null != dto.getHazMat() ) && ( null != origCrit)) {
            origCrit.setValue(dto.getHazMat().toString());
            
        }
        
        origCrit = findCriteria(criteria, CocofsBlockParam.WEIGHT.getName());
        if ((null != dto.getWeight())&& (null == origCrit)) {
            CriteriaUnit unit = new CriteriaUnit();
            unit.setCreated(dto.getUpdateTime());
            unit.setEnabled(true);
            unit.setName(CocofsBlockParam.WEIGHT.getName());
            unit.setOperator(CocofsBlockParam.WEIGHT.getOperator().getCode());
            unit.setUpdated(dto.getUpdateTime());
            unit.setValue(dto.getWeight().toString());
            unit.setValueType(CocofsBlockParam.WEIGHT.getType().name());
            unit.setId(RandomUtils.nextInt());
            criteria.add(unit);

        } else if ( (null == dto.getWeight() ) && ( null != origCrit))  {
            criteria.remove(origCrit);
        } else if ( (null != dto.getWeight() ) && ( null != origCrit)) {
            origCrit.setValue(dto.getWeight().toString());
            
        }
        
        origCrit = findCriteria(criteria, CocofsBlockParam.VOLUMETRIC_WEIGHT.getName());
        if ((null != dto.getVolumetricWeight()) && (null == origCrit)) {
            CriteriaUnit unit = new CriteriaUnit();
            unit.setCreated(dto.getUpdateTime());
            unit.setEnabled(true);
            unit.setName(CocofsBlockParam.VOLUMETRIC_WEIGHT.getName());
            unit.setOperator(CocofsBlockParam.VOLUMETRIC_WEIGHT.getOperator().getCode());
            unit.setUpdated(dto.getUpdateTime());
            unit.setValue(dto.getVolumetricWeight().toString());
            unit.setValueType(CocofsBlockParam.VOLUMETRIC_WEIGHT.getType().name());
            unit.setId(RandomUtils.nextInt());
            criteria.add(unit);

        } else if ( (null == dto.getVolumetricWeight() ) && ( null != origCrit))  {
            criteria.remove(origCrit);
        } else if ( (null != dto.getVolumetricWeight() ) && ( null != origCrit)) {
            origCrit.setValue(dto.getVolumetricWeight().toString());
            
        }
        
    }

    private void updateRuleUnitWithRuleEditDTO(RuleEditDTO dto, CocofsRuleUnit unit) {
        if ( dto.getEnabled()) {
            updateRuleCriteriaUnitWithRuleEditDTO(dto, unit.getCriteria());
        }
        
        
        unit.setEnabled(dto.getEnabled());
        unit.setEndDate(dto.getEndDate());
        unit.setStartDate(dto.getStartDate());
        unit.setUpdated(dto.getUpdateTime());
        
    }
    
    private void updateRuleWithRuleEditDTO(Rule r, RuleEditDTO dto ) {

        if (dto.getEnabled()) {
            // Only update criteria only if rule is not being disabled
            updateRuleCriteriaFromRuleEditDTO(dto, r.getCriterion());
            for (Criteria rc : r.getCriterion()) {
                if (null == rc.getRule()) {
                    rc.setRule(r);
                }
            }
        }
        r.setEnabled(dto.getEnabled());
        r.setEndDate(dto.getEndDate());
        r.setPriority(0);
        r.setRemark("Updating rule for " + dto.getRuleType() + " and " + dto.getCategoryUrl() + " by user " + dto.getUpdatedBy());
        r.setStartDate(dto.getStartDate());
        r.setUpdated(dto.getUpdateTime());
        r.setUpdatedBy(dto.getUpdatedBy());
        
    }

    @Override
    public RuleEditDTO getRuleEditDTOFromRule(Rule rule) {
        RuleEditDTO dto = new RuleEditDTO();
        for ( RuleParams p : rule.getRuleParams()) {
            if ( p.getParamName().equals(CocofsRuleParam.CATEGORY_URL.getName())) {
                dto.setCategoryUrl(p.getParamValue());
            }
        }
        dto.setName(rule.getName());
        dto.setEnabled(rule.isEnabled());
        
        Date d = rule.getEndDate();
        if (null != d) {
            Calendar cal = new GregorianCalendar();
            cal.setTime(d);
            cal.add(Calendar.DATE, -1);
            dto.setEndDate(cal.getTime());
        } else {
            dto.setEndDate(null);
        }
        Criteria crit = findCriteria(rule.getCriterion(), CocofsBlockParam.FRAGILE.getName());
        if ( null != crit ) {
            dto.setFragile(Boolean.valueOf(crit.getValue()));
        }
        crit = findCriteria(rule.getCriterion(), CocofsBlockParam.HAZARDOUS.getName());
        if ( null != crit ) {
            dto.setHazMat(Boolean.valueOf(crit.getValue()));
        }
        crit = findCriteria(rule.getCriterion(), CocofsBlockParam.LIQUID.getName());
        if ( null != crit ) {
            dto.setLiquid(Boolean.valueOf(crit.getValue()));
        }
        crit = findCriteria(rule.getCriterion(), CocofsBlockParam.WEIGHT.getName());
        if ( null != crit ) {
            dto.setWeight(Double.valueOf(crit.getValue()));
        }
        crit = findCriteria(rule.getCriterion(), CocofsBlockParam.VOLUMETRIC_WEIGHT.getName());
        if ( null != crit ) {
            dto.setVolumetricWeight(Double.valueOf(crit.getValue()));
        }
        
        dto.setRuleType(CacheManager.getInstance().getCache(BlocksCache.class).getBlockById(rule.getBlockId()).getName());
        dto.setStartDate(rule.getStartDate());
        dto.setUpdatedBy(rule.getUpdatedBy());
        dto.setUpdateTime(rule.getUpdated());
        
        return dto;
    }
    
    

}
