package com.snapdeal.cocofs.services.lookup.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.cocofs.access.IPendingProductAttributeDBDataReadService;
import com.snapdeal.cocofs.access.IProductAttributeService;
import com.snapdeal.cocofs.entity.PendingProductAttributeUpdate;
import com.snapdeal.cocofs.mongo.model.ProductAttributeUnit;
import com.snapdeal.cocofs.services.IProductAttributeLookupService;
import com.snapdeal.cocofs.services.common.dto.ProductAttributeDownloadData;
import com.snapdeal.cocofs.services.converter.IConverterService;

/**
 * @author nikhil
 */

@Service("productAttributeLookupService")
public class ProductAttributeLookupServiceImpl implements IProductAttributeLookupService {

    @Autowired
    IPendingProductAttributeDBDataReadService ppaDataReadService;

    @Autowired
    IProductAttributeService                  productAttributeService;

    @Autowired
    IConverterService                         converterService;

    @Override
    public ProductAttributeDownloadData getProductAttributeDownloadData(String supc) {
        if (StringUtils.isEmpty(supc)) {
            throw new IllegalArgumentException("Input parameter supc is found to be null");
        }

        ProductAttributeUnit unit = productAttributeService.getProductAttributeUnitBySUPC(supc);

        if (unit != null) {
            List<PendingProductAttributeUpdate> pendingUpdates = ppaDataReadService.getPendingProductAtributeUpdate(supc);
            return converterService.getProductAttributeDownloadData(pendingUpdates, unit);
        } else {
            return null;
        }
    }

}
