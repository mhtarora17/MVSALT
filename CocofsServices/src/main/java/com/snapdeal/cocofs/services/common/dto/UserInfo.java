/**
 * 
 */
package com.snapdeal.cocofs.services.common.dto;

/**
 * @author abhinav
 */
public class UserInfo {

    private String  email;

    private boolean isSuper;

    public UserInfo() {
    }

    public UserInfo(String email, boolean isSuper) {
        super();
        this.email = email;
        this.isSuper = isSuper;
    }

    public boolean isSuper() {
        return isSuper;
    }

    public void setSuper(boolean isSuper) {
        this.isSuper = isSuper;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
