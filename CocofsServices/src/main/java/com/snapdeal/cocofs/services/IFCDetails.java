/*

 *  Copyright 2012 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, May 3, 2012
 *  @author amit
 */
package com.snapdeal.cocofs.services;

import com.snapdeal.cocofs.entity.FCAddress;


/**
 *  interface to add in fc_details table
 * @author indrajit
 *
 */

public interface IFCDetails {

    public void saveFCDetails(FCAddress centerLocation);

    public FCAddress persistFCDetails(FCAddress fcDetail);

    

}

