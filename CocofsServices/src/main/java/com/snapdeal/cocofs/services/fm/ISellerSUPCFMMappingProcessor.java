package com.snapdeal.cocofs.services.fm;

import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.generic.persister.exception.GenericPersisterException;
import com.snapdeal.cocofs.services.events.dto.SellerFMFCUpdateEventObj;
import com.snapdeal.cocofs.services.events.dto.SellerSupcAddEventObj;
import com.snapdeal.cocofs.services.events.dto.SellerSupcFMFCExceptionAddEventObj;
import com.snapdeal.cocofs.services.events.dto.SellerSupcFMFCExceptionDeleteEventObj;
import com.snapdeal.score.data.aerospike.exception.GetFailedException;
import com.snapdeal.score.data.aerospike.exception.PutFailedException;

/**
 * Interface to handle seller/supc-fm/fc update event/notification for seller-supc-fm mapping
 * 
 * @author ssuthar
 */
public interface ISellerSUPCFMMappingProcessor {

    /**
     * API to process the sellercode, SUPC code tuple and find if any notification is to be send for the tuple. All such
     * notifications are sent further by this API.
     * 
     * @param event
     * @return false if the push is not successful and possibly need to re-attempted by client
     * @throws GetFailedException
     * @throws ExternalDataNotFoundException
     * @throws PutFailedException
     * @throws GenericPersisterException
     */
    public boolean processAdditionOfNewSellerSUPCMappingForExternalNotification(SellerSupcAddEventObj event) throws GetFailedException, ExternalDataNotFoundException,
            PutFailedException, GenericPersisterException;

    /**
     * API to process the sellercode, SUPC code, new FM mapping, existing FM mapping, newFCCenters and existingFCCenters
     * tuple and find if any notification is to be send for the tuple. All such notifications are sent further by this
     * API.
     * 
     * @param event - consisting of sellerCode, supc, newFM, existingFM, newFCCenters, existingFCCenters
     * @return false if the push is not successful and possibly need to re-attempted by client
     */
    public boolean processChangeOfSellerSUPCFMMappingExceptionForExternalNotification(SellerSupcFMFCExceptionAddEventObj event) throws Exception;

    /**
     * API to process the sellercode, SUPC code, FM mapping and existingFCCenters tuple on disabling the exception list
     * and find if any notification is to be send for the tuple. All such notifications are sent further by this API.
     * 
     * @param event - consisting of sellerCode, supc, fulfillmentModel, existingFCCenters
     * @return false if the push is not successful and possibly need to re-attempted by client
     */
    public boolean processDisablingOfSellerSUPCFMMappingExceptionForExternalNotification(SellerSupcFMFCExceptionDeleteEventObj event);

    /**
     * API to process the sellercode, newFM, oldFM, newFCCenters and existingFCCenters tuple and find if any
     * notification is to be send for the tuple. All such notifications are sent further by this API.
     * 
     * @param event - consist of sellerCode,newFulfilmentModel,existingFulfilmentModel,newFCCenters,existingFCCenters
     * @return false if processing the change of seller-fm mapping was not successful, used to trigger event driven
     *         handling (possibly need to re-attempted by client)
     */
    public boolean processChangeOfSellerFMMappingForExternalNotification(SellerFMFCUpdateEventObj event);
}
