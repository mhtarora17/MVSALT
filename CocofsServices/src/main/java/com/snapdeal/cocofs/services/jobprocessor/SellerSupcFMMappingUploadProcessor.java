/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 21-Oct-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.services.jobprocessor;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.annotation.PostConstruct;
import javax.naming.OperationNotSupportedException;

import org.hsqldb.lib.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.snapdeal.base.utils.CollectionUtils;
import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.cocofs.common.BulkUploadValidationDTO;
import com.snapdeal.cocofs.common.BulkUploadValidationResponse;
import com.snapdeal.cocofs.common.JobProcessorResponse;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.core.aerospike.vo.SellerFMMappingVO;
import com.snapdeal.cocofs.core.aerospike.vo.SellerSupcFMMappingVO;
import com.snapdeal.cocofs.dataaccess.fm.IFMAerospikeDataReadService;
import com.snapdeal.cocofs.dataaccess.fm.IFMDBDataReadService;
import com.snapdeal.cocofs.entity.FulfillmentCentre;
import com.snapdeal.cocofs.entity.JobDetail;
import com.snapdeal.cocofs.entity.SellerSupcFMMapping;
import com.snapdeal.cocofs.enums.BulkUploadValidationError;
import com.snapdeal.cocofs.enums.JobActionCode;
import com.snapdeal.cocofs.generic.persister.exception.GenericPersisterException;
import com.snapdeal.cocofs.generic.persister.response.GenericPersisterWithAerospikeResponse;
import com.snapdeal.cocofs.services.IJobProcessor;
import com.snapdeal.cocofs.services.IJobSchedulerService;
import com.snapdeal.cocofs.services.data.engine.IDataEngineWithAerospike;
import com.snapdeal.cocofs.services.events.dto.SellerSupcFMFCExceptionAddEventObj;
import com.snapdeal.cocofs.services.events.dto.SellerSupcFMFCExceptionDeleteEventObj;
import com.snapdeal.cocofs.services.fm.IFMMappingDataUpdater;
import com.snapdeal.cocofs.services.fm.IFulfillmentCenterService;
import com.snapdeal.cocofs.services.fm.IFulfilmentModelService;
import com.snapdeal.cocofs.services.fm.ISellerSUPCFMMappingProcessor;
import com.snapdeal.cocofs.services.fm.ISellerToSUPCListDBLookupService;
import com.snapdeal.cocofs.services.fm.dto.SellerSupcFMMappingUploadDto;
import com.snapdeal.cocofs.sro.FulfillmentModel;
import com.snapdeal.cocofs.utils.AerospikeKeyHelper;
import com.snapdeal.cocofs.utils.ExcelReader;
import com.snapdeal.cocofs.utils.FTPUtils;
import com.sun.media.sound.InvalidDataException;

@Service("sellerSupcFMMappingUploadProcessor")
public class SellerSupcFMMappingUploadProcessor implements IJobProcessor {

    private static final Logger                                                                                LOG = LoggerFactory.getLogger(SellerSupcFMMappingUploadProcessor.class);
    @Autowired
    private IJobSchedulerService                                                                               jobService;

    @Autowired
    private IFMMappingDataUpdater                                                                              dataUpdater;

    @Autowired
    private IFMDBDataReadService                                                                               fmDBDataReadService;

    @Autowired
    private IFMAerospikeDataReadService                                                                        fmAerospikeService;

    @Autowired
    @Qualifier("failSafeSellerSUPCFMNotificationSendor")
    private ISellerSUPCFMMappingProcessor                                                                      sellerSUPCFMMappingProcessor;

    @Autowired
    private IFulfilmentModelService                                                                            fulfilmentService;

    @Autowired
    @Qualifier("SellerSupcFMMappingDataEngine")
    private IDataEngineWithAerospike<SellerSupcFMMappingUploadDto, SellerSupcFMMapping, SellerSupcFMMappingVO> dataEngine;

    @Autowired
    private ISellerToSUPCListDBLookupService                                                                   sellerToSUPCListLookupService;

    @Autowired
    private IFulfillmentCenterService                                                                          fulfillmentCenterService;

    @Autowired
    private com.snapdeal.cocofs.services.converter.IConverterService                                           convertorService;

    private IDependency                                                                                        dep = new Dependency();

    public enum SELLER_SUPC_VALIDATION_MAPPING {
        DISABLED, ON_UPLOAD, ON_PROCESS, ALWAYS;
    }

    @PostConstruct
    public void init() {
        jobService.addJobProcessor(JobActionCode.SELLER_SUPC_FM_MAPPING.getCode(), this);
    }

    @Override
    public JobProcessorResponse process(JobDetail jobDetail) {
        JobProcessorResponse response = new JobProcessorResponse();
        LOG.info("processing file:" + jobDetail.getFileName() + " from path:" + jobDetail.getFilePath() + " status:" + jobDetail.getStatus().getCode());
        String ftpServerPath = dep.getStringPropertyValue(Property.COCOFS_FTP_SERVER_PATH);
        String ftpUsername = dep.getStringPropertyValue(Property.COCOFS_FTP_USERNAME);
        String ftpPassword = dep.getStringPropertyValue(Property.COCOFS_FTP_PASSWORD);
        String filepath = jobDetail.getFilePath() + File.separator + jobDetail.getFileName();

        // get enable/disable param from jobdetail
        int enabled = Integer.parseInt(jobDetail.getRemarks());

        // We save file on identical paths on local FS and FTP
        Map<String, List<String>> errorMap = new HashMap<String, List<String>>(0);
        boolean success = false;
        try {
            dep.downloadFile(ftpServerPath, ftpUsername, ftpPassword, filepath, filepath);
            List<SellerSupcFMMappingUploadDto> dtoList = dep.readFile(filepath, SellerSupcFMMappingUploadDto.class, true);

            errorMap = processData(dtoList, errorMap, jobDetail.getUploadedBy() + ";" + jobDetail.getFileCode(), enabled);
            success = true;
        } catch (InvalidDataException e) {
            updateErrorMapForFileError(jobDetail, errorMap, e);
            LOG.error("Exception ", e);
        } catch (IOException e) {
            updateErrorMapForFileError(jobDetail, errorMap, e);
            LOG.error("Exception ", e);
        } catch (ClassNotFoundException e) {
            updateErrorMapForFileError(jobDetail, errorMap, e);
            LOG.error("Exception ", e);
        } catch (Exception e) {
            updateErrorMapForFileError(jobDetail, errorMap, e);
            LOG.error("Exception ", e);
        }
        response.setError(errorMap);
        if ((errorMap != null && errorMap.size() > 0) || !success) {
            response.setProcessingSuccessful(false);
        } else {
            response.setProcessingSuccessful(true);
        }
        return response;

    }

    private void updateErrorMapForFileError(JobDetail jobDetail, Map<String, List<String>> errorMap, Exception e) {
        if (!errorMap.containsKey("FileError")) {
            errorMap.put("FileError", new ArrayList<String>());
        }
        errorMap.get("FileError").add("Error in reading file for job " + jobDetail.getFileCode() + " Error Message " + e.getMessage());
    }

    private Map<String, List<String>> processData(List<SellerSupcFMMappingUploadDto> dtoList, Map<String, List<String>> errorMap, String updatedBy, int enabled) {

        Map<String, Set<String>> mapSellerSUPCMapping = new HashMap<String, Set<String>>();
        boolean bValidateSellerSUPCOnProcess = isValidateSellerSUPCOnProcess();

        int row = 0;
        int total = dtoList.size();
        for (SellerSupcFMMappingUploadDto dto : dtoList) {
            row++;
            long tStart = System.currentTimeMillis();
            LOG.info("processing started for supc {} at row: {} of total: {};", dto.getSupc(), row, total);
            processDataForSeller(dto, errorMap, updatedBy, enabled, mapSellerSUPCMapping, bValidateSellerSUPCOnProcess);
            LOG.info("processing done for supc {} at row: {} of total: {}; took {} ms", dto.getSupc(), row, total, (System.currentTimeMillis() - tStart));
        }

        return errorMap;
    }

    private SellerSupcFMMappingVO getSellerSupcFMMappingRecord(String sellerCode,String supc) {

        if (StringUtil.isEmpty(supc) ||  StringUtil.isEmpty(sellerCode)) {
            return null;
        }

        SellerSupcFMMappingVO sellerSupcFMMappingVO = (SellerSupcFMMappingVO) fmAerospikeService.getFMMapping(AerospikeKeyHelper.getKey(sellerCode, supc),
                SellerSupcFMMappingVO.class);
        if (sellerSupcFMMappingVO != null) {
            return sellerSupcFMMappingVO;
        } else {
            LOG.error("no aerosplike record found for seller: " + sellerCode + " and Supc: " + supc);
            return null;
        }

    }
    

    private SellerFMMappingVO getSellerFMMappingRecord(String sellerCode) {

       
        SellerFMMappingVO sellerFMMappingVO = (SellerFMMappingVO) fmAerospikeService.getFMMapping(sellerCode,
                SellerFMMappingVO.class);
        if (sellerFMMappingVO != null) {
            return sellerFMMappingVO;
        } else {
            LOG.error("no aerosplike record found for seller: " + sellerCode );
            return null;
        }

    }

    private void processDataForSeller(SellerSupcFMMappingUploadDto uploadDto, Map<String, List<String>> errorMap, String updatedBy, int enabled,
            Map<String, Set<String>> mapSellerSUPCMapping, boolean bValidateSellerSUPCOnProcess) {

        uploadDto.setEnabled(enabled == 1);
        uploadDto.setLastUpdated(DateUtils.getCurrentTime());

        SellerSupcFMMapping fmEntity = fmDBDataReadService.getSellerSupcFMMapping(uploadDto.getSellerCode(), uploadDto.getSupc());

        SellerSupcFMMappingVO sellersupcFmRecord = null;
        if(fmEntity!= null){
            LOG.info("reading from aerospike: {}", uploadDto);
            sellersupcFmRecord = getSellerSupcFMMappingRecord(uploadDto.getSellerCode(),uploadDto.getSupc());
        }

        String existingFM =  (sellersupcFmRecord == null ? null : (!sellersupcFmRecord.isEnabled() ? null : sellersupcFmRecord.getFulfillmentModel()));
        List<String> existingFCCenters = (sellersupcFmRecord == null || !sellersupcFmRecord.isEnabled() ? null : sellersupcFmRecord.getFcCenters());;
        
            
        String newFM = uploadDto.getFulfilmentModel();
       
        try {

            if (bValidateSellerSUPCOnProcess) {
                String sellerCode = uploadDto.getSellerCode();
                String supcCode = uploadDto.getSupc();

                boolean bValidSellerSUPCMappingForDTO = isSellerSUPCMappingValid(sellerCode, supcCode, mapSellerSUPCMapping);

                if (!bValidSellerSUPCMappingForDTO) {
                    LOG.info("isSellerSUPCMappingValid returning {}, for seller:{}, supc:{}", bValidSellerSUPCMappingForDTO, sellerCode, supcCode);
                    // have a flag to identify the issue
                    updateErrorMapForRowError(uploadDto, errorMap, BulkUploadValidationError.SELLER_SUPC_MAPPING_NON_EXIST.getDescription());
                    return;
                }
            }

            List<SellerSupcFMMapping> toBeUpdatedFMEntityList = (fmEntity == null) ? null : Arrays.asList(fmEntity);
            List<SellerSupcFMMappingVO> toBeUpdatedFMRecordList = (sellersupcFmRecord == null) ? null : Arrays.asList(sellersupcFmRecord);

            toBeUpdatedFMEntityList = dataEngine.enrichEntities(uploadDto, toBeUpdatedFMEntityList, updatedBy);

            toBeUpdatedFMRecordList = dataEngine.enrichRecords(uploadDto, toBeUpdatedFMRecordList, updatedBy);

            // Override enablement statuses
            for (SellerSupcFMMapping supcMapping : toBeUpdatedFMEntityList) {
                supcMapping.setEnabled(enabled == 1);
            }
            for (SellerSupcFMMappingVO supcMappingVo : toBeUpdatedFMRecordList) {
                supcMappingVo.setEnabled(enabled);
            }

            GenericPersisterWithAerospikeResponse<SellerSupcFMMapping, SellerSupcFMMappingVO> resp = dataUpdater.updateSellerSupcFMMapping(toBeUpdatedFMEntityList,
                    toBeUpdatedFMRecordList);

            if (!resp.isSuccessful()) {
                updateErrorMapForRowError(uploadDto, errorMap, resp.getMessage());
            } else {

                List<String> newFCCenters = getFCCentreListFromDTO(uploadDto.getFcCenters());

                // process the data for search push
                if (enabled == 1) {
                    sellerSUPCFMMappingProcessor.processChangeOfSellerSUPCFMMappingExceptionForExternalNotification(new SellerSupcFMFCExceptionAddEventObj(
                            uploadDto.getSellerCode(), uploadDto.getSupc(), newFM, existingFM, newFCCenters, existingFCCenters));
                } else {
                    sellerSUPCFMMappingProcessor.processDisablingOfSellerSUPCFMMappingExceptionForExternalNotification(new SellerSupcFMFCExceptionDeleteEventObj(
                            uploadDto.getSellerCode(), uploadDto.getSupc(), existingFM, existingFCCenters));
                }
            }

        } catch (OperationNotSupportedException e) {
            LOG.error("Exception ", e);
        } catch (GenericPersisterException e) {
            LOG.error("Exception ", e);
        } catch (Exception e) {
            LOG.error("Error", e);
        }

    }

    private Map<String, List<SellerSupcFMMappingUploadDto>> getSellerToDto(List<SellerSupcFMMappingUploadDto> dtoList) {
        Map<String, List<SellerSupcFMMappingUploadDto>> sellerToSupcToDtoMap = new HashMap<String, List<SellerSupcFMMappingUploadDto>>();
        if (!CollectionUtils.isEmpty(dtoList)) {
            for (SellerSupcFMMappingUploadDto dto : dtoList) {
                if (!sellerToSupcToDtoMap.containsKey(dto.getSellerCode())) {
                    sellerToSupcToDtoMap.put(dto.getSellerCode(), new ArrayList<SellerSupcFMMappingUploadDto>());
                }
                sellerToSupcToDtoMap.get(dto.getSellerCode()).add(dto);
            }
        }
        return sellerToSupcToDtoMap;
    }

    private void updateErrorMapForRowError(SellerSupcFMMappingUploadDto dto, Map<String, List<String>> errorMap, String message) {
        if (!errorMap.containsKey("RowError")) {
            errorMap.put("RowError", new ArrayList<String>());
        }
        errorMap.get("RowError").add("Error in row with SUPC : " + dto.getSupc() + " and seller : " + dto.getSellerCode() + " Error : " + message);
    }

    @Override    public BulkUploadValidationResponse validateJob(File file, String actionCode, String param) {
        BulkUploadValidationResponse response = new BulkUploadValidationResponse();
        List<BulkUploadValidationDTO> validationDTOList = new ArrayList<BulkUploadValidationDTO>();
        List<SellerSupcFMMappingUploadDto> dtoList = null;
        boolean success = false;
        boolean bValidSellerCode = true;
        boolean bValidSellerSUPCMapping = true;
        boolean bValidFCCentreMapping = true;
        boolean bValidSellerFulfilmentModel = true;
        boolean bValidSellerSupcFMMapping =true;
        try {
            int enabled = Integer.parseInt(param);

            dtoList = dep.readFile(file.getAbsolutePath(), SellerSupcFMMappingUploadDto.class, true);
            int rowNo = 1;
            Set<SellerSupcFMMappingUploadDto> dtoSet = new HashSet<SellerSupcFMMappingUploadDto>(dtoList);

            LOG.info("Uploaded records count - {}", dtoList.size());

            if (dtoList.size() != dtoSet.size()) {
                LOG.info("List size dtoList.size() {} set size {}", dtoList.size(), dtoSet.size());
                response.setErrorMessage("Duplicate seller-supc combination exists in the file. Total records: " + dtoList.size() + ", unique records: " + dtoSet.size());
                response.setValid(false);
                return response;
            }

            Map<String, Boolean> map = new HashMap<String, Boolean>();
            Map<String, Set<String>> mapSellerSUPCMapping = new HashMap<String, Set<String>>();
            Map<String, FulfillmentCentre> fcCodeFCCenterMapping = new HashMap<String, FulfillmentCentre>();
            boolean bValidateSellerSUPCEagerOnUpload = isValidateSellerSUPCEagerOnUpload();

            for (SellerSupcFMMappingUploadDto dto : dtoList) {
                rowNo++;
                FulfillmentModel fm = FulfillmentModel.getFulfilmentModelByCode(dto.getFulfilmentModel());
                if (null == fm) {
                    bValidSellerFulfilmentModel = false;
                    validationDTOList.add(new BulkUploadValidationDTO(rowNo, BulkUploadValidationError.INVALID_FUFILMENT_MODEL.getDescription(), dto.getFulfilmentModel()));
                    // no need to validate seller-supc when seller code is not
                    // correct.
                    continue;
                }

                // find where the sellercode exists (seller to fm mapping
                // exists), only allow the record to be inserted,

                // get from local map to reduce aerospike hit for same seller
                String sellerCode = dto.getSellerCode();
                String supcCode = dto.getSupc();
                // use local flag as it may be reset

                if (!isValidSellerSpecified(sellerCode, map)) {
                    LOG.info("row {} isValidSellerSpecified is false for sellerCode:{}", rowNo, sellerCode);
                    // have a flag to identify the issue
                    bValidSellerCode = false;
                    validationDTOList.add(new BulkUploadValidationDTO(rowNo, BulkUploadValidationError.SELLER_NON_EXIST.getDescription(), sellerCode));
                } else if (supcCode == null || supcCode.trim().isEmpty()) {
                    LOG.info(rowNo + " supc is missing validation is false for seller:{}, supc:{}", sellerCode, supcCode);
                    // have a flag to identify the issue
                    bValidSellerSUPCMapping = false;
                    validationDTOList.add(new BulkUploadValidationDTO(rowNo, BulkUploadValidationError.SUPC_MISSING.getDescription(), supcCode));
                } else if (bValidateSellerSUPCEagerOnUpload && !isSellerSUPCMappingValid(sellerCode, supcCode, mapSellerSUPCMapping)) {
                    LOG.info(rowNo + " isSellerSUPCMappingValid returning false, for seller:{}, supc:{}", sellerCode, supcCode);
                    // have a flag to identify the issue
                    bValidSellerSUPCMapping = false;
                    validationDTOList.add(new BulkUploadValidationDTO(rowNo, BulkUploadValidationError.SELLER_SUPC_MAPPING_NON_EXIST.getDescription(), sellerCode + "-" + supcCode));
                }else if(enabled ==0){
                   
                    SellerSupcFMMappingVO sellersupcFmRecord = getSellerSupcFMMappingRecord(dto.getSellerCode(),dto.getSupc());
                    if(sellersupcFmRecord == null || !sellersupcFmRecord.isEnabled()){
                        LOG.info(rowNo + "no enable sellersupc-FM-Mapping  exist for seller:{}, supc:{}", sellerCode, supcCode);
                        // have a flag to identify the issue
                        bValidSellerSupcFMMapping = false;
                        validationDTOList.add(new BulkUploadValidationDTO(rowNo, BulkUploadValidationError.INVALID_SELLER_SUPC_FM_MAPPING_EXIST.getDescription(), sellerCode + "-" + supcCode));
                  
                    }
                } else if (!isSellerSupcFMFCUpdatePushEnabled()) {
                    if (StringUtils.isNotEmpty(dto.getFcCenters())) {
                        LOG.info(rowNo + " not a valid tuple for seller:{}, supc:{}, fm {}, fcCentres:{}", sellerCode, supcCode, fm, dto.getFcCenters());
                        bValidFCCentreMapping = false;
                        validationDTOList.add(new BulkUploadValidationDTO(rowNo, BulkUploadValidationError.FM_FC_MAPPING_NOT_VALID.getDescription(), sellerCode + "-" + supcCode
                                + "-" + fm + ":" + dto.getFcCenters() + " not allowed."));
                    }
                } else if (StringUtils.isNotEmpty(dto.getFcCenters())) {
                    // for non FC_VOI and ONESHIP model and OCPLUS, FC center
                    // must not be specified
                    if (!validateFulFillmentModel(fm)) {
                        LOG.info(rowNo + " not a valid tuple for seller:{}, supc:{}, fm {}, fcCentres:{}", sellerCode, supcCode, fm, dto.getFcCenters());
                        bValidFCCentreMapping = false;
                        validationDTOList.add(new BulkUploadValidationDTO(rowNo, BulkUploadValidationError.FM_FC_MAPPING_NOT_VALID.getDescription(), sellerCode + "-" + supcCode
                                + "-" + fm + ":" + dto.getFcCenters()));
                    } else {
                        BulkUploadValidationError err = checkFCCenterMappingError(sellerCode, fm, getFCCentreListFromDTO(dto.getFcCenters()), fcCodeFCCenterMapping);
                        if (err != null) {
                            // for FC_VOI and ONESHIP and OCPLUS model, FC
                            // center must be valid specified
                            LOG.info(rowNo + " checkFCCenterMappingError returning error, for seller:{}, supc:{}, fcCentres:{}", sellerCode, supcCode, dto.getFcCenters());
                            bValidFCCentreMapping = false;
                            validationDTOList.add(new BulkUploadValidationDTO(rowNo, err.getDescription(), sellerCode + "-" + supcCode + "-" + fm + ":" + dto.getFcCenters()));
                        }
                    }
                } else if (validateFulFillmentModel(fm)) {
                    // for FC_VOI and ONESHIP and OCPLUS model, FC center must
                    // be specified
                    LOG.info(rowNo + " not a valid tuple for seller:{}, supc:{}, fm {}, fcCentres:{}", sellerCode, supcCode, fm, dto.getFcCenters());
                    bValidFCCentreMapping = false;
                    validationDTOList.add(new BulkUploadValidationDTO(rowNo, BulkUploadValidationError.FM_FC_MAPPING_NOT_VALID.getDescription(), sellerCode + "-" + supcCode + "-"
                            + fm + ":" + dto.getFcCenters()));
                }
            }
            success = true;
        } catch (InvalidDataException e) {
            response.setErrorMessage(BulkUploadValidationError.REQUIRED_FIELDS_MISSING.getDescription() + ". " + e.getMessage());
            LOG.error("Error while uploading the data for file {} for action type {} ", file.getAbsoluteFile(), actionCode, e);
        } catch (IOException e) {
            response.setErrorMessage(BulkUploadValidationError.UPLOAD_FILE_READ_ERROR.getDescription());
            LOG.error("Error while uploading the data for file {} for action type {} ", file.getAbsoluteFile(), actionCode, e);
        } catch (ClassNotFoundException e) {
            response.setErrorMessage(BulkUploadValidationError.FILE_TYPE_NOT_CORRECT.getDescription());
            LOG.error("Error while uploading the data for file {} for action type {} ", file.getAbsoluteFile(), actionCode, e);
        }
        response.setValid(success && validationDTOList.isEmpty());

        if (!validationDTOList.isEmpty()) {
            response.setList(validationDTOList);
            String errorMsg = "";

            int errCnt = 1;
            if (!bValidSellerFulfilmentModel) {
                errorMsg += (errCnt++) + ". " + "Fulfillment model incorrect for one or more rows. ";
            }
            if (!bValidSellerCode) {
                errorMsg += (errCnt++) + ". " + "Non existent seller specified in one or more rows. ";
            }
            if (!bValidSellerSUPCMapping) {
                errorMsg += (errCnt++) + ". " + "Non existent seller-supc pair specified in one or more rows. ";
            }
            if (!bValidFCCentreMapping) {
                errorMsg += (errCnt++) + ". " + "Invalid or non existent fulfillment center mapping specified in one or more rows. ";
            }
            if(!bValidSellerSupcFMMapping){
                errorMsg += (errCnt++) + ". " + "No enable sellersupc-FM-Mapping  exist for one or more rows, hence cannot mark them disable ";
            }

            response.setErrorMessage(errorMsg);

        }

        return response;
    }

    private boolean validateFulFillmentModel(FulfillmentModel fm) {
        return fm == FulfillmentModel.FC_VOI || fm == FulfillmentModel.ONESHIP || fm == FulfillmentModel.OCPLUS;
    }

    private boolean isValidSellerSpecified(String sellerCode, Map<String, Boolean> map) {
        boolean bValidSellerCodeForDTO = true;
        if (!map.containsKey(sellerCode)) {
            bValidSellerCodeForDTO = isSellerExists(sellerCode);
            map.put(sellerCode, bValidSellerCodeForDTO);
        } else {
            bValidSellerCodeForDTO = map.get(sellerCode);
        }
        return bValidSellerCodeForDTO;
    }

    private boolean isSellerSUPCMappingValid(String sellerCode, String supcCode, Map<String, Set<String>> mapSellerSUPCMapping) {

        boolean bResult = false;
        Set<String> supcSet = null;
        if (!mapSellerSUPCMapping.containsKey(sellerCode)) {
            supcSet = getSetOfSUPCsForSpecifiedSeller(sellerCode);
            mapSellerSUPCMapping.put(sellerCode, supcSet);
        } else {
            supcSet = mapSellerSUPCMapping.get(sellerCode);
        }
        if (supcSet != null && !supcSet.isEmpty()) {
            if (supcSet.contains(supcCode)) {
                bResult = true;
            }
        }

        return bResult;
    }

    private List<String> getFCCentreListFromDTO(String fcCenterString) {
        // put it into method so that we can put conversion logic here, if
        // needed.
        return convertorService.convertToFCCentreList(fcCenterString);
    }

    private BulkUploadValidationError checkFCCenterMappingError(String sellerCode, FulfillmentModel fm, List<String> fcCentres, Map<String, FulfillmentCentre> fcCodeFCCenterMapping) {

        if (CollectionUtils.isEmpty(fcCentres)) {
            LOG.info("fcCentres {} for seller {} is null/empty, returning false", fcCentres, sellerCode);
            return BulkUploadValidationError.FC_NON_EXIST;
        }

        for (String fcCode : fcCentres) {
            if (fcCode != null && !fcCode.isEmpty()) {
                FulfillmentCentre fcCenter = null;
                if (!fcCodeFCCenterMapping.containsKey(fcCode)) {
                    fcCenter = fulfillmentCenterService.getFulfillmentCentreByCode(fcCode);
                    fcCodeFCCenterMapping.put(fcCode, fcCenter);
                } else {
                    fcCenter = fcCodeFCCenterMapping.get(fcCode);
                }

                LOG.info("fcCenter {} for fcCode {}, seller {}, fm {}, fcCenters {} ", fcCenter, fcCode, sellerCode, fm, fcCentres);
                if (fcCenter == null) {
                    LOG.info("fcCCode {} is not valid, seller {} , fm {}, fcCenters {} ", fcCode, sellerCode, fm, fcCentres);
                    return BulkUploadValidationError.FC_NON_EXIST;
                } else if (!fcCenter.getCode().equals(fcCode)) {
                    LOG.info("fcCCode {} does not match the case of fcCenter code {} , seller {} , fm {}, fcCenters {} ", fcCode, fcCenter, sellerCode, fm, fcCentres);
                    return BulkUploadValidationError.FC_INCORRECT_CASE;
                } else if (isFMToFCMappingValid(fm, fcCenter)) {
                    LOG.info("fcCCode {} is not valid against the FM, seller {} , fm {}, fcCenters {} ", fcCode, sellerCode, fm, fcCentres);
                    return BulkUploadValidationError.FM_FC_MAPPING_NOT_VALID;
                }
            }
        }
        return null;
    }

    private boolean isFMToFCMappingValid(FulfillmentModel fm, FulfillmentCentre fcCenter) {
        // FIXME - get the FC_VOI centers from an enum or property value
        // FC_VOI or ONESHIP is not the FM but a mapping on GUI, hence the logic
        // needs to corrected
        LOG.debug("FulfillmentModel " + fm + "and FulfillmentCentre: " + fcCenter);
        Map<String, String> fmFCMapping = ConfigUtils.getMap(Property.FM_CODE_TO_FC_TYPE_MAP);
        LOG.info("fmFCMapping:" + fmFCMapping);

        return fcCenter != null && fm != null && fmFCMapping != null && !fcCenter.getType().equals(fmFCMapping.get(fm.getCode()));
    }

    private Set<String> getSetOfSUPCsForSpecifiedSeller(String sellerCode) {
        try {
            List<String> supcList = sellerToSUPCListLookupService.getSupcListForSeller(sellerCode);
            if (supcList == null) {
                supcList = new ArrayList<String>();
            }
            return new TreeSet<String>(supcList);
        } catch (Exception e) {
            LOG.error("Error while fetching seller-supc data for seller {}; Exception:{} ", sellerCode, e);
        }
        return null;
    }

    private boolean isValidateSellerSUPCEagerOnUpload() {
        String pVal = getValidateSellerSUPCPropertyValue();
        boolean b = false;
        if (SELLER_SUPC_VALIDATION_MAPPING.ON_UPLOAD.toString().equalsIgnoreCase(pVal) || SELLER_SUPC_VALIDATION_MAPPING.ALWAYS.toString().equalsIgnoreCase(pVal)) {
            b = true;
        }
        return b;
    }

    private boolean isValidateSellerSUPCOnProcess() {
        String pVal = getValidateSellerSUPCPropertyValue();
        boolean b = false;
        if (SELLER_SUPC_VALIDATION_MAPPING.ON_PROCESS.toString().equalsIgnoreCase(pVal) || SELLER_SUPC_VALIDATION_MAPPING.ALWAYS.toString().equalsIgnoreCase(pVal)) {
            b = true;
        }
        return b;
    }

    private String getValidateSellerSUPCPropertyValue() {
        Property p = Property.VALIDATE_SELLER_SUPC_MAPPING;
        String value = dep.getStringPropertyValue(p);
        LOG.info("Property {} is set to {}, for validate seller-supc mapping value ", p, value);
        if (value == null) {
            value = SELLER_SUPC_VALIDATION_MAPPING.DISABLED.toString();
        }
        return value;
    }

    /**
     * Check if any valid seller-fm maping exists for the seller code
     * 
     * @param sellerCode
     * @return
     */
    private boolean isSellerExists(String sellerCode) {
        try {
            return fulfilmentService.isSellerExists(sellerCode);
        } catch (Exception e) {
            LOG.error("Error while checking existence of seller {}; Exception:{} ", sellerCode, e);
        }
        return false;
    }

    private boolean isSellerSupcFMFCUpdatePushEnabled() {
        Property p = Property.SELLER_SUPC_FM_FC_UPDATE_PUSH_ENABLED;
        if (!ConfigUtils.getBooleanScalar(p)) {
            LOG.warn(p + " for push of seller-supc-fm-fcList is disabled ... ");
            return false;
        }
        return true;
    }

    public interface IDependency {
        boolean getBooleanPropertyValue(Property property);

        String getStringPropertyValue(Property property);

        public <T> List<T> readFile(String uploadPath, Class<T> dtoClass) throws IOException, ClassNotFoundException, InvalidDataException;

        public <T> List<T> readFile(String uploadPath, Class<T> dtoClass, boolean emptyFieldAllowed) throws IOException, ClassNotFoundException, InvalidDataException;

        public void downloadFile(String serverName, String userName, String password, String ftpFullPath, String localFullPath);
    }

    static class Dependency implements IDependency {
        @Override
        public boolean getBooleanPropertyValue(Property property) {
            return ConfigUtils.getBooleanScalar(property);
        }

        @Override
        public String getStringPropertyValue(Property property) {
            return ConfigUtils.getStringScalar(property);
        }

        @Override
        public <T> List<T> readFile(String uploadPath, Class<T> dtoClass, boolean emptyFieldAllowed) throws IOException, ClassNotFoundException, InvalidDataException {
            return ExcelReader.readFile(uploadPath, dtoClass, emptyFieldAllowed);
        }

        @Override
        public <T> List<T> readFile(String uploadPath, Class<T> dtoClass) throws IOException, ClassNotFoundException, InvalidDataException {
            return ExcelReader.readFile(uploadPath, dtoClass);
        }

        @Override
        public void downloadFile(String serverName, String userName, String password, String ftpFullPath, String localFullPath) {
            FTPUtils.downloadFile(serverName, userName, password, ftpFullPath, localFullPath);
        }
    }
}
