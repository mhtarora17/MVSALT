/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 18-Mar-2013
 *  @author prateek
 */
package com.snapdeal.cocofs.services.usermanagement.impl;

import java.util.Date;

public class UserRoleDTO {

    private Integer id;
    private String  userEmail;
    private String  parentRole;
    private boolean enabled;
    private Date    created;
    private String  roleCode;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getParentRole() {
        return parentRole;
    }

    public void setParentRole(String parentRole) {
        this.parentRole = parentRole;
    }

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    @Override
    public String toString() {
        return "UserRoleDTO [id=" + id + ", userEmail=" + userEmail + ", parentRole=" + parentRole + ", enabled=" + enabled + ", created=" + created + ", roleCode=" + roleCode
                + "]";
    }

}
