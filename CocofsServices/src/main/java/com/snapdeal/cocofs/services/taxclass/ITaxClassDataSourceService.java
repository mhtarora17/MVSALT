package com.snapdeal.cocofs.services.taxclass;

import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.services.taxclass.dto.SubcatTaxClassMappingDTO;
import com.snapdeal.cocofs.services.taxclass.dto.SupcTaxClassMappingDTO;

public interface ITaxClassDataSourceService {

    /**
     * Returns the Tax Class by supc
     * 
     * @param supc
     * @return TaxClass
     * @throws ExternalDataNotFoundException
     */
    public SupcTaxClassMappingDTO getTaxClassBySupc(String supc) throws ExternalDataNotFoundException;

    /**
     * Returns the Tax Class by subcat
     * 
     * @param supc
     * @return TaxClass
     * @throws ExternalDataNotFoundException
     */
    public SubcatTaxClassMappingDTO getTaxClassBySubcat(String subcat) throws ExternalDataNotFoundException;

}
