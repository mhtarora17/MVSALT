/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 */
package com.snapdeal.cocofs.services.jobprocessor;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.naming.OperationNotSupportedException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.snapdeal.base.cache.CacheManager;
import com.snapdeal.base.utils.CollectionUtils;
import com.snapdeal.cocofs.access.IProductAttributeService;
import com.snapdeal.cocofs.cache.ProductCategoryCache;
import com.snapdeal.cocofs.cache.TaxClassCache;
import com.snapdeal.cocofs.common.BulkUploadValidationDTO;
import com.snapdeal.cocofs.common.BulkUploadValidationResponse;
import com.snapdeal.cocofs.common.JobProcessorResponse;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.core.aerospike.vo.SubcatTaxClassMappingVO;
import com.snapdeal.cocofs.core.aerospike.vo.SupcTaxClassMappingVO;
import com.snapdeal.cocofs.dataaccess.taxclass.ITaxClassAerospikeDataReadService;
import com.snapdeal.cocofs.dataaccess.taxclass.ITaxClassDBDataReadService;
import com.snapdeal.cocofs.entity.JobDetail;
import com.snapdeal.cocofs.entity.ProductAttribute;
import com.snapdeal.cocofs.entity.SubcatTaxClassMapping;
import com.snapdeal.cocofs.entity.SupcTaxClassMapping;
import com.snapdeal.cocofs.enums.BulkUploadValidationError;
import com.snapdeal.cocofs.enums.JobActionCode;
import com.snapdeal.cocofs.generic.persister.exception.GenericPersisterException;
import com.snapdeal.cocofs.generic.persister.response.GenericPersisterWithAerospikeResponse;
import com.snapdeal.cocofs.services.IJobProcessor;
import com.snapdeal.cocofs.services.IJobSchedulerService;
import com.snapdeal.cocofs.services.data.engine.IDataEngineWithAerospike;
import com.snapdeal.cocofs.services.taxclass.ITaxClassMappingDataUpdater;
import com.snapdeal.cocofs.services.taxclass.ITaxClassService;
import com.snapdeal.cocofs.services.taxclass.dto.SubcatTaxClassMappingDTO;
import com.snapdeal.cocofs.services.taxclass.dto.SupcTaxClassMappingDTO;
import com.snapdeal.cocofs.utils.AerospikeKeyHelper;
import com.snapdeal.cocofs.utils.ExcelReader;
import com.snapdeal.cocofs.utils.FTPUtils;
import com.sun.media.sound.InvalidDataException;

@Service("taxClassUploadProcessor")
public class TaxClassUploadProcessor implements IJobProcessor {

    private static final Logger                                                                                LOG = LoggerFactory.getLogger(TaxClassUploadProcessor.class);
    @Autowired
    private IJobSchedulerService                                                                               jobService;

    @Autowired
    private ITaxClassMappingDataUpdater                                                                        dataUpdater;

    @Autowired
    private ITaxClassDBDataReadService                                                                         taxClassDBDataReadService;

    @Autowired
    private ITaxClassAerospikeDataReadService                                                                  taxClassAerospikeService;

    @Autowired
    private IProductAttributeService                                                                           productAttributeService;

    @Autowired
    private ITaxClassService                                                                                   taxClassService;

    @Autowired
    @Qualifier("SupcTaxMappingDataEngine")
    private IDataEngineWithAerospike<SupcTaxClassMappingDTO, SupcTaxClassMapping, SupcTaxClassMappingVO>       supcdataEngine;

    @Autowired
    @Qualifier("SubcatTaxMappingDataEngine")
    private IDataEngineWithAerospike<SubcatTaxClassMappingDTO, SubcatTaxClassMapping, SubcatTaxClassMappingVO> subcatdataEngine;

    private IDependency                                                                                        dep = new Dependency();

    public enum SUPC_VALIDATION_MAPPING {
        DISABLED, ON_UPLOAD, ON_PROCESS, ALWAYS;
    }

    public enum SUBCAT_VALIDATION_MAPPING {
        DISABLED, ON_UPLOAD, ON_PROCESS, ALWAYS;
    }

    @PostConstruct
    public void init() {
        jobService.addJobProcessor(JobActionCode.SUPC_TAX_CLASS.getCode(), this);
        jobService.addJobProcessor(JobActionCode.SUBCAT_TAX_CLASS.getCode(), this);
    }

    @Override
    public JobProcessorResponse process(JobDetail jobDetail) {
        JobProcessorResponse response = new JobProcessorResponse();
        LOG.info("processing file:" + jobDetail.getFileName() + " from path:" + jobDetail.getFilePath() + " status:" + jobDetail.getStatus().getCode());
        String ftpServerPath = dep.getStringPropertyValue(Property.COCOFS_FTP_SERVER_PATH);
        String ftpUsername = dep.getStringPropertyValue(Property.COCOFS_FTP_USERNAME);
        String ftpPassword = dep.getStringPropertyValue(Property.COCOFS_FTP_PASSWORD);
        String filepath = jobDetail.getFilePath() + File.separator + jobDetail.getFileName();
        //        String filepath = "/home/nitish/Downloads/SubcatTaxClassMapping.xls";
        //        String filepath = "/home/nitish/Downloads/SupcTaxClassMapping.xls";
        // We save file on identical paths on local FS and FTP
        Map<String, List<String>> errorMap = new HashMap<String, List<String>>(0);
        boolean success = false;
        try {
            dep.downloadFile(ftpServerPath, ftpUsername, ftpPassword, filepath, filepath);

            if (JobActionCode.SUPC_TAX_CLASS.getCode().equals(jobDetail.getAction().getCode())) {
                errorMap = processSupcTaxClassData(filepath, errorMap, jobDetail.getUploadedBy() + ";" + jobDetail.getFileCode());
            } else if (JobActionCode.SUBCAT_TAX_CLASS.getCode().equals(jobDetail.getAction().getCode())) {
                errorMap = processSubcatTaxClassData(filepath, errorMap, jobDetail.getUploadedBy() + ";" + jobDetail.getFileCode());
            } else {
                LOG.info("Invalid job action code " + jobDetail.getAction().getCode() + "for file" + filepath);

            }
            success = true;
        } catch (InvalidDataException e) {
            updateErrorMapForFileError(jobDetail, errorMap, e);
            LOG.error("Exception ", e);
        } catch (IOException e) {
            updateErrorMapForFileError(jobDetail, errorMap, e);
            LOG.error("Exception ", e);
        } catch (ClassNotFoundException e) {
            updateErrorMapForFileError(jobDetail, errorMap, e);
            LOG.error("Exception ", e);
        } catch (Exception e) {
            updateErrorMapForFileError(jobDetail, errorMap, e);
            LOG.error("Exception ", e);
        }
        response.setError(errorMap);
        if ((errorMap != null && errorMap.size() > 0) || !success) {
            response.setProcessingSuccessful(false);
        } else {
            response.setProcessingSuccessful(true);
        }
        return response;

    }

    private void updateErrorMapForFileError(JobDetail jobDetail, Map<String, List<String>> errorMap, Exception e) {
        if (!errorMap.containsKey("FileError")) {
            errorMap.put("FileError", new ArrayList<String>());
        }
        errorMap.get("FileError").add("Error in reading file for job " + jobDetail.getFileCode() + " Error Message " + e.getMessage());
    }

    private Map<String, List<String>> processSupcTaxClassData(String filepath, Map<String, List<String>> errorMap, String updatedBy) throws ClassNotFoundException,
            InvalidDataException, IOException {

        List<SupcTaxClassMappingDTO> dtoList = dep.readFile(filepath, SupcTaxClassMappingDTO.class, true);
        Map<String, Set<String>> mapSUPCTaxClassMapping = new HashMap<String, Set<String>>();
        boolean bValidateSUPCOnProcess = isValidateSUPCOnProcess();

        int row = 0;
        int total = dtoList.size();
        for (SupcTaxClassMappingDTO dto : dtoList) {
            row++;
            long tStart = System.currentTimeMillis();
            LOG.info("processing started for supc {} at row: {} of total: {};", dto.getSupc(), row, total);
            processDataForSupc(dto, errorMap, updatedBy, mapSUPCTaxClassMapping, bValidateSUPCOnProcess);
            LOG.info("processing done for supc {} at row: {} of total: {}; took {} ms", dto.getSupc(), row, total, (System.currentTimeMillis() - tStart));
        }

        return errorMap;
    }

    private SupcTaxClassMappingVO getSupcTaxClassMappingRecord(SupcTaxClassMapping taxClassEntity) {

        if (taxClassEntity == null) {
            return null;
        }

        SupcTaxClassMappingVO supcTaxClassMappingVO = (SupcTaxClassMappingVO) taxClassAerospikeService.getSupcTaxClassMapping(AerospikeKeyHelper.getKey(taxClassEntity.getSupc()),
                SupcTaxClassMappingVO.class);
        if (supcTaxClassMappingVO != null) {
            return supcTaxClassMappingVO;
        } else {
            LOG.error("no aerosplike record found for Supc: " + taxClassEntity.getSupc());
            return null;
        }

    }

    private void processDataForSupc(SupcTaxClassMappingDTO uploadDto, Map<String, List<String>> errorMap, String updatedBy, Map<String, Set<String>> mapSupcTaxClassMapping,
            boolean bValidateSUPCOnProcess) {

        SupcTaxClassMapping taxClassEntity = taxClassDBDataReadService.getSupcTaxClassMapping(uploadDto.getSupc());
        LOG.info("reading from aerospike for supc: {} ", uploadDto.getSupc());
        SupcTaxClassMappingVO taxRecord = getSupcTaxClassMappingRecord(taxClassEntity);
        try {

            String supcCode = uploadDto.getSupc();

            if (bValidateSUPCOnProcess) {
                boolean bValidSUPCTaxClassMappingForDTO = isValidSupcSpecified(supcCode);

                if (!bValidSUPCTaxClassMappingForDTO) {
                    LOG.info("isValidSupcSpecified returning {}, for supc:{}", bValidSUPCTaxClassMappingForDTO, supcCode);
                    updateErrorMapForRowError(uploadDto, errorMap, BulkUploadValidationError.SUPC_NON_EXIST.getDescription());
                    return;
                }
            }

            List<SupcTaxClassMapping> toBeUpdatedTaxClassEntityList = (taxClassEntity == null) ? null : Arrays.asList(taxClassEntity);
            List<SupcTaxClassMappingVO> toBeUpdatedTaxClassRecordList = (taxRecord == null) ? null : Arrays.asList(taxRecord);
            toBeUpdatedTaxClassEntityList = supcdataEngine.enrichEntities(uploadDto, toBeUpdatedTaxClassEntityList, updatedBy);
            toBeUpdatedTaxClassRecordList = supcdataEngine.enrichRecords(uploadDto, toBeUpdatedTaxClassRecordList, updatedBy);

            GenericPersisterWithAerospikeResponse<SupcTaxClassMapping, SupcTaxClassMappingVO> resp = dataUpdater.updateSupcTaxClassMapping(toBeUpdatedTaxClassEntityList,
                    toBeUpdatedTaxClassRecordList);

            if (!resp.isSuccessful()) {
                updateErrorMapForRowError(uploadDto, errorMap, resp.getMessage());
            }

        } catch (OperationNotSupportedException e) {
            LOG.error("Exception ", e);
        } catch (GenericPersisterException e) {
            LOG.error("Exception ", e);
        } catch (Exception e) {
            LOG.error("Error", e);
        }

    }

    private void updateErrorMapForRowError(SupcTaxClassMappingDTO uploadDto, Map<String, List<String>> errorMap, String message) {
        if (!errorMap.containsKey("RowError")) {
            errorMap.put("RowError", new ArrayList<String>());
        }
        errorMap.get("RowError").add("Error in row with SUPC : " + uploadDto.getSupc() + " Error : " + message);
    }

    private Map<String, List<String>> processSubcatTaxClassData(String filepath, Map<String, List<String>> errorMap, String updatedBy) throws ClassNotFoundException,
            InvalidDataException, IOException {
        List<SubcatTaxClassMappingDTO> dtoList = dep.readFile(filepath, SubcatTaxClassMappingDTO.class, true);
        Map<String, Set<String>> mapSubcatTaxClassMapping = new HashMap<String, Set<String>>();
        boolean bValidateSubcatOnProcess = isValidateSubcatOnProcess();

        int row = 0;
        int total = dtoList.size();
        for (SubcatTaxClassMappingDTO dto : dtoList) {
            row++;
            dto.setEnabled(true);
            long tStart = System.currentTimeMillis();
            LOG.info("processing started for supc {} at row: {} of total: {};", dto.getSubcat(), row, total);
            processDataForSubcat(dto, errorMap, updatedBy, mapSubcatTaxClassMapping, bValidateSubcatOnProcess);
            LOG.info("processing done for supc {} at row: {} of total: {}; took {} ms", dto.getSubcat(), row, total, (System.currentTimeMillis() - tStart));
        }

        return errorMap;
    }

    private void processDataForSubcat(SubcatTaxClassMappingDTO uploadDto, Map<String, List<String>> errorMap, String updatedBy, Map<String, Set<String>> mapSubcatTaxClassMapping,
            boolean bValidateSubcatOnProcess) {
        SubcatTaxClassMapping taxClassEntity = taxClassDBDataReadService.getSubcatTaxClassMapping(uploadDto.getSubcat());
        LOG.info("reading from aerospike for supc: {} ", uploadDto.getSubcat());
        SubcatTaxClassMappingVO taxRecord = getSubcatTaxClassMappingRecord(taxClassEntity);
        try {

            String supcCode = uploadDto.getSubcat();

            if (bValidateSubcatOnProcess) {
                boolean bValidSubcatTaxClassMappingForDTO = isValidSubcatSpecified(supcCode);

                if (!bValidSubcatTaxClassMappingForDTO) {
                    LOG.info("isValidSubcatSpecified returning {}, for supc:{}", bValidSubcatTaxClassMappingForDTO, supcCode);
                    updateErrorMapForRowError(uploadDto, errorMap, BulkUploadValidationError.SUBCAT_NON_EXIST.getDescription());
                    return;
                }
            }

            List<SubcatTaxClassMapping> toBeUpdatedTaxClassEntityList = (taxClassEntity == null) ? null : Arrays.asList(taxClassEntity);
            List<SubcatTaxClassMappingVO> toBeUpdatedTaxClassRecordList = (taxRecord == null) ? null : Arrays.asList(taxRecord);
            toBeUpdatedTaxClassEntityList = subcatdataEngine.enrichEntities(uploadDto, toBeUpdatedTaxClassEntityList, updatedBy);
            toBeUpdatedTaxClassRecordList = subcatdataEngine.enrichRecords(uploadDto, toBeUpdatedTaxClassRecordList, updatedBy);

            GenericPersisterWithAerospikeResponse<SubcatTaxClassMapping, SubcatTaxClassMappingVO> resp = dataUpdater.updateSubcatTaxClassMapping(toBeUpdatedTaxClassEntityList,
                    toBeUpdatedTaxClassRecordList);

            if (!resp.isSuccessful()) {
                updateErrorMapForRowError(uploadDto, errorMap, resp.getMessage());
            }

        } catch (OperationNotSupportedException e) {
            LOG.error("Exception ", e);
        } catch (GenericPersisterException e) {
            LOG.error("Exception ", e);
        } catch (Exception e) {
            LOG.error("Error", e);
        }

    }

    private boolean isValidSubcatSpecified(String subcat) {
        ProductCategoryCache productCategoryCache = CacheManager.getInstance().getCache(ProductCategoryCache.class);
        if (productCategoryCache != null) {
            return productCategoryCache.isValidSubCat(subcat);
        }
        return false;
    }

    private SubcatTaxClassMappingVO getSubcatTaxClassMappingRecord(SubcatTaxClassMapping taxClassEntity) {
        if (taxClassEntity == null) {
            return null;
        }

        SubcatTaxClassMappingVO supcTaxClassMappingVO = (SubcatTaxClassMappingVO) taxClassAerospikeService.getSubcatTaxClassMapping(
                AerospikeKeyHelper.getKey(taxClassEntity.getSubcat()), SubcatTaxClassMappingVO.class);
        if (supcTaxClassMappingVO != null) {
            return supcTaxClassMappingVO;
        } else {
            LOG.error("no aerosplike record found forSubcat: " + taxClassEntity.getSubcat());
            return null;
        }

    }

    private void updateErrorMapForRowError(SubcatTaxClassMappingDTO uploadDto, Map<String, List<String>> errorMap, String message) {
        if (!errorMap.containsKey("RowError")) {
            errorMap.put("RowError", new ArrayList<String>());
        }
        errorMap.get("RowError").add("Error in row with SUPC : " + uploadDto.getSubcat() + " Error : " + message);

    }

    @Override
    public BulkUploadValidationResponse validateJob(File file, String actionCode,String param) {
        BulkUploadValidationResponse response = new BulkUploadValidationResponse();
        try {
            if (JobActionCode.SUPC_TAX_CLASS.getCode().equals(actionCode)) {
                response = validateForSupcTaxClassTypeJob(file);
            } else if (JobActionCode.SUBCAT_TAX_CLASS.getCode().equals(actionCode)) {
                response = validateForSubcatTaxClassTypeJob(file);
            } else {
                response.setValid(false);
                response.setErrorMessage("Invalid action code " + actionCode + "for file" + file.getAbsoluteFile());
            }
        } catch (InvalidDataException e) {
            response.setValid(false);
            response.setErrorMessage(BulkUploadValidationError.INVALID_DATA_VALUE.getDescription() + " " + e.getMessage());
            LOG.error("Error while uploading the data for file {} for action type {} ", file.getAbsoluteFile(), actionCode, e);
        } catch (IOException e) {
            response.setValid(false);
            response.setErrorMessage(BulkUploadValidationError.UPLOAD_FILE_READ_ERROR.getDescription());
            LOG.error("Error while uploading the data for file {} for action type {} ", file.getAbsoluteFile(), actionCode, e);
        } catch (ClassNotFoundException e) {
            response.setValid(false);
            response.setErrorMessage(BulkUploadValidationError.FILE_TYPE_NOT_CORRECT.getDescription());
            LOG.error("Error while uploading the data for file {} for action type {} ", file.getAbsoluteFile(), actionCode, e);
        }
        return response;

    }

    private BulkUploadValidationResponse validateForSubcatTaxClassTypeJob(File file) throws ClassNotFoundException, InvalidDataException, IOException {
        BulkUploadValidationResponse response = new BulkUploadValidationResponse();
        List<BulkUploadValidationDTO> validationDTOList = new ArrayList<BulkUploadValidationDTO>();
        List<SubcatTaxClassMappingDTO> dtoList = null;
        boolean success = false;
        dtoList = dep.readFile(file.getAbsolutePath(), SubcatTaxClassMappingDTO.class, true);
        int rowNo = 1;
        Set<String> dtoSet = new HashSet<String>();

        LOG.info("Uploaded records count - {}", dtoList.size());

        for (SubcatTaxClassMappingDTO dto : dtoList) {
            String subcat = dto.getSubcat();
            dtoSet.add(subcat);
        }

        LOG.info("Uploaded records count - {}", dtoList.size());

        if (dtoList.size() != dtoSet.size()) {
            LOG.info("List size dtoList.size() {} set size {}", dtoList.size(), dtoSet.size());
            response.setErrorMessage("Duplicate subcat exists in the file. Total records: " + dtoList.size() + ", unique records: " + dtoSet.size());
            response.setValid(false);
            return response;
        }

        StringBuilder sb = new StringBuilder();

        for (SubcatTaxClassMappingDTO dto : dtoList) {
            String subcat = dto.getSubcat();

            if (subcat == null || subcat.trim().isEmpty()) {
                LOG.error(rowNo + " subcat is missing, validation is false for subcat:{}", subcat);
                sb.append("subcat is missing from rows : " + rowNo + "<br>");
                validationDTOList.add(new BulkUploadValidationDTO(rowNo, BulkUploadValidationError.SUBCAT_MISSING.getDescription(), subcat));
            } else if (!isValidSubcatSpecified(subcat)) {
                LOG.error("row {} isValidSubcatSpecified is false for subcat:{}", rowNo, subcat);
                sb.append("Invalid Subcat found :  from rows : " + rowNo + ", subcat : " + dto.getSubcat() + "<br>");
                validationDTOList.add(new BulkUploadValidationDTO(rowNo, BulkUploadValidationError.SUBCAT_NON_EXIST.getDescription(), subcat));
            } else if (null == dto.getTaxClass() || dto.getTaxClass().trim().isEmpty()) {
                LOG.error(rowNo + "  tax class empty:{}", dto.getTaxClass());
                sb.append("tax class empty: :  from rows : " + rowNo + "<br>");
                validationDTOList.add(new BulkUploadValidationDTO(rowNo, BulkUploadValidationError.REQUIRED_FIELDS_MISSING.getDescription(), dto.getTaxClass()));
                continue;
            } else if (!validateTaxClass(dto.getTaxClass())) {
                LOG.error(rowNo + " not a valid tax class:{}", dto.getTaxClass());
                sb.append("Invalid Tax class found :  from rows : " + rowNo + ", tax class : " + dto.getTaxClass() + "<br>");
                validationDTOList.add(new BulkUploadValidationDTO(rowNo, BulkUploadValidationError.INVALID_TAX_CLASS.getDescription(), dto.getTaxClass()));
            }
            rowNo++;

        }
        success = true;
        response.setValid(success && validationDTOList.isEmpty());

        if (!validationDTOList.isEmpty()) {
            response.setValid(false);
            response.setList(validationDTOList);
            response.setErrorMessage(sb.toString());

        }

        return response;
    }

    private BulkUploadValidationResponse validateForSupcTaxClassTypeJob(File file) throws ClassNotFoundException, InvalidDataException, IOException {
        BulkUploadValidationResponse response = new BulkUploadValidationResponse();
        List<BulkUploadValidationDTO> validationDTOList = new ArrayList<BulkUploadValidationDTO>();
        List<SupcTaxClassMappingDTO> dtoList = null;
        boolean success = false;

        dtoList = dep.readFile(file.getAbsolutePath(), SupcTaxClassMappingDTO.class, true);
        int rowNo = 1;
        Set<String> dtoSet = new HashSet<String>();

        for (SupcTaxClassMappingDTO dto : dtoList) {
            String supc = dto.getSupc();
            dtoSet.add(supc);
        }

        LOG.info("Uploaded records count - {}", dtoList.size());

        if (dtoList.size() != dtoSet.size()) {
            LOG.info("List size dtoList.size() {} set size {}", dtoList.size(), dtoSet.size());
            response.setErrorMessage("Duplicate supc exists in the file. Total records: " + dtoList.size() + ", unique records: " + dtoSet.size());
            response.setValid(false);
            return response;
        }

        StringBuilder sb = new StringBuilder();

        for (SupcTaxClassMappingDTO dto : dtoList) {
            String supc = dto.getSupc();

            if (supc == null || supc.trim().isEmpty()) {
                LOG.error(rowNo + " supc is missing, validation is false for supc:{}", supc);
                sb.append("supc is missing from rows : " + rowNo + "<br>");
                validationDTOList.add(new BulkUploadValidationDTO(rowNo, BulkUploadValidationError.SUPC_MISSING.getDescription(), supc));
            } else if (!isValidSupcSpecified(supc)) {
                LOG.error("row {} isValidSupcSpecified is false for supc:{}", rowNo, supc);
                sb.append("invalid value for supc from rows : " + rowNo + ", supc : " + dto.getSupc() + "<br>");
                validationDTOList.add(new BulkUploadValidationDTO(rowNo, BulkUploadValidationError.SUPC_NON_EXIST.getDescription(), supc));
            } else if (null == dto.getTaxClass() || dto.getTaxClass().trim().isEmpty()) {
                LOG.error(rowNo + " Supc not exist:{}", dto.getTaxClass());
                sb.append("taxclass is missing from rows : " + rowNo + "<br>");
                validationDTOList.add(new BulkUploadValidationDTO(rowNo, BulkUploadValidationError.REQUIRED_FIELDS_MISSING.getDescription(), dto.getTaxClass()));
                continue;
            } else if (!validateTaxClass(dto.getTaxClass())) {
                LOG.error(rowNo + " not a valid tax class:{}", dto.getTaxClass());
                sb.append("invalid value for taxClass from rows : " + rowNo + ", tax class: " + dto.getTaxClass() + "<br>");
                validationDTOList.add(new BulkUploadValidationDTO(rowNo, BulkUploadValidationError.INVALID_TAX_CLASS.getDescription(), dto.getTaxClass()));
            }
            rowNo++;
        }
        success = true;
        response.setValid(success && validationDTOList.isEmpty());

        if (!validationDTOList.isEmpty()) {
            response.setValid(false);
            response.setList(validationDTOList);
            response.setErrorMessage(sb.toString());

        }

        return response;
    }

    private boolean validateTaxClass(String taxClass) {
        TaxClassCache taxClassCache = CacheManager.getInstance().getCache(TaxClassCache.class);
        if (taxClassCache != null) {
            return taxClassCache.isValidTaxClass(taxClass);
        }
        LOG.info("TaxClass Cache doesnot exists");
        return false;
    }

    private boolean isValidSupcSpecified(String supc) {

        List<ProductAttribute> list = productAttributeService.getAllProductAttributeForSUPC(supc);
        if (list != null && !CollectionUtils.isEmpty(list)) {
            return true;
        }
        return false;
    }

    private String getValidateSUPCTaxClassPropertyValue() {
        Property p = Property.VALIDATE_SUPC_TAX_MAPPING;
        String value = dep.getStringPropertyValue(p);
        LOG.info("Property {} is set to {}, for validate supc tax mapping value ", p, value);
        if (value == null) {
            value = SUPC_VALIDATION_MAPPING.DISABLED.toString();
        }
        return value;
    }

    private boolean isValidateSUPCOnProcess() {
        String pVal = getValidateSUPCTaxClassPropertyValue();
        boolean b = false;
        if (SUPC_VALIDATION_MAPPING.ON_PROCESS.toString().equalsIgnoreCase(pVal) || SUPC_VALIDATION_MAPPING.ALWAYS.toString().equalsIgnoreCase(pVal)) {
            b = true;
        }
        return b;
    }

    private boolean isValidateSubcatOnProcess() {
        String pVal = getValidateSubcatTaxClassPropertyValue();
        boolean b = false;
        if (SUBCAT_VALIDATION_MAPPING.ON_PROCESS.toString().equalsIgnoreCase(pVal) || SUBCAT_VALIDATION_MAPPING.ALWAYS.toString().equalsIgnoreCase(pVal)) {
            b = true;
        }
        return b;
    }

    private String getValidateSubcatTaxClassPropertyValue() {
        Property p = Property.VALIDATE_SUBCAT_TAX_MAPPING;
        String value = dep.getStringPropertyValue(p);
        LOG.info("Property {} is set to {}, for validate subcat tax mapping value ", p, value);
        if (value == null) {
            value = SUBCAT_VALIDATION_MAPPING.DISABLED.toString();
        }
        return value;
    }

    public interface IDependency {
        boolean getBooleanPropertyValue(Property property);

        String getStringPropertyValue(Property property);

        public <T> List<T> readFile(String uploadPath, Class<T> dtoClass) throws IOException, ClassNotFoundException, InvalidDataException;

        public <T> List<T> readFile(String uploadPath, Class<T> dtoClass, boolean emptyFieldAllowed) throws IOException, ClassNotFoundException, InvalidDataException;

        public void downloadFile(String serverName, String userName, String password, String ftpFullPath, String localFullPath);
    }

    static class Dependency implements IDependency {
        @Override
        public boolean getBooleanPropertyValue(Property property) {
            return ConfigUtils.getBooleanScalar(property);
        }

        @Override
        public String getStringPropertyValue(Property property) {
            return ConfigUtils.getStringScalar(property);
        }

        @Override
        public <T> List<T> readFile(String uploadPath, Class<T> dtoClass, boolean emptyFieldAllowed) throws IOException, ClassNotFoundException, InvalidDataException {
            return ExcelReader.readFile(uploadPath, dtoClass, emptyFieldAllowed);
        }

        @Override
        public <T> List<T> readFile(String uploadPath, Class<T> dtoClass) throws IOException, ClassNotFoundException, InvalidDataException {
            return ExcelReader.readFile(uploadPath, dtoClass);
        }

        @Override
        public void downloadFile(String serverName, String userName, String password, String ftpFullPath, String localFullPath) {
            FTPUtils.downloadFile(serverName, userName, password, ftpFullPath, localFullPath);
        }
    }
}
