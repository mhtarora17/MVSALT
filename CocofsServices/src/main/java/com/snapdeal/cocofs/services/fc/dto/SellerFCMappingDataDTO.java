/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.services.fc.dto;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @version 1.0, 23-Mar-2015
 * @author ankur
 */

@XmlRootElement
public class SellerFCMappingDataDTO {

    private String       sellerCode;

    private String       fulfilmentModel;

    private List<String> eligibleFCCodesList = new ArrayList<String>();

    private String       existingFC;
    
    private String       errorMessage;

    public SellerFCMappingDataDTO() {

    }

    public String getSellerCode() {
        return sellerCode;
    }

    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }

    public String getFulfilmentModel() {
        return fulfilmentModel;
    }

    public void setFulfilmentModel(String fulfilmentModel) {
        this.fulfilmentModel = fulfilmentModel;
    }

    public List<String> getEligibleFCCodesList() {
        return eligibleFCCodesList;
    }

    public void setEligibleFCCodesList(List<String> eligibleFCCodesList) {
        this.eligibleFCCodesList = eligibleFCCodesList;
    }

    
       public String getExistingFC() {
        return existingFC;
    }

    public void setExistingFC(String existingFC) {
        this.existingFC = existingFC;
    }
    
    
    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @Override
    public String toString() {
        return "SellerFCMappingDataDTO [sellerCode=" + sellerCode + ", fulfilmentModel=" + fulfilmentModel + ", eligibleFCCodesList=" + eligibleFCCodesList + ", existingFC="
                + existingFC + ", errorMessage=" + errorMessage + "]";
    }

}
