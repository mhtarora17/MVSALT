/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.services.events.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @version 1.0, 3-Apr-2015
 * @author shiv
 */
public class SellerFMFCUpdateEventObj implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -4545679022785278564L;

    private String            sellerCode;
    private String            oldFM;
    private String            newFM;
    private Date              created;
    private List<String>      existingFCCenters;
    private List<String>      newFCCenters;
    private String            oldSellerPincode;
    private String            newSellerPincode;

    public SellerFMFCUpdateEventObj() {
    }

    public SellerFMFCUpdateEventObj(String sellerCode, String newFM, String oldFM, List<String> newFCCenters, List<String> existingFCCenters, String oldPinCode,
            String newPincode) {
        this.sellerCode = sellerCode;
        this.oldFM = oldFM;
        this.newFM = newFM;
        this.setExistingFCCenters(existingFCCenters);
        this.setNewFCCenters(newFCCenters);
        this.created = new Date();
        this.oldSellerPincode = oldPinCode;
        this.newSellerPincode = newPincode;
    
    }

    public String getSellerCode() {
        return sellerCode;
    }

    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }

    public String getOldFM() {
        return oldFM;
    }

    public void setOldFM(String oldFM) {
        this.oldFM = oldFM;
    }

    public String getNewFM() {
        return newFM;
    }

    public void setNewFM(String newFM) {
        this.newFM = newFM;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public List<String> getExistingFCCenters() {
        return existingFCCenters;
    }

    public void setExistingFCCenters(List<String> existingFCCenters) {
        this.existingFCCenters = existingFCCenters;
    }

    public List<String> getNewFCCenters() {
        return newFCCenters;
    }

    public void setNewFCCenters(List<String> newFCCenters) {
        this.newFCCenters = newFCCenters;
    }

    public String getOldSellerPincode() {
        return oldSellerPincode;
    }

    public void setOldSellerPincode(String oldSellerPincode) {
        this.oldSellerPincode = oldSellerPincode;
    }

    public String getNewSellerPincode() {
        return newSellerPincode;
    }

    public void setNewSellerPincode(String newSellerPincode) {
        this.newSellerPincode = newSellerPincode;
    }

    @Override
    public String toString() {
        return "SellerFMFCUpdateEventObj [sellerCode=" + sellerCode + ", oldFM=" + oldFM + ", newFM=" + newFM + ", created=" + created + ", existingFCCenters="
                + existingFCCenters + ", newFCCenters=" + newFCCenters + ", oldSellerPincode=" + oldSellerPincode + ", newSellerPincode=" + newSellerPincode + "]";
    }


}
