/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-Nov-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.services.fm.dto;

import com.snapdeal.cocofs.core.aerospike.vo.SellerSupcFMMappingVO;
import com.snapdeal.cocofs.entity.SellerSupcFMMapping;
import com.snapdeal.cocofs.mongo.model.SellerSupcFMMappingUnit;

public class SellerSupcFMMappingDataDto {

    private SellerSupcFMMapping     entity;

    private SellerSupcFMMappingUnit document;
    
    private SellerSupcFMMappingVO record;

    public SellerSupcFMMappingDataDto() {
    }

    
    public SellerSupcFMMappingDataDto(SellerSupcFMMapping entity, SellerSupcFMMappingUnit document, SellerSupcFMMappingVO record) {
        this.entity = entity;
        this.document = document;
        this.record = record;
    }


    public SellerSupcFMMappingDataDto(SellerSupcFMMapping entity, SellerSupcFMMappingUnit document) {
        this.entity = entity;
        this.document = document;
    }

    public SellerSupcFMMapping getEntity() {
        return entity;
    }

    public void setEntity(SellerSupcFMMapping entity) {
        this.entity = entity;
    }

    public SellerSupcFMMappingUnit getDocument() {
        return document;
    }

    public void setDocument(SellerSupcFMMappingUnit document) {
        this.document = document;
    }
    
    

    public SellerSupcFMMappingVO getRecord() {
        return record;
    }

    public void setRecord(SellerSupcFMMappingVO record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "SellerSupcFMMappingDataDto [entity=" + entity + ", document=" + document + ", record=" + record + "]";
    }

   

}
