/*
*  Copyright 2012 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 27-Nov-2012
 *  @author prateek
 */
package com.snapdeal.cocofs.services.fm.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.snapdeal.base.cache.CacheManager;
import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.cocofs.annotations.TouchyTransaction;
import com.snapdeal.cocofs.cache.FulfillmentCentreCache;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.core.aerospike.vo.FCDetailVO;
import com.snapdeal.cocofs.dataaccess.fc.IFCAerospikeDataReadService;
import com.snapdeal.cocofs.db.dao.IFulfillmentCenterDao;
import com.snapdeal.cocofs.entity.FCAddress;
import com.snapdeal.cocofs.entity.FulfillmentCentre;
import com.snapdeal.cocofs.generic.persister.exception.GenericPersisterException;
import com.snapdeal.cocofs.generic.persister.exception.GenericPersisterExceptionCode;
import com.snapdeal.cocofs.services.converter.IConverterService;
import com.snapdeal.cocofs.services.fm.IFulfillmentCenterService;
import com.snapdeal.score.data.aerospike.service.impl.ScoreAerospikeService;

@Service("fulfillmentCenterService")
public class FulfillmentCenterServiceImpl implements IFulfillmentCenterService {

    @Autowired
    private IFulfillmentCenterDao fcDAO;
    
    @Autowired
    private IConverterService converterService;
    
    @Autowired
    @Qualifier("aerospikeService")
    private ScoreAerospikeService   aerospikeService;
    
    @Autowired IFCAerospikeDataReadService fcaerospikeService;
  

    private static final Logger   LOG = LoggerFactory.getLogger(FulfillmentCenterServiceImpl.class);

    @Override
    public FCAddress getFulfillmentAddressByCode(String fcCode) {
        FCAddress address = null;
        if (StringUtils.isNotEmpty(fcCode)) {
            LOG.info("getting fm address for : " + fcCode);
            FulfillmentCentre fms = getFulfillmentCentreByCode(fcCode);
            if (fms != null) {
                Hibernate.initialize(fms.getFcAddress());
                address = fms.getFcAddress();
            } else {
                LOG.info("fulfillment center not found for code : " + fcCode);
            }
        } else {
            LOG.info("empty fc code");
        }
        return address;

    }

    @Override
    @TouchyTransaction
    public FulfillmentCentre saveOrUpdateFulfilmentCenterWithAerospike(FulfillmentCentre fc) throws GenericPersisterException {
        FulfillmentCentre updatedFc = null;
        //First persist in db...
        try {
            FulfillmentCentre existingFc = getFulfillmentCentreByCode(fc.getCode());
            if (existingFc != null) {
                //Need to update if it already exists...
                fc.setId(existingFc.getId());
                fc.setCreated(existingFc.getCreated());
                fc.getFcAddress().setId(existingFc.getFcAddress().getId());
                fc.getFcAddress().setCreated(existingFc.getFcAddress().getCreated());
                
            } else {
                fc.setCreated(DateUtils.getCurrentTime());
                fc.getFcAddress().setCreated(DateUtils.getCurrentTime());
            }
            updatedFc = fcDAO.updateCenter(fc);
        } catch (Exception ex) {
            LOG.error("Failed to persist fc in db:{}", fc);
            throw new GenericPersisterException(ex, "Not able to persist fc. Reverting tx.", GenericPersisterExceptionCode.RECORD_FAILED_TO_PERSIST);
        }
        //Now try to persist in aerospike
        try {
            FCDetailVO fcVO = converterService.getFCDetailVoFromEntity(updatedFc);
            aerospikeService.put(fcVO);
        } catch (Exception ex) {
            LOG.error("Failed to persist seller zone in aerospike:{} with error:{}", fc, ex.getMessage());
            throw new GenericPersisterException(ex, "Not able to persist seller zone details in aerospike. Reverting tx.", GenericPersisterExceptionCode.RECORD_FAILED_TO_PERSIST);
        }
        return updatedFc;
    }
 
    
    @Override
    @Transactional(readOnly = true)
    public List<FulfillmentCentre> getAllFCs() {
        boolean useCache = ConfigUtils.getBooleanScalar(Property.ALL_FC_CACHE_ENABLED);
        if (useCache) {
            return getAllFCsFromCache();
        } else {
            return getAllFCsFromMysql();
        }
    }

    private List<FulfillmentCentre> getAllFCsFromCache() {
        List<FulfillmentCentre> fcs = CacheManager.getInstance().getCache(FulfillmentCentreCache.class).getAllFulfillmentCentres();
        if (fcs != null) {
            return fcs;
        }

        return new ArrayList<FulfillmentCentre>();
    }
    
    private List<FulfillmentCentre> getAllFCsFromMysql() {
        List<FulfillmentCentre> fcs = fcDAO.getAllFulfillmentCenter();
        if (fcs != null) {
            return fcs;
        }

        return new ArrayList<FulfillmentCentre>();
    }

    @Override
    public FulfillmentCentre getFulfillmentCentreByCode(String fcCode) {
        FulfillmentCentre fms = null;
        if (StringUtils.isNotEmpty(fcCode)) {
            LOG.info("getting FM center details for FCCode: " + fcCode);
            fms = fcDAO.getFulfillmentCenterForFCCode(fcCode);
        }
        return fms;
    }

    @Override
    public Integer updateCenterName(String fcCode, String centerName, String username) {
        // TODO Auto-generated method stub
        return fcDAO.updateFCName(fcCode, centerName, username);
    }

    @Override
    @Transactional(readOnly = true)
    public List<FulfillmentCentre> getAllFCsByType(String centerType) {
        LOG.info("getting FC center details for centerType: " + centerType);
        List<FulfillmentCentre> fcs = getAllFCs();
        List<FulfillmentCentre> toReturn = new ArrayList<FulfillmentCentre>();

        for (FulfillmentCentre centre : fcs) {
            if (centre.getType().equals(centerType)) {
                toReturn.add(centre);
            }
        }

        LOG.info("returning centers size: " + (toReturn == null ? 0 : toReturn.size()));
        return toReturn;
    }

}
