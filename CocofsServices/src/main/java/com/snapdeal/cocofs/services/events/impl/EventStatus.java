/*
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @Desc	Added to track Event Statues. Currently only one status possible. 
 *  		Might want to add further in future
 */
package com.snapdeal.cocofs.services.events.impl;

public enum EventStatus {

    COMPLETED("COMPLETED"), PENDING("PENDING");

    private String code;

    private EventStatus(String code) {
        this.code = code;
    }

    public String getCode() {
        return this.code;
    }
}
