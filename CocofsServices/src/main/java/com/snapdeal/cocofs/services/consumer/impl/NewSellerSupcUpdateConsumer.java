package com.snapdeal.cocofs.services.consumer.impl;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.ObjectMessage;

import org.drools.core.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.snapdeal.base.activemq.IActiveMQManager;
import com.snapdeal.cocofs.access.IPendingProductAttributeDBDataReadService;
import com.snapdeal.cocofs.access.IProductAttributeNotificationMongoDataReadService;
import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.generic.persister.exception.GenericPersisterException;
import com.snapdeal.cocofs.mongo.model.ProductAttributeNotificationUnit;
import com.snapdeal.cocofs.services.consumer.INewSellerSupcUpdateConsumer;
import com.snapdeal.cocofs.services.events.dto.SellerSupcAddEventObj;
import com.snapdeal.cocofs.services.fm.ISellerSUPCFMMappingProcessor;
import com.snapdeal.cocofs.services.fm.ISellerToSUPCListDBLookupService;
import com.snapdeal.ipms.base.queue.update.SUPCSellerUpdateEventSRO;
import com.snapdeal.ipms.base.queue.update.SUPCSellerUpdateEventsSRO;
import com.snapdeal.score.data.aerospike.exception.GetFailedException;
import com.snapdeal.score.data.aerospike.exception.PutFailedException;

/**
 * @author nikhil
 */
@Service("newSellerSupcUpdateConsumer")
public class NewSellerSupcUpdateConsumer implements INewSellerSupcUpdateConsumer, DisposableBean {

    @Autowired
    private IActiveMQManager                          activeMQManager;

    private static Long                               activeMqToken = null;

    private static final Logger                       LOG           = LoggerFactory.getLogger(NewSellerSupcUpdateConsumer.class);

    MessageConsumer                                   consumer      = null;

    @Autowired
    IPendingProductAttributeDBDataReadService         ppaDBDataReadService;

    @Autowired
    IProductAttributeNotificationMongoDataReadService panMongoDataReadService;

    @Autowired
    @Qualifier("failSafeSellerSUPCFMNotificationSendor")
    ISellerSUPCFMMappingProcessor                     failSafeSellerSUPCFMNotificationSendor;
    @Autowired
    private ISellerToSUPCListDBLookupService          sellerToSUPCListLookupService;

    @Override
    public void destroy() throws Exception {
        LOG.info("Unregistering order fulfillment consumer..");
        unregisterConsumer();
    }

    @Override
    public void onMessage(Message message) {
        if (message == null) {
            LOG.error("Got a null message from the seller-supc queue");
        } else {
            SUPCSellerUpdateEventsSRO eventsObj = null;

            try {
                eventsObj = (SUPCSellerUpdateEventsSRO) (((ObjectMessage) message).getObject());
            } catch (JMSException e) {
                LOG.error("Error in casting the queued message {}", message.toString());
                return;
            }

            if (eventsObj.getSucpSellerUpdateEventList() == null || eventsObj.getSucpSellerUpdateEventList().isEmpty()) {
                LOG.error("sucpSellerUpdateEventList is found to be null or empty from the queued message ");
                return;
            }

            LOG.info("total events : {}", eventsObj.getSucpSellerUpdateEventList().size());
            for (SUPCSellerUpdateEventSRO updatedEvent : eventsObj.getSucpSellerUpdateEventList()) {

                if (StringUtils.isEmpty(updatedEvent.getSupc())) {
                    LOG.error("Supc is found to be null from the queued message ");
                    continue;
                }

                if (StringUtils.isEmpty(updatedEvent.getSellerCode())) {
                    LOG.error("Supc is found to be null from the queued message ");
                    continue;
                }

                try {
                    processSellerSupcAssociation(updatedEvent);
                } catch (Exception e) {
                    LOG.error("Throwing RuntimeException for exception:{}", e.toString());
                    throw new RuntimeException("Exception while processing event:" + updatedEvent, e);
                }
            }
        }
    }

    private void processSellerSupcAssociation(SUPCSellerUpdateEventSRO association) throws GetFailedException, ExternalDataNotFoundException, PutFailedException,
            GenericPersisterException {

        String supc = association.getSupc();
        String sellerCode = association.getSellerCode();

        try {
            if (!sellerToSUPCListLookupService.isSUPCExistsInSellerSUPCListMapping(sellerCode, supc)) {
                sellerToSUPCListLookupService.putNewSellerSupcMapping(sellerCode, supc);
                failSafeSellerSUPCFMNotificationSendor.processAdditionOfNewSellerSUPCMappingForExternalNotification(new SellerSupcAddEventObj(sellerCode, supc));
                LOG.info("processing done for addition of new seller SUPC mapping");
            } else {
                LOG.warn("Exising SELLER-SUPC pushed from IPMS :: seller:{} supc:{} ", sellerCode, supc);
            }
        } catch (GenericPersisterException gpe) {
            LOG.error("Could not insert mapping for seller:{} supc:{} ", sellerCode, supc);
            throw gpe;
        }

        ProductAttributeNotificationUnit notification = panMongoDataReadService.getVisibleProductAttributeNotificationBySUPC(supc);
        if (notification == null) {
            LOG.info("No Notification is queued up for the required supc {} ", association.getSupc());
            return;
        }

        if (notification.getSellers().contains(sellerCode)) {
            LOG.info("Notification is already queued up for the required supc-seller combo {} ", association);
        } else {
            LOG.info("Notification queued found for the supc, PAN should be updated for the required supc-seller combo {} ", association);
            notification.getSellers().add(sellerCode);
            panMongoDataReadService.savePAN(notification);
            LOG.info("Successfully processed message of supc-seller combo {} ", association);
        }
    }

    @Override
    public void unregisterConsumer() {
        try {
            if (activeMqToken != null) {
                activeMQManager.unregisterSubscriber(activeMqToken);
            }
            activeMqToken = null;
        } catch (Exception e) {
            LOG.error("unable to unregister order fulfillment consumer to activemq", e);
        }
    }

    @Override
    public void registerConsumer(String queueName, String networkPath, String username, String password) {

        try {
            if (activeMqToken == null) {
                activeMqToken = activeMQManager.registerSubscriber(queueName, networkPath, username, password, this);
                LOG.info("Active mq consumer registered successfully  ...");
            }
        } catch (Exception e) {
            LOG.error("unable to register order fulfillment consumer to activemq", e);
        }
    }

}
