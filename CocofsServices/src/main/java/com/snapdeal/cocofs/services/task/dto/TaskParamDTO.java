/*

 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 28-Aug-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.services.task.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TaskParamDTO {

    private Integer taskParamId;

    private String  paramName;

    private String  paramValue;

    private String  created;

    public Integer getTaskParamId() {
        return taskParamId;
    }

    public void setTaskParamId(Integer id) {
        this.taskParamId = id;
    }

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public String getParamValue() {
        return paramValue;
    }

    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("TaskParamDTO [taskParamId=").append(taskParamId).append(", paramName=").append(paramName).append(", paramValue=").append(paramValue).append(", created=").append(
                created).append("]");
        return builder.toString();
    }

}
