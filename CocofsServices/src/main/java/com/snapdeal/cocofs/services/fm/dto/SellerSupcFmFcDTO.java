package com.snapdeal.cocofs.services.fm.dto;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import com.snapdeal.cocofs.core.aerospike.vo.SellerSupcFMMappingVO;
import com.snapdeal.cocofs.entity.SellerSupcFCCodeMapping;
import com.snapdeal.cocofs.entity.SellerSupcFMMapping;

public class SellerSupcFmFcDTO {

	private String sellerCode;

	private String supc;

	private String fulfilmentModel;

	private boolean enabled;

	private List<String> enabledFCCenters = new ArrayList<>();

	public SellerSupcFmFcDTO() {

	}

	public SellerSupcFmFcDTO(SellerSupcFMMapping entity) {
		this.sellerCode = entity.getSellerCode();
		this.supc = entity.getSupc();
		this.fulfilmentModel = entity.getFulfilmentModel();
		this.enabled = entity.isEnabled();
		
		if (CollectionUtils.isNotEmpty(entity.getFcCenters())) {
			for (SellerSupcFCCodeMapping fcMapping : entity.getFcCenters()) {
				if (fcMapping.isEnabled()) {
					this.getEnabledFCCenters().add(fcMapping.getFcCode());
				}
			}
		}
	}

	public SellerSupcFmFcDTO(SellerSupcFMMappingVO sellersupcFMMappingVO) {
		this.sellerCode = sellersupcFMMappingVO.getSellerCode();
		this.supc = sellersupcFMMappingVO.getSupc();
		this.fulfilmentModel = sellersupcFMMappingVO.getFulfillmentModel();
		this.enabled = sellersupcFMMappingVO.isEnabled();
		
		if (CollectionUtils.isNotEmpty(sellersupcFMMappingVO.getFcCenters())) {
			for (String fcCode : sellersupcFMMappingVO.getFcCenters()) {
				this.getEnabledFCCenters().add(fcCode);
			}
		}
	}
	
	

	public SellerSupcFmFcDTO(String sellerCode, String supc, String fulfilmentModel, boolean enabled, List<String> enabledFCCenters) {
		super();
		this.sellerCode = sellerCode;
		this.supc = supc;
		this.fulfilmentModel = fulfilmentModel;
		this.enabled = enabled;
		this.enabledFCCenters = enabledFCCenters;
	}

	public String getSellerCode() {
		return sellerCode;
	}

	public void setSellerCode(String sellerCode) {
		this.sellerCode = sellerCode;
	}

	public String getSupc() {
		return supc;
	}

	public void setSupc(String supc) {
		this.supc = supc;
	}

	public String getFulfilmentModel() {
		return fulfilmentModel;
	}

	public void setFulfilmentModel(String fulfilmentModel) {
		this.fulfilmentModel = fulfilmentModel;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public List<String> getEnabledFCCenters() {
		return enabledFCCenters;
	}

	public void setEnabledFCCenters(List<String> enabledFCCenters) {
		this.enabledFCCenters = enabledFCCenters;
	}

	@Override
	public String toString() {
		return "SellerSupcFmFcDTO [sellerCode=" + sellerCode + ", supc=" + supc + ", fulfilmentModel=" + fulfilmentModel
				+ ", enabled=" + enabled + ", enabledFCCenters=" + enabledFCCenters + "]";
	}

	
}
