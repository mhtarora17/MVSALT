/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.services.converter;

import com.snapdeal.cocofs.entity.FCAddress;
import com.snapdeal.cocofs.entity.FulfillmentCentre;
import com.snapdeal.cocofs.sro.FCAddressDetailSRO;
import com.snapdeal.cocofs.sro.FCDetailSRO;

/**
 *  
 *  @version     1.0, 30-Mar-2015
 *  @author ankur
 */
public interface IFCConvertorService {


    FCAddressDetailSRO getFCAddressDetailSRO(FCAddress fc);

    FCDetailSRO getFCDetailSRO(FulfillmentCentre fc);

}
