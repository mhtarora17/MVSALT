package com.snapdeal.cocofs.services.common.dto;


/**
 * @version 1.0 4 Nov 2015
 * @author indrajit
 */
public class TaxRateDownloadData {

    private String  sellerCode;

    private String  supc;

    private Double  taxRate;

    private String  taxType;

    private Boolean enabled;

    public String getSellerCode() {
        return sellerCode;
    }

    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }

    public String getSupc() {
        return supc;
    }

    public void setSupc(String supc) {
        this.supc = supc;
    }

    public Double getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(Double taxRate) {
        this.taxRate = taxRate;
    }

    public String getTaxType() {
        return taxType;
    }

    public void setTaxType(String taxType) {
        this.taxType = taxType;
    }

    public Boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((enabled == null) ? 0 : enabled.hashCode());
        result = prime * result + ((sellerCode == null) ? 0 : sellerCode.hashCode());
        result = prime * result + ((supc == null) ? 0 : supc.hashCode());
        result = prime * result + ((taxRate == null) ? 0 : taxRate.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        TaxRateDownloadData other = (TaxRateDownloadData) obj;
        if (enabled == null) {
            if (other.enabled != null)
                return false;
        } else if (!enabled.equals(other.enabled))
            return false;
        if (sellerCode == null) {
            if (other.sellerCode != null)
                return false;
        } else if (!sellerCode.equals(other.sellerCode))
            return false;
        if (supc == null) {
            if (other.supc != null)
                return false;
        } else if (!supc.equals(other.supc))
            return false;
        if (taxRate == null) {
            if (other.taxRate != null)
                return false;
        } else if (!taxRate.equals(other.taxRate))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "TaxRateDownloadData [sellerCode=" + sellerCode + ", supc=" + supc + ", taxRate=" + taxRate + ", enabled=" + enabled + "]";
    }

}
