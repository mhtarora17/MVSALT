/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.services;

import javax.naming.OperationNotSupportedException;

import com.snapdeal.cocofs.generic.persister.exception.GenericPersisterException;
import com.snapdeal.cocofs.generic.persister.response.GenericPersisterWithAerospikeResponse;
import com.snapdeal.cocofs.services.common.dto.UserInfo;
import com.snapdeal.cocofs.services.data.engine.IDataEngineWithAerospike;

/**
 *  
 *  @version     1.0, 15-Sep-2014
 *  @author ankur
 */
public interface IDataUpdaterWithAerospike {

    
    /**
     * Given a DTO object and corresponding data Reader and data Merger interface creates/edit the aerospike records ,mongo documents and
     * entites for it.
     * 
     * @param dto
     * @param reader
     * @param merger
     * @throws GenericPersisterException
     * @throws OperationNotSupportedException
     */
    public <T, E ,R>   GenericPersisterWithAerospikeResponse<E,  R> updateDataWithDTO(T dto, IDataReaderWithAerospike<T, E, R> reader, IDataEngineWithAerospike<T, E, R> engine,
            UserInfo user, boolean skipGenricValidation) throws GenericPersisterException, OperationNotSupportedException;

    
}
