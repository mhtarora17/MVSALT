/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 10-Nov-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.services.usermanagement.impl;

import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.snapdeal.base.cache.CacheManager;
import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.cocofs.cache.RoleCache;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.db.users.IUserService;
import com.snapdeal.cocofs.entity.Role;
import com.snapdeal.cocofs.entity.User;
import com.snapdeal.cocofs.entity.UserRole;
import com.snapdeal.cocofs.services.IEmailService;
import com.snapdeal.cocofs.services.usermanagement.IUserManagementConverterService;
import com.snapdeal.cocofs.services.usermanagement.IUserManagementService;

@Service("UserManagementServiceImpl")
public class UserManagementServiceImpl implements IUserManagementService {

    @Autowired
    private IUserService                    userService;

    @Autowired
    private IUserManagementConverterService userConvertorService;

    @Autowired
    private IEmailService                   emailService;

    private static final Logger             LOG = LoggerFactory.getLogger(UserManagementServiceImpl.class);

    @Override
    public UserDTO getUserInfo(String email) {
        LOG.info("Fetch user info for email {} ", email);
        if (StringUtils.isNotEmpty(email)) {
            User u = userService.getUserByEmailForEdit(email);
            if (u != null) {
                LOG.info("User found for email {} ", email);
                UserDTO uDto = userConvertorService.getUserDTO(u);
                return uDto;
            } else {
                LOG.info("User not found");
            }

        } else {
            LOG.info("Invalid Email");
        }
        return new UserDTO();
    }

    @Override
    public void sendUserCreationEmail(String userEmail, String password) {
        User tempUser = new User();
        tempUser.setEmail(userEmail);
        tempUser.setPassword(password);
        emailService.sendUserCreationEmail(tempUser, ConfigUtils.getStringScalar(Property.CONTENT_PATH));

    }

    @Override
    public void enrichUserWithUserRole(User user, List<String> enabledRoleCodes) {
        Set<UserRole> userRoles = user.getUserRoleMapping();
        for (UserRole userRole : userRoles) {
            if (!enabledRoleCodes.contains(userRole.getRole().getCode())) {
                LOG.info("role {}  disabled for user {} ", userRole.getRole().getCode(), user.getEmail());
                userRole.setEnabled(false);
                userRole.setLastUpdated(DateUtils.getCurrentTime());

            } else {
                //deleting from the list of role codes to be enabled
                enabledRoleCodes.remove(userRole.getRole().getCode());
                LOG.info("Role {} already exists.", userRole.getRole().getCode());
                userRole.setEnabled(true);
                userRole.setLastUpdated(DateUtils.getCurrentTime());
            }
        }

        //adding new user roles created
        for (String newRoleCode : enabledRoleCodes) {
            Role role = CacheManager.getInstance().getCache(RoleCache.class).getRoleByCode(newRoleCode);
            UserRole userRole = new UserRole(user, role);
            userRole.setEnabled(true);
            userRole.setCreated(DateUtils.getCurrentTime());
            userRole.setLastUpdated(DateUtils.getCurrentTime());
            userRoles.add(userRole);
        }
    }

    @Override
    @Transactional
    public void updateUser(User u, String password) {
        boolean toSendNotification = u.getId() == null;
        u = userService.updateUser(u);
        if (toSendNotification) {
            sendUserCreationEmail(u.getEmail(), password);
        }

    }

}
