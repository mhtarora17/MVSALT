/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 10-Nov-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.services;

import com.snapdeal.base.notification.email.EmailMessage;
import com.snapdeal.cocofs.entity.User;

public interface IEmailService {

    public void sendForgotPasswordEmail(User user, String http, String resources, String emailVerificationLink);

    public void send(EmailMessage message);

    void sendUserCreationEmail(User user, String contentPath);

}
