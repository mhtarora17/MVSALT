/*
 *  Copyright 2012 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 27-Nov-2012
 *  @author prateek
 */
package com.snapdeal.cocofs.services.fm;

import java.util.List;

import com.snapdeal.cocofs.core.aerospike.vo.FCDetailVO;
import com.snapdeal.cocofs.entity.FCAddress;
import com.snapdeal.cocofs.entity.FulfillmentCentre;
import com.snapdeal.cocofs.generic.persister.exception.GenericPersisterException;

public interface IFulfillmentCenterService {

    FCAddress getFulfillmentAddressByCode(String fcCode);

    FulfillmentCentre getFulfillmentCentreByCode(String fcCode);

    List<FulfillmentCentre> getAllFCs();
    
    Integer updateCenterName(String fcCode, String centerName, String username);

    List<FulfillmentCentre> getAllFCsByType(String centerType);

    FulfillmentCentre saveOrUpdateFulfilmentCenterWithAerospike(FulfillmentCentre fc) throws GenericPersisterException;

}

