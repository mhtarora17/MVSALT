/*
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 15-Jun-2015
 *  @author gaurav
 */
package com.snapdeal.cocofs.services;

import java.util.List;

import com.snapdeal.cocofs.entity.SellerZoneMapping;
import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.generic.persister.exception.GenericPersisterException;
import com.snapdeal.cocofs.sro.ZoneSRO;

public interface ISellerZoneService {

	public List<SellerZoneMapping> getAllSellerZoneMappings();
    
    public String getSellerZoneMappingBySeller(String sellerCode) throws ExternalDataNotFoundException;

    public String getSellerZoneMappingBySellerFromBaseSource(String sellerCode);
    
    public SellerZoneMapping persistSellerZoneMapping(SellerZoneMapping szm) throws GenericPersisterException;

    public ZoneSRO getZoneForSellerSupc(String sellerCode, String supc) throws ExternalDataNotFoundException;

    List<SellerZoneMapping> getSellerZoneMappingsByPincode(String pincode);

}