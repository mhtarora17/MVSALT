/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.services.taxrate.dto;

import javax.xml.bind.annotation.XmlRootElement;

import com.snapdeal.cocofs.utils.SubClass;

/**
 * @version 1.0, 13-Aug-2014
 * @author ankur
 */
@XmlRootElement
@SubClass
public class StateCategoryPriceTaxRateUploadDTO extends TaxRateUpdateDTO {

    private String state;

    private String subCategoryUrl;

    private Double lowerPriceLimit;

    private Double upperPriceLimit;

    public StateCategoryPriceTaxRateUploadDTO() {

    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getSubCategoryUrl() {
        return subCategoryUrl;
    }

    public void setSubCategoryUrl(String subcategoryUrl) {
        this.subCategoryUrl = subcategoryUrl;
    }

    public Double getLowerPriceLimit() {
        return lowerPriceLimit;
    }

    public void setLowerPriceLimit(Double lowerPriceLimit) {
        this.lowerPriceLimit = lowerPriceLimit;
    }

    public Double getUpperPriceLimit() {
        return upperPriceLimit;
    }

    public void setUpperPriceLimit(Double upperPriceLimit) {
        this.upperPriceLimit = upperPriceLimit;
    }



    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((lowerPriceLimit == null) ? 0 : lowerPriceLimit.hashCode());
        result = prime * result + ((state == null) ? 0 : state.hashCode());
        result = prime * result + ((subCategoryUrl == null) ? 0 : subCategoryUrl.hashCode());
        result = prime * result + ((upperPriceLimit == null) ? 0 : upperPriceLimit.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        StateCategoryPriceTaxRateUploadDTO other = (StateCategoryPriceTaxRateUploadDTO) obj;
        if (lowerPriceLimit == null) {
            if (other.lowerPriceLimit != null)
                return false;
        } else if (!lowerPriceLimit.equals(other.lowerPriceLimit))
            return false;
        if (state == null) {
            if (other.state != null)
                return false;
        } else if (!state.equals(other.state))
            return false;
        if (subCategoryUrl == null) {
            if (other.subCategoryUrl != null)
                return false;
        } else if (!subCategoryUrl.equals(other.subCategoryUrl))
            return false;
        if (upperPriceLimit == null) {
            if (other.upperPriceLimit != null)
                return false;
        } else if (!upperPriceLimit.equals(other.upperPriceLimit))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "StateCategoryPriceTaxRateUploadDTO [state=" + state + ", subCategoryUrl=" + subCategoryUrl + ", lowerPriceLimit=" + lowerPriceLimit + ", upperPriceLimit="
                + upperPriceLimit + ", toString()=" + super.toString() + "]";
    }

}
