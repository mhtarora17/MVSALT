/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.services.jobprocessor;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;

import javax.annotation.PostConstruct;
import javax.naming.OperationNotSupportedException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.snapdeal.base.cache.CacheManager;
import com.snapdeal.base.utils.CollectionUtils;
import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.cocofs.cache.ProductCategoryCache;
import com.snapdeal.cocofs.cache.StateCache;
import com.snapdeal.cocofs.common.BulkUploadValidationDTO;
import com.snapdeal.cocofs.common.BulkUploadValidationResponse;
import com.snapdeal.cocofs.common.JobProcessorResponse;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.dataaccess.taxrate.ITaxRateDBDataReadService;
import com.snapdeal.cocofs.dataaccess.taxrate.ITaxRateMongoService;
import com.snapdeal.cocofs.entity.JobDetail;
import com.snapdeal.cocofs.entity.SellerCategoryTaxRate;
import com.snapdeal.cocofs.entity.SellerSupcTaxRate;
import com.snapdeal.cocofs.entity.StateCategoryPriceTaxRate;
import com.snapdeal.cocofs.enums.BulkUploadValidationError;
import com.snapdeal.cocofs.enums.JobActionCode;
import com.snapdeal.cocofs.generic.persister.exception.GenericPersisterException;
import com.snapdeal.cocofs.generic.persister.response.GenericPersisterResponse;
import com.snapdeal.cocofs.mongo.model.SellerCategoryTaxRateUnit;
import com.snapdeal.cocofs.mongo.model.SellerSupcTaxRateUnit;
import com.snapdeal.cocofs.mongo.model.StateCategoryPriceTaxRateUnit;
import com.snapdeal.cocofs.services.IDataReader;
import com.snapdeal.cocofs.services.IDataUpdater;
import com.snapdeal.cocofs.services.IJobProcessor;
import com.snapdeal.cocofs.services.IJobSchedulerService;
import com.snapdeal.cocofs.services.common.dto.UserInfo;
import com.snapdeal.cocofs.services.data.engine.IDataEngine;
import com.snapdeal.cocofs.services.fm.IFulfilmentModelService;
import com.snapdeal.cocofs.services.fm.ISellerToSUPCListDBLookupService;
import com.snapdeal.cocofs.services.jobprocessor.SellerSupcFMMappingUploadProcessor.SELLER_SUPC_VALIDATION_MAPPING;
import com.snapdeal.cocofs.services.taxrate.ITaxRateUpdater;
import com.snapdeal.cocofs.services.taxrate.dto.SellerCategoryTaxRateUploadDTO;
import com.snapdeal.cocofs.services.taxrate.dto.SellerSUPCTaxRateUploadDTO;
import com.snapdeal.cocofs.services.taxrate.dto.StateCategoryDTO;
import com.snapdeal.cocofs.services.taxrate.dto.StateCategoryPriceTaxRateUploadDTO;
import com.snapdeal.cocofs.utils.ExcelReader;
import com.snapdeal.cocofs.utils.FTPUtils;
import com.sun.media.sound.InvalidDataException;

/**
 * @version 1.0, 13-Aug-2014
 * @author ankur
 */

@Service("taxRateUploadProcessor")
public class TaxRateUploadProcessor implements IJobProcessor {

    private static final Logger                                                                                       LOG = LoggerFactory.getLogger(TaxRateUploadProcessor.class);
    @Autowired
    private IJobSchedulerService                                                                                      jobService;

    @Autowired
    private IDataUpdater                                                                                              dataUpdater;

    @Autowired
    private ITaxRateUpdater                                                                                           taxRateDataUpdater;

    @Autowired
    private ITaxRateMongoService                                                                                      taxRateMongoService;

    @Autowired
    private ITaxRateDBDataReadService                                                                                 taxRateDBDataReadService;

    @Autowired
    @Qualifier("SellerSupcTaxRateDataReader")
    private IDataReader<SellerSUPCTaxRateUploadDTO, SellerSupcTaxRate, SellerSupcTaxRateUnit>                         sellerSupcdataReader;

    @Autowired
    @Qualifier("SellerSupcTaxRateDataEngine")
    private IDataEngine<SellerSUPCTaxRateUploadDTO, SellerSupcTaxRate, SellerSupcTaxRateUnit>                         sellerSupcdataEngine;

    @Autowired
    @Qualifier("SellerCategoryTaxRateDataReader")
    private IDataReader<SellerCategoryTaxRateUploadDTO, SellerCategoryTaxRate, SellerCategoryTaxRateUnit>             sellerCategorydataReader;

    @Autowired
    @Qualifier("SellerCategoryTaxRateDataEngine")
    private IDataEngine<SellerCategoryTaxRateUploadDTO, SellerCategoryTaxRate, SellerCategoryTaxRateUnit>             sellerCategorydataEngine;

    @Autowired
    @Qualifier("StateCategoryPriceTaxRateDataReader")
    private IDataReader<StateCategoryPriceTaxRateUploadDTO, StateCategoryPriceTaxRate, StateCategoryPriceTaxRateUnit> stateCategoryPricedataReader;

    @Autowired
    @Qualifier("StateCategoryPriceTaxRateDataEngine")
    private IDataEngine<StateCategoryPriceTaxRateUploadDTO, StateCategoryPriceTaxRate, StateCategoryPriceTaxRateUnit> stateCategoryPricedataEngine;

    @Autowired
    private IFulfilmentModelService                                                                                   fulfilmentService;

    @Autowired
    private ISellerToSUPCListDBLookupService                                                                          sellerToSUPCListLookupService;

    @PostConstruct
    public void init() {
        jobService.addJobProcessor(JobActionCode.STATE_CATEGORY_PRICE_TAX_RATE.getCode(), this);
        jobService.addJobProcessor(JobActionCode.SELLER_CATEGORY_TAX_RATE.getCode(), this);
        jobService.addJobProcessor(JobActionCode.SELLER_SUPC_TAX_RATE.getCode(), this);

    }

    @Override
    public JobProcessorResponse process(JobDetail jobDetail) {
        JobProcessorResponse response = new JobProcessorResponse();
        LOG.info("processing file:" + jobDetail.getFileName() + " from path:" + jobDetail.getFilePath() + " status:" + jobDetail.getStatus().getCode());
        String ftpServerPath = ConfigUtils.getStringScalar(Property.COCOFS_FTP_SERVER_PATH);
        String ftpUsername = ConfigUtils.getStringScalar(Property.COCOFS_FTP_USERNAME);
        String ftpPassword = ConfigUtils.getStringScalar(Property.COCOFS_FTP_PASSWORD);
        String filepath = jobDetail.getFilePath() + File.separator + jobDetail.getFileName();
        //         We save file on identical paths on local FS and FTP
        //        String filepath = "/home/nitish/Downloads/StateCategoryPriceTaxRate.xls";
        Map<String, List<String>> errorMap = new HashMap<String, List<String>>(0);
        boolean success = false;
        UserInfo userInfo = new UserInfo();
        userInfo.setEmail(StringUtils.isNotEmpty(jobDetail.getUploadedBy()) ? jobDetail.getUploadedBy() + ";" + jobDetail.getFileCode() : "BulkUpload");
        userInfo.setSuper(false);
        try {
            FTPUtils.downloadFile(ftpServerPath, ftpUsername, ftpPassword, filepath, filepath);

            if (JobActionCode.STATE_CATEGORY_PRICE_TAX_RATE.getCode().equals(jobDetail.getAction().getCode())) {
                errorMap = processStateCategoryPriceTaxRateData(filepath, errorMap, userInfo);
            } else if (JobActionCode.SELLER_CATEGORY_TAX_RATE.getCode().equals(jobDetail.getAction().getCode())) {
                errorMap = processSellerCategoryTaxRateData(filepath, errorMap, userInfo);
            } else if (JobActionCode.SELLER_SUPC_TAX_RATE.getCode().equals(jobDetail.getAction().getCode())) {
                errorMap = processSellerSUPCTaxRateData(filepath, errorMap, userInfo);
            } else {
                LOG.info("Invalid job action code " + jobDetail.getAction().getCode() + "for file" + filepath);

            }

            success = true;
        } catch (InvalidDataException e) {
            updateErrorMapForFileError(jobDetail, errorMap, e);
            LOG.error("Exception ", e);
        } catch (IOException e) {
            updateErrorMapForFileError(jobDetail, errorMap, e);
            LOG.error("Exception ", e);
        } catch (ClassNotFoundException e) {
            updateErrorMapForFileError(jobDetail, errorMap, e);
            LOG.error("Exception ", e);
        } catch (Exception e) {
            updateErrorMapForFileError(jobDetail, errorMap, e);
            LOG.error("Exception ", e);
        }
        response.setError(errorMap);
        if ((errorMap != null && errorMap.size() > 0) || !success) {
            response.setProcessingSuccessful(false);
        } else {
            response.setProcessingSuccessful(true);
        }
        return response;

    }

    private void updateErrorMapForFileError(JobDetail jobDetail, Map<String, List<String>> errorMap, Exception e) {
        if (!errorMap.containsKey("FileError")) {
            errorMap.put("FileError", new ArrayList<String>());

        }
        errorMap.get("FileError").add("Error in reading file for job " + jobDetail.getFileCode() + " Error Message " + e.getMessage());
    }

    //###########################################################################################################################################

    /**
     * Process Job For ActionType: SELLER_SUPC_TAX_RATE
     */
    private Map<String, List<String>> processSellerSUPCTaxRateData(String filePath, Map<String, List<String>> errorMap, UserInfo userInfo) throws ClassNotFoundException,
            InvalidDataException, IOException {

        Map<String, Set<String>> mapSellerSUPCMapping = new HashMap<String, Set<String>>();
        boolean bValidateSellerSUPCOnProcess = isValidateSellerSUPCOnProcess();

        List<SellerSUPCTaxRateUploadDTO> dtoList = ExcelReader.readFile(filePath, SellerSUPCTaxRateUploadDTO.class);
        for (SellerSUPCTaxRateUploadDTO dto : dtoList) {
            updateSellerSUPCTaxRate(dto, errorMap, userInfo, mapSellerSUPCMapping, bValidateSellerSUPCOnProcess);
        }
        return errorMap;
    }

    private Map<String, List<String>> updateSellerSUPCTaxRate(SellerSUPCTaxRateUploadDTO dto, Map<String, List<String>> errorMap, UserInfo userInfo,
            Map<String, Set<String>> mapSellerSUPCMapping, boolean bValidateSellerSUPCOnProcess) {
        try {

            if (bValidateSellerSUPCOnProcess) {
                String sellerCode = dto.getSellerCode();
                String supcCode = dto.getSupc();

                boolean bValidSellerSUPCMappingForDTO = isSellerSUPCMappingValid(sellerCode, supcCode, mapSellerSUPCMapping);

                if (!bValidSellerSUPCMappingForDTO) {
                    LOG.info("isSellerSUPCMappingValid returning {}, for seller:{}, supc:{}", bValidSellerSUPCMappingForDTO, sellerCode, supcCode);
                    //have a flag to identify the issue
                    updateErrorMapForSellerSupcRowError(dto, errorMap, BulkUploadValidationError.SELLER_SUPC_MAPPING_NON_EXIST.getDescription());
                    LOG.info("not persisting this record : {}", dto);
                    return errorMap;
                }
            }

            GenericPersisterResponse<SellerSupcTaxRate, SellerSupcTaxRateUnit> resp = dataUpdater.updateDataWithDTO(dto, sellerSupcdataReader, sellerSupcdataEngine, userInfo,
                    true, true);
            if (!resp.isSuccessful()) {
                updateErrorMapForSellerSupcRowError(dto, errorMap, resp.getMessage());
            } else {
                LOG.info("Successfully persisted for supc {} ", dto.getSupc());
            }
        } catch (OperationNotSupportedException e) {
            LOG.error("Looks like we already have data for supc {} ", dto.getSupc());
            updateErrorMapForSellerSupcRowError(dto, errorMap, e.getMessage());
        } catch (Exception e) {
            LOG.error("Error while invoking generic persistence wrapper for supc {} ", dto.getSupc(), e);
            updateErrorMapForSellerSupcRowError(dto, errorMap, e.getMessage());
        }
        return errorMap;
    }

    private void updateErrorMapForSellerSupcRowError(SellerSUPCTaxRateUploadDTO dto, Map<String, List<String>> errorMap, String message) {
        if (!errorMap.containsKey("RowError")) {
            errorMap.put("RowError", new ArrayList<String>());

        }
        errorMap.get("RowError").add("Error in row with SUPC : " + dto.getSupc() + " and seller : " + dto.getSellerCode() + " Error : " + message);
    }

    //###########################################################################################################################################

    /**
     * Process Job For ActionType: SELLER_CATEGORY_TAX_RATE
     */
    private Map<String, List<String>> processSellerCategoryTaxRateData(String filePath, Map<String, List<String>> errorMap, UserInfo userInfo) throws ClassNotFoundException,
            InvalidDataException, IOException {
        List<SellerCategoryTaxRateUploadDTO> dtoList = ExcelReader.readFile(filePath, SellerCategoryTaxRateUploadDTO.class);
        for (SellerCategoryTaxRateUploadDTO dto : dtoList) {

            updateSellerCategoryTaxRate(dto, errorMap, userInfo);
        }
        return errorMap;
    }

    private Map<String, List<String>> updateSellerCategoryTaxRate(SellerCategoryTaxRateUploadDTO dto, Map<String, List<String>> errorMap, UserInfo userInfo) {
        try {
            GenericPersisterResponse<SellerCategoryTaxRate, SellerCategoryTaxRateUnit> resp = dataUpdater.updateDataWithDTO(dto, sellerCategorydataReader,
                    sellerCategorydataEngine, userInfo, true, true);
            if (!resp.isSuccessful()) {
                updateErrorMapForSellerCategoryRowError(dto, errorMap, resp.getMessage());
            } else {
                LOG.info("Successfully persisted for seller {} ", dto.getSellerCode());
            }
        } catch (OperationNotSupportedException e) {
            LOG.error("Error while invoking generic persistence wrapper for seller {} ", dto.getSellerCode());
            updateErrorMapForSellerCategoryRowError(dto, errorMap, e.getMessage());
        } catch (Exception e) {
            LOG.error("Error while invoking generic persistence wrapper for supc {} ", dto.getSellerCode(), e);
            updateErrorMapForSellerCategoryRowError(dto, errorMap, e.getMessage());
        }
        return errorMap;
    }

    private void updateErrorMapForSellerCategoryRowError(SellerCategoryTaxRateUploadDTO dto, Map<String, List<String>> errorMap, String message) {
        if (!errorMap.containsKey("RowError")) {
            errorMap.put("RowError", new ArrayList<String>());

        }
        errorMap.get("RowError").add("Error in row with CategoryUrl : " + dto.getCategoryUrl() + " and seller : " + dto.getSellerCode() + " Error : " + message);
    }

    //###########################################################################################################################################

    /**
     * Process Job For ActionType: STATE_CATEGORY_PRICE_TAX_RATE
     * 
     * @Logic: For this type of taxRate we expect user to give all prices ranges again for a state-category combination
     *         each time he wants any change for that particular state-category tuple. For eg. if 5 rules rules exists
     *         for a state-category tuple in DB and user upload a 2 row this time, After execution of this method only
     *         these 2 rules will be enabled in DB ,all others will be in disabled state.
     */
    private Map<String, List<String>> processStateCategoryPriceTaxRateData(String filePath, Map<String, List<String>> errorMap, UserInfo userInfo) throws ClassNotFoundException,
            InvalidDataException, IOException {
        List<StateCategoryPriceTaxRateUploadDTO> dtoList = ExcelReader.readFile(filePath, StateCategoryPriceTaxRateUploadDTO.class, true);

        Map<StateCategoryDTO, List<StateCategoryPriceTaxRateUploadDTO>> stateCategoryToDtoMap = getStateCategoryToDtoMap(dtoList);

        //        List<StateCategoryPriceTaxRateUnit> toBeDisabledDocumentList = new ArrayList<StateCategoryPriceTaxRateUnit>();
        //        List<StateCategoryPriceTaxRate> toBeDisabledEntityList = new ArrayList<StateCategoryPriceTaxRate>();
        //        fetchStateCategoryPriceDocumentsAndEntitiesToBeDisabled(stateCategoryToDtoMap, toBeDisabledDocumentList, toBeDisabledEntityList);
        // disabling all old docs and entities which were not mentioned in uploaded sheet
        //         disableOldEntitiesAndDocsForStateCategoryPriceTaxRate(toBeDisabledDocumentList, toBeDisabledEntityList, errorMap, userInfo.getEmail());

        for (Entry<StateCategoryDTO, List<StateCategoryPriceTaxRateUploadDTO>> entry : stateCategoryToDtoMap.entrySet()) {
            StateCategoryDTO stateCategoryDTO = entry.getKey();
            updateDataForStateCategoryPriceDTO(stateCategoryDTO, entry.getValue(), errorMap, userInfo.getEmail());
        }

        return errorMap;
    }

    private void fetchStateCategoryPriceDocumentsAndEntitiesToBeDisabled(Map<StateCategoryDTO, List<StateCategoryPriceTaxRateUploadDTO>> stateCategoryToDtoMap,
            List<StateCategoryPriceTaxRateUnit> toBeDisabledDocumentList, List<StateCategoryPriceTaxRate> toBeDisabledEntityList) {
        List<StateCategoryPriceTaxRateUnit> allEnabledDocumentList = taxRateMongoService.getAllEnabledStateCategoryPriceTaxRateUnits();
        List<StateCategoryPriceTaxRate> allEnabledEntityList = taxRateDBDataReadService.getAllEnabledStateCategoryPriceTaxRates();
        for (StateCategoryPriceTaxRateUnit unit : allEnabledDocumentList) {
            StateCategoryDTO scDTO = new StateCategoryDTO(unit.getState(), unit.getCategoryUrl());
            if (stateCategoryToDtoMap != null && !stateCategoryToDtoMap.containsKey(scDTO)) {
                toBeDisabledDocumentList.add(unit);
            }
        }
        for (StateCategoryPriceTaxRate entity : allEnabledEntityList) {
            StateCategoryDTO scDTO = new StateCategoryDTO(entity.getState(), entity.getCategoryUrl());
            if (stateCategoryToDtoMap != null && !stateCategoryToDtoMap.containsKey(scDTO)) {
                toBeDisabledEntityList.add(entity);
            }
        }

    }

    private void updateDataForStateCategoryPriceDTO(StateCategoryDTO stateCategoryDTO, List<StateCategoryPriceTaxRateUploadDTO> uploadDtoList, Map<String, List<String>> errorMap,
            String userEmail) {

        List<StateCategoryPriceTaxRateUnit> documentList = taxRateMongoService.getStateCategoryPriceTaxRateUnit(stateCategoryDTO.getState(), stateCategoryDTO.getCategoryUrl());
        List<StateCategoryPriceTaxRate> entityList = taxRateDBDataReadService.getStateCategoryPriceTaxRate(stateCategoryDTO.getState(), stateCategoryDTO.getCategoryUrl());
        List<StateCategoryPriceTaxRateUnit> toBeUpdatedDocumentList = new ArrayList<StateCategoryPriceTaxRateUnit>();
        List<StateCategoryPriceTaxRate> toBeUpdatedEntityList = new ArrayList<StateCategoryPriceTaxRate>();

        List<StateCategoryPriceTaxRateUploadDTO> copyOfUploadDtoList = new ArrayList<StateCategoryPriceTaxRateUploadDTO>(uploadDtoList);

        createOrUpdateStateCategoryPrice(uploadDtoList, errorMap, userEmail, documentList, entityList, toBeUpdatedDocumentList, toBeUpdatedEntityList, copyOfUploadDtoList);

        // disabling all old docs and entities for this state and category
        //        disableOldEntitiesAndDocsForStateCategoryPriceTaxRate(documentList, entityList, errorMap, userEmail);

    }

    private void createOrUpdateStateCategoryPrice(List<StateCategoryPriceTaxRateUploadDTO> uploadDtoList, Map<String, List<String>> errorMap, String userEmail,
            List<StateCategoryPriceTaxRateUnit> documentList, List<StateCategoryPriceTaxRate> entityList, List<StateCategoryPriceTaxRateUnit> toBeUpdatedDocumentList,
            List<StateCategoryPriceTaxRate> toBeUpdatedEntityList, List<StateCategoryPriceTaxRateUploadDTO> copyOfUploadDtoList) {

        //Update records which are matched
        for (StateCategoryPriceTaxRateUploadDTO uploadDto : uploadDtoList) {
            toBeUpdatedDocumentList.clear();
            toBeUpdatedEntityList.clear();
            StateCategoryPriceTaxRateUnit matchedDocument = getMatchedStateCategoryPriceDocument(documentList, uploadDto);
            StateCategoryPriceTaxRate matchedEntity = getMatchedStateCategoryPriceEntity(entityList, uploadDto);
            if (null != matchedDocument && null != matchedEntity) {
                toBeUpdatedDocumentList.add(matchedDocument);
                documentList.remove(matchedDocument);
                toBeUpdatedEntityList.add(matchedEntity);
                entityList.remove(matchedEntity);
                copyOfUploadDtoList.remove(uploadDto);
                try {
                    toBeUpdatedDocumentList = stateCategoryPricedataEngine.enrichDocuments(uploadDto, toBeUpdatedDocumentList);
                    toBeUpdatedEntityList = stateCategoryPricedataEngine.enrichEntities(uploadDto, toBeUpdatedEntityList, userEmail);
                    GenericPersisterResponse<StateCategoryPriceTaxRate, StateCategoryPriceTaxRateUnit> resp = taxRateDataUpdater.updateStateCategoryPriceTaxRate(
                            toBeUpdatedEntityList, toBeUpdatedDocumentList);
                    if (!resp.isSuccessful()) {
                        updateErrorMapForStateCategoryPriceRowError(uploadDto, errorMap, resp.getMessage());
                    }
                } catch (OperationNotSupportedException e) {
                    LOG.error("Exception ", e);
                } catch (GenericPersisterException e) {
                    LOG.error("Exception ", e);
                } catch (Exception e) {
                    LOG.error("Error", e);
                }
            }

        }

        //        overrides old records for same state and category which are not matched and creates new if new records are greater than old records
        for (StateCategoryPriceTaxRateUploadDTO uploadDto : copyOfUploadDtoList) {
            toBeUpdatedDocumentList.clear();
            toBeUpdatedEntityList.clear();
            if (!CollectionUtils.isEmpty(documentList) && !CollectionUtils.isEmpty(entityList)) {
                documentList.get(0).setCreated(null);
                entityList.get(0).setCreated(null);
                toBeUpdatedDocumentList.add(documentList.get(0));
                documentList.remove(documentList.get(0));
                toBeUpdatedEntityList.add(entityList.get(0));
                entityList.remove(entityList.get(0));
            }
            try {
                toBeUpdatedDocumentList = stateCategoryPricedataEngine.enrichDocuments(uploadDto, toBeUpdatedDocumentList);
                toBeUpdatedEntityList = stateCategoryPricedataEngine.enrichEntities(uploadDto, toBeUpdatedEntityList, userEmail);
                GenericPersisterResponse<StateCategoryPriceTaxRate, StateCategoryPriceTaxRateUnit> resp = taxRateDataUpdater.updateStateCategoryPriceTaxRate(toBeUpdatedEntityList,
                        toBeUpdatedDocumentList);
                if (!resp.isSuccessful()) {
                    updateErrorMapForStateCategoryPriceRowError(uploadDto, errorMap, resp.getMessage());
                }
            } catch (OperationNotSupportedException e) {
                LOG.error("Exception ", e);
            } catch (GenericPersisterException e) {
                LOG.error("Exception ", e);
            } catch (Exception e) {
                LOG.error("Error", e);
            }

        }

        disableOldEntitiesAndDocsForStateCategoryPriceTaxRate(documentList, entityList, errorMap, userEmail);
    }

    private StateCategoryPriceTaxRate getMatchedStateCategoryPriceEntity(List<StateCategoryPriceTaxRate> entityList, StateCategoryPriceTaxRateUploadDTO uploadDto) {
        if (!CollectionUtils.isEmpty(entityList)) {
            for (StateCategoryPriceTaxRate entity : entityList) {
                if (entity.getState().equals(uploadDto.getState()) && entity.getCategoryUrl().equals(uploadDto.getSubCategoryUrl())) {

                    if ((entity.getLowerPriceLimit() == null && uploadDto.getLowerPriceLimit() == null)
                            || (entity.getLowerPriceLimit() != null && entity.getLowerPriceLimit().equals(uploadDto.getLowerPriceLimit()))) {

                        if ((entity.getUpperPriceLimit() == null && uploadDto.getUpperPriceLimit() == null)
                                || (entity.getUpperPriceLimit() != null && entity.getUpperPriceLimit().equals(uploadDto.getUpperPriceLimit()))) {
                            return entity;
                        }
                    }
                }
            }
        }
        return null;
    }

    private StateCategoryPriceTaxRateUnit getMatchedStateCategoryPriceDocument(List<StateCategoryPriceTaxRateUnit> documentList, StateCategoryPriceTaxRateUploadDTO uploadDto) {
        if (!CollectionUtils.isEmpty(documentList)) {
            for (StateCategoryPriceTaxRateUnit document : documentList) {
                if (document.getState().equals(uploadDto.getState()) && document.getCategoryUrl().equals(uploadDto.getSubCategoryUrl())) {

                    if ((document.getLowerPriceLimit() == null && uploadDto.getLowerPriceLimit() == null)
                            || (document.getLowerPriceLimit() != null && document.getLowerPriceLimit().equals(uploadDto.getLowerPriceLimit()))) {

                        if ((document.getUpperPriceLimit() == null && uploadDto.getUpperPriceLimit() == null)
                                || (document.getUpperPriceLimit() != null && document.getUpperPriceLimit().equals(uploadDto.getUpperPriceLimit()))) {
                            return document;
                        }
                    }
                }
            }
        }
        return null;
    }

    private Map<StateCategoryDTO, List<StateCategoryPriceTaxRateUploadDTO>> getStateCategoryToDtoMap(List<StateCategoryPriceTaxRateUploadDTO> dtoList) {
        Map<StateCategoryDTO, List<StateCategoryPriceTaxRateUploadDTO>> stateCategoryToDtoMap = new HashMap<StateCategoryDTO, List<StateCategoryPriceTaxRateUploadDTO>>();
        if (!CollectionUtils.isEmpty(dtoList)) {
            for (StateCategoryPriceTaxRateUploadDTO dto : dtoList) {
                StateCategoryDTO stateCategoryDTO = new StateCategoryDTO(dto.getState(), dto.getSubCategoryUrl());
                if (!stateCategoryToDtoMap.containsKey(stateCategoryDTO)) {
                    stateCategoryToDtoMap.put(stateCategoryDTO, new ArrayList<StateCategoryPriceTaxRateUploadDTO>());
                }
                stateCategoryToDtoMap.get(stateCategoryDTO).add(dto);
            }
        }
        return stateCategoryToDtoMap;
    }

    private void updateErrorMapForStateCategoryPriceRowError(StateCategoryPriceTaxRateUploadDTO dto, Map<String, List<String>> errorMap, String message) {
        if (!errorMap.containsKey("RowError")) {
            errorMap.put("RowError", new ArrayList<String>());

        }
        errorMap.get("RowError").add("Error in row with CategoryUrl : " + dto.getSubCategoryUrl() + " and state : " + dto.getState() + " Error : " + message);
    }

    private void disableOldEntitiesAndDocsForStateCategoryPriceTaxRate(List<StateCategoryPriceTaxRateUnit> documentList, List<StateCategoryPriceTaxRate> entityList,
            Map<String, List<String>> errorMap, String userEmail) {

        if (!CollectionUtils.isEmpty(documentList)) {
            for (StateCategoryPriceTaxRateUnit doc : documentList) {
                LOG.info("disabling old document of StateCategoryPriceTaxRateUnit  {}", doc);
                doc.setUpdated(DateUtils.getCurrentTime());
                doc.setEnabled(false);
            }
        }
        if (!CollectionUtils.isEmpty(entityList)) {
            for (StateCategoryPriceTaxRate entity : entityList) {
                LOG.info("disabling old entity of StateCategoryPriceTaxRate  {}", entity);
                entity.setLastUpdated(DateUtils.getCurrentTime());
                entity.setEnabled(false);
                entity.setUpdatedBy(userEmail);
            }
        }

        GenericPersisterResponse<StateCategoryPriceTaxRate, StateCategoryPriceTaxRateUnit> resp;
        try {
            if (!CollectionUtils.isEmpty(entityList)) {
                resp = taxRateDataUpdater.updateStateCategoryPriceTaxRate(entityList, documentList);
                if (!resp.isSuccessful()) {
                    updateErrorMapForDisableError(errorMap, resp.getMessage());
                }
            }
        } catch (GenericPersisterException e) {
            updateErrorMapForDisableError(errorMap, e.getMessage());
            LOG.error("Error", e);
        } catch (Exception e) {
            updateErrorMapForDisableError(errorMap, e.getMessage());
            LOG.error("Error", e);
        }

    }

    private void updateErrorMapForDisableError(Map<String, List<String>> errorMap, String message) {
        if (!errorMap.containsKey("RowError")) {
            errorMap.put("RowError", new ArrayList<String>());

        }
        errorMap.get("RowError").add("StateCategoryPrice Level taxrate cannot be disabled. Error : " + message);

    }

    //###########################################################################################################################################

    @Override
    public BulkUploadValidationResponse validateJob(File file, String actionCode, String param) {
        BulkUploadValidationResponse response = new BulkUploadValidationResponse();
        try {
            if (JobActionCode.STATE_CATEGORY_PRICE_TAX_RATE.getCode().equals(actionCode)) {
                response = validateForStateCatgoryPriceTypeJob(file);
            } else if (JobActionCode.SELLER_CATEGORY_TAX_RATE.getCode().equals(actionCode)) {
                response = validateForSellerCatgoryTypeJob(file);
            } else if (JobActionCode.SELLER_SUPC_TAX_RATE.getCode().equals(actionCode)) {
                response = validateForSellerSUPCTypeJob(file);
            } else {
                response.setValid(false);
                response.setErrorMessage("Invalid action code " + actionCode + "for file" + file.getAbsoluteFile());
            }
        } catch (InvalidDataException e) {
            response.setValid(false);
            response.setErrorMessage(BulkUploadValidationError.INVALID_DATA_VALUE.getDescription() + " " + e.getMessage());
            LOG.error("Error while uploading the data for file {} for action type {} ", file.getAbsoluteFile(), actionCode, e);
        } catch (IOException e) {
            response.setValid(false);
            response.setErrorMessage(BulkUploadValidationError.UPLOAD_FILE_READ_ERROR.getDescription());
            LOG.error("Error while uploading the data for file {} for action type {} ", file.getAbsoluteFile(), actionCode, e);
        } catch (ClassNotFoundException e) {
            response.setValid(false);
            response.setErrorMessage(BulkUploadValidationError.FILE_TYPE_NOT_CORRECT.getDescription());
            LOG.error("Error while uploading the data for file {} for action type {} ", file.getAbsoluteFile(), actionCode, e);
        }
        return response;
    }

    public BulkUploadValidationResponse validateForStateCatgoryPriceTypeJob(File file) throws ClassNotFoundException, InvalidDataException, IOException {
        BulkUploadValidationResponse response = new BulkUploadValidationResponse();
        boolean emptyFieldAllowed = true;

        List<StateCategoryPriceTaxRateUploadDTO> dtoList = ExcelReader.readFile(file.getAbsolutePath(), StateCategoryPriceTaxRateUploadDTO.class, emptyFieldAllowed, 0, false, true);

        List<BulkUploadValidationDTO> validationDTOList = new ArrayList<BulkUploadValidationDTO>();

        StateCache stateCache = CacheManager.getInstance().getCache(StateCache.class);
        ProductCategoryCache productCategoryCache = CacheManager.getInstance().getCache(ProductCategoryCache.class);
        int row = 0;
        StringBuilder sb = new StringBuilder();

        Map<StateCategoryDTO, String> map = new HashMap<StateCategoryDTO, String>();
        if (CollectionUtils.isEmpty(dtoList)) {
            validationDTOList.add(new BulkUploadValidationDTO(row, BulkUploadValidationError.INVALID_DATA_VALUE.getDescription(), ""));
            sb.append("No related data found in sheet for  State Category bulk upload" + "<br>");

        }

        for (StateCategoryPriceTaxRateUploadDTO dto : dtoList) {
            row++;

            if (dto.getEnabled() == null || dto.getTaxType() == null) {
                validationDTOList.add(new BulkUploadValidationDTO(row, BulkUploadValidationError.INVALID_DATA_VALUE.getDescription(), null));
                sb.append("Values for both columns TaxType and Enabled are mandatory :  from rows : " + row + "<br>");
                break;
            }

            if ((dto.getEnabled() == true && dto.getTaxRate() == null) || (dto.getTaxRate() != null && dto.getTaxRate() < 0)) {
                validationDTOList.add(new BulkUploadValidationDTO(row, BulkUploadValidationError.INVALID_DATA_VALUE.getDescription(), null));
                sb.append("invalid tax rate value from rows : " + row + "<br>");
            }

            if (dto.getTaxRate() != null && !isValidDecimal(dto.getTaxRate())) {
                validationDTOList.add(new BulkUploadValidationDTO(row, BulkUploadValidationError.INVALID_DATA_VALUE.getDescription(), null));
                sb.append("Tax Rate must be less than or equal to " + ConfigUtils.getIntegerScalar(Property.TAX_RATE_MAX_VALUE) + " and must have "
                        + ConfigUtils.getIntegerScalar(Property.ROUND_OFF_SCALE) + " digits after decimal : in rows : " + row + "<br>");
            }

            isValidTaxTypeForStateCategory(validationDTOList, row, sb, map, dto);

            if (dto.getLowerPriceLimit() != null && dto.getLowerPriceLimit() < 0) {
                validationDTOList.add(new BulkUploadValidationDTO(row, BulkUploadValidationError.INVALID_DATA_VALUE.getDescription(), dto.getLowerPriceLimit().toString()));
                sb.append("negative lower Price from rows : " + row + "<br>");
            }

            if (dto.getUpperPriceLimit() != null && dto.getUpperPriceLimit() < 0) {
                validationDTOList.add(new BulkUploadValidationDTO(row, BulkUploadValidationError.INVALID_DATA_VALUE.getDescription(), dto.getUpperPriceLimit().toString()));
                sb.append("negative upper Price from rows : " + row + "<br>");
            }

            if (dto.getUpperPriceLimit() != null && dto.getLowerPriceLimit() != null && dto.getUpperPriceLimit() < dto.getLowerPriceLimit()) {
                validationDTOList.add(new BulkUploadValidationDTO(row, BulkUploadValidationError.INVALID_DATA_VALUE.getDescription(), dto.getUpperPriceLimit().toString()));
                sb.append("upper Price should be more than lower price from rows : " + row + "<br>");
            }

            if (StringUtils.isEmpty(dto.getSubCategoryUrl())) {
                validationDTOList.add(new BulkUploadValidationDTO(row, BulkUploadValidationError.INVALID_DATA_VALUE.getDescription(), dto.getSubCategoryUrl()));
                sb.append("empty category url from rows : " + row + "<br>");
            } else if (!productCategoryCache.isValidSubCat(dto.getSubCategoryUrl())) {
                validationDTOList.add(new BulkUploadValidationDTO(row, BulkUploadValidationError.INVALID_DATA_VALUE.getDescription(), dto.getSubCategoryUrl()));
                sb.append("invalid category url from rows : " + row + "<br>");
            }

            if (StringUtils.isEmpty(dto.getState())) {
                validationDTOList.add(new BulkUploadValidationDTO(row, BulkUploadValidationError.INVALID_DATA_VALUE.getDescription(), dto.getState()));
                sb.append("empty state name from rows : " + row + "<br>");
            } else if (stateCache == null) {
                validationDTOList.add(new BulkUploadValidationDTO(row, BulkUploadValidationError.INVALID_DATA_VALUE.getDescription(), dto.getState()));
                sb.append("internal server error: <b> something bad happened</b> <br>");
            } else if (stateCache != null && !stateCache.isValidState(dto.getState())) {
                validationDTOList.add(new BulkUploadValidationDTO(row, BulkUploadValidationError.INVALID_DATA_VALUE.getDescription(), dto.getState()));
                sb.append("invalid state name: <b>" + dto.getState() + "</b> from rows : " + row + "<br>");
            }

        }

        if (!validationDTOList.isEmpty()) {
            response.setValid(false);
            response.setErrorMessage(sb.toString());
            response.setList(validationDTOList);
            return response;
        }

        Map<StateCategoryDTO, List<StateCategoryPriceTaxRateUploadDTO>> stateCategoryToDtoMap = getStateCategoryToDtoMap(dtoList);

        for (Entry<StateCategoryDTO, List<StateCategoryPriceTaxRateUploadDTO>> entry : stateCategoryToDtoMap.entrySet()) {
            List<StateCategoryPriceTaxRateUploadDTO> stateCategoryWiseList = entry.getValue();
            if (isPriceRangeOverlapping(stateCategoryWiseList)) {
                response.setErrorMessage("conflicting price ranges found for state " + entry.getKey().getState() + " category " + entry.getKey().getCategoryUrl());
                response.setValid(false);
                return response;
            }
        }

        response.setValid(true);
        return response;

    }

    private void isValidTaxTypeForStateCategory(List<BulkUploadValidationDTO> validationDTOList, int row, StringBuilder sb, Map<StateCategoryDTO, String> map,
            StateCategoryPriceTaxRateUploadDTO dto) {
        StateCategoryDTO stateCategoryDTO = new StateCategoryDTO(dto.getState(), dto.getSubCategoryUrl());
        String tax;
        if (map.get(stateCategoryDTO) == null) {
            tax = configureTaxType(dto);
            map.put(stateCategoryDTO, tax);
        } else {
            String dtoTax = map.get(stateCategoryDTO);
            tax = configureTaxType(dto);
            if (tax != null && !tax.equals(dtoTax)) {
                validationDTOList.add(new BulkUploadValidationDTO(row, BulkUploadValidationError.INVALID_DATA_VALUE.getDescription(), null));
                sb.append("All records of state <b> " + stateCategoryDTO.getState() + "</b>  and subcategory url <b> " + stateCategoryDTO.getCategoryUrl()
                        + "</b>  must have same tax type from rows " + row + "<br>");
            }
        }
    }

    private String configureTaxType(StateCategoryPriceTaxRateUploadDTO dto) {
        return dto.getTaxType();
    }

    private boolean isPriceRangeOverlapping(List<StateCategoryPriceTaxRateUploadDTO> stateCategoryWiseList) {

        if (stateCategoryWiseList == null || stateCategoryWiseList.size() <= 1) {
            return false;
        }
        for (int i = 0; i < stateCategoryWiseList.size(); i++) {
            for (int j = i + 1; j < stateCategoryWiseList.size(); j++) {
                StateCategoryPriceTaxRateUploadDTO dto0 = stateCategoryWiseList.get(i);
                StateCategoryPriceTaxRateUploadDTO dto1 = stateCategoryWiseList.get(j);
                Double a0 = dto0.getLowerPriceLimit() == null ? 0 : dto0.getLowerPriceLimit();
                Double a1 = dto0.getUpperPriceLimit() == null ? Double.MAX_VALUE : dto0.getUpperPriceLimit();
                Double b0 = dto1.getLowerPriceLimit() == null ? 0 : dto1.getLowerPriceLimit();
                Double b1 = dto1.getUpperPriceLimit() == null ? Double.MAX_VALUE : dto1.getUpperPriceLimit();

                if (a0 <= b1 && b0 <= a1) {
                    LOG.info("conflicting price ranges found for state {} , category {} ", dto0.getState(), dto0.getSubCategoryUrl());
                    return true;
                }

            }
        }

        return false;

    }

    public BulkUploadValidationResponse validateForSellerCatgoryTypeJob(File file) throws ClassNotFoundException, InvalidDataException, IOException {
        BulkUploadValidationResponse response = new BulkUploadValidationResponse();
        List<SellerCategoryTaxRateUploadDTO> dtoList = ExcelReader.readFile(file.getAbsolutePath(), SellerCategoryTaxRateUploadDTO.class, false, 0, false, true);
        Set<SellerCategoryTaxRateUploadDTO> dtoSet = new HashSet<SellerCategoryTaxRateUploadDTO>(dtoList);

        if (dtoList.size() != dtoSet.size()) {
            LOG.info("List size dtoList.size() {} set size {}", dtoList.size(), dtoSet.size());
            response.setErrorMessage("Duplicate seller-category combination exists in the file");
            response.setValid(false);
            return response;
        }

        ProductCategoryCache productCategoryCache = CacheManager.getInstance().getCache(ProductCategoryCache.class);

        Map<String, Boolean> map = new HashMap<String, Boolean>();
        StringBuilder sb = new StringBuilder();
        int row = 0;
        List<BulkUploadValidationDTO> validationDTOList = new ArrayList<BulkUploadValidationDTO>();

        if (CollectionUtils.isEmpty(dtoList)) {
            validationDTOList.add(new BulkUploadValidationDTO(row, BulkUploadValidationError.INVALID_DATA_VALUE.getDescription(), ""));
            sb.append("No related data found in sheet for Seller Category bulk upload" + "<br>");

        }

        for (SellerCategoryTaxRateUploadDTO dto : dtoList) {
            row++;

            if (dto.getEnabled() == null || dto.getTaxType() == null) {
                validationDTOList.add(new BulkUploadValidationDTO(row, BulkUploadValidationError.INVALID_DATA_VALUE.getDescription(), null));
                sb.append("Values for both columns TaxType and Enabled are mandatory :  from rows : " + row + "<br>");
                break;
            }

            if ((dto.getEnabled() == true && dto.getTaxRate() == null) || (dto.getTaxRate() != null && dto.getTaxRate() < 0)) {
                validationDTOList.add(new BulkUploadValidationDTO(row, BulkUploadValidationError.INVALID_DATA_VALUE.getDescription(), null));
                sb.append("invalid tax rate value from rows : " + row + "<br>");
            }

            if (dto.getTaxRate() != null && !isValidDecimal(dto.getTaxRate())) {
                validationDTOList.add(new BulkUploadValidationDTO(row, BulkUploadValidationError.INVALID_DATA_VALUE.getDescription(), null));
                sb.append("Tax Rate must be less than or equal to " + ConfigUtils.getIntegerScalar(Property.TAX_RATE_MAX_VALUE) + " and must have "
                        + ConfigUtils.getIntegerScalar(Property.ROUND_OFF_SCALE) + " digits after decimal : in rows : " + row + "<br>");
            }

            //get from local map to reduce aerospike hit for same seller
            String sellerCode = dto.getSellerCode();
            // use local flag as it may be reset
            boolean bValidSellerCodeForDTO = true;
            if (!map.containsKey(sellerCode)) {
                bValidSellerCodeForDTO = isSellerExists(sellerCode);
                map.put(sellerCode, bValidSellerCodeForDTO);
            } else {
                bValidSellerCodeForDTO = map.get(sellerCode);
            }

            if (!bValidSellerCodeForDTO) {
                validationDTOList.add(new BulkUploadValidationDTO(row, BulkUploadValidationError.SELLER_NON_EXIST.getDescription(), null));
                sb.append("invalid value for sellerCode column from rows : " + row + "<br>");
            }

            if (StringUtils.isEmpty(dto.getCategoryUrl())) {
                validationDTOList.add(new BulkUploadValidationDTO(row, BulkUploadValidationError.INVALID_DATA_VALUE.getDescription(), dto.getCategoryUrl()));
                sb.append("empty category url from rows : " + row + "<br>");
            } else if (!productCategoryCache.isValidSubCat(dto.getCategoryUrl())) {
                validationDTOList.add(new BulkUploadValidationDTO(row, BulkUploadValidationError.INVALID_DATA_VALUE.getDescription(), dto.getCategoryUrl()));
                sb.append("invalid category url from rows : " + row + "<br>");
            }

        }

        if (!validationDTOList.isEmpty()) {
            response.setValid(false);
            response.setErrorMessage(sb.toString());
            response.setList(validationDTOList);
            return response;
        }

        response.setValid(true);
        return response;

    }

    public BulkUploadValidationResponse validateForSellerSUPCTypeJob(File file) throws ClassNotFoundException, InvalidDataException, IOException {
        BulkUploadValidationResponse response = new BulkUploadValidationResponse();
        List<SellerSUPCTaxRateUploadDTO> dtoList = ExcelReader.readFile(file.getAbsolutePath(), SellerSUPCTaxRateUploadDTO.class, false, 0, false, true);
        Set<SellerSUPCTaxRateUploadDTO> dtoSet = new HashSet<SellerSUPCTaxRateUploadDTO>(dtoList);

        if (dtoList.size() != dtoSet.size()) {
            LOG.info("List size dtoList.size() {} set size {}", dtoList.size(), dtoSet.size());
            response.setErrorMessage("Duplicate seller-supc combination exists in the file");
            response.setValid(false);
            return response;
        }
        Map<String, Boolean> map = new HashMap<String, Boolean>();
        Map<String, Set<String>> mapSellerSUPCMapping = new HashMap<String, Set<String>>();
        boolean bValidateSellerSUPCEagerOnUpload = isValidateSellerSUPCEagerOnUpload();
        StringBuilder sb = new StringBuilder();
        int row = 0;

        List<BulkUploadValidationDTO> validationDTOList = new ArrayList<BulkUploadValidationDTO>();
        if (CollectionUtils.isEmpty(dtoList)) {
            validationDTOList.add(new BulkUploadValidationDTO(row, BulkUploadValidationError.INVALID_DATA_VALUE.getDescription(), ""));
            sb.append("No related data found in sheet for Seller Supc bulk upload" + "<br>");

        }
        for (SellerSUPCTaxRateUploadDTO dto : dtoList) {
            row++;

            if (dto.getEnabled() == null || dto.getTaxType() == null) {
                validationDTOList.add(new BulkUploadValidationDTO(row, BulkUploadValidationError.INVALID_DATA_VALUE.getDescription(), null));
                sb.append("Values for both columns TaxType and Enabled are mandatory :  from rows : " + row + "<br>");
                break;
            }

            if ((dto.getEnabled() == true && dto.getTaxRate() == null) || (dto.getTaxRate() != null && dto.getTaxRate() < 0)) {
                validationDTOList.add(new BulkUploadValidationDTO(row, BulkUploadValidationError.INVALID_DATA_VALUE.getDescription(), null));
                sb.append("invalid tax rate value from rows : " + row + "<br>");
            }

            if (dto.getTaxRate() != null && !isValidDecimal(dto.getTaxRate())) {
                validationDTOList.add(new BulkUploadValidationDTO(row, BulkUploadValidationError.INVALID_DATA_VALUE.getDescription(), null));
                sb.append("Tax Rate must be less than or equal to " + ConfigUtils.getIntegerScalar(Property.TAX_RATE_MAX_VALUE) + " and must have "
                        + ConfigUtils.getIntegerScalar(Property.ROUND_OFF_SCALE) + " digits after decimal : in rows : " + row + "<br>");
            }

            //get from local map to reduce aerospike hit for same seller
            String sellerCode = dto.getSellerCode();
            // use local flag as it may be reset
            boolean bValidSellerCodeForDTO = true;
            if (!map.containsKey(sellerCode)) {
                bValidSellerCodeForDTO = isSellerExists(sellerCode);
                map.put(sellerCode, bValidSellerCodeForDTO);
            } else {
                bValidSellerCodeForDTO = map.get(sellerCode);
            }

            if (!bValidSellerCodeForDTO) {
                validationDTOList.add(new BulkUploadValidationDTO(row, BulkUploadValidationError.SELLER_NON_EXIST.getDescription(), dto.getSellerCode()));
                sb.append("invalid value for sellerCode column from rows : " + row + ", sellerCode : " + dto.getSellerCode() + "<br>");
                // seller code is not valid, no more validation is needed.
                continue;
            }

            if (bValidateSellerSUPCEagerOnUpload) {
                boolean bSellerSUPCMappingValid = isSellerSUPCMappingValid(dto.getSellerCode(), dto.getSupc(), mapSellerSUPCMapping);
                if (!bSellerSUPCMappingValid) {
                    validationDTOList.add(new BulkUploadValidationDTO(row, BulkUploadValidationError.SELLER_SUPC_MAPPING_NON_EXIST.getDescription(), dto.getSellerCode() + " - "
                            + dto.getSupc()));
                    sb.append("invalid value for sellerCode and supc combination from rows : " + row + ", sellerCode : " + dto.getSellerCode() + ", supc : " + dto.getSupc()
                            + "<br>");
                }
            }
        }
        if (!validationDTOList.isEmpty()) {
            response.setValid(false);
            response.setErrorMessage(sb.toString());
            response.setList(validationDTOList);
            return response;
        }

        response.setValid(true);
        return response;

    }

    private boolean isValidDecimal(Double taxValue) {

        if (taxValue > ConfigUtils.getDoubleScalar(Property.TAX_RATE_MAX_VALUE))
            return false;

        int decimalPlaces = ConfigUtils.getIntegerScalar(Property.ROUND_OFF_SCALE);
        if (decimalPlaces <= 0) {
            if (taxValue.toString().indexOf(".") == -1) {
                return true;
            } else {
                Double left = BigDecimal.valueOf(taxValue).subtract(BigDecimal.valueOf(taxValue.longValue())).doubleValue();
                if (left == 0.0) {
                    return true;
                }
                return false;

            }

        }

        if (taxValue.toString().indexOf(".") == -1) {
            return false;
        } else {
            if (taxValue.toString().substring(taxValue.toString().indexOf(".") + 1, taxValue.toString().length()).length() > decimalPlaces) {
                return false;
            }
            return true;
        }
    }

    private boolean isSellerSUPCMappingValid(String sellerCode, String supcCode, Map<String, Set<String>> mapSellerSUPCMapping) {

        boolean bResult = false;
        Set<String> supcSet = null;
        if (!mapSellerSUPCMapping.containsKey(sellerCode)) {
            supcSet = getSetOfSUPCsForSpecifiedSeller(sellerCode);
            mapSellerSUPCMapping.put(sellerCode, supcSet);
        } else {
            supcSet = mapSellerSUPCMapping.get(sellerCode);
        }
        if (supcSet != null && !supcSet.isEmpty()) {
            if (supcSet.contains(supcCode)) {
                bResult = true;
            }
        }
        return bResult;
    }

    private Set<String> getSetOfSUPCsForSpecifiedSeller(String sellerCode) {
        try {
            List<String> supcList = sellerToSUPCListLookupService.getSupcListForSeller(sellerCode);
            if (supcList == null) {
                supcList = new ArrayList<String>();
            }
            return new TreeSet<String>(supcList);
        } catch (Exception e) {
            LOG.error("Error while fetching seller-supc data for seller {}; Exception:{} ", sellerCode, e);
        }
        return null;
    }

    private boolean isValidateSellerSUPCEagerOnUpload() {
        String pVal = getValidateSellerSUPCPropertyValue();
        boolean b = false;
        if (SELLER_SUPC_VALIDATION_MAPPING.ON_UPLOAD.toString().equalsIgnoreCase(pVal) || SELLER_SUPC_VALIDATION_MAPPING.ALWAYS.toString().equalsIgnoreCase(pVal)) {
            b = true;
        }
        return b;
    }

    private boolean isValidateSellerSUPCOnProcess() {
        String pVal = getValidateSellerSUPCPropertyValue();
        boolean b = false;
        if (SELLER_SUPC_VALIDATION_MAPPING.ON_PROCESS.toString().equalsIgnoreCase(pVal) || SELLER_SUPC_VALIDATION_MAPPING.ALWAYS.toString().equalsIgnoreCase(pVal)) {
            b = true;
        }
        return b;
    }

    private String getValidateSellerSUPCPropertyValue() {
        Property p = Property.VALIDATE_SELLER_SUPC_MAPPING;
        String value = ConfigUtils.getStringScalar(p);
        LOG.info("Property {} is set to {}, for validate seller-supc mapping value ", p, value);
        if (value == null) {
            value = SELLER_SUPC_VALIDATION_MAPPING.DISABLED.toString();
        }
        return value;
    }

    /**
     * Check if any valid seller-fm maping exists for the seller code
     * 
     * @param sellerCode
     * @return
     */
    private boolean isSellerExists(String sellerCode) {
        try {
            return fulfilmentService.isSellerExists(sellerCode);
        } catch (Exception e) {
            LOG.error("Error while checking existence of seller {}; Exception:{} ", sellerCode, e);
        }
        return false;
    }

}
