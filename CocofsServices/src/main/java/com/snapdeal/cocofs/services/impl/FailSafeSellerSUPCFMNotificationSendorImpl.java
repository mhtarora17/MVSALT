package com.snapdeal.cocofs.services.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.generic.persister.exception.GenericPersisterException;
import com.snapdeal.cocofs.services.events.IEventProducer;
import com.snapdeal.cocofs.services.events.dto.FCAddressUpdateEventObj;
import com.snapdeal.cocofs.services.events.dto.FCAttributeUpdateEventObj;
import com.snapdeal.cocofs.services.events.dto.SellerFMFCUpdateEventObj;
import com.snapdeal.cocofs.services.events.dto.SellerSupcAddEventObj;
import com.snapdeal.cocofs.services.events.dto.SellerSupcFMFCExceptionAddEventObj;
import com.snapdeal.cocofs.services.events.dto.SellerSupcFMFCExceptionDeleteEventObj;
import com.snapdeal.cocofs.services.fm.ISellerSUPCFMFCMappingProcessor;
import com.snapdeal.cocofs.services.fm.ISellerSUPCFMMappingProcessor;
import com.snapdeal.cocofs.services.fm.ISellerSupcSdInstantMappingProcessor;
import com.snapdeal.cocofs.services.fm.ISellerSupcShipFromMappingProcessor;
import com.snapdeal.score.data.aerospike.exception.GetFailedException;
import com.snapdeal.score.data.aerospike.exception.PutFailedException;

/**
 * Class to process Seller SUPC FM FC mapping data update in fail safe manner. If push attempt is not successful
 * (derived via a flag), an event object is pushed in data source and later retried by task upto max re-attempts. <br>
 * Collect the notifications and push to external service to publish these notification to activemq (toward search/other
 * external listeners) <br>
 * 1) SALESFORCE send SELLER-FM mapping add/updates via API hit <br>
 * 2) IPSM send SELLER-SUPC updates (new SUPC addition for SELLER) <br>
 * 3) CoCoFS Admin send SELLER-SUPC-FM/FC mappings to update/disable exceptions.<br>
 * Data to be pushed - Seller, SUPC, FM, List<FC>
 * 
 * @author ssuthar/ankur
 */

@Service("failSafeSellerSUPCFMNotificationSendor")
public class FailSafeSellerSUPCFMNotificationSendorImpl implements ISellerSUPCFMMappingProcessor,ISellerSupcShipFromMappingProcessor,ISellerSupcSdInstantMappingProcessor {

    private static final Logger                                   LOG = LoggerFactory.getLogger(FailSafeSellerSUPCFMNotificationSendorImpl.class);

    // FM change event producers below
    @Autowired
    @Qualifier("sellerSUPCFMMappingProcessor")
    private ISellerSUPCFMMappingProcessor                         fmUpdateListener;
    
 
    @Autowired
    @Qualifier("SellerFMUpdateEventProducer")
    private IEventProducer<SellerFMFCUpdateEventObj>              sellerFMUpdateEventProducer;

    @Autowired
    @Qualifier("SellerSupcAddFMUpdateEventProducer")
    private IEventProducer<SellerSupcAddEventObj>                 sellerSupcAddFMUpdateEventProducer;

    @Autowired
    @Qualifier("SellerSupcFMExceptionAddFMUpdateEventProducer")
    private IEventProducer<SellerSupcFMFCExceptionAddEventObj>    sellerSupcFMExceptionAddFMUpdateEventProducer;

    @Autowired
    @Qualifier("SellerSupcFMExceptionDeleteFMUpdateEventProducer")
    private IEventProducer<SellerSupcFMFCExceptionDeleteEventObj> sellerSupcFMExceptionDeleteFMUpdateEventProducer;
    // FM change event producers above

    // FC change event producers below

    @Autowired
    @Qualifier("sellerSUPCFMFCMappingProcessor")
    private ISellerSUPCFMFCMappingProcessor                       fmAndFCUpdateListener;

    @Autowired
    @Qualifier("SellerFMAndFCUpdateEventProducer")
    private IEventProducer<SellerFMFCUpdateEventObj>              sellerFMAndFCUpdateEventProducer;

    @Autowired
    @Qualifier("SellerSupcAddFMAndFCUpdateEventProducer")
    private IEventProducer<SellerSupcAddEventObj>                 sellerSupcAddFMAndFCUpdateEventProducer;

    @Autowired
    @Qualifier("SellerSupcFMExceptionAddFMAndFCUpdateEventProducer")
    private IEventProducer<SellerSupcFMFCExceptionAddEventObj>    sellerSupcFMExceptionAddFMAndFCUpdateEventProducer;

    @Autowired
    @Qualifier("SellerSupcFMExceptionDeleteFMAndFCUpdateEventProducer")
    private IEventProducer<SellerSupcFMFCExceptionDeleteEventObj> sellerSupcFMExceptionDeleteFMAndFCUpdateEventProducer;
    
    
    //ShipFromUpdate   below
    
    @Autowired
    @Qualifier("SellerSUPCShipFromMappingProcessorImpl")
    private ISellerSupcShipFromMappingProcessor                     shipFromUpdateListener;
    
    @Autowired
    @Qualifier("FCAddressUpdateEventProducer")
    private IEventProducer<FCAddressUpdateEventObj>              FCAddressUpdateEventProducer;

    @Autowired
    @Qualifier("sellerFMFCAddressUpdateEventProducer")
    private IEventProducer<SellerFMFCUpdateEventObj>            sellerFMFCShipFromUpdateEventProducer;
    
    @Autowired
    @Qualifier("SellerSupcAddShipFromUpdateEventProducer")
    private IEventProducer<SellerSupcAddEventObj>               sellerSupcAddShipFromUpdateEventProducer; 
    
    
    @Autowired
    @Qualifier("SellerSupcFMExceptionAddShipFromUpdateEventProducer")
    private IEventProducer<SellerSupcFMFCExceptionAddEventObj>    sellerSupcFMExceptionAddShipFromUpdateEventProducer;

    @Autowired
    @Qualifier("SellerSupcFMExceptionDeleteShipFromUpdateEventProducer")
    private IEventProducer<SellerSupcFMFCExceptionDeleteEventObj> sellerSupcFMExceptionDeleteShipFromUpdateEventProducer;
 
    

    //sdInstantUpdate   below
    
    @Autowired
    @Qualifier("SellerSUPCSdInstantMappingProcessorImpl")
    private ISellerSupcSdInstantMappingProcessor                   sdInstantUpdateListener;
    
    @Autowired
    @Qualifier("FCSdInstantUpdateEventProducer")
    private IEventProducer<FCAttributeUpdateEventObj>               fcSdinstantsUpdateEventProducer;

    @Autowired
    @Qualifier("SellerFMFCSdInstantUpdateEventProducer")
    private IEventProducer<SellerFMFCUpdateEventObj>              sellerSdInstantUpdateEventProducer;
    
    @Autowired
    @Qualifier("SellerSupcAddSdInstantUpdateEventProducer")
    private IEventProducer<SellerSupcAddEventObj>               sellerSupcAddSdInstantUpdateEventProducer; 
    
    
    @Autowired
    @Qualifier("SellerSupcFMExceptionAddSdInstantUpdateEventProducer")
    private IEventProducer<SellerSupcFMFCExceptionAddEventObj>    sellerSupcFMExceptionAddSdInstantUpdateEventProducer;

    @Autowired
    @Qualifier("SellerSupcFMExceptionDeleteSdInstantUpdateEventProducer")
    private IEventProducer<SellerSupcFMFCExceptionDeleteEventObj> sellerSupcFMExceptionDeleteSdInstantUpdateEventProducer;
 

    private IDependency                                           dep = new Dependency();

    // case when SELLER details added/changed from salesforce
    @Override
    public boolean processChangeOfSellerFMMappingForExternalNotification(SellerFMFCUpdateEventObj event) {

        safelyPushUpdateForChangeOfSellerFMMappingToListeners(event, fmUpdateListener, sellerFMUpdateEventProducer, isFMPushAllowed(), "-async-forked-1");
        safelyPushUpdateForChangeOfSellerFMMappingToListeners(event, fmAndFCUpdateListener, sellerFMAndFCUpdateEventProducer, isFCPushAllowed(), "-async-forked-2");
        safelyPushUpdateForChangeOfSellerFMMappingToListeners(event, shipFromUpdateListener, sellerFMFCShipFromUpdateEventProducer, isShipFromPushAllowed(), "-async-forked-3");
        safelyPushUpdateForChangeOfSellerFMMappingToListeners(event, sdInstantUpdateListener, sellerSdInstantUpdateEventProducer,isSdInstantPushAllowed(),"-async-forked-4");

        return true;
    }

    @Override
    public boolean processAdditionOfNewSellerSUPCMappingForExternalNotification(SellerSupcAddEventObj event) throws GetFailedException, ExternalDataNotFoundException,
            PutFailedException, GenericPersisterException {

        safelyPushUpdateForAdditionOfNewSellerSUPCMappingToListeners(event, fmUpdateListener, sellerSupcAddFMUpdateEventProducer);
        safelyPushUpdateForAdditionOfNewSellerSUPCMappingToListeners(event, fmAndFCUpdateListener, sellerSupcAddFMAndFCUpdateEventProducer);
        safelyPushUpdateForAdditionOfNewSellerSUPCMappingToListeners(event, shipFromUpdateListener, sellerSupcAddShipFromUpdateEventProducer);
        safelyPushUpdateForAdditionOfNewSellerSUPCMappingToListeners(event, sdInstantUpdateListener, sellerSupcAddSdInstantUpdateEventProducer);


        return true;
    }

    // case when SELLER-SUPC-FM mapping is changed in exception list from CoCoFS admin
    @Override
    public boolean processChangeOfSellerSUPCFMMappingExceptionForExternalNotification(SellerSupcFMFCExceptionAddEventObj event) {

        safelyPushUpdateForChangeOfSellerSUPCFMMappingExceptionToListeners(event, fmUpdateListener, sellerSupcFMExceptionAddFMUpdateEventProducer);
        safelyPushUpdateForChangeOfSellerSUPCFMMappingExceptionToListeners(event, fmAndFCUpdateListener, sellerSupcFMExceptionAddFMAndFCUpdateEventProducer);

        safelyPushUpdateForChangeOfSellerSUPCFMMappingExceptionToListeners(event, shipFromUpdateListener, sellerSupcFMExceptionAddShipFromUpdateEventProducer);
        safelyPushUpdateForChangeOfSellerSUPCFMMappingExceptionToListeners(event, sdInstantUpdateListener, sellerSupcFMExceptionAddSdInstantUpdateEventProducer);

        return true;
    }

    // case when SELLER-SUPC-FM mapping is disabled in exception list from admin
    @Override
    public boolean processDisablingOfSellerSUPCFMMappingExceptionForExternalNotification(SellerSupcFMFCExceptionDeleteEventObj event) {
        safelyPushUpdateForDisablingOfSellerSUPCFMMappingExceptionToListeners(event, fmUpdateListener, sellerSupcFMExceptionDeleteFMUpdateEventProducer);
        safelyPushUpdateForDisablingOfSellerSUPCFMMappingExceptionToListeners(event, fmAndFCUpdateListener, sellerSupcFMExceptionDeleteFMAndFCUpdateEventProducer);
        
        safelyPushUpdateForDisablingOfSellerSUPCFMMappingExceptionToListeners(event, shipFromUpdateListener, sellerSupcFMExceptionDeleteShipFromUpdateEventProducer);
        safelyPushUpdateForDisablingOfSellerSUPCFMMappingExceptionToListeners(event, sdInstantUpdateListener, sellerSupcFMExceptionDeleteSdInstantUpdateEventProducer);

        return true;
    }
    
    @Override
    public boolean processChangeOfFCAddressForExternalNotification(FCAddressUpdateEventObj event) throws GetFailedException, ExternalDataNotFoundException
     {
        safelyPushUpdateForChangeOfFCAddressExceptionToListeners(event, shipFromUpdateListener, FCAddressUpdateEventProducer);
        return true;
    }
    
    //When SdInstant is changed of a FC
    @Override
    public boolean processChangeOfFCAttributesForExternalNotification(FCAttributeUpdateEventObj event) throws GetFailedException, ExternalDataNotFoundException {
        safelyPushUpdateForChangeOfFCAttributeExceptionToListeners(event, sdInstantUpdateListener, fcSdinstantsUpdateEventProducer);
        return true;
    }

    private void safelyPushUpdateForChangeOfSellerFMMappingToListeners(final SellerFMFCUpdateEventObj event, final ISellerSUPCFMMappingProcessor updateListener,
            final IEventProducer<SellerFMFCUpdateEventObj> eventProducer, final boolean pushAllowed, final String threadId) {
        if (!pushAllowed) {
            return;
        }

        Runnable r = new Runnable() {
            @Override
            public void run() {

                try {
                    // persist was successful
                    boolean bResult = updateListener.processChangeOfSellerFMMappingForExternalNotification(event);

                    if (!bResult) {
                        //normal processing was not successful, need to add event for later processing.
                        LOG.warn("normal processing was not successful, adding event for later processing. {}", event);
                        eventProducer.createEventAndCallAsynchronously(event);
                        LOG.info("event submission completed.");
                    }
                } catch (Exception e) {
                    LOG.error("Exception while processing the request for event:"+event+" . Exception {}",  e);
                }
            }
        };

        LOG.info("Processing asynchronously for change of seller fm mapping for event {}", event);
        new Thread(r, Thread.currentThread().getName() + threadId).start();
    }

    private void safelyPushUpdateForAdditionOfNewSellerSUPCMappingToListeners(SellerSupcAddEventObj event, ISellerSUPCFMMappingProcessor updateListener,
            IEventProducer<SellerSupcAddEventObj> eventProducer) {
        // do not check FM push allowed here as insert in seller_supc_mapping table is initiated in this flow
        try {
            boolean bResult = false;
            try {
                bResult = updateListener.processAdditionOfNewSellerSUPCMappingForExternalNotification(event);
            } catch (Exception e) {
                LOG.error("Exception while processing the request for event: {}. Exception {}", event, e);
                throw e;
            }

            if (!bResult) {
                //normal processing was not successful, need to add event for later processing.
                LOG.warn("normal processing was not successful, adding event for later processing. {}", event);
                eventProducer.createEventAndCallAsynchronously(event);
                LOG.info("event submission completed.");
            }
        } catch (Exception e) {
            LOG.error("Exception while processing the request for event: {} . Exception {}", event, e);
        }
    }

    private void safelyPushUpdateForChangeOfSellerSUPCFMMappingExceptionToListeners(SellerSupcFMFCExceptionAddEventObj event, ISellerSUPCFMMappingProcessor updateListener,
            IEventProducer<SellerSupcFMFCExceptionAddEventObj> eventProducer) {

        try {
            boolean bResult = false;
            try {
                bResult = updateListener.processChangeOfSellerSUPCFMMappingExceptionForExternalNotification(event);
            } catch (Exception e) {
                LOG.error("Exception while processing the request for event: {}. Exception {}", event, e);
            }

            if (!bResult) {
                //normal processing was not successful, need to add event for later processing.
                LOG.warn("normal processing was not successful, adding event for later processing. {}", event);
                eventProducer.createEventAndCallAsynchronously(event);
                LOG.info("event submission completed.");
            }
        } catch (Exception e) {
            LOG.error("Exception while processing the request for event: {} . Exception {}", event, e);
        }

    }

    private void safelyPushUpdateForDisablingOfSellerSUPCFMMappingExceptionToListeners(SellerSupcFMFCExceptionDeleteEventObj event, ISellerSUPCFMMappingProcessor updateListener,
            IEventProducer<SellerSupcFMFCExceptionDeleteEventObj> eventProducer) {

        try {
            boolean bResult = false;
            try {
                bResult = updateListener.processDisablingOfSellerSUPCFMMappingExceptionForExternalNotification(event);
            } catch (Exception e) {
                LOG.error("Exception while processing the request for event: {}. Exception {}", event, e);
            }

            if (!bResult) {
                //normal processing was not successful, need to add event for later processing.
                LOG.warn("normal processing was not successful, adding event for later processing. {}", event);
                eventProducer.createEventAndCallAsynchronously(event);
                LOG.info("event submission completed.");
            }
        } catch (Exception e) {
            LOG.error("Exception while processing the request for event: {} . Exception {}", event, e);
        }

    }
    
    private void safelyPushUpdateForChangeOfFCAddressExceptionToListeners(FCAddressUpdateEventObj event, ISellerSupcShipFromMappingProcessor updateListener,
            IEventProducer<FCAddressUpdateEventObj> eventProducer) {

        try {
            boolean bResult = false;
            try {
                bResult = updateListener.processChangeOfFCAddressForExternalNotification(event);
            } catch (Exception e) {
                LOG.error("Exception while processing the request for event: {}. Exception {}", event, e);
            }

            if (!bResult) {
                //normal processing was not successful, need to add event for later processing.
                LOG.warn("normal processing was not successful, adding event for later processing. {}", event);
                eventProducer.createEventAndCallAsynchronously(event);
                LOG.info("event submission completed.");
            }
        } catch (Exception e) {
            LOG.error("Exception while processing the request for event: {} . Exception {}", event, e);
        }

    }
    
    private void safelyPushUpdateForChangeOfFCAttributeExceptionToListeners(FCAttributeUpdateEventObj event, ISellerSupcSdInstantMappingProcessor updateListener,
            IEventProducer<FCAttributeUpdateEventObj> eventProducer) {

        try {
            boolean bResult = false;
            try {
                bResult = updateListener.processChangeOfFCAttributesForExternalNotification(event);
            } catch (Exception e) {
                LOG.error("Exception while processing the request for event: {}. Exception {}", event, e);
            }

            if (!bResult) {
                //normal processing was not successful, need to add event for later processing.
                LOG.warn("normal processing was not successful, adding event for later processing. {}", event);
                eventProducer.createEventAndCallAsynchronously(event);
                LOG.info("event submission completed.");
            }
        } catch (Exception e) {
            LOG.error("Exception while processing the request for event: {} . Exception {}", event, e);
        }

    }

    private boolean isFCPushAllowed() {
        Property p = Property.ACTIVE_MQ_SELLER_SUPC_FM_FC_PUSH_ENABLED;
        boolean bAllowed = dep.getBooleanPropertyValue(p);
        if (!bAllowed) {
            LOG.warn(p + " for activemq queue push is disabled, not processing the add of seller-supc mapping further.");
        }
        return bAllowed;
    }

    private boolean isFMPushAllowed() {
        Property p = Property.ACTIVE_MQ_SEARCH_PUSH_ENABLED;
        boolean bAllowed = dep.getBooleanPropertyValue(p);
        if (!bAllowed) {
            LOG.warn(p + " for activemq fm push is disabled, not processing the add of seller-supc mapping further.");
        }
        return bAllowed;
    }
    
    private boolean isShipFromPushAllowed() {
        Property p = Property.ACTIVE_MQ_SHIPFROM_PUSH_ENABLED;
        boolean bAllowed = dep.getBooleanPropertyValue(p);
        if (!bAllowed) {
            LOG.warn(p + " for activemq shipfrom push is disabled, not processing the add of seller-supc mapping further.");
        }
        return bAllowed;
    }

    
    private boolean isSdInstantPushAllowed() {
        Property p = Property.ACTIVE_MQ_SDINSTANT_PUSH_ENABLED;
        boolean bAllowed = dep.getBooleanPropertyValue(p);
        if (!bAllowed) {
            LOG.warn(p + " for activemq sdinstant push is disabled, not processing the add of seller-supc mapping further.");
        }
        return bAllowed;
    }

    public interface IDependency {
        boolean getBooleanPropertyValue(Property property);
    }

    static class Dependency implements IDependency {
        @Override
        public boolean getBooleanPropertyValue(Property property) {
            return ConfigUtils.getBooleanScalar(property);
        }
    }

    

}
