/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 25-Nov-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.services.fm.dto;

import com.snapdeal.cocofs.core.aerospike.vo.SellerSubcatFMMappingVO;
import com.snapdeal.cocofs.entity.SellerSubcatFMMapping;

public class SellerSubcatFMMappingDataDto {


    private SellerSubcatFMMapping     entity;
    
    private SellerSubcatFMMappingVO     record;


    public SellerSubcatFMMappingDataDto() {
    }

    
    public SellerSubcatFMMappingDataDto( SellerSubcatFMMapping entity, SellerSubcatFMMappingVO record) {
        this.entity = entity;
        this.record = record;
    }


   

    public SellerSubcatFMMapping getEntity() {
        return entity;
    }

    public void setEntity(SellerSubcatFMMapping entity) {
        this.entity = entity;
    }
    
    

    public SellerSubcatFMMappingVO getRecord() {
        return record;
    }

    public void setRecord(SellerSubcatFMMappingVO record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "SellerSubcatFMMappingDataDto [entity=" + entity + ", record=" + record + "]";
    }

    

}
