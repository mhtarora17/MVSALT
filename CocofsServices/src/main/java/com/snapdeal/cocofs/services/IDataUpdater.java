/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 09-Sep-2013
 *  @author himanshum
 */
package com.snapdeal.cocofs.services;

import javax.naming.OperationNotSupportedException;

import com.snapdeal.cocofs.generic.persister.exception.GenericPersisterException;
import com.snapdeal.cocofs.generic.persister.response.GenericPersisterResponse;
import com.snapdeal.cocofs.services.common.dto.UserInfo;
import com.snapdeal.cocofs.services.data.engine.IDataEngine;

public interface IDataUpdater {

    /**
     * Given a DTO object and corresponding data Reader and data Merger interface creates/edit the mongo documents and
     * entites for it.
     * 
     * @param dto
     * @param reader
     * @param merger
     * @throws GenericPersisterException
     * @throws OperationNotSupportedException
     */
    public <T, E, D> GenericPersisterResponse<E, D> updateDataWithDTO(T dto, IDataReader<T, E, D> reader, IDataEngine<T, E, D> engine, UserInfo user)
            throws GenericPersisterException, OperationNotSupportedException;

    /**
     * Given a DTO object and corresponding data Reader and data Merger interface creates/edit the mongo documents and
     * entites for it.
     * 
     * @param dto
     * @param reader
     * @param merger
     * @throws GenericPersisterException
     * @throws OperationNotSupportedException
     */
    public <T, E, D> GenericPersisterResponse<E, D> updateDataWithDTO(T dto, IDataReader<T, E, D> reader, IDataEngine<T, E, D> engine, UserInfo user, boolean skipGenricValidation, boolean skipDataModifier)
            throws GenericPersisterException, OperationNotSupportedException;
}
