/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 07-Oct-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.services.fm.impl;

import java.util.ArrayList;
import java.util.List;

import javax.naming.OperationNotSupportedException;

import org.springframework.stereotype.Service;

import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.cocofs.core.aerospike.vo.SellerSubcatFMMappingVO;
import com.snapdeal.cocofs.entity.SellerSubcatFMMapping;
import com.snapdeal.cocofs.services.fm.dto.SellerSubcatFMMappingUpdateDto;
import com.snapdeal.cocofs.utils.AerospikeKeyHelper;

@Service("SellerSubcatFMMappingDataEngine")
public class SellerSubcatFMMappingDataEngine extends AbstractFMMappingDataEngine<SellerSubcatFMMappingUpdateDto, SellerSubcatFMMapping,SellerSubcatFMMappingVO> {

   


    @Override
    public List<SellerSubcatFMMapping> enrichEntities(SellerSubcatFMMappingUpdateDto dto, List<SellerSubcatFMMapping> entities, String userEmail)
            throws OperationNotSupportedException {
        if (null != dto) {
            SellerSubcatFMMapping mapping = null;
            List<SellerSubcatFMMapping> mappingList = new ArrayList<SellerSubcatFMMapping>();
            if (null != entities && !entities.isEmpty()) {
                mapping = entities.get(0);
            } else {
                mapping = new SellerSubcatFMMapping();
            }
            updateSellerSubcatFMMapping(dto, mapping, userEmail);
            mappingList.add(mapping);
            return mappingList;
        }
        return null;
    }

    private void updateSellerSubcatFMMapping(SellerSubcatFMMappingUpdateDto dto, SellerSubcatFMMapping mapping, String userEmail) {
        enrichFMMappingEntity(dto, mapping, userEmail);
        mapping.setSubcat(dto.getSubcat());

    }
    
    
    @Override
    public List<SellerSubcatFMMappingVO> enrichRecords(SellerSubcatFMMappingUpdateDto dto, List<SellerSubcatFMMappingVO> records, String userEmail)
            throws OperationNotSupportedException {
       if (dto != null) {
            SellerSubcatFMMappingVO record = null;
            List<SellerSubcatFMMappingVO> recordList = new ArrayList<SellerSubcatFMMappingVO>();
            if (null != records && !records.isEmpty()) {
                record = records.get(0);
            } else {
                record = new SellerSubcatFMMappingVO();
            }
            updateSellerSubcatFMMappingVO(dto, record);
            recordList.add(record);
            return recordList;
        }
        return null;
    }

    private void updateSellerSubcatFMMappingVO(SellerSubcatFMMappingUpdateDto dto, SellerSubcatFMMappingVO record) {
        record.setSubcatSellerCode(AerospikeKeyHelper.getKey(dto.getSellerCode(),dto.getSubcat()));
        record.setSubcat(dto.getSubcat());
        record.setSellerCode(dto.getSellerCode());
        record.setCreatedStr(record.getCreatedStr() != null ? record.getCreatedStr() : DateUtils.getCurrentTime().getTime() +"" );
        record.setUpdatedStr(DateUtils.getCurrentTime().getTime() + "" );
        record.setFulfillmentModel(dto.getFulfilmentModel());
        record.setEnabled(1);
    }

        

}
