package com.snapdeal.cocofs.services.taxclass.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.base.cache.CacheManager;
import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.cocofs.cache.loader.impl.SubcatTaxClassMappingCache;
import com.snapdeal.cocofs.cache.loader.impl.SupcTaxClassMappingCache;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.services.taxclass.ITaxClassDataSourceService;
import com.snapdeal.cocofs.services.taxclass.dto.SubcatTaxClassMappingDTO;
import com.snapdeal.cocofs.services.taxclass.dto.SupcTaxClassMappingDTO;

@Service("TaxClassDataSourceServiceForCache")
public class TaxClassDataSourceServiceForCache implements ITaxClassDataSourceService {

    @Autowired
    private TaxClassDataSourceServiceFactory taxClassDataSource;

    private static final Logger              LOG = LoggerFactory.getLogger(TaxClassDataSourceServiceForCache.class);

    /**
     * Returns the Tax Class by supc from cache
     * 
     * @param supc
     * @return tax class
     * @throws ExternalDataNotFoundException
     */
    @Override
    public SupcTaxClassMappingDTO getTaxClassBySupc(String supc) throws ExternalDataNotFoundException {
        SupcTaxClassMappingDTO taxClass = null;
        SupcTaxClassMappingDTO supcTaxClassMappingDTO = getTaxClassDTOForSupc(supc);
        if (supcTaxClassMappingDTO != null && supcTaxClassMappingDTO.isEnabled()) {
            taxClass = supcTaxClassMappingDTO;
        }
        LOG.debug("Returning taxClass {}  for supc {}  ", taxClass, supc);
        return taxClass;
    }

    /**
     * Returns the Tax Class by subcat from cache
     * 
     * @param subcat
     * @return tax class
     * @throws ExternalDataNotFoundException
     */
    @Override
    public SubcatTaxClassMappingDTO getTaxClassBySubcat(String subcat) throws ExternalDataNotFoundException {
        SubcatTaxClassMappingDTO taxClass = null;
        SubcatTaxClassMappingDTO subcatTaxClassMappingDTO = getTaxClassDTOForSubcat(subcat);
        if (subcatTaxClassMappingDTO != null && subcatTaxClassMappingDTO.isEnabled()) {
            taxClass = subcatTaxClassMappingDTO;
        }
        LOG.info("Returning taxClass {}  for subcat {}  ", taxClass, subcat);
        return taxClass;
    }

    private SupcTaxClassMappingDTO getTaxClassDTOForSupc(String supc) throws ExternalDataNotFoundException {
        if (StringUtils.isEmpty(supc)) {
            LOG.warn("Illegal paramters passed while trying to find taxclass at supc level");
            throw new ExternalDataNotFoundException("Required Parameters not there while getting taxClass At SUPC Level");
        }

        LOG.debug("Going to read tax classfrom cache for supc {}", supc);
        SupcTaxClassMappingDTO supcTaxClassMappingDTO = null;
        supcTaxClassMappingDTO = CacheManager.getInstance().getCache(SupcTaxClassMappingCache.class).getSupcTaxClassMappingForSupc(supc);
        LOG.debug("Tax Class  obtained from SupcTaxClassMappingCache:" + supcTaxClassMappingDTO);

        return supcTaxClassMappingDTO;
    }

    private SubcatTaxClassMappingDTO getTaxClassDTOForSubcat(String subcat) throws ExternalDataNotFoundException {
        if (StringUtils.isEmpty(subcat)) {
            LOG.warn("Illegal paramters passed while trying to find taxclass at subcat level");
            throw new ExternalDataNotFoundException("Required Parameters not there while getting taxClass At Subcat Level");
        }

        LOG.debug("Going to read tax classfrom cache for subcat {}", subcat);
        SubcatTaxClassMappingDTO subcatTaxClassMappingDTO = null;
        subcatTaxClassMappingDTO = CacheManager.getInstance().getCache(SubcatTaxClassMappingCache.class).getSubcatTaxClassMappingForSubcat(subcat);
        LOG.info("Tax Class  obtained from SubcatTaxClassMappingCache:" + subcatTaxClassMappingDTO);

        return subcatTaxClassMappingDTO;
    }

}
