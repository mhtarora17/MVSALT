/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 07-Oct-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.services.taxclass.impl;

import java.util.ArrayList;
import java.util.List;

import javax.naming.OperationNotSupportedException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.cocofs.core.aerospike.vo.SubcatTaxClassMappingVO;
import com.snapdeal.cocofs.dataaccess.taxclass.ITaxClassDBDataReadService;
import com.snapdeal.cocofs.entity.SubcatTaxClassMapping;
import com.snapdeal.cocofs.services.taxclass.dto.SubcatTaxClassMappingDTO;

@Service("SubcatTaxMappingDataEngine")
public class SubcatTaxMappingDataEngine extends AbstractTaxMappingDataEngine<SubcatTaxClassMappingDTO, SubcatTaxClassMapping, SubcatTaxClassMappingVO> {

    @Autowired
    private ITaxClassDBDataReadService                               taxClassDBDataReadService;

    @Autowired
    private com.snapdeal.cocofs.services.converter.IConverterService convertorService;

    @Override
    public List<SubcatTaxClassMapping> enrichEntities(SubcatTaxClassMappingDTO dto, List<SubcatTaxClassMapping> entities, String userEmail) throws OperationNotSupportedException {
        if (null != dto) {
            SubcatTaxClassMapping mapping = null;
            List<SubcatTaxClassMapping> mappingList = new ArrayList<SubcatTaxClassMapping>();

            if (null != entities && !entities.isEmpty()) {
                mapping = entities.get(0);
            } else {
                mapping = new SubcatTaxClassMapping();
            }

            enrichTaxClassMappingEntity(dto, mapping, userEmail);
            
            mappingList.add(mapping);
            return mappingList;
        }
        return null;
    }

    @Override
    public List<SubcatTaxClassMappingVO> enrichRecords(SubcatTaxClassMappingDTO dto, List<SubcatTaxClassMappingVO> records, String userEmail) throws OperationNotSupportedException {
        if (dto != null) {
            SubcatTaxClassMappingVO record = null;
            List<SubcatTaxClassMappingVO> recordList = new ArrayList<SubcatTaxClassMappingVO>();
            if (null != records && !records.isEmpty()) {
                record = records.get(0);
            } else {
                record = new SubcatTaxClassMappingVO();
            }
            updateSubcatTaxClassMappingVO(dto, record);
            recordList.add(record);
            return recordList;

        }
        return null;
    }

    private void updateSubcatTaxClassMappingVO(SubcatTaxClassMappingDTO dto, SubcatTaxClassMappingVO record) {
        record.setCreatedStr(record.getCreated() != null ? String.valueOf(record.getCreated().getTime()) : String.valueOf(DateUtils.getCurrentTime().getTime()));
        record.setUpdatedStr(DateUtils.getCurrentTime().getTime() + "");
        record.setTaxClass(dto.getTaxClass());
        record.setSubcat(dto.getSubcat());
        record.setEnabled(dto.isEnabled() == true ? 1 : 0);
    }

}
