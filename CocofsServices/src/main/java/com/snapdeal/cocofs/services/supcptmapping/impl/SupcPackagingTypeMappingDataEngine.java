/**
 *  Copyright 2016 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.services.supcptmapping.impl;

import java.util.ArrayList;
import java.util.List;

import javax.naming.OperationNotSupportedException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.entity.SupcPackagingTypeMapping;
import com.snapdeal.cocofs.excelupload.SupcPackagingTypeUploadDTO;
import com.snapdeal.cocofs.mongo.model.SupcPackagingTypeMappingUnit;

/**
 *  
 *  @version     1.0, 04-Jan-2016
 *  @author vikassharma03
 */

@Service("SupcPackagingTypeMappingDataEngine")
public class SupcPackagingTypeMappingDataEngine extends AbstractSupcPackagingTypeMappingDataEngine<SupcPackagingTypeUploadDTO, SupcPackagingTypeMapping, SupcPackagingTypeMappingUnit>{

    private static final Logger  LOG = LoggerFactory.getLogger(SupcPackagingTypeMappingDataEngine.class);
    @Override
    public List<SupcPackagingTypeMappingUnit> enrichDocuments(SupcPackagingTypeUploadDTO dto, List<SupcPackagingTypeMappingUnit> documents) throws OperationNotSupportedException {
        if (dto != null) {
            SupcPackagingTypeMappingUnit document = null;
            List<SupcPackagingTypeMappingUnit> documentList = new ArrayList<SupcPackagingTypeMappingUnit>();
            if (null != documents && !documents.isEmpty()) {
                document = documents.get(0);
            } else {
                document = new SupcPackagingTypeMappingUnit();
            }
            updateSupcPackagingTypeMappingUnit(dto, document);
            documentList.add(document);
            return documentList;
        }
        return null;
    }

    private void updateSupcPackagingTypeMappingUnit(SupcPackagingTypeUploadDTO dto, SupcPackagingTypeMappingUnit document) {
        
        
        enrichSupcPackagingTypeMappingUnit(dto, document);
        document.setSupc(dto.getSupc());
        document.setStoreCode(dto.getStoreCode());
        document.setPackagingType(dto.getPackagingType());   

        
    }
    
    @Override
    public List<SupcPackagingTypeMapping> enrichEntities(SupcPackagingTypeUploadDTO dto, List<SupcPackagingTypeMapping> entities, String userEmail)
            throws OperationNotSupportedException {
        // TODO Auto-generated method stub
        if (null != dto) {
            SupcPackagingTypeMapping entity = null;
            List<SupcPackagingTypeMapping> entityList = new ArrayList<SupcPackagingTypeMapping>();
            if (null != entities && !entities.isEmpty()) {
                entity = entities.get(0);
            } else {
                entity = new SupcPackagingTypeMapping();
            }
            updateSupcPackagingTypeMapping(dto, entity, userEmail);
            entityList.add(entity);
            return entityList;
        }
        return null;
    }
    
    private void updateSupcPackagingTypeMapping(SupcPackagingTypeUploadDTO dto, SupcPackagingTypeMapping entity, String userEmail) {
        enrichSupcPackagingTypeMappingEntity(dto, entity, userEmail);
        entity.setSupc(dto.getSupc());
        entity.setPackagingType(dto.getPackagingType());
        entity.setStoreCode(dto.getStoreCode());
        entity.setEnabled(dto.isEnable());

    }


  


    
}
