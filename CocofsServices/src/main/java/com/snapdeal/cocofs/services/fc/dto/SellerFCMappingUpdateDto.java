/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 07-Oct-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.services.fc.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class SellerFCMappingUpdateDto {

    private String sellerCode;

    private String fulfilmentModel;
    
    private String fcCode;

    public SellerFCMappingUpdateDto() {
    }

    public SellerFCMappingUpdateDto(String sellerCode, String fulfilmentModel, String fcCode) {
        this.sellerCode = sellerCode;
        this.fulfilmentModel = fulfilmentModel;
        this.fcCode = fcCode;
    }

    public String getSellerCode() {
        return sellerCode;
    }

    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }

    public String getFulfilmentModel() {
        return fulfilmentModel;
    }

    public void setFulfilmentModel(String fulfilmentModel) {
        this.fulfilmentModel = fulfilmentModel;
    }

    
    public String getFcCode() {
        return fcCode;
    }

    public void setFcCode(String fcCode) {
        this.fcCode = fcCode;
    }

    @Override
    public String toString() {
        return "SellerFCMappingUpdateDto [sellerCode=" + sellerCode + ", fulfilmentModel=" + fulfilmentModel + ", fcCode=" + fcCode + "]";
    }

}
