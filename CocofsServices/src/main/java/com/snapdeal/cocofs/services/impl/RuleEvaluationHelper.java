/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 10, 2013
 *  @author himanshu
 */
package com.snapdeal.cocofs.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import com.snapdeal.cocofs.mongo.model.CocofsRuleUnit;
import com.snapdeal.cocofs.mongo.model.CriteriaUnit;
import com.snapdeal.packman.enums.PackagingRuleParam;
import com.snapdeal.packman.mongo.model.PackmanRuleUnit;
import com.snapdeal.rule.engine.entity.Criteria;
import com.snapdeal.rule.engine.entity.Rule;
import com.snapdeal.rule.engine.entity.RuleParams;
import com.snapdeal.rule.engine.entity.ValueTypes;

public class RuleEvaluationHelper {
    public static Rule getRuleFromCocofsRuleUnit(CocofsRuleUnit unit ) {
        Rule r = new Rule();
        r.setId(unit.getId());
        r.setName(unit.getName());
        r.setPriority(unit.getPriority());
        for ( CriteriaUnit  cu : unit.getCriteria() ) {
            r.getCriterion().add(getCriteriaFromCriteriaUnit(cu));
        }
        
        return r;
    }
    
    public static Rule getRuleFromCocofsRuleUnit(PackmanRuleUnit unit ) {
        Rule r = new Rule();
        r.setId(unit.getId());
        r.setName(unit.getName());
        r.setPriority(unit.getPriority());
        Set<RuleParams> ruleParam = new HashSet<RuleParams>();
        RuleParams ruleParams = new RuleParams(unit.getId(), null, PackagingRuleParam.RETURN_VALUE.getCode(), unit.getReturnValue());
        ruleParam.add(ruleParams);
        r.setRuleParams(ruleParam);
        for ( com.snapdeal.packman.mongo.model.CriteriaUnit  cu : unit.getCriteria() ) {
            r.getCriterion().add(getCriteriaFromCriteriaUnit(cu));
        }        
        return r;
    }


    private static Criteria getCriteriaFromCriteriaUnit(com.snapdeal.packman.mongo.model.CriteriaUnit cu) {
        Criteria c = new Criteria();
        c.setCreated(cu.getCreated());
        c.setEnabled(cu.isEnabled());
        c.setId(cu.getId());
        c.setName(cu.getName());
        c.setOperatorByLogicalOperator(cu.getOperator());
        c.setValue(cu.getValue());
        c.setValueType(ValueTypes.valueOf(cu.getValueType()));
        return c;
    }
    private static Criteria getCriteriaFromCriteriaUnit(CriteriaUnit cu) {
        Criteria c = new Criteria();
        c.setCreated(cu.getCreated());
        c.setEnabled(cu.isEnabled());
        c.setId(cu.getId());
        c.setName(cu.getName());
        c.setOperatorByLogicalOperator(cu.getOperator());
        c.setValue(cu.getValue());
        c.setValueType(ValueTypes.valueOf(cu.getValueType()));
        return c;
    }
    

    public static List<CocofsRuleUnit>  getRuleValidOnDate(List<CocofsRuleUnit> ruleList, Date now ) {
        List<CocofsRuleUnit> outList = new ArrayList<CocofsRuleUnit>();
        if ( null == ruleList ) {
            return outList;
        }
        for (CocofsRuleUnit r : ruleList) {
            if (r.getStartDate().before(now) && ((null == r.getEndDate()) || (r.getEndDate().after(now))) && r.isEnabled()) {
                outList.add(r);
            }
        }
        return outList;
    }
    
    public static List<PackmanRuleUnit>  getRuleValidForDate(List<PackmanRuleUnit> ruleList, Date now ) {
        List<PackmanRuleUnit> outList = new ArrayList<PackmanRuleUnit>();
        if ( null == ruleList ) {
            return outList;
        }
        for (PackmanRuleUnit r : ruleList) {
            if (r.getStartDate().before(now) && ((null == r.getEndDate()) || (r.getEndDate().after(now))) && r.isEnabled()) {
                outList.add(r);
            }
        }
        return outList;
    }

}
