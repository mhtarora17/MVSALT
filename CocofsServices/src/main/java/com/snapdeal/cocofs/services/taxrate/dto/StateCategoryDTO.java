/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.services.taxrate.dto;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *  
 *  @version     1.0, 19-Aug-2014
 *  @author ankur
 */
@XmlRootElement
public class StateCategoryDTO {
    private String state;

    private String categoryUrl;

    public StateCategoryDTO(){
        
    }
    
    public StateCategoryDTO(String state, String categoryUrl) {
        super();
        this.state = state;
        this.categoryUrl = categoryUrl;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCategoryUrl() {
        return categoryUrl;
    }

    public void setCategoryUrl(String categoryUrl) {
        this.categoryUrl = categoryUrl;
    }

    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((categoryUrl == null) ? 0 : categoryUrl.hashCode());
        result = prime * result + ((state == null) ? 0 : state.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        StateCategoryDTO other = (StateCategoryDTO) obj;
        if (categoryUrl == null) {
            if (other.categoryUrl != null)
                return false;
        } else if (!categoryUrl.equals(other.categoryUrl))
            return false;
        if (state == null) {
            if (other.state != null)
                return false;
        } else if (!state.equals(other.state))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "StateCategoryDTO [state=" + state + ", categoryUrl=" + categoryUrl + "]";
    }
    
    
    
}
