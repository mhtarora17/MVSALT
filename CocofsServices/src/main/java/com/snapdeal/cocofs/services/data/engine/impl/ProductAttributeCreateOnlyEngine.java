package com.snapdeal.cocofs.services.data.engine.impl;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.naming.OperationNotSupportedException;

import org.springframework.stereotype.Service;

import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.entity.ProductAttribute;
import com.snapdeal.cocofs.excelupload.ProductAttributeUploadDTO;
import com.snapdeal.cocofs.mongo.model.ProductAttributeUnit;

/**
 * @author nikhil
 */
@Service("productAttributeCreateOnlyEngine")
public class ProductAttributeCreateOnlyEngine extends AbstractProductAttributeEngine {

    @Override
    public List<ProductAttributeUnit> enrichDocuments(ProductAttributeUploadDTO dto, List<ProductAttributeUnit> documents) throws OperationNotSupportedException {

        if (documents != null && !documents.isEmpty()) {
            throw new OperationNotSupportedException("The current engine implementation supports create only functionality");
        }

        List<ProductAttributeUnit> docList = new ArrayList<ProductAttributeUnit>();

        if (documents.isEmpty()) {
            LOG.info("No Existing mongo doc for supc " + dto.getSupc() + " creating one now " + dto);
            ProductAttributeUnit pau = createNewProductAttributeUnitDocument(dto);
            docList.add(pau);
        }
        return docList;
    }

    @Override
    public List<ProductAttribute> enrichEntities(ProductAttributeUploadDTO dto, List<ProductAttribute> entities, String userEmail) throws OperationNotSupportedException {

        List<ProductAttribute> toReturn = new ArrayList<ProductAttribute>();

        if (entities != null && !entities.isEmpty()) {
            throw new OperationNotSupportedException("The current engine implementation supports create only functionality");
        } else {

            Map<String, String> m = ConfigUtils.getMap(Property.PRODUCT_ATTRIBUTE_UPLOAD_DTO_FIELD_MAP);
            Field[] flds = dto.getClass().getDeclaredFields();

            for (Field fld : flds) {
                int mods = fld.getModifiers();
                if (Modifier.isStatic(mods)) {
                    continue;
                }

                //                // a quick hack, will do it in a better way -nikhil ... doing it in a better way :P :)
                //                if (fld.getName().equals("supc") || fld.getName().equals("createdByEmailMap")) {
                //                    continue;
                //                }

                String attributeName = m.get(fld.getName());

                //seems to be a better way for me atleast - A. Singhal... might not be .. so do it in a better way :P :D
                if (StringUtils.isEmpty(attributeName)) {
                    continue;
                }

                //There is no default value for serializedType  field
                // so  a quick hack to skip serializedType Field in create API if no value for it has been passed.
                if (fld.getName().equals("serializedType")) {
                    if (StringUtils.isEmpty(dto.getSerializedType())) {
                        continue;
                    }
                }
                
              //There is no default value for PackagingType  field
                // so  a quick hack to skip PackagingType Field in create API if no value for it has been passed.
                  if (fld.getName().equals("packagingType")) {
                      if (StringUtils.isEmpty(dto.getPackagingType())) {
                          continue;
                      }
                  }
                  
                //There is no default value for PrimaryLength  field
                  // so  a quick hack to skip PrimaryLength Field in create API if no value for it has been passed.
                  
                  if(fld.getName().equals("primaryLength")) {
                      if(dto.getPrimaryLength() == null)
                          continue;
                  }
                  
                //There is no default value for PrimaryBreadth  field
                  // so  a quick hack to skip PrimaryBreadth Field in create API if no value for it has been passed.
                  
                  if(fld.getName().equals("primaryBreadth")) {
                      if(dto.getPrimaryBreadth() == null)
                          continue;
                  }
               
                  
                //There is no default value for PrimaryHeight  field
                  // so  a quick hack to skip PrimaryHeight Field in create API if no value for it has been passed.
                  if(fld.getName().equals("primaryHeight")) {
                      if(dto.getPrimaryHeight() == null)
                          continue;
                  }

                  
                  
                String fieldValueAsString = getNotNullFieldValueAsString(attributeName, fld, dto);

                LOG.info("Creating new product attribute for supc " + dto.getSupc() + " attribute " + attributeName + " with value " + fieldValueAsString + " per bulk update by "
                        + userEmail);

                ProductAttribute newAttr = createNewProductAttribute(dto, userEmail, attributeName, fieldValueAsString);
                toReturn.add(newAttr);

            }

        }
        return toReturn;
    }
}
