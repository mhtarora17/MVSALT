/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 07-Oct-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.services.fm.dto;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.cocofs.annotation.IgnoreInDownloadTemplate;

@XmlRootElement
public class SellerFMMappingUpdateDto {

    private String sellerCode;

    private String fulfilmentModel;

    //form Excel the fcCenters shall come via a string, having comma separated fc center 
    private String fcCenters;

    @IgnoreInDownloadTemplate
    private Date   lastUpdated = DateUtils.getCurrentTime();

    public SellerFMMappingUpdateDto() {
    }

    public SellerFMMappingUpdateDto(String sellerCode, String fulfilmentModel, String fcCenters) {
        this.sellerCode = sellerCode;
        this.fulfilmentModel = fulfilmentModel;
        this.fcCenters = fcCenters;
    }

    public SellerFMMappingUpdateDto(String sellerCode, String fulfilmentModel, String fcCenters, Date lastUpdated) {
        this.sellerCode = sellerCode;
        this.fulfilmentModel = fulfilmentModel;
        this.fcCenters = fcCenters;
        this.lastUpdated = lastUpdated;
    }

    public String getSellerCode() {
        return sellerCode;
    }

    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }

    public String getFulfilmentModel() {
        return fulfilmentModel;
    }

    public void setFulfilmentModel(String fulfilmentModel) {
        this.fulfilmentModel = fulfilmentModel;
    }

    public String getFcCenters() {
        return fcCenters;
    }

    public void setFcCenters(String fcCenters) {
        this.fcCenters = fcCenters;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    @Override
    public String toString() {
        return "SellerFMMappingUpdateDto [sellerCode=" + sellerCode + ", fulfilmentModel=" + fulfilmentModel + ", fcCenters=" + fcCenters + "]";
    }

}
