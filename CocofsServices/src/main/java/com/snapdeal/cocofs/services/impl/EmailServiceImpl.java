/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 10-Nov-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.services.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.base.cache.CacheManager;
import com.snapdeal.base.cache.EmailChannelCache;
import com.snapdeal.base.notification.INotificationService;
import com.snapdeal.base.notification.email.EmailMessage;
import com.snapdeal.base.vo.EmailTemplateVO;
import com.snapdeal.cocofs.cache.EmailTemplateCache;
import com.snapdeal.cocofs.configurations.IStartupService;
import com.snapdeal.cocofs.entity.User;
import com.snapdeal.cocofs.services.IEmailService;

@Service("EmailServiceImpl")
public class EmailServiceImpl implements IEmailService {

    @Autowired
    private INotificationService notificationService;

    @Autowired
    private IStartupService      startupService;

    private static final Logger  LOG = LoggerFactory.getLogger(EmailServiceImpl.class);

    @Override
    public void send(EmailMessage message) {
        if (CacheManager.getInstance().getCache(EmailChannelCache.class) == null) {
            try {
                startupService.loadEmailChannels(true);
            } catch (Exception e) {
                LOG.error("Exception {}", e);
            }
        }
        EmailTemplateVO template = CacheManager.getInstance().getCache(EmailTemplateCache.class).getTemplateByName(message.getTemplateName());
        notificationService.sendEmail(message, template);
    }

    @Override
    public void sendForgotPasswordEmail(User user, String contextPath, String contentPath, String forgotPasswordLink) {
        EmailMessage emailMessage = new EmailMessage(user.getEmail(), "forgotPasswordEmail");
        emailMessage.addTemplateParam("user", user);
        emailMessage.addTemplateParam("contentPath", contentPath);
        emailMessage.addTemplateParam("contextPath", contextPath);
        emailMessage.addTemplateParam("forgotPasswordLink", forgotPasswordLink);
        LOG.info("Sending forgot password email for {} ", user.getEmail());
        send(emailMessage);

    }

    @Override
    public void sendUserCreationEmail(User user, String contentPath) {
        EmailMessage emailMessage = new EmailMessage(user.getEmail(), "userCreationEmail");
        emailMessage.addTemplateParam("user", user);
        emailMessage.addTemplateParam("contentPath", contentPath);
        send(emailMessage);
    }

}
