/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.services.impl;



import java.util.ArrayList;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.cocofs.db.dao.ISupcPackagingTypeDao;
//import com.snapdeal.cocofs.db.dao.impl.RoleStoreMapping;
import com.snapdeal.cocofs.entity.SupcPackagingTypeMapping;
import com.snapdeal.cocofs.services.ISupcToPackagingTypeMappingService;
import com.snapdeal.packaman.storecodeandpt.dto.ScAndPtDownloadDto;
import com.snapdeal.packman.dao.IPackmanExclusivelyDao;
import com.snapdeal.packman.entity.PackagingType;
import com.snapdeal.packman.entity.Store;

/**
 *  
 *  @version     1.0, 21-Dec-2015
 *  @author vikassharma03
 */
@Service("SupcToPackagingTypeMappingServiceImpl")
public class SupcToPackagingTypeMappingServiceImpl implements ISupcToPackagingTypeMappingService {

    @Autowired
    @Qualifier("SupcPackagingTypeDaoImpl")
    private ISupcPackagingTypeDao supcPackagingTypeDao;
    
    @Autowired
    private IPackmanExclusivelyDao packmanExclusivelyDao;
    
    @Autowired
    private SessionFactory      sessionFactory;
    
    
   

    @Transactional
    @Override
    public SupcPackagingTypeMapping persistSupcPackagingTypeMappingDetails(SupcPackagingTypeMapping supcPackagingTypeMapping) {
        // TODO Auto-generated method stub
        supcPackagingTypeMapping.setCreated(DateUtils.getCurrentTime());
        return (SupcPackagingTypeMapping)supcPackagingTypeDao.persistSupcPackagingTypeMappingDetails(supcPackagingTypeMapping);
    }




    @Override
    public SupcPackagingTypeMapping getSupcPackagingTypeMappingBySupcAndStoreCode(String supc, String storeCode) {
        // TODO Auto-generated method stub
        return supcPackagingTypeDao.getSupcPackagingTypeMappingBySupcAndStoreCode(supc,storeCode);
    }


    @Override
    public List<SupcPackagingTypeMapping> getSupcPackagignTypeMappingListBySUPC(String supc) {
        // TODO Auto-generated method stub
        return supcPackagingTypeDao.getSupcPackagignTypeMappingListBySUPC(supc);
    }
  
    @Transactional
    @Override
    public List<ScAndPtDownloadDto> getAllSCs() {      
       List<ScAndPtDownloadDto> scDTOList = new ArrayList<ScAndPtDownloadDto>();
       List<Store> lstore =  packmanExclusivelyDao.fetchAllStores();
       
       for(Store s : lstore)
       {
           List<PackagingType> lpt = packmanExclusivelyDao.getAllPackagingType(s.getCode());
           for(PackagingType packagingType : lpt)
           {
            ScAndPtDownloadDto dto = new ScAndPtDownloadDto(s.getCode(),packagingType.getType());
            scDTOList.add(dto);
           }
           
           
       }
       return scDTOList;
    }    
        
    }