package com.snapdeal.cocofs.services.taxrate;

import com.snapdeal.cocofs.model.request.GetTaxInfoRequest;
import com.snapdeal.cocofs.model.response.GetTaxInfoResponse;

public interface ITaxRateservice {

    GetTaxInfoResponse getTaxInfoBySellerSUPCState(GetTaxInfoRequest request);

}
