package com.snapdeal.cocofs.services.fm.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.catalog.base.model.GetMinProductInfoBySupcRequest;
import com.snapdeal.catalog.base.model.GetMinProductInfoResponse;
import com.snapdeal.cocofs.core.aerospike.vo.SellerFMMappingVO;
import com.snapdeal.cocofs.core.aerospike.vo.SellerSubcatFMMappingVO;
import com.snapdeal.cocofs.core.aerospike.vo.SellerSupcFMMappingVO;
import com.snapdeal.cocofs.dataaccess.fm.IFMAerospikeDataReadService;
import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.external.service.factory.IExternalServiceFactory;
import com.snapdeal.cocofs.services.fm.IFMDataSourceService;
import com.snapdeal.cocofs.services.fm.dto.SellerFmFcDTO;
import com.snapdeal.cocofs.services.fm.dto.SellerSupcFmFcDTO;
import com.snapdeal.cocofs.utils.AerospikeKeyHelper;

@Service("FMDataSourceServiceForAerospike")
public class FMDataSourceServiceForAerospike implements IFMDataSourceService{

	@Autowired
	private IFMAerospikeDataReadService		fmAerospikeService;

	@Autowired
	private IExternalServiceFactory			externalServiceFactory;

	private static final Logger				LOG = LoggerFactory.getLogger(FMDataSourceServiceForAerospike.class);

	/**
	 * Returns the fulfilment model by sellercode from aerospike
	 * 
	 * @param sellerCode
	 * @return fulfilmentModel
	 * @throws ExternalDataNotFoundException
	 */
	@Override
	public String getFulfilmentModelBySeller(String sellerCode)	throws ExternalDataNotFoundException {
		String fulfilmentModel = null;
		//Get SellerFM mapping for sellerCode
		SellerFMMappingVO sellerFMMappingVO = getSellerFMMappingVO(sellerCode);
		if (sellerFMMappingVO != null) {
			fulfilmentModel = sellerFMMappingVO.getFulfillmentModel();
		}
		LOG.debug("Returning fm from seller mapping {} for seller {} ", fulfilmentModel, sellerCode);
		return fulfilmentModel;
	}
	
	/**
     * Returns the fc centers by sellercode from aerospike
     * 
     * @param sellerCode
     * @return fulfilmentModel
     * @throws ExternalDataNotFoundException
     */
    @Override
    public List<String> getFCBySeller(String sellerCode) throws ExternalDataNotFoundException {
        List<String> centers = null;
        //Get SellerFM mapping for sellerCode
        SellerFMMappingVO sellerFMMappingVO = getSellerFMMappingVO(sellerCode);
        if (sellerFMMappingVO != null) {
            centers = sellerFMMappingVO.getFcCenters();
        }
        LOG.debug("Returning centers from seller mapping {} for seller {} ", centers, sellerCode);
        return centers;
    }

	/**
	 * Returns the fulfilment model by sellercode and supc from aerospike
	 * 
	 * @param sellerCode
	 * @param supc
	 * @return fulfilmentModel
	 * @throws ExternalDataNotFoundException
	 */
	@Override
	public String getFulfilmentModelBySellerAndSupc(String sellerCode, String supc) throws ExternalDataNotFoundException{
		String fulfilmentModel = null;
		//Get SellerSupcFM mapping for sellerCode and supc
		SellerSupcFMMappingVO sellerSupcFMMappingVO = getSellerSupcFMMappingVO(sellerCode, supc);
		if (null != sellerSupcFMMappingVO && sellerSupcFMMappingVO.isEnabled()) {
			fulfilmentModel = sellerSupcFMMappingVO.getFulfillmentModel();
		}
		LOG.debug("Returning fm from seller-supc mapping {} for seller {}  supc {} ", fulfilmentModel, sellerCode, supc);
		return fulfilmentModel;
	}

	/**
	 * Returns the fulfilment model by sellercode and subcat from aerospike
	 * 
	 * @param sellerCode
	 * @param supc
	 * @param subcat
	 * @return fulfilmentModel
	 * @throws ExternalDataNotFoundException
	 */
	@Override
	public String getFulfilmentModelBySellerAndSubcat(String sellerCode, String supc, String subcat) throws ExternalDataNotFoundException{
		GetMinProductInfoResponse resp = null;
		String fulfilmentModel = null;
		if (StringUtils.isEmpty(subcat)) {
			try {
				resp = externalServiceFactory.getCAMSExternalService().getMinProductInfo(new GetMinProductInfoBySupcRequest(supc));
			} catch (ExternalDataNotFoundException e) {
				LOG.error("No subcat found for supc {}", e, supc);
				throw e;
			}
			if (null != resp && resp.isSuccessful() && resp.getProductInfo() != null) {
				subcat = resp.getProductInfo().getCategoryUrl();
			}
		}
		//Get SellerSubcatFM mapping for sellerCode and subcat
		SellerSubcatFMMappingVO sellerSubcatFMMappingVO = getSellerSubcatFMMappingVO(sellerCode, subcat);
		if (null != sellerSubcatFMMappingVO && sellerSubcatFMMappingVO.isEnabled()) {
			fulfilmentModel = sellerSubcatFMMappingVO.getFulfillmentModel();
		}
		LOG.info("Returning fm from seller-subcat mapping {} for seller {}  subcat {} ", fulfilmentModel, sellerCode, subcat);
		return fulfilmentModel;
	}

	/**
	 * Returns true if seller exist in aerospike otherwise return False
	 * 
	 * @param sellerCode
	 * @return isSellerExist
	 * @throws ExternalDataNotFoundException
	 */
	@Override
	public boolean isSellerExists(String sellerCode) throws ExternalDataNotFoundException {
		boolean bResult = false;
		//Get SellerFM mapping for sellerCode
		SellerFMMappingVO sellerFMMappingVO = getSellerFMMappingVO(sellerCode);
		if (null != sellerFMMappingVO && sellerFMMappingVO.isEnabled()) {
			bResult = true;
		}
		return bResult;
	}

	/**
	 * Returns the SellerFM Mapping from Aerospike.
	 * 
	 * @param sellerCode
	 * @return sellerFMMappingDTO
	 * @throws ExternalDataNotFoundException
	 */
	@Override
	public SellerFmFcDTO getSellerFmFcDTO(String sellerCode) throws ExternalDataNotFoundException {
		//Get SellerFM mapping for sellerCode
		SellerFMMappingVO sellerFMMappingVO = getSellerFMMappingVO(sellerCode);
		if(sellerFMMappingVO != null){
			return new SellerFmFcDTO(sellerFMMappingVO);
		}
		return null;
	}

	/**
	 * Returns the SellerSupcFmFc Mapping from Aerospike.
	 * 
	 * @param sellerCode
	 * @Param supc
	 * @return SellerSupcFmFcDTO
	 * @throws ExternalDataNotFoundException
	 */
	@Override
	public SellerSupcFmFcDTO getSellerSupcFmFcDTO(String sellerCode,String supc) throws ExternalDataNotFoundException {
		//Get SellerFM mapping for sellerCode
		SellerSupcFMMappingVO sellersupcFMMappingVO = getSellerSupcFMMappingVO(sellerCode,supc);
		if(sellersupcFMMappingVO != null){
			return new SellerSupcFmFcDTO(sellersupcFMMappingVO);
		}
		return null;
	}
	
	/////////////////////////////////
	//////// Private Methods ////////
	/////////////////////////////////

	private SellerFMMappingVO getSellerFMMappingVO(String sellerCode) throws ExternalDataNotFoundException{
		if (StringUtils.isEmpty(sellerCode)) {
			LOG.warn("Illegal paramters passed while trying to find fm at seller level");
			throw new ExternalDataNotFoundException("Required Parameters not there while getting FM At Seller Level");
		}

		LOG.debug("Going to read fulfilment model from Aerospike for sellercode {}", sellerCode);
		SellerFMMappingVO sellerFMMappingVO = (SellerFMMappingVO) fmAerospikeService.getFMMapping(AerospikeKeyHelper.getKey(sellerCode), SellerFMMappingVO.class);
		LOG.debug("Fulfilment Model obtained from Aerospike: " + sellerFMMappingVO);
		return sellerFMMappingVO;
	}

	private SellerSupcFMMappingVO getSellerSupcFMMappingVO(String sellerCode, String supc) throws ExternalDataNotFoundException{
		if (StringUtils.isEmpty(sellerCode) || StringUtils.isEmpty(supc)) {
			LOG.warn("Illegal paramters passed while trying to find fm at seller and supc level");
			throw new ExternalDataNotFoundException("Required Parameters not there while getting FM At Seller and Supc Level");
		}

		LOG.debug("Going to read fulfilment model from Aerospike for sellercode {} and supc {}", sellerCode, supc);
		SellerSupcFMMappingVO sellerSupcFMMappingVO = (SellerSupcFMMappingVO) fmAerospikeService.getFMMapping(AerospikeKeyHelper.getKey(sellerCode, supc),
				SellerSupcFMMappingVO.class);
		LOG.debug("Fulfilment Model obtained from Aerospike:" + sellerSupcFMMappingVO);
		return sellerSupcFMMappingVO;
	}

	private SellerSubcatFMMappingVO getSellerSubcatFMMappingVO(String sellerCode, String subcat) throws ExternalDataNotFoundException{
		if (StringUtils.isEmpty(subcat) || StringUtils.isEmpty(sellerCode)) {
			LOG.warn("Illegal paramters passed while trying to find fm at seller and subcat level");
			throw new ExternalDataNotFoundException("Required Parameters not there while getting FM At Seller and Subcat Level");
		}

		LOG.debug("Going to read fulfilment model from Aerospike for sellercode {} and subcat {}", sellerCode, subcat);
		SellerSubcatFMMappingVO sellerSubcatFMMappingVO = (SellerSubcatFMMappingVO) fmAerospikeService.getFMMapping(AerospikeKeyHelper.getKey(sellerCode, subcat),
				SellerSubcatFMMappingVO.class);
		LOG.debug("Fulfilment Model obtained from Aerospike:" + sellerSubcatFMMappingVO);
		return sellerSubcatFMMappingVO;
	}
}
