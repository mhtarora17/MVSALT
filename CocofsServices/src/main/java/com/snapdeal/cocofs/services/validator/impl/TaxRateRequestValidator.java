package com.snapdeal.cocofs.services.validator.impl;

import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.model.request.GetTaxInfoRequest;
import com.snapdeal.cocofs.model.request.GetTaxRateInfoBySUPCStateSellerPriceRequest;
import com.snapdeal.cocofs.services.validator.IRequestValidator;

/**
 * Request vaidator for Taxrate API
 * 
 * @author gaurav
 *
 */
@Service("taxRateRequestValidator")
public class TaxRateRequestValidator implements IRequestValidator<GetTaxRateInfoBySUPCStateSellerPriceRequest>{

	
	@Override
	public boolean validate(GetTaxRateInfoBySUPCStateSellerPriceRequest request) {
		
		//Price cannot be negative
		if(request.getPrice()==null || request.getPrice() < 0)
			return false;
			
		//All input constraints passed...
		return true;
	}
	
	public boolean validate(GetTaxInfoRequest request) {
        
        //Price cannot be negative
        if(request.getPrice()==null || request.getPrice() < 0)
            return false;
            
        //All input constraints passed...
        return true;
    }

}
