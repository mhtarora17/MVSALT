/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 9, 2013
 *  @author himanshu
 */
package com.snapdeal.cocofs.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.base.validation.ValidationError;
import com.snapdeal.catalog.base.model.GetMinProductInfoBySupcRequest;
import com.snapdeal.catalog.base.model.GetMinProductInfoResponse;
import com.snapdeal.cocofs.access.IProductAttributeService;
import com.snapdeal.cocofs.common.CocofsRuleBlock;
import com.snapdeal.cocofs.common.PackagingTypeEvaluationParams;
import com.snapdeal.cocofs.common.PackagingTypeForSUPCResponse;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.external.service.factory.IExternalServiceFactory;
import com.snapdeal.cocofs.model.response.ApiErrorCode;
import com.snapdeal.cocofs.mongo.CocofsMongoQuery;
import com.snapdeal.cocofs.mongo.CocofsMongoQuery.Operator;
import com.snapdeal.cocofs.mongo.Criteria;
import com.snapdeal.cocofs.mongo.mao.IGenericMao;
import com.snapdeal.cocofs.mongo.model.CocofsRuleUnit;
import com.snapdeal.cocofs.mongo.model.CocofsRuleUnit.CocofsRuleUnitFieldName;
import com.snapdeal.cocofs.mongo.model.ProductAttributeUnit;
import com.snapdeal.cocofs.services.IPackagingTypeService;
import com.snapdeal.cocofs.sro.PackagingType;
import com.snapdeal.cocofs.utils.DangerousGoodsTypeConversionUtil;
import com.snapdeal.cocofs.utils.UtilHelper;
import com.snapdeal.rule.engine.entity.BlockParams;
import com.snapdeal.rule.engine.entity.Rule;
import com.snapdeal.rule.engine.evaluator.impl.RuleEvaluator;
import com.snapdeal.rule.engine.services.IRulesService;

@Service("packagingTypeService")
public class PackagingTypeServiceImpl extends RuleEvaluator implements IPackagingTypeService {

    private static final Logger      LOG = LoggerFactory.getLogger(PackagingTypeServiceImpl.class);

    @Autowired
    private IExternalServiceFactory  factory;

    @Autowired
    private IProductAttributeService productAttrService;

    @Autowired
    private IRulesService            ruleService;

    @Autowired
    private IGenericMao              genericMao;

    @Override
    public PackagingTypeForSUPCResponse getPackagingTypeForSUPC(String supc) {

        return getPackagingTypeForSUPC(supc, null);

    }

    @Override
    public PackagingTypeForSUPCResponse getPackagingTypeForSUPC(String supc, String categoryUrl) {

        return getPackagingType(StringUtils.isNotEmpty(supc) ? productAttrService.getProductAttributeUnitBySUPC(supc) : null, supc, categoryUrl);

    }

    @Override
    public PackagingTypeForSUPCResponse getPackagingType(ProductAttributeUnit paUnit, String supc, String categoryUrl) {
        PackagingTypeForSUPCResponse response = new PackagingTypeForSUPCResponse();
        response.setSupc(supc);
        response.setError(true);
        if (StringUtils.isEmpty(supc)) {
            LOG.error("Empty supc passed in request to get packaging type");
            ValidationError validationError = new ValidationError();
            validationError.setCode(ApiErrorCode.REQUIRED_FIELD_NULL.code());
            validationError.setMessage("SUPC passed is empty/null");
            response.setValidationError(validationError);
            return response;
        }
        if (paUnit != null) {
            response.setError(false);
            if (ConfigUtils.getBooleanScalar(Property.WOODEN_PACKAGING_WITH_SUPC_LEVEL_EXCEPTION)) {
                response.setPackageType(paUnit.isWoodenPackaging() ? PackagingType.WOODEN : getPackagingTypeForSUPCByCategoryRules(paUnit, categoryUrl));
            } else if (ConfigUtils.getBooleanScalar(Property.WOODEN_PACKAGING_AT_ONLY_SUPC_LEVEL)) {
                response.setPackageType(paUnit.isWoodenPackaging() ? PackagingType.WOODEN : PackagingType.NORMAL);
            } else {
                response.setPackageType(getPackagingTypeForSUPCByCategoryRules(paUnit, categoryUrl));
            }
        } else {
            LOG.error("No pa unit found for supc " + supc);
            ValidationError validationError = new ValidationError();
            validationError.setCode(ApiErrorCode.REQUESTED_DATA_DOES_NOT_EXISTS.code());
            validationError.setMessage("No Product Attributes found for supc " + supc);
            response.setValidationError(validationError);
        }
        return response;
    }

    @Override
    public PackagingTypeForSUPCResponse getPackagingType(ProductAttributeUnit paUnit, String supc) {
        return getPackagingType(paUnit, supc, null);
    }

    public PackagingType getPackagingTypeForSUPCByCategoryRules(ProductAttributeUnit pau, String categoryUrl) {

        List<Rule> rules = getPackagingTypeRulesForSUPC(pau.getSupc(), categoryUrl);
        if ((null == rules) || rules.isEmpty()) {
            return PackagingType.NORMAL;
        } else {
            return evaluatePackagingType(pau, rules);

        }
    }

    private PackagingType evaluatePackagingType(ProductAttributeUnit pau, List<Rule> rules) {
        LOG.debug("Going to evaluate packaging type rules for supc {}", pau.getSupc());

        boolean isWoodenPackaging = isProductChosenForWoodenPackaging(rules, pau);
        if (isWoodenPackaging) {
            LOG.info("Wooden Packaging True for SUPC " + pau.getSupc() + " Atleast one Rule passed ");
            return PackagingType.WOODEN;
        } else {
            LOG.info("Normal Packaging True for SUPC " + pau.getSupc() + " no Rule passed ");
            return PackagingType.NORMAL;
        }

    }

    private boolean isProductChosenForWoodenPackaging(List<Rule> rules, ProductAttributeUnit pau) {
        boolean isWoodenPackaging;
        List<BlockParams> blockParams = ruleService.getBlockParamsByBlockName(CocofsRuleBlock.PACKAGING_TYPE.getName());
        LOG.debug("Got Packaging Type Block Param for supc {}", pau.getSupc());
        PackagingTypeEvaluationParams paramDTO = getPackagingTypeEvaluationParam(pau);
        LOG.debug("Got Packaging Type Evaluation Param for supc {} param {} ", pau.getSupc(), paramDTO);

        /*
         * The following populateRuleParamsFromBlockObject method is incorrectly named
         * what it really does is takes a set of block params ( bunch of fields against which
         * a rule is evaluated, e.g. price, pickup pincode, weight etc ) 
         * and an dto object which has concrete values for these fields
         * e.g dto.getPrice() returns 50 and dto.getWeight() returns 100.4 etc.
         * it returns a simple map e.g {"price" : 50 , "weight" : 100.4 }
         * which is used to evaluate rules of the given block.
         */
        Map<String, Object> ruleParam = populateRuleParamsFromBlockObject(paramDTO, blockParams);

        for (Entry<String, Object> e : ruleParam.entrySet()) {
            LOG.debug("Parameter " + e.getKey() + " Value " + e.getValue());
        }
        isWoodenPackaging = evaluateRules(rules, ruleParam);
        return isWoodenPackaging;
    }

    private List<Rule> getPackagingTypeRulesForSUPC(String supc, String categoryUrl) {

        if (StringUtils.isEmpty(categoryUrl)) {
            GetMinProductInfoBySupcRequest request = new GetMinProductInfoBySupcRequest(supc);
            GetMinProductInfoResponse response;
            try {
                response = factory.getCAMSExternalService().getMinProductInfo(request);
            } catch (ExternalDataNotFoundException e) {
                LOG.info("Could not obtain category for supc " + supc, e);
                return new ArrayList<Rule>();
            }
            if ((null == response) || !response.isSuccessful()) {
                LOG.info("Could not obtain category for supc response null or unsuccessfull " + supc);
                return new ArrayList<Rule>();
            }
            categoryUrl = response.getProductInfo().getCategoryUrl();
        }
        LOG.info("SUPC " + supc + " Category " + categoryUrl);
        List<CocofsRuleUnit> ruleList = fetchWoodenPackagingRuleForCategoryOrAllCategory(categoryUrl);

        if (null == ruleList || ruleList.isEmpty()) {
            LOG.info("There are no packaging type rule for ALL category or category " + categoryUrl + " for supc " + supc);
            return new ArrayList<Rule>();
        } else {
            LOG.info("For SUPC " + supc + " of Category " + categoryUrl + " rule found " + ruleList);
        }
        // RuleEvalutor converts Rule To RuleDTO to actually evaluate rules
        // only required fields are id, name, criteria,  priority
        // hence we populate only populate relevant fields from
        // cocofs rule unit
        List<Rule> list = new ArrayList<Rule>();
        for (CocofsRuleUnit unit : ruleList) {
            Rule r = RuleEvaluationHelper.getRuleFromCocofsRuleUnit(unit);
            list.add(r);
        }

        return list;
    }

    private List<CocofsRuleUnit> fetchWoodenPackagingRuleForCategoryOrAllCategory(String categoryUrl) {
        List<CocofsRuleUnit> outList = new ArrayList<CocofsRuleUnit>();
        List<Criteria> queryCriterias = new ArrayList<Criteria>();
        Criteria mqcForCategory = new Criteria(CocofsRuleUnitFieldName.CATEGORY_URL.getName(), categoryUrl, Operator.EQUAL);
        Criteria mqcForType = new Criteria(CocofsRuleUnitFieldName.TYPE.getName(), CocofsRuleBlock.PACKAGING_TYPE.getName(), Operator.EQUAL);
        queryCriterias.add(mqcForCategory);
        queryCriterias.add(mqcForType);

        LOG.info("Fetching packaging type rules for category " + categoryUrl);
        List<CocofsRuleUnit> ruleList = genericMao.getDocumentList(new CocofsMongoQuery(queryCriterias), CocofsRuleUnit.class);
        if (null == ruleList) {
            ruleList = new ArrayList<CocofsRuleUnit>();
        }
        Date now = DateUtils.getCurrentTime();
        LOG.debug("Fetched " + ruleList.size() + " rules for packaging type with category " + categoryUrl);
        outList = RuleEvaluationHelper.getRuleValidOnDate(ruleList, now);

        if ((null == outList) || outList.isEmpty()) {
            queryCriterias = new ArrayList<Criteria>();
            mqcForCategory = new Criteria(CocofsRuleUnitFieldName.CATEGORY_URL.getName(), "ALL", Operator.EQUAL);
            mqcForType = new Criteria(CocofsRuleUnitFieldName.TYPE.getName(), CocofsRuleBlock.PACKAGING_TYPE.getName(), Operator.EQUAL);
            queryCriterias.add(mqcForCategory);
            queryCriterias.add(mqcForType);
            LOG.info("Fetching packaging type rules for ALL as no rule found for category " + categoryUrl);
            ruleList = genericMao.getDocumentList(new CocofsMongoQuery(queryCriterias), CocofsRuleUnit.class);
            outList = RuleEvaluationHelper.getRuleValidOnDate(ruleList, now);
        }
        return outList;
    }

    private PackagingTypeEvaluationParams getPackagingTypeEvaluationParam(ProductAttributeUnit pau) {
        PackagingTypeEvaluationParams p = new PackagingTypeEvaluationParams();
        p.setFragile(pau.isFragile());
        p.setHazMat(DangerousGoodsTypeConversionUtil.isHazmat(pau));
        p.setLiquid(DangerousGoodsTypeConversionUtil.isLiquid(pau));
        // Volumetric Weight as defined in Arjun's doc on SNAPDEALTECH-11840 for purpose of rule evaluation
        p.setVolumetricWeight(UtilHelper.getVolumetricWeightFromLBH(pau.getLength(), pau.getBreadth(), pau.getHeight()));
        p.setWeight(pau.getWeight());
        LOG.info("SUPC : " + pau.getSupc() + " PackagingType Param " + p);
        return p;
    }
}
