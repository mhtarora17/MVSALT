package com.snapdeal.cocofs.services.fm;

import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.generic.persister.exception.GenericPersisterException;
import com.snapdeal.cocofs.services.events.dto.FCAddressUpdateEventObj;
import com.snapdeal.cocofs.services.events.dto.FCAttributeUpdateEventObj;
import com.snapdeal.score.data.aerospike.exception.GetFailedException;
import com.snapdeal.score.data.aerospike.exception.PutFailedException;

public interface ISellerSupcSdInstantMappingProcessor extends ISellerSUPCFMMappingProcessor {
 // update of fc
    // add of fc
    /**
    * API to process the fc address update and find sellercode, SUPC code tuple on that fc and check if any notification is to be send for the tuple. All such
    * notifications are sent further by this API.
    * 
    * @param event
    * @return false if the push is not successful and possibly need to re-attempted by client
    * @throws GetFailedException
    * @throws ExternalDataNotFoundException
    * @throws PutFailedException
    * @throws GenericPersisterException
    */
   public boolean processChangeOfFCAttributesForExternalNotification(FCAttributeUpdateEventObj event) throws GetFailedException, ExternalDataNotFoundException;

}
