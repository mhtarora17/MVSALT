/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 07-Oct-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.services.fm.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.core.aerospike.vo.SellerSubcatFMMappingVO;
import com.snapdeal.cocofs.dataaccess.fm.IFMAerospikeDataReadService;
import com.snapdeal.cocofs.dataaccess.fm.IFMDBDataReadService;
import com.snapdeal.cocofs.entity.SellerSubcatFMMapping;
import com.snapdeal.cocofs.services.IDataReaderWithAerospike;
import com.snapdeal.cocofs.services.fm.dto.SellerSubcatFMMappingUpdateDto;
import com.snapdeal.cocofs.utils.AerospikeKeyHelper;

@Service("SellerSubcatFMMappingDataReader")
public class SellerSubcatFMMappingDataReader implements IDataReaderWithAerospike<SellerSubcatFMMappingUpdateDto, SellerSubcatFMMapping, SellerSubcatFMMappingVO> {

     @Autowired
    private IFMDBDataReadService fmDBDataReadService;

    @Autowired
    private IFMAerospikeDataReadService  fmAerospikeReadService;

    
    /*@Override
    @Deprecated
    public List<SellerSubcatFMMappingUnit> getDocumentsForDTO(SellerSubcatFMMappingUpdateDto dto) {
        return null;
        //return fmMongoService.getSellerSubcatFMMappingUnit(dto.getSellerCode());
    }*/

    @Override
    public List<SellerSubcatFMMapping> getEntitiesForDTO(SellerSubcatFMMappingUpdateDto dto) {
        return fmDBDataReadService.getSellerSubcatFMMapping(dto.getSellerCode());
    }
    
    @Override
    public SellerSubcatFMMappingVO getRecordForDTO(SellerSubcatFMMappingUpdateDto dto) {
        SellerSubcatFMMappingVO record = (SellerSubcatFMMappingVO)fmAerospikeReadService.getFMMapping(AerospikeKeyHelper.getKey(dto.getSellerCode(),dto.getSubcat()), SellerSubcatFMMappingVO.class);
        
        return record;
    }

}
