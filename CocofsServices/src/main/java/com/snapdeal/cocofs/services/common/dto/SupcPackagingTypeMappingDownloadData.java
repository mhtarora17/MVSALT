/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.services.common.dto;

import java.util.Date;

/**
 *  
 *  @version     1.0, 23-Dec-2015
 *  @author vikassharma03
 */
public class SupcPackagingTypeMappingDownloadData {
    
   
    private String              supc;
    
    private String              storeCode;
    
    private String              packagingType;
    
    private Boolean             enable;
    
    private Date              created;
    
    private Date              lastUpdated;

    private String            updatedBy;
    

    
    
    
    public SupcPackagingTypeMappingDownloadData() {
       
    }


    public SupcPackagingTypeMappingDownloadData(String supc, String storeCode, String packagingType, Boolean enable, Date created,
            Date lastUpdated, String updatedBy) {
        this.supc = supc;
        this.storeCode = storeCode;
        this.packagingType = packagingType;
        this.enable = enable;
        this.created = created;
        this.lastUpdated = lastUpdated;
        this.updatedBy = updatedBy;
    }

    public String getSupc() {
        return supc;
    }

    public void setSupc(String supc) {
        this.supc = supc;
    }

    public String getStoreCode() {
        return storeCode;
    }

    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }

    public String getPackagingType() {
        return packagingType;
    }

    public void setPackagingType(String packagingType) {
        this.packagingType = packagingType;
    }

    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }
    
    

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Override
    public String toString() {
        return "SupcPackagingTypeMappingDownloadData [supc=" + supc + ", storeCode=" + storeCode + ", packagingTypeCode=" + packagingType + ", enable=" + enable + ", created=" + created + ", lastUpdated=" + lastUpdated + ", updatedBy=" + updatedBy + "]";
    }

   
    
    
    

}
