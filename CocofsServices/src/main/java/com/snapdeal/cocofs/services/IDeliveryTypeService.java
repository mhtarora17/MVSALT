/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 9, 2013
 *  @author himanshu
 */
package com.snapdeal.cocofs.services;

import java.util.List;

import com.snapdeal.cocofs.common.DeliveryTypeForSUPCResponse;
import com.snapdeal.cocofs.sro.DeliveryType;

public interface IDeliveryTypeService {
    
    public DeliveryTypeForSUPCResponse getDeliveryTypeForSUPC(String supc);

    DeliveryType getDeliverTypeForShippingModes(List<String> shippingModes);

    DeliveryTypeForSUPCResponse getDeliveryTypeForSUPC(String supc, String categoryUrl);

}
