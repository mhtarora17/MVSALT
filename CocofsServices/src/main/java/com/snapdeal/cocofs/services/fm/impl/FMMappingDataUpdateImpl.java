/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-Oct-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.services.fm.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.cocofs.annotations.Throttled;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.core.aerospike.vo.SellerFMMappingVO;
import com.snapdeal.cocofs.core.aerospike.vo.SellerSubcatFMMappingVO;
import com.snapdeal.cocofs.core.aerospike.vo.SellerSupcFMMappingVO;
import com.snapdeal.cocofs.dataaccess.fm.IFMAerospikeDataReadService;
import com.snapdeal.cocofs.dataaccess.fm.IFMDBDataReadService;
import com.snapdeal.cocofs.entity.SellerFMMapping;
import com.snapdeal.cocofs.entity.SellerSubcatFMMapping;
import com.snapdeal.cocofs.entity.SellerSupcFMMapping;
import com.snapdeal.cocofs.generic.persister.exception.GenericPersisterException;
import com.snapdeal.cocofs.generic.persister.request.GenericPersisterWithAerospikeRequest;
import com.snapdeal.cocofs.generic.persister.response.GenericPersisterWithAerospikeResponse;
import com.snapdeal.cocofs.generic.persister.service.IGenericPersisterService;
import com.snapdeal.cocofs.services.fm.IFMMappingDataUpdater;

@Service("FMMappingDataUpdateImpl")
public class FMMappingDataUpdateImpl implements IFMMappingDataUpdater {

    private static final Logger         LOG = LoggerFactory.getLogger(FMMappingDataUpdateImpl.class);

    @Autowired
    private IGenericPersisterService    persistenceService;

    @Autowired
    private IFMDBDataReadService        fmDBDataReadService;

    @Autowired
    private IFMAerospikeDataReadService fmAerospikeReadService;

    @Override
    @Throttled(overridingProperty = Property.SELLER_SUPC_UPLOAD_THROTTLE_PROPERTY)
    public GenericPersisterWithAerospikeResponse<SellerSupcFMMapping, SellerSupcFMMappingVO> updateSellerSupcFMMapping(List<SellerSupcFMMapping> entityListToSave,
            List<SellerSupcFMMappingVO> recordListToSave) throws GenericPersisterException {

        GenericPersisterWithAerospikeRequest<SellerSupcFMMapping, SellerSupcFMMappingVO> req = new GenericPersisterWithAerospikeRequest<SellerSupcFMMapping, SellerSupcFMMappingVO>();
        req.setEntitiesToPersist(entityListToSave);
        req.setRecordsToPersist(recordListToSave);
        req.setAsynchToAerospike(false);
        req.setSkipValidation(false);
        GenericPersisterWithAerospikeResponse<SellerSupcFMMapping, SellerSupcFMMappingVO> resp = persistenceService.persistOneWithAerospike(req);

        if (resp.isSuccessful()) {
            for (SellerSupcFMMapping mapping : entityListToSave) {

                //If a seller-supc mappping is being disabled, then we also have to check the exception flag at seller level
                if (!mapping.isEnabled()) {
                    checkAndMarkSupcExistsForSellerFMMapping(mapping.getSellerCode());
                } else {
                    markSupcExistsForSellerFMMapping(mapping.getSellerCode(), mapping.getSupc());
                }
            }

        }
        return resp;
    }

    @Override
    public GenericPersisterWithAerospikeResponse<SellerSubcatFMMapping, SellerSubcatFMMappingVO> updateSellerSubcatFMMapping(List<SellerSubcatFMMapping> entityListToSave,
            List<SellerSubcatFMMappingVO> recordListToSave) throws GenericPersisterException {
        GenericPersisterWithAerospikeRequest<SellerSubcatFMMapping, SellerSubcatFMMappingVO> req = new GenericPersisterWithAerospikeRequest<SellerSubcatFMMapping, SellerSubcatFMMappingVO>();
        req.setEntitiesToPersist(entityListToSave);
        req.setRecordsToPersist(recordListToSave);
        req.setAsynchToAerospike(false);
        req.setSkipValidation(false);
        GenericPersisterWithAerospikeResponse<SellerSubcatFMMapping, SellerSubcatFMMappingVO> resp = persistenceService.persistOneWithAerospike(req);

        if (resp.isSuccessful()) {
            for (SellerSubcatFMMapping mapping : entityListToSave) {
                markSubcatExistsForSellerFMMapping(mapping.getSellerCode(), mapping.getSubcat());
            }
        }

        return resp;
    }

    private void markSubcatExistsForSellerFMMapping(String sellerCode, String subcat) throws GenericPersisterException {

        SellerFMMapping mapping = fmDBDataReadService.getEnabledSellerFMMapping(sellerCode);
        SellerFMMappingVO record = (SellerFMMappingVO) fmAerospikeReadService.getFMMapping(sellerCode, SellerFMMappingVO.class);

        if (mapping != null && !mapping.isSubcatExist()) {
            LOG.info("marking subcatExists true in sellerFMMapping while setting fulfillmentModel for seller {}, subcat {}", sellerCode, subcat);
            mapping.setSubcatExist(true);

            if (record != null) {
                record.setSubcatExist(1);
            }
            updateSellerFMMapping(mapping, record);
        }

    }

    private void markSupcExistsForSellerFMMapping(String sellerCode, String supc) throws GenericPersisterException {
        SellerFMMapping mapping = fmDBDataReadService.getEnabledSellerFMMapping(sellerCode);

        if (mapping != null && !mapping.isSupcExist()) {
            LOG.info("marking supcExists true in sellerFMMapping while setting fulfillmentModel for seller {}, supc {}", sellerCode, supc);
            mapping.setSupcExist(true);

            SellerFMMappingVO record = (SellerFMMappingVO) fmAerospikeReadService.getFMMapping(sellerCode, SellerFMMappingVO.class);
            if (record != null) {
                record.setSupcExist(1);
            }

            updateSellerFMMapping(mapping, record);
        }

    }

    /*
     * If a Seller-SUPC mapping is getting disabled, we also need to check whether the exception flag
     * at seller level needs to be disabled or not.  If size(sellercode,supc,enabled = 1) ==0, then we need
     * reset the flag at seller level 
     */
    private void checkAndMarkSupcExistsForSellerFMMapping(String sellerCode) throws GenericPersisterException {
        long count = fmDBDataReadService.getEnabledSellerSupcFMMappingCount(sellerCode);

        // if count is not ZERO, let it be
        if (count > 0) {
            return;
        }

        SellerFMMapping mapping = fmDBDataReadService.getEnabledSellerFMMapping(sellerCode);

        //update only when size of enabled seller-supc is 0 and the flag is 1, if flag is 0 no need to update...
        if (mapping != null && mapping.isSupcExist()) {
            LOG.info("Marking supcExists false in sellerFMMapping for seller {}", sellerCode);
            mapping.setSupcExist(false);

            SellerFMMappingVO record = (SellerFMMappingVO) fmAerospikeReadService.getFMMapping(sellerCode, SellerFMMappingVO.class);
            if (record != null) {
                record.setSupcExist(0);
            }

            updateSellerFMMapping(mapping, record);
        }
    }

    private void updateSellerFMMapping(SellerFMMapping entity, SellerFMMappingVO record) throws GenericPersisterException {
        List<SellerFMMapping> entityList = new ArrayList<SellerFMMapping>();
        entity.setLastUpdated(DateUtils.getCurrentTime());
        entityList.add(entity);

        List<SellerFMMappingVO> recordList = new ArrayList<SellerFMMappingVO>();
        if (record != null) {
            recordList.add(record);
        }

        GenericPersisterWithAerospikeRequest<SellerFMMapping, SellerFMMappingVO> req = new GenericPersisterWithAerospikeRequest<SellerFMMapping, SellerFMMappingVO>();

        /** Not required as we will not set subcat.supc flag in mongo **/
        // req.setDocumentsToPersist(docListToSave); 
        req.setEntitiesToPersist(entityList);
        req.setRecordsToPersist(recordList);
        req.setAsynchToAerospike(false);
        req.setSkipValidation(false);
        GenericPersisterWithAerospikeResponse<SellerFMMapping, SellerFMMappingVO> resp = persistenceService.persistOneWithAerospike(req);

    }
    
}
