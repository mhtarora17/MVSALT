package com.snapdeal.cocofs.services.task.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.snapdeal.cocofs.entity.EventInstance;
import com.snapdeal.cocofs.services.events.IEventHandlerService;

public class EventInstanceTask extends QuartzTask {

    private static final Logger  LOG = LoggerFactory.getLogger(EventInstanceTask.class);

    @Autowired
    private IEventHandlerService eventHandlerService;

    @Override
    public void execute(String taskName) {
        LOG.info("Event Instance Task Started...");
        List<EventInstance> eventsToExecute = eventHandlerService.getEventsToExecute(false);
        LOG.info("Fetched events to execute. Count=" + eventsToExecute.size());
        for (EventInstance event : eventsToExecute) {
            try {
                LOG.info("Events execution started for event: {}", event);
                eventHandlerService.callEventSynchronouslyWithEvent(event);
                LOG.info("Events excution completed.");
            } catch (Exception ex) {
                LOG.error("An error occured while executind an event:{}", ex);
            }
        }
        LOG.info("Event Instance Task Completed...");
    }

}
