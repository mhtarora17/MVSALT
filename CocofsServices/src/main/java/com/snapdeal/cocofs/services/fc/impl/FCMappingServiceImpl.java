/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.services.fc.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.cocofs.core.aerospike.vo.SellerFMMappingVO;
import com.snapdeal.cocofs.core.aerospike.vo.SellerSupcFMMappingVO;
import com.snapdeal.cocofs.dataaccess.fm.IFMAerospikeDataReadService;
import com.snapdeal.cocofs.dataaccess.fm.IFMDBDataReadService;
import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.services.fc.IFCMappingService;
import com.snapdeal.cocofs.services.fm.IFulfilmentModelService;
import com.snapdeal.cocofs.services.fm.dto.SellerFmFcDTO;
import com.snapdeal.cocofs.services.fm.dto.SellerSupcFmFcDTO;
import com.snapdeal.cocofs.services.fm.impl.FMDataSourceServiceFactory;
import com.snapdeal.cocofs.sro.FMFCPair;
import com.snapdeal.cocofs.sro.SellerSUPCPair;
import com.snapdeal.cocofs.utils.AerospikeKeyHelper;

/**
 * @version 1.0, 27-Mar-2015
 * @author ankur
 */

@Service("FCMappingServiceImpl")
public class FCMappingServiceImpl implements IFCMappingService {

    private static final Logger         LOG = LoggerFactory.getLogger(FCMappingServiceImpl.class);

    @Autowired
    private IFMAerospikeDataReadService fmAerospikeService;

    @Autowired
    private IFMDBDataReadService        fmDBReadService;

    @Autowired
    IFulfilmentModelService             fmModelService;
    
    @Autowired
    private FMDataSourceServiceFactory                                                                               dataSourceService;


    @Override
    public Map<SellerSUPCPair, FMFCPair> getFulfilmentCentres(List<SellerSUPCPair> sroList) throws ExternalDataNotFoundException {
        Map<SellerSUPCPair, FMFCPair> sellerSupcSROToFCMap = new HashMap<SellerSUPCPair, FMFCPair>();
        for (SellerSUPCPair sro : sroList) {
            FMFCPair fmfc = getFulfilmentCentreCodes(sro.getSellerCode(), sro.getSupc());
            sellerSupcSROToFCMap.put(sro, fmfc);
        }
        return sellerSupcSROToFCMap;
    }

    @Override
    public FMFCPair getFulfilmentCentreCodes(String sellerCode, String supc) throws ExternalDataNotFoundException {
        FMFCPair fmfc = new FMFCPair();
        String fmModel = null;
        boolean supcExists = false;
        List<String> fcCodes = null;
        LOG.debug("Looking up fulfilment centres by sellercode  first..");

        SellerFmFcDTO fmDTO = dataSourceService.getFMServiceForSellerFmLookup().getSellerFmFcDTO(sellerCode);


        LOG.debug("Fulfilment Model and centres obtained from aerospike:" + fmDTO);
        if (fmDTO != null && fmDTO.isEnabled()) {
            fcCodes = fmDTO.getEnabledFCCenters();
            fmModel = fmDTO.getFulfilmentModel();
            supcExists = fmDTO.isSupcExist();

        }

        //if fm is not 
        if (supcExists) {

            LOG.debug("Looking up fulfilment centres by seller and supc  now..");
            SellerSupcFmFcDTO sellersupcFmFcDTO = dataSourceService.getFMServiceForSellerSupcFmLookup().getSellerSupcFmFcDTO(sellerCode,supc);
            if (sellersupcFmFcDTO != null && StringUtils.isNotEmpty(sellersupcFmFcDTO.getFulfilmentModel())) {
                if (sellersupcFmFcDTO.isEnabled()) {
                    fcCodes = sellersupcFmFcDTO.getEnabledFCCenters();
                    fmModel = sellersupcFmFcDTO.getFulfilmentModel();
                }
            }

        }
        fmfc.setFcCodes(fcCodes);
        fmfc.setFulfilmentModel(fmModel);
        return fmfc;

    }

    /* Get the fulfilment centre by sellercode from aerospike
    * 
    * @param sellerCode
    * @return
    * @throws ExternalDataNotFoundException
    */
    @Override
    public SellerFMMappingVO getFulfilmentCentreBySellerFromAerospike(String sellerCode) throws ExternalDataNotFoundException {
        SellerFMMappingVO sellerfcVO = null;
        if (StringUtils.isNotEmpty(sellerCode)) {
            LOG.debug("Going to read fulfilment centre mapping from Aerospike for sellercode {}", sellerCode);
            SellerFMMappingVO sellerFCMappingUnit = (SellerFMMappingVO) fmAerospikeService.getFMMapping(AerospikeKeyHelper.getKey(sellerCode), SellerFMMappingVO.class);
            if (null != sellerFCMappingUnit && sellerFCMappingUnit.isEnabled()) {
                sellerfcVO = sellerFCMappingUnit;
            }
        } else {
            LOG.warn("Illegal paramters passed while trying to find fc at seler level");
            throw new ExternalDataNotFoundException("Required Parameters not there while getting FC At Seller Level");
        }
        return sellerfcVO;
    }

    @Override
    public SellerSupcFMMappingVO getFulfilmentCentreBySellerAndSupcFromAerospike(String sellerCode, String supc) throws ExternalDataNotFoundException {
        SellerSupcFMMappingVO sellerSupcFMMappingVO = null;
        if (StringUtils.isNotEmpty(sellerCode) && StringUtils.isNotEmpty(supc)) {
            //read data from Aerospike
            LOG.debug("Going to read fulfilment centre mapping from Aerospike for sellerCode {} and supc {} ", sellerCode, supc);
            SellerSupcFMMappingVO aerospikeVo = (SellerSupcFMMappingVO) fmAerospikeService.getFMMapping(AerospikeKeyHelper.getKey(sellerCode, supc), SellerSupcFMMappingVO.class);
            LOG.debug("Fulfilment Centre Mapping obtained from Aerospike:" + aerospikeVo);
            if (null != aerospikeVo && aerospikeVo.getEnabled() == 1) {
                sellerSupcFMMappingVO = aerospikeVo;
            }

        } else {
            LOG.warn("Illegal paramters passed while trying to find fc at seller and supc level");
            throw new ExternalDataNotFoundException("Required paramters empty");
        }
        LOG.debug("Returning  seller-supc mapping {} for seller {}  supc {} ", sellerSupcFMMappingVO, sellerCode, supc);
        return sellerSupcFMMappingVO;
    }

}
