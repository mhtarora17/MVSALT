package com.snapdeal.cocofs.services.impl;

import java.util.List;

import javax.naming.OperationNotSupportedException;

import org.drools.core.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.snapdeal.cocofs.access.IPendingProductAttributeDBDataWriteService;
import com.snapdeal.cocofs.entity.PendingProductAttributeUpdate;
import com.snapdeal.cocofs.entity.ProductAttribute;
import com.snapdeal.cocofs.enums.FulfilmentFeeImpactingAttribute;
import com.snapdeal.cocofs.excelupload.ProductAttributeUploadDTO;
import com.snapdeal.cocofs.external.service.IScoreExternalService;
import com.snapdeal.cocofs.generic.persister.exception.GenericPersisterException;
import com.snapdeal.cocofs.generic.persister.response.GenericPersisterResponse;
import com.snapdeal.cocofs.mongo.model.ProductAttributeUnit;
import com.snapdeal.cocofs.services.IDataReader;
import com.snapdeal.cocofs.services.IDataUpdater;
import com.snapdeal.cocofs.services.IPendingPAUDataUpdaterService;
import com.snapdeal.cocofs.services.common.dto.UserInfo;
import com.snapdeal.cocofs.services.converter.IConverterService;
import com.snapdeal.cocofs.services.data.engine.IDataEngine;
import com.snapdeal.cocofs.services.events.IEventProducer;
import com.snapdeal.cocofs.services.events.dto.ProductAttributeScoreEvent;
import com.snapdeal.score.model.request.UpdateProductFulfilmentAttributeRequest;

/**
 * @author nikhil
 */
@Service("pendingPAUDataUpdaterService")
public class PendingPAUDataUpdaterService implements IPendingPAUDataUpdaterService {

    @Autowired
    IDataUpdater                                                                           dataUpdater;

    @Autowired
    @Qualifier("productAttributeUploadReader")
    private IDataReader<ProductAttributeUploadDTO, ProductAttribute, ProductAttributeUnit> dataReader;

    @Autowired
    @Qualifier("productAttributeUploadEngine")
    private IDataEngine<ProductAttributeUploadDTO, ProductAttribute, ProductAttributeUnit> dataEngine;

    @Autowired
    private IPendingProductAttributeDBDataWriteService                                     ppauDataWriteService;

    @Autowired
    private IScoreExternalService                                                          scoreService;

    @Autowired
    private IConverterService                                                              converterService;

    @Autowired
    @Qualifier("ProductAttributeUpdateScoreEventProducer")
    private IEventProducer<ProductAttributeScoreEvent>                                     productAttributeUpdateScoreEventProducer;

    private String                                                                         appender = "PPAU";

    private static final Logger                                                            LOG      = LoggerFactory.getLogger(PendingPAUDataUpdaterService.class);

    @Override
    @Transactional
    public void promotePendingPAU(List<PendingProductAttributeUpdate> updates, String supc) {
        ProductAttributeUploadDTO updateDTOForSupc = createDTO(updates);
        try {
            LOG.info("Request recieved to promote udates for supc {}", supc);
            GenericPersisterResponse<ProductAttribute, ProductAttributeUnit> response = dataUpdater.updateDataWithDTO(updateDTOForSupc, dataReader, dataEngine, new UserInfo(
                    "task", false), true, true);
            if (response.isSuccessful()) {
                LOG.info("Successfully processed pendancy for supc {}, lets mark it unpending now", supc);

                for (PendingProductAttributeUpdate update : updates) {
                    update.setPending(false);
                    ppauDataWriteService.updatePPAU(update);
                }
                /*
                 * At the end, update the attributes in Score in silent mode 
                 */
                try {
                    LOG.info("Pushing updates to SCORE as well ...");
                    ProductAttributeUnit pau = response.getPersistedDocument().get(0);
                    UpdateProductFulfilmentAttributeRequest scoreRequest = converterService.getUpdateProductAttributesSRORequest(pau);
                    boolean result = scoreService.updateProductFulfilmentAttributesInScore(scoreRequest);
                    if (!result) {
                        //add event for retrial
                        ProductAttributeScoreEvent event = new ProductAttributeScoreEvent(scoreRequest);
                        productAttributeUpdateScoreEventProducer.createEventAndCallAsynchronously(event);
                        LOG.info("Added event for updating PA in score...");
                    }
                } catch (Exception ex) {
                    LOG.error("Unable to add event to push PAs to score.{}", ex);
                }
            } else {
                LOG.info("Successfully processed pendancy for supc {}, lets mark it unpeding now", supc);
            }
        } catch (OperationNotSupportedException e) {
            LOG.error("Operation not supported caught for supc attribute for {}, wrong dataEngine could be the reason", supc);
        } catch (GenericPersisterException e) {
            LOG.error("Exception caught while processing pending supc attribute for {} ", supc, e);
        }
    }

    private ProductAttributeUploadDTO createDTO(List<PendingProductAttributeUpdate> updates) {
        ProductAttributeUploadDTO dto = new ProductAttributeUploadDTO();
        if (updates != null && !updates.isEmpty()) {
            for (PendingProductAttributeUpdate update : updates) {
                updateField(dto, update);
                dto.setSupc(update.getSupc());
            }
        }
        return dto;
    }

    private void updateField(ProductAttributeUploadDTO dto, PendingProductAttributeUpdate update) {
        String attrName = update.getAttributeName();
        if (StringUtils.isEmpty(attrName)) {
            throw new IllegalArgumentException("pending record has empty attrName ");
        } else {

            if (attrName.equals(FulfilmentFeeImpactingAttribute.WEIGHT.name())) {
                dto.setWeight(Double.valueOf(update.getNewValue()));
                dto.addCreatedByforAttribute(FulfilmentFeeImpactingAttribute.WEIGHT.name(), update.getCreatedBy() + ";" + appender);
            } else if (attrName.equals(FulfilmentFeeImpactingAttribute.LENGTH.name())) {
                dto.setLength(Double.valueOf(update.getNewValue()));
                dto.addCreatedByforAttribute(FulfilmentFeeImpactingAttribute.LENGTH.name(), update.getCreatedBy() + ";" + appender);
            } else if (attrName.equals(FulfilmentFeeImpactingAttribute.HEIGHT.name())) {
                dto.setHeight(Double.valueOf(update.getNewValue()));
                dto.addCreatedByforAttribute(FulfilmentFeeImpactingAttribute.HEIGHT.name(), update.getCreatedBy() + ";" + appender);
            } else if (attrName.equals(FulfilmentFeeImpactingAttribute.BREADTH.name())) {
                dto.setBreadth(Double.valueOf(update.getNewValue()));
                dto.addCreatedByforAttribute(FulfilmentFeeImpactingAttribute.BREADTH.name(), update.getCreatedBy() + ";" + appender);
            } else if (attrName.equals(FulfilmentFeeImpactingAttribute.SYSTEMWEIGHTCAPTURED.name())) {
                dto.setSystemWeightCaptured(Boolean.valueOf(update.getNewValue()));
                dto.addCreatedByforAttribute(FulfilmentFeeImpactingAttribute.SYSTEMWEIGHTCAPTURED.name(), update.getCreatedBy() + ";" + appender);
            } else {
                throw new IllegalArgumentException("Unknown attribute found in pending state");
            }
        }
    }

}
