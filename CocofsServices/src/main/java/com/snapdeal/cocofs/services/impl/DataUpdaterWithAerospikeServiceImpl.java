/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.services.impl;

import java.util.ArrayList;
import java.util.List;

import javax.naming.OperationNotSupportedException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.generic.persister.exception.GenericPersisterException;
import com.snapdeal.cocofs.generic.persister.exception.GenericPersisterExceptionCode;
import com.snapdeal.cocofs.generic.persister.request.GenericPersisterWithAerospikeRequest;
import com.snapdeal.cocofs.generic.persister.response.GenericPersisterWithAerospikeResponse;
import com.snapdeal.cocofs.generic.persister.service.IGenericPersisterService;
import com.snapdeal.cocofs.services.IDataReaderWithAerospike;
import com.snapdeal.cocofs.services.IDataUpdaterWithAerospike;
import com.snapdeal.cocofs.services.common.dto.UserInfo;
import com.snapdeal.cocofs.services.data.engine.IDataEngineWithAerospike;

/**
 *  
 *  @version     1.0, 15-Sep-2014
 *  @author ankur
 */

@Service("aerospikedataUpdateService")
public class DataUpdaterWithAerospikeServiceImpl implements IDataUpdaterWithAerospike {


    private static final Logger              LOG = LoggerFactory.getLogger(DataUpdaterWithAerospikeServiceImpl.class);
   
    @Autowired
    private IGenericPersisterService         persistenceService;

 
 
    @Override
    @SuppressWarnings("unchecked")
    public <T, E, R> GenericPersisterWithAerospikeResponse<E, R> updateDataWithDTO(T dto, IDataReaderWithAerospike<T, E, R> reader, IDataEngineWithAerospike<T, E, R> engine, UserInfo user, boolean skipGenricValidation
            ) throws GenericPersisterException, OperationNotSupportedException {
 
        

        List<E> entityList = reader.getEntitiesForDTO(dto);
        List<R> recordList = new ArrayList<R>();
       
        R record = reader.getRecordForDTO(dto);
        if(record != null){
            recordList.add(record);
        }

        
        List<E> entityListToSave = engine.enrichEntities(dto, entityList, user.getEmail());
        List<R> recordListToSave = engine.enrichRecords(dto, recordList, user.getEmail());
        
        GenericPersisterWithAerospikeRequest<E, R> req = new GenericPersisterWithAerospikeRequest<E, R>();
        req.setEntitiesToPersist(entityListToSave);
        req.setRecordsToPersist(recordListToSave);
        req.setAsynchToAerospike(false);
        req.setSkipValidation(skipGenricValidation);

        GenericPersisterWithAerospikeResponse<E, R> resp = persistenceService.persistOneWithAerospike(req);
        if (!resp.isSuccessful()) {
            String msg = "DTO " + dto + " not saved , message - " + resp.getMessage();
            LOG.info("GenericPersisterResponse messgae - {}", msg);
            throw new GenericPersisterException(null, msg, GenericPersisterExceptionCode.ENTITY_FAILED_TO_PERSIST);
        }

      
       LOG.info("Successfully persisted all entities.");

        return resp;
    }


}
