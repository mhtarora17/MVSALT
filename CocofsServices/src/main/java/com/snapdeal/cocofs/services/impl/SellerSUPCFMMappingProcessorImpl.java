package com.snapdeal.cocofs.services.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.generic.persister.exception.GenericPersisterException;
import com.snapdeal.cocofs.services.events.dto.SellerFMFCUpdateEventObj;
import com.snapdeal.cocofs.services.events.dto.SellerSupcAddEventObj;
import com.snapdeal.cocofs.services.events.dto.SellerSupcFMFCExceptionAddEventObj;
import com.snapdeal.cocofs.services.events.dto.SellerSupcFMFCExceptionDeleteEventObj;
import com.snapdeal.cocofs.services.fm.IFulfilmentModelService;
import com.snapdeal.cocofs.services.fm.IManualPushOfSellerFMUpdateForExternalNotification;
import com.snapdeal.cocofs.services.fm.ISellerSUPCFMMappingProcessor;
import com.snapdeal.cocofs.services.fm.ISellerToSUPCListDBLookupService;
import com.snapdeal.cocofs.services.producer.IExternalNotificationsServiceForSearchPush;
import com.snapdeal.cocofs.sro.FulfillmentModel;
import com.snapdeal.cocofs.sro.SellerSupcUpdateSRO;
import com.snapdeal.score.data.aerospike.exception.GetFailedException;
import com.snapdeal.score.data.aerospike.exception.PutFailedException;

/**
 * Class to process Seller SUPC FM mapping data update. Collect the notifications and push to external service to
 * publish these notification to activemq (toward search) <br>
 * 1) SALESFORCE send SELLER-FM mapping add/updates via API hit <br>
 * 2) IPSM send SELLER-SUPC updates (new SUPC addition for SELLER) <br>
 * 3) CoCoFS Admin send SELLER-SUPC-FM mappings to update/disable exceptions.<br>
 * Data to be pushed - Seller, SUPC, FM, List<FC>
 * 
 * @author ssuthar
 */

@Service("sellerSUPCFMMappingProcessor")
public class SellerSUPCFMMappingProcessorImpl implements ISellerSUPCFMMappingProcessor, IManualPushOfSellerFMUpdateForExternalNotification {

    private static final Logger                        LOG = LoggerFactory.getLogger(SellerSUPCFMMappingProcessorImpl.class);

    @Autowired
    private ISellerToSUPCListDBLookupService           sellerToSUPCListLookupService;
    @Autowired
    private IFulfilmentModelService                    fulfilmentModelService;

    @Qualifier("externalNotificationsServiceForSearchPush")
    @Autowired
    private IExternalNotificationsServiceForSearchPush sellerSupcSDFulfilledProducer;

    private IDependency                                dep = new Dependency();

    // case when SELLER-FM mapping is added/changed from salesforce
    @Override
    public boolean processChangeOfSellerFMMappingForExternalNotification(SellerFMFCUpdateEventObj event) {

        if (!isSOIFilterFeatureEnabled()) {
            // returning true as this is logically correct termination of the process flow
            return true;
        }
        LOG.info("processing started for change of seller fm mapping, event: {}", event);

        // find the corresponding FM type
        FulfillmentModel newFMType = FulfillmentModel.getFulfilmentModelByCode(event.getNewFM());
        FulfillmentModel oldFMType = FulfillmentModel.getFulfilmentModelByCode(event.getOldFM());

        List<SellerSupcUpdateSRO> sellerSUPCFMMappingForSearchPush = new ArrayList<SellerSupcUpdateSRO>();
        // case 1) SELLER-FM is added first time, as this is supposed to be handled at SUPC push from IPMS
        if (isNewFMFC_VOIAndExistingFMWasNone(newFMType, oldFMType)) {
            LOG.info("new SELLER-FM mapping is added for seller:{} FM:{}, no information is to be published for external service", event.getSellerCode(), event.getNewFM());
        } else if (isFC_VOIFMToggled(newFMType, oldFMType)) {
            // case 2) default FM for SELLER-FM is updated and FC_VOI is toggled

            // find all SUPC for the seller and create search push for all SUPC
            // except in exception list
            prepareSupcSellerListToBePushed(event.getSellerCode(), newFMType, sellerSUPCFMMappingForSearchPush);
        }

        boolean bResult = pushToExternalNotificationService(sellerSUPCFMMappingForSearchPush);
        LOG.info("processing done for change of seller fm mapping, returning {}", bResult);
        return bResult;
    }

    // case when we forcefully push the sellerFM to serach from RetriggerSerachEvent Cocofs admin Panel
    @Override
    public boolean processManualPushOfExistingSellerFMMappingForExternalNotification(String sellerCode, String existingFulfilmentModel) {
        if (!isSOIFilterFeatureEnabled()) {
            // returning true as this is logically correct termination of the process flow
            return true;
        }
        LOG.info("processing started for pushing existing seller fm mapping from retriggerSearchEvent Panel");

        // find the corresponding FM type
        FulfillmentModel currentFMType = FulfillmentModel.getFulfilmentModelByCode(existingFulfilmentModel);

        List<SellerSupcUpdateSRO> sellerSUPCFMMappingForSearchPush = new ArrayList<SellerSupcUpdateSRO>();
        // find all SUPC for the seller and create search push for all SUPC
        // except in exception list
        prepareSupcSellerListToBePushed(sellerCode, currentFMType, sellerSUPCFMMappingForSearchPush);

        boolean bResult = pushToExternalNotificationService(sellerSUPCFMMappingForSearchPush);
        LOG.info("processing done for pushing existing seller fm mapping from retriggerSearchEvent Panel, returning {}", bResult);
        return bResult;
    }

    @Override
    public boolean processAdditionOfNewSellerSUPCMappingForExternalNotification(SellerSupcAddEventObj event) throws GetFailedException, ExternalDataNotFoundException,
            PutFailedException, GenericPersisterException {

        String sellerCode = event.getSellerCode();
        String supc = event.getSupc();

        if (!isSOIFilterFeatureEnabled()) {
            // returning true as this is logically correct termination of the process flow
            return true;
        }
        boolean bResult = true;
        LOG.info("processing started for addition of new seller SUPC mapping, seller:{}, supc:{}", sellerCode, supc);
        try {
            FulfillmentModel fm = fulfilmentModelService.getFulfilmentModel(sellerCode, supc);
            //push only if SDfulfilled is the FM
            if (isFMFC_VOI(fm)) {
                bResult = pushNotification(sellerCode, supc, fm);
            }
        } catch (ExternalDataNotFoundException e) {
            LOG.error("Could not fetch FM Model for seller:{} supc:{} ", sellerCode, supc);
            throw e;
        }

        LOG.info("processing done for addition of seller-SUPC pair, returning {}", bResult);
        return bResult;
    }

    // case when SELLER-SUPC-FM mapping is changed in exception list from CoCoFS admin
    @Override
    public boolean processChangeOfSellerSUPCFMMappingExceptionForExternalNotification(SellerSupcFMFCExceptionAddEventObj event) {

        if (!isSOIFilterFeatureEnabled()) {
            // returning true as this is logically correct termination of the process flow
            return true;
        }
        LOG.info("processing started for change of seller-SUPC-FM exception mapping, event:{}", event);

        // find the corresponding FM type
        FulfillmentModel newFMType = FulfillmentModel.getFulfilmentModelByCode(event.getNewFM());
        FulfillmentModel oldFMType = FulfillmentModel.getFulfilmentModelByCode(event.getOldFM());
        // if any update requires push-to-search, collect it for
        // sending to push.

        // case when new supc is added first time in exception list 
        if (oldFMType == null) {
            try {
                String defaultFMStr = fulfilmentModelService.getFulfilmentModelBySeller(event.getSellerCode(), false);
                oldFMType = FulfillmentModel.getFulfilmentModelByCode(defaultFMStr);
            } catch (ExternalDataNotFoundException e) {
                LOG.error("Could not fetch default FM Model from seller-fm-mapping for event:{} Exception:{} ", event, e);
            }
        }
        boolean bResult = true;
        // either toggle in FC_VOI state between existing and new
        // or oldFM=null(no exception mapping - fetch from seller-fm) and new=FC_VOI
        if (isFC_VOIFMToggled(newFMType, oldFMType)) {
            // add this record to later push-to-search.
            bResult = pushNotification(event.getSellerCode(), event.getSupc(), newFMType);
        }

        LOG.info("processing done for change of seller-SUPC-FM exception mapping, returning {}", bResult);
        return bResult;
    }

    // case when we forcefully push the sellerSupcFM to search from RetriggerSerachEvent Cocofs admin Panel
    @Override
    public boolean processExistingSellerSUPCFMMappingExceptionForExternalNotification(String sellerCode, String supc, String existingFM) {

        if (!isSOIFilterFeatureEnabled()) {
            // returning true as this is logically correct termination of the process flow
            return true;
        }

        LOG.info("processing started for pushing existing  seller-SUPC-FM exception mapping, seller:{}, supc:{}", sellerCode, supc);

        // find the corresponding FM type
        FulfillmentModel currentFMType = FulfillmentModel.getFulfilmentModelByCode(existingFM);
        // sending to push.
        boolean bResult = pushNotification(sellerCode, supc, currentFMType);
        LOG.info("processing done for pushing existing   seller-SUPC-FM exception mapping, returning {}", bResult);
        return bResult;
    }

    // case when SELLER-SUPC-FM mapping is disabled in exception list from admin
    @Override
    public boolean processDisablingOfSellerSUPCFMMappingExceptionForExternalNotification(SellerSupcFMFCExceptionDeleteEventObj event) {

        if (!isSOIFilterFeatureEnabled()) {
            return true;
        }
        LOG.info("processing started for disabling of seller-SUPC-FM exception mapping, event: {}", event);

        boolean bResult = true;
        FulfillmentModel fmType = FulfillmentModel.getFulfilmentModelByCode(event.getFM());
        try {
            String defaultFMStr = fulfilmentModelService.getFulfilmentModelBySeller(event.getSellerCode(), false);
            FulfillmentModel defaultFMType = FulfillmentModel.getFulfilmentModelByCode(defaultFMStr);
            // case when FC_VOI is toggled in exception list vs default
            if (isFC_VOIFMToggled(fmType, defaultFMType)) {
                bResult = pushNotification(event.getSellerCode(), event.getSupc(), defaultFMType);
            }
        } catch (ExternalDataNotFoundException e) {
            bResult = false;
            LOG.error("Could not fetch FM Model from seller-fm-mapping for event:{} Exception:{} ", event, e);
        }

        LOG.info("processing done for disabling of seller-SUPC-FM exception mapping, returning {}", bResult);
        return bResult;
    }

    private void prepareSupcSellerListToBePushed(String sellerCode, FulfillmentModel newFMType, List<SellerSupcUpdateSRO> sellerSUPCFMMappingForSearchPush) {
        // find all SUPC for the seller and create search push for all SUPC
        // except in exception list
        try {
            // ask from aerospike
            List<String> listOfSUPCsFromDataSource = sellerToSUPCListLookupService.getSupcListForSeller(sellerCode);
            if (listOfSUPCsFromDataSource == null) {
                listOfSUPCsFromDataSource = new ArrayList<String>();
            }
            LOG.info("supc-list-size {} for seller:{} ", listOfSUPCsFromDataSource.size(), sellerCode);

            // for each item in the SELLER-List<SUPC> mapping, update to
            // external push if no exception exists for SELLER-SUPC
            // combination

            List<String> listOfSUPCExceptionsFromDataSource = sellerToSUPCListLookupService.getSupcExceptionListForSeller(sellerCode);
            if (listOfSUPCExceptionsFromDataSource == null) {
                listOfSUPCExceptionsFromDataSource = new ArrayList<String>();
            }

            LOG.info("supc-exception-list-size {} for seller:{} ", listOfSUPCExceptionsFromDataSource.size(), sellerCode);

            // create a set for faster lookup
            Set<String> supcExceptionSet = new HashSet<String>(listOfSUPCExceptionsFromDataSource);
            LOG.info("unique supc-exception-set-size {} for seller:{} ", supcExceptionSet.size(), sellerCode);

            //compare the items in list with exceptions
            for (String supc : listOfSUPCsFromDataSource) {
                //if SUPC does not exist in Exception list, push to search
                if (!supcExceptionSet.contains(supc)) {
                    //collect all such events
                    addToNotificationList(sellerCode, supc, newFMType, sellerSUPCFMMappingForSearchPush);
                }
            }

        } catch (ExternalDataNotFoundException e) {
            LOG.error("Could not fetch seller-supc-mapping for seller:{} Exception:{} ", sellerCode, e);
        }
    }

    private boolean pushNotification(String sellerCode, String supc, FulfillmentModel fm) {
        List<SellerSupcUpdateSRO> sellerSUPCFMMappingForSearchPush = new ArrayList<SellerSupcUpdateSRO>();
        addToNotificationList(sellerCode, supc, fm, sellerSUPCFMMappingForSearchPush);
        return pushToExternalNotificationService(sellerSUPCFMMappingForSearchPush);
    }

    private void addToNotificationList(String sellerCode, String supc, FulfillmentModel fm, List<SellerSupcUpdateSRO> sellerSUPCFMMappingForSearchPush) {
        // if any update requires push-to-search, collect it for
        // sending to push.
        // add this record to later push-to-search.
        SellerSupcUpdateSRO obj = createSellerSUPCUpdateSRO(sellerCode, supc, isFMFC_VOI(fm));
        LOG.info("adding SRO:{} to external notification list", obj);
        sellerSUPCFMMappingForSearchPush.add(obj);
    }

    private SellerSupcUpdateSRO createSellerSUPCUpdateSRO(String sellerCode, String supc, boolean sdFulfilled) {
        SellerSupcUpdateSRO obj = new SellerSupcUpdateSRO();
        obj.setSellerCode(sellerCode);
        obj.setSupc(supc);
        obj.setSdFulfilled(sdFulfilled);
        return obj;
    }

    /**
     * push the data to external notification service currently backed by activemq, data pushed is in form:
     * seller-supc-sdfulfilled. The current intended recipient is Search module, which may change in future.
     * 
     * @param sellerSUPCFMMappingForSearchPush
     */
    private boolean pushToExternalNotificationService(List<SellerSupcUpdateSRO> sellerSUPCFMMappingForSearchPush) {
        // all seller/supc processed, push to search if anything
        if (!sellerSUPCFMMappingForSearchPush.isEmpty()) {
            // forward the data for push-to-search
            return sellerSupcSDFulfilledProducer.publishToSearch(sellerSUPCFMMappingForSearchPush);
        }
        // return true in case of no events
        return true;
    }

    private boolean isFC_VOIFMToggled(FulfillmentModel newFMType, FulfillmentModel oldFMType) {
        LOG.debug("oldFMType:{},newFMType:{}", oldFMType, newFMType);
        return oldFMType != newFMType && (isFMFC_VOI(newFMType) || isFMFC_VOI(oldFMType));
    }

    private boolean isNewFMFC_VOIAndExistingFMWasNone(FulfillmentModel newFMType, FulfillmentModel oldFMType) {
        return oldFMType == null && isFMFC_VOI(newFMType);
    }

    private boolean isFMFC_VOI(FulfillmentModel newFMType) {
        return newFMType == FulfillmentModel.FC_VOI;
    }

    private boolean isSOIFilterFeatureEnabled() {
        if (!dep.getBooleanPropertyValue(Property.SOI_FILTER_FEATURE_ENABLED)) {
            LOG.warn("SOI filter feature is disabled. Returning without any further processing ... ");
            return false;
        }
        return true;
    }

    public interface IDependency {
        boolean getBooleanPropertyValue(Property property);
    }

    static class Dependency implements IDependency {
        @Override
        public boolean getBooleanPropertyValue(Property property) {
            return ConfigUtils.getBooleanScalar(property);
        }
    }
}
