/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.services.taxrate.dto;

import javax.xml.bind.annotation.XmlRootElement;

import com.snapdeal.cocofs.utils.SubClass;

/**
 * @version 1.0, 13-Aug-2014
 * @author ankur
 */

@XmlRootElement
@SubClass
public class SellerSUPCTaxRateUploadDTO extends TaxRateUpdateDTO {

    private String supc;

    private String sellerCode;

    public SellerSUPCTaxRateUploadDTO() {
    }

    public String getSupc() {
        return supc;
    }

    public void setSupc(String supc) {
        this.supc = supc;
    }

    public String getSellerCode() {
        return sellerCode;
    }

    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((sellerCode == null) ? 0 : sellerCode.hashCode());
        result = prime * result + ((supc == null) ? 0 : supc.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SellerSUPCTaxRateUploadDTO other = (SellerSUPCTaxRateUploadDTO) obj;
        if (sellerCode == null) {
            if (other.sellerCode != null)
                return false;
        } else if (!sellerCode.equals(other.sellerCode))
            return false;
        if (supc == null) {
            if (other.supc != null)
                return false;
        } else if (!supc.equals(other.supc))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "SellerSUPCTaxRateUploadDTO [supc=" + supc + ", sellerCode=" + sellerCode + ", toString()=" + super.toString() + "]";
    }

}
