/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 28-Aug-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.services.task.update;

import java.util.List;

import com.snapdeal.cocofs.entity.TaskDetail;
import com.snapdeal.cocofs.entity.TaskParam;
import com.snapdeal.cocofs.services.task.dto.TaskDetailDTO;
import com.snapdeal.cocofs.services.task.dto.TaskParamDTO;
import com.snapdeal.task.core.TaskRequest;

public interface ITaskUpdateConverterService {

    public TaskDetail getTaskDetail(TaskDetailDTO taskDetailDTO, String updatedBy);

    public TaskDetailDTO getTaskDetailDTO(TaskDetail taskDetail);

    public TaskParamDTO getTaskParamDTO(TaskParam taskParam);

    public TaskRequest getTaskRequest(TaskDetail taskDetail);

    List<TaskParamDTO> getTaskParamDTOList(List<TaskParam> taskParams);

    List<TaskParam> getTaskParams(List<TaskParamDTO> taskParamsDTO, String taskName, String updatedBy);

}
