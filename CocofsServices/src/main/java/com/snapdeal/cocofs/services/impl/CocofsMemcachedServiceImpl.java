package com.snapdeal.cocofs.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import net.spy.memcached.AddrUtil;
import net.spy.memcached.CASResponse;
import net.spy.memcached.CASValue;
import net.spy.memcached.MemcachedClient;
import net.spy.memcached.internal.OperationFuture;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.snapdeal.base.cache.Cachable;
import com.snapdeal.base.memcached.service.impl.MemcachedServiceImpl;
import com.snapdeal.cocofs.memcache.ICocofsMemcachedService;

@Service("cocofsMemcachedService")
public class CocofsMemcachedServiceImpl implements ICocofsMemcachedService, DisposableBean {

    private static final Logger   LOG             = LoggerFactory.getLogger(MemcachedServiceImpl.class);

    private static final int      NO_OF_INSTANCES = 30;

    private List<MemcachedClient> memcachedClients;

    private String                serverList;

    @Override
    public void initialize(String memcacheServerList) throws Exception {
        if (memcacheServerList.equals(serverList)) {
            LOG.info("Memcache service already initialized, returning");
            return;

        }

        serverList = memcacheServerList;

        LOG.info("Initializing Memcached Service, servers : {} ...", memcacheServerList);
        if (memcachedClients != null) {
            for (int i = 0; i < memcachedClients.size(); i++) {
                memcachedClients.get(i).shutdown();
            }
            memcachedClients.clear();
        } else {
            memcachedClients = new ArrayList<MemcachedClient>(NO_OF_INSTANCES);
        }
        for (int i = 0; i < NO_OF_INSTANCES; i++) {
            MemcachedClient mc = new MemcachedClient(AddrUtil.getAddresses(memcacheServerList));
            memcachedClients.add(mc);
        }
        LOG.info("Memcached Service Initialized... SUCCESSFULLY...");

    }

    @Override
    public void shutdownMemcacheClient() {
        for (int i = 0; i < memcachedClients.size(); i++) {
            memcachedClients.get(i).shutdown();
        }
        memcachedClients.clear();
    }

    @Override
    public void destroy() throws Exception {
        shutdownMemcacheClient();

    }

    @Override
    protected void finalize() throws Throwable {
        shutdownMemcacheClient();
    }

    private String makeupKey(String key) {
        return StringUtils.trimAllWhitespace(key);
    }

    private MemcachedClient getRandomClient() {
        int random = (int) (Math.random() * NO_OF_INSTANCES);
        return memcachedClients.get(random);
    }

    @Override
    public Integer getInteger(String key) {
        Object value = get(key);
        if (value != null) {
            try {
                return Integer.parseInt(String.valueOf(value));
            } catch (NumberFormatException e) {
                return null;
            }
        } else {
            return null;
        }
    }

    @Override
    public void putInteger(String key, Integer value, int secondToLive) {
        put(key, String.valueOf(value), secondToLive);

    }

    @Override
    public boolean keyExists(String key) {
        return (getRandomClient().get(key) != null);
    }

    @Override
    public Object get(String key) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("get:: key: {}", key);
        }
        if (key != null) {
            try {
                return getRandomClient().get(makeupKey(key));
            } catch (Throwable t) {
                LOG.error("Error while retrieving the key from memcache", t);
                return null;
            }
        }
        return null;
    }
    
    /**
     * Gets a value of an element which matches the given key.
     * 
     * @param key the key of the element to return.
     * @return The value placed into the cache with an earlier put, or null if not found or expired
     */
    @Override
    @SuppressWarnings("unchecked")
    public <T extends Cachable> T get(String key, Class<T> cachable) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("get:: key: {}", key);
        }
        if (key != null) {
            String str = (String) getRandomClient().get(makeupKey(key));
            if (str != null) {
                try {
                    return (T) cachable.newInstance().loadFromMemString(str);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    
    @Override
    public void put(String key, Cachable cachable, int secondToLive) {
        //String json = new Gson().toJson(cachable);
        //LOG.debug("put:: key: {} value: {}", key, json);
        getRandomClient().set(makeupKey(key), secondToLive, cachable.toMemString());
    }

    @Override
    public void put(String key, Object value, int secondToLive) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("put:: key: {} value: {}", makeupKey(key), value);
        }
        getRandomClient().set(makeupKey(key), secondToLive, value);
    }

    @Override
    public void remove(String key) {
        getRandomClient().delete(makeupKey(key));
    }

    @SuppressWarnings("unchecked")
    @Override
    public CASValue<Boolean> getS(String key) {

        Object response = getRandomClient().gets(key);
        return (CASValue<Boolean>) response;
    }

    @Override
    public CASResponse compareAndSet(String key, Boolean value, long casId) {
        return getRandomClient().cas(key, casId, value);
    }

    @Override
    public Boolean add(String key, Boolean value) {
        OperationFuture<Boolean> future = getRandomClient().add(key, 5000, value);
        try {
            return future.get(5, TimeUnit.SECONDS);
        } catch (Exception e) {
            future.cancel(false);
            return false;
        }
    }

    @Override
    public Boolean delete(String key) {
        OperationFuture<Boolean> future = getRandomClient().delete(key);

        try {
            return future.get(5, TimeUnit.SECONDS);
        } catch (Exception e) {
            future.cancel(false);
            return false;
        }
    }

}
