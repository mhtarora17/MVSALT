package com.snapdeal.cocofs.services.fm.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.catalog.base.model.GetMinProductInfoBySupcRequest;
import com.snapdeal.catalog.base.model.GetMinProductInfoResponse;
import com.snapdeal.cocofs.dataaccess.fm.IFMDBDataReadService;
import com.snapdeal.cocofs.entity.SellerFCCodeMapping;
import com.snapdeal.cocofs.entity.SellerFMMapping;
import com.snapdeal.cocofs.entity.SellerSubcatFMMapping;
import com.snapdeal.cocofs.entity.SellerSupcFMMapping;
import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.external.service.factory.IExternalServiceFactory;
import com.snapdeal.cocofs.services.fm.IFMDataSourceService;
import com.snapdeal.cocofs.services.fm.dto.SellerFmFcDTO;
import com.snapdeal.cocofs.services.fm.dto.SellerSupcFmFcDTO;

@Service("FMDataSourceServiceForMySql")
public class FMDataSourceServiceForMySql implements IFMDataSourceService{

	@Autowired
	private IFMDBDataReadService		fmDBDataReadService;

	@Autowired
	private IExternalServiceFactory		externalServiceFactory;

	private static final Logger			LOG = LoggerFactory.getLogger(FMDataSourceServiceForMySql.class);

	/**
	 * Returns the fulfilment model by sellercode from MySql
	 * 
	 * @param sellerCode
	 * @return fulfilmentModel
	 * @throws ExternalDataNotFoundException
	 */
	@Override
	public String getFulfilmentModelBySeller(String sellerCode)	throws ExternalDataNotFoundException {
		String fulfilmentModel = null;
		//Get SellerFM mapping for sellerCode
		SellerFMMapping sellerFMMapping = getSellerFMMapping(sellerCode);
		if(sellerFMMapping != null && sellerFMMapping.isEnabled()) {
			fulfilmentModel = sellerFMMapping.getFulfilmentModel();
		}
		LOG.debug("Returning fm from seller mapping {} for seller {} ", fulfilmentModel, sellerCode);
		return fulfilmentModel;
	}
	
	/**
     * Returns the fc centers by sellercode from aerospike
     * 
     * @param sellerCode
     * @return fulfilmentModel
     * @throws ExternalDataNotFoundException
     */
    @Override
    public List<String> getFCBySeller(String sellerCode) throws ExternalDataNotFoundException {
        List<String> centers = null;
        //Get SellerFM mapping for sellerCode
        SellerFMMapping sellerFMMapping = getSellerFMMapping(sellerCode);
        if (sellerFMMapping != null && sellerFMMapping.getFcCenters()!= null) {
            centers = new ArrayList<String>();
            for(SellerFCCodeMapping fcmapping: sellerFMMapping.getFcCenters()){
              if(fcmapping.isEnabled()){
                    centers.add(fcmapping.getFcCode());
              }
            }
            
        }
        LOG.debug("Returning centers from seller mapping {} for seller {} ", centers, sellerCode);
        return centers;
    }

	/**
	 * Returns the fulfilment model by sellercode and supc from MySql
	 * 
	 * @param sellerCode
	 * @param supc
	 * @return fulfilmentModel
	 * @throws ExternalDataNotFoundException
	 */
	@Override
	public String getFulfilmentModelBySellerAndSupc(String sellerCode, String supc) throws ExternalDataNotFoundException{
		String fulfilmentModel = null;

		//Get SellerSupcFM mapping for sellerCode
		SellerSupcFMMapping sellerSupcFMMapping = getSellerSupcFMMapping(sellerCode, supc);
		if (null != sellerSupcFMMapping && sellerSupcFMMapping.isEnabled()) {
			fulfilmentModel = sellerSupcFMMapping.getFulfilmentModel();
		}
		LOG.debug("Returning fm from seller-supc mapping {} for seller {}  supc {} ", fulfilmentModel, sellerCode, supc);
		return fulfilmentModel;
	}

	/**
	 * Returns the fulfilment model by sellercode and subcat from MySql
	 * 
	 * @param sellerCode
	 * @param supc
	 * @param subcat
	 * @return fulfilmentModel
	 * @throws ExternalDataNotFoundException
	 */
	@Override
	public String getFulfilmentModelBySellerAndSubcat(String sellerCode, String supc, String subcat) throws ExternalDataNotFoundException{
		GetMinProductInfoResponse resp = null;
		String fulfilmentModel = null;
		if (StringUtils.isEmpty(subcat)) {
			try {
				resp = externalServiceFactory.getCAMSExternalService().getMinProductInfo(new GetMinProductInfoBySupcRequest(supc));
			} catch (ExternalDataNotFoundException e) {
				LOG.error("No subcat found for supc {}", e, supc);
				throw e;
			}
			if (null != resp && resp.isSuccessful() && resp.getProductInfo() != null) {
				subcat = resp.getProductInfo().getCategoryUrl();
			}
		}

		//Get SellerSubcatFM mapping for sellerCode
		SellerSubcatFMMapping sellerSubcatFMMapping = getSellerSubcatFMMapping(sellerCode, subcat);
		if (null != sellerSubcatFMMapping && sellerSubcatFMMapping.isEnabled()) {
			fulfilmentModel = sellerSubcatFMMapping.getFulfilmentModel();
		}
		LOG.info("Returning fm from seller-subcat mapping {} for seller {}  subcat {} ", fulfilmentModel, sellerCode, subcat);
		return fulfilmentModel;
	}

	/**
	 * Returns True if seller exists in MySql otherwise return False.
	 * 
	 * @param sellerCode
	 * @return isSellerExist
	 * @throws ExternalDataNotFoundException
	 */
	@Override
	public boolean isSellerExists(String sellerCode) throws ExternalDataNotFoundException {
		boolean bResult = false;
		//Get SellerFM mapping for sellerCode
		SellerFMMapping sellerFMMapping = getSellerFMMapping(sellerCode);
		//DB allow case insensitive search, hence need to check case again.
		if (null != sellerFMMapping && sellerFMMapping.isEnabled() && sellerFMMapping.getSellerCode().equals(sellerCode)) {
			bResult = true;
		}
		return bResult;
	}

	/**
	 * Returns the SellerFM Mapping from MySql.
	 * 
	 * @param sellerCode
	 * @return sellerFMMappingDTO
	 * @throws ExternalDataNotFoundException
	 */
	@Override
	public SellerFmFcDTO getSellerFmFcDTO(String sellerCode)	throws ExternalDataNotFoundException {
		//Get SellerFM mapping for sellerCode
		SellerFMMapping sellerFMMapping = getSellerFMMapping(sellerCode);
		if(sellerFMMapping != null){
			return new SellerFmFcDTO(sellerFMMapping);
		}
		return null;
	}
	
	/**
	 * Returns the SellerSupcFmFcDTO  from MySql.
	 * @param sellerCode
	 * @param supc
	 * @return
	 * @throws ExternalDataNotFoundException
	 */
	@Override
	public SellerSupcFmFcDTO getSellerSupcFmFcDTO(String sellerCode,String supc)	throws ExternalDataNotFoundException {
		//Get SellerFM mapping for sellerCode
		SellerSupcFMMapping sellerSupcFMMapping = getSellerSupcFMMapping(sellerCode, supc);
		if(sellerSupcFMMapping != null){
			return new SellerSupcFmFcDTO(sellerSupcFMMapping);
		}
		return null;
	}

	/////////////////////////////////
	//////// Private Methods ////////
	/////////////////////////////////

	private SellerFMMapping getSellerFMMapping(String sellerCode) throws ExternalDataNotFoundException{
		if (StringUtils.isEmpty(sellerCode)) {
			LOG.warn("Illegal paramters passed while trying to find fm at seller level");
			throw new ExternalDataNotFoundException("Required Parameters not there while getting FM At Seller Level");
		}

		LOG.debug("Going to read fulfilment model from MySql for sellercode {}", sellerCode);
		SellerFMMapping sellerFMMapping = (SellerFMMapping) fmDBDataReadService.getSellerFMMapping(sellerCode);
		LOG.debug("Fulfilment Model obtained from MySql:" + sellerFMMapping);
		return sellerFMMapping;
	}

	private SellerSupcFMMapping getSellerSupcFMMapping(String sellerCode, String supc) throws ExternalDataNotFoundException{
		if (StringUtils.isEmpty(sellerCode) || StringUtils.isEmpty(supc)) {
			LOG.warn("Illegal paramters passed while trying to find fm at seller and supc level");
			throw new ExternalDataNotFoundException("Required Parameters not there while getting FM At Seller Level");
		}

		LOG.debug("Going to read fulfilment model from MySql for sellerCode {} and supc {} ", sellerCode, supc);
		SellerSupcFMMapping sellerSupcFMMapping = (SellerSupcFMMapping) fmDBDataReadService.getSellerSupcFMMapping(sellerCode, supc);
		LOG.debug("Fulfilment Model obtained from MySql:" + sellerSupcFMMapping);
		return sellerSupcFMMapping;

	}

	private SellerSubcatFMMapping getSellerSubcatFMMapping(String sellerCode,String subcat) throws ExternalDataNotFoundException{
		if (StringUtils.isEmpty(subcat) || StringUtils.isEmpty(sellerCode)) {
			LOG.warn("Illegal paramters passed while trying to find fm at seller and subcat level");
			throw new ExternalDataNotFoundException("Required Parameters not there while getting FM At Seller and Subcat Level");
		}

		LOG.debug("Going to read fulfilment model from MySql for sellerCode {} and subcat {}", sellerCode, subcat);
		SellerSubcatFMMapping sellerSubcatFMMapping = (SellerSubcatFMMapping) fmDBDataReadService.getSellerSubcatFMMapping(sellerCode, subcat);
		LOG.debug("Fulfilment Model obtained from MySql:" + sellerSubcatFMMapping);
		return sellerSubcatFMMapping;
	}
}
