/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-Oct-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.services.fm;

import java.util.List;

import com.snapdeal.cocofs.core.aerospike.vo.SellerSubcatFMMappingVO;
import com.snapdeal.cocofs.core.aerospike.vo.SellerSupcFMMappingVO;
import com.snapdeal.cocofs.entity.SellerSubcatFMMapping;
import com.snapdeal.cocofs.entity.SellerSupcFMMapping;
import com.snapdeal.cocofs.generic.persister.exception.GenericPersisterException;
import com.snapdeal.cocofs.generic.persister.response.GenericPersisterWithAerospikeResponse;

public interface IFMMappingDataUpdater {

    GenericPersisterWithAerospikeResponse<SellerSupcFMMapping, SellerSupcFMMappingVO> updateSellerSupcFMMapping(List<SellerSupcFMMapping> entityListToSave,
            List<SellerSupcFMMappingVO> recordListToSave) throws GenericPersisterException;

    GenericPersisterWithAerospikeResponse<SellerSubcatFMMapping, SellerSubcatFMMappingVO> updateSellerSubcatFMMapping(List<SellerSubcatFMMapping> toBeUpdatedEntityList,
            List<SellerSubcatFMMappingVO> recordListToSave) throws GenericPersisterException;

   
}
