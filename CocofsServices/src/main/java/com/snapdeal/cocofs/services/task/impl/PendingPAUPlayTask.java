package com.snapdeal.cocofs.services.task.impl;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.cocofs.services.IPendingPAUPlayService;

/**
 * @author nikhil
 */

public class PendingPAUPlayTask extends QuartzTask {
    
    @Autowired
    private IPendingPAUPlayService pendingPAUPlayService;

    private static final Logger    LOG = LoggerFactory.getLogger(PendingPAUPlayTask.class);

    @Override
    public void execute(String taskName) {
        LOG.info("Starting PendingPAUPlayTask task..");
        Date startDate = DateUtils.getStartOfDay(DateUtils.getCurrentDate());
        Date endDate = DateUtils.getEndOfDay(DateUtils.getCurrentDate());
        pendingPAUPlayService.promotePendingPAUs(startDate, endDate);
        LOG.info("Finished PendingPAUPlayTask task..");
    }

}
