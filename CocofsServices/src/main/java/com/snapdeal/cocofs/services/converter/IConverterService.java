package com.snapdeal.cocofs.services.converter;

import java.util.List;

import com.snapdeal.cocofs.core.aerospike.vo.FCDetailVO;
import com.snapdeal.cocofs.core.aerospike.vo.SellerDetailVO;
import com.snapdeal.cocofs.core.aerospike.vo.SellerZoneMappingVO;
import com.snapdeal.cocofs.entity.EventInstance;
import com.snapdeal.cocofs.entity.FulfillmentCentre;
import com.snapdeal.cocofs.entity.PendingProductAttributeUpdate;
import com.snapdeal.cocofs.entity.SellerCategoryTaxRate;
import com.snapdeal.cocofs.entity.SellerContactDetails;
import com.snapdeal.cocofs.entity.SellerSupcTaxRate;
import com.snapdeal.cocofs.entity.SellerZoneMapping;
import com.snapdeal.cocofs.entity.StateCategoryPriceTaxRate;
import com.snapdeal.cocofs.entity.SubcatTaxClassMapping;
import com.snapdeal.cocofs.entity.SupcTaxClassMapping;
import com.snapdeal.cocofs.excelupload.ProductAttributeUploadDTO;
import com.snapdeal.cocofs.mongo.model.ProductAttributeUnit;
import com.snapdeal.cocofs.services.common.dto.ProductAttributeDownloadData;
import com.snapdeal.cocofs.services.events.dto.EventDetailDTO;
import com.snapdeal.cocofs.services.fm.dto.AddressDTO;
import com.snapdeal.cocofs.services.taxclass.dto.SubcatTaxClassMappingDTO;
import com.snapdeal.cocofs.services.taxclass.dto.SupcTaxClassMappingDTO;
import com.snapdeal.cocofs.services.taxrate.dto.SellerCategoryTaxRateUploadDTO;
import com.snapdeal.cocofs.services.taxrate.dto.SellerSUPCTaxRateUploadDTO;
import com.snapdeal.cocofs.services.taxrate.dto.StateCategoryPriceTaxRateUploadDTO;
import com.snapdeal.cocofs.sro.AddressSRO;
import com.snapdeal.cocofs.sro.SellerContactDetailSRO;
import com.snapdeal.score.model.request.UpdateProductFulfilmentAttributeRequest;

public interface IConverterService {

    ProductAttributeDownloadData getProductAttributeDownloadData(List<PendingProductAttributeUpdate> pendingUpdates, ProductAttributeUnit unit);

    StateCategoryPriceTaxRateUploadDTO getStateCategoryPriceTaxRateUploadDTO(StateCategoryPriceTaxRate entity);

    SellerCategoryTaxRateUploadDTO getSellerCategoryTaxRateUploadDTO(SellerCategoryTaxRate entity);

    SellerSUPCTaxRateUploadDTO getSellerSUPCTaxRateUploadDTO(SellerSupcTaxRate entity);

    EventDetailDTO getEventDetailDTO(EventInstance instance);

    public ProductAttributeUploadDTO getProductAttributeUploadDTO(ProductAttributeUnit pau);

    UpdateProductFulfilmentAttributeRequest getUpdateProductAttributesSRORequest(ProductAttributeUnit pau);

    List<String> convertToFCCentreList(String fcCenterString);

    String convertFCCentreListToString(List<String> fcCenterList);
    
    SellerContactDetails getSellerContactDetailsFromSRO(SellerContactDetailSRO sro, String sellerCode, String updatedBy);
    
    SellerZoneMapping getSellerZoneMappingFromDetails(SellerContactDetails scd, String zone);
    
    SellerZoneMappingVO getSellerZoneMappingVoFromEntity(SellerZoneMapping szm);

    SellerDetailVO getSellerDetailVoFromEntity(SellerContactDetails scd);

    AddressDTO getAddressDTO(SellerDetailVO vo);

    AddressDTO getAddressDTO(SellerContactDetails entity);

    AddressSRO getAddressSROfromDTO(AddressDTO dto);

    SupcTaxClassMappingDTO getSupcTaxClassMappingUploadDTO(SupcTaxClassMapping entity);

    SubcatTaxClassMappingDTO getSubcatTaxClassMappingUploadDTO(SubcatTaxClassMapping entity);

    FCDetailVO getFCDetailVoFromEntity(FulfillmentCentre fc);

}
