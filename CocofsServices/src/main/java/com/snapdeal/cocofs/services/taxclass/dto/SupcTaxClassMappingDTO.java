/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.services.taxclass.dto;

import javax.xml.bind.annotation.XmlRootElement;

import com.snapdeal.cocofs.core.aerospike.vo.SupcTaxClassMappingVO;
import com.snapdeal.cocofs.entity.SupcTaxClassMapping;

/**
 * @version 1.0, 23-Dec-2015
 * @author nitish
 */

@XmlRootElement
public class SupcTaxClassMappingDTO {

    private String  supc;

    private String  taxClass;

    private Boolean enabled;

    public SupcTaxClassMappingDTO() {
    }

    public SupcTaxClassMappingDTO(SupcTaxClassMapping entity) {
        super();
        this.supc = entity.getSupc();
        this.taxClass = entity.getTaxClass();
        this.enabled = entity.isEnabled();
    }

    public SupcTaxClassMappingDTO(SupcTaxClassMappingVO supcTaxClassMappingVO) {
        super();
        this.supc = supcTaxClassMappingVO.getSupc();
        this.taxClass = supcTaxClassMappingVO.getTaxClass();
        this.enabled = supcTaxClassMappingVO.getEnabled() == 1 ? true : false;
    }

    public String getSupc() {
        return supc;
    }

    public void setSupc(String supc) {
        this.supc = supc;
    }

    public String getTaxClass() {
        return taxClass;
    }

    public void setTaxClass(String taxClass) {
        this.taxClass = taxClass;
    }

    public Boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public String toString() {
        return "SupcTaxClassMappingDTO [supc=" + supc + ", taxClass=" + taxClass + ", enabled=" + enabled + "]";
    }

}
