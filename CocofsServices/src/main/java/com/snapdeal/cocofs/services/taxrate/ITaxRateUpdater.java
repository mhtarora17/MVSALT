package com.snapdeal.cocofs.services.taxrate;

import java.util.List;

import com.snapdeal.cocofs.entity.StateCategoryPriceTaxRate;
import com.snapdeal.cocofs.generic.persister.exception.GenericPersisterException;
import com.snapdeal.cocofs.generic.persister.response.GenericPersisterResponse;
import com.snapdeal.cocofs.mongo.model.StateCategoryPriceTaxRateUnit;


public interface ITaxRateUpdater {

    GenericPersisterResponse<StateCategoryPriceTaxRate, StateCategoryPriceTaxRateUnit> updateStateCategoryPriceTaxRate(List<StateCategoryPriceTaxRate> entityListToSave,
            List<StateCategoryPriceTaxRateUnit> docListToSave) throws GenericPersisterException;

   
}
