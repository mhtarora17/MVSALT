/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 09-Sep-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.services.task.update.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.entity.TaskDetailUpdate;
import com.snapdeal.cocofs.enums.SystemProperty;
import com.snapdeal.cocofs.enums.TaskDetailAction;
import com.snapdeal.cocofs.services.task.update.ITaskUpdateSchedulerService;
import com.snapdeal.cocofs.services.task.update.ITaskUpdateService;
import com.snapdeal.cocofs.task.db.dataaccess.ITaskDBDataReadService;
import com.snapdeal.cocofs.task.db.dataaccess.ITaskDBDataWriteService;
import com.snapdeal.task.exception.TaskException;

@Service("taskUpdateSchedulerService")
public class TaskUpdateSchedulerServiceImpl implements ITaskUpdateSchedulerService {

    private static final Logger     LOG      = LoggerFactory.getLogger(TaskUpdateSchedulerServiceImpl.class);

    private static final String     hostName = System.getProperty(SystemProperty.HOST_NAME.getCode());

    @Autowired
    private ITaskUpdateService      taskUpdateService;

    @Autowired
    private ITaskDBDataReadService  taskDBDataReadService;

    @Autowired
    private ITaskDBDataWriteService taskDBDataWriteService;

    @Override
    public void execute() {
        List<TaskDetailUpdate> taskDetailUpdateList = taskDBDataReadService.getAllTaskDetailUpdateByHostName(hostName);
        if (null != taskDetailUpdateList && !taskDetailUpdateList.isEmpty()) {
            for (TaskDetailUpdate taskDetailUpdate : taskDetailUpdateList) {
                try {
                    LOG.info("Trying to execute the action {} on {} ", taskDetailUpdate.getCommand(), taskDetailUpdate.getTaskName());
                    doSpecificAction(taskDetailUpdate);
                    taskDetailUpdate.setExecuted(true);
                    taskDBDataWriteService.mergeTaskDetailUpdate(taskDetailUpdate);
                } catch (TaskException e) {
                    LOG.error("Error while handling specific actions {} ", e, taskDetailUpdateList);
                }
            }
        }

    }

    private void doSpecificAction(TaskDetailUpdate taskDetailUpdate) throws TaskException {
        String actionCode = taskDetailUpdate.getCommand();
        if (TaskDetailAction.RESUME_ALL.getCode().equals(actionCode)) {
            taskUpdateService.resumeAllTasks();
        } else if (TaskDetailAction.STOP_All.getCode().equals(actionCode)) {
            taskUpdateService.stopAllTasks();
        } else if (TaskDetailAction.STOP_TASK.getCode().equals(actionCode)) {
            taskUpdateService.stopTask(taskDetailUpdate.getTaskName());
        } else if (TaskDetailAction.START_TASK.getCode().equals(actionCode)) {
            taskUpdateService.startTask(taskDetailUpdate.getTaskName());
        } else if (TaskDetailAction.UPDATE_TASK_DETAIL.getCode().equals(actionCode)) {
            taskUpdateService.updateTask(taskDetailUpdate.getTaskName());
        } else if (TaskDetailAction.START_ALL.getCode().equals(actionCode)) {
            try {
                LOG.info("Sleeping for some time before executing startALl {}",ConfigUtils.getStringScalar(Property.GET_START_ALL_ACTION_SLEEP_TIME));
                Thread.sleep(Long.parseLong(ConfigUtils.getStringScalar(Property.GET_START_ALL_ACTION_SLEEP_TIME)));
            } catch (InterruptedException e) {
                LOG.error("InterruptedException in doSpecificAction", e);
            } catch (Exception e) {
                LOG.error("Exception in doSpecificAction", e);
            }
            taskUpdateService.startAllTasks();
        } else if (TaskDetailAction.PAUSE_ALL.getCode().equals(actionCode)) {
            taskUpdateService.pauseAllTasks();
        }

    }
}