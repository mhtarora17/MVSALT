/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.services.fm.dto;

import javax.xml.bind.annotation.XmlRootElement;

import com.snapdeal.cocofs.core.aerospike.vo.SellerFMMappingVO;
import com.snapdeal.cocofs.entity.SellerFMMapping;

/**
 *  
 *  @version     1.0, 10-Dec-2014
 *  @author ankur
 */

@XmlRootElement
public class SellerFMMappingDTO {

    private String sellerCode;

    private String fulfilmentModel;
    
    private boolean  supcExist;
    
    private boolean subcatExist;
    
    private boolean enabled;

    public SellerFMMappingDTO(){
        
    }
    
    public SellerFMMappingDTO(SellerFMMappingVO vo){
        this.sellerCode = vo.getSellerCode();
        this.fulfilmentModel = vo.getFulfillmentModel();
        this.supcExist = vo.isSupcExist();
        this.subcatExist = vo.isSubcatExist();
        this.enabled = vo.isEnabled();
    }
    
    public SellerFMMappingDTO(SellerFMMapping entity){
        this.sellerCode = entity.getSellerCode();
        this.fulfilmentModel = entity.getFulfilmentModel();
        this.supcExist = entity.isSupcExist();
        this.subcatExist = entity.isSubcatExist();
        this.enabled = entity.isEnabled();
    }
    
    public String getSellerCode() {
        return sellerCode;
    }

    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }

    public String getFulfilmentModel() {
        return fulfilmentModel;
    }

    public void setFulfilmentModel(String fulfilmentModel) {
        this.fulfilmentModel = fulfilmentModel;
    }

    public boolean isSupcExist() {
        return supcExist;
    }

    public void setSupcExist(boolean supcExist) {
        this.supcExist = supcExist;
    }

    public boolean isSubcatExist() {
        return subcatExist;
    }

    public void setSubcatExist(boolean subcatExist) {
        this.subcatExist = subcatExist;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public String toString() {
        return "SellerFMMappingDTO [sellerCode=" + sellerCode + ", fulfilmentModel=" + fulfilmentModel + ", supcExist=" + supcExist + ", subcatExist=" + subcatExist + ", enabled=" + enabled + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((fulfilmentModel == null) ? 0 : fulfilmentModel.hashCode());
        result = prime * result + ((sellerCode == null) ? 0 : sellerCode.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SellerFMMappingDTO other = (SellerFMMappingDTO) obj;
        if (fulfilmentModel == null) {
            if (other.fulfilmentModel != null)
                return false;
        } else if (!fulfilmentModel.equals(other.fulfilmentModel))
            return false;
        if (sellerCode == null) {
            if (other.sellerCode != null)
                return false;
        } else if (!sellerCode.equals(other.sellerCode))
            return false;
        if(enabled != other.enabled)
        	return false;
        return true;
    }
    
    
    
    
    
    
}
