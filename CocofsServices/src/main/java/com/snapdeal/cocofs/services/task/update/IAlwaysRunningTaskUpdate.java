/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 03-Sep-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.services.task.update;


public interface IAlwaysRunningTaskUpdate {

    public void stopTask(String taskName);

    public void startTask(String taskName);

}
