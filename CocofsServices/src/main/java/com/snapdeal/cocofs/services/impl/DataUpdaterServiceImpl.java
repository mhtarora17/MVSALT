/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 09-Sep-2013
 *  @author himanshum
 */
package com.snapdeal.cocofs.services.impl;


import java.util.List;

import javax.naming.OperationNotSupportedException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.access.IProductAttributeService;
import com.snapdeal.cocofs.generic.persister.exception.GenericPersisterException;
import com.snapdeal.cocofs.generic.persister.exception.GenericPersisterExceptionCode;
import com.snapdeal.cocofs.generic.persister.request.GenericPersisterRequest;
import com.snapdeal.cocofs.generic.persister.response.GenericPersisterResponse;
import com.snapdeal.cocofs.generic.persister.service.IGenericPersisterService;
import com.snapdeal.cocofs.generic.persister.validation.IPersisterPostValidationHandler;
import com.snapdeal.cocofs.generic.persister.validation.IPostValidationHandlerFactory;
import com.snapdeal.cocofs.services.IDataReader;
import com.snapdeal.cocofs.services.IDataUpdater;
import com.snapdeal.cocofs.services.IUploadDataModifier;
import com.snapdeal.cocofs.services.common.dto.UserInfo;
import com.snapdeal.cocofs.services.data.engine.IDataEngine;
import com.snapdeal.cocofs.services.factory.UPloadDataModifierServiceFactory;

@Service("dataUpdateService")
public class DataUpdaterServiceImpl implements IDataUpdater {

    private static final Logger              LOG = LoggerFactory.getLogger(DataUpdaterServiceImpl.class);

    @Autowired
    private IGenericPersisterService         persistenceService;

    @Autowired
    private IPostValidationHandlerFactory    postValidationHandlerFactory;

    @Autowired
    private IProductAttributeService         productAttrService;

    @Autowired
    private UPloadDataModifierServiceFactory dataModifierServiceFactory;

    @Override
    @SuppressWarnings("unchecked")
    public <T, E, D> GenericPersisterResponse<E, D> updateDataWithDTO(T dto, IDataReader<T, E, D> reader, IDataEngine<T, E, D> engine, UserInfo userInfo)
            throws GenericPersisterException, OperationNotSupportedException {
        return updateDataWithDTO(dto, reader, engine, userInfo, false, false);
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T, E, D> GenericPersisterResponse<E, D> updateDataWithDTO(T dto, IDataReader<T, E, D> reader, IDataEngine<T, E, D> engine, UserInfo user, boolean skipGenricValidation,
            boolean skipDataModifier) throws GenericPersisterException, OperationNotSupportedException {

        if (!skipDataModifier) {
            IUploadDataModifier<T> modifierService = dataModifierServiceFactory.getDataModifier(dto);
            try {
                LOG.info("Data modifier fetched {}", modifierService.getClass().getName());
                modifierService.modifyData(dto);
            } catch (Exception e) {
                LOG.error("Error in modifying upload data as per the configuration ", e);
                throw new GenericPersisterException(e, "Error in modifying upload data as per the configuration", GenericPersisterExceptionCode.UPLOAD_DATA_MODIFICATION_FAILED);
            }
        }

        List<D> docList = reader.getDocumentsForDTO(dto);
        List<E> entityList = reader.getEntitiesForDTO(dto);

        List<D> docListToSave = engine.enrichDocuments(dto, docList);
        List<E> entityListToSave = engine.enrichEntities(dto, entityList, user.getEmail());
        GenericPersisterRequest<E, D> req = new GenericPersisterRequest<E, D>();
        req.setDocumentsToPersist(docListToSave);
        req.setEntitiesToPersist(entityListToSave);
        req.setAsynchToMongo(false);
        req.setSkipValidation(skipGenricValidation);

        GenericPersisterResponse<E, D> resp = persistenceService.persistOne(req);
        if (!resp.isSuccessful()) {
            String msg = "DTO " + dto + " not saved , message - " + resp.getMessage();
            LOG.info("GenericPersisterResponse message - {}", msg);
            throw new GenericPersisterException(null, msg, GenericPersisterExceptionCode.ENTITY_FAILED_TO_PERSIST);
        }

        if (resp.isSuccessful() && (!resp.getValidationFailedEntityList().isEmpty() || !resp.getValidationPassedEntityList().isEmpty())) {

            LOG.info("Validation has been done for passed entities {} & failed entities {}, will call post validation handler.", resp.getValidationPassedEntityList(),
                    resp.getValidationFailedEntityList());

            IPersisterPostValidationHandler<E, D, GenericPersisterResponse<E, D>> validationHandler = postValidationHandlerFactory.getService(req.getEntitiesToPersist().get(0).getClass());

            resp = validationHandler.handleValidationResponse(resp, user);
        } else {

            LOG.info("Successfully persisted all entities.");
        }

        return resp;
    }

}
