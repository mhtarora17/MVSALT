/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 09-Oct-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.services.fm.impl;

import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.cocofs.entity.AbstractFMMapping;
import com.snapdeal.cocofs.services.data.engine.IDataEngineWithAerospike;
import com.snapdeal.cocofs.services.fm.dto.SellerFMMappingUpdateDto;

public abstract class AbstractFMMappingDataEngine<T, E , R> implements IDataEngineWithAerospike<T, E ,R> {

   

    public void enrichFMMappingEntity(SellerFMMappingUpdateDto dto, AbstractFMMapping mapping, String userEmail) {
        mapping.setCreated(mapping.getCreated() != null ? mapping.getCreated() : DateUtils.getCurrentTime());
        mapping.setSellerCode(dto.getSellerCode());
        mapping.setFulfilmentModel(dto.getFulfilmentModel());
        mapping.setLastUpdated(dto.getLastUpdated());
        mapping.setEnabled(true);
        mapping.setUpdatedBy(userEmail);
    }

}
