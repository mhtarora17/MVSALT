package com.snapdeal.cocofs.services.fm.impl;	

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.DataSource;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.services.fm.IFMDataSourceService;

@Service("FMDataSourceServiceFactory")
public class FMDataSourceServiceFactory {

	@Autowired
	@Qualifier("FMDataSourceServiceForCache")	
	private IFMDataSourceService  				   	 fmDataSourceServiceForCache;

	@Autowired
	@Qualifier("FMDataSourceServiceForAerospike")
	private IFMDataSourceService 					 fmDataSourceServiceForAerospike;

	@Autowired
	@Qualifier("FMDataSourceServiceForMySql")
	private IFMDataSourceService					 fmDataSourceServiceForMySql;

	/**
	 * It returns concrete object of @IFMDataSourceService, depending on the 
	 * configured value of {@link Property.DATA_SOURCE_FOR_FM_READ.}
	 * Default vale (@link FMDataSourceServiceForAerospike)
	 * 
	 * @return {@link IFMDataSourceService}
	 */

	public IFMDataSourceService getFMDataSourceService() {
		DataSource dataSource = DataSource.valueOf(ConfigUtils.getStringScalar(Property.DATA_SOURCE_FOR_FM_READ).toUpperCase());
		IFMDataSourceService fmdataSource = fmDataSourceServiceForAerospike;
		if(dataSource == DataSource.AEROSPIKE){
			fmdataSource = fmDataSourceServiceForAerospike;
		} else if(dataSource == DataSource.MYSQL){
			fmdataSource = fmDataSourceServiceForMySql;
		}
		return fmdataSource;
	}
	
	public IFMDataSourceService getFMServiceForSellerFmLookup() {
        IFMDataSourceService fmDataSourceService;
        if (ConfigUtils.getBooleanScalar(Property.ENABLE_SELLER_FM_MAPPING_CACHE)) {
            fmDataSourceService = getFMDataSourceServiceForCache();
        } else {
            fmDataSourceService = getFMDataSourceService();
        }
        return fmDataSourceService;
    }

    public IFMDataSourceService getFMServiceForSellerSupcFmLookup() {
        IFMDataSourceService fmDataSourceService;
        if (ConfigUtils.getBooleanScalar(Property.ENABLE_SELLER_SUPC_FM_MAPPING_CACHE)) {
            fmDataSourceService = getFMDataSourceServiceForCache();
        } else {
            fmDataSourceService = getFMDataSourceService();
        }
        return fmDataSourceService;
    }


	public IFMDataSourceService getFMDataSourceServiceForCache() {
		return fmDataSourceServiceForCache;
	}

	public IFMDataSourceService getFMDataSourceServiceForAerospike() {
		return fmDataSourceServiceForAerospike;
	}

	public IFMDataSourceService getFMDataSourceServiceForMySql() {
		return fmDataSourceServiceForMySql;
	}
}
