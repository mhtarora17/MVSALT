/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 28-Aug-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.services.task.update;

import java.util.Date;
import java.util.List;

import com.snapdeal.cocofs.entity.TaskDetail;
import com.snapdeal.cocofs.services.task.dto.TaskDetailDTO;
import com.snapdeal.cocofs.services.task.dto.TaskDetailUpdateResponseDTO;
import com.snapdeal.cocofs.services.task.dto.TaskInfoDTO;
import com.snapdeal.cocofs.services.task.dto.TaskParamDTO;
import com.snapdeal.task.exception.TaskException;

public interface ITaskUpdateService {

    public List<TaskDetailDTO> getAllTasks();

    public List<TaskDetailDTO> getAllTaskByClass(String implClass);

    public void updateTaskDetailAndParams(TaskDetailDTO taskDetailDTO, String updatedBy) throws TaskException;

    public TaskDetail getTaskDetailByName(String taskName);

    public List<TaskParamDTO> getTaskParamsByTaskName(String taskName);

    public void startAllTasks() throws TaskException;

    public void stopAllTasks() throws TaskException;

    public void updateTask(TaskDetail taskDetail) throws TaskException;

    public void updateTask(String taskName) throws TaskException;

    public void startTask(String taskName) throws TaskException;

    public void stopTask(String taskName) throws TaskException;

    List<TaskInfoDTO> getAllEnabledTaskInfo();

    void createTaskDetailUpdate(String actionCode, String taskName, String updatedBy);

    void createTaskDetailUpdateForAllHosts(String actionCode, String taskName,String updatedBy);

    void updateTaskStatusInfo(String taskName, Date nextExecutionTime, Date previousExecutionTime, String code);

    public void updateTaskStatusInfo(String taskName, String code);

    void createTaskDetailUpdate(String actionCode, String taskName, String hostName, String requestedBy);

    TaskDetailUpdateResponseDTO validateRequest(TaskDetailDTO taskDetailDTO);

    void resumeAllTasks() throws TaskException;

    void updateAllTasks() throws TaskException;

    public void pauseAllTasks() throws TaskException;

}
