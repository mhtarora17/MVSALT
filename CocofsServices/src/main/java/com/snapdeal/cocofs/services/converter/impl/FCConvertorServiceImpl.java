/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.services.converter.impl;

import org.springframework.stereotype.Service;

import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.cocofs.entity.FCAddress;
import com.snapdeal.cocofs.entity.FulfillmentCentre;
import com.snapdeal.cocofs.services.converter.IFCConvertorService;
import com.snapdeal.cocofs.sro.FCAddressDetailSRO;
import com.snapdeal.cocofs.sro.FCDetailSRO;

/**
 *  
 *  @version     1.0, 30-Mar-2015
 *  @author ankur
 */
@Service("fCConvertorServiceImpl")
public class FCConvertorServiceImpl implements IFCConvertorService {
    
    @Override
    public FCDetailSRO getFCDetailSRO(FulfillmentCentre fc) {

        if (fc == null) {
            throw new IllegalArgumentException("Input fulfillmentCenter is found to be null");
        } else if (StringUtils.isEmpty(fc.getCode())) {
            throw new IllegalArgumentException("Input fulfillmentCenter code  is found to be null");
        } else if (fc.getFcAddress()== null) {
            throw new IllegalArgumentException("Input fulfillmentCenter address is found to be null");
        }

        FCDetailSRO fcDetail = new FCDetailSRO();
        FCAddressDetailSRO address = getFCAddressDetailSRO(fc.getFcAddress());
        fcDetail.setFcAddress(address);
        fcDetail.setName(fc.getName());
        fcDetail.setCode(fc.getCode());
        fcDetail.setType(fc.getType());
        fcDetail.setSdInstant(fc.getSdinstant() == null ? false :fc.getSdinstant());

        return fcDetail;
    }
    
    @Override
    public FCAddressDetailSRO getFCAddressDetailSRO(FCAddress fc) {
        FCAddressDetailSRO sro = new FCAddressDetailSRO();
        if( null != fc){
        sro.setName(fc.getName());
        sro.setAddressLine1(fc.getAddressLine1());
        sro.setAddressLine2(fc.getAddressLine2());
        sro.setCity(fc.getCity());
        sro.setState(fc.getState());
        sro.setPincode(fc.getPincode());
        sro.setMobile(fc.getMobile());
        sro.setLandline(fc.getLandLine());
        sro.setEmail(fc.getEmail());
        }
        return sro;
    }
}
