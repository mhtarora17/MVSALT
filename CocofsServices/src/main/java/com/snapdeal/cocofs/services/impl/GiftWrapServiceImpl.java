/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.base.cache.CacheManager;
import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.catalog.base.model.GetMinProductInfoBySupcRequest;
import com.snapdeal.catalog.base.model.GetMinProductInfoResponse;
import com.snapdeal.cocofs.access.IProductAttributeService;
import com.snapdeal.cocofs.cache.GiftWrapRulesCache;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.enums.BlockName;
import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.external.service.exception.FulfillmentModelNotFoundException;
import com.snapdeal.cocofs.external.service.factory.IExternalServiceFactory;
import com.snapdeal.cocofs.giftwrap.dto.GiftWrapEvaluationParams;
import com.snapdeal.cocofs.mongo.CocofsMongoQuery;
import com.snapdeal.cocofs.mongo.CocofsMongoQuery.Operator;
import com.snapdeal.cocofs.mongo.Criteria;
import com.snapdeal.cocofs.mongo.mao.IGenericMao;
import com.snapdeal.cocofs.mongo.model.CocofsRuleUnit;
import com.snapdeal.cocofs.mongo.model.CocofsRuleUnit.CocofsRuleUnitFieldName;
import com.snapdeal.cocofs.mongo.model.CocofsRuleUnit.CocofsRuleUnitType;
import com.snapdeal.cocofs.mongo.model.ProductAttributeUnit;
import com.snapdeal.cocofs.services.IGiftWrapService;
import com.snapdeal.cocofs.services.fm.IFulfilmentModelService;
import com.snapdeal.cocofs.sro.FulfillmentModel;
import com.snapdeal.cocofs.sro.GiftWrapProductSRO;
import com.snapdeal.cocofs.sro.GiftWrapperInfo;
import com.snapdeal.cocofs.utils.DangerousGoodsTypeConversionUtil;
import com.snapdeal.cocofs.utils.UtilHelper;
import com.snapdeal.ipms.base.api.getInventoryPricingBySUPCSeller.v1.SUPCSellerInventoryPricingSRO;
import com.snapdeal.rule.engine.entity.BlockParams;
import com.snapdeal.rule.engine.entity.Rule;
import com.snapdeal.rule.engine.evaluator.impl.RuleEvaluator;
import com.snapdeal.rule.engine.services.IRulesService;

/**
 * @version 1.0, 17-Jul-2014
 * @author ankur
 */

@Service("giftWrapService")
public class GiftWrapServiceImpl extends RuleEvaluator implements IGiftWrapService {

    private static final Logger      LOG = LoggerFactory.getLogger(GiftWrapServiceImpl.class);

    @Autowired
    private IFulfilmentModelService  fulfilmentModelService;

    @Autowired
    private IProductAttributeService productAttrService;

    @Autowired
    private IRulesService            ruleService;

    @Autowired
    private IGenericMao              genericMao;

    @Autowired
    private IExternalServiceFactory  factory;

    @Override
    public Map<GiftWrapProductSRO, Boolean> getGiftWrapInfoBySUPCSellerList(List<GiftWrapProductSRO> giftwrapProducts) {
        Map<GiftWrapProductSRO, Boolean> productGiftwrapMap = new HashMap<GiftWrapProductSRO, Boolean>();
        Map<String, ProductAttributeUnit> supcToPauMap = new HashMap<String, ProductAttributeUnit>();
        for (GiftWrapProductSRO gfpSRO : giftwrapProducts) {
            GiftWrapperInfo giftwrapInfo;
            String sellerCode = gfpSRO.getSellerCode();
            String supc = gfpSRO.getSupc();

            try {
                giftwrapInfo = getGiftWrapInfoBySUPCSeller(supc, gfpSRO.getSellerCode(), gfpSRO.getSubcat(), gfpSRO.getSellingPrice(), supcToPauMap);
                productGiftwrapMap.put(gfpSRO, giftwrapInfo.isGiftWrapApplicable());
            } catch (ExternalDataNotFoundException e) {
                productGiftwrapMap.put(gfpSRO, false);
                LOG.error("data not found for seller " + sellerCode + ",supc " + supc + " Exception: {}", e.getMessage());
            } catch (FulfillmentModelNotFoundException e) {
                productGiftwrapMap.put(gfpSRO, false);
                LOG.error("Fulfilment model not found for seller " + sellerCode + ",supc " + supc + " Exception: {}", e.getMessage());
            }
        }

        return productGiftwrapMap;
    }

    @Override
    public GiftWrapperInfo getGiftWrapInfoBySUPCSeller(String supc, String sellerCode, String subcat, Integer sellingPrice) throws ExternalDataNotFoundException,
            FulfillmentModelNotFoundException {
        return getGiftWrapInfoBySUPCSeller(supc, sellerCode, subcat, sellingPrice, null);
    }

    private GiftWrapperInfo getGiftWrapInfoBySUPCSeller(String supc, String sellerCode, String subcat, Integer sellingPrice, Map<String, ProductAttributeUnit> supcToPauMap)
            throws ExternalDataNotFoundException, FulfillmentModelNotFoundException {

        GiftWrapperInfo giftWrapperInfo = new GiftWrapperInfo(supc, sellerCode, false);

        boolean applyGiftWrapLogic = ConfigUtils.getBooleanScalar(Property.APPLY_GIFTWARP_LOGIC_FOR_API);

        if (!applyGiftWrapLogic) {
            LOG.warn("apply.giftwarp.logic.for.api property is set false hence returning false for giftwrap");
            return giftWrapperInfo;
        }

        //we are passing subcat null here, fulfilmentModelService service will internally fetch subcategory from cams

        FulfillmentModel ffModel = fulfilmentModelService.getFulfilmentModel(sellerCode, supc, subcat);

        if (null == ffModel) {
            LOG.warn("No fullfilment model found for passed parameters");
            throw new FulfillmentModelNotFoundException("No fullfilment model found for passed parameters");
        }

        if (!(FulfillmentModel.FC_VOI.getCode().equals(ffModel.getCode()) || FulfillmentModel.OCPLUS.getCode().equals(ffModel.getCode()))) {
            LOG.debug("FM is {} for supc {}, seller {}, hence giftwrap is not allowed (allowed only for FC_VOI or OCPLUS FM)",ffModel.getCode(), supc, sellerCode);
            return giftWrapperInfo;
        }

        boolean canGiftWrap = canGiftWrapForSUPCSeller(supc, sellerCode, subcat, sellingPrice, supcToPauMap);
        giftWrapperInfo.setGiftWrapApplicable(canGiftWrap);
        return giftWrapperInfo;
    }

    private boolean canGiftWrapForSUPCSeller(String supc, String sellerCode, String subcat, Integer sellingPrice, Map<String, ProductAttributeUnit> supcToPauMap) {

        //if paUnit was not passed by caller
        if (supcToPauMap == null) {
            supcToPauMap = new HashMap<String, ProductAttributeUnit>();
        }
        ProductAttributeUnit paUnit = supcToPauMap.get(supc);

        if (paUnit == null) {
            paUnit = productAttrService.getProductAttributeUnitBySUPC(supc);
            supcToPauMap.put(supc, paUnit);
        }

        if (paUnit != null) {
            return canGiftWrapForSUPCSellerByCategoryRules(paUnit, supc, sellerCode, subcat, sellingPrice);
        } else {
            LOG.error("No pa unit found for supc " + supc + ",hence not allowing giftwrap by default");
        }
        return false;
    }

    public boolean canGiftWrapForSUPCSellerByCategoryRules(ProductAttributeUnit pau, String supc, String sellerCode, String subcat, Integer sellingPrice) {

        //rules are for banning
        List<Rule> rules = getGiftWrapRulesForSUPC(supc, subcat);
        if ((null == rules) || rules.isEmpty()) {
            return true;
        } else {
            return evaluateGiftWrapCategoryRules(pau, rules, sellerCode, sellingPrice);

        }
    }

    private List<Rule> getGiftWrapRulesForSUPC(String supc, String categoryUrl) {
        if (StringUtils.isEmpty(categoryUrl)) {
            categoryUrl = getCategoryUrlForSUPC(supc);
        }

        //should not happen
        if (null == categoryUrl) {
            LOG.info("Could not obtain category for supc " + supc);
            return new ArrayList<Rule>();
        }

        LOG.debug("SUPC " + supc + " Category " + categoryUrl);
        List<CocofsRuleUnit> ruleList = fetchGiftWrapRuleForCategory(categoryUrl);

        if (null == ruleList || ruleList.isEmpty()) {
            LOG.debug("There are no enabled gift wrap  rule for category " + categoryUrl + " for supc " + supc);
            return new ArrayList<Rule>();
        } else {
            LOG.debug("For SUPC " + supc + " of Category " + categoryUrl + " rule found " + ruleList);
        }
        // RuleEvalutor converts Rule To RuleDTO to actually evaluate rules
        // only required fields are id, name, criteria,  priority
        // hence we populate only populate relevant fields from
        // cocofs rule unit
        List<Rule> list = new ArrayList<Rule>();
        for (CocofsRuleUnit unit : ruleList) {
            Rule r = RuleEvaluationHelper.getRuleFromCocofsRuleUnit(unit);
            list.add(r);
        }

        return list;
    }

    private List<CocofsRuleUnit> fetchGiftWrapRuleForCategory(String categoryUrl) {
        List<CocofsRuleUnit> outList = new ArrayList<CocofsRuleUnit>();
        List<CocofsRuleUnit> ruleList = null;

        if (!ConfigUtils.getBooleanScalar(Property.USE_GIFTWRAP_RULES_CACHE)) {
            LOG.warn("use.giftwrap.rules.cache is set false hence fetching giftwrap rules from mongo for category {}", categoryUrl);
            List<Criteria> queryCriterias = new ArrayList<Criteria>();
            Criteria mqcForCategory = new Criteria(CocofsRuleUnitFieldName.PARAM_VALUE.getName(), categoryUrl, Operator.EQUAL);
            Criteria mqcForType = new Criteria(CocofsRuleUnitFieldName.TYPE.getName(), CocofsRuleUnitType.GIFTWRAP_CATEGORY.getCode(), Operator.EQUAL);
            queryCriterias.add(mqcForCategory);
            queryCriterias.add(mqcForType);

            LOG.debug("Fetching giftwrap  rules for category " + categoryUrl);
            ruleList = genericMao.getDocumentList(new CocofsMongoQuery(queryCriterias), CocofsRuleUnit.class);
        } else {

            ruleList = CacheManager.getInstance().getCache(GiftWrapRulesCache.class).getRules(categoryUrl);
        }

        if (null == ruleList) {
            ruleList = new ArrayList<CocofsRuleUnit>();
        }
        Date now = DateUtils.getCurrentTime();
        LOG.debug("Fetched " + ruleList.size() + " rules for giftwrap  with category " + categoryUrl);
        outList = RuleEvaluationHelper.getRuleValidOnDate(ruleList, now);

        return outList;
    }

    /**
     * <p>
     * If any rule gets passed, it means giftwrap is banned for that supc.
     * </p>
     * <p>
     * This function returns true only if giftwrap is applicable for requested supc-seller.
     * </p>
     * 
     * @param pau
     * @param rules
     * @return
     */
    private boolean evaluateGiftWrapCategoryRules(ProductAttributeUnit pau, List<Rule> rules, String sellerCode, Integer sellingPrice) {
        LOG.debug("Going to evaluate gift wrap rules for supc {}", pau.getSupc());
        List<BlockParams> blockParams = ruleService.getBlockParamsByBlockName(BlockName.GiftWrapCategoryRuleblock.getCode());
        LOG.debug("Got giftwrap rule Block Param for supc {}", pau.getSupc());

        GiftWrapEvaluationParams paramDTO;
        try {
            paramDTO = getGiftWrapEvaluationParam(pau, sellerCode, sellingPrice);
            LOG.debug("Got giftwrap rule  Evaluation Param for supc {} param {} ", pau.getSupc(), paramDTO);
        } catch (ExternalDataNotFoundException e) {
            LOG.info("Error while trying to get giftwrap evaluation param ", e.getMessage());
            LOG.info("hence allowing giftwrap for this supc {}, sellerCode {}", pau.getSupc(), sellerCode);
            return true;
        }

        Map<String, Object> ruleParam = populateRuleParamsFromBlockObject(paramDTO, blockParams);

        for (Entry<String, Object> e : ruleParam.entrySet()) {
            LOG.debug("Parameter " + e.getKey() + " Value " + e.getValue());
        }

        boolean isGiftWrapBanned = evaluateRules(rules, ruleParam);
        if (isGiftWrapBanned) {
            LOG.info("Gifwrap is found to be banned after evaluation of rules for supc {}, sellerCode {}", pau.getSupc(), sellerCode);
        } else {
            LOG.info("Gifwrap is found to be allowed after evaluation of rules for supc {}, sellerCode {}", pau.getSupc(), sellerCode);
        }
        return !isGiftWrapBanned;

    }

    private GiftWrapEvaluationParams getGiftWrapEvaluationParam(ProductAttributeUnit pau, String sellerCode, Integer sellingPrice) throws ExternalDataNotFoundException {
        GiftWrapEvaluationParams p = new GiftWrapEvaluationParams();
        if (null == sellingPrice) {
            LOG.debug("selling price not passed in api request , hence calling IPMS to get price for supc {} ", pau.getSupc());
            sellingPrice = getPriceForSUPCVendor(pau.getSupc(), sellerCode);
        }
        p.setPrice(sellingPrice);
        p.setFragile(pau.isFragile());
        // Volumetric Weight as defined in SNAPDEALTECH-11840 for purpose of rule evaluation
        p.setVolume(UtilHelper.getVolumetricWeightFromLBH(pau.getLength(), pau.getBreadth(), pau.getHeight()));
        p.setWeight(pau.getWeight());

        String dangerousGoodsType = DangerousGoodsTypeConversionUtil.getDangerousGoodsType(pau.getDangerousGoodsType(), pau.isHazMat(), pau.isLiquid());
        if (StringUtils.isNotEmpty(dangerousGoodsType)) {
            ArrayList<String> dlist = new ArrayList<String>();
            dlist.add(dangerousGoodsType);
            p.setDangerousGoodsType(dlist);
        }
        LOG.debug("SUPC : " + pau.getSupc() + " Giftwrap Param " + p);
        return p;
    }

    /**
     * This returns price for a supc-vendor by calling a VIPMS API
     */
    private int getPriceForSUPCVendor(String supc, String sellerCode) throws ExternalDataNotFoundException {
        SUPCSellerInventoryPricingSRO pricingSRO = null;

        pricingSRO = factory.getVIPMSPSExternalService().getInventoryPricingBySUPCSeller(supc, sellerCode);
        if ((null == pricingSRO)) {
            LOG.debug("pricing details are recieved as null for supc {} seller {} ", supc, sellerCode);
            throw new ExternalDataNotFoundException(ExternalDataNotFoundException.Type.DEFAULT, "null pricing details recieved from VIPMS");
        }

        // can happen in case of prebook
        if (null == pricingSRO.getSellingPrice()) {
            return 0;
        }
        return pricingSRO.getSellingPrice();
    }

    /**
     * This returns category url for a supc by calling a CAMS API
     */
    private String getCategoryUrlForSUPC(String supc) {
        String categoryUrl = null;
        GetMinProductInfoBySupcRequest request = new GetMinProductInfoBySupcRequest(supc);
        GetMinProductInfoResponse response;
        try {
            response = factory.getCAMSExternalService().getMinProductInfo(request);
        } catch (ExternalDataNotFoundException e) {
            LOG.info("Could not obtain category for supc " + supc, e);
            return null;
        }
        if ((null == response) || !response.isSuccessful()) {
            LOG.info("Could not obtain category for supc response null or unsuccessfull " + supc);
            return null;
        }
        categoryUrl = response.getProductInfo().getCategoryUrl();
        return categoryUrl;
    }

}
