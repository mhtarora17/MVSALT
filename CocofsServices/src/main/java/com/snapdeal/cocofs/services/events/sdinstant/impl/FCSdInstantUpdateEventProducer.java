/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.services.events.sdinstant.impl;

import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.entity.EventInstance;
import com.snapdeal.cocofs.services.events.dto.FCAttributeUpdateEventObj;
import com.snapdeal.cocofs.services.events.impl.BaseEventProducer;
import com.snapdeal.cocofs.services.events.impl.EventType;

/**
 * @version 1.0, 25-Mar-2015
 * @author shiv
 */

@Service("FCSdInstantUpdateEventProducer")
public class FCSdInstantUpdateEventProducer extends BaseEventProducer<FCAttributeUpdateEventObj> {

    @Override
    protected String getEventCode() {
        return EventType.SDINSTANT_UPDATE_EVENT_ON_FC_UPDATE.getCode();
    }

    @Override
    public void setParams(EventInstance eventInstance, FCAttributeUpdateEventObj eventObj) {
        eventInstance.setParam1(eventObj.isSdInstant() ? "true":"false");
        eventInstance.setParam2(eventObj.getFcCode());
        eventInstance.setCreated(eventObj.getCreated());
    }

}
