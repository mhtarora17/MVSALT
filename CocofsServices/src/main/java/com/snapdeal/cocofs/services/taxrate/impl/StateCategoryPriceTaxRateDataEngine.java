/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.services.taxrate.impl;

import java.util.ArrayList;
import java.util.List;

import javax.naming.OperationNotSupportedException;

import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.entity.StateCategoryPriceTaxRate;
import com.snapdeal.cocofs.mongo.model.StateCategoryPriceTaxRateUnit;
import com.snapdeal.cocofs.services.taxrate.dto.StateCategoryPriceTaxRateUploadDTO;

/**
 *  
 *  @version     1.0, 18-Aug-2014
 *  @author ankur
 */
@Service("StateCategoryPriceTaxRateDataEngine")
public class StateCategoryPriceTaxRateDataEngine  extends AbstractTaxRateDataEngine<StateCategoryPriceTaxRateUploadDTO, StateCategoryPriceTaxRate, StateCategoryPriceTaxRateUnit> {



    @Override
    public List<StateCategoryPriceTaxRateUnit> enrichDocuments(StateCategoryPriceTaxRateUploadDTO dto, List<StateCategoryPriceTaxRateUnit> documents) throws OperationNotSupportedException {
        if (dto != null) {
            StateCategoryPriceTaxRateUnit document = null;
            List<StateCategoryPriceTaxRateUnit> documentList = new ArrayList<StateCategoryPriceTaxRateUnit>();
            if (null != documents && !documents.isEmpty()) {
                document = documents.get(0);
            } else {
                document = new StateCategoryPriceTaxRateUnit();
            }
            updateSellerCategoryTaxRateUnit(dto, document);
            documentList.add(document);
            return documentList;

        }
        return null;
    }

    private void updateSellerCategoryTaxRateUnit(StateCategoryPriceTaxRateUploadDTO dto, StateCategoryPriceTaxRateUnit document) {
        enrichTaxRateUnit(dto, document);
        document.setCategoryUrl(dto.getSubCategoryUrl());
        document.setState(dto.getState());
        document.setLowerPriceLimit(dto.getLowerPriceLimit());
        document.setUpperPriceLimit(dto.getUpperPriceLimit());

    }
    
    
    @Override
    public List<StateCategoryPriceTaxRate> enrichEntities(StateCategoryPriceTaxRateUploadDTO dto, List<StateCategoryPriceTaxRate> entities, String userEmail) throws OperationNotSupportedException {
        if (null != dto) {
            StateCategoryPriceTaxRate entity = null;
            List<StateCategoryPriceTaxRate> mappingList = new ArrayList<StateCategoryPriceTaxRate>();
            if (null != entities && !entities.isEmpty()) {
                entity = entities.get(0);
            } else {
                entity = new StateCategoryPriceTaxRate();
            }
            updateSellerCategoryTaxRate(dto, entity, userEmail);
            mappingList.add(entity);
            return mappingList;
        }
        return null;
    }


    private void updateSellerCategoryTaxRate(StateCategoryPriceTaxRateUploadDTO dto, StateCategoryPriceTaxRate entity, String userEmail) {
        enrichTaxRateEntity(dto, entity, userEmail);
        entity.setCategoryUrl(dto.getSubCategoryUrl());
        entity.setState(dto.getState());
        entity.setLowerPriceLimit(dto.getLowerPriceLimit());
        entity.setUpperPriceLimit(dto.getUpperPriceLimit());

    }
}
