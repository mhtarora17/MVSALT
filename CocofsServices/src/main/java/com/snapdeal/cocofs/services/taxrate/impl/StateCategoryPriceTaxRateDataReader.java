/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.services.taxrate.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.dataaccess.taxrate.ITaxRateDBDataReadService;
import com.snapdeal.cocofs.dataaccess.taxrate.ITaxRateMongoService;
import com.snapdeal.cocofs.entity.StateCategoryPriceTaxRate;
import com.snapdeal.cocofs.mongo.model.StateCategoryPriceTaxRateUnit;
import com.snapdeal.cocofs.services.IDataReader;
import com.snapdeal.cocofs.services.taxrate.dto.StateCategoryPriceTaxRateUploadDTO;

/**
 *  
 *  @version     1.0, 18-Aug-2014
 *  @author ankur
 */

@Service("StateCategoryPriceTaxRateDataReader")
public class StateCategoryPriceTaxRateDataReader implements IDataReader<StateCategoryPriceTaxRateUploadDTO, StateCategoryPriceTaxRate, StateCategoryPriceTaxRateUnit> {
   
    @Autowired
    private ITaxRateMongoService     taxRateMongoService;

    @Autowired
    private ITaxRateDBDataReadService taxRateDBDataReadService;

    @Override
    public List<StateCategoryPriceTaxRateUnit> getDocumentsForDTO(StateCategoryPriceTaxRateUploadDTO dto) {
        List<StateCategoryPriceTaxRateUnit> documents = taxRateMongoService.getEnabledStateCategoryPriceTaxRateUnit(dto.getState(), dto.getSubCategoryUrl());
        List<StateCategoryPriceTaxRateUnit> documentList = new ArrayList<StateCategoryPriceTaxRateUnit>();
        if (null != documents) {
            return documents;
        }
        return documentList;
    }

    @Override
    public List<StateCategoryPriceTaxRate> getEntitiesForDTO(StateCategoryPriceTaxRateUploadDTO dto) {
        List<StateCategoryPriceTaxRate> existingEntityList = taxRateDBDataReadService.getStateCategoryPriceTaxRate(dto.getState(), dto.getSubCategoryUrl());
        List<StateCategoryPriceTaxRate> entityList = new ArrayList<StateCategoryPriceTaxRate>();
       
        if (null != existingEntityList) {
            return existingEntityList;
        }
        return entityList;
    }
    
    
    
    private boolean isPriceRangeOverlapping(StateCategoryPriceTaxRateUploadDTO dto ,StateCategoryPriceTaxRate stateCategoryPriceTaxRate){
        Double a0 = dto.getLowerPriceLimit();
        Double a1 =  dto.getUpperPriceLimit() ;
        Double b0 = stateCategoryPriceTaxRate.getLowerPriceLimit();
        Double b1 =  stateCategoryPriceTaxRate.getUpperPriceLimit();
        if(a0 != null && a1 != null && b0 != null && b1 != null){
            return a0 <= b1 && b0 <= a1;
        
        }
        
        
        return false;
        
    }

}
