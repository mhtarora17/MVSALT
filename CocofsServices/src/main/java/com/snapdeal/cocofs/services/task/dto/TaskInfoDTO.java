/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 02-Sep-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.services.task.dto;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import com.snapdeal.base.utils.DateUtils;

@XmlRootElement
public class TaskInfoDTO {

    private String  name;

    private String  implClass;

    private String  lastExecutionTime;

    private String  nextExecutionTime;

    private String  currentStatus;

    private String  hostName;

    private Boolean quartzTask;

    public TaskInfoDTO() {
    }

    public TaskInfoDTO(String name, String implClass, Date lastExecutionTime, Date nextExecutionTime, String currentStatus, String hostName) {
        this.name = name;
        this.implClass = implClass;
        this.lastExecutionTime = DateUtils.dateToString(lastExecutionTime, "yyyy-MM-dd HH:mm:ss");
        this.nextExecutionTime = DateUtils.dateToString(nextExecutionTime, "yyyy-MM-dd HH:mm:ss");
        this.currentStatus = currentStatus;
        this.hostName = hostName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImplClass() {
        return implClass;
    }

    public void setImplClass(String implClass) {
        this.implClass = implClass;
    }

    public String getLastExecutionTime() {
        return lastExecutionTime;
    }

    public void setLastExecutionTime(String lastExecutionTime) {
        this.lastExecutionTime = lastExecutionTime;
    }

    public String getNextExecutionTime() {
        return nextExecutionTime;
    }

    public void setNextExecutionTime(String nextExecutionTime) {
        this.nextExecutionTime = nextExecutionTime;
    }

    public String getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public Boolean getQuartzTask() {
        return quartzTask;
    }

    public void setQuartzTask(Boolean quartzTask) {
        this.quartzTask = quartzTask;
    }

}
