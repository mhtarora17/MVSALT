package com.snapdeal.cocofs.services.impl;

import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.services.IUploadDataModifier;

@Service("defaultDataModifier")
public class DefaultDataModifier implements IUploadDataModifier<Object> {

    @Override
    public void modifyData(Object t) throws IllegalArgumentException, IllegalAccessException {
        return;
    }

}
