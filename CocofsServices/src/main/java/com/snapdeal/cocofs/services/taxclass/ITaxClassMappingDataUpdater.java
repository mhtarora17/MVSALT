/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 24-Oct-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.services.taxclass;

import java.util.List;

import com.snapdeal.cocofs.core.aerospike.vo.SubcatTaxClassMappingVO;
import com.snapdeal.cocofs.core.aerospike.vo.SupcTaxClassMappingVO;
import com.snapdeal.cocofs.entity.SubcatTaxClassMapping;
import com.snapdeal.cocofs.entity.SupcTaxClassMapping;
import com.snapdeal.cocofs.generic.persister.exception.GenericPersisterException;
import com.snapdeal.cocofs.generic.persister.response.GenericPersisterWithAerospikeResponse;

public interface ITaxClassMappingDataUpdater {

    GenericPersisterWithAerospikeResponse<SupcTaxClassMapping, SupcTaxClassMappingVO> updateSupcTaxClassMapping(List<SupcTaxClassMapping> entityListToSave,
            List<SupcTaxClassMappingVO> recordListToSave) throws GenericPersisterException;

    GenericPersisterWithAerospikeResponse<SubcatTaxClassMapping, SubcatTaxClassMappingVO> updateSubcatTaxClassMapping(List<SubcatTaxClassMapping> toBeUpdatedEntityList,
            List<SubcatTaxClassMappingVO> recordListToSave) throws GenericPersisterException;

}
