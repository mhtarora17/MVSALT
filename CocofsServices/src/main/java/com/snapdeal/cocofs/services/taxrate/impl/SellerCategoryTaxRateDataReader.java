/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.services.taxrate.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.dataaccess.taxrate.ITaxRateDBDataReadService;
import com.snapdeal.cocofs.dataaccess.taxrate.ITaxRateMongoService;
import com.snapdeal.cocofs.entity.SellerCategoryTaxRate;
import com.snapdeal.cocofs.mongo.model.SellerCategoryTaxRateUnit;
import com.snapdeal.cocofs.services.IDataReader;
import com.snapdeal.cocofs.services.taxrate.dto.SellerCategoryTaxRateUploadDTO;

/**
 *  
 *  @version     1.0, 18-Aug-2014
 *  @author ankur
 */
@Service("SellerCategoryTaxRateDataReader")
public class SellerCategoryTaxRateDataReader implements IDataReader<SellerCategoryTaxRateUploadDTO, SellerCategoryTaxRate, SellerCategoryTaxRateUnit> {
   
    @Autowired
    private ITaxRateMongoService     taxRateMongoService;

    @Autowired
    private ITaxRateDBDataReadService taxRateDBDataReadService;

    @Override
    public List<SellerCategoryTaxRateUnit> getDocumentsForDTO(SellerCategoryTaxRateUploadDTO dto) {
        SellerCategoryTaxRateUnit document = taxRateMongoService.getSellerCategoryTaxRateUnit(dto.getSellerCode(), dto.getCategoryUrl());
        List<SellerCategoryTaxRateUnit> documentList = new ArrayList<SellerCategoryTaxRateUnit>();
        if (null != document) {
            documentList.add(document);
        }
        return documentList;
    }

    @Override
    public List<SellerCategoryTaxRate> getEntitiesForDTO(SellerCategoryTaxRateUploadDTO dto) {
        SellerCategoryTaxRate entity = taxRateDBDataReadService.getSellerCategoryTaxRate(dto.getSellerCode(), dto.getCategoryUrl());
        List<SellerCategoryTaxRate> entityList = new ArrayList<SellerCategoryTaxRate>();
        if (null != entity) {
            entityList.add(entity);
        }
        return entityList;
    }
}
