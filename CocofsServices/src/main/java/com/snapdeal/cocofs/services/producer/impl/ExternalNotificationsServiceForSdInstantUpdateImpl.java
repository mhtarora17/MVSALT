/**
 * 
 */
package com.snapdeal.cocofs.services.producer.impl;

import java.util.ArrayList;
import java.util.List;

import javax.jms.JMSException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.base.activemq.IActiveMQManager;
import com.snapdeal.base.activemq.exception.ActiveMQException;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.model.queue.SdInstantUpdateEvent;
import com.snapdeal.cocofs.model.queue.SdInstantUpdateMessage;
import com.snapdeal.cocofs.services.producer.IExternalNotificationsServiceForSdInstantUpdate;
import com.snapdeal.cocofs.services.producer.IExternalNotificationsServiceForSearchPush;
import com.snapdeal.cocofs.sro.SellerSupcUpdateEventsSRO;
import com.snapdeal.cocofs.sro.SellerSupcUpdateSRO;

/**
 * Producer Class to publish external events to ActiveMQ towards search. This class handles register, deregister and
 * push to activeMQ in batches.
 * 
 * @author ssuthar
 */

@Service("externalNotificationsServiceForSdInstantUpdateImpl")
public class ExternalNotificationsServiceForSdInstantUpdateImpl implements IExternalNotificationsServiceForSdInstantUpdate, DisposableBean {

    @Autowired
    private IActiveMQManager    activeMQManager;

    private static Long         activeMqToken = null;
    private String              queueName     = null;
    private String              queueURL      = null;

    private static final Logger LOG           = LoggerFactory.getLogger(ExternalNotificationsServiceForSdInstantUpdateImpl.class);

    @Override
    public void registerProducer(String queueName, String networkPath, String username, String password) {

        this.queueName = queueName;
        this.queueURL = networkPath;

        try {
            if (activeMqToken == null) {
                activeMqToken = activeMQManager.registerPublisher(queueName, networkPath, username, password);
                LOG.info("Sdinstant Active mq producer registered successfully  ... activeMqToken = {}", activeMqToken);
            } else {
                LOG.info("Sdinstant Active mq producer was already registered ... not attempting registration");
            }
        } catch (Exception e) {
            LOG.error("unable to register external-notification-service producer to Sdinstant activemq. " + "attempted with queueName:" + queueName + ", queueURL:" + queueURL + ", username"
                    + username, e);
        }
    }

    @Override
    public void unregisterProducer() {
        try {
            if (activeMqToken != null) {
                LOG.info("Sdinstant Active mq producer unregistered attempted ... activeMqToken = {}", activeMqToken);
                activeMQManager.unregisterPublisher(activeMqToken);
                LOG.info("Sdinstant Active mq producer unregistered successfully  ... activeMqToken = {}", activeMqToken);
            }
            activeMqToken = null;
        } catch (Exception e) {
            LOG.error("unable to unregister external-notification-service producer from activemq", e);
        }
    }

    @Override
    public void destroy() throws Exception {
        LOG.info("destory - Unregistering external-notification-service producer..");
        unregisterProducer();
    }

    /*
      * (non-Javadoc)
      * 
      * @see
      * com.snapdeal.cocofs.services.producer.IPublishExternalNotificationsService
      * #publishToSearch(com.snapdeal.cocofs.sro.SellerSupcUpdateEventsSRO)
      */

    //return false only in case on can not handle ungracefully and retry in needed
    @Override
    public boolean publishToQueue(List<SdInstantUpdateMessage> msg) {

        int totalMsgs = msg.size();
        LOG.info("publishing to sdinstant update started ... items:" + totalMsgs);

        if (!ConfigUtils.getBooleanScalar(Property.ACTIVE_MQ_SDINSTANT_PUSH_ENABLED)) {
            LOG.warn("activemq search push is disabled, not publishing updates on url: " + queueURL + " on queue: " + queueName);
            return true;
        }
   
        boolean bResult = true;
        int count = 0;
        if (!msg.isEmpty()) {

            // get no of records to send in 1 go , accordingly packetize and
            // send
            LOG.info("Publishing updates on url: " + queueURL + " on queue: " + queueName + ", total:" + totalMsgs);

            int maxPublishSize = ConfigUtils.getIntegerScalar(Property.ACTIVE_MQ_SDINSTANT_PUSH_ITEM_COUNT);
            int startIndex = 0;
            while (startIndex < totalMsgs) {
                count++;
                int projectedEndIndex = startIndex + maxPublishSize;
                int endIndex = Math.min(projectedEndIndex, totalMsgs);

                //since sublist is not serializable, have to use arraylist
                List<SdInstantUpdateMessage> publishList = new ArrayList<SdInstantUpdateMessage>(msg.subList(startIndex, endIndex));

                SdInstantUpdateEvent event = new SdInstantUpdateEvent();
                event.setSellersupcSdInstantUpdates(publishList);

                // TODO - how to handle exception if the send is failed?
                try {
                    LOG.info("Publishing updates size:" + publishList.size() + ", upto:" + endIndex + " total:" + totalMsgs);
                    LOG.debug("Publishing event: " + event);
                    activeMQManager.publish(activeMqToken, event);
                    LOG.info("Publishing updates finished.");
                } catch (ActiveMQException e) {
                    LOG.error("Error publishing post update on url: " + queueURL + " with update event: " + event + " on queue: " + queueName);
                    LOG.error("Exception", e);
                    bResult = false;
                } catch (JMSException e) {
                    LOG.error("Error publishing on url: " + queueURL + " with update event: " + event + " on queue: " + queueName);
                    LOG.error("Exception", e);
                    bResult = false;
                } catch (Exception e) {
                    LOG.error("Unable to publish on url: " + queueURL + " with update event: " + event + " on queue: " + queueName);
                    LOG.error("Exception", e);
                    bResult = false;
                }
                if (!bResult) {
                    LOG.error("stopping to publish events- last attempt failed for :" + publishList.size() + ", upto:" + endIndex + " total:" + totalMsgs);
                    break;
                }
                startIndex = endIndex;
            }
        }
        LOG.info("publishing to sdinstant queue done ... total event sent:{}, returning {}", count, bResult);
        return bResult;
    }

  
    public void setActiveMQManager(IActiveMQManager activeMQManager) {
        this.activeMQManager = activeMQManager;
    }

}
