//package com.snapdeal.cocofs.services.impl;
//
//import java.util.List;
//
//import org.springframework.stereotype.Service;
//
//import com.snapdeal.cocofs.entity.ProductAttribute;
//import com.snapdeal.cocofs.excelupload.ProductAttributeUploadDTO;
//import com.snapdeal.cocofs.mongo.model.ProductAttributeUnit;
//import com.snapdeal.cocofs.services.data.engine.IDataEngine;
//
//@Service("productAttributeCreateOnlyUploadMerger")
//public class ProductAttributeCreateOnlyMerger implements IDataEngine<ProductAttributeUploadDTO , ProductAttribute, ProductAttributeUnit>{
//
//    @Override
//    public List<ProductAttributeUnit> mergeDTOWithDocuments(ProductAttributeUploadDTO dto, List<ProductAttributeUnit> documents) {
//
//        return null;
//    }
//
//    @Override
//    public List<ProductAttribute> mergeDTOWithEntities(ProductAttributeUploadDTO dto, List<ProductAttribute> entities, String userEmail) {
//
//        return null;
//    }
//
//}
