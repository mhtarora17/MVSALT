package com.snapdeal.cocofs.services.taxclass.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.DataSource;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.services.taxclass.ITaxClassDataSourceService;

@Service("TaxClassDataSourceServiceFactory")
public class TaxClassDataSourceServiceFactory {

    @Autowired
    @Qualifier("TaxClassDataSourceServiceForCache")
    private ITaxClassDataSourceService taxClassDataSourceServiceForCache;

    @Autowired
    @Qualifier("TaxClassDataSourceServiceForAerospike")
    private ITaxClassDataSourceService taxClassDataSourceServiceForAerospike;

    @Autowired
    @Qualifier("TaxClassDataSourceServiceForMySql")
    private ITaxClassDataSourceService taxClassDataSourceServiceForMySql;

    /**
     * It returns concrete object of @ITaxClassDataSourceService, depending on the configured value of {@link
     * Property.DATA_SOURCE_FOR_TaxClass_READ.} Default value (@link TaxClassDataSourceServiceForAerospike)
     * 
     * @return {@link ITaxClassDataSourceService}
     */

    public ITaxClassDataSourceService getTaxClassDataSourceService() {
        DataSource dataSource = DataSource.valueOf(ConfigUtils.getStringScalar(Property.DATA_SOURCE_FOR_TAX_CLASS_READ).toUpperCase());
        ITaxClassDataSourceService taxClassdataSource = taxClassDataSourceServiceForAerospike;
        if (dataSource == DataSource.AEROSPIKE) {
            taxClassdataSource = taxClassDataSourceServiceForAerospike;
        } else if (dataSource == DataSource.MYSQL) {
            taxClassdataSource = taxClassDataSourceServiceForMySql;
        }
        return taxClassdataSource;
    }

    public ITaxClassDataSourceService getTaxClassServiceForSupcTaxClassLookup() {
        ITaxClassDataSourceService taxClassDataSourceService;
        if (ConfigUtils.getBooleanScalar(Property.ENABLE_SUPC_TAX_CLASS_MAPPING_CACHE)) {
            taxClassDataSourceService = getTaxClassDataSourceServiceForCache();
        } else {
            taxClassDataSourceService = getTaxClassDataSourceService();
        }
        return taxClassDataSourceService;
    }

    public ITaxClassDataSourceService getTaxClassServiceForSubcatTaxClassLookup() {
        ITaxClassDataSourceService taxClassDataSourceService;
        if (ConfigUtils.getBooleanScalar(Property.ENABLE_SUBCAT_TAX_CLASS_MAPPING_CACHE)) {
            taxClassDataSourceService = getTaxClassDataSourceServiceForCache();
        } else {
            taxClassDataSourceService = getTaxClassDataSourceService();
        }
        return taxClassDataSourceService;
    }

    public ITaxClassDataSourceService getTaxClassDataSourceServiceForCache() {
        return taxClassDataSourceServiceForCache;
    }

    public ITaxClassDataSourceService getTaxClassDataSourceServiceForAerospike() {
        return taxClassDataSourceServiceForAerospike;
    }

    public ITaxClassDataSourceService getTaxClassDataSourceServiceForMySql() {
        return taxClassDataSourceServiceForMySql;
    }
}
