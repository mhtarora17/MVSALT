/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 09-Oct-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.services.taxclass.impl;

import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.cocofs.entity.AbstractTaxClass;
import com.snapdeal.cocofs.entity.SubcatTaxClassMapping;
import com.snapdeal.cocofs.entity.SupcTaxClassMapping;
import com.snapdeal.cocofs.services.data.engine.IDataEngineWithAerospike;
import com.snapdeal.cocofs.services.taxclass.dto.SubcatTaxClassMappingDTO;
import com.snapdeal.cocofs.services.taxclass.dto.SupcTaxClassMappingDTO;

public abstract class AbstractTaxMappingDataEngine<T, E, R> implements IDataEngineWithAerospike<T, E, R> {

    public void enrichTaxClassMappingEntity(SupcTaxClassMappingDTO dto, SupcTaxClassMapping mapping, String userEmail) {
        mapping.setCreated(mapping.getCreated() != null ? mapping.getCreated() : DateUtils.getCurrentTime());
        mapping.setTaxClass(dto.getTaxClass());
        mapping.setEnabled(dto.isEnabled());
        mapping.setSupc(dto.getSupc());
        mapping.setUpdatedBy(userEmail);
        mapping.setLastUpdated(DateUtils.getCurrentTime());
    }

    public void enrichTaxClassMappingEntity(SubcatTaxClassMappingDTO dto, SubcatTaxClassMapping mapping, String userEmail) {
        mapping.setCreated(mapping.getCreated() != null ? mapping.getCreated() : DateUtils.getCurrentTime());
        mapping.setTaxClass(dto.getTaxClass());
        mapping.setEnabled(dto.isEnabled());
        mapping.setSubcat(dto.getSubcat());
        mapping.setUpdatedBy(userEmail);
        mapping.setLastUpdated(DateUtils.getCurrentTime());

    }

}
