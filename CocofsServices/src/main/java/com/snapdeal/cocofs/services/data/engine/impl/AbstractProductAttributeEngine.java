/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 09-Sep-2013
 *  @author himanshum
 */
package com.snapdeal.cocofs.services.data.engine.impl;

import java.lang.reflect.Field;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.cocofs.entity.ProductAttribute;
import com.snapdeal.cocofs.entity.ProductAttributeHistory;
import com.snapdeal.cocofs.excelupload.ProductAttributeUploadDTO;
import com.snapdeal.cocofs.mongo.model.ProductAttributeUnit;
import com.snapdeal.cocofs.services.data.engine.IDataEngine;

public abstract class AbstractProductAttributeEngine implements IDataEngine<ProductAttributeUploadDTO, ProductAttribute, ProductAttributeUnit> {

    protected static final Logger LOG = LoggerFactory.getLogger(AbstractProductAttributeEngine.class);

    protected ProductAttributeUnit createNewProductAttributeUnitDocument(ProductAttributeUploadDTO dto) {
        ProductAttributeUnit pau = new ProductAttributeUnit();
        pau.setBreadth(dto.getBreadth());
        pau.setFragile(dto.isFragile());
        pau.setHeight(dto.getHeight());
        pau.setLength(dto.getLength());
        pau.setPrimaryLength(dto.getPrimaryLength());
        pau.setPrimaryBreadth(dto.getPrimaryBreadth());
        pau.setPrimaryHeight(dto.getPrimaryHeight());
        pau.setProductParts(dto.getProductParts());
        //pau.setSerialized(dto.isSerialized());
        pau.setSupc(dto.getSupc());
        pau.setWeight(dto.getWeight());
        pau.setSystemWeightCaptured(dto.getSystemWeightCaptured());
        pau.setDangerousGoodsType(dto.getDangerousGoodsType());
        pau.setWoodenPackaging(dto.isWoodenPackaging());
        pau.setSerializedType(dto.getSerializedType());
        pau.setPackagingType(dto.getPackagingType());
        return pau;
    }

    protected void updateProductAttribute(String userEmail, ProductAttribute attr, String fieldValueAsString) {
        attr.getHistory().add(createPAHistory(attr.getSupc(), userEmail, attr.getAttribute(), fieldValueAsString, attr));
        attr.setValue(fieldValueAsString);
        attr.setLastUpdated(new Date());
        attr.setUpdatedBy(userEmail);
    }

    protected ProductAttribute createNewProductAttribute(ProductAttributeUploadDTO dto, String userEmail, String attributeName, String fieldValueAsString) {
        ProductAttribute newAttr = new ProductAttribute();
        newAttr.setAttribute(attributeName);
        newAttr.setCreated(new Date());
        newAttr.setCreatedBy(userEmail);
        newAttr.setEnabled(true);
        newAttr.setLastUpdated(newAttr.getCreated());
        newAttr.setSupc(dto.getSupc());
        newAttr.setUpdatedBy(userEmail);
        newAttr.setValue(fieldValueAsString);
        ProductAttributeHistory attrHistory = createNewPAHistory(dto, userEmail, attributeName, fieldValueAsString);
        newAttr.getHistory().add(attrHistory);
        return newAttr;
    }

    protected ProductAttributeHistory createNewPAHistory(ProductAttributeUploadDTO dto, String userEmail, String attributeName, String fieldValueAsString) {
        ProductAttributeHistory attrHistory = new ProductAttributeHistory();
        attrHistory.setAttribute(attributeName);
        attrHistory.setCreated(new Date());
        attrHistory.setCreatedBy(userEmail);
        attrHistory.setLastUpdated(attrHistory.getCreated());
        attrHistory.setNewValue(fieldValueAsString);
        attrHistory.setOldValue(null);
        attrHistory.setRemark("Creating attribute " + attributeName + " on upload request by " + userEmail);
        attrHistory.setSupc(dto.getSupc());
        attrHistory.setUpdatedBy(userEmail);
        return attrHistory;
    }

    protected ProductAttributeHistory createPAHistory(String supc, String userEmail, String attributeName, String fieldValueAsString, ProductAttribute attr) {
        ProductAttributeHistory attrHistory = new ProductAttributeHistory();
        attrHistory.setAttribute(attributeName);
        attrHistory.setCreated(new Date());
        attrHistory.setCreatedBy(userEmail);
        attrHistory.setLastUpdated(attrHistory.getCreated());
        attrHistory.setNewValue(fieldValueAsString);
        attrHistory.setOldValue(attr.getValue());
        attrHistory.setRemark("Updating attribute " + attributeName + " on upload request by " + userEmail);
        attrHistory.setSupc(supc);
        attrHistory.setUpdatedBy(userEmail);
        return attrHistory;
    }

    protected String getFieldValueAsString(Field fld, ProductAttributeUploadDTO dto) throws IllegalArgumentException, IllegalAccessException {
        fld.setAccessible(true);
        Object o = fld.get(dto);
        if (null == o) {
            LOG.info("Field value for field " + fld.getName() + " is null , supc " + dto.getSupc());
            return null;
        }
        if (o instanceof Date) {
            // Except for Dates we are okay with default toString for most objects
            return DateUtils.dateToString((Date) o, "YYYY-MM-dd HH:mm:ss");

        } else {
            return o.toString();
        }

    }

    protected ProductAttribute findProductAttributeForName(String attributeName, List<ProductAttribute> entities) {
        for (ProductAttribute attr : entities) {
            if (attr.getAttribute().equals(attributeName)) {
                return attr;
            }
        }
        return null;
    }

    protected String getNotNullFieldValueAsString(String fieldName, Field field, ProductAttributeUploadDTO dto) {

        if (StringUtils.isEmpty(fieldName)) {
            LOG.error("Could not find attribute correspoding to dto field " + field.getName());
            throw new IllegalArgumentException("Could not recognize the attribute :" + field.getName());
        }

        String fieldValueAsString = null;

        try {
            
            fieldValueAsString = getFieldValueAsString(field, dto);
        } catch (Exception e) {
            LOG.error("Could not obtain attribute value as string from dto field " + field.getName(), e);
            throw new IllegalArgumentException("Could not obtain attribute value as string for :" + field.getName());
        }

        if (null == fieldValueAsString) {
            LOG.error("Could not obtain attribute value as string from dto field " + field.getName());
            throw new IllegalArgumentException("Could not obtain attribute value as string for :" + field.getName());
        }

        return fieldValueAsString;
    }

    protected String getFieldAsString(String fieldName, Field field, ProductAttributeUploadDTO dto) {
        if (StringUtils.isEmpty(fieldName)) {
            LOG.error("Could not find attribute correspoding to dto field " + field.getName());
            throw new IllegalArgumentException("Could not recognize the attribute :" + field.getName());
        }

        String fieldValueAsString = null;

        try {
            fieldValueAsString = getFieldValueAsString(field, dto);
        } catch (Exception e) {
            LOG.error("Could not obtain attribute value as string from dto field " + field.getName(), e);
            throw new IllegalArgumentException("Could not obtain attribute value as string for :" + field.getName());
        }
        
        return fieldValueAsString;
    }
}
