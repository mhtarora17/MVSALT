/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.services;

import java.util.List;

import com.snapdeal.cocofs.common.RuleUpdateResponse;
import com.snapdeal.cocofs.giftwrap.dto.GiftWrapCategoryRuleEditForm;
import com.snapdeal.cocofs.giftwrap.dto.GiftWrapRuleUpdateDTO;
import com.snapdeal.rule.engine.entity.Rule;

/**
 *  
 *  @version     1.0, 21-Jul-2014
 *  @author ankur
 */
public interface IGiftWrapRuleWrapperService {
   
    public Rule saveServiceabilityRules(GiftWrapRuleUpdateDTO rules);

    GiftWrapCategoryRuleEditForm getGiftWrapRuleEditDTOFromRule(Rule rule);

    List<Rule> getGiftWrapRulesByCode(String ruleCode);

    RuleUpdateResponse upadteGiftWrapRules(GiftWrapRuleUpdateDTO dto);

}
