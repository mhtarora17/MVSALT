/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 07-Oct-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.services.taxclass.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.core.aerospike.vo.SubcatTaxClassMappingVO;
import com.snapdeal.cocofs.core.aerospike.vo.SupcTaxClassMappingVO;
import com.snapdeal.cocofs.dataaccess.taxclass.ITaxClassAerospikeDataReadService;
import com.snapdeal.cocofs.dataaccess.taxclass.ITaxClassDBDataReadService;
import com.snapdeal.cocofs.entity.SubcatTaxClassMapping;
import com.snapdeal.cocofs.services.IDataReaderWithAerospike;
import com.snapdeal.cocofs.services.taxclass.dto.SubcatTaxClassMappingDTO;

@Service("SubcatTaxClassMappingDataReader")
public class SubcatTaxClassMappingDataReader implements IDataReaderWithAerospike<SubcatTaxClassMappingDTO, SubcatTaxClassMapping, SubcatTaxClassMappingVO> {

    @Autowired
    private ITaxClassDBDataReadService        taxClassDBDataReadService;

    @Autowired
    private ITaxClassAerospikeDataReadService taxClassAerospikeReadService;

    @Override
    public List<SubcatTaxClassMapping> getEntitiesForDTO(SubcatTaxClassMappingDTO dto) {
        SubcatTaxClassMapping mapping = taxClassDBDataReadService.getSubcatTaxClassMapping(dto.getSubcat());
        List<SubcatTaxClassMapping> mappingList = new ArrayList<SubcatTaxClassMapping>();
        if (null != mapping) {
            mappingList.add(mapping);
        }
        return mappingList;
    }

    @Override
    public SubcatTaxClassMappingVO getRecordForDTO(SubcatTaxClassMappingDTO dto) {
        SubcatTaxClassMappingVO record = (SubcatTaxClassMappingVO) taxClassAerospikeReadService.getSubcatTaxClassMapping(dto.getSubcat(), SubcatTaxClassMappingVO.class);

        return record;
    }
}
