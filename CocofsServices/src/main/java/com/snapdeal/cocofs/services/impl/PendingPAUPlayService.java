package com.snapdeal.cocofs.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.access.IPendingProductAttributeDBDataReadService;
import com.snapdeal.cocofs.entity.PendingProductAttributeUpdate;
import com.snapdeal.cocofs.services.IPendingPAUDataUpdaterService;
import com.snapdeal.cocofs.services.IPendingPAUPlayService;

/**
 * @author shiv
 */
@Service("pendingPAUPlayService")
public class PendingPAUPlayService implements IPendingPAUPlayService {

    @Autowired
    private IPendingProductAttributeDBDataReadService pendingProductAttributeDBDataReadService;

    @Autowired
    private IPendingPAUDataUpdaterService             pendingPAUDataUpdaterService;

    private static final Logger                       LOG = LoggerFactory.getLogger(PendingPAUPlayService.class);

    public void promotePendingPAUs(Date startDate, Date endDate) {
        LOG.info("promotePAUs started for startDate:{} to endDate:{}", startDate, endDate);
        long t = System.currentTimeMillis();
        List<PendingProductAttributeUpdate> supcUpdates = pendingProductAttributeDBDataReadService.getAllPendingProductAttributes(startDate, endDate);
        LOG.info("promotePAUs - total records to process - {}", (supcUpdates == null ? 0 : supcUpdates.size()));
        Map<String, List<PendingProductAttributeUpdate>> supcToPendingupdates = groupBySupc(supcUpdates);
        int total = supcToPendingupdates == null ? 0 : supcToPendingupdates.size();
        LOG.info("promotePAUs - total supcs to process - {}", total);
        int row = 0;
        for (Entry<String, List<PendingProductAttributeUpdate>> entry : supcToPendingupdates.entrySet()) {
            row++;
            String supc = entry.getKey();
            long t2 = System.currentTimeMillis();
            LOG.info("promotePendingPAUs processing started for supc {} at index: {} of total: {};", entry.getValue(), row, total);
            pendingPAUDataUpdaterService.promotePendingPAU(entry.getValue(), supc);
            LOG.info("Finished promotePAUs task.. tooks " + (System.currentTimeMillis() - t2) + " ms.", supc);
        }
        LOG.info("Finished promotePendingPAUs .. tooks " + (System.currentTimeMillis() - t) + " ms.");
    }

    private Map<String, List<PendingProductAttributeUpdate>> groupBySupc(List<PendingProductAttributeUpdate> pendingUpdates) {

        Map<String, List<PendingProductAttributeUpdate>> supcToPendingUpdates = new HashMap<String, List<PendingProductAttributeUpdate>>();

        for (PendingProductAttributeUpdate update : pendingUpdates) {
            List<PendingProductAttributeUpdate> supcWiselist = null;
            if (!supcToPendingUpdates.containsKey(update.getSupc())) {
                supcWiselist = new ArrayList<PendingProductAttributeUpdate>();
                supcWiselist.add(update);
                supcToPendingUpdates.put(update.getSupc(), supcWiselist);
            } else {
                supcToPendingUpdates.get(update.getSupc()).add(update);
            }
        }

        return supcToPendingUpdates;
    }

}
