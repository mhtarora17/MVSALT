/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 04-Sep-2013
 *  @author himanshum
 */
package com.snapdeal.cocofs.services;

import java.util.List;

import com.snapdeal.cocofs.common.JobDetailDTO;
import com.snapdeal.cocofs.common.MyJobDetailDTO;
import com.snapdeal.cocofs.entity.JobDetail;

public interface IJobsConvertorService {

    /**
     * Job Detail DTO corresponding to JobDetails provided 
     * @param allJobDetails
     * @return
     */
    List<JobDetailDTO> getJobDetailDTOList(List<JobDetail> allJobDetails);

	List<MyJobDetailDTO> getMyJobDetailDTOList(List<JobDetail> allJobDetails);

}
