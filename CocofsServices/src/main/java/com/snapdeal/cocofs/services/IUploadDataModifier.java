package com.snapdeal.cocofs.services;

public interface IUploadDataModifier<T> {

    /**
     * @param t
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     */
    void modifyData(T t) throws IllegalArgumentException, IllegalAccessException;

}
