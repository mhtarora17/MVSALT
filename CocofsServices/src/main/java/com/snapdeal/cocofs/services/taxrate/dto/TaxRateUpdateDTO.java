/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.services.taxrate.dto;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @version 1.0, 13-Aug-2014
 * @author ankur
 */

@XmlRootElement
public class TaxRateUpdateDTO {

    private Double  taxRate;

    private String  taxType;
    
    private Boolean enabled;


    public TaxRateUpdateDTO() {
    }

    public Double getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(Double taxRate) {
        this.taxRate = taxRate;
    }


    public String getTaxType() {
        return taxType;
    }

    public void setTaxType(String taxType) {
        this.taxType = taxType;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public String toString() {
        return "TaxRateUpdateDTO [taxRate=" + taxRate + ", taxType=" + taxType + ", enabled=" + enabled + "]";
    }



}
