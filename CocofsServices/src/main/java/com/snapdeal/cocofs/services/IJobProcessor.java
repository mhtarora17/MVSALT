/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 04-Sep-2013
 *  @author himanshum
 */
package com.snapdeal.cocofs.services;

import java.io.File;

import com.snapdeal.cocofs.common.BulkUploadValidationResponse;
import com.snapdeal.cocofs.common.JobProcessorResponse;
import com.snapdeal.cocofs.entity.JobDetail;

public interface IJobProcessor {
    
   public JobProcessorResponse process(JobDetail jobDetail);
   
   public BulkUploadValidationResponse validateJob(File file, String actionCode, String param);


}
