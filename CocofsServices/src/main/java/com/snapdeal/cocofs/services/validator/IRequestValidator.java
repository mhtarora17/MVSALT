package com.snapdeal.cocofs.services.validator;

/**
 * A generic request validator
 * imposing server-side constraints on request parameters
 * @author gaurav
 *
 * @param <K>
 */
public interface IRequestValidator<K> {
	public boolean validate(K request);
}
