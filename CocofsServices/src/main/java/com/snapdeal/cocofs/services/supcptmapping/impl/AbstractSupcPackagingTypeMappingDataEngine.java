/**
 *  Copyright 2016 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.services.supcptmapping.impl;


import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.cocofs.entity.AbstractSupcPackagingTypeMapping;
import com.snapdeal.cocofs.excelupload.SupcPackagingTypeUploadDTO;
import com.snapdeal.cocofs.mongo.model.AbstractSupcPackagingTypeMappingUnit;
import com.snapdeal.cocofs.services.data.engine.IDataEngine;

/**
 *  
 *  @version     1.0, 04-Jan-2016
 *  @author vikassharma03
 */
public abstract class AbstractSupcPackagingTypeMappingDataEngine<T, E, D> implements IDataEngine<T, E, D>{

    
    public void enrichSupcPackagingTypeMappingUnit(SupcPackagingTypeUploadDTO dto, AbstractSupcPackagingTypeMappingUnit document) {
        document.setCreated(document.getCreated() != null ? document.getCreated() : DateUtils.getCurrentTime());
        document.setUpdated(DateUtils.getCurrentTime()); 
        document.setEnabled(dto.isEnable());

    }

    public void enrichSupcPackagingTypeMappingEntity(SupcPackagingTypeUploadDTO dto, AbstractSupcPackagingTypeMapping entity, String userEmail) {
        entity.setCreated(entity.getCreated() != null ? entity.getCreated() : DateUtils.getCurrentTime());
        entity.setLastUpdated(DateUtils.getCurrentTime());
        entity.setUpdatedBy(userEmail); 

    }

}
