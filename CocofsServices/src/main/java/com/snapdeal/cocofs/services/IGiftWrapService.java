/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.services;

import java.util.List;
import java.util.Map;

import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.external.service.exception.FulfillmentModelNotFoundException;
import com.snapdeal.cocofs.sro.GiftWrapProductSRO;
import com.snapdeal.cocofs.sro.GiftWrapperInfo;

/**
 *  
 *  @version     1.0, 17-Jul-2014
 *  @author ankur
 */
public interface IGiftWrapService {


    GiftWrapperInfo getGiftWrapInfoBySUPCSeller(String supc, String sellerCode, String subcat, Integer sellingPrice) throws ExternalDataNotFoundException,
            FulfillmentModelNotFoundException;

    Map<GiftWrapProductSRO, Boolean> getGiftWrapInfoBySUPCSellerList(List<GiftWrapProductSRO> giftwrapProducts);

}
