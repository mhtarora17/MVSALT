/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.services.data.engine;

import java.util.List;

import javax.naming.OperationNotSupportedException;

/**
 *  
 *  @version     1.0, 15-Sep-2014
 *  @author ankur
 */

/**
 * @param <T> DTO on which data engine will work
 * @param <E> Entity which will be enriched
 * @param <D> Document which will be enriched
 * @param <R> Aerospike Record which will be enriched
 */
public interface IDataEngineWithAerospike<T, E ,R> {

    
    /**
     * Update given entity list to correspond to dto T, and create new entities if required. The resulting update
     * documents list is returned.
     * 
     * @param dto
     * @param documents
     * @return
     * @throws OperationNotSupportedException
     */
    List<E> enrichEntities(T dto, List<E> entities, String userEmail) throws OperationNotSupportedException;

    /**
     * Update given Aerospike record list to correspond to dto T, and create new records if required. The resulting
     * update record list is returned.
     * toBeUpdatedFMEntityList
     * @param dto
     * @param documents
     * @return
     * @throws OperationNotSupportedException
     */
    List<R> enrichRecords(T dto, List<R> records, String userEmail) throws OperationNotSupportedException;

}
