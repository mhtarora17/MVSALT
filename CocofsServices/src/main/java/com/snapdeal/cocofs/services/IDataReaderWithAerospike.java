/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.services;

import java.util.List;

/**
 *  
 *  @version     1.0, 15-Sep-2014
 *  @author ankur
 */
public interface IDataReaderWithAerospike <T,E ,R> {
    

    /**
     * Returns list of db entities of Type E that correspond to the object 
     * dto of type T
     * @param dto
     * @return
     */
    List<E> getEntitiesForDTO(T dto);
    
    /**
     * Returns entity from  Aerospike of Type R that correspond to the 
     * object dto of Type T
     * @param dto
     * @return
     */
    R getRecordForDTO(T dto);
    
}
