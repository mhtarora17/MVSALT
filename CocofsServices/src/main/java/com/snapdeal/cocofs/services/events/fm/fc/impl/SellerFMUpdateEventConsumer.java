/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.services.events.fm.fc.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.services.events.dto.SellerFMFCUpdateEventObj;
import com.snapdeal.cocofs.services.events.impl.BaseEventConsumer;
import com.snapdeal.cocofs.services.events.impl.EventType;
import com.snapdeal.cocofs.services.fm.ISellerSUPCFMMappingProcessor;

/**
 * @version 1.0, 26-Jan-2015
 * @author shiv
 */
@Service("SellerFMUpdateEventConsumer")
public class SellerFMUpdateEventConsumer extends BaseEventConsumer<SellerFMFCUpdateEventObj> {

    @Autowired
    @Qualifier("sellerSUPCFMMappingProcessor")
    private ISellerSUPCFMMappingProcessor sellerSUPCFMMappingProcessor;

    private static final Logger           LOG = LoggerFactory.getLogger(SellerFMUpdateEventConsumer.class);

    @Override
    protected String getEventCode() {
        return EventType.FM_UPDATE_EVENT_ON_SELLER_FM_UPDATE.getCode();
    }

    @Override
    protected boolean processEventObj(SellerFMFCUpdateEventObj eventObject) {

        try {
            return sellerSUPCFMMappingProcessor.processChangeOfSellerFMMappingForExternalNotification(eventObject);
        } catch (Exception e) {
            LOG.error("Exception while processing the request for event: {}. Exception {}", eventObject, e);
        }

        return false;
    }
}
