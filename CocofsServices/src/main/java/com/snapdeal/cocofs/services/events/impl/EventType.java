/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.services.events.impl;

/**
 * @version 1.0, 28-Jan-2015
 * @author shiv
 */
public enum EventType {

    FM_UPDATE_EVENT_ON_SELLER_FM_UPDATE("FMUpdateEventOnSellerFMUpdate"), //  
    FM_UPDATE_EVENT_ON_SELLER_SUPC_ADD("FMUpdateEventOnSellerSupcAdd"), //  
    FM_UPDATE_EVENT_ON_SELLER_SUPC_FM_EXC_UPDATE("FMUpdateEventOnSellerSupcFMExcUpdate"), //  
    FM_UPDATE_EVENT_ON_SELLER_SUPC_FM_EXC_DELETE("FMUpdateEventOnSellerSupcFMExcDelete"), //  

    FC_UPDATE_EVENT_ON_SELLER_FM_UPDATE("FCUpdateEventOnSellerFMUpdate"), //  
    FC_UPDATE_EVENT_ON_SELLER_SUPC_ADD("FCUpdateEventOnSellerSupcAdd"), //  
    FC_UPDATE_EVENT_ON_SELLER_SUPC_FM_EXC_UPDATE("FCUpdateEventOnSellerSupcFMExcUpdate"), //  
    FC_UPDATE_EVENT_ON_SELLER_SUPC_FM_EXC_DELETE("FCUpdateEventOnSellerSupcFMExcDelete"), //  

    PRODUCT_ATTRIBUTE_UPDATE_SCORE_EVENT("ProductAttributeUpdateEvent"),
    
    SDINSTANT_UPDATE_EVENT_ON_SELLER_UPDATE("SdinstantUpdateEventOnSellerUpdate"),
    SDINSTANT_UPDATE_EVENT_ON_SELLER_SUPC_ADD("SdinstantUpdateEventOnSellerSupcAdd"), //  
    SDINSTANT_UPDATE_EVENT_ON_SELLER_SUPC_FM_EXC_UPDATE("SdinstantUpdateEventOnSellerSupcFMExcUpdate"), //  
    SDINSTANT_UPDATE_EVENT_ON_SELLER_SUPC_FM_EXC_DELETE("SdinstantUpdateEventOnSellerSupcFMExcDelete"), //  
    SDINSTANT_UPDATE_EVENT_ON_FC_UPDATE("SdinstantUpdateEventOnFCUpdate"),//
    
    SHIPFROM_UPDATE_EVENT_ON_SELLER_UPDATE("ShipFromUpdateEventOnSellerUpdate"),
    SHIPFROM_UPDATE_EVENT_ON_SELLER_SUPC_ADD("ShipFromUpdateEventOnSellerSupcAdd"), //  
    SHIPFROM_UPDATE_EVENT_ON_SELLER_SUPC_FM_EXC_UPDATE("ShipFromUpdateEventOnSellerSupcFMExcUpdate"), //  
    SHIPFROM_UPDATE_EVENT_ON_SELLER_SUPC_FM_EXC_DELETE("ShipFromUpdateEventOnSellerSupcFMExcDelete"), //  
    SHIPFROM_UPDATE_EVENT_ON_FC_ADDRESS_UPDATE("ShipFromUpdateEventOnFCAddressUpdate") ;//add future event-types here

    private String code;

    private EventType(String code) {
        this.code = code;
    }

    public String getCode() {
        return this.code;
    }

    public static EventType fromString(String eventCodeStr) {
        if (eventCodeStr != null) {
            for (EventType b : EventType.values()) {
                if (b.getCode().equalsIgnoreCase(eventCodeStr)) {
                    return b;
                }
            }
        }
        return null;
    }
}