package com.snapdeal.cocofs.services.healthcheck;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.snapdeal.fs.healthcheck.api.ConfigParams;
import com.snapdeal.fs.healthcheck.api.IConfiguration;
import com.snapdeal.score.data.aerospike.service.IScoreAerospikeService;

@Service("healthCheckConfiguration")
public class HealthCheckConfiguration implements IConfiguration {

    @Autowired
    @Qualifier("aerospikeService")
    IScoreAerospikeService scoreAerospikeService;

    @Autowired
    DataSource dataSource;

    @Override
    public <T> T get(String systemName, String paramName, Class<T> clazz) {

        //config for aerospike
        switch (paramName) {
            case ConfigParams.FS_AEROSPIKE_CLIENT:
                return (T) scoreAerospikeService;
            case ConfigParams.AEROSPIKE_NAMESPACE:
                return (T) "cocofs";
            case ConfigParams.DATABASE_DATASOURCE:
                return (T)dataSource;

        }
        return null;
    }

}
