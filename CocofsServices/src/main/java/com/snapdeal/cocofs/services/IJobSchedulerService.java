/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 04-Sep-2013
 *  @author himanshum
 */
package com.snapdeal.cocofs.services;

import java.io.IOException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.snapdeal.cocofs.common.JobValidationException;
import com.snapdeal.cocofs.entity.JobDetail;

public interface IJobSchedulerService {

    boolean stopDaemonProcess();

    boolean startDaemonProcess();

    void processJob(JobDetail jobDetail);

    boolean removeJobProcessor(String actionCode);

    boolean addJobProcessor(String actionCode, IJobProcessor jobProcessor);

    void startJob(JobDetail jobDetail);

    List<JobDetail> getAllJobDetailsToRun();

    void searchAndStartJobs(String requestedBy);

    JobDetail saveJob(MultipartFile multipartFile, String actionCode, String username) throws JobValidationException, IOException;

    JobDetail saveJob(MultipartFile multipartFile, String actionCode, String username, String param) throws JobValidationException, IOException;

}
