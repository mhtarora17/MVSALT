/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Oct 28, 2013
 *  @author himanshu
 */
package com.snapdeal.cocofs.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.cocofs.access.IProductAttributeService;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.entity.ProductAttribute;
import com.snapdeal.cocofs.mongo.model.ProductAttributeUnit;
import com.snapdeal.cocofs.services.IProductAttributeUnitResetService;

@Service("productAttributeUnitResetService")
public class ProductAttributeUnitResetService implements IProductAttributeUnitResetService {

    @Autowired
    private IProductAttributeService productAttributeService;

    @Override
    public ProductAttributeUnit resetAttributes(ProductAttributeUnit unit, List<ProductAttribute> list) {
        ProductAttributeUnit pau = productAttributeService.getProductAttributeUnitBySUPC(unit.getSupc());
        for (ProductAttribute pa : list) {
            updateForField(pa, pau, unit);
        }
        return unit;

    }

    private void updateForField(ProductAttribute pa, ProductAttributeUnit pau, ProductAttributeUnit unit) {
        if (pa.getAttribute().equals("SERIALIZED")) {
            unit.setSerialized(pau.isSerialized());
        } else if (pa.getAttribute().equals("HAZARDOUS")) {
            unit.setHazMat(pau.isHazMat());

        } else if (pa.getAttribute().equals("LIQUID")) {

            unit.setLiquid(pau.isLiquid());
        } else if (pa.getAttribute().equals("VOLWEIGHT")) {

            // No mongo attr for this
        } else if (pa.getAttribute().equals("WEIGHT")) {
            unit.setWeight(pau.getWeight());
            if (ConfigUtils
    				.getStringScalar(Property.SYSTEM_WEIGHT_CAPTURE_ENABLED) != null
    				&& ConfigUtils.getStringScalar(
    						Property.SYSTEM_WEIGHT_CAPTURE_ENABLED).equals("true")) {
            	unit.setSystemWeightCaptured(pau.isSystemWeightCaptured());
            }

        } else if (pa.getAttribute().equals("BREADTH")) {
            unit.setBreadth(pau.getBreadth());

        } else if (pa.getAttribute().equals("HEIGHT")) {
            unit.setHeight(pau.getHeight());

        } else if (pa.getAttribute().equals("LENGTH")) {
            unit.setLength(pau.getLength());

        }else if (pa.getAttribute().equals("PRIMARYBREADTH")) {
            unit.setBreadth(pau.getPrimaryBreadth());

        } else if (pa.getAttribute().equals("PRIMARYHEIGHT")) {
            unit.setHeight(pau.getPrimaryHeight());

        } else if (pa.getAttribute().equals("PRIMARYLENGTH")) {
            unit.setLength(pau.getPrimaryLength());

        } 
        else if (pa.getAttribute().equals("FRAGILE")) {
            unit.setFragile(pau.isFragile());

        } else if (pa.getAttribute().equals("PRODPARTS")) {

            unit.setProductParts(pau.getProductParts());
        }

    }
}
