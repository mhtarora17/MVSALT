/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.services.taxrate.dto;

import javax.xml.bind.annotation.XmlRootElement;

import com.snapdeal.cocofs.utils.SubClass;

/**
 * @version 1.0, 13-Aug-2014
 * @author ankur
 */

@XmlRootElement
@SubClass
public class SellerCategoryTaxRateUploadDTO extends TaxRateUpdateDTO {

    private String categoryUrl;

    private String sellerCode;

    public SellerCategoryTaxRateUploadDTO() {

    }

    public String getCategoryUrl() {
        return categoryUrl;
    }

    public void setCategoryUrl(String categoryUrl) {
        this.categoryUrl = categoryUrl;
    }

    public String getSellerCode() {
        return sellerCode;
    }

    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((categoryUrl == null) ? 0 : categoryUrl.hashCode());
        result = prime * result + ((sellerCode == null) ? 0 : sellerCode.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SellerCategoryTaxRateUploadDTO other = (SellerCategoryTaxRateUploadDTO) obj;
        if (categoryUrl == null) {
            if (other.categoryUrl != null)
                return false;
        } else if (!categoryUrl.equals(other.categoryUrl))
            return false;
        if (sellerCode == null) {
            if (other.sellerCode != null)
                return false;
        } else if (!sellerCode.equals(other.sellerCode))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "SellerCategoryTaxRateUploadDTO [categoryUrl=" + categoryUrl + ", sellerCode=" + sellerCode + ", toString()=" + super.toString() + "]";
    }

}
