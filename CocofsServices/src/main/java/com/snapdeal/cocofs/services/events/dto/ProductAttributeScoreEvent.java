/*
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 27-Jan-2015
 *  @author gaurav
 */
package com.snapdeal.cocofs.services.events.dto;

import java.io.Serializable;

import com.snapdeal.score.model.request.UpdateProductFulfilmentAttributeRequest;

public class ProductAttributeScoreEvent implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 9051731171749923634L;
	private UpdateProductFulfilmentAttributeRequest updateProductFulfilmentAttributeRequest;
	
	public ProductAttributeScoreEvent(){
	}
	
	public ProductAttributeScoreEvent(UpdateProductFulfilmentAttributeRequest updateProductFulfilmentAttributeRequest){
		this.updateProductFulfilmentAttributeRequest = updateProductFulfilmentAttributeRequest;
	}
	
	
	
	public UpdateProductFulfilmentAttributeRequest getUpdateProductFulfilmentAttributeRequest() {
        return updateProductFulfilmentAttributeRequest;
    }

    public void setUpdateProductFulfilmentAttributeRequest(UpdateProductFulfilmentAttributeRequest updateProductFulfilmentAttributeRequest) {
        this.updateProductFulfilmentAttributeRequest = updateProductFulfilmentAttributeRequest;
    }

    @Override
	public String toString() {
		return "ProductAttributeScoreEvent [updateProductFulfilmentAttributeRequest="
				+ updateProductFulfilmentAttributeRequest + "]";
	}
	
}
