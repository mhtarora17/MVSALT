/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 26-Nov-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.services.fm;

import java.util.List;

import com.snapdeal.cocofs.services.fm.dto.SellerDto;
import com.snapdeal.cocofs.services.fm.dto.SellerSupcFMMappingData;

public interface IFMLookupService {

    List<SellerDto> getSellersWithExceptionAtSupcLevel();

    List<SellerSupcFMMappingData> getSellerSupcFMMappingData(String sellerCode);

}
