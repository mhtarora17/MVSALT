/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.services.fm;

/**
 *  
 *  @version     1.0, 25-Mar-2015
 *  @author shiv
 */
public interface IManualPushOfSellerFMUpdateForExternalNotification {

    /**
     * API to process the sellercode, existingFulfilmentModel and find if any notification is to be send for the tuple.
     * This is to be used when we forcefully want to push messages to search, in case we missed earlier updates All such
     * notifications are sent further by this API.
     * 
     * @param sellerCode
     * @param newFulfilmentModel
     * @param existingFulfilmentModel
     * @return false if processing the change of seller-fm mapping was not successful, used to take further action at
     *         client such as inform user or possibly re-attempt by client.
     */
    public abstract boolean processManualPushOfExistingSellerFMMappingForExternalNotification(String sellerCode, String existingFulfilmentModel);

    /**
     * API to process the sellercode, SUPC code and FM mapping , existing FM and find if any notification is to be send
     * for the tuple. All such notifications are sent further by this API. This is to be used when we forcefully want to
     * push messages to search, in case we missed earlier updates
     * 
     * @param sellerCode
     * @param supc
     * @param newFM
     * @param existingFM
     * @return false if processing the change of seller-fm mapping was not successful, used to take further action at
     *         client such as inform user or possibly re-attempt by client.
     */
    public abstract boolean processExistingSellerSUPCFMMappingExceptionForExternalNotification(String sellerCode, String supc, String existingFM);

}