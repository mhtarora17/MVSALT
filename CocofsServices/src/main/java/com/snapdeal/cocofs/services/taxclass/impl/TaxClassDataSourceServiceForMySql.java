package com.snapdeal.cocofs.services.taxclass.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.cocofs.dataaccess.taxclass.ITaxClassDBDataReadService;
import com.snapdeal.cocofs.entity.SellerFMMapping;
import com.snapdeal.cocofs.entity.SubcatTaxClassMapping;
import com.snapdeal.cocofs.entity.SupcTaxClassMapping;
import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.external.service.factory.IExternalServiceFactory;
import com.snapdeal.cocofs.services.taxclass.ITaxClassDataSourceService;
import com.snapdeal.cocofs.services.taxclass.dto.SubcatTaxClassMappingDTO;
import com.snapdeal.cocofs.services.taxclass.dto.SupcTaxClassMappingDTO;

@Service("TaxClassDataSourceServiceForMySql")
public class TaxClassDataSourceServiceForMySql implements ITaxClassDataSourceService {

    @Autowired
    private ITaxClassDBDataReadService taxClassDBDataReadService;

    @Autowired
    private IExternalServiceFactory    externalServiceFactory;

    private static final Logger        LOG = LoggerFactory.getLogger(TaxClassDataSourceServiceForMySql.class);

    @Override
    public SupcTaxClassMappingDTO getTaxClassBySupc(String supc) throws ExternalDataNotFoundException {
        SupcTaxClassMappingDTO taxClass = null;
        SupcTaxClassMapping supcTaxClassMapping = getSupcTaxClass(supc);
        if (supcTaxClassMapping != null) {
            taxClass = new SupcTaxClassMappingDTO(supcTaxClassMapping);
        }
        LOG.info("Returning taxClass {} for supc {} from Mysql", taxClass, supc);
        return taxClass;
    }

    @Override
    public SubcatTaxClassMappingDTO getTaxClassBySubcat(String subcat) throws ExternalDataNotFoundException {
        SubcatTaxClassMappingDTO taxClass = null;
        SubcatTaxClassMapping subcatTaxClassMapping = getSubcatTaxClass(subcat);
        if (subcatTaxClassMapping != null) {
            taxClass = new SubcatTaxClassMappingDTO(subcatTaxClassMapping);
        }
        LOG.debug("Returning taxClass {} for subcat {} from Mysql", taxClass, subcat);
        return taxClass;
    }

    /////////////////////////////////
    //////// Private Methods ////////
    /////////////////////////////////

    private SubcatTaxClassMapping getSubcatTaxClass(String subcat) throws ExternalDataNotFoundException {
        if (StringUtils.isEmpty(subcat)) {
            LOG.warn("Illegal paramters passed while trying to find tax class at subcat level");
            throw new ExternalDataNotFoundException("Required Parameters not there while getting tax class At subcat Level");
        }

        LOG.debug("Going to read fulfilment model from MySql for subcat {}", subcat);
        SubcatTaxClassMapping subcatTaxClassMapping = (SubcatTaxClassMapping) taxClassDBDataReadService.getSubcatTaxClassMapping(subcat);
        LOG.debug("Tax Class obtained from MySql:" + subcatTaxClassMapping);
        return subcatTaxClassMapping;
    }

    private SupcTaxClassMapping getSupcTaxClass(String supc) throws ExternalDataNotFoundException {
        if (StringUtils.isEmpty(supc)) {
            LOG.warn("Illegal paramters passed while trying to find tax class at subcat level");
            throw new ExternalDataNotFoundException("Required Parameters not there while getting tax class At subcat Level");
        }

        LOG.debug("Going to read fulfilment model from MySql for subcat {}", supc);
        SupcTaxClassMapping supcTaxClassMapping = (SupcTaxClassMapping) taxClassDBDataReadService.getSupcTaxClassMapping(supc);
        LOG.debug("Fulfilment Model obtained from MySql:" + supcTaxClassMapping);
        return supcTaxClassMapping;
    }

}
