package com.snapdeal.cocofs.services.converter.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;

import org.springframework.stereotype.Service;

import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.cocofs.core.aerospike.vo.FCDetailVO;
import com.snapdeal.cocofs.core.aerospike.vo.SellerDetailVO;
import com.snapdeal.cocofs.core.aerospike.vo.SellerZoneMappingVO;
import com.snapdeal.cocofs.entity.EventInstance;
import com.snapdeal.cocofs.entity.FulfillmentCentre;
import com.snapdeal.cocofs.entity.PendingProductAttributeUpdate;
import com.snapdeal.cocofs.entity.SellerCategoryTaxRate;
import com.snapdeal.cocofs.entity.SellerContactDetails;
import com.snapdeal.cocofs.entity.SellerSupcTaxRate;
import com.snapdeal.cocofs.entity.SellerZoneMapping;
import com.snapdeal.cocofs.entity.StateCategoryPriceTaxRate;
import com.snapdeal.cocofs.entity.SubcatTaxClassMapping;
import com.snapdeal.cocofs.entity.SupcTaxClassMapping;
import com.snapdeal.cocofs.excelupload.ProductAttributeUploadDTO;
import com.snapdeal.cocofs.mongo.model.ProductAttributeUnit;
import com.snapdeal.cocofs.services.common.dto.ProductAttributeDownloadData;
import com.snapdeal.cocofs.services.converter.IConverterService;
import com.snapdeal.cocofs.services.events.dto.EventDetailDTO;
import com.snapdeal.cocofs.services.fm.dto.AddressDTO;
import com.snapdeal.cocofs.services.taxclass.dto.SubcatTaxClassMappingDTO;
import com.snapdeal.cocofs.services.taxclass.dto.SupcTaxClassMappingDTO;
import com.snapdeal.cocofs.services.taxrate.dto.SellerCategoryTaxRateUploadDTO;
import com.snapdeal.cocofs.services.taxrate.dto.SellerSUPCTaxRateUploadDTO;
import com.snapdeal.cocofs.services.taxrate.dto.StateCategoryPriceTaxRateUploadDTO;
import com.snapdeal.cocofs.sro.AddressSRO;
import com.snapdeal.cocofs.sro.ProductFulfilmentAttributeSRO;
import com.snapdeal.cocofs.sro.SellerContactDetailSRO;
import com.snapdeal.cocofs.utils.DangerousGoodsTypeConversionUtil;
import com.snapdeal.score.model.request.UpdateProductFulfilmentAttributeRequest;
import com.snapdeal.score.sro.UpdateProductFulfilmentAttributeSRO;

@Service("converterServiceImpl")
public class ConverterServiceImpl implements IConverterService {

    final String SEPARATOR_COMMA = ",";

    @Override
    public ProductAttributeDownloadData getProductAttributeDownloadData(List<PendingProductAttributeUpdate> pendingUpdates, ProductAttributeUnit unit) {
        ProductAttributeDownloadData data = new ProductAttributeDownloadData();

        if (unit != null) {
            data.setSupc(unit.getSupc());
            data.setBreadth(Double.valueOf(unit.getBreadth()));
            data.setLength(Double.valueOf(unit.getLength()));
            data.setHeight(Double.valueOf(unit.getHeight()));
            data.setWeight(Double.valueOf(unit.getWeight()));
            data.setFragile(unit.isFragile());
            data.setSerialized(unit.isSerialized());
            data.setProductParts(unit.getProductParts());
            data.setDangerousGoodsType(unit.getDangerousGoodsType());
            data.setWoodenPackaging(unit.isWoodenPackaging());
            data.setSerializedType(unit.getSerializedType());
            data.setPackagingType(unit.getPackagingType());
            data.setPrimaryLength(unit.getPrimaryLength());
            data.setPrimaryBreadth(unit.getPrimaryBreadth());
            data.setPrimaryHeight(unit.getPrimaryHeight());
        }

        if (pendingUpdates != null && pendingUpdates.size() > 0) {
            for (PendingProductAttributeUpdate update : pendingUpdates) {
                if (update.getAttributeName().equalsIgnoreCase(PendingProductAttributeUpdate.PendingAttributes.length.getCode())) {
                    data.setPendingLength(Double.valueOf(update.getNewValue()));
                    data.setWefPendingLength(update.getPlayDate());
                } else if (update.getAttributeName().equalsIgnoreCase(PendingProductAttributeUpdate.PendingAttributes.breadth.getCode())) {
                    data.setPendingBreadth(Double.valueOf(update.getNewValue()));
                    data.setWefPendingBreadth(update.getPlayDate());
                } else if (update.getAttributeName().equalsIgnoreCase(PendingProductAttributeUpdate.PendingAttributes.height.getCode())) {
                    data.setPendingHeight(Double.valueOf(update.getNewValue()));
                    data.setWefPendingHeight(update.getPlayDate());
                } else if (update.getAttributeName().equalsIgnoreCase(PendingProductAttributeUpdate.PendingAttributes.weight.getCode())) {
                    data.setPendingWeight(Double.valueOf(update.getNewValue()));
                    data.setWefPendingWeight(update.getPlayDate());
                }
            }
        }
        return data;
    }

    @Override
    public StateCategoryPriceTaxRateUploadDTO getStateCategoryPriceTaxRateUploadDTO(StateCategoryPriceTaxRate entity) {
        if (entity == null) {
            return null;
        }
        StateCategoryPriceTaxRateUploadDTO dto = new StateCategoryPriceTaxRateUploadDTO();
        dto.setTaxRate(entity.getTaxRate());
        dto.setEnabled(entity.isEnabled());
        dto.setTaxType(entity.getTaxType());
        dto.setSubCategoryUrl(entity.getCategoryUrl());
        dto.setLowerPriceLimit(entity.getLowerPriceLimit());
        dto.setUpperPriceLimit(entity.getUpperPriceLimit());
        dto.setState(entity.getState());
        return dto;
    }

    @Override
    public SellerCategoryTaxRateUploadDTO getSellerCategoryTaxRateUploadDTO(SellerCategoryTaxRate entity) {
        if (entity == null) {
            return null;
        }
        SellerCategoryTaxRateUploadDTO dto = new SellerCategoryTaxRateUploadDTO();
        dto.setTaxRate(entity.getTaxRate());
        dto.setEnabled(entity.isEnabled());
        dto.setTaxType(entity.getTaxType());
        dto.setCategoryUrl(entity.getCategoryUrl());
        dto.setSellerCode(entity.getSellerCode());
        dto.setEnabled(entity.isEnabled());
        return dto;
    }

    @Override
    public SellerSUPCTaxRateUploadDTO getSellerSUPCTaxRateUploadDTO(SellerSupcTaxRate entity) {
        if (entity == null) {
            return null;
        }
        SellerSUPCTaxRateUploadDTO dto = new SellerSUPCTaxRateUploadDTO();
        dto.setTaxRate(entity.getTaxRate());
        dto.setTaxType(entity.getTaxType());
        dto.setEnabled(entity.isEnabled());
        dto.setSupc(entity.getSupc());
        dto.setSellerCode(entity.getSellerCode());
        dto.setEnabled(entity.isEnabled());
        return dto;
    }

    /**
     * Will be used to convert entity Event Instance to its corresponding DTO.
     * 
     * @param instance
     * @return
     */
    @Override
    public EventDetailDTO getEventDetailDTO(EventInstance instance) {
        EventDetailDTO dto = new EventDetailDTO();
        if (instance != null) {
            dto.setId(instance.getId());
            dto.setEventType(instance.getEventType());
            dto.setParam1(instance.getParam1());
            dto.setParam2(instance.getParam2());
            dto.setEnabled(instance.isEnabled());
            dto.setExecuted(instance.isExecuted());
            dto.setJsonArguments(instance.getJsonArguments());
            dto.setRetryCount(instance.getRetryCount());
            dto.setCreated(instance.getCreated());
            dto.setUpdated(instance.getUpdated());
        }
        return dto;
    }

    @Override
    public ProductAttributeUploadDTO getProductAttributeUploadDTO(ProductAttributeUnit pau) {
        if (pau != null) {
            ProductAttributeUploadDTO paud = new ProductAttributeUploadDTO();
            paud.setBreadth(pau.getBreadth());
            paud.setDangerousGoodsType(pau.getDangerousGoodsType());
            paud.setFragile(pau.isFragile());
            paud.setHeight(pau.getHeight());
            paud.setLength(pau.getLength());
            paud.setPrimaryBreadth(pau.getPrimaryBreadth());
            paud.setPrimaryHeight(pau.getPrimaryHeight());
            paud.setPrimaryLength(pau.getPrimaryLength());
            paud.setProductParts(pau.getProductParts());
            //paud.setSerialized(pau.isSerialized());
            paud.setSupc(pau.getSupc());
            paud.setSystemWeightCaptured(pau.isSystemWeightCaptured());
            paud.setWeight(pau.getWeight());
            paud.setWoodenPackaging(pau.isWoodenPackaging());
            paud.setSerializedType(pau.getSerializedType());
            return paud;
        } else
            return null;
    }

    @Override
    public UpdateProductFulfilmentAttributeRequest getUpdateProductAttributesSRORequest(ProductAttributeUnit pau) {
        if (pau != null) {
            UpdateProductFulfilmentAttributeRequest request = new UpdateProductFulfilmentAttributeRequest();
            UpdateProductFulfilmentAttributeSRO sro = new UpdateProductFulfilmentAttributeSRO();
            sro.setBreadth(pau.getBreadth());
            sro.setDangerousGoodsType(pau.getDangerousGoodsType());
            sro.setFragile(pau.isFragile());
            sro.setHeight(pau.getHeight());
            sro.setLength(pau.getLength());
            sro.setProductParts(pau.getProductParts());
            sro.setSerialized(pau.isSerialized());
            sro.setSupc(pau.getSupc());
            sro.setSystemWeightCaptured(pau.isSystemWeightCaptured());
            sro.setWeight(pau.getWeight());
            sro.setWoodenPackaging(pau.isWoodenPackaging());
            request.setRequestList(Arrays.asList(sro));
            return request;
        }
        return null;
    }

    @Override
    public List<String> convertToFCCentreList(String fcCenterString) {

        //only use comma as separator and trim result
        List<String> fcCenterList = null;
        if (StringUtils.isNotEmpty(fcCenterString)) {
            fcCenterList = new ArrayList<String>();
            StringTokenizer t = new StringTokenizer(fcCenterString, SEPARATOR_COMMA);
            while (t.hasMoreTokens()) {
                String item = t.nextToken().trim();
                if (StringUtils.isNotEmpty(item)) {
                    fcCenterList.add(item);
                }
            }
        }

        return fcCenterList;
    }

    @Override
    public String convertFCCentreListToString(List<String> fcCenterList) {
        if (fcCenterList == null || fcCenterList.isEmpty()) {
            return null;
        }
        String sResult = "";

        for (String fc : fcCenterList) {
            sResult += fc;
            sResult += SEPARATOR_COMMA;
        }

        sResult = sResult.substring(0, sResult.lastIndexOf(SEPARATOR_COMMA));
        return sResult;
    }

    @Override
    public SellerContactDetails getSellerContactDetailsFromSRO(SellerContactDetailSRO sro, String sellerCode, String updatedBy) {
        SellerContactDetails scd = new SellerContactDetails();
        scd.setAddressLine1(sro.getAddressLine1());
        scd.setAddressLine2(sro.getAddressLine2());
        scd.setCity(sro.getCity());
        scd.setPinCode(sro.getPincode());
        scd.setState(sro.getState());
        scd.setSellerCode(sellerCode);
        scd.setUpdatedBy(updatedBy);
        return scd;
    }

    @Override
    public SellerZoneMapping getSellerZoneMappingFromDetails(SellerContactDetails scd, String zone) {
        SellerZoneMapping szm = new SellerZoneMapping();
        szm.setSellerCode(scd.getSellerCode());
        szm.setZone(zone);
        szm.setUpdatedBy(scd.getUpdatedBy());
        return szm;
    }

    @Override
    public SellerZoneMappingVO getSellerZoneMappingVoFromEntity(SellerZoneMapping szm) {
        SellerZoneMappingVO szmv = new SellerZoneMappingVO();
        szmv.setSellerCode(szm.getSellerCode());
        szmv.setZone(szm.getZone());
        return szmv;
    }

    @Override
    public SellerDetailVO getSellerDetailVoFromEntity(SellerContactDetails scd) {
        SellerDetailVO sdv = new SellerDetailVO();
        sdv.setSellerCode(scd.getSellerCode());
        sdv.setAddressLine1(scd.getAddressLine1());
        sdv.setAddressLine2(scd.getAddressLine2());
        sdv.setCity(scd.getCity());
        sdv.setState(scd.getState());
        sdv.setPincode(scd.getPinCode());
        return sdv;
    }

    public static ProductFulfilmentAttributeSRO getProductAttributeSro(ProductAttributeUnit paUnit) {
        ProductFulfilmentAttributeSRO sro = null;
        if (paUnit != null) {
            sro = new ProductFulfilmentAttributeSRO();
            sro.setBreadth(paUnit.getBreadth());
            sro.setPrimaryBreadth(paUnit.getPrimaryBreadth());
            sro.setFragile(paUnit.isFragile());
            if (StringUtils.isEmpty(paUnit.getDangerousGoodsType())) {
                sro.setHazMat(paUnit.isHazMat());
                sro.setLiquid(paUnit.isLiquid());
            } else {
                sro.setHazMat(DangerousGoodsTypeConversionUtil.checkIfHazmat(paUnit.getDangerousGoodsType()));
                sro.setLiquid(DangerousGoodsTypeConversionUtil.checkIfLiquid(paUnit.getDangerousGoodsType()));
            }
            sro.setHeight(paUnit.getHeight());
            sro.setPrimaryHeight(paUnit.getPrimaryHeight());
            sro.setLength(paUnit.getLength());
            sro.setPrimaryLength(paUnit.getPrimaryLength());
            sro.setProductParts(paUnit.getProductParts());
            sro.setSerialized(paUnit.isSerialized());
            sro.setSupc(paUnit.getSupc());
            sro.setWeight(paUnit.getWeight());
            sro.setSystemWeightCaptured(paUnit.isSystemWeightCaptured());
            sro.setWoodenPackaging(paUnit.isWoodenPackaging());
            sro.setDangerousGoodsType(DangerousGoodsTypeConversionUtil.getDangerousGoodsType(paUnit.getDangerousGoodsType(), paUnit.isHazMat(), paUnit.isLiquid()));
            sro.setSerializedType(paUnit.getSerializedType());
            sro.setPackagingType(paUnit.getPackagingType());
        }

        return sro;
    }

    @Override
    public AddressDTO getAddressDTO(SellerDetailVO vo) {
        AddressDTO add = new AddressDTO();
        add.setCity(vo.getCity());
        add.setState(vo.getState());
        add.setCountry(vo.getCountry());
        add.setPincode(vo.getPincode());
        add.setAddressLine1(vo.getAddressLine1());
        add.setAddressLine2(vo.getAddressLine2());

        return add;

    }

    @Override
    public AddressDTO getAddressDTO(SellerContactDetails entity) {
        AddressDTO add = new AddressDTO();
        add.setCity(entity.getCity());
        add.setState(entity.getState());
        add.setPincode(entity.getPinCode());
        add.setCountry(null);
        add.setAddressLine1(entity.getAddressLine1());
        add.setAddressLine2(entity.getAddressLine2());

        return add;

    }

    @Override
    public AddressSRO getAddressSROfromDTO(AddressDTO dto) {
        AddressSRO add = new AddressSRO();
        add.setCity(dto.getCity());
        add.setState(dto.getState());
        add.setPincode(dto.getPincode());
        add.setCountry(dto.getCountry());
        add.setAddressLine1(dto.getAddressLine1());
        add.setAddressLine2(dto.getAddressLine2());

        return add;

    }

    @Override
    public SupcTaxClassMappingDTO getSupcTaxClassMappingUploadDTO(SupcTaxClassMapping entity) {
        if (entity == null) {
            return null;
        }
        SupcTaxClassMappingDTO dto = new SupcTaxClassMappingDTO();
        dto.setSupc(entity.getSupc());
        dto.setTaxClass(entity.getTaxClass());
        dto.setEnabled(entity.isEnabled());
        return dto;
    }

    @Override
    public SubcatTaxClassMappingDTO getSubcatTaxClassMappingUploadDTO(SubcatTaxClassMapping entity) {
        if (entity == null) {
            return null;
        }
        SubcatTaxClassMappingDTO dto = new SubcatTaxClassMappingDTO();
        dto.setSubcat(entity.getSubcat());
        dto.setTaxClass(entity.getTaxClass());
        dto.setEnabled(entity.isEnabled());
        return dto;
    }
    
    @Override
    public FCDetailVO getFCDetailVoFromEntity(FulfillmentCentre fc) {
        FCDetailVO vo = new FCDetailVO();
        vo.setCode(fc.getCode());
        vo.setFcName(fc.getName());
        vo.setType(fc.getType());
        vo.setSdinstant( fc.getSdinstant() ? 1: 0);
        vo.setEnabled( fc.getEnabled() ? 1 : 0);
        
        vo.setAddrName(fc.getFcAddress().getName());
        vo.setAddressLine1(fc.getFcAddress().getAddressLine1());
        vo.setAddressLine2(fc.getFcAddress().getAddressLine2());
        vo.setPincode(fc.getFcAddress().getPincode());
        vo.setState(fc.getFcAddress().getState());;
        vo.setCity(fc.getFcAddress().getCity());
        vo.setEmail(fc.getFcAddress().getEmail());
        vo.setLandLine(fc.getFcAddress().getLandLine());
        vo.setMobile(fc.getFcAddress().getMobile());
        

        return vo;
    }

}
