/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 01-Oct-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.services.fm.dto;

import javax.xml.bind.annotation.XmlRootElement;

import com.snapdeal.cocofs.annotation.IgnoreInDownloadTemplate;
import com.snapdeal.cocofs.utils.SubClass;

@XmlRootElement
@SubClass
public class SellerSupcFMMappingUploadDto extends SellerFMMappingUpdateDto {
    //ensure that this class is in sync with com.snapdeal.cocofs.adminweb.controller.FMMappingController.TempSellerFMFCMappingUpdateDto

    private String supc;

    @IgnoreInDownloadTemplate
    private boolean enabled = true;
    
    public SellerSupcFMMappingUploadDto() {
    }

    public SellerSupcFMMappingUploadDto(String sellerCode, String supc, String fulfilmentModel, String fcCentres) {
        super(sellerCode, fulfilmentModel, fcCentres);
        this.supc = supc;
    }

    public String getSupc() {
        return supc;
    }

    public void setSupc(String supc) {
        this.supc = supc;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public String toString() {
        return "SellerSupcFMMappingUploadDto [supc=" + supc + ", getSellerCode()=" + getSellerCode() + ", getFulfilmentModel()=" + getFulfilmentModel() + ", getFcCenters()="
                + getFcCenters() + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.getSellerCode() == null) ? 0 : this.getSellerCode().hashCode());
        result = prime * result + ((supc == null) ? 0 : supc.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SellerSupcFMMappingUploadDto other = (SellerSupcFMMappingUploadDto) obj;
        if (this.getSellerCode() == null) {
            if (other.getSellerCode() != null)
                return false;
        } else if (!this.getSellerCode().equals(other.getSellerCode()))
            return false;
        if (supc == null) {
            if (other.supc != null)
                return false;
        } else if (!supc.equals(other.supc))
            return false;
        return true;
    }

}
