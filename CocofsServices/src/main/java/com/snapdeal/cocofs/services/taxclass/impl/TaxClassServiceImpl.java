package com.snapdeal.cocofs.services.taxclass.impl;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.base.exception.TransportException;
import com.snapdeal.catalog.base.model.GetMinProductInfoBySupcRequest;
import com.snapdeal.catalog.base.model.GetMinProductInfoResponse;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.dataaccess.taxclass.ITaxClassAerospikeDataReadService;
import com.snapdeal.cocofs.dataaccess.taxclass.ITaxClassDBDataReadService;
import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.external.service.factory.IExternalServiceFactory;
import com.snapdeal.cocofs.services.taxclass.ITaxClassService;
import com.snapdeal.cocofs.services.taxclass.dto.SubcatTaxClassMappingDTO;
import com.snapdeal.cocofs.services.taxclass.dto.SupcTaxClassMappingDTO;
import com.snapdeal.product.client.service.IProductClientService;

/**
 * Cocofs tax class landing internal service implementation
 * 
 * @author nitish
 */
@Service("TaxClassServiceImpl")
public class TaxClassServiceImpl implements ITaxClassService {

    @Autowired
    IProductClientService                     productClientService;

    @Autowired
    private ITaxClassDBDataReadService        taxClassDBDataReadService;

    @Autowired
    private ITaxClassAerospikeDataReadService taxClassAerospikeDataReadService;

    @Autowired
    private IExternalServiceFactory           externalServiceFactory;

    @Autowired
    private TaxClassDataSourceServiceFactory  dataSourceService;

    private static final Logger               LOG = LoggerFactory.getLogger(TaxClassServiceImpl.class);

    @Override
    public String getTaxClassBySupcSubcatPair(String supc, String subcat) throws ExternalDataNotFoundException {

        SupcTaxClassMappingDTO supcTaxClassMappingDTO = null;
        SubcatTaxClassMappingDTO subcatTaxClassMappingDTO = null;

        String taxClass = null;

        try {
            supcTaxClassMappingDTO = getTaxClassBySupc(supc);
        } catch (Exception e) {
            LOG.error(e.getMessage());
        }
        if (supcTaxClassMappingDTO != null && supcTaxClassMappingDTO.isEnabled()) {
            taxClass = supcTaxClassMappingDTO.getTaxClass();
            return taxClass;
        }

        GetMinProductInfoBySupcRequest request = new GetMinProductInfoBySupcRequest();
        request.setSupc(supc);

        if (subcat == null || StringUtils.isEmpty(subcat)) {
            GetMinProductInfoResponse resp = null;
            try {
                resp = productClientService.getMinProductInfoBySupc(request);
            } catch (TransportException e) {
                LOG.error("Exception in getting subcat from CAMS  with exception ", e.getMessage());
            }
            if (resp.isSuccessful() && resp.getProductInfo() != null && resp.getProductInfo().getCategoryUrl() != null) {
                subcat = resp.getProductInfo().getCategoryUrl();
                LOG.debug("Subcat found SUCCESSFULLY from CAMS" + subcat);

            } else {
                LOG.warn("Not able to get Subcat from CAMS response:" + resp);
            }
        }

        if (subcat != null && !StringUtils.isEmpty(subcat)) {
            LOG.debug("Getting tax class for subcat {} from data source ", subcat);
            try {
                subcatTaxClassMappingDTO = getTaxClassBySubcat(subcat);
            } catch (Exception e) {
                LOG.error(e.getMessage());
            }
            if (subcatTaxClassMappingDTO != null && subcatTaxClassMappingDTO.isEnabled()) {
                taxClass = subcatTaxClassMappingDTO.getTaxClass();
                return taxClass;
            }
        }

        LOG.error("Not able to find Tax class by supc and subcat. Returning tax class DEFAULT.");
        return ConfigUtils.getStringScalar(Property.DEFAULT_SUBCAT_TAX_MAPPING);
    }

    public SubcatTaxClassMappingDTO getTaxClassBySubcat(String subcat) throws ExternalDataNotFoundException {
        return dataSourceService.getTaxClassServiceForSubcatTaxClassLookup().getTaxClassBySubcat(subcat);
    }

    public SupcTaxClassMappingDTO getTaxClassBySupc(String supc) throws ExternalDataNotFoundException {
        return dataSourceService.getTaxClassServiceForSupcTaxClassLookup().getTaxClassBySupc(supc);
    }

    public SubcatTaxClassMappingDTO getTaxClassBySubcatFromDataSource(String subcat) throws ExternalDataNotFoundException {
        return dataSourceService.getTaxClassDataSourceService().getTaxClassBySubcat(subcat);
    }

    public SupcTaxClassMappingDTO getTaxClassBySupcFromDataSource(String supc) throws ExternalDataNotFoundException {
        return dataSourceService.getTaxClassDataSourceService().getTaxClassBySupc(supc);
    }

}
