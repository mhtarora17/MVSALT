package com.snapdeal.cocofs.services;

import com.snapdeal.cocofs.services.common.dto.ProductAttributeDownloadData;

public interface IProductAttributeLookupService {

    public ProductAttributeDownloadData getProductAttributeDownloadData(String supc);

}
