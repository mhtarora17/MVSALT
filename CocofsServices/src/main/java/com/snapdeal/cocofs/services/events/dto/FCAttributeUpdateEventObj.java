/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.services.events.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * @version 1.0, 25-Mar-2015
 * @author shiv
 */
public class FCAttributeUpdateEventObj implements Serializable {
   
    /**
     * 
     */
    private static final long serialVersionUID = 4177728312123452003L;
    private String            fcCode;
    private boolean           sdInstant;
    private Date              created;

    public FCAttributeUpdateEventObj() {
    }

    
    public FCAttributeUpdateEventObj(String fcCode, boolean sdInstant) {
        super();
        this.fcCode = fcCode;
        this.sdInstant = sdInstant;
        this.created = new Date();
    }


    public String getFcCode() {
        return fcCode;
    }

    public void setFcCode(String fcCode) {
        this.fcCode = fcCode;
    }

    

    public boolean isSdInstant() {
        return sdInstant;
    }


    public void setSdInstant(boolean sdInstant) {
        this.sdInstant = sdInstant;
    }


    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }


    @Override
    public String toString() {
        return "FCAttributeUpdateEventObj [fcCode=" + fcCode + ", sdInstant=" + sdInstant + ", created=" + created + "]";
    }


    
}
