/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 02-Sep-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.services.task.alwaysrunning.impl;

import com.snapdeal.cocofs.services.task.alwaysrunning.IAlwaysRunningTask;

public abstract class AbstractAlwaysRunningTask implements IAlwaysRunningTask{
    
    @Override
    protected void finalize() throws Throwable {
        stopProcess();
    }

    @Override
    public void destroy() throws Exception {
        stopProcess();
    }

}
