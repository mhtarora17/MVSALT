/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.services.producer;

/**
 *  
 *  @version     1.0, 22-Mar-2015
 *  @author shiv
 */
public interface IQueueListener {

    public abstract void unregisterProducer();

    public abstract void registerProducer(String queueName, String networkPath, String username, String password);

}