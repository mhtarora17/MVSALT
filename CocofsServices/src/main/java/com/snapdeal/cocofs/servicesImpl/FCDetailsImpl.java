/*
 *  Copyright 2012 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *  @version     1.0, May 3, 2012
 *  @author amit
 */

package com.snapdeal.cocofs.servicesImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.cocofs.db.dao.IFCDao;
import com.snapdeal.cocofs.entity.FCAddress;
import com.snapdeal.cocofs.services.IFCDetails;

@Service("FCDetailsImpl")
public class FCDetailsImpl implements IFCDetails {

	@Autowired
	@Qualifier("FCDaoImpl")
	private IFCDao fcDao;

	@Override
	public void saveFCDetails(FCAddress centerLocation) {
		// TODO Auto-generated method stub
		centerLocation.setCreated(DateUtils.getCurrentTime());
		fcDao.saveFCDetails(centerLocation);

	}

	@Override
	public FCAddress persistFCDetails(FCAddress centerLocation) {
		// TODO Auto-generated method stub
	 return fcDao.persistFCDetails(centerLocation);

	}

}
