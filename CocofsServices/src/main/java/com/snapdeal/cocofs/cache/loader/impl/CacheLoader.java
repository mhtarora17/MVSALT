package com.snapdeal.cocofs.cache.loader.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Nullable;

import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import com.snapdeal.base.cache.CacheManager;
import com.snapdeal.base.cache.EmailChannelCache;
import com.snapdeal.base.entity.EmailChannel;
import com.snapdeal.base.exception.TransportException;
import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.catalog.base.model.GetAllCategoryListRequest;
import com.snapdeal.catalog.base.model.GetAllMinBrandsRequest;
import com.snapdeal.catalog.base.model.GetAllMinBrandsResponse;
import com.snapdeal.catalog.base.model.GetCategoryListResponse;
import com.snapdeal.catalog.base.sro.MinProductBrandSRO;
import com.snapdeal.cocofs.annotations.Reload;
import com.snapdeal.cocofs.cache.AttributeCache;
import com.snapdeal.cocofs.cache.EmailTemplateCache;
import com.snapdeal.cocofs.cache.FulfillmentCentreCache;
import com.snapdeal.cocofs.cache.GiftWrapRulesCache;
import com.snapdeal.cocofs.cache.JobActionRoleMappingCache;
import com.snapdeal.cocofs.cache.JobCache;
import com.snapdeal.cocofs.cache.ProductBrandCache;
import com.snapdeal.cocofs.cache.ProductCategoryCache;
import com.snapdeal.cocofs.cache.RoleCache;
import com.snapdeal.cocofs.cache.StateCache;
import com.snapdeal.cocofs.cache.TaskParamCache;
import com.snapdeal.cocofs.cache.TaxClassCache;
import com.snapdeal.cocofs.cache.UploadSheetFieldDomainCache;
import com.snapdeal.cocofs.cache.loader.ICacheLoader;
import com.snapdeal.cocofs.configuration.CocofsMapPropertiesCache;
import com.snapdeal.cocofs.configuration.CocofsPropertiesCache;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.ConfigurationUtils;
import com.snapdeal.cocofs.configuration.FilePropertiesCache;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.configuration.exception.InvalidConfigurationException;
import com.snapdeal.cocofs.configuration.exception.InvalidFormatException;
import com.snapdeal.cocofs.dataaccess.fm.IFMDBDataReadService;
import com.snapdeal.cocofs.db.dao.IFulfillmentCenterDao;
import com.snapdeal.cocofs.db.dao.ISellerZoneDao;
import com.snapdeal.cocofs.db.dao.IStartupDao;
import com.snapdeal.cocofs.db.rules.IRuleDBReadService;
import com.snapdeal.cocofs.entity.CocofsMapProperty;
import com.snapdeal.cocofs.entity.CocofsProperty;
import com.snapdeal.cocofs.entity.EmailTemplate;
import com.snapdeal.cocofs.entity.FulfillmentCentre;
import com.snapdeal.cocofs.entity.JobAction;
import com.snapdeal.cocofs.entity.JobActionRoleMapping;
import com.snapdeal.cocofs.entity.JobStatus;
import com.snapdeal.cocofs.entity.Role;
import com.snapdeal.cocofs.entity.SellerFMMapping;
import com.snapdeal.cocofs.entity.TaskParam;
import com.snapdeal.cocofs.entity.UploadSheetFieldDomain;
import com.snapdeal.cocofs.enums.BlockName;
import com.snapdeal.cocofs.external.service.IShippingExternalService;
import com.snapdeal.cocofs.mongo.model.CocofsRuleUnit;
import com.snapdeal.cocofs.services.fm.dto.SellerFmFcDTO;
import com.snapdeal.cocofs.services.impl.GiftWrapRuleWrapperServiceImpl;
import com.snapdeal.ops.client.service.IOPSClientService;
import com.snapdeal.packman.cache.PackagingTypeCache;
import com.snapdeal.packman.cache.PincodeCityStateMappingCache;
import com.snapdeal.packman.cache.ShippingProviderCache;
import com.snapdeal.packman.cache.StoreFrontToStoreCodeMapCache;
import com.snapdeal.packman.cache.UrlRoleMappingCache;
import com.snapdeal.packman.dao.IPackmanExclusivelyDao;
import com.snapdeal.packman.dao.IUrlPatternDao;
import com.snapdeal.packman.entity.PackagingType;
import com.snapdeal.packman.entity.PackagingTypeItem;
import com.snapdeal.packman.entity.Store;
import com.snapdeal.packman.entity.StoreProperties;
import com.snapdeal.packman.entity.UrlPattern;
import com.snapdeal.packman.entity.UrlRoleMapping;
import com.snapdeal.packman.enums.StoreAttribute;
import com.snapdeal.product.client.service.IProductClientService;
import com.snapdeal.rule.cache.BlocksCache;
import com.snapdeal.rule.engine.entity.Rule;
import com.snapdeal.rule.engine.services.IRuleStartupService;
import com.snapdeal.scm.taxengine.client.ITaxengineClient;
import com.snapdeal.scm.taxengine.client.request.GetAllTaxcodesRequest;
import com.snapdeal.scm.taxengine.client.response.GetAllTaxcodesResponse;
import com.snapdeal.score.client.service.IScoreClientService;
import com.snapdeal.score.model.request.GetAllCityStateTupleRequest;
import com.snapdeal.score.model.request.GetAllPincodesRequest;
import com.snapdeal.score.model.request.GetAllShippingProvidersRequest;
import com.snapdeal.score.model.response.GetAllCityStateTupleResponse;
import com.snapdeal.score.model.response.GetAllPincodesResponse;
import com.snapdeal.score.model.response.GetAllShippingProvidersResponse;

@Service("CacheLoader")
@Transactional(readOnly = true)
public class CacheLoader implements ICacheLoader {

    private static final Logger LOG = LoggerFactory.getLogger(CacheLoader.class);

    @Autowired
    IStartupDao                 startupDao;

    @Autowired
    IProductClientService       productClientService;

    @Autowired
    private IOPSClientService   opsClientService;
    @Autowired
    private IRuleStartupService ruleStartupService;

    @Autowired
    private ITaxengineClient    taxEngineClientService;

    @Autowired
    private IRuleDBReadService  ruleDBService;

    @Autowired
    IShippingExternalService    shippingExternalService;

    @Autowired
    IFMDBDataReadService        fmDBDataReadService;

    @Autowired
    IFulfillmentCenterDao       fulfilmentCentreDao;

    @Autowired
    ISellerZoneDao              sellerZoneDao;

    @Autowired
    IScoreClientService         scoreClientService;

    @Autowired
    IPackmanExclusivelyDao      packmanExclusiveDao;
    
    @Autowired
    IUrlPatternDao              urlPatternDao;
    
    @Override
    @Reload(MIN_REPEAT_TIME_IN_MINUTE = 120, CACHE_GROUP = { "ALL" })
    public void loadProperties() {
        LOG.info("Loading Properties..");
        List<CocofsProperty> properties = startupDao.getProperties();
        CocofsPropertiesCache propertiesCache = new CocofsPropertiesCache();
        for (CocofsProperty property : properties) {
            propertiesCache.addProperty(property.getName(), property.getValue());
        }

        CacheManager.getInstance().setCache(propertiesCache);
        LOG.info("Loaded Properties SUCCESSFULLY!");
    }

    @Override
    @Reload(MIN_REPEAT_TIME_IN_MINUTE = 120, CACHE_GROUP = { "ALL" })
    public void loadCocofsMapPropertiesCache() {
        LOG.info("Loading Map Properties..");
        List<CocofsMapProperty> mapProperties = startupDao.getMapProperties();
        Map<String, Map<String, String>> mapCacheProperties = new HashMap<String, Map<String, String>>();
        for (CocofsMapProperty property : mapProperties) {
            if (mapCacheProperties.get(property.getPropertyName().toLowerCase()) != null) {
                mapCacheProperties.get(property.getPropertyName().toLowerCase()).put(property.getKey(), property.getValue());
            } else {
                Map<String, String> keyValues = new HashMap<String, String>();
                keyValues.put(property.getKey(), property.getValue());
                mapCacheProperties.put(property.getPropertyName().toLowerCase(), keyValues);
            }
        }
        LOG.info("MapProperties = " + mapCacheProperties);
        CocofsMapPropertiesCache cache = new CocofsMapPropertiesCache(mapCacheProperties);
        CacheManager.getInstance().setCache(cache);
        LOG.info("Loaded Map Properties SUCCESSFULLY!");
    }

    @Override
    @Reload(MIN_REPEAT_TIME_IN_MINUTE = 120, CACHE_GROUP = { "ALL" })
    public void loadCocofsPropertiesCache() throws InvalidFormatException, InvalidConfigurationException {
        String basePath = ConfigUtils.getStringScalar(Property.CONFIGURATION_DIR_PATH);
        String filePath = ConfigUtils.getStringScalar(Property.CONFIG_RELATIVE_PATH);
        loadCocofsPropertiesCacheWithParams(basePath, filePath);
    }

    @Override
    @Reload(MIN_REPEAT_TIME_IN_MINUTE = 120, CACHE_GROUP = { "ALL" })
    public void loadCocofsPropertiesCacheWithParams(String confgPath, String fileName) throws InvalidFormatException, InvalidConfigurationException {
        LOG.info("Loading Cocofs Properties Cache ..");
        FilePropertiesCache opc = null;
        Map<String, Object> properties = new HashMap<String, Object>();
        LOG.info("Reading Cocofs Configuration File: " + confgPath + fileName);
        properties = ConfigurationUtils.getMaps(confgPath + fileName);

        opc = new FilePropertiesCache(properties);
        CacheManager.getInstance().setCache(opc);
        LOG.info("Cocofs properties loaded SUCCESSFULLY!");
    }

    @Override
    @Reload(MIN_REPEAT_TIME_IN_MINUTE = 120, CACHE_GROUP = { "ALL" })
    @Transactional
    public void loadAttributes() {
        LOG.info("Loading Attributes ...");
        AttributeCache ac = new AttributeCache();
        ac.setAttributes(startupDao.getAllAttributes());
        CacheManager.getInstance().setCache(ac);
        LOG.info("Enabled attribute count " + ac.getEnabledAttributes().size());
        LOG.info("Disabled attribute count " + ac.getDisabledAttributes().size());
        LOG.info("Attributes loaded SUCCESSFULLY");
    }

    @Override
    @Reload(MIN_REPEAT_TIME_IN_MINUTE = 120, CACHE_GROUP = { "ALL" })
    @Transactional
    public void loadJobDefinitions() {
        LOG.info("Loading Jobs ...");
        List<JobAction> jaList = startupDao.getAllJobActions();
        List<JobStatus> jsList = startupDao.getAllJobStatus();
        JobCache jc = new JobCache();
        for (JobAction ja : jaList) {
            jc.addJobActionByCode(ja);
        }
        for (JobStatus js : jsList) {
            jc.addJobStatusByCode(js);
        }

        @SuppressWarnings("unchecked")
        List<JobActionRoleMapping> jarmList = startupDao.getJobActionRoleMapping();
        JobActionRoleMappingCache jarmCache = new JobActionRoleMappingCache();
        for (JobActionRoleMapping jarm : jarmList) {
            jarmCache.addJobActionRoleMapping(jarm);
        }
        CacheManager.getInstance().setCache(jc);
        CacheManager.getInstance().setCache(jarmCache);
        LOG.info("Jobs loaded SUCCESSFULLY");
    }
    
    @Override
    @Reload(MIN_REPEAT_TIME_IN_MINUTE = 60, CACHE_GROUP = { "ALL" })
    public void loadFulfillmentCentersCache() {
        LOG.info("Loading all fulfillment Centers...");
        FulfillmentCentreCache fcCache = new FulfillmentCentreCache();
        List<FulfillmentCentre> fulfillmentCenterList = fulfilmentCentreDao.getAllFulfillmentCenter();
        for (FulfillmentCentre fc : fulfillmentCenterList) {
            Hibernate.initialize(fc.getFcAddress());
            fcCache.addFulfillmentCenter(fc);
        }
        CacheManager.getInstance().setCache(fcCache);
        LOG.info("Loaded all fulfillment Centers !");
    }

    @Override
    @Reload(MIN_REPEAT_TIME_IN_MINUTE = 120, CACHE_GROUP = { "ALL" })
    public void loadEmailTemplates() {

        LOG.info("Loading Email Templates  ...");
        EmailTemplateCache etc = new EmailTemplateCache();

        List<EmailTemplate> etList = startupDao.getEmailTemplates();
        for (EmailTemplate et : etList) {
            etc.addEmailTemplate(et);
        }
        CacheManager.getInstance().setCache(etc);

        LOG.info("Email Templates loaded SUCCESSFULLY");
    }

    @Override
    @Reload(MIN_REPEAT_TIME_IN_MINUTE = 120, CACHE_GROUP = { "ALL" })
    public void loadTaskParams() {
        LOG.info("Loading Task Params  ...");
        TaskParamCache paramCache = new TaskParamCache();
        List<TaskParam> taskParamList = startupDao.getAllTaskParams();
        for (TaskParam taskParam : taskParamList) {
            paramCache.addToTaskParamCache(taskParam);
        }
        CacheManager.getInstance().setCache(paramCache);
        LOG.info("Task Params loaded SUCCESSFULLY");

    }

    @Override
    @Reload(MIN_REPEAT_TIME_IN_MINUTE = 120, CACHE_GROUP = { "ALL" })
    public void setWebServiceUrl() throws Exception {
        String catalogURL = ConfigUtils.getStringScalar(Property.CATALOG_WEB_SERVICE_URL);
        if (StringUtils.isEmpty(catalogURL)) {
            throw new Exception("Could not find catalogUrl while startup");
        }
        productClientService.setWebServiceBaseURL(catalogURL);

        String opsUrl = ConfigUtils.getStringScalar(Property.OPS_WEB_SERVICE_URL);
        if (StringUtils.isEmpty(opsUrl)) {
            throw new Exception("Could not find OPS while startup");
        }
        opsClientService.setWebServiceBaseURL(opsUrl);
    }

    @Override
    @Reload(MIN_REPEAT_TIME_IN_MINUTE = 120, CACHE_GROUP = { "ALL" })
    public void loadRules() {
        LOG.info("Loading Rules  ...");
        ruleStartupService.loadAll();
        LOG.info("Rules loaded SUCCESSFULLY");

    }

    @Override
    @Reload(MIN_REPEAT_TIME_IN_MINUTE = 120, CACHE_GROUP = { "ALL" })
    public void loadRoles() {
        LOG.info("Loading Roles...");
        List<Role> roles = startupDao.getAllRoles();
        RoleCache roleCache = new RoleCache();
        for (Role role : roles) {
            roleCache.addRole(role);
        }
        CacheManager.getInstance().setCache(roleCache);
        LOG.info("Loaded Roles SUCCESSFULLY!");
    }

    @Override
    @Reload(MIN_REPEAT_TIME_IN_MINUTE = 120, CACHE_GROUP = { "ALL" })
    public void loadEmailChannels() {
        LOG.info("Loading email channels from shipping...");
        try {
            List<EmailChannel> channels = shippingExternalService.getEmailChannels();
            EmailChannelCache emailChannelCache = new EmailChannelCache();
            for (EmailChannel channel : channels) {
                try {
                    LOG.info("Loading class - :" + channel.getClassName() + " for email channel {}", channel.getName());
                    emailChannelCache.addChannel(channel);
                } catch (ClassNotFoundException ex) {
                    LOG.error("could not found class:" + channel.getClassName() + " for email channel {}", channel.getName());
                }
            }

            CacheManager.getInstance().setCache(emailChannelCache);
            LOG.info("Loaded email channels... SUCCESSFULLY!");
        } catch (Exception e) {
            LOG.error("UNSUCCESSFUL IN LOADING EMAIL CHANNELS!", e);
        }

    }

    @Override
    @Reload(MIN_REPEAT_TIME_IN_MINUTE = 120, CACHE_GROUP = { "ALL" })
    public void loadGiftRules() {
        LOG.info("Loading giftWrap Rules Cache...");
        try {
            CacheManager cm = CacheManager.getInstance();

            Integer ruleBlockId = cm.getCache(BlocksCache.class).getBlockIDByName(BlockName.GiftWrapCategoryRuleblock.getCode());

            GiftWrapRulesCache giftWrapRulesCache = new GiftWrapRulesCache();
            giftWrapRulesCache.addRules(getRuleUnitsFromRules(ruleDBService.getEnabledAndSynchedRulesByBlockId(ruleBlockId)));

            cm.setCache(giftWrapRulesCache);
            LOG.info("giftWrap Rules added SUCCESSFULLY !!");
        } catch (Exception e) {
            LOG.error("error while loading giftwrap rules cache", e);
        }

    }

    @Override
    @Reload(MIN_REPEAT_TIME_IN_MINUTE = 120, CACHE_GROUP = { "ALL" })
    @Deprecated
    public void loadPackmanSlabs() {
        
    }
    
    @Override
    @Reload(MIN_REPEAT_TIME_IN_MINUTE = 120, CACHE_GROUP = { "ALL" })
    public void loadUrlToRoleMap() {
        LOG.info("Loading UrlToRoleMap Cache...");
        UrlRoleMappingCache urlroleCache = new UrlRoleMappingCache();
        Map<String, List<String>> finalResultMap = new HashMap<String, List<String>>();

        List<UrlPattern> urlPatternList = urlPatternDao.getAllUrlPattern();
        for (UrlPattern u : urlPatternList) {
            List<String> roleList = new ArrayList<String>();
            if (finalResultMap.containsKey(u.getUrl())) {
                roleList = finalResultMap.get(u.getUrl());
            }
            Set<UrlRoleMapping> urlRole = u.getUrlRoalMappingSet();
            for (UrlRoleMapping ur : urlRole)
                roleList.add(ur.getRole_code());
            finalResultMap.put(u.getUrl(), roleList);
        }
        urlroleCache.setUrlRoleMap(finalResultMap);
        CacheManager.getInstance().setCache(urlroleCache);
        LOG.info("Loading UrlToRoleMap Cache completed...");

    }

    @Override
    @Reload(MIN_REPEAT_TIME_IN_MINUTE = 120, CACHE_GROUP = { "ALL" })
    public void loadStoreFrontIdToStoreCode() {
        LOG.info("Loading Store Front to Store Code Map cache");
        StoreFrontToStoreCodeMapCache cache = new StoreFrontToStoreCodeMapCache();
        List<Store> storeList = packmanExclusiveDao.fetchAllEnabledStores();
        Map<String, String> storeFrontIdToStorecodemap = new HashMap<String, String>();
        Map<String, List<String>> storeToLabelMap = new HashMap<String, List<String>>();
        if (null != storeList) {
            for (Store store : storeList){
                storeFrontIdToStorecodemap.put(store.getStoreFrontId(), store.getCode());
                storeToLabelMap.put(store.getCode(), fetchLabelsForStore(store.getCode()));
            }

            cache.setStoreFrontToStoreCodeMap(storeFrontIdToStorecodemap);
            cache.setStoreToLabelMap(storeToLabelMap);
            CacheManager.getInstance().setCache(cache);
        } else {
            LOG.error("StoreFrontToStoreCode cache is not loaded ");
            return;
        }

        LOG.info("Loading Store Front to Store Code Map is completed successfully...");
    }
    
    private List<String> fetchLabelsForStore(String storeCode) {
        StoreProperties result = packmanExclusiveDao.fetchStoreProperty(storeCode, StoreAttribute.LABEL.getCode());
        List<String> resultList = new ArrayList<String>();
        ;
        if (null != result) {
            String[] str = result.getValue().split(",");
            for (String s : str)
                resultList.add(s);
        }

        return resultList;
    }

    /**
     * This method will get Rules from DB converted to RuleUnits of MongoDB Need to save them as RuleUnits in Cache so
     * that at run time there is minimal processing
     * 
     * @param rules
     * @return
     */
    private List<CocofsRuleUnit> getRuleUnitsFromRules(List<Rule> rules) {
        Collection<CocofsRuleUnit> ruleUnits = Collections2.transform(rules, new Function<Rule, CocofsRuleUnit>() {
            public CocofsRuleUnit apply(@Nullable Rule rule) {
                try {
                    return GiftWrapRuleWrapperServiceImpl.getGiftWrapRuleUnitFromRule(rule);
                } catch (Exception e) {
                    return null;
                }
            }
        });
        LOG.info("adding a total of {} gift wrap rules to cache", ruleUnits.size());
        return Collections.list(Collections.enumeration(ruleUnits));
    }

    @Override
    @Reload(MIN_REPEAT_TIME_IN_MINUTE = 15, CACHE_GROUP = { "ALL" })
    public void loadSellerFMMappingIncrementalCache() {
        LOG.info("Loading sellerFMMapping Incremental Cache...");
        SellerFMMappingCache oldCache = CacheManager.getInstance().getCache(SellerFMMappingCache.class);
        try {
            Date currentTime = DateUtils.getCurrentTime();
            if (oldCache == null) {
                SellerFMMappingCache sellerFMMappingCache = new SellerFMMappingCache();
                sellerFMMappingCache.addSellerFmFcList(getSellerFmFcDtosFromSellerFCMapping(fmDBDataReadService.getAllSellerFMAndFCMapping()));
                sellerFMMappingCache.setLastUpdated(currentTime);
                CacheManager.getInstance().setCache(sellerFMMappingCache);

            } else if (oldCache.getLastUpdated() != null) {
                oldCache.addSellerFmFcList(getSellerFmFcDtosFromSellerFCMapping(fmDBDataReadService.getAllSellerFMAndFCMappingByLastUpdated(oldCache.getLastUpdated())));
                oldCache.setLastUpdated(currentTime);
                CacheManager.getInstance().setCache(oldCache);
            }
            LOG.info("sellerFMMapping incremental Cache loaded SUCCESSFULLY !!");
        } catch (Exception e) {
            LOG.error("error while loading sellerFMMapping incremental  cache", e);
        }

    }

    /**
     * This method will convert db entites of sellerFMMapping and sellerFCMapping to SellerFMFCDTO
     * 
     * @param sellerFmMappings
     * @return
     */
    private List<SellerFmFcDTO> getSellerFmFcDtosFromSellerFCMapping(List<SellerFMMapping> sellerFmMappings) {
        Collection<SellerFmFcDTO> dtos = Collections2.transform(sellerFmMappings, new Function<SellerFMMapping, SellerFmFcDTO>() {
            public SellerFmFcDTO apply(@Nullable SellerFMMapping sellerFmMapping) {
                try {
                    return new SellerFmFcDTO(sellerFmMapping);
                } catch (Exception e) {
                    return null;
                }
            }
        });
        LOG.info("adding a total of {} SellerFmFcMappingDTOs to cache", dtos.size());
        return Collections.list(Collections.enumeration(dtos));
    }

    @Override
    @Reload(MIN_REPEAT_TIME_IN_MINUTE = 120, CACHE_GROUP = { "ALL" })
    public void loadUploadSheetFieldDomains() {
        LOG.info("Loading UploadSheetFieldDomain Cache  ...");
        try {
            UploadSheetFieldDomainCache cache = new UploadSheetFieldDomainCache();
            List<UploadSheetFieldDomain> fieldDomainList = startupDao.getAllUploadSheetFieldDomains();
            for (UploadSheetFieldDomain fieldDomain : fieldDomainList) {
                cache.addToUploadSheetFieldDomainCache(fieldDomain);
            }
            CacheManager.getInstance().setCache(cache);
            LOG.info("UploadSheetFieldDomain Cache loaded SUCCESSFULLY");
        } catch (Exception e) {
            LOG.error("error while loading UploadSheetFieldDomain  cache", e);
        }

    }

    @Override
    @Reload(MIN_REPEAT_TIME_IN_MINUTE = 300, CACHE_GROUP = { "ALL" })
    public void loadAllPackagingTypes() {
        LOG.info("Loading all Packaging Types...");
        PackagingTypeCache ptCache = new PackagingTypeCache();
        List<PackagingType> typeList = packmanExclusiveDao.getAllPackagingType();
        ptCache.addPackagingType(typeList);
        List<PackagingTypeItem> typeItemList = packmanExclusiveDao.getAllPackagingTypeItem();
        ptCache.addPackagingTypeByPTI(typeItemList);
        CacheManager.getInstance().setCache(ptCache);
        LOG.info("Loaded all Packaging Types !");
    }

    @Override
    @Reload(MIN_REPEAT_TIME_IN_MINUTE = 120, CACHE_GROUP = { "ALL" })
    public void loadShippingProviderCache() {
        LOG.info("Loading ShippingProviderCache  ...");
        try {
            GetAllShippingProvidersResponse resp = scoreClientService.getAllShippingProviders(new GetAllShippingProvidersRequest());
            if (resp.isSuccessful()) {
                ShippingProviderCache cache = new ShippingProviderCache();
                cache.addMappings(resp.getShippingProviderSROs());
                CacheManager.getInstance().setCache(cache);
                LOG.info("ShippingProviderCache loaded SUCCESSFULLY");
            } else {
                LOG.info("ShippingProviderCache loading skipped due to failure. response:" + resp);
            }
        } catch (Exception e) {
            LOG.error("error while loading ShippingProviderCache cache", e);
        }
    }

    @Override
    @Reload(MIN_REPEAT_TIME_IN_MINUTE = 60 * 24, CACHE_GROUP = { "ALL" })
    public void loadPincodeCityStateMappingCache() {
        LOG.info("Loading PincodeCityStateMappingCache  ...");
        try {
            GetAllPincodesResponse resp = scoreClientService.getAllPincodes(new GetAllPincodesRequest());
            if (resp.isSuccessful()) {
                PincodeCityStateMappingCache cache = new PincodeCityStateMappingCache();
                cache.addMappings(resp.getPostalCodeSROs());
                CacheManager.getInstance().setCache(cache);
                LOG.info("PincodeCityStateMappingCache loaded SUCCESSFULLY");
            } else {
                LOG.info("PincodeCityStateMappingCache loading skipped due to failure. response:" + resp);
            }
        } catch (Exception e) {
            LOG.error("error while loading PincodeCityStateMappingCache cache", e);
        }
    }

    @Override
    @Reload(MIN_REPEAT_TIME_IN_MINUTE = 60 * 24, CACHE_GROUP = { "ALL" })
    public void loadProductCategoryCache() {
        LOG.info("Loading ProductCategoryCache  ...");
        try {
            GetCategoryListResponse resp = productClientService.getAllCategories(new GetAllCategoryListRequest());
            if (resp.isSuccessful()) {
                ProductCategoryCache cache = new ProductCategoryCache();
                cache.addMappings(resp.getProductCategories());
                CacheManager.getInstance().setCache(cache);
                LOG.info("ProductCategoryCache loaded SUCCESSFULLY");
            } else {
                LOG.info("ProductCategoryCache loading skipped due to failure. response:" + resp);
            }
        } catch (Exception e) {
            LOG.error("error while loading ProductCategoryCache cache", e);
        }
    }

    @Override
    //reload this cache every 30 days
    @Reload(MIN_REPEAT_TIME_IN_MINUTE = 60 * 24 * 30, CACHE_GROUP = { "ALL" })
    public void loadStateCache() {
        LOG.info("Loading StateCache  ...");
        try {
            GetAllCityStateTupleResponse resp = scoreClientService.getAllCityStates(new GetAllCityStateTupleRequest());
            if (resp.isSuccessful()) {
                StateCache cache = new StateCache();
                cache.addMappings(resp.getCitystateList());
                CacheManager.getInstance().setCache(cache);
                LOG.info("StateCache loaded SUCCESSFULLY");
            } else {
                LOG.info("StateCache loading skipped due to failure. response:" + resp);
            }
        } catch (Exception e) {
            LOG.error("error while loading StateCache cache", e);
        }
    }

    @Override
    @Reload(MIN_REPEAT_TIME_IN_MINUTE = 60 * 24, CACHE_GROUP = { "ALL" })
    public void loadProductBrands() {
        LOG.info("Loading ProductBrands  ...");
        try {
            List<MinProductBrandSRO> sros = new ArrayList<MinProductBrandSRO>();
            GetAllMinBrandsRequest request = new GetAllMinBrandsRequest();
            int batch = ConfigUtils.getIntegerScalar(Property.CAMS_GET_BRANDS_BATCH_SIZE);
            for (int i = 0;; i += batch) {
                request.setStartResult(i);
                request.setNumResults(batch);
                GetAllMinBrandsResponse resp = productClientService.getAllMinProductBrands(request);
                if (resp.isSuccessful()) {
                    if (null != resp.getProductBrands() && resp.getProductBrands().size() > 0) {
                        sros.addAll(resp.getProductBrands());
                        if (resp.getProductBrands().size() < batch) {
                            break;
                        }
                    } else {
                        break;
                    }
                } else {
                    LOG.error("product response not successful while getting data for brands - resp {} ", resp);
                    break;
                }
            }

            ProductBrandCache cache = new ProductBrandCache();
            cache.addMappings(sros);
            CacheManager.getInstance().setCache(cache);
            LOG.info("ProductBrands loaded SUCCESSFULLY");

        } catch (TransportException e) {
            LOG.error("error while loading ProductBrands cache.", e);
        }
    }
    //reload this cache every day
    @Override    
    @Reload(MIN_REPEAT_TIME_IN_MINUTE = 60 * 24, CACHE_GROUP = { "ALL" })
    public void loadTaxClassCache() {

        if (ConfigUtils.getBooleanScalar(Property.ENABLE_TAX_CLASS)) {
            LOG.info("Loading TaxClassCache  ...");
            try {
                GetAllTaxcodesResponse resp = taxEngineClientService.getAllTaxcodes(new GetAllTaxcodesRequest());
                if (resp.isSuccessful()) {
                    TaxClassCache cache = new TaxClassCache();
                    cache.addMappings(resp.getTaxCodes());
                    CacheManager.getInstance().setCache(cache);
                    LOG.info("TaxClassCache loaded SUCCESSFULLY " + cache.getTaxClassList());
                } else {
                    LOG.error("TaxClassCache loading skipped due to failure. response:" + resp);
                }
            } catch (Exception e) {
                LOG.error("error while loading TaxClassCache cache", e);
            }
        } else {
            LOG.info("TaxClass Not Active...");
        }
    }
}
