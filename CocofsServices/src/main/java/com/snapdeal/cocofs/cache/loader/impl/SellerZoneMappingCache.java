/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.cache.loader.impl;

import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.snapdeal.base.annotations.Cache;
import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.services.ISellerZoneService;

/**
 *  
 *  @version     1.0, 15-Jun-2015
 *  @author Gaurav
 */

@Cache(name = "SellerZoneMappingCache")
public class SellerZoneMappingCache {


    private static final Logger          LOG = LoggerFactory.getLogger(SellerZoneMappingCache.class);

    private ISellerZoneService sellerZoneService;
    
    @SuppressWarnings("rawtypes")
    private com.google.common.cache.LoadingCache<String, String> sellerZoneMappingCache;
    
    private  int ttl;
    
    private int cacheSize;
    
    private static final String NO_SELLER_ZONE_EXISTS =   "f";
    
    
    
    private void  initializeProperties(ISellerZoneService sellerZoneService){
       
        this.sellerZoneService = sellerZoneService;
        this.ttl = ConfigUtils.getIntegerScalar(Property.SELLER_ZONE_MAPPING_CACHE_TTL);
        this.cacheSize = ConfigUtils.getIntegerScalar(Property.SELLER_ZONE_MAPPING_CACHE_SIZE);
        
        LOG.info("Parameters for SellerZoneMappingCache size {}, ttl {} ", this.cacheSize, this.ttl);

        
    }
    
    public void init(ISellerZoneService sellerZoneService){
              
        initializeProperties(sellerZoneService);
        
        sellerZoneMappingCache = CacheBuilder.newBuilder().
                maximumSize(this.cacheSize).expireAfterWrite(this.ttl, TimeUnit.SECONDS).build(new CacheLoader<String, String>(){
                   
                    @Override
                    public String load( String key ) throws Exception {
                      return retrieveSellerZone(key);
                    }
                });  
       
    }
    
    public String getSellerZoneMapping(final String sellerCode) throws ExternalDataNotFoundException {

        String zone = null;
        try {
            zone = sellerZoneMappingCache.get(sellerCode);            
            if(NO_SELLER_ZONE_EXISTS.equals(zone)){
                zone = null;
            }
        } catch (Exception e) {
            LOG.error("Error while fetching zone from SellerZoneMappingCache for seller:{} error: {}",sellerCode, e.getMessage());
            throw new ExternalDataNotFoundException("No seller-zone mapping found in aerospike for seller:" + sellerCode + "" );
        }

        return zone;
    }

   
    public void  logCacheStats(){
        LOG.info("=========CACHE STATS for SellerZoneMappingCache ==========");
        LOG.info("Number of entries in Cache {}",sellerZoneMappingCache.size());
        LOG.info(sellerZoneMappingCache.stats().toString());

    }
    
    public boolean isCacheReloadRequiredForSellerZoneMapping() {

        logCacheStats();
        
        int cacheSize = ConfigUtils.getIntegerScalar(Property.SELLER_ZONE_MAPPING_CACHE_SIZE);
        int ttl = ConfigUtils.getIntegerScalar(Property.SELLER_ZONE_MAPPING_CACHE_TTL);
        
        if (ttl != this.ttl || cacheSize != this.cacheSize) {
            LOG.info("ttl {} or cacheSize {} property has been changed , so reloading SellerZoneMappingCache", ttl, cacheSize);
            return true;
        }

        return false;
    }
       
    
    private String retrieveSellerZone(String key) throws Exception {
        String zone = null;
        String sellerCode = key;
        try {    
            LOG.info("Retrieving zone for seller {} on demand and adding it to cache", sellerCode);
            zone = sellerZoneService.getSellerZoneMappingBySellerFromBaseSource(sellerCode);
            if(StringUtils.isEmpty(zone)){
                return NO_SELLER_ZONE_EXISTS;
            }
        } catch (Exception e) {
            LOG.error("No seller-zone mapping found in aerospike for seller" + sellerCode);
        }
        
        return zone;
        
    }
}
