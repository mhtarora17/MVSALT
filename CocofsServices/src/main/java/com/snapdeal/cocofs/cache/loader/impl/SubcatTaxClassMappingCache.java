/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.cache.loader.impl;

import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.snapdeal.base.annotations.Cache;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.services.taxclass.ITaxClassService;
import com.snapdeal.cocofs.services.taxclass.dto.SubcatTaxClassMappingDTO;

/**
 * @version 1.0, 23-Dec-2015
 * @author nitish
 */

@Cache(name = "SubcatTaxClassMappingCache")
public class SubcatTaxClassMappingCache {

    private static final Logger                                                    LOG = LoggerFactory.getLogger(SubcatTaxClassMappingCache.class);

    private ITaxClassService                                                       taxClassService;

    @SuppressWarnings("rawtypes")
    private com.google.common.cache.LoadingCache<String, SubcatTaxClassMappingDTO> subcatTaxClassMappingCache;

    private int                                                                    ttl;

    private int                                                                    cacheSize;

    private void initializeProperties(ITaxClassService taxClassService) {

        this.taxClassService = taxClassService;
        this.ttl = ConfigUtils.getIntegerScalar(Property.SUBCAT_TAX_CLASS_CACHE_TTL);
        this.cacheSize = ConfigUtils.getIntegerScalar(Property.SUBCAT_TAX_CLASS_CACHE_SIZE);

        LOG.info("Parameters for SubcatTaxClassMappingCache size {}, ttl {} ", this.cacheSize, this.ttl);

    }

    public void init(ITaxClassService taxClassService) {

        initializeProperties(taxClassService);

        subcatTaxClassMappingCache = CacheBuilder.newBuilder().maximumSize(this.cacheSize).expireAfterWrite(this.ttl, TimeUnit.SECONDS).build(
                new CacheLoader<String, SubcatTaxClassMappingDTO>() {

                    @Override
                    public SubcatTaxClassMappingDTO load(String key) throws Exception {
                        return retrieveTaxClass(key);
                    }
                });

    }

    public void logCacheStats() {
        LOG.info("=========CACHE STATS for SubcatTaxClassMappingCache ==========");
        LOG.info("Number of entries in Cache {}", subcatTaxClassMappingCache.size());
        LOG.info(subcatTaxClassMappingCache.stats().toString());

    }

    public boolean isCacheReloadRequiredForSubcatTaxClassMapping() {

        logCacheStats();

        int cacheSize = ConfigUtils.getIntegerScalar(Property.SUPC_TAX_CLASS_CACHE_SIZE);
        int ttl = ConfigUtils.getIntegerScalar(Property.SUPC_TAX_CLASS_CACHE_TTL);

        if (ttl != this.ttl || cacheSize != this.cacheSize) {
            LOG.info("ttl {} or cacheSize {} property has been changed , so reloading SubcatTaxClassMappingCache", ttl, cacheSize);
            return true;
        }

        return false;
    }

    private SubcatTaxClassMappingDTO retrieveTaxClass(String key) throws Exception {
        SubcatTaxClassMappingDTO taxClass = null;
        try {
            LOG.debug("Retrieving tax class  for subcat {} on demand and adding it to cache", key);
            taxClass = taxClassService.getTaxClassBySubcatFromDataSource(key);
            if (taxClass == null) {
                return null;
            }
        } catch (Exception e) {
            LOG.error("No tax class details found in datasource for subcat" + key);
        }

        return taxClass;

    }

    public SubcatTaxClassMappingDTO getSubcatTaxClassMappingForSubcat(String subcat) throws ExternalDataNotFoundException {
        SubcatTaxClassMappingDTO taxClass = null;
        try {
            taxClass = subcatTaxClassMappingCache.get(subcat);
        } catch (Exception e) {
            throw new ExternalDataNotFoundException("No tax class details found in datasource for subcat:" + subcat + "");
        }

        return taxClass;
    }

}