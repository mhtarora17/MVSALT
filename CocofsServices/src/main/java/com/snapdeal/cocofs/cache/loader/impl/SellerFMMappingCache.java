/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.cache.loader.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.snapdeal.base.annotations.Cache;
import com.snapdeal.base.utils.CollectionUtils;
import com.snapdeal.cocofs.services.fm.dto.SellerFmFcDTO;

/**
 *  
 *  @version     1.0, 10-Dec-2014
 *  @author ankur
 */

@Cache(name = "SellerFMMappingCache")
public class SellerFMMappingCache {

    
    private HashMap<String, SellerFmFcDTO> sellerFmFcMap = new HashMap<String, SellerFmFcDTO>();


    
    private volatile Date lastUpdated = null;
    
    public void addSellerFmFcList(List<SellerFmFcDTO> dtos){
        if(CollectionUtils.isEmpty(dtos)){
            return;
        }
        
        for(SellerFmFcDTO dto : dtos){
        	sellerFmFcMap.put(dto.getSellerCode(), dto);
        }
    }
    
    public void addSellerFmFc(SellerFmFcDTO dto){
        if(dto == null){
            return;
        }
        sellerFmFcMap.put(dto.getSellerCode(), dto);
        
    }
   
    
    
    public SellerFmFcDTO getSellerFmFcMapping(String sellerCode){

        if (sellerFmFcMap == null || sellerFmFcMap.isEmpty()) {
            return null;
        } else {
            return sellerFmFcMap.get(sellerCode);
        }

    }
    
 
    

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }
    
    
    

    
}
