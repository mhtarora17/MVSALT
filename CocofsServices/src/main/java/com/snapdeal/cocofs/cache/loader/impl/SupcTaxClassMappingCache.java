/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.cache.loader.impl;

import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.snapdeal.base.annotations.Cache;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.services.ISellerDetailsService;
import com.snapdeal.cocofs.services.fm.dto.AddressDTO;
import com.snapdeal.cocofs.services.fm.dto.SellerSupcFmFcDTO;
import com.snapdeal.cocofs.services.taxclass.ITaxClassService;
import com.snapdeal.cocofs.services.taxclass.dto.SupcTaxClassMappingDTO;

/**
 * @version 1.0, 23-Dec-2015
 * @author nitish
 */

@Cache(name = "SupcTaxClassMappingCache")
public class SupcTaxClassMappingCache {

    private static final Logger                                                  LOG = LoggerFactory.getLogger(SupcTaxClassMappingCache.class);

    private ITaxClassService                                                     taxClassService;

    @SuppressWarnings("rawtypes")
    private com.google.common.cache.LoadingCache<String, SupcTaxClassMappingDTO> supcTaxClassMappingCache;

    private int                                                                  ttl;

    private int                                                                  cacheSize;

    private void initializeProperties(ITaxClassService taxClassService) {

        this.taxClassService = taxClassService;
        this.ttl = ConfigUtils.getIntegerScalar(Property.SUPC_TAX_CLASS_CACHE_TTL);
        this.cacheSize = ConfigUtils.getIntegerScalar(Property.SUPC_TAX_CLASS_CACHE_SIZE);

        LOG.info("Parameters for SupcTaxClassMappingCache size {}, ttl {} ", this.cacheSize, this.ttl);

    }

    public void init(ITaxClassService taxClassService) {

        initializeProperties(taxClassService);

        supcTaxClassMappingCache = CacheBuilder.newBuilder().maximumSize(this.cacheSize).expireAfterWrite(this.ttl, TimeUnit.SECONDS).build(
                new CacheLoader<String, SupcTaxClassMappingDTO>() {

                    @Override
                    public SupcTaxClassMappingDTO load(String key) throws Exception {
                        return retrieveTaxClass(key);
                    }
                });

    }

    public void logCacheStats() {
        LOG.info("=========CACHE STATS for SupcTaxClassMappingCache ==========");
        LOG.info("Number of entries in Cache {}", supcTaxClassMappingCache.size());
        LOG.info(supcTaxClassMappingCache.stats().toString());

    }

    public boolean isCacheReloadRequiredForSupcTaxClassMapping() {

        logCacheStats();

        int cacheSize = ConfigUtils.getIntegerScalar(Property.SUPC_TAX_CLASS_CACHE_SIZE);
        int ttl = ConfigUtils.getIntegerScalar(Property.SUPC_TAX_CLASS_CACHE_TTL);

        if (ttl != this.ttl || cacheSize != this.cacheSize) {
            LOG.info("ttl {} or cacheSize {} property has been changed , so reloading SupcTaxClassMappingCache", ttl, cacheSize);
            return true;
        }

        return false;
    }

    private SupcTaxClassMappingDTO retrieveTaxClass(String key) throws Exception {
        SupcTaxClassMappingDTO taxClass = null;
        try {
            LOG.debug("Retrieving tax class  for supc {} on demand and adding it to cache", key);
            taxClass = taxClassService.getTaxClassBySupcFromDataSource(key);
            if (taxClass == null) {
                return null;
            }
        } catch (Exception e) {
            LOG.error("No tax class details found in datasource for supc" + key);
        }

        return taxClass;

    }

    public SupcTaxClassMappingDTO getSupcTaxClassMappingForSupc(String supc) throws ExternalDataNotFoundException {
        SupcTaxClassMappingDTO taxClass = null;
        try {
            taxClass = supcTaxClassMappingCache.get(supc);
        } catch (Exception e) {
            throw new ExternalDataNotFoundException("No tax class details found in datasource for supc:" + supc + "");
        }

        return taxClass;
    }

}
