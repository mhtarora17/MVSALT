/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.cache.loader.impl;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.snapdeal.base.annotations.Cache;
import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.services.fm.IFulfilmentModelService;
import com.snapdeal.cocofs.services.fm.dto.SellerSupcFmFcDTO;

/**
 * 
 * @version 1.0, 12-Dec-2014
 * @author ankur
 */

@Cache(name = "SellerSupcFMMappingCache")
public class SellerSupcFMMappingCache {

	private static final Logger LOG = LoggerFactory.getLogger(SellerSupcFMMappingCache.class);

	IFulfilmentModelService fmService;

	private com.google.common.cache.LoadingCache<String, SellerSupcFmFcDTO> sellerSupcFMMappingCache;

	private int ttl;

	private int cacheSize;

	private static final SellerSupcFmFcDTO NO_DATA = new SellerSupcFmFcDTO();

	private void initializeProperties(IFulfilmentModelService fulfilmentModelService) {

		this.fmService = fulfilmentModelService;
		this.ttl = ConfigUtils.getIntegerScalar(Property.SELLER_SUPC_FM_MAPPING_CACHE_TTL);
		this.cacheSize = ConfigUtils.getIntegerScalar(Property.SELLER_SUPC_FM_MAPPING_CACHE_SIZE);

		LOG.info("Parameters for SellerSUPCFMMappingCache size {}, ttl {} ", this.cacheSize, this.ttl);

	}

	public void init(IFulfilmentModelService fulfilmentModelService) {

		initializeProperties(fulfilmentModelService);

		sellerSupcFMMappingCache = CacheBuilder.newBuilder().maximumSize(this.cacheSize)
				.expireAfterWrite(this.ttl, TimeUnit.SECONDS).build(new CacheLoader<String, SellerSupcFmFcDTO>() {

					@Override
					public SellerSupcFmFcDTO load(String key) throws Exception {
						return retrieveSellerSupcFM(key);
					}
				});

	}

	public SellerSupcFmFcDTO getSellerSupcFMMapping(final String sellerCode, final String supc)
			throws ExternalDataNotFoundException {

		SellerSupcFmFcDTO dto = null;
		try {
			String key = prepareKey(sellerCode, supc);
			dto = sellerSupcFMMappingCache.get(key);

			if (dto != null && NO_DATA == dto) {
				return null;
			}

		} catch (Exception e) {
			LOG.error("error while fetching fulfilmentModel from SellerSUPCFMMappingCache for seller: " + sellerCode
					+ " supc: " + supc + " error: {}", e.getMessage());
			throw new ExternalDataNotFoundException(
					"no enabled fulfilmentModel found in aerospike for seller" + sellerCode + ", supc");
		}

		return dto;
	}

	public void logCacheStats() {
		LOG.info("=========CACHE STATS for SellerSUPCFMMappingCache ==========");
		LOG.info("Number of entries in Cache {}", sellerSupcFMMappingCache.size());
		LOG.info(sellerSupcFMMappingCache.stats().toString());

	}

	public boolean isCacheReloadRequiredForSellerSupcFMMapping() {

		logCacheStats();

		int cacheSize = ConfigUtils.getIntegerScalar(Property.SELLER_SUPC_FM_MAPPING_CACHE_SIZE);
		int ttl = ConfigUtils.getIntegerScalar(Property.SELLER_SUPC_FM_MAPPING_CACHE_TTL);

		if (ttl != this.ttl || cacheSize != this.cacheSize) {
			LOG.info("ttl {} or cacheSize {} property has been changed , so reloading SellerSUPCFMMappingCache", ttl,
					cacheSize);
			return true;
		}

		return false;
	}

	private SellerSupcFmFcDTO retrieveSellerSupcFM(String key) throws Exception {

		SellerSupcFmFcDTO dto = null;
		try {
			List<String> subkeys = StringUtils.split(key, "-");
			String sellerCode = subkeys.get(0);
			String supc = subkeys.get(1);

			LOG.info("Retrieving fulfilment for seller {} , supc {} on demand and adding it to cache", sellerCode,
					supc);

			dto = fmService.getSellerSupcFmFcDTOFromDataSource(sellerCode, supc);
			if (dto == null) {
				// in case no exception exists at seller-supc level, put a flag in cache ,
				// If we do not do this , it will hit aerospike each time.
				dto = NO_DATA;
			}
		} catch (ExternalDataNotFoundException e) {
			LOG.error("no enabled fulfilmentModel found in aerospike for seller,supc " + key);
		}

		return dto;

	}

	public static String prepareKey(String... subkeys) {
		if (subkeys == null)
			return null;

		StringBuilder key = new StringBuilder("");

		key.append(subkeys[0]);
		for (int i = 1; i < subkeys.length; i++) {
			key.append("-");
			key.append(subkeys[i]);
		}

		return key.toString();
	}

}
