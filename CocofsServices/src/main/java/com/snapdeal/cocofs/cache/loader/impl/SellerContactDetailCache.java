/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.cache.loader.impl;

import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.snapdeal.base.annotations.Cache;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.services.ISellerDetailsService;
import com.snapdeal.cocofs.services.fm.dto.AddressDTO;

/**
 *  
 *  @version     1.0, 15-Jun-2015
 *  @author Gaurav
 */

@Cache(name = "SellerContactDetailCache")
public class SellerContactDetailCache {


    private static final Logger          LOG = LoggerFactory.getLogger(SellerContactDetailCache.class);

    private ISellerDetailsService sellerDetailService;
    
    @SuppressWarnings("rawtypes")
    private com.google.common.cache.LoadingCache<String, AddressDTO> sellerAddressMappingCache;
    
    private  int ttl;
    
    private int cacheSize;
    
    private static final AddressDTO NO_SELLER_DETAIL_EXISTS =   new AddressDTO();;
    
    
    
    private void  initializeProperties(ISellerDetailsService sellerDetailService){
       
        this.sellerDetailService = sellerDetailService;
        this.ttl = ConfigUtils.getIntegerScalar(Property.SELLER_DETAIL_CACHE_TTL);
        this.cacheSize = ConfigUtils.getIntegerScalar(Property.SELLER_DETAIL_CACHE_SIZE);
        
        LOG.info("Parameters for SellerContactDetailCache size {}, ttl {} ", this.cacheSize, this.ttl);

        
    }
    
    public void init(ISellerDetailsService sellerDetailService){
              
        initializeProperties(sellerDetailService);
        
        sellerAddressMappingCache = CacheBuilder.newBuilder().
                maximumSize(this.cacheSize).expireAfterWrite(this.ttl, TimeUnit.SECONDS).build(new CacheLoader<String, AddressDTO>(){
                   
                    @Override
                    public AddressDTO load( String key ) throws Exception {
                      return retrieveSellerDetail(key);
                    }
                });  
       
    }
    
    public AddressDTO getSellerDetails(final String sellerCode) throws ExternalDataNotFoundException {

        AddressDTO address = null;
        try {
            address = sellerAddressMappingCache.get(sellerCode);            
            if(NO_SELLER_DETAIL_EXISTS.equals(address)){
                address = null;
            }
        } catch (Exception e) {
            LOG.error("Error while fetching details from SellerContactDetailsCache for seller:{} error: {}",sellerCode, e.getMessage());
            throw new ExternalDataNotFoundException("No seller details found in aerospike for seller:" + sellerCode + "" );
        }

        return address;
    }

   
    public void  logCacheStats(){
        LOG.info("=========CACHE STATS for SellerContactDetailsCache ==========");
        LOG.info("Number of entries in Cache {}",sellerAddressMappingCache.size());
        LOG.info(sellerAddressMappingCache.stats().toString());

    }
    
    public boolean isCacheReloadRequiredForSellerContactDetail() {

        logCacheStats();
        
        int cacheSize = ConfigUtils.getIntegerScalar(Property.SELLER_DETAIL_CACHE_SIZE);
        int ttl = ConfigUtils.getIntegerScalar(Property.SELLER_DETAIL_CACHE_TTL);
        
        if (ttl != this.ttl || cacheSize != this.cacheSize) {
            LOG.info("ttl {} or cacheSize {} property has been changed , so reloading SellerDetailCache", ttl, cacheSize);
            return true;
        }

        return false;
    }
       
    
    private AddressDTO retrieveSellerDetail(String key) throws Exception {
        AddressDTO address = null;
        String sellerCode = key;
        try {    
            LOG.info("Retrieving address for seller {} on demand and adding it to cache", sellerCode);
            address = sellerDetailService.getSellerDetailsFromBaseSource(sellerCode);
            if( address == null){
                return NO_SELLER_DETAIL_EXISTS;
            }
        } catch (Exception e) {
            LOG.error("No seller details found in aerospike for seller" + sellerCode);
        }
        
        return address;
        
    }
}
