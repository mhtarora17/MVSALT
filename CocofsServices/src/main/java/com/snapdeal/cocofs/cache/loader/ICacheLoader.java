package com.snapdeal.cocofs.cache.loader;

import com.snapdeal.cocofs.configuration.exception.InvalidConfigurationException;
import com.snapdeal.cocofs.configuration.exception.InvalidFormatException;

public interface ICacheLoader {

    void loadProperties();

    void loadCocofsPropertiesCache() throws InvalidFormatException, InvalidConfigurationException;

    void loadCocofsMapPropertiesCache() throws InvalidFormatException, InvalidConfigurationException;

    void loadCocofsPropertiesCacheWithParams(String confgPath, String fileName) throws InvalidFormatException, InvalidConfigurationException;

    /**
     * Load definiteve set of product attributes from database
     */
    void loadAttributes();

    /**
     * Load all Job Actions, JobStatus and JobActionRoleMappings
     */
    void loadJobDefinitions();

    /**
     * load all Email Templates
     */
    void loadEmailTemplates();

    /**
     * Loads all task params
     */
    void loadTaskParams();

    /**
     * @throws Exception
     */
    void setWebServiceUrl() throws Exception;

    void loadRules();

    /**
     * Load all Roles
     */
    void loadRoles();

    void loadEmailChannels();

    void loadGiftRules();

    void loadSellerFMMappingIncrementalCache();

    void loadUploadSheetFieldDomains();


    void loadPackmanSlabs();

    void loadShippingProviderCache();

    void loadPincodeCityStateMappingCache();

    void loadUrlToRoleMap();

    void loadAllPackagingTypes();
    
    public void loadStoreFrontIdToStoreCode();

    void loadProductCategoryCache();

    void loadStateCache();

    void loadProductBrands();

    void loadTaxClassCache();

    void loadFulfillmentCentersCache();

}