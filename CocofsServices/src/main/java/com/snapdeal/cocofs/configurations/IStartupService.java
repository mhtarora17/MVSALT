/*
 *  Copyright 2012 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 28-May-2012
 *  @author amd
 */
package com.snapdeal.cocofs.configurations;

import java.lang.reflect.InvocationTargetException;

public interface IStartupService {

    void loadProperties(boolean forceReload) throws Exception;

    void loadAll(boolean forcedReload) throws Exception;

    void loadTasks(boolean load) throws Exception;

    void loadAttributes(boolean forcedReload) throws Exception;

    void loadJobDefinitions(boolean forcedReload) throws Exception;

    void loadEmailTemplates(boolean forcedReload) throws Exception;

    void loadRules(boolean forcedReload) throws Exception;

    void loadCacheByMethodName(String methodName) throws Exception;

    void registerSellerSupcAssocicationQueueConsumer();

    void unregisterSellerSupcAssocicationQueueConsumer();

    void registerSellerSupcSDFulfilledAssocicationQueueProducer();

    void unregisterSellerSupcSDFulfilledAssocicationQueueProducer();

    void registerFCChangeQueueEventProducer();

    void unregisterFCChangeQueueEventProducer();

    void loadEmailChannels(boolean startup) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException;

    void loadAerospikeConfig(boolean startup);

    void loaAerospikeLDTConfig(boolean startup);

    /*void initializeSellerFMMappingCache();
*/
    void initializeSellerSupcFMMappingCache();

    void loadSellerFMMappingIncrementalCache(boolean forcedReload) throws Exception;

    void loadGiftWrapRules(boolean forcedReload) throws Exception;
    
    void initializeSellerZoneMappingCache();

    void loadPackmanCache(boolean forceReload) throws Exception;

    void initializeSellerDetailCache();

    void registerShipFromChangeQueueEventProducer();

    void unregisterShipFromChangeQueueEventProducer();

    void registerSdInstantChangeQueueEventProducer();

    void unregisterSdInstantChangeQueueEventProducer(); 

    void initializeSupcTaxClassMappingCache();

    void initializeSubcatTaxClassMappingCache(); 
}
