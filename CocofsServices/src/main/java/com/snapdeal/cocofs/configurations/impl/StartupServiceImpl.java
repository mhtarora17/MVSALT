/*

 *  Copyright 2012 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 28-May-2012
 *  @author amd
 */
package com.snapdeal.cocofs.configurations.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.runtime.RuntimeConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.snapdeal.base.cache.CacheManager;
import com.snapdeal.catalog.client.service.ICatalogClientService;
import com.snapdeal.cocofs.annotation.RateLimited;
import com.snapdeal.cocofs.cache.loader.ICacheLoader;
import com.snapdeal.cocofs.cache.loader.impl.SellerContactDetailCache;
import com.snapdeal.cocofs.cache.loader.impl.SellerSupcFMMappingCache;
import com.snapdeal.cocofs.cache.loader.impl.SellerZoneMappingCache;
import com.snapdeal.cocofs.cache.loader.impl.SubcatTaxClassMappingCache;
import com.snapdeal.cocofs.cache.loader.impl.SupcTaxClassMappingCache;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.configurations.ICacheLoadService;
import com.snapdeal.cocofs.configurations.IStartupService;
import com.snapdeal.cocofs.enums.SystemProperty;
import com.snapdeal.cocofs.enums.TaskDetailAction;
import com.snapdeal.cocofs.services.ISellerDetailsService;
import com.snapdeal.cocofs.services.ISellerZoneService;
import com.snapdeal.cocofs.services.consumer.INewSellerSupcUpdateConsumer;
import com.snapdeal.cocofs.services.fm.IFulfilmentModelService;
import com.snapdeal.cocofs.services.producer.IQueueListener;
import com.snapdeal.cocofs.services.task.update.ITaskUpdateService;
import com.snapdeal.cocofs.services.taxclass.ITaxClassService;
import com.snapdeal.geo.client.IGeoClientService;
import com.snapdeal.ipms.client.service.IIPMSClientService;
import com.snapdeal.packman.utils.PackmanConstants;
import com.snapdeal.product.client.service.IProductClientService;
import com.snapdeal.scm.taxengine.client.ITaxengineClient;
import com.snapdeal.score.client.service.IScoreClientService;
import com.snapdeal.score.data.aerospike.core.LoadAerospikeConfigRequest;
import com.snapdeal.score.data.aerospike.exception.LoadFailedException;
import com.snapdeal.score.data.aerospike.service.impl.ScoreAerospikeBase;
import com.snapdeal.score.data.aerospike.service.impl.ScoreAerospikeLDTService;
import com.snapdeal.score.data.aerospike.service.impl.ScoreAerospikeService;
import com.snapdeal.shipping.service.IShippingClientService;

@Service("startupService")
public class StartupServiceImpl implements IStartupService {

    private static final Logger      LOG      = LoggerFactory.getLogger(StartupServiceImpl.class);

    @Autowired
    ICacheLoadService                cacheLoadService;

    @Autowired
    ICacheLoader                     cacheLoader;

    @Autowired
    ITaskUpdateService               taskUpdateService;

    @Autowired
    IIPMSClientService               ipmsClientService;

    @Autowired
    ICatalogClientService            catalogClientService;

    @Autowired
    IProductClientService            productClientService;

    @Autowired
    INewSellerSupcUpdateConsumer     newSellerSupcUpdateConsumer;

    @Autowired
    @Qualifier("externalNotificationsServiceForSearchPush")
    IQueueListener                   sellerSupcSDFulfilledProducer;

    @Autowired
    @Qualifier("externalNotificationsServiceForSellerSupcFMFCUpdate")
    IQueueListener                   fcChangeQueueEventProducer;
    
    @Autowired
    @Qualifier("externalNotificationsServiceForShipFromUpdateImpl")
    IQueueListener                   shipFromProducer;

    @Autowired
    @Qualifier("externalNotificationsServiceForSdInstantUpdateImpl")
    IQueueListener                   sdInstantProducer;

    @Autowired
    IShippingClientService           shippingClientService;

    @Autowired
    IScoreClientService              scoreClientService;

    @Autowired
    IGeoClientService                sdGeoClientService;

    @Autowired
    ITaxengineClient                 taxEngineClientService;

    @Autowired
    @Qualifier("aerospikeService")
    private ScoreAerospikeService    aerospikeService;

    @Autowired
    @Qualifier("aerospikeLDTService")
    private ScoreAerospikeLDTService aerospikeLDTService;

    @Autowired
    private ApplicationContext       applicationContext;

    @Autowired
    Environment                      env;

    @Autowired
    private static final String      hostName = System.getProperty(SystemProperty.HOST_NAME.getCode());

    private boolean                  startup  = true;

    @Override
    public void loadProperties(boolean forcedReload) throws Exception {
        List<String> methodNameList = new ArrayList<String>();
        methodNameList.add("loadProperties");
        methodNameList.add("loadCocofsPropertiesCache");
        methodNameList.add("loadCocofsMapPropertiesCache");
        cacheLoadService.loadCacheByMethodName(cacheLoader, methodNameList, null, startup || forcedReload, "host");
    }

    @Override
    @RateLimited(calls = 10000, seconds = 10)
    public void loadAll(boolean forcedReload) throws Exception {
        Velocity.setProperty(RuntimeConstants.VM_MESSAGES_ON, false);

        Velocity.setProperty(RuntimeConstants.RUNTIME_LOG_LOGSYSTEM_CLASS, "org.apache.velocity.runtime.log.CommonsLogLogChute");
        Velocity.init();
        loadProperties(forcedReload);
        setWebServiceURLs();
        loadAerospikeConfig(startup);

        
        List<String> methodNameList = new ArrayList<String>();
        methodNameList.add("loadTaskParams");
        methodNameList.add("setWebServiceUrl");
        methodNameList.add("loadRoles");
        methodNameList.add("loadUploadSheetFieldDomains");
        methodNameList.add("loadFulfillmentCentersCache");
        methodNameList.add("loadUrlToRoleMap");
        methodNameList.add("loadStoreFrontIdToStoreCode");
        methodNameList.add("loadProductCategoryCache");
        methodNameList.add("loadStateCache");
        methodNameList.add("loadTaxClassCache");
        cacheLoadService.loadCacheByMethodName(cacheLoader, methodNameList, null, startup || forcedReload, "host");
        loadAttributes(forcedReload);
        loadJobDefinitions(forcedReload);
        loadEmailTemplates(forcedReload);
        loadEmailChannels(startup || forcedReload);
        loadRules(forcedReload);
        loadGiftWrapRules(forcedReload);
        loadSellerFMMappingIncrementalCache(forcedReload);
        initializeSellerSupcFMMappingCache();
        initializeSellerZoneMappingCache();

        initializeSupcTaxClassMappingCache();
        initializeSubcatTaxClassMappingCache();
        initializeSellerDetailCache();
        loadPackmanCache(startup || forcedReload);
      
        //Please keep loadTasks at end 
        loadTasks(startup || forcedReload);
        startup = false;
    }

    @Override
    public void loadPackmanCache(boolean forceReload) throws Exception {
        if (isPackmanServer()) {
            List<String> methodNameList = new ArrayList<String>();
            methodNameList.add("loadAllPackagingTypes");
            methodNameList.add("loadPackmanSlabs");
            methodNameList.add("loadShippingProviderCache");
            methodNameList.add("loadPincodeCityStateMappingCache");
            methodNameList.add("loadProductBrands");
            cacheLoadService.loadCacheByMethodName(cacheLoader, methodNameList, null, forceReload, "host");
        }

    }

    private boolean isPackmanServer() {
        String[] profiles = env.getActiveProfiles();
        if (profiles != null) {
            List<String> profileList = Arrays.asList(profiles);
            if (profileList.contains(PackmanConstants.PACKMAN_PROFILE_NAME)) {
                return true;
            }
        }
        return false;

    }

    @Override
    public void loadEmailChannels(boolean startup) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        List<String> methodNameList = new ArrayList<String>();
        methodNameList.add("loadEmailChannels");
        cacheLoadService.loadCacheByMethodName(cacheLoader, methodNameList, null, startup, "host");

    }

    @Override
    public void loadCacheByMethodName(String methodName) throws Exception {
        if (StringUtils.isNotEmpty(methodName)) {
            List<String> methodNameList = new ArrayList<String>();
            methodNameList.add(methodName);
            cacheLoadService.loadCacheByMethodName(cacheLoader, methodNameList, null, true, "host");
        }
    }

    private void setWebServiceURLs() throws Exception {

        String ipmsURL = ConfigUtils.getStringScalar(Property.IPMS_WEB_SERVICE_URL);
        String catalogURL = ConfigUtils.getStringScalar(Property.CATALOG_WEB_SERVICE_URL);
        String shippingUrl = ConfigUtils.getStringScalar(Property.SHIPPING_WEB_SERVICE_URL);
        String scoreUrl = ConfigUtils.getStringScalar(Property.SCORE_WEB_SERVICE_URL);
        String sdGeoUrl = ConfigUtils.getStringScalar(Property.SDGEO_WEB_SERVICE_URL);
        String taxEngineUrl = ConfigUtils.getStringScalar(Property.TAX_ENGINE_WEB_SERVICE_URL);

        LOG.info("Web service URL's in property cache are ipms : " + ipmsURL + " catalog : " + catalogURL + " shipping : " + shippingUrl + " score : " + scoreUrl + " sdgeo : "
                + sdGeoUrl + "tax engine : " + taxEngineUrl);

        if (StringUtils.isNotEmpty(ipmsURL) && StringUtils.isNotEmpty(catalogURL) && StringUtils.isNotEmpty(shippingUrl) && StringUtils.isNotEmpty(scoreUrl)) {
            catalogClientService.setWebServiceBaseURL(catalogURL);
            productClientService.setWebServiceBaseURL(catalogURL);
            ipmsClientService.setWebServiceBaseURL(ipmsURL);
            shippingClientService.setShippingWebServiceURL(shippingUrl);
            scoreClientService.setScoreWebServiceUrl(scoreUrl);
            sdGeoClientService.setWebServiceBaseURL(sdGeoUrl);

            if (ConfigUtils.getBooleanScalar(Property.ENABLE_TAX_CLASS) && StringUtils.isNotEmpty(taxEngineUrl)) {

                taxEngineClientService.setTaxengineWebServiceURL(taxEngineUrl);

            }

        } else {
            throw new Exception("One of the web service URL's is found to be null.");
        }
    }

    @Override
    public void loadAerospikeConfig(boolean startup) {
        if (startup) {
            loadAerospikeConfig(aerospikeService, getASConfig());
        }
    }

    @Override
    public void loaAerospikeLDTConfig(boolean startup) {
        if (startup) {
            LOG.info("Loading LDT  Aerospike Config..");

            loadAerospikeConfig(aerospikeLDTService, getLDTAerospikeConfig());

            LOG.info("Loaded LDT Aerospike Config SUCCESSFULLY!");
        }
    }

    private void loadAerospikeConfig(ScoreAerospikeBase aerospikeService, LoadAerospikeConfigRequest request) {
        try {
            LOG.info("Loading Aerospike Config..");
            aerospikeService.loadAerospikeConfig(request);
            LOG.info("Loaded Aerospike Config SUCCESSFULLY!");
        } catch (LoadFailedException e) {
            LOG.error("Failed to load aerospike config", e);
        }
    }

    private LoadAerospikeConfigRequest getASConfig() {
        String endPoint = ConfigUtils.getStringScalar(Property.AEROSPIKE_URL);
        String packageToScan = ConfigUtils.getStringScalar(Property.AEROSPIKE_VO_BASE_PACKAGE);
        boolean failIfNotConnect = ConfigUtils.getBooleanScalar(Property.AEROSPIKE_FAIL_IF_NOT_CONNECTED);
        int maxSocketIdle = ConfigUtils.getIntegerScalar(Property.AEROSPIKE_MAX_SOCKET_IDLE);
        int timeout = ConfigUtils.getIntegerScalar(Property.AEROSPIKE_TIMEOUT);
        int maxThreads = ConfigUtils.getIntegerScalar(Property.AEROSPIKE_MAX_THREADS);
        return new LoadAerospikeConfigRequest(endPoint, packageToScan, failIfNotConnect, maxSocketIdle, maxThreads, timeout);
    }

    private LoadAerospikeConfigRequest getLDTAerospikeConfig() {
        String endPoint = ConfigUtils.getStringScalar(Property.AEROSPIKE_LDT_URL);
        String packageToScan = ConfigUtils.getStringScalar(Property.AEROSPIKE_LDT_VO_BASE_PACKAGE);
        boolean failIfNotConnect = ConfigUtils.getBooleanScalar(Property.AEROSPIKE_LDT_FAIL_IF_NOT_CONNECTED);
        int maxSocketIdle = ConfigUtils.getIntegerScalar(Property.AEROSPIKE_LDT_MAX_SOCKET_IDLE);
        int timeout = ConfigUtils.getIntegerScalar(Property.AEROSPIKE_LDT_TIMEOUT);
        int maxThreads = ConfigUtils.getIntegerScalar(Property.AEROSPIKE_LDT_MAX_THREADS);
        return new LoadAerospikeConfigRequest(endPoint, packageToScan, failIfNotConnect, maxSocketIdle, maxThreads, timeout);
    }

    @Override
    public void loadAttributes(boolean forcedReload) throws Exception {
        List<String> methodNameList = new ArrayList<String>();
        methodNameList.add("loadAttributes");
        cacheLoadService.loadCacheByMethodName(cacheLoader, methodNameList, null, startup || forcedReload, "host");

    }

    @Override
    public void loadTasks(boolean loadTask) throws Exception {
        if (loadTask && ConfigUtils.getStringList(Property.HOST_NAMES).contains(hostName)) {
            LOG.info("Loading Tasks details..");
            taskUpdateService.createTaskDetailUpdate(TaskDetailAction.START_ALL.getCode(), null, System.getProperty(SystemProperty.HOST_NAME.getCode()), "startup");
            LOG.info("Tasks detail created successfully...");
        } else {
            LOG.info("Not loading tasks since we are not forced to do so.....");
        }
    }

    @Override
    public void loadJobDefinitions(boolean forcedReload) throws Exception {
        List<String> methodNameList = new ArrayList<String>();
        methodNameList.add("loadJobDefinitions");
        cacheLoadService.loadCacheByMethodName(cacheLoader, methodNameList, null, startup || forcedReload, "host");
    }

    @Override
    public void loadEmailTemplates(boolean forcedReload) throws Exception {
        List<String> methodNameList = new ArrayList<String>();
        methodNameList.add("loadEmailTemplates");
        cacheLoadService.loadCacheByMethodName(cacheLoader, methodNameList, null, startup || forcedReload, "host");
    }

    @Override
    public void loadRules(boolean forcedReload) throws Exception {
        List<String> methodNameList = new ArrayList<String>();
        methodNameList.add("loadRules");
        cacheLoadService.loadCacheByMethodName(cacheLoader, methodNameList, null, startup || forcedReload, "host");
    }

    @Override
    public void loadGiftWrapRules(boolean forcedReload) throws Exception {
        List<String> methodNameList = new ArrayList<String>();
        methodNameList.add("loadGiftRules");
        cacheLoadService.loadCacheByMethodName(cacheLoader, methodNameList, null, startup || forcedReload, "host");
    }

    @Override
    public void loadSellerFMMappingIncrementalCache(boolean forcedReload) throws Exception {
        List<String> methodNameList = new ArrayList<String>();
        methodNameList.add("loadSellerFMMappingIncrementalCache");
        cacheLoadService.loadCacheByMethodName(cacheLoader, methodNameList, null, forcedReload, "host");
    }

    @Override
    public void registerSellerSupcAssocicationQueueConsumer() {
        LOG.info("Registering activemq consumers - newSellerSupcUpdateConsumer  ...");
        String queueName = ConfigUtils.getStringScalar(Property.SELLER_SUPC_ASSOCIATION_QUEUE);
        String brokerURL = ConfigUtils.getStringScalar(Property.ACTIVE_MQ_URL);
        String username = ConfigUtils.getStringScalar(Property.ACTIVE_MQ_USERNAME);
        String password = ConfigUtils.getStringScalar(Property.ACTIVE_MQ_PASSWORD);
        LOG.info("params queueName = {}, brokerURL = {}, username = {}", queueName, brokerURL, username);
        newSellerSupcUpdateConsumer.registerConsumer(queueName, brokerURL, username, password);
        LOG.info("newSellerSupcUpdateConsumer active mq registration attempt completed  ...");
    }

    @Override
    public void unregisterSellerSupcAssocicationQueueConsumer() {
        LOG.info("Unregistering activemq consumers  ...");
        newSellerSupcUpdateConsumer.unregisterConsumer();
        LOG.info("Active mq unregistration attempted  ...");
    }

    @Override
    public void registerSellerSupcSDFulfilledAssocicationQueueProducer() {
        LOG.info("Registering activemq producer - sellerSupcSDFulfilledProducer  ...");
        String queueName = ConfigUtils.getStringScalar(Property.ACTIVE_MQ_SEARCH_PUSH_QUEUE_NAME);
        String brokerURL = ConfigUtils.getStringScalar(Property.ACTIVE_MQ_SEARCH_PUSH_QUEUE_URL);
        String username = ConfigUtils.getStringScalar(Property.ACTIVE_MQ_SEARCH_PUSH_USERNAME);
        String password = ConfigUtils.getStringScalar(Property.ACTIVE_MQ_SEARCH_PUSH_PASSWORD);
        LOG.info("params queueName = {}, brokerURL = {}, username = {}", queueName, brokerURL, username);
        sellerSupcSDFulfilledProducer.registerProducer(queueName, brokerURL, username, password);
        LOG.info("sellerSupcSDFulfilledProducer active mq registration attempt completed  ...");
    }

    @Override
    public void unregisterSellerSupcSDFulfilledAssocicationQueueProducer() {
        LOG.info("Unregistering activemq producer - sellerSupcSDFulfilledProducer...");
        sellerSupcSDFulfilledProducer.unregisterProducer();
        LOG.info("sellerSupcSDFulfilledProducer active mq unregistration attempt completed  ...");
    }

    @Override
    public void registerFCChangeQueueEventProducer() {
        LOG.info("Registering activemq producer - fcChangeQueueEventProducer  ...");
        String queueName = ConfigUtils.getStringScalar(Property.ACTIVE_MQ_SELLER_SUPC_FM_FC_PUSH_QUEUE_NAME);
        String brokerURL = ConfigUtils.getStringScalar(Property.ACTIVE_MQ_SELLER_SUPC_FM_FC_PUSH_QUEUE_URL);
        String username = ConfigUtils.getStringScalar(Property.ACTIVE_MQ_SELLER_SUPC_FM_FC_PUSH_USERNAME);
        String password = ConfigUtils.getStringScalar(Property.ACTIVE_MQ_SELLER_SUPC_FM_FC_PUSH_PASSWORD);
        LOG.info("params queueName = {}, brokerURL = {}, username = {}", queueName, brokerURL, username);
        fcChangeQueueEventProducer.registerProducer(queueName, brokerURL, username, password);
        LOG.info("fcChangeQueueEventProducer active mq registration attempt completed  ...");
    }

    @Override
    public void unregisterFCChangeQueueEventProducer() {
        LOG.info("Unregistering activemq producer - sellerSupcSDFulfilledProducer...");
        fcChangeQueueEventProducer.unregisterProducer();
        LOG.info("sellerSupcSDFulfilledProducer active mq unregistration attempt completed  ...");
    }
    
    @Override
    public void registerShipFromChangeQueueEventProducer() {
        LOG.info("Registering activemq producer - shipFromChangeQueueEventProducer  ...");
        String queueName = ConfigUtils.getStringScalar(Property.ACTIVE_MQ_SHIPFROM_PUSH_QUEUE_NAME);
        String brokerURL = ConfigUtils.getStringScalar(Property.ACTIVE_MQ_SHIPFROM_PUSH_QUEUE_URL);
        String username = ConfigUtils.getStringScalar(Property.ACTIVE_MQ_SHIPFROM_PUSH_USERNAME);
        String password = ConfigUtils.getStringScalar(Property.ACTIVE_MQ_SHIPFROM_PUSH_PASSWORD);
        LOG.info("params queueName = {}, brokerURL = {}, username = {}", queueName, brokerURL, username);
        shipFromProducer.registerProducer(queueName, brokerURL, username, password);
        LOG.info("shipFromChangeQueueEventProducer active mq registration attempt completed  ...");
    }

    @Override
    public void unregisterShipFromChangeQueueEventProducer() {
        LOG.info("Unregistering activemq producer - shipFromChangeQueueEventProducer...");
        shipFromProducer.unregisterProducer();
        LOG.info("shipFromChangeQueueEventProducer active mq unregistration attempt completed  ...");
    }
    
    @Override
    public void registerSdInstantChangeQueueEventProducer() {
        LOG.info("Registering activemq producer - sdInstantChangeQueueEventProducer  ...");
        String queueName = ConfigUtils.getStringScalar(Property.ACTIVE_MQ_SDINSTANT_PUSH_QUEUE_NAME);
        String brokerURL = ConfigUtils.getStringScalar(Property.ACTIVE_MQ_SDINSTANT_PUSH_QUEUE_URL);
        String username = ConfigUtils.getStringScalar(Property.ACTIVE_MQ_SDINSTANT_PUSH_USERNAME);
        String password = ConfigUtils.getStringScalar(Property.ACTIVE_MQ_SDINSTANT_PUSH_PASSWORD);
        LOG.info("params queueName = {}, brokerURL = {}, username = {}", queueName, brokerURL, username);
        sdInstantProducer.registerProducer(queueName, brokerURL, username, password);
        LOG.info("shipFromChangeQueueEventProducer active mq registration attempt completed  ...");
    }

    @Override
    public void unregisterSdInstantChangeQueueEventProducer() {
        LOG.info("Unregistering activemq producer - sdinstantChangeQueueEventProducer...");
        sdInstantProducer.unregisterProducer();
        LOG.info("sdinstantChangeQueueEventProducer active mq unregistration attempt completed  ...");
    }

    @Override
    public void initializeSellerSupcFMMappingCache() {
        SellerSupcFMMappingCache oldCache = CacheManager.getInstance().getCache(SellerSupcFMMappingCache.class);

        if (oldCache == null || oldCache.isCacheReloadRequiredForSellerSupcFMMapping()) {

            LOG.info("initializing SellerSupcFMMappingCache...");
            IFulfilmentModelService fulfilmentModelService = applicationContext.getBean(IFulfilmentModelService.class);
            SellerSupcFMMappingCache sellersupcFMMappingCache = new SellerSupcFMMappingCache();
            sellersupcFMMappingCache.init(fulfilmentModelService);
            CacheManager.getInstance().setCache(sellersupcFMMappingCache);
            LOG.info("initialized SellerSupcFMMappingCache SUCCESSFULLY!");

        } else {

            LOG.info("Not resetting  SellerSupcFMMappingCache as no property like ttl,cacheSize were changed...");

        }

    }

    @Override
    public void initializeSellerZoneMappingCache() {
        SellerZoneMappingCache oldCache = CacheManager.getInstance().getCache(SellerZoneMappingCache.class);

        if (oldCache == null || oldCache.isCacheReloadRequiredForSellerZoneMapping()) {

            LOG.info("Initializing SellerZoneMappingCache...");
            ISellerZoneService sellerZoneService = applicationContext.getBean(ISellerZoneService.class);
            SellerZoneMappingCache sellerZoneMappingCache = new SellerZoneMappingCache();
            sellerZoneMappingCache.init(sellerZoneService);
            CacheManager.getInstance().setCache(sellerZoneMappingCache);
            LOG.info("initialized SellerZoneMappingCache SUCCESSFULLY!");

        } else {
            LOG.info("Not resetting  SellerZoneMappingCache as no property like ttl,cacheSize were changed...");
        }

    }

    @Override
    public void initializeSellerDetailCache() {
        SellerContactDetailCache oldCache = CacheManager.getInstance().getCache(SellerContactDetailCache.class);

        if (oldCache == null || oldCache.isCacheReloadRequiredForSellerContactDetail()) {

            LOG.info("Initializing SellerContactDetailCache...");
            ISellerDetailsService sellerDetailService = applicationContext.getBean(ISellerDetailsService.class);
            SellerContactDetailCache sellerContactDetailCache = new SellerContactDetailCache();
            sellerContactDetailCache.init(sellerDetailService);
            CacheManager.getInstance().setCache(sellerContactDetailCache);
            LOG.info("initialized SellerContactDetailCache SUCCESSFULLY!");

        } else {
            LOG.info("Not resetting  SellerContactDetailCache as no property like ttl,cacheSize were changed...");
        }

    }

    @Override
    public void initializeSupcTaxClassMappingCache() {
        if (ConfigUtils.getBooleanScalar(Property.ENABLE_TAX_CLASS)) {

            SupcTaxClassMappingCache oldCache = CacheManager.getInstance().getCache(SupcTaxClassMappingCache.class);

            if (oldCache == null || oldCache.isCacheReloadRequiredForSupcTaxClassMapping()) {

                LOG.info("Initializing SupcTaxClassMappingCache...");
                ITaxClassService taxClassService = applicationContext.getBean(ITaxClassService.class);
                SupcTaxClassMappingCache sellerContactDetailCache = new SupcTaxClassMappingCache();
                sellerContactDetailCache.init(taxClassService);
                CacheManager.getInstance().setCache(sellerContactDetailCache);
                LOG.info("initialized sellerContactDetailCache SUCCESSFULLY!");

            } else {
                LOG.info("Not resetting  sellerContactDetailCache as no property like ttl,cacheSize were changed...");
            }
        }
    }

    @Override
    public void initializeSubcatTaxClassMappingCache() {
        if (ConfigUtils.getBooleanScalar(Property.ENABLE_TAX_CLASS)) {
            SubcatTaxClassMappingCache oldCache = CacheManager.getInstance().getCache(SubcatTaxClassMappingCache.class);
            if (oldCache == null || oldCache.isCacheReloadRequiredForSubcatTaxClassMapping()) {

                LOG.info("Initializing SubcatTaxClassMappingCache...");
                ITaxClassService taxClassService = applicationContext.getBean(ITaxClassService.class);
                SubcatTaxClassMappingCache sellerContactDetailCache = new SubcatTaxClassMappingCache();
                sellerContactDetailCache.init(taxClassService);
                CacheManager.getInstance().setCache(sellerContactDetailCache);
                LOG.info("initialized sellerContactDetailCache SUCCESSFULLY!");

            } else {
                LOG.info("Not resetting  sellerContactDetailCache as no property like ttl,cacheSize were changed...");
            }
        }
    }

}
