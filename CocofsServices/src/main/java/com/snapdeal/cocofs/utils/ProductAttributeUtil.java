/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.utils;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.snapdeal.base.cache.CacheManager;
import com.snapdeal.cocofs.cache.UploadSheetFieldDomainCache;
import com.snapdeal.cocofs.excelupload.ProductAttributeUploadDTO;

/**
 *  
 *  @version     1.0, 16-Feb-2015
 *  @author ankur
 */
public class ProductAttributeUtil {

    private static final Logger  LOG = LoggerFactory.getLogger(ProductAttributeUtil.class);

    public static boolean isValidSerializedType(String serializedType) {
        try {
            List<String> serializedTypeList = CacheManager.getInstance().getCache(UploadSheetFieldDomainCache.class).getValuesForClassField(
                    ProductAttributeUploadDTO.class.getSimpleName(), "serializedType");

            if (serializedTypeList != null && !serializedTypeList.isEmpty()) {
                return serializedTypeList.contains(serializedType);
            }
            
        } catch (Exception e) {
            LOG.error("error while retriving values from UploadSheetFieldDomainCache for serializedType " + serializedType, e);
        }
        LOG.info("either cache is not loaded or no entry exist for serialized type {}, hence returning  false",serializedType);
        return false;
    }

    public static boolean isValidPackagingType(String packagingType) {
        try {
            List<String> packagingTypeList = CacheManager.getInstance().getCache(UploadSheetFieldDomainCache.class).getValuesForClassField(
                    ProductAttributeUploadDTO.class.getSimpleName(), "packagingType");

            if (packagingTypeList != null && !packagingTypeList.isEmpty()) {
                return packagingTypeList.contains(packagingType);
            }
            
        } catch (Exception e) {
            LOG.error("error while retriving values from UploadSheetFieldDomainCache for packagingType " + packagingType, e);
        }
        LOG.info("either cache is not loaded or no entry exist for packagingType{}, hence returning  false",packagingType);
        return false;
    }

}
