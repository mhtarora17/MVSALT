/*
 *  Copyright 2012 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 21-Jan-2013
 *  @author prateek
 */
package com.snapdeal.cocofs.utils;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.io.Files;

public class FTPUtils {

    private static final Logger LOG = LoggerFactory.getLogger(FTPUtils.class);

    public static boolean uploadFileToFtpLocation(File file, String serverName, String userName, String password, String workingDir, String moveToFolder) throws IOException {
        boolean uploadedToFtp = false;
        try {

            if (file != null) {
                File[] fileEntries = file.listFiles();

                if (fileEntries != null && fileEntries.length > 0) {
                    for (File fileEntry : fileEntries) {
                        if (fileEntry.isDirectory()) {
                            uploadFileToFtpLocation(fileEntry, serverName, userName, password, workingDir, null);
                        } else {
                            uploadedToFtp = uploadFile(fileEntry, workingDir, moveToFolder, serverName, userName, password);
                        }
                    }
                } else {
                    uploadedToFtp = uploadFile(file, workingDir, moveToFolder, serverName, userName, password);
                }
            }

        } catch (IOException e) {
            LOG.error("Error upoading file to ftp server ", e);
            throw e;
        } catch (Exception e) {
            LOG.error("Error upoading file to ftp server ", e);
            throw new RuntimeException("Error upoading file to ftp server");
        } finally {

        }
        return uploadedToFtp;
    }

    private static boolean uploadFile(File fileEntry, String workingDir, String moveToFolder, String serverName, String userName, String password) throws IOException {
        FTPClient client = new FTPClient();
        boolean uploadedToFtp = false;
        FileInputStream fis = null;
        try {
            client.connect(serverName);
            if (!client.login(userName, password)) {
                client.logout();
            }

            int reply = client.getReplyCode();
            //FTPReply stores a set of constants for FTP reply codes. 
            if (!FTPReply.isPositiveCompletion(reply)) {
                client.disconnect();
            }
            fis = new FileInputStream(fileEntry);

            //client.cwd(workingDir);

            boolean dirExists = true;

            //tokenize the string and attempt to change into each directory level.  If you cannot, then start creating.
            String[] directories = workingDir.split("/");

            for (String dir : directories) {
                if (!dir.isEmpty()) {
                    if (dirExists) {
                        dirExists = client.changeWorkingDirectory(dir);
                    }
                    if (!dirExists) {
                        if (!client.makeDirectory(dir)) {
                            throw new IOException("Unable to create remote directory '" + dir + "'.  error='" + client.getReplyString() + "'");
                        }
                        if (!client.changeWorkingDirectory(dir)) {
                            throw new IOException("Unable to change into newly created remote directory '" + dir + "'.  error='" + client.getReplyString() + "'");
                        }
                    }
                }
            }

            client.enterLocalPassiveMode();
            client.setFileType(FTP.BINARY_FILE_TYPE);
            uploadedToFtp = client.storeFile(fileEntry.getName(), fis);
            if (uploadedToFtp) {
                LOG.info("Successfully uploaded file {}", fileEntry.getName());
            }

            if (moveToFolder != null) {
                client.changeToParentDirectory();
                String from = workingDir + "/" + fileEntry.getName();
                String to = moveToFolder + "/" + fileEntry.getName();
                if (client.rename(from, to))
                    if (LOG.isInfoEnabled())
                        LOG.info("Failed to move file { } ", fileEntry.getName());
            }
        } catch (FileNotFoundException e) {
            LOG.error("Error working on file: {}", e);
            throw e;
        } catch (IOException e) {
            LOG.error("Error uploading file: ", e);
            throw e;
        } finally {
            try {
                client.disconnect();
                if (fis != null) {

                    fis.close();
                }
            } catch (IOException e) {
                LOG.error("Error closing fis: {}", e);
            }

        }
        return uploadedToFtp;
    }

    /**
     * Download file from FTP give its absolute location e.g /myfolder/yourfile.txt to given local file path e.g.
     * /mylocalfolder/yourlocalfile.txt
     * 
     * @param serverName
     * @param userName
     * @param password
     * @param ftpFullPath
     * @param localFullPath
     */
    public static void downloadFile(String serverName, String userName, String password, String ftpFullPath, String localFullPath) {
        FTPClient client = new FTPClient();
        BufferedOutputStream os = null;
        try {
            client.connect(serverName);
            if (!client.login(userName, password)) {
                client.logout();
            }

            int reply = client.getReplyCode();
            //FTPReply stores a set of constants for FTP reply codes. 
            if (!FTPReply.isPositiveCompletion(reply)) {
                client.disconnect();
            }

            //enter passive mode
            client.enterLocalPassiveMode();
            client.setFileType(FTP.BINARY_FILE_TYPE);
            File localFile = new File(localFullPath);
            Files.createParentDirs(localFile);
            if (!localFile.exists()) {
                localFile.createNewFile();
            }

            File tempFTPFile = new File(ftpFullPath);
            boolean changed = client.changeWorkingDirectory(tempFTPFile.getParent());
            LOG.info("Changed: {}", changed);

            FTPFile ftpFile;
            FTPFile[] ftpFiles = client.listFiles(tempFTPFile.getName());
            if (ftpFiles.length == 1) {
                ftpFile = ftpFiles[0];
                try {

                    os = new BufferedOutputStream(new FileOutputStream(localFile));
                    boolean downloaded = client.retrieveFile(ftpFile.getName(), os);
                    LOG.info("File downloaded: {}", downloaded);
                } catch (Exception e) {
                    LOG.error("Error downloading file from ftp server", e);
                } finally {
                    if (os != null) {
                        os.close();
                    }
                }

            }

            client.logout();
        } catch (Exception e) {
            LOG.error("Error getting files from FTP: {}", e);
        } finally {
            try {
                client.disconnect();
            } catch (IOException e) {
                LOG.error("Error disconnecting FTP client: {}", e);
            }
        }
    }

    public static void downloadFilesFromDirectory(String serverName, String userName, String password, String workingDir, String destDir) {
        FTPClient client = new FTPClient();
        BufferedOutputStream os = null;
        try {
            client.connect(serverName);
            if (!client.login(userName, password)) {
                client.logout();
            }

            int reply = client.getReplyCode();
            //FTPReply stores a set of constants for FTP reply codes. 
            if (!FTPReply.isPositiveCompletion(reply)) {
                client.disconnect();
            }

            //enter passive mode
            client.enterLocalPassiveMode();
            client.setFileType(FTP.BINARY_FILE_TYPE);
            boolean changed = client.changeWorkingDirectory(workingDir);
            LOG.info("Changed: {}", changed);
            FTPFile[] ftpFiles = client.listFiles();

            File newFile = new File(destDir);
            newFile.mkdirs();
            if (ftpFiles != null && ftpFiles.length > 0) {
                for (FTPFile ftpFile : client.listFiles()) {
                    if (!ftpFile.isFile()) {
                        continue;
                    }

                    try {
                        os = new BufferedOutputStream(new FileOutputStream(destDir + "/" + ftpFile.getName()));
                        boolean downloaded = client.retrieveFile(ftpFile.getName(), os);
                        LOG.info("File downloaded: {}", downloaded);
                    } catch (Exception e) {
                        LOG.error("Error downloading file from ftp server", e);
                    } finally {
                        os.close();
                    }
                }
            }

            client.logout();
        } catch (Exception e) {
            LOG.error("Error getting files from FTP: {}", e);
        } finally {
            try {
                client.disconnect();
            } catch (IOException e) {
                LOG.error("Error disconnecting FTP client: {}", e);
            }
        }
    }

    public static boolean deleteFileInDirectory(String serverName, String userName, String password, String workingDir, String fileName) {
        boolean deletedFileFromFTP = false;
        FTPClient client = new FTPClient();
        try {
            client.connect(serverName);
            if (!client.login(userName, password)) {
                client.logout();
            }

            int reply = client.getReplyCode();
            //FTPReply stores a set of constants for FTP reply codes. 
            if (!FTPReply.isPositiveCompletion(reply)) {
                client.disconnect();
            }

            //enter passive mode
            client.enterLocalPassiveMode();
            client.changeWorkingDirectory(workingDir);
            deletedFileFromFTP = client.deleteFile(fileName);
            client.logout();
        } catch (Exception e) {
            LOG.error("Error deleting file from FTP: {}", e);
        } finally {
            try {
                client.disconnect();
            } catch (IOException e) {
                LOG.error("Error deleting file : {}", e);
            }
        }

        return deletedFileFromFTP;
    }
}
