/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 22-Jan-2013
 *  @author ajinkya
 */
package com.snapdeal.cocofs.utils;

import java.io.Serializable;

import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.ipms.admin.base.supcSellerNewAssociation.SUPCSellerNewAssociation;

public class ProducerThread implements Runnable {

    private static Session         session;

    private static MessageProducer messageProducer;

    private Logger                 LOG         = LoggerFactory.getLogger(ProducerThread.class);

    @Override
    public void run() {

        SUPCSellerNewAssociation publishObject = new SUPCSellerNewAssociation("121212", "121352", DateUtils.getCurrentTime());

        try {
            ObjectMessage obj = session.createObjectMessage((Serializable) publishObject);
            messageProducer.send(obj);
            System.out.println("sent");
        } catch (JMSException e) {
            LOG.error("Error in sending message : {}", e);
        }

    }

    public static Session getSession() {
        return session;
    }

    public static void setSession(Session session) {
        ProducerThread.session = session;
    }

    public static MessageProducer getMessageProducer() {
        return messageProducer;
    }

    public static void setMessageProducer(MessageProducer messageProducer) {
        ProducerThread.messageProducer = messageProducer;
    }

}
