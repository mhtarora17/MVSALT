/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 22-Jan-2013
 *  @author ajinkya
 */
package com.snapdeal.cocofs.utils;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.snapdeal.ipms.base.common.ActiveMQConstants;

public class ProducerExecutor {

    private static Logger LOG              = LoggerFactory.getLogger(ProducerExecutor.class);

    public static int     mapCounter       = 0;

    private static int    threadRunCounter = 0;

    public static void main(String[] args) throws InterruptedException {

        int threadPoolSize = 1;
        int threshold = 1;

        //register publisher
        String queueName = ActiveMQConstants.SUPC_SELLER_NEW_ASSOCIATION_QUEUE_NAME;
        String url = "failover:(tcp://10.125.1.150:26001)";
        String userName = ActiveMQConstants.DEFAULT_USERNAME;
        String password = ActiveMQConstants.DEFAULT_PASSWORD;

        ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(url);
        Connection connection = null;
        Session session = null;
        Destination queue = null;
        MessageProducer messProducer = null;
        try {
            connection = factory.createConnection(userName, password);
            connection.start();
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            queue = session.createQueue(queueName);
            messProducer = session.createProducer(queue);
            messProducer.setDeliveryMode(DeliveryMode.PERSISTENT);
        } catch (JMSException e) {
            LOG.error("Could not create connection : {}", e);
        }

        ProducerThread.setSession(session);
        ProducerThread.setMessageProducer(messProducer);

        ExecutorService executor = Executors.newFixedThreadPool(threadPoolSize);
        for (int i = 0; i < threshold; i++) {
            executor.execute(new ProducerThread());
        }

        executor.shutdown();

        //You may need to change this time to termination if running a long task
        executor.awaitTermination(1, TimeUnit.DAYS);

        if (executor.isTerminated()) {
            try {
                connection.close();
            } catch (JMSException e) {
                e.printStackTrace();
            }
        }
    }

    public synchronized static int getAndUpdateThreadRunCounter() {

        threadRunCounter++;

        return threadRunCounter - 1;
    }

}
