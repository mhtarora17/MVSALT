/*
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 28-Mar-2014
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.utils;

import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.mongo.model.ProductAttributeUnit;

public class DangerousGoodsTypeConversionUtil {

    public static String getDangerousGoodsType(String dangerousGoodsType, boolean hazMat, boolean liquid) {
        if (StringUtils.isNotEmpty(dangerousGoodsType)) {
            return dangerousGoodsType;
        }
        return getDangerousGoodsTypeByHazmatAndLiquidValues(hazMat, liquid);
    }

    public static String getDangerousGoodsType(ProductAttributeUnit unit) {
        return getDangerousGoodsType(unit.getDangerousGoodsType(), unit.isHazMat(), unit.isLiquid());
    }

    public static String getDangerousGoodsTypeByHazmatAndLiquidValues(boolean hazMat, boolean liquid) {
        String dangerousGoodsType;
        if (hazMat && liquid) {
            dangerousGoodsType = ConfigUtils.getStringScalar(Property.DANGEROUS_GOODS_TYPE_LIQUID_HAZMAT);
        } else if (hazMat) {
            dangerousGoodsType = ConfigUtils.getStringScalar(Property.DANGEROUS_GOODS_TYPE_HAZMAT);
        } else if (liquid) {
            dangerousGoodsType = ConfigUtils.getStringScalar(Property.DANGEROUS_GOODS_TYPE_LIQUID);
        } else {
            dangerousGoodsType = ConfigUtils.getStringScalar(Property.DANGEROUS_GOODS_TYPE_DEFAULT_VALUE);
        }
        return dangerousGoodsType;

    }

    public static boolean checkIfHazmat(String dangerousGoodsType) {
        return ConfigUtils.getStringList(Property.HAZMAT_DANGEROUS_GOODS_TYPES).contains(dangerousGoodsType);
    }

    public static boolean checkIfLiquid(String dangerousGoodsType) {
        return ConfigUtils.getStringList(Property.LIQUID_DANGEROUS_GOODS_TYPES).contains(dangerousGoodsType);
    }

    public static boolean isHazmat(ProductAttributeUnit unit) {
        return isHazmat(unit.getDangerousGoodsType(), unit.isHazMat());
    }

    private static boolean isHazmat(String dangerousGoodsType, boolean hazMat) {
        return StringUtils.isEmpty(dangerousGoodsType) ? hazMat : checkIfHazmat(dangerousGoodsType);
    }

    public static boolean isLiquid(ProductAttributeUnit unit) {
        return isLiquid(unit.getDangerousGoodsType(), unit.isLiquid());
    }

    private static boolean isLiquid(String dangerousGoodsType, boolean liquid) {
        return StringUtils.isEmpty(dangerousGoodsType) ? liquid : checkIfLiquid(dangerousGoodsType);
    }
    
    public static boolean isValidDangerousGoodsType(String dangerousGoodsType){
        return ConfigUtils.getStringList(Property.DANGEROUS_GOODS_TYPE).contains(dangerousGoodsType);
    }

}
