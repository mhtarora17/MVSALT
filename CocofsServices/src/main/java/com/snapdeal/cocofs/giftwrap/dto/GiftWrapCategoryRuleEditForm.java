/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.giftwrap.dto;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @version 1.0, 21-Jul-2014
 * @author ankur
 */
@XmlRootElement
public class GiftWrapCategoryRuleEditForm {

    private Integer ruleId; 
    private String  name;
    private String   ruleCode;
    private String  updatedBy;
    private String  ruleType;
    private Date    updateTime;
    private String categoryUrl;
    private String fragile;
    private Double price;
    private Double volume;
    private Double  weight;
    private Double priceTo;
    private Double volumeTo;
    private Double  weightTo;
    private String priceOperator;
    private String volumeOperator;
    private String  weightOperator;
    private List<String> dangerousGoodsType;

    private String    startDate;
    private String    endDate;
    private Boolean enabled;

    
    
    public Integer getRuleId() {
        return ruleId;
    }

    public void setRuleId(Integer ruleId) {
        this.ruleId = ruleId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    

    public String getRuleCode() {
        return ruleCode;
    }

    public void setRuleCode(String ruleCode) {
        this.ruleCode = ruleCode;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getRuleType() {
        return ruleType;
    }

    public void setRuleType(String ruleType) {
        this.ruleType = ruleType;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getVolume() {
        return volume;
    }

    public void setVolume(Double volume) {
        this.volume = volume;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

   

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
    
    
    public String getCategoryUrl() {
        return categoryUrl;
    }

    public void setCategoryUrl(String categoryUrl) {
        this.categoryUrl = categoryUrl;
    }
    
    

    public String getFragile() {
        return fragile;
    }

    public void setFragile(String fragile) {
        this.fragile = fragile;
    }
    
    

    public Double getPriceTo() {
        return priceTo;
    }

    public void setPriceTo(Double priceTo) {
        this.priceTo = priceTo;
    }

    public Double getVolumeTo() {
        return volumeTo;
    }

    public void setVolumeTo(Double volumeTo) {
        this.volumeTo = volumeTo;
    }

    public Double getWeightTo() {
        return weightTo;
    }

    public void setWeightTo(Double weightTo) {
        this.weightTo = weightTo;
    }

    public String getPriceOperator() {
        return priceOperator;
    }

    public void setPriceOperator(String priceOperator) {
        this.priceOperator = priceOperator;
    }

    public String getVolumeOperator() {
        return volumeOperator;
    }

    public void setVolumeOperator(String volumeOperator) {
        this.volumeOperator = volumeOperator;
    }

    public String getWeightOperator() {
        return weightOperator;
    }

    public void setWeightOperator(String weightOperator) {
        this.weightOperator = weightOperator;
    }
    
    

    public List<String> getDangerousGoodsType() {
        return dangerousGoodsType;
    }

    public void setDangerousGoodsType(List<String> dangerousGoodsType) {
        this.dangerousGoodsType = dangerousGoodsType;
    }

    @Override
    public String toString() {
        return "GiftWrapRuleEditDTO [name=" + name + ", updatedBy=" + updatedBy + ", ruleType=" + ruleType + ", updateTime=" + updateTime + ", price=" + price + ", volume="
                + volume + ", weight=" + weight + ", startDate=" + startDate + ", endDate=" + endDate + ", enabled=" + enabled + ", category="+ categoryUrl +"]";
    }

}
