/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.giftwrap.dto;

import java.util.ArrayList;

/**
 * @version 1.0, 23-Jul-2014
 * @author ankur
 */
public class GiftWrapEvaluationParams {

    private boolean      fragile;
    private double       weight;
    private double       volume;
    private double       price;
    private ArrayList<String> dangerousGoodsType;

    public boolean isFragile() {
        return fragile;
    }

    public boolean getFragile() {
        return fragile;
    }

    public void setFragile(boolean fragile) {
        this.fragile = fragile;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getVolume() {
        return volume;
    }

    public void setVolume(double volume) {
        this.volume = volume;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public ArrayList<String> getDangerousGoodsType() {
        return dangerousGoodsType;
    }

    public void setDangerousGoodsType(ArrayList<String> dangerousGoodsType) {
        this.dangerousGoodsType = dangerousGoodsType;
    }

    @Override
    public String toString() {
        return "GiftWrapEvaluationParams [fragile=" + fragile + ", weight=" + weight + ", volume=" + volume + ", price=" + price + ", dangerousGoodsType=" + dangerousGoodsType
                + "]";
    }
    
    

}
