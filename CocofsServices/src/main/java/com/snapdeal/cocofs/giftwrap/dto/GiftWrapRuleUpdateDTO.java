package com.snapdeal.cocofs.giftwrap.dto;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.snapdeal.cocofs.common.CriteriaDTO;
import com.snapdeal.cocofs.enums.BlockName;
import com.snapdeal.rule.dto.RuleParamsDTO;

public class GiftWrapRuleUpdateDTO {

    private Integer            id;

    private String             name;

    private String             ruleCode;
    
    private String             paramValue; 

    private Set<CriteriaDTO>   criterion  = new HashSet<CriteriaDTO>();

    private Set<RuleParamsDTO> ruleParams = new HashSet<RuleParamsDTO>();

    private Date               startDate;

    private Date               endDate;

    private BlockName          blockName;

    private String             updatedBy;

    private String             remarks;

    private Integer            priority;

    private Date               created;

    private Boolean            enabled;

    private Boolean            banned;

    public GiftWrapRuleUpdateDTO() {

    }

    public GiftWrapRuleUpdateDTO(Integer id, String name, Set<CriteriaDTO> criterion, Set<RuleParamsDTO> ruleParams, Date endDate, BlockName blockName) {
        super();
        this.id = id;
        this.name = name;
        this.criterion = criterion;
        this.ruleParams = ruleParams;
        this.endDate = endDate;
        this.blockName = blockName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    
    

    

    public String getParamValue() {
        return paramValue;
    }

    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }

    public Set<CriteriaDTO> getCriterion() {
        return criterion;
    }

    public void setCriterion(Set<CriteriaDTO> criterion) {
        this.criterion = criterion;
    }

    public Set<RuleParamsDTO> getRuleParams() {
        return ruleParams;
    }

    public void setRuleParams(Set<RuleParamsDTO> ruleParams) {
        this.ruleParams = ruleParams;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public BlockName getBlockName() {
        return blockName;
    }

    public void setBlockName(BlockName blockName) {
        this.blockName = blockName;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getRuleCode() {
        return ruleCode;
    }

    public void setRuleCode(String ruleCode) {
        this.ruleCode = ruleCode;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Boolean getBanned() {
        return banned;
    }

    public void setBanned(Boolean banned) {
        this.banned = banned;
    }

    @Override
    public String toString() {
        return "GiftWrapRuleUpdateDTO [id=" + id + ", name=" + name + ", ruleCode=" + ruleCode + ", criterion=" + criterion + ", ruleParams=" + ruleParams + ", startDate="
                + startDate + ", endDate=" + endDate + ", blockName=" + blockName + ", updatedBy=" + updatedBy + ", remarks=" + remarks + ", priority=" + priority + ", created="
                + created + ", enabled=" + enabled + ", banned=" + banned + "]";
    }

}
