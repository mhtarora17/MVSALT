/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.giftwrap.converter;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.snapdeal.base.cache.CacheManager;
import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.cocofs.common.CocofsRuleParam;
import com.snapdeal.cocofs.common.CriteriaDTO;
import com.snapdeal.cocofs.enums.BlockName;
import com.snapdeal.cocofs.enums.GiftWrapCategoryServiceabilityRuleCriteria;
import com.snapdeal.cocofs.giftwrap.dto.GiftWrapCategoryRuleCreateForm;
import com.snapdeal.cocofs.giftwrap.dto.GiftWrapRuleUpdateDTO;
import com.snapdeal.rule.cache.BlocksCache;
import com.snapdeal.rule.dto.RuleParamsDTO;
import com.snapdeal.rule.engine.entity.Criteria;
import com.snapdeal.rule.engine.entity.Operators;
import com.snapdeal.rule.engine.entity.Rule;
import com.snapdeal.rule.engine.entity.RuleParams;
import com.snapdeal.rule.engine.entity.ValueTypes;

/**
 *  
 *  @version     1.0, 18-Jul-2014
 *  @author ankur
 */
public class GiftWrapRuleCreateFormConverter {
    
    
    public static final String DISABLED_RULE_CODE = "Disabled-Rule";
    
    public static List<GiftWrapRuleUpdateDTO> getRuleDTOFromCategoryRuleCreateForm(GiftWrapCategoryRuleCreateForm form, List<Rule> rules) {
        Set<CriteriaDTO> criteriaDTO = getCritieriaDTO(form);
        List<GiftWrapRuleUpdateDTO> rulesDTOs = getRuleUpdateDTO(form, criteriaDTO, rules);
        return rulesDTOs;
    }
    
    private static List<GiftWrapRuleUpdateDTO> getRuleUpdateDTO(GiftWrapCategoryRuleCreateForm form, Set<CriteriaDTO> criteriaDTO, List<Rule> rules) {
        List<GiftWrapRuleUpdateDTO> rulesDTO = getRulesDTO(form, criteriaDTO, rules);
        return rulesDTO;
    }

    private static List<GiftWrapRuleUpdateDTO> getRulesDTO(GiftWrapCategoryRuleCreateForm form, Set<CriteriaDTO> criteriaDTO, List<Rule> rules) {

        List<GiftWrapRuleUpdateDTO> finalRulesDTOs = new ArrayList<GiftWrapRuleUpdateDTO>();
        List<GiftWrapRuleUpdateDTO> categoryWiseRulesDTOs = null;

        categoryWiseRulesDTOs = new ArrayList<GiftWrapRuleUpdateDTO>(0);

        categoryWiseRulesDTOs = updateRuleUpdateDTO(rules, form.getCategoryUrl(), form, criteriaDTO);
        finalRulesDTOs.addAll(categoryWiseRulesDTOs);

        List<GiftWrapRuleUpdateDTO> toBeDisabledRulesDTO = markRemainingRulesAsDisabled(rules, form.getUserEmail());
        finalRulesDTOs.addAll(toBeDisabledRulesDTO);
        return finalRulesDTOs;
    }
    
  
    private static Set<CriteriaDTO> getCritieriaDTO(GiftWrapCategoryRuleCreateForm form) {
        Set<CriteriaDTO> criteriaDTOs = new HashSet<CriteriaDTO>();
        
       

        if (StringUtils.isNotEmpty(form.getPriceOperator())) {
            criteriaDTOs.add(getPriceCriteriaDTO(form));
        }
        if (StringUtils.isNotEmpty(form.getVolumeOperator())) {
            criteriaDTOs.add(getVolumeCriteriaDTO(form));
        }
        if (StringUtils.isNotEmpty(form.getWeightOperator())) {
            criteriaDTOs.add(getWeightCriteriaDTO(form));
        }

       
        
        if (null != form.getDangerousGoodsType() && form.getDangerousGoodsType().size() > 0) {
            criteriaDTOs.add(getDangerousGoodsTypeCriteriaDTO(form));
        }
        
        if (null != form.isFragile()) {
            criteriaDTOs.add(getFragileCriteriaDTO(form));
        }
        
        
        return criteriaDTOs;
    }
    
    private static CriteriaDTO getPriceCriteriaDTO(GiftWrapCategoryRuleCreateForm form) {
        CriteriaDTO dto = new CriteriaDTO();
        dto.setName(GiftWrapCategoryServiceabilityRuleCriteria.PRICE.getCode());
        dto.setOperator(Operators.getOperatorFromString(form.getPriceOperator()));
        dto.setValueType(ValueTypes.DOUBLE);
        if (Operators.BETWEEN.equals(dto.getOperator()) || Operators.NOT_BETWEEN.equals(dto.getOperator())) {
            List<Object> priceList = new ArrayList<Object>();
            priceList.add(Double.parseDouble(form.getPrice().trim()));
            priceList.add(Double.parseDouble(form.getPriceTo().trim()));
            dto.setValues(priceList);
        } else {
            dto.setValue(form.getPrice());
        }
        return dto;
    }

    private static CriteriaDTO getVolumeCriteriaDTO(GiftWrapCategoryRuleCreateForm form) {
        CriteriaDTO dto = new CriteriaDTO();
        dto.setName(GiftWrapCategoryServiceabilityRuleCriteria.VOLUME.getCode());
        dto.setOperator(Operators.getOperatorFromString(form.getVolumeOperator()));
        dto.setValueType(ValueTypes.DOUBLE);
        if (Operators.BETWEEN.equals(dto.getOperator()) || Operators.NOT_BETWEEN.equals(dto.getOperator())) {
            List<Object> volumeList = new ArrayList<Object>();
            volumeList.add(Double.parseDouble(form.getVolume().trim()));
            volumeList.add(Double.parseDouble(form.getVolumeTo().trim()));
            dto.setValues(volumeList);
        } else {
            dto.setValue(form.getVolume());
        }
        return dto;
    }

    private static CriteriaDTO getWeightCriteriaDTO(GiftWrapCategoryRuleCreateForm form) {
        CriteriaDTO dto = new CriteriaDTO();
        dto.setName(GiftWrapCategoryServiceabilityRuleCriteria.WEIGHT.getCode());
        dto.setOperator(Operators.getOperatorFromString(form.getWeightOperator()));
        dto.setValueType(ValueTypes.DOUBLE);
        if (Operators.BETWEEN.equals(dto.getOperator()) || Operators.NOT_BETWEEN.equals(dto.getOperator())) {
            List<Object> weightList = new ArrayList<Object>();
            weightList.add(Double.parseDouble((form.getWeight().trim())));
            weightList.add(Double.parseDouble(form.getWeightTo().trim()));
            dto.setValues(weightList);
        } else {
            dto.setValue(form.getWeight());
        }
        return dto;
    }
    
    private static CriteriaDTO getDangerousGoodsTypeCriteriaDTO(GiftWrapCategoryRuleCreateForm form) {
        CriteriaDTO attributesCriteriaDTO = new CriteriaDTO();
        attributesCriteriaDTO.setName(GiftWrapCategoryServiceabilityRuleCriteria.DANGEROUSGOODSTYPE.getCode());
        attributesCriteriaDTO.setOperator(Operators.MATCHES);
        attributesCriteriaDTO.setValues(new ArrayList<Object>(form.getDangerousGoodsType()));
        attributesCriteriaDTO.setValueType(ValueTypes.STRING);
        return attributesCriteriaDTO;
    }
    
   
    private static CriteriaDTO getFragileCriteriaDTO(GiftWrapCategoryRuleCreateForm form) {
        CriteriaDTO fragileCriteriaDTO = new CriteriaDTO();
        fragileCriteriaDTO.setName(GiftWrapCategoryServiceabilityRuleCriteria.FRAGILE.getCode());
        fragileCriteriaDTO.setOperator(Operators.EQUAL_TO);
        fragileCriteriaDTO.setValue(form.isFragile());
        fragileCriteriaDTO.setValueType(ValueTypes.BOOLEAN);
        return fragileCriteriaDTO;
    }
   
    private static List<GiftWrapRuleUpdateDTO> updateRuleUpdateDTO(List<Rule> rules, String categoryUrl, GiftWrapCategoryRuleCreateForm form, Set<CriteriaDTO> criteriaDTOs) {
        List<GiftWrapRuleUpdateDTO> ruleDTOs = new ArrayList<GiftWrapRuleUpdateDTO>();
        GiftWrapRuleUpdateDTO ruleDTO = null;
                Set<RuleParamsDTO> paramDTO = new HashSet<RuleParamsDTO>();
                paramDTO.add(new RuleParamsDTO(CocofsRuleParam.CATEGORY_URL.getName(), categoryUrl));
                ruleDTO = getRuleUpdateDtoEnrichedWithRuleParamsAndCriteria(rules, paramDTO, criteriaDTOs);

                ruleDTO.setName(form.getRuleName());
                ruleDTO.setBlockName(BlockName.GiftWrapCategoryRuleblock);
                ruleDTO.setParamValue(categoryUrl);

                ruleDTOs.add(ruleDTO);
         
        updateRuleInfoInDTO(ruleDTOs, form, new HashSet<CriteriaDTO>());
        return ruleDTOs;
    }

    
    public static GiftWrapRuleUpdateDTO getRuleUpdateDtoEnrichedWithRuleParamsAndCriteria(List<Rule> existingRules, Set<RuleParamsDTO> paramDTO, Set<CriteriaDTO> criteriaDTOs) {
        GiftWrapRuleUpdateDTO ruleDTO = new GiftWrapRuleUpdateDTO();
        Set<CriteriaDTO> toBeIncluedCriteriaDTOs = null;
        if (existingRules != null && !existingRules.isEmpty()) {
            //getMatchingRuleEarlier created
            Rule alreadyCreatedRule = getMatchingRuleWithRuleParamsDTO(existingRules, paramDTO);
            if (null != alreadyCreatedRule) {
                //filerRuleParamDTO
                enrichRuleParamDTO(paramDTO, alreadyCreatedRule.getRuleParams());
                //filerCriteriaDTO
                toBeIncluedCriteriaDTOs = cloneCategoryObject(criteriaDTOs);
                toBeIncluedCriteriaDTOs.addAll(filterAlreadyExistingCriteria(toBeIncluedCriteriaDTOs, alreadyCreatedRule));
                //set id of the rule
                ruleDTO.setId(alreadyCreatedRule.getId());
                ruleDTO.setCreated(alreadyCreatedRule.getCreated());
            } else {
                toBeIncluedCriteriaDTOs = criteriaDTOs;
            }
        } else {
            toBeIncluedCriteriaDTOs = criteriaDTOs;
        }
        ruleDTO.setRuleParams(paramDTO);
        ruleDTO.setCriterion(toBeIncluedCriteriaDTOs);
        return ruleDTO;
    }
    
    public static Rule getMatchingRuleWithRuleParamsDTO(List<Rule> ruleList, Set<RuleParamsDTO> ruleParamsDTO) {
        Rule alreadyCreatedRule = null;

        for (Rule rule : ruleList) {
            if (isRulesParamsMatch(ruleParamsDTO, rule.getRuleParams())) {
                alreadyCreatedRule = rule;
                break;
            }
        }
        ruleList.remove(alreadyCreatedRule);
        return alreadyCreatedRule;
    }

    public static boolean isRulesParamsMatch(Set<RuleParamsDTO> ruleParamsDTO, Set<RuleParams> ruleParams) {
        for (RuleParamsDTO dto : ruleParamsDTO) {
            for (RuleParams ruleParam : ruleParams) {
                if (ruleParam.getParamName().equals(dto.getParamName())) {
                    if (!ruleParam.getParamValue().equals(dto.getParamValue())) {
                        return false;
                    }
                }
            }
        }
        return true;
    }
    
    public static void enrichRuleParamDTO(Set<RuleParamsDTO> paramDTO, Set<RuleParams> ruleParams) {
        for (RuleParamsDTO dto : paramDTO) {
            for (RuleParams param : ruleParams) {
                if (param.getParamName().equals(dto.getParamName())) {
                    dto.setId(param.getId());
                    break;
                }
            }
        }

    }
    
    public static Set<CriteriaDTO> cloneCategoryObject(Set<CriteriaDTO> criteriaDTOs) {
        Set<CriteriaDTO> clonedDTOs = new HashSet<CriteriaDTO>();
        for (CriteriaDTO dto : criteriaDTOs) {
            clonedDTOs.add(new CriteriaDTO(dto));
        }
        return clonedDTOs;
    }
    
    public static Set<CriteriaDTO> filterAlreadyExistingCriteria(Set<CriteriaDTO> criteriaDTOs, Rule rule) {
        Set<CriteriaDTO> toBeAddedDTO = new HashSet<CriteriaDTO>();
        if (null != rule) {
            Set<Criteria> criterion = rule.getCriterion();
            if (null != criterion && !criterion.isEmpty()) {
                for (Criteria crit : criterion) {
                    isOldCriteriaNameExist(crit, criteriaDTOs); // set id if criteria is already existing onm this field
                    /*if (!isOldCriteriaNameExist(crit, criteriaDTOs)) {
                        CriteriaDTO dto = getCriteriaDTOByCriteria(crit);
                        dto.setEnabled(false);
                        toBeAddedDTO.add(dto);
                    }*/
                }
            }
        }
        return toBeAddedDTO;
    }
    
    private static boolean isOldCriteriaNameExist(Criteria oldCrit, Set<CriteriaDTO> criteriaDTOs) {
        if (null != criteriaDTOs && !criteriaDTOs.isEmpty()) {
            for (CriteriaDTO dto : criteriaDTOs) {
                if (oldCrit.getName().equals(dto.getName())) {
                    dto.setId(oldCrit.getId());
                    dto.setCreated(oldCrit.getCreated());
                    return true;
                }
            }
        }
        return false;
    }
    
    private static CriteriaDTO getCriteriaDTOByCriteria(Criteria crit) {
        CriteriaDTO dto = new CriteriaDTO();
        dto.setId(crit.getId());
        dto.setEnabled(crit.isEnabled());
        dto.setName(crit.getName());
        dto.setOperator(crit.getOperator());
        dto.setValue(crit.getValue());
        dto.setValueType(crit.getValueType());
        return dto;
    }
    
    public static void updateRuleInfoInDTO(List<GiftWrapRuleUpdateDTO> rulesDTOs, GiftWrapCategoryRuleCreateForm form, Set<CriteriaDTO> dtos) {

        String currentUsername = form.getUserEmail();

        for (GiftWrapRuleUpdateDTO dto : rulesDTOs) {

            if (StringUtils.isNotEmpty(form.getEndDate())) {
                dto.setEndDate(DateUtils.stringToDate(form.getEndDate(), "yyyy-MM-dd"));
            }

            if (StringUtils.isNotEmpty(form.getStartDate())) {
                dto.setStartDate(DateUtils.stringToDate(form.getStartDate(), "yyyy-MM-dd"));
            }

            dto.setRuleCode(getGiftWrapRuleCodeForCategoryAndType(form.getCategoryUrl(), form.getRuleType()));
            //dto.setRemarks(form.getRemarks());
            dto.setEnabled(form.isEnabled());
            dto.setUpdatedBy(currentUsername);
            dto.setPriority(0);
            dto.setBanned(true);
            dto.setCreated(DateUtils.getCurrentTime());
        }
    }

    public static String getGiftWrapRuleCodeForCategoryAndType(String categoryUrl, String type) {
        return type + "|" + categoryUrl;
    }
    
    public static List<GiftWrapRuleUpdateDTO> markRemainingRulesAsDisabled(List<Rule> rules, String currentUsername) {
        List<GiftWrapRuleUpdateDTO> toBeDisabledRulesDTO = new ArrayList<GiftWrapRuleUpdateDTO>();
        if (null != rules && !rules.isEmpty()) {
            for (Rule rule : rules) {
                GiftWrapRuleUpdateDTO dto = getGiftWrapRuleUpdateDTO(rule);
                dto.setEnabled(false);
                dto.setRuleCode(DISABLED_RULE_CODE);
                dto.setUpdatedBy(currentUsername);
                toBeDisabledRulesDTO.add(dto);
            }
        }
        return toBeDisabledRulesDTO;

    }
    
    public static GiftWrapRuleUpdateDTO getGiftWrapRuleUpdateDTO(Rule rule) {
        GiftWrapRuleUpdateDTO dto = new GiftWrapRuleUpdateDTO();
        dto.setId(rule.getId());
        dto.setName(rule.getName());
        dto.setBlockName(BlockName.valueOf(CacheManager.getInstance().getCache(BlocksCache.class).getBlockById(rule.getBlockId()).getName()));
        dto.setCreated(rule.getCreated());
        dto.setCriterion(getCriteriaDTOSet(rule.getCriterion()));
        dto.setRuleParams(getRuleParamsDTOSet(rule.getRuleParams()));
        dto.setEndDate(rule.getEndDate());
        dto.setPriority(rule.getPriority());
        dto.setRemarks(rule.getRemark());
        dto.setRuleCode(rule.getCode());
        dto.setStartDate(rule.getStartDate());
        dto.setBanned(rule.isBanned());
        return dto;
    }
    
    private static Set<CriteriaDTO> getCriteriaDTOSet(Set<Criteria> criterias) {
        Set<CriteriaDTO> criteriaDTOSet = new HashSet<CriteriaDTO>();
        for (Criteria crit : criterias) {
            criteriaDTOSet.add(getCriteriaDTOByCriteria(crit));
        }
        return criteriaDTOSet;
    }

    public static RuleParamsDTO getRuleParamsDTO(RuleParams ruleParams) {
        RuleParamsDTO dto = new RuleParamsDTO();
        dto.setId(ruleParams.getId());
        dto.setParamName(ruleParams.getParamName());
        dto.setParamValue(ruleParams.getParamValue());
        return dto;
    }

    private static Set<RuleParamsDTO> getRuleParamsDTOSet(Set<RuleParams> ruleParamsSet) {
        Set<RuleParamsDTO> ruleParamsDTOSet = new HashSet<RuleParamsDTO>();
        for (RuleParams ruleParams : ruleParamsSet) {
            ruleParamsDTOSet.add(getRuleParamsDTO(ruleParams));
        }
        return ruleParamsDTOSet;
    }

    
    
    
    
}
