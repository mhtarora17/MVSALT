/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.giftwrap.dto;

import java.util.List;

/**
 *  
 *  @version     1.0, 18-Jul-2014
 *  @author ankur
 */
public class GiftWrapCategoryRuleCreateForm {

    private String  ruleType;

    private String  ruleName;

    private String  startDate;

    private String  endDate;

    private boolean enabled;

    private String  categoryUrl;

    private String  priceOperator;

    private String  price;

    private String  priceTo;

    private String  weightOperator;

    private String  weight;

    private String  weightTo;

    private String  volumeOperator;

    private String  volume;

    private String  volumeTo;
    
    private List<String> dangerousGoodsType;
    
    private Boolean   fragile;
    
    private String previousRuleCode;
    
    private String userEmail;


    public String getRuleType() {
        return ruleType;
    }

    public void setRuleType(String ruleType) {
        this.ruleType = ruleType;
    }

    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    
    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getCategoryUrl() {
        return categoryUrl;
    }

    public void setCategoryUrl(String categoryUrl) {
        this.categoryUrl = categoryUrl;
    }

    public String getPriceOperator() {
        return priceOperator;
    }

    public void setPriceOperator(String priceOperator) {
        this.priceOperator = priceOperator;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPriceTo() {
        return priceTo;
    }

    public void setPriceTo(String priceTo) {
        this.priceTo = priceTo;
    }

    public String getWeightOperator() {
        return weightOperator;
    }

    public void setWeightOperator(String weightOperator) {
        this.weightOperator = weightOperator;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getWeightTo() {
        return weightTo;
    }

    public void setWeightTo(String weightTo) {
        this.weightTo = weightTo;
    }

    public String getVolumeOperator() {
        return volumeOperator;
    }

    public void setVolumeOperator(String volumeOperator) {
        this.volumeOperator = volumeOperator;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public String getVolumeTo() {
        return volumeTo;
    }

    public void setVolumeTo(String volumeTo) {
        this.volumeTo = volumeTo;
    }

    public List<String> getDangerousGoodsType() {
        return dangerousGoodsType;
    }

    public void setDangerousGoodsType(List<String> dangerousGoodsType) {
        this.dangerousGoodsType = dangerousGoodsType;
    }

    public Boolean isFragile() {
        return fragile;
    }

    public void setFragile(Boolean fragile) {
        this.fragile = fragile;
    }

    public String getPreviousRuleCode() {
        return previousRuleCode;
    }

    public void setPreviousRuleCode(String previousRuleCode) {
        this.previousRuleCode = previousRuleCode;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

   
    
}
