/**
 *  Copyright 2016 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.packaman.storecodeandpt.dto;

/**
 *  
 *  @version     1.0, 06-Jan-2016
 *  @author vikassharma03
 */
import javax.xml.bind.annotation.XmlRootElement;

/**
 *  
 *  @version     1.0, 06-Jan-2016
 *  @author vikassharma03
 */

@XmlRootElement
public class ScAndPtDownloadDto {

    String StoreCode;
    
    String PackagingType;
    
   


    public ScAndPtDownloadDto(String storeCode, String packagingType) {
        super();
        StoreCode = storeCode;
        PackagingType = packagingType;
    }


    public String getStoreCode() {
        return StoreCode;
    }

  
    public void setStoreCode(String storeCode) {
        StoreCode = storeCode;
    }

    public String getPackagingType() {
        return PackagingType;
    }

    public void setPackagingType(String packagingType) {
        PackagingType = packagingType;
    }

    @Override
    public String toString() {
        return "ScAndPtDownloadDTO [StoreCode=" + StoreCode + ", PackagingType=" + PackagingType + "]";
    }
    
    
    
    
}

