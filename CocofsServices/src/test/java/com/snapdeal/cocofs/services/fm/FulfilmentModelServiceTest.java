package com.snapdeal.cocofs.services.fm;

import static org.junit.Assert.assertEquals;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import javax.naming.OperationNotSupportedException;

import mockit.Expectations;
import mockit.Mocked;
import mockit.integration.junit4.JMockit;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.test.util.ReflectionTestUtils;

import com.snapdeal.base.cache.CacheManager;
import com.snapdeal.cocofs.cache.loader.impl.SellerFMMappingCache;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.core.aerospike.vo.SellerFMMappingVO;
import com.snapdeal.cocofs.dataaccess.fm.IFMAerospikeDataReadService;
import com.snapdeal.cocofs.dataaccess.fm.IFMDBDataReadService;
import com.snapdeal.cocofs.dataaccess.fm.impl.FMAerospikeDataReadServiceImpl;
import com.snapdeal.cocofs.dataaccess.fm.impl.FMDBDataReadServiceImpl;
import com.snapdeal.cocofs.entity.SellerFMMapping;
import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.generic.persister.exception.GenericPersisterException;
import com.snapdeal.cocofs.services.fm.dto.SellerFMMappingDTO;
import com.snapdeal.cocofs.services.fm.dto.SellerFmFcDTO;
import com.snapdeal.cocofs.services.fm.impl.FMDataSourceServiceFactory;
import com.snapdeal.cocofs.services.fm.impl.FMDataSourceServiceForAerospike;
import com.snapdeal.cocofs.services.fm.impl.FMDataSourceServiceForCache;
import com.snapdeal.cocofs.services.fm.impl.FMDataSourceServiceForMySql;
import com.snapdeal.cocofs.services.fm.impl.FulfilmentModelServiceImpl;

@RunWith(JMockit.class)
public class FulfilmentModelServiceTest {

	private static final String 			SELLER_CODE = "sellerCode";
	private static final String 			FULFILLMENT_MODEL = "fulfillmentModel";

	private IFulfilmentModelService			fulfilmentModelServiceImpl;
	private FMDataSourceServiceFactory		fmDataSourceFactory;
	
	private IFMDataSourceService			fmDataSourceServiceCache;
	private IFMDataSourceService			fmDataSourceServiceAerospike;
	private IFMDataSourceService			fmDataSourceServiceMySql;

	private IFMDBDataReadService			fmDBDataReadService;
	private IFMAerospikeDataReadService		fmAerospikeService;
	private SellerFMMappingCache			sellerFMMappingCache;

	private SellerFMMapping					sellerFMMapping;
	private SellerFMMappingVO				sellerFMMappingVO;
	private SellerFMMappingDTO				sellerFMMappingDTO;
	private SellerFmFcDTO					sellerFmFcDTO;

	@Mocked
	private ConfigUtils						configUtils;
	@Mocked
	private CacheManager					cacheManager;

	@Rule
	public ExpectedException exception = ExpectedException.none();

	@Before
	public void setUp() throws OperationNotSupportedException, GenericPersisterException, ExternalDataNotFoundException {
		fulfilmentModelServiceImpl = new FulfilmentModelServiceImpl();

		fmDataSourceFactory = new FMDataSourceServiceFactory();
		ReflectionTestUtils.setField(fulfilmentModelServiceImpl, "dataSourceService", fmDataSourceFactory);

		fmDataSourceServiceCache = new FMDataSourceServiceForCache();
		ReflectionTestUtils.setField(fmDataSourceServiceCache, "fmDataSource", fmDataSourceFactory);
		sellerFMMappingDTO = new SellerFMMappingDTO();
		sellerFmFcDTO = new SellerFmFcDTO();

		fmDataSourceServiceAerospike = new FMDataSourceServiceForAerospike();
		fmAerospikeService = mock(FMAerospikeDataReadServiceImpl.class);
		ReflectionTestUtils.setField(fmDataSourceServiceAerospike, "fmAerospikeService", fmAerospikeService);
		sellerFMMappingVO = new SellerFMMappingVO();

		fmDataSourceServiceMySql = new FMDataSourceServiceForMySql();
		fmDBDataReadService = mock(FMDBDataReadServiceImpl.class);
		ReflectionTestUtils.setField(fmDataSourceServiceMySql, "fmDBDataReadService", fmDBDataReadService);
		sellerFMMapping = new SellerFMMapping();

		ReflectionTestUtils.setField(fmDataSourceFactory, "fmDataSourceServiceForCache", fmDataSourceServiceCache);
		ReflectionTestUtils.setField(fmDataSourceFactory, "fmDataSourceServiceForAerospike", fmDataSourceServiceAerospike);
		ReflectionTestUtils.setField(fmDataSourceFactory, "fmDataSourceServiceForMySql", fmDataSourceServiceMySql);

		cacheManager = mock(CacheManager.class);
		sellerFMMappingCache= mock(SellerFMMappingCache.class);
	}

	@After
	public void tearDown() {
		fulfilmentModelServiceImpl = null; 
		fmDataSourceFactory = null;

		fmDataSourceServiceCache = null;
		sellerFMMappingDTO = null;

		fmDataSourceServiceAerospike = null;
		fmAerospikeService = null;
		sellerFMMappingVO = null;

		fmDataSourceServiceMySql = null;
		fmDBDataReadService = null;
		sellerFMMapping = null;

		cacheManager = null;
		sellerFMMappingCache = null;
	}

	@Test
	public void testGetFM_BySeller_FromMySql() throws ExternalDataNotFoundException{
		createDBExpectations("MYSQL", false);

		sellerFMMapping.setFulfilmentModel(FULFILLMENT_MODEL);
		sellerFMMapping.setEnabled(true);

		when(fmDBDataReadService.getSellerFMMapping(SELLER_CODE)).thenReturn(sellerFMMapping);

		assertEquals(FULFILLMENT_MODEL, fulfilmentModelServiceImpl.getFulfilmentModelBySeller(SELLER_CODE, false));

	}

	@Test
	public void testGetFM_ByNullSeller_FromMySql() throws ExternalDataNotFoundException{
		createDBExpectations("MYSQL", false);

		sellerFMMapping.setFulfilmentModel(FULFILLMENT_MODEL);
		sellerFMMapping.setEnabled(true);

		when(fmDBDataReadService.getSellerFMMapping(SELLER_CODE)).thenReturn(sellerFMMapping);

		exception.expect(ExternalDataNotFoundException.class);
		fulfilmentModelServiceImpl.getFulfilmentModelBySeller(null, false);
	}

	@Test
	public void testGetFM_BySeller_FromAerospike() throws ExternalDataNotFoundException{
		createDBExpectations("AEROSPIKE", false);

		sellerFMMappingVO.setFulfillmentModel(FULFILLMENT_MODEL);
		sellerFMMappingVO.setEnabled(0);

		when(fmAerospikeService.getFMMapping(SELLER_CODE, SellerFMMappingVO.class)).thenReturn(sellerFMMappingVO);

		assertEquals(FULFILLMENT_MODEL, fulfilmentModelServiceImpl.getFulfilmentModelBySeller(SELLER_CODE, false));

	}

	@Test
	public void testGetFM_ByNullSeller_FromAerospike() throws ExternalDataNotFoundException{
		createDBExpectations("AEROSPIKE", false);

		sellerFMMappingVO.setFulfillmentModel(FULFILLMENT_MODEL);
		sellerFMMappingVO.setEnabled(0);

		when(fmAerospikeService.getFMMapping(SELLER_CODE, SellerFMMappingVO.class)).thenReturn(sellerFMMappingVO);

		exception.expect(ExternalDataNotFoundException.class);
		fulfilmentModelServiceImpl.getFulfilmentModelBySeller(null, false);
	}

	@Test
	public void testGetFM_BySeller_FromCache() throws ExternalDataNotFoundException{
		createCacheExpectations("MYSQL", true);

		sellerFMMappingDTO.setFulfilmentModel(FULFILLMENT_MODEL);

		when(cacheManager.getCache(SellerFMMappingCache.class)).thenReturn(sellerFMMappingCache);
		when(sellerFMMappingCache.getSellerFmFcMapping(SELLER_CODE)).thenReturn(sellerFmFcDTO);

		assertEquals(FULFILLMENT_MODEL, fulfilmentModelServiceImpl.getFulfilmentModelBySeller(SELLER_CODE, true));
	}

	@Test
	public void testGetFM_ByNullSeller_FromCache() throws ExternalDataNotFoundException{
		createNotFoundInCacheExpectations_ByNullSeller("MYSQL", true);

		sellerFMMappingVO.setFulfillmentModel(FULFILLMENT_MODEL);
		sellerFMMappingVO.setEnabled(0);

		when(fmDBDataReadService.getSellerFMMapping(SELLER_CODE)).thenReturn(sellerFMMapping);

		exception.expect(ExternalDataNotFoundException.class);
		fulfilmentModelServiceImpl.getFulfilmentModelBySeller(null, true);
	}

	@Test
	public void testGetFM_BySeller_FromMySqlNotFoundInCache() throws ExternalDataNotFoundException{
		createNotFoundInCacheExpectations("MYSQL", true);

		sellerFMMapping.setFulfilmentModel(FULFILLMENT_MODEL);
		sellerFMMapping.setEnabled(true);

		when(cacheManager.getCache(SellerFMMappingCache.class)).thenReturn(sellerFMMappingCache);
		when(sellerFMMappingCache.getSellerFmFcMapping(SELLER_CODE)).thenReturn(null);
		when(fmDBDataReadService.getSellerFMMapping(SELLER_CODE)).thenReturn(sellerFMMapping);

		assertEquals(FULFILLMENT_MODEL, fulfilmentModelServiceImpl.getFulfilmentModelBySeller(SELLER_CODE, true));
	}

	private void createDBExpectations(final String string, final boolean bool) {
		new Expectations() {
			{
				ConfigUtils.getStringScalar(Property.DATA_SOURCE_FOR_FM_READ);
				result = string;
			}
		};
	}

	private void createNotFoundInCacheExpectations(final String string, final boolean bool) {
		new Expectations() {
			{
				ConfigUtils.getStringScalar(Property.DATA_SOURCE_FOR_FM_READ);
				result = string;

				ConfigUtils.getBooleanScalar(Property.ENABLE_SELLER_FM_MAPPING_CACHE);
				result = bool;

				ConfigUtils.getBooleanScalar(Property.ENABLE_SELLER_FM_MAPPING_CACHE);
				result = bool;

				CacheManager.getInstance();
				result = cacheManager;

				ConfigUtils.getStringScalar(Property.DATA_SOURCE_FOR_FM_READ);
				result = string;

				CacheManager.getInstance();
				result = cacheManager;
			}
		};
	}

	private void createNotFoundInCacheExpectations_ByNullSeller(final String string, final boolean bool) {
		new Expectations() {
			{
				ConfigUtils.getStringScalar(Property.DATA_SOURCE_FOR_FM_READ);
				result = string;

				ConfigUtils.getBooleanScalar(Property.ENABLE_SELLER_FM_MAPPING_CACHE);
				result = bool;
			}
		};
	}

	private void createCacheExpectations(final String string, final boolean bool) {
		new Expectations() {
			{
				ConfigUtils.getStringScalar(Property.DATA_SOURCE_FOR_FM_READ);
				result = string;

				ConfigUtils.getBooleanScalar(Property.ENABLE_SELLER_FM_MAPPING_CACHE);
				result = bool;

				ConfigUtils.getBooleanScalar(Property.ENABLE_SELLER_FM_MAPPING_CACHE);
				result = bool;

				CacheManager.getInstance();
				result = cacheManager;
			}
		};
	}

}

