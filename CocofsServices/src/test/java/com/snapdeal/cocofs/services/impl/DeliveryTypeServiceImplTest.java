package com.snapdeal.cocofs.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mockit.Injectable;
import mockit.Mocked;
import mockit.NonStrictExpectations;
import mockit.Tested;
import mockit.UsingMocksAndStubs;
import mockit.integration.logging.Slf4jMocks;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.snapdeal.catalog.base.model.GetMinProductInfoBySupcRequest;
import com.snapdeal.catalog.base.model.GetMinProductInfoResponse;
import com.snapdeal.catalog.base.sro.MinProductInfoSRO;
import com.snapdeal.cocofs.access.IProductAttributeService;
import com.snapdeal.cocofs.common.CocofsRuleBlock;
import com.snapdeal.cocofs.external.service.ICAMSExternalService;
import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.external.service.factory.IExternalServiceFactory;
import com.snapdeal.cocofs.mongo.CocofsMongoQuery;
import com.snapdeal.cocofs.mongo.mao.IGenericMao;
import com.snapdeal.cocofs.mongo.model.CocofsRuleUnit;
import com.snapdeal.cocofs.mongo.model.CriteriaUnit;
import com.snapdeal.cocofs.mongo.model.ProductAttributeUnit;
import com.snapdeal.cocofs.sro.DeliveryType;
import com.snapdeal.rule.engine.entity.BlockParams;
import com.snapdeal.rule.engine.entity.Operators;
import com.snapdeal.rule.engine.entity.ValueTypes;
import com.snapdeal.rule.engine.services.IRulesService;

@UsingMocksAndStubs(Slf4jMocks.class)
public class DeliveryTypeServiceImplTest {
    @Tested
    private DeliveryTypeServiceImpl          deliveryTypeService;
    @Injectable
    private IExternalServiceFactory  factory;
    @Injectable
    private IProductAttributeService productAttrService;
    @Injectable
    private IRulesService            ruleService;

    @Injectable
    private IGenericMao              genericMao;
    
    @Mocked
    private ICAMSExternalService     camsService;
    
    @Test
    public void getDeliveryTypeForSUPC_001() {
        final String supc = "DummySUPC";
        try {
            new NonStrictExpectations() {
                {
                    camsService.getMinProductInfo((GetMinProductInfoBySupcRequest) any);
                    GetMinProductInfoResponse minPInfo = getProdcutInfo();
                    result = minPInfo ;
                    
                    factory.getCAMSExternalService();
                    result = camsService;
                    
                    ProductAttributeUnit pau = new ProductAttributeUnit();
                    pau.setBreadth(50.0);
                    pau.setHeight(100.0);
                    pau.setLength(100.0);
                    pau.setLiquid(false);
                    pau.setHazMat(true);
                    pau.setFragile(false);
                    pau.setWeight(150.0);
                    pau.setSupc(supc);
                    productAttrService.getProductAttributeUnitBySUPC(supc);
                    result = pau;
                    
                    List<CocofsRuleUnit> cocoRuleList = getRuleList_001();
                    genericMao.getDocumentList((CocofsMongoQuery) any, CocofsRuleUnit.class);
                    result = cocoRuleList;
                    
                    List<BlockParams> blockPList = getBlockParams();
                    ruleService.getBlockParamsByBlockName(CocofsRuleBlock.DELIVERY_TYPE.getName());
                    result = blockPList;
                }
            };
        } catch (ExternalDataNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        
        Assert.assertTrue(deliveryTypeService.getDeliveryTypeForSUPC(supc).getDeliveryType().equals(DeliveryType.NORMAL));
    }
    
    private List<CocofsRuleUnit> getRuleList_001() {
        List<CocofsRuleUnit> cocoRuleList = new ArrayList<CocofsRuleUnit>();
        CocofsRuleUnit r = new CocofsRuleUnit();
        r.setCategoryUrl("XYZ");
        r.setCreated(new Date());
        r.setEnabled(true);
        r.setId(1);
        r.setName("Dummy_NAME_1");
        r.setPriority(0);
        r.setType(CocofsRuleBlock.DELIVERY_TYPE.getName());
        r.setStartDate(r.getCreated());
        r.setUpdated(r.getCreated());
        List<CriteriaUnit> criteria = new ArrayList<CriteriaUnit>();
        CriteriaUnit u1 = new CriteriaUnit();
        u1.setId(1);
        u1.setCreated(r.getCreated());
        u1.setEnabled(true);
        u1.setName("FRAGILE");
        u1.setUpdated(r.getCreated());
        u1.setValue("false");
        u1.setValueType(ValueTypes.BOOLEAN.toString());
        u1.setOperator(Operators.EQUAL_TO.getCode());
        criteria.add(u1);
        
        r.setCriteria(criteria );
        cocoRuleList.add(r );
        return cocoRuleList;
    }
    
    private GetMinProductInfoResponse getProdcutInfo() {
        GetMinProductInfoResponse minPInfo = new GetMinProductInfoResponse();
        minPInfo.setSuccessful(true);
        MinProductInfoSRO productInfo = new MinProductInfoSRO();
        productInfo.setCategoryUrl("XYZ");
        
        minPInfo.setProductInfo(productInfo );
        return minPInfo;
    }
    private List<BlockParams> getBlockParams() {
        List<BlockParams> blockPList = new ArrayList<BlockParams>();
        BlockParams bp1 = new BlockParams();
        bp1.setBlock(null);
        bp1.setCreated(new Date());
        bp1.setCreatedBy("Fool");
        bp1.setFieldValue("Fragile");
        bp1.setId(1);
        bp1.setUpdated(new Date());
        bp1.setParamName("FRAGILE");
        
        BlockParams bp2 = new BlockParams();
        bp2.setBlock(null);
        bp2.setCreated(new Date());
        bp2.setCreatedBy("Fool");
        bp2.setFieldValue("HazMat");
        bp2.setId(1);
        bp2.setUpdated(new Date());
        bp2.setParamName("HAZMAT");
        
        BlockParams bp3 = new BlockParams();
        bp3.setBlock(null);
        bp3.setCreated(new Date());
        bp3.setCreatedBy("Fool");
        bp3.setFieldValue("Liquid");
        bp3.setId(1);
        bp3.setUpdated(new Date());
        bp3.setParamName("LIQUID");
        
        BlockParams bp4 = new BlockParams();
        bp4.setBlock(null);
        bp4.setCreated(new Date());
        bp4.setCreatedBy("Fool");
        bp4.setFieldValue("Weight");
        bp4.setId(1);
        bp4.setUpdated(new Date());
        bp4.setParamName("WEIGHT");
        
        BlockParams bp5 = new BlockParams();
        bp5.setBlock(null);
        bp5.setCreated(new Date());
        bp5.setCreatedBy("Fool");
        bp5.setFieldValue("VolumetricWeight");
        bp5.setId(1);
        bp5.setUpdated(new Date());
        bp5.setParamName("VOLWEIGHT");
        blockPList.add(bp1);
        blockPList.add(bp2);
        blockPList.add(bp3);
        blockPList.add(bp4);
        blockPList.add(bp5);
        
        return blockPList;
    }

}
