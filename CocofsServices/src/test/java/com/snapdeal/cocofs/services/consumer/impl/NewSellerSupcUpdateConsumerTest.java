/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.services.consumer.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.jms.JMSException;
import javax.jms.ObjectMessage;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.util.ReflectionTestUtils;

import com.snapdeal.base.activemq.IActiveMQManager;
import com.snapdeal.cocofs.access.IPendingProductAttributeDBDataReadService;
import com.snapdeal.cocofs.access.IProductAttributeNotificationMongoDataReadService;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.services.consumer.INewSellerSupcUpdateConsumer;
import com.snapdeal.cocofs.services.fm.IFulfilmentModelService;
import com.snapdeal.cocofs.services.fm.IManualPushOfSellerFMUpdateForExternalNotification;
import com.snapdeal.cocofs.services.fm.ISellerToSUPCListDBLookupService;
import com.snapdeal.cocofs.services.impl.SellerSUPCFMMappingProcessorImpl;
import com.snapdeal.cocofs.services.impl.SellerSUPCFMMappingProcessorImpl.IDependency;
import com.snapdeal.cocofs.services.producer.IExternalNotificationsServiceForSearchPush;
import com.snapdeal.cocofs.sro.FulfillmentModel;
import com.snapdeal.ipms.base.queue.update.SUPCSellerUpdateEventSRO;
import com.snapdeal.ipms.base.queue.update.SUPCSellerUpdateEventsSRO;
import com.snapdeal.score.data.aerospike.exception.PutFailedException;

/**
 * @version 1.0, 03-Dec-2014
 * @author shiv
 */
public class NewSellerSupcUpdateConsumerTest {

    //class under test
    INewSellerSupcUpdateConsumer               cut                            = null;

    //dependencies
    IManualPushOfSellerFMUpdateForExternalNotification              mSellerSUPCFMMappingProcessor  = null;
    IFulfilmentModelService                    mFulfilmentModelService        = null;
   
    @Qualifier("externalNotificationsServiceForSearchPush")
    IExternalNotificationsServiceForSearchPush mSellerSupcSDFulfilledProducer = null;
    
    ISellerToSUPCListDBLookupService           mSellerToSUPCListLookupService = null;
    IDependency                                mDependency                    = null;

    @Before
    public void setUp() {
        cut = new NewSellerSupcUpdateConsumer();

        IActiveMQManager mockIActiveMQManager = mock(IActiveMQManager.class);
        ReflectionTestUtils.setField(cut, "activeMQManager", mockIActiveMQManager);
        mSellerSUPCFMMappingProcessor = new SellerSUPCFMMappingProcessorImpl();
        ReflectionTestUtils.setField(cut, "sellerSUPCFMMappingProcessor", mSellerSUPCFMMappingProcessor);

        IPendingProductAttributeDBDataReadService ppaDBDataReadService = mock(IPendingProductAttributeDBDataReadService.class);
        ReflectionTestUtils.setField(cut, "ppaDBDataReadService", ppaDBDataReadService);
        IProductAttributeNotificationMongoDataReadService panMongoDataReadService = mock(IProductAttributeNotificationMongoDataReadService.class);
        ReflectionTestUtils.setField(cut, "panMongoDataReadService", panMongoDataReadService);

        mDependency = mock(IDependency.class);
        when(mDependency.getBooleanPropertyValue(any(Property.class))).thenReturn(Boolean.TRUE);
        ReflectionTestUtils.setField(mSellerSUPCFMMappingProcessor, "dep", mDependency);
        mSellerToSUPCListLookupService = mock(ISellerToSUPCListDBLookupService.class);
        ReflectionTestUtils.setField(mSellerSUPCFMMappingProcessor, "sellerToSUPCListLookupService", mSellerToSUPCListLookupService);
        mFulfilmentModelService = mock(IFulfilmentModelService.class);
        ReflectionTestUtils.setField(mSellerSUPCFMMappingProcessor, "fulfilmentModelService", mFulfilmentModelService);
        mSellerSupcSDFulfilledProducer = mock(IExternalNotificationsServiceForSearchPush.class);
        ReflectionTestUtils.setField(mSellerSUPCFMMappingProcessor, "sellerSupcSDFulfilledProducer", mSellerSupcSDFulfilledProducer);

    }

    @After
    public void tearDown() {
        cut = null;
    }

    @Test
    public void testOnMessageVerifyNoOperationWhenSOIIsDisabled() throws JMSException, PutFailedException, ExternalDataNotFoundException {

        //setup
        SUPCSellerUpdateEventsSRO eventsObj = getEventPayloadObject();

        ObjectMessage msg = mock(ObjectMessage.class);
        when(msg.getObject()).thenReturn(eventsObj);
        when(mDependency.getBooleanPropertyValue(any(Property.class))).thenReturn(Boolean.FALSE);

        //action
        cut.onMessage(msg);

        //verification
        verify(mFulfilmentModelService, times(0)).getFulfilmentModel(any(String.class), any(String.class));
        //        verify(mSellerToSUPCListLookupService, times(0)).putNewSUPCSetInSellerSUPCLDTListForSeller(any(SellerToSupcListVO.class), any(String.class), any(Set.class));
        verify(mSellerSupcSDFulfilledProducer, times(0)).publishToSearch(any(List.class));
    }

    @Test
    public void testOnMessageVerifyPushNotificationWhenFMIsFC_VOI() throws JMSException, PutFailedException, ExternalDataNotFoundException {

        //setup
        SUPCSellerUpdateEventsSRO eventsObj = getEventPayloadObject();

        ObjectMessage msg = mock(ObjectMessage.class);
        when(msg.getObject()).thenReturn(eventsObj);

        when(mFulfilmentModelService.getFulfilmentModel(any(String.class), any(String.class))).thenReturn(FulfillmentModel.FC_VOI);

        //action
        cut.onMessage(msg);

        //verification
        verify(mFulfilmentModelService, times(1)).getFulfilmentModel(any(String.class), any(String.class));
        //        verify(mSellerToSUPCListLookupService, times(1)).putNewSUPCSetInSellerSUPCLDTListForSeller(any(SellerToSupcListVO.class), any(String.class), any(Set.class));
        verify(mSellerSupcSDFulfilledProducer, times(1)).publishToSearch(any(List.class));
    }

    @Test
    public void testOnMessageNoPushNotificationWhenFMIsDS() throws JMSException, ExternalDataNotFoundException, PutFailedException {

        //setup
        SUPCSellerUpdateEventsSRO eventsObj = getEventPayloadObject();

        ObjectMessage msg = mock(ObjectMessage.class);
        when(msg.getObject()).thenReturn(eventsObj);
        when(mFulfilmentModelService.getFulfilmentModel(any(String.class), any(String.class))).thenReturn(FulfillmentModel.DROPSHIP);

        //action
        cut.onMessage(msg);

        //verification
        verify(mFulfilmentModelService, times(1)).getFulfilmentModel(any(String.class), any(String.class));
        //        verify(mSellerToSUPCListLookupService, times(1)).putNewSUPCSetInSellerSUPCLDTListForSeller(any(SellerToSupcListVO.class), any(String.class), any(Set.class));
        verify(mSellerSupcSDFulfilledProducer, times(0)).publishToSearch(any(List.class));
    }

    private SUPCSellerUpdateEventsSRO getEventPayloadObject() {
        List<SUPCSellerUpdateEventSRO> sucpSellerUpdateEventList = new ArrayList<SUPCSellerUpdateEventSRO>();
        SUPCSellerUpdateEventsSRO eventsObj = new SUPCSellerUpdateEventsSRO(sucpSellerUpdateEventList);

        String SELLER = "ABCDEF";
        String SUPC = "SDL_ABCDEF";
        Date updatedAt = new Date();
        SUPCSellerUpdateEventSRO o1 = new SUPCSellerUpdateEventSRO(SUPC, SELLER, updatedAt);
        sucpSellerUpdateEventList.add(o1);
        return eventsObj;
    }

}
