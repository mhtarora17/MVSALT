/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.services.jobprocessor;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.OperationNotSupportedException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.util.ReflectionTestUtils;

import com.snapdeal.cocofs.common.BulkUploadValidationResponse;
import com.snapdeal.cocofs.common.JobProcessorResponse;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.core.aerospike.vo.SellerSupcFMMappingVO;
import com.snapdeal.cocofs.dataaccess.fm.IFMAerospikeDataReadService;
import com.snapdeal.cocofs.dataaccess.fm.IFMDBDataReadService;
import com.snapdeal.cocofs.entity.JobDetail;
import com.snapdeal.cocofs.entity.JobStatus;
import com.snapdeal.cocofs.entity.SellerSupcFMMapping;
import com.snapdeal.cocofs.enums.JobActionCode;
import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.generic.persister.exception.GenericPersisterException;
import com.snapdeal.cocofs.generic.persister.response.GenericPersisterWithAerospikeResponse;
import com.snapdeal.cocofs.services.IJobProcessor;
import com.snapdeal.cocofs.services.data.engine.IDataEngineWithAerospike;
import com.snapdeal.cocofs.services.fm.IFMMappingDataUpdater;
import com.snapdeal.cocofs.services.fm.IFulfilmentModelService;
import com.snapdeal.cocofs.services.fm.IManualPushOfSellerFMUpdateForExternalNotification;
import com.snapdeal.cocofs.services.fm.ISellerToSUPCListDBLookupService;
import com.snapdeal.cocofs.services.fm.dto.SellerSupcFMMappingUploadDto;
import com.snapdeal.cocofs.services.impl.SellerSUPCFMMappingProcessorImpl;
import com.snapdeal.cocofs.services.impl.SellerSUPCFMMappingProcessorImpl.IDependency;
import com.snapdeal.cocofs.services.producer.IExternalNotificationsServiceForSearchPush;
import com.snapdeal.cocofs.sro.FulfillmentModel;
import com.snapdeal.cocofs.sro.SellerSupcUpdateSRO;
import com.snapdeal.score.data.aerospike.exception.GetFailedException;
import com.snapdeal.score.data.aerospike.exception.PutFailedException;
import com.snapdeal.score.data.aerospike.exception.RemoveFailedException;
import com.sun.media.sound.InvalidDataException;

/**
 * @version 1.0, 04-Dec-2014
 * @author shiv
 */
public class SellerSupcFMMappingUploadProcessorTest {

    private IJobProcessor                                                                            cut                              = null;
    private IFMMappingDataUpdater                                                                    dataUpdater;
    private IFMDBDataReadService                                                                     fmDBDataReadService;
    private IFMAerospikeDataReadService                                                              fmAerospikeService;
    private IManualPushOfSellerFMUpdateForExternalNotification                                                            sellerSUPCFMMappingProcessor;
    private IFulfilmentModelService                                                                  fulfilmentService;
    private IDataEngineWithAerospike//
    <SellerSupcFMMappingUploadDto, SellerSupcFMMapping, //
    SellerSupcFMMappingVO>                                                                          //       
                                                                                                     dataEngine;
    private com.snapdeal.cocofs.services.jobprocessor.SellerSupcFMMappingUploadProcessor.IDependency dep                              = null;
    private IDependency                                                                              mDependencyProcessor             = null;

    @Qualifier("externalNotificationsServiceForSearchPush")
    IExternalNotificationsServiceForSearchPush                                                             mSellerSupcSDFulfilledProducer   = null;
    ISellerToSUPCListDBLookupService                                                                 mSellerToSUPCListDBLookupService = null;

    @Before
    public void setUp() throws OperationNotSupportedException, GenericPersisterException {
        cut = new SellerSupcFMMappingUploadProcessor();

        sellerSUPCFMMappingProcessor = new SellerSUPCFMMappingProcessorImpl();
        ReflectionTestUtils.setField(cut, "sellerSUPCFMMappingProcessor", sellerSUPCFMMappingProcessor);

        dep = mock(com.snapdeal.cocofs.services.jobprocessor.SellerSupcFMMappingUploadProcessor.IDependency.class);
        ReflectionTestUtils.setField(cut, "dep", dep);
        fulfilmentService = mock(IFulfilmentModelService.class);
        ReflectionTestUtils.setField(cut, "fulfilmentService", fulfilmentService);
        ReflectionTestUtils.setField(sellerSUPCFMMappingProcessor, "fulfilmentModelService", fulfilmentService);

        mDependencyProcessor = mock(IDependency.class);
        when(mDependencyProcessor.getBooleanPropertyValue(any(Property.class))).thenReturn(Boolean.TRUE);
        ReflectionTestUtils.setField(sellerSUPCFMMappingProcessor, "dep", mDependencyProcessor);
        mSellerToSUPCListDBLookupService = mock(ISellerToSUPCListDBLookupService.class);
        ReflectionTestUtils.setField(sellerSUPCFMMappingProcessor, "sellerToSUPCListLookupService", mSellerToSUPCListDBLookupService);
        mSellerSupcSDFulfilledProducer = mock(IExternalNotificationsServiceForSearchPush.class);
        ReflectionTestUtils.setField(sellerSUPCFMMappingProcessor, "sellerSupcSDFulfilledProducer", mSellerSupcSDFulfilledProducer);

        dataUpdater = mock(IFMMappingDataUpdater.class);
        ReflectionTestUtils.setField(cut, "dataUpdater", dataUpdater);
        fmDBDataReadService = mock(IFMDBDataReadService.class);
        ReflectionTestUtils.setField(cut, "fmDBDataReadService", fmDBDataReadService);
        fmAerospikeService = mock(IFMAerospikeDataReadService.class);
        ReflectionTestUtils.setField(cut, "fmAerospikeService", fmAerospikeService);

        dataEngine = mock(IDataEngineWithAerospike.class);
        ReflectionTestUtils.setField(cut, "dataEngine", dataEngine);

    }

    @After
    public void tearDown() throws Exception {
        cut = null;
        dataUpdater = null;
        fmDBDataReadService = null;
        fmAerospikeService = null;
        sellerSUPCFMMappingProcessor = null;
        fulfilmentService = null;
        dataEngine = null;
        dep = null;
        mDependencyProcessor = null;
        mSellerSupcSDFulfilledProducer = null;
        mSellerToSUPCListDBLookupService = null;
    }

    @Test
    public void testProcessOnSaveNotSuccessful() throws ClassNotFoundException, InvalidDataException, IOException, GenericPersisterException, PutFailedException {

        //setup data 
        int size = 20;
        List<SellerSupcFMMappingUploadDto> dtoList = getDTOList(size, FulfillmentModel.DROPSHIP);
        when(dep.readFile(any(String.class), any(Class.class))).thenReturn(dtoList);

        JobDetail job = createJobDetailMock();

        GenericPersisterWithAerospikeResponse updateResp = mock(GenericPersisterWithAerospikeResponse.class);

        //List<SellerSupcFMMapping> entityListToSave,
        //        List<SellerSupcFMMappingUnit> docListToSave, List<SellerSupcFMMappingVO> recordListToSave
        when(dataUpdater.updateSellerSupcFMMapping(//
                anyListOf(SellerSupcFMMapping.class), //
                anyListOf(SellerSupcFMMappingVO.class))//
        ).thenReturn(updateResp);

        when(updateResp.isSuccessful()).thenReturn(Boolean.FALSE);
        //action
        JobProcessorResponse resp = cut.process(job);
        assertFalse("valid flag false expected", resp.getProcessingSuccessful());
        assertFalse("expected error messages", resp.getError().isEmpty());
        //verify
        verify(mSellerSupcSDFulfilledProducer, times(0)).publishToSearch(anyListOf(SellerSupcUpdateSRO.class));
        //        verify(mSellerToSUPCListDBLookupService, times(0)).putNewSUPCSetInSellerSUPCLDTListForSeller(any(SellerToSupcListVO.class), any(String.class), anySetOf(String.class));
    }

    private JobDetail createJobDetailMock() {
        JobDetail job = mock(JobDetail.class);
        JobStatus jobStatus = mock(JobStatus.class);
        when(job.getStatus()).thenReturn(jobStatus);
        when(job.getRemarks()).thenReturn("1");
        when(jobStatus.getCode()).thenReturn("some value");
        return job;
    }

    @Test
    public void testProcessSUPCAdditionToExceptionListWhenFMIsNotFC_VOI() throws ClassNotFoundException, InvalidDataException, IOException, GenericPersisterException,
            PutFailedException {

        //setup data 
        int size = 20;
        List<SellerSupcFMMappingUploadDto> dtoList = getDTOList(size, FulfillmentModel.DROPSHIP);
        when(dep.readFile(any(String.class), any(Class.class))).thenReturn(dtoList);

        JobDetail job = createJobDetailMock();

        GenericPersisterWithAerospikeResponse updateResp = mock(GenericPersisterWithAerospikeResponse.class);

        when(dataUpdater.updateSellerSupcFMMapping(//
                anyListOf(SellerSupcFMMapping.class), //
                anyListOf(SellerSupcFMMappingVO.class))//
        ).thenReturn(updateResp);

        when(updateResp.isSuccessful()).thenReturn(Boolean.TRUE);

        //action
        JobProcessorResponse resp = cut.process(job);
        assertTrue("valid flag true expected", resp.getProcessingSuccessful());
        assertTrue("did not expected error messages", resp.getError().isEmpty());

        //verify
        verify(mSellerSupcSDFulfilledProducer, times(0)).publishToSearch(anyListOf(SellerSupcUpdateSRO.class));
        //        verify(mSellerToSUPCListDBLookupService, times(size)).putNewSUPCSetInSellerSUPCLDTListForSeller(any(SellerToSupcListVO.class), any(String.class), anySetOf(String.class));
    }

    /**
     * for the SELLER CODE <br>
     * SUPC in browsed files = 20, FM=FC_VOI <br>
     * SUPC in DB/AEROSPIKE = 22, FM=FC_VOI<br>
     * SELLER-FM-MAPPING FM=FC_VOI<br>
     */
    @Test
    public void testProcessSUPCAdditionToExceptionListWhenFMIsFC_VOIAndDefaultIsFC_VOI() throws ClassNotFoundException, InvalidDataException, IOException,
            GenericPersisterException, PutFailedException, ExternalDataNotFoundException, GetFailedException, RemoveFailedException {

        //setup data 
        int size = 20;
        final FulfillmentModel usefdFM = FulfillmentModel.FC_VOI;
        final FulfillmentModel defaultFM = FulfillmentModel.FC_VOI;
        List<SellerSupcFMMappingUploadDto> dtoList = getDTOListSameSeller(size, usefdFM);
        when(dep.readFile(any(String.class), any(Class.class))).thenReturn(dtoList);

        JobDetail job = createJobDetailMock();

        GenericPersisterWithAerospikeResponse updateResp = mock(GenericPersisterWithAerospikeResponse.class);

        when(dataUpdater.updateSellerSupcFMMapping(//
                anyListOf(SellerSupcFMMapping.class), //
                anyListOf(SellerSupcFMMappingVO.class))//
        ).thenReturn(updateResp);

        when(updateResp.isSuccessful()).thenReturn(Boolean.TRUE);

        int relativeIndex = 1;
        List<SellerSupcFMMapping> dbEntities = getEntityListSameSeller(relativeIndex, size + 2, defaultFM);

        String sellerCode = dbEntities.get(0).getSellerCode();

        when(fmDBDataReadService.getAllEnabledSellerSupcFMMappingBySeller(sellerCode)).thenReturn(dbEntities);

        SellerSupcFMMappingVO vo = createVO(relativeIndex, usefdFM);
        //        when(fmAerospikeService.getFMMapping(any(String.class), any(Class.class))).thenReturn(vo);
        when(fmAerospikeService.getFMMapping(any(String.class), any(Class.class))).thenAnswer(new Answer<SellerSupcFMMappingVO>() {
            @Override
            public SellerSupcFMMappingVO answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();

                String sellerCode = ((String) args[0]).split("-")[0];
                String supcCode = ((String) args[0]).split("-")[1];

                SellerSupcFMMappingVO vo = new SellerSupcFMMappingVO();

                vo.setSellerCode(sellerCode);
                vo.setSupc(supcCode);
                vo.setFulfillmentModel(defaultFM.name());
                vo.setEnabled(1);

                return vo;
            }
        });

        //        // no item already exists in exception list
        //        when(mSellerToSUPCListLookupService.isSUPCExistsInSellerSUPCExceptionListMapping(vo.getSellerCode(), vo.getSupc())).thenReturn(Boolean.TRUE);

        // the default fm check for sellerCode
        when(fulfilmentService.getFulfilmentModelBySeller(sellerCode, false)).thenReturn(defaultFM.name());

        //action
        JobProcessorResponse resp = cut.process(job);

        //verify
        assertTrue("valid flag true expected", resp.getProcessingSuccessful());
        assertTrue("did not expected error messages", resp.getError().isEmpty());

        //        verify(mSellerToSUPCListDBLookupService, times(size)).putNewSUPCSetInSellerSUPCLDTListForSeller(any(SellerToSupcListVO.class), any(String.class), anySetOf(String.class));
        //no change in FC_VOI so no push
        verify(mSellerSupcSDFulfilledProducer, times(0)).publishToSearch(anyListOf(SellerSupcUpdateSRO.class));
        verify(mSellerToSUPCListDBLookupService, times(0)).removeSellerSupcMapping(any(String.class), any(String.class));
    }

    /**
     * for the SELLER CODE <br>
     * SUPC in browsed files = 20, FM=FC_VOI <br>
     * SUPC in DB/AEROSPIKE = 22, FM=DS<br>
     * SELLER-FM-MAPPING FM=DS<br>
     */
    @Test
    public void testProcessSUPCAdditionToExceptionListWhenFMIsFC_VOIAndDefaultIsDS() throws ClassNotFoundException, InvalidDataException, IOException, GenericPersisterException,
            PutFailedException, ExternalDataNotFoundException, GetFailedException, RemoveFailedException {

        //setup data 
        int size = 20;
        final FulfillmentModel usefdFM = FulfillmentModel.FC_VOI;
        final FulfillmentModel defaultFM = FulfillmentModel.DROPSHIP;
        List<SellerSupcFMMappingUploadDto> dtoList = getDTOListSameSeller(size, usefdFM);
        when(dep.readFile(any(String.class), any(Class.class))).thenReturn(dtoList);

        JobDetail job = createJobDetailMock();

        GenericPersisterWithAerospikeResponse updateResp = mock(GenericPersisterWithAerospikeResponse.class);

        when(dataUpdater.updateSellerSupcFMMapping(//
                anyListOf(SellerSupcFMMapping.class), //
                anyListOf(SellerSupcFMMappingVO.class))//
        ).thenReturn(updateResp);

        when(updateResp.isSuccessful()).thenReturn(Boolean.TRUE);

        int relativeIndex = 1;
        List<SellerSupcFMMapping> dbEntities = getEntityListSameSeller(relativeIndex, size + 2, defaultFM);

        String sellerCode = dbEntities.get(0).getSellerCode();

        when(fmDBDataReadService.getAllEnabledSellerSupcFMMappingBySeller(sellerCode)).thenReturn(dbEntities);

        SellerSupcFMMappingVO vo = createVO(relativeIndex, usefdFM);
        //        when(fmAerospikeService.getFMMapping(any(String.class), any(Class.class))).thenReturn(vo);
        when(fmAerospikeService.getFMMapping(any(String.class), any(Class.class))).thenAnswer(new Answer<SellerSupcFMMappingVO>() {
            @Override
            public SellerSupcFMMappingVO answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();

                String sellerCode = ((String) args[0]).split("-")[0];
                String supcCode = ((String) args[0]).split("-")[1];

                SellerSupcFMMappingVO vo = new SellerSupcFMMappingVO();

                vo.setSellerCode(sellerCode);
                vo.setSupc(supcCode);
                vo.setFulfillmentModel(defaultFM.name());
                vo.setEnabled(1);

                return vo;
            }
        });

        //        //item already exists in exception list
        //        when(mSellerToSUPCListLookupService.isSUPCExistsInSellerSUPCExceptionListMapping(any(String.class), any(String.class))).thenReturn(Boolean.TRUE);

        // the default fm check for sellerCode
        when(fulfilmentService.getFulfilmentModelBySeller(sellerCode, false)).thenReturn(FulfillmentModel.DROPSHIP.name());

        //action
        JobProcessorResponse resp = cut.process(job);

        //verify
        assertTrue("valid flag true expected", resp.getProcessingSuccessful());
        assertTrue("did not expected error messages", resp.getError().isEmpty());

        //change in FC_VOI so push
        verify(mSellerSupcSDFulfilledProducer, times(size)).publishToSearch(anyListOf(SellerSupcUpdateSRO.class));
    }

    /**
     * for the SELLER CODE <br>
     * SUPC in browsed files = 20, FM=DS <br>
     * SUPC in DB/AEROSPIKE = 22, FM=FC_VOI <br>
     * SELLER-FM-MAPPING FM=DS<br>
     */
    @Test
    public void testProcessSUPCAdditionToExceptionListWhenFMIsDSAndDefaultIsFC_VOI() throws ClassNotFoundException, InvalidDataException, IOException, GenericPersisterException,
            PutFailedException, ExternalDataNotFoundException, GetFailedException, RemoveFailedException {

        //setup data 
        int size = 20;
        final FulfillmentModel usefdFM = FulfillmentModel.DROPSHIP;
        final FulfillmentModel defaultFM = FulfillmentModel.FC_VOI;
        List<SellerSupcFMMappingUploadDto> dtoList = getDTOListSameSeller(size, usefdFM);
        when(dep.readFile(any(String.class), any(Class.class))).thenReturn(dtoList);

        JobDetail job = createJobDetailMock();

        GenericPersisterWithAerospikeResponse updateResp = mock(GenericPersisterWithAerospikeResponse.class);

        when(dataUpdater.updateSellerSupcFMMapping(//
                anyListOf(SellerSupcFMMapping.class), //
                anyListOf(SellerSupcFMMappingVO.class))//
        ).thenReturn(updateResp);

        when(updateResp.isSuccessful()).thenReturn(Boolean.TRUE);

        int relativeIndex = 1;
        List<SellerSupcFMMapping> dbEntities = getEntityListSameSeller(relativeIndex, size + 2, defaultFM);

        String sellerCode = dbEntities.get(0).getSellerCode();

        when(fmDBDataReadService.getAllEnabledSellerSupcFMMappingBySeller(sellerCode)).thenReturn(dbEntities);

        when(fmAerospikeService.getFMMapping(any(String.class), any(Class.class))).thenAnswer(new Answer<SellerSupcFMMappingVO>() {
            @Override
            public SellerSupcFMMappingVO answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();

                String sellerCode = ((String) args[0]).split("-")[0];
                String supcCode = ((String) args[0]).split("-")[1];

                SellerSupcFMMappingVO vo = new SellerSupcFMMappingVO();

                vo.setSellerCode(sellerCode);
                vo.setSupc(supcCode);
                vo.setFulfillmentModel(defaultFM.name());
                vo.setEnabled(1);

                return vo;
            }
        });

        // the default fm check for sellerCode
        when(fulfilmentService.getFulfilmentModelBySeller(sellerCode, false)).thenReturn(defaultFM.name());

        //action
        JobProcessorResponse resp = cut.process(job);

        //verify
        assertTrue("valid flag true expected", resp.getProcessingSuccessful());
        assertTrue("did not expected error messages", resp.getError().isEmpty());

        //change in FC_VOI so push
        verify(mSellerSupcSDFulfilledProducer, times(size)).publishToSearch(anyListOf(SellerSupcUpdateSRO.class));
    }

    @Test
    public void testValidateJobOnExceptionForExistentSeller() throws ClassNotFoundException, InvalidDataException, IOException, ExternalDataNotFoundException {

        File file = mock(File.class);
        List<SellerSupcFMMappingUploadDto> dtoList = getDTOList(20, FulfillmentModel.DROPSHIP);
        when(dep.readFile(any(String.class), any(Class.class), any(Boolean.class))).thenReturn(dtoList);

        when(fulfilmentService.isSellerExists(any(String.class))).thenReturn(Boolean.TRUE);
        BulkUploadValidationResponse resp = cut.validateJob(file, JobActionCode.SELLER_SUPC_FM_MAPPING.getCode(),"1");

        assertTrue("valid flag true expected", resp.getValid());
        assertEquals("validation error", resp.getErrorMessage(), null);

    }

    @Test
    public void testValidateJobOnExceptionForNonExistentSeller() throws ClassNotFoundException, InvalidDataException, IOException {

        File file = mock(File.class);
        List<SellerSupcFMMappingUploadDto> dtoList = getDTOList(20, FulfillmentModel.DROPSHIP);
        when(dep.readFile(any(String.class), any(Class.class), any(Boolean.class))).thenReturn(dtoList);
        BulkUploadValidationResponse resp = cut.validateJob(file, JobActionCode.SELLER_SUPC_FM_MAPPING.getCode(),"1");

        assertFalse("valid flag false expected", resp.getValid());
        assertEquals("validation error", resp.getErrorMessage(), "Non existent seller specified in one or more rows");

    }

    private List<SellerSupcFMMappingUploadDto> getDTOListSameSeller(int size, FulfillmentModel fm) {
        List<SellerSupcFMMappingUploadDto> dtoList = new ArrayList<SellerSupcFMMappingUploadDto>();

        for (int i = 1; i <= size; i++) {
            SellerSupcFMMappingUploadDto o = createDTOSameSeller(i, fm);
            dtoList.add(o);
        }
        return dtoList;
    }

    private List<SellerSupcFMMappingUploadDto> getDTOList(int size, FulfillmentModel fm) {
        List<SellerSupcFMMappingUploadDto> dtoList = new ArrayList<SellerSupcFMMappingUploadDto>();

        for (int i = 1; i <= size; i++) {
            SellerSupcFMMappingUploadDto o = createDTO(i, fm);
            dtoList.add(o);
        }
        return dtoList;
    }

    private SellerSupcFMMappingUploadDto createDTOSameSeller(int index, FulfillmentModel fm) {
        int seed = 100000;
        String sellerCode = String.valueOf(seed + 100);
        String supcCode = String.valueOf(seed + index * 3);
        String fulfilmentModel = fm.name();
        //FIXME
        SellerSupcFMMappingUploadDto o = new SellerSupcFMMappingUploadDto(sellerCode, supcCode, fulfilmentModel,null);
        return o;
    }

    private SellerSupcFMMappingUploadDto createDTO(int index, FulfillmentModel fm) {
        int seed = 100000;
        String sellerCode = String.valueOf(seed + index * 100);
        String supcCode = String.valueOf(seed + index * 3);
        String fulfilmentModel = fm.name();
        //FIXME
        SellerSupcFMMappingUploadDto o = new SellerSupcFMMappingUploadDto(sellerCode, supcCode, fulfilmentModel,null);
        return o;
    }

    private List<SellerSupcFMMapping> getEntityList(int relativeIndex, FulfillmentModel fm) {
        List<SellerSupcFMMapping> dtoList = new ArrayList<SellerSupcFMMapping>();
        SellerSupcFMMapping o = createEntity(relativeIndex, fm);
        dtoList.add(o);
        return dtoList;
    }

    private SellerSupcFMMapping createEntity(int index, FulfillmentModel fm) {
        int seed = 100000;
        String sellerCode = String.valueOf(seed + index * 100);
        String supcCode = String.valueOf(seed + index * 3);
        String fulfilmentModel = fm.name();
        SellerSupcFMMapping o = new SellerSupcFMMapping();
        o.setSellerCode(sellerCode);
        o.setSupc(supcCode);
        o.setFulfilmentModel(fulfilmentModel);
        return o;
    }

    private List<SellerSupcFMMapping> getEntityListSameSeller(int relativeIndex, int size, FulfillmentModel fm) {
        List<SellerSupcFMMapping> dtoList = new ArrayList<SellerSupcFMMapping>();
        for (int i = 1; i <= size; i++) {
            SellerSupcFMMapping o = createEntitySameSeller(i, fm);
            dtoList.add(o);
        }

        return dtoList;
    }

    private SellerSupcFMMapping createEntitySameSeller(int index, FulfillmentModel fm) {
        int seed = 100000;
        String sellerCode = String.valueOf(seed + 100);
        String supcCode = String.valueOf(seed + index * 3);
        String fulfilmentModel = fm.name();
        SellerSupcFMMapping o = new SellerSupcFMMapping();
        o.setSellerCode(sellerCode);
        o.setSupc(supcCode);
        o.setFulfilmentModel(fulfilmentModel);
        return o;
    }

    private SellerSupcFMMappingVO createVO(int index, FulfillmentModel fm) {
        int seed = 100000;
        String sellerCode = String.valueOf(seed + index * 100);
        String supcCode = String.valueOf(seed + index * 3);
        String fulfilmentModel = fm.name();
        SellerSupcFMMappingVO o = new SellerSupcFMMappingVO();
        o.setSellerCode(sellerCode);
        o.setSupc(supcCode);
        o.setFulfillmentModel(fulfilmentModel);
        return o;
    }

}
