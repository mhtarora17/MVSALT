
insert into job_action values (null,'StateCategoryPriceTaxRate','State Category and Price Tax Rate','State Category and Price Tax Rate',now(),now(),'TaxRate');

 insert into job_action values (null,'SellerCategoryTaxRate','Seller Category Tax Rate','Seller Category Tax Rate',now(),now(),'TaxRate');

insert into job_action values (null,'SellerSUPCTaxRate','Seller Supc Tax Rate','Seller Supc Tax Rate',now(),now(),'TaxRate');

insert into role values (null,'taxrateeditor','taxrateeditor','admin',1,'/admin/taxRate',now(),now(),now());

CREATE TABLE `state_category_price_tax_rate` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `state` varchar(30) NOT NULL,
  `category_url` varchar(128) NOT NULL,
  `tax_rate` decimal(10,2) NOT NULL,
  `lower_price_limit` decimal(10,2) DEFAULT NULL,
  `upper_price_limit` decimal(10,2) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `created` timestamp NULL DEFAULT NULL,
  `last_updated` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `idx_state` (`state`)
) ENGINE=InnoDB;

 CREATE TABLE `seller_category_tax_rate` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `seller_code` varchar(20) NOT NULL,
  `category_url` varchar(128) NOT NULL,
  `tax_rate` decimal(10,2) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `created` timestamp NULL DEFAULT NULL,
  `last_updated` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `idx_seller` (`seller_code`)
) ENGINE=InnoDB;



 CREATE TABLE `seller_supc_tax_rate` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `seller_code` varchar(20) NOT NULL,
  `supc` varchar(32) NOT NULL,
  `tax_rate` decimal(10,2) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `created` timestamp NULL DEFAULT NULL,
  `last_updated` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `idx_seller` (`seller_code`)
) ENGINE=InnoDB;

alter table seller_supc_tax_rate add UNIQUE KEY  `seller_supc_tx_rate_UNIQUE` (seller_code,supc);

alter table seller_category_tax_rate add UNIQUE KEY  `seller_category_tx_rate_UNIQUE` (seller_code,category_url);

alter table state_category_price_tax_rate add  KEY  `state_category_tx_rate_idx` (state,category_url);
