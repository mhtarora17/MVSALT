use cocofs;
CREATE TABLE `forgot_password` (
      `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
      `email` varchar(100) NOT NULL,
      `verification_code` varchar(50) NOT NULL,
      `ttl` int(10) NOT NULL,
      `created` timestamp NULL DEFAULT NULL,
      `updated` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
      PRIMARY KEY (`id`),
      KEY `idx_verification_code` (`verification_code`),
      KEY `idx_created` (`created`)
);
