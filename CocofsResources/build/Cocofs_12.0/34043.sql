use cocofs;
CREATE TABLE `seller_supc_mapping` (
      `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
      `seller_code` varchar(15) NOT NULL,
      `supc` varchar(30) NOT NULL,
      `created` timestamp NULL DEFAULT NULL,
      `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
      PRIMARY KEY (`id`,`seller_code`),
      KEY `idx_seller_code` (`seller_code`),
      KEY `idx_supc` (`supc`),
      KEY `idx_seller_supc` (`seller_code`,`supc`),
      KEY `idx_updated` (`updated`)
)PARTITION BY KEY(seller_code) PARTITIONS 101;
