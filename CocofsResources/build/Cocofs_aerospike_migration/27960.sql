use cocofs;

alter table seller_fm_mapping
ADD supc_exist Boolean default false;

alter table seller_fm_mapping
ADD subcat_exist Boolean default false;

/*update seller_fm_mapping set supc_exist = true where seller_code in (select seller_code from seller_supc_fm_mapping);

update seller_fm_mapping set subcat_exist = true where seller_code in (select seller_code from seller_subcat_fm_mapping);
*/

insert into cocofs_property values (null,'aerospike.url','127.0.0.1:3000',now(),now());

insert into cocofs_property values (null,'data.source.for.fm.read','MONGODB',now(),now());

insert into cocofs_property values (null,'write.in.mongo.for.fm.update','true',now(),now());

insert into cocofs_property values (null,'write.in.aerospike.for.fm.update','true',now(),now());

insert into cocofs_property values (null,'get.sd.fulfilled.concurrently','false',now(),now());

insert into cocofs_property values (null,'update.fm.from.api','false',now(),now());


