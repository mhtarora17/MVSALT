/* 
 * Dev : Himanshu
 * Reviewer : Abhinav Singhal
 * Task : Get DeliverType based on Shipping Modes as Specified by SCORE
 * 
 */
use cocofs;
delete from rule_params where rule_id in (select r.id from rule r join block b on (b.id = r.block_id and b.name = 'DeliveryType' ) );
delete from criteria where rule_id in (select r.id from rule r join block b on (b.id = r.block_id and b.name = 'DeliveryType' ) );
delete from rule where block_id in ( select id from block where name = 'DeliveryType' );
delete from block_params where block_id in ( select id from block where name = 'DeliveryType' );
delete from block where name = 'DeliveryType';

