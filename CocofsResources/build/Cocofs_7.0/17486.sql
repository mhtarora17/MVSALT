/*dev: ankur garg
 JIRA: 17486
*/

insert into block values (null,'GiftWrapCategoryRuleblock',now());

SET @blockId:= (select id from block where name='GiftWrapCategoryRuleblock');
insert into block_params values (null,'FRAGILE','Fragile',@blockId,now(),now(),'ankur.garg@jasperindia.com');
insert into block_params values (null,'WEIGHT','Weight',@blockId,now(),now(),'ankur.garg@jasperindia.com');
insert into block_params values (null,'PRICE','Price',@blockId,now(),now(),'ankur.garg@jasperindia.com');
insert into block_params values (null,'VOLUME','Volume',@blockId,now(),now(),'ankur.garg@jasperindia.com');
insert into block_params values (null,'DANGEROUSGOODSTYPE','DangerousGoodsType',@blockId,now(),now(),'ankur.garg@jasperindia.com');
