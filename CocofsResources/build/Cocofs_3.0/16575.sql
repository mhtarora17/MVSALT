/* 
 * Dev : Kirti
 * Reviewer : 
 * Task : Weight capture for OS
 * 
 */
use cocofs;

alter table pending_product_attribute_update add column created_by varchar(255) NULL DEFAULT 'script';
insert into attribute (attribute, attribute_description,enabled,updated_by,created_by,created,last_updated,updated) values ('SYSTEMWEIGHTCAPTURED','Flag that indicates if system weight of the supc is captured',1,'udit.srivastava@jasperindia.com', 'udit.srivastava@jasperindia.com',now(),now(),now());

