use cocofs;

CREATE TABLE seller_supc_fm_mapping_old LIKE seller_supc_fm_mapping;
INSERT INTO seller_supc_fm_mapping_old SELECT * FROM seller_supc_fm_mapping WHERE enabled = 1;
RENAME TABLE seller_supc_fm_mapping TO seller_supc_fm_mapping_copy, seller_supc_fm_mapping_old TO seller_supc_fm_mapping;
