create index idx_last_updated on seller_fm_mapping (last_updated);

alter table rule add column `local_priority_constant` int(5) DEFAULT '0';

insert into cocofs_property values (null,"aerospike.ldt.url","127.0.0.1:3000",now(),now());
