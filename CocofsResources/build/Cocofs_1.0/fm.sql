use cocofs;

CREATE TABLE `seller_fm_mapping` (
      `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
      `seller_code` varchar(15) NOT NULL,
      `fulfilment_model` varchar(63) NOT NULL,
      `enabled` tinyint(1) NOT NULL DEFAULT '1',
      `created` timestamp NULL DEFAULT NULL,
      `last_updated` timestamp NULL DEFAULT NULL,
      `updated_by` varchar(255) NOT NULL,
      `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
      PRIMARY KEY (`id`),
      KEY `idx_seller_code` (`seller_code`,`enabled`),
      KEY `idx_updated` (`updated`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `seller_supc_fm_mapping` (
      `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
      `seller_code` varchar(16) NOT NULL,
      `supc` varchar(32) NOT NULL,
      `fulfilment_model` varchar(128) NOT NULL,
      `enabled` tinyint(1) NOT NULL DEFAULT '1',
      `created` timestamp NULL DEFAULT NULL,
      `last_updated` timestamp NULL DEFAULT NULL,
      `updated_by` varchar(255) NOT NULL,
      `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
      PRIMARY KEY (`id`),
      KEY `idx_seller_code_supc` (`seller_code`,`supc`,`enabled`),
      KEY `idx_updated` (`updated`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `seller_subcat_fm_mapping` (
      `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
      `seller_code` varchar(16) NOT NULL,
      `subcat` varchar(128) NOT NULL,
      `fulfilment_model` varchar(64) NOT NULL,
      `enabled` tinyint(1) NOT NULL DEFAULT '1',
      `created` timestamp NULL DEFAULT NULL,
      `last_updated` timestamp NULL DEFAULT NULL,
      `updated_by` varchar(255) NOT NULL,
      `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
      PRIMARY KEY (`id`),
      KEY `idx_seller_code_subcat` (`seller_code`,`subcat`,`enabled`),
      KEY `idx_updated` (`updated`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;