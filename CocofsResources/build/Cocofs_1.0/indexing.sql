

Alter table product_attribute_notification ADD INDEX `idx_supc_not_end_date`(`supc`,`notification_end_date`);

Alter table pending_product_attribute_update DROP INDEX `play_date_pending_idx`;

Alter table pending_product_attribute_update ADD INDEX `idx_supc_pending_play_date`(`supc`,`pending`,`play_date`);