/*
    Inital setup

*/

CREATE DATABASe cocofs;

use cocofs;

CREATE TABLE `cocofs_property` (
      `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
      `name` varchar(48) NOT NULL,
      `value` varchar(255) NOT NULL,
      `created` timestamp NULL DEFAULT NULL,
      `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
      PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `access_control` (
      `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
      `role_code_csv` varchar(64) NOT NULL,
      `url_prefix` varchar(255) NOT NULL,
      `feature` varchar(100) DEFAULT NULL,
      `enabled` tinyint(1) NOT NULL DEFAULT '1',
      `created` datetime NOT NULL,
      `last_updated` timestamp NULL DEFAULT NULL,
      `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
      PRIMARY KEY (`id`),
      UNIQUE KEY `role_code_csv` (`role_code_csv`,`url_prefix`),
      KEY `enabled` (`enabled`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `role` (
      `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
      `code` varchar(64) NOT NULL,
      `description` varchar(64) DEFAULT NULL,
      `parent_role_code` varchar(64) DEFAULT NULL,
      `enabled` tinyint(1) NOT NULL,
      `default_url` varchar(255) NOT NULL DEFAULT '/inbound',
      `created` timestamp NULL DEFAULT NULL,
      `last_updated` timestamp NULL DEFAULT NULL,
      `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
      PRIMARY KEY (`id`),
      UNIQUE KEY `code_UNIQUE` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `user` (
      `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
      `email` varchar(255) NOT NULL,
      `password` varchar(255) NOT NULL,
      `enabled` tinyint(1) NOT NULL DEFAULT '1',
      `email_verified` tinyint(1) NOT NULL DEFAULT '0',
      `display_name` varchar(48) DEFAULT NULL,
      `first_name` varchar(48) DEFAULT NULL,
      `middle_name` varchar(48) DEFAULT NULL,
      `last_name` varchar(48) DEFAULT NULL,
      `email_verification_code` varchar(48) DEFAULT NULL,
      `created` timestamp NULL DEFAULT NULL,
      `last_updated` timestamp NULL DEFAULT NULL,
      `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
      `uid` varchar(48) DEFAULT NULL,
      PRIMARY KEY (`id`),
      UNIQUE KEY `email_index` (`email`(255))
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `user_role` (
      `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
      `user_id` int(10) unsigned NOT NULL,
      `role_code` varchar(64) NOT NULL,
      `enabled` tinyint(1) NOT NULL DEFAULT '1',
      `created` timestamp NULL DEFAULT NULL,
      `last_updated` timestamp NULL DEFAULT NULL,
      `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
      PRIMARY KEY (`id`),
      KEY `FK_user_role` (`user_id`),
      KEY `FK_role_user_role` (`role_code`),
      KEY `enabled` (`enabled`),
      CONSTRAINT `FK_role_user_role` FOREIGN KEY (`role_code`) REFERENCES `role` (`code`) ON DELETE CASCADE ON UPDATE CASCADE,
      CONSTRAINT `FK_user_to_user_role` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `cocofs_pdf_template` (
      `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
      `name` varchar(30) NOT NULL,
      `template` longtext NOT NULL,
      `created` datetime DEFAULT NULL,
      `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
      PRIMARY KEY (`id`),
      UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;


CREATE TABLE `attribute` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `attribute` varchar(32) NOT NULL,
  `attribute_description` varchar(255) NOT NULL,
  `enabled` tinyint(1) DEFAULT '1',
  `updated_by` varchar(255) NOT NULL,
  `created_by` varchar(255) NOT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `last_updated` timestamp NULL DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `attribute_idx` (`attribute`),
  KEY `attributedescription_idx` (`attribute_description`),
  KEY `enabled_attribute_idx` (`enabled`,`attribute`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

CREATE TABLE `product_attribute` (
      `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
      `supc` varchar(32) NOT NULL,
      `attribute` varchar(32) NOT NULL,
      `value` varchar(255) NOT NULL,
      `enabled` tinyint(1) DEFAULT '1',
      `updated_by` varchar(255) NOT NULL,
      `created_by` varchar(255) NOT NULL,
      `created` timestamp NULL DEFAULT NULL,
      `last_updated` timestamp NULL DEFAULT NULL,
      `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
      PRIMARY KEY (`id`),
      KEY `supc_attribute_enabled_idx` (`supc`,`attribute`,`enabled`),
      KEY `created_supc_attribute_idx` (`created`,`supc`,`attribute`),
      KEY `lastupdated_supc_attribute_idx` (`last_updated`,`supc`,`attribute`),
      KEY `enabled_supc_lastupdated_idx` (`enabled`,`supc`,`last_updated`) ,
      CONSTRAINT `product_attribute_ibfk_1` FOREIGN KEY (`attribute`) REFERENCES `attribute` (`attribute`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `product_attribute_history` (
      `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
      `supc` varchar(32) NOT NULL,
      `attribute` varchar(32) NOT NULL,
      `old_value` varchar(255) NULL,
      `new_value` varchar(255) NOT NULL,
      `remark` varchar(1024) DEFAULT NULL,
      `updated_by` varchar(255) NOT NULL,
      `created_by` varchar(255) NOT NULL,
      `created` timestamp NULL DEFAULT NULL,
      `last_updated` timestamp NULL DEFAULT NULL,
      `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
      PRIMARY KEY (`id`),
      KEY `supc_attribute_idx` (`supc`,`attribute`,`old_value`),
      KEY `created_supc_attribute_idx` (`created`,`supc`,`attribute`),
      CONSTRAINT `product_attribute_history_ibfk_1` FOREIGN KEY (`attribute`) REFERENCES `attribute` (`attribute`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO attribute ( attribute, attribute_description, enabled, updated_by, created_by, created, last_updated, updated) VALUES 
('SERIALIZED', 'Product Units have serial numbers of some sort like IMEI',1,'udit.srivastava@jasperindia.com','udit.srivastava@jasperindia.com', now() , now(), now()),
('HAZARDOUS', 'Product classified as hazardous for shipping purpose',1,'udit.srivastava@jasperindia.com','udit.srivastava@jasperindia.com', now() , now(), now()),
('LIQUID', 'Product claissified as liquid for shipping purpose',1,'udit.srivastava@jasperindia.com','udit.srivastava@jasperindia.com', now() , now(), now()),
('VOLWEIGHT', 'Volumetric Weight of the product package',1,'udit.srivastava@jasperindia.com','udit.srivastava@jasperindia.com', now() , now(), now()),
('WEIGHT', 'Weight of the product package',1,'udit.srivastava@jasperindia.com','udit.srivastava@jasperindia.com', now() , now(), now()),
('BREADTH', 'Breadth of product package',1,'udit.srivastava@jasperindia.com','udit.srivastava@jasperindia.com', now() , now(), now()),
('HEIGHT', 'Height of product package',1,'udit.srivastava@jasperindia.com','udit.srivastava@jasperindia.com', now() , now(), now()),
('LENGTH', 'Length of product package',1,'udit.srivastava@jasperindia.com','udit.srivastava@jasperindia.com', now() , now(), now()),
('FRAGILE', 'Product classified as fragile for shipping purpose',1,'udit.srivastava@jasperindia.com','udit.srivastava@jasperindia.com', now() , now(), now()),
('PRODPARTS','Number of physical packages that constitute single Product package to be shipped',1,'udit.srivastava@jasperindia.com','udit.srivastava@jasperindia.com', now() , now(), now());



CREATE TABLE `job_status` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(15) NOT NULL,
  `value` varchar(50) NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `REQ_STATUS_CODE_UNIQUE` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `job_action` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(50) NOT NULL,
  `value` varchar(50) NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `job_type` varchar(15) NOT NULL DEFAULT 'default',
  PRIMARY KEY (`id`),
  UNIQUE KEY `REQ_STATUS_CODE_UNIQUE` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `job_action_role_mapping` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `action_code` varchar(50) NOT NULL,
  `role_code` varchar(64) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `created` timestamp NULL DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_action_code` (`action_code`),
  KEY `fk_role_code` (`role_code`),
  CONSTRAINT `fk_job_action_code` FOREIGN KEY (`action_code`) REFERENCES `job_action` (`code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_role_code` FOREIGN KEY (`role_code`) REFERENCES `role` (`code`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;


CREATE TABLE `job_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_code` varchar(32) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `filepath` varchar(512) NOT NULL,
  `retry` tinyint(1) NOT NULL DEFAULT '0',
  `action_code` varchar(50) NOT NULL,
  `status_code` varchar(15) NOT NULL,
  `priority` int(5) NOT NULL DEFAULT '1',
  `remarks` varchar(255) NOT NULL,
  `uploaded_by` varchar(64) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `created` timestamp NULL DEFAULT NULL,
  `last_updated` timestamp NULL DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `file_code_idx` (`file_code`),
  KEY `fk_action_code` (`action_code`),
  KEY `fk_status_code` (`status_code`),
  CONSTRAINT `fk_action_code` FOREIGN KEY (`action_code`) REFERENCES `job_action` (`code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_status_code` FOREIGN KEY (`status_code`) REFERENCES `job_status` (`code`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `job_detail_history` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `job_detail_id` int(11) NOT NULL,
  `prev_status_code` varchar(50) DEFAULT NULL,
  `next_status_code` varchar(50) NOT NULL,
  `remark` varchar(200) NOT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_job_detail` (`job_detail_id`),
  KEY `fk_prev_status_code` (`prev_status_code`),
  KEY `fk_next_status_code` (`next_status_code`),
  CONSTRAINT `fk_job_detail` FOREIGN KEY (`job_detail_id`) REFERENCES `job_detail` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_next_status_code` FOREIGN KEY (`next_status_code`) REFERENCES `job_status` (`code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_prev_status_code` FOREIGN KEY (`prev_status_code`) REFERENCES `job_status` (`code`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;


INSERT INTO job_status ( code, value,description,created, updated )
values ( 'VALD' , 'VALIDATED'         , 'Validated'         ,  now() ,now() )
, ('EXEC' , 'EXECUTED'          , 'Executed'          , now() ,now() )
, ('PROC' , 'PROCESSING'        , 'Processing'        , now() ,now() )
, ('FLD'  , 'FAILED'            , 'Failed'            , now() ,now() );

INSERT INTO job_action (code, value, description, created, updated, job_type)
values ('SellerSupcFMMapping','Seller Supc FM Mapping','Seller Supc FM Mapping',now(),now(),'FMMapping'),
('SuperAttributeUpdate','Super Attribute update','Super Product Attribute update' ,now(),now() ,'ProductUpdate'),
('AttributeUpdate','Attribute update','Product Attribute update' ,now(),now() ,'ProductUpdate');



CREATE TABLE `email_template` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `subject_template` varchar(255) NOT NULL,
  `body_template` mediumtext NOT NULL,
  `from` varchar(100) NOT NULL,
  `to` varchar(500) DEFAULT NULL,
  `cc` varchar(500) DEFAULT NULL,
  `bcc` varchar(500) DEFAULT NULL,
  `reply_to` varchar(100) NOT NULL,
  `email_channel_id` int(11) unsigned NOT NULL DEFAULT '2',
  `created` datetime DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  KEY `key_channel_id` (`email_channel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


insert into email_template ( name, subject_template, body_template, `from` , reply_to, created, email_channel_id) 
values ('JobExecutionSuccessfullEmail', 'Job ${jobDetail.fileCode} Uploaded by ${jobDetail.uploadedBy} Executed Successfully', 'Test Body', 'Snapdeal<noreply@snapdeals.co.in>', 'noreply@snapdeal.com', now(), 4),
('JobExecutionFailureEmail', 'Job ${jobDetail.fileCode} Uploaded by ${jobDetail.uploadedBy} Execution Failed', 'Test Body', 'Snapdeal<noreply@snapdeals.co.in>', 'noreply@snapdeal.com', now(),4),
('JobSavedEmail', 'Job ${jobDetail.fileCode} Uploaded by ${jobDetail.uploadedBy} Saved Successfully', 'Test Body', 'Snapdeal<noreply@snapdeals.co.in>', 'noreply@snapdeal.com', now(), 4),
('userCreationEmail', 'User account created for Cocofs ${user.email}', 'Your password is ${user.password}', 'Snapdeal<noreply@snapdeals.co.in>', 'noreply@snapdeal.com', now(), 4),
('forgotPasswordEmail', 'Test Subject', 'Test Body', 'Snapdeal<noreply@snapdeals.co.in>', 'noreply@snapdeal.com', now(), 4);


CREATE TABLE `block` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `block_params` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `param_name` varchar(255) NOT NULL,
  `field_value` varchar(255) NOT NULL,
  `block_id` int(10) unsigned NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `block_id` (`block_id`),
  CONSTRAINT `block_params_ibfk_1` FOREIGN KEY (`block_id`) REFERENCES `block` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


insert into block ( name, created ) values ( 'PackagingType', now()), ('DeliveryType', now());
insert into block_params (param_name, field_value, block_id, created, updated, created_by) values
('FRAGILE', 'Fragile' , 1 , now(), now(), 'udit.srivastava@jasperindia.com') ,
('HAZMAT', 'HazMat' , 1 , now(), now(), 'udit.srivastava@jasperindia.com') ,
('LIQUID', 'Liquid' , 1 , now(), now(), 'udit.srivastava@jasperindia.com') ,
('WEIGHT', 'Weight' , 1 , now(), now(), 'udit.srivastava@jasperindia.com') ,
('VOLWEIGHT', 'VolumetricWeight' , 1 , now(), now(), 'udit.srivastava@jasperindia.com') ,
('FRAGILE', 'Fragile' , 2 , now(), now(), 'udit.srivastava@jasperindia.com') ,
('HAZMAT', 'HazMat' , 2 , now(), now(), 'udit.srivastava@jasperindia.com') ,
('LIQUID', 'Liquid' , 2 , now(), now(), 'udit.srivastava@jasperindia.com') ,
('WEIGHT', 'Weight' , 2 , now(), now(), 'udit.srivastava@jasperindia.com') ,
('VOLWEIGHT', 'VolumetricWeight' , 2 , now(), now(), 'udit.srivastava@jasperindia.com') ;

CREATE TABLE `rule` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `priority` int(10) NOT NULL DEFAULT '0',
  `block_id` int(10) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  `start_date` datetime NOT NULL,
  `end_date` datetime DEFAULT NULL,
  `remark` varchar(1024) DEFAULT NULL,
  `is_synched` tinyint(1) DEFAULT '0',
  `is_banned` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_rule_code` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `criteria` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `operator` varchar(64) NOT NULL,
  `value` varchar(2048) NOT NULL,
  `value_type` varchar(64) NOT NULL,
  `rule_id` int(10) unsigned NOT NULL,
  `created` datetime NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `rule_id` (`rule_id`),
  CONSTRAINT `criteria_ibfk_1` FOREIGN KEY (`rule_id`) REFERENCES `rule` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `rule_params` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rule_id` int(10) unsigned NOT NULL,
  `param_name` varchar(255) NOT NULL,
  `param_value` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `rule_id` (`rule_id`),
  CONSTRAINT `rule_params_ibfk_1` FOREIGN KEY (`rule_id`) REFERENCES `rule` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;



insert into role (code, description, parent_role_code, enabled, default_url, created, last_updated, updated ) 
values ('admin', 'admin', NULL, 1 , '/admin/productattribute/paupload', now(), now(), now()),
('tech', 'tech', NULL, 1 , '/admin/productattribute/paupload', now(), now(), now()),
('superuploader', 'superuploader', 'admin', 1 , '/admin/productattribute/paupload', now(), now(), now()),
('uploader', 'uploader', 'superuploader', 1 , '/admin/productattribute/paupload', now(), now(), now()),
('ratecardeditor', 'ratecardeditor', 'admin', 1 , '/admin/ratecard/', now(), now(), now()),
('ruleeditor', 'ruleeditor', 'admin', 1 , '/admin/rules/', now(), now(), now()),
('fmMappingUploader', 'fmMappingUploader', 'admin', 1 , '/admin/sellerSupcFMMapping/', now(), now(), now());

insert into access_control ( created, role_code_csv, enabled, feature, last_updated, url_prefix ) 
values (now(), 'registered,admin,tech,uploader,superuploader', 1, 'ProductAttributeUpload', now(), '/admin/productattribute'),
(now(), 'registered,admin,tech', 1, 'Jobs', now(), '/admin/jobs'),
(now(), 'registered,admin,tech,ruleeditor', 1, 'Rules', now(), '/admin/rules'),
(now(), 'registered,admin,tech,ratecardeditor', 1, 'RateCard', now(), '/admin/ratecard'),
(now(), 'registered,admin,tech', 1, 'TaskAdmin', now(), '/admin/task'),
(now(), 'registered,tech', 1, 'Cache', now(), '/admin/cache'),
(now(), 'registered,admin,tech,fmMappingUploader', 1, 'FMMapping', now(), '/admin/sellerSupcFMMapping')
;

CREATE TABLE `pending_product_attribute_update` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `supc` varchar(32) NOT NULL,
  `attribute_name` varchar(32) NOT NULL,
  `old_value` varchar(255) DEFAULT NULL,
  `new_value` varchar(255) NOT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `play_date` timestamp NULL DEFAULT NULL,
  `pending` tinyint(1) NOT NULL,
  `pendancy_reason` varchar(255) NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `play_date_pending_idx` (`play_date`, `pending`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `product_attribute_notification` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `supc` varchar(32) NOT NULL,
  `notification_type` varchar(32) NOT NULL,
  `current_value` varchar(255) DEFAULT NULL,
  `new_value` varchar(255) NOT NULL,
  `with_effect_from` timestamp NULL DEFAULT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `notification_end_date` timestamp NULL DEFAULT NULL,
  `user_type` varchar(32) NOT NULL,
  `visible` tinyint(1) NOT NULL,
  `ff_changed` tinyint(1) NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*ratecardeditor ruleeditor  admin tech */

create index status_created_idx on job_detail (status_code, created, uploaded_by);
create index user_idx on job_detail (uploaded_by, action_code, status_code);


insert into cocofs_property VALUES (null,'sellerSupcAssociationQueue','SellerSupcNewAssociationQueue', now(),now());
insert into cocofs_property VALUES (null,'activemqURL','to be decide', now(),now());
insert into cocofs_property VALUES (null,'activemqUsername','username', now(),now());
insert into cocofs_property VALUES (null,'activemqPassword','password', now(),now());
insert into cocofs_property VALUES (null,'validation.eligible.entities.list','ProductAttribute', now(),now());
insert into cocofs_property VALUES (null,'memcached.server.list','to be decided', now(),now());

insert into cocofs_property (name , value, created, updated) values ('ops.web.service.url', 'http://ops.url.to.be.inserted:port', now(), now());
insert into cocofs_property (name , value, created, updated) values ('product.catalog.web.service.url', 'httpms://catalog.url.to.be.inserted:port', now(), now());
insert into cocofs_property (name , value, created, updated) values ('ipms.web.service.url', 'http://ip.url.to.be.inserted:port', now(), now());
insert into cocofs_property (name , value, created, updated) values ('use.hystrix.for.generic.command', 'true', now(), now());


insert into cocofs_property (name , value, created, updated) values ('ppau.waiting.period', '3', now(), now());
insert into cocofs_property (name , value, created, updated) values ('panu.notificaiton.expiry', '3', now(), now());
insert into cocofs_property (name , value, created, updated) values ('hostNames', 'cocofs01', now(), now());
insert into cocofs_property (name , value, created, updated) values ('max.allowed.size.lookup', '1000', now(), now());
insert into cocofs_property (name , value, created, updated) values ('temp.dir', '/tmp/', now(), now());

