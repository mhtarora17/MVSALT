/**
 *  Copyright 2014 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.web.landing.layer.impl;


import static junit.framework.Assert.assertEquals;

import static junit.framework.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import javax.naming.OperationNotSupportedException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.util.Assert;

import com.snapdeal.base.transport.service.ITransportService.Protocol;
import com.snapdeal.base.validation.ValidationError;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.generic.persister.exception.GenericPersisterException;
import com.snapdeal.cocofs.generic.persister.response.GenericPersisterWithAerospikeResponse;
import com.snapdeal.cocofs.model.request.SetSellerFMMappingRequest;
import com.snapdeal.cocofs.model.response.SetSellerFMMappingResponse;
import com.snapdeal.cocofs.services.IDataReaderWithAerospike;
import com.snapdeal.cocofs.services.IDataUpdaterWithAerospike;
import com.snapdeal.cocofs.services.common.dto.UserInfo;
import com.snapdeal.cocofs.services.data.engine.IDataEngineWithAerospike;
import com.snapdeal.cocofs.services.fm.IFulfilmentModelService;
import com.snapdeal.cocofs.services.fm.IManualPushOfSellerFMUpdateForExternalNotification;
import com.snapdeal.cocofs.services.fm.ISellerToSUPCListDBLookupService;
import com.snapdeal.cocofs.services.fm.dto.SellerFMMappingUpdateDto;
import com.snapdeal.cocofs.services.impl.SellerSUPCFMMappingProcessorImpl;
import com.snapdeal.cocofs.services.impl.SellerSUPCFMMappingProcessorImpl.IDependency;
import com.snapdeal.cocofs.services.producer.IExternalNotificationsServiceForSearchPush;
import com.snapdeal.cocofs.sro.FulfillmentModel;
import com.snapdeal.cocofs.sro.SellerSupcUpdateSRO;
import com.snapdeal.cocofs.web.landing.layer.ICocofsFMLandingService;
import com.snapdeal.cocofs.web.landing.layer.IRequestValidationService;

/**
 * @version 1.0, 03-Dec-2014
 * @author shiv
 */

public class CocofsFMLandingServiceImplTest {

    //class under test
    ICocofsFMLandingService                                                           cut                            = null;

    //dependencies
    IManualPushOfSellerFMUpdateForExternalNotification                                mSellerSUPCFMMappingProcessor  = null;
    IFulfilmentModelService                                                           mFulfilmentModelService        = null;
    @Qualifier("externalNotificationsServiceForSearchPush")
    IExternalNotificationsServiceForSearchPush                                        mSellerSupcSDFulfilledProducer = null;
    ISellerToSUPCListDBLookupService                                                  mSellerToSUPCListLookupService = null;
    IDependency                                                                       mDependencyProcessor           = null;
    com.snapdeal.cocofs.web.landing.layer.impl.CocofsFMLandingServiceImpl.IDependency mDependencyCoCo                = null;

    IRequestValidationService                                                         requestValidationService       = null;
    IDataUpdaterWithAerospike                                                         aerospikedataUpdater           = null;

    @SuppressWarnings("unchecked")
    @Before
    public void setUp() throws OperationNotSupportedException, GenericPersisterException {
        cut = new CocofsFMLandingServiceImpl();

        mSellerSUPCFMMappingProcessor = new SellerSUPCFMMappingProcessorImpl();
        ReflectionTestUtils.setField(cut, "sellerSUPCFMMappingProcessor", mSellerSUPCFMMappingProcessor);

        requestValidationService = mock(IRequestValidationService.class);
        ReflectionTestUtils.setField(cut, "requestValidationService", requestValidationService);

        ValidationError error = null;
        when(requestValidationService.requestBasicFieldValidation(any())).thenReturn(error);

        mDependencyCoCo = mock(com.snapdeal.cocofs.web.landing.layer.impl.CocofsFMLandingServiceImpl.IDependency.class);
        when(mDependencyCoCo.getBooleanPropertyValue(Property.UPDATE_FM_FROM_API)).thenReturn(Boolean.TRUE);
        when(mDependencyCoCo.getBooleanPropertyValue(Property.ACTIVE_MQ_SEARCH_PUSH_ENABLED)).thenReturn(Boolean.TRUE);
        ReflectionTestUtils.setField(cut, "dep", mDependencyCoCo);

        mFulfilmentModelService = mock(IFulfilmentModelService.class);
        ReflectionTestUtils.setField(cut, "fulfilmentModelService", mFulfilmentModelService);
        ReflectionTestUtils.setField(mSellerSUPCFMMappingProcessor, "fulfilmentModelService", mFulfilmentModelService);

        aerospikedataUpdater = mock(IDataUpdaterWithAerospike.class);
        ReflectionTestUtils.setField(cut, "aerospikedataUpdater", aerospikedataUpdater);

        GenericPersisterWithAerospikeResponse mResp = mock(GenericPersisterWithAerospikeResponse.class);
        when(mResp.isSuccessful()).thenReturn(Boolean.TRUE);

        when(aerospikedataUpdater.updateDataWithDTO(any(SellerFMMappingUpdateDto.class), any(IDataReaderWithAerospike.class), any(IDataEngineWithAerospike.class),
                any(UserInfo.class), any(Boolean.class))).thenReturn(mResp);

        mDependencyProcessor = mock(IDependency.class);
        when(mDependencyProcessor.getBooleanPropertyValue(any(Property.class))).thenReturn(Boolean.TRUE);
        ReflectionTestUtils.setField(mSellerSUPCFMMappingProcessor, "dep", mDependencyProcessor);
        mSellerToSUPCListLookupService = mock(ISellerToSUPCListDBLookupService.class);
        ReflectionTestUtils.setField(mSellerSUPCFMMappingProcessor, "sellerToSUPCListLookupService", mSellerToSUPCListLookupService);
        mSellerSupcSDFulfilledProducer = mock(IExternalNotificationsServiceForSearchPush.class);
        ReflectionTestUtils.setField(mSellerSUPCFMMappingProcessor, "sellerSupcSDFulfilledProducer", mSellerSupcSDFulfilledProducer);
        when(mSellerSupcSDFulfilledProducer.publishToSearch(anyListOf(SellerSupcUpdateSRO.class))).thenReturn(Boolean.TRUE);
    }

    @After
    public void tearDown() {
        cut = null;
        mSellerSUPCFMMappingProcessor = null;
        mFulfilmentModelService = null;
        mSellerSupcSDFulfilledProducer = null;
        mSellerToSUPCListLookupService = null;
        mDependencyProcessor = null;
        mDependencyCoCo = null;
        requestValidationService = null;
        aerospikedataUpdater = null;
    }

    @Test
    public void testSetSellerFMMappingNewFMIsFC_VOIAndOldFMIsNULL() {

        SetSellerFMMappingRequest request = mock(SetSellerFMMappingRequest.class);
        Protocol proto = Protocol.PROTOCOL_JSON;
        when(request.getRequestProtocol()).thenReturn(proto);
        when(request.getFulfilmentModel()).thenReturn(FulfillmentModel.FC_VOI.name());

        SetSellerFMMappingResponse resp = cut.setSellerFMMapping(request);

        assertTrue("resp ", resp.isSuccessful());
        verify(mSellerSupcSDFulfilledProducer, times(0)).publishToSearch(anyListOf(SellerSupcUpdateSRO.class));
    }

    @Test
    public void testSetSellerFMMappingNewFMIsFC_VOIAndOldFMIsDS() throws ExternalDataNotFoundException {

        final FulfillmentModel fmUsed = FulfillmentModel.FC_VOI;
        FulfillmentModel fmExisting = FulfillmentModel.DROPSHIP;
        verifySetSellerFMBehavior(fmUsed, fmExisting);
    }

    @Test
    public void testSetSellerFMMappingNewFMIsFC_VOIAndOldFMIsFC_VOI() throws ExternalDataNotFoundException {

        SetSellerFMMappingRequest request = mock(SetSellerFMMappingRequest.class);
        Protocol proto = Protocol.PROTOCOL_JSON;
        when(request.getRequestProtocol()).thenReturn(proto);
        FulfillmentModel fmExisting = FulfillmentModel.FC_VOI;
        when(mFulfilmentModelService.getFulfilmentModelBySeller(any(String.class), any(Boolean.class))).thenReturn(fmExisting.name());
        final FulfillmentModel fmUsed = fmExisting;
        when(request.getFulfilmentModel()).thenReturn(fmUsed.name());
        when(request.getSellerCode()).thenReturn("ABCDEF");

        // action
        SetSellerFMMappingResponse resp = cut.setSellerFMMapping(request);

        //verify
        assertTrue("resp ", resp.isSuccessful());

        verify(mSellerSupcSDFulfilledProducer, times(0)).publishToSearch(anyListOf(SellerSupcUpdateSRO.class));
    }

    @Test
    public void testSetSellerFMMappingNewFMIsDSAndOldFMIsNULL() {

        SetSellerFMMappingRequest request = mock(SetSellerFMMappingRequest.class);
        Protocol proto = Protocol.PROTOCOL_JSON;
        when(request.getRequestProtocol()).thenReturn(proto);
        when(request.getFulfilmentModel()).thenReturn(FulfillmentModel.DROPSHIP.name());

        SetSellerFMMappingResponse resp = cut.setSellerFMMapping(request);

        assertTrue("resp ", resp.isSuccessful());
        verify(mSellerSupcSDFulfilledProducer, times(0)).publishToSearch(anyListOf(SellerSupcUpdateSRO.class));
    }

    @Test
    public void testSetSellerFMMappingNewFMIsDSAndOldFMIsDS() throws ExternalDataNotFoundException {

        SetSellerFMMappingRequest request = mock(SetSellerFMMappingRequest.class);
        Protocol proto = Protocol.PROTOCOL_JSON;
        when(request.getRequestProtocol()).thenReturn(proto);
        when(request.getFulfilmentModel()).thenReturn(FulfillmentModel.DROPSHIP.name());
        when(mFulfilmentModelService.getFulfilmentModelBySeller(any(String.class), any(Boolean.class))).thenReturn(FulfillmentModel.DROPSHIP.name());

        SetSellerFMMappingResponse resp = cut.setSellerFMMapping(request);

        assertTrue("resp ", resp.isSuccessful());
        verify(mSellerSupcSDFulfilledProducer, times(0)).publishToSearch(anyListOf(SellerSupcUpdateSRO.class));
    }

    @Test
    public void testSetSellerFMMappingNewFMIsDSAndOldFMIsFC_VOI() throws ExternalDataNotFoundException {
        final FulfillmentModel fmUsed = FulfillmentModel.DROPSHIP;
        FulfillmentModel fmExisting = FulfillmentModel.FC_VOI;
        verifySetSellerFMBehavior(fmUsed, fmExisting);
    }

    private void verifySetSellerFMBehavior(final FulfillmentModel fmUsed, FulfillmentModel fmExisting) throws ExternalDataNotFoundException {
        SetSellerFMMappingRequest request = mock(SetSellerFMMappingRequest.class);
        Protocol proto = Protocol.PROTOCOL_JSON;
        when(request.getRequestProtocol()).thenReturn(proto);
        when(request.getFulfilmentModel()).thenReturn(fmUsed.name());
        final String sellerCode = "abcdef";
        when(mFulfilmentModelService.getFulfilmentModelBySeller(sellerCode, false)).thenReturn(fmExisting.name());

        //prepare a dummy list of SUPCs for the seller-supc-list mapping 

        when(request.getSellerCode()).thenReturn(sellerCode);

        List<String> supcListForSeller = new ArrayList<String>();

        int start = 100000;

        final int SUPC_LIST_SIZE = 200;
        for (int i = 1; i <= SUPC_LIST_SIZE; i++) {
            supcListForSeller.add(String.valueOf(start + i));
        }
        when(mSellerToSUPCListLookupService.getSupcListForSeller(sellerCode)).thenReturn(supcListForSeller);

        //prepare a dummy list of SUPCs for the seller-supc-exception-list mapping 
        List<String> supcExceptionListForSeller = new ArrayList<String>();

        final int SUPC_EXC_LIST_SIZE = 20;
        for (int i = 1; i <= SUPC_EXC_LIST_SIZE; i++) {
            supcExceptionListForSeller.add(String.valueOf(start + i * 7));
        }
        when(mSellerToSUPCListLookupService.getSupcExceptionListForSeller(sellerCode)).thenReturn(supcExceptionListForSeller);

        //write expected assert on list
        doAnswer(new Answer<Boolean>() {
            public Boolean answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                List<SellerSupcUpdateSRO> items = (List<SellerSupcUpdateSRO>) args[0];

                Assert.notNull(items);
                Assert.notEmpty(items);
                Assert.isTrue(items.size() == (SUPC_LIST_SIZE - SUPC_EXC_LIST_SIZE));

                SellerSupcUpdateSRO item = items.get(0);

                assertEquals("seller code", sellerCode, item.getSellerCode());
                assertEquals("fm model", (fmUsed == FulfillmentModel.FC_VOI), item.isSdFulfilled());

                return Boolean.TRUE;
            }

        }).when(mSellerSupcSDFulfilledProducer).publishToSearch(anyListOf(SellerSupcUpdateSRO.class));

        // action
        SetSellerFMMappingResponse resp = cut.setSellerFMMapping(request);

        try {
            //sleep 1 ms for the invoked thread to finish
            Thread.sleep(100);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        //verify
        assertTrue("resp ", resp.isSuccessful());

        verify(mSellerSupcSDFulfilledProducer, times(1)).publishToSearch(anyListOf(SellerSupcUpdateSRO.class));
    }

}
