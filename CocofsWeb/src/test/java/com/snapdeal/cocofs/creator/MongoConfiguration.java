/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 09-Oct-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.creator;

import java.io.IOException;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.stereotype.Component;

import com.mongodb.Mongo;

@Configuration
@Component
public class MongoConfiguration implements DisposableBean {

    public @Bean(name = "mongoCocofs")
    Mongo mongo() throws IOException {
        return MongoUtils.initMongo();
    }

    public @Bean(name = "mongoDbFactoryCocofs")
    MongoDbFactory mongoDbFactory() throws Exception {
        return new SimpleMongoDbFactory(mongo(), "cocofs");
    }

    public @Bean(name = "mongoTemplateCocofs")
    MongoTemplate mongoTemplate() throws Exception {
        return new MongoTemplate(mongoDbFactory());
    }

    @Override
    public void destroy() throws Exception {
        MongoUtils.shutdownMongo();

    }
}