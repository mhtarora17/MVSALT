/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 11-Oct-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.test;

import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.cocofs.configurations.IStartupService;
import com.snapdeal.cocofs.entity.CocofsProperty;
import com.snapdeal.cocofs.sro.FulfillmentModel;
import com.snapdeal.cocofs.web.controller.CocofsWebServiceController;

@ContextConfiguration(locations = { "/applicationContextWeb-test.xml" })
@Service("TestValidation")
public class TestValidation extends AbstractTransactionalTestNGSpringContextTests {

    private static final Logger        LOG = LoggerFactory.getLogger(TestValidation.class);

    @Autowired
    private CocofsWebServiceController controller;

    @Autowired
    IStartupService                    startupService;

    @Autowired
    SessionFactory                     sessionFactory;

    @BeforeMethod
    public void setup() throws Exception {
        insertWebServiceUrl();
        startupService.loadAll(true);
    }

        @Test
        public void test1() {
            for(int i = 0 ; i <  50;i++){
                long start = System.currentTimeMillis();
//                startupService.doSth(false);
                LOG.info(i + " " + (System.currentTimeMillis()- start) + " ms");
            }
            
            System.out.println(FulfillmentModel.getFulfilmentModelByCode("asdasd") == null);
    
        }

    private void insertWebServiceUrl() {
        CocofsProperty property = new CocofsProperty();
        property.setName(com.snapdeal.cocofs.configuration.Property.CATALOG_WEB_SERVICE_URL.getName());
        property.setValue("http://127.0.0.1");
        property.setCreated(DateUtils.getCurrentTime());
        sessionFactory.getCurrentSession().merge(property);
        CocofsProperty property1 = new CocofsProperty();
        property1.setName(com.snapdeal.cocofs.configuration.Property.OPS_WEB_SERVICE_URL.getName());
        property1.setValue("http://127.0.0.1");
        property1.setCreated(DateUtils.getCurrentTime());
        sessionFactory.getCurrentSession().merge(property1);
        CocofsProperty property2 = new CocofsProperty();
        property2.setName(com.snapdeal.cocofs.configuration.Property.IPMS_WEB_SERVICE_URL.getName());
        property2.setValue("http://127.0.0.1");
        property2.setCreated(DateUtils.getCurrentTime());
        sessionFactory.getCurrentSession().merge(property2);
    }
}
