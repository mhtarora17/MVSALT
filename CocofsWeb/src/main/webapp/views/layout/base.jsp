<%@ include file="/tld_includes.jsp"%>

<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><tiles:getAsString name="title" />
</title>
<tiles:insertAttribute name="head" defaultValue="" />
<%--  CSS files --%>
<link rel="shortcut icon" href="${path.resources('img/favicon.ico')}" type="image/x-icon" />
<link rel=icon type=image/ico href="${path.resources('img/favicon.ico')}" />

<link href="${path.css('snapdeal/jquery-ui-1.9.2.custom.css')}" rel="stylesheet" type="text/css" /> 
<link rel="stylesheet" type="text/css"
	href="${path.css('snapdeal/ui.jqgrid.css')}" />
<link href="${path.css('cocofs/style.css')}" rel="stylesheet"
	type="text/css" />
<link rel="stylesheet" type="text/css"
	href="${path.css('snapdeal/navstyle.css')}" />
<link href="${path.css('bootstrap/bootstrap.min.css')}" rel="stylesheet"
	type="text/css" />

	

<link href="${path.css('smoke/smoke.css')}" rel="stylesheet"
	type="text/css" />
<link id="theme" media="screen" type="text/css"
	href="${path.css('smoke/themes/dark.css')}" rel="stylesheet">
	
<style type="text/css">
.ui-jqgrid tr.jqgrow td {
	word-wrap: break-word; /* IE 5.5+ and CSS3 */
	white-space: pre-wrap; /* CSS3 */
	white-space: -moz-pre-wrap; /* Mozilla, since 1999 */
	white-space: -pre-wrap; /* Opera 4-6 */
	white-space: -o-pre-wrap; /* Opera 7 */
	overflow: hidden;
	height: auto;
	vertical-align: middle;
	padding-top: 3px;
	padding-bottom: 3px
}
</style>

	
	
<%-- JAVASCRIPT --%>
<script type="text/javascript"
	src="${path.js('jquery/jquery-1.9.1.min.js')}"></script>
<script type="text/javascript"
	src="${path.js('jquery/jquery.ui.core.min.js')}"></script>
<script type="text/javascript"
	src="${path.js('jquery/grid.locale-en.js')}"></script>
<script type="text/javascript"
	src="${path.js('jquery/jquery.jqGrid.min.js')}"></script>
<script type="text/javascript"
	src="${path.js('bootstrap/bootstrap.min.js')}"></script>

<script type="text/javascript"
	src="${path.js('mousetrap/mousetrap.min.js')}"></script>
<script type="text/javascript" src="${path.js('smoke/smoke.js')}">
	
</script>



<script type="text/javascript">
function checkJsonResponse(data, type) {
	if (data.successfull == false) {
		if (typeof type === 'undefined' || type == "signal") {
			smoke.signal(data.errorMessage, 3000);
		} else if (type == "alert") {
			smoke.alert(data.errorMessage);
		}
	}
}

function checkJsonResponseWithSuccessMessage(data, type, successMessage) {
	if (data.successfull == false) {
		if (typeof type === 'undefined' || type == "signal") {
			smoke.signal(data.errorMessage, 3000);
		} else if (type == "alert") {
			smoke.alert(data.errorMessage);
		}
	} else {
		smoke.alert(successMessage);
	}
}

	$(document).ready( function() {


				// For Numbers Only fields 	
				jQuery('.priceField').keypress(function(event) {
					var character = String.fromCharCode(event.which);
					var oldval = $(this).attr("value") + "";
					if (oldval.indexOf(".") == -1) {
						keyEnterResult(event);
					} else if (character == '.') {
						event.preventDefault();
					} else {
						keyEnterResult(event);
					}

				});

				function keyEnterResult(event) {
					// Backspace, tab, enter, end, home, left, right
					// We don't support the del key in Opera because del == . == 46.
					var controlKeys = [ 8, 9, 13, 35, 36, 37, 39 ];
					// IE doesn't support indexOf
					var isControlKey = controlKeys.join(",").match(
							new RegExp(event.which));
					// Some browsers just don't raise events for control keys. Easy.
					// e.g. Safari backspace.

					if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
					(48 <= event.which && event.which <= 57) || // Always 1 through 9
					(46 == event.which) || // No 0 first digit
					isControlKey) { // Opera assigns values for control keys.
						return;
					} else {
						event.preventDefault();
					}
				}

				function fixArray(array) {
					if ($.isArray(array)) {
						return array;
					} else {
						return [ array ];
					}
				}

				jQuery('.numbersOnly').keypress(
						function(event) {
							// Backspace, tab, enter, end, home, left, right
							// We don't support the del key in Opera because del == . == 46.
							var controlKeys = [ 8, 9, 13, 35, 36, 37, 39 ];
							// IE doesn't support indexOf
							var isControlKey = controlKeys.join(",").match(
									new RegExp(event.which));
							// Some browsers just don't raise events for control keys. Easy.
							// e.g. Safari backspace.
							if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
							(48 <= event.which && event.which <= 57) || // Always 1 through 9
							isControlKey) { // Opera assigns values for control keys.
								return;
							} else {
								event.preventDefault();
							}
						});

			});
</script>


<tiles:insertAttribute name="extraScripts" defaultValue="" />
</head>
<body class="body-container">
	<tiles:insertAttribute name="header"
		defaultValue="/views/layout/header.jsp" />
	<tiles:insertAttribute name="sidebar"
		defaultValue="" />
	<tiles:insertAttribute name="systemMessage"
		defaultValue="/views/layout/systemMessage.jsp" />
	<tiles:insertAttribute name="subheader" />
	<tiles:insertAttribute name="body" />
	<tiles:insertAttribute name="footer"
		defaultValue="/views/layout/footer.jsp" />
	<tiles:insertAttribute name="deferredScript" defaultValue="" />
</body>
</html>
