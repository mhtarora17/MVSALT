<%@ include file="/tld_includes.jsp"%>

<div class=" navbar navbar-inverse navbar-fixed-top cfloat">
	<div class="navbar-inner">
		<a class="brand" href="#">CoCoFS</a>
		<div id="header" class="header lpadding-normal wp82 lfloat"></div>

		<sec:authentication property="principal" var="user" />
		<ul class="nav">
			<sec:authorize ifAnyGranted="tech">
				<li id="header-nav-admintasks"><a
					href="${path.http}/admin/cache/cacheview"> Admin Tasks</a>
				</li>
				<li class="divider-vertical"></li>
			</sec:authorize>

		</ul>
		<ul class="nav pull-right">
			<li class="dropdown"><a class="dropdown-toggle"
				data-toggle="dropdown" href="#"> Hello, <c:choose>
						<c:when test="${currentUser == null}">
				      User 
				     </c:when>
						<c:otherwise>
				     ${currentUser}
				     </c:otherwise>
					</c:choose> <span class="caret"></span> </a>
				<ul class="dropdown-menu">
					<li><a href="${path.http}/updatePasswordLanding">Change
							Password</a>
					</li>
					<li><a href="${path.http}/logout"> Logout</a>
					</li>
				</ul></li>
		</ul>

	</div>
</div>