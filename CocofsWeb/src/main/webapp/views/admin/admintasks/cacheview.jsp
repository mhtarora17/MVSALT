<%@ include file="/tld_includes.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<tiles:insertTemplate template="/views/layout/base.jsp">
	<tiles:putAttribute name="title" value="Cache View" />

	<tiles:putAttribute name="systemMessage">
		<div class="system-message"></div>
	</tiles:putAttribute>

	<tiles:putAttribute name="extraScripts">
    <link href="${path.css('chosen/chosen.css')}" rel="stylesheet"
    type="text/css" />
    <link rel="stylesheet" type="text/css"
            href="${path.css('snapdeal/ui.jqgrid.css')}" />

    <script type="text/javascript"
    src="${path.js('chosen/chosen.jquery.min.js')}"></script>
    


	</tiles:putAttribute>
	<tiles:putAttribute name="sidebar">
		<tiles:insertTemplate template="/views/admin/admintasks/sidebar.jsp">
		</tiles:insertTemplate>
	</tiles:putAttribute>
	<tiles:putAttribute name="systemMessage">
		<div class="system-message">
			<input type="hidden" value="${message }" id="messageHiddenField"/>
		</div>
	</tiles:putAttribute>


	<tiles:putAttribute name="subheader">
		<tiles:insertTemplate template="/views/admin/admintasks/cacheview-subheader.jsp">
		</tiles:insertTemplate>
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
		<div class="main-content lfloat">
			<div class="container" style="width: 85%;">
			<div>
					<form id="access-form" class="form-horizontal" method="get" action='#'>
					    
                        <div class="control-group">
                        <label class="control-label" for="frm-key">Access Key</label>
                        <div class="controls">
                            <input type="password" id="frm-key" />
                        </div>
                        </div>
                        
                        <div class="control-group">
                        <div class="controls">
                            <button id="fetch-summary" type="button" class="btn btn-primary">Fetch Summary</button>
                    
                        </div>
                        </div>
                        
                        <br/>
                        
                        <div class="control-group">
                        <label class="control-label" for="frm-cachename">Cache Name</label>
                        <div class="controls">
                            <input type="text" id="frm-cachename" />
                        </div>
                        </div>
                        
                        <div class="control-group">
                        <div class="controls">
                            <button id="fetch-cachesize" type="button" class="btn btn-primary">Get Cache Size</button>
                            <button id="fetch-cachesizeinmemory" type="button" class="btn btn-primary">Get Cache Size In Memory</button>
                        </div>
                        </div>
                       
                        <br/>
                        
                        <div class="control-group">
                        <label class="control-label" for="frm-keyname">Key Name</label>
                        <div class="controls">
                            <input type="text" id="frm-keyname" />
                        </div>
                        </div>
                        
                        <div class="control-group">
                        <div class="controls">
                            <button id="fetch-clearkey" type="button" class="btn btn-primary">Clear Key</button>
                            <button id="fetch-getkey" type="button" class="btn btn-primary">Get Key Data</button>
                        </div>
                        </div>
                        
					</form>
			</div>
            <div id="all-stats-div"></div>
            


            </div>
		</div>
	</tiles:putAttribute>
	<tiles:putAttribute name="deferredScript">
	<%-- Activate relevant sidebar function   --%>
	<%-- Activate relevant subheader function --%>
		<script type="text/javascript">
			function hideSidebar() {
				$(".subheader").css({
					"left" : "-11%"
				});
				$(".functionAnchor").css({
					"display" : "none"
				});
				$(".sidebar").css({
					"left" : "1%",
					"width" : "99%"
				});
				$(".main-content").css({
					"left" : "1%",
					"width" : "98%"
				});
			}
			function showSidebar() {
				$(".subheader").css({
					"left" : "0%"
				});
				$(".functionAnchor").css({
					"display" : "block"
				});
				$(".sidebar").css({
					"left" : "12%",
					"width" : "88%"
				});
				$(".main-content").css({
					"left" : "12%",
					"width" : "87%"
				});
			}
			
			
			function getCacheData(urltoget) {
				
				var responseObj = {success:false, message:'Dummy message', obj:null};
				$.ajax( {dataType:'json' , url:urltoget, 
                    async : false ,
                    type : 'GET' , 
                    success: function(data, textStatus,  jqXHR ) {
                    	if ( (! data.code) || (data.code == 200)) {
                    		/* We got valid response */
                    		responseObj.success = true;
                    		responseObj.obj = data;
                    	} else {
                    		responseObj.message = data.message;
                    	}
                    },
                    error : function(jqXHR, textStatus, errorThrown) { responseObj.message = textStatus + errorThrown },
				});
				return responseObj;
            }
			
			function resetcachestat(accesskey, cachename) {
				/*populateAllStats*/
				var url = '/cache/' + encodeURIComponent(accesskey) + '/resetStats/' + encodeURIComponent(cachename);
                var resp = getCacheData(url);
                if (resp.success) {
                	alert("Cache Stats Reset " + cachename );
                	populateAllStats(accesskey);
                } else {
                	alert("Cache Stats Not Reset " + resp.message );
                }
			}
			function clearcache(accesskey,cachename) {
				var url = '/cache/' + encodeURIComponent(accesskey) + '/clear/' + encodeURIComponent(cachename);
                var resp = getCacheData(url);
                if (resp.success) {
                    alert("Cache Cleared " + cachename );
                    populateAllStats(accesskey);
                } else {
                    alert("Cache Not Cleared " + resp.message );
                }
			}
			
			
			function buildAllStatsHtml(accesskey, slist ) {
				var str = '<table class="table table-bordered">';
				str += '<thead><tr>';
				str += '<th>Name</th>';
				str += '<th>Hits</th>';
				str += '<th>Misses</th>';
				str += '<th>Hit Ratio</th>';
				str += '<th>Object Count</th>';
				str += '<th>Reset</th>';
				str += '<th>Clear Cache</th>';
				str += '</tr></thead>';
				str += '<tbody>';
				var i = 0;
				for ( i = 0; i < slist.length; i++ ) {
					str += '<tr>';
					str += '<td>'+ slist[i].cacheName + '</td>';
					str += '<td>'+ slist[i].hits + '</td>';
					str += '<td>'+ slist[i].misses + '</td>';
					str += '<td>'+ slist[i].hitRatio + '</td>';
					str += '<td>'+ slist[i].objectCount + '</td>';
					str += '<td>' + '<button id="frm-statreset-btn-'+i;
					str += '" type="button"  class="btn" onclick="resetcachestat(\''+accesskey+'\',\''+slist[i].cacheName+'\')"';
					str += '>Reset Stats </button>'; 
					str += '</td>';
					str += '<td>' + '<button id="frm-cacheclear-btn-'+i;
                    str += '" type="button"  class="btn" onclick="clearcache(\''+accesskey+'\',\''+slist[i].cacheName+'\')"';
                    str += '>Clear cache </button>'; 
                    str += '</td>';
					str += '</tr>';
				}
				str += '</tbody>';
				str += '</table>';
			    return str;
			}
			function buildErrorAlertWithMessage(message ) {
				var str = '<div class="alert alert-error"> <button type="button" class="close" data-dismiss="alert">&times;</button>' + message + '</div>';
			    return str;
			}
			
			function populateAllStats(accesskey) {
				var url = '/cache/' + encodeURIComponent(accesskey) + '/getStats';
				var resp = getCacheData(url);
				if (resp.success) {
					$('#all-stats-div').html(buildAllStatsHtml(accesskey, resp.obj));
				} else {
					$('#all-stats-div').html(buildErrorAlertWithMessage(resp.message));
				}
			}
			
			function getCacheSize(accesskey, cachename) {
				var url = '/cache/' + encodeURIComponent(accesskey) + '/getSize/' + encodeURIComponent(cachename);
                var resp = getCacheData(url);
                if (resp.success) {
                    alert("Cache Size " + resp.obj );
                } else {
                    alert("Could not get cache size " + resp.message );
                }
				
			}
            function getCacheSizeInMemory(accesskey, cachename) {
            	
            	var url = '/cache/' + encodeURIComponent(accesskey) + '/getSizeInMemory/' + encodeURIComponent(cachename);
                var resp = getCacheData(url);
                if (resp.success) {
                    alert("Cache Size " + resp.obj );
                } else {
                    alert("Could not get cache size " + resp.message );
                }
                
            }
            
            function getKeyData(accesskey, cachename, keyname ) {
            	var url = '/cache/' + encodeURIComponent(accesskey) ;
            	url += '/getObject/' + encodeURIComponent(cachename) 
            	url += '/' + encodeURIComponent(keyname);
                var resp = getCacheData(url);
                if (resp.success) {
                	/* TODO pop up over */
                	if ( resp.message == undefined ) {
                		alert("Cache Key Data " + JSON.stringify(resp.obj )   );
                        
                	} else {
                		alert("Cache Key Data  message " +  resp.message  );
                	}
                    
                } else {
                    alert("Could not get cache key data " + resp.message );
                }
            }
            
            function clearKeyData(accesskey, cachename, keyname ) {
            	var url = '/cache/' + encodeURIComponent(accesskey) ;
                url += '/remove/' + encodeURIComponent(cachename) 
                url += '/' + encodeURIComponent(keyname);
                var resp = getCacheData(url);
                if (resp.success) {
                    alert("Cleared cache key data cache " + cachename + ' key ' +keyname + ' response ' +  resp.message );
                } else {
                    alert("Could not clear cache key data " + resp.message );
                }
            }
			
			

			$(document).ready(function() {
				var subheader = true;
				$(".subheaderToggle").click(function() {
					if (subheader) {
						hideSidebar();
						$(".subheaderToggleContent").html(">");
						subheader = false;
					} else {
						showSidebar();
						$(".subheaderToggleContent").html("<");
						subheader = true;
					}

				});
				/* check sidebar jsp for relevant div ids
				 */
				$('#sidebar-fn-cacheview').addClass('active');
				$('#header-nav-admintasks').addClass('active');
				$('.chosen-select').chosen();
				
				$('#fetch-summary').click(function() {
					var accesskey = $("#frm-key").val().trim();
					populateAllStats(accesskey);
				});
				
				$('#fetch-cachesize').click(function() {
					var accesskey = $("#frm-key").val().trim();
                    var cachename = $("#frm-cachename").val().trim();
                    getCacheSize(accesskey, cachename);
                });
				$('#fetch-cachesizeinmemory').click(function() {
					var accesskey = $("#frm-key").val().trim();
                    var cachename = $("#frm-cachename").val().trim();
                    getCacheSizeInMemory(accesskey, cachename);
                });
				
				$('#fetch-clearkey').click(function() {
                    var accesskey = $("#frm-key").val().trim();
                    var cachename = $("#frm-cachename").val().trim();
                    var keyname   = $("#frm-keyname").val().trim();
                    clearKeyData(accesskey, cachename, keyname );
                });
				
				$('#fetch-getkey').click(function() {
                    var accesskey = $("#frm-key").val().trim();
                    var cachename = $("#frm-cachename").val().trim();
                    var keyname   = $("#frm-keyname").val().trim();
                    getKeyData(accesskey, cachename, keyname );
                });
				/* check subheader jsp for relevant div ids
				/cache
				 */
				/* $('#subheader-fn-usrmanagement').addClass('active') */

			});
		</script>
			
		
	</tiles:putAttribute>
</tiles:insertTemplate>