<%@ include file="/tld_includes.jsp"%>


<div id="subheader" class="subheader"
	style="background-image: url(${path.resources('img/background/bg_blue.png')});">
	<div class="tabbable">
		<!-- Only required for left/right tabs -->
		<div class="subheaderTab">
        <sec:authorize ifAnyGranted="tech">
		    <div id="sidebar-fn-cacheview" onclick="javascript:window.location.href='/admin/cache/cacheview'">
		    Cache Control </div>
        </sec:authorize>
        
		</div>
	</div>
	<div class="subheaderToggle">
		<div class="subheaderToggleContent">&lt</div>
	</div>
</div>