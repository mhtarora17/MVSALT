package com.snapdeal.cocofs.web.landing.layer;

import com.snapdeal.cocofs.model.request.GetFMAndZonesBySellerSupcListRequest;
import com.snapdeal.cocofs.model.request.GetZonesBySellerSupcListRequest;
import com.snapdeal.cocofs.model.response.GetFMAndZonesBySellerSupcListResponse;
import com.snapdeal.cocofs.model.response.GetZonesBySellerSupcListResponse;

/**
 * @author ankur
 */
public interface ICocofsZoneLandingService {


    public GetZonesBySellerSupcListResponse getZonesBySellerSupcList(GetZonesBySellerSupcListRequest request);

    GetFMAndZonesBySellerSupcListResponse getFMAndZonesBySellerSupcList(GetFMAndZonesBySellerSupcListRequest request);
    

}
