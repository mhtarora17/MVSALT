package com.snapdeal.cocofs.web.landing.layer.impl;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.snapdeal.base.annotations.NotNull;
import com.snapdeal.base.model.common.ServiceRequest;
import com.snapdeal.base.utils.CollectionUtils;
import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.cocofs.annotation.Limited;
import com.snapdeal.cocofs.annotation.NonNegative;
import com.snapdeal.cocofs.annotation.NonZero;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.web.landing.layer.IRequestValidationHelper;

/**
 * @author nikhil
 */
@Service("requestValidationHelper")
public class RequestValidationHelper implements IRequestValidationHelper {

    private static final Logger LOG = LoggerFactory.getLogger(RequestValidationHelper.class);

    @Override
    public <T> List<String> checkForNullFields(T t) throws IllegalArgumentException, IllegalAccessException {
        List<String> nullFieldList = new ArrayList<String>();
        if (t == null) {
            nullFieldList.add(ServiceRequest.class.getName());
        }

        for (Field field : t.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            if (field.isAnnotationPresent(NotNull.class)) {
                if (!field.getType().isPrimitive()) {
                    if (field.get(t) == null || (field.get(t) != null && field.getType().equals(String.class) && StringUtils.isEmpty((String) field.get(t)))) {
                        nullFieldList.add(field.getName());
                    } else {
                        nullFieldList.addAll(checkForInnerObjects(field.get(t), field));
                    }
                }
            }
        }
        return nullFieldList;
    }

    private <T> List<String> checkForInnerObjects(T t, Field field) throws IllegalArgumentException, IllegalAccessException {
        List<String> nullFieldList = new ArrayList<String>();
        if (t instanceof List) {
            if (CollectionUtils.isEmpty((List<?>) t)) {
                nullFieldList.add(field.getName());
                return nullFieldList;
            } else {
                for (Object obj : (List<?>) t) {
                    if (null == obj) {
                        nullFieldList.add(field.getName());
                        return nullFieldList;
                    }
                    nullFieldList.addAll(checkForNullFields(obj));
                }
            }
        } else if (t instanceof Set) {
            if (CollectionUtils.isEmpty((Set<?>) t)) {
                nullFieldList.add(field.getName());
                return nullFieldList;
            } else {
                for (Object obj : (Set<?>) t) {
                    if (nullFieldList == obj) {
                        nullFieldList.add(field.getName());
                        return nullFieldList;
                    }
                    nullFieldList.addAll(checkForNullFields(obj));
                }
            }
        } else if (t instanceof Map) {
            if (((Map<?, ?>) t).isEmpty()) {
                nullFieldList.add(field.getName());
                return nullFieldList;
            } else {
                for (Entry<?, ?> entry : ((Map<?, ?>) t).entrySet()) {
                    nullFieldList.addAll(checkForNullFields(entry.getKey()));
                    if (null != entry.getValue()) {
                        nullFieldList.addAll(checkForNullFields(entry.getValue()));
                    }
                }
            }
        } else {
            nullFieldList.addAll(checkForNullFields(t));
        }
        return nullFieldList;

    }

    @Override
    public <T> boolean batchRequestSizeAllowed(T t) throws IllegalArgumentException, IllegalAccessException {
        Field[] fields = t.getClass().getFields();

        for (Field field : fields) {
            if (field.isAnnotationPresent(Limited.class)) {
                String batchSize = ConfigUtils.getMapProperty(Property.BATCH_SIZE_FOR_API, t.getClass().getName());
                int configuredBatchSize = 0;
                int collectionSize = 0;
                if (StringUtils.isEmpty(batchSize)) {
                    LOG.warn("No batch size found for the given batched API {} ", t.getClass().getName());
                    throw new RuntimeException();
                } else {
                    configuredBatchSize = Integer.valueOf(batchSize);
                }
                Object collection = field.get(t);

                if (collection instanceof Map) {
                    collectionSize = ((Map<?, ?>) collection).size();
                } else if (collection instanceof List) {
                    collectionSize = ((List<?>) collection).size();
                } else if (collection instanceof Set) {
                    collectionSize = ((Set<?>) collection).size();
                }

                return (configuredBatchSize > collectionSize);
            }
        }

        return true;

    }

    @Override
    public <T> List<String> checkForZeroFields(T t) throws IllegalArgumentException, IllegalAccessException {

        List<String> zeroFieldList = new ArrayList<String>();
        if (t == null) {
            throw new IllegalArgumentException("Null object recieved as an argument");
        }

        for (Field field : t.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            if (field.isAnnotationPresent(NonZero.class)) {

                if (!field.getType().isPrimitive()) {
                    if (field.getType().equals(Integer.class)) {
                        if (((Integer) field.get(t)).intValue() == 0) {
                            zeroFieldList.add(field.getName());
                        }
                    } else if (field.getType().equals(Double.class)) {
                        if (((Double) field.get(t)).doubleValue() == 0.0) {
                            zeroFieldList.add(field.getName());
                        }
                    } else if (field.getType().equals(Long.class)) {
                        if (((Long) field.get(t)).longValue() == 0L) {
                            zeroFieldList.add(field.getName());
                        }
                    }
                } else {
                    if (field.getType().equals(int.class)) {
                        if (field.getInt(t) == 0) {
                            zeroFieldList.add(field.getName());
                        }
                    } else if (field.getType().equals(double.class)) {
                        if (field.getDouble(t) == 0.0) {
                            zeroFieldList.add(field.getName());
                        }
                    } else if (field.getType().equals(long.class)) {
                        if (field.getLong(t) == 0l) {
                            zeroFieldList.add(field.getName());
                        }
                    }
                }
            }

        }
        return zeroFieldList;
    }

    @Override
    public <T> List<String> checkForNegativeFields(T t) throws IllegalArgumentException, IllegalAccessException {

        List<String> negativeFieldList = new ArrayList<String>();
        if (t == null) {
            throw new IllegalArgumentException("Null object recieved as an argument");
        }

        for (Field field : t.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            if (field.isAnnotationPresent(NonNegative.class)) {

                if (!field.getType().isPrimitive()) {
                    if (field.getType().equals(Integer.class)) {
                        if (((Integer) field.get(t)).intValue() < 0) {
                            negativeFieldList.add(field.getName());
                        }
                    } else if (field.getType().equals(Double.class)) {
                        if (((Double) field.get(t)).doubleValue() < 0 ) {
                            negativeFieldList.add(field.getName());
                        }
                    } else if (field.getType().equals(Long.class)) {
                        if (((Long) field.get(t)).longValue() < 0) {
                            negativeFieldList.add(field.getName());
                        }
                    }
                } else {
                    if (field.getType().equals(int.class)) {
                        if (field.getInt(t) < 0) {
                            negativeFieldList.add(field.getName());
                        }
                    } else if (field.getType().equals(double.class)) {
                        if (field.getDouble(t) < 0) {
                            negativeFieldList.add(field.getName());
                        }
                    } else if (field.getType().equals(long.class)) {
                        if (field.getLong(t) < 0) {
                            negativeFieldList.add(field.getName());
                        }
                    }
                }
            }

        }
        return negativeFieldList;
    }


//    public static void main(String[] args) throws Exception {
    //        //            GetFulfilmentModelResponse response = new GetFulfilmentModelResponse();
    //        //            Map<SellerSUPCPair, FulfillmentModel> map = new HashMap<SellerSUPCPair, FulfillmentModel>();
    //        //            SellerSUPCPair pair = new SellerSUPCPair();
    //        //            map.put(pair, null);
    //        //            response.setSellerSupcToFulfilmentModel(map);
    //        //                GetProductPackagingTypeRequest request = new GetProductPackagingTypeRequest();
    //        //                List<String> sellers = new ArrayList<String>();
    //        //                sellers.add("121");
    //        //                sellers.add(null);
    //        //                request.setSupcList(sellers);
    //        //        
    //        //        AddOrUpdateSellerSubcatFulfilmentModelMappingRequest request = new AddOrUpdateSellerSubcatFulfilmentModelMappingRequest();
    //        //        List<SellerSubcatFulfilmentModelMappingSRO> list = new ArrayList<SellerSubcatFulfilmentModelMappingSRO>();
    //        //        SellerSubcatFulfilmentModelMappingSRO sro = new SellerSubcatFulfilmentModelMappingSRO();
    //        //        list.add(sro);
    //        //        request.setSellerSubcatFulfilmentModelMappingList(list);
    //    }
}
