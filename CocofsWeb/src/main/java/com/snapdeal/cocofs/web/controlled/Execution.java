/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 18-Sep-2013
 *  @author himanshum
 */
package com.snapdeal.cocofs.web.controlled;

public interface Execution<Q, S> {
    
    public S execute(Q q);

}
