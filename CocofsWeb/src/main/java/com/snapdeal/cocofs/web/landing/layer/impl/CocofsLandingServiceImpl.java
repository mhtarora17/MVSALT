package com.snapdeal.cocofs.web.landing.layer.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.naming.OperationNotSupportedException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.base.validation.ValidationError;
import com.snapdeal.cocofs.access.IPendingProductAttributeDBDataReadService;
import com.snapdeal.cocofs.access.IProductAttributeNotificationMongoDataReadService;
import com.snapdeal.cocofs.access.IProductAttributeService;
import com.snapdeal.cocofs.access.IShippingModeService;
import com.snapdeal.cocofs.common.DeliveryTypeForSUPCResponse;
import com.snapdeal.cocofs.common.PackagingTypeForSUPCResponse;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.core.aerospike.vo.FCDetailVO;
import com.snapdeal.cocofs.entity.FulfillmentCentre;
import com.snapdeal.cocofs.entity.PendingProductAttributeUpdate;
import com.snapdeal.cocofs.entity.ProductAttribute;
import com.snapdeal.cocofs.excelupload.ProductAttributeUploadDTO;
import com.snapdeal.cocofs.external.service.IScoreExternalService;
import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.external.service.exception.FulfillmentModelNotFoundException;
import com.snapdeal.cocofs.model.request.AddOrUpdateCategoryModeMappingRequest;
import com.snapdeal.cocofs.model.request.AddOrUpdateSupcModeMappingRequest;
import com.snapdeal.cocofs.model.request.GetAllCategoryModeMappingRequest;
import com.snapdeal.cocofs.model.request.GetAllCenterTypesRequest;
import com.snapdeal.cocofs.model.request.GetAllFCDetailRequest;
import com.snapdeal.cocofs.model.request.GetAllSupcModeMappingRequest;
import com.snapdeal.cocofs.model.request.GetCategoryModeMappingRequest;
import com.snapdeal.cocofs.model.request.GetFulfilmentFeeParamRequest;
import com.snapdeal.cocofs.model.request.GetFulfilmentModelAndProductAttributesRequest;
import com.snapdeal.cocofs.model.request.GetFulfilmentModelAndWeightRequest;
import com.snapdeal.cocofs.model.request.GetGiftWrapperInfoBySUPCSellerListRequest;
import com.snapdeal.cocofs.model.request.GetGiftWrapperInfoBySUPCSellerListResponse;
import com.snapdeal.cocofs.model.request.GetGiftWrapperInfoBySUPCVendorRequest;
import com.snapdeal.cocofs.model.request.GetMultiplierForPerimeterRequest;
import com.snapdeal.cocofs.model.request.GetProductDeliveryAndPackagingTypeRequest;
import com.snapdeal.cocofs.model.request.GetProductDeliveryTypeRequest;
import com.snapdeal.cocofs.model.request.GetProductFulfilmentAttributeRequest;
import com.snapdeal.cocofs.model.request.GetProductPackagingTypeRequest;
import com.snapdeal.cocofs.model.request.GetProductSystemWeightCapturedFlagRequest;
import com.snapdeal.cocofs.model.request.GetProductWeightNotificationRequest;
import com.snapdeal.cocofs.model.request.GetSupcModeMappingRequest;
import com.snapdeal.cocofs.model.request.GetVolumetricWeightFormulaParamsRequest;
import com.snapdeal.cocofs.model.request.RemoveCategoryModeMappingRequest;
import com.snapdeal.cocofs.model.request.RemoveSupcModeMappingRequest;
import com.snapdeal.cocofs.model.request.SetProductCapturedWeightRequest;
import com.snapdeal.cocofs.model.request.SetProductFulfilmentAttributeRequest;
import com.snapdeal.cocofs.model.response.AddOrUpdateCategoryModeMappingResponse;
import com.snapdeal.cocofs.model.response.AddOrUpdateSupcModeMappingResponse;
import com.snapdeal.cocofs.model.response.AllCenterTypesResponse;
import com.snapdeal.cocofs.model.response.AllFCDetailResponse;
import com.snapdeal.cocofs.model.response.ApiErrorCode;
import com.snapdeal.cocofs.model.response.GetAllCategoryModeMappingResponse;
import com.snapdeal.cocofs.model.response.GetAllDangerousGoodsTypeResponse;
import com.snapdeal.cocofs.model.response.GetAllSupcModeMappingResponse;
import com.snapdeal.cocofs.model.response.GetCategoryModeMappingResponse;
import com.snapdeal.cocofs.model.response.GetFulFilmentFeeParamResponse;
import com.snapdeal.cocofs.model.response.GetFulfilmentModelAndProductAttributesResponse;
import com.snapdeal.cocofs.model.response.GetFulfilmentModelAndWeightResponse;
import com.snapdeal.cocofs.model.response.GetGiftWrapperInfoBySUPCVendorResponse;
import com.snapdeal.cocofs.model.response.GetMultiplierForPerimeterResponse;
import com.snapdeal.cocofs.model.response.GetProductDeliveryAndPackagingTypeResponse;
import com.snapdeal.cocofs.model.response.GetProductDeliveryTypeResponse;
import com.snapdeal.cocofs.model.response.GetProductFulfilmentAttributeResponse;
import com.snapdeal.cocofs.model.response.GetProductPackagingTypeResponse;
import com.snapdeal.cocofs.model.response.GetProductSystemWeightCapturedFlagResponse;
import com.snapdeal.cocofs.model.response.GetProductWeightNotificationResponse;
import com.snapdeal.cocofs.model.response.GetSupcModeMappingResponse;
import com.snapdeal.cocofs.model.response.GetVolumetricWeightFormulaParamsResponse;
import com.snapdeal.cocofs.model.response.RemoveCategoryModeMappingResponse;
import com.snapdeal.cocofs.model.response.RemoveSupcModeMappingResponse;
import com.snapdeal.cocofs.model.response.SetProductAttributeFulfilmentResponse;
import com.snapdeal.cocofs.model.response.SetProductCapturedWeightResponse;
import com.snapdeal.cocofs.mongo.model.ProductAttributeNotificationUnit;
import com.snapdeal.cocofs.mongo.model.ProductAttributeUnit;
import com.snapdeal.cocofs.services.IDataReader;
import com.snapdeal.cocofs.services.IDataUpdater;
import com.snapdeal.cocofs.services.IDeliveryTypeService;
import com.snapdeal.cocofs.services.IGiftWrapService;
import com.snapdeal.cocofs.services.IPackagingTypeService;
import com.snapdeal.cocofs.services.ISellerZoneService;
import com.snapdeal.cocofs.services.common.dto.UserInfo;
import com.snapdeal.cocofs.services.converter.IFCConvertorService;
import com.snapdeal.cocofs.services.data.engine.IDataEngine;
import com.snapdeal.cocofs.services.fm.IFulfillmentCenterService;
import com.snapdeal.cocofs.services.fm.IFulfilmentModelService;
import com.snapdeal.cocofs.sro.CategoryShippingModeSRO;
import com.snapdeal.cocofs.sro.FCDetailSRO;
import com.snapdeal.cocofs.sro.FulfillmentModel;
import com.snapdeal.cocofs.sro.FulfilmentFeeParamSRO;
import com.snapdeal.cocofs.sro.GiftWrapProductSRO;
import com.snapdeal.cocofs.sro.GiftWrapperInfo;
import com.snapdeal.cocofs.sro.ProductFulfilmentAttributeSRO;
import com.snapdeal.cocofs.sro.SellerSUPCPair;
import com.snapdeal.cocofs.sro.SellerSupcSubcatSro;
import com.snapdeal.cocofs.sro.SupcShippingModeSRO;
import com.snapdeal.cocofs.utils.ProductAttributeUtil;
import com.snapdeal.cocofs.utils.UtilHelper;
import com.snapdeal.cocofs.web.builder.IResponseBuilder;
import com.snapdeal.cocofs.web.landing.layer.ICocofsLandingService;
import com.snapdeal.cocofs.web.landing.layer.IConverterService;
import com.snapdeal.cocofs.web.landing.layer.IRequestValidationService;

/**
 * @author nikhil
 */
@Service("cocofsLandingService")
public class CocofsLandingServiceImpl implements ICocofsLandingService {

    @Autowired
    private IProductAttributeService                                                       productAttributeService;

    @Autowired
    private IResponseBuilder                                                               responseBuilder;

    @Autowired
    private IRequestValidationService                                                      requestValidationService;

    @Autowired
    private IConverterService                                                              converterService;

    @Autowired
    private IDataUpdater                                                                   dataUpdater;

    @Autowired
    private IPackagingTypeService                                                          packagingTypeService;

    @Autowired
    private IDeliveryTypeService                                                           deliveryTypeService;

    @Autowired
    private IFulfilmentModelService                                                        fulfilmentModelService;

    @Autowired
    private IShippingModeService                                                           shippingModeService;

    @Autowired
    private IScoreExternalService                                                          scoreService;

    @Autowired
    @Qualifier("productAttributeUploadReader")
    private IDataReader<ProductAttributeUploadDTO, ProductAttribute, ProductAttributeUnit> dataReader;

    @Autowired
    @Qualifier("productAttributeCreateOnlyEngine")
    private IDataEngine<ProductAttributeUploadDTO, ProductAttribute, ProductAttributeUnit> dataEngine;

    @Autowired
    @Qualifier("productAttributeUploadEngine")
    private IDataEngine<ProductAttributeUploadDTO, ProductAttribute, ProductAttributeUnit> uploadDataEngine;

    @Autowired
    IProductAttributeNotificationMongoDataReadService                                      panMongoDataReadService;

    @Autowired
    IPendingProductAttributeDBDataReadService                                              pendingPADBDataReadService;

    @Autowired
    IGiftWrapService                                                                       giftWrapService;

    @Autowired
    IFulfillmentCenterService                                                              fulfillmentCenterService;

    @Autowired
    ISellerZoneService                                                                     sellerZoneService;

    @Autowired
    IFCConvertorService                                                                    fcConvertorService;

    private static final Logger                                                            LOG = LoggerFactory.getLogger(CocofsLandingServiceImpl.class);

    @Override
    public GetGiftWrapperInfoBySUPCVendorResponse getGiftWrapperInfoBySUPCVendor(GetGiftWrapperInfoBySUPCVendorRequest request) {
        LOG.debug("Request received {}", request);
        GetGiftWrapperInfoBySUPCVendorResponse response = new GetGiftWrapperInfoBySUPCVendorResponse();
        response.setProtocol(request.getResponseProtocol());

        ValidationError error = requestValidationService.requestBasicFieldValidation(request);
        if (error != null) {
            LOG.error("Validation error found request for supc {}", request != null ? request.getSupc() : null);
            responseBuilder.buildFailedResponse(error, response);
            return response;
        }

        GiftWrapperInfo giftWrapperInfo = null;
        try {
            giftWrapperInfo = giftWrapService.getGiftWrapInfoBySUPCSeller(request.getSupc(), request.getVendorCode(), request.getSubcat(), request.getSellingPrice());

        } catch (ExternalDataNotFoundException e) {
            LOG.error("No info found for request supc:" + request.getSupc() + " vendor:" + request.getVendorCode());
            responseBuilder.buildFailedResponse(ApiErrorCode.REQUESTED_DATA_DOES_NOT_EXISTS, response);
        } catch (FulfillmentModelNotFoundException e) {
            LOG.error("No fullfilment model found for request supc:" + request.getSupc() + " vendor:" + request.getVendorCode());
            responseBuilder.buildFailedResponse(ApiErrorCode.NO_FULFILLMENT_MODEL_FOUND, response);
        } catch (Exception e) {
            LOG.error("unexpected error occured while trying to get giftWrap info  for request supc:" + request.getSupc() + " vendor:" + request.getVendorCode(), e);
            responseBuilder.buildFailedResponse(ApiErrorCode.INTERNAL_ERROR, response);
        }

        response.setGiftWrapperInfo(giftWrapperInfo);
        LOG.debug("Sending response {}", response);
        return response;
    }

    @Override
    public GetGiftWrapperInfoBySUPCSellerListResponse getGiftWrapperInfoBySUPCSellerList(GetGiftWrapperInfoBySUPCSellerListRequest request) {
        LOG.debug("Request received {}", request);
        GetGiftWrapperInfoBySUPCSellerListResponse response = new GetGiftWrapperInfoBySUPCSellerListResponse();
        response.setProtocol(request.getResponseProtocol());

        ValidationError error = requestValidationService.requestBasicFieldValidation(request);
        if (error != null) {
            LOG.error("Validation error found for request {}", request != null ? request : null);
            responseBuilder.buildFailedResponse(error, response);
            return response;
        } else if (request.getGiftWrapProducts().size() > ConfigUtils.getIntegerScalar(Property.GIFTWRAP_API_SELLER_BATCH_SIZE)) {
            LOG.error("sellers size is more than the allowed batch size for request {}", request);
            responseBuilder.buildFailedResponse(ApiErrorCode.REQUEST_BATCH_SIZE_LIMIT_EXCEEDED, response);
            return response;
        }

        Map<GiftWrapProductSRO, Boolean> productGiftwrapMap = null;
        try {
            productGiftwrapMap = giftWrapService.getGiftWrapInfoBySUPCSellerList(request.getGiftWrapProducts());

        } catch (Exception e) {
            LOG.error("unexpected error occured while trying to get giftWrap info  for request supc:" + request.getGiftWrapProducts(), e);
            responseBuilder.buildFailedResponse(ApiErrorCode.INTERNAL_ERROR, response);
        }

        if (null != productGiftwrapMap && !productGiftwrapMap.isEmpty()) {
            response.setProductGiftWrapMap(productGiftwrapMap);
        } else {
            responseBuilder.buildFailedResponse(ApiErrorCode.REQUESTED_DATA_DOES_NOT_EXISTS, response);
        }
        LOG.debug("Sending response {}", response);
        return response;
    }

    @Override
    public GetProductFulfilmentAttributeResponse getProductFulfilmentAttributes(GetProductFulfilmentAttributeRequest request) {
        LOG.debug("Request received {}", request);
        GetProductFulfilmentAttributeResponse response = new GetProductFulfilmentAttributeResponse();
        response.setProtocol(request.getResponseProtocol());

        ValidationError error = requestValidationService.requestBasicFieldValidation(request);
        if (error != null) {
            LOG.error("Validation error found request for supc {}", request != null ? request.getSupc() : null);
            responseBuilder.buildFailedResponse(error, response);
            return response;
        }

        ProductAttributeUnit productAttributeUnit = productAttributeService.getProductAttributeUnitBySUPC(request.getSupc());

        if (productAttributeUnit == null) {
            LOG.error("No info found for requested supc {}", request != null ? request.getSupc() : null);
            responseBuilder.buildFailedResponse(ApiErrorCode.REQUESTED_DATA_DOES_NOT_EXISTS, response);
            return response;
        }

        response = responseBuilder.buildSuccesfulProductFulfilmentAttributeResponse(productAttributeUnit, response);
        LOG.debug("Sending Response {}", response);
        return response;
    }

    @Override
    public SetProductAttributeFulfilmentResponse setProductFulfilmentAttributes(SetProductFulfilmentAttributeRequest request) {
        LOG.info("Request received {}", request);
        SetProductAttributeFulfilmentResponse response = new SetProductAttributeFulfilmentResponse();
        response.setProtocol(request.getResponseProtocol());

        Map<String, ValidationError> failures = new HashMap<String, ValidationError>();

        int successCount = 0;
        for (Entry<String, ProductFulfilmentAttributeSRO> entry : request.getRequestMap().entrySet()) {
            ValidationError error = requestValidationService.requestBasicFieldValidation(entry.getValue());
            if (null == error) {
                error = validationForSetPA(entry.getValue());
            }

            if (error != null) {
                LOG.error("Validation error found  for {}, error : {} ", entry.getKey(), error.getMessage());
                failures.put(entry.getKey(), error);
            } else if (!ConfigUtils.getBooleanScalar(Property.UPDATE_PA_FROM_API)) {
                LOG.info("product attribute update flag is set off , hence not processing the request");
                ValidationError err = new ValidationError(ApiErrorCode.DATA_NOT_PERSISTED.code(), "product attribute update flag is set off in cocofs");
                failures.put(entry.getKey(), err);
            } else {
                ProductAttributeUploadDTO dto = converterService.getProductAttributeUploadDTO(entry.getValue());
                UserInfo i = new UserInfo();
                i.setEmail(StringUtils.isNotEmpty(request.getUserEmail()) ? request.getUserEmail() : "API");
                i.setSuper(false);
                try {
                    dataUpdater.updateDataWithDTO(dto, dataReader, dataEngine, i, true, true);
                    successCount++;
                    LOG.info("Successfully persisted for supc {} ", entry.getKey());
                } catch (OperationNotSupportedException e) {
                    LOG.error("Looks like we already have data for supc {} ", dto.getSupc());
                    failures.put(dto.getSupc(), new ValidationError(ApiErrorCode.DATA_ALREADY_EXISTS.code(), e.getMessage()));
                } catch (Exception e) {
                    LOG.error("Error while invoking generic persistence wrapper for supc {} ", dto.getSupc(), e);
                    failures.put(dto.getSupc(), new ValidationError(ApiErrorCode.INTERNAL_ERROR.code(), e.getMessage()));
                }
            }
        }

        response = responseBuilder.buildSetProductAttributeFulfilmentResponse(failures, successCount, response);
        LOG.info("Sending response {}", response);
        return response;
    }

    private ValidationError validationForSetPA(ProductFulfilmentAttributeSRO value) {

        if (StringUtils.isNotEmpty(value.getDangerousGoodsType())) {
            if (!ConfigUtils.getStringList(Property.DANGEROUS_GOODS_TYPE).contains(value.getDangerousGoodsType())) {
                return new ValidationError(ApiErrorCode.UNEXPECTED_DATA_ENCOUNTERED.code(), value.getDangerousGoodsType() + " found to be not in the configured list");
            }
        }

        if (StringUtils.isNotEmpty(value.getSerializedType())) {
            if (!ProductAttributeUtil.isValidSerializedType(value.getSerializedType())) {
                return new ValidationError(ApiErrorCode.UNEXPECTED_DATA_ENCOUNTERED.code(), value.getSerializedType() + " found to be not in the serializedType configured list");
            }
        }

        if (StringUtils.isNotEmpty(value.getPackagingType())) {
            if (!ProductAttributeUtil.isValidPackagingType(value.getPackagingType())) {
                return new ValidationError(ApiErrorCode.UNEXPECTED_DATA_ENCOUNTERED.code(), value.getPackagingType() + " found to be not in the packagingType configured list");
            }
        }

        if (value.getProductParts() != null) {
            if (value.getProductParts() > ConfigUtils.getIntegerScalar(Property.MAX_PRODUCT_PARTS)) {
                return new ValidationError(ApiErrorCode.DATA_OUT_OF_RANGE.code(), value.getProductParts() + " found to be greater than max allowed value for product part field");
            }
            if (value.getProductParts() == 0) {
                return new ValidationError(ApiErrorCode.DATA_OUT_OF_RANGE.code(), value.getProductParts() + " is not a valid value for product part field");
            }
        }

        return null;
    }

    @Override
    public GetProductDeliveryAndPackagingTypeResponse getProductDeliveryAndPackgingType(GetProductDeliveryAndPackagingTypeRequest request) {
        LOG.info("Request received {}", request);
        GetProductDeliveryAndPackagingTypeResponse response = new GetProductDeliveryAndPackagingTypeResponse();
        boolean atLeastOneSucces = false;
        int successCount = 0;
        for (String supc : request.getSupcList()) {
            PackagingTypeForSUPCResponse pResp = packagingTypeService.getPackagingTypeForSUPC(supc);
            DeliveryTypeForSUPCResponse dResp = deliveryTypeService.getDeliveryTypeForSUPC(supc);

            if (pResp.isError()) {
                response.addFailures(pResp.getValidationError(), supc);
            } else if (dResp.isError()) {
                response.addFailures(dResp.getValidationError(), supc);
            } else {
                response.getSupcToPackagingTypeMap().put(supc, pResp.getPackageType());
                response.getSupcToDeliveryTypeMap().put(supc, dResp.getDeliveryType());
                atLeastOneSucces = true;
                successCount = successCount + 1;
            }
        }
        response.setSuccessCount(successCount);
        response.setSuccessful(atLeastOneSucces);
        response.setProtocol(request.getResponseProtocol());
        LOG.info("Sending response {}", response);
        return response;
    }

    @Override
    public GetProductDeliveryTypeResponse getProductDeliveryType(GetProductDeliveryTypeRequest request) {
        LOG.info("Request received {}", request);
        GetProductDeliveryTypeResponse response = new GetProductDeliveryTypeResponse();
        response.setSuccessful(false);
        boolean atLeastOneSucces = false;
        int successCount = 0;
        for (String supc : request.getSupcList()) {
            DeliveryTypeForSUPCResponse resp = deliveryTypeService.getDeliveryTypeForSUPC(supc);
            if (resp.isError()) {
                response.addFailures(resp.getValidationError(), supc);
            } else {
                response.getSupcToDeliveryTypeMap().put(supc, resp.getDeliveryType());
                atLeastOneSucces = true;
                successCount = successCount + 1;
            }
        }
        response.setSuccessCount(successCount);
        response.setSuccessful(atLeastOneSucces);
        response.setProtocol(request.getResponseProtocol());
        return response;
    }

    @Override
    public GetProductPackagingTypeResponse getProductPackagingType(GetProductPackagingTypeRequest request) {
        LOG.info("Request received {}", request);
        GetProductPackagingTypeResponse response = new GetProductPackagingTypeResponse();
        boolean atLeastOneSucces = false;
        int successCount = 0;
        for (String supc : request.getSupcList()) {
            PackagingTypeForSUPCResponse resp = packagingTypeService.getPackagingTypeForSUPC(supc);
            if (resp.isError()) {
                response.addFailures(resp.getValidationError(), supc);
            } else {
                response.getSupcToPackagingTypeMap().put(supc, resp.getPackageType());
                atLeastOneSucces = true;
                successCount = successCount + 1;
            }
        }
        response.setSuccessCount(successCount);
        response.setSuccessful(atLeastOneSucces);
        response.setProtocol(request.getResponseProtocol());
        return response;
    }

    @Override
    public GetFulFilmentFeeParamResponse getFulfillmentFeeParams(GetFulfilmentFeeParamRequest request) {
        LOG.info("Request received {}", request);
        GetFulFilmentFeeParamResponse response = new GetFulFilmentFeeParamResponse();

        boolean atLeastOneSucces = false;
        int successCount = 0;
        for (SellerSUPCPair pair : request.getSellerProductPairs()) {
            ProductAttributeUnit pau = productAttributeService.getProductAttributeUnitBySUPC(pair.getSupc());
            if (pau == null) {
                response.addFailures(new ValidationError(ApiErrorCode.REQUESTED_DATA_DOES_NOT_EXISTS.code(), "SUPC Not Found"), pair);
                continue;
            }
            LOG.debug("Getting packaging  type for pair {} ", pair);
            PackagingTypeForSUPCResponse pResp = packagingTypeService.getPackagingType(pau, pair.getSupc());
            if (pResp.isError()) {
                LOG.debug("Packaging Type Could not be evaluated for pair " + pair);
                response.addFailures(pResp.getValidationError(), pair);
                continue;
            }
            LOG.debug("Getting delivery  type for pair {} ", pair);
            DeliveryTypeForSUPCResponse dResp = deliveryTypeService.getDeliveryTypeForSUPC(pair.getSupc());
            if (dResp.isError()) {
                LOG.debug("Delivery Type Could not be evaluated for pair {}", pair);
                response.addFailures(dResp.getValidationError(), pair);
                continue;
            }
            LOG.debug("Getting fulfillment model for pair {}", pair);
            FulfillmentModel fulfilmentModel = null;
            try {
                fulfilmentModel = fulfilmentModelService.getFulfilmentModel(pair.getSellerCode(), pair.getSupc());
            } catch (ExternalDataNotFoundException e) {
                LOG.error("Exception in getFulfillmentFeeParams", e);
            }
            if (fulfilmentModel == null) {
                response.addFailures(new ValidationError(ApiErrorCode.REQUESTED_DATA_DOES_NOT_EXISTS.code(), "Fulfillment model can not be determined"), pair);
            } else {

                double perimeter = UtilHelper.getPerimterFromLBH(pau.getLength(), pau.getBreadth(), pau.getHeight());

                FulfilmentFeeParamSRO value = new FulfilmentFeeParamSRO(fulfilmentModel, dResp.getDeliveryType(), pResp.getPackageType(), pau.getWeight(),
                        UtilHelper.getVolumetricWeightFromLBH(pau.getLength(), pau.getBreadth(), pau.getHeight()), perimeter);
                response.getSellerSupcFulfilmentFeeParams().put(pair, value);
                atLeastOneSucces = true;
                successCount = successCount + 1;
            }
        }
        response.setSuccessCount(successCount);
        response.setSuccessful(atLeastOneSucces);
        response.setProtocol(request.getResponseProtocol());
        LOG.info("Sending response {}", response);
        return response;
    }

    @Override
    public GetProductWeightNotificationResponse getProductWeightNotifications(GetProductWeightNotificationRequest request) {
        LOG.info("Request received for getProductWeightNotifications {}", request);
        GetProductWeightNotificationResponse response = new GetProductWeightNotificationResponse();
        response.setProtocol(request.getResponseProtocol());

        ValidationError error = requestValidationService.requestBasicFieldValidation(request);
        if (error != null) {
            LOG.error("Validation error found request for seller {}", request != null ? request.getSellerCode() : null);
            responseBuilder.buildFailedResponse(error, response);
            return response;
        }

        Long total = panMongoDataReadService.getTotalLiveProductAttributeNotificationCount(request.getSellerCode());
        int maxPageAllowed = (int) Math.ceil((float) total / (float) request.getPageSize());
        if (request.getPageNum() > maxPageAllowed) {
            LOG.error("Max page allowed is {}, requested page is {}", maxPageAllowed, request.getPageNum());
            responseBuilder.buildFailedResponse(ApiErrorCode.REQUESTED_DATA_DOES_NOT_EXISTS, response);
            return response;
        }
        List<ProductAttributeNotificationUnit> panus = panMongoDataReadService.getLivePaginatedAndSortedProductAttributeNotificationUnit(request.getSellerCode(),
                request.getPageNum(), request.getPageSize());
        response = responseBuilder.buildProductWeightNotificationResponse(panus, request.getSellerCode(), total);
        response.setSuccessful(true);
        response.setProtocol(request.getRequestProtocol());
        return response;
    }

    @Override
    public GetFulfilmentModelAndWeightResponse getFulfilmentModelAndWeight(GetFulfilmentModelAndWeightRequest request) {
        LOG.debug("Request received {}", request);
        GetFulfilmentModelAndWeightResponse response = new GetFulfilmentModelAndWeightResponse();
        response.setProtocol(request.getResponseProtocol());
        ValidationError error = requestValidationService.requestBasicFieldValidation(request);
        if (error != null) {
            LOG.error("Validation error found request for supc {}", request != null ? request.getSupc() : null);
            responseBuilder.buildFailedResponse(error, response);
            return response;
        }
        ProductAttributeUnit productAttributeUnit = productAttributeService.getProductAttributeUnitBySUPC(request.getSupc());
        FulfillmentModel model = null;
        try {
            model = fulfilmentModelService.getFulfilmentModel(request.getSellerCode(), request.getSupc());
        } catch (ExternalDataNotFoundException e) {
            LOG.error("Exception in getFulfilmentModelAndWeight ", e);
        }
        if (productAttributeUnit == null || model == null) {
            LOG.error("No info found for requested supc {}", request != null ? request.getSupc() : null);
            responseBuilder.buildFailedResponse(ApiErrorCode.REQUESTED_DATA_DOES_NOT_EXISTS, response);
            return response;
        } else {
            response.setFulfilmentModel(model);
            response.setWeight(productAttributeUnit.getWeight());
            return response;
        }
    }

    @Override
    public GetFulfilmentModelAndProductAttributesResponse getFulfilmentModelAndProductAttributes(GetFulfilmentModelAndProductAttributesRequest request) {
        LOG.info("Request received {}", request);
        GetFulfilmentModelAndProductAttributesResponse response = new GetFulfilmentModelAndProductAttributesResponse();
        response.setProtocol(request.getResponseProtocol());
        ValidationError error = requestValidationService.requestBasicFieldValidation(request);
        if (error != null) {
            LOG.error("Validation error found request for supc {}", request != null ? request.getSupc() : null);
            responseBuilder.buildFailedResponse(error, response);
            return response;
        }
        ProductAttributeUnit productAttributeUnit = productAttributeService.getProductAttributeUnitBySUPC(request.getSupc());
        FulfillmentModel model = null;
        try {
            model = fulfilmentModelService.getFulfilmentModel(request.getSellerCode(), request.getSupc());
        } catch (ExternalDataNotFoundException e) {
            LOG.error("Exception in getFulfilmentModelAndWeight ", e);
        }
        if (productAttributeUnit == null || model == null) {
            LOG.error("No info found for requested supc {}", request != null ? request.getSupc() : null);
            responseBuilder.buildFailedResponse(ApiErrorCode.REQUESTED_DATA_DOES_NOT_EXISTS, response);
            LOG.info("Sending response {}", response);
            return response;
        } else {
            response.setFulfilmentModel(model);
            response.setProductAttributeSro(converterService.getProductAttributeSro(productAttributeUnit));
            LOG.info("Sending response {}", response);
            return response;
        }
    }

    @Override
    public RemoveSupcModeMappingResponse removeSupcModeMapping(RemoveSupcModeMappingRequest request) {
        LOG.info("Request received {}", request);
        RemoveSupcModeMappingResponse response = new RemoveSupcModeMappingResponse();

        response.setSuccessful(true);
        response.setProtocol(request.getResponseProtocol());
        for (SupcShippingModeSRO x : request.getSupcShippingModes()) {
            try {
                boolean success = shippingModeService.removeSupcModeMapping(x);
                response.getSupcResult().put(x.getSupc(), success);

            } catch (RuntimeException e) {
                response.getSupcResult().put(x.getSupc(), false);
            }
        }
        LOG.info("Sending response {}", response);
        return response;
    }

    @Override
    public RemoveCategoryModeMappingResponse removeCategoryModeMapping(RemoveCategoryModeMappingRequest request) {
        LOG.info("Request received {}", request);
        RemoveCategoryModeMappingResponse response = new RemoveCategoryModeMappingResponse();

        response.setSuccessful(true);
        response.setProtocol(request.getResponseProtocol());
        for (CategoryShippingModeSRO x : request.getCategoryShippingModes()) {

            try {
                boolean success = shippingModeService.removeCategoryModeMapping(x);
                response.getCategoryResult().put(x.getCategoryUrl(), success);

            } catch (RuntimeException e) {
                response.getCategoryResult().put(x.getCategoryUrl(), false);
            }

        }
        LOG.info("Sending response {}", response);
        return response;
    }

    @Override
    public AddOrUpdateSupcModeMappingResponse addOrUpdateSupcModeMapping(AddOrUpdateSupcModeMappingRequest request) {
        LOG.info("Request received {}", request);
        AddOrUpdateSupcModeMappingResponse response = new AddOrUpdateSupcModeMappingResponse();

        response.setSuccessful(true);
        response.setProtocol(request.getResponseProtocol());
        for (SupcShippingModeSRO x : request.getSupcShippingModes()) {

            try {
                boolean success = shippingModeService.addOrUpdateSupcModeMapping(x);
                response.getSupcResult().put(x.getSupc(), success);

            } catch (RuntimeException e) {
                response.getSupcResult().put(x.getSupc(), false);
            }
        }
        LOG.info("Sending response {}", response);
        return response;
    }

    @Override
    public AddOrUpdateCategoryModeMappingResponse addOrUpdateCategoryModeMapping(AddOrUpdateCategoryModeMappingRequest request) {
        LOG.info("Request received {}", request);
        AddOrUpdateCategoryModeMappingResponse response = new AddOrUpdateCategoryModeMappingResponse();

        response.setSuccessful(true);
        response.setProtocol(request.getResponseProtocol());
        for (CategoryShippingModeSRO x : request.getCategoryShippingModes()) {
            try {
                boolean success = shippingModeService.addOrUpdateCategoryModeMapping(x);
                response.getCategoryResult().put(x.getCategoryUrl(), success);

            } catch (RuntimeException e) {
                response.getCategoryResult().put(x.getCategoryUrl(), false);
            }
        }
        LOG.info("Sending response {}", response);
        return response;
    }

    @Override
    public GetSupcModeMappingResponse getSupcModeMapping(GetSupcModeMappingRequest request) {
        LOG.info("Request received {}", request);
        GetSupcModeMappingResponse response = new GetSupcModeMappingResponse();
        response.setSuccessful(true);
        List<String> supcs = request.getSupcList();
        Map<String, SupcShippingModeSRO> supcModes = shippingModeService.getSupcModeMapping(supcs);
        response.setSupcModes(supcModes);
        response.setProtocol(request.getResponseProtocol());
        LOG.info("Sending response {}", response);
        return response;
    }

    @Override
    public GetCategoryModeMappingResponse getCategoryModeMapping(GetCategoryModeMappingRequest request) {
        LOG.info("Request received {}", request);
        GetCategoryModeMappingResponse response = new GetCategoryModeMappingResponse();
        response.setSuccessful(true);
        List<String> categories = request.getCategoryList();
        Map<String, CategoryShippingModeSRO> categoryModes = shippingModeService.getCategoryModeMapping(categories);
        response.setCategoryModes(categoryModes);
        response.setProtocol(request.getResponseProtocol());
        LOG.info("Sending response {}", response);
        return response;
    }

    @Override
    public GetAllSupcModeMappingResponse getAllSupcModeMapping(GetAllSupcModeMappingRequest request) {
        LOG.info("Request received {}", request);
        GetAllSupcModeMappingResponse response = new GetAllSupcModeMappingResponse();
        response.setSuccessful(true);

        List<SupcShippingModeSRO> supcModes = shippingModeService.getAllSupcModeMapping();
        response.setSupcModes(supcModes);
        response.setProtocol(request.getResponseProtocol());
        LOG.info("Sending response {}", response);
        return response;
    }

    @Override
    public GetAllCategoryModeMappingResponse getAllCategoryModeMapping(GetAllCategoryModeMappingRequest request) {
        LOG.info("Request received {}", request);
        GetAllCategoryModeMappingResponse response = new GetAllCategoryModeMappingResponse();
        response.setSuccessful(true);

        List<CategoryShippingModeSRO> categoryModes = shippingModeService.getAllCategoryModeMapping();
        response.setCategoryModes(categoryModes);
        response.setProtocol(request.getResponseProtocol());
        LOG.info("Sending response {}", response);
        return response;
    }

    @Override
    public SetProductCapturedWeightResponse setProductCapturedWeight(SetProductCapturedWeightRequest request) {
        LOG.info("Request received {}", request);
        SetProductCapturedWeightResponse response = new SetProductCapturedWeightResponse();
        response.setProtocol(request.getResponseProtocol());
        Map<String, ValidationError> failures = new HashMap<String, ValidationError>();
        ValidationError error = requestValidationService.requestBasicFieldValidation(request);
        if (error != null) {
            LOG.error("Validation error found request for supc {}", request != null ? request.getSupc() : null);
            responseBuilder.buildFailedResponse(error, response);
            return response;
        } else if (!ConfigUtils.getBooleanScalar(Property.UPDATE_PA_FROM_API)) {
            LOG.info("product attribute update flag is set off , hence not processing the request");
            ValidationError err = new ValidationError(ApiErrorCode.DATA_NOT_PERSISTED.code(), "product attribute update flag is set off in cocofs");
            responseBuilder.buildFailedResponse(err, response);
            return response;
        }
        //setting the systemWeightCaptured flag to true while creating dto
        ProductAttributeUploadDTO dto = converterService.getProductAttributeUploadDTO(request.getSupc(), request.getWeight(), true);
        UserInfo i = new UserInfo();
        i.setEmail(request.getEmail());
        i.setSuper(request.isSuperUser() != null ? request.isSuperUser() : false);
        try {
            dataUpdater.updateDataWithDTO(dto, dataReader, uploadDataEngine, i);
            LOG.info("Weight Successfully captured for supc {} ", request.getSupc());
            return responseBuilder.buildSetProductCapturedWeightResponse(null, 1, response);
        } catch (Exception e) {
            LOG.error("Error while invoking generic persistence wrapper for supc {} ", dto.getSupc(), e);
            failures.put(dto.getSupc(), new ValidationError(ApiErrorCode.INTERNAL_ERROR.code(), e.getMessage()));
            return responseBuilder.buildSetProductCapturedWeightResponse(failures, 0, response);
        }
    }

    @Override
    public GetProductSystemWeightCapturedFlagResponse getProductSystemWeightCapturedFlag(GetProductSystemWeightCapturedFlagRequest request) {
        LOG.info("Request received {}", request);
        GetProductSystemWeightCapturedFlagResponse response = new GetProductSystemWeightCapturedFlagResponse();
        response.setProtocol(request.getResponseProtocol());

        ValidationError error = requestValidationService.requestBasicFieldValidation(request);
        if (error != null) {
            LOG.error("Validation error found request for supc {}", request != null ? request.getSupc() : null);
            responseBuilder.buildFailedResponse(error, response);
            return response;
        }
        String supc = null;
        String systemWeightCaptured = null;
        List<PendingProductAttributeUpdate> ppauList = pendingPADBDataReadService.getProductSystemWeightCapturedFlag(request.getSupc());
        if (ppauList == null || ppauList.isEmpty()) {
            ProductAttributeUnit productAttributeUnit = productAttributeService.getProductAttributeUnitBySUPC(request.getSupc());
            if (productAttributeUnit == null) {
                LOG.error("No info found for requested supc {}", request != null ? request.getSupc() : null);
                responseBuilder.buildFailedResponse(ApiErrorCode.REQUESTED_DATA_DOES_NOT_EXISTS, response);
                return response;
            }
            supc = productAttributeUnit.getSupc();
            systemWeightCaptured = String.valueOf(productAttributeUnit.isSystemWeightCaptured());
        } else {
            supc = ppauList.get(0).getSupc();
            systemWeightCaptured = ppauList.get(0).getNewValue();
        }

        return responseBuilder.buildGetProductSystemWeightCapturedResponse(supc, systemWeightCaptured, response);
    }

    @Override
    public com.snapdeal.cocofs.model.response.v2.GetFulFilmentFeeParamResponse getFulfillmentFeeParams(com.snapdeal.cocofs.model.request.v2.GetFulfilmentFeeParamRequest request) {
        LOG.info("Request received {}", request);
        com.snapdeal.cocofs.model.response.v2.GetFulFilmentFeeParamResponse response = new com.snapdeal.cocofs.model.response.v2.GetFulFilmentFeeParamResponse();
        response.setProtocol(request.getResponseProtocol());

        ValidationError error = requestValidationService.requestBasicFieldValidation(request);
        if (error != null) {
            LOG.error("Validation error found in request {}", request);
            responseBuilder.buildFailedResponse(error, response);
            return response;
        }
        Map<String, Map<String, SellerSupcSubcatSro>> supcToSellerToSroMap = getSupcToSellerToSroMap(request.getSellerProductPairs());
        boolean atLeastOneSucces = false;
        int successCount = 0;
        for (Entry<String, Map<String, SellerSupcSubcatSro>> supcSellerEntrySet : supcToSellerToSroMap.entrySet()) {
            String supc = supcSellerEntrySet.getKey();
            Collection<SellerSupcSubcatSro> sroCollection = supcSellerEntrySet.getValue().values();
            String categoryUrl = supcSellerEntrySet.getValue().values().iterator().next().getCategoryUrl();
            ProductAttributeUnit pau = productAttributeService.getProductAttributeUnitBySUPC(supc);
            if (pau == null) {
                response.addFailures(new ValidationError(ApiErrorCode.REQUESTED_DATA_DOES_NOT_EXISTS.code(), "SUPC Not Found"), sroCollection);
                continue;
            }
            LOG.debug("Getting packaging  type for supc {} ", supc);
            PackagingTypeForSUPCResponse pResp = packagingTypeService.getPackagingType(pau, supc, categoryUrl);
            if (pResp.isError()) {
                LOG.debug("Packaging Type Could not be evaluated for supc {} ", supc);
                response.addFailures(pResp.getValidationError(), sroCollection);
                continue;
            }
            LOG.debug("Getting delivery  type for supc {} ", supc);
            DeliveryTypeForSUPCResponse dResp = deliveryTypeService.getDeliveryTypeForSUPC(supc, categoryUrl);
            if (dResp.isError()) {
                LOG.debug("Delivery Type Could not be evaluated for supc {}", supc);
                response.addFailures(dResp.getValidationError(), sroCollection);
                continue;
            }
            for (SellerSupcSubcatSro sro : supcSellerEntrySet.getValue().values()) {
                LOG.debug("Getting fulfillment model for pair {}", sro);
                FulfillmentModel fulfilmentModel = null;
                try {
                    fulfilmentModel = fulfilmentModelService.getFulfilmentModel(sro.getSellerCode(), sro.getSupc(), sro.getCategoryUrl());
                } catch (ExternalDataNotFoundException e) {
                    LOG.error("Exception in getFulfillmentFeeParams", e);
                }
                if (fulfilmentModel == null) {
                    response.addFailures(new ValidationError(ApiErrorCode.REQUESTED_DATA_DOES_NOT_EXISTS.code(), "Fulfillment model can not be determined"), sro);
                } else {
                    double perimeter = UtilHelper.getPerimterFromLBH(pau.getLength(), pau.getBreadth(), pau.getHeight());

                    FulfilmentFeeParamSRO value = new FulfilmentFeeParamSRO(fulfilmentModel, dResp.getDeliveryType(), pResp.getPackageType(), pau.getWeight(),
                            UtilHelper.getVolumetricWeightFromLBH(pau.getLength(), pau.getBreadth(), pau.getHeight()), perimeter);
                    response.getSellerSupcFulfilmentFeeParams().put(sro, value);
                    atLeastOneSucces = true;
                    successCount = successCount + 1;
                }

            }

        }
        response.setSuccessCount(successCount);
        response.setSuccessful(atLeastOneSucces);
        response.setProtocol(request.getResponseProtocol());
        LOG.info("Sending Response {}", response);

        return response;
    }

    private Map<String, Map<String, SellerSupcSubcatSro>> getSupcToSellerToSroMap(Set<SellerSupcSubcatSro> sroList) {
        Map<String, Map<String, SellerSupcSubcatSro>> supcToSellerToSroMap = new HashMap<String, Map<String, SellerSupcSubcatSro>>();
        for (SellerSupcSubcatSro sro : sroList) {
            if (null == supcToSellerToSroMap.get(sro.getSupc())) {
                supcToSellerToSroMap.put(sro.getSupc(), new HashMap<String, SellerSupcSubcatSro>());
            }
            supcToSellerToSroMap.get(sro.getSupc()).put(sro.getSellerCode(), sro);

        }
        return supcToSellerToSroMap;

    }

    @Override
    public GetAllDangerousGoodsTypeResponse getAllDangerousGoodsType() {
        return new GetAllDangerousGoodsTypeResponse(ConfigUtils.getStringList(Property.DANGEROUS_GOODS_TYPE));
    }

    @Override
    public AllCenterTypesResponse getAllCenterTypes(GetAllCenterTypesRequest req) {
        LOG.info("Request received {}", req);
        AllCenterTypesResponse response = new AllCenterTypesResponse();
        response.setCenterTypes(getAllKnownCenterTypes());
        response.setSuccessful(true);
        response.setProtocol(req.getResponseProtocol());
        LOG.info("Sending Response {}", response);

        return response;
    }

    private List<String> getAllKnownCenterTypes() {
        List<String> listOfCenters = new ArrayList<String>();
        Property p = Property.COCOFS_CENTER_TYPES;
        String centerTypes = ConfigUtils.getStringScalar(p);
        if (StringUtils.isNotEmpty(centerTypes)) {
            String[] centers = centerTypes.split(",");
            for (String center : centers) {
                listOfCenters.add(center.trim());
            }
        }
        return listOfCenters;
    }

    @SuppressWarnings("deprecation")
    @Override
    public AllFCDetailResponse getAllFCDetails(GetAllFCDetailRequest req) {
        LOG.info("Request received {}", req);
        boolean bSuccessful = false;
        AllFCDetailResponse response = new AllFCDetailResponse();

        if (!validateCenterType(req.getType())) {
            String msg = "Unexpected data encountered for center type: " + req.getType() + ". Known center types:" + getAllKnownCenterTypes();
            LOG.info(msg);
            response.setSuccessful(bSuccessful);
            response.setMessage(msg);
            ValidationError ve = new ValidationError(ApiErrorCode.UNEXPECTED_DATA_ENCOUNTERED.code(), "Unknown center type: " + req.getType());
            List<ValidationError> veList = new ArrayList<ValidationError>();
            veList.add(ve);
            response.setValidationErrors(veList);
        } else {
            bSuccessful = true;
            response.setFcDetailList(getAllFCDetailsForCenterType(req.getType()));
        }
        response.setSuccessful(bSuccessful);
        response.setProtocol(req.getResponseProtocol());
        LOG.info("Returned fcDetails of size {}", (response.getFcDetailList() != null) ? response.getFcDetailList().size() : 0);
        LOG.debug("Sending Response {}", response);
        return response;
    }

    private List<FCDetailSRO> getAllFCDetailsForCenterType(String centerType) {
        List<FulfillmentCentre> allFCs = null;
        if (isAllCentersRequired(centerType)) {
            allFCs = fulfillmentCenterService.getAllFCs();
        } else {
            allFCs = fulfillmentCenterService.getAllFCsByType(centerType);
        }

        List<FCDetailSRO> fcDetailList = new ArrayList<FCDetailSRO>();
        if (allFCs != null) {
            for (FulfillmentCentre center : allFCs) {
                FCDetailSRO detail = fcConvertorService.getFCDetailSRO(center);
                fcDetailList.add(detail);
            }
        }
        return fcDetailList;
    }

    private boolean validateCenterType(String type) {

        if (isAllCentersRequired(type)) {
            return true;
        }

        if (getAllKnownCenterTypes().contains(type)) {
            return true;
        }

        return false;
    }

    private boolean isAllCentersRequired(String type) {

        if (StringUtils.isEmpty(type)) {
            return true;
        }

        if ("ALL".equals(type.trim())) {
            return true;
        }
        return false;
    }

    @Override
    public GetVolumetricWeightFormulaParamsResponse getVolumetricWeightFormulaParams(GetVolumetricWeightFormulaParamsRequest request) {
        double denominator = ConfigUtils.getDoubleScalar(Property.VOLUMETRIC_WEIGHT_FORMULA_DENOMINATOR);
        GetVolumetricWeightFormulaParamsResponse response = new GetVolumetricWeightFormulaParamsResponse(denominator);
        response.setProtocol(request.getResponseProtocol());
        return response;
    }

    @Override
    public GetMultiplierForPerimeterResponse getMultiplierForPerimeter(GetMultiplierForPerimeterRequest request) {
        double multiplier = ConfigUtils.getDoubleScalar(Property.PERIMETER_MULTIPLIER_VALUE);
        GetMultiplierForPerimeterResponse response = new GetMultiplierForPerimeterResponse(multiplier);
        response.setProtocol(request.getResponseProtocol());
        return response;
    }

}
