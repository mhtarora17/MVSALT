/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 30-Sep-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.web.landing.layer.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.naming.OperationNotSupportedException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.snapdeal.base.utils.CollectionUtils;
import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.base.validation.ValidationError;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.core.aerospike.vo.SellerFMMappingVO;
import com.snapdeal.cocofs.entity.SellerContactDetails;
import com.snapdeal.cocofs.entity.SellerFMMapping;
import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.generic.persister.exception.GenericPersisterException;
import com.snapdeal.cocofs.generic.persister.response.GenericPersisterWithAerospikeResponse;
import com.snapdeal.cocofs.model.request.GetAllSellersForFMRequest;
import com.snapdeal.cocofs.model.request.GetFMMappingAtSellerAndSellerSubcatRequest;
import com.snapdeal.cocofs.model.request.GetFulfilmentModelBySellerSupcSubcatRequest;
import com.snapdeal.cocofs.model.request.GetFulfilmentModelRequest;
import com.snapdeal.cocofs.model.request.GetFulfilmentModelsForSellerRequest;
import com.snapdeal.cocofs.model.request.GetSDFulfilledStatusBySupcSellersSubcatRequest;
import com.snapdeal.cocofs.model.request.GetSDFulfilledStatusBySupcSellersSubcatResponse;
import com.snapdeal.cocofs.model.request.GetSellerSubcatFMMappingRequest;
import com.snapdeal.cocofs.model.request.SetFMMappingRequest;
import com.snapdeal.cocofs.model.request.SetSellerDetailsRequest;
import com.snapdeal.cocofs.model.request.SetSellerFMMappingRequest;
import com.snapdeal.cocofs.model.request.SetSellerSubcatFMMappingRequest;
import com.snapdeal.cocofs.model.request.ValidateSellerCodeRequest;
import com.snapdeal.cocofs.model.response.ApiErrorCode;
import com.snapdeal.cocofs.model.response.GetAllSellersForFMResponse;
import com.snapdeal.cocofs.model.response.GetFMMappingAtSellerAndSellerSubcatResponse;
import com.snapdeal.cocofs.model.response.GetFulfilmentModelBySellerSupcSubcatResponse;
import com.snapdeal.cocofs.model.response.GetFulfilmentModelResponse;
import com.snapdeal.cocofs.model.response.GetFulfilmentModelsForSellerResponse;
import com.snapdeal.cocofs.model.response.GetSellerSubcatFMMappingResponse;
import com.snapdeal.cocofs.model.response.SetFMMappingResponse;
import com.snapdeal.cocofs.model.response.SetSellerDetailsResponse;
import com.snapdeal.cocofs.model.response.SetSellerFMMappingResponse;
import com.snapdeal.cocofs.model.response.SetSellerSubcatFMMappingResponse;
import com.snapdeal.cocofs.model.response.ValidateSellerCodeResponse;
import com.snapdeal.cocofs.services.IDataReaderWithAerospike;
import com.snapdeal.cocofs.services.IDataUpdater;
import com.snapdeal.cocofs.services.IDataUpdaterWithAerospike;
import com.snapdeal.cocofs.services.ISellerDetailsService;
import com.snapdeal.cocofs.services.common.dto.UserInfo;
import com.snapdeal.cocofs.services.data.engine.IDataEngineWithAerospike;
import com.snapdeal.cocofs.services.events.dto.SellerFMFCUpdateEventObj;
import com.snapdeal.cocofs.services.fm.IFulfilmentModelService;
import com.snapdeal.cocofs.services.fm.ISellerSUPCFMMappingProcessor;
import com.snapdeal.cocofs.services.fm.dto.SellerFMMappingUpdateDto;
import com.snapdeal.cocofs.sro.FulfillmentModel;
import com.snapdeal.cocofs.sro.SellerFMMappingUpdateFailedSro;
import com.snapdeal.cocofs.sro.SellerFulfilmentModelMappingSRO;
import com.snapdeal.cocofs.sro.SellerSUPCPair;
import com.snapdeal.cocofs.sro.SellerSubcatFulfilmentModelMappingSRO;
import com.snapdeal.cocofs.web.builder.IResponseBuilder;
import com.snapdeal.cocofs.web.landing.layer.ICocofsFMLandingService;
import com.snapdeal.cocofs.web.landing.layer.IRequestValidationService;

@Service("CocofsFMLandingServiceImpl")
public class CocofsFMLandingServiceImpl implements ICocofsFMLandingService {

    @Autowired
    private IResponseBuilder                                                                       responseBuilder;

    @Autowired
    private IRequestValidationService                                                              requestValidationService;

    @Autowired
    private IDataUpdater                                                                           dataUpdater;

    @Autowired
    private IDataUpdaterWithAerospike                                                              aerospikedataUpdater;

    @Autowired
    @Qualifier("SellerFMMappingDataEngine")
    private IDataEngineWithAerospike<SellerFMMappingUpdateDto, SellerFMMapping, SellerFMMappingVO> sellerFMMappingDataEngine;

    @Autowired
    @Qualifier("SellerFMMappingDataReader")
    private IDataReaderWithAerospike<SellerFMMappingUpdateDto, SellerFMMapping, SellerFMMappingVO> sellerFMMappingDataReader;

    @Autowired
    private IFulfilmentModelService                                                                fulfilmentModelService;

    @Autowired
    @Qualifier("failSafeSellerSUPCFMNotificationSendor")
    private ISellerSUPCFMMappingProcessor                                                          failSafeSellerSUPCFMMappingProcessor;

    @Autowired
    @Qualifier("converterServiceImpl")
    private com.snapdeal.cocofs.services.converter.IConverterService                               converterService;
    
    @Autowired
    private ISellerDetailsService															sellerDetailsService;
    

    private static final Logger                                                                    LOG = LoggerFactory.getLogger(CocofsFMLandingServiceImpl.class);

    //to stub out dependencies during unit testing
    private IDependency                                                                            dep = new Dependency();

    @Override
    public GetFulfilmentModelResponse getFulfilmentModel(GetFulfilmentModelRequest request) {
        LOG.debug("Request received {}", request);
        GetFulfilmentModelResponse response = new GetFulfilmentModelResponse();
        response.setProtocol(request.getResponseProtocol());
        ValidationError error = requestValidationService.requestBasicFieldValidation(request);
        if (error != null) {
            LOG.error("Validation error found request {}", request);
            responseBuilder.buildFailedResponse(error, response);
        } else {
            Map<SellerSUPCPair, FulfillmentModel> sellerSupcToFM = new HashMap<SellerSUPCPair, FulfillmentModel>();
            FulfillmentModel fulfilmentModel = null;
            int successCount = 0;
            for (SellerSUPCPair pair : request.getSellerSupcPairs()) {
                try {
                    fulfilmentModel = null;
                    fulfilmentModel = fulfilmentModelService.getFulfilmentModel(pair.getSellerCode(), pair.getSupc());
                } catch (ExternalDataNotFoundException e) {
                    LOG.error("Exeption in getFulfilmentModel ", e);
                }
                if (null == fulfilmentModel) {
                    response.addFailures(new ValidationError(ApiErrorCode.REQUESTED_DATA_DOES_NOT_EXISTS.code(), "Fulfillment model can not be determined"), pair);
                } else {
                    sellerSupcToFM.put(pair, fulfilmentModel);
                    successCount++;
                }

            }

            if (successCount > 0) {
                response.setSellerSupcToFulfilmentModel(sellerSupcToFM);
                response.setSuccessCount(successCount);
            } else {
                responseBuilder.buildFailedResponse(ApiErrorCode.REQUESTED_DATA_DOES_NOT_EXISTS, response);
            }
        }

        LOG.debug("Sending response {}", response);

        return response;
    }

    @Override
    public GetFulfilmentModelBySellerSupcSubcatResponse getFulfilmentModelBySellerSupcSubcat(GetFulfilmentModelBySellerSupcSubcatRequest request) {
        LOG.debug("Request received {}", request);
        GetFulfilmentModelBySellerSupcSubcatResponse response = new GetFulfilmentModelBySellerSupcSubcatResponse();
        response.setProtocol(request.getResponseProtocol());
        ValidationError error = requestValidationService.requestBasicFieldValidation(request);
        if (error != null) {
            LOG.error("Validation error found request {}", request);
            responseBuilder.buildFailedResponse(error, response);
        } else if (request.getSellerCodes().size() > ConfigUtils.getIntegerScalar(Property.FM_API_SELLER_BATCH_SIZE)) {
            LOG.error("sellers size is more than the allowed batch size for request {}", request);
            responseBuilder.buildFailedResponse(ApiErrorCode.REQUEST_BATCH_SIZE_LIMIT_EXCEEDED, response);
        } else {
            Map<String, FulfillmentModel> sellerToFM = null;
            boolean applyFMLogic = ConfigUtils.getBooleanScalar(Property.APPLY_FM_LOGIC_FOR_WEB_API);

            if (applyFMLogic) {

                sellerToFM = fulfilmentModelService.getFulfilmentModel(request.getSellerCodes(), request.getSupc(), request.getSubcat());

            } else {
                sellerToFM = setDefaultFMModel(request.getSellerCodes());

            }

            if (null != sellerToFM && !sellerToFM.isEmpty()) {
                response.setSellerToFulfilmentModel(sellerToFM);
            } else {
                responseBuilder.buildFailedResponse(ApiErrorCode.REQUESTED_DATA_DOES_NOT_EXISTS, response);
            }
        }
        LOG.debug("Sending response {}", response);
        return response;
    }

    private Map<String, FulfillmentModel> setDefaultFMModel(List<String> sellerCodes) {
        Map<String, FulfillmentModel> sellerToFM = new HashMap<String, FulfillmentModel>();
        FulfillmentModel model = FulfillmentModel.getFulfilmentModelByCode(ConfigUtils.getStringScalar(Property.DEFAULT_FM_FOR_WEB_API));
        if (model == null) {
            model = FulfillmentModel.DROPSHIP;
        }
        for (String sellerCode : sellerCodes) {
            sellerToFM.put(sellerCode, model);
        }
        LOG.warn("apply.fm.logic.for.web.api property is set false hence returning default FulfilmentModel {} ", model.getCode());
        return sellerToFM;
    }

    @Override
    public GetFMMappingAtSellerAndSellerSubcatResponse getFMMappingAtSellerAndSellerSubcat(GetFMMappingAtSellerAndSellerSubcatRequest request) {
        LOG.info("Request received {}", request);
        GetFMMappingAtSellerAndSellerSubcatResponse response = new GetFMMappingAtSellerAndSellerSubcatResponse();
        response.setProtocol(request.getResponseProtocol());
        ValidationError error = requestValidationService.requestBasicFieldValidation(request);
        if (error != null) {
            LOG.error("Validation error found request {}", request);
            responseBuilder.buildFailedResponse(error, response);
        } else {
            String fulfilmentModel = null;
            // getting FM Mapping At Seller Level
            try {
                fulfilmentModel = fulfilmentModelService.getFulfilmentModelBySeller(request.getSellerCode(), true);
            } catch (ExternalDataNotFoundException e) {
                LOG.error("ExternalDataNotFoundException ", e);
            }
            if (StringUtils.isNotEmpty(fulfilmentModel)) {
                response.setSellerFMMappingSRO(new SellerFulfilmentModelMappingSRO(request.getSellerCode(), FulfillmentModel.getFulfilmentModelByCode(fulfilmentModel)));
            }
            //Getting FM Mapping at Seller Subcat level for a paricular seller
            boolean subactLevelFMEnabled = ConfigUtils.getBooleanScalar(Property.SUBCAT_LEVEL_FM_ENABLED);
            if (!subactLevelFMEnabled) {
                LOG.info("subcat level fm is disabled ,hence returning empty fm list for subcats for seller {} ", request.getSellerCode());
                response.setSellerSubcatFMMappingSroList(new ArrayList<SellerSubcatFulfilmentModelMappingSRO>());
            } else {
                response.setSellerSubcatFMMappingSroList(fulfilmentModelService.getSellerSubcatFMMappingSRO(request.getSellerCode()));
            }
        }
        if (LOG.isDebugEnabled()) {
            LOG.debug("Response returned from getFMMappingAtSellerAndSellerSubcat {}", response);
        }
        return response;
    }

    @Override
    public SetFMMappingResponse setFMMapping(SetFMMappingRequest request) {
        LOG.info("Request received {}", request);
        SetFMMappingResponse response = new SetFMMappingResponse();
        response.setProtocol(request.getResponseProtocol());
        ValidationError error = requestValidationService.requestBasicFieldValidation(request);
        if (null == error) {
            error = basicValidationForSetFMMapping(request);
        }
        boolean atLeastOneSuccess = false;
        if (error != null) {
            LOG.error("Validation error found request {}", request);
            responseBuilder.buildFailedResponse(error, response);
        } else if (!ConfigUtils.getBooleanScalar(Property.UPDATE_FM_FROM_API)) {
            LOG.info("fulfillment model update flag is set off , hence not processing the request");
            response.setSuccessful(false);
            response.setMessage("update mode is set off for fulfillment model data");
            return response;
        }

        else {
            UserInfo info = new UserInfo(request.getUserEmail(), false);
            SellerContactDetails existingSCD = sellerDetailsService.getSellerContactDetailsBySeller(request.getSellerCode());
            String oldPincode = existingSCD != null ? existingSCD.getPinCode() : null;
            SellerFMMappingUpdateFailedSro sro = updateSellerFMMapping(request.getSellerCode(), request.getDefaultSellerFMMapping().getCode(), oldPincode, oldPincode,info);
            response.setSellerFMMappingUpdateFailedSro(sro);
            if (sro == null) {
                atLeastOneSuccess = true;
            }
            Map<String, ValidationError> errorMap = fulfilmentModelService.updateSellerSubcatFMMapping(request.getSellerCode(), request.getSubcatToFMMapping(), info);
            response.setSubcatToErrorMap(errorMap);
            if (!(errorMap != null && !errorMap.isEmpty()) && request.getSubcatToFMMapping() != null && !request.getSubcatToFMMapping().isEmpty()) {
                atLeastOneSuccess = true;
            }
        }
        if (LOG.isDebugEnabled()) {
            LOG.debug("Response returned from addOrUpdateFMMapping {}", response);
        }
        response.setSuccessful(atLeastOneSuccess);
        LOG.info("Sending response {}", response);
        return response;

    }

    private ValidationError basicValidationForSetFMMapping(SetFMMappingRequest request) {
        if (null != request) {
            if (null != request.getSubcatToFMMapping()) {
                for (Entry<String, FulfillmentModel> entry : request.getSubcatToFMMapping().entrySet()) {
                    if (null == entry.getValue()) {
                        return new ValidationError(ApiErrorCode.REQUIRED_FIELD_NULL.code(), "Fulfiment Model found to be null for subcat " + entry.getKey());
                    }
                }
            }

        } else {
            return new ValidationError(ApiErrorCode.REQUIRED_FIELD_NULL.code(), " Request found to be null");
        }
        return null;
    }

    private SellerFMMappingUpdateFailedSro updateSellerFMMapping(String sellerCode, String newFulfilmentModel, String oldPincode, String newPincode, UserInfo info) {
        SellerFMMappingUpdateFailedSro sro = null;

        String existingFulfilmentModel = null;
        // getting FM Mapping At Seller Level
        //by default newFCCenters is null, non serviceable until manual mapping is done
        List<String> newFCCenters = null;
        //according to above, pass fcCenters to null in DTO  
        String fcCenters = null;
        List<String> existingFCCenters = null;
        try {
            SellerFMMappingVO vo = fulfilmentModelService.getFulfilmentModelBySellerFromAerospike(sellerCode);
            if (vo != null && vo.isEnabled()) {
                existingFulfilmentModel = vo.getFulfillmentModel();
                existingFCCenters = vo.getFcCenters();
                // retain Existing FC centers if no change in FM. 
                fcCenters = getFCentersForFMUpdate(newFulfilmentModel, existingFulfilmentModel, existingFCCenters);
                newFCCenters = getNewFCentersForFMUpdate(newFulfilmentModel, existingFulfilmentModel, existingFCCenters);
            }
        } catch (ExternalDataNotFoundException e) {
            LOG.error("Exception when trying to find FulfilmentModel for Seller{}, Message:{}", sellerCode, e);
        }

        try {

            GenericPersisterWithAerospikeResponse<SellerFMMapping, SellerFMMappingVO> resp = aerospikedataUpdater.updateDataWithDTO(new SellerFMMappingUpdateDto(sellerCode,
                    newFulfilmentModel, fcCenters, DateUtils.getCurrentTime()), sellerFMMappingDataReader, sellerFMMappingDataEngine, info, true);

            if (!resp.isSuccessful()) {
                sro = new SellerFMMappingUpdateFailedSro(sellerCode, new ValidationError(ApiErrorCode.DATA_NOT_PERSISTED.code(), resp.getMessage()));
            } else {
                processAsynchForChangeOfSellerFMMapping(sellerCode, newFulfilmentModel, existingFulfilmentModel, 
                        newFCCenters, existingFCCenters, oldPincode, newPincode);
            }
        } catch (OperationNotSupportedException e) {
            LOG.error("Exception ", e);
            sro = new SellerFMMappingUpdateFailedSro(sellerCode, new ValidationError(ApiErrorCode.DATA_NOT_PERSISTED.code(), e.getMessage()));
        } catch (GenericPersisterException e) {
            LOG.error("Generic Persistence Exception", e);
            sro = new SellerFMMappingUpdateFailedSro(sellerCode, new ValidationError(ApiErrorCode.DATA_NOT_PERSISTED.code(), e.getMessage()));
        } catch (Exception e) {
            LOG.error("Exception", e);
            sro = new SellerFMMappingUpdateFailedSro(sellerCode, new ValidationError(ApiErrorCode.DATA_NOT_PERSISTED.code(), e.getMessage()));
        }
        return sro;
    }

    private List<String> getNewFCentersForFMUpdate(String newFulfilmentModel, String existingFulfilmentModel, List<String> existingFCCenters) {
        //FM has not changed
        if (newFulfilmentModel != null && newFulfilmentModel.trim().equals(existingFulfilmentModel)) {
            return existingFCCenters;
        }
        return null;
    }

    private String getFCentersForFMUpdate(String newFulfilmentModel, String existingFulfilmentModel, List<String> existingFCCenters) {
        //FM has not changed
        if (newFulfilmentModel != null && newFulfilmentModel.trim().equals(existingFulfilmentModel)) {
            return converterService.convertFCCentreListToString(existingFCCenters);
        }
        return null;
    }

    //    @Async
    private void processAsynchForChangeOfSellerFMMapping(final String sellerCode, final String newFulfilmentModel, final String existingFulfilmentModel, List<String> newFCCenters,
            List<String> existingFCCenters, String oldPincode, String newPincode) {
        failSafeSellerSUPCFMMappingProcessor.processChangeOfSellerFMMappingForExternalNotification(new SellerFMFCUpdateEventObj(sellerCode, newFulfilmentModel,
                existingFulfilmentModel, newFCCenters, existingFCCenters,oldPincode, newPincode));
    }

    @Override
    public GetFulfilmentModelsForSellerResponse getFulfilmentModelsForSeller(GetFulfilmentModelsForSellerRequest request) {
        LOG.info("Request received {}", request);
        GetFulfilmentModelsForSellerResponse response = new GetFulfilmentModelsForSellerResponse();
        response.setProtocol(request.getResponseProtocol());
        ValidationError error = requestValidationService.requestBasicFieldValidation(request);
        if (error != null) {
            LOG.error("Validation error found request for supc {}", request != null ? request.getSellerCode() : null);
            responseBuilder.buildFailedResponse(error, response);
            return response;
        }
        List<String> fulfilmentModels = fulfilmentModelService.getFulfilmentModelsForSeller(request.getSellerCode());
        if (CollectionUtils.isEmpty(fulfilmentModels)) {
            LOG.error("No info found for requested supc {}", request != null ? request.getSellerCode() : null);
            responseBuilder.buildFailedResponse(ApiErrorCode.REQUESTED_DATA_DOES_NOT_EXISTS, response);
        } else {
            response.setFulfilmentModels(fulfilmentModels);
        }

        LOG.info("Sending response {}", response);
        return response;
    }

    @Override
    public SetSellerSubcatFMMappingResponse setSellerSubcatFMMapping(SetSellerSubcatFMMappingRequest request) {
        LOG.info("Request received {}", request);
        SetSellerSubcatFMMappingResponse response = new SetSellerSubcatFMMappingResponse();
        response.setProtocol(request.getResponseProtocol());
        ValidationError error = requestValidationService.requestBasicFieldValidation(request);
        if (null == error) {
            error = basicValidationForSetSellerSubcatFMMapping(request.getSubcatToFMMapping());
        }
        boolean atLeastOneSuccess = false;
        if (error != null) {
            LOG.error("Validation error found request {}", request);
            responseBuilder.buildFailedResponse(error, response);
        } else if (!ConfigUtils.getBooleanScalar(Property.UPDATE_FM_FROM_API)) {
            LOG.info("fulfillment model update flag is set off , hence not processing the request");
            response.setSuccessful(false);
            response.setMessage("update mode is set off for fulfillment model data");
            return response;
        } else {
            UserInfo info = new UserInfo(request.getUserEmail(), false);
            Map<String, ValidationError> errorMap = fulfilmentModelService.addOrUpdateSellerSubcatFMMapping(request.getSellerCode(), request.getSubcatToFMMapping(), info);
            response.setSubcatToErrorMap(errorMap);
            if (!(errorMap != null && !errorMap.isEmpty()) && request.getSubcatToFMMapping() != null && !request.getSubcatToFMMapping().isEmpty()) {
                atLeastOneSuccess = true;
            }
        }
        response.setSuccessful(atLeastOneSuccess);
        LOG.info("Sending response {}", response);
        return response;

    }

    private ValidationError basicValidationForSetSellerSubcatFMMapping(Map<String, String> map) {
        if (null != map) {
            for (Entry<String, String> subcatToFM : map.entrySet()) {
                if (null == FulfillmentModel.getFulfilmentModelByCode(subcatToFM.getValue())) {
                    return new ValidationError(ApiErrorCode.INVALID_FULFILMENT_MODEL.code(), "Invalid Fulfiment Model for subcat " + subcatToFM.getKey());
                }
            }
        }
        return null;
    }

    @Override
    public SetSellerFMMappingResponse setSellerFMMapping(SetSellerFMMappingRequest request) {
        LOG.info("Request received {}", request);
        SetSellerFMMappingResponse response = new SetSellerFMMappingResponse();
        response.setProtocol(request.getResponseProtocol());
        ValidationError error = requestValidationService.requestBasicFieldValidation(request);
        if (null == error) {
            error = checkFMModel(request.getFulfilmentModel());
        }
        boolean success = false;
        if (error != null) {
            LOG.error("Validation error found request {}", request);
            responseBuilder.buildFailedResponse(error, response);
        } else if (!dep.getBooleanPropertyValue(Property.UPDATE_FM_FROM_API)) {
            LOG.info("fulfillment model update flag is set off , hence not processing the request");
            response.setSuccessful(false);
            response.setMessage("update mode is set off for fulfillment model data");
            return response;
        } else {
            UserInfo info = new UserInfo(request.getUserEmail(), false);
            SellerContactDetails existingSCD = sellerDetailsService.getSellerContactDetailsBySeller(request.getSellerCode());
            String oldPincode = existingSCD != null ? existingSCD.getPinCode() : null;
          
            SellerFMMappingUpdateFailedSro sro = updateSellerFMMapping(request.getSellerCode(), request.getFulfilmentModel(), oldPincode,oldPincode, info);
            response.setSellerFMMappingUpdateFailedSro(sro);
            if (sro == null) {
                success = true;
            }
        }
        if (LOG.isDebugEnabled()) {
            LOG.debug("Response returned from setSellerFMMapping {}", response);
        }
        response.setSuccessful(success);
        LOG.info("Sending response {}", response);
        return response;

    }

    private ValidationError checkFMModel(String fulfilmentModel) {
        if (null == FulfillmentModel.getFulfilmentModelByCode(fulfilmentModel)) {
            return new ValidationError(ApiErrorCode.INVALID_FULFILMENT_MODEL.code(), "Invalid Fulfiment Model " + fulfilmentModel);
        }
        return null;
    }

    @Override
    public GetSellerSubcatFMMappingResponse getSellerSubcatFMMapping(GetSellerSubcatFMMappingRequest request) {
        LOG.info("Request received {}", request);
        GetSellerSubcatFMMappingResponse response = new GetSellerSubcatFMMappingResponse();
        response.setProtocol(request.getResponseProtocol());
        ValidationError error = requestValidationService.requestBasicFieldValidation(request);
        if (error != null) {
            LOG.error("Validation error found request {}", request);
            responseBuilder.buildFailedResponse(error, response);
        } else {
            //Getting FM Mapping at Seller Subcat level for a particular seller
            boolean ipmsAPIEnabled = ConfigUtils.getBooleanScalar(Property.IPMS_SUBAT_FM_API_ENABLED);
            if (!ipmsAPIEnabled) {
                LOG.info("subcat level fm is disabled for ipms , hence returning empty fm list for subcats for seller {} ", request.getSellerCode());
                response.setSellerSubcatFMMappingSroList(new ArrayList<SellerSubcatFulfilmentModelMappingSRO>());
            } else {
                response.setSellerSubcatFMMappingSroList(fulfilmentModelService.getSellerSubcatFMMappingSRO(request.getSellerCode()));
            }
        }
        LOG.info("Sending Response {}", response);
        return response;
    }

    @Override
    public GetSDFulfilledStatusBySupcSellersSubcatResponse getSDFulfilledStatusBySupcSellersSubcat(GetSDFulfilledStatusBySupcSellersSubcatRequest request) {
        LOG.debug("Request received to get SD fulfilled status by supc/seller/subcat: {}", request);
        GetSDFulfilledStatusBySupcSellersSubcatResponse response = new GetSDFulfilledStatusBySupcSellersSubcatResponse();
        response.setProtocol(request.getResponseProtocol());
        ValidationError error = requestValidationService.requestBasicFieldValidation(request);
        if (error != null) {
            LOG.error("Validation error found for request {}", request);
            responseBuilder.buildFailedResponse(error, response);
        } else {
            try {
                response.setSdFulfilled(fulfilmentModelService.getSDFUlfilledBySellerSupcSubcat(request.getSupcGroups()));
            } catch (ExternalDataNotFoundException e) {
                LOG.error("Failed to determine SD Fulfillability with exception {}", e.getMessage());
                response.setCode(ApiErrorCode.INTERNAL_ERROR.name());
                response.setSuccessful(false);
            }
        }

        LOG.debug("Sending response {}", response);
        return response;
    }

    public interface IDependency {
        boolean getBooleanPropertyValue(Property property);
    }

    static class Dependency implements IDependency {
        @Override
        public boolean getBooleanPropertyValue(Property property) {
            return ConfigUtils.getBooleanScalar(property);
        }
    }

    @Override
    public GetAllSellersForFMResponse getAllSellerForFM(GetAllSellersForFMRequest request) {
        LOG.info("Request received {}", request);
        GetAllSellersForFMResponse response = new GetAllSellersForFMResponse();
        response.setProtocol(request.getResponseProtocol());
        ValidationError error = requestValidationService.requestBasicFieldValidation(request);
        if (error != null) {
            LOG.error("Validation error found request {}", request);
            responseBuilder.buildFailedResponse(error, response);
        } else {
            Set<String> sellerList = fulfilmentModelService.getAllSellerForFM(request.getFulfillmentModel().getCode());
            response.setSellerCodes(sellerList);
        }

        LOG.info("for FM {} Sending response {}", request.getFulfillmentModel(), response);

        return response;
    }
    
    @Override
	public SetSellerDetailsResponse setSellerDetails(
			SetSellerDetailsRequest request) {
		SetSellerDetailsResponse response = new SetSellerDetailsResponse();
        response.setProtocol(request.getResponseProtocol());
        ValidationError error = requestValidationService.requestBasicFieldValidation(request);
        if (!ConfigUtils.getBooleanScalar(Property.SHIP_NEAR)) {
            LOG.info("ship near flag is set off , hence not processing the request");
            response.setSuccessful(false);
            response.setMessage("Ship near flag is set off for seller details update");
        } else if (!dep.getBooleanPropertyValue(Property.UPDATE_FM_FROM_API)) {
            LOG.info("fulfillment model update flag is set off , hence not processing the request");
            response.setSuccessful(false);
            response.setMessage("update mode is set off for fulfillment model data");
        }else if (error != null) {
            LOG.error("Validation error found request for request {}", request);
            responseBuilder.buildFailedResponse(error, response);
            response.setError(error);
        }else if(request.getSellerContactDetails()!=null && StringUtils.isEmpty(request.getSellerContactDetails().getPincode())){
        	LOG.error("Validation error found request for request {}", request);
        	ValidationError err = new ValidationError(ApiErrorCode.INSUFFICIENT_DATA.code(), "Pincode cannot be empty.");
            responseBuilder.buildFailedResponse(err,response);
        }else if(FulfillmentModel.getFulfilmentModelByCode(request.getDefaultSellerFMMapping()) == null){
        	LOG.error("Validation error found request for request {}", request);
        	ValidationError err = new ValidationError(ApiErrorCode.INVALID_FULFILMENT_MODEL.code(), "Unknown fulfilment model.");
            responseBuilder.buildFailedResponse(err,response);
        }
        else{
        	boolean success = true;
        	String message = "";
        	/*
        	 * For the moment, the two persistence calls is not inside a transaction as this involves 2 db and 2 aerospike calls which cannot be 
        	 * completely rolled back using touchyTransaction. Hence, this may result in partial transactional success/failure currently.
        	 */
        	
        	//First try creating/updating seller details
    		SellerContactDetails scd = null;
    		SellerContactDetails existingSCD = null;
			try {
			    existingSCD = sellerDetailsService.getSellerContactDetailsBySeller(request.getSellerCode());
				scd = sellerDetailsService.createUpdateSellerContactDetails(converterService
						.getSellerContactDetailsFromSRO(request.getSellerContactDetails(),request.getSellerCode(), request.getUserEmail()));
			} catch (Exception e) {
				success = false;
    			message += e.getMessage();
			}
    		if(success){
	        	//Now try updating FM for seller
	        	UserInfo info = new UserInfo(request.getUserEmail(), false);
	        	String oldPincode = existingSCD != null ? existingSCD.getPinCode() : scd.getPinCode();
	            SellerFMMappingUpdateFailedSro sro = updateSellerFMMapping(request.getSellerCode(), request.getDefaultSellerFMMapping(),oldPincode, scd.getPinCode(), info);
	            if(sro != null){
	            	success = false;
	            	message += sro.getValidationError().getMessage();
	        	}
    		}
            response.setSuccessful(success);
            if(!success){
	            response.setMessage(message);
	            response.setError(new ValidationError(ApiErrorCode.INTERNAL_ERROR.code(), message));
            }
        }
        
        LOG.info("Sending response {}", response);
        return response;
	}

	@Override
	public ValidateSellerCodeResponse validateSellers(ValidateSellerCodeRequest request) {
		 LOG.info("Request received {}", request);
		 ValidateSellerCodeResponse response = new ValidateSellerCodeResponse();
	        response.setProtocol(request.getResponseProtocol());
	        Map<String, Boolean> resultMap = null;
	        ValidationError error = requestValidationService.requestBasicFieldValidation(request);
	        if (error != null) {
	            LOG.error("Validation error found request {}", request);
	            responseBuilder.buildFailedResponse(error, response);
	        } else {
	        	 try {
	        		 resultMap = fulfilmentModelService.validateSellers(request.getSellerList());
	        		 response.setSuccessful(true);
	        		 response.setSellerCodeValidationMap(resultMap);
	                 
	             } catch (ExternalDataNotFoundException e) {
	                 LOG.error("Failed to determine SD Fulfillability with exception {}", e.getMessage());
	                 response.setCode(ApiErrorCode.INTERNAL_ERROR.name());
	                 response.setSuccessful(false);
	             }
	        }

	        LOG.info("Validation request completed");

	        return response;
	}
	
	
}
