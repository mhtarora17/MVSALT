package com.snapdeal.cocofs.web.landing.layer;

import com.snapdeal.cocofs.model.request.AddOrUpdateCategoryModeMappingRequest;
import com.snapdeal.cocofs.model.request.AddOrUpdateSupcModeMappingRequest;
import com.snapdeal.cocofs.model.request.GetAllCategoryModeMappingRequest;
import com.snapdeal.cocofs.model.request.GetAllCenterTypesRequest;
import com.snapdeal.cocofs.model.request.GetAllFCDetailRequest;
import com.snapdeal.cocofs.model.request.GetAllSupcModeMappingRequest;
import com.snapdeal.cocofs.model.request.GetCategoryModeMappingRequest;
import com.snapdeal.cocofs.model.request.GetFulfilmentFeeParamRequest;
import com.snapdeal.cocofs.model.request.GetFulfilmentModelAndProductAttributesRequest;
import com.snapdeal.cocofs.model.request.GetFulfilmentModelAndWeightRequest;
import com.snapdeal.cocofs.model.request.GetGiftWrapperInfoBySUPCSellerListRequest;
import com.snapdeal.cocofs.model.request.GetGiftWrapperInfoBySUPCSellerListResponse;
import com.snapdeal.cocofs.model.request.GetGiftWrapperInfoBySUPCVendorRequest;
import com.snapdeal.cocofs.model.request.GetMultiplierForPerimeterRequest;
import com.snapdeal.cocofs.model.request.GetProductDeliveryAndPackagingTypeRequest;
import com.snapdeal.cocofs.model.request.GetProductDeliveryTypeRequest;
import com.snapdeal.cocofs.model.request.GetProductFulfilmentAttributeRequest;
import com.snapdeal.cocofs.model.request.GetProductPackagingTypeRequest;
import com.snapdeal.cocofs.model.request.GetProductSystemWeightCapturedFlagRequest;
import com.snapdeal.cocofs.model.request.GetProductWeightNotificationRequest;
import com.snapdeal.cocofs.model.request.GetSupcModeMappingRequest;
import com.snapdeal.cocofs.model.request.GetVolumetricWeightFormulaParamsRequest;
import com.snapdeal.cocofs.model.request.RemoveCategoryModeMappingRequest;
import com.snapdeal.cocofs.model.request.RemoveSupcModeMappingRequest;
import com.snapdeal.cocofs.model.request.SetProductCapturedWeightRequest;
import com.snapdeal.cocofs.model.request.SetProductFulfilmentAttributeRequest;
import com.snapdeal.cocofs.model.response.AddOrUpdateCategoryModeMappingResponse;
import com.snapdeal.cocofs.model.response.AddOrUpdateSupcModeMappingResponse;
import com.snapdeal.cocofs.model.response.AllCenterTypesResponse;
import com.snapdeal.cocofs.model.response.AllFCDetailResponse;
import com.snapdeal.cocofs.model.response.GetAllCategoryModeMappingResponse;
import com.snapdeal.cocofs.model.response.GetAllDangerousGoodsTypeResponse;
import com.snapdeal.cocofs.model.response.GetAllSupcModeMappingResponse;
import com.snapdeal.cocofs.model.response.GetCategoryModeMappingResponse;
import com.snapdeal.cocofs.model.response.GetFulFilmentFeeParamResponse;
import com.snapdeal.cocofs.model.response.GetFulfilmentModelAndProductAttributesResponse;
import com.snapdeal.cocofs.model.response.GetFulfilmentModelAndWeightResponse;
import com.snapdeal.cocofs.model.response.GetGiftWrapperInfoBySUPCVendorResponse;
import com.snapdeal.cocofs.model.response.GetMultiplierForPerimeterResponse;
import com.snapdeal.cocofs.model.response.GetProductDeliveryAndPackagingTypeResponse;
import com.snapdeal.cocofs.model.response.GetProductDeliveryTypeResponse;
import com.snapdeal.cocofs.model.response.GetProductFulfilmentAttributeResponse;
import com.snapdeal.cocofs.model.response.GetProductPackagingTypeResponse;
import com.snapdeal.cocofs.model.response.GetProductSystemWeightCapturedFlagResponse;
import com.snapdeal.cocofs.model.response.GetProductWeightNotificationResponse;
import com.snapdeal.cocofs.model.response.GetSupcModeMappingResponse;
import com.snapdeal.cocofs.model.response.GetVolumetricWeightFormulaParamsResponse;
import com.snapdeal.cocofs.model.response.RemoveCategoryModeMappingResponse;
import com.snapdeal.cocofs.model.response.RemoveSupcModeMappingResponse;
import com.snapdeal.cocofs.model.response.SetProductAttributeFulfilmentResponse;
import com.snapdeal.cocofs.model.response.SetProductCapturedWeightResponse;

/**
 * @author nikhil
 */
public interface ICocofsLandingService {

    /**
     * @param request
     * @return
     */
    public GetProductFulfilmentAttributeResponse getProductFulfilmentAttributes(GetProductFulfilmentAttributeRequest request);

    /**
     * @param request
     * @return
     */
    public SetProductAttributeFulfilmentResponse setProductFulfilmentAttributes(SetProductFulfilmentAttributeRequest request);

    /**
     * Get package and delivery type for given supc list
     * 
     * @param request
     * @return
     */
    public GetProductDeliveryAndPackagingTypeResponse getProductDeliveryAndPackgingType(GetProductDeliveryAndPackagingTypeRequest request);

    /**
     * Get delivery type for given supc list
     * 
     * @param request
     * @return
     */
    public GetProductDeliveryTypeResponse getProductDeliveryType(GetProductDeliveryTypeRequest request);

    /**
     * Get package type for given supc list
     * 
     * @param request
     * @return
     */

    public GetProductPackagingTypeResponse getProductPackagingType(GetProductPackagingTypeRequest request);

    /**
     * Get package type , delivery type and and fulfillment model for the given
     * 
     * @param request
     * @return
     */
    public GetFulFilmentFeeParamResponse getFulfillmentFeeParams(GetFulfilmentFeeParamRequest request);

    /**
     * @param request
     * @return
     */
    GetProductWeightNotificationResponse getProductWeightNotifications(GetProductWeightNotificationRequest request);

    /**
     * Get weight and fulfilment model for the given seller and supc
     * 
     * @param request
     * @return
     */
    public GetFulfilmentModelAndWeightResponse getFulfilmentModelAndWeight(GetFulfilmentModelAndWeightRequest request);

    /**
     * Gives fulfilment model and product attributes for given seller and supc
     * 
     * @param request
     * @return
     */
    public GetFulfilmentModelAndProductAttributesResponse getFulfilmentModelAndProductAttributes(GetFulfilmentModelAndProductAttributesRequest request);

    /**
     * Remove given supc shipping mode mappings
     * 
     * @param request
     * @return
     */
    public RemoveSupcModeMappingResponse removeSupcModeMapping(RemoveSupcModeMappingRequest request);

    /**
     * Remove given category shipping mode mappings
     * 
     * @param request
     * @return
     */
    public RemoveCategoryModeMappingResponse removeCategoryModeMapping(RemoveCategoryModeMappingRequest request);

    /**
     * Add or update given supc shipping mode mappings
     * 
     * @param request
     * @return
     */
    public AddOrUpdateSupcModeMappingResponse addOrUpdateSupcModeMapping(AddOrUpdateSupcModeMappingRequest request);

    /**
     * Add or update given supc shipping mode mappings
     * 
     * @param request
     * @return
     */
    public AddOrUpdateCategoryModeMappingResponse addOrUpdateCategoryModeMapping(AddOrUpdateCategoryModeMappingRequest request);

    /**
     * get Supc mode mappings for given list of supc
     * 
     * @param request
     * @return
     */
    public GetSupcModeMappingResponse getSupcModeMapping(GetSupcModeMappingRequest request);

    /**
     * get category mode mappings for given list of category
     * 
     * @param request
     * @return
     */
    public GetCategoryModeMappingResponse getCategoryModeMapping(GetCategoryModeMappingRequest request);

    /**
     * get all supc shipping mode mappings
     * 
     * @param request
     * @return
     */
    public GetAllSupcModeMappingResponse getAllSupcModeMapping(GetAllSupcModeMappingRequest request);

    /**
     * get all category mode mappings
     * 
     * @param request
     * @return
     */
    public GetAllCategoryModeMappingResponse getAllCategoryModeMapping(GetAllCategoryModeMappingRequest request);

    /**
     * Set system captured weight. This weight is actually captured by user via weighing machine and updated.
     * 
     * @param request
     * @return
     */
    public SetProductCapturedWeightResponse setProductCapturedWeight(SetProductCapturedWeightRequest request);

    /**
     * Get weight along with system captured weight flag. flag 0 - weight is uploaded by admin, flag 1- weight is
     * updated by actual weighing machine
     * 
     * @param request
     * @return
     */
    public GetProductSystemWeightCapturedFlagResponse getProductSystemWeightCapturedFlag(GetProductSystemWeightCapturedFlagRequest request);

    public com.snapdeal.cocofs.model.response.v2.GetFulFilmentFeeParamResponse getFulfillmentFeeParams(com.snapdeal.cocofs.model.request.v2.GetFulfilmentFeeParamRequest request);

    public GetAllDangerousGoodsTypeResponse getAllDangerousGoodsType();

    public GetGiftWrapperInfoBySUPCVendorResponse getGiftWrapperInfoBySUPCVendor(GetGiftWrapperInfoBySUPCVendorRequest request);

    GetGiftWrapperInfoBySUPCSellerListResponse getGiftWrapperInfoBySUPCSellerList(GetGiftWrapperInfoBySUPCSellerListRequest request);

    AllFCDetailResponse getAllFCDetails(GetAllFCDetailRequest req);

	GetVolumetricWeightFormulaParamsResponse getVolumetricWeightFormulaParams(
			GetVolumetricWeightFormulaParamsRequest request);

    public AllCenterTypesResponse getAllCenterTypes(GetAllCenterTypesRequest request);
    
    GetMultiplierForPerimeterResponse getMultiplierForPerimeter(GetMultiplierForPerimeterRequest request);
    
    
 

}
