/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 30-Sep-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.web.landing.layer;

import com.snapdeal.cocofs.model.request.GetAllSellersForFMRequest;
import com.snapdeal.cocofs.model.request.GetFMMappingAtSellerAndSellerSubcatRequest;
import com.snapdeal.cocofs.model.request.GetFulfilmentModelBySellerSupcSubcatRequest;
import com.snapdeal.cocofs.model.request.GetFulfilmentModelRequest;
import com.snapdeal.cocofs.model.request.GetFulfilmentModelsForSellerRequest;
import com.snapdeal.cocofs.model.request.GetSDFulfilledStatusBySupcSellersSubcatRequest;
import com.snapdeal.cocofs.model.request.GetSDFulfilledStatusBySupcSellersSubcatResponse;
import com.snapdeal.cocofs.model.request.GetSellerSubcatFMMappingRequest;
import com.snapdeal.cocofs.model.request.SetFMMappingRequest;
import com.snapdeal.cocofs.model.request.SetSellerDetailsRequest;
import com.snapdeal.cocofs.model.request.SetSellerFMMappingRequest;
import com.snapdeal.cocofs.model.request.SetSellerSubcatFMMappingRequest;
import com.snapdeal.cocofs.model.request.ValidateSellerCodeRequest;
import com.snapdeal.cocofs.model.response.GetAllSellersForFMResponse;
import com.snapdeal.cocofs.model.response.GetFMMappingAtSellerAndSellerSubcatResponse;
import com.snapdeal.cocofs.model.response.GetFulfilmentModelBySellerSupcSubcatResponse;
import com.snapdeal.cocofs.model.response.GetFulfilmentModelResponse;
import com.snapdeal.cocofs.model.response.GetFulfilmentModelsForSellerResponse;
import com.snapdeal.cocofs.model.response.GetSellerSubcatFMMappingResponse;
import com.snapdeal.cocofs.model.response.SetFMMappingResponse;
import com.snapdeal.cocofs.model.response.SetSellerDetailsResponse;
import com.snapdeal.cocofs.model.response.SetSellerFMMappingResponse;
import com.snapdeal.cocofs.model.response.SetSellerSubcatFMMappingResponse;
import com.snapdeal.cocofs.model.response.ValidateSellerCodeResponse;

public interface ICocofsFMLandingService {

    /**
     * @param request
     * @return
     */
    public GetFulfilmentModelResponse getFulfilmentModel(GetFulfilmentModelRequest request);

    /**
     * @param request
     * @return
     */
    public GetFulfilmentModelBySellerSupcSubcatResponse getFulfilmentModelBySellerSupcSubcat(GetFulfilmentModelBySellerSupcSubcatRequest request);

    /**
     * @param request
     * @return
     */
    public GetFMMappingAtSellerAndSellerSubcatResponse getFMMappingAtSellerAndSellerSubcat(GetFMMappingAtSellerAndSellerSubcatRequest request);

    /**
     * @param request
     * @return
     */
    public SetFMMappingResponse setFMMapping(SetFMMappingRequest request);

    /**
     * Returns all possible fm for a seller
     * 
     * @param request
     * @return
     */
    public GetFulfilmentModelsForSellerResponse getFulfilmentModelsForSeller(GetFulfilmentModelsForSellerRequest request);

    /**
     * sets fm mapping at seller subcat level
     * 
     * @param request
     * @return
     */
    public SetSellerSubcatFMMappingResponse setSellerSubcatFMMapping(SetSellerSubcatFMMappingRequest request);

    /**
     * Sets default seller fm mapping
     * 
     * @param request
     * @return
     */
    public SetSellerFMMappingResponse setSellerFMMapping(SetSellerFMMappingRequest request);

    public GetSellerSubcatFMMappingResponse getSellerSubcatFMMapping(GetSellerSubcatFMMappingRequest request);
    
    /**
     * SNAPDEALTECH-28817
     * Get SD fulfilled status by seller/supc/subcat
     * 
     * @param request
     * @return
     */
    public GetSDFulfilledStatusBySupcSellersSubcatResponse getSDFulfilledStatusBySupcSellersSubcat(GetSDFulfilledStatusBySupcSellersSubcatRequest request);

    /**
     * Returns all enabled seller for a given Fulfillment
     * 
     * @param request
     * @return
     */
	public GetAllSellersForFMResponse getAllSellerForFM(GetAllSellersForFMRequest request);
        

    public SetSellerDetailsResponse setSellerDetails(SetSellerDetailsRequest request);
    
    ValidateSellerCodeResponse validateSellers(ValidateSellerCodeRequest request);
}
