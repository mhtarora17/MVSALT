package com.snapdeal.cocofs.web.builder.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.base.model.common.ServiceResponse;
import com.snapdeal.base.validation.ValidationError;
import com.snapdeal.cocofs.access.IProductAttributeService;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.enums.NotificationType;
import com.snapdeal.cocofs.model.response.ApiErrorCode;
import com.snapdeal.cocofs.model.response.GetProductFulfilmentAttributeResponse;
import com.snapdeal.cocofs.model.response.GetProductSystemWeightCapturedFlagResponse;
import com.snapdeal.cocofs.model.response.GetProductWeightNotificationResponse;
import com.snapdeal.cocofs.model.response.SetProductAttributeFulfilmentResponse;
import com.snapdeal.cocofs.model.response.SetProductCapturedWeightResponse;
import com.snapdeal.cocofs.mongo.model.NotificationSubUnit;
import com.snapdeal.cocofs.mongo.model.ProductAttributeNotificationUnit;
import com.snapdeal.cocofs.mongo.model.ProductAttributeUnit;
import com.snapdeal.cocofs.sro.ProductFulfilmentAttributeSRO;
import com.snapdeal.cocofs.sro.WeightUpdateSRO;
import com.snapdeal.cocofs.utils.UtilHelper;
import com.snapdeal.cocofs.web.builder.IResponseBuilder;
import com.snapdeal.cocofs.web.landing.layer.IConverterService;

/**
 * @author nikhil
 */
@Service("responseBuilder")
public class ResponseBuilderImpl implements IResponseBuilder {

    @Autowired
    IProductAttributeService  productAttributeService;

    @Autowired
    private IConverterService converterService;

    @Override
    public GetProductFulfilmentAttributeResponse buildSuccesfulProductFulfilmentAttributeResponse(ProductAttributeUnit pau, GetProductFulfilmentAttributeResponse response) {
        ProductFulfilmentAttributeSRO attributeSRO = converterService.getProductAttributeSro(pau);
        response.setProductFulfilmentAttribute(attributeSRO != null ? attributeSRO : new ProductFulfilmentAttributeSRO());
        response.setSuccessful(true);
        return response;
    }

    @Override
    public <T extends ServiceResponse> void buildFailedResponse(ValidationError error, T response) {
        List<ValidationError> validationErrors = new ArrayList<ValidationError>();
        response.setSuccessful(false);
        response.setMessage(error.getMessage());
        validationErrors.add(error);
        response.setValidationErrors(validationErrors);
    }

    @Override
    public <T extends ServiceResponse> void buildFailedResponse(ApiErrorCode errorCode, T response) {
        response.setSuccessful(false);
        List<ValidationError> validationErrors = new ArrayList<ValidationError>();
        if (errorCode.equals(ApiErrorCode.REQUESTED_DATA_DOES_NOT_EXISTS)) {
            String responseMessage = "No information exists for the requested params";
            response.setMessage(responseMessage);
            validationErrors.add(new ValidationError(ApiErrorCode.REQUESTED_DATA_DOES_NOT_EXISTS.code(), responseMessage));
            response.setValidationErrors(validationErrors);
        }else if(errorCode.equals(ApiErrorCode.REQUEST_BATCH_SIZE_LIMIT_EXCEEDED)){
            String responseMessage = "requested items are more than the  allowed batch size";
            response.setMessage(responseMessage);
            validationErrors.add(new ValidationError(ApiErrorCode.REQUEST_BATCH_SIZE_LIMIT_EXCEEDED.code(), responseMessage));
            response.setValidationErrors(validationErrors);
        }
    }

    @Override
    public SetProductAttributeFulfilmentResponse buildSetProductAttributeFulfilmentResponse(Map<String, ValidationError> validationErrors, int successCount,
            SetProductAttributeFulfilmentResponse response) {
        response.setSuccessCount(successCount);
        response.setFailures(validationErrors);
        response.setSuccessful(true);
        return response;
    }

    @Override
    public GetProductWeightNotificationResponse buildProductWeightNotificationResponse(List<ProductAttributeNotificationUnit> productAttributeNotificationUnits, String sellerCode,
            Long totalCount) {

        GetProductWeightNotificationResponse response = new GetProductWeightNotificationResponse();
        if (productAttributeNotificationUnits != null && productAttributeNotificationUnits.size() > 0) {

            response.setSellerCode(sellerCode);
            response.setTotal(totalCount != null ? totalCount.intValue() : 0);
            for (ProductAttributeNotificationUnit unit : productAttributeNotificationUnits) {
                WeightUpdateSRO weightUpdateSRO = new WeightUpdateSRO();
                weightUpdateSRO.setCreated(unit.getCreated());
                weightUpdateSRO.setSupc(unit.getSupc());
                weightUpdateSRO.setWithEffectFrom(unit.getWithEffectFrom());
                List<NotificationSubUnit> subUnits = unit.getSubUnits();
                if (subUnits != null && subUnits.size() > 0) {
                    boolean weightNotificationFound = false;
                    boolean volWeightNotificationFound = false;
                    for (NotificationSubUnit subUnit : subUnits) {
                        if (subUnit.getNotificationType().equals(NotificationType.DEAD_WEIGHT.getCode())) {
                            weightUpdateSRO.setNewDeadWeight(nearestApproximateDouble(Double.valueOf(subUnit.getNewValue())));
                            weightUpdateSRO.setOldDeadWeight(nearestApproximateDouble(Double.valueOf(subUnit.getCurrValue())));
                            weightNotificationFound = true;
                        }

                        if (subUnit.getNotificationType().equals(NotificationType.VOLUMETRIC_WEIGHT.getCode())) {
                            weightUpdateSRO.setNewVolumetricWeight(nearestApproximateDouble(Double.valueOf(subUnit.getNewValue())));
                            weightUpdateSRO.setOldVolumetricWeight(nearestApproximateDouble(Double.valueOf(subUnit.getCurrValue())));
                            volWeightNotificationFound = true;
                        }
                    }
                    // As discussed with AT, if any one of the weight or volumetric notification is not found, we have to populate existing value in both 
                    if (!volWeightNotificationFound || !weightNotificationFound) {
                        ProductAttributeUnit paUnit = productAttributeService.getProductAttributeUnitBySUPC(unit.getSupc());
                        if (!weightNotificationFound) {
                            weightUpdateSRO.setOldDeadWeight(nearestApproximateDouble(Double.valueOf(paUnit.getWeight())));
                            weightUpdateSRO.setNewDeadWeight(nearestApproximateDouble(Double.valueOf(paUnit.getWeight())));
                        } else if (!volWeightNotificationFound) {
                            Double volumetricWeight = UtilHelper.getVolumetricWeightFromLBH(Double.valueOf(paUnit.getLength()), Double.valueOf(paUnit.getBreadth()),
                                    Double.valueOf(paUnit.getHeight()));
                            weightUpdateSRO.setOldVolumetricWeight(nearestApproximateDouble(volumetricWeight));
                            weightUpdateSRO.setNewVolumetricWeight(nearestApproximateDouble(volumetricWeight));
                        }
                    }
                }
                response.addSupcToLatestUpdates(unit.getSupc(), weightUpdateSRO);
            }
        }
        return response;
    }

    private Double nearestApproximateDouble(Double d1) {
        int decimalPlaces = ConfigUtils.getIntegerScalar(Property.DECIMAL_PLACES_PRECISION);
        return Math.rint(d1 * Math.pow(10, decimalPlaces)) / (double) Math.pow(10, decimalPlaces);
    }

    @Override
    public SetProductCapturedWeightResponse buildSetProductCapturedWeightResponse(Map<String, ValidationError> validationErrors, int successCount,
            SetProductCapturedWeightResponse response) {
        if (validationErrors != null && !validationErrors.isEmpty()) {
            response.setFailures(validationErrors);
            response.setSuccessful(false);
        } else {
            response.setSuccessful(true);
        }
        response.setSuccessCount(successCount);
        return response;
    }

    public GetProductSystemWeightCapturedFlagResponse buildGetProductSystemWeightCapturedResponse(String supc, String systemWeightCaptured,
            GetProductSystemWeightCapturedFlagResponse response) {
        response.setSupc(supc);
        response.setSystemWeightCaptured(systemWeightCaptured);
        response.setSuccessful(true);
        return response;
    }
}