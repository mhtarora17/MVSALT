/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.web.landing.layer;

import com.snapdeal.cocofs.model.request.GetFMAndFCBySellerSupcListRequest;
import com.snapdeal.cocofs.model.request.GetShipFromLocationBySellerSupcListRequest;
import com.snapdeal.cocofs.model.response.GetFMAndFCBySellerSupcListResponse;
import com.snapdeal.cocofs.model.response.GetShipFromLocationBySellerSupcListResponse;

/**
 * @version 1.0, 27-Mar-2015
 * @author ankur
 */
public interface ICocofsFCLandingService {

    public GetShipFromLocationBySellerSupcListResponse getShipFromLocation(GetShipFromLocationBySellerSupcListRequest request) ;

    GetFMAndFCBySellerSupcListResponse GetFMAndFCBySellerSupcList(GetFMAndFCBySellerSupcListRequest request);
}
