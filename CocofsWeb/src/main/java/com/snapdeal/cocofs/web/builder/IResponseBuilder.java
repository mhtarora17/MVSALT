package com.snapdeal.cocofs.web.builder;

import java.util.List;
import java.util.Map;

import com.snapdeal.base.model.common.ServiceResponse;
import com.snapdeal.base.validation.ValidationError;
import com.snapdeal.cocofs.model.response.ApiErrorCode;
import com.snapdeal.cocofs.model.response.GetProductFulfilmentAttributeResponse;
import com.snapdeal.cocofs.model.response.GetProductSystemWeightCapturedFlagResponse;
import com.snapdeal.cocofs.model.response.GetProductWeightNotificationResponse;
import com.snapdeal.cocofs.model.response.SetProductAttributeFulfilmentResponse;
import com.snapdeal.cocofs.model.response.SetProductCapturedWeightResponse;
import com.snapdeal.cocofs.mongo.model.ProductAttributeNotificationUnit;
import com.snapdeal.cocofs.mongo.model.ProductAttributeUnit;

/**
 * @author nikhil
 */
public interface IResponseBuilder {

    public GetProductFulfilmentAttributeResponse buildSuccesfulProductFulfilmentAttributeResponse(ProductAttributeUnit pau, GetProductFulfilmentAttributeResponse response);

    public <T extends ServiceResponse> void buildFailedResponse(ApiErrorCode errorCode, T response);

    SetProductAttributeFulfilmentResponse buildSetProductAttributeFulfilmentResponse(Map<String, ValidationError> validationErrors, int successCount,
            SetProductAttributeFulfilmentResponse response);

    public <T extends ServiceResponse> void buildFailedResponse(ValidationError error, T response);
    
    public GetProductWeightNotificationResponse buildProductWeightNotificationResponse(List<ProductAttributeNotificationUnit> productAttributeNotificationUnits, String sellerCode,
            Long totalCount);
    
    public SetProductCapturedWeightResponse buildSetProductCapturedWeightResponse(Map<String, ValidationError> validationErrors, int successCount,
    		SetProductCapturedWeightResponse response);
    
    public GetProductSystemWeightCapturedFlagResponse buildGetProductSystemWeightCapturedResponse(String supc, String systemWeightCaptured,
    		GetProductSystemWeightCapturedFlagResponse response);
}
