package com.snapdeal.cocofs.web.controller;

import javax.ws.rs.Path;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.snapdeal.base.exception.TransportException;
import com.snapdeal.cocofs.model.request.AddOrUpdateCategoryModeMappingRequest;
import com.snapdeal.cocofs.model.request.AddOrUpdateSupcModeMappingRequest;
import com.snapdeal.cocofs.model.request.GetAllCategoryModeMappingRequest;
import com.snapdeal.cocofs.model.request.GetAllCenterTypesRequest;
import com.snapdeal.cocofs.model.request.GetAllDangerousGoodsTypeRequest;
import com.snapdeal.cocofs.model.request.GetAllFCDetailRequest;
import com.snapdeal.cocofs.model.request.GetAllSellersForFMRequest;
import com.snapdeal.cocofs.model.request.GetAllSupcModeMappingRequest;
import com.snapdeal.cocofs.model.request.GetCategoryModeMappingRequest;
import com.snapdeal.cocofs.model.request.GetFMAndFCBySellerSupcListRequest;
import com.snapdeal.cocofs.model.request.GetFMAndZonesBySellerSupcListRequest;
import com.snapdeal.cocofs.model.request.GetFMMappingAtSellerAndSellerSubcatRequest;
import com.snapdeal.cocofs.model.request.GetFulfilmentFeeParamRequest;
import com.snapdeal.cocofs.model.request.GetFulfilmentModelAndProductAttributesRequest;
import com.snapdeal.cocofs.model.request.GetFulfilmentModelAndWeightRequest;
import com.snapdeal.cocofs.model.request.GetFulfilmentModelBySellerSupcSubcatRequest;
import com.snapdeal.cocofs.model.request.GetFulfilmentModelRequest;
import com.snapdeal.cocofs.model.request.GetFulfilmentModelsForSellerRequest;
import com.snapdeal.cocofs.model.request.GetGiftWrapperInfoBySUPCSellerListRequest;
import com.snapdeal.cocofs.model.request.GetGiftWrapperInfoBySUPCSellerListResponse;
import com.snapdeal.cocofs.model.request.GetGiftWrapperInfoBySUPCVendorRequest;
import com.snapdeal.cocofs.model.request.GetMultiplierForPerimeterRequest;
import com.snapdeal.cocofs.model.request.GetProductDeliveryAndPackagingTypeRequest;
import com.snapdeal.cocofs.model.request.GetProductDeliveryTypeRequest;
import com.snapdeal.cocofs.model.request.GetProductFulfilmentAttributeRequest;
import com.snapdeal.cocofs.model.request.GetProductPackagingTypeRequest;
import com.snapdeal.cocofs.model.request.GetProductSystemWeightCapturedFlagRequest;
import com.snapdeal.cocofs.model.request.GetProductWeightNotificationRequest;
import com.snapdeal.cocofs.model.request.GetSDFulfilledStatusBySupcSellersSubcatRequest;
import com.snapdeal.cocofs.model.request.GetSDFulfilledStatusBySupcSellersSubcatResponse;
import com.snapdeal.cocofs.model.request.GetSellerSubcatFMMappingRequest;
import com.snapdeal.cocofs.model.request.GetShipFromLocationBySellerSupcListRequest;
import com.snapdeal.cocofs.model.request.GetSupcModeMappingRequest;
import com.snapdeal.cocofs.model.request.GetTaxClassRequest;
import com.snapdeal.cocofs.model.request.GetTaxInfoRequest;
import com.snapdeal.cocofs.model.request.GetTaxRateInfoBySUPCStateSellerPriceRequest;
import com.snapdeal.cocofs.model.request.GetVolumetricWeightFormulaParamsRequest;
import com.snapdeal.cocofs.model.request.GetZonesBySellerSupcListRequest;
import com.snapdeal.cocofs.model.request.RemoveCategoryModeMappingRequest;
import com.snapdeal.cocofs.model.request.RemoveSupcModeMappingRequest;
import com.snapdeal.cocofs.model.request.SetFMMappingRequest;
import com.snapdeal.cocofs.model.request.SetProductCapturedWeightRequest;
import com.snapdeal.cocofs.model.request.SetProductFulfilmentAttributeRequest;
import com.snapdeal.cocofs.model.request.SetSellerDetailsRequest;
import com.snapdeal.cocofs.model.request.SetSellerFMMappingRequest;
import com.snapdeal.cocofs.model.request.SetSellerSubcatFMMappingRequest;
import com.snapdeal.cocofs.model.request.ValidateSellerCodeRequest;
import com.snapdeal.cocofs.model.response.AddOrUpdateCategoryModeMappingResponse;
import com.snapdeal.cocofs.model.response.AddOrUpdateSupcModeMappingResponse;
import com.snapdeal.cocofs.model.response.AllCenterTypesResponse;
import com.snapdeal.cocofs.model.response.AllFCDetailResponse;
import com.snapdeal.cocofs.model.response.GetAllCategoryModeMappingResponse;
import com.snapdeal.cocofs.model.response.GetAllDangerousGoodsTypeResponse;
import com.snapdeal.cocofs.model.response.GetAllSellersForFMResponse;
import com.snapdeal.cocofs.model.response.GetAllSupcModeMappingResponse;
import com.snapdeal.cocofs.model.response.GetCategoryModeMappingResponse;
import com.snapdeal.cocofs.model.response.GetFMAndFCBySellerSupcListResponse;
import com.snapdeal.cocofs.model.response.GetFMAndZonesBySellerSupcListResponse;
import com.snapdeal.cocofs.model.response.GetFMMappingAtSellerAndSellerSubcatResponse;
import com.snapdeal.cocofs.model.response.GetFulFilmentFeeParamResponse;
import com.snapdeal.cocofs.model.response.GetFulfilmentModelAndProductAttributesResponse;
import com.snapdeal.cocofs.model.response.GetFulfilmentModelAndWeightResponse;
import com.snapdeal.cocofs.model.response.GetFulfilmentModelBySellerSupcSubcatResponse;
import com.snapdeal.cocofs.model.response.GetFulfilmentModelResponse;
import com.snapdeal.cocofs.model.response.GetFulfilmentModelsForSellerResponse;
import com.snapdeal.cocofs.model.response.GetGiftWrapperInfoBySUPCVendorResponse;
import com.snapdeal.cocofs.model.response.GetMultiplierForPerimeterResponse;
import com.snapdeal.cocofs.model.response.GetProductDeliveryAndPackagingTypeResponse;
import com.snapdeal.cocofs.model.response.GetProductDeliveryTypeResponse;
import com.snapdeal.cocofs.model.response.GetProductFulfilmentAttributeResponse;
import com.snapdeal.cocofs.model.response.GetProductPackagingTypeResponse;
import com.snapdeal.cocofs.model.response.GetProductSystemWeightCapturedFlagResponse;
import com.snapdeal.cocofs.model.response.GetProductWeightNotificationResponse;
import com.snapdeal.cocofs.model.response.GetSellerSubcatFMMappingResponse;
import com.snapdeal.cocofs.model.response.GetShipFromLocationBySellerSupcListResponse;
import com.snapdeal.cocofs.model.response.GetSupcModeMappingResponse;
import com.snapdeal.cocofs.model.response.GetTaxClassResponse;
import com.snapdeal.cocofs.model.response.GetTaxInfoResponse;
import com.snapdeal.cocofs.model.response.GetTaxRateInfoBySUPCStateSellerPriceResponse;
import com.snapdeal.cocofs.model.response.GetVolumetricWeightFormulaParamsResponse;
import com.snapdeal.cocofs.model.response.GetZonesBySellerSupcListResponse;
import com.snapdeal.cocofs.model.response.RemoveCategoryModeMappingResponse;
import com.snapdeal.cocofs.model.response.RemoveSupcModeMappingResponse;
import com.snapdeal.cocofs.model.response.SetFMMappingResponse;
import com.snapdeal.cocofs.model.response.SetProductAttributeFulfilmentResponse;
import com.snapdeal.cocofs.model.response.SetProductCapturedWeightResponse;
import com.snapdeal.cocofs.model.response.SetSellerDetailsResponse;
import com.snapdeal.cocofs.model.response.SetSellerFMMappingResponse;
import com.snapdeal.cocofs.model.response.SetSellerSubcatFMMappingResponse;
import com.snapdeal.cocofs.model.response.ValidateSellerCodeResponse;
import com.snapdeal.cocofs.services.ISellerDetailsService;
import com.snapdeal.cocofs.utils.CocofsConstants;
import com.snapdeal.cocofs.web.controlled.Execution;
import com.snapdeal.cocofs.web.controlled.GenericCommand;
import com.snapdeal.cocofs.web.landing.layer.ICocofsFCLandingService;
import com.snapdeal.cocofs.web.landing.layer.ICocofsFMLandingService;
import com.snapdeal.cocofs.web.landing.layer.ICocofsLandingService;
import com.snapdeal.cocofs.web.landing.layer.ICocofsTaxRateLandingService;
import com.snapdeal.cocofs.web.landing.layer.ICocofsZoneLandingService;

@Controller
@RequestMapping(CocofsWebServiceController.URL)
@Path(CocofsWebServiceController.URL)
@Profile(CocofsConstants.COCOFS_PROFILE_NAME)
public class CocofsWebServiceController {

    public static final String   URL = "/service/cocofs/";

    private static final Logger  LOG = LoggerFactory.getLogger(CocofsWebServiceController.class);
    @Autowired
    ICocofsLandingService        cocoFSLandingService;

    @Autowired
    ICocofsFMLandingService      cocofsFMLandingService;

    @Autowired
    ICocofsFCLandingService      cocofsFCLandingService;

    @Autowired
    ICocofsTaxRateLandingService cocofsTaxLandingService;

    @Autowired
    ICocofsZoneLandingService    cocofsZoneLandingService;

    @Autowired
    ISellerDetailsService        sellerDetailsService;

    @RequestMapping(value = "getGiftWrapperInfoBySUPCVendor", produces = "application/sd-service")
    @ResponseBody
    public GetGiftWrapperInfoBySUPCVendorResponse getGiftWrapperInfoBySUPCVendor(@RequestBody GetGiftWrapperInfoBySUPCVendorRequest request) {
        Execution<GetGiftWrapperInfoBySUPCVendorRequest, GetGiftWrapperInfoBySUPCVendorResponse> execution = new Execution<GetGiftWrapperInfoBySUPCVendorRequest, GetGiftWrapperInfoBySUPCVendorResponse>() {

            @Override
            public GetGiftWrapperInfoBySUPCVendorResponse execute(GetGiftWrapperInfoBySUPCVendorRequest request) {
                try {
                    GetGiftWrapperInfoBySUPCVendorResponse response = cocoFSLandingService.getGiftWrapperInfoBySUPCVendor(request);
                    return response;
                } catch (RuntimeException e) {
                    LOG.debug("setProductFulfilmentAttributes", e);
                    throw e;
                }
            }
        };
        return GenericCommand.controlledExecute(request, execution, "getGiftWrapperInfoBySUPCVendor", "getGiftWrapperInfoBySUPCVendor");
    }

    @RequestMapping(value = "getGiftWrapperInfoBySUPCSellerList", produces = "application/sd-service")
    @ResponseBody
    public GetGiftWrapperInfoBySUPCSellerListResponse getGiftWrapperInfoBySUPCSellerList(@RequestBody GetGiftWrapperInfoBySUPCSellerListRequest request) {
        Execution<GetGiftWrapperInfoBySUPCSellerListRequest, GetGiftWrapperInfoBySUPCSellerListResponse> execution = new Execution<GetGiftWrapperInfoBySUPCSellerListRequest, GetGiftWrapperInfoBySUPCSellerListResponse>() {

            @Override
            public GetGiftWrapperInfoBySUPCSellerListResponse execute(GetGiftWrapperInfoBySUPCSellerListRequest request) {
                try {
                    GetGiftWrapperInfoBySUPCSellerListResponse response = cocoFSLandingService.getGiftWrapperInfoBySUPCSellerList(request);
                    return response;
                } catch (RuntimeException e) {
                    LOG.error("setProductFulfilmentAttributes", e);
                    throw e;
                }
            }
        };
        return GenericCommand.controlledExecute(request, execution, "getGiftWrapperInfoBySUPCSellerList", "getGiftWrapperInfoBySUPCSellerList");
    }

    @RequestMapping(value = "getProductFulfilmentAttributes", produces = "application/sd-service")
    @ResponseBody
    public GetProductFulfilmentAttributeResponse getProductFulfilmentAttributes(@RequestBody GetProductFulfilmentAttributeRequest request) {
        Execution<GetProductFulfilmentAttributeRequest, GetProductFulfilmentAttributeResponse> execution = new Execution<GetProductFulfilmentAttributeRequest, GetProductFulfilmentAttributeResponse>() {

            @Override
            public GetProductFulfilmentAttributeResponse execute(GetProductFulfilmentAttributeRequest request) {
                try {
                    GetProductFulfilmentAttributeResponse response = cocoFSLandingService.getProductFulfilmentAttributes(request);
                    return response;
                } catch (RuntimeException e) {
                    LOG.error("setProductFulfilmentAttributes", e);
                    throw e;
                }
            }
        };
        return GenericCommand.controlledExecute(request, execution, "getProductFulfilmentAttribute", "getProductFulfilmentAttribute");
    }

    @RequestMapping(value = "setProductFulfilmentAttributes", produces = "application/sd-service")
    @ResponseBody
    public SetProductAttributeFulfilmentResponse setProductFulfilmentAttributes(@RequestBody SetProductFulfilmentAttributeRequest request) {
        Execution<SetProductFulfilmentAttributeRequest, SetProductAttributeFulfilmentResponse> execution = new Execution<SetProductFulfilmentAttributeRequest, SetProductAttributeFulfilmentResponse>() {

            @Override
            public SetProductAttributeFulfilmentResponse execute(SetProductFulfilmentAttributeRequest request) {
                try {
                    SetProductAttributeFulfilmentResponse response = cocoFSLandingService.setProductFulfilmentAttributes(request);
                    return response;
                } catch (RuntimeException e) {
                    LOG.error("setProductFulfilmentAttributes", e);
                    throw e;
                }
            }

        };
        return GenericCommand.controlledExecute(request, execution, "supccreationgroup", "setProductFulfilmentAttributes");
    }

    @RequestMapping(value = "getDeliveryAndPackagingType", produces = "application/sd-service")
    @ResponseBody
    public GetProductDeliveryAndPackagingTypeResponse getProductDeliveryAndPackgingType(@RequestBody GetProductDeliveryAndPackagingTypeRequest request) {
        Execution<GetProductDeliveryAndPackagingTypeRequest, GetProductDeliveryAndPackagingTypeResponse> execution = new Execution<GetProductDeliveryAndPackagingTypeRequest, GetProductDeliveryAndPackagingTypeResponse>() {

            @Override
            public GetProductDeliveryAndPackagingTypeResponse execute(GetProductDeliveryAndPackagingTypeRequest request) {
                try {
                    GetProductDeliveryAndPackagingTypeResponse response = cocoFSLandingService.getProductDeliveryAndPackgingType(request);
                    return response;
                } catch (RuntimeException e) {
                    LOG.error("getProductDeliveryAndPackgingType", e);
                    throw e;
                }
            }
        };
        return GenericCommand.controlledExecute(request, execution, "packagedeliverytypegroup", "getProductDeliveryAndPackgingType");

    }

    @RequestMapping(value = "getDeliveryType", produces = "application/sd-service")
    @ResponseBody
    public GetProductDeliveryTypeResponse getProductDeliveryType(@RequestBody GetProductDeliveryTypeRequest request) {
        Execution<GetProductDeliveryTypeRequest, GetProductDeliveryTypeResponse> execution = new Execution<GetProductDeliveryTypeRequest, GetProductDeliveryTypeResponse>() {

            @Override
            public GetProductDeliveryTypeResponse execute(GetProductDeliveryTypeRequest request) {
                try {
                    GetProductDeliveryTypeResponse response = cocoFSLandingService.getProductDeliveryType(request);
                    return response;
                } catch (RuntimeException e) {
                    LOG.error("getProductDeliveryType", e);
                    throw e;
                }

            }
        };
        return GenericCommand.controlledExecute(request, execution, "packagedeliverytypegroup", "getProductDeliveryType");

    }

    @RequestMapping(value = "getPackagingType", produces = "application/sd-service")
    @ResponseBody
    public GetProductPackagingTypeResponse getProductPackagingType(@RequestBody GetProductPackagingTypeRequest request) {
        Execution<GetProductPackagingTypeRequest, GetProductPackagingTypeResponse> execution = new Execution<GetProductPackagingTypeRequest, GetProductPackagingTypeResponse>() {

            @Override
            public GetProductPackagingTypeResponse execute(GetProductPackagingTypeRequest request) {
                try {
                    GetProductPackagingTypeResponse response = cocoFSLandingService.getProductPackagingType(request);
                    return response;
                } catch (RuntimeException e) {
                    LOG.error("getProductPackagingType", e);
                    throw e;
                }
            }
        };
        return GenericCommand.controlledExecute(request, execution, "packagedeliverytypegroup", "getProductPackagingType");

    }

    @RequestMapping(value = "getFulfilmentFeeParams", produces = "application/sd-service")
    @ResponseBody
    public GetFulFilmentFeeParamResponse getFulfilmentFeeParams(@RequestBody GetFulfilmentFeeParamRequest request) {
        Execution<GetFulfilmentFeeParamRequest, GetFulFilmentFeeParamResponse> execution = new Execution<GetFulfilmentFeeParamRequest, GetFulFilmentFeeParamResponse>() {

            @Override
            public GetFulFilmentFeeParamResponse execute(GetFulfilmentFeeParamRequest request) {
                try {
                    GetFulFilmentFeeParamResponse response = cocoFSLandingService.getFulfillmentFeeParams(request);
                    return response;
                } catch (RuntimeException e) {
                    LOG.error("getFulfilmentFeeParams", e);
                    throw e;
                }

            }
        };
        return GenericCommand.controlledExecute(request, execution, "fulfilmentfeeparamsgroup", "getFulfilmentFeeParams");
    }

    @RequestMapping(value = "/v2/getFulfilmentFeeParams", produces = "application/sd-service")
    @ResponseBody
    public com.snapdeal.cocofs.model.response.v2.GetFulFilmentFeeParamResponse getFulfilmentFeeParams(
            @RequestBody com.snapdeal.cocofs.model.request.v2.GetFulfilmentFeeParamRequest request) {
        Execution<com.snapdeal.cocofs.model.request.v2.GetFulfilmentFeeParamRequest, com.snapdeal.cocofs.model.response.v2.GetFulFilmentFeeParamResponse> execution = new Execution<com.snapdeal.cocofs.model.request.v2.GetFulfilmentFeeParamRequest, com.snapdeal.cocofs.model.response.v2.GetFulFilmentFeeParamResponse>() {

            @Override
            public com.snapdeal.cocofs.model.response.v2.GetFulFilmentFeeParamResponse execute(com.snapdeal.cocofs.model.request.v2.GetFulfilmentFeeParamRequest request) {
                try {
                    com.snapdeal.cocofs.model.response.v2.GetFulFilmentFeeParamResponse response = cocoFSLandingService.getFulfillmentFeeParams(request);
                    return response;
                } catch (RuntimeException e) {
                    LOG.error("getFulfilmentFeeParamsV2", e);
                    throw e;
                }

            }
        };
        return GenericCommand.controlledExecute(request, execution, "fulfilmentfeeparamsgroupV2", "getFulfilmentFeeParamsV2");
    }

    @RequestMapping(value = "getFulfilmentModel", produces = "application/sd-service")
    @ResponseBody
    public GetFulfilmentModelResponse getFulfilmentModel(@RequestBody GetFulfilmentModelRequest request) {
        Execution<GetFulfilmentModelRequest, GetFulfilmentModelResponse> execution = new Execution<GetFulfilmentModelRequest, GetFulfilmentModelResponse>() {

            @Override
            public GetFulfilmentModelResponse execute(GetFulfilmentModelRequest request) {
                try {
                    GetFulfilmentModelResponse response = cocofsFMLandingService.getFulfilmentModel(request);
                    return response;
                } catch (RuntimeException e) {
                    LOG.error("getFulfilmentModel", e);
                    throw e;
                }
            }
        };
        return GenericCommand.controlledExecute(request, execution, "fulfillmentmodelgroup", "getFulfilmentModel");
    }

    @RequestMapping(value = "getProductWeightNotifications", produces = "application/sd-service")
    @ResponseBody
    public GetProductWeightNotificationResponse getProductWeightNotifications(@RequestBody GetProductWeightNotificationRequest request) {
        Execution<GetProductWeightNotificationRequest, GetProductWeightNotificationResponse> execution = new Execution<GetProductWeightNotificationRequest, GetProductWeightNotificationResponse>() {

            @Override
            public GetProductWeightNotificationResponse execute(GetProductWeightNotificationRequest request) {
                try {
                    GetProductWeightNotificationResponse response = cocoFSLandingService.getProductWeightNotifications(request);
                    return response;
                } catch (RuntimeException e) {
                    LOG.error("getProductWeightNotifications", e);
                    throw e;
                }
            }
        };
        return GenericCommand.controlledExecute(request, execution, "getProductWeightNotifications", "getProductWeightNotifications");

    }

    @RequestMapping(value = "getFulfilmentModelAndWeight", produces = "application/sd-service")
    @ResponseBody
    public GetFulfilmentModelAndWeightResponse getFulfilmentModelAndWeight(@RequestBody GetFulfilmentModelAndWeightRequest request) {
        Execution<GetFulfilmentModelAndWeightRequest, GetFulfilmentModelAndWeightResponse> execution = new Execution<GetFulfilmentModelAndWeightRequest, GetFulfilmentModelAndWeightResponse>() {

            @Override
            public GetFulfilmentModelAndWeightResponse execute(GetFulfilmentModelAndWeightRequest request) {
                try {
                    GetFulfilmentModelAndWeightResponse response = cocoFSLandingService.getFulfilmentModelAndWeight(request);
                    return response;
                } catch (RuntimeException e) {
                    LOG.error("getFulfilmentModelAndWeight", e);
                    throw e;
                }
            }
        };
        return GenericCommand.controlledExecute(request, execution, "fulfillmentmodelandweightgroup", "getFulfilmentModelAndWeight");

    }

    @RequestMapping(value = "getFulfilmentModelBySellerSupcSubcat", produces = "application/sd-service")
    @ResponseBody
    public GetFulfilmentModelBySellerSupcSubcatResponse getFulfilmentModelBySellerSupcSubcat(@RequestBody GetFulfilmentModelBySellerSupcSubcatRequest request) {
        Execution<GetFulfilmentModelBySellerSupcSubcatRequest, GetFulfilmentModelBySellerSupcSubcatResponse> execution = new Execution<GetFulfilmentModelBySellerSupcSubcatRequest, GetFulfilmentModelBySellerSupcSubcatResponse>() {

            @Override
            public GetFulfilmentModelBySellerSupcSubcatResponse execute(GetFulfilmentModelBySellerSupcSubcatRequest request) {
                try {
                    return cocofsFMLandingService.getFulfilmentModelBySellerSupcSubcat(request);
                } catch (RuntimeException e) {
                    LOG.error("getFulfilmentModelBySellerSupcSubcat", e);
                    throw e;
                }
            }
        };
        return GenericCommand.controlledExecute(request, execution, "getFulfilmentModelBySellersAndSupc", "getFulfilmentModelBySellersAndSupc");
    }

    @RequestMapping(value = "getFMMappingAtSellerAndSellerSubcat", produces = "application/sd-service")
    @ResponseBody
    public GetFMMappingAtSellerAndSellerSubcatResponse getFMMappingAtSellerAndSellerSubcat(@RequestBody GetFMMappingAtSellerAndSellerSubcatRequest request) {
        Execution<GetFMMappingAtSellerAndSellerSubcatRequest, GetFMMappingAtSellerAndSellerSubcatResponse> execution = new Execution<GetFMMappingAtSellerAndSellerSubcatRequest, GetFMMappingAtSellerAndSellerSubcatResponse>() {

            @Override
            public GetFMMappingAtSellerAndSellerSubcatResponse execute(GetFMMappingAtSellerAndSellerSubcatRequest request) {
                try {
                    return cocofsFMLandingService.getFMMappingAtSellerAndSellerSubcat(request);
                } catch (RuntimeException e) {
                    LOG.error("getFMMappingAtSellerAndSellerSubcat", e);
                    throw e;
                }
            }
        };
        return GenericCommand.controlledExecute(request, execution, "getFMMappingAtSellerAndSellerSubcat", "getFMMappingAtSellerAndSellerSubcat");

    }

    @RequestMapping(value = "setFMMapping", produces = "application/sd-service")
    @ResponseBody
    public SetFMMappingResponse setFMMapping(@RequestBody SetFMMappingRequest request) {
        Execution<SetFMMappingRequest, SetFMMappingResponse> execution = new Execution<SetFMMappingRequest, SetFMMappingResponse>() {

            @Override
            public SetFMMappingResponse execute(SetFMMappingRequest request) {
                try {
                    return cocofsFMLandingService.setFMMapping(request);
                } catch (RuntimeException e) {
                    LOG.error("setFMMapping", e);
                    throw e;
                }
            }
        };
        return GenericCommand.controlledExecute(request, execution, "setFMMapping", "setFMMapping");
    }

    @RequestMapping(value = "getFulfilmentModelAndProductAttributes", produces = "application/sd-service")
    @ResponseBody
    public GetFulfilmentModelAndProductAttributesResponse getFulfilmentModelAndProductAttributes(@RequestBody GetFulfilmentModelAndProductAttributesRequest request) {
        Execution<GetFulfilmentModelAndProductAttributesRequest, GetFulfilmentModelAndProductAttributesResponse> execution = new Execution<GetFulfilmentModelAndProductAttributesRequest, GetFulfilmentModelAndProductAttributesResponse>() {

            @Override
            public GetFulfilmentModelAndProductAttributesResponse execute(GetFulfilmentModelAndProductAttributesRequest request) {
                try {
                    GetFulfilmentModelAndProductAttributesResponse response = cocoFSLandingService.getFulfilmentModelAndProductAttributes(request);
                    return response;
                } catch (RuntimeException e) {
                    LOG.error("getFulfilmentModelAndProductAttributes", e);
                    throw e;
                }
            }
        };
        return GenericCommand.controlledExecute(request, execution, "getFulfilmentModelAndProductAttributesgroup", "getFulfilmentModelAndProductAttributes");

    }

    @RequestMapping(value = "getFulfilmentModelsForSeller", produces = "application/sd-service")
    @ResponseBody
    public GetFulfilmentModelsForSellerResponse getFulfilmentModelsForSeller(@RequestBody GetFulfilmentModelsForSellerRequest request) {
        Execution<GetFulfilmentModelsForSellerRequest, GetFulfilmentModelsForSellerResponse> execution = new Execution<GetFulfilmentModelsForSellerRequest, GetFulfilmentModelsForSellerResponse>() {

            @Override
            public GetFulfilmentModelsForSellerResponse execute(GetFulfilmentModelsForSellerRequest request) {
                try {
                    GetFulfilmentModelsForSellerResponse response = cocofsFMLandingService.getFulfilmentModelsForSeller(request);
                    return response;
                } catch (RuntimeException e) {
                    LOG.error("getFulfilmentModelsForSeller", e);
                    throw e;
                }
            }
        };
        return GenericCommand.controlledExecute(request, execution, "getFulfilmentModelsForSeller", "getFulfilmentModelsForSeller");

    }

    @RequestMapping(value = "getAllCategoryModeMapping", produces = "application/sd-service")
    @ResponseBody
    public GetAllCategoryModeMappingResponse getAllCategoryModeMapping(@RequestBody GetAllCategoryModeMappingRequest request) {
        Execution<GetAllCategoryModeMappingRequest, GetAllCategoryModeMappingResponse> execution = new Execution<GetAllCategoryModeMappingRequest, GetAllCategoryModeMappingResponse>() {

            @Override
            public GetAllCategoryModeMappingResponse execute(GetAllCategoryModeMappingRequest request) {
                try {
                    GetAllCategoryModeMappingResponse response = cocoFSLandingService.getAllCategoryModeMapping(request);
                    return response;
                } catch (RuntimeException e) {
                    LOG.error("getAllCategoryModeMapping", e);
                    throw e;
                }

            }
        };
        return GenericCommand.controlledExecute(request, execution, "getAllCategoryModeMappinggroup", "getAllCategoryModeMapping");
    }

    @RequestMapping(value = "getAllSupcModeMapping", produces = "application/sd-service")
    @ResponseBody
    public GetAllSupcModeMappingResponse getAllSupcModeMapping(@RequestBody GetAllSupcModeMappingRequest request) {
        Execution<GetAllSupcModeMappingRequest, GetAllSupcModeMappingResponse> execution = new Execution<GetAllSupcModeMappingRequest, GetAllSupcModeMappingResponse>() {

            @Override
            public GetAllSupcModeMappingResponse execute(GetAllSupcModeMappingRequest request) {
                try {
                    GetAllSupcModeMappingResponse response = cocoFSLandingService.getAllSupcModeMapping(request);
                    return response;
                } catch (RuntimeException e) {
                    LOG.error("getAllSupcModeMapping", e);
                    throw e;
                }

            }
        };
        return GenericCommand.controlledExecute(request, execution, "getAllSupcModeMappinggroup", "getAllSupcModeMapping");
    }

    @RequestMapping(value = "getCategoryModeMapping", produces = "application/sd-service")
    @ResponseBody
    public GetCategoryModeMappingResponse getCategoryModeMapping(@RequestBody GetCategoryModeMappingRequest request) {
        Execution<GetCategoryModeMappingRequest, GetCategoryModeMappingResponse> execution = new Execution<GetCategoryModeMappingRequest, GetCategoryModeMappingResponse>() {

            @Override
            public GetCategoryModeMappingResponse execute(GetCategoryModeMappingRequest request) {
                try {
                    GetCategoryModeMappingResponse response = cocoFSLandingService.getCategoryModeMapping(request);
                    return response;
                } catch (RuntimeException e) {
                    LOG.error("getCategoryModeMapping", e);
                    throw e;
                }

            }
        };
        return GenericCommand.controlledExecute(request, execution, "getCategoryModeMappinggroup", "getCategoryModeMapping");
    }

    @RequestMapping(value = "getSupcModeMapping", produces = "application/sd-service")
    @ResponseBody
    public GetSupcModeMappingResponse getSupcModeMapping(@RequestBody GetSupcModeMappingRequest request) {
        Execution<GetSupcModeMappingRequest, GetSupcModeMappingResponse> execution = new Execution<GetSupcModeMappingRequest, GetSupcModeMappingResponse>() {

            @Override
            public GetSupcModeMappingResponse execute(GetSupcModeMappingRequest request) {
                try {
                    GetSupcModeMappingResponse response = cocoFSLandingService.getSupcModeMapping(request);
                    return response;
                } catch (RuntimeException e) {
                    LOG.error("getSupcModeMapping", e);
                    throw e;
                }

            }
        };
        return GenericCommand.controlledExecute(request, execution, "getSupcModeMappinggroup", "getSupcModeMapping");
    }

    @RequestMapping(value = "addOrUpdateCategoryModeMapping", produces = "application/sd-service")
    @ResponseBody
    public AddOrUpdateCategoryModeMappingResponse addOrUpdateCategoryModeMapping(@RequestBody AddOrUpdateCategoryModeMappingRequest request) {
        Execution<AddOrUpdateCategoryModeMappingRequest, AddOrUpdateCategoryModeMappingResponse> execution = new Execution<AddOrUpdateCategoryModeMappingRequest, AddOrUpdateCategoryModeMappingResponse>() {

            @Override
            public AddOrUpdateCategoryModeMappingResponse execute(AddOrUpdateCategoryModeMappingRequest request) {
                try {
                    AddOrUpdateCategoryModeMappingResponse response = cocoFSLandingService.addOrUpdateCategoryModeMapping(request);
                    return response;
                } catch (RuntimeException e) {
                    LOG.error("addOrUpdateCategoryModeMapping", e);
                    throw e;
                }

            }
        };
        return GenericCommand.controlledExecute(request, execution, "addOrUpdateCategoryModeMappinggroup", "addOrUpdateCategoryModeMapping");
    }

    @RequestMapping(value = "addOrUpdateSupcModeMapping", produces = "application/sd-service")
    @ResponseBody
    public AddOrUpdateSupcModeMappingResponse addOrUpdateSupcModeMapping(@RequestBody AddOrUpdateSupcModeMappingRequest request) {
        Execution<AddOrUpdateSupcModeMappingRequest, AddOrUpdateSupcModeMappingResponse> execution = new Execution<AddOrUpdateSupcModeMappingRequest, AddOrUpdateSupcModeMappingResponse>() {

            @Override
            public AddOrUpdateSupcModeMappingResponse execute(AddOrUpdateSupcModeMappingRequest request) {
                try {
                    AddOrUpdateSupcModeMappingResponse response = cocoFSLandingService.addOrUpdateSupcModeMapping(request);
                    return response;
                } catch (RuntimeException e) {
                    LOG.error("addOrUpdateSupcModeMapping", e);
                    throw e;
                }

            }
        };
        return GenericCommand.controlledExecute(request, execution, "addOrUpdateSupcModeMappinggroup", "addOrUpdateSupcModeMapping");
    }

    @RequestMapping(value = "removeCategoryModeMapping", produces = "application/sd-service")
    @ResponseBody
    public RemoveCategoryModeMappingResponse removeCategoryModeMapping(@RequestBody RemoveCategoryModeMappingRequest request) {
        Execution<RemoveCategoryModeMappingRequest, RemoveCategoryModeMappingResponse> execution = new Execution<RemoveCategoryModeMappingRequest, RemoveCategoryModeMappingResponse>() {

            @Override
            public RemoveCategoryModeMappingResponse execute(RemoveCategoryModeMappingRequest request) {
                try {
                    RemoveCategoryModeMappingResponse response = cocoFSLandingService.removeCategoryModeMapping(request);
                    return response;
                } catch (RuntimeException e) {
                    LOG.error("removeCategoryModeMapping", e);
                    throw e;
                }

            }
        };
        return GenericCommand.controlledExecute(request, execution, "removeCategoryModeMappinggroup", "removeCategoryModeMapping");
    }

    @RequestMapping(value = "removeSupcModeMapping", produces = "application/sd-service")
    @ResponseBody
    public RemoveSupcModeMappingResponse removeSupcModeMapping(@RequestBody RemoveSupcModeMappingRequest request) {
        Execution<RemoveSupcModeMappingRequest, RemoveSupcModeMappingResponse> execution = new Execution<RemoveSupcModeMappingRequest, RemoveSupcModeMappingResponse>() {

            @Override
            public RemoveSupcModeMappingResponse execute(RemoveSupcModeMappingRequest request) {
                try {
                    RemoveSupcModeMappingResponse response = cocoFSLandingService.removeSupcModeMapping(request);
                    return response;
                } catch (RuntimeException e) {
                    LOG.error("removeSupcModeMapping", e);
                    throw e;
                }

            }
        };
        return GenericCommand.controlledExecute(request, execution, "removeSupcModeMappinggroup", "removeSupcModeMapping");
    }

    /**
     * Set system captured weight. Also explicitly set systemWeightCaptured flag to true, this suggests that the weight
     * is captured by actual weighing machine.
     * 
     * @param request
     * @return
     */
    @RequestMapping(value = "setProductCapturedWeight", produces = "application/sd-service")
    @ResponseBody
    public SetProductCapturedWeightResponse setProductCapturedWeight(@RequestBody SetProductCapturedWeightRequest request) {
        Execution<SetProductCapturedWeightRequest, SetProductCapturedWeightResponse> execution = new Execution<SetProductCapturedWeightRequest, SetProductCapturedWeightResponse>() {

            @Override
            public SetProductCapturedWeightResponse execute(SetProductCapturedWeightRequest request) {
                try {
                    SetProductCapturedWeightResponse response = cocoFSLandingService.setProductCapturedWeight(request);
                    return response;
                } catch (RuntimeException e) {
                    LOG.error("SetProductCapturedWeight", e);
                    throw e;
                }
            }

        };
        return GenericCommand.controlledExecute(request, execution, "supccreationgroup", "setProductCapturedWeight");
    }

    /**
     * get systemCapturedWeight flag which indicates if the weight is system captured/admin captured.
     * 
     * @param request
     * @return
     */
    @RequestMapping(value = "getProductSystemWeightCapturedFlag", produces = "application/sd-service")
    @ResponseBody
    public GetProductSystemWeightCapturedFlagResponse getProductSystemWeightCapturedFlag(@RequestBody GetProductSystemWeightCapturedFlagRequest request) {
        Execution<GetProductSystemWeightCapturedFlagRequest, GetProductSystemWeightCapturedFlagResponse> execution = new Execution<GetProductSystemWeightCapturedFlagRequest, GetProductSystemWeightCapturedFlagResponse>() {

            @Override
            public GetProductSystemWeightCapturedFlagResponse execute(GetProductSystemWeightCapturedFlagRequest request) {
                try {
                    GetProductSystemWeightCapturedFlagResponse response = cocoFSLandingService.getProductSystemWeightCapturedFlag(request);
                    return response;
                } catch (RuntimeException e) {
                    LOG.error("getProductSystemWeightCapturedFlag", e);
                    throw e;
                }
            }

        };
        return GenericCommand.controlledExecute(request, execution, "getProductSystemWeightCapturedFlag", "getProductSystemWeightCapturedFlag");
    }

    /**
     * Returns all possible values of dangerous Goods Type
     * 
     * @param request
     * @return
     * @throws TransportException
     */
    @RequestMapping(value = "getAllDangerousGoodsType", produces = "application/sd-service")
    @ResponseBody
    public GetAllDangerousGoodsTypeResponse getAllDangerousGoodsType(@RequestBody GetAllDangerousGoodsTypeRequest request) throws TransportException {
        GetAllDangerousGoodsTypeResponse resp = cocoFSLandingService.getAllDangerousGoodsType();
        resp.setProtocol(request.getResponseProtocol());
        return resp;
    }

    @RequestMapping(value = "setSellerSubcatFMMapping", produces = "application/sd-service")
    @ResponseBody
    public SetSellerSubcatFMMappingResponse setSellerSubcatFMMapping(@RequestBody SetSellerSubcatFMMappingRequest request) {
        Execution<SetSellerSubcatFMMappingRequest, SetSellerSubcatFMMappingResponse> execution = new Execution<SetSellerSubcatFMMappingRequest, SetSellerSubcatFMMappingResponse>() {

            @Override
            public SetSellerSubcatFMMappingResponse execute(SetSellerSubcatFMMappingRequest request) {
                try {
                    return cocofsFMLandingService.setSellerSubcatFMMapping(request);
                } catch (RuntimeException e) {
                    LOG.error("setSellerSubcatFMMapping", e);
                    throw e;
                }
            }
        };
        return GenericCommand.controlledExecute(request, execution, "setSellerSubcatFMMapping", "setSellerSubcatFMMapping");
    }

    @RequestMapping(value = "setSellerFMMapping", produces = "application/sd-service")
    @ResponseBody
    public SetSellerFMMappingResponse setSellerFMMapping(@RequestBody SetSellerFMMappingRequest request) {
        Execution<SetSellerFMMappingRequest, SetSellerFMMappingResponse> execution = new Execution<SetSellerFMMappingRequest, SetSellerFMMappingResponse>() {

            @Override
            public SetSellerFMMappingResponse execute(SetSellerFMMappingRequest request) {
                try {
                    return cocofsFMLandingService.setSellerFMMapping(request);
                } catch (RuntimeException e) {
                    LOG.error("setSellerFMMapping", e);
                    throw e;
                }
            }
        };
        return GenericCommand.controlledExecute(request, execution, "setSellerFMMapping", "setSellerFMMapping");
    }

    @RequestMapping(value = "getSellerSubcatFMMapping", produces = "application/sd-service")
    @ResponseBody
    public GetSellerSubcatFMMappingResponse getSellerSubcatFMMapping(@RequestBody GetSellerSubcatFMMappingRequest request) {
        Execution<GetSellerSubcatFMMappingRequest, GetSellerSubcatFMMappingResponse> execution = new Execution<GetSellerSubcatFMMappingRequest, GetSellerSubcatFMMappingResponse>() {

            @Override
            public GetSellerSubcatFMMappingResponse execute(GetSellerSubcatFMMappingRequest request) {
                try {
                    return cocofsFMLandingService.getSellerSubcatFMMapping(request);
                } catch (RuntimeException e) {
                    LOG.error("getSellerSubcatFMMapping", e);
                    throw e;
                }
            }
        };
        return GenericCommand.controlledExecute(request, execution, "getSellerSubcatFMMapping", "getSellerSubcatFMMapping");

    }

    /**
     * SNAPDEALTECH-26120 Business/Logic layer - Tax Rate Retrieval for Sellercode/SUPC/State and price
     * 
     * @param request
     * @return
     */
    @RequestMapping(value = "getTaxRateBySellerSUPCState", produces = "application/sd-service")
    @ResponseBody
    public GetTaxRateInfoBySUPCStateSellerPriceResponse getTaxRateBySellerSUPCState(@RequestBody GetTaxRateInfoBySUPCStateSellerPriceRequest request) {
        Execution<GetTaxRateInfoBySUPCStateSellerPriceRequest, GetTaxRateInfoBySUPCStateSellerPriceResponse> execution = new Execution<GetTaxRateInfoBySUPCStateSellerPriceRequest, GetTaxRateInfoBySUPCStateSellerPriceResponse>() {

            @Override
            public GetTaxRateInfoBySUPCStateSellerPriceResponse execute(GetTaxRateInfoBySUPCStateSellerPriceRequest request) {
                try {
                    LOG.info("Started processing request to retrieve tax rate info for request: " + request);
                    GetTaxRateInfoBySUPCStateSellerPriceResponse response = cocofsTaxLandingService.getTaxRateBySellerSUPCState(request);
                    response.setProtocol(request.getResponseProtocol());
                    LOG.info("Response formed: " + response);
                    return response;
                } catch (RuntimeException e) {
                    LOG.error("getTaxRateBySellerSUPCState", e);
                    throw e;
                }
            }
        };
        return GenericCommand.controlledExecute(request, execution, "getTaxRateBySellerSUPCState", "getTaxRateBySellerSUPCState");

    }
    
    @RequestMapping(value = "getTaxInfoBySellerSUPCState", produces = "application/sd-service")
    @ResponseBody
    public GetTaxInfoResponse getTaxInfoBySellerSUPCState(@RequestBody GetTaxInfoRequest request) {
        Execution<GetTaxInfoRequest, GetTaxInfoResponse> execution = new Execution<GetTaxInfoRequest, GetTaxInfoResponse>() {

            @Override
            public GetTaxInfoResponse execute(GetTaxInfoRequest request) {
                try {
                    LOG.info("Started processing request to retrieve tax rate info for request: " + request);
                    GetTaxInfoResponse response = cocofsTaxLandingService.getTaxInfoBySellerSUPCState(request);
                    response.setProtocol(request.getResponseProtocol());
                    LOG.info("Response formed: " + response);
                    return response;
                } catch (RuntimeException e) {
                    LOG.error("getTaxInfoBySellerSUPCState", e);
                    throw e;
                }
            }
        };
        return GenericCommand.controlledExecute(request, execution, "getTaxInfoBySellerSUPCState", "getTaxInfoBySellerSUPCState");

    }

    /**
     * SNAPDEALTECH-28817 - Get SD fulfilled status for a list of supplied SUPCs+seller+subcat combinations If even one
     * of the combination has FC_VOI as the fulfilment model => send true
     * 
     * @param request
     * @return
     */
    @RequestMapping(value = "getSDFulfilledStatusBySupcSellersSubcat", produces = "application/sd-service")
    @ResponseBody
    public GetSDFulfilledStatusBySupcSellersSubcatResponse getSDFulfilledStatusBySupcSellersSubcat(@RequestBody GetSDFulfilledStatusBySupcSellersSubcatRequest request) {
        Execution<GetSDFulfilledStatusBySupcSellersSubcatRequest, GetSDFulfilledStatusBySupcSellersSubcatResponse> execution = new Execution<GetSDFulfilledStatusBySupcSellersSubcatRequest, GetSDFulfilledStatusBySupcSellersSubcatResponse>() {

            @Override
            public GetSDFulfilledStatusBySupcSellersSubcatResponse execute(GetSDFulfilledStatusBySupcSellersSubcatRequest request) {
                try {
                    return cocofsFMLandingService.getSDFulfilledStatusBySupcSellersSubcat(request);
                } catch (RuntimeException e) {
                    LOG.error("getSDFulfilledStatusBySupcSellersSubcat", e);
                    throw e;
                }
            }
        };
        return GenericCommand.controlledExecute(request, execution, "getSDFulfilledStatusBySupcSellersSubcat", "getSDFulfilledStatusBySupcSellersSubcat");

    }

    @RequestMapping(value = "getFMAndFCBySellerSupcList", produces = "application/sd-service")
    @ResponseBody
    public GetFMAndFCBySellerSupcListResponse getFMAndFCBySellerSupcList(@RequestBody GetFMAndFCBySellerSupcListRequest request) {
        Execution<GetFMAndFCBySellerSupcListRequest, GetFMAndFCBySellerSupcListResponse> execution = new Execution<GetFMAndFCBySellerSupcListRequest, GetFMAndFCBySellerSupcListResponse>() {

            @Override
            public GetFMAndFCBySellerSupcListResponse execute(GetFMAndFCBySellerSupcListRequest request) {
                try {
                    return cocofsFCLandingService.GetFMAndFCBySellerSupcList(request);
                } catch (RuntimeException e) {
                    LOG.error("getFMAndFCBySellerSupcList", e);
                    throw e;
                }
            }
        };
        return GenericCommand.controlledExecute(request, execution, "getFMAndFCBySellerSupcList", "getFMAndFCBySellerSupcList");

    }

    @RequestMapping(value = "getAllCenterTypes", produces = "application/sd-service")
    @ResponseBody
    public AllCenterTypesResponse getAllCenterTypes(@RequestBody GetAllCenterTypesRequest request) {
        Execution<GetAllCenterTypesRequest, AllCenterTypesResponse> execution = new Execution<GetAllCenterTypesRequest, AllCenterTypesResponse>() {

            @Override
            public AllCenterTypesResponse execute(GetAllCenterTypesRequest request) {
                try {
                    return cocoFSLandingService.getAllCenterTypes(request);
                } catch (RuntimeException e) {
                    LOG.error("getAllFCDetails", e);
                    throw e;
                }
            }
        };
        return GenericCommand.controlledExecute(request, execution, "getAllFCDetails", "getAllFCDetails");

    }

    @RequestMapping(value = "getAllFCDetails", produces = "application/sd-service")
    @ResponseBody
    public AllFCDetailResponse getAllFCDetails(@RequestBody GetAllFCDetailRequest request) {
        Execution<GetAllFCDetailRequest, AllFCDetailResponse> execution = new Execution<GetAllFCDetailRequest, AllFCDetailResponse>() {

            @Override
            public AllFCDetailResponse execute(GetAllFCDetailRequest request) {
                try {
                    return cocoFSLandingService.getAllFCDetails(request);
                } catch (RuntimeException e) {
                    LOG.error("getAllFCDetails", e);
                    throw e;
                }
            }
        };
        return GenericCommand.controlledExecute(request, execution, "getAllFCDetails", "getAllFCDetails");

    }

    @RequestMapping(value = "getAllSellersForFM", produces = "application/sd-service")
    @ResponseBody
    public GetAllSellersForFMResponse getAllSellersForFM(@RequestBody GetAllSellersForFMRequest request) {
        Execution<GetAllSellersForFMRequest, GetAllSellersForFMResponse> execution = new Execution<GetAllSellersForFMRequest, GetAllSellersForFMResponse>() {

            @Override
            public GetAllSellersForFMResponse execute(GetAllSellersForFMRequest request) {
                try {
                    return cocofsFMLandingService.getAllSellerForFM(request);
                } catch (RuntimeException e) {
                    LOG.error("getAllSellerForFM", e);
                    throw e;
                }
            }
        };
        return GenericCommand.controlledExecute(request, execution, "getAllSellerForFM", "getAllSellerForFM");

    }

    @RequestMapping(value = "createOrUpdateSeller", produces = "application/sd-service")
    @ResponseBody
    public SetSellerDetailsResponse setSellerDetails(@RequestBody SetSellerDetailsRequest request) {
        Execution<SetSellerDetailsRequest, SetSellerDetailsResponse> execution = new Execution<SetSellerDetailsRequest, SetSellerDetailsResponse>() {

            @Override
            public SetSellerDetailsResponse execute(SetSellerDetailsRequest request) {
                try {
                    return cocofsFMLandingService.setSellerDetails(request);
                } catch (RuntimeException e) {
                    LOG.error("createOrUpdateSeller", e);
                    throw e;
                }
            }
        };
        return GenericCommand.controlledExecute(request, execution, "createOrUpdateSeller", "createOrUpdateSeller");
    }

    @RequestMapping(value = "getZonesBySellerSupcList", produces = "application/sd-service")
    @ResponseBody
    public GetZonesBySellerSupcListResponse getZonesBySellerSupcList(@RequestBody GetZonesBySellerSupcListRequest request) {
        Execution<GetZonesBySellerSupcListRequest, GetZonesBySellerSupcListResponse> execution = new Execution<GetZonesBySellerSupcListRequest, GetZonesBySellerSupcListResponse>() {

            @Override
            public GetZonesBySellerSupcListResponse execute(GetZonesBySellerSupcListRequest request) {
                try {
                    return cocofsZoneLandingService.getZonesBySellerSupcList(request);
                } catch (RuntimeException e) {
                    LOG.error("getZonesBySellerSupcList", e);
                    throw e;
                }
            }
        };
        return GenericCommand.controlledExecute(request, execution, "getZonesBySellerSupcList", "getZonesBySellerSupcList");
    }

    @RequestMapping(value = "getFMAndZonesBySellerSupcList", produces = "application/sd-service")
    @ResponseBody
    public GetFMAndZonesBySellerSupcListResponse getFMAndZonesBySellerSupcList(@RequestBody GetFMAndZonesBySellerSupcListRequest request) {
        Execution<GetFMAndZonesBySellerSupcListRequest, GetFMAndZonesBySellerSupcListResponse> execution = new Execution<GetFMAndZonesBySellerSupcListRequest, GetFMAndZonesBySellerSupcListResponse>() {

            @Override
            public GetFMAndZonesBySellerSupcListResponse execute(GetFMAndZonesBySellerSupcListRequest request) {
                try {
                    return cocofsZoneLandingService.getFMAndZonesBySellerSupcList(request);
                } catch (RuntimeException e) {
                    LOG.error("getFMAndZonesBySellerSupcList", e);
                    throw e;
                }
            }
        };
        return GenericCommand.controlledExecute(request, execution, "getFMAndZonesBySellerSupcList", "getFMAndZonesBySellerSupcList");
    }

    @RequestMapping(value = "getVolumetricWeightFormulaParams", produces = "application/sd-service")
    @ResponseBody
    public GetVolumetricWeightFormulaParamsResponse getVolumetricWeightFormulaParams(@RequestBody GetVolumetricWeightFormulaParamsRequest request) {
        Execution<GetVolumetricWeightFormulaParamsRequest, GetVolumetricWeightFormulaParamsResponse> execution = new Execution<GetVolumetricWeightFormulaParamsRequest, GetVolumetricWeightFormulaParamsResponse>() {

            @Override
            public GetVolumetricWeightFormulaParamsResponse execute(GetVolumetricWeightFormulaParamsRequest request) {
                try {
                    return cocoFSLandingService.getVolumetricWeightFormulaParams(request);
                } catch (RuntimeException e) {
                    LOG.error("getVolumetricWeightFormulaParams", e);
                    throw e;
                }
            }
        };
        return GenericCommand.controlledExecute(request, execution, "getVolumetricWeightFormulaParams", "getVolumetricWeightFormulaParams");
    }

    @RequestMapping(value = "validateSellerCodes", produces = "application/sd-service")
    @ResponseBody
    public ValidateSellerCodeResponse validateSellerCodes(@RequestBody ValidateSellerCodeRequest request) {
        Execution<ValidateSellerCodeRequest, ValidateSellerCodeResponse> execution = new Execution<ValidateSellerCodeRequest, ValidateSellerCodeResponse>() {

            @Override
            public ValidateSellerCodeResponse execute(ValidateSellerCodeRequest request) {
                try {
                    return cocofsFMLandingService.validateSellers(request);
                } catch (RuntimeException e) {
                    LOG.error("validateSellerCodes", e);
                    throw e;
                }
            }
        };
        return GenericCommand.controlledExecute(request, execution, "validateSellerCode", "validateSellerCode");
    }
    
    @RequestMapping(value = "getMultiplierForPerimeter", produces = "application/sd-service")
    @ResponseBody
    public GetMultiplierForPerimeterResponse getMultiplierForPerimeter(@RequestBody GetMultiplierForPerimeterRequest request) {
        Execution<GetMultiplierForPerimeterRequest, GetMultiplierForPerimeterResponse> execution = new Execution<GetMultiplierForPerimeterRequest, GetMultiplierForPerimeterResponse>() {

            @Override
            public GetMultiplierForPerimeterResponse execute(GetMultiplierForPerimeterRequest request) {
                try {
                    return cocoFSLandingService.getMultiplierForPerimeter(request);
                } catch (RuntimeException e) {
                    LOG.error("getMultiplierForPerimeter", e);
                    throw e;
                }
            }
        };
        return GenericCommand.controlledExecute(request, execution, "getMultiplierForPerimeter", "getMultiplierForPerimeter");
    }
    
    

    @RequestMapping(value = "getTaxClassBySupcSubcatPairList", produces = "application/sd-service")
    @ResponseBody
    public GetTaxClassResponse getTaxClassBySupcSubcatPairList(@RequestBody GetTaxClassRequest request) {
        Execution<GetTaxClassRequest, GetTaxClassResponse> execution = new Execution<GetTaxClassRequest, GetTaxClassResponse>() {

            @Override
            public GetTaxClassResponse execute(GetTaxClassRequest request) {
                try {
                    return cocofsTaxLandingService.getTaxClassBySupcSubcatPairList(request);
                } catch (RuntimeException e) {
                    LOG.error("getTaxClassBySupcSubcatPairList", e);
                    throw e;
                }
            }
        };
        return GenericCommand.controlledExecute(request, execution, "getTaxClassBySupcSubcatPairList", "getTaxClassBySupcSubcatPairList");
    }
    
    @RequestMapping(value = "getShipFromLocationBySellerSupcList", produces = "application/sd-service")
    @ResponseBody
    public GetShipFromLocationBySellerSupcListResponse getShipFromLocationBySellerSupcListRequest(@RequestBody GetShipFromLocationBySellerSupcListRequest request) {
        Execution<GetShipFromLocationBySellerSupcListRequest, GetShipFromLocationBySellerSupcListResponse> execution = new Execution<GetShipFromLocationBySellerSupcListRequest, GetShipFromLocationBySellerSupcListResponse>() {

            @Override
            public GetShipFromLocationBySellerSupcListResponse execute(GetShipFromLocationBySellerSupcListRequest request) {
                try {
                    return cocofsFCLandingService.getShipFromLocation(request);
                } catch (RuntimeException e) {
                    LOG.error("getFMAndZonesBySellerSupcList", e);
                    throw e;
                }
            }
        };
        return GenericCommand.controlledExecute(request, execution, "getShipFromLocationBySellerSupcList", "getShipFromLocationBySellerSupcList");
    }

    
}
