/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.snapdeal.cocofs.web.landing.layer.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.base.cache.CacheManager;
import com.snapdeal.base.validation.ValidationError;
import com.snapdeal.cocofs.core.aerospike.vo.FCDetailVO;
import com.snapdeal.cocofs.core.aerospike.vo.SellerFMMappingVO;
import com.snapdeal.cocofs.dataaccess.fc.IFCAerospikeDataReadService;
import com.snapdeal.cocofs.entity.FCAddress;
import com.snapdeal.cocofs.entity.FulfillmentCentre;
import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.model.request.GetFMAndFCBySellerSupcListRequest;
import com.snapdeal.cocofs.model.request.GetShipFromLocationBySellerSupcListRequest;
import com.snapdeal.cocofs.model.response.ApiErrorCode;
import com.snapdeal.cocofs.model.response.GetFMAndFCBySellerSupcListResponse;
import com.snapdeal.cocofs.model.response.GetShipFromLocationBySellerSupcListResponse;
import com.snapdeal.cocofs.services.ISellerDetailsService;
import com.snapdeal.cocofs.services.fc.IFCMappingService;
import com.snapdeal.cocofs.sro.AddressSRO;
import com.snapdeal.cocofs.sro.FMFCPair;
import com.snapdeal.cocofs.sro.FulfillmentModel;
import com.snapdeal.cocofs.sro.SellerSUPCPair;
import com.snapdeal.cocofs.sro.ShipFromInfoSRO;
import com.snapdeal.cocofs.web.builder.IResponseBuilder;
import com.snapdeal.cocofs.web.landing.layer.ICocofsFCLandingService;
import com.snapdeal.cocofs.web.landing.layer.IRequestValidationService;

/**
 * @version 1.0, 27-Mar-2015
 * @author ankur
 */

@Service("CocofsFCLandingServiceImpl")
public class CocofsFCLandingServiceImpl implements ICocofsFCLandingService {

    @Autowired
    private IResponseBuilder          responseBuilder;

    @Autowired
    private IRequestValidationService requestValidationService;

    @Autowired
    private IFCMappingService         fcMappingService;
    
    @Autowired
    private ISellerDetailsService     sellerDetailsService;
    
    @Autowired
    IFCAerospikeDataReadService        fcAerospikeService;
    

    private static final Logger       LOG = LoggerFactory.getLogger(CocofsFMLandingServiceImpl.class);

    @Override
    public GetFMAndFCBySellerSupcListResponse GetFMAndFCBySellerSupcList(GetFMAndFCBySellerSupcListRequest request) {
        LOG.debug("Request received {}", request);
        GetFMAndFCBySellerSupcListResponse response = new GetFMAndFCBySellerSupcListResponse();
        response.setProtocol(request.getResponseProtocol());
        ValidationError error = requestValidationService.requestBasicFieldValidation(request);
        if (error != null) {
            LOG.error("Validation error found request {}", request);
            responseBuilder.buildFailedResponse(error, response);
        } else {
            Map<SellerSUPCPair, FMFCPair> sellerSupcToFMFC = new HashMap<SellerSUPCPair, FMFCPair>();
            FMFCPair fmfcPair = null;
            int successCount = 0;
            for (SellerSUPCPair pair : request.getSellerSupcPairs()) {
                try {
                    fmfcPair  = null;
                    fmfcPair = fcMappingService.getFulfilmentCentreCodes(pair.getSellerCode(), pair.getSupc());
                } catch (ExternalDataNotFoundException e) {
                    LOG.error("Exeption in getFulfilmentModel ", e);
                }
                if (null == fmfcPair || fmfcPair.getFulfilmentModel() == null) {
                    response.addFailures(new ValidationError(ApiErrorCode.REQUESTED_DATA_DOES_NOT_EXISTS.code(), "Fulfillment model can not be determined"), pair);
                } else {
                    sellerSupcToFMFC.put(pair, fmfcPair);
                    successCount++;
                }

            }

            if (successCount > 0) {
                response.setSellerSupcToFMAndFC(sellerSupcToFMFC);
                response.setSuccessCount(successCount);
            } else {
                responseBuilder.buildFailedResponse(ApiErrorCode.REQUESTED_DATA_DOES_NOT_EXISTS, response);
            }
        }

        LOG.debug("Sending response {}", response);

        return response;
    }
    
    @Override
    public GetShipFromLocationBySellerSupcListResponse getShipFromLocation(GetShipFromLocationBySellerSupcListRequest request) {
        LOG.info("Request received {}", request);
        GetShipFromLocationBySellerSupcListResponse response = new GetShipFromLocationBySellerSupcListResponse();
        response.setProtocol(request.getResponseProtocol());
        ValidationError error = requestValidationService.requestBasicFieldValidation(request);
        if (error != null) {
            LOG.error("Validation error found request {}", request);
            responseBuilder.buildFailedResponse(error, response);
        } else {
            Map<SellerSUPCPair, ShipFromInfoSRO> sellerSupcToShipFrom = new HashMap<SellerSUPCPair, ShipFromInfoSRO>();
            AddressSRO address = null;
            FMFCPair fmfcPair = null;
            int successCount = 0;
            for (SellerSUPCPair pair : request.getSellerSupcPairs()) {
                boolean sdInstant = false;
                try {
                    fmfcPair = fcMappingService.getFulfilmentCentreCodes(pair.getSellerCode(), pair.getSupc());
                } catch (ExternalDataNotFoundException e) {
                    LOG.error("Exeption in getFulfilmentModel ", e);
                }
                
                if (hasFCEnabledFM(fmfcPair)) {
                    if (CollectionUtils.isEmpty(fmfcPair.getFcCodes())) {
                        LOG.info("Fulfillment center can not be determined ,hence marking it failure {}", pair);
                        response.addFailures(new ValidationError(ApiErrorCode.REQUESTED_DATA_DOES_NOT_EXISTS.code(), "Fulfillment center can not be determined"), pair);
                    } else {
                        String centerCode = fmfcPair.getFcCodes().get(0);
                        FCDetailVO fcVO = fcAerospikeService.getFCDetailVO(centerCode);
                        if(fcVO != null){    
                            sdInstant = fcVO.getSdinstant() ==1 ?true : false;
                            address = new AddressSRO();
                            address.setAddressLine1(fcVO.getAddressLine1());
                            address.setAddressLine2(fcVO.getAddressLine2());
                            address.setCity(fcVO.getCity()); 
                            address.setState(fcVO.getState());
                            address.setPincode(fcVO.getPincode());
                        }
                    }
                }else{
                       address = sellerDetailsService.getSellerDetails(pair.getSellerCode());
                }

                if(address != null){
                    ShipFromInfoSRO shipFromInfo = new ShipFromInfoSRO();
                    shipFromInfo.setAddress(address);
                    shipFromInfo.setWarehouseSdInstant(sdInstant);
                    sellerSupcToShipFrom.put(pair, shipFromInfo);
                    successCount++;
                }else{
                    LOG.info("No address found for seller supc {}",pair);
                }
            }

            if (successCount > 0) {
                response.setSellerSupcToShipFromInfo(sellerSupcToShipFrom);
                response.setSuccessCount(successCount);
            } else {
                responseBuilder.buildFailedResponse(ApiErrorCode.REQUESTED_DATA_DOES_NOT_EXISTS, response);
            }
        }

        LOG.info("Sending response {}", response);

        return response;
    }
    
    private boolean hasFCEnabledFM(FMFCPair fmfcPair) {
        if (null == fmfcPair || fmfcPair.getFulfilmentModel() == null) {
            return false;
        }
        FulfillmentModel fulfilmentModel = FulfillmentModel.getFulfilmentModelByCode(fmfcPair.getFulfilmentModel());
        if(fulfilmentModel!= null && fulfilmentModel.isLocationRequired()){
                return true;
        }

        return false;
    }

}
