package com.snapdeal.cocofs.web.landing.layer;

import com.snapdeal.cocofs.excelupload.ProductAttributeUploadDTO;
import com.snapdeal.cocofs.mongo.model.ProductAttributeUnit;
import com.snapdeal.cocofs.sro.ProductFulfilmentAttributeSRO;

public interface IConverterService {

    public ProductAttributeUploadDTO getProductAttributeUploadDTO(ProductFulfilmentAttributeSRO attribute);

    ProductFulfilmentAttributeSRO getProductAttributeSro(ProductAttributeUnit paUnit);
    public ProductAttributeUploadDTO getProductAttributeUploadDTO(String supc,Double weight, Boolean systemWeightCaptured);

}
