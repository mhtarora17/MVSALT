package com.snapdeal.cocofs.web.landing.layer;

import com.snapdeal.cocofs.model.request.GetTaxClassRequest;
import com.snapdeal.cocofs.model.request.GetTaxInfoRequest;
import com.snapdeal.cocofs.model.request.GetTaxRateInfoBySUPCStateSellerPriceRequest;
import com.snapdeal.cocofs.model.response.GetTaxClassResponse;
import com.snapdeal.cocofs.model.response.GetTaxInfoResponse;
import com.snapdeal.cocofs.model.response.GetTaxRateInfoBySUPCStateSellerPriceResponse;

/**
 * Landing Internal service to retrieve tax rate information
 * based on Sellercode/SUPC/State and price
 * from MongoDB
 * 
 * @author gauravg
 *
 */
public interface ICocofsTaxRateLandingService {
	
	/**
	 * Get Tax rate information by supc/state/sellercode
	 * @param request
	 * @return
	 */
	public GetTaxRateInfoBySUPCStateSellerPriceResponse getTaxRateBySellerSUPCState(GetTaxRateInfoBySUPCStateSellerPriceRequest request);

    GetTaxInfoResponse getTaxInfoBySellerSUPCState(GetTaxInfoRequest request);

    public GetTaxClassResponse getTaxClassBySupcSubcatPairList(GetTaxClassRequest request);
}
