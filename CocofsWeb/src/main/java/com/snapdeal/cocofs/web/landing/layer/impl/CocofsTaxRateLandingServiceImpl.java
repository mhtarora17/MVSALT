package com.snapdeal.cocofs.web.landing.layer.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.axis.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.base.cache.CacheManager;
import com.snapdeal.base.validation.ValidationError;
import com.snapdeal.catalog.base.model.GetMinProductInfoBySupcRequest;
import com.snapdeal.catalog.base.model.GetMinProductInfoResponse;
import com.snapdeal.cocofs.cache.StateCache;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.dataaccess.taxrate.ITaxRateMongoService;
import com.snapdeal.cocofs.entity.AbstractTaxRate;
import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.external.service.factory.IExternalServiceFactory;
import com.snapdeal.cocofs.model.request.GetTaxClassRequest;
import com.snapdeal.cocofs.model.request.GetTaxInfoRequest;
import com.snapdeal.cocofs.model.request.GetTaxRateInfoBySUPCStateSellerPriceRequest;
import com.snapdeal.cocofs.model.response.ApiErrorCode;
import com.snapdeal.cocofs.model.response.GetTaxClassResponse;
import com.snapdeal.cocofs.model.response.GetTaxInfoResponse;
import com.snapdeal.cocofs.model.response.GetTaxRateInfoBySUPCStateSellerPriceResponse;
import com.snapdeal.cocofs.mongo.model.SellerCategoryTaxRateUnit;
import com.snapdeal.cocofs.mongo.model.SellerSupcTaxRateUnit;
import com.snapdeal.cocofs.mongo.model.StateCategoryPriceTaxRateUnit;
import com.snapdeal.cocofs.services.taxclass.ITaxClassService;
import com.snapdeal.cocofs.services.taxrate.ITaxRateservice;
import com.snapdeal.cocofs.services.validator.IRequestValidator;
import com.snapdeal.cocofs.sro.SupcSubcatPair;
import com.snapdeal.cocofs.web.builder.IResponseBuilder;
import com.snapdeal.cocofs.web.landing.layer.ICocofsTaxRateLandingService;
import com.snapdeal.cocofs.web.landing.layer.IRequestValidationService;

/**
 * Cocofs tax rate landing internal service implementation
 * 
 * @author gauravg
 */
@Service("CocofsTaxRateLandingServiceImpl")
public class CocofsTaxRateLandingServiceImpl implements ICocofsTaxRateLandingService {

    @Autowired
    private ITaxRateMongoService                                           taxRateMongoService;

    @Autowired
    private ITaxClassService                                               taxClassService;

    @Autowired
    private ITaxRateservice                                                taxRateService;

    @Autowired
    private IExternalServiceFactory                                        externalServiceFactory;

    @Autowired
    private IResponseBuilder                                               responseBuilder;

    @Autowired
    private IRequestValidationService                                      requestValidationService;

    @Autowired
    private IRequestValidator<GetTaxRateInfoBySUPCStateSellerPriceRequest> requestValidator;

    private static final Logger                                            LOG = LoggerFactory.getLogger(CocofsTaxRateLandingServiceImpl.class);

    @Override
    public GetTaxRateInfoBySUPCStateSellerPriceResponse getTaxRateBySellerSUPCState(GetTaxRateInfoBySUPCStateSellerPriceRequest request) {
        LOG.info("Request received {}", request);
        ValidationError error = requestValidationService.requestBasicFieldValidation(request);
        if (error != null) {
            GetTaxRateInfoBySUPCStateSellerPriceResponse response = new GetTaxRateInfoBySUPCStateSellerPriceResponse();
            LOG.error("Validation error found request {}", request);
            responseBuilder.buildFailedResponse(error, response);
            return response;
        }

        //Extra input validations/constraints
        if (!requestValidator.validate(request)) {
            LOG.info("Request validation failed! Please check the request parameters.");
            return generateFailureResponse("Request Vaildation failed! Please check the request parameters.");
        }

        //Validation has passed. Proceed with the retrieval of tax rate from Mongo service...

        //Business logic/Rules implementation

        //RULE 1
        GetTaxRateInfoBySUPCStateSellerPriceResponse response = getTaxRateBySellerCodeAndSUPC(request);

        if (response == null) {
            //Enrich category from CAMS if category is not present in the request
            if (enrichRequest(request)) {
                //RULE 2
                response = getTaxRateBySellerCodeAndCat(request);

                if (response == null) {
                    //RULE 3
                    response = getTaxRateByCategoryStateAndPrice(request);

                }
            } else {
                //Enrichment was unsuccessful - category cannot be retrieved from CAMS
                LOG.info("Enrichment of request parameters was unsuccessful. Sending back an error response now.");
                response = generateFailureResponse("Could not get category from CAMS for this request.");
            }

            if (response == null) {
                //RULE 4
                response = getTaxRateByStateDefault(request);
            }
        }

        /*
         * If response is still null
         * this means that the system was not able to find the tax rate
         * using any of the predefined retrieval rules
         * => send failure response
         */
        if (response == null) {
            LOG.info("Unable to fetch tax rate information for this request:" + request);
            return generateFailureResponse("Unable to fetch tax rate information for this request.");
        }

        LOG.info("Sending response {}", response);
        return response;
    }

    /**
     * RULE 1: Try to fetch the tax rate based upon the seller/SUPC combination at the finest level
     * 
     * @return
     */
    private GetTaxRateInfoBySUPCStateSellerPriceResponse getTaxRateBySellerCodeAndSUPC(GetTaxRateInfoBySUPCStateSellerPriceRequest request) {
        LOG.debug("Applying RULE 1: Tax rate by Seller+SUPC...");
        SellerSupcTaxRateUnit taxRateUnitBySupc = taxRateMongoService.getSellerSupcTaxRateUnit(request.getSellerCode(), request.getSupc());
        if (taxRateUnitBySupc != null && taxRateUnitBySupc.isEnabled() && !AbstractTaxRate.Type.SERVICE_TAX.code().equals(taxRateUnitBySupc.getTaxType())) {
            LOG.debug("RULE 1 successfully executed..");
            return generateSuccessResponse(taxRateUnitBySupc.getTaxRate());
        }
        LOG.debug("Unable to fetch tax rate using RULE 1.");
        return null;

    }

    /**
     * If no tax rate information available through rule 1 RULE 2: Try to fetch the tax rate based upon the
     * seller/category combination at the next level
     * 
     * @return
     */
    private GetTaxRateInfoBySUPCStateSellerPriceResponse getTaxRateBySellerCodeAndCat(GetTaxRateInfoBySUPCStateSellerPriceRequest request) {
        LOG.debug("Applying RULE 2: Tax rate by Seller+Category...");
        SellerCategoryTaxRateUnit taxRateUnitByCat = taxRateMongoService.getSellerCategoryTaxRateUnit(request.getSellerCode(), request.getCategory());
        if (taxRateUnitByCat != null && taxRateUnitByCat.isEnabled() && !AbstractTaxRate.Type.SERVICE_TAX.code().equals(taxRateUnitByCat.getTaxType())) {

            LOG.debug("RULE 2 successfully executed..");
            return generateSuccessResponse(taxRateUnitByCat.getTaxRate());
        }
        LOG.debug("Unable to fetch tax rate using RULE 2.");
        return null;
    }

    /**
     * If no tax rate information available through rules 1 and 2 RULE 3: Try to fetch the tax rate based upon the
     * category/state/Price combination at the coarse level
     * 
     * @return
     */
    private GetTaxRateInfoBySUPCStateSellerPriceResponse getTaxRateByCategoryStateAndPrice(GetTaxRateInfoBySUPCStateSellerPriceRequest request) {
        LOG.debug("Applying RULE 3: Tax rate by Category+State+Price...");
        List<StateCategoryPriceTaxRateUnit> taxRateUnitsByPrice = taxRateMongoService.getEnabledStateCategoryPriceTaxRateUnit(request.getState(), request.getCategory());
        List<StateCategoryPriceTaxRateUnit> filteredList = filterStateCategoryPriceTaxRateUnitByTaxType(taxRateUnitsByPrice, AbstractTaxRate.Type.VAT_CST.code());

        if (filteredList != null) {
            StateCategoryPriceTaxRateUnit filteredTaxRateUnit = findTaxRateUnitByRange(request.getPrice(), filteredList);
            if (filteredTaxRateUnit != null) {
                LOG.debug("RULE 3 successfully executed..");
                return generateSuccessResponse(filteredTaxRateUnit.getTaxRate());
            }
        }
        LOG.debug("Unable to fetch tax rate using RULE 3.");
        return null;
    }

    /**
     * If no tax rate information available through rules 1,2 and 3 RULE 4: Use default tax rates defined for states in
     * configuration
     */
    private GetTaxRateInfoBySUPCStateSellerPriceResponse getTaxRateByStateDefault(GetTaxRateInfoBySUPCStateSellerPriceRequest request) {
        LOG.debug("Applying RULE 4: Tax rate by State Default...");
        Map<String, String> defaultTaxRateMap = ConfigUtils.getMap(Property.DEFAULT_TAX_RATE_FOR_ALL_STATES);

        if (CacheManager.getInstance().getCache(StateCache.class).isValidState(request.getState())) {
            String defaultTaxRateStr = "0";
            if (defaultTaxRateMap != null && defaultTaxRateMap.containsKey(request.getState())) {
                defaultTaxRateStr = defaultTaxRateMap.get(request.getState());
            } else {
                LOG.debug("Default value not found in default map : " + defaultTaxRateMap);
            }
            LOG.debug("RULE 4 successfully executed - using default tax rate as " + defaultTaxRateStr);
            return generateSuccessResponse(Double.valueOf(defaultTaxRateStr));
        }
        LOG.info("Unable to fetch tax rate using any of the RULES.");
        return null;
    }

    private List<StateCategoryPriceTaxRateUnit> filterStateCategoryPriceTaxRateUnitByTaxType(List<StateCategoryPriceTaxRateUnit> taxRateUnitsByPrice, String taxType) {
        List<StateCategoryPriceTaxRateUnit> filteredList = new ArrayList<StateCategoryPriceTaxRateUnit>();
        for (StateCategoryPriceTaxRateUnit taxRateUnit : taxRateUnitsByPrice) {
            if (StringUtils.isEmpty(taxRateUnit.getTaxType()) && AbstractTaxRate.Type.VAT_CST.code().equals(taxType)) {
                filteredList.add(taxRateUnit);
            } else if (taxType.equals(taxRateUnit.getTaxType())) {
                filteredList.add(taxRateUnit);
            }
        }

        return filteredList;
    }

    private StateCategoryPriceTaxRateUnit findTaxRateUnitByRange(Integer price, List<StateCategoryPriceTaxRateUnit> taxRateUnitsByPrice) {

        for (StateCategoryPriceTaxRateUnit taxRateUnit : taxRateUnitsByPrice) {
            double lowerLimit = taxRateUnit.getLowerPriceLimit() == null ? -1 : taxRateUnit.getLowerPriceLimit();
            double upperLimit = taxRateUnit.getUpperPriceLimit() == null ? Double.MAX_VALUE : taxRateUnit.getUpperPriceLimit();
            //Need to cast here for comparison
            double dprice = (double) price;

            LOG.debug("Checking for LowerLimit=" + lowerLimit + " UpperLimit=" + upperLimit + " Price = " + dprice);
            if (dprice >= lowerLimit && dprice <= upperLimit) {
                return taxRateUnit;
            }

        }
        return null;
    }

    /**
     * Enrich request with the category if category is empty
     * 
     * @param request
     */
    private boolean enrichRequest(GetTaxRateInfoBySUPCStateSellerPriceRequest request) {
        //If category is not provided in the request
        //fetch the information from CAMS
        if (StringUtils.isEmpty(request.getCategory())) {
            LOG.info("Category is not present in he request. Enriching the category from CAMS...");
            try {
                GetMinProductInfoResponse resp = externalServiceFactory.getCAMSExternalService().getMinProductInfo(new GetMinProductInfoBySupcRequest(request.getSupc()));
                LOG.debug("Response received from CAMS:" + resp);
                if (null != resp && resp.isSuccessful() && resp.getProductInfo() != null) {
                    LOG.debug("Setting category in the request as :" + resp.getProductInfo().getCategoryUrl());
                    request.setCategory(resp.getProductInfo().getCategoryUrl());
                } else {
                    LOG.info("Null/Unsuccessful response received from CAMS. Could not fetch category from CAMS.");
                    return false;
                }

            } catch (ExternalDataNotFoundException e) {
                LOG.error("Could not fetch category from CAMS :" + e.getMessage());
                return false;
            }

        } else {
            LOG.info("Enrichment not required. Category already present in the request.");
        }
        return true;
    }

    private GetTaxRateInfoBySUPCStateSellerPriceResponse generateFailureResponse(String errorMessage) {
        GetTaxRateInfoBySUPCStateSellerPriceResponse response = new GetTaxRateInfoBySUPCStateSellerPriceResponse();
        response.setCode("FAILED");
        response.setSuccessful(false);
        response.setMessage(errorMessage);
        return response;
    }

    private GetTaxRateInfoBySUPCStateSellerPriceResponse generateSuccessResponse(Double taxRate) {
        GetTaxRateInfoBySUPCStateSellerPriceResponse response = new GetTaxRateInfoBySUPCStateSellerPriceResponse();
        response.setCode("SUCCESS");
        response.setSuccessful(true);
        response.setTax(taxRate);
        return response;
    }

    @Override
    public GetTaxInfoResponse getTaxInfoBySellerSUPCState(GetTaxInfoRequest request) {
        LOG.info("Request received {}", request);
        ValidationError error = requestValidationService.requestBasicFieldValidation(request);
        if (error != null) {
            GetTaxInfoResponse response = new GetTaxInfoResponse();
            LOG.error("Validation error found request {}", request);
            responseBuilder.buildFailedResponse(error, response);
            return response;
        }

        return taxRateService.getTaxInfoBySellerSUPCState(request);
    }

    @Override
    public GetTaxClassResponse getTaxClassBySupcSubcatPairList(GetTaxClassRequest request) {
        LOG.info("Request received {}", request);
        GetTaxClassResponse response = new GetTaxClassResponse();
        response.setProtocol(request.getResponseProtocol());
        ValidationError error = requestValidationService.requestBasicFieldValidation(request);
        if (error != null) {
            LOG.error("Validation error found request {}", request);
            responseBuilder.buildFailedResponse(error, response);
            return response;
        }

        if (ConfigUtils.getIntegerScalar(Property.MAX_BATCH_SIZE_FOR_TAX_CLASS) < request.getSupcSubcatPairList().size()) {
            LOG.error("request size {} more than allowed batch request size {}", request.getSupcSubcatPairList().size(),
                    ConfigUtils.getIntegerScalar(Property.MAX_BATCH_SIZE_FOR_TAX_CLASS));
            response.setMessage(ApiErrorCode.REQUEST_BATCH_SIZE_LIMIT_EXCEEDED.toString() + "  Allowed Batch Size "
                    + ConfigUtils.getIntegerScalar(Property.MAX_BATCH_SIZE_FOR_TAX_CLASS));
            response.setSuccessful(false);
            return response;
        }

        Map<SupcSubcatPair, String> supcSubcatPairToTaxClass = new HashMap<SupcSubcatPair, String>();
        String taxClass = null;
        int successCount = 0;
        for (SupcSubcatPair pair : request.getSupcSubcatPairList()) {
            try {
                taxClass = null;
                taxClass = taxClassService.getTaxClassBySupcSubcatPair(pair.getSupc(), pair.getSubcat());
            } catch (ExternalDataNotFoundException e) {
                LOG.error("Exeption in get Tax Class ", e);
            }
            if (null == taxClass) {
                response.addFailures(new ValidationError(ApiErrorCode.REQUESTED_DATA_DOES_NOT_EXISTS.code(), "Tax Class can not be determined"), pair);
            } else {
                supcSubcatPairToTaxClass.put(pair, taxClass);
                successCount++;
            }

        }

        response.setSupcSubcatToTaxClass(supcSubcatPairToTaxClass);
        response.setSuccessCount(successCount);
        LOG.info("Sending response {}", response);
        return response;
    }
}
