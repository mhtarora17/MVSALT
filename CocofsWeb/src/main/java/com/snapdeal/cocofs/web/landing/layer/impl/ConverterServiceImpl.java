package com.snapdeal.cocofs.web.landing.layer.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.cocofs.excelupload.ProductAttributeUploadDTO;
import com.snapdeal.cocofs.mongo.model.ProductAttributeUnit;
import com.snapdeal.cocofs.sro.ProductFulfilmentAttributeSRO;
import com.snapdeal.cocofs.utils.DangerousGoodsTypeConversionUtil;
import com.snapdeal.cocofs.web.landing.layer.IConverterService;

@Service("converterService")
public class ConverterServiceImpl implements IConverterService {

    @Autowired
    private com.snapdeal.cocofs.services.converter.IConverterService converterService;

    @Override
    public ProductAttributeUploadDTO getProductAttributeUploadDTO(ProductFulfilmentAttributeSRO attribute) {
        ProductAttributeUploadDTO dto = null;
        if (attribute != null) {
            dto = new ProductAttributeUploadDTO();
            dto.setBreadth(attribute.getBreadth());
            dto.setFragile(attribute.isFragile());
            dto.setDangerousGoodsType(DangerousGoodsTypeConversionUtil.getDangerousGoodsType(attribute.getDangerousGoodsType(), attribute.isHazMat(), attribute.isLiquid()));
            dto.setHeight(attribute.getHeight());
            dto.setLength(attribute.getLength());
            if (attribute.getProductParts() == null) {
                dto.setProductParts(1); // by default it is set to 1.
            } else {
                dto.setProductParts(attribute.getProductParts());
            }
            //dto.setSerialized(attribute.isSerialized());
            dto.setSupc(attribute.getSupc());
            dto.setWeight(attribute.getWeight());
            dto.setSystemWeightCaptured(attribute.isSystemWeightCaptured());
            dto.setWoodenPackaging(attribute.isWoodenPackaging());
            dto.setSerializedType(attribute.getSerializedType());
            dto.setPackagingType(attribute.getPackagingType());
            dto.setPrimaryLength(attribute.getPrimaryLength());
            dto.setPrimaryBreadth(attribute.getPrimaryBreadth());
            dto.setPrimaryHeight(attribute.getPrimaryHeight());

        }

        return dto;
    }

    @Override
    public ProductFulfilmentAttributeSRO getProductAttributeSro(ProductAttributeUnit paUnit) {
        ProductFulfilmentAttributeSRO sro = null;
        if (paUnit != null) {
            sro = new ProductFulfilmentAttributeSRO();
            sro.setBreadth(paUnit.getBreadth());
            sro.setFragile(paUnit.isFragile());
            if (StringUtils.isEmpty(paUnit.getDangerousGoodsType())) {
                sro.setHazMat(paUnit.isHazMat());
                sro.setLiquid(paUnit.isLiquid());
            } else {
                sro.setHazMat(DangerousGoodsTypeConversionUtil.checkIfHazmat(paUnit.getDangerousGoodsType()));
                sro.setLiquid(DangerousGoodsTypeConversionUtil.checkIfLiquid(paUnit.getDangerousGoodsType()));
            }
            sro.setHeight(paUnit.getHeight());
            sro.setLength(paUnit.getLength());
            sro.setPrimaryBreadth(paUnit.getPrimaryBreadth());
            sro.setPrimaryHeight(paUnit.getPrimaryHeight());
            sro.setPrimaryLength(paUnit.getPrimaryLength());
            sro.setProductParts(paUnit.getProductParts());
            sro.setSerialized(paUnit.isSerialized());
            sro.setSupc(paUnit.getSupc());
            sro.setWeight(paUnit.getWeight());
            sro.setSystemWeightCaptured(paUnit.isSystemWeightCaptured());
            sro.setWoodenPackaging(paUnit.isWoodenPackaging());
            sro.setDangerousGoodsType(DangerousGoodsTypeConversionUtil.getDangerousGoodsType(paUnit.getDangerousGoodsType(), paUnit.isHazMat(), paUnit.isLiquid()));
            sro.setSerializedType(paUnit.getSerializedType());
            sro.setPackagingType(paUnit.getPackagingType());
        }

        return sro;
    }

    @Override
    public ProductAttributeUploadDTO getProductAttributeUploadDTO(String supc, Double weight, Boolean systemWeightCaptured) {
        ProductAttributeUploadDTO dto = new ProductAttributeUploadDTO();
        dto.setSupc(supc);
        dto.setWeight(weight);
        dto.setSystemWeightCaptured(systemWeightCaptured);

        return dto;
    }
    
  
}
