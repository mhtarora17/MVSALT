/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 18-Sep-2013
 *  @author himanshum
 */
package com.snapdeal.cocofs.web.controlled;

import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;
import com.netflix.hystrix.HystrixCommandKey;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;

public class GenericCommand {
    
    public static <S, Q> S controlledExecute(final Q req, final Execution<Q, S> execution, final String group, final String name ) {

        if (! ConfigUtils.getBooleanScalar(Property.USE_HYSTRIX_FOR_GENERIC_COMMAND)) {
            // Do not use hystrix if flag off
            return execution.execute(req);
        }
        
        // Else create dynamically a Hystrix Command and execute it
        class CommandClass extends HystrixCommand<S> {

            public CommandClass(String groupKey ) {
                super(HystrixCommandGroupKey.Factory.asKey(groupKey));
            }
            
            public CommandClass(String groupKey, String name ) {
                super(Setter.withGroupKey(HystrixCommandGroupKey.Factory.asKey(groupKey))
                        .andCommandKey(HystrixCommandKey.Factory.asKey(name)));
                
            }


            @Override
            protected S run() throws Exception {
                return execution.execute(req);
            }
            
        }
        return new CommandClass(group, name).execute();
        

        
    }



}
