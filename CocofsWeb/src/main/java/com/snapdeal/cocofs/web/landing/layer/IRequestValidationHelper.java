package com.snapdeal.cocofs.web.landing.layer;

import java.util.List;

public interface IRequestValidationHelper {

    public <T> List<String> checkForNullFields(T t) throws IllegalArgumentException, IllegalAccessException;

    public <T> boolean batchRequestSizeAllowed(T t) throws IllegalArgumentException, IllegalAccessException;
    
    public <T> List<String> checkForZeroFields(T t) throws IllegalArgumentException, IllegalAccessException;

    public <T> List<String> checkForNegativeFields(T t) throws IllegalArgumentException, IllegalAccessException;

}
