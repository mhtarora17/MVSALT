package com.snapdeal.cocofs.web.landing.layer.impl;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.base.validation.ValidationError;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.external.service.exception.ExternalDataNotFoundException;
import com.snapdeal.cocofs.model.request.GetFMAndZonesBySellerSupcListRequest;
import com.snapdeal.cocofs.model.request.GetZonesBySellerSupcListRequest;
import com.snapdeal.cocofs.model.response.ApiErrorCode;
import com.snapdeal.cocofs.model.response.GetFMAndZonesBySellerSupcListResponse;
import com.snapdeal.cocofs.model.response.GetZonesBySellerSupcListResponse;
import com.snapdeal.cocofs.services.ISellerZoneService;
import com.snapdeal.cocofs.services.fm.IFulfillmentCenterService;
import com.snapdeal.cocofs.services.fm.IFulfilmentModelService;
import com.snapdeal.cocofs.sro.FMZonePair;
import com.snapdeal.cocofs.sro.FulfillmentModel;
import com.snapdeal.cocofs.sro.SellerSUPCPair;
import com.snapdeal.cocofs.sro.ZoneSRO;
import com.snapdeal.cocofs.web.builder.IResponseBuilder;
import com.snapdeal.cocofs.web.landing.layer.ICocofsZoneLandingService;
import com.snapdeal.cocofs.web.landing.layer.IRequestValidationService;

/**
 * @author ankur
 */
@Service("cocofsZoneLandingService")
public class CocofsZoneLandingServiceImpl implements ICocofsZoneLandingService {

  
    @Autowired
    private IResponseBuilder                                                               responseBuilder;

    @Autowired
    private IRequestValidationService                                                      requestValidationService;

  
    @Autowired
    private IFulfilmentModelService                                                        fulfilmentModelService;

    
    @Autowired
    IFulfillmentCenterService                                                              fulfillmentCenterService; 
    
    @Autowired
    ISellerZoneService                                                                     sellerZoneService;         
    
     
    private static final Logger                                                            LOG = LoggerFactory.getLogger(CocofsZoneLandingServiceImpl.class);

   

 
    @Override
    public GetZonesBySellerSupcListResponse getZonesBySellerSupcList(GetZonesBySellerSupcListRequest request){

        LOG.debug("Request received {}", request);
        GetZonesBySellerSupcListResponse response = new GetZonesBySellerSupcListResponse();
        response.setProtocol(request.getResponseProtocol());
        ValidationError error = requestValidationService.requestBasicFieldValidation(request);
        if (error != null) {
            LOG.error("Validation error found request {}", request);
            responseBuilder.buildFailedResponse(error, response);
        } else if (!ConfigUtils.getBooleanScalar(Property.SHIP_NEAR)) {
            LOG.error("ship near flag is set off , hence not processing the request");
            response.setSuccessful(false);
            response.setMessage("Ship near flag is set off");
        }  else if (request.getSellerSupcPairs().size() > ConfigUtils.getIntegerScalar(Property.ZONE_API_BATCH_SIZE)) {
            LOG.error("seller-supc pairs size is more than the allowed batch size for request {}", request);
            responseBuilder.buildFailedResponse(ApiErrorCode.REQUEST_BATCH_SIZE_LIMIT_EXCEEDED, response);
        }else {
            Map<SellerSUPCPair, ZoneSRO> sellerSupcToZone = new HashMap<SellerSUPCPair, ZoneSRO>();
            ZoneSRO zoneSRO = null;
            int successCount = 0;
            for (SellerSUPCPair pair : request.getSellerSupcPairs()) {
                try {
                    zoneSRO = null;
                    zoneSRO = sellerZoneService.getZoneForSellerSupc(pair.getSellerCode(), pair.getSupc());
                } catch (ExternalDataNotFoundException e) {
                    LOG.error("Exeption in getZoneForSellerSupc ", e);
                } catch(Exception ex){
                	LOG.error("Internal server error occurred.",ex.getMessage());
                }
          
                if (null == zoneSRO) {
                    response.addFailures(new ValidationError(ApiErrorCode.REQUESTED_DATA_DOES_NOT_EXISTS.code(), "Zone can not be determined"), pair);
                } else {
                    sellerSupcToZone.put(pair, zoneSRO);
                    successCount++;
                }

            }

            if (successCount > 0) {
                response.setSellerSupcToZone(sellerSupcToZone);
                response.setSuccessCount(successCount);
            } else {
                responseBuilder.buildFailedResponse(ApiErrorCode.REQUESTED_DATA_DOES_NOT_EXISTS, response);
            }
        }

        LOG.debug("Sending response {}", response);

        return response;
    
    }
    
    @Override
    public GetFMAndZonesBySellerSupcListResponse getFMAndZonesBySellerSupcList(GetFMAndZonesBySellerSupcListRequest request) {
        LOG.debug("Request received {}", request);
        GetFMAndZonesBySellerSupcListResponse response = new GetFMAndZonesBySellerSupcListResponse();
        response.setProtocol(request.getResponseProtocol());
        ValidationError error = requestValidationService.requestBasicFieldValidation(request);
        if (error != null) {
            LOG.error("Validation error found request {}", request);
            responseBuilder.buildFailedResponse(error, response);

        } else if (!ConfigUtils.getBooleanScalar(Property.SHIP_NEAR)) {
            LOG.error("ship near flag is set off , hence not processing the request");
            response.setSuccessful(false);
            response.setMessage("Ship near flag is set off");
        }else if (request.getSellerSupcPairs().size() > ConfigUtils.getIntegerScalar(Property.ZONE_FM_API_BATCH_SIZE)) {
            LOG.error("seller-supc pairs size is more than the allowed batch size for request {}", request);
            responseBuilder.buildFailedResponse(ApiErrorCode.REQUEST_BATCH_SIZE_LIMIT_EXCEEDED, response);
        } else {
            Map<SellerSUPCPair, FMZonePair> sellerSupcToFMAndZone = new HashMap<SellerSUPCPair, FMZonePair>();
            FulfillmentModel fulfilmentModel = null;
            ZoneSRO zoneDetail = null;
            int successCount = 0;
            for (SellerSUPCPair pair : request.getSellerSupcPairs()) {
                try {
                    fulfilmentModel = null;
                    zoneDetail = null;
                    
                    fulfilmentModel = fulfilmentModelService.getFulfilmentModel(pair.getSellerCode(), pair.getSupc());
                    
                    zoneDetail =  sellerZoneService.getZoneForSellerSupc(pair.getSellerCode(), pair.getSupc());

                } catch (ExternalDataNotFoundException e) {
                    LOG.error("Exeption in getFMAndZoneBySellerSupcList ", e);
                } catch(Exception ex){
                	LOG.error("Internal server error occurred. {}",ex.getMessage());
                }
                
                
                if (null == fulfilmentModel &&  null == zoneDetail ) {
                    response.addFailures(new ValidationError(ApiErrorCode.REQUESTED_DATA_DOES_NOT_EXISTS.code(), "Fulfillment model And Zone can not be determined"), pair);
                }else if (null == fulfilmentModel){
                    response.addFailures(new ValidationError(ApiErrorCode.REQUESTED_DATA_DOES_NOT_EXISTS.code(), "Fulfillment model can not be determined"), pair);                   
                }else if (null == zoneDetail){
                    response.addFailures(new ValidationError(ApiErrorCode.REQUESTED_DATA_DOES_NOT_EXISTS.code(), "Zone can not be determined"), pair);                   
                }

                //if either of zone or fm is not null send the data.
                if(null != fulfilmentModel || null != zoneDetail){
              
                    FMZonePair fmZonePair = new FMZonePair();
                    fmZonePair.setFulfilmentModel(fulfilmentModel.getCode());
                    fmZonePair.setZoneDetails(zoneDetail);
                    sellerSupcToFMAndZone.put(pair, fmZonePair);
                    successCount++;
                }

            }

            if (successCount > 0) {
                response.setSellerSupcToFMAndZone(sellerSupcToFMAndZone);
                response.setSuccessCount(successCount);
            } else {
                responseBuilder.buildFailedResponse(ApiErrorCode.REQUESTED_DATA_DOES_NOT_EXISTS, response);
            }
        }

        LOG.debug("Sending response {}", response);

        return response;
    }
	
}
