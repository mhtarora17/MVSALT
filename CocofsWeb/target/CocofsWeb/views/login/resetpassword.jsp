<%@ include file="/tld_includes.jsp"%>

<tiles:insertTemplate template="/views/layout/loginBase.jsp">

	<tiles:putAttribute name="title" value="Cocofs - Reset Your Password"/>
	
	<tiles:putAttribute name="deferredScript">
   	<script>
	function cancelResetPwd(url){
			window.location.href =  url;
	}
	
	function validateInput() {
		var npwd  = $('#newPassword').val();
		if ( npwd.length < 6) {
			alert('Password should be at least 6 characters long');
			return false;
		}
		var cpwd =  $('#confirmPassword').val();
		if ( npwd != cpwd ) {
			alert('Passwords are different in Confirm and New Password box');
			return false;
		}
		if ( $('#oldPassword').length > 0 ) {
			if ($('#oldPassword').val().length <= 0 ) {
				alert('Old password not given ');
				return false;
			}
		}
		return true;
		
	}
	function submitForm() {
		var valid = validateInput();
		if ( valid ) {
			$('#resetPassword').submit();
		}
	}

   	$(document).ready(function() {
   		
   		// validate signup form on keyup and submit
   		$("#frm-submit").click(submitForm);
		
	});
   	</script>
   </tiles:putAttribute>
	
   <tiles:putAttribute name="body">
	<div align="center">
		<div id="internal-content">
	  	<div class="signupBoxDiv">
	  		<div class="register_head" >Reset Password</div>
	  	<div class="resetpass">
			<form id="resetPassword" method="post" action="${path.http}/changePassword"> <br/>
			<sec:authorize access="isAuthenticated()">
			<input type="hidden" name="validateOldPassword" value="true"/>
			<div class="cus_info_wrap">
				<label class="labelTop">Old Password *</label>
				<input type="password" name="oldPassword" id="oldPassword" class="input" />
				<c:if test="${passwordUpdateFailed}">
					<div id="pwdUpdatedMessage" style="color : red">Wrong old password</div>
				</c:if>
			</div> <br/>
			</sec:authorize>
	     	<div class="cus_info_wrap">
				<label class="labelTop">New Password *</label>
				<input type="password" name="newPassword" id="newPassword" class="input" />
			</div> <br/>
			<div class="cus_info_wrap">
				<label class="labelTop"> Confirm Password *</label>
				<input type="password" name="confirmPassword" id="confirmPassword" class="input" />
			</div> <br/>
			<input type="hidden" name="email" value="${email}"/>
			<sec:authorize access="isAnonymous()">
				<input type="hidden" name="code" value="${param['code']}"/>
				<input type="hidden" name="targetUrl" value="${param['targetUrl']}"/> 
			</sec:authorize>
			<table>
				<tr>
					<td>
						<input type="button" id="frm-submit" value="Submit"  class="button"/>
					</td>
					<td>
						<input type="button" value="Cancel" class="button" onclick="cancelResetPwd('/login')" />  <!--  redirecting to home page of the user, /login would lead to home page of user, if he's logged in -->
					</td>
				</tr>
			</table>
			<br/>
			</form>
		 </div>
		</div>
		</div>
	</div>	
	
   </tiles:putAttribute>
</tiles:insertTemplate>