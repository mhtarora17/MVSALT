/**
 *  supports download to csv.
 */

function jqGridDownloadAllData(table){
    var str = '';
	var headerLine = getHeaderRow(table);
	var dataRows = getDataRows(table);
	str+= headerLine  + '\r\n' + dataRows +'\r\n';
    window.open('data:text/csv;charset=utf-8,' + escape(str));
} 

/**
 * 
 * @param table
 * 
 * returns only that data which is visible in jqgrid table.
 */

function jqGridDownloadVisibleData(table){
	var str = jqGridGetVisibleRowsAsCSVString(table);
    window.open('data:text/csv;charset=utf-8,' + str);
}

function jqGridGetVisibleRowsAsCSVString(table) {
    var str = '';
    var headerLine = getHeaderRow(table);
    var dataRows = getVisibleRows(table);
    str+= headerLine  + '\r\n' + dataRows +'\r\n';
    return escape(str);
}

/**
 * 
 * @param table
 * @returns {header row}
 */
function getHeaderRow(table){
	var colarray = table.jqGrid('getGridParam','colNames');
	var headerLine = '';
	for(var j = 1 ; j < colarray.length ; j++){
		if(headerLine != '') headerLine += ','
			headerLine += colarray[j];
 	}
	return headerLine;
}

/**
 * 
 * @param table
 * @returns {dataRows}
 */
function getDataRows(table){
	var objArray = table.jqGrid('getGridParam','data');
	var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
	var rowData = '';
	
	for (var index = 0; index < array.length; index++) {
    	var line = '';
		for (var newindex in array[index]) {
			if(line != '') line += ','
            	line += array[index][newindex];	
        }	
		rowData += line + '\r\n';
    }
	return rowData;
}

/**
 * 
 * @param table
 * @returns {visible rows}
 */

function getVisibleRows(table){
	
	var idArray = table.getDataIDs();
	var array = typeof idArray != 'object' ? JSON.parse(idArray) : idArray;
	var visibleRows = '';
	var colProp = table.getGridParam('colModel');
	
	for (var index = 0; index < array.length; index++) {
    	var line = '';
    	var rowObject = table.getRowData(array[index]);
    	if(rowObject != null){
    		for(var colIndex = 1 ; colIndex < colProp.length ;  colIndex++){
    			if(line != ''){
    				line += ','
    			}
    			var colName = colProp[colIndex]['index'];
                line += rowObject[colName];	
    		}
    	}
    	visibleRows += line + '\r\n';
    }
	return visibleRows;
}