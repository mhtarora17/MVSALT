/*
 *  
Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 19-Apr-2013
 *  @author abhinav singhal
 */
package com.snapdeal.cocofs.commonweb.utils.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class JqgridResponse<T> implements Serializable {

    private static final long serialVersionUID = -1526503153618805194L;

    private String            page;
    private Integer           total;
    private String            records;
    private List<T>           rows             = new ArrayList<T>();

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getRecords() {
        return records;
    }

    public void setRecords(String records) {
        this.records = records;
    }

    public List<T> getRows() {
        return rows;
    }

    public void setRows(List<T> rows) {
        this.rows = rows;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

}
