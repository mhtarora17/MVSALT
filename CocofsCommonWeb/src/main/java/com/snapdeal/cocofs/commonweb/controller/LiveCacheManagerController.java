/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Nov 6, 2013
 *  @author himanshu
 */
package com.snapdeal.cocofs.commonweb.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.annotation.XmlRootElement;

import net.sf.ehcache.Cache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.stereotype.Controller;

import com.google.gson.Gson;
import com.snapdeal.cocofs.cache.EhCacheStatsManager;
import com.snapdeal.cocofs.cache.EhCacheStatsManager.CacheStats;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
@Controller("liveCacheManagerController")
@Path("/cache")
public class LiveCacheManagerController {
    /**
     */
    private static final Logger                   LOGGER = LoggerFactory.getLogger(LiveCacheManagerController.class);
    /**
     * {@link EhCacheCacheManager} instance retrieved from spring context.
     */
    @Autowired
    private EhCacheCacheManager                   ehCacheCacheManager;
    /**
     * {@link EhCacheStatsManager} instance  to get Cache  statistics for given cache.  
     */
    @Autowired
    private EhCacheStatsManager                   ehCacheStatsManager;

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{cacheAccessKey}/get")
    @GET
    public String getCaches(@PathParam(value = "cacheAccessKey") String cacheAccessKey) {
        CacheManagerMessage message = authenticate(cacheAccessKey);
        if(message.code == 200) {
            String[] caches = ehCacheCacheManager.getCacheManager().getCacheNames();
            return new Gson().toJson(caches);
        }
        return new Gson().toJson(message);
    }
    
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{cacheAccessKey}/remove/{cache}/{key}")
    @GET
    public String removeElement(@PathParam(value = "cacheAccessKey") String cacheAccessKey, @PathParam(value = "cache") String cache, 
            @PathParam(value = "key") String key) {
        CacheManagerMessage message = authenticate(cacheAccessKey);
        if(message.code == 200) {
            Cache cacheData = ehCacheCacheManager.getCacheManager().getCache(cache);
            if(cacheData != null) {
                cacheData.removeQuiet(key);
                message.setMessage("Cache data removed successfully");
                LOGGER.info("Cache data removed for cache {} and key {}", cache, key);
            } else {
                message.setMessage("Invalid cache name specified");
            }
        }
        return new Gson().toJson(message);
    }
    
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{cacheAccessKey}/getStats/{cache}")
    @GET
    public String getStats(@PathParam(value = "cacheAccessKey") String cacheAccessKey, @PathParam(value = "cache") String cache) {
        CacheManagerMessage message = authenticate(cacheAccessKey);
        if(message.code == 200) {
            CacheStats stats = ehCacheStatsManager.getCacheStats(cache);
            if(stats != null) {
                return new Gson().toJson(stats);
            } else {
                message.setMessage("Invalid cache name specified");
            }
        }
        return new Gson().toJson(message);
    }
    
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{cacheAccessKey}/getStats")
    @GET
    public String getStats(@PathParam(value = "cacheAccessKey") String cacheAccessKey) {
        CacheManagerMessage message = authenticate(cacheAccessKey);
        if(message.code == 200) {
            String[] caches = ehCacheCacheManager.getCacheManager().getCacheNames();
            List<CacheStats> cacheStats = new ArrayList<CacheStats>();
            for (String cacheName : caches) {
                CacheStats stats = ehCacheStatsManager.getCacheStats(cacheName);
                if(stats != null) {
                    cacheStats.add(stats);
                }
            }
            return new Gson().toJson(cacheStats);
        }
        return new Gson().toJson(message);
    }
    
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{cacheAccessKey}/clear/{cache}")
    @GET
    public String clearCache(@PathParam(value = "cacheAccessKey") String cacheAccessKey, @PathParam(value = "cache") String cache) {
        CacheManagerMessage message = authenticate(cacheAccessKey);
        if(message.code == 200) {
            Cache cacheData = ehCacheCacheManager.getCacheManager().getCache(cache);
            if(cacheData != null) {
                cacheData.removeAll(true);
                LOGGER.info("Cache {} cleared", cacheData);
            } else {
                message.setMessage("Invalid cache name specified");
            }
        }
        return new Gson().toJson(message);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{cacheAccessKey}/getObject/{cache}/{key}")
    @GET
    public String getData(@PathParam(value = "cacheAccessKey") String cacheAccessKey, @PathParam(value = "cache") String cache, 
            @PathParam(value = "key") String key) {
        CacheManagerMessage message = authenticate(cacheAccessKey);
        if(message.code == 200) {
            Cache cacheData = ehCacheCacheManager.getCacheManager().getCache(cache);
            if(cacheData != null) {
                return new Gson().toJson(cacheData.getQuiet(key));
            } else {
                message.setMessage("Invalid cache name specified");
            }
        }
        return new Gson().toJson(message);
    }
    
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{cacheAccessKey}/getSize/{cache}")
    @GET
    public String getSize(@PathParam(value = "cacheAccessKey") String cacheAccessKey, @PathParam(value = "cache") String cache) {
        CacheManagerMessage message = authenticate(cacheAccessKey);
        if(message.code == 200) {
            Cache cacheData = ehCacheCacheManager.getCacheManager().getCache(cache);
            if(cacheData != null) {
                return new Gson().toJson(cacheData.getSize());
            } else {
                message.setMessage("Invalid cache name specified");
            }
        }
        return new Gson().toJson(message);
    }

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{cacheAccessKey}/getKeys/{cache}/{start}/{end}")
    @GET
    public String getKeys(@PathParam(value = "cacheAccessKey") String cacheAccessKey, @PathParam(value = "cache") String cache, 
            @PathParam(value = "start") int start, @PathParam(value = "end") int end) {
        CacheManagerMessage message = authenticate(cacheAccessKey);
        if(message.code == 200) {
            if(end < start || start == end) {
                message.setMessage("Please specify a valid range");
                return new Gson().toJson(message);
            }
            Cache cacheData = ehCacheCacheManager.getCacheManager().getCache(cache);
            if(cacheData != null) {
                List keys = cacheData.getKeysNoDuplicateCheck();
                if(keys != null) {
                    if(keys.size() > end){
                        return new Gson().toJson(keys.subList(start, end).toArray());
                    } else {
                        return new Gson().toJson(keys.subList(start, Math.min(keys.size(), end)).toArray());
                    }
                } else {
                    return Collections.EMPTY_LIST.toString();
                }
            } else {
                message.setMessage("Invalid cache name specified");
            }
        }
        return new Gson().toJson(message);
    }
    
     
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{cacheAccessKey}/resetStats/{cache}")
    @GET
    public String resetStats(@PathParam(value = "cacheAccessKey") String cacheAccessKey, @PathParam(value = "cache") String cache) {
        CacheManagerMessage message = authenticate(cacheAccessKey);
        if(message.code == 200) {
            Cache cacheData = ehCacheCacheManager.getCacheManager().getCache(cache);
            if(cacheData != null) {
                cacheData.clearStatistics();
                LOGGER.info("Cache {} statsReset", cacheData);
            } else {
                message.setMessage("Invalid cache name specified");
            }
        }
        return new Gson().toJson(message);
    }
    /**
     * Gets the size of the in memory for this cache. This method relies on calculating
     * Serialized sizes. If the Element values are not Serializable they will show as zero.
     * <p/>
     * Warning: This method can be very expensive to run. Allow approximately 1 second
     * per 1MB of entries. Running this method could create liveness problems
     * because the object lock is held for a long period
     * <p/>
     *
     * @return the approximate size of the memory store in bytes
     * @throws IllegalStateException
     * @author nikhil
     */
    
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{cacheAccessKey}/getSizeInMemory/{cache}")
    @GET
    public String getSizeInMemory(@PathParam(value = "cacheAccessKey") String cacheAccessKey, @PathParam(value = "cache") String cache) {
        CacheManagerMessage message = authenticate(cacheAccessKey);
        if(message.code == 200) {
            Cache cacheData = ehCacheCacheManager.getCacheManager().getCache(cache);
            if(cacheData != null) {
                return new Gson().toJson(cacheData.calculateInMemorySize());
            } else {
                message.setMessage("Invalid cache name specified");
            }
        }
        return new Gson().toJson(message);
    }
    
    /**
     * Validates the cache key for any unauthorised access. If authenticated a <code>200</code> response is 
     * returned and calling method can continue for further processing.<p>
     * For other status code simply the response {@link CacheManagerMessage} can be returned.
     * 
     * @param cacheAccessKey
     * @return
     */
    private CacheManagerMessage authenticate(String cacheAccessKey) {
        CacheManagerMessage message = new CacheManagerMessage();
        String cacheKey = ConfigUtils.getStringScalar(Property.CACHE_ACCESS_KEY);
        if(cacheKey.equals(cacheAccessKey)) {
            message.setCode(200);
        } else {
            message.setCode(403);
            message.setMessage("Unauthorised access");
        }
        return message;
    }
    
    /**
     * 
     * @author fanendra
     *
     */
    @XmlRootElement
    private static class CacheManagerMessage implements Serializable {
        /**
         * 
         */
        private static final long serialVersionUID = -1713919181076407152L;
        private int         code;
        private String      message;
        
        public int getCode() {
            return code;
        }
        
        public void setCode(int code) {
            this.code = code;
        }
        
        public String getMessage() {
            return message;
        }
        
        public void setMessage(String message) {
            this.message = message;
        }
    }
}