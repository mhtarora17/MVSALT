/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 05-Sep-2013
 *  @author himanshum
 */
package com.snapdeal.cocofs.commonweb.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.cocofs.commonweb.utils.WebContextUtils;

@Component
public class UsernameInterceptor extends HandlerInterceptorAdapter {

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        super.postHandle(request, response, handler, modelAndView);
        try {
            String username = WebContextUtils.getCurrentUserEmail();
            if (!StringUtils.isEmpty(username) && (null != modelAndView) && (!modelAndView.getViewName().startsWith("redirect:"))) {
                modelAndView.getModel().put("currentUser", username);
            }
        } catch (Exception e) {
            //Do nothing
        }
    }

}
