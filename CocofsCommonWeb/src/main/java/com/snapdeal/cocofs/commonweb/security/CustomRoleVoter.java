/**
 *  Copyright 2015 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */  
package com.snapdeal.cocofs.commonweb.security;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.springframework.security.access.AccessDecisionVoter;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.FilterInvocation;

import com.snapdeal.base.cache.CacheManager;
import com.snapdeal.base.utils.CollectionUtils;
import com.snapdeal.packman.cache.UrlRoleMappingCache;

/**
 *  This class will verify the access of a user for a particular task.
 *  @version     1.0, 07-Dec-2015
 *  @author indrajit
 */
public class CustomRoleVoter implements AccessDecisionVoter{
    
    private String rolePrefix = "ROLE_";

    public String getRolePrefix() {
        return rolePrefix;
    }

    public void setRolePrefix(String rolePrefix) {
        this.rolePrefix = rolePrefix;
    }

    @Override
    public boolean supports(ConfigAttribute attribute) {
	return true;
    }
    
    private boolean supportRole(String role){
	
	if(null != role && role.startsWith(getRolePrefix()))
	    return true;
	
	return false;
    }

    @Override
    public boolean supports(Class<?> clazz) {
	// TODO Auto-generated method stub
	return true;
    }

    @Override
    public int vote(Authentication authentication, Object object, Collection<ConfigAttribute> attributes) {
	if (null == object)
	    return ACCESS_DENIED;

	FilterInvocation fi = (FilterInvocation) object;
	String url = fi.getRequestUrl().split("\\?")[0]; 
	Map<String, List<String>> urlRoleMappingCache = getUserRoleMappingcache().getUrlRoleMap();
	if(null == urlRoleMappingCache)
	    return ACCESS_DENIED;
	List<String> roleList = urlRoleMappingCache.get(url);
	Collection<GrantedAuthority> grantedAuthoritiesList = authentication.getAuthorities();
	if (null == roleList || CollectionUtils.isEmpty(roleList) || CollectionUtils.isEmpty(grantedAuthoritiesList))
	    return ACCESS_DENIED;
	Collections.sort(roleList);
	for (GrantedAuthority auth : grantedAuthoritiesList) {
	    String role = auth.getAuthority();
	    if (supportRole(role))
		if (Collections.binarySearch(roleList, auth.getAuthority()) >= 0)
		    return ACCESS_GRANTED;
	    

	}

	// TODO Auto-generated method stub
	return ACCESS_DENIED;
    }
    
    private UrlRoleMappingCache getUserRoleMappingcache(){
	return CacheManager.getInstance().getCache(UrlRoleMappingCache.class);
    }
    
   

}
