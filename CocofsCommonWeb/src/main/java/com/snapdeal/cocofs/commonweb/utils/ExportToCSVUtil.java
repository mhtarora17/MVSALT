package com.snapdeal.cocofs.commonweb.utils;

import java.io.BufferedOutputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.snapdeal.cocofs.services.common.dto.TaxRateDownloadData;

public class ExportToCSVUtil {
	
	private static final String FILE_HEADER = "SELLERCODE,SUPC,TAXRATE,TAXRATEENABLED,SERVICETAX,SERVICETAXENABLED";
	private static final String COMMA_DELIMITER = ",";
	private static final String NEW_LINE_SEPARATOR = "\n";

	public static void generateCSVFileFromDTO(HttpServletResponse httpResponse, String fileName, List<TaxRateDownloadData> dtoList){
		httpResponse.setContentType("text/csv");
		httpResponse.setHeader("Content-disposition", "attachment; filename=" + fileName + ".csv");
		 try
		    {
		        OutputStream outputStream = httpResponse.getOutputStream();
		        OutputStream buffer = new BufferedOutputStream(outputStream);
		        ObjectOutput output = new ObjectOutputStream(buffer);
		        output.writeObject(dtoList);
		        outputStream.flush();
		        outputStream.close();
		    }
		    catch(Exception e)
		    {
		        System.out.println(e.toString());
		    }
	}

	
	

}
