/*
 *  Copyright 2010 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Aug 16, 2010
 *  @author rahul
 */
package com.snapdeal.cocofs.commonweb.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.snapdeal.base.cache.CacheManager;
import com.snapdeal.base.utils.DateUtils;
import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.cocofs.commonweb.utils.PathResolver;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.configurations.IStartupService;
import com.snapdeal.cocofs.core.metrics.Metrics;
import com.snapdeal.cocofs.core.metrics.reporter.MetricsReporter;

public class CocofsContextListener implements ServletContextListener {

    private static final Logger LOG = LoggerFactory.getLogger(CocofsContextListener.class);


    @Override
    public void contextInitialized(ServletContextEvent sce) {
        LOG.info("Context initialized event called...");
        if (StringUtils.isNotEmpty(System.getProperty("env"))) {
            WebApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(sce.getServletContext());
            IStartupService startupService = context.getBean(IStartupService.class);
            try {
                startupService.loadAll(true);
                if (ConfigUtils.getBooleanScalar(Property.LOAD_ACTIVE_MQ)) {
                    startupService.registerSellerSupcAssocicationQueueConsumer();
                }
                if (ConfigUtils.getBooleanScalar(Property.LOAD_ACTIVE_MQ_SEARCH_PUSH)) {
                    startupService.registerSellerSupcSDFulfilledAssocicationQueueProducer();
                }
                if (ConfigUtils.getBooleanScalar(Property.LOAD_ACTIVE_MQ_SELLER_SUPC_FM_FC_PUSH)) {
                    startupService.registerFCChangeQueueEventProducer();
                }
                if (ConfigUtils.getBooleanScalar(Property.LOAD_ACTIVE_MQ_SHIPFROM_PUSH)) {
                    startupService.registerShipFromChangeQueueEventProducer();
                }
                if (ConfigUtils.getBooleanScalar(Property.LOAD_ACTIVE_MQ_SDINSTANT_PUSH)) {
                    startupService.registerSdInstantChangeQueueEventProducer();
                }
                
            } catch (Exception e) {
                LOG.error("error while initializing application:", e);
            }
            sce.getServletContext().setAttribute("path", new PathResolver());
            sce.getServletContext().setAttribute("cache", CacheManager.getInstance());
            sce.getServletContext().setAttribute("dateUtils", new DateUtils());

            LOG.info("Settings up Metrics Reporting");
            sce.getServletContext().setAttribute(Metrics.REGISTRY_ATTRIBUTE, Metrics.getRegistry());
            MetricsReporter gReporter = new MetricsReporter();
            gReporter.reportToGraphite();
        } else {
            LOG.error("System property 'env' not found. EXITING!");
            throw new RuntimeException("Property 'env' not found");
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        // nothing needed here
    }

}
