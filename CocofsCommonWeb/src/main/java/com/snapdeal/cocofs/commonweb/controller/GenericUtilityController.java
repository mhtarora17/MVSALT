package com.snapdeal.cocofs.commonweb.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.fs.common.FSRuntimeException;
import com.snapdeal.fs.healthcheck.api.IHealthCheckManager;

@Controller
public class GenericUtilityController {

    @Autowired
    private IHealthCheckManager healthCheckManager;

    @RequestMapping("/checkSystemHealth")
    @ResponseBody
    public String checkSystemHealth() throws Exception {
        String OK = "200";

        if (!ConfigUtils.getBooleanScalar(Property.ENABLE_CHECK_HEALTH)) {
            return OK;
        }
        final Map<String, Boolean> healthOfAllSystems = healthCheckManager.getHealthOfAllSystems();

        for (Map.Entry<String, Boolean> nameToHealthEntry : healthOfAllSystems.entrySet()) {
            String systemName = nameToHealthEntry.getKey();
            boolean isHealthy = nameToHealthEntry.getValue();
            if (!isHealthy) {
                throw new FSRuntimeException(String.format("Health check failed for system [%s], data [%s]", systemName, healthOfAllSystems));
            }
        }

        return healthOfAllSystems.toString();

    }
}
