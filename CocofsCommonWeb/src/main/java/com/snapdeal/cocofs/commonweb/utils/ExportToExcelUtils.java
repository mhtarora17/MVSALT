/*
 *  Copyright 2012 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 11-Oct-2012
 *  @author prateek
 */
package com.snapdeal.cocofs.commonweb.utils;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.DVConstraint;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataValidation;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.DataValidation;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import com.snapdeal.base.cache.CacheManager;
import com.snapdeal.cocofs.annotation.IgnoreInDownloadTemplate;
import com.snapdeal.cocofs.cache.UploadSheetFieldDomainCache;
import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;

public class ExportToExcelUtils {

    private static final Logger LOG = LoggerFactory.getLogger(ExportToExcelUtils.class);

    /**
     * This method exports data to the excel sheet along with header. This is dependent on type of datalist passed.
     * Headers of the exported list will be given by the name of the fields defined in the <T>
     * 
     * @param <T>
     * @param httpResponse
     * @param dtoClass
     * @param reportName
     * @throws IOException
     * @throws ClassNotFoundException
     */

    @SuppressWarnings("rawtypes")
    public static <T> void exportToExcel(HttpServletResponse httpResponse, Class dtoClass, String reportName, List<T> dataList) throws IOException, ClassNotFoundException {
        exportToExcel(httpResponse, dtoClass, reportName, dataList, true, true);
    }

    @SuppressWarnings("rawtypes")
    public static <T> void exportToExcel(HttpServletResponse httpResponse, Class dtoClass, String reportName, List<T> dataList, boolean hasSuperClass, boolean prepend)
            throws IOException, ClassNotFoundException {

        httpResponse.setContentType("application/vnd.ms-excel");
        httpResponse.setHeader("Content-disposition", "attachment; filename=" + reportName + ".xls");
        Field[] fields = getEligibleFields(dtoClass, hasSuperClass, prepend);
        HSSFWorkbook doc = getExcelDoc(dtoClass, fields, dataList, reportName);
        ServletOutputStream out = httpResponse.getOutputStream();
        doc.write(out);
        out.flush();
        out.close();
    }

    @SuppressWarnings("rawtypes")
    public static <T> void exportToCsv(HttpServletResponse httpResponse, Class dtoClass, String reportName, List<T> dataList,boolean hasSuperClass, boolean prepend)
            throws IOException{
        String csvFileName = reportName+".csv";
        httpResponse.setContentType("text/csv");
        String headerKey = "Content-Disposition";
        String headerValue = String.format("attachment; filename=\"%s\"", csvFileName);
        httpResponse.setHeader(headerKey, headerValue);
        ICsvBeanWriter csvWriter = new CsvBeanWriter(httpResponse.getWriter(), CsvPreference.STANDARD_PREFERENCE);
        
        
        Field[] fields = getEligibleFields(dtoClass, hasSuperClass, prepend);
        
        String[] fileHeader = new String[fields.length];
        String[] fieldNames = new String[fields.length];

        int i =0;
        for(Field field: fields){
            fileHeader[i] = field.getName().toUpperCase();
            fieldNames[i] = field.getName();
            i++;
        }
                
        csvWriter.writeHeader(fileHeader);

        
        for (T dto : dataList) {
            csvWriter.write(dto, fieldNames);
        }

        csvWriter.close();
        
    }
    @SuppressWarnings("rawtypes")
    private static Field[] getEligibleFields(Class dtoClass, boolean hasSuperClass, boolean prepend) {
        Set<Field> fieldSet = new LinkedHashSet<Field>();
        if (hasSuperClass) {
            if (prepend) {
                Collections.addAll(fieldSet, dtoClass.getSuperclass().getDeclaredFields());
                Collections.addAll(fieldSet, dtoClass.getDeclaredFields());
            } else {
                Collections.addAll(fieldSet, dtoClass.getDeclaredFields());
                Collections.addAll(fieldSet, dtoClass.getSuperclass().getDeclaredFields());
            }
        } else {
            Collections.addAll(fieldSet, dtoClass.getDeclaredFields());
        }
        Iterator<Field> itr = fieldSet.iterator();
        while (itr.hasNext()) {
            Field field = itr.next();
            if (ignoreField(field))
                itr.remove();
        }
        return fieldSet.toArray(new Field[0]);
    }

    private static boolean ignoreField(Field field) {
        boolean ignoreField = field.isAnnotationPresent(IgnoreInDownloadTemplate.class);
        // if annotation present
        if (ignoreField) {
            IgnoreInDownloadTemplate type = field.getAnnotation(IgnoreInDownloadTemplate.class);
            if (type != null) {
                boolean onValue = type.onValue();
                String pKey = type.propertyName();
                Property p = Property.getPropertyByKey(pKey);
                // override property is defined
                if (p != null) {
                    if (ConfigUtils.getBooleanScalar(p) == onValue) {
                        ignoreField = true;
                    } else {
                        ignoreField = false;
                    }
                }
            }
        }

        return ignoreField;
    }

    /**
     * This method exports data to excel. The DataList passed to this method contains data as well as headers. The first
     * element of the list gives you the header of the excel sheet rest elements contains data.
     * 
     * @param httpResponse
     * @param reportName
     * @param dataList
     * @throws IOException
     */
    public static void exportDataToExcel(HttpServletResponse httpResponse, String reportName, List<String> dataList) throws IOException {

        httpResponse.setContentType("application/vnd.ms-excel");
        httpResponse.setHeader("Content-disposition", "attachment; filename=" + reportName + ".xls");

        HSSFWorkbook doc = new HSSFWorkbook();
        HSSFSheet page = doc.createSheet();
        List<HSSFCellStyle> styles = styleExcelDoc(doc, page);
        HSSFRow row = page.createRow(0);

        String headString = dataList.get(0);
        String[] header = headString.split(",");
        for (int i = 0; i < header.length; i++) {
            HSSFCell cellHEAD = row.createCell(i);
            cellHEAD.setCellValue(header[i]);
            cellHEAD.setCellStyle(styles.get(0));
            page.setColumnWidth(i, 5000);
        }

        for (int i = 1; i < dataList.size(); i++) {
            row = page.createRow(i);
            String dataString = dataList.get(i);
            String[] data = dataString.split(",");
            for (int k = 0; k < data.length; k++) {
                HSSFCell cell = row.createCell(k);
                cell.setCellValue(data[k]);
                cell.setCellStyle(styles.get(1));
            }
        }

        ServletOutputStream out = httpResponse.getOutputStream();
        doc.write(out);
        out.flush();
        out.close();
    }

    private static List<HSSFCellStyle> styleExcelDoc(HSSFWorkbook doc, HSSFSheet page) {
        page.createFreezePane((short) 0, (short) 1);
        HSSFCellStyle style = doc.createCellStyle();
        HSSFFont font = doc.createFont();
        font.setColor(HSSFColor.BLACK.index);
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        style.setFont(font);
        style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        style.setBorderRight(HSSFCellStyle.BORDER_THIN);
        style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        style.setBorderTop(HSSFCellStyle.BORDER_THIN);
        style.setWrapText(true);
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style.setFillForegroundColor((new HSSFColor.GREY_25_PERCENT()).getIndex());
        HSSFCellStyle style1 = doc.createCellStyle();
        style1.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        style1.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        style1.setBorderRight(HSSFCellStyle.BORDER_THIN);
        style1.setWrapText(true);
        List<HSSFCellStyle> list = new ArrayList<HSSFCellStyle>();
        list.add(style);
        list.add(style1);
        return list;
    }

    @SuppressWarnings("rawtypes")
    private static <T> HSSFWorkbook getExcelDoc(Class dtoClass, Field[] fields, List<T> dataList, String reportName) {
        HSSFWorkbook doc = new HSSFWorkbook();
        DataFormat format = doc.createDataFormat();
        HSSFSheet page = doc.createSheet();
        doc.setSheetName(0, reportName);
        List<HSSFCellStyle> styles = styleExcelDoc(doc, page);
        int noOfColoums = fields.length;
        int rowCount = 1;
        HSSFRow row = page.createRow(0);

        HSSFCellStyle style = doc.createCellStyle();
        style.setDataFormat(format.getFormat("@"));
        for (int i = 0; i < noOfColoums; i++) {
            page.setDefaultColumnStyle(i, style);
        }
        for (int i = 0; i < noOfColoums; i++) {
            HSSFCell cell = row.createCell(i);
            cell.setCellType(HSSFCell.CELL_TYPE_STRING);
            cell.setCellValue(fields[i].getName().toUpperCase());
            cell.setCellStyle(styles.get(0));
            page.setColumnWidth(i, 5000);
            applyDropDownOnColumnCellsIfRequired(dtoClass, page, i, fields[i].getName());
        }

        if (null != dataList && dataList.size() > 0) {
            for (T data : dataList) {
                row = page.createRow(rowCount);
                rowCount++;
                for (int i = 0; i < noOfColoums; i++) {
                    HSSFCell cell = row.createCell(i);
                    try {
                        fields[i].setAccessible(true);
                        cell.setCellType(HSSFCell.CELL_TYPE_STRING);
                        if (fields[i].get(data) != null) {
                            cell.setCellValue(fields[i].get(data) + "");
                        }
                    } catch (IllegalArgumentException e) {
                        LOG.error("exception {} ", e);
                    } catch (IllegalAccessException e) {
                        LOG.error("exception {} ", e);
                    }
                    cell.setCellStyle(styles.get(1));
                    page.setColumnWidth(i, 5000);
                }
            }
        }
        return doc;

    }

    @SuppressWarnings("rawtypes")
    private static <T> void applyDropDownOnColumnCellsIfRequired(Class dtoClass, HSSFSheet page, int columnIndex, String fieldName) {
        CellRangeAddressList addressList = new CellRangeAddressList(1, -1, columnIndex, columnIndex);
        UploadSheetFieldDomainCache cache = CacheManager.getInstance().getCache(UploadSheetFieldDomainCache.class);
        if (null != cache) {
            List<String> constraintValues = cache.getValuesForClassField(dtoClass.getSimpleName(), fieldName);
            if (constraintValues != null && !constraintValues.isEmpty()) {
                String[] constraintValuesArray = constraintValues.toArray(new String[constraintValues.size()]);
                DVConstraint dvConstraint = DVConstraint.createExplicitListConstraint((String[]) constraintValuesArray);
                DataValidation dataValidation = new HSSFDataValidation(addressList, dvConstraint);
                dataValidation.setSuppressDropDownArrow(false);
                page.addValidationData(dataValidation);
            }

        }

    }

    public static <T> void downloadTemplate(HttpServletResponse httpResponse, Class<T> clazz, String reportName) throws IOException, ClassNotFoundException {
        exportToExcel(httpResponse, clazz, reportName, null);
    }
}
