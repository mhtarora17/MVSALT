/*
 *  Copyright 2010 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Sep 16, 2010
 *  @author singla
 */
package com.snapdeal.cocofs.commonweb.security;

import org.springframework.security.core.GrantedAuthority;

import com.snapdeal.cocofs.entity.Role;

public class CocofsAuthority implements GrantedAuthority {

    private static final long serialVersionUID = 68620155670337310L;
    private final String      role;

    public CocofsAuthority(Role role) {
        this.role = role.getCode();
    }

    @Override
    public String getAuthority() {
        return role;
    }

}
