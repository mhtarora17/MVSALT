/*

 *  Copyright 2010 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Aug 2, 2010
 *  @author singla
 */
package com.snapdeal.cocofs.commonweb.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.snapdeal.base.utils.StringUtils;
import com.snapdeal.base.utils.ValidatorUtils;
import com.snapdeal.cocofs.commonweb.controller.form.SignupForm;
import com.snapdeal.cocofs.commonweb.response.SystemResponse;
import com.snapdeal.cocofs.commonweb.response.SystemResponse.ResponseStatus;
import com.snapdeal.cocofs.commonweb.security.CocofsUser;
import com.snapdeal.cocofs.commonweb.utils.CocofsWebUtils;
import com.snapdeal.cocofs.commonweb.utils.PathResolver;
import com.snapdeal.cocofs.commonweb.utils.WebContextUtils;
import com.snapdeal.cocofs.db.users.IUserRoleService;
import com.snapdeal.cocofs.db.users.IUserService;
import com.snapdeal.cocofs.entity.PasswordVerification;
import com.snapdeal.cocofs.entity.User;
import com.snapdeal.cocofs.services.IEmailService;
import com.snapdeal.cocofs.utils.EncryptionUtils;

@Controller
public class LoginController {

    public static String        REDIRECT_PATH      = "spring-security-redirect";
    public static String        THIRD_PARTY_SOURCE = "facebookgoogleyahoo";

    @Autowired
    private IUserService        userService;

    @Autowired
    IUserRoleService            userRoleService;

    @Autowired
    IEmailService               emailService;

    private static final Logger LOG                = LoggerFactory.getLogger(LoginController.class);

    @RequestMapping("/login")
    public String loginPage(@RequestParam(value = "authenticated", required = false) Boolean authenticated, @RequestParam(value = "source", required = false) String source,
            @RequestParam(value = "targetUrl", required = false) String targetUrl, ModelMap model) {
        SignupForm signupForm = new SignupForm();
        if (StringUtils.isNotEmpty(source)) {
            signupForm.setSource(source);
        }
        if (StringUtils.isNotEmpty(targetUrl)) {
            signupForm.setTargetUrl(targetUrl);
        }
        model.addAttribute("signupForm", signupForm);

        CocofsUser user = WebContextUtils.getCurrentUser();
        if (user != null) {
            return "redirect:" + CocofsWebUtils.determineAutoRedirctUrl(user);
        }

        if (authenticated != null && !authenticated) {
            return "redirect:login?systemcode=508";
        } else {
            return "login/login";
        }
    }

    @RequestMapping("/forgotPassword")
    public @ResponseBody
    SystemResponse sendForgotPasswordEmail(@RequestParam("email") String email) {

        SystemResponse response = null;

        if (!ValidatorUtils.isEmailPatternValid(email)) {
            response = new SystemResponse(ResponseStatus.FAIL, email + " :invalid email");
        } else if (!userService.isPasswordChangeAllowed(email)) {
            response = new SystemResponse(ResponseStatus.FAIL, "");
        } else {
            User user = userService.getUserByEmail(email);
            if (user == null) {
                response = new SystemResponse(ResponseStatus.FAIL, email + "  :not a registered user");
            } else {
                PasswordVerification passwordVerification = userService.createPasswordVerification(email, "", "");
                LOG.info("Email verification code {} for password reset of user {}", passwordVerification.getVerificationCode(), email);
                emailService.sendForgotPasswordEmail(user, new PathResolver().getHttp(), new PathResolver().resources(""),
                        CocofsWebUtils.getEmailVerificationLink("resetPassword", user.getEmail(), passwordVerification));
                response = new SystemResponse(ResponseStatus.SUCCESS, "Please check your email to reset password");
            }
        }

        return response;
    }

    @RequestMapping("/resetPassword")
    public String resetPassword(@RequestParam(value = "email", required = false) String email, @RequestParam(value = "code", required = false) String code,
            @RequestParam(value = "targetUrl", required = false) String targetUrl, ModelMap map) {

        if (WebContextUtils.getCurrentUser() == null) {
            map.clear();
            User user = userService.getUserByEmail(email);
            map.addAttribute("email", user.getEmail());
            return userService.verifyUser(user, code) ? "login/resetpassword" : "redirect:/?systemcode=501";
        } else {
            return "redirect:/?systemcode=501";
        }
    }

    @RequestMapping("/updatePasswordLanding")
    public String returnUpdatePasswordPage(ModelMap map) {
        User user = WebContextUtils.getCurrentUser().getUser();
        map.clear();
        map.addAttribute("email", user.getEmail());
        return "login/resetpassword";
    }

    @RequestMapping("/changePassword")
    public String changePassword(@RequestParam(value = "oldPassword", required = false) String oldPassword, @RequestParam("newPassword") String newPassword,
            @RequestParam(value = "email", required = false) String email, @RequestParam(value = "code", required = false) String code,
            @RequestParam(value = "validateOldPassword", required = false) boolean validateOldPassword, @RequestParam(value = "targetUrl", required = false) String targetUrl,
            ModelMap map) {

        User user = null;
        user = userService.getUserByEmail(email);
        if (validateOldPassword) {
            if (!userService.verifyPassword(user, oldPassword)) {
                LOG.info("Input password do not matched with the user's actual password for the email {}", email);
                map.addAttribute("email", user.getEmail());
                map.addAttribute("passwordUpdateFailed", true);
                return "login/resetpassword";
            }
        }

        if (validateOldPassword || userService.verifyUser(user, code)) {
            userService.clearEmailVerificationCode(email);
            user.setPassword(EncryptionUtils.getMD5EncodedPassword(newPassword));
            userService.updateUser(user);
            map.addAttribute("passwordUpdated", true);
            return "redirect:/logout";
        }
        return "redirect:/?systemcode=501";
    }
}
