/*
 *  Copyright 2010 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Aug 21, 2010
 *  @author singla
 */
package com.snapdeal.cocofs.commonweb.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.MDC;
import org.springframework.web.filter.OncePerRequestFilter;

/**
 *@author ankur
 *
 */
public class MDCCleanupFilter extends OncePerRequestFilter {

   
    @Override
    protected void initFilterBean() throws ServletException {
        super.initFilterBean();
    }

    /**
     * It is used to clear MDC, which has been used to put requestId|appId in server logs.
     * The utility where it gets it's value is in SnapdealBase: Class# RequestContextProcessingAspect 
    **/
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
        
        try{
            chain.doFilter(request, response);
        }finally{
            MDC.clear();
        }
    }

   
}
