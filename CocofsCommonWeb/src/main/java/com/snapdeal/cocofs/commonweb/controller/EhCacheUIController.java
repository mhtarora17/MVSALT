/*
 *  Copyright 2013 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Nov 7, 2013
 *  @author himanshu
 */
package com.snapdeal.cocofs.commonweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping(EhCacheUIController.URL)
public class EhCacheUIController {
    public static final String    URL = "/admin/cache/";
    
    @RequestMapping("")
    public String defaultView() {
        return "redirect:cacheview";
    }
    
    @RequestMapping("cacheview")
    public String showCacheStatus( ModelMap model) {
        return "admin/admintasks/cacheview";
    }

}
