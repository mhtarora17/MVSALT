/*
 *  Copyright 2012 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, Dec 17, 2012
 *  @author Nikhil Rajpal
 */
package com.snapdeal.cocofs.commonweb.security;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.snapdeal.cocofs.db.users.IUserRoleService;
import com.snapdeal.cocofs.db.users.IUserService;
import com.snapdeal.cocofs.entity.Role;
import com.snapdeal.cocofs.entity.User;
import com.snapdeal.cocofs.entity.UserRole;

public class CustomUserDetailsService implements UserDetailsService {

    private static final Logger LOG = LoggerFactory.getLogger(CustomUserDetailsService.class);

    @Autowired
    private IUserService        userService;

    @Autowired
    private IUserRoleService    userRoleService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException, DataAccessException {
        User user = userService.getUserByEmail(username);
        if (user == null) {
            LOG.info("No user found with login username " + username);
            throw new UsernameNotFoundException("Invalid email", username);
        } else {
            List<Role> childRoles = new ArrayList<Role>();
            List<UserRole> userRoleMapping = userService.getAllUserRoleMappings(user);
            if (userRoleMapping == null) {
                LOG.info("User " + username + " does not have any roles defined ");
                throw new UsernameNotFoundException("Invalid user-role mapping", username);
            } else {
                for (UserRole userRole : userRoleMapping) {
                    if (userRole.isEnabled()) {
                        childRoles.addAll(userRoleService.getRoleTree(userRole.getRole()));
                    }
                }
            }
            CocofsUser cocofsUser = new CocofsUser(user, childRoles);
            return cocofsUser;
        }
    }
}
