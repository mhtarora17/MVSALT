/*
 *  Copyright 2012 Jasper Infotech (P) Limited . All Rights Reserved.
 *  JASPER INFOTECH PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *  
 *  @version     1.0, 11-Jul-2012
 *  @author prateek
 */
package com.snapdeal.cocofs.commonweb.controller;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.snapdeal.cocofs.configuration.ConfigUtils;
import com.snapdeal.cocofs.configuration.Property;
import com.snapdeal.cocofs.configurations.IStartupService;

/**
 * Supports cocofs cache reload via UI.
 * <p>
 * If no RequestParam <i>type</i> is passed, then it reloads entire cache by default. Else, reloads specific cache
 * depending upon the value of <i>type</i>.
 * 
 * @author abhinav
 */
@Controller
public class ReloadCacheController {

    private static final Logger LOG = LoggerFactory.getLogger(ReloadCacheController.class);

    @Autowired
    private IStartupService     startupService;

    @RequestMapping("/reloadcocofscache")
    @ResponseBody
    public String reloadCache(@RequestParam(required = false, value = "type") String type, @RequestParam(required = false, value = "pwd") String password) {
        try {
            if (StringUtils.trimToEmpty(ConfigUtils.getStringScalar(Property.CACHE_RELOAD_PASSWD)).equals(password)) {
                if (StringUtils.isEmpty(type)) {
                    LOG.info("Starting CoCoFS Cache Reload");
                    startupService.loadAll(true);
                    LOG.info("Cache loaded Successfully");
                } else if ("fileProperties".equalsIgnoreCase(type)) {
                    LOG.info("Starting CoCoFS Cache Reload for file properties");
                    startupService.loadProperties(true);
                    LOG.info("file properties Cache loaded Successfully");
                } else if ("registerConsumer".equalsIgnoreCase(type)) {
                    startupService.registerSellerSupcAssocicationQueueConsumer();
                } else if ("unregisterConsumer".equalsIgnoreCase(type)) {
                    startupService.unregisterSellerSupcAssocicationQueueConsumer();
                } else if ("registerSearchPushProducer".equalsIgnoreCase(type)) {
                    startupService.registerSellerSupcSDFulfilledAssocicationQueueProducer();
                } else if ("unregisterSearchPushProducer".equalsIgnoreCase(type)) {
                    startupService.unregisterSellerSupcSDFulfilledAssocicationQueueProducer();
                } else if ("registerFCUpdatePushProducer".equalsIgnoreCase(type)) {
                    startupService.registerFCChangeQueueEventProducer();
                } else if ("unregisterFCUpdatePushProducer".equalsIgnoreCase(type)) {
                    startupService.unregisterFCChangeQueueEventProducer();
                } else if ("registerShipFromUpdatePushProducer".equalsIgnoreCase(type)) {
                    startupService.registerShipFromChangeQueueEventProducer();
                }else if ("unregisterShipFromUpdatePushProducer".equalsIgnoreCase(type)) {
                    startupService.unregisterShipFromChangeQueueEventProducer();
                } else if ("registerSdInstantUpdatePushProducer".equalsIgnoreCase(type)) {
                    startupService.registerSdInstantChangeQueueEventProducer();
                } else if ("unregisterSdInstantUpdatePushProducer".equalsIgnoreCase(type)) {
                    startupService.unregisterSdInstantChangeQueueEventProducer();
                } else if ("reloadAerospikeConfig".equalsIgnoreCase(type)) {
                    startupService.loadAerospikeConfig(true);
                } else if ("reloadAerospikeLDTConfig".equalsIgnoreCase(type)) {
                    startupService.loaAerospikeLDTConfig(true);
                } else {
                    LOG.info("Starting cache reload for method name {}", type);
                    startupService.loadCacheByMethodName(type);
                    LOG.info("Finished invoking method");
                }
            }
        } catch (Exception e) {
            LOG.error("unable to reload cache.", e);
            return "unable to reload cache ... check logs for details";
        }
        return "Cache Reloaded Successfully";
    }
}
